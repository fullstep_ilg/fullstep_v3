VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{1E673DEF-C82F-4FBB-BAB9-77B0634E8F4D}#1.0#0"; "ProceSelector.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmOFERec 
   Caption         =   "Recepci�n de ofertas"
   ClientHeight    =   7500
   ClientLeft      =   2025
   ClientTop       =   3060
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmOFERec.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7500
   ScaleWidth      =   11880
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   510
      Index           =   0
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   11880
      TabIndex        =   102
      TabStop         =   0   'False
      Top             =   6990
      Visible         =   0   'False
      Width           =   11880
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Enabled         =   0   'False
         Height          =   345
         Index           =   0
         Left            =   1260
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Enabled         =   0   'False
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin VB.PictureBox picApartado 
      BackColor       =   &H00808000&
      ForeColor       =   &H00000000&
      Height          =   6495
      Index           =   4
      Left            =   0
      ScaleHeight     =   6435
      ScaleWidth      =   11220
      TabIndex        =   100
      Top             =   510
      Width           =   11275
      Begin SSDataWidgets_B.SSDBGrid sdbgPrecios 
         Height          =   5745
         Left            =   120
         TabIndex        =   95
         Top             =   600
         Width           =   11010
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         GroupHeadLines  =   2
         Col.Count       =   20
         stylesets.count =   4
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmOFERec.frx":014A
         stylesets(1).Name=   "Concoment"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmOFERec.frx":0166
         stylesets(1).AlignmentPicture=   1
         stylesets(2).Name=   "styEspAdjSi"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmOFERec.frx":01F9
         stylesets(2).AlignmentPicture=   1
         stylesets(3).Name=   "ITEMCERRADO"
         stylesets(3).ForeColor=   16777215
         stylesets(3).BackColor=   9408399
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmOFERec.frx":0276
         BevelColorHighlight=   16777215
         BevelColorShadow=   8421504
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         BalloonHelp     =   0   'False
         MaxSelectedRows =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   53
         ActiveRowStyleSet=   "Normal"
         SplitterPos     =   2
         SplitterVisible =   -1  'True
         Columns.Count   =   20
         Columns(0).Width=   1535
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   100
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777152
         Columns(1).Width=   2381
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   200
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777152
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ITEM"
         Columns(2).Name =   "ITEM"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1773
         Columns(3).Caption=   "Inicio"
         Columns(3).Name =   "INI"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16777152
         Columns(4).Width=   1746
         Columns(4).Caption=   "Fin"
         Columns(4).Name =   "FIN"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   16777152
         Columns(5).Width=   820
         Columns(5).Caption=   "Dest"
         Columns(5).Name =   "DEST"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   16777152
         Columns(6).Width=   767
         Columns(6).Caption=   "Uni"
         Columns(6).Name =   "UNI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   16777152
         Columns(7).Width=   1614
         Columns(7).Caption=   "Cantidad"
         Columns(7).Name =   "CANT"
         Columns(7).Alignment=   1
         Columns(7).CaptionAlignment=   2
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).NumberFormat=   "standard"
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   16777152
         Columns(8).Width=   1958
         Columns(8).Caption=   "Pres.unitario"
         Columns(8).Name =   "PREC"
         Columns(8).Alignment=   1
         Columns(8).CaptionAlignment=   2
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).NumberFormat=   "standard"
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   16777152
         Columns(9).Width=   900
         Columns(9).Caption=   "Pago"
         Columns(9).Name =   "PAG"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Locked=   -1  'True
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   16777152
         Columns(10).Width=   2143
         Columns(10).Caption=   "Precio1"
         Columns(10).Name=   "PREC1"
         Columns(10).Alignment=   1
         Columns(10).CaptionAlignment=   2
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).NumberFormat=   "standard"
         Columns(10).FieldLen=   256
         Columns(10).Locked=   -1  'True
         Columns(11).Width=   1058
         Columns(11).Caption=   "Comentario"
         Columns(11).Name=   "COMENT1"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Style=   4
         Columns(11).ButtonsAlways=   -1  'True
         Columns(12).Width=   2223
         Columns(12).Caption=   "Precio2"
         Columns(12).Name=   "PREC2"
         Columns(12).Alignment=   1
         Columns(12).CaptionAlignment=   2
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).NumberFormat=   "standard"
         Columns(12).FieldLen=   256
         Columns(12).Locked=   -1  'True
         Columns(13).Width=   1058
         Columns(13).Caption=   "Comentario"
         Columns(13).Name=   "COMENT2"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(13).Style=   4
         Columns(13).ButtonsAlways=   -1  'True
         Columns(14).Width=   1984
         Columns(14).Caption=   "Precio3"
         Columns(14).Name=   "PREC3"
         Columns(14).Alignment=   1
         Columns(14).CaptionAlignment=   2
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).NumberFormat=   "standard"
         Columns(14).FieldLen=   256
         Columns(14).Locked=   -1  'True
         Columns(15).Width=   1058
         Columns(15).Caption=   "Comentario"
         Columns(15).Name=   "COMENT3"
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   8
         Columns(15).FieldLen=   256
         Columns(15).Style=   4
         Columns(15).ButtonsAlways=   -1  'True
         Columns(16).Width=   1640
         Columns(16).Caption=   "Cant.max."
         Columns(16).Name=   "CANTMAX"
         Columns(16).Alignment=   1
         Columns(16).CaptionAlignment=   2
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).NumberFormat=   "standard"
         Columns(16).FieldLen=   256
         Columns(16).Locked=   -1  'True
         Columns(17).Width=   3200
         Columns(17).Visible=   0   'False
         Columns(17).Caption=   "CERRADO"
         Columns(17).Name=   "CERRADO"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(17).Locked=   -1  'True
         Columns(18).Width=   1984
         Columns(18).Caption=   "Usar"
         Columns(18).Name=   "USAR"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   8
         Columns(18).FieldLen=   256
         Columns(18).Locked=   -1  'True
         Columns(19).Width=   3200
         Columns(19).Visible=   0   'False
         Columns(19).Caption=   "ANYOIMPUTACION"
         Columns(19).Name=   "ANYOIMPUTACION"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(19).Locked=   -1  'True
         Columns(19).Style=   1
         _ExtentX        =   19420
         _ExtentY        =   10134
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPreciosEscalados 
         Height          =   5745
         Left            =   120
         TabIndex        =   209
         Top             =   600
         Width           =   11010
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   12
         stylesets.count =   5
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmOFERec.frx":0292
         stylesets(1).Name=   "Concoment"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmOFERec.frx":02AE
         stylesets(1).AlignmentPicture=   1
         stylesets(2).Name=   "HeadBold"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmOFERec.frx":0341
         stylesets(3).Name=   "styEspAdjSi"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmOFERec.frx":035D
         stylesets(3).AlignmentPicture=   1
         stylesets(4).Name=   "ITEMCERRADO"
         stylesets(4).ForeColor=   16777215
         stylesets(4).BackColor=   9408399
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmOFERec.frx":03DA
         UseGroups       =   -1  'True
         BevelColorHighlight=   16777215
         BevelColorShadow=   8421504
         MultiLine       =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         BalloonHelp     =   0   'False
         MaxSelectedRows =   0
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   53
         ActiveRowStyleSet=   "Normal"
         SplitterVisible =   -1  'True
         Groups(0).Width =   14049
         Groups(0).Columns.Count=   12
         Groups(0).Columns(0).Width=   1720
         Groups(0).Columns(0).Caption=   "C�digo"
         Groups(0).Columns(0).Name=   "COD"
         Groups(0).Columns(0).CaptionAlignment=   0
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Locked=   -1  'True
         Groups(0).Columns(0).HasBackColor=   -1  'True
         Groups(0).Columns(0).BackColor=   16777152
         Groups(0).Columns(1).Width=   2275
         Groups(0).Columns(1).Caption=   "Denominaci�n"
         Groups(0).Columns(1).Name=   "DEN"
         Groups(0).Columns(1).CaptionAlignment=   0
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(1).Locked=   -1  'True
         Groups(0).Columns(1).HasBackColor=   -1  'True
         Groups(0).Columns(1).BackColor=   16777152
         Groups(0).Columns(2).Width=   450
         Groups(0).Columns(2).Visible=   0   'False
         Groups(0).Columns(2).Caption=   "ITEM"
         Groups(0).Columns(2).Name=   "ITEM"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(3).Width=   1085
         Groups(0).Columns(3).Caption=   "Inicio"
         Groups(0).Columns(3).Name=   "INI"
         Groups(0).Columns(3).CaptionAlignment=   0
         Groups(0).Columns(3).DataField=   "Column 3"
         Groups(0).Columns(3).DataType=   8
         Groups(0).Columns(3).FieldLen=   256
         Groups(0).Columns(3).Locked=   -1  'True
         Groups(0).Columns(3).HasBackColor=   -1  'True
         Groups(0).Columns(3).BackColor=   16777152
         Groups(0).Columns(4).Width=   1085
         Groups(0).Columns(4).Caption=   "Fin"
         Groups(0).Columns(4).Name=   "FIN"
         Groups(0).Columns(4).CaptionAlignment=   0
         Groups(0).Columns(4).DataField=   "Column 4"
         Groups(0).Columns(4).DataType=   8
         Groups(0).Columns(4).FieldLen=   256
         Groups(0).Columns(4).Locked=   -1  'True
         Groups(0).Columns(4).HasBackColor=   -1  'True
         Groups(0).Columns(4).BackColor=   16777152
         Groups(0).Columns(5).Width=   1058
         Groups(0).Columns(5).Caption=   "Dest"
         Groups(0).Columns(5).Name=   "DEST"
         Groups(0).Columns(5).CaptionAlignment=   0
         Groups(0).Columns(5).DataField=   "Column 5"
         Groups(0).Columns(5).DataType=   8
         Groups(0).Columns(5).FieldLen=   256
         Groups(0).Columns(5).Locked=   -1  'True
         Groups(0).Columns(5).HasBackColor=   -1  'True
         Groups(0).Columns(5).BackColor=   16777152
         Groups(0).Columns(6).Width=   873
         Groups(0).Columns(6).Caption=   "Uni"
         Groups(0).Columns(6).Name=   "UNI"
         Groups(0).Columns(6).CaptionAlignment=   0
         Groups(0).Columns(6).DataField=   "Column 6"
         Groups(0).Columns(6).DataType=   8
         Groups(0).Columns(6).FieldLen=   256
         Groups(0).Columns(6).Locked=   -1  'True
         Groups(0).Columns(6).HasBackColor=   -1  'True
         Groups(0).Columns(6).BackColor=   16777152
         Groups(0).Columns(7).Width=   1667
         Groups(0).Columns(7).Caption=   "Cantidad"
         Groups(0).Columns(7).Name=   "CANT"
         Groups(0).Columns(7).Alignment=   1
         Groups(0).Columns(7).CaptionAlignment=   0
         Groups(0).Columns(7).DataField=   "Column 7"
         Groups(0).Columns(7).DataType=   8
         Groups(0).Columns(7).FieldLen=   256
         Groups(0).Columns(7).Locked=   -1  'True
         Groups(0).Columns(7).HasBackColor=   -1  'True
         Groups(0).Columns(7).BackColor=   16777152
         Groups(0).Columns(8).Width=   1640
         Groups(0).Columns(8).Caption=   "Pres.unitario"
         Groups(0).Columns(8).Name=   "PREC"
         Groups(0).Columns(8).Alignment=   1
         Groups(0).Columns(8).CaptionAlignment=   0
         Groups(0).Columns(8).DataField=   "Column 8"
         Groups(0).Columns(8).DataType=   8
         Groups(0).Columns(8).FieldLen=   256
         Groups(0).Columns(8).Locked=   -1  'True
         Groups(0).Columns(8).HasBackColor=   -1  'True
         Groups(0).Columns(8).BackColor=   16777152
         Groups(0).Columns(9).Width=   1138
         Groups(0).Columns(9).Caption=   "Pago"
         Groups(0).Columns(9).Name=   "PAG"
         Groups(0).Columns(9).Alignment=   1
         Groups(0).Columns(9).CaptionAlignment=   0
         Groups(0).Columns(9).DataField=   "Column 9"
         Groups(0).Columns(9).DataType=   8
         Groups(0).Columns(9).FieldLen=   256
         Groups(0).Columns(9).Locked=   -1  'True
         Groups(0).Columns(9).HasBackColor=   -1  'True
         Groups(0).Columns(9).BackColor=   16777152
         Groups(0).Columns(10).Width=   1508
         Groups(0).Columns(10).Caption=   "Comentario"
         Groups(0).Columns(10).Name=   "COMENT"
         Groups(0).Columns(10).CaptionAlignment=   0
         Groups(0).Columns(10).DataField=   "Column 10"
         Groups(0).Columns(10).DataType=   8
         Groups(0).Columns(10).FieldLen=   256
         Groups(0).Columns(10).Style=   4
         Groups(0).Columns(10).ButtonsAlways=   -1  'True
         Groups(0).Columns(11).Width=   5212
         Groups(0).Columns(11).Visible=   0   'False
         Groups(0).Columns(11).Caption=   "CERRADO"
         Groups(0).Columns(11).Name=   "CERRADO"
         Groups(0).Columns(11).DataField=   "Column 11"
         Groups(0).Columns(11).DataType=   8
         Groups(0).Columns(11).FieldLen=   256
         _ExtentX        =   19420
         _ExtentY        =   10134
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtNumDec 
         Height          =   285
         Left            =   10635
         TabIndex        =   197
         Top             =   160
         Width           =   285
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddUsar 
         Height          =   915
         Left            =   3000
         TabIndex        =   178
         Top             =   2490
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmOFERec.frx":03F6
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns(0).Width=   3201
         Columns(0).Name =   "NOMBRE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         ForeColor       =   -2147483630
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddCombo 
         Height          =   915
         Left            =   3150
         TabIndex        =   177
         Top             =   1470
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmOFERec.frx":0412
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns(0).Width=   3201
         Columns(0).Name =   "NOMBRE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         ForeColor       =   -2147483630
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picChkItems 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   8880
         ScaleHeight     =   195
         ScaleWidth      =   1785
         TabIndex        =   175
         Top             =   30
         Width           =   1785
         Begin VB.CheckBox ChkMostrarItems 
            BackColor       =   &H00808000&
            Caption         =   "Mostrar items cerrados"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   0
            TabIndex        =   94
            Top             =   0
            Value           =   1  'Checked
            Width           =   1965
         End
      End
      Begin VB.PictureBox picComboGrupo 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   405
         Left            =   570
         ScaleHeight     =   405
         ScaleWidth      =   4695
         TabIndex        =   174
         Top             =   30
         Width           =   4695
         Begin SSDataWidgets_B.SSDBCombo sdbcGrupo 
            Height          =   285
            Left            =   30
            TabIndex        =   92
            Top             =   60
            Width           =   4605
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   8123
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   47031
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picMenu 
         BorderStyle     =   0  'None
         ForeColor       =   &H00000000&
         Height          =   495
         Left            =   0
         Picture         =   "frmOFERec.frx":042E
         ScaleHeight     =   495
         ScaleWidth      =   495
         TabIndex        =   107
         Top             =   0
         Width           =   495
      End
      Begin MSComCtl2.UpDown UpDownDec 
         Height          =   300
         Left            =   10945
         TabIndex        =   196
         Top             =   160
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   529
         _Version        =   393216
         OrigLeft        =   1800
         OrigTop         =   200
         OrigRight       =   2040
         OrigBottom      =   485
         Max             =   20
         Enabled         =   -1  'True
      End
      Begin VB.Label lblNumDec 
         BackColor       =   &H00808000&
         Caption         =   "N�mero decimales:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   135
         Left            =   9240
         TabIndex        =   198
         Top             =   265
         Width           =   1400
      End
      Begin VB.Label lblPresGrupo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E1FFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty DataFormat 
            Type            =   0
            Format          =   """#,###.########"""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   3082
            SubFormatType   =   0
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   1
         Left            =   6930
         TabIndex        =   93
         Top             =   90
         Width           =   1725
      End
      Begin VB.Line Line14 
         BorderColor     =   &H00FFFFFF&
         X1              =   8790
         X2              =   8790
         Y1              =   0
         Y2              =   480
      End
      Begin VB.Line Line11 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   11220
         Y1              =   480
         Y2              =   480
      End
      Begin VB.Line Line15 
         BorderColor     =   &H00FFFFFF&
         X1              =   5400
         X2              =   5400
         Y1              =   0
         Y2              =   480
      End
      Begin VB.Label lblCPresGrupo 
         BackColor       =   &H00808000&
         Caption         =   "Pres. proceso:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   1
         Left            =   5550
         TabIndex        =   110
         Top             =   120
         Width           =   1365
      End
      Begin VB.Label lblCPresGrupo 
         BackColor       =   &H00808000&
         Caption         =   "Pres. grupo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Index           =   0
         Left            =   5550
         TabIndex        =   109
         Top             =   120
         Width           =   1395
      End
      Begin VB.Label lblPresGrupo 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00E1FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   0
         Left            =   6930
         TabIndex        =   108
         Top             =   90
         Width           =   1725
      End
   End
   Begin MSComctlLib.ImageList ImageList3 
      Left            =   1890
      Top             =   6960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   14
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":0870
            Key             =   "Proceso"
            Object.Tag             =   "Proceso"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":0CC2
            Key             =   "Oferta2"
            Object.Tag             =   "Oferta2"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":159C
            Key             =   "Prove"
            Object.Tag             =   "Prove"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":1E76
            Key             =   "Oferta"
            Object.Tag             =   "Oferta"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":2750
            Key             =   "Ficheros"
            Object.Tag             =   "Ficheros"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":302A
            Key             =   "Grupo"
            Object.Tag             =   "Grupo"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":47BC
            Key             =   "Atributos"
            Object.Tag             =   "Atributos"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":5096
            Key             =   "Item"
            Object.Tag             =   "Item"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":5970
            Key             =   "ItemGris"
            Object.Tag             =   "ItemGris"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":61C2
            Key             =   "AtributosGris"
            Object.Tag             =   "AtributosGris"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":6A16
            Key             =   "FicherosGris"
            Object.Tag             =   "FicherosGris"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":72F0
            Key             =   "sobrecerrado"
            Object.Tag             =   "sobrecerrado"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":7390
            Key             =   "sobreabierto"
            Object.Tag             =   "sobreabierto"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":7421
            Key             =   "NOOFE"
            Object.Tag             =   "NOOFE"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdAdjun 
      Left            =   495
      Top             =   7815
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin VB.Frame fraProce 
      BorderStyle     =   0  'None
      ForeColor       =   &H00000000&
      Height          =   450
      Left            =   30
      TabIndex        =   97
      Top             =   30
      Width           =   11750
      Begin VB.CommandButton cmdColaboracion 
         Enabled         =   0   'False
         Height          =   285
         Left            =   11260
         Picture         =   "frmOFERec.frx":74E2
         Style           =   1  'Graphical
         TabIndex        =   210
         Top             =   105
         Width           =   315
      End
      Begin VB.CommandButton cmdResponsable 
         Enabled         =   0   'False
         Height          =   285
         Left            =   10905
         Picture         =   "frmOFERec.frx":776B
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscar 
         Height          =   285
         Left            =   10550
         Picture         =   "frmOFERec.frx":77F2
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   120
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   585
         TabIndex        =   0
         Top             =   120
         Width           =   900
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1587
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
         Height          =   285
         Left            =   4485
         TabIndex        =   3
         Top             =   120
         Width           =   1065
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BevelColorFrame =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2011
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   8546
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483630
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
         Height          =   285
         Left            =   5580
         TabIndex        =   4
         Top             =   120
         Width           =   4940
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   7064
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1879
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INVI"
         Columns(2).Name =   "INVI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8714
         _ExtentY        =   503
         _StockProps     =   93
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
      End
      Begin SelectorDeProcesos.ProceSelector ProceSelector1 
         Height          =   315
         Left            =   3180
         TabIndex        =   2
         Top             =   120
         Width           =   315
         _ExtentX        =   556
         _ExtentY        =   556
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   2100
         TabIndex        =   1
         Top             =   120
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Label lblCProceCod 
         Caption         =   "Proceso:"
         ForeColor       =   &H00000000&
         Height          =   180
         Left            =   3615
         TabIndex        =   101
         Top             =   135
         Width           =   960
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comm:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   1545
         TabIndex        =   99
         Top             =   135
         Width           =   690
      End
      Begin VB.Label lblAnyo 
         Caption         =   "A�o:"
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   30
         TabIndex        =   98
         Top             =   135
         Width           =   615
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":787F
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":79D9
            Key             =   "HAND"
            Object.Tag             =   "HAND"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOFERec.frx":7CF3
            Key             =   "VIEW"
            Object.Tag             =   "VIEW"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwProce 
      Height          =   6435
      Left            =   30
      TabIndex        =   7
      Top             =   510
      Width           =   3815
      _ExtentX        =   6720
      _ExtentY        =   11351
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList3"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      ForeColor       =   &H00000000&
      Height          =   480
      Left            =   0
      ScaleHeight     =   480
      ScaleWidth      =   11880
      TabIndex        =   103
      TabStop         =   0   'False
      Top             =   4980
      Visible         =   0   'False
      Width           =   11880
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   345
         Left            =   4365
         TabIndex        =   74
         TabStop         =   0   'False
         Top             =   75
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   345
         Left            =   5610
         TabIndex        =   76
         TabStop         =   0   'False
         Top             =   75
         Width           =   1005
      End
   End
   Begin VB.PictureBox picPrincipal 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H00000000&
      Height          =   6435
      Left            =   3810
      ScaleHeight     =   6435
      ScaleWidth      =   7425
      TabIndex        =   111
      Top             =   540
      Width           =   7425
      Begin VB.PictureBox picApartado 
         ForeColor       =   &H00000000&
         Height          =   6195
         Index           =   0
         Left            =   120
         ScaleHeight     =   6135
         ScaleWidth      =   7035
         TabIndex        =   112
         Top             =   90
         Width           =   7095
         Begin VB.ListBox lstMaterial 
            BackColor       =   &H80000018&
            Height          =   450
            Left            =   1170
            TabIndex        =   206
            Top             =   405
            Width           =   5700
         End
         Begin VB.TextBox txtMat 
            BackColor       =   &H00E1FFFF&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1170
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   480
            Width           =   5685
         End
         Begin VB.PictureBox Picture1 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   3180
            ScaleHeight     =   315
            ScaleWidth      =   3420
            TabIndex        =   113
            Top             =   1500
            Width           =   3420
            Begin VB.CheckBox chkPermAdjDir 
               Caption         =   "Permitir adjudicaci�n directa"
               ForeColor       =   &H00000000&
               Height          =   315
               Left            =   90
               TabIndex        =   14
               Top             =   60
               Width           =   3000
            End
         End
         Begin VB.Label lblHoraLimite 
            Caption         =   "Hora"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   3255
            TabIndex        =   131
            Top             =   2325
            Width           =   1350
         End
         Begin VB.Label lblFecLimit 
            Caption         =   "Cierre subasta"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   130
            Top             =   2325
            Width           =   1320
         End
         Begin VB.Label lblPago 
            Caption         =   "Forma de pago"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   150
            TabIndex        =   129
            Top             =   5340
            Width           =   1395
         End
         Begin VB.Label lblMon 
            Caption         =   "Moneda"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   128
            Top             =   3585
            Width           =   1380
         End
         Begin VB.Label lblSolicitud 
            BackStyle       =   0  'Transparent
            Caption         =   "Solicitud de compra"
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   3240
            TabIndex        =   127
            Top             =   3090
            Width           =   1905
         End
         Begin VB.Label lblPresupuesto 
            BackStyle       =   0  'Transparent
            Caption         =   "Presupuesto"
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   126
            Top             =   3090
            Width           =   1380
         End
         Begin VB.Label lblFecIniSub 
            Caption         =   "Inicio Subasta"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   125
            Top             =   1935
            Width           =   1320
         End
         Begin VB.Label lblFecApe 
            BackStyle       =   0  'Transparent
            Caption         =   "Apertura"
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   124
            Top             =   1155
            Width           =   720
         End
         Begin VB.Label lblFecNec 
            BackStyle       =   0  'Transparent
            Caption         =   "Necesidad:"
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   3255
            TabIndex        =   123
            Top             =   1155
            Width           =   1350
         End
         Begin VB.Label lblFecPres 
            BackStyle       =   0  'Transparent
            Caption         =   "Presentaci�n"
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   122
            Top             =   1575
            Width           =   1290
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "Material:"
            ForeColor       =   &H80000006&
            Height          =   225
            Left            =   120
            TabIndex        =   120
            Top             =   525
            Width           =   1290
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00808080&
            BorderWidth     =   2
            X1              =   0
            X2              =   7230
            Y1              =   4095
            Y2              =   4080
         End
         Begin VB.Label lblDest 
            Caption         =   "Destino"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   119
            Top             =   4860
            Width           =   1395
         End
         Begin VB.Label lblHoraIniSub 
            Caption         =   "Hora"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   3255
            TabIndex        =   118
            Top             =   1935
            Width           =   1350
         End
         Begin VB.Line Line7 
            BorderColor     =   &H00808080&
            BorderWidth     =   2
            X1              =   0
            X2              =   7230
            Y1              =   2820
            Y2              =   2805
         End
         Begin VB.Line Line8 
            BorderColor     =   &H00808080&
            BorderWidth     =   2
            X1              =   0
            X2              =   7230
            Y1              =   945
            Y2              =   930
         End
         Begin VB.Label lblDatosGenProce 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Datos generales de proceso"
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   0
            TabIndex        =   117
            Top             =   0
            Width           =   7080
         End
         Begin VB.Label lblFecFin 
            BackStyle       =   0  'Transparent
            Caption         =   "Fin suministro"
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   3315
            TabIndex        =   116
            Top             =   4395
            Width           =   1395
         End
         Begin VB.Label lblFecIni 
            BackStyle       =   0  'Transparent
            Caption         =   "Inicio suministro"
            ForeColor       =   &H00000000&
            Height          =   240
            Index           =   0
            Left            =   180
            TabIndex        =   115
            Top             =   4395
            Width           =   1395
         End
         Begin VB.Label lblFecApe 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   11
            Top             =   1155
            Width           =   1350
         End
         Begin VB.Label lblFecPres 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   13
            Top             =   1545
            Width           =   1350
         End
         Begin VB.Label lblFecIniSub 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   15
            Top             =   1920
            Width           =   1350
         End
         Begin VB.Label lblFecLimit 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   17
            Top             =   2310
            Width           =   1350
         End
         Begin VB.Label lblFecNec 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   5505
            TabIndex        =   12
            Top             =   1140
            Width           =   1350
         End
         Begin VB.Label lblPresupuesto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   18
            Top             =   3075
            Width           =   1350
         End
         Begin VB.Label lblSolicitud 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   5250
            TabIndex        =   20
            Top             =   3030
            Width           =   1605
         End
         Begin VB.Label lblCambio 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   4530
            TabIndex        =   22
            Top             =   3540
            Width           =   2325
         End
         Begin VB.Label lblFecFin 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   5460
            TabIndex        =   24
            Top             =   4380
            Width           =   1395
         End
         Begin VB.Label lblDest 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   3000
            TabIndex        =   26
            Top             =   4830
            Width           =   3855
         End
         Begin VB.Label lblPago 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   3000
            TabIndex        =   28
            Top             =   5325
            Width           =   3855
         End
         Begin VB.Label lblHoraIniSub 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   5505
            TabIndex        =   16
            Top             =   1905
            Width           =   1350
         End
         Begin VB.Label lblHoraLimite 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   5505
            TabIndex        =   19
            Top             =   2295
            Width           =   1350
         End
         Begin VB.Label lblProveProce 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   3000
            TabIndex        =   30
            Top             =   5790
            Width           =   3855
         End
         Begin VB.Label lblProveProce 
            Caption         =   "Proveedor actual"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   150
            TabIndex        =   114
            Top             =   5790
            Visible         =   0   'False
            Width           =   1410
         End
         Begin VB.Label lblPago 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1590
            TabIndex        =   27
            Top             =   5310
            Width           =   1350
         End
         Begin VB.Label lblDest 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1590
            TabIndex        =   25
            Top             =   4830
            Width           =   1350
         End
         Begin VB.Label lblFecIni 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1590
            TabIndex        =   23
            Top             =   4380
            Width           =   1350
         End
         Begin VB.Label lblMon 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   21
            Top             =   3540
            Width           =   1350
         End
         Begin VB.Label lblProveProce 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1590
            TabIndex        =   29
            Top             =   5790
            Width           =   1350
         End
         Begin VB.Label lblCambio 
            Caption         =   "Equivalencia"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   3255
            TabIndex        =   121
            Top             =   3585
            Width           =   1230
         End
      End
      Begin VB.PictureBox picApartado 
         ForeColor       =   &H00000000&
         Height          =   6195
         Index           =   1
         Left            =   120
         ScaleHeight     =   6135
         ScaleWidth      =   7035
         TabIndex        =   132
         Top             =   90
         Width           =   7095
         Begin VB.Frame fraFechas 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   1575
            Left            =   75
            TabIndex        =   138
            Top             =   330
            Width           =   7095
            Begin VB.TextBox txtHoraOfe 
               Height          =   285
               Left            =   3060
               TabIndex        =   195
               Top             =   540
               Width           =   900
            End
            Begin VB.TextBox txtFecVal 
               BackColor       =   &H00FFFFFF&
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   5295
               MaxLength       =   10
               TabIndex        =   56
               Top             =   540
               Width           =   1080
            End
            Begin VB.TextBox txtFecOfe 
               BackColor       =   &H00FFFFFF&
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1600
               MaxLength       =   10
               TabIndex        =   54
               Top             =   540
               Width           =   1080
            End
            Begin VB.CommandButton cmdCalendarFecOfe 
               Height          =   315
               Left            =   2710
               Picture         =   "frmOFERec.frx":8245
               Style           =   1  'Graphical
               TabIndex        =   55
               TabStop         =   0   'False
               Top             =   525
               Width           =   315
            End
            Begin VB.CommandButton cmdCalendarValHas 
               Height          =   315
               Left            =   6420
               Picture         =   "frmOFERec.frx":8557
               Style           =   1  'Graphical
               TabIndex        =   57
               TabStop         =   0   'False
               Top             =   525
               Width           =   315
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEstDen 
               Height          =   285
               Left            =   2715
               TabIndex        =   59
               Top             =   1170
               Width           =   4050
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7153
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcEstCod 
               Height          =   285
               Left            =   1605
               TabIndex        =   58
               Top             =   1170
               Width           =   1080
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1905
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin VB.Label lblTZ 
               Caption         =   "TZ"
               Height          =   255
               Left            =   3060
               TabIndex        =   207
               Top             =   885
               Width           =   3075
            End
            Begin VB.Label lblOrigen 
               AutoSize        =   -1  'True
               Caption         =   "Oferta introducida desde....."
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   15
               TabIndex        =   201
               Top             =   90
               Width           =   2100
            End
            Begin VB.Label lblFecVal 
               Caption         =   "V�lida hasta:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   4100
               TabIndex        =   141
               Top             =   585
               Width           =   1190
            End
            Begin VB.Label lblFecOfe 
               Caption         =   "Fecha:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   15
               TabIndex        =   140
               Top             =   585
               Width           =   1590
            End
            Begin VB.Label lblEstCod 
               Caption         =   "Estado de la oferta:"
               ForeColor       =   &H00000000&
               Height          =   225
               Left            =   15
               TabIndex        =   139
               Top             =   1200
               Width           =   1530
            End
         End
         Begin VB.Frame fraMoneda 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   1500
            Left            =   90
            TabIndex        =   135
            Top             =   2070
            Width           =   7095
            Begin VB.TextBox txtCambio 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1600
               MaxLength       =   20
               MultiLine       =   -1  'True
               TabIndex        =   62
               Top             =   945
               Width           =   5150
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
               Height          =   285
               Left            =   2710
               TabIndex        =   61
               Top             =   120
               Width           =   4055
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "Cod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   7153
               _ExtentY        =   503
               _StockProps     =   93
               ForeColor       =   -2147483630
               BackColor       =   16777215
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
               Height          =   285
               Left            =   1600
               TabIndex        =   60
               Top             =   120
               Width           =   1095
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "Cod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "Denominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1931
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin VB.Label lblLitMonProc 
               Caption         =   "DMoneda del proceso:"
               Height          =   255
               Left            =   30
               TabIndex        =   204
               Top             =   600
               Width           =   1580
            End
            Begin VB.Label lblEquivalencia 
               Caption         =   "D1 equivale a 1 EUR"
               Height          =   285
               Left            =   1600
               TabIndex        =   203
               Top             =   1300
               Width           =   5000
            End
            Begin VB.Label lblMonProceso 
               Caption         =   "DEUR - Euro"
               Height          =   255
               Left            =   1600
               TabIndex        =   202
               Top             =   600
               Width           =   5060
            End
            Begin VB.Label lblMonCod 
               Caption         =   "DMoneda de la oferta:"
               ForeColor       =   &H00000000&
               Height          =   225
               Left            =   30
               TabIndex        =   136
               Top             =   165
               Width           =   1620
            End
            Begin VB.Label lblCambio 
               Caption         =   "DEquivalencia:"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   2
               Left            =   30
               TabIndex        =   137
               Top             =   990
               Width           =   1500
            End
         End
         Begin VB.Frame fraObs 
            BorderStyle     =   0  'None
            ForeColor       =   &H00000000&
            Height          =   2250
            Left            =   90
            TabIndex        =   133
            Top             =   3825
            Width           =   7095
            Begin VB.TextBox txtObs 
               BackColor       =   &H00FFFFFF&
               ForeColor       =   &H00000000&
               Height          =   1800
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   63
               Top             =   285
               Width           =   6660
            End
            Begin VB.Label lblObs 
               Caption         =   "Observaciones"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   90
               TabIndex        =   134
               Top             =   0
               Width           =   1920
            End
         End
         Begin VB.Label lblDatosGeneralesOferta 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Datos generales de la oferta"
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   -30
            TabIndex        =   142
            Top             =   0
            Width           =   7290
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00808080&
            BorderWidth     =   2
            X1              =   0
            X2              =   7200
            Y1              =   3690
            Y2              =   3690
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00808080&
            BorderWidth     =   2
            X1              =   -60
            X2              =   7140
            Y1              =   1950
            Y2              =   1950
         End
      End
      Begin VB.PictureBox picApartado 
         ForeColor       =   &H00000000&
         Height          =   6195
         Index           =   3
         Left            =   150
         ScaleHeight     =   6135
         ScaleWidth      =   7035
         TabIndex        =   146
         Top             =   90
         Width           =   7095
         Begin VB.TextBox txtObsAdjun 
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H00000000&
            Height          =   2060
            Left            =   180
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   75
            Top             =   480
            Width           =   6800
         End
         Begin VB.PictureBox picBarraAdjun 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H00000000&
            Height          =   360
            Left            =   4485
            ScaleHeight     =   360
            ScaleWidth      =   2580
            TabIndex        =   147
            Top             =   5745
            Width           =   2580
            Begin VB.CommandButton cmdModificarAdjun 
               Height          =   300
               Left            =   1035
               Picture         =   "frmOFERec.frx":8869
               Style           =   1  'Graphical
               TabIndex        =   148
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdAbrirAdjun 
               Height          =   300
               Left            =   1995
               Picture         =   "frmOFERec.frx":89B3
               Style           =   1  'Graphical
               TabIndex        =   80
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdSalvarAdjun 
               Height          =   300
               Left            =   1515
               Picture         =   "frmOFERec.frx":8A2F
               Style           =   1  'Graphical
               TabIndex        =   79
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdEliminarAdjun 
               Height          =   300
               Left            =   555
               Picture         =   "frmOFERec.frx":8AB0
               Style           =   1  'Graphical
               TabIndex        =   78
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdA�adirAdjun 
               Height          =   300
               Left            =   75
               Picture         =   "frmOFERec.frx":8B36
               Style           =   1  'Graphical
               TabIndex        =   77
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAdjun 
            Height          =   2865
            Left            =   180
            TabIndex        =   208
            Top             =   2850
            Width           =   6825
            _Version        =   196617
            DataMode        =   2
            RecordSelectors =   0   'False
            GroupHeaders    =   0   'False
            GroupHeadLines  =   0
            Col.Count       =   4
            stylesets.count =   2
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmOFERec.frx":8B97
            stylesets(1).Name=   "Fichero"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmOFERec.frx":8BB3
            stylesets(1).AlignmentPicture=   2
            DividerType     =   0
            BevelColorHighlight=   16777215
            BevelColorShadow=   8421504
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            MaxSelectedRows =   0
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   53
            ActiveRowStyleSet=   "Normal"
            Columns.Count   =   4
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "FICHERO"
            Columns(1).Name =   "FICHERO"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HeadStyleSet=   "Normal"
            Columns(1).StyleSet=   "Fichero"
            Columns(2).Width=   3200
            Columns(2).Caption=   "TAMANYO"
            Columns(2).Name =   "TAMANYO"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "COMENTARIO"
            Columns(3).Name =   "COMENTARIO"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Style=   1
            Columns(3).ButtonsAlways=   -1  'True
            _ExtentX        =   12039
            _ExtentY        =   5054
            _StockProps     =   79
            BackColor       =   16777215
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Archivos adjuntos"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   150
            TabIndex        =   149
            Top             =   2610
            Width           =   2145
         End
         Begin VB.Label lblAdjuntos 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Ficheros adjuntos"
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   -30
            TabIndex        =   150
            Top             =   0
            Width           =   7290
         End
      End
      Begin VB.PictureBox picApartado 
         ForeColor       =   &H00000000&
         Height          =   6195
         Index           =   2
         Left            =   120
         ScaleHeight     =   6135
         ScaleWidth      =   7035
         TabIndex        =   143
         Top             =   90
         Width           =   7095
         Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
            Height          =   915
            Left            =   4800
            TabIndex        =   176
            Top             =   1230
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmOFERec.frx":8D0D
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns(0).Width=   3201
            Columns(0).Name =   "NOMBRE"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            ForeColor       =   -2147483630
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.PictureBox picAtrib 
            Height          =   6105
            Left            =   0
            ScaleHeight     =   6045
            ScaleWidth      =   7215
            TabIndex        =   144
            Top             =   330
            Width           =   7275
            Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
               Height          =   5790
               Left            =   0
               TabIndex        =   70
               Top             =   0
               Width           =   7035
               _Version        =   196617
               DataMode        =   2
               Col.Count       =   8
               stylesets.count =   3
               stylesets(0).Name=   "Checked"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmOFERec.frx":8D29
               stylesets(1).Name=   "Normal"
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmOFERec.frx":8F85
               stylesets(2).Name=   "UnChecked"
               stylesets(2).HasFont=   -1  'True
               BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(2).Picture=   "frmOFERec.frx":8FA1
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   0
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               ExtraHeight     =   53
               Columns.Count   =   8
               Columns(0).Width=   2143
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).Locked=   -1  'True
               Columns(0).HasHeadForeColor=   -1  'True
               Columns(0).HasHeadBackColor=   -1  'True
               Columns(0).HasBackColor=   -1  'True
               Columns(0).HeadForeColor=   16777215
               Columns(0).HeadBackColor=   32896
               Columns(0).BackColor=   52428
               Columns(1).Width=   5741
               Columns(1).Caption=   "Nombre"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   200
               Columns(1).Locked=   -1  'True
               Columns(1).Style=   1
               Columns(1).ButtonsAlways=   -1  'True
               Columns(1).HasHeadForeColor=   -1  'True
               Columns(1).HasHeadBackColor=   -1  'True
               Columns(1).HasBackColor=   -1  'True
               Columns(1).HeadForeColor=   16777215
               Columns(1).HeadBackColor=   32896
               Columns(1).BackColor=   52428
               Columns(2).Width=   3519
               Columns(2).Caption=   "Valor"
               Columns(2).Name =   "VALOR"
               Columns(2).Alignment=   1
               Columns(2).CaptionAlignment=   0
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).Locked=   -1  'True
               Columns(2).HasHeadForeColor=   -1  'True
               Columns(2).HasHeadBackColor=   -1  'True
               Columns(2).HeadForeColor=   16777215
               Columns(2).HeadBackColor=   32896
               Columns(3).Width=   3200
               Columns(3).Visible=   0   'False
               Columns(3).Caption=   "TIPO"
               Columns(3).Name =   "TIPO"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(4).Width=   3200
               Columns(4).Visible=   0   'False
               Columns(4).Caption=   "INTRO"
               Columns(4).Name =   "INTRO"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(5).Width=   3200
               Columns(5).Visible=   0   'False
               Columns(5).Caption=   "ID_A"
               Columns(5).Name =   "ID_A"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(6).Width=   3200
               Columns(6).Visible=   0   'False
               Columns(6).Caption=   "ID_P"
               Columns(6).Name =   "ID_P"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(7).Width=   3200
               Columns(7).Visible=   0   'False
               Columns(7).Caption=   "OBL"
               Columns(7).Name =   "OBL"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   11
               Columns(7).FieldLen=   256
               _ExtentX        =   12409
               _ExtentY        =   10213
               _StockProps     =   79
               ForeColor       =   -2147483630
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Label lblAtributosProceso 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Atributos"
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   -15
            TabIndex        =   145
            Top             =   0
            Width           =   7080
         End
      End
      Begin VB.PictureBox picApartado 
         ForeColor       =   &H00000000&
         Height          =   6195
         Index           =   7
         Left            =   120
         ScaleHeight     =   5947.783
         ScaleMode       =   0  'User
         ScaleWidth      =   6903.641
         TabIndex        =   182
         Top             =   90
         Visible         =   0   'False
         Width           =   7095
         Begin VB.TextBox txtFechaAperturaSobre 
            BackColor       =   &H00E1FFFF&
            Height          =   294
            Left            =   360
            Locked          =   -1  'True
            TabIndex        =   187
            Top             =   1995
            Width           =   2850
         End
         Begin VB.TextBox txtUsuAperSobre 
            BackColor       =   &H00E1FFFF&
            Height          =   294
            Left            =   345
            Locked          =   -1  'True
            TabIndex        =   186
            Top             =   2595
            Width           =   6435
         End
         Begin VB.TextBox txtObsSobre 
            BackColor       =   &H00E1FFFF&
            Height          =   2805
            Left            =   345
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   185
            Top             =   3180
            Width           =   6435
         End
         Begin VB.PictureBox picStop 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   990
            Left            =   165
            Picture         =   "frmOFERec.frx":91FD
            ScaleHeight     =   990
            ScaleWidth      =   990
            TabIndex        =   184
            Top             =   330
            Width           =   990
         End
         Begin VB.TextBox txtFechaRealAperturaSobre 
            BackColor       =   &H00E1FFFF&
            Height          =   294
            Left            =   3930
            Locked          =   -1  'True
            TabIndex        =   183
            Top             =   1995
            Width           =   2850
         End
         Begin VB.Label lblCabeceraSobre 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Datos del sobre"
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   -30
            TabIndex        =   194
            Top             =   0
            Width           =   7125
         End
         Begin VB.Label lblEstado 
            Alignment       =   2  'Center
            Caption         =   "Abierto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   2310
            TabIndex        =   193
            Top             =   480
            Width           =   2445
         End
         Begin VB.Line Line4 
            BorderColor     =   &H00808080&
            BorderStyle     =   6  'Inside Solid
            X1              =   29.44
            X2              =   7124.44
            Y1              =   1366.972
            Y2              =   1366.972
         End
         Begin VB.Label lblFechaAperturaSobre 
            Caption         =   "Fecha prevista de apertura de sobre:"
            Height          =   345
            Left            =   180
            TabIndex        =   192
            Top             =   1755
            Width           =   3495
         End
         Begin VB.Label lblUsuAperSobre 
            Caption         =   "Usuario encargado de apertura:"
            Height          =   345
            Left            =   150
            TabIndex        =   191
            Top             =   2340
            Width           =   3855
         End
         Begin VB.Label lblObsSobre 
            Caption         =   "Observaciones"
            Height          =   345
            Left            =   150
            TabIndex        =   190
            Top             =   2940
            Width           =   3855
         End
         Begin VB.Label lblMsjSobreCerrado 
            Caption         =   "El sobre est� cerrado. No podr� ver el contenido del sobre hasta su apertura"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   165
            TabIndex        =   189
            Top             =   1470
            Width           =   6630
         End
         Begin VB.Label lblFechaRealAperturaSobre 
            Caption         =   "Fecha real de apertura de sobre:"
            Height          =   345
            Left            =   3735
            TabIndex        =   188
            Top             =   1755
            Width           =   3390
         End
      End
      Begin VB.PictureBox picApartado 
         ForeColor       =   &H00000000&
         Height          =   6195
         Index           =   6
         Left            =   120
         ScaleHeight     =   6135
         ScaleWidth      =   7035
         TabIndex        =   164
         Top             =   90
         Visible         =   0   'False
         Width           =   7095
         Begin VB.TextBox txtCodGrupo 
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   1770
            Locked          =   -1  'True
            TabIndex        =   81
            TabStop         =   0   'False
            Top             =   600
            Width           =   2085
         End
         Begin VB.TextBox txtDenGrupo 
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   1770
            Locked          =   -1  'True
            TabIndex        =   82
            TabStop         =   0   'False
            Top             =   1020
            Width           =   3435
         End
         Begin VB.TextBox txtDescrGrupo 
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H00000000&
            Height          =   1035
            Left            =   210
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   83
            TabStop         =   0   'False
            Top             =   1920
            Width           =   6675
         End
         Begin VB.TextBox txtFinGrupo 
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   4740
            Locked          =   -1  'True
            TabIndex        =   85
            TabStop         =   0   'False
            Top             =   3630
            Visible         =   0   'False
            Width           =   1380
         End
         Begin VB.TextBox txtIniGrupo 
            BackColor       =   &H00FFFFFF&
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   1770
            Locked          =   -1  'True
            TabIndex        =   84
            TabStop         =   0   'False
            Top             =   3660
            Visible         =   0   'False
            Width           =   1380
         End
         Begin VB.Label lblProveGrupo 
            Caption         =   "Proveedor actual"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   173
            Top             =   4860
            Visible         =   0   'False
            Width           =   1530
         End
         Begin VB.Label lblFinGrupo 
            BackStyle       =   0  'Transparent
            Caption         =   "Fin suministro"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   3420
            TabIndex        =   172
            Top             =   3660
            Visible         =   0   'False
            Width           =   1245
         End
         Begin VB.Label lblIniGrupo 
            BackStyle       =   0  'Transparent
            Caption         =   "Inicio suministro"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   180
            TabIndex        =   171
            Top             =   3660
            Visible         =   0   'False
            Width           =   1485
         End
         Begin VB.Label lblPagGrupo 
            Caption         =   "Forma de pago"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   170
            Top             =   4440
            Visible         =   0   'False
            Width           =   1470
         End
         Begin VB.Label lblDestGrupo 
            Caption         =   "Destino"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   0
            Left            =   180
            TabIndex        =   169
            Top             =   4050
            Visible         =   0   'False
            Width           =   1380
         End
         Begin VB.Label lblCodGrupo 
            Caption         =   "C�digo"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   210
            TabIndex        =   168
            Top             =   630
            Width           =   735
         End
         Begin VB.Label lblDenGrupo 
            Caption         =   "Denominaci�n"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   240
            TabIndex        =   167
            Top             =   1020
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00808080&
            BorderWidth     =   2
            X1              =   30
            X2              =   7050
            Y1              =   1530
            Y2              =   1530
         End
         Begin VB.Label lblDescrGrupo 
            Caption         =   "Descripici�n del grupo"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   240
            TabIndex        =   166
            Top             =   1680
            Width           =   2295
         End
         Begin VB.Line Line10 
            BorderColor     =   &H00808080&
            BorderWidth     =   2
            X1              =   -60
            X2              =   7020
            Y1              =   3300
            Y2              =   3300
         End
         Begin VB.Label Label18 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Datos generales del grupo"
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   0
            TabIndex        =   165
            Top             =   0
            Width           =   7455
         End
         Begin VB.Label lblPagGrupo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1740
            TabIndex        =   88
            Top             =   4440
            Width           =   1380
         End
         Begin VB.Label lblDestGrupo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1740
            TabIndex        =   86
            Top             =   4050
            Width           =   1380
         End
         Begin VB.Label lblProveGrupo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   1740
            TabIndex        =   90
            Top             =   4830
            Width           =   1380
         End
         Begin VB.Label lblDestGrupo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   3150
            TabIndex        =   87
            Top             =   4050
            Width           =   3720
         End
         Begin VB.Label lblPagGrupo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   3150
            TabIndex        =   89
            Top             =   4440
            Width           =   3720
         End
         Begin VB.Label lblProveGrupo 
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   3150
            TabIndex        =   91
            Top             =   4830
            Width           =   3720
         End
      End
      Begin VB.PictureBox picApartado 
         ForeColor       =   &H00000000&
         Height          =   6195
         Index           =   5
         Left            =   120
         ScaleHeight     =   5947.783
         ScaleMode       =   0  'User
         ScaleWidth      =   6903.641
         TabIndex        =   151
         Top             =   90
         Visible         =   0   'False
         Width           =   7095
         Begin VB.CommandButton cmdNoOfe 
            Caption         =   "NO OFERTA"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Index           =   0
            Left            =   3735
            TabIndex        =   199
            Top             =   502
            Width           =   1515
         End
         Begin VB.PictureBox picDatos 
            BorderStyle     =   0  'None
            ForeColor       =   &H00000000&
            Height          =   4950
            Left            =   210
            ScaleHeight     =   4950
            ScaleWidth      =   7470
            TabIndex        =   96
            Top             =   870
            Width           =   7470
            Begin VB.TextBox txtObsProve 
               BackColor       =   &H00E1FFFF&
               ForeColor       =   &H00000000&
               Height          =   1005
               Left            =   3360
               Locked          =   -1  'True
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   49
               Top             =   3825
               Width           =   3375
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgContactos 
               Height          =   1425
               Left            =   0
               TabIndex        =   42
               Top             =   2340
               Width           =   6705
               ScrollBars      =   3
               _Version        =   196617
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BorderStyle     =   0
               GroupHeaders    =   0   'False
               Col.Count       =   11
               stylesets.count =   2
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmOFERec.frx":9520
               stylesets(1).Name=   "Tan"
               stylesets(1).BackColor=   10079487
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmOFERec.frx":953C
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   0
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               BalloonHelp     =   0   'False
               MaxSelectedRows =   1
               HeadStyleSet    =   "Normal"
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorEven   =   12632256
               BackColorOdd    =   12632256
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns.Count   =   11
               Columns(0).Width=   3200
               Columns(0).Caption=   "Apellidos"
               Columns(0).Name =   "APE"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(0).HasBackColor=   -1  'True
               Columns(0).BackColor=   16777215
               Columns(1).Width=   4022
               Columns(1).Caption=   "Nombre"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   50
               Columns(1).HasBackColor=   -1  'True
               Columns(1).BackColor=   16777215
               Columns(2).Width=   3200
               Columns(2).Caption=   "Departamento"
               Columns(2).Name =   "DEP"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               Columns(2).HasBackColor=   -1  'True
               Columns(2).BackColor=   16777215
               Columns(3).Width=   3200
               Columns(3).Caption=   "Cargo"
               Columns(3).Name =   "CAR"
               Columns(3).DataField=   "Column 3"
               Columns(3).DataType=   8
               Columns(3).FieldLen=   256
               Columns(3).HasBackColor=   -1  'True
               Columns(3).BackColor=   16777215
               Columns(4).Width=   2328
               Columns(4).Caption=   "Tel�fono"
               Columns(4).Name =   "TFNO"
               Columns(4).DataField=   "Column 4"
               Columns(4).DataType=   8
               Columns(4).FieldLen=   256
               Columns(4).HasBackColor=   -1  'True
               Columns(4).BackColor=   16777215
               Columns(5).Width=   3200
               Columns(5).Caption=   "Fax"
               Columns(5).Name =   "FAX"
               Columns(5).DataField=   "Column 5"
               Columns(5).DataType=   8
               Columns(5).FieldLen=   256
               Columns(5).HasBackColor=   -1  'True
               Columns(5).BackColor=   16777215
               Columns(6).Width=   3200
               Columns(6).Caption=   "Tel�fono m�vil"
               Columns(6).Name =   "TFNO_MOVIL"
               Columns(6).DataField=   "Column 6"
               Columns(6).DataType=   8
               Columns(6).FieldLen=   256
               Columns(6).HasBackColor=   -1  'True
               Columns(6).BackColor=   16777215
               Columns(7).Width=   3200
               Columns(7).Caption=   "Mail"
               Columns(7).Name =   "MAIL"
               Columns(7).DataField=   "Column 7"
               Columns(7).DataType=   8
               Columns(7).FieldLen=   256
               Columns(7).HasBackColor=   -1  'True
               Columns(7).BackColor=   16777215
               Columns(8).Width=   2540
               Columns(8).Caption=   "Recibe peticiones"
               Columns(8).Name =   "DEF"
               Columns(8).DataField=   "Column 8"
               Columns(8).DataType=   8
               Columns(8).FieldLen=   256
               Columns(8).Style=   2
               Columns(8).HasBackColor=   -1  'True
               Columns(8).BackColor=   16777215
               Columns(9).Width=   3200
               Columns(9).Visible=   0   'False
               Columns(9).Caption=   "PORT"
               Columns(9).Name =   "PORT"
               Columns(9).DataField=   "Column 9"
               Columns(9).DataType=   11
               Columns(9).FieldLen=   256
               Columns(10).Width=   1614
               Columns(10).Caption=   "Subasta"
               Columns(10).Name=   "SUBASTA"
               Columns(10).DataField=   "Column 10"
               Columns(10).DataType=   8
               Columns(10).FieldLen=   256
               Columns(10).Style=   2
               Columns(10).HasBackColor=   -1  'True
               Columns(10).BackColor=   16777215
               _ExtentX        =   11827
               _ExtentY        =   2514
               _StockProps     =   79
               ForeColor       =   -2147483630
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblCp 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1185
               TabIndex        =   33
               Top             =   390
               Width           =   1020
            End
            Begin VB.Label lblPaiCod 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1185
               TabIndex        =   35
               Top             =   750
               Width           =   1020
            End
            Begin VB.Label lblMonProveCod 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1185
               TabIndex        =   37
               Top             =   1110
               Width           =   1020
            End
            Begin VB.Label lblProviCod 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1185
               TabIndex        =   39
               Top             =   1470
               Width           =   1020
            End
            Begin VB.Label lblURL 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   315
               Left            =   1185
               TabIndex        =   41
               Top             =   1860
               Width           =   5520
            End
            Begin VB.Label Label13 
               Caption         =   "Moneda:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   40
               TabIndex        =   161
               Top             =   1170
               Width           =   915
            End
            Begin VB.Label Label12 
               Caption         =   "Direcci�n:"
               ForeColor       =   &H00000000&
               Height          =   210
               Left            =   40
               TabIndex        =   160
               Top             =   90
               Width           =   885
            End
            Begin VB.Label Label10 
               Caption         =   "C�digo postal:"
               ForeColor       =   &H00000000&
               Height          =   225
               Left            =   40
               TabIndex        =   159
               Top             =   450
               Width           =   1150
            End
            Begin VB.Label Label9 
               Caption         =   "Poblaci�n:"
               ForeColor       =   &H00000000&
               Height          =   225
               Left            =   2280
               TabIndex        =   158
               Top             =   420
               Width           =   825
            End
            Begin VB.Label Label4 
               Caption         =   "Pa�s:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   40
               TabIndex        =   157
               Top             =   810
               Width           =   1020
            End
            Begin VB.Label Label3 
               Caption         =   "Provincia:"
               ForeColor       =   &H00000000&
               Height          =   225
               Left            =   40
               TabIndex        =   156
               Top             =   1530
               Width           =   1200
            End
            Begin VB.Label lblcalif1 
               Caption         =   "Calificaci�n 1"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   40
               TabIndex        =   155
               Top             =   3900
               Width           =   1290
            End
            Begin VB.Label lblCal1Den 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1380
               TabIndex        =   43
               Top             =   3825
               Width           =   915
            End
            Begin VB.Label lblcal2den 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1380
               TabIndex        =   45
               Top             =   4185
               Width           =   915
            End
            Begin VB.Label lblcal3den 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1380
               TabIndex        =   47
               Top             =   4545
               Width           =   915
            End
            Begin VB.Label lblCalif2 
               Caption         =   "Calificaci�n 2"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   40
               TabIndex        =   154
               Top             =   4245
               Width           =   1290
            End
            Begin VB.Label lblcalif3 
               Caption         =   "Calificaci�n 3"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   40
               TabIndex        =   153
               Top             =   4575
               Width           =   1290
            End
            Begin VB.Label lblMonProveDen 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   2235
               TabIndex        =   38
               Top             =   1110
               Width           =   4455
            End
            Begin VB.Label lblPaiDen 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   2235
               TabIndex        =   36
               Top             =   750
               Width           =   4455
            End
            Begin VB.Label lblProviDen 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   2235
               TabIndex        =   40
               Top             =   1470
               Width           =   4455
            End
            Begin VB.Label lblPob 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   3090
               TabIndex        =   34
               Top             =   390
               Width           =   3600
            End
            Begin VB.Label lblCal1Val 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   2340
               TabIndex        =   44
               Top             =   3825
               Width           =   930
            End
            Begin VB.Label lblCal2Val 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   2340
               TabIndex        =   46
               Top             =   4185
               Width           =   930
            End
            Begin VB.Label lblCal3Val 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   2340
               TabIndex        =   48
               Top             =   4545
               Width           =   930
            End
            Begin VB.Label Label5 
               Caption         =   "URL:"
               ForeColor       =   &H00000000&
               Height          =   255
               Left            =   40
               TabIndex        =   152
               Top             =   1980
               Width           =   915
            End
            Begin VB.Label lblDir 
               BackColor       =   &H00E1FFFF&
               BorderStyle     =   1  'Fixed Single
               ForeColor       =   &H00000000&
               Height          =   285
               Left            =   1185
               TabIndex        =   32
               Top             =   30
               Width           =   5505
            End
         End
         Begin VB.Label lblComent 
            Caption         =   "Comentario"
            Height          =   330
            Left            =   5415
            TabIndex        =   200
            Top             =   525
            Width           =   1275
         End
         Begin VB.Image imgNoOfe 
            Height          =   240
            Left            =   3435
            Picture         =   "frmOFERec.frx":9558
            Top             =   525
            Width           =   240
         End
         Begin VB.Label lblCodPort 
            Caption         =   "C�digo portal:"
            ForeColor       =   &H00000000&
            Height          =   232
            Left            =   234
            TabIndex        =   163
            Top             =   555
            Width           =   1085
         End
         Begin VB.Label lblCodPortProve 
            BackColor       =   &H0099CCFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   1410
            TabIndex        =   31
            Top             =   510
            Width           =   1845
         End
         Begin VB.Label Label11 
            Alignment       =   2  'Center
            BackColor       =   &H00C0E0FF&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Datos del proveedor"
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   -30
            TabIndex        =   162
            Top             =   0
            Width           =   7125
         End
      End
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   300
      Left            =   0
      TabIndex        =   211
      Top             =   0
      Width           =   300
      ExtentX         =   529
      ExtentY         =   529
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   510
      Index           =   4
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   11880
      TabIndex        =   179
      TabStop         =   0   'False
      Top             =   4470
      Visible         =   0   'False
      Width           =   11880
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Index           =   4
         Left            =   1455
         TabIndex        =   181
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdAbrirSobre 
         Caption         =   "&Abrir sobre"
         Height          =   345
         Left            =   120
         TabIndex        =   180
         TabStop         =   0   'False
         Top             =   90
         Width           =   1215
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   510
      Index           =   2
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   11880
      TabIndex        =   104
      TabStop         =   0   'False
      Top             =   5970
      Visible         =   0   'False
      Width           =   11880
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Index           =   2
         Left            =   120
         TabIndex        =   71
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Index           =   2
         Left            =   1230
         TabIndex        =   72
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   10110
         TabIndex        =   73
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Height          =   345
         Left            =   120
         TabIndex        =   106
         TabStop         =   0   'False
         Top             =   90
         Width           =   1005
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   510
      Index           =   3
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   11880
      TabIndex        =   105
      TabStop         =   0   'False
      Top             =   6480
      Visible         =   0   'False
      Width           =   11880
      Begin VB.CommandButton cmdNoOfe 
         Caption         =   "&No ofertar"
         Height          =   345
         Index           =   1
         Left            =   6810
         TabIndex        =   205
         TabStop         =   0   'False
         Top             =   135
         Width           =   1005
      End
      Begin VB.CommandButton cmdGenerarXLS 
         Caption         =   "&Generar excel"
         Height          =   345
         Left            =   3180
         TabIndex        =   67
         TabStop         =   0   'False
         Top             =   135
         Width           =   1245
      End
      Begin VB.CommandButton cmdCargarXLS 
         Caption         =   "&Cargar desde excel"
         Height          =   345
         Left            =   1485
         TabIndex        =   66
         TabStop         =   0   'False
         Top             =   135
         Width           =   1590
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&Neues Angebot"
         Height          =   345
         Left            =   135
         TabIndex        =   65
         TabStop         =   0   'False
         Top             =   135
         Width           =   1260
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Index           =   3
         Left            =   4545
         TabIndex        =   68
         TabStop         =   0   'False
         Top             =   135
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Index           =   3
         Left            =   5670
         TabIndex        =   69
         TabStop         =   0   'False
         Top             =   135
         Width           =   1005
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      Height          =   510
      Index           =   1
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   11880
      TabIndex        =   64
      Top             =   5460
      Width           =   11880
      Begin VB.CommandButton cmdAdjunMasivos 
         Caption         =   "DA&rchivos adjuntos"
         Height          =   345
         Left            =   1230
         TabIndex        =   212
         TabStop         =   0   'False
         Top             =   120
         Width           =   1515
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Index           =   1
         Left            =   5070
         TabIndex        =   53
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   135
         TabIndex        =   50
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Index           =   1
         Left            =   3960
         TabIndex        =   52
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdModificar 
         Caption         =   "&Modificar"
         Height          =   345
         Left            =   2850
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmOFERec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
'Valor m�ximo de NVarcha(MAX)
Private Const cLongAtribEsp_Texto As Long = 32471

'Variables para func. combos
Public bRespetarCombo As Boolean
Public bCargarComboDesde As Boolean
Private m_sLayOut As String 'Contiene el fichero de layout de la grid de precios
Private m_sTemp As String
Private m_iTotalCols As Integer
'Anyo seleccionado
Private m_bMouse As Boolean
Private m_bCargarLayout As Boolean
'Variable de control de flujo de proceso
Public Accion As AccionesSummit
Private sOperacion As String
Public g_bVisor As Boolean
'Variables de seguridad
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRPerfUON As Boolean
Private m_bRMat As Boolean  'Se aplica a la combo de procesos
Private m_bREqp As Boolean  'Se aplica a la combo de procesos y a los proveedores
Private m_bRComp As Boolean 'Se aplica a la combo de procesos y a los proveedores
Private m_bRCompResp As Boolean
Private m_bRUsuAper As Boolean
Private m_bOfeAsigEqp As Boolean
Private m_bOfeAsigComp As Boolean
Private m_bModifResponsable As Boolean
Private m_bModifEquivalencia As Boolean
Private m_bAlta As Boolean
Private m_bAltaProvAsig As Boolean
Private m_bAltaProvCompAsig As Boolean
Private m_bModifOfeDeProv As Boolean
Private m_bModifOfeDeUsu As Boolean
Private m_bRestModifOfeProvUsu As Boolean
Private m_bRestModifOfeUsu As Boolean
Private m_bElimOfeDeProv As Boolean
Private m_bElimOfeDeUsu As Boolean
Private m_bRestElimOfeProvUsu As Boolean
Private m_bRestElimOfeUsu As Boolean
Private g_bSoloInvitado As Boolean
Private m_bGrupoConEscalados As Boolean

Private m_bRestProvMatComp As Boolean

Public bOrigenBuzon As Boolean
Public bOrigenComparativa As Boolean
Public g_sPrec1Obs As String
Public g_sPrec2Obs As String
Public g_sPrec3Obs As String
'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private sayFileNames() As String

'Proceso seleccionado
Public m_oProcesoSeleccionado As cProceso
Public g_oOfertaSeleccionada As COferta
Public m_oProveSeleccionado As CProveedor
Private m_oGrupoSeleccionado As CGrupo
Public g_oGrupoOferSeleccionado As COfertaGrupo
Private m_oGMN1Seleccionado As CGrupoMatNivel1
Private m_oGruposMN1 As CGruposMatNivel1
Private m_oProveedoresProceso As CProveedores
Public m_oOfertasProceso As COfertas
Private m_oIOfertas As iOfertas
Private m_oAtribEnEdicion As CAtributoOfertado
Private m_oAtributo As CAtributo
Public g_oSobreSeleccionado As CSobre

Private m_oProveedoresAsigUsu As CProveedores
Private m_oProveedoresAsigEqp As CProveedores

'Para las asignaciones a grupos
Public m_oAsigs As CAsignaciones

Private oOfertaAA�adir As COferta
Public g_oLineaEnEdicion As CPrecioItem
Private oIBaseDatos As IBaseDatos
Private oIBaseBloqueo As IBaseDatos  'Para controlar el bloqueo del proceso al modificar.

'Colecci�n para cargar los combos
Private oMonedas As CMonedas
Private oMonedasGen As CMonedas 'Colecci�n para saber si la moneda se equivalencia o cambio
Private oEstados As COfeEstados
Private oProcesos As CProcesos

'Modo de edici�n de la grid de items
Private bModoEdicion As Boolean
Private bModError As Boolean

Private dProceEquiv As Double
' variable para listado
Public ofrmLstPROCE As frmLstPROCE

'Ficheros adjuntos
Public bCancelarEsp As Boolean
Public sComentario As String

'Variable para mostrar/ocultar los �tems cerrados en la grid
Public bMostrarTodosItems As Boolean

'Multilenguaje
Private m_sMeCaption As String
Private m_sIdiAtribProce As String
Private m_sIdiAtribGrupo As String
Private m_sIdiAdjunProce As String
Private m_sIdiAdjunGrupo As String
Private m_sIdiProceso As String
Private m_sIdiAtributos As String
Private m_sIdiAdjuntos As String
Private m_sIdiItems As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sIdiUsarPrec(1 To 3) As String
Private m_sIdiTodosItems As String
Private m_sIdiFecFinTradicional As String
Private m_sIdiFecFinSubasta  As String
Private m_sIdiCaptionObs As String

Private m_sIdiSelProceso  As String

Private sIdiMayor As String
Private sIdiMenor As String
Private sIdiEntre As String
Private sIdiFecOfe As String
Private sIdiFecVal As String
Private sIdiMon As String
Private sIdiLaOfe As String
Private sIdiEdicion As String
Private sIdiConsulta As String
Private sIdiCodigo As String
Private sIdiEquivale As String
Private sIdiEquivaleA1 As String
Private sIdiMaterial As String
Private sIdiProve As String
Private sIdiPrecio As String
Private sIdiCantidadMax As String
Private sIdiSelecAdjunto As String
Private sIdiTodosArchivos As String
Private sIdiElArchivo As String
Private sIdiGuardar As String
Private sIdiTipoOrig As String
Private sIdiEstadoSobreAbierto As String
Private sIdiEstadoSobreCerrado As String
Private sIdiSobre As String
Private sIdiOferta As String
Private sIdiDialogoExcel As String
Private sIdiFiltroExcel As String
Private sIdiDialogoCargarXls As String
Private sIdiNoOfe As String
Private sIdiIntroPortal As String
Private sIdiIntroGS As String
Private sIdiUsuario As String
Private sIdiContacto  As String
Private m_sIdiNoOfeGS As String
Private m_sIdiNoOfePORT As String
Private m_sIdiPresUni As String
Private m_sPresUniAbrv As String
Private m_sAtribItem As String
Private m_skb As String
Private m_sHoraRec As String
Private m_vTZHora As Variant

'Variables de escalados
Private m_oEscalados As CEscalados
Private m_oEscalado As CEscalado

Const iAptDatosGenerales = 0
Const iAptDatosGenOferta = 1
Const iAptAtributos = 2
Const iAptAdjuntos = 3
Const iAptItems = 4
Const iAptProveedor = 5
Const iAptGrupo = 6
Const iAptSobre = 7
Const sFormatoNumber As String = "#,##0.########"

'Variables para control de movimiento del rat�n y bloqueo
Private Enum OrigenBloqueo
    NoBloqueo = 0
    Eliminar = 1
    Modificar = 2
    ModifItems = 3
    Anyadir = 4
    responsable = 5
End Enum
Private m_udtOrigBloqueo As OrigenBloqueo

Public g_bIncluir As Boolean
Public g_bCancelar As Boolean

Public g_bAnyadirOfertaObl As Boolean
Private m_nodeSel As MSComctlLib.node
Private m_sPathNuevoNodo As String

Private m_sLitRecalculando As String
Private m_sMensajeModifAtrib As String
Private m_sMensajeModifPrecio As String
Private m_sMensajeAfecta As String
Private m_sMensajeSiContinua As String
Private m_sMensajeContinuar As String
Private m_dcListaZonasHorarias As Dictionary

Private m_sRango As String
Private m_sIdiOtros As String

Private m_sGeneraAdjunto As String
Private m_sGeneraAdjuntos As String


Private Sub ListarProceso()

    Set ofrmLstPROCE = New frmLstPROCE
    ofrmLstPROCE.sOrigen = "frmOFERec"
    ofrmLstPROCE.g_bEsInvitado = m_oProcesoSeleccionado.Invitado
    If Not m_oProcesoSeleccionado Is Nothing Then
        If Not m_oProveSeleccionado Is Nothing Then
             Set ofrmLstPROCE.g_oProveSeleccionado = m_oProveSeleccionado
        End If
        Set ofrmLstPROCE.g_oProceSeleccionado = m_oProcesoSeleccionado
        ofrmLstPROCE.sdbcAnyo.Text = m_oProcesoSeleccionado.Anyo
        ofrmLstPROCE.sdbcGMN1Proce_4Cod.Text = m_oProcesoSeleccionado.GMN1Cod
        ofrmLstPROCE.sdbcGMN1Proce_4Cod_Validate False
        ofrmLstPROCE.txtCod.Text = CStr(m_oProcesoSeleccionado.Cod)
        ofrmLstPROCE.txtDen.Text = m_oProcesoSeleccionado.Den
    End If

    ofrmLstPROCE.Show vbModal
End Sub

Private Sub chkMostrarItems_Click()
    Dim dblPres As Double

        If g_oOfertaSeleccionada Is Nothing Then Exit Sub
        
        If m_bGrupoConEscalados Then
            If Not m_oProcesoSeleccionado Is Nothing Then
                sdbgPreciosEscalados.Scroll -sdbgPreciosEscalados.Cols, 1
            End If
            sdbgPreciosEscalados.RemoveAll
            BorrarGruposPreciosEscalados
        Else
            If Not m_oProcesoSeleccionado Is Nothing Then
                sdbgPrecios.Scroll -sdbgPrecios.Cols, 1
            End If
            sdbgPrecios.RemoveAll
        End If
        
        m_bMouse = False

        bMostrarTodosItems = IIf(ChkMostrarItems.Value = vbChecked, True, False)
        
        If m_bGrupoConEscalados Then
            dblPres = Rellenar_sdbgPreciosEscalados
        Else
            dblPres = Rellenar_sdbgPrecios
        End If
        
        If lblPresGrupo(0).Visible Then
            lblPresGrupo(0).caption = FormateoNumerico(dblPres)
        Else
            lblPresGrupo(1).caption = FormateoNumerico(dblPres)
        End If
        m_bMouse = True
End Sub


Private Sub BorrarGruposPreciosEscalados()
    Dim i As Integer
    Dim iNumGr As Integer
    iNumGr = sdbgPreciosEscalados.Groups.Count
    If iNumGr > 1 Then
        For i = iNumGr - 1 To 1 Step -1
            sdbgPreciosEscalados.Groups.Remove (i)
        Next
    End If
End Sub

Private Sub cmdAbrirAdjun_Click()
    Dim oAdjun As CAdjunto
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
        
    On Error GoTo Cancelar:
    
    If g_oOfertaSeleccionada Is Nothing Then Exit Sub
    
    If sdbgAdjun.Row < 0 Then
        oMensajes.SeleccioneFichero
        Exit Sub
    Else
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & sdbgAdjun.Columns("FICHERO").Value
        sFileTitle = sdbgAdjun.Columns("FICHERO").Value
    End If
    
    If m_oGrupoSeleccionado Is Nothing Then
        'De oferta
        Set oAdjun = g_oOfertaSeleccionada.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
    Else 'De grupo
        Set oAdjun = g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
        Set oAdjun.Grupo = m_oGrupoSeleccionado
    End If
    
    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If
    
    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then FOSFile.DeleteFile sFileName, True
    Set FOSFile = Nothing
    
    Set oAdjun.Oferta = g_oOfertaSeleccionada
    teserror = oAdjun.ComenzarLecturaData
    If teserror.NumError <> TESnoerror Then
        If teserror.NumError = TESAdjuntoEliminadoPortal Then
            oMensajes.ImposibleDescargarArchivo
            Set oAdjun = Nothing
            Exit Sub
        End If
        TratarError teserror
        Set oAdjun = Nothing
        Exit Sub
    End If
    
    oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName
    
    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1

Cancelar:
    If err.Number = 70 Then Resume Next
    Set oAdjun = Nothing
End Sub

''' <summary>
''' Abrir Sobre
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAbrirSobre_Click()
    Dim NodeGrupo As MSComctlLib.node
    Dim NodeAtribsGrupo As MSComctlLib.node
    Dim NodeAdjunGrupo As MSComctlLib.node
    Dim NodeItemsGrupo As MSComctlLib.node
    Dim oOfe As COferta
    Dim oProve As CProveedor
    Dim oGrupo As CGrupo
    Dim sCodOfe As String
    Dim sCodSobre As String
    Dim oNode As MSComctlLib.node
    Dim bCargar As Boolean
    Dim sCod As String
    Dim teserror As TipoErrorSummit
    
    If MostrarFormAbrirSobre(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, g_oSobreSeleccionado.Sobre) Then
        If oMensajes.PreguntaAperturaSobre() = vbYes Then
            g_oSobreSeleccionado.Estado = 1
            g_oSobreSeleccionado.FechaAperturaReal = oFSGSRaiz.DevolverFechaServidor()
            teserror = g_oSobreSeleccionado.AbrirSobre
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            End If
        End If
    End If
    
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If g_oSobreSeleccionado.Estado = 1 Then
        
        MostrarSobre
        
        For Each oNode In tvwProce.Nodes
            If oNode.Tag = "SO_" & CStr(g_oSobreSeleccionado.Sobre) Then
                oNode.Image = "sobreabierto"
            End If
        Next
        
        For Each oProve In m_oProveedoresProceso
            For Each oOfe In m_oOfertasProceso
                If Trim(oOfe.Prove) = Trim(oProve.Cod) Then
                    sCodOfe = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod)) & CStr(oOfe.Num)
                    For Each oGrupo In m_oProcesoSeleccionado.Grupos
                        If oGrupo.Sobre = g_oSobreSeleccionado.Sobre Then
                            If gParametrosGenerales.gbProveGrupos Then
                                sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If m_oAsigs.Item(sCod).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                    bCargar = False
                                Else
                                    bCargar = True
                                End If
                            Else
                                bCargar = True
                            End If
                            If bCargar Then
                                sCodSobre = "SO_" & oProve.Cod & "_" & oOfe.Num & "_" & CStr(oGrupo.Sobre)
                                Set NodeGrupo = tvwProce.Nodes.Add(sCodSobre, tvwChild, "GRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, oGrupo.Codigo & " - " & oGrupo.Den, "Grupo")
                                NodeGrupo.Tag = "GR_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                                
                                If Not oOfe.AtribGrOfertados Is Nothing Then
                                    If oOfe.Grupos.Item(oGrupo.Codigo).NumAtribOfer > 0 Then
                                        Set NodeAtribsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ATGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAtributos, "Atributos")
                                    Else
                                        Set NodeAtribsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ATGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAtributos, "AtributosGris")
                                    End If
                                    NodeAtribsGrupo.Tag = "GT_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                                End If
                                
                                If m_oProcesoSeleccionado.AdjuntarAGrupos Then
                                    If oOfe.Grupos.Item(oGrupo.Codigo).NumAdjuntos > 0 Or (NullToStr(oOfe.Grupos.Item(oGrupo.Codigo).ObsAdjun) <> "") Then
                                        Set NodeAdjunGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ADJUNGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAdjuntos, "Ficheros")
                                    Else
                                        Set NodeAdjunGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ADJUNGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAdjuntos, "FicherosGris")
                                    End If
                                    NodeAdjunGrupo.Tag = "GA_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                                End If
                            
                                If oOfe.Grupos.Item(oGrupo.Codigo).NumPrecios > 0 Then
                                    Set NodeItemsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ITEMSGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiItems, "Item")
                                Else
                                    Set NodeItemsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ITEMSGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiItems, "ItemGris")
                                End If
                                NodeItemsGrupo.Tag = "GI_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                            End If
                        End If
                    Next
                End If
            Next
        Next
        
        Set NodeGrupo = Nothing
        Set NodeAtribsGrupo = Nothing
        Set NodeAdjunGrupo = Nothing
        Set NodeItemsGrupo = Nothing
    End If
End Sub

''' <summary>
''' Boton Aceptar para crear una nueva oferta
''  </summary>
''' <returns>Nada, es una llamada de sistema</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub cmdAceptar_Click()
    Dim teserror As TipoErrorSummit
    Dim sCodOfe As String
    Dim irespuesta As Integer
    Dim bRestaurar As Boolean
    Dim sNodo As String
    Dim bReleerPrecios As Boolean
    Dim oGrupo As COfertaGrupo
    Dim sNumant As String
    Dim vNom As Variant
    Dim bEliminar As Boolean
    Dim bModificar As Boolean
    Dim bHayVacios As Boolean
    Dim oAtr As CAtributo
    Dim oGrupoP As CGrupo
    Dim dtFechaRec As Date
    Dim dtHoraRec As Date
    Dim strHoraRec As String
    Dim bCambioEstado As Boolean
    'Comprobar datos
    If Trim(txtCambio.Text) = "" Then
        oMensajes.NoValido sIdiEquivale
        If Me.Visible Then txtCambio.SetFocus
        Exit Sub
    End If

    If Not IsNumeric(txtCambio.Text) Then
        oMensajes.NoValido sIdiEquivale
        If Me.Visible Then txtCambio.SetFocus
        Exit Sub
    Else
        If CDbl(txtCambio.Text) = 0 Then
            oMensajes.NoValido sIdiEquivale
            If Me.Visible Then txtCambio.SetFocus
            Exit Sub
        End If
    End If
    If txtFecOfe.Text = "" Then
        oMensajes.NoValido sIdiFecOfe
        If Me.Visible Then txtFecOfe.SetFocus
        Exit Sub
    End If
    If Not IsDate(txtFecOfe) Then
        oMensajes.NoValido sIdiFecOfe
        If Me.Visible Then txtFecOfe.SetFocus
        Exit Sub
    End If
    If Not IsDate(txtFecVal) Then
        oMensajes.NoValido sIdiFecVal
        If Me.Visible Then txtFecVal.SetFocus
        Exit Sub
    End If
    If sdbcMonCod.Value = "" Then
        oMensajes.NoValido 6 '"Moneda"
        If Me.Visible Then sdbcMonCod.SetFocus
        Exit Sub
    End If
    If sdbcEstCod.Value = "" Then
        oMensajes.NoValido 92 '"Estado de la oferta"
        If Me.Visible Then sdbcEstCod.SetFocus
        Exit Sub
    End If

    If txtHoraOfe.Text <> "" Then
        If Not IsTime(Trim(txtHoraOfe.Text)) Then
            oMensajes.NoValido m_sHoraRec
            If Me.Visible Then txtHoraOfe.SetFocus
            Exit Sub
        End If
    End If
    bCambioEstado = False
    
    If Accion = ACCRecOfeModDatGen Then
        bCambioEstado = g_oOfertaSeleccionada.CodEst <> sdbcEstCod.Value
        If g_oOfertaSeleccionada.CodEst <> sdbcEstCod.Value Then
            oEstados.CargarTodosLosOfeEstados sdbcEstCod.Value, , True, , , , , basPublic.gParametrosInstalacion.gIdioma
            If oEstados.Item(1).Comparable = False Then
                If m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then
                    teserror = g_oOfertaSeleccionada.OfertaModificable(True, True)
                    If teserror.NumError = TESOfertaAdjudicada Then
                        oMensajes.ImposibleModOfertaProcPedidos teserror.Arg1
                        g_oOfertaSeleccionada.EliminarAdj = False
                        Exit Sub
                    End If
                Else
                    teserror = g_oOfertaSeleccionada.OfertaModificable(True)
                    If teserror.NumError = TESOfertaAdjudicada Then
                        irespuesta = oMensajes.PreguntaHayAdjudicacionesGuardadas
                        If irespuesta = vbNo Then
                            g_oOfertaSeleccionada.EliminarAdj = False
                            Exit Sub
                        Else
                            g_oOfertaSeleccionada.EliminarAdj = True
                        End If
                    End If
                End If
            End If
        End If
    End If

    'Comprobar que no hay un adjudicaci�n a una oferta previa del proveedor
    Dim sMonAdj As String
    If m_oProcesoSeleccionado.HayAdjudicacionesPreviasProveOtraMon(g_oOfertaSeleccionada.Prove, sdbcMonCod.Value, sMonAdj) Then
        oMensajes.ImposibleModificarMonedaAdjudicacionesProce
        Exit Sub
    End If
    
    Select Case Accion
    'A�adiendo una oferta
        Case ACCRecOfeAnya
            'Pasar FECREC a UTC
            strHoraRec = IIf(txtHoraOfe.Text <> "", txtHoraOfe.Text, "0:00:00")
            ConvertirTZaUTC m_vTZHora, txtFecOfe.Text, strHoraRec, dtFechaRec, dtHoraRec
        
            oOfertaAA�adir.FechaHasta = txtFecVal.Text
            oOfertaAA�adir.FechaRec = dtFechaRec    'txtFecOfe.Text
            oOfertaAA�adir.Cambio = txtCambio.Text
            oOfertaAA�adir.CodMon = sdbcMonCod.Value
            oOfertaAA�adir.DenMon = sdbcMonDen.Value
            oOfertaAA�adir.CodEst = sdbcEstCod.Value
            oOfertaAA�adir.DenEst = sdbcEstDen.Value
            oOfertaAA�adir.obs = txtObs.Text
            oOfertaAA�adir.Leida = True
            oOfertaAA�adir.HoraRec = dtHoraRec  'Trim(txtHoraOfe.Text)
            Set oOfertaAA�adir.Usuario = oUsuarioSummit
            
            bRestaurar = False
            If Not oOfertaAA�adir.ComprobarPosibleNumOferta Then
                irespuesta = oMensajes.NumeroOfertaCambiado(oOfertaAA�adir.Num)
                If irespuesta = vbNo Then
                    sNodo = "P_" & oOfertaAA�adir.Prove
                    cmdCancelar_Click 'Se desbloquea aqu�
                    ProcesoSeleccionado
                    tvwProce.Nodes(sNodo).Selected = True
                    tvwProce.Nodes(sNodo).Expanded = True
                    Exit Sub
                End If
                bRestaurar = True
            End If
            
            
            Dim nodx As MSComctlLib.node

            If m_oProcesoSeleccionado.AtributosOfeObl Then
                                    
                g_bAnyadirOfertaObl = True
                
                If picApartado(2).Visible Then
                    'Atributos
                    If sdbgAtributos.DataChanged Then
                        bModError = False
                        If sdbgAtributos.Rows = 1 Then
                            sdbgAtributos.Update
                        Else
                            sdbgAtributos.MoveNext
                            If Not bModError Then
                                sdbgAtributos.MovePrevious
                            End If
                        End If
                    End If
                End If
                                                               
                If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                
                    For Each oAtr In m_oProcesoSeleccionado.ATRIBUTOS
                        If oAtr.Obligatorio Then
                        
                            If g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(oAtr.idAtribProce)) Is Nothing Then
                                                                        
                                bHayVacios = True
                                oOfertaAA�adir.ConAtribsObligatorios = True
                                oMensajes.AtributosOblSinRellenar
                                
                                sNodo = "ATOFE_" & m_oProveSeleccionado.Cod & "_" & CStr(oOfertaAA�adir.Num)
                                tvwProce.Nodes(sNodo).Selected = True
        
                                Set nodx = tvwProce.selectedItem
                                                        
                                Set m_nodeSel = nodx
                                tvwProce_NodeClick nodx
                                
                                
                                tvwProce.Enabled = True
                                picAtrib.Enabled = True
                                sdbgAtributos.Columns(2).Locked = False
                                DoEvents
                                sdbgAtributos.col = 2
                                                                    
                                Exit Sub
                            End If
                    
                            If (IsNull(oAtr.valorBool) And IsNull(oAtr.valorFec) And IsNull(oAtr.valorText) And IsNull(oAtr.valorNum)) _
                                And (IsNull(g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(oAtr.idAtribProce)).valorBool) And IsNull(g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(oAtr.idAtribProce)).valorFec) And IsNull(g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(oAtr.idAtribProce)).valorNum) And IsNull(g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(oAtr.idAtribProce)).valorText)) And oAtr.Obligatorio Then
                                                    
                                bHayVacios = True
                                oOfertaAA�adir.ConAtribsObligatorios = True
                                oMensajes.AtributosOblSinRellenar
                                
                                sNodo = "ATOFE_" & m_oProveSeleccionado.Cod & "_" & CStr(oOfertaAA�adir.Num)
                                tvwProce.Nodes(sNodo).Selected = True
        
                                Set nodx = tvwProce.selectedItem
                                                        
                                Set m_nodeSel = nodx
                                tvwProce_NodeClick nodx
                                
                                
                                tvwProce.Enabled = True
                                picAtrib.Enabled = True
                                sdbgAtributos.Columns(2).Locked = False
                                DoEvents
                                sdbgAtributos.col = 2
                                                                    
                                Exit Sub
                            End If
                        End If
                    Next
                End If
                For Each oGrupoP In m_oProcesoSeleccionado.Grupos
                
                    If Not oGrupoP.ATRIBUTOS Is Nothing Then
                    
                        For Each oAtr In oGrupoP.ATRIBUTOS
                        
                            If oAtr.Obligatorio Then
                        
                                If g_oOfertaSeleccionada.Grupos.Item(oGrupoP.Codigo).AtribOfertados Is Nothing Then
                                                                    
                                    bHayVacios = True
                                    oOfertaAA�adir.ConAtribsObligatorios = True
                                    oMensajes.AtributosOblSinRellenar
                                
                                    sNodo = "ATGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(oOfertaAA�adir.Num) & "_" & oGrupoP.Codigo
                                    tvwProce.Nodes(sNodo).Selected = True
                                        
                                    picAtrib.Enabled = True
                                    sdbgAtributos.Columns(2).Locked = False
                                    DoEvents
                                    sdbgAtributos.col = 2
                                
                                
                                    Set nodx = tvwProce.selectedItem
                                    Set m_nodeSel = nodx
                                    
                                    tvwProce_NodeClick nodx
                                    
                                    Exit Sub
                                                                    
                                Else
                                
                                    If Not g_oOfertaSeleccionada.Grupos.Item(oGrupoP.Codigo).AtribOfertados.Item(CStr(oAtr.idAtribProce)) Is Nothing Then
                                    
                                        If (IsNull(oAtr.valorBool) And IsNull(oAtr.valorFec) And IsNull(oAtr.valorText) And IsNull(oAtr.valorNum)) _
                                        And (IsNull(g_oOfertaSeleccionada.Grupos.Item(oGrupoP.Codigo).AtribOfertados.Item(CStr(oAtr.idAtribProce)).valorBool) And IsNull(g_oOfertaSeleccionada.Grupos.Item(oGrupoP.Codigo).AtribOfertados.Item(CStr(oAtr.idAtribProce)).valorText) And IsNull(g_oOfertaSeleccionada.Grupos.Item(oGrupoP.Codigo).AtribOfertados.Item(CStr(oAtr.idAtribProce)).valorFec) And IsNull(g_oOfertaSeleccionada.Grupos.Item(oGrupoP.Codigo).AtribOfertados.Item(CStr(oAtr.idAtribProce)).valorNum)) _
                                        And oAtr.Obligatorio Then
                                            
                                            bHayVacios = True
                                            oOfertaAA�adir.ConAtribsObligatorios = True
                                            oMensajes.AtributosOblSinRellenar
                                        
                                            sNodo = "ATGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(oOfertaAA�adir.Num) & "_" & oGrupoP.Codigo
                                            tvwProce.Nodes(sNodo).Selected = True
                                                
                                            picAtrib.Enabled = True
                                            sdbgAtributos.Columns(2).Locked = False
                                            DoEvents
                                            sdbgAtributos.col = 2
                                        
                                        
                                            Set nodx = tvwProce.selectedItem
                                            Set m_nodeSel = nodx
                                            
                                            tvwProce_NodeClick nodx
                                            
                                            Exit Sub
                                        End If

                                    Else
                                        bHayVacios = True
                                        oOfertaAA�adir.ConAtribsObligatorios = True
                                        oMensajes.AtributosOblSinRellenar
                                    
                                        sNodo = "ATGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(oOfertaAA�adir.Num) & "_" & oGrupoP.Codigo
                                        tvwProce.Nodes(sNodo).Selected = True
                                            
                                        picAtrib.Enabled = True
                                        sdbgAtributos.Columns(2).Locked = False
                                        DoEvents
                                        sdbgAtributos.col = 2
                                                                        
                                        Set nodx = tvwProce.selectedItem
                                        Set m_nodeSel = nodx
                                        
                                        tvwProce_NodeClick nodx
                                        
                                        Exit Sub
                                    End If
                                End If
                            End If
                        Next
                    End If
                Next
                    
                bRestaurar = True
            End If
            
            Set oIBaseDatos = oOfertaAA�adir
            If gParametrosGenerales.giINSTWEB = ConPortal Then
                frmEsperaPortal.Show
                DoEvents
            End If

            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.AnyadirABaseDatos
            Screen.MousePointer = vbNormal

            If gParametrosGenerales.giINSTWEB = ConPortal Then
                Unload frmEsperaPortal
            End If
            
            Set oIBaseDatos = Nothing
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
            
            Accion = ACCRecOfeCon

            basSeguridad.RegistrarAccion ACCRecOfeAnya, "Anyo:" & sdbcAnyo.Value & "GMN1:" & sdbcGMN1_4Cod.Value & "Proce:" & sdbcProceCod.Value & "Prove:" & m_oProveSeleccionado.Cod & "Ofe:" & oOfertaAA�adir.Num
            With oOfertaAA�adir
                If Not oUsuarioSummit.Persona Is Nothing Then
                    vNom = oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
                Else
                    vNom = Null
                End If
                
                m_oOfertasProceso.Add .Anyo, .GMN1Cod, .Proce, .Prove, .Num, .CodEst, .FechaHasta, .FechaRec, _
                .CodMon, .Cambio, sdbcMonDen.Text, .DenEst, .obs, , , True, , , True, .FECACT, .HoraRec, , False, .Usuario.Cod, vNom
            End With
            
            sCodOfe = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod)) & CStr(oOfertaAA�adir.Num)
            Set m_oOfertasProceso.Item(sCodOfe).AtribProcOfertados = oFSGSRaiz.Generar_CAtributosOfertados
            Set m_oOfertasProceso.Item(sCodOfe).AtribGrOfertados = oFSGSRaiz.Generar_CAtributosOfertados
            Set m_oOfertasProceso.Item(sCodOfe).Grupos = oOfertaAA�adir.Grupos
            
            If bRestaurar Then
                g_bAnyadirOfertaObl = False
                sNodo = "O_" & oOfertaAA�adir.Prove & "_" & oOfertaAA�adir.Num
                ProcesoSeleccionado
                tvwProce.Nodes(sNodo).Selected = True
            Else 'Pongo vacio el estado de la oferta anterior que se ha modificado para que cuando la pinche se recargue
                If tvwProce.selectedItem.Parent.Children > 1 Then
                    sNumant = Trim(tvwProce.selectedItem.Next.Text)
                    sCodOfe = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod)) & sNumant
                    If Not m_oOfertasProceso.Item(sCodOfe) Is Nothing Then
                        m_oOfertasProceso.Item(sCodOfe).CodEst = ""
                    End If
                End If
            End If
            
            tvwProce.Nodes.Item("P_" & oOfertaAA�adir.Prove).Image = "Prove"
            
            DesbloquearProceso
            
            'Realizamos el recalculo de la nueva oferta (basada en otra ya existente) para que cargue la OFE_GR_COSTESDESC
            If oOfertaAA�adir.Num <> 1 Then 'Si es la primera oferta que se a�ade no hay precios ni nada por lo que no hay que recalcular.
                teserror = m_oProcesoSeleccionado.RecalcularOfertas(True, , oOfertaAA�adir.Num, oOfertaAA�adir.Prove)
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Exit Sub
                Else
                    'recargar datos generales de la oferta
                    oOfertaAA�adir.CargarDatosGeneralesOferta gParametrosGenerales.gIdioma
                                        
                    'Registramos el recalculo de la oferta
                    basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Ofertas Anyo:" & CStr(sdbcAnyo) & " Gmn1:" & CStr(sdbcGMN1_4Cod) & " Proce:" & CStr(sdbcProceCod)
                End If
            End If
            
            Set oOfertaAA�adir = Nothing
            
            If g_bAnyadirOfertaObl Then g_bAnyadirOfertaObl = False
            
        Case ACCRecOfeModDatGen

            g_oOfertaSeleccionada.FechaHasta = txtFecVal.Text
            
            If IsDate(g_oOfertaSeleccionada.FechaRec) And IsDate(txtFecOfe.Text) Then
                strHoraRec = IIf(txtHoraOfe.Text <> "", txtHoraOfe.Text, "0:00:00")
                ConvertirTZaUTC m_vTZHora, txtFecOfe.Text, strHoraRec, dtFechaRec, dtHoraRec
                
                If CDate(Format(g_oOfertaSeleccionada.FechaRec, "short date")) <> dtFechaRec Then
                    g_oOfertaSeleccionada.FechaRec = dtFechaRec
                End If
                g_oOfertaSeleccionada.HoraRec = dtHoraRec
            End If
            g_oOfertaSeleccionada.Cambio = txtCambio.Text
            bReleerPrecios = False
            If sdbcMonCod.Value <> g_oOfertaSeleccionada.CodMon Then bReleerPrecios = True
            
            g_oOfertaSeleccionada.CodMon = sdbcMonCod.Value
            g_oOfertaSeleccionada.DenMon = sdbcMonDen.Value
            g_oOfertaSeleccionada.CodEst = sdbcEstCod.Value
            g_oOfertaSeleccionada.DenEst = sdbcEstDen.Value
            g_oOfertaSeleccionada.obs = txtObs.Text
            g_oOfertaSeleccionada.Leida = True
            
            Set oIBaseDatos = g_oOfertaSeleccionada
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.FinalizarEdicionModificando
            Screen.MousePointer = vbNormal
            Set oIBaseDatos = Nothing
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
            
            If bCambioEstado Then teserror = m_oProcesoSeleccionado.CalcularPrecioItem()

            Accion = ACCRecOfeCon
            basSeguridad.RegistrarAccion ACCRecOfeModDatGen, "Anyo:" & sdbcAnyo.Value & "GMN1:" & sdbcGMN1_4Cod.Value & "Proce:" & sdbcProceCod.Value & "Prove:" & g_oOfertaSeleccionada.Prove & "Ofe:" & g_oOfertaSeleccionada.Num
            DesbloquearProceso
            If bReleerPrecios = True Then
               m_oProcesoSeleccionado.CargarAtributos
               g_oOfertaSeleccionada.CargarAtributosProcesoOfertados
               For Each oGrupo In g_oOfertaSeleccionada.Grupos
                    oGrupo.Grupo.CargarAtributos ambito:=AmbGrupo
                    oGrupo.CargarAtributosOfertados
                    oGrupo.Grupo.CargarAtributos ambito:=AmbItem
                    oGrupo.CargarPrecios
                Next
            End If
    End Select
    
    picEdit.Visible = False
    
    '''''''''''********** Permisos para eliminar **********************
    bEliminar = PermisoEliminarOferta
    If bEliminar = True Then
        cmdEliminar.Visible = True
        cmdAdjunMasivos.Left = cmdEliminar.Left + cmdEliminar.Width + 120
        cmdModificar.Left = cmdAdjunMasivos.Left + cmdAdjunMasivos.Width + 120
    Else
        cmdEliminar.Visible = False
        cmdModificar.Left = cmdAdjunMasivos.Left
        cmdModificar.Left = cmdAdjunMasivos.Left + cmdAdjunMasivos.Width + 120
    End If
    cmdRestaurar(1).Left = cmdModificar.Left + cmdModificar.Width + 120
    cmdListado(1).Left = cmdRestaurar(1).Left + cmdRestaurar(1).Width + 120
        
    '''''''''''********** Permisos de modificaci�n **********************
    bModificar = PermisoModificarOferta
    If bModificar = True Then
        cmdModificar.Visible = True
        cmdRestaurar(1).Left = cmdModificar.Left + cmdModificar.Width + 120
    Else
        cmdModificar.Visible = False
        cmdRestaurar(1).Left = cmdModificar.Left
    End If
    cmdListado(1).Left = cmdRestaurar(1).Left + cmdRestaurar(1).Width + 120
    
    picNavigate(1).Visible = True
    picNavigate(0).Visible = False
    fraFechas.Enabled = False
    fraMoneda.Enabled = False
    txtObs.Locked = True
    tvwProce.Enabled = True
    fraProce.Enabled = True
End Sub

Private Sub cmdAdjunMasivos_Click()
Dim sURL, params, strSessionId As String

frmADJCargar.Show
frmADJCargar.Refresh
With WebBrowser1
    .Width = 1
    .Height = 1
    .Top = -5
End With
'Creamos el parametro a pasar con el id de la oferta
params = "&Anyo=" & g_oOfertaSeleccionada.Anyo & "&GMN1=" & g_oOfertaSeleccionada.GMN1Cod & "&Proce=" & g_oOfertaSeleccionada.Proce & _
         "&Prove=" & g_oOfertaSeleccionada.Prove & "&Ofe=" & g_oOfertaSeleccionada.Num
' Se crea el id de sesion de fullstep web
strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
sURL = gParametrosGenerales.gsRutasFullstepWeb & "/App_Pages/GS/Ofertas/downloadAdjuntosZip.aspx"
sURL = sURL & "?desdeGS=1&SessionId=" & strSessionId & params
WebBrowser1.Navigate2 sURL, 4

End Sub

Public Sub cmdA�adir_Click()
    If m_oProveSeleccionado Is Nothing Then Exit Sub
    Accion = ACCRecOfeAnya
    AnyadirOfertaProve
End Sub

''' <summary> Datos por defecto para la nueva oferta </summary>
''' <remarks>Llamada desde: cmdA�adir_Click</remarks>

Private Sub AnyadirOfertaProve()
    Dim i As Integer
    Dim iOferta As Integer
    Dim oMon As CMoneda
    Dim Num As Integer
    Dim sCod As String
    Dim dtFechaOfe As Date
    Dim dtHoraOfe As Date
    Dim vFechaUTC As Variant
    
    LimpiarCampos (1)
    MostrarApartado 1, True
    
    'Mostrar la hora del la TZ del usuario
    dtFechaOfe = Now
    dtHoraOfe = Time
    vFechaUTC = ObtenerFechaUTC(Now)
    If vFechaUTC <> "" Then ConvertirUTCaTZ DateValue(CDate(vFechaUTC)), TimeValue(CDate(vFechaUTC)), m_vTZHora, dtFechaOfe, dtHoraOfe
        
    txtFecOfe.Text = Format(dtFechaOfe, "short date")
    txtHoraOfe.Text = Format(dtHoraOfe, "long time")
    For i = 0 To 3
        picNavigate(i).Visible = False
    Next
    
    picEdit.Visible = True
    fraFechas.Enabled = True
    fraMoneda.Enabled = True
    txtObs.Locked = False
    tvwProce.Enabled = False
    fraProce.Enabled = False
    
    Set oOfertaAA�adir = oFSGSRaiz.Generar_COferta
    oOfertaAA�adir.Anyo = val(sdbcAnyo.Value)
    oOfertaAA�adir.GMN1Cod = sdbcGMN1_4Cod.Value
    oOfertaAA�adir.Proce = sdbcProceCod.Value
    oOfertaAA�adir.Prove = m_oProveSeleccionado.Cod
        
    iOferta = oOfertaAA�adir.DevolverPosibleNumOferta
    
    'Si hay ofertas anteriores cojo las Obs y las ObsAdjun
    If Not m_oOfertasProceso Is Nothing Then
        Num = 1
        sCod = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod))
        sCod = sCod & CStr(Num)
        While Not m_oOfertasProceso.Item(sCod) Is Nothing
            Num = Num + 1
            sCod = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod))
            sCod = sCod & CStr(Num)
        Wend
        Num = Num - 1
        sCod = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod))
        sCod = sCod & CStr(Num)
        If Num > 0 Then
            oOfertaAA�adir.obs = m_oOfertasProceso.Item(sCod).obs
            txtObs.Text = NullToStr(m_oOfertasProceso.Item(sCod).obs)
            oOfertaAA�adir.ObsAdjun = m_oOfertasProceso.Item(sCod).ObsAdjun
        End If
    End If
    
    AnyadirOfertaATree iOferta
    
    Set oMon = MonedaNuevaOferta(m_oProveSeleccionado.Cod, iOferta)
    
    If oMon Is Nothing Then
        If Not oMonedasGen.Item(gParametrosInstalacion.gsMoneda) Is Nothing Then
            bRespetarCombo = True
            sdbcMonCod.Value = gParametrosInstalacion.gsMoneda
            sdbcMonDen.Value = oMonedasGen.Item(gParametrosInstalacion.gsMoneda).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            bRespetarCombo = False
        Else
            If Not oMonedasGen.Item(gParametrosGenerales.gsMONCEN) Is Nothing Then
                bRespetarCombo = True
                sdbcMonCod.Value = gParametrosGenerales.gsMONCEN
                sdbcMonDen.Value = oMonedasGen.Item(gParametrosGenerales.gsMONCEN).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                bRespetarCombo = False
            End If
        End If
    Else
        bRespetarCombo = True
        sdbcMonCod.Value = oMon.Cod
        sdbcMonDen.Value = oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        bRespetarCombo = False
    End If
    Set oMon = Nothing
    
    If sdbcMonCod.Value = m_oProcesoSeleccionado.MonCod Then
        txtCambio.Text = 1
        lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
    Else
        txtCambio.Text = oMonedasGen.Item(sdbcMonCod.Value).Equiv / dProceEquiv
        lblEquivalencia.caption = oMonedasGen.Item(sdbcMonCod.Value).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
    End If
    
    txtCambio.ToolTipText = DblToStr(txtCambio.Text)
    
    If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Value).EQ_ACT = False Then
        txtCambio.Backcolor = RGB(255, 255, 192)
        txtCambio.Locked = True
        txtCambio.TabStop = False
    Else
        txtCambio.Backcolor = RGB(255, 255, 255)
        txtCambio.Locked = False
        txtCambio.TabStop = True
    End If
    If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
        txtCambio.Backcolor = RGB(255, 255, 192)
        txtCambio.Locked = True
        txtCambio.TabStop = False
    End If
    
    lblMonProceso.caption = m_oProcesoSeleccionado.MonCod & " - " & oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    If gParametrosGenerales.gsESTINI <> "" Then
        bRespetarCombo = True
        sdbcEstCod = gParametrosGenerales.gsESTINI
        sdbcEstCod_Validate False
        bRespetarCombo = False
    End If
    
    Me.lblOrigen.Visible = True
    If oUsuarioSummit.Persona Is Nothing Then
        Me.lblOrigen.caption = sIdiIntroGS & " " & sIdiUsuario & " " & oUsuarioSummit.Cod
    Else
        Me.lblOrigen.caption = sIdiIntroGS & " " & sIdiUsuario & " " & oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
    End If
End Sub

''' <summary>Devuelve la moneda para una oferta nueva</summary>
''' <param name="sCodProve">C�digo del proveedor de la nueva oferta</param>
''' <param name="iNumOfe">N�mero de la nueva oferta</param>
''' <remarks>Llamada desde: AnyadirOfertaProve</remarks>
Private Function MonedaNuevaOferta(ByVal scodProve As String, ByVal iNumOfe As Integer) As CMoneda
    Dim oMon As CMoneda
    Dim oOferta As COferta
    
    If m_oProcesoSeleccionado.CambiarMonOferta Then
        Set oMon = oOfertaAA�adir.DevolverPosibleMoneda
    Else
        If Not oMonedasGen.Item(m_oProcesoSeleccionado.MonCod) Is Nothing Then
            Set oMon = oMonedasGen.Item(m_oProcesoSeleccionado.MonCod)
        End If
    End If
    
    'Si el proveedor ya tiene ofertas adjudicadas la moneda deber� ser la de las ofertas ya creadas
    For Each oOferta In m_oOfertasProceso
        If oOferta.Prove = scodProve Then
            'Comprobar si ya hay adjudicaciones
            Dim sMonAdj As String
            If m_oProcesoSeleccionado.HayAdjudicacionesPreviasProveOtraMon(scodProve, oMon.Cod, sMonAdj) Then
                Set oMon = oMonedasGen.Item(sMonAdj)
            End If
        End If
    Next
    
    Set MonedaNuevaOferta = oMon
End Function

''' <summary>
''' A�ade un adjunto a la oferta
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: depende del tama�o de fichero</remarks>
Private Sub cmdA�adirAdjun_Click()
Dim DataFile As Integer
Dim Fl As Long
Dim Chunk() As Byte
Dim lSize As Long
Dim i As Integer
Dim oAdjun As CAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject
Dim dAvanceEspera As Double
Dim bLleva(9) As Boolean

On Error GoTo Cancelar:

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub

    teserror = g_oOfertaSeleccionada.OfertaModificable
    If teserror.NumError <> TESnoerror Then
        If teserror.NumError <> TESOfertaAdjudicada Then
            TratarError teserror
            Exit Sub
        End If
    End If

    cmmdAdjun.DialogTitle = sIdiSelecAdjunto
    cmmdAdjun.Filter = sIdiTodosArchivos & "|*.*"
    cmmdAdjun.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    cmmdAdjun.filename = ""
    cmmdAdjun.ShowOpen

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle

    If sFileName = "" Then Exit Sub
    
    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = False
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    frmPROCEComFich.sOrigen = "frmOFERec"
    bCancelarEsp = False
    frmPROCEComFich.Show 1

    If Not bCancelarEsp Then
        If UBound(arrFileNames) > 1 Then
            frmESPERA.lblGeneral.caption = m_sGeneraAdjuntos
        Else
            frmESPERA.lblGeneral.caption = m_sGeneraAdjunto
        End If
        frmESPERA.lblGeneral.Refresh
        frmESPERA.lblDetalle = ""
        frmESPERA.lblDetalle.Refresh
        
        frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
        frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
        
        frmESPERA.ProgressBar1.Max = UBound(arrFileNames)
        frmESPERA.ProgressBar2.Max = 100
        
        frmESPERA.Show
        DoEvents
    
        frmESPERA.ProgressBar1.Value = 0
        
        Set oFos = New Scripting.FileSystemObject
        For iFile = 1 To UBound(arrFileNames)
            frmESPERA.Frame2.caption = arrFileNames(iFile)
            frmESPERA.Frame2.Refresh
                                
            frmESPERA.ProgressBar2.Value = 0
            
            For i = 0 To 9
                bLleva(i) = False
            Next
                                
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            Set oAdjun = oFSGSRaiz.generar_cadjunto
            Set oAdjun.Oferta = g_oOfertaSeleccionada
            If Not m_oGrupoSeleccionado Is Nothing Then
                Set oAdjun.Grupo = m_oGrupoSeleccionado
            End If
            
            oAdjun.nombre = sFileTitle
            oAdjun.Comentario = sComentario

            DataFile = 1
            'Abrimos el fichero para lectura binaria
            Open sFileName For Binary Access Read As DataFile
            Fl = LOF(DataFile)    ' Length of data in file
            If Fl = 0 Then
                Unload frmESPERA
                Close DataFile
                Set oAdjun = Nothing
                Exit Sub
            End If
    
            If Fl + g_oOfertaSeleccionada.AdjunSize > gParametrosGenerales.glMAXADJUN * 1024 Then
                Unload frmESPERA
                oMensajes.TamanoMaximoOfertaSuperado
                Close DataFile
                Set oAdjun = Nothing
                Exit Sub
            End If
    
            ' Se lo asignamos en bloques a la especificacion
            If giChunkSize > Fl Then
                lSize = Fl
            Else
                lSize = giChunkSize
            End If
            ReDim Chunk(lSize - 1)
            Get DataFile, , Chunk()
            teserror = oAdjun.ComenzarEscrituraData
            If teserror.NumError <> TESnoerror Then
                Unload frmESPERA
                basErrores.TratarError teserror
                Set oAdjun = Nothing
                Close DataFile
                Exit Sub
            End If
            
            dAvanceEspera = (lSize * 100) / Fl
                            
            Dim sAdjunto As String
            Dim ArrayAdjunto() As String
            Dim oFile As File
            Dim bites As Long
                
            Set oFile = oFos.GetFile(sFileName)
            bites = oFile.Size
            oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
            
            sAdjunto = oAdjun.GrabarAdjuntoOferta(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites)
            
            'Creamos un array, cada "substring" se asignar� a un elemento del array
            ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
            oAdjun.Id = ArrayAdjunto(0)
            oAdjun.DataSize = bites
            
            teserror = oAdjun.FinalizarEscrituraData(Fl)
            If teserror.NumError <> TESnoerror Then
                Unload frmESPERA
                basErrores.TratarError teserror
                Set oAdjun = Nothing
                Close DataFile
                Exit Sub
            End If
    
            Close DataFile

            basSeguridad.RegistrarAccion AccionesSummit.ACCRecOfeAnyaAdjun, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & m_oProveSeleccionado.Cod & "Ofe:" & g_oOfertaSeleccionada.Num & "Archivo:" & sFileTitle

            g_oOfertaSeleccionada.AdjunSize = g_oOfertaSeleccionada.AdjunSize + Fl
            If g_oGrupoOferSeleccionado Is Nothing Then
                g_oOfertaSeleccionada.Adjuntos.Add oAdjun.Id, oAdjun.nombre, oAdjun.DataSize, oAdjun.Comentario
                g_oOfertaSeleccionada.NumAdjuntos = g_oOfertaSeleccionada.NumAdjuntos + 1
            Else
                g_oGrupoOferSeleccionado.Adjuntos.Add oAdjun.Id, oAdjun.nombre, oAdjun.DataSize, oAdjun.Comentario
                g_oGrupoOferSeleccionado.NumAdjuntos = g_oGrupoOferSeleccionado.NumAdjuntos + 1
            End If
            If tvwProce.selectedItem.Image = "FicherosGris" Then
                tvwProce.selectedItem.Image = "Ficheros"
            End If
            
            sdbgAdjun.AddItem oAdjun.Id & Chr(m_lSeparador) & sFileTitle & Chr(m_lSeparador) & TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb & Chr(m_lSeparador) & NullToStr(oAdjun.Comentario)
            
            frmESPERA.ProgressBar2.Value = 100
            ProgresoBarra bLleva
        Next
        sdbgAdjun.Refresh
    End If

    Unload frmESPERA

    Set oAdjun = Nothing
    Exit Sub

Cancelar:
    On Error Resume Next
    Unload frmESPERA
    If err.Number <> 32755 Then
        Close DataFile
        Set oAdjun = Nothing
    End If
End Sub

Private Sub cmdBuscar_Click()
    
    If Accion = ACCRecOfeCon Then
        frmPROCEBuscar.bRDest = False
        frmPROCEBuscar.bRUsuAper = m_bRUsuAper
        frmPROCEBuscar.bRAsig = m_bRComp
        frmPROCEBuscar.bREqpAsig = m_bREqp
        frmPROCEBuscar.bRMat = m_bRMat
        frmPROCEBuscar.bRCompResponsable = m_bRCompResp
        frmPROCEBuscar.bRUsuDep = m_bRUsuDep
        frmPROCEBuscar.bRUsuUON = m_bRUsuUON
        frmPROCEBuscar.m_bProveAsigComp = m_bOfeAsigComp
        frmPROCEBuscar.m_bProveAsigEqp = m_bOfeAsigEqp
        frmPROCEBuscar.bRestProvMatComp = m_bRestProvMatComp
        frmPROCEBuscar.bRestProvEquComp = False
        frmPROCEBuscar.sOrigen = "frmOFERec"
        frmPROCEBuscar.sdbcAnyo = sdbcAnyo
        frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
        frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
        frmPROCEBuscar.Show 1
    End If
End Sub
Public Sub CargarProcesoConBusqueda()

    LimpiarCampos (10)
    
    Set oProcesos = Nothing
    Set oProcesos = frmPROCEBuscar.oProceEncontrados
    
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo = oProcesos.Item(1).Anyo
    bRespetarCombo = True
    sdbcGMN1_4Cod = oProcesos.Item(1).GMN1Cod
    sdbcGMN1_4Cod_Validate False
    If Not oProcesos.Item(1) Is Nothing Then
        If oProcesos.Item(1).Estado >= TipoEstadoProceso.conasignacionvalida And oProcesos.Item(1).Estado <= TipoEstadoProceso.ConItemsSinValidar Then
            ProceSelector1.Seleccion = 0
        Else
            If oProcesos.Item(1).Estado >= TipoEstadoProceso.validado And oProcesos.Item(1).Estado <= TipoEstadoProceso.PreadjYConObjNotificados Then
                ProceSelector1.Seleccion = 1
            ElseIf oProcesos.Item(1).Estado = TipoEstadoProceso.ParcialmenteCerrado Then
                ProceSelector1.Seleccion = 7
            Else
                ProceSelector1.Seleccion = 2
            End If
        End If
    End If
    sdbcProceCod = oProcesos.Item(1).Cod
    sdbcProceDen = oProcesos.Item(1).Den
    sdbcProceCod_Validate False
    bRespetarCombo = False
    ProcesoSeleccionado
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendarFecOfe_Click()
    AbrirFormCalendar Me, txtFecOfe
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendarValHas_Click()
    AbrirFormCalendar Me, txtFecVal
End Sub

Private Sub cmdCancelar_Click()
    Select Case Accion
        Case ACCRecOfeAnya
            Accion = ACCRecOfeCon
            Set g_oOfertaSeleccionada = Nothing
            Set oOfertaAA�adir = Nothing
            QuitarOfertaDeTree
            DesbloquearProceso
            
        Case ACCRecOfeModDatGen
            Accion = ACCRecOfeCon
            DesbloquearProceso
            MostrarApartado 1
            MostrarOferta
    End Select
    
    picEdit.Visible = False
    fraProce.Enabled = True
    fraFechas.Enabled = False
    fraMoneda.Enabled = False
    txtObs.Locked = True
    tvwProce.Enabled = True
End Sub

Private Sub cmdCargarXLS_Click()
    ObtenerDatosDeXLS
End Sub

Private Sub cmdColaboracion_Click()
    Dim strSessionId As String
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    With frmEditor
        .caption = "FULLSTEP Networks"
        .g_sRuta = gParametrosGenerales.gsURLCN & "cn_NuevoMensaje_GS.aspx?sessionId=" & strSessionId & "&desdeGS=1"
        .g_sOrigen = "Colaboracion"
        .g_iAnio_ProceCompra = sdbcAnyo.Value
        .g_sGMN1_ProceCompra = sdbcGMN1_4Cod.Value
        .g_iCod_ProceCompra = sdbcProceCod.Value
        .g_bReadOnly = False
        .Show vbModal
    End With
End Sub

Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la Destino actual
bModError = False
If picApartado(2).Visible Then
    sdbgAtributos.CancelUpdate
    sdbgAtributos.DataChanged = False

    If Not m_oAtribEnEdicion Is Nothing Then
        Set m_oAtribEnEdicion = Nothing
    End If

    cmdDeshacer.Enabled = False

    Accion = ACCRecOfeCon

    If Me.Enabled And Me.Visible Then sdbgAtributos.SetFocus

ElseIf picApartado(4).Visible Then
    If m_bGrupoConEscalados Then
        sdbgPreciosEscalados.CancelUpdate
        sdbgPreciosEscalados.DataChanged = False
    Else
        sdbgPrecios.CancelUpdate
        sdbgPrecios.DataChanged = False
    End If
    
    If Not g_oLineaEnEdicion Is Nothing Then Set g_oLineaEnEdicion = Nothing
    
    cmdDeshacer.Enabled = False

    Accion = ACCRecOfeCon
    
    If m_bGrupoConEscalados Then
        If Me.Enabled And Me.Visible Then sdbgPreciosEscalados.SetFocus
    Else
        If Me.Enabled And Me.Visible Then sdbgPrecios.SetFocus
    End If
End If
End Sub

''' <summary>
''' Eliminar una oferta
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdEliminar_Click()
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim sCod As String

    If Not BloquearProceso Then Exit Sub
    m_udtOrigBloqueo = OrigenBloqueo.Eliminar
    
    irespuesta = oMensajes.PreguntaEliminar(sIdiLaOfe & " " & g_oOfertaSeleccionada.Num)

    If irespuesta = vbYes Then
        Screen.MousePointer = vbHourglass
        
        teserror = g_oOfertaSeleccionada.OfertaModificable
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESOfertaAdjudicada Then
                oMensajes.OfertaAdjudicada (1) 'Eliminar
                DesbloquearProceso
                Exit Sub
            Else
                DesbloquearProceso
                TratarError teserror
                Exit Sub
            End If
        End If
        Set oIBaseDatos = g_oOfertaSeleccionada
        teserror = oIBaseDatos.EliminarDeBaseDatos
        
        If teserror.NumError = TESnoerror Then teserror = m_oProcesoSeleccionado.CalcularPrecioItem()
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            
            basErrores.TratarError teserror
        Else
            basSeguridad.RegistrarAccion ACCRecOfeEli, "Anyo:" & sdbcAnyo.Value & "GMN1:" & sdbcGMN1_4Cod.Value & "Proce:" & sdbcProceCod.Value & "Prove:" & g_oOfertaSeleccionada.Prove & "Ofe:" & g_oOfertaSeleccionada.Num
            sCod = g_oOfertaSeleccionada.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(g_oOfertaSeleccionada.Prove)) & CStr(g_oOfertaSeleccionada.Num)
            m_oOfertasProceso.Remove sCod
            If m_oOfertasProceso.Count = 0 Then
                m_oProveSeleccionado.HaOfertado = False
            End If
            Set g_oOfertaSeleccionada = Nothing
            QuitarOfertaDeTree
        End If
        
    End If

    DesbloquearProceso
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub cmdEliminarAdjun_Click()
    Dim oAdjun As CAdjunto
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    
    On Error GoTo Cancelar:

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub

    If sdbgAdjun.Row < 0 Then
        oMensajes.SeleccioneFichero
    Else
    
        'Comprobamos que se pueda editar la oferta
        teserror = g_oOfertaSeleccionada.OfertaModificable
        If teserror.NumError <> TESnoerror Then
            If teserror.NumError <> TESOfertaAdjudicada Then
                TratarError teserror
                Exit Sub
            End If
        End If
    
        irespuesta = oMensajes.PreguntaEliminar(sIdiElArchivo & ": " & sdbgAdjun.Columns("FICHERO").Value)

        If irespuesta = vbNo Then Exit Sub

        Screen.MousePointer = vbHourglass
        
        If m_oGrupoSeleccionado Is Nothing Then
            'De oferta
            Set oAdjun = g_oOfertaSeleccionada.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
        Else 'De grupo
            Set oAdjun = g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
            Set oAdjun.Grupo = m_oGrupoSeleccionado
        End If
        Set oAdjun.Oferta = g_oOfertaSeleccionada
        Set oIBaseDatos = oAdjun

        teserror = oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            If m_oGrupoSeleccionado Is Nothing Then
                g_oOfertaSeleccionada.Adjuntos.Remove (CStr(sdbgAdjun.Columns("ID").Value))
                g_oOfertaSeleccionada.NumAdjuntos = g_oOfertaSeleccionada.NumAdjuntos - 1
                If g_oOfertaSeleccionada.NumAdjuntos <= 0 Then
                    tvwProce.selectedItem.Image = "FicherosGris"
                End If
            Else
                g_oGrupoOferSeleccionado.Adjuntos.Remove (CStr(sdbgAdjun.Columns("ID").Value))
                g_oGrupoOferSeleccionado.NumAdjuntos = g_oGrupoOferSeleccionado.NumAdjuntos - 1
                If g_oGrupoOferSeleccionado.NumAdjuntos <= 0 Then
                    tvwProce.selectedItem.Image = "FicherosGris"
                End If
            End If
            
            basSeguridad.RegistrarAccion AccionesSummit.ACCRecOfeAdjunEli, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & g_oOfertaSeleccionada.Prove & "Ofe:" & g_oOfertaSeleccionada.Num & "Archivo:" & sdbgAdjun.Columns("FICHERO").Value

            sdbgAdjun.RemoveItem sdbgAdjun.AddItemRowIndex(sdbgAdjun.Bookmark)
        End If

        Set oAdjun = Nothing
        Set oIBaseDatos = Nothing
    End If

    Screen.MousePointer = vbNormal

Cancelar:
    Set oAdjun = Nothing
    Set oIBaseDatos = Nothing
End Sub

Private Sub cmdGenerarXLS_Click()
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim bEnsenyarAtrEsp As Boolean
    Dim lCountAtrEspeGrupo As Long
    Dim i As Integer

    bEnsenyarAtrEsp = False
    'se llama a la pantalla frmAtEspeExcell si el proceso tiene atributos de especificacion (bEnsenyarAtrEsp = True)
    If Not m_oProcesoSeleccionado Is Nothing Then
        m_oProcesoSeleccionado.CargarAEspecificacionesP
        If m_oProcesoSeleccionado.AtributosEspecificacion.Count > 0 Then
            bEnsenyarAtrEsp = True
        Else
            m_oProcesoSeleccionado.CargarTodosLosGrupos bSinpres:=True
            For Each oGrupo In m_oProcesoSeleccionado.Grupos
                oGrupo.CargarAEspecificacionesG
                lCountAtrEspeGrupo = IIf(oGrupo.AtributosEspecificacion Is Nothing, 0, oGrupo.AtributosEspecificacion.Count)
                If lCountAtrEspeGrupo > 0 Then
                    bEnsenyarAtrEsp = True: Exit For
                End If
            Next
            
            If Not bEnsenyarAtrEsp Then
                For Each oGrupo In m_oProcesoSeleccionado.Grupos
                    oGrupo.CargarTodosLosItems OrdItemPorOrden, bSoloConfirmados:=True
                    If oGrupo.Items.Count > 0 Then
                        For i = 1 To oGrupo.Items.Count
                            Set oItem = oGrupo.Items.Item(i)
                            oItem.CargarTodosLosAtributosEspecificacion
                            If Not oItem.AtributosEspecificacion Is Nothing Then
                                If oItem.AtributosEspecificacion.Count > 0 Then
                                    bEnsenyarAtrEsp = True: Exit For
                                End If
                            End If
                        Next
                    End If
                    If bEnsenyarAtrEsp Then Exit For
                Next
            End If
        End If
        '�����������������������������������������������������������������������

        g_bIncluir = (bEnsenyarAtrEsp And m_oProcesoSeleccionado.MostrarAtribExpExcelOfe)
    End If
    
    GenerarExcel
End Sub

Private Sub cmdlistado_Click(Index As Integer)
    ListarProceso
End Sub


''' <summary>
''' Evento del boton modificar a nivel de oferta en el arbol
''' </summary>
''' <returns>Nada, es una llamada de sistema</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub cmdModificar_Click()
    Dim teserror As TipoErrorSummit
    Dim bModificar As Boolean

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub
    
    If Not BloquearProceso Then Exit Sub
    m_udtOrigBloqueo = Modificar
    
    teserror = g_oOfertaSeleccionada.OfertaModificable
    If teserror.NumError <> TESnoerror Then
        If teserror.NumError = TESOfertaAdjudicada Then
            oMensajes.OfertaAdjudicada
            DesbloquearProceso
            Exit Sub
        Else
            DesbloquearProceso
            TratarError teserror
            Exit Sub
        End If
    End If
    
    bModificar = PermisoModificarOferta
    If bModificar = True Then
    
        fraFechas.Enabled = True
        cmdCalendarFecOfe.Enabled = True
        cmdCalendarValHas.Enabled = True
        txtFecOfe.Locked = False
        txtHoraOfe.Locked = False
        txtFecVal.Locked = False
        
        fraMoneda.Enabled = True
        If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        Else
            txtCambio.Backcolor = RGB(255, 255, 255)
            txtCambio.Locked = False
            txtCambio.TabStop = True
        End If
        If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        End If
        
        txtObs.Locked = False
        
        picNavigate(1).Visible = False
        picEdit.Visible = True
        fraProce.Enabled = False
        tvwProce.Enabled = False
        Accion = ACCRecOfeModDatGen
    Else
    
        fraFechas.Enabled = True
        cmdCalendarFecOfe.Enabled = False
        cmdCalendarValHas.Enabled = False
        txtFecOfe.Locked = True
        txtHoraOfe.Locked = True
        txtFecVal.Locked = True
        
        fraMoneda.Enabled = False
        
        txtObs.Locked = True
        picNavigate(1).Visible = False
        picEdit.Visible = True
        fraProce.Enabled = False
        tvwProce.Enabled = False
        Accion = ACCRecOfeModDatGen
    
    End If
End Sub

Private Sub cmdModificarAdjun_Click()
    Dim teserror As TipoErrorSummit

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub

    If sdbgAdjun.Row < 0 Then
        oMensajes.SeleccioneFichero
    Else
        'Comprobamos que se pueda editar la oferta
        teserror = g_oOfertaSeleccionada.OfertaModificable
        If teserror.NumError <> TESnoerror Then
            If teserror.NumError <> TESOfertaAdjudicada Then
                TratarError teserror
                Exit Sub
            End If
        End If

        Accion = ACCRecOfeAdjunMod
        If m_oGrupoSeleccionado Is Nothing Then
            Set g_oOfertaSeleccionada.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value)).Oferta = g_oOfertaSeleccionada
            Set oIBaseDatos = g_oOfertaSeleccionada.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
        Else
            Set g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value)).Oferta = g_oOfertaSeleccionada
            Set g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value)).Grupo = m_oGrupoSeleccionado
            Set oIBaseDatos = g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
        End If
        teserror = oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oIBaseDatos = Nothing
            Exit Sub
        End If
        
        If m_oGrupoSeleccionado Is Nothing Then
            frmPROCEEspMod.g_sOrigen = "frmOFERecO"
        Else
            frmPROCEEspMod.g_sOrigen = "frmOFERecG"
        End If
        Set frmPROCEEspMod.g_oIBaseDatos = oIBaseDatos
        frmPROCEEspMod.Show 1

    End If
    
    Set oIBaseDatos = Nothing
End Sub

''' <summary>
''' Evento del boton Edicion del grid de items de grupos de la oferta
''' </summary>
''' <returns>Nada, es una llamada de sistema</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub cmdModoEdicion_Click()
Dim teserror As TipoErrorSummit
Dim i, j As Integer
Dim bModificar As Boolean
Dim ogroup As SSDataWidgets_B.Group
    If bModoEdicion Then  'De edici�n a consulta
        If picApartado(2).Visible Then
            'Atributos
            If sdbgAtributos.DataChanged Then
                bModError = False
                If sdbgAtributos.Rows = 1 Then
                    sdbgAtributos.Update
                Else
                    sdbgAtributos.MoveNext
                    If Not bModError Then
                        sdbgAtributos.MovePrevious
                    End If
                End If
            End If
        ElseIf picApartado(4).Visible Then
            'Items
            picChkItems.Enabled = True
            picComboGrupo.Enabled = True
            If m_bGrupoConEscalados Then
                If sdbgPreciosEscalados.DataChanged Then
                    bModError = False
                    If sdbgPreciosEscalados.Rows = 1 Then
                        sdbgPreciosEscalados.Update
                    Else
                        sdbgPreciosEscalados.MoveNext
                        If Not bModError Then
                            sdbgPreciosEscalados.MovePrevious
                        End If
                    End If
                End If
            Else
                If sdbgPrecios.DataChanged Then
                    bModError = False
                    If sdbgPrecios.Rows = 1 Then
                        sdbgPrecios.Update
                    Else
                        sdbgPrecios.MoveNext
                        If Not bModError Then
                            sdbgPrecios.MovePrevious
                        End If
                    End If
                End If
            End If
        End If
        If Not bModError Then
            cmdModoEdicion.caption = sIdiEdicion
            cmdRestaurar(2).Visible = True
            cmdListado(2).Visible = True
            cmdDeshacer.Visible = False
            bModoEdicion = False
            tvwProce.Enabled = True
            fraProce.Enabled = True
            If picApartado(2).Visible Then
                sdbgAtributos.Columns(2).Locked = True
                Select Case sdbgAtributos.Columns("TIPO").Value
                    Case TipoString, TipoTextoCorto, TipoTextoMedio, TipoTextoLargo
                        sdbgAtributos.Columns("VALOR").Style = ssStyleEditButton
                    Case Else
                        sdbgAtributos.Columns("VALOR").Style = ssStyleEdit
                End Select
            ElseIf picApartado(4).Visible Then
                If m_bGrupoConEscalados Then
                    For i = 1 To sdbgPreciosEscalados.Groups.Count - 1
                        If sdbgPreciosEscalados.Groups(i).TagVariant = "ATRIBSNOCOSTESDESC" Then
                            Set ogroup = sdbgPreciosEscalados.Groups(i)
                            For j = 0 To ogroup.Columns.Count - 1
                                If Left(ogroup.Columns(j).Name, 2) = "AT" And ogroup.Columns(j).TagVariant = "1" Then
                                    If ogroup.Columns(j).Visible = True Then
                                        ogroup.Columns(j).Locked = True
                                        If Left(ogroup.Columns(j).Name, 3) = "AT_" Then
                                            ogroup.Columns(j).DropDownHwnd = 0
                                        End If
                                    End If
                                End If
                            Next j
                        Else
                            Set ogroup = sdbgPreciosEscalados.Groups(i)
                            For j = 1 To ogroup.Columns.Count - 1
                                If ogroup.Columns(j).Visible = True Then
                                    ogroup.Columns(j).Locked = True
                                 End If
                            Next j
                        End If
                    Next i
                    If sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.Cols - 1).Name = "C#_ADJ" Then
                        sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.Cols - 1).Locked = False
                    End If
                Else
                    For i = 10 To sdbgPrecios.Cols - 1
                        If sdbgPrecios.Columns(i).Visible = True Then
                            sdbgPrecios.Columns(i).Locked = True
                            If Left(sdbgPrecios.Columns(i).Name, 3) = "AT_" Then
                                sdbgPrecios.Columns(i).DropDownHwnd = 0
                            End If
                            If sdbgPrecios.Columns(i).Name = "USAR" Then
                                sdbgPrecios.Columns(i).DropDownHwnd = 0
                            End If
                        End If
                    Next
                    If sdbgPrecios.Columns(sdbgPrecios.Cols - 1).Name = "C#_ADJ" Then
                        sdbgPrecios.Columns(sdbgPrecios.Cols - 1).Locked = False
                    End If
                End If
            End If
            DesbloquearProceso
            Accion = ACCRecOfeCon
        End If
    Else 'De consulta a edicion
        'Bloqueo el proceso para que nadie lo use
        
        If Not BloquearProceso Then Exit Sub
        m_udtOrigBloqueo = ModifItems
        
        teserror = g_oOfertaSeleccionada.OfertaModificable
        If teserror.NumError <> TESnoerror Then
            If teserror.NumError = TESOfertaAdjudicada Then
                oMensajes.OfertaAdjudicada
                DesbloquearProceso
                Exit Sub
            Else
                DesbloquearProceso
                TratarError teserror
                Exit Sub
            End If
        End If
        Screen.MousePointer = vbHourglass
        
        If picApartado(2).Visible Then
            'Atributos
            picAtrib.Enabled = True
            sdbgAtributos.Columns(2).Locked = False
            If Me.Enabled And Me.Visible Then sdbgAtributos.SetFocus
            DoEvents
            sdbgAtributos.col = 2

        ElseIf picApartado(4).Visible Then
            'Items
            bModificar = PermisoModificarOferta
            If bModificar = True Then
                picComboGrupo.Enabled = False
                picChkItems.Enabled = False
                If m_bGrupoConEscalados Then
                    For i = 1 To sdbgPreciosEscalados.Groups.Count - 1
                        If sdbgPreciosEscalados.Groups(i).TagVariant = "ATRIBSNOCOSTESDESC" Then
                            Set ogroup = sdbgPreciosEscalados.Groups(i)
                            For j = 0 To ogroup.Columns.Count - 1
                                If Left(ogroup.Columns(j).Name, 3) = "AT_" Then
                                    If ogroup.Columns(j).Visible = True Then
                                        ogroup.Columns(j).Locked = False
                                     End If
                                End If
                            Next j
                        Else
                            Set ogroup = sdbgPreciosEscalados.Groups(i)
                            For j = 1 To ogroup.Columns.Count - 1
                                If ogroup.Columns(j).Visible = True Then
                                    ogroup.Columns(j).Locked = False
                                 End If
                            Next j
                        End If
                    Next i
                    
                    sdbgPreciosEscalados.AllowUpdate = True
                    If Me.Enabled And Me.Visible Then sdbgPreciosEscalados.SetFocus
                Else
                    For i = 10 To sdbgPrecios.Cols - 1
                        If sdbgPrecios.Columns(i).Visible = True Then
                            sdbgPrecios.Columns(i).Locked = False
                            If sdbgPrecios.Columns(i).Name = "USAR" And sdbgPrecios.Columns(i).Visible = True Then
                                sdbddUsar.RemoveAll
                                sdbddUsar.AddItem m_sIdiUsarPrec(1)
                                sdbddUsar.AddItem m_sIdiUsarPrec(2)
                                sdbddUsar.AddItem m_sIdiUsarPrec(3)
                                sdbgPrecios.Columns(i).DropDownHwnd = sdbddUsar.hWnd
                                sdbddUsar.Enabled = True
                            End If
        
                        End If
                    Next
            
                    sdbgPrecios.AllowUpdate = True
                    If Me.Enabled And Me.Visible Then sdbgPrecios.SetFocus
                End If
                DoEvents
            'sdbgPrecios.col = 9
            Else
                'EN ESTE CASO NO PUEDE MODIFICAR LA OFERTA, PERO SI EL CAMPO PRECIO A USAR
                picComboGrupo.Enabled = False
                picChkItems.Enabled = False
                
                If m_bGrupoConEscalados Then
            
                    For i = 1 To sdbgPreciosEscalados.Groups.Count - 1
                        If sdbgPreciosEscalados.Groups(i).TagVariant = "ATRIBSNOCOSTESDESC" Then
                            Set ogroup = sdbgPreciosEscalados.Groups(i)
                            For j = 0 To ogroup.Columns.Count - 1
                                If Left(ogroup.Columns(j).Name, 3) = "AT_" Then
                                    If ogroup.Columns(j).Visible = True Then
                                        ogroup.Columns(j).Locked = False
                                     End If
                                End If
                            Next j
                        Else
                            Set ogroup = sdbgPreciosEscalados.Groups(i)
                            For j = 1 To ogroup.Columns.Count - 1
                                If ogroup.Columns(j).Visible = True Then
                                    ogroup.Columns(j).Locked = False
                                 End If
                            Next j
                        End If
                    Next i
            
                    sdbgPreciosEscalados.AllowUpdate = True
                    If Me.Enabled And Me.Visible Then sdbgPreciosEscalados.SetFocus

                Else
                    For i = 10 To sdbgPrecios.Cols - 1
                        If sdbgPrecios.Columns(i).Visible = True Then
                            sdbgPrecios.Columns(i).Locked = True
                            If sdbgPrecios.Columns(i).Name = "USAR" And sdbgPrecios.Columns(i).Visible = True Then
                                sdbgPrecios.Columns(i).Locked = False
                                sdbddUsar.RemoveAll
                                sdbddUsar.AddItem m_sIdiUsarPrec(1)
                                sdbddUsar.AddItem m_sIdiUsarPrec(2)
                                sdbddUsar.AddItem m_sIdiUsarPrec(3)
                                sdbgPrecios.Columns(i).DropDownHwnd = sdbddUsar.hWnd
                                sdbddUsar.Enabled = True
                            End If
        
                        End If
                    Next
            
                    sdbgPrecios.AllowUpdate = True
                    If Me.Enabled And Me.Visible Then sdbgPrecios.SetFocus
                
                End If
                DoEvents
            End If

        End If
        Screen.MousePointer = vbNormal


        tvwProce.Enabled = False
        fraProce.Enabled = False
        cmdRestaurar(2).Visible = False
        cmdListado(2).Visible = False
        cmdDeshacer.Visible = True
        cmdModoEdicion.caption = sIdiConsulta

        bModoEdicion = True
    End If
End Sub

Private Sub LimpiarGrupo()
    txtCodGrupo.Text = ""
    txtDenGrupo.Text = ""
    txtDescrGrupo.Text = ""
    txtIniGrupo.Text = ""
    txtFinGrupo.Text = ""
    lblDestGrupo(1).caption = ""
    lblDestGrupo(2).caption = ""
    lblPagGrupo(1).caption = ""
    lblPagGrupo(2).caption = ""
    lblProveGrupo(1).caption = ""
    lblProveGrupo(2).caption = ""
End Sub

''' <summary>
''' Muestra en el pic los datos del grupo seleccionado en el arbol
''
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cmdRestaurar_click; tvwProce.Node_click Tiempo m�ximo:0</remarks>

Private Sub MostrarDatosGrupo()
LimpiarGrupo
txtIniGrupo.Visible = True
txtFinGrupo.Visible = True
lblIniGrupo.Visible = True
lblFinGrupo.Visible = True
lblDestGrupo(0).Visible = True
lblDestGrupo(1).Visible = True
lblDestGrupo(2).Visible = True
lblPagGrupo(0).Visible = True
lblPagGrupo(1).Visible = True
lblPagGrupo(2).Visible = True
lblProveGrupo(0).Visible = True
lblProveGrupo(1).Visible = True
lblProveGrupo(2).Visible = True
txtIniGrupo.Top = 3630
txtFinGrupo.Top = 3630
lblIniGrupo.Top = 3630
lblFinGrupo.Top = 3630
lblDestGrupo(0).Top = 4050
lblDestGrupo(1).Top = 4050
lblDestGrupo(2).Top = 4050
lblPagGrupo(0).Top = 4440
lblPagGrupo(1).Top = 4440
lblPagGrupo(2).Top = 4440
lblProveGrupo(0).Top = 4830
lblProveGrupo(1).Top = 4830
lblProveGrupo(2).Top = 4830

If Not g_oOfertaSeleccionada Is Nothing Then g_oOfertaSeleccionada.MarcarComoLeida basOptimizacion.gCodPersonaUsuario
    
With m_oGrupoSeleccionado
    txtCodGrupo.Text = .Codigo
    txtDenGrupo.Text = .Den
    txtDescrGrupo.Text = NullToStr(.Descripcion)
    If .DefFechasSum And .DefDestino And .DefFormaPago And .DefProveActual Then
        txtIniGrupo.Text = .FechaInicioSuministro
        txtFinGrupo.Text = .FechaFinSuministro
        lblDestGrupo(1).caption = .DestCod
        lblDestGrupo(2).caption = .DestDen
        lblPagGrupo(1).caption = .PagCod
        lblPagGrupo(2).caption = .PagDen
        lblProveGrupo(1).caption = NullToStr(.ProveActual)
        lblProveGrupo(2).caption = NullToStr(.ProveActDen)
        MostrarApartado iAptGrupo, IIf(g_bAnyadirOfertaObl, True, False)
        Exit Sub
    End If
    If Not .DefDestino And Not .DefFechasSum And Not .DefFormaPago And Not .DefProveActual Then
        txtIniGrupo.Visible = False
        txtFinGrupo.Visible = False
        lblIniGrupo.Visible = False
        lblFinGrupo.Visible = False
        lblDestGrupo(0).Visible = False
        lblDestGrupo(1).Visible = False
        lblDestGrupo(2).Visible = False
        lblPagGrupo(0).Visible = False
        lblPagGrupo(1).Visible = False
        lblPagGrupo(2).Visible = False
        lblProveGrupo(0).Visible = False
        lblProveGrupo(1).Visible = False
        lblProveGrupo(2).Visible = False
        MostrarApartado iAptGrupo, IIf(g_bAnyadirOfertaObl, True, False)
        Exit Sub
    End If
    If Not .DefFechasSum Then
        txtIniGrupo.Visible = False
        txtFinGrupo.Visible = False
        lblIniGrupo.Visible = False
        lblFinGrupo.Visible = False
        lblProveGrupo(0).Top = lblPagGrupo(1).Top
        lblProveGrupo(1).Top = lblPagGrupo(1).Top
        lblProveGrupo(2).Top = lblPagGrupo(1).Top
        lblPagGrupo(0).Top = lblDestGrupo(0).Top
        lblPagGrupo(1).Top = lblDestGrupo(0).Top
        lblPagGrupo(2).Top = lblDestGrupo(0).Top
        lblDestGrupo(0).Top = txtIniGrupo.Top
        lblDestGrupo(1).Top = txtIniGrupo.Top
        lblDestGrupo(2).Top = txtIniGrupo.Top
        If Not .DefDestino Then
            lblDestGrupo(0).Visible = False
            lblDestGrupo(1).Visible = False
            lblDestGrupo(2).Visible = False
            lblProveGrupo(0).Top = lblPagGrupo(1).Top
            lblProveGrupo(1).Top = lblPagGrupo(1).Top
            lblProveGrupo(2).Top = lblPagGrupo(1).Top
            lblPagGrupo(0).Top = lblDestGrupo(0).Top
            lblPagGrupo(1).Top = lblDestGrupo(0).Top
            lblPagGrupo(2).Top = lblDestGrupo(0).Top
            If Not .DefFormaPago Then
                lblPagGrupo(0).Visible = False
                lblPagGrupo(1).Visible = False
                lblPagGrupo(2).Visible = False
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(0).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).Top = lblPagGrupo(1).Top
                    lblProveGrupo(2).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            Else
                lblPagGrupo(1).caption = .PagCod
                lblPagGrupo(2).caption = .PagDen
                lblPagGrupo(0).Visible = True
                lblPagGrupo(1).Visible = True
                lblPagGrupo(2).Visible = True
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            End If
        Else
            lblDestGrupo(0).Visible = True
            lblDestGrupo(1).Visible = True
            lblDestGrupo(2).Visible = True
            lblDestGrupo(1).caption = .DestCod
            lblDestGrupo(2).caption = .DestDen
            If Not .DefFormaPago Then
                lblPagGrupo(0).Visible = False
                lblPagGrupo(1).Visible = False
                lblPagGrupo(2).Visible = False
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(0).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).Top = lblPagGrupo(1).Top
                    lblProveGrupo(2).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            Else
                lblPagGrupo(0).Visible = True
                lblPagGrupo(1).Visible = True
                lblPagGrupo(2).Visible = True
                lblPagGrupo(1).caption = .PagCod
                lblPagGrupo(2).caption = .PagDen
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            End If
            
        End If
    Else
        txtIniGrupo.Visible = True
        txtFinGrupo.Visible = True
        lblIniGrupo.Visible = True
        lblFinGrupo.Visible = True
        txtIniGrupo.Text = .FechaInicioSuministro
        txtFinGrupo.Text = .FechaFinSuministro
        If Not .DefDestino Then
            lblDestGrupo(0).Visible = False
            lblDestGrupo(1).Visible = False
            lblDestGrupo(2).Visible = False
            lblProveGrupo(0).Top = lblPagGrupo(1).Top
            lblProveGrupo(1).Top = lblPagGrupo(1).Top
            lblProveGrupo(2).Top = lblPagGrupo(1).Top
            lblPagGrupo(0).Top = lblDestGrupo(0).Top
            lblPagGrupo(1).Top = lblDestGrupo(0).Top
            lblPagGrupo(2).Top = lblDestGrupo(0).Top
            If Not .DefFormaPago Then
                lblPagGrupo(0).Visible = False
                lblPagGrupo(1).Visible = False
                lblPagGrupo(2).Visible = False
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(0).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).Top = lblPagGrupo(1).Top
                    lblProveGrupo(2).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            Else
                lblPagGrupo(0).Visible = True
                lblPagGrupo(1).Visible = True
                lblPagGrupo(2).Visible = True
                lblPagGrupo(1).caption = .PagCod
                lblPagGrupo(2).caption = .PagDen
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            End If
        Else
            lblDestGrupo(0).Visible = True
            lblDestGrupo(1).Visible = True
            lblDestGrupo(2).Visible = True
            lblDestGrupo(1).caption = .DestCod
            lblDestGrupo(2).caption = .DestDen
            If Not .DefFormaPago Then
                lblPagGrupo(0).Visible = False
                lblPagGrupo(1).Visible = False
                lblPagGrupo(2).Visible = False
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(0).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).Top = lblPagGrupo(1).Top
                    lblProveGrupo(2).Top = lblPagGrupo(1).Top
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            Else
                lblPagGrupo(0).Visible = True
                lblPagGrupo(1).Visible = True
                lblPagGrupo(2).Visible = True
                lblPagGrupo(1).caption = .PagCod
                lblPagGrupo(2).caption = .PagDen
                If Not .DefProveActual Then
                    lblProveGrupo(0).Visible = False
                    lblProveGrupo(1).Visible = False
                    lblProveGrupo(2).Visible = False
                Else
                    lblProveGrupo(0).Visible = True
                    lblProveGrupo(1).Visible = True
                    lblProveGrupo(2).Visible = True
                    lblProveGrupo(1).caption = NullToStr(.ProveActual)
                    lblProveGrupo(2).caption = NullToStr(.ProveActDen)
                End If
            End If
        
        End If
    End If
End With

MostrarApartado iAptGrupo, IIf(g_bAnyadirOfertaObl, True, False)

End Sub

Private Sub cmdNoOfe_Click(Index As Integer)
Dim sUsu As String
Dim bModif As Boolean

bModif = True
With m_oProveSeleccionado
    If .NoOfePortal Then
        If Not IsNull(.NoOfeNomUsu) Then
            sUsu = "(" & m_sIdiNoOfePORT & " " & .NoOfeNomUsu & ")"
        ElseIf Not IsNull(.NoOfeUsu) Then
            sUsu = "(" & m_sIdiNoOfePORT & " " & .NoOfeUsu & ")"
        End If
        If m_bModifOfeDeProv Then
            If m_bRestModifOfeProvUsu Then
                If Not m_oProveedoresAsigUsu Is Nothing Then
                    If m_oProveedoresAsigUsu.Item(CStr(.Cod)) Is Nothing Then
                        bModif = False
                    End If
                End If
            End If
        Else
            bModif = False
        End If
    Else
        If Not IsNull(.NoOfeNomUsu) Then
            sUsu = "(" & m_sIdiNoOfeGS & " " & .NoOfeNomUsu & ")"
        ElseIf Not IsNull(.NoOfeUsu) Then
            sUsu = "(" & m_sIdiNoOfeGS & " " & .NoOfeUsu & ")"
        End If
        If m_bModifOfeDeUsu Then
            If m_bRestModifOfeUsu = True Then 'Restringir la modificaci�n a las ofertas introducidas por el usuario
                If NullToStr(.NoOfeUsu) <> oUsuarioSummit.Cod Then
                    bModif = False
                End If
            End If
        Else
            bModif = False
        End If
    End If

    If Index = 0 Then 'Muestra el motivo
        If .HaOfertado Then
            frmMotivoNoOfe.g_bEdicion = False
            frmMotivoNoOfe.lblFec(0).caption = frmMotivoNoOfe.lblFec(0).caption & .NoOfeFecha
        Else
            frmMotivoNoOfe.g_bEdicion = bModif
            frmMotivoNoOfe.lblFec(0).caption = frmMotivoNoOfe.lblFec(0).caption & .NoOfeFecha
            frmMotivoNoOfe.txtFec.Text = .NoOfeFecha
        End If
        
    Else 'Indicar que no oferta
        frmMotivoNoOfe.g_bEdicion = True
        frmMotivoNoOfe.txtFec.Text = Date
    End If
    frmMotivoNoOfe.lblFec(2).caption = sUsu
    frmMotivoNoOfe.caption = frmMotivoNoOfe.caption & " " & m_oProcesoSeleccionado.Anyo & "/" & m_oProcesoSeleccionado.GMN1Cod & "/" & m_oProcesoSeleccionado.Cod & " " & m_oProcesoSeleccionado.Den
    frmMotivoNoOfe.txtDescr.Text = NullToStr(.NoOfeMotivo)
    
End With
        
frmMotivoNoOfe.Show 1

If m_oProveSeleccionado.NoOfe And Not m_oProveSeleccionado.HaOfertado And tvwProce.Nodes("P_" & m_oProveSeleccionado.Cod).Image <> "NOOFE" Then
    cmdNoOfe(0).Width = 1486.631
    cmdNoOfe(0).Left = 3665.259
    cmdNoOfe(0).caption = sIdiNoOfe
    imgNoOfe.Visible = True
    lblComent.Visible = False
    tvwProce.Nodes("P_" & m_oProveSeleccionado.Cod).Image = "NOOFE"
    cmdNoOfe(0).Visible = True
    cmdNoOfe(1).Visible = False
End If
End Sub


Private Sub cmdResponsable_Click()
    'Si no tiene permisos para modificar el responsable del proceso muestra directamente el detalle del responsable,
    'si no muestra el men� del responsable de proceso
    If m_bModifResponsable = False Or m_oProcesoSeleccionado.Estado >= conadjudicaciones Or m_oProcesoSeleccionado.Invitado Then
        VerDetalleResponsable
    Else
        Set MDI.g_ofrmOrigenResponsable = Me
        PopupMenu MDI.mnuPOPUPResponsable
    End If
End Sub

Private Sub cmdRestaurar_Click(Index As Integer)

Select Case Index
Case 0
    If picApartado(0).Visible Then
        ProcesoSeleccionado
        
    ElseIf picApartado(3).Visible Then
        'Adjuntos
        If g_oOfertaSeleccionada Is Nothing Then Exit Sub
        If g_oGrupoOferSeleccionado Is Nothing Then
            g_oOfertaSeleccionada.CargarAdjuntos
            MostrarAdjuntosOfe
        Else
            g_oGrupoOferSeleccionado.CargarAdjuntos
            MostrarAdjuntosGrupoOfe
        End If
    ElseIf picApartado(6).Visible Then
        'Grupo
        If m_oGrupoSeleccionado Is Nothing Then Exit Sub
        m_oProcesoSeleccionado.CargarTodosLosGrupos bSinpres:=True
        Set m_oGrupoSeleccionado = m_oProcesoSeleccionado.Grupos.Item(m_oGrupoSeleccionado.Codigo)
        MostrarDatosGrupo
    End If
Case 1
    If g_oOfertaSeleccionada Is Nothing Then Exit Sub
    g_oOfertaSeleccionada.CargarDatosGeneralesOferta gParametrosGenerales.gIdioma
    MostrarOferta

Case 2
    If picApartado(2).Visible Then
        'Atributos
        If g_oOfertaSeleccionada Is Nothing Then Exit Sub
        If g_oGrupoOferSeleccionado Is Nothing Then
            g_oOfertaSeleccionada.CargarAtributosProcesoOfertados
            m_oProcesoSeleccionado.CargarAtributos
            MostrarAtributosOfe
        Else
            g_oGrupoOferSeleccionado.CargarAtributosOfertados
            m_oGrupoSeleccionado.CargarAtributos ambito:=AmbGrupo
            MostrarAtributosGrupoOfe
        End If
    ElseIf picApartado(4).Visible Then
        'Items
        m_bMouse = False
        If g_oGrupoOferSeleccionado Is Nothing Then
            m_oProcesoSeleccionado.CargarAtributos ambito:=AmbItem, vGrupo:="#"
            Set g_oOfertaSeleccionada.proceso = m_oProcesoSeleccionado
            g_oOfertaSeleccionada.CargarPrecios
            MostrarItemsOfe
        Else
            m_oGrupoSeleccionado.CargarAtributos ambito:=AmbItem
            g_oGrupoOferSeleccionado.CargarPrecios
            MostrarItemsGrupoOfe
        End If
        m_bMouse = True
    End If
Case 3
    m_oProveedoresProceso.CargarDatosProveedor (m_oProveSeleccionado.Cod)
    m_oProveSeleccionado.CargarTodosLosContactos , , , , True
    MostrarProveedor
Case 4
    m_oProcesoSeleccionado.CargarSobres False
    MostrarSobre

End Select
End Sub

Private Sub cmdSalvarAdjun_Click()
    Dim oAdjun As CAdjunto
    Dim sFileName As String
    Dim sFileTitle As String
    Dim teserror As TipoErrorSummit
            
    On Error GoTo Cancelar:

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub

    If sdbgAdjun.Row < 0 Then
        oMensajes.SeleccioneFichero
    Else
        sFileTitle = ""
        cmmdAdjun.DialogTitle = sIdiGuardar
        cmmdAdjun.CancelError = True
        cmmdAdjun.Filter = sIdiTipoOrig & "|*.*"
        cmmdAdjun.filename = sdbgAdjun.Columns("FICHERO").Value
        cmmdAdjun.ShowSave

        sFileName = cmmdAdjun.filename
        sFileTitle = cmmdAdjun.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido sIdiElArchivo
            Exit Sub
        End If

        ' Cargamos el contenido en la esp.
        If g_oGrupoOferSeleccionado Is Nothing Then
            Set oAdjun = g_oOfertaSeleccionada.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
        Else
            Set oAdjun = g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
            Set oAdjun.Grupo = m_oGrupoSeleccionado
        End If
        If oAdjun.DataSize = 0 Then
            oMensajes.NoValido sIdiElArchivo & " " & oAdjun.nombre
            Set oAdjun = Nothing
            Exit Sub
        End If
        Set oAdjun.Oferta = g_oOfertaSeleccionada
        teserror = oAdjun.ComenzarLecturaData
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oAdjun = Nothing
            Exit Sub
        End If
        oAdjun.LeerAdjuntoOferta oAdjun.Id, oAdjun.DataSize, sFileName
    End If

Cancelar:
    On Error Resume Next

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oAdjun = Nothing
    End If
End Sub


Private Sub Form_Activate()
    Dim dtFechaIniSub As Date
    Dim dtHoraIniSub As Date
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    Dim dtFechaRec As Date
    Dim dtHoraRec As Date
    
    If g_bVisor Then
        g_bVisor = False
        LockWindowUpdate 0&
    End If
    
    'Es posible que la zona horaria haya cambiado en otra pantalla, actualizarla
    If oUsuarioSummit.TimeZone <> m_vTZHora Then
        m_vTZHora = oUsuarioSummit.TimeZone
        lblTZ.caption = m_dcListaZonasHorarias.Item(m_vTZHora)
        
        If Not m_oProcesoSeleccionado Is Nothing Then
            m_oProcesoSeleccionado.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True, sUsu:=oUsuarioSummit.Cod
            
            If Not IsNull(m_oProcesoSeleccionado.FechaMinimoLimOfertas) And m_oProcesoSeleccionado.FechaMinimoLimOfertas <> "" Then
                ConvertirUTCaTZ DateValue(m_oProcesoSeleccionado.FechaMinimoLimOfertas), TimeValue(m_oProcesoSeleccionado.FechaMinimoLimOfertas), m_vTZHora, dtFechaFinSub, dtHoraFinSub
                lblFecLimit(1).caption = dtFechaFinSub
                lblHoraLimite(1).caption = dtHoraFinSub
            End If
            
            If m_oProcesoSeleccionado.ModoSubasta And gParametrosGenerales.gbSubasta Then
                If Not IsNull(m_oProcesoSeleccionado.FechaInicioSubasta) And m_oProcesoSeleccionado.FechaInicioSubasta <> "" Then
                    ConvertirUTCaTZ DateValue(m_oProcesoSeleccionado.FechaInicioSubasta), TimeValue(m_oProcesoSeleccionado.FechaInicioSubasta), m_vTZHora, dtFechaIniSub, dtHoraIniSub
                    lblFecIniSub(1).caption = dtFechaIniSub
                    lblHoraIniSub(1).caption = dtHoraIniSub
                End If
            End If
            
            If Not g_oOfertaSeleccionada Is Nothing Then
                ConvertirUTCaTZ DateValue(g_oOfertaSeleccionada.FechaRec), TimeValue(g_oOfertaSeleccionada.HoraRec), m_vTZHora, dtFechaRec, dtHoraRec
                
                txtFecOfe.Text = Format(dtFechaRec, "short date")
                If g_oOfertaSeleccionada.HoraRec = "" Then
                    txtHoraOfe.Text = "00:00:00"
                Else
                    txtHoraOfe.Text = dtHoraRec
                End If
            End If
        End If
    End If
End Sub

Private Sub Form_Load()
    
    Me.Height = 7905
    Me.Width = 11445

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    m_bGrupoConEscalados = False
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    m_sTemp = FSGSLibrary.DevolverPathFichTemp
    m_sLayOut = m_sTemp & "LayPrecios"
    If m_bGrupoConEscalados Then
        sdbgPreciosEscalados.GroupHeaders = True
        sdbgPreciosEscalados.SaveLayout m_sLayOut, ssSaveLayoutAll
    Else
        sdbgPrecios.SaveLayout m_sLayOut, ssSaveLayoutAll
    End If
    
    SetFormTZ
    SetTZValue
    
    ConfigurarSeguridad
    LimpiarCampos 10
    ConfigurarPicProceInstalacion
    MostrarApartado 0
        
    bMostrarTodosItems = True
    
    CargarAnyos
    
    Accion = ACCRecOfeCon
    bModoEdicion = False
    
    Set oProcesos = oFSGSRaiz.generar_CProcesos
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    Set oMonedasGen = oFSGSRaiz.Generar_CMonedas
    Set oEstados = oFSGSRaiz.Generar_COfeEstados
    
    'Inicializar el array de ficheros
    ReDim sayFileNames(0)
    
    oMonedasGen.CargarTodasLasMonedas , , , , , , True, False
   
    txtObs.Locked = True
    
    If g_bVisor Then
        LockWindowUpdate Me.hWnd
    End If
    
    g_bSoloInvitado = False
    If oUsuarioSummit.EsInvitado Then
        If oUsuarioSummit.Acciones Is Nothing Then
            g_bSoloInvitado = True
        Else
            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEConsultar)) Is Nothing Then
                g_bSoloInvitado = True
            End If
        End If
    End If
       'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador And m_bRMat Then
        CargarGMN1Automaticamente
    End If
    
    cmdColaboracion.Visible = (gParametrosGenerales.gsAccesoFSCN = TipoAccesoFSCN.AccesoFSCN)
End Sub

''' <summary>Asigna la zona horaria por defecto que se usar� para las horas de subasta</summary>

Private Sub SetFormTZ()
    If Not IsNull(oUsuarioSummit.TimeZone) And Not IsEmpty(oUsuarioSummit.TimeZone) Then
        m_vTZHora = oUsuarioSummit.TimeZone
    Else
        'Asignar la zona horaria del equipo
        m_vTZHora = GetTimeZone.key
    End If
End Sub

''' <summary>Establece el valor de los label de zona horaria</summary>

Private Sub SetTZValue()
    Set m_dcListaZonasHorarias = ObtenerZonasHorarias
    lblTZ.caption = m_dcListaZonasHorarias.Item(m_vTZHora)
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

''' <summary>
''' Descarga el formulario
''
''' </summary>
''' <param name="cancel">Sirve para parar la descarga del formulario</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Al descargar el fromulario. Tiempo m�ximo:0</remarks>

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer
    Dim FOSFile As Scripting.FileSystemObject
    Dim bBorrando As Boolean
        
    DesbloquearProceso
    
    Set oProcesos = Nothing
    Set oMonedas = Nothing
    Set oMonedasGen = Nothing
    Set oEstados = Nothing
        
    Set m_oGMN1Seleccionado = Nothing
    Set m_oProcesoSeleccionado = Nothing
    Set m_oGruposMN1 = Nothing
    Set m_oIOfertas = Nothing
    Set m_oOfertasProceso = Nothing
    Set m_oProveSeleccionado = Nothing
    Set g_oOfertaSeleccionada = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Set g_oGrupoOferSeleccionado = Nothing
    
    Set m_oProveedoresProceso = Nothing
    Set m_oProveedoresAsigUsu = Nothing
    Set m_oProveedoresAsigEqp = Nothing
    
    Accion = ACCRecOfeCon
    
    g_bAnyadirOfertaObl = False
    
On Error GoTo ERROR
    Set FOSFile = New Scripting.FileSystemObject
    
    FOSFile.DeleteFile m_sLayOut, True 'Borro el layout de la grid
    
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    Me.Visible = False
    Exit Sub

ERROR:
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
End Sub

Private Sub fraProce_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If picApartado(4).Visible = True And m_bMouse = True Then Screen.MousePointer = vbNormal
End Sub

Private Sub lblCPresGrupo_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bMouse Then Screen.MousePointer = vbNormal
End Sub

Private Sub lblPresGrupo_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bMouse Then Screen.MousePointer = vbNormal
End Sub

Private Sub picApartado_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Index = 4 And m_bMouse Then Screen.MousePointer = vbNormal
End Sub

Private Sub picMenu_Click()
 Dim node As MSComctlLib.node
 Dim bPrecios As Boolean
 Dim oLinea As CPrecioItem
 Dim oGrupoOf As COfertaGrupo
 Dim oAtribOf As CAtributoOfertado
 Dim bCargar As Boolean
 Dim sCod As String
 
 Dim oEscalado As CEscalado
 
If bModoEdicion Then
    cmdModoEdicion_Click
    If bModError Then Exit Sub 'Ha ocurrido alg�n error
End If

Screen.MousePointer = vbNormal
sCod = g_oOfertaSeleccionada.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(g_oOfertaSeleccionada.Prove))
        
'Compruebo que hay precios para el icono color o gris
For Each oGrupoOf In g_oOfertaSeleccionada.Grupos
    bCargar = True
    If gParametrosGenerales.gbProveGrupos Then
        If m_oAsigs.Item(sCod).Grupos.Item(oGrupoOf.Grupo.Codigo) Is Nothing Then bCargar = False
    End If
    If bCargar Then
        bPrecios = False
        If Not oGrupoOf.Lineas Is Nothing Then
            For Each oLinea In oGrupoOf.Lineas
                If oLinea.Precio <> "" Then
                    bPrecios = True
                    Exit For
                End If
                If oLinea.Precio2 <> "" Then
                    bPrecios = True
                    Exit For
                End If
                If oLinea.Precio3 <> "" Then
                    bPrecios = True
                    Exit For
                End If
                If oLinea.CantidadMaxima <> "" Then
                    bPrecios = True
                    Exit For
                End If
                If Not oLinea.AtribOfertados Is Nothing Then
                    For Each oAtribOf In oLinea.AtribOfertados
                        If Not IsNull(oAtribOf.valorText) Or Not IsNull(oAtribOf.valorNum) Or Not IsNull(oAtribOf.valorFec) Or Not IsNull(oAtribOf.valorBool) Then
                            bPrecios = True
                            Exit For
                        End If
                    Next
                End If
                If bPrecios Then Exit For
                If Not IsNull(oLinea.ObsAdjun) And Not IsEmpty(oLinea.ObsAdjun) Then
                    If oLinea.ObsAdjun <> "" Then
                        bPrecios = True
                        Exit For
                    End If
                End If
                If Not oLinea.Adjuntos Is Nothing Then
                    If oLinea.Adjuntos.Count > 0 Then
                        bPrecios = True
                        Exit For
                    End If
                End If
                '21521 Control de si tiene precios por escalados (para el caso de que la cantidad sea 0)
                If Not oLinea.Escalados Is Nothing Then
                    If oLinea.Escalados.Count > 0 Then
                        For Each oEscalado In oLinea.Escalados
                            If oEscalado.Precio <> "" Then
                                bPrecios = True
                                Exit For
                            End If
                        Next
                        If bPrecios Then
                            Exit For
                        End If
                    End If
                End If
            Next
            If bPrecios Then
                oGrupoOf.NumPrecios = 1
                tvwProce.Nodes.Item("ITEMSGRUPO_" & g_oOfertaSeleccionada.Prove & "_" & g_oOfertaSeleccionada.Num & "_" & oGrupoOf.Grupo.Codigo).Image = "Item"
            Else
                oGrupoOf.NumPrecios = 0
                tvwProce.Nodes.Item("ITEMSGRUPO_" & g_oOfertaSeleccionada.Prove & "_" & g_oOfertaSeleccionada.Num & "_" & oGrupoOf.Grupo.Codigo).Image = "ItemGris"
            End If
        End If
    End If
Next
'Por si se ha cambiado de grupo con el combo
If Not m_oGrupoSeleccionado Is Nothing And Not g_oOfertaSeleccionada Is Nothing Then
    Set node = tvwProce.Nodes.Item("GRUPO_" & g_oOfertaSeleccionada.Prove & "_" & g_oOfertaSeleccionada.Num & "_" & m_oGrupoSeleccionado.Codigo)
End If
If node Is Nothing Then
    Set node = tvwProce.selectedItem
    Set node = node.Parent
End If
Set tvwProce.selectedItem = Nothing
Set tvwProce.selectedItem = node
Me.caption = m_sMeCaption

tvwProce_NodeClick node
DoEvents

End Sub

Private Sub picMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bMouse Then
        Screen.MousePointer = vbCustom
        Screen.MouseIcon = ImageList1.ListImages("HAND").Picture
    End If
End Sub

Private Sub picNavigate_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Index = 2 And m_bMouse Then Screen.MousePointer = vbNormal
End Sub

Public Sub ProceSelector1_Click(ByVal Opcion As SelectorDeProcesos.PSSeleccion)
    sdbcProceCod = ""
    sdbcProceCod.RemoveAll
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
End Sub

Private Sub sdbcAnyo_Click()
    'Lo hace todo el change de proce cod
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Text = ""
    sdbcProceCod.Columns(1).Text = ""
    sdbcProceCod.RemoveAll
End Sub

Private Sub sdbcEstCod_PositionList(ByVal Text As String)
PositionList sdbcEstCod, Text
End Sub

Private Sub sdbcEstDen_PositionList(ByVal Text As String)
PositionList sdbcEstDen, Text
End Sub

Private Sub sdbcGMN1_4Cod_Click()
    If Not sdbcGMN1_4Cod.DroppedDown Then sdbcGMN1_4Cod = ""
End Sub

Private Sub sdbcGrupo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bMouse Then Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMonCod_Click()
    If Not sdbcMonCod.DroppedDown Then
        sdbcMonCod = ""
        sdbcMonDen = ""
    End If
End Sub

Private Sub sdbcMonCod_PositionList(ByVal Text As String)
    PositionList sdbcMonCod, Text
End Sub

Private Sub sdbcMonDen_Click()
    If Not sdbcMonDen.DroppedDown Then
        sdbcMonCod = ""
        sdbcMonDen = ""
    End If
End Sub

Private Sub sdbcMonDen_PositionList(ByVal Text As String)
    PositionList sdbcMonDen, Text
End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then sdbcProceCod.Text = ""
End Sub

Private Sub sdbcProceCod_Change()
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
    
        sdbcProceDen.Text = ""
        Set m_oProcesoSeleccionado = Nothing
        Set m_oIOfertas = Nothing
        Set m_oOfertasProceso = Nothing
        Set m_oProveSeleccionado = Nothing
        Set g_oOfertaSeleccionada = Nothing
        Set m_oGrupoSeleccionado = Nothing
        Set g_oGrupoOferSeleccionado = Nothing

        bCargarComboDesde = True
        
        LimpiarCampos (10)
        MostrarApartado 0
        
        bRespetarCombo = False
    End If
End Sub

Private Sub sdbcProceCod_CloseUp()
    If sdbcProceCod.Value = "..." Or Trim(sdbcProceCod.Value) = "" Then
        sdbcProceCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
End Sub

Private Sub sdbcProceCod_DropDown()
    Dim udtEstIni As TipoEstadoProceso
    Dim udtEstFin As TipoEstadoProceso
    Dim oProceso As cProceso
    Dim lIdPerfil As Long
    
    sdbcProceCod.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set m_oProcesoSeleccionado = Nothing
    Set m_oIOfertas = Nothing
    Set m_oOfertasProceso = Nothing
    Set m_oProveSeleccionado = Nothing
    Set g_oOfertaSeleccionada = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Set g_oGrupoOferSeleccionado = Nothing
    
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstIni = conasignacionvalida
            udtEstFin = conofertas
        Case PSSeleccion.PSAbiertos
            udtEstIni = conasignacionvalida
            udtEstFin = PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstIni = ParcialmenteCerrado
            udtEstFin = ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstIni = conadjudicaciones
            udtEstFin = Cerrado
        Case PSSeleccion.PSTodos
            udtEstIni = conasignacionvalida
            udtEstFin = Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstIni, udtEstFin, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstIni, udtEstFin, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    End If
    
    For Each oProceso In oProcesos
        sdbcProceCod.AddItem oProceso.Cod & Chr(m_lSeparador) & oProceso.Den & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
           
    If Not oProcesos.EOF Then sdbcProceCod.AddItem "..."
    
    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceCod_InitColumnProps()
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProceCod_PositionList(ByVal Text As String)
    PositionList sdbcProceCod, Text
End Sub
Public Sub sdbcProceCod_Validate(Cancel As Boolean)
    Dim udtEstIni As TipoEstadoProceso
    Dim udtEstFin As TipoEstadoProceso
    Dim lIdPerfil As Long
    
    If Trim(sdbcProceCod.Text = "") Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        Set m_oProcesoSeleccionado = Nothing
        Set m_oIOfertas = Nothing
        Set m_oOfertasProceso = Nothing
        Set m_oProveSeleccionado = Nothing
        Set g_oOfertaSeleccionada = Nothing
        Set m_oGrupoSeleccionado = Nothing
        Set g_oGrupoOferSeleccionado = Nothing
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido m_sIdiProceso
        Set m_oProcesoSeleccionado = Nothing
        Set m_oIOfertas = Nothing
        Set m_oOfertasProceso = Nothing
        Set m_oProveSeleccionado = Nothing
        Set g_oOfertaSeleccionada = Nothing
        Set m_oGrupoSeleccionado = Nothing
        Set g_oGrupoOferSeleccionado = Nothing
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiCodigo
        Set m_oProcesoSeleccionado = Nothing
        Set m_oIOfertas = Nothing
        Set m_oOfertasProceso = Nothing
        Set m_oProveSeleccionado = Nothing
        Set g_oOfertaSeleccionada = Nothing
        Set m_oGrupoSeleccionado = Nothing
        Set g_oGrupoOferSeleccionado = Nothing
        Exit Sub
    End If
    
    If Not bOrigenBuzon And Not bOrigenComparativa Then
        If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
            bRespetarCombo = True
            sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
            bRespetarCombo = False
            If m_oProcesoSeleccionado Is Nothing Then ProcesoSeleccionado
            Exit Sub
        End If
        
        If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
            bRespetarCombo = True
            sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
            bRespetarCombo = False
            If m_oProcesoSeleccionado Is Nothing Then ProcesoSeleccionado
            Exit Sub
        End If
    End If
        
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstIni = conasignacionvalida
            udtEstFin = conofertas
        Case PSSeleccion.PSAbiertos
            udtEstIni = conasignacionvalida
            udtEstFin = PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstIni = ParcialmenteCerrado
            udtEstFin = ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstIni = conadjudicaciones
            udtEstFin = Cerrado
        Case PSSeleccion.PSTodos
            udtEstIni = conasignacionvalida
            udtEstFin = Cerrado
    End Select
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstIni, udtEstFin, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , val(sdbcProceCod.Value), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, , , m_bRPerfUON, lIdPerfil
             
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Text = ""
        Screen.MousePointer = vbNormal
        If bOrigenBuzon Then
            oMensajes.PermisoDenegadoProceso
            frmOFEBuzon.g_bConsultProce = False
            Set m_oProcesoSeleccionado = Nothing
            Set m_oIOfertas = Nothing
            Set m_oOfertasProceso = Nothing
            Set m_oProveSeleccionado = Nothing
            Set g_oOfertaSeleccionada = Nothing
            Set m_oGrupoSeleccionado = Nothing
            Set g_oGrupoOferSeleccionado = Nothing
            Exit Sub
        Else
            oMensajes.NoValido m_sIdiProceso
            Set m_oProcesoSeleccionado = Nothing
            Set m_oIOfertas = Nothing
            Set m_oOfertasProceso = Nothing
            Set m_oProveSeleccionado = Nothing
            Set g_oOfertaSeleccionada = Nothing
            Set m_oGrupoSeleccionado = Nothing
            Set g_oGrupoOferSeleccionado = Nothing
        End If
    Else
        bRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        
        sdbcProceCod.Columns("COD").Text = sdbcProceCod.Text
        sdbcProceCod.Columns("DEN").Text = sdbcProceDen.Text
        sdbcProceCod.Columns("INVI").Text = BooleanToSQLBinary(oProcesos.Item(1).Invitado)
        
        bRespetarCombo = False
        Set m_oProcesoSeleccionado = Nothing
        Set m_oIOfertas = Nothing
        Set m_oOfertasProceso = Nothing
        Set m_oProveSeleccionado = Nothing
        Set g_oOfertaSeleccionada = Nothing
        Set m_oGrupoSeleccionado = Nothing
        Set g_oGrupoOferSeleccionado = Nothing
        Set m_oProcesoSeleccionado = oProcesos.Item(1)
       
        bCargarComboDesde = False
        ProcesoSeleccionado
    End If
    
End Sub

Private Sub CargarAnyos()
    Dim iAnyoActual As Integer
    Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem iInd
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
End Sub

''' <summary>Carga el combo de grupos</summary>
''' <param name="sProve">Cod. proveedor</param>
''' <remarks>Llamada desde: ConfigurarOpciones; Tiempo m�ximo: 0</remarks>
''' <revision>LTG 13/02/2012</revision>

Private Sub CargarGrupos(ByVal sProve As String)
    Dim oGrupo As CGrupo
    Dim iInd As Integer
    Dim bCargar As Boolean
    Dim sCod As String
    Dim bHayEsc As Boolean
    Dim bHayNoEsc As Boolean
    
    sdbcGrupo.RemoveAll
    iInd = 0
    With m_oProcesoSeleccionado
        For Each oGrupo In .Grupos
            If gParametrosGenerales.gbProveGrupos Then
                sCod = sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
                If m_oAsigs.Item(sCod).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                    bCargar = False
                Else
                    bCargar = True
                End If
            Else
                bCargar = True
            End If
            If bCargar Then
                If oGrupo.UsarEscalados Then
                    bHayEsc = True
                Else
                    bHayNoEsc = True
                End If
                
                iInd = iInd + 1
                sdbcGrupo.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
            End If
        Next
        
        If iInd > 1 Then
            'Si hay grupos con ys sin escalados � escalados distintos no mostrar la opci�n de Todos
            If Not ((bHayEsc And bHayNoEsc) Or (bHayEsc And Not m_oProcesoSeleccionado.Escalados)) Then
                sdbcGrupo.AddItem "**********" & Chr(m_lSeparador) & m_sIdiTodosItems
            End If
        End If
        
        sdbcGrupo.ListAutoPosition = True
        sdbcGrupo.Scroll 1, 7
    End With
End Sub

Private Sub ProcesoSeleccionado()
Dim sCod As String
Dim oSobre As CSobre
Dim oIAsigs  As IAsignaciones
Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    
    sCod = sdbcGMN1_4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod))
    sCod = CStr(sdbcAnyo) & sCod & sdbcProceCod
       
    Set m_oProcesoSeleccionado = oFSGSRaiz.Generar_CProceso
    
    m_oProcesoSeleccionado.Anyo = sdbcAnyo.Value
    m_oProcesoSeleccionado.GMN1Cod = sdbcGMN1_4Cod.Value
    m_oProcesoSeleccionado.Cod = sdbcProceCod.Value
    If sdbcProceCod.Columns("INVI").Value = "" Then
        m_oProcesoSeleccionado.Invitado = GridCheckToBoolean(sdbcProceDen.Columns("INVI").Value)
    Else
        m_oProcesoSeleccionado.Invitado = GridCheckToBoolean(sdbcProceCod.Columns("INVI").Value)
    End If
    
    'Datos del proceso
    m_oProcesoSeleccionado.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True, sUsu:=oUsuarioSummit.Cod
    
    If m_oProcesoSeleccionado.Den = "" Then
        LimpiarCampos (10)
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If m_oProcesoSeleccionado.AdminPublica Then
        m_oProcesoSeleccionado.CargarSobres False
    
        For Each oSobre In m_oProcesoSeleccionado.Sobres
            If IsNull(oSobre.FechaAperturaPrevista) Then
                oMensajes.FechaDeSobreCorrupta
                sdbcProceCod.Value = ""
                Set m_oProcesoSeleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Next
    
    End If
    If m_oProcesoSeleccionado.Invitado Then
        m_bModifResponsable = False
        m_bModifEquivalencia = False
        m_bAlta = False
        m_bAltaProvAsig = False
        m_bAltaProvCompAsig = False
        m_bModifOfeDeProv = False
        m_bModifOfeDeUsu = False
        m_bRestModifOfeProvUsu = False
        m_bRestModifOfeUsu = False
        m_bElimOfeDeProv = False
        m_bElimOfeDeUsu = False
        m_bRestElimOfeProvUsu = False
        m_bRestElimOfeUsu = False
        ConfigurarBotones
    Else
        ConfigurarSeguridad
    End If
    
    m_oProcesoSeleccionado.CargarTodosLosGrupos bSinpres:=True
    
    Set oIAsigs = m_oProcesoSeleccionado
    If oIAsigs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    'Cargamos los proveedores asignados al proceso
    If m_bOfeAsigComp Then
        Set m_oProveedoresProceso = oIAsigs.DevolverProveedoresDesde(, , , OrdAsigPorCodProve, gCodEqpUsuario, gCodCompradorUsuario)
    Else
        If m_bOfeAsigEqp Then
            Set m_oProveedoresProceso = oIAsigs.DevolverProveedoresDesde(, , , OrdAsigPorCodProve, gCodEqpUsuario)
        Else
            Set m_oProveedoresProceso = oIAsigs.DevolverProveedoresDesde(, , , OrdAsigPorCodProve)
        End If
    End If
    If m_oProveedoresProceso Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    'Si hay asignaciones a grupos cargamos las asigbaciones con los grupos
    If gParametrosGenerales.gbProveGrupos Then Set m_oAsigs = oIAsigs.DevolverAsignaciones(OrdAsigPorCodProve, , , , , True)
        
    If (m_bAltaProvAsig = True Or m_bRestModifOfeProvUsu = True Or m_bRestElimOfeProvUsu = True) And Not m_bOfeAsigComp Then
        Set m_oProveedoresAsigUsu = oIAsigs.DevolverProveedoresDesde(, , , OrdAsigPorCodProve, gCodEqpUsuario, gCodCompradorUsuario)
    Else
        Set m_oProveedoresAsigUsu = Nothing
    End If
    
    If m_bAltaProvCompAsig = True And Not m_bOfeAsigEqp Then
        Set m_oProveedoresAsigEqp = oIAsigs.DevolverProveedoresDesde(, , , OrdAsigPorCodProve, gCodEqpUsuario)
    Else
        Set m_oProveedoresAsigEqp = Nothing
    End If
        
    'Cargamos si hay atributos o no
    m_oProcesoSeleccionado.CargarAtributos
    
    'Cargamos las ofertas del proceso
    Set m_oIOfertas = m_oProcesoSeleccionado
    Set m_oOfertasProceso = m_oIOfertas.CargarOfertasDelProceso
    
    dProceEquiv = m_oProcesoSeleccionado.Cambio
    lblPresGrupo(1).caption = m_oProcesoSeleccionado.DevolverPresupuestoAbierto
    Set oIAsigs = Nothing
    
    CargarTreeViewProce
    
    MostrarDatosGenerales
        
    'N� de decimales a mostrar:si no tiene asociado el n� de decimales al proceso almacena el de par�metros de la instalaci�n
    If IsNull(m_oProcesoSeleccionado.NumDecimalesOfe) Then
        teserror = m_oProcesoSeleccionado.GuardarNumDecimalesOfe(oUsuarioSummit.Cod, basPublic.gParametrosInstalacion.giNumDecimalesOfe, True)
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Exit Sub
        End If
    End If
    txtNumDec.Text = m_oProcesoSeleccionado.NumDecimalesOfe
    UpDownDec.Tag = "C"
    UpDownDec.Value = m_oProcesoSeleccionado.NumDecimalesOfe
    UpDownDec.Tag = ""
        
    If m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
        cmdA�adir.Enabled = False
        cmdCargarXLS.Enabled = False
        cmdModificar.Enabled = False
        cmdModoEdicion.Enabled = False
        cmdEliminar.Enabled = False
        txtObsAdjun.Locked = True
    Else
        cmdA�adir.Enabled = True
        cmdCargarXLS.Enabled = True
        cmdModificar.Enabled = True
        cmdModoEdicion.Enabled = True
        cmdEliminar.Enabled = True
    End If
    
    cmdRestaurar(0).Enabled = True
    cmdListado(0).Enabled = True
    cmdColaboracion.Enabled = True
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_Change()
    If Not bRespetarCombo Then
        bCargarComboDesde = True
        Set m_oGMN1Seleccionado = Nothing
        sdbcProceCod.Text = ""
        sdbcProceCod.Columns(0).Text = ""
        sdbcProceCod.Columns(1).Text = ""
        sdbcProceCod.RemoveAll
    End If
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub

    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    bRespetarCombo = False
    sdbcProceCod.Text = ""
    sdbcProceCod.Columns(0).Text = ""
    sdbcProceCod.Columns(1).Text = ""
    sdbcProceCod.RemoveAll
    
    GMN1Seleccionado
    bCargarComboDesde = False
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    Set m_oGruposMN1 = Nothing
    Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If bCargarComboDesde Then
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
      
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
         
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        Else
            Set m_oGruposMN1 = Nothing
            Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
       
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oGruposMN1.EOF Then sdbcGMN1_4Cod.AddItem "..."
    
    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
    PositionList sdbcGMN1_4Cod, Text
End Sub
Public Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el grupo
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador

        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario, True, sdbcAnyo.Value)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If m_oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiMaterial
        Else
            Set m_oGMN1Seleccionado = Nothing
            Set m_oGMN1Seleccionado = m_oGruposMN1.Item(scod1)
            bCargarComboDesde = False
        End If
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
 
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
            
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiMaterial
        Else
            bRespetarCombo = True
            sdbcGMN1_4Cod.Value = oGMN1.Cod
            bRespetarCombo = False
            Set m_oGMN1Seleccionado = Nothing
            Set m_oGMN1Seleccionado = oGMN1
            bCargarComboDesde = False
        End If
    End If
    Screen.MousePointer = vbNormal
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set m_oGruposMN1 = Nothing
    Set oIMAsig = Nothing
End Sub

Public Sub GMN1Seleccionado()
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    m_oGMN1Seleccionado.Cod = sdbcGMN1_4Cod.Text
End Sub

Private Sub sdbcProceDen_Click()
    LimpiarTreeViewProce
    MostrarApartado 1000
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen = ""
        sdbcProceCod = ""
    End If
End Sub

Private Sub sdbcProceDen_Change()
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        Set m_oProcesoSeleccionado = Nothing
        Set m_oIOfertas = Nothing
        Set m_oOfertasProceso = Nothing
        Set m_oProveSeleccionado = Nothing
        Set g_oOfertaSeleccionada = Nothing
        Set m_oGrupoSeleccionado = Nothing
        Set g_oGrupoOferSeleccionado = Nothing
        bCargarComboDesde = True 'ZZ cuidadin
        
        LimpiarCampos (10)
        MostrarApartado 0
        
        bRespetarCombo = False
    End If
End Sub

Private Sub sdbcProceDen_CloseUp()
    If sdbcProceDen.Value = "....." Or Trim(sdbcProceDen.Value) = "" Then
        sdbcProceDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    bRespetarCombo = False
    
    ProcesoSeleccionado
    bCargarComboDesde = False
End Sub

Private Sub sdbcProceDen_DropDown()
    Dim udtEstIni As TipoEstadoProceso
    Dim udtEstFin As TipoEstadoProceso
    Dim oProceso As cProceso
    Dim lIdPerfil As Long
    
    sdbcProceDen.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set m_oProcesoSeleccionado = Nothing
    Set m_oIOfertas = Nothing
    Set m_oOfertasProceso = Nothing
    Set m_oProveSeleccionado = Nothing
    Set g_oOfertaSeleccionada = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Set g_oGrupoOferSeleccionado = Nothing
    
    Select Case ProceSelector1.Seleccion
        Case PSSeleccion.PSPendientes
            udtEstIni = conasignacionvalida
            udtEstFin = conofertas
        Case PSSeleccion.PSAbiertos
            udtEstIni = conasignacionvalida
            udtEstFin = PreadjYConObjNotificados
        Case PSSeleccion.PSParcialCerrados
            udtEstIni = ParcialmenteCerrado
            udtEstFin = ParcialmenteCerrado
        Case PSSeleccion.PSCerrados
            udtEstIni = conadjudicaciones
            udtEstFin = Cerrado
        Case PSSeleccion.PSTodos
            udtEstIni = conasignacionvalida
            udtEstFin = Cerrado
    End Select

    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If bCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstIni, udtEstFin, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , sdbcProceDen, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, udtEstIni, udtEstFin, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo.Value, sdbcGMN1_4Cod.Value, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRMat, m_bRComp, m_bRCompResp, m_bREqp, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, , , , True, g_bSoloInvitado, , m_bRPerfUON, lIdPerfil
    End If

    For Each oProceso In oProcesos
        sdbcProceDen.AddItem oProceso.Den & Chr(m_lSeparador) & oProceso.Cod & Chr(m_lSeparador) & BooleanToSQLBinary(oProceso.Invitado)
    Next
        
    If Not oProcesos.EOF Then sdbcProceDen.AddItem "....."
    
    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProceDen_InitColumnProps()
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcProceDen_PositionList(ByVal Text As String)
    PositionList sdbcProceDen, Text
End Sub
  
Private Sub LimpiarCampos(ByVal Index As Integer)
    cmdColaboracion.Enabled = False
    Select Case Index
        Case 0
            'Limpiar campos de proceso datos
            LimpiarProce
        Case 1
            'Limpiar campos de oferta
            LimpiarOfe
        Case 2
            'Limpiar campos de atributos
            sdbgAtributos.RemoveAll
        Case 3
            'Limpiar campos de adjuntos
            bRespetarCombo = True
            txtObsAdjun.Text = ""
            bRespetarCombo = False
            sdbgAdjun.RemoveAll
        Case 4
            'Limpiar campos de items
            If m_bGrupoConEscalados Then
                LimpiarPreciosEscalados
            Else
                LimpiarPrecios
            End If
        Case 5
            'Limpiar campos de proveedor
            LimpiarProve
        Case 6
            LimpiarGrupo
        Case 10
            LimpiarProce
            LimpiarProve
            LimpiarOfe
            LimpiarGrupo
            sdbgAtributos.RemoveAll
            bRespetarCombo = True
            txtObsAdjun.Text = ""
            bRespetarCombo = False
            sdbgAdjun.RemoveAll
            LimpiarPreciosEscalados
            LimpiarPrecios
            cmdRestaurar(0).Enabled = False
            cmdListado(0).Enabled = False
            cmdResponsable.Enabled = False
            cmdResponsable.ToolTipText = ""
            sdbcProceDen.Text = ""
    End Select
End Sub
Private Sub LimpiarProce()
    txtMat.Text = ""
    lstMaterial.clear
    lstMaterial.Visible = False
    txtMat.Visible = True
    lblFecApe(1).caption = ""
    lblFecPres(1).caption = ""
    lblFecIniSub(1).caption = ""
    lblFecLimit(1).caption = ""
    lblFecNec(1).caption = ""
    lblHoraIniSub(1).caption = ""
    lblHoraLimite(1).caption = ""
    chkPermAdjDir.Value = vbUnchecked
    lblPresupuesto(1).caption = ""
    lblMon(1).caption = ""
    lblSolicitud(1).caption = ""
    lblCambio(1).caption = ""
    lblFecIni(1).caption = ""
    lblFecFin(1).caption = ""
    lblDest(1).caption = ""
    lblDest(2).caption = ""
    lblPago(1).caption = ""
    lblPago(2).caption = ""
    lblProveProce(1).caption = ""
    lblProveProce(2).caption = ""
    LimpiarTreeViewProce
End Sub
Private Sub LimpiarOfe()
    txtFecOfe.Text = ""
    txtFecVal.Text = ""
    bRespetarCombo = True
    sdbcEstCod.Text = ""
    sdbcEstDen.Text = ""
    sdbcMonCod.Text = ""
    sdbcMonDen.Text = ""
    bRespetarCombo = False
    txtCambio.Text = ""
    txtCambio.Backcolor = RGB(255, 255, 255)
    txtCambio.Locked = False
    txtCambio.TabStop = True
    txtObs.Text = ""
    txtHoraOfe.Text = ""
    lblEquivalencia.caption = ""
    lblMonProceso.caption = ""
End Sub
Private Sub LimpiarProve()
    lblCodPortProve.caption = ""
    lblDir.caption = ""
    lblCp.caption = ""
    lblPob.caption = ""
    lblPaiCod.caption = ""
    lblPaiDen.caption = ""
    lblProviCod.caption = ""
    lblProviDen.caption = ""
    lblMonProveCod.caption = ""
    lblMonProveDen.caption = ""
    lblURL.caption = ""
    sdbgContactos.RemoveAll
    lblCal1Den.caption = ""
    lblcal2den.caption = ""
    lblcal3den.caption = ""
    lblCal1Val.caption = ""
    lblCal2Val.caption = ""
    lblCal3Val.caption = ""
    txtObsProve.Text = ""
End Sub

Private Sub LimpiarPreciosEscalados()
    sdbgPreciosEscalados.RemoveAll
    BorrarGruposPreciosEscalados
End Sub

Private Sub LimpiarPrecios()
    sdbgPrecios.RemoveAll
    If m_bCargarLayout Then
        sdbgPrecios.LoadLayout m_sLayOut
        sdbgPrecios.FieldSeparator = Chr(m_lSeparador)
        m_bCargarLayout = False
    End If
End Sub

Private Sub sdbcMonCod_Change()
     If Not bRespetarCombo Then
        If Not m_oProcesoSeleccionado.CambiarMonOferta And sdbcMonCod.Text <> "" Then
            If Not g_oOfertaSeleccionada Is Nothing Then
                If g_oOfertaSeleccionada.CodMon = m_oProcesoSeleccionado.MonCod Then
                    oMensajes.OfertaEnMonedaDelProceso m_oProcesoSeleccionado.MonCod
                    bRespetarCombo = True
                    sdbcMonCod.Value = m_oProcesoSeleccionado.MonCod
                    bRespetarCombo = False
                    sdbcMonCod_Validate False
                    Exit Sub
                End If
                
                'Comprobar que no hay un adjudicaci�n a una oferta previa del proveedor
                Dim sMonAdj As String
                If m_oProcesoSeleccionado.HayAdjudicacionesPreviasProveOtraMon(g_oOfertaSeleccionada.Prove, sdbcMonCod.Value, sMonAdj) Then
                    oMensajes.ImposibleModificarMonedaAdjudicacionesProce
                    bRespetarCombo = True
                    sdbcMonCod.Value = sMonAdj
                    bRespetarCombo = False
                    sdbcMonCod_Validate False
                    Exit Sub
                End If
            End If
        End If
        bRespetarCombo = True
        sdbcMonDen.Text = ""
        bRespetarCombo = False
        
        txtCambio.Text = ""
        lblEquivalencia.caption = ""
    
        If sdbcMonCod.Value <> "" Then
            bCargarComboDesde = True
        Else
            bCargarComboDesde = False
        End If
    End If
End Sub

Private Sub sdbcMonCod_CloseUp()
    If sdbcMonCod.Value = "..." Or Trim(sdbcMonCod.Value) = "" Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    
    If m_oProcesoSeleccionado.MonCod = sdbcMonCod.Value Then
        txtCambio.Text = 1
        lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
    Else
        txtCambio.Text = oMonedas.Item(sdbcMonCod).Equiv / dProceEquiv
        lblEquivalencia.caption = oMonedas.Item(sdbcMonCod).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
    End If
        
    txtCambio.ToolTipText = DblToStr(txtCambio.Text)
    If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
        txtCambio.Backcolor = RGB(255, 255, 192)
        txtCambio.Locked = True
        txtCambio.TabStop = False
    Else
        txtCambio.Backcolor = RGB(255, 255, 255)
        txtCambio.Locked = False
        txtCambio.TabStop = True
    End If
    If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
        txtCambio.Backcolor = RGB(255, 255, 192)
        txtCambio.Locked = True
        txtCambio.TabStop = False
    End If
        
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcMonCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
Dim sDesde As String

    Screen.MousePointer = vbHourglass
    
    sdbcMonCod.RemoveAll

    sDesde = ""
    If bCargarComboDesde Then
        sDesde = sdbcMonCod.Value
    End If

    If m_oProcesoSeleccionado.CambiarMonOferta Then
        oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, sDesde
    Else
        oMonedas.CargarTodasLasMonedasDesde 1, m_oProcesoSeleccionado.MonCod, , , , , True
    End If
    Codigos = oMonedas.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMonCod_InitColumnProps()
    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcMonCod_Validate(Cancel As Boolean)
    If Trim(sdbcMonCod.Value) = "" Then
        txtCambio.Locked = True
        txtCambio.TabStop = False
        Exit Sub
    End If

    If Not m_oProcesoSeleccionado.CambiarMonOferta Then
        If m_oProcesoSeleccionado.MonCod <> Trim(sdbcMonCod.Value) Then
            oMensajes.OfertaEnMonedaDelProceso m_oProcesoSeleccionado.MonCod
            bRespetarCombo = True
            sdbcMonCod.Text = m_oProcesoSeleccionado.MonCod
            bRespetarCombo = False
        End If
    End If
    
    If Not g_oOfertaSeleccionada Is Nothing Then
        'Comprobar que no hay un adjudicaci�n a una oferta previa del proveedor
        Dim sMonAdj As String
        If m_oProcesoSeleccionado.HayAdjudicacionesPreviasProveOtraMon(g_oOfertaSeleccionada.Prove, sdbcMonCod.Value, sMonAdj) Then
            oMensajes.ImposibleModificarMonedaAdjudicacionesProce
            bRespetarCombo = True
            If Accion = ACCRecOfeAnya Then
                sdbcMonCod.Value = sMonAdj
            Else
                sdbcMonCod.Value = g_oOfertaSeleccionada.CodMon
            End If
            sdbcMonCod_Validate False
            bRespetarCombo = False
            Cancel = True
            Exit Sub
        End If
    End If
    
    If UCase(Trim(sdbcMonCod.Value)) = UCase(Trim(sdbcMonCod.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonCod.Value = sdbcMonCod.Columns(0).Value ' edu incidencia 6578
        sdbcMonDen.Text = sdbcMonCod.Columns(1).Value
        sdbcMonDen.Value = sdbcMonCod.Columns(1).Value
        If txtCambio.Text = "" Then
            If UCase(sdbcMonCod.Value) = UCase(m_oProcesoSeleccionado.MonCod) Then
                txtCambio.Text = 1
                lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            Else
                txtCambio.Text = oMonedasGen.Item(sdbcMonCod.Text).Equiv / dProceEquiv
                lblEquivalencia.caption = oMonedasGen.Item(sdbcMonCod.Text).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            End If
        End If
        txtCambio.ToolTipText = DblToStr(txtCambio.Text)
        If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        Else
            txtCambio.Backcolor = RGB(255, 255, 255)
            txtCambio.Locked = False
            txtCambio.TabStop = True
        End If
        If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        End If
        bRespetarCombo = False
        Exit Sub
    End If
    
    If UCase(Trim(sdbcMonCod.Value)) = UCase(Trim(sdbcMonDen.Columns(1).Value)) Then
        bRespetarCombo = True
        sdbcMonDen.Text = sdbcMonDen.Columns(0).Value
        If txtCambio.Text = "" Then
            If UCase(sdbcMonCod.Value) = UCase(m_oProcesoSeleccionado.MonCod) Then
                txtCambio.Text = 1
                lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            Else
                txtCambio.Text = oMonedasGen.Item(sdbcMonCod.Text).Equiv / dProceEquiv
                lblEquivalencia.caption = oMonedasGen.Item(sdbcMonCod.Text).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            End If
        End If
        txtCambio.ToolTipText = DblToStr(txtCambio.Text)
        If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = True Or oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = True Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        Else
            txtCambio.Backcolor = RGB(255, 255, 255)
            txtCambio.Locked = False
            txtCambio.TabStop = True
        End If
        If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        End If
        
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    If Accion = ACCRecOfeAnya Or Accion = ACCRecOfeModDatGen Then
        oMonedas.CargarTodasLasMonedasDesde 1, sdbcMonCod.Text, , , , , True
    Else
        oMonedas.CargarTodasLasMonedasDesde 1, sdbcMonCod.Text, , , True, , True
    End If
    Screen.MousePointer = vbNormal
    
    If oMonedas.Item(1) Is Nothing Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiMon
        sdbcMonCod = ""
        sdbcMonCod.RemoveAll
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Cod) <> UCase(sdbcMonCod.Value) Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiMon
            sdbcMonCod = ""
            sdbcMonCod.RemoveAll
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonCod.Value = oMonedas.Item(1).Cod 'edu incidencia 6578
            sdbcMonDen = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            
            sdbcMonCod.Columns(0).Value = sdbcMonCod.Text
            sdbcMonCod.Columns(1).Value = sdbcMonDen.Text
            
            If UCase(sdbcMonCod.Value) = UCase(m_oProcesoSeleccionado.MonCod) Then
                txtCambio.Text = 1
                lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            Else
                txtCambio.Text = oMonedas.Item(1).Equiv / dProceEquiv
                lblEquivalencia.caption = oMonedas.Item(1).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            End If
            txtCambio.ToolTipText = DblToStr(txtCambio.Text)
            If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
                txtCambio.Backcolor = RGB(255, 255, 192)
                txtCambio.Locked = True
                txtCambio.TabStop = False
            Else
                txtCambio.Backcolor = RGB(255, 255, 255)
                txtCambio.Locked = False
                txtCambio.TabStop = True
            End If
            If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
                txtCambio.Backcolor = RGB(255, 255, 192)
                txtCambio.Locked = True
                txtCambio.TabStop = False
            End If
            
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    End If
End Sub

Private Sub sdbcMonden_Change()
     
     If Not bRespetarCombo Then
        If Not m_oProcesoSeleccionado.CambiarMonOferta And sdbcMonDen.Text <> "" Then
            If Not g_oOfertaSeleccionada Is Nothing Then
                If g_oOfertaSeleccionada.CodMon = m_oProcesoSeleccionado.MonCod Then
                    oMensajes.OfertaEnMonedaDelProceso m_oProcesoSeleccionado.MonCod
                    bRespetarCombo = True
                    sdbcMonCod.Value = m_oProcesoSeleccionado.MonCod
                    bRespetarCombo = False
                    sdbcMonCod_Validate False
                    Exit Sub
                End If
                
                'Comprobar que no hay un adjudicaci�n a una oferta previa del proveedor
                Dim sMonAdj As String
                If m_oProcesoSeleccionado.HayAdjudicacionesPreviasProveOtraMon(g_oOfertaSeleccionada.Prove, sdbcMonCod.Value, sMonAdj) Then
                    oMensajes.ImposibleModificarMonedaAdjudicacionesProce
                    bRespetarCombo = True
                    sdbcMonCod.Value = sMonAdj
                    bRespetarCombo = False
                    sdbcMonCod_Validate False
                    Exit Sub
                End If
            End If
        End If
        bRespetarCombo = True
        sdbcMonCod.Text = ""
        bRespetarCombo = False
        
        txtCambio.Text = ""
        lblEquivalencia.caption = ""
        bCargarComboDesde = True
        If sdbcMonDen.Value <> "" Then
            bCargarComboDesde = True
        Else
            bCargarComboDesde = False
        End If
    End If
    
End Sub

Private Sub sdbcMonDen_CloseUp()
    
    If sdbcMonDen.Value = "....." Or Trim(sdbcMonDen.Value) = "" Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    
    If m_oProcesoSeleccionado.MonCod = sdbcMonDen.Columns(1).Value Then
        txtCambio.Text = 1
        lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
    Else
        txtCambio.Text = oMonedas.Item(sdbcMonCod).Equiv / dProceEquiv
        lblEquivalencia.caption = oMonedas.Item(sdbcMonCod).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
    End If
    
    txtCambio.ToolTipText = DblToStr(txtCambio.Text)
    
    If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
        txtCambio.Backcolor = RGB(255, 255, 192)
        txtCambio.Locked = True
        txtCambio.TabStop = False
    Else
        txtCambio.Backcolor = RGB(255, 255, 255)
        txtCambio.Locked = False
        txtCambio.TabStop = True
    End If
    If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
        txtCambio.Backcolor = RGB(255, 255, 192)
        txtCambio.Locked = True
        txtCambio.TabStop = False
    End If
    
    bRespetarCombo = False
    bCargarComboDesde = False
End Sub

Private Sub sdbcMonDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim sDesde As String

    Screen.MousePointer = vbHourglass
    sdbcMonDen.RemoveAll
    
    sDesde = ""
    If bCargarComboDesde Then
        sDesde = sdbcMonDen.Value
    End If

    If m_oProcesoSeleccionado.CambiarMonOferta Then
        oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , sDesde, True, False
    Else
        oMonedas.CargarTodasLasMonedasDesde 1, m_oProcesoSeleccionado.MonCod, , , False, , True
    End If
        
    Codigos = oMonedas.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMonDen_InitColumnProps()
    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcMonDen_Validate(Cancel As Boolean)
    If Trim(sdbcMonDen.Value) = "" Then
        txtCambio.Locked = True
        txtCambio.TabStop = False
        Exit Sub
    End If
        
    If Not g_oOfertaSeleccionada Is Nothing Then
        'Comprobar que no hay un adjudicaci�n a una oferta previa del proveedor
        Dim sMonAdj As String
        If m_oProcesoSeleccionado.HayAdjudicacionesPreviasProveOtraMon(g_oOfertaSeleccionada.Prove, sdbcMonCod.Value, sMonAdj) Then
            oMensajes.ImposibleModificarMonedaAdjudicacionesProce
            bRespetarCombo = True
            sdbcMonCod.Value = sMonAdj
            sdbcMonCod_Validate False
            bRespetarCombo = False
            Cancel = True
            Exit Sub
        End If
    End If
    
    If UCase(Trim(sdbcMonDen.Value)) = UCase(Trim(sdbcMonDen.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcMonCod.Text = sdbcMonDen.Columns(1).Value
        sdbcMonCod.Value = sdbcMonDen.Columns(1).Value
        If txtCambio.Text = "" Then
            If sdbcMonCod.Value = m_oProcesoSeleccionado.MonCod Then
                txtCambio.Text = 1
                lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            Else
                txtCambio.Text = oMonedasGen.Item(sdbcMonCod.Text).Equiv / dProceEquiv
                lblEquivalencia.caption = oMonedasGen.Item(sdbcMonCod.Text).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            End If
        End If
        txtCambio.ToolTipText = DblToStr(txtCambio.Text)
        If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        Else
            txtCambio.Backcolor = RGB(255, 255, 255)
            txtCambio.Locked = False
            txtCambio.TabStop = True
        End If
        If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
            txtCambio.Backcolor = RGB(255, 255, 192)
            txtCambio.Locked = True
            txtCambio.TabStop = False
        End If
        
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    If Accion = ACCRecOfeAnya Or Accion = ACCRecOfeModDatGen Then
        oMonedas.CargarTodasLasMonedasDesde 1, , sdbcMonDen.Text, True, , , True
    Else
        oMonedas.CargarTodasLasMonedasDesde 1, , sdbcMonDen.Text, True, True, , True
    End If
    Screen.MousePointer = vbNormal
    
    If oMonedas.Item(1) Is Nothing Then
        oMensajes.NoValido sIdiMon
        sdbcMonDen.Text = ""
        sdbcMonDen.RemoveAll
        Exit Sub
    Else
        If UCase(oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) <> UCase(sdbcMonDen.Value) Then
            oMensajes.NoValido sIdiMon
            sdbcMonDen.Text = ""
            sdbcMonDen.RemoveAll
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcMonCod.Text = oMonedas.Item(1).Cod
            sdbcMonCod.Value = oMonedas.Item(1).Cod
            sdbcMonDen.Columns(0).Value = sdbcMonDen.Text
            sdbcMonDen.Columns(1).Value = sdbcMonCod.Text
            
            If sdbcMonCod.Value = m_oProcesoSeleccionado.MonCod Then
                txtCambio.Text = 1
                lblEquivalencia.caption = "1 " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            Else
                If Not m_oProcesoSeleccionado.CambiarMonOferta Then
                    oMensajes.OfertaEnMonedaDelProceso m_oProcesoSeleccionado.MonCod
                    sdbcMonCod.Text = m_oProcesoSeleccionado.MonCod
                    sdbcMonCod_Validate False
                    Exit Sub
                End If
                txtCambio.Text = oMonedas.Item(1).Equiv / dProceEquiv
                lblEquivalencia.caption = oMonedas.Item(1).Equiv / dProceEquiv & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
            End If
            
            txtCambio.ToolTipText = DblToStr(txtCambio.Text)
            
            If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
                txtCambio.Backcolor = RGB(255, 255, 192)
                txtCambio.Locked = True
                txtCambio.TabStop = False
            Else
                txtCambio.Backcolor = RGB(255, 255, 255)
                txtCambio.Locked = False
                txtCambio.TabStop = True
            End If
            If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
                txtCambio.Backcolor = RGB(255, 255, 192)
                txtCambio.Locked = True
                txtCambio.TabStop = False
            End If
            
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    End If
End Sub

Private Sub sdbcEstCod_Change()
     If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcEstDen.Text = ""
        bRespetarCombo = False
        
        If sdbcEstCod <> "" Then
            bCargarComboDesde = True
        Else
            bCargarComboDesde = False
        End If
    End If
End Sub

Private Sub sdbcEstCod_CloseUp()
    If sdbcEstCod.Value = "..." Or Trim(sdbcEstCod.Value) = "" Then
        sdbcEstCod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEstDen.Text = sdbcEstCod.Columns(1).Text
    sdbcEstCod.Text = sdbcEstCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcEstCod_DropDown()
Dim sDesde As String
Dim oest As COfeEstado

    Screen.MousePointer = vbHourglass
    
    sdbcEstCod.RemoveAll

    sDesde = ""
    If bCargarComboDesde Then
        sDesde = sdbcEstCod.Value
    End If
    
    oEstados.CargarTodosLosOfeEstados sDesde, , , , , , , basPublic.gParametrosInstalacion.gIdioma
    
    For Each oest In oEstados
        sdbcEstCod.AddItem oest.Cod & Chr(m_lSeparador) & oest.Den
    Next

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEstCod_InitColumnProps()
    sdbcEstCod.DataFieldList = "Column 0"
    sdbcEstCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcEstCod_Validate(Cancel As Boolean)

If Trim(sdbcEstCod.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcEstCod.Value)) = UCase(Trim(sdbcEstCod.Columns(0).Value)) Then
        bRespetarCombo = True
        sdbcEstDen = sdbcEstCod.Columns(1).Value
        bRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oEstados.CargarTodosLosOfeEstados sdbcEstCod.Text, , True, , , , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    If oEstados.Item(1) Is Nothing Then
        oMensajes.NoValido 92 ' "Estado"
        sdbcEstCod = ""
        Exit Sub
    Else
        If UCase(oEstados.Item(1).Cod) <> UCase(sdbcEstCod.Value) Then
            oMensajes.NoValido 92 '"Estado"
            sdbcEstCod = ""
            Exit Sub
        Else
            bRespetarCombo = True
            sdbcEstCod.Value = oEstados.Item(1).Cod 'edu incidencia 6578
            sdbcEstDen = oEstados.Item(1).Den
            sdbcEstCod.Columns(0).Value = sdbcEstCod.Text
            sdbcEstCod.Columns(1).Value = sdbcEstDen.Text
            bRespetarCombo = False
            bCargarComboDesde = False
        End If
    End If
End Sub

Private Sub sdbcEstden_Change()
     If Not bRespetarCombo Then
        bRespetarCombo = True
        sdbcEstCod.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = False
        If sdbcEstDen.Text <> "" Then bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcEstDen_CloseUp()
    If sdbcEstDen.Value = "....." Or Trim(sdbcEstDen.Value) = "" Then
        sdbcEstDen.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcEstCod.Text = sdbcEstDen.Columns(1).Text
    sdbcEstDen.Text = sdbcEstDen.Columns(0).Text
    bRespetarCombo = False
    bCargarComboDesde = False
End Sub

Private Sub sdbcEstDen_DropDown()
Dim sDesde As String
Dim oest As COfeEstado

    Screen.MousePointer = vbHourglass
    
    sdbcEstDen.RemoveAll

    sDesde = ""
    If bCargarComboDesde Then sDesde = sdbcEstDen.Value
        
    oEstados.CargarTodosLosOfeEstados , sDesde, , True, , , , basPublic.gParametrosInstalacion.gIdioma
    
    For Each oest In oEstados
        sdbcEstDen.AddItem oest.Den & Chr(m_lSeparador) & oest.Cod
    Next

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEstDen_InitColumnProps()
    sdbcEstDen.DataFieldList = "Column 0"
    sdbcEstDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValor_DropDown()
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim bCam As Boolean
Dim oatrib As CAtributo


If sdbgAtributos.Columns("VALOR").Locked Then
    sdbddValor.Enabled = False
    Exit Sub
End If

sdbddValor.RemoveAll
If sdbgAtributos.Columns("INTRO").Value = "1" Then
    If Not m_oGrupoSeleccionado Is Nothing Then
        Set oatrib = m_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
    Else
        Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
    End If
     If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
        bCam = True
    End If
    Set oLista = oatrib.ListaPonderacion
    For Each oElem In oLista
        If bCam Then
            sdbddValor.AddItem oElem.ValorLista * g_oOfertaSeleccionada.Cambio
        Else
            sdbddValor.AddItem oElem.ValorLista
        End If
    Next
    Set oatrib = Nothing
    Set oLista = Nothing
Else
    If sdbgAtributos.Columns("TIPO").Value = 4 Then
        sdbddValor.AddItem m_sIdiTrue
        sdbddValor.AddItem m_sIdiFalse
    End If
End If
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns("VALOR").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo
Dim bCam As Boolean

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            If Not m_oGrupoSeleccionado Is Nothing Then
                Set oatrib = m_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
            Else
                Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
            End If
            bCam = False
            If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                bCam = True
            End If
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If bCam Then
                    If (oElem.ValorLista * g_oOfertaSeleccionada.Cambio) = CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                        bExiste = True
                        Exit For
                    End If
                Else
                    If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                        bExiste = True
                        Exit For
                    End If
                End If
            Next
        Else
            If sdbgAtributos.Columns("TIPO").Value = 4 Then
                If UCase(m_sIdiTrue) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                End If
                If UCase(m_sIdiFalse) = UCase(sdbgAtributos.Columns("VALOR").Text) Then
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns("VALOR").Text = ""
            oMensajes.NoValido sdbgAtributos.Columns("VALOR").caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub
Private Sub sdbddCombo_DropDown()
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo
Dim lIdAtrib  As Long
Dim bCam As Boolean

If m_bGrupoConEscalados Then
    If Left(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name, 3) <> "AT_" Then Exit Sub
    
    If sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Locked Then
        sdbddCombo.Enabled = False
        Exit Sub
    End If
    
    sdbddCombo.RemoveAll
    lIdAtrib = Right(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name, Len(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name) - 3)
Else
    If Left(sdbgPrecios.Columns(sdbgPrecios.col).Name, 3) <> "AT_" Then Exit Sub
    
    If sdbgPrecios.Columns(sdbgPrecios.col).Locked Then
        sdbddCombo.Enabled = False
        Exit Sub
    End If
    
    sdbddCombo.RemoveAll
    lIdAtrib = Right(sdbgPrecios.Columns(sdbgPrecios.col).Name, Len(sdbgPrecios.Columns(sdbgPrecios.col).Name) - 3)

End If
If sdbcGrupo.Text = m_sIdiTodosItems Then
    Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
Else
    Set oatrib = m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
End If
If oatrib Is Nothing Then Exit Sub

If oatrib.TipoIntroduccion = Introselec Then
    If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
        bCam = True
    End If
    Set oLista = oatrib.ListaPonderacion
    For Each oElem In oLista
        If bCam Then
            sdbddCombo.AddItem oElem.ValorLista * g_oOfertaSeleccionada.Cambio
        Else
            sdbddCombo.AddItem oElem.ValorLista
        End If
    Next
Else
    If oatrib.Tipo = TipoBoolean Then
        sdbddCombo.AddItem m_sIdiTrue
        sdbddCombo.AddItem m_sIdiFalse
    End If
End If
Set oatrib = Nothing
Set oLista = Nothing
End Sub

Private Sub sdbddCombo_InitColumnProps()
    sdbddCombo.DataFieldList = "Column 0"
    sdbddCombo.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddCombo_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddCombo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddCombo.Rows - 1
            bm = sdbddCombo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddCombo.Columns(0).CellText(bm), 1, Len(Text))) Then
                If m_bGrupoConEscalados Then
                    sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Value = Mid(sdbddCombo.Columns(0).CellText(bm), 1, Len(Text))
                Else
                    sdbgPrecios.Columns(sdbgPrecios.col).Value = Mid(sdbddCombo.Columns(0).CellText(bm), 1, Len(Text))
                End If
                sdbddCombo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddCombo_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo
Dim lIdAtrib  As Long
Dim bCam As Boolean

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        If m_bGrupoConEscalados Then
            lIdAtrib = Right(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name, Len(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name) - 3)
        Else
            lIdAtrib = Right(sdbgPrecios.Columns(sdbgPrecios.col).Name, Len(sdbgPrecios.Columns(sdbgPrecios.col).Name) - 3)
        End If
        If sdbcGrupo.Text = m_sIdiTodosItems Then
            Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
        Else
            Set oatrib = m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
        End If
        If oatrib Is Nothing Then Exit Sub
        
        ''' Comprobar la existencia en la lista
        If oatrib.TipoIntroduccion = Introselec Then
            If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                bCam = True
            End If
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If m_bGrupoConEscalados Then
                    If bCam Then
                        If (oElem.ValorLista * g_oOfertaSeleccionada.Cambio) = CDbl(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text) Then
                            bExiste = True
                            Exit For
                        End If
                    Else
                        If UCase(oElem.ValorLista) = UCase(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text) Then
                            bExiste = True
                            Exit For
                        End If
                    End If
                Else
                    If bCam Then
                        If (oElem.ValorLista * g_oOfertaSeleccionada.Cambio) = CDbl(sdbgPrecios.Columns(sdbgPrecios.col).Text) Then
                            bExiste = True
                            Exit For
                        End If
                    Else
                        If UCase(oElem.ValorLista) = UCase(sdbgPrecios.Columns(sdbgPrecios.col).Text) Then
                            bExiste = True
                            Exit For
                        End If
                    End If
                End If
                
            Next
        Else
            If m_bGrupoConEscalados Then
                If oatrib.Tipo = TipoBoolean Then
                    If UCase(m_sIdiTrue) = UCase(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text) Then
                        bExiste = True
                    End If
                    If UCase(m_sIdiFalse) = UCase(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text) Then
                        bExiste = True
                    End If
                    
                End If
            Else
                If oatrib.Tipo = TipoBoolean Then
                    If UCase(m_sIdiTrue) = UCase(sdbgPrecios.Columns(sdbgPrecios.col).Text) Then
                        bExiste = True
                    End If
                    If UCase(m_sIdiFalse) = UCase(sdbgPrecios.Columns(sdbgPrecios.col).Text) Then
                        bExiste = True
                    End If
                    
                End If
            End If
        End If
        If Not bExiste Then
            If m_bGrupoConEscalados Then
                sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text = ""
            Else
                sdbgPrecios.Columns(sdbgPrecios.col).Text = ""
            End If
            If oatrib.Tipo = TipoBoolean Then
                oMensajes.AtributoValorNoValido "TIPO4"
            Else
                oMensajes.AtributoValorNoValido "NO_LISTA"
            End If
            RtnPassed = False
        Else
            RtnPassed = True
        End If
    End If
End Sub

Private Sub sdbddUsar_InitColumnProps()
    sdbddUsar.DataFieldList = "Column 0"
    sdbddUsar.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddUsar_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddUsar.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddUsar.Rows - 1
            bm = sdbddUsar.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddUsar.Columns(0).CellText(bm), 1, Len(Text))) Then
                If m_bGrupoConEscalados Then
                    sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Value = Mid(sdbddUsar.Columns(0).CellText(bm), 1, Len(Text))
                Else
                    sdbgPrecios.Columns(sdbgPrecios.col).Value = Mid(sdbddUsar.Columns(0).CellText(bm), 1, Len(Text))
                End If
                sdbddUsar.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddUsar_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim bExiste As Boolean

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If m_bGrupoConEscalados Then
            If UCase(m_sIdiUsarPrec(1)) = UCase(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text) Then
                bExiste = True
            End If
            If UCase(m_sIdiUsarPrec(2)) = UCase(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text) Then
                bExiste = True
            End If
            If UCase(m_sIdiUsarPrec(3)) = UCase(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text) Then
                bExiste = True
            End If
            If Not bExiste Then
                sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Text = m_sIdiUsarPrec(1)
                RtnPassed = False
            Else
                RtnPassed = True
            End If
        Else
            If UCase(m_sIdiUsarPrec(1)) = UCase(sdbgPrecios.Columns(sdbgPrecios.col).Text) Then
                bExiste = True
            End If
            If UCase(m_sIdiUsarPrec(2)) = UCase(sdbgPrecios.Columns(sdbgPrecios.col).Text) Then
                bExiste = True
            End If
            If UCase(m_sIdiUsarPrec(3)) = UCase(sdbgPrecios.Columns(sdbgPrecios.col).Text) Then
                bExiste = True
            End If
            If Not bExiste Then
                sdbgPrecios.Columns(sdbgPrecios.col).Text = m_sIdiUsarPrec(1)
                RtnPassed = False
            Else
                RtnPassed = True
            End If
        End If
    End If
End Sub

Private Sub sdbgAdjun_BtnClick()
    If sdbgAdjun.Columns("COMENTARIO").Value <> "" Then
    
        If m_oGrupoSeleccionado Is Nothing Then
            Set g_oOfertaSeleccionada.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value)).Oferta = g_oOfertaSeleccionada
            Set oIBaseDatos = g_oOfertaSeleccionada.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
        Else
            Set g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value)).Oferta = g_oOfertaSeleccionada
            Set g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value)).Grupo = m_oGrupoSeleccionado
            Set oIBaseDatos = g_oGrupoOferSeleccionado.Adjuntos.Item(CStr(sdbgAdjun.Columns("ID").Value))
        End If
        
        If m_oGrupoSeleccionado Is Nothing Then
            frmPROCEEspMod.g_sOrigen = "frmOFERecO"
        Else
            frmPROCEEspMod.g_sOrigen = "frmOFERecG"
        End If
        Set frmPROCEEspMod.g_oIBaseDatos = oIBaseDatos
        frmPROCEEspMod.g_bSoloLectura = True
        frmPROCEEspMod.Show 1
    End If
End Sub

Private Sub sdbgAdjun_RowLoaded(ByVal Bookmark As Variant)
    sdbgAdjun.Columns("FICHERO").CellStyleSet "Fichero"
End Sub

Private Sub sdbgAtributos_BtnClick()
Dim oatrib As CAtributo
Select Case sdbgAtributos.Columns(sdbgAtributos.col).Name
    Case "VALOR"
        frmATRIBDescr.g_bEdicion = bModoEdicion
        frmATRIBDescr.g_sOrigen = "frmOFERec"
        frmATRIBDescr.caption = sdbgAtributos.Columns(0).Value
        frmATRIBDescr.txtDescr.Text = NullToStr(sdbgAtributos.Columns(sdbgAtributos.col).Value)
        frmATRIBDescr.Show vbModal
        sdbgatributos_Change
    Case "DEN"
        If m_oGrupoSeleccionado Is Nothing Then
            Set oatrib = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
        Else
            Set oatrib = m_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
        End If
        Set frmDetAtribProce.g_oAtributo = oatrib
        Set frmDetAtribProce.g_oGrupoSeleccionado = m_oGrupoSeleccionado
        If Not frmDetAtribProce.g_oGrupoSeleccionado Is Nothing Then
            Set frmDetAtribProce.g_oGrupoSeleccionado.proceso = m_oProcesoSeleccionado
        End If
        Set frmDetAtribProce.g_oProceso = m_oProcesoSeleccionado
        frmDetAtribProce.g_sOrigen = "frmOFEREC"
        frmDetAtribProce.Show 1
End Select
End Sub

Private Sub sdbgPreciosEscalados_AfterColUpdate(ByVal ColIndex As Integer)
    Dim teserror As TipoErrorSummit
    Dim lIdA As Long
    Dim v As Variant
    Dim sCod As String
    Dim ogroup As SSDataWidgets_B.Group
    Dim oOferta As COferta
    Dim oAtribEsc As CEscalado
    Dim iAccion As Integer
    Dim bEspera As Boolean

    If g_oLineaEnEdicion Is Nothing Then
        DoEvents
        sdbgPreciosEscalados_Change
        DoEvents
    End If

    bModError = False

    ''' Modificamos en la base de datos

    ''Comprobacion de atributos de oferta obligatorios
    With sdbgPreciosEscalados
        Set ogroup = .Groups(.Grp)
        
        'Comprobamos si ha habido cambio en el valor de los atributos de oferta (de los aplicados).
        If ogroup.TagVariant = "ATRIBSNOCOSTESDESC" Then
            If Left(.Columns(ColIndex).Name, 3) = "AT_" Then
                lIdA = CLng(Right(.Columns(ColIndex).Name, Len(.Columns(ColIndex).Name) - 3))
                sCod = CStr(.Columns("ITEM").Value) & "$" & CStr(lIdA)
                
                g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorNum = Null
                g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorFec = Null
                g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorText = Null
                g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorBool = Null
                
                If .Columns(ColIndex).Value <> "" Then
                    Select Case g_oLineaEnEdicion.AtribOfertados.Item(sCod).objeto.Tipo
                        Case TiposDeAtributos.TipoNumerico
                            g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorNum = .Columns(ColIndex).Value
                        Case TiposDeAtributos.TipoFecha
                            g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorFec = .Columns(ColIndex).Value
                        Case TiposDeAtributos.TipoString
                            g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorText = .Columns(ColIndex).Value
                        Case TiposDeAtributos.TipoBoolean
                            Select Case UCase(.Columns(ColIndex).Value)
                                Case UCase(m_sIdiTrue)
                                    g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorBool = 1
                                Case UCase(m_sIdiFalse)
                                    g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorBool = 0
                            End Select
                    End Select
                End If
            End If
            
            'Se actualizan en BD
            teserror = g_oLineaEnEdicion.AtribOfertados.Item(sCod).GuardarCambiosOfertaAtributo(g_oLineaEnEdicion.Id)
            
            iAccion = ACCRecOfeAtribMod
        ElseIf ogroup.TagVariant <> "ADJUNTOS" Then
            If Left(.Columns(ColIndex).Name, 6) = "Precio" Then
                If .Columns(ColIndex).Value = "" Then
                    g_oLineaEnEdicion.Escalados.Item(CStr(ogroup.TagVariant)).Precio = Null
                Else
                    g_oLineaEnEdicion.Escalados.Item(CStr(ogroup.TagVariant)).Precio = CDbl(.Columns(ColIndex).Value)
                End If
                
                ComprobarRecalcularOferta ogroup.TagVariant
        
                bEspera = False
                If g_oOfertaSeleccionada.Recalcular Or g_oLineaEnEdicion.Oferta.Recalcular Then
                    g_oLineaEnEdicion.Oferta.Recalcular = True
                    Set g_oLineaEnEdicion.Oferta.proceso = m_oProcesoSeleccionado
                    bEspera = True
                    frmADJCargar.Show
                    frmADJCargar.lblCargando.caption = m_sLitRecalculando
                    frmADJCargar.Refresh
                End If
                
                Set oOferta = g_oLineaEnEdicion.Oferta
                teserror = g_oLineaEnEdicion.Escalados.Item(CStr(ogroup.TagVariant)).GuardarCambiosOferta(g_oLineaEnEdicion)
                g_oLineaEnEdicion.EliminarAdj = False
                Set oOferta = Nothing
                               
                iAccion = ACCRecOfeModPrec
            ElseIf Left(.Columns(ColIndex).Name, 4) = "ATE_" Then
                lIdA = CLng(Right(.Columns(ColIndex).Name, Len(.Columns(ColIndex).Name) - InStr(5, .Columns(ColIndex).Name, "_")))
                sCod = CStr(.Columns("ITEM").Value) & "$" & CStr(lIdA)
                
                g_oLineaEnEdicion.AtribOfertados.Item(sCod).Escalados.Item(CStr(ogroup.TagVariant)).valorNum = Null
                If .Columns(ColIndex).Value <> "" Then
                    g_oLineaEnEdicion.AtribOfertados.Item(sCod).Escalados.Item(CStr(ogroup.TagVariant)).valorNum = .Columns(ColIndex).Value
                End If
                
                ComprobarRecalcularOferta ogroup.TagVariant
        
                bEspera = False
                If g_oOfertaSeleccionada.Recalcular Or g_oLineaEnEdicion.Oferta.Recalcular Then
                    g_oLineaEnEdicion.Oferta.Recalcular = True
                    bEspera = True
                    frmADJCargar.Show
                    frmADJCargar.lblCargando.caption = m_sLitRecalculando
                    frmADJCargar.Refresh
                End If
                
                'Se actualizan en BD
                Set g_oLineaEnEdicion.Oferta.proceso = m_oProcesoSeleccionado
                Set oAtribEsc = g_oLineaEnEdicion.AtribOfertados.Item(sCod).Escalados.Item(CStr(ogroup.TagVariant))
                teserror = oAtribEsc.GuardarCambiosOfertaCosteDescuentoAplicado(g_oLineaEnEdicion, lIdA)
                Set oAtribEsc = Nothing
                
                iAccion = ACCRecOfeAtribMod
            End If
            
            If bEspera Then
                Unload frmADJCargar
                g_oOfertaSeleccionada.Recalcular = False
                g_oLineaEnEdicion.Oferta.Recalcular = False
            End If
        End If
        
        If teserror.NumError <> TESnoerror Then
            v = .ActiveCell.Value
            TratarError teserror
            If Me.Visible Then .SetFocus
            bModError = True
            .ActiveCell.Value = v
            Accion = ACCRecOfeCon
            .DataChanged = False
        Else
            SincronizarPrecioItem
            ''' Registro de acciones
            basSeguridad.RegistrarAccion iAccion, "Anyo:" & sdbcAnyo & "GMN1:" & sdbcGMN1_4Cod & "Proce:" & sdbcProceCod.Text & "Item:" & CStr(g_oLineaEnEdicion.Id) & "Prove:" & g_oOfertaSeleccionada.Prove & "Oferta:" & g_oOfertaSeleccionada.Num
            'Registramos llamada a recalcular
            If bEspera Then
                basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Ofertas Anyo:" & CStr(sdbcAnyo) & " Gmn1:" & CStr(sdbcGMN1_4Cod) & " Proce:" & CStr(sdbcProceCod)
            End If
    
            Accion = ACCRecOfeCon
            cmdDeshacer.Enabled = False
            Set g_oLineaEnEdicion = Nothing
        End If
    End With
End Sub

Private Sub sdbgPrecios_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila

    Dim teserror As TipoErrorSummit
    Dim lIdA As Long
    Dim v As Variant
    Dim i As Integer
    Dim sCod As String
    Dim oatrib As CAtributoOfertado

    If g_oLineaEnEdicion Is Nothing Then
        DoEvents
        sdbgPrecios_Change
        DoEvents
    End If

    bModError = False

    ''' Modificamos en la base de datos
    
    If sdbgPrecios.Columns(16).Value = "" Then
        g_oLineaEnEdicion.CantidadMaxima = Null
    Else
        g_oLineaEnEdicion.CantidadMaxima = CDbl(sdbgPrecios.Columns(16).Value)
    End If

    If sdbgPrecios.Columns(10).Value = "" Then
        g_oLineaEnEdicion.Precio = Null
    Else
        g_oLineaEnEdicion.Precio = CDbl(sdbgPrecios.Columns(10).Value)
    End If

    If sdbgPrecios.Columns(12).Value = "" Then
        g_oLineaEnEdicion.Precio2 = Null
    Else
        g_oLineaEnEdicion.Precio2 = CDbl(sdbgPrecios.Columns(12).Value)
    End If
    
    If sdbgPrecios.Columns(14).Value = "" Then
        g_oLineaEnEdicion.Precio3 = Null
    Else
        g_oLineaEnEdicion.Precio3 = CDbl(sdbgPrecios.Columns(14).Value)
    End If
    g_oLineaEnEdicion.Hom = False
    Select Case sdbgPrecios.Columns(18).Text
        Case m_sIdiUsarPrec(1)
            g_oLineaEnEdicion.Usar = 1
        Case m_sIdiUsarPrec(2)
            g_oLineaEnEdicion.Usar = 2
        Case m_sIdiUsarPrec(3)
            g_oLineaEnEdicion.Usar = 3
    End Select
    g_oLineaEnEdicion.Precio1Obs = g_sPrec1Obs
    g_oLineaEnEdicion.Precio2Obs = g_sPrec2Obs
    g_oLineaEnEdicion.Precio3Obs = g_sPrec3Obs
    
    For i = 19 To sdbgPrecios.Cols - 1
        If Left(sdbgPrecios.Columns(i).Name, 2) = "AT" Then
            lIdA = CLng(Right(sdbgPrecios.Columns(i).Name, Len(sdbgPrecios.Columns(i).Name) - 3))
            sCod = CStr(sdbgPrecios.Columns("ITEM").Value) & "$" & CStr(lIdA)
            g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorNum = Null
            g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorFec = Null
            g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorText = Null
            g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorBool = Null
            If sdbgPrecios.Columns(i).Value <> "" Then
                Select Case g_oLineaEnEdicion.AtribOfertados.Item(sCod).objeto.Tipo
                Case TiposDeAtributos.TipoNumerico
                    g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorNum = sdbgPrecios.Columns(i).Value
                Case TiposDeAtributos.TipoFecha
                    g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorFec = sdbgPrecios.Columns(i).Value
                Case TiposDeAtributos.TipoString
                    g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorText = sdbgPrecios.Columns(i).Value
                Case TiposDeAtributos.TipoBoolean
                    Select Case UCase(sdbgPrecios.Columns(i).Value)
                    Case UCase(m_sIdiTrue)
                        g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorBool = 1
                    Case UCase(m_sIdiFalse)
                        g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorBool = 0
                    End Select
                End Select
            End If
           
        End If
    Next i
    
    
    Set oIBaseDatos = g_oLineaEnEdicion
    Dim bEspera As Boolean
    bEspera = False
    If Not g_oOfertaSeleccionada.Recalcular Then
        If Not g_oOfertaSeleccionada.AtribProcOfertados Is Nothing Then
            If g_oOfertaSeleccionada.AtribProcOfertados.Count > 0 Then
                For Each oatrib In g_oOfertaSeleccionada.AtribProcOfertados
                    If Not IsNull(oatrib.valorNum) And m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(oatrib.idAtribProce)).UsarPrec = 1 Then
                        g_oOfertaSeleccionada.Recalcular = True
                        g_oLineaEnEdicion.Oferta.Recalcular = True
                        Exit For
                    End If
                Next
            End If
        End If
        
        If Not g_oGrupoOferSeleccionado.AtribOfertados Is Nothing And Not g_oOfertaSeleccionada.Recalcular Then
            If g_oGrupoOferSeleccionado.AtribOfertados.Count > 0 Then
                For Each oatrib In g_oGrupoOferSeleccionado.AtribOfertados
                    If Not IsNull(oatrib.valorNum) And m_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(oatrib.idAtribProce)).UsarPrec = 1 Then
                        g_oOfertaSeleccionada.Recalcular = True
                        g_oLineaEnEdicion.Oferta.Recalcular = True
                        Exit For
                    End If
                Next
            End If
        End If
            
        If Not g_oLineaEnEdicion.AtribOfertados Is Nothing And Not g_oOfertaSeleccionada.Recalcular Then
            If g_oLineaEnEdicion.AtribOfertados.Count > 0 Then
                For Each oatrib In g_oLineaEnEdicion.AtribOfertados
                    If Not IsNull(oatrib.valorNum) And m_oProcesoSeleccionado.Grupos.Item(g_oLineaEnEdicion.grupoCod).AtributosItem.Item(CStr(oatrib.idAtribProce)).UsarPrec = 1 Then
                        g_oOfertaSeleccionada.Recalcular = True
                        g_oLineaEnEdicion.Oferta.Recalcular = True
                        Exit For
                    End If
                Next
            End If
        End If
    End If
    
    If g_oOfertaSeleccionada.Recalcular Or g_oLineaEnEdicion.Oferta.Recalcular Then
        g_oLineaEnEdicion.Oferta.Recalcular = True
        Set g_oLineaEnEdicion.Oferta.proceso = m_oProcesoSeleccionado
        bEspera = True
        frmADJCargar.Show
        frmADJCargar.lblCargando.caption = m_sLitRecalculando
        frmADJCargar.Refresh
    End If
    
    teserror = oIBaseDatos.FinalizarEdicionModificando
    
    If bEspera Then
        Unload frmADJCargar
        g_oOfertaSeleccionada.Recalcular = False
        g_oLineaEnEdicion.Oferta.Recalcular = True
        If teserror.NumError = TESnoerror Then teserror = m_oProcesoSeleccionado.CalcularPrecioItem(g_oLineaEnEdicion.Id)
    End If
    If teserror.NumError <> TESnoerror Then
        v = sdbgPrecios.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgPrecios.SetFocus
        bModError = True
        sdbgPrecios.ActiveCell.Value = v
        Accion = ACCRecOfeCon
        sdbgPrecios.DataChanged = False
    Else
        SincronizarPrecioItem
        'Como puedfe ser que se recalcule el precio del item en funcion de la configuracion general y la configuracion del proceso,
        'recargamos los precios del item para que sean los recalculados.
        sdbgPrecios.RemoveAll
        g_oGrupoOferSeleccionado.CargarPrecios
        Rellenar_sdbgPrecios
        ''' Registro de acciones
        basSeguridad.RegistrarAccion ACCRecOfeModPrec, "Anyo:" & sdbcAnyo & "GMN1:" & sdbcGMN1_4Cod & "Proce:" & sdbcProceCod.Text & "Item:" & CStr(g_oLineaEnEdicion.Id) & "Prove:" & g_oOfertaSeleccionada.Prove & "Oferta:" & g_oOfertaSeleccionada.Num
        'Registramos llamada a recalcular
        If bEspera Then
            basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Ofertas Anyo:" & CStr(sdbcAnyo) & " Gmn1:" & CStr(sdbcGMN1_4Cod) & " Proce:" & CStr(sdbcProceCod)
        End If

        Accion = ACCRecOfeCon
        cmdDeshacer.Enabled = False
        Set oIBaseDatos = Nothing
        Set g_oLineaEnEdicion = Nothing
    End If
End Sub
Private Sub SincronizarPrecioItem()
Dim oAtribO As CAtributoOfertado

'Se sincronizan los cambios entre la oferta y los grupos
    If sdbcGrupo.Text = m_sIdiTodosItems Then
        If g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).Lineas Is Nothing Then
           g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).Grupo.CargarAtributos ambito:=AmbItem
           g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).CargarPrecios
        ElseIf g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).Lineas.Count = 0 Then
            g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).Grupo.CargarAtributos ambito:=AmbItem
            g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).CargarPrecios
        End If
        If g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).Lineas.Item(CStr(g_oLineaEnEdicion.Id)) Is Nothing Then Exit Sub
        With g_oOfertaSeleccionada.Grupos.Item(g_oLineaEnEdicion.grupoCod).Lineas.Item(CStr(g_oLineaEnEdicion.Id))
            Set .Adjuntos = g_oLineaEnEdicion.Adjuntos
            .NumAdjuns = g_oLineaEnEdicion.NumAdjuns
            If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                For Each oAtribO In g_oLineaEnEdicion.AtribOfertados
                    If Not .AtribOfertados Is Nothing Then
                        If Not .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)) Is Nothing Then
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorBool = oAtribO.valorBool
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorFec = oAtribO.valorFec
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorNum = oAtribO.valorNum
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorText = oAtribO.valorText
                        Else
                            If IsValorAtributo(oAtribO) Then
                                .AtribOfertados.Add oAtribO.idAtribProce, oAtribO.UltimaOferta, oAtribO.Item, oAtribO.Grupo, oAtribO.valorBool, oAtribO.valorFec, oAtribO.valorNum, oAtribO.ValorPond, oAtribO.valorText, , , oAtribO.objeto
                            End If
                        End If
                    Else
                        If IsValorAtributo(oAtribO) Then
                            Set .AtribOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                            .AtribOfertados.Add oAtribO.idAtribProce, oAtribO.UltimaOferta, oAtribO.Item, oAtribO.Grupo, oAtribO.valorBool, oAtribO.valorFec, oAtribO.valorNum, oAtribO.ValorPond, oAtribO.valorText, , , oAtribO.objeto
                        End If
                    End If
                Next
            End If
            .CantidadMaxima = g_oLineaEnEdicion.CantidadMaxima
            .NumAdjuns = g_oLineaEnEdicion.NumAdjuns
            .ObsAdjun = g_oLineaEnEdicion.ObsAdjun
            .Precio = g_oLineaEnEdicion.Precio
            .Precio1Obs = g_oLineaEnEdicion.Precio1Obs
            .Precio2 = g_oLineaEnEdicion.Precio2
            .Precio2Obs = g_oLineaEnEdicion.Precio2Obs
            .Precio3 = g_oLineaEnEdicion.Precio3
            .Precio3Obs = g_oLineaEnEdicion.Precio3Obs
            .PrecioOferta = g_oLineaEnEdicion.PrecioOferta
            .Usar = g_oLineaEnEdicion.Usar
        End With
    Else
        If g_oOfertaSeleccionada.Lineas Is Nothing Then Exit Sub
        If g_oOfertaSeleccionada.Lineas.Count = 0 Then Exit Sub
        If g_oOfertaSeleccionada.Lineas.Item(CStr(g_oLineaEnEdicion.Id)) Is Nothing Then
            g_oOfertaSeleccionada.proceso.CargarAtributos ambito:=AmbItem, vGrupo:="#"
            g_oOfertaSeleccionada.CargarPrecios
            If g_oOfertaSeleccionada.Lineas.Item(CStr(g_oLineaEnEdicion.Id)) Is Nothing Then Exit Sub
        End If
        
        With g_oOfertaSeleccionada.Lineas.Item(CStr(g_oLineaEnEdicion.Id))
            Set .Adjuntos = g_oLineaEnEdicion.Adjuntos
            .NumAdjuns = g_oLineaEnEdicion.NumAdjuns
            If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                For Each oAtribO In g_oLineaEnEdicion.AtribOfertados
                    If Not .AtribOfertados Is Nothing Then
                        If Not .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)) Is Nothing Then
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorBool = oAtribO.valorBool
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorFec = oAtribO.valorFec
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorNum = oAtribO.valorNum
                            .AtribOfertados.Item(CStr(oAtribO.Item) & "$" & CStr(oAtribO.idAtribProce)).valorText = oAtribO.valorText
                        Else
                            If IsValorAtributo(oAtribO) Then
                                .AtribOfertados.Add oAtribO.idAtribProce, oAtribO.UltimaOferta, oAtribO.Item, oAtribO.Grupo, oAtribO.valorBool, oAtribO.valorFec, oAtribO.valorNum, oAtribO.ValorPond, oAtribO.valorText, , , oAtribO.objeto
                            End If
                        End If
                    Else
                        If IsValorAtributo(oAtribO) Then
                            Set .AtribOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                            .AtribOfertados.Add oAtribO.idAtribProce, oAtribO.UltimaOferta, oAtribO.Item, oAtribO.Grupo, oAtribO.valorBool, oAtribO.valorFec, oAtribO.valorNum, oAtribO.ValorPond, oAtribO.valorText, , , oAtribO.objeto
                        End If
                    End If
                Next
            End If

            .CantidadMaxima = g_oLineaEnEdicion.CantidadMaxima
            .NumAdjuns = g_oLineaEnEdicion.NumAdjuns
            .ObsAdjun = g_oLineaEnEdicion.ObsAdjun
            .Precio = g_oLineaEnEdicion.Precio
            .Precio1Obs = g_oLineaEnEdicion.Precio1Obs
            .Precio2 = g_oLineaEnEdicion.Precio2
            .Precio2Obs = g_oLineaEnEdicion.Precio2Obs
            .Precio3 = g_oLineaEnEdicion.Precio3
            .Precio3Obs = g_oLineaEnEdicion.Precio3Obs
            .PrecioOferta = g_oLineaEnEdicion.PrecioOferta
            .Usar = g_oLineaEnEdicion.Usar
        End With
    End If
    
End Sub

''' <summary>Realiza la comprobaci�n para la necesiad del rec�lculo de la oferta</summary>
''' <param name="lIdEsc">Id. escalado</param>
''' <remarks>Llamada desde: sdbgPreciosEscalados_AfterUpdate; Tiempo m�ximo:0,1</remarks>

Private Sub ComprobarRecalcularOferta(ByVal lIdEsc As Long)
    Dim oatrib As CAtributoOfertado
    
    If Not g_oOfertaSeleccionada.Recalcular Then
        If Not g_oOfertaSeleccionada.AtribProcOfertados Is Nothing Then
            If g_oOfertaSeleccionada.AtribProcOfertados.Count > 0 Then
                For Each oatrib In g_oOfertaSeleccionada.AtribProcOfertados
                    If Not IsNull(oatrib.valorNum) And m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(oatrib.idAtribProce)).UsarPrec = 1 Then
                        g_oOfertaSeleccionada.Recalcular = True
                        g_oLineaEnEdicion.Oferta.Recalcular = True
                        Exit For
                    End If
                Next
            End If
        End If
        
        If Not g_oGrupoOferSeleccionado.AtribOfertados Is Nothing And Not g_oOfertaSeleccionada.Recalcular Then
            If g_oGrupoOferSeleccionado.AtribOfertados.Count > 0 Then
                For Each oatrib In g_oGrupoOferSeleccionado.AtribOfertados
                    If Not IsNull(oatrib.valorNum) And m_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(oatrib.idAtribProce)).UsarPrec = 1 Then
                        g_oOfertaSeleccionada.Recalcular = True
                        g_oLineaEnEdicion.Oferta.Recalcular = True
                        Exit For
                    End If
                Next
            End If
        End If
            
        If Not g_oLineaEnEdicion.AtribOfertados Is Nothing And Not g_oOfertaSeleccionada.Recalcular Then
            If g_oLineaEnEdicion.AtribOfertados.Count > 0 Then
                If m_oProcesoSeleccionado.Grupos.Item(g_oLineaEnEdicion.grupoCod).UsarEscalados = 0 Then
                    For Each oatrib In g_oLineaEnEdicion.AtribOfertados
                        If Not IsNull(oatrib.valorNum) And m_oProcesoSeleccionado.Grupos.Item(g_oLineaEnEdicion.grupoCod).AtributosItem.Item(CStr(oatrib.idAtribProce)).UsarPrec = 1 Then
                            g_oOfertaSeleccionada.Recalcular = True
                            g_oLineaEnEdicion.Oferta.Recalcular = True
                            Exit For
                        End If
                    Next
                Else
                    For Each oatrib In g_oLineaEnEdicion.AtribOfertados
                        If Not oatrib.Escalados Is Nothing Then
                            If oatrib.Escalados.Count > 0 Then
                                If Not oatrib.Escalados.Item(CStr(lIdEsc)) Is Nothing Then
                                    If Not IsNull(oatrib.Escalados.Item(CStr(lIdEsc)).valorNum) And m_oProcesoSeleccionado.Grupos.Item(g_oLineaEnEdicion.grupoCod).AtributosItem.Item(CStr(oatrib.idAtribProce)).UsarPrec = 1 Then
                                        If oatrib.Escalados.Usar(lIdEsc, g_oLineaEnEdicion.Cantidad) Then
                                            g_oOfertaSeleccionada.Recalcular = True
                                            g_oLineaEnEdicion.Oferta.Recalcular = True
                                            Exit For
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        End If
        
        Set oatrib = Nothing
    End If
End Sub

Private Sub sdbgPreciosEscalados_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim i, j As Integer
    Dim lIdA As Long
    Dim sCod As String
    Dim vValorNumAntiguo As Variant
    Dim irespuesta As Integer
    Dim bTienePrecio As Boolean
    Dim bTieneAlgunPrecio As Boolean
    Dim bCambioPrecio As Boolean
    Dim bCambioAtrib As Boolean
    Dim ogroup As SSDataWidgets_B.Group
    Dim sEscalado As String
    Dim lIdItem As Long

    If g_oLineaEnEdicion Is Nothing Then Exit Sub
    
    bCambioAtrib = False
    bCambioPrecio = False
    
    With sdbgPreciosEscalados
        ' JVS Comprueba que existe alg�n precio v�lido entre los escalados
        If .Groups(.Grp).TagVariant <> "ATRIBSNOCOSTESDESC" And .Groups(.Grp).TagVariant <> "ADJUNTOS" Then
            If Left(.Columns(ColIndex).Name, 6) = "Precio" Then
                bTienePrecio = False
            
                lIdItem = .Columns("ITEM").Value
                sEscalado = .Groups(.Grp).TagVariant
                Set ogroup = .Groups(.Grp)
                If .Columns(ColIndex).Text <> "" Then
                    If Not IsNumeric(.Columns(ColIndex).Text) Then
                        bModError = True
                        oMensajes.NoValido sIdiPrecio
                        .SetFocus
                        .col = .Columns(ColIndex).Position
                        Cancel = True
                        Exit Sub
                    Else
                        If NullToStr(g_oLineaEnEdicion.Escalados.Item(sEscalado).Precio) <> .Columns(ColIndex).Text Then
                            'Ha cambiado el precio en alguno de los escalados
                            bCambioPrecio = True
                        End If
                    End If
                                        
                    bTienePrecio = True
                End If
                
                ''Comprobacion de atributos de oferta obligatorios
                If m_oProcesoSeleccionado.AtributosOfeObl And bTienePrecio Then
                    For j = 2 To ogroup.Columns.Count - 1
                        If Left(ogroup.Columns(j).Name, 2) = "AT" And ogroup.Columns(j).TagVariant = "1" Then
                            If ogroup.Columns(j).Value = "" Then
                                oMensajes.AtributosOblSinRellenar
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    Next j
                End If
                            
                'Comprobamos si ha habido cambio en el valor de los atributos de oferta (de los aplicados).
                For j = 2 To ogroup.Columns.Count - 1
                    If Left(ogroup.Columns(j).Name, 3) = "ATE" Then
                        sCod = "ATE_" & sEscalado
                        lIdA = CLng(Right(ogroup.Columns(j).Name, Len(ogroup.Columns(j).Name) - (Len(sCod) + 1)))
                        sCod = CStr(.Columns("ITEM").Value) & "$" & CStr(lIdA)
                        If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                            If Not g_oLineaEnEdicion.AtribOfertados.Item(sCod) Is Nothing Then
                                vValorNumAntiguo = g_oLineaEnEdicion.AtribOfertados.Item(sCod).Escalados.Item(sEscalado).valorNum
                            End If
                        End If
                        If ogroup.Columns(j).Value <> "" Then
                            If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                                If Not g_oLineaEnEdicion.AtribOfertados.Item(sCod) Is Nothing Then
                                    Select Case g_oLineaEnEdicion.AtribOfertados.Item(sCod).objeto.Tipo
                                    Case TiposDeAtributos.TipoNumerico
                                        'Si ha habido cambio en el valornum y se tratra de un atributo aplicado habr�a que recalcular la oferta.
                                        If VarToDbl0(vValorNumAntiguo) <> StrToDbl0(ogroup.Columns(j).Value) And _
                                            Not IsNull(m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdA)).PrecioAplicarA) And _
                                            m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdA)).UsarPrec = 1 Then
                                                bCambioAtrib = True
                                                Exit For
                                        End If
                                    End Select
                                End If
                            End If
                        End If
                    End If
                Next j
            End If
        End If
            
        ' Columna actual / columna primer atributo
        If Not ComprobarValorAtributo(.col, sdbgPreciosEscalados) Then
            bModError = True
            If Me.Enabled And Me.Visible Then .SetFocus
            Cancel = True
            Exit Sub
        End If
        
        If Not ComprobarExistenAdjudicacionesEscalados() Then
            If Me.Enabled And Me.Visible Then .SetFocus
            Cancel = True
            Exit Sub
        End If
        
        ''Comprobacion de atributos de oferta obligatorios
        If m_oProcesoSeleccionado.AtributosOfeObl Then
            For i = 1 To .Groups.Count - 1
                bTieneAlgunPrecio = False
                If .Groups(i).TagVariant <> "ATRIBSNOCOSTESDESC" And .Groups(i).TagVariant <> "ADJUNTOS" Then
                    If .Columns("Precio_" & .Groups(i).TagVariant).Text <> "" Then
                        bTieneAlgunPrecio = True
                    End If
                End If
            Next
            
            If bTieneAlgunPrecio Then
                Set ogroup = .Groups(.Groups.Count - 1)
                For i = 0 To ogroup.Columns.Count - 1
                    If Left(ogroup.Columns(i).Name, 2) = "AT" And ogroup.Columns(i).TagVariant = "1" Then
                        If ogroup.Columns(i).Value = "" Then
                            oMensajes.AtributosOblSinRellenar
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                Next i
                Set ogroup = Nothing
            End If
        End If
        
        'Comprobamos si ha habido cambio en el valor de los atributos de oferta (de los aplicados).
        Set ogroup = .Groups(.Groups.Count - 1)
        For i = 0 To ogroup.Columns.Count - 1
            If Left(ogroup.Columns(i).Name, 2) = "AT" Then
                lIdA = CLng(Right(ogroup.Columns(i).Name, Len(ogroup.Columns(i).Name) - 3))
                sCod = CStr(.Columns("ITEM").Value) & "$" & CStr(lIdA)
                If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                    If Not g_oLineaEnEdicion.AtribOfertados.Item(sCod) Is Nothing Then
                        vValorNumAntiguo = g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorNum
                    End If
                End If
                If ogroup.Columns(i).Value <> "" Then
                    If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                        If Not g_oLineaEnEdicion.AtribOfertados.Item(sCod) Is Nothing Then
                            Select Case g_oLineaEnEdicion.AtribOfertados.Item(sCod).objeto.Tipo
                            Case TiposDeAtributos.TipoNumerico
                                'Si ha habido cambio en el valornum y se tratra de un atributo aplicado habr�a que recalcular la oferta.
                                If VarToDbl0(vValorNumAntiguo) <> StrToDbl0(ogroup.Columns(i).Value) And _
                                    Not IsNull(m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdA)).PrecioAplicarA) And _
                                    m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdA)).UsarPrec = 1 Then
                                        bCambioAtrib = True
                                        Exit For
                                End If
                            End Select
                        End If
                    End If
                End If
               
            End If
        Next i
    End With
    
    'Le avisamos de que ha cambiado el valor de los atributos aplicados (solo para ofertas del portal)
    If bCambioAtrib And g_oOfertaSeleccionada.portal = True Then
        irespuesta = oMensajes.AvisoConConfirmacion(m_sMensajeModifAtrib)
        If irespuesta = vbNo Then
            bCambioAtrib = False
            Cancel = True
            Exit Sub
        End If
    End If
        
    'Le avisamos de que ha cambiado el precio de los items (solo para ofertas del portal)
    If bCambioPrecio And g_oOfertaSeleccionada.portal = True Then
        irespuesta = oMensajes.AvisoConConfirmacion(m_sMensajeModifPrecio)
        If irespuesta = vbNo Then
            bCambioPrecio = False
            Cancel = True
            Exit Sub
        End If
    End If
        
    'Si ha cambiado el valor del atributo aplicado, o el valor del precio o el precio que se usa se recalcula la oferta.
    If bCambioPrecio Or bCambioAtrib Then
        g_oOfertaSeleccionada.Recalcular = (bCambioAtrib Or g_oLineaEnEdicion.Escalados.Usar(sEscalado, g_oLineaEnEdicion.Cantidad))
        Set g_oOfertaSeleccionada.proceso = m_oProcesoSeleccionado
    End If
End Sub

''' <summary>
''' Evento que salta en el grid de items
''' </summary>
''' <param name="cancel">Sirve para parar el cambio</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Al hacer cambios en el grid;  Tiempo m�ximo:0</remarks>
Private Sub sdbgPrecios_BeforeUpdate(Cancel As Integer)

Dim i As Integer
Dim lIdA As Long
Dim sCod As String
Dim vValorNumAntiguo As Variant
Dim irespuesta As Integer
Dim bTienePrecio As Boolean
Dim bCambioPrecio As Boolean
Dim bCambioAtrib As Boolean

    If g_oLineaEnEdicion Is Nothing Then Exit Sub
    
    If sdbgPrecios.Columns(10).Text <> "" Then
        If Not IsNumeric(sdbgPrecios.Columns(10).Text) Then
            bModError = True
            oMensajes.NoValido sIdiPrecio
            If Me.Visible Then sdbgPrecios.SetFocus
            sdbgPrecios.col = 10
            Cancel = True
            Exit Sub
        End If
        bTienePrecio = True
    End If
    
    If m_oProcesoSeleccionado.PedirAlterPrecios Then
        If sdbgPrecios.Columns(12).Text <> "" Then
            If Not IsNumeric(sdbgPrecios.Columns(12).Text) Then
                bModError = True
                oMensajes.NoValido sIdiPrecio
                If Me.Visible Then sdbgPrecios.SetFocus
                sdbgPrecios.col = 12
                Cancel = True
                Exit Sub
            End If
            bTienePrecio = True
        End If
        If sdbgPrecios.Columns(14).Text <> "" Then
            If Not IsNumeric(sdbgPrecios.Columns(14).Text) Then
                bModError = True
                oMensajes.NoValido sIdiPrecio
                If Me.Visible Then sdbgPrecios.SetFocus
                sdbgPrecios.col = 14
                Cancel = True
                Exit Sub
            End If
            bTienePrecio = True
        End If
    End If
    
    If sdbgPrecios.Columns(16).Text <> "" Then
        If Not IsNumeric(sdbgPrecios.Columns(16).Text) Then
            bModError = True
            oMensajes.NoValido sIdiCantidadMax
            If Me.Visible Then sdbgPrecios.SetFocus
            sdbgPrecios.col = 16
            Cancel = True
            Exit Sub
        End If
    End If
    
    If Not ComprobarValorAtributo(sdbgPrecios.col, sdbgPrecios) Then
        bModError = True
        If Me.Enabled And Me.Visible Then sdbgPrecios.SetFocus
        Cancel = True
        Exit Sub
    End If
    
    If Not ComprobarExistenAdjudicaciones() Then
        If Me.Enabled And Me.Visible Then sdbgPrecios.SetFocus
        Cancel = True
        Exit Sub
    End If
    
''Comprobacion de atributos de oferta obligatorios
    If m_oProcesoSeleccionado.AtributosOfeObl And bTienePrecio Then
        For i = 1 To sdbgPrecios.Cols - 1
            If Left(sdbgPrecios.Columns(i).Name, 2) = "AT" And sdbgPrecios.Columns(i).TagVariant = "1" Then
                If sdbgPrecios.Columns(i).Value = "" Then
                    oMensajes.AtributosOblSinRellenar
                    Cancel = True
                    Exit Sub
                End If
            End If
        Next i

    End If
    
    bCambioAtrib = False
    bCambioPrecio = False
    
    'Comprobamos si ha habido cambio en el valor de los atributos de oferta (de los aplicados).
    For i = 19 To sdbgPrecios.Cols - 1
        If Left(sdbgPrecios.Columns(i).Name, 2) = "AT" Then
            lIdA = CLng(Right(sdbgPrecios.Columns(i).Name, Len(sdbgPrecios.Columns(i).Name) - 3))
            sCod = CStr(sdbgPrecios.Columns("ITEM").Value) & "$" & CStr(lIdA)
            If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                If Not g_oLineaEnEdicion.AtribOfertados.Item(sCod) Is Nothing Then
                    vValorNumAntiguo = g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorNum
                End If
            End If
            If sdbgPrecios.Columns(i).Value <> "" Then
                If Not g_oLineaEnEdicion.AtribOfertados Is Nothing Then
                    If Not g_oLineaEnEdicion.AtribOfertados.Item(sCod) Is Nothing Then
                        Select Case g_oLineaEnEdicion.AtribOfertados.Item(sCod).objeto.Tipo
                        Case TiposDeAtributos.TipoNumerico
                            'Si ha habido cambio en el valornum y se tratra de un atributo aplicado habr�a que recalcular la oferta.
                            If VarToDbl0(vValorNumAntiguo) <> StrToDbl0(sdbgPrecios.Columns(i).Value) And _
                                Not IsNull(m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdA)).PrecioAplicarA) And _
                                m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdA)).UsarPrec = 1 Then
                                    bCambioAtrib = True
                                    Exit For
                            End If
                        End Select
                    End If
                End If
            End If
        End If
    Next i
    
    'Comprobamos si ha cambiado en el precio que se usa.
    If g_oLineaEnEdicion.Usar <> Mid(sdbgPrecios.Columns(18).Text, 8, 1) Then bCambioPrecio = True
            
    'Comprobamos si ha habido cambio en el valor del precio.
    Select Case g_oLineaEnEdicion.Usar
    Case 1
        If NullToStr(g_oLineaEnEdicion.Precio) <> sdbgPrecios.Columns(10).Text Then
            bCambioPrecio = True
            g_oLineaEnEdicion.PrecioOld = g_oLineaEnEdicion.Precio
        End If
    Case 2
        If NullToStr(g_oLineaEnEdicion.Precio2) <> sdbgPrecios.Columns(12).Text Then
            bCambioPrecio = True
            g_oLineaEnEdicion.PrecioOld = g_oLineaEnEdicion.Precio2
        End If
    Case 3
        If NullToStr(g_oLineaEnEdicion.Precio3) <> sdbgPrecios.Columns(14).Text Then
            bCambioPrecio = True
            g_oLineaEnEdicion.PrecioOld = g_oLineaEnEdicion.Precio3
        End If
    End Select
        
    'Le avisamos de que ha cambiado el valor de los atributos aplicados (solo para ofertas del portal)
    If bCambioAtrib And g_oOfertaSeleccionada.portal = True Then
        irespuesta = oMensajes.AvisoConConfirmacion(m_sMensajeModifAtrib)
        If irespuesta = vbNo Then
            bCambioAtrib = False
            Cancel = True
            Exit Sub
        End If
    End If
        
    'Le avisamos de que ha cambiado el precio de los items (solo para ofertas del portal)
    If bCambioPrecio And g_oOfertaSeleccionada.portal = True Then
        irespuesta = oMensajes.AvisoConConfirmacion(m_sMensajeModifPrecio)
        If irespuesta = vbNo Then
            bCambioPrecio = False
            Cancel = True
            Exit Sub
        End If
    End If
   
    'Si ha cambiado el valor del atributo aplicado, o el valor del precio o el precio que se usa se recalcula la oferta.
    If bCambioPrecio Or bCambioAtrib Then
        g_oOfertaSeleccionada.Recalcular = True
        Set g_oOfertaSeleccionada.proceso = m_oProcesoSeleccionado
    End If
End Sub

''' <summary>
''' En el grid de items hay varios datos accesibles mediante botones. Este procedimiento controla si se puede acceder
''  a esos datos, si son accesibles los muestra y actualiza la oferta.
''' </summary>
''' <returns>Nada, es una llamada de sistema</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbgPreciosEscalados_BtnClick()
    Dim sObs As String
    Dim i As Integer
    Dim sGrupo As String
    Dim bSincronizar As Boolean
    Dim teserror As TipoErrorSummit
    Dim bModificar As Boolean

    If sdbgPreciosEscalados.col = -1 Then Exit Sub
       
    If sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name = "C#_ADJ" Then
        'ADJUNTOS DEL ITEM:
        If sdbgPreciosEscalados.Columns("CERRADO").Value = "0" Then
            bModificar = PermisoModificarOferta
            If bModificar = True Then
                If bModoEdicion Then
                     sdbgPreciosEscalados.Update
                     DoEvents
                     
                     If bModError Then
                         Exit Sub
                     End If
                 End If
                'Comprobamos que se pueda editar la oferta
                teserror = g_oOfertaSeleccionada.OfertaModificable
                If teserror.NumError <> TESnoerror Then
                    frmOFERecAdj.g_bModif = False
                Else
                    frmOFERecAdj.g_bModif = True
                End If
            Else
                frmOFERecAdj.g_bModif = False
            End If
        Else
            frmOFERecAdj.g_bModif = False
        End If
                        
        If Not sdbgPreciosEscalados.IsAddRow Then
            If g_oGrupoOferSeleccionado Is Nothing Then
                Set frmOFERecAdj.g_oPrecio = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
            Else
                Set frmOFERecAdj.g_oPrecio = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
            End If
            If frmOFERecAdj.g_oPrecio Is Nothing Then Exit Sub
            frmOFERecAdj.g_oPrecio.CargarAdjuntos

            'Configurar parametros del commondialog
            frmOFERecAdj.cmmdAdjun.FLAGS = cdlOFNHideReadOnly
            
            frmOFERecAdj.g_bRespetarCombo = True
            frmOFERecAdj.txtProceEsp = NullToStr(frmOFERecAdj.g_oPrecio.ObsAdjun)
            frmOFERecAdj.g_bRespetarCombo = False
            frmOFERecAdj.AnyadirEspsALista
            
            frmOFERecAdj.Show 1
            If m_oProcesoSeleccionado Is Nothing Then Exit Sub
            
            sdbgPreciosEscalados.Refresh
            'Se sincronizan los cambios entre la oferta y los grupos
            If sdbcGrupo.Text = m_sIdiTodosItems Then
                sGrupo = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).grupoCod
                If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas Is Nothing Then
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                ElseIf g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Count = 0 Then
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                End If
                
                If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                
                With g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
                    Set .Adjuntos = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).Adjuntos
                    .ObsAdjun = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).ObsAdjun
                    .NumAdjuns = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).NumAdjuns
                End With
            Else
                If g_oOfertaSeleccionada.Lineas Is Nothing Then Exit Sub
                If g_oOfertaSeleccionada.Lineas.Count = 0 Then Exit Sub
                If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)) Is Nothing Then
                    g_oOfertaSeleccionada.proceso.CargarAtributos ambito:=AmbItem, vGrupo:="#"
                    If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                End If
                
                With g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
                    Set .Adjuntos = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).Adjuntos
                    .ObsAdjun = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).ObsAdjun
                    .NumAdjuns = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).NumAdjuns
                End With
            End If
        End If
    Else
        Dim bModif As Boolean
        
        If sdbgPreciosEscalados.Columns("CERRADO").Value = "0" Then
            bModificar = PermisoModificarOferta
            If bModificar = True Then
                If g_oLineaEnEdicion Is Nothing Then
                    If g_oGrupoOferSeleccionado Is Nothing Then
                        Set g_oLineaEnEdicion = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
                    Else
                        Set g_oLineaEnEdicion = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
                    End If
                End If
                g_sPrec1Obs = g_oLineaEnEdicion.Precio1Obs
                
                'Comprobamos que se pueda editar la oferta
                teserror = g_oOfertaSeleccionada.OfertaModificable
                If teserror.NumError <> TESnoerror Then
                    bSincronizar = False
                    bModif = False
                Else
                    bSincronizar = True
                    bModif = True
                End If

            Else
                bSincronizar = False
                bModif = False
            End If
        Else
            bSincronizar = False
            bModif = False
        End If
        sObs = g_sPrec1Obs
        i = 1
                      
        If MostrarFormOFERecObs(m_sIdiCaptionObs & "  " & i, cmdAceptar.caption, cmdCancelar.caption, sObs, bModif, "Prec1", (sdbcProceCod.Columns("INVI").Value = "True" Or sdbcProceDen.Columns("INVI").Value = "True")) Then
            ActualizarObsLinea "Prec1", sObs
        End If
        
        If m_oProcesoSeleccionado Is Nothing Then Exit Sub
        sdbgPreciosEscalados.Refresh
            
        If bSincronizar And sdbgPreciosEscalados.DataChanged = False Then
            'Se sincronizan los cambios entre la oferta y los grupos
            If sdbcGrupo.Text = m_sIdiTodosItems Then
                sGrupo = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).grupoCod
                If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas Is Nothing Then
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                ElseIf g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Count = 0 Then
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                End If
                If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                
                With g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
                    .Precio1Obs = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).Precio1Obs
                End With
            Else
                If g_oOfertaSeleccionada.Lineas Is Nothing Then Exit Sub
                If g_oOfertaSeleccionada.Lineas.Count = 0 Then Exit Sub
                If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)) Is Nothing Then
                    g_oOfertaSeleccionada.proceso.CargarAtributos ambito:=AmbItem, vGrupo:="#"
                    g_oOfertaSeleccionada.CargarPrecios
                    If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                End If
                
                With g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
                    .Precio1Obs = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value)).Precio1Obs
                End With
            End If
        End If
    End If
End Sub

''' <summary>Actualiza las observaciones de la l�nea</summary>
''' <param name="sOrigen">Origen</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub ActualizarObsLinea(ByVal sOrigen As String, ByVal sObs As String)
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    
    Select Case sOrigen
        Case "Prec1"
            g_sPrec1Obs = sObs
        Case "Prec2"
            g_sPrec2Obs = sObs
        Case "Prec3"
            g_sPrec3Obs = sObs
    End Select
    
    If Not g_oLineaEnEdicion Is Nothing Then
        g_oLineaEnEdicion.Precio1Obs = g_sPrec1Obs
        g_oLineaEnEdicion.Precio2Obs = g_sPrec2Obs
        g_oLineaEnEdicion.Precio3Obs = g_sPrec3Obs
    
        If Not sdbgPrecios.DataChanged Then
            Set oIBaseDatos = g_oLineaEnEdicion
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Set oIBaseDatos = Nothing
            End If
            Set g_oLineaEnEdicion = Nothing
        End If
    End If
End Sub

''' <summary>
''' En el grid de items hay varios datos accesibles mediante botones. Este procedimiento controla si se puede acceder
''  a esos datos, si son accesibles los muestra y actualiza la oferta.
''' </summary>
''' <returns>Nada, es una llamada de sistema</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub sdbgPrecios_BtnClick()
Dim sObs As String
Dim i As Integer
Dim sGrupo As String
Dim bSincronizar As Boolean
Dim teserror As TipoErrorSummit
Dim bModificar As Boolean
Dim bModif As Boolean
Dim sOrigen As String

If sdbgPrecios.col = -1 Then Exit Sub
   
Select Case sdbgPrecios.Columns(sdbgPrecios.col).Name
    Case "ANYOIMPUTACION"
        If sdbcGrupo.Text = m_sIdiTodosItems Then
            sGrupo = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).grupoCod
        Else
            sGrupo = sdbcGrupo.Columns(0).Value
        End If
        If m_oProcesoSeleccionado.Grupos.Item(sGrupo).Items.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).SolicitudId <> "" Then
            Dim IdSolicitud As Long
            IdSolicitud = m_oProcesoSeleccionado.Grupos.Item(sGrupo).Items.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).SolicitudId
            'Detalle de la solicitud
            Dim ofrmDetalleSolic As frmSolicitudDetalle
            Dim oInstancias As CInstancias
            Set oInstancias = oFSGSRaiz.Generar_CInstancias
            
            Dim vEstado(10) As Variant
            vEstado(1) = EstadoSolicitud.GenEnviada
            vEstado(2) = EstadoSolicitud.GenPendiente
            vEstado(3) = EstadoSolicitud.GenRechazada
            vEstado(4) = EstadoSolicitud.GenAprobada
             vEstado(5) = EstadoSolicitud.GenAnulada
            vEstado(6) = EstadoSolicitud.Pendiente
            vEstado(7) = EstadoSolicitud.Aprobada
            vEstado(8) = EstadoSolicitud.Rechazada
            vEstado(9) = EstadoSolicitud.Anulada
            vEstado(10) = EstadoSolicitud.Cerrada
            oInstancias.BuscarSolicitudes OrdSolicPorFecNec, , IdSolicitud, , , vEstado
                                                             
            If oInstancias.Count = 0 Then
                oMensajes.DatoEliminado sdbgPrecios.Columns(sdbgPrecios.col).caption
            Else
                
                Set ofrmDetalleSolic = New frmSolicitudDetalle

                ofrmDetalleSolic.g_sOrigen = "frmPROCE"
                Set ofrmDetalleSolic.g_oSolicitudSeleccionada = oInstancias.Item(1)

                Screen.MousePointer = vbHourglass
                MDI.MostrarFormulario ofrmDetalleSolic
                Screen.MousePointer = vbNormal
            End If
            Set oInstancias = Nothing
        End If
    Case "C#_ADJ"
        'ADJUNTOS DEL ITEM:
        If sdbgPrecios.Columns("CERRADO").Value = "0" Then
            bModificar = PermisoModificarOferta
            If bModificar = True Then
                If bModoEdicion Then
                     sdbgPrecios.Update
                     DoEvents
                     
                     If bModError Then
                         Exit Sub
                     End If
                 End If
                'Comprobamos que se pueda editar la oferta
                teserror = g_oOfertaSeleccionada.OfertaModificable
                If teserror.NumError <> TESnoerror Then
                    frmOFERecAdj.g_bModif = False
                Else
                    frmOFERecAdj.g_bModif = True
                End If
            Else
                frmOFERecAdj.g_bModif = False
            End If
        Else
            frmOFERecAdj.g_bModif = False
        End If
                        
        If Not sdbgPrecios.IsAddRow Then
            If g_oGrupoOferSeleccionado Is Nothing Then
                Set frmOFERecAdj.g_oPrecio = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
            Else
                Set frmOFERecAdj.g_oPrecio = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
            End If
            If frmOFERecAdj.g_oPrecio Is Nothing Then Exit Sub
            frmOFERecAdj.g_oPrecio.CargarAdjuntos

            'Configurar parametros del commondialog
            frmOFERecAdj.cmmdAdjun.FLAGS = cdlOFNHideReadOnly
            
            frmOFERecAdj.g_bRespetarCombo = True
            frmOFERecAdj.txtProceEsp = NullToStr(frmOFERecAdj.g_oPrecio.ObsAdjun)
            frmOFERecAdj.g_bRespetarCombo = False
            frmOFERecAdj.AnyadirEspsALista
            
            frmOFERecAdj.Show 1
            If m_oProcesoSeleccionado Is Nothing Then Exit Sub
            
            sdbgPrecios.Refresh
            'Se sincronizan los cambios entre la oferta y los grupos
            If sdbcGrupo.Text = m_sIdiTodosItems Then
                sGrupo = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).grupoCod
                If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas Is Nothing Then
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                ElseIf g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Count = 0 Then
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                    g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                End If
                If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                
                With g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
                    Set .Adjuntos = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Adjuntos
                    .ObsAdjun = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).ObsAdjun
                    .NumAdjuns = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).NumAdjuns
                End With
            Else
                If g_oOfertaSeleccionada.Lineas Is Nothing Then Exit Sub
                If g_oOfertaSeleccionada.Lineas.Count = 0 Then Exit Sub
                If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)) Is Nothing Then
                    g_oOfertaSeleccionada.proceso.CargarAtributos ambito:=AmbItem, vGrupo:="#"
                    g_oOfertaSeleccionada.CargarPrecios
                    If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                End If
                
                With g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
                    Set .Adjuntos = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Adjuntos
                    .ObsAdjun = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).ObsAdjun
                    .NumAdjuns = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).NumAdjuns
                End With
            End If
        End If
    Case Else
        If Left(sdbgPrecios.Columns(sdbgPrecios.col).Name, 3) = "AT_" Then
            'Columnas de atributos
            frmATRIBDescr.g_bEdicion = bModoEdicion
            frmATRIBDescr.g_sOrigen = "frmOFERec_Item"
            frmATRIBDescr.caption = sdbgPrecios.Columns(sdbgPrecios.col).Value
            frmATRIBDescr.txtDescr.Text = NullToStr(sdbgPrecios.Columns(sdbgPrecios.col).Value)
            frmATRIBDescr.Show vbModal
        Else
            If sdbgPrecios.Columns("CERRADO").Value = "0" Then
                bModificar = PermisoModificarOferta
                If bModificar = True Then
                    If g_oLineaEnEdicion Is Nothing Then
                        If g_oGrupoOferSeleccionado Is Nothing Then
                            Set g_oLineaEnEdicion = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
                        Else
                            Set g_oLineaEnEdicion = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
                        End If
                    End If
                    g_sPrec1Obs = g_oLineaEnEdicion.Precio1Obs
                    g_sPrec2Obs = g_oLineaEnEdicion.Precio2Obs
                    g_sPrec3Obs = g_oLineaEnEdicion.Precio3Obs
                    
                    'Comprobamos que se pueda editar la oferta
                    teserror = g_oOfertaSeleccionada.OfertaModificable
                    If teserror.NumError <> TESnoerror Then
                        bSincronizar = False
                        bModif = False
                    Else
                        bSincronizar = True
                        bModif = True
                    End If
                Else
                    bSincronizar = False
                    bModif = False
                End If
            Else
                bSincronizar = False
                bModif = False
            End If
            Select Case sdbgPrecios.Columns(sdbgPrecios.col).Name
                Case "COMENT1"
                    sOrigen = "Prec1"
                    sObs = g_sPrec1Obs
                    i = 1
                    
                Case "COMENT2"
                    sOrigen = "Prec2"
                    sObs = g_sPrec2Obs
                    i = 2
                    
                Case "COMENT3"
                    sOrigen = "Prec3"
                    sObs = g_sPrec3Obs
                    i = 3
            End Select
            
            If MostrarFormOFERecObs(m_sIdiCaptionObs & "  " & i, cmdAceptar.caption, cmdCancelar.caption, sObs, bModif, sOrigen, (sdbcProceCod.Columns("INVI").Value = "True" Or sdbcProceDen.Columns("INVI").Value = "True")) Then
                ActualizarObsLinea sOrigen, sObs
            End If
            
            If Not sdbgPrecios.DataChanged Then Set g_oLineaEnEdicion = Nothing
            
            If m_oProcesoSeleccionado Is Nothing Then Exit Sub
            sdbgPrecios.Refresh
                
            If bSincronizar And sdbgPrecios.DataChanged = False Then
                'Se sincronizan los cambios entre la oferta y los grupos
                If sdbcGrupo.Text = m_sIdiTodosItems Then
                    sGrupo = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).grupoCod
                    If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas Is Nothing Then
                        g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                        g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                    ElseIf g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Count = 0 Then
                        g_oOfertaSeleccionada.Grupos.Item(sGrupo).Grupo.CargarAtributos ambito:=AmbItem
                        g_oOfertaSeleccionada.Grupos.Item(sGrupo).CargarPrecios
                    End If
                    If g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                    
                    With g_oOfertaSeleccionada.Grupos.Item(sGrupo).Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
                        .Precio1Obs = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio1Obs
                        .Precio2Obs = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio2Obs
                        .Precio3Obs = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio3Obs
                    End With
                Else
                    If g_oOfertaSeleccionada.Lineas Is Nothing Then Exit Sub
                    If g_oOfertaSeleccionada.Lineas.Count = 0 Then Exit Sub
                    If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)) Is Nothing Then
                        g_oOfertaSeleccionada.proceso.CargarAtributos ambito:=AmbItem, vGrupo:="#"
                        g_oOfertaSeleccionada.CargarPrecios
                        If g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)) Is Nothing Then Exit Sub
                    End If
                    
                    With g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
                        .Precio1Obs = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio1Obs
                        .Precio2Obs = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio2Obs
                        .Precio3Obs = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio3Obs
                    End With
                End If
            End If
        End If
End Select
End Sub

Private Sub sdbgPreciosEscalados_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
   If g_oOfertaSeleccionada Is Nothing Then Exit Sub
    If bModoEdicion Then
        DoEvents
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If sdbgPreciosEscalados.Columns("CERRADO").Value = "1" Then
                oMensajes.ImposibleModItem
                sdbgPreciosEscalados.DataChanged = False
                Exit Sub
            End If
        End If
        
        Set g_oLineaEnEdicion = Nothing

        If sdbcGrupo.Text = m_sIdiTodosItems Then
            Set g_oLineaEnEdicion = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
        Else
            If g_oGrupoOferSeleccionado Is Nothing Then Exit Sub
            Set g_oLineaEnEdicion = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPreciosEscalados.Columns("ITEM").Value))
        End If
            
            
        Accion = ACCRecOfeModPrec
        cmdDeshacer.Enabled = True

    End If

End Sub


Private Sub sdbgPrecios_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
   If g_oOfertaSeleccionada Is Nothing Then Exit Sub
    If bModoEdicion Then
        DoEvents
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If sdbgPrecios.Columns("CERRADO").Value = "1" Then
                oMensajes.ImposibleModItem
                sdbgPrecios.DataChanged = False
                Exit Sub
            End If
        End If
        
        Set g_oLineaEnEdicion = Nothing

        If sdbcGrupo.Text = m_sIdiTodosItems Then
            Set g_oLineaEnEdicion = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
        Else
            If g_oGrupoOferSeleccionado Is Nothing Then Exit Sub
            Set g_oLineaEnEdicion = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value))
        End If
            
            
        Accion = ACCRecOfeModPrec
        cmdDeshacer.Enabled = True

    End If
End Sub

Private Sub sdbgPreciosEscalados_ComboDropDown()
    If sdbgPreciosEscalados.Columns("CERRADO").Value = "1" Then sdbgPreciosEscalados.ComboDroppedDown = False
End Sub

Private Sub sdbgPrecios_ComboDropDown()
    If sdbgPrecios.Columns("CERRADO").Value = "1" Then
        sdbgPrecios.ComboDroppedDown = False
    End If
End Sub

Private Sub sdbgPreciosEscalados_DblClick()
    If sdbgPreciosEscalados.Row < 0 Then Exit Sub
    If Not bModoEdicion Then MostrarDetalleItemEscalados
End Sub

Private Sub sdbgPrecios_DblClick()
    If sdbgPrecios.Row < 0 Then Exit Sub
    If Not bModoEdicion Then MostrarDetalleItem
End Sub

Private Sub sdbgPreciosEscalados_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
    Dim i As Integer
  
    'Comprobar que es un grupo de escalados
    With sdbgPreciosEscalados
        If GrpIndex > 0 And GrpIndex < .Groups.Count - 1 Then
            If Not m_oGrupoSeleccionado.Escalados.Item(CStr(.Groups(GrpIndex).TagVariant)) Is Nothing Then
                'Se redimensionan los grupos de escalados
                For i = 1 To m_oGrupoSeleccionado.Escalados.Count
                    If i <> GrpIndex Then
                        .Groups(i).Width = .ResizeWidth
                    End If
                Next i
            End If
        End If
    End With
End Sub

Private Sub sdbgPreciosEscalados_HeadClick(ByVal ColIndex As Integer)
Dim sHeadName As String
Dim oPreciosAOrdenar As CPrecioItems

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub
  
    If bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadName = sdbgPreciosEscalados.Columns(ColIndex).Name
    
    ''' Llamamos a la funci�n que ordena la coleccion de precios
    
    If g_oGrupoOferSeleccionado Is Nothing Then
        Set oPreciosAOrdenar = g_oOfertaSeleccionada.Lineas
        Set g_oOfertaSeleccionada.Lineas = OrdenarGridPrecios(sHeadName, oPreciosAOrdenar)
    Else
        Set oPreciosAOrdenar = g_oGrupoOferSeleccionado.Lineas
        Set g_oGrupoOferSeleccionado.Lineas = OrdenarGridPrecios(sHeadName, oPreciosAOrdenar)
    End If
      
    sdbgPreciosEscalados.RemoveAll

    Rellenar_sdbgPreciosEscalados

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgPrecios_HeadClick(ByVal ColIndex As Integer)
    Dim sHeadName As String
    Dim oPreciosAOrdenar As CPrecioItems

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub
  
    If bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadName = sdbgPrecios.Columns(ColIndex).Name
    
    ''' Llamamos a la funci�n que ordena la coleccion de precios
    
    If g_oGrupoOferSeleccionado Is Nothing Then
        Set oPreciosAOrdenar = g_oOfertaSeleccionada.Lineas
        Set g_oOfertaSeleccionada.Lineas = OrdenarGridPrecios(sHeadName, oPreciosAOrdenar)
    Else
        Set oPreciosAOrdenar = g_oGrupoOferSeleccionado.Lineas
        Set g_oGrupoOferSeleccionado.Lineas = OrdenarGridPrecios(sHeadName, oPreciosAOrdenar)
    End If
      
    sdbgPrecios.RemoveAll
    Rellenar_sdbgPrecios

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgPreciosEscalados_KeyPress(KeyAscii As Integer)

    If KeyAscii = vbKeyEscape Then

        If sdbgPreciosEscalados.DataChanged = False Then

            sdbgPreciosEscalados.CancelUpdate
            sdbgPreciosEscalados.DataChanged = False
            
            Set g_oLineaEnEdicion = Nothing
            
            cmdDeshacer.Enabled = False
            Accion = ACCRecOfeCon
        End If
    End If
    
    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        If Left(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name, 6) = "COMENT" Or sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name = "C#_ADJ" Then
            sdbgPreciosEscalados_BtnClick
        End If
    End If
End Sub

Private Sub sdbgPrecios_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        If sdbgPrecios.DataChanged = False Then
            sdbgPrecios.CancelUpdate
            sdbgPrecios.DataChanged = False
            
            Set g_oLineaEnEdicion = Nothing
            
            cmdDeshacer.Enabled = False
            Accion = ACCRecOfeCon
        End If
    End If
    
    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        If Left(sdbgPrecios.Columns(sdbgPrecios.col).Name, 6) = "COMENT" Or sdbgPrecios.Columns(sdbgPrecios.col).Name = "C#_ADJ" Then
            sdbgPrecios_BtnClick
        End If
    End If
End Sub

Private Sub sdbgPreciosEscalados_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bMouse Then Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgPrecios_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If m_bMouse Then Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgPreciosEscalados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim oatrib As CAtributo
Dim lIdAtrib As Long

If bModoEdicion Then
    If LastCol <> -1 Then '
        If Left(sdbgPreciosEscalados.Columns(LastCol).Name, 3) = "AT_" Then
            If Not ComprobarValorAtributo(LastCol, sdbgPreciosEscalados) Then
                If Me.Enabled And Me.Visible Then sdbgPreciosEscalados.SetFocus
                sdbgPreciosEscalados.col = LastCol
                Exit Sub
            End If
        ElseIf Left(sdbgPreciosEscalados.Columns(LastCol).Name, 4) = "ATE_" Then
            If Not ComprobarValorAtributo(LastCol, sdbgPreciosEscalados) Then
                If Me.Enabled And Me.Visible Then sdbgPreciosEscalados.SetFocus
                sdbgPreciosEscalados.col = LastCol
                Exit Sub
            End If
        End If
    End If
    If sdbgPreciosEscalados.col >= 0 Then
        If Left(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name, 3) = "AT_" Then
            lIdAtrib = Right(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name, Len(sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).Name) - 3)
            If sdbcGrupo.Text = m_sIdiTodosItems Then
                Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
            Else
                Set oatrib = m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
            End If
            If Not oatrib Is Nothing Then
                If oatrib.TipoIntroduccion = Introselec Then
                    sdbddCombo.RemoveAll
                    sdbddCombo.AddItem ""
                    If oatrib.Tipo = TipoNumerico Then
                        sdbddCombo.Columns(0).Alignment = ssCaptionAlignmentRight
                    Else
                        sdbddCombo.Columns(0).Alignment = ssCaptionAlignmentLeft
                    End If
                    sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).DropDownHwnd = sdbddCombo.hWnd
                    sdbddCombo.Enabled = True
                End If
                If oatrib.TipoIntroduccion <> Introselec Then
                    If oatrib.Tipo = TipoBoolean Then
                        sdbddCombo.RemoveAll
                        sdbddCombo.AddItem ""
                        sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).DropDownHwnd = sdbddCombo.hWnd
                        sdbddCombo.Enabled = True
                    Else
                        sdbgPreciosEscalados.Columns(sdbgPreciosEscalados.col).DropDownHwnd = 0
                        sdbddCombo.Enabled = False
                    End If
                End If
            End If
        End If
    End If
End If
End Sub

Private Sub sdbgPrecios_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim oatrib As CAtributo
Dim lIdAtrib As Long

If bModoEdicion Then
    If Not ComprobarValorAtributo(LastCol, sdbgPrecios) Then
        If Me.Enabled And Me.Visible Then sdbgPrecios.SetFocus
        sdbgPrecios.col = LastCol
        Exit Sub
    End If
End If

If sdbgPrecios.col >= 0 Then
    If Left(sdbgPrecios.Columns(sdbgPrecios.col).Name, 3) = "AT_" Then
        lIdAtrib = Right(sdbgPrecios.Columns(sdbgPrecios.col).Name, Len(sdbgPrecios.Columns(sdbgPrecios.col).Name) - 3)
        If sdbcGrupo.Text = m_sIdiTodosItems Then
            Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
        Else
            Set oatrib = m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
        End If
        If Not oatrib Is Nothing Then
            If oatrib.TipoIntroduccion = Introselec And bModoEdicion Then
                sdbddCombo.RemoveAll
                sdbddCombo.AddItem ""
                If oatrib.Tipo = TipoNumerico Then
                    sdbddCombo.Columns(0).Alignment = ssCaptionAlignmentRight
                Else
                    sdbddCombo.Columns(0).Alignment = ssCaptionAlignmentLeft
                End If
                sdbgPrecios.Columns(sdbgPrecios.col).DropDownHwnd = sdbddCombo.hWnd
                sdbddCombo.Enabled = True
            End If
            If oatrib.TipoIntroduccion <> Introselec Then
                Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                        sdbgPrecios.Columns(sdbgPrecios.col).Locked = Not bModoEdicion
                        sdbgPrecios.Columns(sdbgPrecios.col).Style = ssStyleEditButton
                        sdbgPrecios.Columns(sdbgPrecios.col).Alignment = ssCaptionAlignmentLeft
                    Case TipoBoolean
                        If bModoEdicion Then
                            sdbddCombo.RemoveAll
                            sdbddCombo.AddItem ""
                            sdbgPrecios.Columns(sdbgPrecios.col).DropDownHwnd = sdbddCombo.hWnd
                            sdbddCombo.Enabled = True
                        End If
                    Case Else
                        If bModoEdicion Then
                            sdbgPrecios.Columns(sdbgPrecios.col).DropDownHwnd = 0
                            sdbddCombo.Enabled = False
                        End If
                End Select
            End If
        End If
    End If
End If

If g_oGrupoOferSeleccionado Is Nothing Then
    g_sPrec1Obs = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio1Obs
    g_sPrec2Obs = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio2Obs
    g_sPrec3Obs = g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio3Obs
Else
    g_sPrec1Obs = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio1Obs
    g_sPrec2Obs = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio2Obs
    g_sPrec3Obs = g_oGrupoOferSeleccionado.Lineas.Item(CStr(sdbgPrecios.Columns("ITEM").Value)).Precio3Obs
End If
End Sub

Private Sub sdbgPreciosEscalados_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    Dim oLinea As CPrecioItem

    If sdbgPreciosEscalados.Columns("CERRADO").Value = "1" Then 'CERRADO
        For i = 0 To sdbgPreciosEscalados.Cols - 1
            If sdbgPreciosEscalados.Columns(i).Name <> "C#_ADJ" Then
                sdbgPreciosEscalados.Columns(i).CellStyleSet "ITEMCERRADO"
            End If
        Next i
    End If

    If m_oProcesoSeleccionado.AdjuntarAItems Then
        If Not g_oGrupoOferSeleccionado Is Nothing Then
            If g_oGrupoOferSeleccionado.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value).NumAdjuns > 0 Or _
               (Not IsEmpty(g_oGrupoOferSeleccionado.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value).ObsAdjun) And _
               Not IsNull(g_oGrupoOferSeleccionado.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value).ObsAdjun)) Then
                sdbgPreciosEscalados.Columns("C#_ADJ").CellStyleSet "styEspAdjSi"
            Else
                sdbgPreciosEscalados.Columns("C#_ADJ").CellStyleSet ""
            End If
        Else
            If g_oOfertaSeleccionada.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value).NumAdjuns > 0 Or _
               (Not IsEmpty(g_oOfertaSeleccionada.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value).ObsAdjun) And _
               Not IsNull(g_oOfertaSeleccionada.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value).ObsAdjun)) Then
                sdbgPreciosEscalados.Columns("C#_ADJ").CellStyleSet "styEspAdjSi"
            Else
                sdbgPreciosEscalados.Columns("C#_ADJ").CellStyleSet ""
            End If
        End If
    End If

    If Not g_oGrupoOferSeleccionado Is Nothing Then
        Set oLinea = g_oGrupoOferSeleccionado.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value)
    Else
        Set oLinea = g_oOfertaSeleccionada.Lineas.Item(sdbgPreciosEscalados.Columns("ITEM").Value)
    End If
    
    If Not oLinea Is Nothing Then
        If oLinea.Precio1Obs <> "" Then
            sdbgPreciosEscalados.Columns("COMENT").CellStyleSet "Concoment"
        Else
            sdbgPreciosEscalados.Columns("COMENT").CellStyleSet ""
        End If
    End If
End Sub

Private Sub sdbgPrecios_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    Dim oLinea As CPrecioItem

    If sdbgPrecios.Columns(17).Value = "1" Then 'CERRADO
        For i = 0 To sdbgPrecios.Cols - 1
            If sdbgPrecios.Columns(i).Name <> "C#_ADJ" Then
                sdbgPrecios.Columns(i).CellStyleSet "ITEMCERRADO"
            End If
        Next i
    End If
    
    If m_oProcesoSeleccionado.AdjuntarAItems Then
        If Not g_oGrupoOferSeleccionado Is Nothing Then
            If g_oGrupoOferSeleccionado.Lineas.Item(sdbgPrecios.Columns("ITEM").Value).NumAdjuns > 0 Or _
               (Not IsEmpty(g_oGrupoOferSeleccionado.Lineas.Item(sdbgPrecios.Columns("ITEM").Value).ObsAdjun) And _
               Not IsNull(g_oGrupoOferSeleccionado.Lineas.Item(sdbgPrecios.Columns("ITEM").Value).ObsAdjun)) Then
                sdbgPrecios.Columns("C#_ADJ").CellStyleSet "styEspAdjSi"
            Else
                sdbgPrecios.Columns("C#_ADJ").CellStyleSet ""
            End If
        Else
            If g_oOfertaSeleccionada.Lineas.Item(sdbgPrecios.Columns("ITEM").Value).NumAdjuns > 0 Or _
               (Not IsEmpty(g_oOfertaSeleccionada.Lineas.Item(sdbgPrecios.Columns("ITEM").Value).ObsAdjun) And _
               Not IsNull(g_oOfertaSeleccionada.Lineas.Item(sdbgPrecios.Columns("ITEM").Value).ObsAdjun)) Then
                sdbgPrecios.Columns("C#_ADJ").CellStyleSet "styEspAdjSi"
            Else
                sdbgPrecios.Columns("C#_ADJ").CellStyleSet ""
            End If
        End If
    End If
    
    If Not g_oGrupoOferSeleccionado Is Nothing Then
        Set oLinea = g_oGrupoOferSeleccionado.Lineas.Item(sdbgPrecios.Columns("ITEM").Value)
    Else
        Set oLinea = g_oOfertaSeleccionada.Lineas.Item(sdbgPrecios.Columns("ITEM").Value)
    End If
    If Not oLinea Is Nothing Then
        If oLinea.Precio1Obs <> "" Then
            sdbgPrecios.Columns("COMENT1").CellStyleSet "Concoment"
        Else
            sdbgPrecios.Columns("COMENT1").CellStyleSet ""
        End If
                
        If oLinea.Precio2Obs <> "" Then
            sdbgPrecios.Columns("COMENT2").CellStyleSet "Concoment"
        Else
            sdbgPrecios.Columns("COMENT2").CellStyleSet ""
        End If
                
        If oLinea.Precio3Obs <> "" Then
            sdbgPrecios.Columns("COMENT3").CellStyleSet "Concoment"
        Else
            sdbgPrecios.Columns("COMENT3").CellStyleSet ""
        End If
        Set oLinea = Nothing
    End If
End Sub

Private Sub MostrarItemsOfe()
Dim dblPresProce As Double
Dim bModificar As Boolean

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub
    
    g_oOfertaSeleccionada.MarcarComoLeida basOptimizacion.gCodPersonaUsuario
    
    If m_bGrupoConEscalados Then
        LimpiarPreciosEscalados
        ConfigurarGridItemsEscalados (0)
        dblPresProce = Rellenar_sdbgPreciosEscalados
    Else
        LimpiarPrecios
        ConfigurarGridItems (0)
        dblPresProce = Rellenar_sdbgPrecios
    End If
        
    If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
        If m_bGrupoConEscalados Then
            If sdbgPreciosEscalados.Rows = 0 Then
                ChkMostrarItems.Visible = False
            Else
                ChkMostrarItems.Visible = True
            End If
        Else
            If sdbgPrecios.Rows = 0 Then
                ChkMostrarItems.Visible = False
            Else
                ChkMostrarItems.Visible = True
            End If
        End If
    Else
        ChkMostrarItems.Visible = False
    End If
    
    sdbcGrupo.Text = m_sIdiTodosItems
    lblPresGrupo(0).Visible = False
    lblCPresGrupo(0).Visible = False
    lblPresGrupo(1).Visible = True
    lblCPresGrupo(1).Visible = True
    lblPresGrupo(1).caption = FormateoNumerico(dblPresProce)
    
    bModificar = PermisoModificarOferta
    If bModificar = True Then
        cmdModoEdicion.Visible = True
    Else
        cmdModoEdicion.Visible = False
    End If
    
    MostrarApartado iAptItems
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub MostrarDetalleItemEscalados()
Dim oAdj As CAdjDeArt
Dim oItem As CItem
Dim oDestinos As CDestinos
Dim oPagos As CPagos
Dim oUnidades As CUnidades
Dim oArticulo As CArticulo
Dim oAdjudicaciones As CAdjsDeArt


    If sdbgPreciosEscalados.Rows = 0 Then Exit Sub

    Screen.MousePointer = vbHourglass

    Set oItem = oFSGSRaiz.Generar_CItem

    Set oItem.proceso = m_oProcesoSeleccionado
    oItem.Id = sdbgPreciosEscalados.Columns("ITEM").Value
    Set frmItemDetalle.g_oItemSeleccionado = oItem

    If sdbgPreciosEscalados.Columns(0).Value <> "" Then
        frmItemDetalle.lblItem.caption = sdbgPreciosEscalados.Columns(0).Value & " - " & sdbgPreciosEscalados.Columns(1).Value
    Else
        frmItemDetalle.lblItem.caption = sdbgPreciosEscalados.Columns(1).Value
    End If

    frmItemDetalle.lblCodDest.caption = sdbgPreciosEscalados.Columns("DEST").Value
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    oDestinos.CargarTodosLosDestinos frmItemDetalle.lblCodDest.caption, , True, , , , , , , , True
    If oDestinos.Count <> 0 Then
        frmItemDetalle.lblDenDest.caption = oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblCodPag.caption = sdbgPreciosEscalados.Columns("PAG").Value
    Set oPagos = oFSGSRaiz.Generar_CPagos
    oPagos.CargarTodosLosPagos frmItemDetalle.lblCodPag.caption, , True
    If oPagos.Count <> 0 Then
        frmItemDetalle.lblDenPag.caption = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblCodUni.caption = sdbgPreciosEscalados.Columns("UNI").Value
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    oUnidades.CargarTodasLasUnidades frmItemDetalle.lblCodUni.caption, , True
    If oUnidades.Count <> 0 Then
        frmItemDetalle.lblDenUni.caption = oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblFecFin.caption = sdbgPreciosEscalados.Columns("INI").Value
    frmItemDetalle.lblFecIni.caption = sdbgPreciosEscalados.Columns("FIN").Value
    
    oItem.CargarEspGeneral
    frmItemDetalle.txtEsp.Text = NullToStr(oItem.esp)

    'Realizar la carga de las adjudicaciones anteriores
    'Comprobamos haber si existen adjudicaciones de summit

    If sdbgPreciosEscalados.Columns("COD").Value <> "" Then

        Set oArticulo = oFSGSRaiz.Generar_CArticulo

        oArticulo.Cod = sdbgPreciosEscalados.Columns("COD").Value
               
        Set oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt
        ' Las actuales
        oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, False, True

        For Each oAdj In oAdjudicaciones
           frmItemDetalle.sdbgAdjudicaciones.AddItem oAdj.Descr & Chr(m_lSeparador) & oAdj.ProveCod & Chr(m_lSeparador) & oAdj.Precio * m_oProcesoSeleccionado.Cambio * CDbl(g_oOfertaSeleccionada.Cambio) & Chr(m_lSeparador) & oAdj.Cantidad & Chr(m_lSeparador) & oAdj.Porcentaje & Chr(m_lSeparador) & oAdj.FechaInicioSuministro & Chr(m_lSeparador) & oAdj.FechaFinSuministro & Chr(m_lSeparador) & oAdj.DestCod & Chr(m_lSeparador) & oAdj.UniCod
        Next
    End If

    Screen.MousePointer = vbNormal
    frmItemDetalle.Show 1
    Unload frmItemDetalle
    Set oUnidades = Nothing
    Set oDestinos = Nothing
    Set oPagos = Nothing
    Set oAdjudicaciones = Nothing
    Set oItem = Nothing
    Set oArticulo = Nothing
End Sub

Private Sub MostrarDetalleItem()
Dim oAdj As CAdjDeArt
Dim oItem As CItem
Dim oDestinos As CDestinos
Dim oPagos As CPagos
Dim oUnidades As CUnidades
Dim oArticulo As CArticulo
Dim oAdjudicaciones As CAdjsDeArt

    If sdbgPrecios.Rows = 0 Then Exit Sub

    Screen.MousePointer = vbHourglass

    Set oItem = oFSGSRaiz.Generar_CItem

    Set oItem.proceso = m_oProcesoSeleccionado
    oItem.Id = sdbgPrecios.Columns("ITEM").Value
    oItem.ArticuloCod = sdbgPrecios.Columns("COD").Value
    Set frmItemDetalle.g_oItemSeleccionado = oItem
    
    If sdbgPrecios.Columns(0).Value <> "" Then
        frmItemDetalle.lblItem.caption = sdbgPrecios.Columns(0).Value & " - " & sdbgPrecios.Columns(1).Value '& " " & sIdiCantidad & ": " & DblToStr(g_oOfertaSeleccionada.Lineas.Item(CStr(sdbgPrecios.AddItemRowIndex(sdbgPrecios.Bookmark))).Cantidad)
    Else
        frmItemDetalle.lblItem.caption = sdbgPrecios.Columns(1).Value
    End If

    frmItemDetalle.lblCodDest.caption = sdbgPrecios.Columns("DEST").Value
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    oDestinos.CargarTodosLosDestinos frmItemDetalle.lblCodDest.caption, , True, , , , , , , , True
    If oDestinos.Count <> 0 Then
        frmItemDetalle.lblDenDest.caption = oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblCodPag.caption = sdbgPrecios.Columns("PAG").Value
    Set oPagos = oFSGSRaiz.Generar_CPagos
    oPagos.CargarTodosLosPagos frmItemDetalle.lblCodPag.caption, , True
    If oPagos.Count <> 0 Then
        frmItemDetalle.lblDenPag.caption = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblCodUni.caption = sdbgPrecios.Columns("UNI").Value
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    oUnidades.CargarTodasLasUnidades frmItemDetalle.lblCodUni.caption, , True
    If oUnidades.Count <> 0 Then
        frmItemDetalle.lblDenUni.caption = oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If

    frmItemDetalle.lblFecFin.caption = sdbgPrecios.Columns("INI").Value
    frmItemDetalle.lblFecIni.caption = sdbgPrecios.Columns("FIN").Value
    
    oItem.CargarEspGeneral
    frmItemDetalle.txtEsp.Text = NullToStr(oItem.esp)

    'Realizar la carga de las adjudicaciones anteriores
    'Comprobamos haber si existen adjudicaciones de summit
    If sdbgPrecios.Columns("COD").Value <> "" Then
        Set oArticulo = oFSGSRaiz.Generar_CArticulo
        oArticulo.Cod = sdbgPrecios.Columns("COD").Value
       
        Set oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt
        ' Las actuales
        oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, oArticulo, False, True

        For Each oAdj In oAdjudicaciones
           frmItemDetalle.sdbgAdjudicaciones.AddItem oAdj.Descr & Chr(m_lSeparador) & oAdj.ProveCod & Chr(m_lSeparador) & oAdj.Precio * m_oProcesoSeleccionado.Cambio * CDbl(g_oOfertaSeleccionada.Cambio) & Chr(m_lSeparador) & oAdj.Cantidad & Chr(m_lSeparador) & oAdj.Porcentaje & Chr(m_lSeparador) & oAdj.FechaInicioSuministro & Chr(m_lSeparador) & oAdj.FechaFinSuministro & Chr(m_lSeparador) & oAdj.DestCod & Chr(m_lSeparador) & oAdj.UniCod
        Next
    End If

    Screen.MousePointer = vbNormal
    frmItemDetalle.Show 1
    Unload frmItemDetalle
    Set oUnidades = Nothing
    Set oDestinos = Nothing
    Set oPagos = Nothing
    Set oAdjudicaciones = Nothing
    Set oItem = Nothing
    Set oArticulo = Nothing
End Sub

''' <summary>
''' Funci�n que carga los recursos de la ventana, etiquetas...
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 16/11/2011</revision>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(frm_oferec, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value '1
        m_sMeCaption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value
        Ador.MoveNext
        lblCProceCod.caption = Ador(0).Value '3
        m_sIdiProceso = Left(lblCProceCod.caption, Len(lblCProceCod.caption) - 1)
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value '4 Cod
        sdbcProceCod.Columns(0).caption = Ador(0).Value
        sdbcProceDen.Columns(1).caption = Ador(0).Value
        sdbcGrupo.Columns(0).caption = Ador(0).Value
        sdbcEstCod.Columns(0).caption = Ador(0).Value
        sdbcEstDen.Columns(1).caption = Ador(0).Value
        sdbcMonCod.Columns(0).caption = Ador(0).Value
        sdbcMonDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value '5 Denominacion
        sdbcProceCod.Columns(1).caption = Ador(0).Value
        sdbcProceDen.Columns(0).caption = Ador(0).Value
        sdbgPreciosEscalados.Columns("DEN").caption = Ador(0).Value
        sdbgPrecios.Columns(1).caption = Ador(0).Value
        sdbcGrupo.Columns(1).caption = Ador(0).Value
        sdbgAtributos.Columns(1).caption = Ador(0).Value
        sdbcEstCod.Columns(1).caption = Ador(0).Value
        sdbcEstDen.Columns(0).caption = Ador(0).Value
        sdbcMonCod.Columns(1).caption = Ador(0).Value
        sdbcMonDen.Columns(0).caption = Ador(0).Value
        lblDenGrupo.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgPreciosEscalados.Columns("COD").caption = Ador(0).Value '6 C�digo
        sdbgPrecios.Columns(0).caption = Ador(0).Value '6 C�digo
        sdbgAtributos.Columns(0).caption = Ador(0).Value
        lblCodGrupo.caption = Ador(0).Value & ":"
        sIdiCodigo = Ador(0).Value
        Ador.MoveNext
        lblFecOfe.caption = Ador(0).Value
        Ador.MoveNext
        lblFecVal.caption = Ador(0).Value
        Ador.MoveNext
        lblEstCod.caption = Ador(0).Value
        Ador.MoveNext
        lblMonCod.caption = Ador(0).Value '10  Moneda de la oferta:
        sIdiMon = Left(lblMonCod.caption, Len(lblMonCod.caption) - 1)
        Ador.MoveNext
        sIdiEquivaleA1 = Ador(0).Value
        Ador.MoveNext
        lblObs.caption = Ador(0).Value
        Ador.MoveNext
        sIdiMaterial = Ador(0).Value
        Ador.MoveNext
        lblFecApe.Item(0).caption = Ador(0).Value
        Ador.MoveNext
        lblFecNec(0).caption = Ador(0).Value
        Ador.MoveNext
        lblFecPres(0).caption = Ador(0).Value
        Ador.MoveNext
        chkPermAdjDir.caption = Ador(0).Value
        Ador.MoveNext
        lblFecIniSub(0).caption = Ador(0).Value
        Ador.MoveNext
        lblHoraIniSub(0).caption = Ador(0).Value & ":"
        lblHoraLimite(0).caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblFecLimit(0).caption = Ador(0).Value '20
        m_sIdiFecFinSubasta = Ador(0).Value
        Ador.MoveNext
        lblPresupuesto(0).caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblCambio(0).caption = Ador(0).Value & ":"
        lblCambio(2).caption = Ador(0).Value & ":"
        sIdiEquivale = Ador(0).Value
        Ador.MoveNext
        lblFecIni(0).caption = Ador(0).Value
        lblIniGrupo.caption = Ador(0).Value
        Ador.MoveNext
        lblFecFin(0).caption = Ador(0).Value
        lblFinGrupo.caption = Ador(0).Value
        Ador.MoveNext
        lblDest(0).caption = Ador(0).Value
        lblDestGrupo(0).caption = Ador(0).Value
        Ador.MoveNext
        lblPago(0).caption = Ador(0).Value
        lblPagGrupo(0).caption = Ador(0).Value
        Ador.MoveNext
        lblProveProce(0).caption = Ador(0).Value
        lblProveGrupo(0).caption = Ador(0).Value
        Ador.MoveNext
        lblDatosGenProce.caption = Ador(0).Value
        Ador.MoveNext
        lblDatosGeneralesOferta.caption = Ador(0).Value '29
        Ador.MoveNext
        m_sIdiAtribProce = Ador(0).Value
        Ador.MoveNext
        m_sIdiAtribGrupo = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.MoveNext
        m_sIdiAdjunProce = Ador(0).Value '35
        Ador.MoveNext
        m_sIdiAdjunGrupo = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        m_sIdiAdjuntos = Ador(0).Value
        Ador.MoveNext
        m_sIdiAtributos = Ador(0).Value
        Ador.MoveNext
        m_sIdiItems = Ador(0).Value
        Ador.MoveNext
        m_sIdiFecFinTradicional = Ador(0).Value '40
        Ador.MoveNext
        m_sIdiSelProceso = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaPendientes = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.AbrevParaTodos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaPendientes = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaAbiertos = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaParcialCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaCerrados = Ador(0).Value
        Ador.MoveNext
        ProceSelector1.DenParaTodos = Ador(0).Value
        Ador.MoveNext
        sIdiTodosArchivos = Ador(0).Value
        Ador.MoveNext
        sIdiFecOfe = Ador(0).Value
        Ador.MoveNext
        sIdiFecVal = Ador(0).Value
        Ador.MoveNext
        sIdiLaOfe = Ador(0).Value '55
        Ador.MoveNext
        sIdiElArchivo = Ador(0).Value
        sdbgAdjun.Columns("FICHERO").caption = Ador(0).Value
        Ador.MoveNext
        sIdiGuardar = Ador(0).Value
        Ador.MoveNext
        sIdiTipoOrig = Ador(0).Value
        Ador.MoveNext
        sIdiPrecio = Ador(0).Value
        Ador.MoveNext
        sIdiCantidadMax = Ador(0).Value '60
        sdbgPrecios.Columns(16).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjun.Columns("COMENTARIO").caption = Ador(0).Value
        sdbgPrecios.Columns(11).caption = Ador(0).Value
        sdbgPrecios.Columns(13).caption = Ador(0).Value
        sdbgPrecios.Columns(15).caption = Ador(0).Value
        Me.lblComent.caption = Ador(0).Value
        Ador.MoveNext
        lblDescrGrupo.caption = Ador(0).Value
        Ador.MoveNext
        Label18.caption = Ador(0).Value
        Ador.MoveNext
        Label11.caption = Ador(0).Value
        Ador.MoveNext
        lblCodPort.caption = Ador(0).Value '65
        Ador.MoveNext
        Label12.caption = Ador(0).Value 'Direccion
        Ador.MoveNext
        Label10.caption = Ador(0).Value 'CP
        Ador.MoveNext
        Label9.caption = Ador(0).Value 'Poblacion
        Ador.MoveNext
        Label4.caption = Ador(0).Value 'Pais
        Ador.MoveNext
        Label13.caption = Ador(0).Value 'Moneda '70
        lblMon(0).caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Ador(0).Value 'Provincia
        Ador.MoveNext
        Label5.caption = Ador(0).Value 'url
        Ador.MoveNext
        With sdbgContactos
            .Columns(0).caption = Ador(0).Value
            Ador.MoveNext
            .Columns(1).caption = Ador(0).Value
            Ador.MoveNext
            .Columns(2).caption = Ador(0).Value '75
            Ador.MoveNext
            .Columns(3).caption = Ador(0).Value
            Ador.MoveNext
            .Columns(4).caption = Ador(0).Value
            Ador.MoveNext
            .Columns(5).caption = Ador(0).Value
            Ador.MoveNext
            .Columns(6).caption = Ador(0).Value
            Ador.MoveNext
            .Columns(7).caption = Ador(0).Value '80
            Ador.MoveNext
            .Columns(8).caption = Ador(0).Value
            Ador.MoveNext
            .Columns(10).caption = Ador(0).Value
            Ador.MoveNext
        End With
        m_sIdiUsarPrec(1) = Ador(0).Value
        Ador.MoveNext
        m_sIdiUsarPrec(2) = Ador(0).Value
        Ador.MoveNext
        m_sIdiUsarPrec(3) = Ador(0).Value '85
        Ador.MoveNext
        m_sIdiTodosItems = Ador(0).Value
        Ador.MoveNext
        sIdiMayor = Ador(0).Value
        Ador.MoveNext
        sIdiMenor = Ador(0).Value
        Ador.MoveNext
        sIdiEntre = Ador(0).Value
        Ador.MoveNext
        sIdiEdicion = Ador(0).Value  '90
        cmdModoEdicion.caption = Ador(0).Value
        Ador.MoveNext
        sIdiConsulta = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value '95
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        With cmdListado
            .Item(0).caption = Ador(0).Value
            .Item(1).caption = Ador(0).Value
            .Item(2).caption = Ador(0).Value
            .Item(3).caption = Ador(0).Value
        End With
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        With cmdRestaurar
            .Item(0).caption = Ador(0).Value
            .Item(1).caption = Ador(0).Value
            .Item(2).caption = Ador(0).Value
            .Item(3).caption = Ador(0).Value
            .Item(4).caption = Ador(0).Value
        End With
        Ador.MoveNext
        ChkMostrarItems.caption = Ador(0).Value '100
        Ador.MoveNext
        sdbgPreciosEscalados.Columns("DEST").caption = Ador(0).Value
        sdbgPrecios.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPreciosEscalados.Columns("UNI").caption = Ador(0).Value
        sdbgPrecios.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPreciosEscalados.Columns("INI").caption = Ador(0).Value
        sdbgPrecios.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPreciosEscalados.Columns("FIN").caption = Ador(0).Value
        sdbgPrecios.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPreciosEscalados.Columns("PAGO").caption = Ador(0).Value '105
        sdbgPrecios.Columns(9).caption = Ador(0).Value '105
        Ador.MoveNext
        sdbgPreciosEscalados.Columns("CANT").caption = Ador(0).Value
        sdbgPrecios.Columns("CANT").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiPresUni = Ador(0).Value
        sdbgPrecios.Columns(8).caption = Ador(0).Value
        sdbgPreciosEscalados.Columns("PREC").caption = Ador(0).Value
        Ador.MoveNext
        lblCPresGrupo(0).caption = Ador(0).Value
        Ador.MoveNext
        lblCPresGrupo(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns(18).caption = Ador(0).Value '110
        Ador.MoveNext
        m_sIdiCaptionObs = Ador(0).Value
        Ador.MoveNext
        sIdiSelecAdjunto = Ador(0).Value
        Ador.MoveNext
        sIdiEstadoSobreAbierto = Ador(0).Value
        Ador.MoveNext
        sIdiEstadoSobreCerrado = Ador(0).Value
        Ador.MoveNext
        Me.lblFechaAperturaSobre.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFechaRealAperturaSobre.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblUsuAperSobre.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblObsSobre.caption = Me.lblObs.caption
        sIdiSobre = Ador(0).Value
        Ador.MoveNext
        Me.lblMsjSobreCerrado.caption = Ador(0).Value
        Ador.MoveNext
        sIdiOferta = Ador(0).Value
        Ador.MoveNext
        sIdiProve = Ador(0).Value
        
        Ador.MoveNext
        m_sHoraRec = Ador(0).Value
        Ador.MoveNext
        
        cmdAbrirSobre.caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiDialogoExcel = Ador(0).Value
        Ador.MoveNext
        
        sIdiFiltroExcel = Ador(0).Value
        Ador.MoveNext
        
        sIdiDialogoCargarXls = Ador(0).Value
        
        Ador.MoveNext
        Me.cmdCargarXLS.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdGenerarXLS.caption = Ador(0).Value
        
        Ador.MoveNext
        lblNumDec.caption = Ador(0).Value

        Ador.MoveNext
        m_skb = Ador(0).Value
        Ador.MoveNext
        sdbgAdjun.Columns("TAMANYO").caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdNoOfe(0).caption = Ador(0).Value
        sIdiNoOfe = Ador(0).Value
        Ador.MoveNext
        
        sIdiIntroGS = Ador(0).Value
        Ador.MoveNext
        
        sIdiIntroPortal = Ador(0).Value
        Ador.MoveNext
        sIdiUsuario = Ador(0).Value
        Ador.MoveNext
        
        lblLitMonProc.caption = Ador(0).Value
        Ador.MoveNext
        sIdiContacto = Ador(0).Value
        Ador.MoveNext
        cmdNoOfe(1).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiNoOfeGS = Ador(0).Value
        Ador.MoveNext
        m_sIdiNoOfePORT = Ador(0).Value
        
        Ador.MoveNext
        m_sLitRecalculando = Ador(0).Value
        Ador.MoveNext
        m_sMensajeModifAtrib = Ador(0).Value
        Ador.MoveNext
        m_sMensajeModifPrecio = Ador(0).Value
        Ador.MoveNext
        m_sMensajeAfecta = Ador(0).Value
        Ador.MoveNext
        m_sMensajeSiContinua = Ador(0).Value
        Ador.MoveNext
        m_sMensajeContinuar = Ador(0).Value
        Ador.MoveNext
        m_sRango = Ador(0).Value
        Ador.MoveNext
        m_sIdiOtros = Ador(0).Value '209 Otros atributos
        
        m_sMensajeModifAtrib = m_sMensajeModifAtrib & vbCrLf & m_sMensajeAfecta
        m_sMensajeModifAtrib = m_sMensajeModifAtrib & vbCrLf & m_sMensajeSiContinua
        m_sMensajeModifAtrib = m_sMensajeModifAtrib & vbCrLf & m_sMensajeContinuar
        
        m_sMensajeModifPrecio = m_sMensajeModifPrecio & vbCrLf & m_sMensajeAfecta
        m_sMensajeModifPrecio = m_sMensajeModifPrecio & vbCrLf & m_sMensajeSiContinua
        m_sMensajeModifPrecio = m_sMensajeModifPrecio & vbCrLf & m_sMensajeContinuar

        Ador.MoveNext
        m_sPresUniAbrv = Ador(0).Value '210 Pres. Uni.
        Ador.MoveNext
        m_sAtribItem = Ador(0).Value
        Ador.MoveNext
        m_sGeneraAdjuntos = Ador(0).Value
        Ador.MoveNext
        m_sGeneraAdjunto = Ador(0).Value
        Ador.MoveNext
        cmdAdjunMasivos.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPrecios.Columns("ANYOIMPUTACION").caption = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing
    
    sdbgPreciosEscalados.Columns("COMENT").caption = Me.lblComent.caption
    sdbgPrecios.Columns(10).caption = m_sIdiUsarPrec(1)
    sdbgPrecios.Columns(12).caption = m_sIdiUsarPrec(2)
    sdbgPrecios.Columns(14).caption = m_sIdiUsarPrec(3)
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    lblSolicitud(0).caption = gParametrosGenerales.gsDenSolicitudCompra & ":"
End Sub

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestComprador)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        m_bRComp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestEquipo)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        m_bREqp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestResponsable)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        m_bRCompResp = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestMatComprador)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        m_bRMat = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestUsuAper)) Is Nothing) Then
        m_bRUsuAper = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestOfeComp)) Is Nothing) Then
        m_bOfeAsigComp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestOfeEqp)) Is Nothing) Then
        m_bOfeAsigEqp = True
    End If
    
    m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestUsuUON)) Is Nothing)
    m_bRUsuDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestUsuDep)) Is Nothing)
    m_bRPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestPerfUON)) Is Nothing)
    
    m_bModifResponsable = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEModifResponsable)) Is Nothing)
    m_bModifEquivalencia = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEModifEquivalencia)) Is Nothing)
    m_bAlta = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEAlta)) Is Nothing)
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEAltaProvAsig)) Is Nothing) Then
        m_bAltaProvAsig = True
    Else
        m_bAltaProvAsig = False
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEAltaProvCompAsig)) Is Nothing) Then
        m_bAltaProvCompAsig = True
    Else
        m_bAltaProvCompAsig = False
    End If
    
    m_bModifOfeDeProv = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEModifOfeProv)) Is Nothing)
    m_bModifOfeDeUsu = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEModifOfeUsu)) Is Nothing)
        
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestModifOfeProv)) Is Nothing) Then
        m_bRestModifOfeProvUsu = True
    Else
        m_bRestModifOfeProvUsu = False
    End If
    
    m_bRestModifOfeUsu = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestModifOfeUsu)) Is Nothing)
    m_bElimOfeDeProv = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEElimOfeProv)) Is Nothing)
    m_bElimOfeDeUsu = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEElimOfeUsu)) Is Nothing)
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestElimOfeProv)) Is Nothing) Then
        m_bRestElimOfeProvUsu = True
    Else
        m_bRestElimOfeProvUsu = False
    End If
    
    m_bRestElimOfeUsu = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestElimOfeUsu)) Is Nothing)
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFERestProvMatComp)) Is Nothing Then
        m_bRestProvMatComp = True
    Else
        m_bRestProvMatComp = False
    End If

    ConfigurarBotones
End Sub

Private Sub ConfigurarBotones()

 'No hay permisos para dar de alta
    If Not m_bAlta And Not m_bAltaProvAsig And Not m_bAltaProvCompAsig Then
        cmdA�adir.Visible = False 'picNavigate 3
        cmdCargarXLS.Visible = False
        cmdGenerarXLS.Visible = False
        cmdRestaurar(3).Left = cmdA�adir.Left
        cmdListado(3).Left = cmdRestaurar(3).Left + cmdRestaurar(3).Width + 120
        cmdNoOfe(1).Visible = False
    End If
    
    'no hay permisos para eliminar:
    If Not m_bElimOfeDeProv And Not m_bElimOfeDeUsu Then
        cmdEliminar.Visible = False
        cmdAdjunMasivos.Left = cmdEliminar.Left
        cmdModificar.Left = cmdAdjunMasivos.Left + cmdAdjunMasivos.Width + 120
        cmdRestaurar(1).Left = cmdModificar.Left + cmdModificar.Width + 120
        cmdListado(1).Left = cmdRestaurar(1).Left + cmdRestaurar(1).Width + 120
    End If
    
    'No hay permisos para modificar:
    If Not m_bModifOfeDeUsu And Not m_bModifOfeDeProv Then
        cmdModificar.Visible = False 'picNavigate 1
        cmdRestaurar(1).Left = cmdModificar.Left
        cmdListado(1).Left = cmdRestaurar(1).Left + cmdRestaurar(1).Width + 120
        cmdModoEdicion.Visible = False 'picNavigate 2
        
        cmdA�adirAdjun.Visible = False
        cmdEliminarAdjun.Visible = False
        cmdModificarAdjun.Visible = False
        txtObsAdjun.Locked = True
    End If
    
    If Not m_bModifEquivalencia Then
        txtCambio.Visible = False
        lblEquivalencia.Top = lblCambio(2).Top
    End If
End Sub

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Select Case sdbgAtributos.Columns("INTRO").Value
    Case 1  'Seleccion
        sdbddValor.RemoveAll
        sdbddValor.AddItem ""
        If sdbgAtributos.Columns("TIPO").Value = 2 Then
            sdbddValor.Columns(0).Alignment = ssCaptionAlignmentRight
        Else
            sdbddValor.Columns(0).Alignment = ssCaptionAlignmentLeft
        End If
        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
    Case 0 'Libre
        Select Case sdbgAtributos.Columns("TIPO").Value
            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                sdbgAtributos.Columns("VALOR").Locked = False
                sdbgAtributos.Columns("VALOR").Style = ssStyleEditButton
                sdbgAtributos.Columns("VALOR").Alignment = ssCaptionAlignmentLeft
            Case 4
                If bModoEdicion Then
                    sdbddValor.RemoveAll
                    sdbddValor.AddItem ""
                    sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
                    sdbddValor.Enabled = True
                Else
                    sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                    sdbddValor.Enabled = False
                End If
            Case Else
                sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
                sdbddValor.Enabled = False
        End Select
End Select
End Sub

''' <summary>
''' Carga el �rbol del proceso seg�n su configuraci�n.
''' </summary>
''' <remarks>Llamada desde: ProcesoSeleccionado ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarTreeViewProce()

    Dim NodeRaiz As MSComctlLib.node
    Dim NodeProveedor As MSComctlLib.node
    Dim NodeOferta As MSComctlLib.node
    Dim NodeAtribsGen As MSComctlLib.node
    Dim NodeAdjunGen As MSComctlLib.node
    Dim NodeGrupo As MSComctlLib.node
    Dim NodeAtribsGrupo As MSComctlLib.node
    Dim NodeAdjunGrupo As MSComctlLib.node
    Dim NodeItemsGrupo As MSComctlLib.node
    Dim NodeSobre As MSComctlLib.node
    Dim oOfe As COferta
    Dim oProve As CProveedor
    Dim oGrupo As CGrupo
    Dim oSobre As CSobre
    Dim sCodOfe As String
    Dim sCodSobre As String
    Dim bCargar As Boolean
    Dim sCod As String

    tvwProce.Nodes.clear
    
    Set m_oIOfertas = m_oProcesoSeleccionado
    Set NodeRaiz = tvwProce.Nodes.Add(, , "Raiz", m_sIdiProceso, "Proceso")
    NodeRaiz.Expanded = True
    NodeRaiz.Tag = "Raiz"


    For Each oProve In m_oProveedoresProceso
        'Proveedor
        Set NodeProveedor = tvwProce.Nodes.Add("Raiz", tvwChild, "P_" & oProve.Cod, oProve.Cod & "-" & oProve.Den, IIf(oProve.NoOfe And Not oProve.HaOfertado, "NOOFE", "Prove"))
        NodeProveedor.Tag = "PV_" & oProve.Cod
        For Each oOfe In m_oOfertasProceso
            
            If Trim(oOfe.Prove) = Trim(oProve.Cod) Then
                If m_oProcesoSeleccionado.AdminPublica Then
                
                    sCodOfe = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod)) & CStr(oOfe.Num)
                    Set NodeOferta = tvwProce.Nodes.Add(NodeProveedor.key, tvwChild, "O_" & oProve.Cod & "_" & oOfe.Num, sIdiOferta & oOfe.Num, IIf(Not oOfe.Adjuntos Is Nothing, "Oferta2", "Oferta"))
                    NodeOferta.Tag = "OF_" & sCodOfe
                
                    For Each oSobre In m_oProcesoSeleccionado.Sobres
                        sCodSobre = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod)) & CStr(oOfe.Num) & "_" & CStr(oSobre.Sobre)
                        Set NodeSobre = tvwProce.Nodes.Add(NodeOferta.key, tvwChild, "SO_" & oProve.Cod & "_" & oOfe.Num & "_" & CStr(oSobre.Sobre), sIdiSobre & oSobre.Sobre, IIf(oSobre.Estado = 1, "sobreabierto", "sobrecerrado"))
                        NodeSobre.Tag = "SO_" & CStr(oSobre.Sobre)
                    Next
                    
                    For Each oGrupo In m_oProcesoSeleccionado.Grupos
                        If gParametrosGenerales.gbProveGrupos Then
                            sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            If m_oAsigs.Item(sCod).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                bCargar = False
                            Else
                                bCargar = True
                            End If
                        Else
                            bCargar = True
                        End If
                        
                        If bCargar Then
                            If m_oProcesoSeleccionado.Sobres.Item(oGrupo.Sobre).Estado = 1 Then
                                sCodSobre = "SO_" & oProve.Cod & "_" & oOfe.Num & "_" & CStr(oGrupo.Sobre)
                                Set NodeGrupo = tvwProce.Nodes.Add(sCodSobre, tvwChild, "GRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, oGrupo.Codigo & " - " & oGrupo.Den, "Grupo")
                                NodeGrupo.Tag = "GR_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                                
                                If Not oOfe.AtribGrOfertados Is Nothing Then
                                    If oOfe.Grupos.Item(oGrupo.Codigo).NumAtribOfer > 0 Then
                                        Set NodeAtribsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ATGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAtributos, "Atributos")
                                    Else
                                        Set NodeAtribsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ATGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAtributos, "AtributosGris")
                                    End If
                                    NodeAtribsGrupo.Tag = "GT_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                                End If
                                If Not m_bGrupoConEscalados Then
                                    If m_oProcesoSeleccionado.AdjuntarAGrupos Then
                                        If oOfe.Grupos.Item(oGrupo.Codigo).NumAdjuntos > 0 Or (NullToStr(oOfe.Grupos.Item(oGrupo.Codigo).ObsAdjun) <> "") Then
                                            Set NodeAdjunGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ADJUNGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAdjuntos, "Ficheros")
                                        Else
                                            Set NodeAdjunGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ADJUNGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAdjuntos, "FicherosGris")
                                        End If
                                        NodeAdjunGrupo.Tag = "GA_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                                    End If
                                End If
                            
                                If oOfe.Grupos.Item(oGrupo.Codigo).NumPrecios > 0 Then
                                    Set NodeItemsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ITEMSGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiItems, "Item")
                                Else
                                    Set NodeItemsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ITEMSGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiItems, "ItemGris")
                                End If
                                NodeItemsGrupo.Tag = "GI_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                            End If
                        End If
                    Next
                Else
                    'Oferta
                    sCodOfe = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod)) & CStr(oOfe.Num)
                    Set NodeOferta = tvwProce.Nodes.Add(NodeProveedor.key, tvwChild, "O_" & oProve.Cod & "_" & oOfe.Num, sIdiOferta & oOfe.Num, IIf(Not oOfe.Adjuntos Is Nothing, "Oferta2", "Oferta"))
                    NodeOferta.Tag = "OF_" & sCodOfe
                    If Not oOfe.AtribProcOfertados Is Nothing Then
                        'Si tiene atributos ofertados o no --> icono color o gris
                        If oOfe.NumAtribOfer > 0 Then
                            Set NodeAtribsGen = tvwProce.Nodes.Add(NodeOferta.key, tvwChild, "ATOFE_" & oProve.Cod & "_" & oOfe.Num, m_sIdiAtributos, "Atributos")
                        Else
                            Set NodeAtribsGen = tvwProce.Nodes.Add(NodeOferta.key, tvwChild, "ATOFE_" & oProve.Cod & "_" & oOfe.Num, m_sIdiAtributos, "AtributosGris")
                        End If
                        NodeAtribsGen.Tag = "OT_" & sCodOfe
                    End If
                    
                    If m_oProcesoSeleccionado.AdjuntarAOfertas Then
                        If oOfe.NumAdjuntos > 0 Or (NullToStr(oOfe.ObsAdjun) <> "") Then
                            Set NodeAdjunGen = tvwProce.Nodes.Add(NodeOferta.key, tvwChild, "ADJUNOFE_" & oProve.Cod & "_" & oOfe.Num, m_sIdiAdjuntos, "Ficheros")
                        Else
                            Set NodeAdjunGen = tvwProce.Nodes.Add(NodeOferta.key, tvwChild, "ADJUNOFE_" & oProve.Cod & "_" & oOfe.Num, m_sIdiAdjuntos, "FicherosGris")
                        End If
                        NodeAdjunGen.Tag = "OA_" & sCodOfe
                    End If
                    
                    For Each oGrupo In m_oProcesoSeleccionado.Grupos
                        If gParametrosGenerales.gbProveGrupos Then
                            sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            If m_oAsigs.Item(sCod).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                                bCargar = False
                            Else
                                bCargar = True
                            End If
                        Else
                            bCargar = True
                        End If
                        If bCargar Then
                            Set NodeGrupo = tvwProce.Nodes.Add(NodeOferta.key, tvwChild, "GRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, oGrupo.Codigo & " - " & oGrupo.Den, "Grupo")
                            NodeGrupo.Tag = "GR_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                            
                            If Not oOfe.AtribGrOfertados Is Nothing Then
                                If oOfe.Grupos.Item(oGrupo.Codigo).NumAtribOfer > 0 Then
                                    Set NodeAtribsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ATGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAtributos, "Atributos")
                                Else
                                    Set NodeAtribsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ATGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAtributos, "AtributosGris")
                                End If
                                NodeAtribsGrupo.Tag = "GT_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                            End If
                            
                            If Not m_bGrupoConEscalados Then
                                If m_oProcesoSeleccionado.AdjuntarAGrupos Then
                                    If oOfe.Grupos.Item(oGrupo.Codigo).NumAdjuntos > 0 Or (NullToStr(oOfe.Grupos.Item(oGrupo.Codigo).ObsAdjun) <> "") Then
                                        Set NodeAdjunGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ADJUNGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAdjuntos, "Ficheros")
                                    Else
                                        Set NodeAdjunGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ADJUNGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiAdjuntos, "FicherosGris")
                                    End If
                                    NodeAdjunGrupo.Tag = "GA_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                                End If
                            End If
                        
                            If oOfe.Grupos.Item(oGrupo.Codigo).NumPrecios > 0 Then
                                Set NodeItemsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ITEMSGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiItems, "Item")
                            Else
                                Set NodeItemsGrupo = tvwProce.Nodes.Add(NodeGrupo.key, tvwChild, "ITEMSGRUPO_" & oProve.Cod & "_" & oOfe.Num & "_" & oGrupo.Codigo, m_sIdiItems, "ItemGris")
                            End If
                            NodeItemsGrupo.Tag = "GI_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                        End If
                    Next
                End If
            End If
        Next
    Next

Set NodeOferta = Nothing
Set NodeProveedor = Nothing
Set NodeRaiz = Nothing
Set NodeAtribsGen = Nothing
Set NodeAdjunGen = Nothing
Set NodeGrupo = Nothing
Set NodeAtribsGrupo = Nothing
Set NodeAdjunGrupo = Nothing
Set NodeItemsGrupo = Nothing
End Sub

'***************************************************************************************
' Oculta el �rbol
'***************************************************************************************
Private Sub LimpiarTreeViewProce()
Dim NodeRaiz As MSComctlLib.node

tvwProce.Nodes.clear
Set m_oIOfertas = m_oProcesoSeleccionado
Set NodeRaiz = tvwProce.Nodes.Add(, , "Raiz", m_sIdiSelProceso, "Proceso")
NodeRaiz.Expanded = True
NodeRaiz.Tag = "Raiz"
Set NodeRaiz = Nothing
End Sub

''' <summary>
''' Evento que salta al hacer click en el arbol de ofertas
''' </summary>
''' <param name="Node">Es el nodo sobre el que se ha hecho cambios</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Al hacer click en cualquier nodo del arbol;  Tiempo m�ximo:0</remarks>

Public Sub tvwProce_NodeClick(ByVal node As MSComctlLib.node)
Dim sTag As String
Dim sCod As String
Dim scod1 As String
Dim scod2 As String
Dim pos As Byte

If m_oProcesoSeleccionado Is Nothing Then Exit Sub

sTag = Left(node.Tag, 3)

If g_bAnyadirOfertaObl Then
    'Estamos a�adiendo una oferta con atributos obligatorios que no se estan rellenando
    If picApartado(2).Visible Then
        'Atributos
        If sdbgAtributos.DataChanged Then
            bModError = False
            If sdbgAtributos.Rows = 1 Then
                sdbgAtributos.Update
            Else
                sdbgAtributos.MoveNext
                If Not bModError Then
                    sdbgAtributos.MovePrevious
                End If
            End If
        End If
    End If

    Select Case Left(node.Tag, 3)
    Case "Rai"
            tvwProce_NodeClick m_nodeSel
            tvwProce.Nodes(m_nodeSel).Selected = True
            Exit Sub
    Case "PV_"
        sCod = Right(node.Tag, Len(node.Tag) - 3)
        If Not m_oProveSeleccionado Is Nothing Then
            If m_oProveSeleccionado.Cod <> m_oProveedoresProceso.Item(sCod).Cod Then
                tvwProce_NodeClick m_nodeSel
                tvwProce.Nodes(m_nodeSel).Selected = True
                Exit Sub
            End If
        End If
        
    Case "OT_"
            pos = InStr(1, node.FullPath, m_sPathNuevoNodo)
            If pos > 0 Then
                Set m_nodeSel = node
            Else
                tvwProce_NodeClick m_nodeSel
                tvwProce.Nodes(m_nodeSel).Selected = True
                Exit Sub
            End If
       
    Case "OF_"
            pos = InStr(1, node.FullPath, m_sPathNuevoNodo)
            If pos > 0 Then
                Set m_nodeSel = node
            Else
                tvwProce_NodeClick m_nodeSel
                tvwProce.Nodes(m_nodeSel).Selected = True
                Exit Sub
            End If
       
    Case "GR_"
            pos = InStr(1, node.FullPath, m_sPathNuevoNodo)
            If pos > 0 Then
                Set m_nodeSel = node
            Else
                tvwProce_NodeClick m_nodeSel
                tvwProce.Nodes(m_nodeSel).Selected = True
                Exit Sub
            End If
            
    Case "GT_"
            pos = InStr(1, node.FullPath, m_sPathNuevoNodo)
            If pos > 0 Then
                Set m_nodeSel = node
            Else
                tvwProce_NodeClick m_nodeSel
                tvwProce.Nodes(m_nodeSel).Selected = True
                Exit Sub
            End If
                           
    End Select

End If

sTag = Left(node.Tag, 3)

Select Case Left(node.Tag, 1)
Case "R"
    Set m_oProveSeleccionado = Nothing
    Set g_oOfertaSeleccionada = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Set g_oGrupoOferSeleccionado = Nothing
Case "P"
    sCod = Right(node.Tag, Len(node.Tag) - 3)
    Set m_oProveSeleccionado = m_oProveedoresProceso.Item(sCod)
    Set g_oOfertaSeleccionada = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Set g_oGrupoOferSeleccionado = Nothing
Case "O"
    sCod = Right(node.Tag, Len(node.Tag) - 3)
    If Not g_bAnyadirOfertaObl Then
        Set g_oOfertaSeleccionada = m_oOfertasProceso.Item(sCod)
        g_oOfertaSeleccionada.CargarDatosGeneralesOferta gParametrosInstalacion.gIdioma
    End If
    Set m_oProveSeleccionado = m_oProveedoresProceso.Item(g_oOfertaSeleccionada.Prove)
    Set m_oGrupoSeleccionado = Nothing
    Set g_oGrupoOferSeleccionado = Nothing
    If Left(node.Tag, 3) = "OF_" Then cmdAdjunMasivos.Enabled = g_oOfertaSeleccionada.HayAdjuntos
Case "G"
    sCod = Right(node.Tag, Len(node.Tag) - 3)
    scod1 = Left(sCod, InStr(1, sCod, "_$$$_") - 1)
    scod2 = Right(sCod, Len(sCod) - (InStr(1, sCod, "_$$$_") + 4))
    If Not g_bAnyadirOfertaObl Then
        Set g_oOfertaSeleccionada = m_oOfertasProceso.Item(scod2)
    End If
    Set m_oProveSeleccionado = m_oProveedoresProceso.Item(g_oOfertaSeleccionada.Prove)
    Set m_oGrupoSeleccionado = g_oOfertaSeleccionada.Grupos.Item(scod1).Grupo
    'Cargar escalados del grupo
    m_oGrupoSeleccionado.CargarEscalados
    If m_oGrupoSeleccionado.UsarEscalados Then
        m_bGrupoConEscalados = True
        sdbgPrecios.Visible = False
        sdbgPreciosEscalados.Visible = True
    Else
        m_bGrupoConEscalados = False
        sdbgPreciosEscalados.Visible = False
        sdbgPrecios.Visible = True
    End If
    Set g_oGrupoOferSeleccionado = g_oOfertaSeleccionada.Grupos.Item(scod1)
Case "S"
    sCod = Right(node.Tag, Len(node.Tag) - 3)
    Set g_oSobreSeleccionado = m_oProcesoSeleccionado.Sobres.Item(sCod)
    
End Select

If Not g_oOfertaSeleccionada Is Nothing Then
    g_oOfertaSeleccionada.DevolverTamanyoAdjuntos
End If

Select Case sTag
    Case "Rai"
        MostrarDatosGenerales
    Case "PV_"
        If m_oProveSeleccionado.Contactos Is Nothing Then
            m_oProveedoresProceso.CargarDatosProveedor sCod
            m_oProveSeleccionado.CargarTodosLosContactos
        End If
        MostrarProveedor
    Case "OF_"
        If g_oOfertaSeleccionada.CodEst = "" Then
            g_oOfertaSeleccionada.CargarDatosGeneralesOferta gParametrosInstalacion.gIdioma
        End If
        MostrarOferta
    Case "OT_"
        If g_oOfertaSeleccionada.AtribProcOfertados.Count = 0 Then
            g_oOfertaSeleccionada.CargarAtributosProcesoOfertados
            If m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                m_oProcesoSeleccionado.CargarAtributos
            Else
                If m_oProcesoSeleccionado.ATRIBUTOS.Count = 0 Then
                    m_oProcesoSeleccionado.CargarAtributos
                End If
            End If
        End If
        MostrarAtributosOfe
    Case "OA_"
        If g_oOfertaSeleccionada.Adjuntos Is Nothing Then
            g_oOfertaSeleccionada.CargarAdjuntos
        Else
            If g_oOfertaSeleccionada.Adjuntos.Count = 0 Then
                g_oOfertaSeleccionada.CargarAdjuntos
            End If
        End If
        MostrarAdjuntosOfe
    Case "GR_"
        
        MostrarDatosGrupo
        
    Case "GT_"
        
        If g_oGrupoOferSeleccionado.AtribOfertados Is Nothing Then
            g_oGrupoOferSeleccionado.CargarAtributosOfertados
            m_oGrupoSeleccionado.CargarAtributos ambito:=AmbGrupo
        ElseIf g_oGrupoOferSeleccionado.AtribOfertados.Count = 0 Then
            g_oGrupoOferSeleccionado.CargarAtributosOfertados
            m_oGrupoSeleccionado.CargarAtributos ambito:=AmbGrupo
        End If
        MostrarAtributosGrupoOfe
    Case "GA_"
        If g_oGrupoOferSeleccionado.Adjuntos Is Nothing Then
            g_oGrupoOferSeleccionado.CargarAdjuntos
        Else
            If g_oGrupoOferSeleccionado.Adjuntos.Count = 0 Then
                g_oGrupoOferSeleccionado.CargarAdjuntos
            End If
        End If
        
        MostrarAdjuntosGrupoOfe
    Case "GI_"
        If m_oGrupoSeleccionado.AtributosItem Is Nothing Then
            m_oGrupoSeleccionado.CargarAtributos ambito:=AmbItem
            g_oGrupoOferSeleccionado.CargarPrecios
        Else
            If m_oGrupoSeleccionado.AtributosItem.Count = 0 Then
                m_oGrupoSeleccionado.CargarAtributos ambito:=AmbItem
                g_oGrupoOferSeleccionado.CargarPrecios
            End If
        End If
        If g_oGrupoOferSeleccionado.Lineas Is Nothing Then
            m_oGrupoSeleccionado.CargarAtributos ambito:=AmbItem
            g_oGrupoOferSeleccionado.CargarPrecios
        Else
            If g_oGrupoOferSeleccionado.Lineas.Count = 0 Then
                m_oGrupoSeleccionado.CargarAtributos ambito:=AmbItem
                g_oGrupoOferSeleccionado.CargarPrecios
            End If
        End If
        If g_oOfertaSeleccionada.CodEst = "" Then
            g_oOfertaSeleccionada.CargarDatosGeneralesOferta gParametrosGenerales.gIdioma
        End If
        CargarGrupos g_oOfertaSeleccionada.Prove
        MostrarItemsGrupoOfe
    Case "SO_":
        MostrarSobre
End Select

End Sub

''' <summary> Carga en pantalla los datos del proceso seleccionado </summary>
''' <remarks> Revisado por: LTG     Fecha: 11/03/2011 </remarks>
''' <remarks>Llamada desde: ProcesoSeleccionado, tvwProce_NodeClick; Tiempo m�ximo:0,1</remarks>

Private Sub MostrarDatosGenerales()
    Dim dtFechaIniSub As Date
    Dim dtHoraIniSub As Date
    Dim dtFechaFinSub As Date
    Dim dtHoraFinSub As Date
    
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    lstMaterial.Visible = False
    lblFecIni(0).Visible = True
    lblFecIni(1).Visible = True
    lblFecFin(0).Visible = True
    lblFecFin(1).Visible = True
    lblDest(0).Visible = True
    lblDest(1).Visible = True
    lblDest(2).Visible = True
    lblPago(0).Visible = True
    lblPago(1).Visible = True
    lblPago(2).Visible = True
    lblProveProce(0).Visible = True
    lblProveProce(1).Visible = True
    lblProveProce(2).Visible = True
    lblFecIni(0).Top = 4380
    lblFecIni(1).Top = 4380
    lblFecFin(0).Top = 4380
    lblFecFin(1).Top = 4380
    lblDest(0).Top = 4830
    lblDest(1).Top = 4830
    lblDest(2).Top = 4830
    lblPago(0).Top = 5310
    lblPago(1).Top = 5310
    lblPago(2).Top = 5310
    lblProveProce(0).Top = 5790
    lblProveProce(1).Top = 5790
    lblProveProce(2).Top = 5790

    Dim oMat As CGrupoMatNivel4

    With m_oProcesoSeleccionado
        If Not .MaterialProce Is Nothing Then
            If .MaterialProce.Count = 1 Then
                txtMat.Visible = True
                txtMat.Text = .MaterialProce.Item(1).GMN1Cod & "-" & .MaterialProce.Item(1).GMN2Cod & "-" & .MaterialProce.Item(1).GMN3Cod & "-" & .MaterialProce.Item(1).Cod & "-" & .MaterialProce.Item(1).Den
                lstMaterial.Visible = False
            Else
                txtMat.Visible = False
                lstMaterial.Visible = True
                lstMaterial.clear
                For Each oMat In .MaterialProce
                    lstMaterial.AddItem oMat.GMN1Cod & "-" & oMat.GMN2Cod & "-" & oMat.GMN3Cod & "-" & oMat.Cod & "-" & oMat.Den
                Next
            End If
        End If
      Arrange
        Me.lblFecApe(1).caption = NullToStr(.FechaApertura)
        Me.lblFecNec(1).caption = NullToStr(.FechaNecesidad)
        Me.lblFecPres(1).caption = NullToStr(.FechaPresentacion)
        If .PermitirAdjDirecta Then
            Me.chkPermAdjDir.Value = vbChecked
        Else
            Me.chkPermAdjDir.Value = vbUnchecked
        End If
        Me.lblPresupuesto(1).caption = DblToStr(.PresGlobal)
        Me.lblSolicitud(1).caption = NullToStr(.Referencia)
        Me.lblMon(1).caption = NullToStr(.MonCod)
        Me.lblCambio(1).caption = FormateoNumerico(.Cambio, sFormatoNumber)
            
        If .ModoSubasta Then
            Me.lblFecIniSub(0).Visible = True
            Me.lblFecIniSub(1).Visible = True
            Me.lblHoraIniSub(0).Visible = True
            Me.lblHoraIniSub(1).Visible = True
            Me.lblHoraLimite(0).Visible = True
            Me.lblHoraLimite(1).Visible = True
            Me.lblFecLimit(0).caption = m_sIdiFecFinSubasta
            If NullToStr(.FechaInicioSubasta) = "" Then
                Me.lblFecIniSub(1).caption = ""
                Me.lblHoraIniSub(1).caption = ""
            Else
                ConvertirUTCaTZ DateValue(.FechaInicioSubasta), TimeValue(.FechaInicioSubasta), m_vTZHora, dtFechaIniSub, dtHoraIniSub
                Me.lblFecIniSub(1).caption = dtFechaIniSub
                Me.lblHoraIniSub(1).caption = dtHoraIniSub
            End If
            If NullToStr(.FechaMinimoLimOfertas) = "" Then
                Me.lblFecLimit(1).caption = ""
                Me.lblHoraLimite(1).caption = ""
            Else
                ConvertirUTCaTZ DateValue(.FechaMinimoLimOfertas), TimeValue(.FechaMinimoLimOfertas), m_vTZHora, dtFechaFinSub, dtHoraFinSub
                Me.lblFecLimit(1).caption = dtFechaFinSub
                Me.lblHoraLimite(1).caption = dtHoraFinSub
            End If
        Else
            Me.lblFecIniSub(0).Visible = False
            Me.lblFecIniSub(1).Visible = False
            Me.lblHoraIniSub(0).Visible = False
            Me.lblHoraIniSub(1).Visible = False
            Me.lblHoraLimite(0).Visible = False
            Me.lblHoraLimite(1).Visible = False
            Me.lblFecLimit(0).caption = m_sIdiFecFinTradicional
            Me.lblFecLimit(1).caption = NullToStr(.FechaMinimoLimOfertas)
        End If
            
        'Muestra el responsable del proceso
        cmdResponsable.ToolTipText = ""
        cmdResponsable.Enabled = True
        If Not .responsable Is Nothing Then
            If .responsable.Cod <> "" Then
                If .responsable.nombre <> "" Then
                    cmdResponsable.ToolTipText = .responsable.nombre & " " & .responsable.Apel
                Else
                    cmdResponsable.ToolTipText = .responsable.Apel
                End If
            End If
        End If
        
        If .DefFechasSum = EnProceso And .DefDestino = EnProceso And .DefFormaPago = EnProceso And .DefProveActual = EnProceso Then
            lblFecIni(1).caption = NullToStr(.FechaInicioSuministro)
            lblFecFin(1).caption = NullToStr(.FechaFinSuministro)
            lblDest(1).caption = NullToStr(.DestCod)
            lblDest(2).caption = NullToStr(.DestDen)
            lblPago(1).caption = NullToStr(.PagCod)
            lblPago(2).caption = NullToStr(.PagDen)
            lblProveProce(1).caption = NullToStr(.ProveActual)
            lblProveProce(2).caption = NullToStr(.ProveActDen)
            MostrarApartado iAptDatosGenerales
            Exit Sub
        End If
        If .DefDestino <> EnProceso And .DefFechasSum <> EnProceso And .DefFormaPago <> EnProceso And .DefProveActual <> EnProceso Then
            lblFecIni(0).Visible = False
            lblFecIni(1).Visible = False
            lblFecFin(0).Visible = False
            lblFecFin(1).Visible = False
            lblDest(0).Visible = False
            lblDest(1).Visible = False
            lblDest(2).Visible = False
            lblPago(0).Visible = False
            lblPago(1).Visible = False
            lblPago(2).Visible = False
            lblProveProce(0).Visible = False
            lblProveProce(1).Visible = False
            lblProveProce(2).Visible = False
            MostrarApartado iAptDatosGenerales
            Exit Sub
        End If
    
        If Not .DefFechasSum = EnProceso Then
            lblFecIni(0).Visible = False
            lblFecIni(1).Visible = False
            lblFecFin(0).Visible = False
            lblFecFin(1).Visible = False
            lblProveProce(0).Top = lblPago(1).Top
            lblProveProce(1).Top = lblPago(1).Top
            lblProveProce(2).Top = lblPago(1).Top
            lblPago(0).Top = lblDest(0).Top
            lblPago(1).Top = lblDest(0).Top
            lblPago(2).Top = lblDest(0).Top
            lblDest(0).Top = lblFecIni(0).Top
            lblDest(1).Top = lblFecIni(0).Top
            lblDest(2).Top = lblFecIni(0).Top
            If Not .DefDestino = EnProceso Then
                lblDest(0).Visible = False
                lblDest(1).Visible = False
                lblDest(2).Visible = False
                lblProveProce(0).Top = lblPago(1).Top
                lblProveProce(1).Top = lblPago(1).Top
                lblProveProce(2).Top = lblPago(1).Top
                lblPago(0).Top = lblDest(0).Top
                lblPago(1).Top = lblDest(0).Top
                lblPago(2).Top = lblDest(0).Top
                If Not .DefFormaPago = EnProceso Then
                    lblPago(0).Visible = False
                    lblPago(1).Visible = False
                    lblPago(2).Visible = False
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(0).Top = lblPago(1).Top
                        lblProveProce(1).Top = lblPago(1).Top
                        lblProveProce(2).Top = lblPago(1).Top
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                Else
                    lblPago(1).caption = NullToStr(.PagCod)
                    lblPago(2).caption = NullToStr(.PagDen)
                    lblPago(0).Visible = True
                    lblPago(1).Visible = True
                    lblPago(2).Visible = True
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                End If
            Else
                lblDest(0).Visible = True
                lblDest(1).Visible = True
                lblDest(2).Visible = True
                lblDest(1).caption = NullToStr(.DestCod)
                lblDest(2).caption = NullToStr(.DestDen)
                If Not .DefFormaPago = EnProceso Then
                    lblPago(0).Visible = False
                    lblPago(1).Visible = False
                    lblPago(2).Visible = False
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(0).Top = lblPago(1).Top
                        lblProveProce(1).Top = lblPago(1).Top
                        lblProveProce(2).Top = lblPago(1).Top
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                Else
                    lblPago(0).Visible = True
                    lblPago(1).Visible = True
                    lblPago(2).Visible = True
                    lblPago(1).caption = NullToStr(.PagCod)
                    lblPago(2).caption = NullToStr(.PagDen)
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                End If
                
            End If
        Else
            lblFecIni(0).Visible = True
            lblFecIni(1).Visible = True
            lblFecFin(0).Visible = True
            lblFecFin(1).Visible = True
            lblFecIni(1).caption = NullToStr(.FechaInicioSuministro)
            lblFecFin(1).caption = NullToStr(.FechaFinSuministro)
            If Not .DefDestino = EnProceso Then
                lblDest(0).Visible = False
                lblDest(1).Visible = False
                lblDest(2).Visible = False
                lblProveProce(0).Top = lblPago(1).Top
                lblProveProce(1).Top = lblPago(1).Top
                lblProveProce(2).Top = lblPago(1).Top
                lblPago(0).Top = lblDest(0).Top
                lblPago(1).Top = lblDest(0).Top
                lblPago(2).Top = lblDest(0).Top
                If Not .DefFormaPago = EnProceso Then
                    lblPago(0).Visible = False
                    lblPago(1).Visible = False
                    lblPago(2).Visible = False
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(0).Top = lblPago(1).Top
                        lblProveProce(1).Top = lblPago(1).Top
                        lblProveProce(2).Top = lblPago(1).Top
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                Else
                    lblPago(1).caption = NullToStr(.PagCod)
                    lblPago(2).caption = NullToStr(.PagDen)
                    lblPago(0).Visible = True
                    lblPago(1).Visible = True
                    lblPago(2).Visible = True
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                End If
            Else
                lblDest(0).Visible = True
                lblDest(1).Visible = True
                lblDest(2).Visible = True
                lblDest(1).caption = NullToStr(.DestCod)
                lblDest(2).caption = NullToStr(.DestDen)
                If Not .DefFormaPago = EnProceso Then
                    lblPago(0).Visible = False
                    lblPago(1).Visible = False
                    lblPago(2).Visible = False
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(0).Top = lblPago(1).Top
                        lblProveProce(1).Top = lblPago(1).Top
                        lblProveProce(2).Top = lblPago(1).Top
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                Else
                    lblPago(0).Visible = True
                    lblPago(1).Visible = True
                    lblPago(2).Visible = True
                    lblPago(1).caption = NullToStr(.PagCod)
                    lblPago(2).caption = NullToStr(.PagDen)
                    If Not .DefProveActual = EnProceso Then
                        lblProveProce(0).Visible = False
                        lblProveProce(1).Visible = False
                        lblProveProce(2).Visible = False
                    Else
                        lblProveProce(0).Visible = True
                        lblProveProce(1).Visible = True
                        lblProveProce(2).Visible = True
                        lblProveProce(1).caption = NullToStr(.ProveActual)
                        lblProveProce(2).caption = NullToStr(.ProveActDen)
                    End If
                End If
                
            End If
        End If
    
        
        
    End With
    
    MostrarApartado iAptDatosGenerales
End Sub

Private Sub MostrarProveedor()
Dim oCon As CContacto
Dim oOferta As COferta
Dim iNumOfe As Integer

LimpiarProve

With m_oProveSeleccionado
    If m_oProveSeleccionado.NoOfe Then
        If m_oProveSeleccionado.HaOfertado Then
            cmdNoOfe(0).Left = 4700
            cmdNoOfe(0).Width = 450
            cmdNoOfe(0).caption = "..."
            imgNoOfe.Visible = False
            lblComent.Visible = True
            tvwProce.Nodes("P_" & m_oProveSeleccionado.Cod).Image = "Prove"
            
        Else
            cmdNoOfe(0).Width = 1486.631
            cmdNoOfe(0).Left = 3665.259
            cmdNoOfe(0).caption = sIdiNoOfe
            imgNoOfe.Visible = True
            lblComent.Visible = False
            tvwProce.Nodes("P_" & m_oProveSeleccionado.Cod).Image = "NOOFE"
        End If
        cmdNoOfe(0).Visible = True
        cmdNoOfe(1).Visible = False
    Else
        lblComent.Visible = False
        cmdNoOfe(0).Visible = False
        imgNoOfe.Visible = False
        If m_oProveSeleccionado.HaOfertado Then
            cmdNoOfe(1).Visible = False
        Else
            If m_bAlta Or m_bAltaProvAsig Or m_bAltaProvCompAsig Then
                
                cmdNoOfe(1).Visible = True
            End If
        End If
    End If
    lblCodPortProve.caption = NullToStr(.CodPortal)
    lblDir.caption = NullToStr(.Direccion)
    lblCp.caption = NullToStr(.cP)
    lblPob.caption = NullToStr(.Poblacion)
    lblPaiCod.caption = NullToStr(.CodPais)
    lblPaiDen.caption = NullToStr(.DenPais)
    lblProviCod.caption = NullToStr(.CodProvi)
    lblProviDen.caption = NullToStr(.DenProvi)
    lblMonProveCod.caption = NullToStr(.CodMon)
    lblMonProveDen.caption = NullToStr(.DenMon)
    lblURL.caption = NullToStr(.URLPROVE)
    sdbgContactos.RemoveAll
    For Each oCon In .Contactos
        sdbgContactos.AddItem NullToStr(oCon.Apellidos) & Chr(m_lSeparador) & NullToStr(oCon.nombre) & Chr(m_lSeparador) & NullToStr(oCon.Departamento) & Chr(m_lSeparador) & NullToStr(oCon.Cargo) & Chr(m_lSeparador) & NullToStr(oCon.Tfno) _
        & Chr(m_lSeparador) & NullToStr(oCon.Fax) & Chr(m_lSeparador) & NullToStr(oCon.tfnomovil) & Chr(m_lSeparador) & NullToStr(oCon.mail) & Chr(m_lSeparador) & BooleanToSQLBinary(oCon.Def) & Chr(m_lSeparador) & BooleanToSQLBinary(oCon.NotifSubasta)
    Next
    
    lblCal1Den = NullToStr(.Calif1)
    lblcal2den = NullToStr(.Calif2)
    lblcal3den = NullToStr(.Calif3)
    
    lblCal1Val = DblToStr(.Val1, sFormatoNumber)
    lblCal2Val = DblToStr(.Val2, sFormatoNumber)
    lblCal3Val = DblToStr(.Val3, sFormatoNumber)
    txtObsProve.Text = NullToStr(.obs)
    
    iNumOfe = 0
    For Each oOferta In m_oOfertasProceso
        If oOferta.Prove = m_oProveSeleccionado.Cod Then
            iNumOfe = iNumOfe + 1
        End If
    Next
    
    If m_oProcesoSeleccionado.AdminPublica = True Or (Not m_bAlta And Not m_bAltaProvAsig And Not m_bAltaProvCompAsig) Then
        cmdA�adir.Visible = False
        cmdCargarXLS.Visible = False
        cmdGenerarXLS.Visible = False
        cmdRestaurar(3).Left = cmdA�adir.Left
    Else
        cmdA�adir.Visible = True
        cmdCargarXLS.Visible = True
        cmdGenerarXLS.Visible = True
        cmdCargarXLS.Left = cmdA�adir.Left + cmdA�adir.Width + 120
        cmdGenerarXLS.Left = cmdCargarXLS.Left + cmdCargarXLS.Width + 120
        cmdRestaurar(3).Left = cmdGenerarXLS.Left + cmdGenerarXLS.Width + 120
    End If
    cmdListado(3).Left = cmdRestaurar(3).Left + cmdRestaurar(3).Width + 120
    
    If m_oProcesoSeleccionado.Estado < conadjudicaciones Then
        If m_bAlta = True Then
            cmdA�adir.Enabled = True
            cmdCargarXLS.Enabled = True
            cmdNoOfe(1).Enabled = True
        ElseIf m_bAltaProvAsig = True Then  'Solo puede crear nuevas ofertas de proveedores asignados al usuario
            If m_bOfeAsigComp Then
                cmdA�adir.Enabled = True
                cmdCargarXLS.Enabled = True
                cmdNoOfe(1).Enabled = True
            ElseIf Not m_oProveedoresAsigUsu Is Nothing Then
                If m_oProveedoresAsigUsu.Item(CStr(m_oProveSeleccionado.Cod)) Is Nothing Then
                    cmdA�adir.Enabled = False
                    cmdCargarXLS.Enabled = False
                    cmdNoOfe(1).Enabled = False
                Else
                    cmdA�adir.Enabled = True
                    cmdCargarXLS.Enabled = True
                    cmdNoOfe(1).Enabled = True
                End If
            Else
                cmdA�adir.Enabled = False
                cmdCargarXLS.Enabled = False
                cmdNoOfe(1).Enabled = False
            End If
        ElseIf m_bAltaProvCompAsig = True Then   'Solo puede crear nuevas ofertas de proveedores asignados a compradores del equipo del usuario
            If m_bOfeAsigEqp Then
                cmdA�adir.Enabled = True
                cmdCargarXLS.Enabled = True
                cmdNoOfe(1).Enabled = True
            ElseIf Not m_oProveedoresAsigEqp Is Nothing Then
                If m_oProveedoresAsigEqp.Item(CStr(m_oProveSeleccionado.Cod)) Is Nothing Then
                    cmdA�adir.Enabled = False
                    cmdCargarXLS.Enabled = False
                    cmdNoOfe(1).Enabled = False
                Else
                    cmdA�adir.Enabled = True
                    cmdCargarXLS.Enabled = True
                    cmdNoOfe(1).Enabled = True
                End If
            Else
                cmdA�adir.Enabled = False
                cmdCargarXLS.Enabled = False
                cmdNoOfe(1).Enabled = True
            End If
        End If
    End If
    
End With

MostrarApartado iAptProveedor

End Sub


''' <summary>
''' Muestra los datos de una oferta
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cmdCancelar_click;cmdRestaurar_Click;tvwProce.Node_click; Tiempo m�ximo:0</remarks>
Private Sub MostrarOferta()
    Dim bPermitirEliminar As Boolean
    Dim oSobre As CSobre
    Dim bEliminar As Boolean
    Dim dtFechaRec As Date
    Dim dtHoraRec As Date
    
    Screen.MousePointer = vbHourglass
    
    If g_oOfertaSeleccionada Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    LimpiarOfe
    
    bRespetarCombo = True
    
    With g_oOfertaSeleccionada
        sdbcMonCod.Value = .CodMon
        sdbcMonCod.Text = .CodMon
        sdbcMonDen.Value = .DenMon
        sdbcMonDen.Text = .DenMon
        sdbcEstCod.Value = .CodEst
        sdbcEstCod.Text = .CodEst
        sdbcEstDen.Value = .DenEst
        sdbcEstDen.Text = .DenEst
        
        bRespetarCombo = False
        
        'FECREC se guarda en UTC
        ConvertirUTCaTZ DateValue(.FechaRec), TimeValue(.HoraRec), m_vTZHora, dtFechaRec, dtHoraRec
                
        txtFecOfe.Text = Format(dtFechaRec, "short date")    'Format(.FechaRec, "short date")
        txtFecVal.Text = Format(.FechaHasta, "short date")
        If .HoraRec = "" Then
            txtHoraOfe.Text = "00:00:00"
        Else
            txtHoraOfe.Text = dtHoraRec '.HoraRec
        End If
        
        txtCambio.Text = FormateoNumerico(.Cambio, sFormatoNumber)
        txtCambio.ToolTipText = DblToStr(txtCambio.Text)
        lblMonProceso.caption = m_oProcesoSeleccionado.MonCod & " - " & oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        lblEquivalencia.caption = FormateoNumerico(.Cambio, sFormatoNumber) & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
        
        If m_bModifEquivalencia = True Then
            If sdbcMonCod.Value <> "" Then
                If oMonedasGen.Item(m_oProcesoSeleccionado.MonCod).EQ_ACT = False And oMonedasGen.Item(sdbcMonCod.Text).EQ_ACT = False Then
                    txtCambio.Backcolor = RGB(255, 255, 192)
                    txtCambio.Locked = True
                    txtCambio.TabStop = False
                Else
                    txtCambio.Backcolor = RGB(255, 255, 255)
                    txtCambio.Locked = True
                    txtCambio.TabStop = True
                End If
                If UCase(m_oProcesoSeleccionado.MonCod) = UCase(Trim(sdbcMonCod.Text)) Then
                    txtCambio.Backcolor = RGB(255, 255, 192)
                    txtCambio.Locked = True
                    txtCambio.TabStop = False
                End If
            End If
        End If
        
        If Not .Usuario Is Nothing Then
            Me.lblOrigen.Visible = True
            If .portal Then
                If .Usuario.Persona Is Nothing Then
                    Me.lblOrigen.caption = sIdiIntroPortal
                Else
                    Me.lblOrigen.caption = sIdiIntroPortal & " " & sIdiContacto & " " & .Usuario.Persona.nombre
                End If
            Else
                If .Usuario.Persona Is Nothing Then
                    If .Usuario.Cod = " " Then
                        Me.lblOrigen.caption = sIdiIntroGS
                    Else
                        Me.lblOrigen.caption = sIdiIntroGS & " " & sIdiUsuario & " " & .Usuario.Cod
                    End If
                Else
                    Me.lblOrigen.caption = sIdiIntroGS & " " & sIdiUsuario & " " & .Usuario.Persona.nombre
                End If
            End If
        Else
            Me.lblOrigen.Visible = False
        End If
        
        bRespetarCombo = True
        txtObs = NullToStr(.obs)
        bRespetarCombo = False
    End With
    
        
    If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
        If g_oOfertaSeleccionada.AntesDeCierreParcial Then
            cmdModificar.Enabled = False
            cmdEliminar.Enabled = False
        Else
            cmdModificar.Enabled = True
            cmdEliminar.Enabled = True
        End If
    ElseIf m_oProcesoSeleccionado.Estado >= conadjudicaciones Then
        cmdModificar.Enabled = False
        cmdEliminar.Enabled = False
    Else
        cmdModificar.Enabled = True
        cmdEliminar.Enabled = True
    End If
    
    bPermitirEliminar = True
    If m_oProcesoSeleccionado.AdminPublica Then
        For Each oSobre In m_oProcesoSeleccionado.Sobres
            If oSobre.Estado = 1 Then
                bPermitirEliminar = False
                Set oSobre = Nothing
                Exit For
            End If
        Next
    End If
    If Not bPermitirEliminar Then
        cmdModificar.Enabled = False
        cmdEliminar.Enabled = False
    End If
    
    '''''''''''********** Permisos para eliminar **********************
    bEliminar = PermisoEliminarOferta
    If bEliminar = True Then
        cmdEliminar.Visible = True
        cmdModificar.Left = cmdAdjunMasivos.Left + cmdAdjunMasivos.Width + 120
    Else
        cmdEliminar.Visible = False
        cmdAdjunMasivos.Left = cmdEliminar.Left
        cmdModificar.Left = cmdAdjunMasivos.Left + cmdAdjunMasivos.Width + 120
    End If
    cmdRestaurar(1).Left = cmdModificar.Left + cmdModificar.Width + 120
    cmdListado(1).Left = cmdRestaurar(1).Left + cmdRestaurar(1).Width + 120
        
    '''''''''''********** Permisos de modificaci�n **********************
    cmdModificar.Visible = True
    cmdRestaurar(1).Left = cmdModificar.Left + cmdModificar.Width + 120
    cmdListado(1).Left = cmdRestaurar(1).Left + cmdRestaurar(1).Width + 120

    MostrarApartado iAptDatosGenOferta, IIf(g_bAnyadirOfertaObl, True, False)
    
    g_oOfertaSeleccionada.MarcarComoLeida basOptimizacion.gCodPersonaUsuario

    On Error Resume Next
    frmEST.ComprobarNuevasOfertas
    Me.Show
    On Error GoTo 0
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Muestra los atributos de proceso de una oferta
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cmdRestaurar_Click;tvwProce.Node_click; Tiempo m�ximo:0</remarks>

Private Sub MostrarAtributosOfe()
Dim oAtr As CAtributo
Dim bModificar As Boolean
Dim sCod As String

    MostrarApartado iAptAtributos, IIf(g_bAnyadirOfertaObl, True, False)
    
    If g_bAnyadirOfertaObl Then bModoEdicion = True
    
    lblAtributosProceso.caption = m_sIdiAtribProce
    
    sdbgAtributos.RemoveAll
    
    If Not g_oOfertaSeleccionada.AtribProcOfertados Is Nothing Then
        For Each oAtr In m_oProcesoSeleccionado.ATRIBUTOS
            If oAtr.Obligatorio Then
                sCod = "(*) " & oAtr.Cod
            Else
                sCod = oAtr.Cod
            End If
        
            If Not g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(oAtr.idAtribProce)) Is Nothing Then
                sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & MostrarAtributo(g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(oAtr.idAtribProce)), oAtr.Tipo, m_sIdiFalse, m_sIdiTrue) & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Id & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
            Else
                sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Id & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
            End If
        Next
    Else
        For Each oAtr In m_oProcesoSeleccionado.ATRIBUTOS
            If oAtr.Obligatorio Then
                sCod = "(*) " & oAtr.Cod
            Else
                sCod = oAtr.Cod
            End If
        
            sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Id & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
        Next
    End If
    
    bModificar = PermisoModificarOferta
    If bModificar = True Then
        cmdModoEdicion.Visible = True
    Else
        cmdModoEdicion.Visible = False
    End If
End Sub

''' <summary>
''' Muestra los atributos de un grupo  de una oferta
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cmdRestaurar_Click;tvwProce.Node_click; Tiempo m�ximo:0</remarks>
Private Sub MostrarAtributosGrupoOfe()
Dim oAtr As CAtributo
Dim bModificar As Boolean
Dim sCod As String

    sdbgAtributos.RemoveAll
    
    MostrarApartado iAptAtributos, IIf(g_bAnyadirOfertaObl, True, False)
    If g_bAnyadirOfertaObl Then
        bModoEdicion = True
    End If
    lblAtributosProceso.caption = m_sIdiAtribGrupo & "  " & m_oGrupoSeleccionado.Den
    
    If Not g_oGrupoOferSeleccionado.AtribOfertados Is Nothing Then
        For Each oAtr In m_oGrupoSeleccionado.ATRIBUTOS
            If oAtr.Obligatorio Then
                sCod = "(*) " & oAtr.Cod
            Else
                sCod = oAtr.Cod
            End If
            If Not g_oGrupoOferSeleccionado.AtribOfertados.Item(CStr(oAtr.idAtribProce)) Is Nothing Then
                sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & MostrarAtributo(g_oGrupoOferSeleccionado.AtribOfertados.Item(CStr(oAtr.idAtribProce)), oAtr.Tipo, m_sIdiFalse, m_sIdiTrue) & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Id & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
            Else
                sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Id & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
            End If
        Next
    Else
        For Each oAtr In m_oGrupoSeleccionado.ATRIBUTOS
            If oAtr.Obligatorio Then
                sCod = "(*) " & oAtr.Cod
            Else
                sCod = oAtr.Cod
            End If
            sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & oAtr.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtr.Tipo & Chr(m_lSeparador) & oAtr.TipoIntroduccion & Chr(m_lSeparador) & oAtr.Id & Chr(m_lSeparador) & oAtr.idAtribProce & Chr(m_lSeparador) & oAtr.Obligatorio
        Next
    End If
    
    bModificar = PermisoModificarOferta
    If bModificar = True Then
        cmdModoEdicion.Visible = True
    Else
        cmdModoEdicion.Visible = False
    End If
End Sub

''' <summary>Muestra los adjuntos de la oferta</summary>
''' <remarks>Llamada desde: cmdRestaurar_Click, tvwProce_NodeClick; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 15/06/2011</revision>
Private Sub MostrarAdjuntosOfe()
    Dim oAdjun As CAdjunto
    Dim teserror As TipoErrorSummit
    Dim bModificar As Boolean
    
    If Not g_oOfertaSeleccionada Is Nothing Then
        g_oOfertaSeleccionada.MarcarComoLeida basOptimizacion.gCodPersonaUsuario
    End If

    MostrarApartado iAptAdjuntos
    lblAdjuntos.caption = m_sIdiAdjunProce
    sdbgAdjun.RemoveAll
    
    bRespetarCombo = True
    txtObsAdjun.Text = NullToStr(g_oOfertaSeleccionada.ObsAdjun)
    bRespetarCombo = False
    
    For Each oAdjun In g_oOfertaSeleccionada.Adjuntos
        sdbgAdjun.AddItem oAdjun.Id & Chr(m_lSeparador) & oAdjun.nombre & Chr(m_lSeparador) & TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb & Chr(m_lSeparador) & NullToStr(oAdjun.Comentario)
    Next

    'Comprobamos que se pueda editar la oferta
    bModificar = PermisoModificarOferta
    If bModificar = True Then
        teserror = g_oOfertaSeleccionada.OfertaModificable
        If teserror.NumError <> TESnoerror Then
            txtObsAdjun.Locked = True
            cmdA�adirAdjun.Visible = False
            cmdEliminarAdjun.Visible = False
            cmdModificarAdjun.Visible = False
        Else
            txtObsAdjun.Locked = False
            cmdA�adirAdjun.Visible = True
            cmdEliminarAdjun.Visible = True
            cmdModificarAdjun.Visible = True
        End If
    Else
        cmdA�adirAdjun.Visible = False
        cmdEliminarAdjun.Visible = False
        cmdModificarAdjun.Visible = False
        txtObsAdjun.Locked = True
    End If
End Sub

''' <summary>Muestra los adjuntos de un grupo</summary>
''' <remarks>Llamada desde: cmdRestaurar_Click, tvwProce_NodeClick; Tiempo m�ximo:0,1</remarks>
''' <revision>LTG 15/06/2011</revision>
Private Sub MostrarAdjuntosGrupoOfe()
    Dim oAdjun As CAdjunto
    Dim teserror As TipoErrorSummit
    Dim bModificar As Boolean

    MostrarApartado iAptAdjuntos
    lblAdjuntos.caption = m_sIdiAdjunGrupo & "  " & m_oGrupoSeleccionado.Den
    sdbgAdjun.RemoveAll
    
    bRespetarCombo = True
    txtObsAdjun.Text = NullToStr(g_oGrupoOferSeleccionado.ObsAdjun)
    bRespetarCombo = False
    
    For Each oAdjun In g_oGrupoOferSeleccionado.Adjuntos
        sdbgAdjun.AddItem oAdjun.Id & Chr(m_lSeparador) & oAdjun.nombre & Chr(m_lSeparador) & TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb & Chr(m_lSeparador) & NullToStr(oAdjun.Comentario)
    Next

    'Comprobamos que se pueda editar la oferta
    bModificar = PermisoModificarOferta
    If bModificar = True Then
        teserror = g_oOfertaSeleccionada.OfertaModificable
        If teserror.NumError <> TESnoerror Then
            txtObsAdjun.Locked = True
            cmdA�adirAdjun.Visible = False
            cmdEliminarAdjun.Visible = False
            cmdModificarAdjun.Visible = False
        Else
            txtObsAdjun.Locked = False
            cmdA�adirAdjun.Visible = True
            cmdEliminarAdjun.Visible = True
            cmdModificarAdjun.Visible = True
        End If
    Else
        cmdA�adirAdjun.Visible = False
        cmdEliminarAdjun.Visible = False
        cmdModificarAdjun.Visible = False
        txtObsAdjun.Locked = True
    End If
End Sub

Private Sub MostrarItemsGrupoOfe()
Dim dblPresGrupo As Double

    If g_oOfertaSeleccionada Is Nothing Then Exit Sub
    
    g_oOfertaSeleccionada.MarcarComoLeida basOptimizacion.gCodPersonaUsuario
    
    If Not m_oProveSeleccionado Is Nothing Then
        Me.caption = m_sMeCaption & "                         " & sIdiProve & " " & m_oProveSeleccionado.Den & "      " & sIdiOferta & ": " & g_oOfertaSeleccionada.Num
    End If

    If m_bGrupoConEscalados Then
        LimpiarPreciosEscalados
        ConfigurarGridItemsEscalados (1)
        dblPresGrupo = Rellenar_sdbgPreciosEscalados
    Else
        LimpiarPrecios
        ConfigurarGridItems (1)
        dblPresGrupo = Rellenar_sdbgPrecios
    End If
    
    If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
        If m_bGrupoConEscalados Then
            If sdbgPreciosEscalados.Rows = 0 Then
                ChkMostrarItems.Visible = False
            Else
                ChkMostrarItems.Visible = True
            End If
        Else
            If sdbgPrecios.Rows = 0 Then
                ChkMostrarItems.Visible = False
            Else
                ChkMostrarItems.Visible = True
            End If
        End If
    Else
        ChkMostrarItems.Visible = False
    End If
    
    sdbcGrupo.Text = m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
    lblPresGrupo(1).Visible = False
    lblCPresGrupo(1).Visible = False
    lblPresGrupo(0).Visible = True
    lblCPresGrupo(0).Visible = True
    lblPresGrupo(0).caption = FormateoNumerico(dblPresGrupo)
        
    cmdModoEdicion.Visible = True

    MostrarApartado iAptItems

    Screen.MousePointer = vbNormal
End Sub


''' <summary>
''' Introducir los precios de los escalados
''' </summary>
''' <returns>Presupuesto</returns>
''' <remarks>Llamada desde: chkMostrarItems_Click; sdbgPreciosEscalados_HeadClick;
''' MostrarItemsOfe(); MostrarItemsGrupoOfe(); Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 02/11/2011</revision>
Private Function Rellenar_sdbgPreciosEscalados() As Double
    Dim oatrib  As CAtributo
    Dim oAtributos  As CAtributos
    Dim oPrecio As CPrecioItem
    Dim oPrecios As CPrecioItems
    Dim iPosition As Integer
    Dim sFilaGrid As String
    Dim dblPres As Double
    Dim iCerrado As Integer
    Dim oEscalado As CEscalado
    Dim iGroupIndex
    Dim oGrupoOfe As COfertaGrupo
    Dim oLinea As CPrecioItem
   
    dblPres = 0
    iPosition = 0
    iGroupIndex = 0
    
    If g_oGrupoOferSeleccionado Is Nothing Then
        Set oPrecios = oFSGSRaiz.Generar_CPrecioItems
        For Each oGrupoOfe In g_oOfertaSeleccionada.Grupos
            If oGrupoOfe.Lineas Is Nothing Then
                oGrupoOfe.CargarPrecios
            End If
            For Each oLinea In oGrupoOfe.Lineas
                oPrecios.AddPrecio oLinea
            Next
        Next
        Set oAtributos = m_oProcesoSeleccionado.AtributosItem
    Else
        Set oPrecios = g_oGrupoOferSeleccionado.Lineas
        Set oAtributos = m_oGrupoSeleccionado.AtributosItem
    End If
    
                    
    For Each oPrecio In oPrecios
        With oPrecio
            If .Cerrado Then
                iCerrado = 1
            Else
                iCerrado = 0
            End If
            
            If bMostrarTodosItems Or (Not bMostrarTodosItems And Not .Cerrado) Then
                sFilaGrid = NullToStr(.ArticuloCod)
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .Descr & Chr(m_lSeparador) & .Id & Chr(m_lSeparador) & .FechaInicioSuministro & Chr(m_lSeparador) & .FechaFinSuministro & Chr(m_lSeparador) & .DestCod
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .UniCod & Chr(m_lSeparador) & .Cantidad & Chr(m_lSeparador) & .PrecioApertura & Chr(m_lSeparador) & .PagCod & Chr(m_lSeparador) & ""
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & CStr(iCerrado)
                
                If Not oPrecio.Escalados Is Nothing Then
                    dblPres = dblPres + NullToDbl0(.Presupuesto)
                    
                    For Each oEscalado In oPrecio.Escalados
                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & oEscalado.Presupuesto & Chr(m_lSeparador) & oEscalado.Precio
                        
                        If Not oAtributos Is Nothing Then
                            For Each oatrib In oAtributos
                                If oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "" Then
                                    If .AtribOfertados Is Nothing Then
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                    ElseIf .AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                    ElseIf .AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                                    Else
                                        sFilaGrid = sFilaGrid & Chr(m_lSeparador) & MostrarEscaladoAtributo(.AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)).Escalados.Item(CStr(oEscalado.Id)))
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
                'Los atributos de caracteristica
                If Not oAtributos Is Nothing Then
                    For Each oatrib In oAtributos
                        If Not (oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "") Then
                            If .AtribOfertados Is Nothing Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                            ElseIf .AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                            Else
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & MostrarAtributo(.AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)), oatrib.Tipo, m_sIdiFalse, m_sIdiTrue)
                            End If
                        End If
                      Next
                End If
                If m_oProcesoSeleccionado.AdjuntarAItems Then
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador)
                End If
            End If
        End With
        
        sdbgPreciosEscalados.AddItem sFilaGrid
    Next
        
    Set oPrecios = Nothing
    Set oAtributos = Nothing
    
    Rellenar_sdbgPreciosEscalados = dblPres
End Function

Public Function MostrarEscaladoAtributo(ByVal oEscalado As CEscalado) As Variant
    MostrarEscaladoAtributo = oEscalado.valorNum
End Function

Private Function Rellenar_sdbgPrecios() As Double
Dim oatrib  As CAtributo
Dim oAtributos  As CAtributos
Dim oPrecio As CPrecioItem
Dim oPrecios As CPrecioItems
Dim sFilaGrid As String
Dim dblPres As Double
Dim iCerrado As Integer
Dim iAnyoImputacion As Integer
Dim oItem As CGrupo
Dim i As Integer
    dblPres = 0
    
    If g_oGrupoOferSeleccionado Is Nothing Then
        Set oPrecios = g_oOfertaSeleccionada.Lineas
        Set oAtributos = m_oProcesoSeleccionado.AtributosItem
    Else
        Set oPrecios = g_oGrupoOferSeleccionado.Lineas
        Set oAtributos = m_oGrupoSeleccionado.AtributosItem
    End If
    
    If bMostrarTodosItems = True Then

        For Each oPrecio In oPrecios
            If m_oProcesoSeleccionado.Grupos.Item(oPrecio.grupoCod).Items Is Nothing Then m_oProcesoSeleccionado.CargarTodosLosItemsGrupos bActivoSM:=(Not g_oParametrosSM Is Nothing)
            iAnyoImputacion = Obtener_AnyoImputacion_Item(m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.GMN1Cod, m_oProcesoSeleccionado.Cod, NullToDbl0(oPrecio.Id), NullToDbl0(m_oProcesoSeleccionado.Grupos.Item(oPrecio.grupoCod).Items.Item(CStr(oPrecio.Id)).SolicitudId), NullToDbl0(m_oProcesoSeleccionado.Grupos.Item(oPrecio.grupoCod).Items.Item(CStr(oPrecio.Id)).IdLineaSolicit))
            If iAnyoImputacion > 0 Then
                sdbgPrecios.Columns("ANYOIMPUTACION").Visible = True
            End If
            
            With oPrecio
                If .Cerrado Then
                    iCerrado = 1
                Else
                    iCerrado = 0
                End If
                sFilaGrid = NullToStr(.ArticuloCod) & Chr(m_lSeparador) & .Descr & Chr(m_lSeparador) & .Id & Chr(m_lSeparador) & .FechaInicioSuministro & Chr(m_lSeparador) & .FechaFinSuministro & Chr(m_lSeparador) & .DestCod
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .UniCod & Chr(m_lSeparador) & .Cantidad & Chr(m_lSeparador) & .PrecioApertura & Chr(m_lSeparador) & .PagCod & Chr(m_lSeparador) & .Precio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & .Precio2 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & .Precio3 & Chr(m_lSeparador) & ""
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .CantidadMaxima & Chr(m_lSeparador) & CStr(iCerrado)
                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_sIdiUsarPrec(.Usar) & Chr(m_lSeparador) & IIf(iAnyoImputacion = 0, "", iAnyoImputacion)
                If Not oAtributos Is Nothing Then
                    For Each oatrib In oAtributos
                        If .AtribOfertados Is Nothing Then
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                        ElseIf .AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                        Else
                            sFilaGrid = sFilaGrid & Chr(m_lSeparador) & MostrarAtributo(.AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)), oatrib.Tipo, m_sIdiFalse, m_sIdiTrue)
                        End If
                      Next
                End If
                dblPres = dblPres + NullToDbl0(.Presupuesto)
            End With
            sdbgPrecios.AddItem sFilaGrid
        Next
        
    Else
        For Each oPrecio In oPrecios
            If m_oProcesoSeleccionado.Grupos.Item(oPrecio.grupoCod).Items Is Nothing Then m_oProcesoSeleccionado.CargarTodosLosItemsGrupos bActivoSM:=(Not g_oParametrosSM Is Nothing)
            iAnyoImputacion = Obtener_AnyoImputacion_Item(m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.GMN1Cod, m_oProcesoSeleccionado.Cod, NullToDbl0(oPrecio.Id), NullToDbl0(m_oProcesoSeleccionado.Grupos.Item(oPrecio.grupoCod).Items.Item(CStr(oPrecio.Id)).SolicitudId), NullToDbl0(m_oProcesoSeleccionado.Grupos.Item(oPrecio.grupoCod).Items.Item(CStr(oPrecio.Id)).IdLineaSolicit))
            If iAnyoImputacion > 0 Then
                sdbgPrecios.Columns("ANYOIMPUTACION").Visible = True
            End If
            With oPrecio
                If .Cerrado = False Then
                    sFilaGrid = NullToStr(.ArticuloCod) & Chr(m_lSeparador) & .Descr & Chr(m_lSeparador) & .Id & Chr(m_lSeparador) & .FechaInicioSuministro & Chr(m_lSeparador) & .FechaFinSuministro & Chr(m_lSeparador) & .DestCod
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .UniCod & Chr(m_lSeparador) & .Cantidad & Chr(m_lSeparador) & .PrecioApertura & Chr(m_lSeparador) & .PagCod & Chr(m_lSeparador) & .Precio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & .Precio2 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & .Precio3 & Chr(m_lSeparador) & ""
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & .CantidadMaxima & Chr(m_lSeparador) & "0"
                    sFilaGrid = sFilaGrid & Chr(m_lSeparador) & m_sIdiUsarPrec(.Usar) & Chr(m_lSeparador) & IIf(iAnyoImputacion = 0, "", iAnyoImputacion)
                    If Not oAtributos Is Nothing Then
                        For Each oatrib In oAtributos
                            If .AtribOfertados Is Nothing Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                            ElseIf .AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & ""
                            Else
                                sFilaGrid = sFilaGrid & Chr(m_lSeparador) & MostrarAtributo(.AtribOfertados.Item(CStr(.Id) & "$" & CStr(oatrib.idAtribProce)), oatrib.Tipo, m_sIdiFalse, m_sIdiTrue)
                            End If
                        Next
                    End If
                    dblPres = dblPres + NullToDbl0(.Presupuesto)
                    sdbgPrecios.AddItem sFilaGrid
                End If
            End With
        Next
        
    End If
    
    Set oPrecios = Nothing
    Set oAtributos = Nothing
    
    Rellenar_sdbgPrecios = dblPres
    
End Function



Private Sub MostrarApartado(idx As Integer, Optional ByVal bEdicion As Boolean)
Dim i As Integer
Dim iNav As Integer

For i = 0 To Me.picApartado.UBound
    Me.picApartado(i).Visible = IIf(idx = i, True, False)
Next i
If idx = 4 Then
    m_bMouse = True
Else
    m_bMouse = False
End If
If Not bEdicion Then
'Los botones
    Select Case idx
    Case 0, 6, 3
        iNav = 0
    Case 1
        iNav = 1
    Case 2, 4
        iNav = 2
    Case 5
        iNav = 3
    Case 7
        iNav = 4
    End Select
    For i = 0 To 4
        picNavigate(i).Visible = IIf(iNav = i, True, False)
    Next i
End If
End Sub


Private Function IsValorAtributo(ByVal oValorAtrib As CAtributoOfertado) As Boolean

IsValorAtributo = False

Select Case oValorAtrib.objeto.Tipo
    Case TiposDeAtributos.TipoString
        If Not IsNull(oValorAtrib.valorText) And Not IsMissing(oValorAtrib.valorText) And Not IsEmpty(oValorAtrib.valorText) Then
            If CStr(oValorAtrib.valorText) <> "" Then IsValorAtributo = True
        End If
    Case TiposDeAtributos.TipoNumerico
        If Not IsNull(oValorAtrib.valorNum) And Not IsMissing(oValorAtrib.valorNum) And Not IsEmpty(oValorAtrib.valorNum) Then
            If IsNumeric(oValorAtrib.valorNum) Then IsValorAtributo = True
        End If
    Case TiposDeAtributos.TipoFecha
        If Not IsNull(oValorAtrib.valorFec) And Not IsMissing(oValorAtrib.valorFec) And Not IsEmpty(oValorAtrib.valorFec) Then
            If IsDate(oValorAtrib.valorFec) Then IsValorAtributo = True
        End If
    Case TiposDeAtributos.TipoBoolean
        If Not IsNull(oValorAtrib.valorBool) And Not IsMissing(oValorAtrib.valorBool) And Not IsEmpty(oValorAtrib.valorBool) Then
            Select Case oValorAtrib.valorBool
            Case 0, False
                IsValorAtributo = True
            Case 1, True
                IsValorAtributo = True
            Case Else
                IsValorAtributo = False
            End Select
        End If
End Select

End Function

Private Sub sdbcGrupo_Click()
    sdbcGrupo.Value = ""
End Sub

Private Sub sdbcGrupo_CloseUp()
    Dim bUsarEscalados As Boolean
    
    bUsarEscalados = False
    If Not m_oGrupoSeleccionado Is Nothing Then
        bUsarEscalados = m_oGrupoSeleccionado.UsarEscalados
        
        If sdbcGrupo.Columns(1).Value = m_sIdiTodosItems Then
            If bUsarEscalados And Not m_oProcesoSeleccionado.Escalados Then
                'Como los escalados son diferentes no se pueden mostrar todos los �tems
                sdbcGrupo.Value = m_oGrupoSeleccionado.Codigo & " - " & m_oGrupoSeleccionado.Den
                oMensajes.ItemsDistintosEscalados ' No se pueden mostrar todos los items. Los grupos tienen distintos escalados.
                Exit Sub
            End If
        End If
        If m_oGrupoSeleccionado.Codigo = sdbcGrupo.Columns(0).Value Then
            sdbcGrupo.Value = sdbcGrupo.Columns(0).Value & " - " & sdbcGrupo.Columns(1).Value
            Exit Sub
        End If
    End If
    
    If m_bGrupoConEscalados Then
        sdbgPreciosEscalados.RemoveAll
    Else
        sdbgPrecios.RemoveAll
    End If
    If sdbcGrupo.Columns(0).Value = "**********" Then
        sdbcGrupo.Value = sdbcGrupo.Columns(1).Value
        Set m_oGrupoSeleccionado = Nothing
        Set g_oGrupoOferSeleccionado = Nothing
        If g_oOfertaSeleccionada.Lineas Is Nothing Then
            Set g_oOfertaSeleccionada.proceso = m_oProcesoSeleccionado
            m_oProcesoSeleccionado.CargarAtributos ambito:=AmbItem, vGrupo:="#"
            g_oOfertaSeleccionada.CargarPrecios
        Else
            If g_oOfertaSeleccionada.Lineas.Count = 0 Then
                Set g_oOfertaSeleccionada.proceso = m_oProcesoSeleccionado
                m_oProcesoSeleccionado.CargarAtributos ambito:=AmbItem, vGrupo:="#"
                g_oOfertaSeleccionada.CargarPrecios
            End If
        End If
        If bUsarEscalados Then
            m_bGrupoConEscalados = True
            sdbgPrecios.Visible = False
            sdbgPreciosEscalados.Visible = True
        Else
            m_bGrupoConEscalados = False
            sdbgPreciosEscalados.Visible = False
            sdbgPrecios.Visible = True
        End If
        
        DoEvents
        MostrarItemsOfe
    Else
        sdbcGrupo.Value = sdbcGrupo.Columns(0).Value & " - " & sdbcGrupo.Columns(1).Value
        Set m_oGrupoSeleccionado = g_oOfertaSeleccionada.Grupos.Item(sdbcGrupo.Columns(0).Value).Grupo
        Set g_oGrupoOferSeleccionado = g_oOfertaSeleccionada.Grupos.Item(sdbcGrupo.Columns(0).Value)
        If g_oGrupoOferSeleccionado.Lineas Is Nothing Then
            m_oGrupoSeleccionado.CargarAtributos ambito:=AmbItem
            g_oGrupoOferSeleccionado.CargarPrecios
        Else
            If g_oGrupoOferSeleccionado.Lineas.Count = 0 Then
                m_oGrupoSeleccionado.CargarAtributos ambito:=AmbItem
                g_oGrupoOferSeleccionado.CargarPrecios
            End If
        End If
        If m_oGrupoSeleccionado.UsarEscalados Then
            m_bGrupoConEscalados = True
            sdbgPrecios.Visible = False
            sdbgPreciosEscalados.Visible = True
        Else
            m_bGrupoConEscalados = False
            sdbgPreciosEscalados.Visible = False
            sdbgPrecios.Visible = True
        End If

        DoEvents
        MostrarItemsGrupoOfe
    End If
End Sub

Private Sub sdbcGrupo_InitColumnProps()
    sdbcGrupo.DataFieldList = "Column 0"
    sdbcGrupo.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGrupo_PositionList(ByVal Text As String)
PositionList sdbcGrupo, Text
End Sub


''' <summary>
''' A�ade una nueva oferta al arbol
''' </summary>
''' <param name="iOferta">numero de oferta</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: AnyadirOfertaProve;  Tiempo m�ximo:0</remarks>

Private Sub AnyadirOfertaATree(ByVal iOferta As Integer)
Dim nodx As MSComctlLib.node
Dim nody As MSComctlLib.node
Dim Nodz As MSComctlLib.node
Dim sCodOfe As String
Dim oGrupo As CGrupo
Dim sCod As String
Dim i As Integer
Dim bBrot As Boolean
Dim bCargar As Boolean
Dim scodProve As String

If Not m_oProcesoSeleccionado.AtributosOfeObl Then
    'Se a�ade la oferta al arbol
    sCodOfe = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod)) & CStr(iOferta)
    If tvwProce.selectedItem.Children > 0 Then 'Hay ofertas para en proveedor
        bBrot = True
        Set nodx = tvwProce.Nodes.Add(tvwProce.selectedItem.Child.key, tvwFirst, "O_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), sIdiOferta & CStr(iOferta), "Oferta")
    Else
        bBrot = False
        Set nodx = tvwProce.Nodes.Add(tvwProce.selectedItem.key, tvwChild, "O_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), sIdiOferta & CStr(iOferta), "Oferta")
    End If
    nodx.Tag = "OF_" & sCodOfe
    
    If m_oOfertasProceso.Count = 0 Or tvwProce.selectedItem.Children < 2 Then 'Si solo hay mi nueva oferta
        m_oProcesoSeleccionado.CargarAtributos
        If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
            Set oOfertaAA�adir.AtribProcOfertados = oFSGSRaiz.Generar_CAtributosOfertados
            oOfertaAA�adir.NumAtribOfer = 0
            Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "ATOFE_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), m_sIdiAtributos, "AtributosGris")
            nody.Tag = "OT_" & sCodOfe
        End If
    Else
        sCod = Right(nodx.Next.Tag, Len(nodx.Next.Tag) - 3)
        If Not m_oOfertasProceso.Item(sCod).AtribProcOfertados Is Nothing Then
            Set oOfertaAA�adir.AtribProcOfertados = m_oOfertasProceso.Item(sCod).AtribProcOfertados
            Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "ATOFE_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), m_sIdiAtributos, "Atributos")
            nody.Tag = "OT_" & sCodOfe
            oOfertaAA�adir.NumAtribOfer = m_oOfertasProceso.Item(sCod).NumAtribOfer
            If m_oOfertasProceso.Item(sCod).NumAtribOfer = 0 Then
                nody.Image = "AtributosGris"
            End If
        End If
    End If
    
    If Not m_bGrupoConEscalados Then
        If m_oProcesoSeleccionado.AdjuntarAOfertas Then
            Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "ADJUNOFE_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), m_sIdiAdjuntos, "Ficheros")
            nody.Tag = "OA_" & sCodOfe
            If m_oOfertasProceso.Count = 0 Or tvwProce.selectedItem.Children < 2 Then 'Si solo hay mi nueva oferta
                nody.Image = "FicherosGris"
                oOfertaAA�adir.NumAdjuntos = 0
            Else
                oOfertaAA�adir.NumAdjuntos = m_oOfertasProceso.Item(sCod).NumAdjuntos
                If m_oOfertasProceso.Item(sCod).NumAdjuntos = 0 And (m_oOfertasProceso.Item(sCod).ObsAdjun = "" Or IsNull(m_oOfertasProceso.Item(sCod).ObsAdjun)) Then
                    nody.Image = "FicherosGris"
                End If
            End If
        
        End If
    End If
    Set oOfertaAA�adir.Grupos = oFSGSRaiz.Generar_COfertaGrupos
    For Each oGrupo In m_oProcesoSeleccionado.Grupos
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod))
            If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            Else
                bCargar = True
            End If
        Else
            bCargar = True
        End If
        If bCargar Then
            Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "GRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, oGrupo.Codigo & " - " & oGrupo.Den, "Grupo")
            nody.Tag = "GR_" & oGrupo.Codigo & "_$$$_" & sCodOfe
            oOfertaAA�adir.Grupos.Add oOfertaAA�adir, oGrupo
            If m_oOfertasProceso.Count = 0 Or tvwProce.selectedItem.Children < 2 Then
                oGrupo.CargarAtributos ambito:=AmbGrupo
                If oGrupo.ATRIBUTOS.Count > 0 Then
                    Set oOfertaAA�adir.AtribGrOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                    Set Nodz = tvwProce.Nodes.Add(nody.key, tvwChild, "ATGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, m_sIdiAtributos, "AtributosGris")
                    Nodz.Tag = "GT_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                End If
            Else
                If Not m_oOfertasProceso.Item(sCod).AtribGrOfertados Is Nothing Then
                    Set oOfertaAA�adir.AtribGrOfertados = m_oOfertasProceso.Item(sCod).AtribGrOfertados
                    oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumAtribOfer = m_oOfertasProceso.Item(sCod).Grupos.Item(oGrupo.Codigo).NumAtribOfer
                    Set Nodz = tvwProce.Nodes.Add(nody.key, tvwChild, "ATGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, m_sIdiAtributos, "Atributos")
                    Nodz.Tag = "GT_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                    If oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumAtribOfer = 0 Then
                        Nodz.Image = "AtributosGris"
                    End If
                End If
            End If
            If m_oProcesoSeleccionado.AdjuntarAGrupos Then
                Set Nodz = tvwProce.Nodes.Add(nody.key, tvwChild, "ADJUNGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, m_sIdiAdjuntos, "Ficheros")
                Nodz.Tag = "GA_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                If m_oOfertasProceso.Count = 0 Or tvwProce.selectedItem.Children < 2 Then 'Si solo hay mi nueva oferta
                    Nodz.Image = "FicherosGris"
                    oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumAdjuntos = 0
                Else
                    oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumAdjuntos = m_oOfertasProceso.Item(sCod).Grupos.Item(oGrupo.Codigo).NumAdjuntos
                    If m_oOfertasProceso.Item(sCod).Grupos.Item(oGrupo.Codigo).NumAdjuntos = 0 And (m_oOfertasProceso.Item(sCod).Grupos.Item(oGrupo.Codigo).ObsAdjun = "" Or IsNull(m_oOfertasProceso.Item(sCod).Grupos.Item(oGrupo.Codigo).ObsAdjun)) Then
                        Nodz.Image = "FicherosGris"
                    End If
                End If
            End If
        
            Set Nodz = tvwProce.Nodes.Add(nody.key, tvwChild, "ITEMSGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, m_sIdiItems, "Item")
            Nodz.Tag = "GI_" & oGrupo.Codigo & "_$$$_" & sCodOfe
            If m_oOfertasProceso.Count = 0 Or tvwProce.selectedItem.Children < 2 Then 'Si solo hay mi nueva oferta
                Nodz.Image = "ItemGris"
                oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumPrecios = 0
            Else
                oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumPrecios = m_oOfertasProceso.Item(sCod).Grupos.Item(oGrupo.Codigo).NumPrecios
                If oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumPrecios = 0 Then
                    Nodz.Image = "ItemGris"
                End If
            End If
        End If
    Next

    
    Set g_oOfertaSeleccionada = oOfertaAA�adir
    nodx.Selected = True
    
    
    Set nodx = Nothing
    Set nody = Nothing
    Set Nodz = Nothing
    
Else
    Dim bExpandirRaiz As Boolean
    'Se a�ade la oferta al arbol
    sCodOfe = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod)) & CStr(iOferta)
    If tvwProce.selectedItem.Children > 0 Then 'Hay ofertas para en proveedor
        bBrot = True
        Set nodx = tvwProce.Nodes.Add(tvwProce.selectedItem.Child.key, tvwFirst, "O_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), sIdiOferta & CStr(iOferta), "Oferta")
    Else
        bBrot = False
        Set nodx = tvwProce.Nodes.Add(tvwProce.selectedItem.key, tvwChild, "O_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), sIdiOferta & CStr(iOferta), "Oferta")
    End If
    nodx.Tag = "OF_" & sCodOfe
    
    m_sPathNuevoNodo = nodx.FullPath
    
    If m_oOfertasProceso.Count = 0 Or tvwProce.selectedItem.Children < 2 Then 'Si solo hay mi nueva oferta
        m_oProcesoSeleccionado.CargarAtributos
        If Not m_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
            Set oOfertaAA�adir.AtribProcOfertados = oFSGSRaiz.Generar_CAtributosOfertados
            oOfertaAA�adir.NumAtribOfer = 0
            Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "ATOFE_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), m_sIdiAtributos, "AtributosGris")
            nody.Tag = "OT_" & sCodOfe
            
            For i = 1 To m_oProcesoSeleccionado.ATRIBUTOS.Count
                If m_oProcesoSeleccionado.ATRIBUTOS.Item(i).Obligatorio Then
                        bExpandirRaiz = True
                        Exit For
                End If
            Next
        End If
    Else
        sCod = Right(nodx.Next.Tag, Len(nodx.Next.Tag) - 3)
        If Not m_oOfertasProceso.Item(sCod).AtribProcOfertados Is Nothing Then
            Set oOfertaAA�adir.AtribProcOfertados = m_oOfertasProceso.Item(sCod).AtribProcOfertados
            Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "ATOFE_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta), m_sIdiAtributos, "Atributos")
            nody.Tag = "OT_" & sCodOfe
            oOfertaAA�adir.NumAtribOfer = m_oOfertasProceso.Item(sCod).NumAtribOfer
            If m_oOfertasProceso.Item(sCod).NumAtribOfer = 0 Then
                nody.Image = "AtributosGris"
            End If
            
            For i = 1 To m_oProcesoSeleccionado.ATRIBUTOS.Count
                If m_oProcesoSeleccionado.ATRIBUTOS.Item(i).Obligatorio Then
                        bExpandirRaiz = True
                        Exit For
                End If
            Next
            
        End If
    End If
    
    If bExpandirRaiz Then
        nody.Parent.Expanded = True
    End If
    
    Set oOfertaAA�adir.Grupos = oFSGSRaiz.Generar_COfertaGrupos
    For Each oGrupo In m_oProcesoSeleccionado.Grupos
    
        bExpandirRaiz = False
        
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = m_oProveSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(m_oProveSeleccionado.Cod))
            If m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            Else
                bCargar = True
            End If
        Else
            bCargar = True
        End If
        If bCargar Then
            If m_oOfertasProceso.Count = 0 Or tvwProce.selectedItem.Children < 2 Then
                
                oGrupo.CargarAtributos ambito:=AmbGrupo
                    For i = 1 To oGrupo.ATRIBUTOS.Count
                        If oGrupo.ATRIBUTOS.Item(i).Obligatorio Then
                                bExpandirRaiz = True
                                Exit For
                        End If
                    Next
                    
                If bExpandirRaiz Then
                    Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "GRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, oGrupo.Codigo & " - " & oGrupo.Den, "Grupo")
                    nody.Tag = "GR_" & oGrupo.Codigo & "_$$$_" & sCodOfe
    
                    oOfertaAA�adir.Grupos.Add oOfertaAA�adir, oGrupo
                    Set oOfertaAA�adir.AtribGrOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                    Set Nodz = tvwProce.Nodes.Add(nody.key, tvwChild, "ATGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, m_sIdiAtributos, "AtributosGris")
                    Nodz.Tag = "GT_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                    Nodz.Parent.Expanded = True

                End If
                
            Else
      
                If Not m_oOfertasProceso.Item(sCod).AtribGrOfertados Is Nothing Then
                     oGrupo.CargarAtributos ambito:=AmbGrupo
                    For i = 1 To oGrupo.ATRIBUTOS.Count
                        If oGrupo.ATRIBUTOS.Item(i).Obligatorio Then
                                bExpandirRaiz = True
                                Exit For
                        End If
                    Next
                    
                    If bExpandirRaiz Then
                        Set nody = tvwProce.Nodes.Add(nodx.key, tvwChild, "GRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, oGrupo.Codigo & " - " & oGrupo.Den, "Grupo")
                        nody.Tag = "GR_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                    
                        oOfertaAA�adir.Grupos.Add oOfertaAA�adir, oGrupo
                        Set oOfertaAA�adir.AtribGrOfertados = oFSGSRaiz.Generar_CAtributosOfertados
                        oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumAtribOfer = m_oOfertasProceso.Item(sCod).Grupos.Item(oGrupo.Codigo).NumAtribOfer
                        Set Nodz = tvwProce.Nodes.Add(nody.key, tvwChild, "ATGRUPO_" & m_oProveSeleccionado.Cod & "_" & CStr(iOferta) & "_" & oGrupo.Codigo, m_sIdiAtributos, "Atributos")
                        Nodz.Tag = "GT_" & oGrupo.Codigo & "_$$$_" & sCodOfe
                        If oOfertaAA�adir.Grupos.Item(oGrupo.Codigo).NumAtribOfer = 0 Then
                            Nodz.Image = "AtributosGris"
                        End If
                        
                        Nodz.Parent.Expanded = True
                    End If
                End If
            End If
        End If
        
    Next

    Set g_oOfertaSeleccionada = oOfertaAA�adir
    nodx.Selected = True
    
    Set nodx = Nothing
    Set nody = Nothing
    Set Nodz = Nothing
End If
End Sub

''' <summary>
''' Elimina oferta al arbol
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: cmdCancelar_click;cmdEliminar_click;  Tiempo m�ximo:0</remarks>

Private Sub QuitarOfertaDeTree()
Dim nodx As MSComctlLib.node
Dim nody As MSComctlLib.node

If g_bAnyadirOfertaObl Then
    
    Set nodx = tvwProce.selectedItem
    Dim i As Integer
    i = 5
    
    Dim sTag As String
    sTag = Left(nodx.Tag, 3)
    
    If sTag = "OF_" Then
        Set nody = nodx.Parent
    Else
        Set nodx = nodx.Parent
        sTag = Left(nodx.Tag, 3)
    
        If sTag = "OF_" Then
            Set nody = nodx.Parent
        Else
            Set nodx = nodx.Parent
            sTag = Left(nodx.Tag, 3)
            If sTag = "OF_" Then
                Set nody = nodx.Parent
            End If
        End If
    End If
    
    g_bAnyadirOfertaObl = False
Else
    Set nodx = tvwProce.selectedItem
    Set nody = nodx.Parent
End If

tvwProce.Nodes.Remove nodx.Index
nody.Selected = True
tvwProce_NodeClick nody

Set nodx = Nothing
Set nody = Nothing
End Sub

''' <summary>
''' Evento que salta al hacer cambios en el grid de atributos, Actualizar la fila en edicion
''' </summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila </param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:   Tiempo m�ximo:0</remarks>

Private Sub sdbgatributos_AfterUpdate(RtnDispErrMsg As Integer)

Dim teserror As TipoErrorSummit
Dim bGuardar As Boolean
Dim v As Variant
Dim dFechaAct As Date
Dim bSumar As Boolean
Dim bEliminarAdj As Boolean
Dim oUsar As CAtribTotalGrupo

    RtnDispErrMsg = 0

    If m_oAtribEnEdicion Is Nothing Then
        DoEvents
        sdbgatributos_Change
        DoEvents
    End If

    bModError = False
    bGuardar = True
    ''' Modificamos en la base de datos

    If sdbgAtributos.Columns("VALOR").Value = "" Then
        If sOperacion = "I" Then
            sOperacion = ""
            Set m_oAtribEnEdicion = Nothing
            Set m_oAtributo = Nothing
            bGuardar = False
        Else
            sOperacion = "D"
            m_oAtribEnEdicion.valorBool = Null
            m_oAtribEnEdicion.valorFec = Null
            m_oAtribEnEdicion.valorNum = Null
            m_oAtribEnEdicion.valorText = Null
        End If
    Else
        bSumar = False
        Select Case m_oAtributo.Tipo
        Case TiposDeAtributos.TipoString
            m_oAtribEnEdicion.valorText = CStr(sdbgAtributos.Columns("VALOR").Value)
        Case TiposDeAtributos.TipoNumerico
            m_oAtribEnEdicion.valorNum = CDbl(sdbgAtributos.Columns("VALOR").Value)
        Case TiposDeAtributos.TipoFecha
            m_oAtribEnEdicion.valorFec = CDate(sdbgAtributos.Columns("VALOR").Value)
        Case TiposDeAtributos.TipoBoolean
            If UCase(sdbgAtributos.Columns("VALOR").Value) = UCase(m_sIdiTrue) Then
                m_oAtribEnEdicion.valorBool = True
            Else
                m_oAtribEnEdicion.valorBool = False
            End If
        End Select
    End If
    
    If bGuardar Then
        bEliminarAdj = False
        If m_oAtributo.Tipo = TipoNumerico And Not IsNull(m_oAtributo.PrecioAplicarA) Then
            If m_oAtributo.PrecioAplicarA = TotalGrupo Then
                'es de �mbito grupo.Miramos en la tabla USAR_GR_ATRIB para ver si se est� aplicando
                If Not m_oAtributo.AtribTotalGrupo Is Nothing Then
                    For Each oUsar In m_oAtributo.AtribTotalGrupo
                        If oUsar.UsarPrec = 1 Then
                            bEliminarAdj = True
                            Exit For
                        End If
                    Next
                End If
            Else
                If m_oAtributo.UsarPrec = 1 Then bEliminarAdj = True
            End If
        End If
   
        
        If m_oProcesoSeleccionado.AtributosOfeObl And g_bAnyadirOfertaObl Then
        
        
            Select Case m_oAtributo.Tipo
            Case TiposDeAtributos.TipoString
                m_oAtributo.valorText = CStr(sdbgAtributos.Columns("VALOR").Value)
            Case TiposDeAtributos.TipoNumerico
                m_oAtributo.valorNum = CDbl(sdbgAtributos.Columns("VALOR").Value)
            Case TiposDeAtributos.TipoFecha
                m_oAtributo.valorFec = CDate(sdbgAtributos.Columns("VALOR").Value)
            Case TiposDeAtributos.TipoBoolean
                If UCase(sdbgAtributos.Columns("VALOR").Value) = UCase(m_sIdiTrue) Then
                    m_oAtributo.valorBool = True
                Else
                    m_oAtributo.valorBool = False
                End If
            End Select
       
            ''' Registro de acciones
            basSeguridad.RegistrarAccion ACCRecOfeAtribMod, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & g_oOfertaSeleccionada.Prove & "Oferta:" & g_oOfertaSeleccionada.Num & "Atributo:" & m_oAtributo.Cod
            Select Case sOperacion
                Case "I"
                    If g_oGrupoOferSeleccionado Is Nothing Then
                        g_oOfertaSeleccionada.AtribProcOfertados.Add m_oAtribEnEdicion.idAtribProce, g_oOfertaSeleccionada, , , m_oAtribEnEdicion.valorBool, m_oAtribEnEdicion.valorFec, m_oAtribEnEdicion.valorNum, , m_oAtribEnEdicion.valorText
                        g_oOfertaSeleccionada.NumAtribOfer = g_oOfertaSeleccionada.NumAtribOfer + 1
                    Else
                        g_oGrupoOferSeleccionado.AtribOfertados.Add m_oAtribEnEdicion.idAtribProce, g_oOfertaSeleccionada, , , m_oAtribEnEdicion.valorBool, m_oAtribEnEdicion.valorFec, m_oAtribEnEdicion.valorNum, , m_oAtribEnEdicion.valorText
                        g_oGrupoOferSeleccionado.NumAtribOfer = g_oGrupoOferSeleccionado.NumAtribOfer + 1
                    End If
                    If tvwProce.selectedItem.Image = "AtributosGris" Then
                        tvwProce.selectedItem.Image = "Atributos"
                    End If
                Case "D"
                    If g_oGrupoOferSeleccionado Is Nothing Then
                        g_oOfertaSeleccionada.NumAtribOfer = g_oOfertaSeleccionada.NumAtribOfer - 1
                        g_oOfertaSeleccionada.AtribProcOfertados.Remove CStr(m_oAtribEnEdicion.idAtribProce)
                        If g_oOfertaSeleccionada.NumAtribOfer <= 0 Then
                            tvwProce.selectedItem.Image = "AtributosGris"
                        End If
                    Else
                        g_oGrupoOferSeleccionado.NumAtribOfer = g_oGrupoOferSeleccionado.NumAtribOfer - 1
                        g_oGrupoOferSeleccionado.AtribOfertados.Remove CStr(m_oAtribEnEdicion.idAtribProce)
                        If g_oGrupoOferSeleccionado.NumAtribOfer <= 0 Then
                            tvwProce.selectedItem.Image = "AtributosGris"
                        End If
                    End If
            End Select
            
            cmdDeshacer.Enabled = False
            Set m_oAtribEnEdicion = Nothing
        Else
        
            Dim bEspera As Boolean
            bEspera = False
            If g_oOfertaSeleccionada.Recalcular Then
                bEspera = True
                frmADJCargar.Show
                frmADJCargar.lblCargando.caption = m_sLitRecalculando
                frmADJCargar.Refresh
            End If
            
            If g_oGrupoOferSeleccionado Is Nothing Then
                If m_oAtributo.Tipo <> TipoBoolean Then
                    teserror = g_oOfertaSeleccionada.AtribProcOfertados.GuardarAtributoOfertado(sOperacion, g_oOfertaSeleccionada, sdbgAtributos.Columns("ID_P").Value, sdbgAtributos.Columns("VALOR").Value, , , g_oOfertaSeleccionada.Cambio, bEliminarAdj, m_oAtributo)
                Else
                    teserror = g_oOfertaSeleccionada.AtribProcOfertados.GuardarAtributoOfertado(sOperacion, g_oOfertaSeleccionada, sdbgAtributos.Columns("ID_P").Value, m_oAtribEnEdicion.valorBool, , , , bEliminarAdj)
                End If
            Else
                If m_oAtributo.Tipo <> TipoBoolean Then
                    teserror = g_oGrupoOferSeleccionado.AtribOfertados.GuardarAtributoOfertado(sOperacion, g_oOfertaSeleccionada, sdbgAtributos.Columns("ID_P").Value, sdbgAtributos.Columns("VALOR").Value, g_oGrupoOferSeleccionado.Grupo.Id, , g_oOfertaSeleccionada.Cambio, bEliminarAdj, m_oAtributo)
                Else
                    teserror = g_oGrupoOferSeleccionado.AtribOfertados.GuardarAtributoOfertado(sOperacion, g_oOfertaSeleccionada, sdbgAtributos.Columns("ID_P").Value, m_oAtribEnEdicion.valorBool, g_oGrupoOferSeleccionado.Grupo.Id, , , bEliminarAdj)
                End If
            End If
            
            If bEspera Then
                Unload frmADJCargar
            End If
            
            If teserror.NumError <> TESnoerror Then
        
                v = sdbgAtributos.ActiveCell.Value
                TratarError teserror
                If Me.Visible Then sdbgAtributos.SetFocus
                bModError = True
                sdbgAtributos.ActiveCell.Value = v
                Accion = ACCRecOfeCon
                sdbgAtributos.DataChanged = False
        
            Else
                dFechaAct = teserror.Arg1
                ''' Registro de acciones
                basSeguridad.RegistrarAccion ACCRecOfeAtribMod, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & g_oOfertaSeleccionada.Prove & "Oferta:" & g_oOfertaSeleccionada.Num & "Atributo:" & m_oAtributo.Cod
                'Registramos llamada a recalcular
                If bEspera Then
                    basSeguridad.RegistrarAccion AccionesSummit.ACCCondOfeRecalculo, "Llamada desde:Ofertas Anyo:" & CStr(sdbcAnyo) & " Gmn1:" & CStr(sdbcGMN1_4Cod) & " Proce:" & CStr(sdbcProceCod)
                End If
                Select Case sOperacion
                    Case "I"
                        If g_oGrupoOferSeleccionado Is Nothing Then
                            g_oOfertaSeleccionada.AtribProcOfertados.Add m_oAtribEnEdicion.idAtribProce, g_oOfertaSeleccionada, , , m_oAtribEnEdicion.valorBool, m_oAtribEnEdicion.valorFec, m_oAtribEnEdicion.valorNum, , m_oAtribEnEdicion.valorText, , dFechaAct
                            g_oOfertaSeleccionada.NumAtribOfer = g_oOfertaSeleccionada.NumAtribOfer + 1
                        Else
                            g_oGrupoOferSeleccionado.AtribOfertados.Add m_oAtribEnEdicion.idAtribProce, g_oOfertaSeleccionada, , , m_oAtribEnEdicion.valorBool, m_oAtribEnEdicion.valorFec, m_oAtribEnEdicion.valorNum, , m_oAtribEnEdicion.valorText, , dFechaAct
                            g_oGrupoOferSeleccionado.NumAtribOfer = g_oGrupoOferSeleccionado.NumAtribOfer + 1
                        End If
                        If tvwProce.selectedItem.Image = "AtributosGris" Then
                            tvwProce.selectedItem.Image = "Atributos"
                        End If
                    Case "D"
                        If g_oGrupoOferSeleccionado Is Nothing Then
                            g_oOfertaSeleccionada.NumAtribOfer = g_oOfertaSeleccionada.NumAtribOfer - 1
                            g_oOfertaSeleccionada.AtribProcOfertados.Remove CStr(m_oAtribEnEdicion.idAtribProce)
                            If g_oOfertaSeleccionada.NumAtribOfer <= 0 Then
                                tvwProce.selectedItem.Image = "AtributosGris"
                            End If
                        Else
                            g_oGrupoOferSeleccionado.NumAtribOfer = g_oGrupoOferSeleccionado.NumAtribOfer - 1
                            g_oGrupoOferSeleccionado.AtribOfertados.Remove CStr(m_oAtribEnEdicion.idAtribProce)
                            If g_oGrupoOferSeleccionado.NumAtribOfer <= 0 Then
                                tvwProce.selectedItem.Image = "AtributosGris"
                            End If
                        End If
                End Select
                
                Accion = ACCRecOfeCon
                cmdDeshacer.Enabled = False
                
                Set m_oAtribEnEdicion = Nothing
                
            End If
        End If
    End If
End Sub

Private Sub sdbgatributos_BeforeUpdate(Cancel As Integer)
    Dim oElem As CValorPond
    Dim bEncontrado As Boolean
    Dim bSalir As Boolean
    Dim sError As String
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim bPreguntar As Boolean
    Dim oUsar As CAtribTotalGrupo
    Dim vValorM As Variant

    If sdbgAtributos.Columns("VALOR").Text = "" Then
        If sdbgAtributos.Columns("OBL").Value Then
            oMensajes.DatoObligatorio 111
            If Me.Visible Then sdbgAtributos.SetFocus
            sdbgAtributos.col = 2
            Cancel = True
            bModError = True
            Exit Sub
        End If
        If sOperacion = "U" Then sOperacion = "D"
    End If

    If sdbgAtributos.Columns("VALOR").Text <> "" Then
        bSalir = False
        Select Case sdbgAtributos.Columns("TIPO").Value
            Case TiposDeAtributos.TipoNumerico
                sError = "TIPO2"
                If Not IsNumeric(sdbgAtributos.Columns("VALOR").Text) Then
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoFecha
                sError = "TIPO3"
                If Not IsDate(sdbgAtributos.Columns("VALOR").Text) Then
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoBoolean
                sError = "TIPO4"
                If UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiTrue) And UCase(sdbgAtributos.Columns("VALOR").Text) <> UCase(m_sIdiFalse) Then
                    bSalir = True
                End If
        End Select
        If bSalir Then
            oMensajes.AtributoValorNoValido sError
            Cancel = True
            bModError = True
            If Me.Visible Then sdbgAtributos.SetFocus
            sdbgAtributos.col = 2
            Exit Sub
        End If
        
        bEncontrado = False
        If m_oAtributo.TipoIntroduccion = Introselec Then
            For Each oElem In m_oAtributo.ListaPonderacion
                Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoString
                    If oElem.ValorLista = sdbgAtributos.Columns("VALOR").Text Then
                        bEncontrado = True
                        Exit For
                    End If
                Case TiposDeAtributos.TipoNumerico
                    If (m_oAtributo.PrecioFormula = "+" Or m_oAtributo.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                        If CDbl(oElem.ValorLista * g_oOfertaSeleccionada.Cambio) = CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    Else
                        If CDbl(oElem.ValorLista) = CDbl(sdbgAtributos.Columns("VALOR").Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    End If
                Case TiposDeAtributos.TipoFecha
                    If CDate(oElem.ValorLista) = CDate(sdbgAtributos.Columns("VALOR").Text) Then
                        bEncontrado = True
                        Exit For
                    End If
                End Select
            Next
            If Not bEncontrado Then
                oMensajes.AtributoValorNoValido "NO_LISTA"
                bModError = True
                If Me.Visible Then sdbgAtributos.SetFocus
                sdbgAtributos.col = 2
                Cancel = True
                Exit Sub
            End If
        End If
        bSalir = False
        sError = ""
        If m_oAtributo.TipoIntroduccion = IntroLibre Then
            Select Case sdbgAtributos.Columns("TIPO").Value
                Case TiposDeAtributos.TipoNumerico
                    If (m_oAtributo.PrecioFormula = "+" Or m_oAtributo.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                        vValorM = CDec(sdbgAtributos.Columns("VALOR").Text) / g_oOfertaSeleccionada.Cambio
                    Else
                        vValorM = CDec(sdbgAtributos.Columns("VALOR").Text)
                    End If
                    If IsNumeric(m_oAtributo.Maximo) Then
                        If (m_oAtributo.PrecioFormula = "+" Or m_oAtributo.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                            sError = FormateoNumerico(m_oAtributo.Maximo * g_oOfertaSeleccionada.Cambio)
                        Else
                            sError = FormateoNumerico(m_oAtributo.Maximo)
                        End If
                        If CDbl(m_oAtributo.Maximo) < CDbl(vValorM) Then
                            bSalir = True
                        End If
                    End If
                    If IsNumeric(m_oAtributo.Minimo) Then
                        If sError = "" Then
                            If (m_oAtributo.PrecioFormula = "+" Or m_oAtributo.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                                sError = sIdiMayor & " " & FormateoNumerico(m_oAtributo.Minimo * g_oOfertaSeleccionada.Cambio)
                            Else
                                sError = sIdiMayor & " " & FormateoNumerico(m_oAtributo.Minimo)
                            End If
                        Else
                            If (m_oAtributo.PrecioFormula = "+" Or m_oAtributo.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                                sError = sIdiEntre & "  " & FormateoNumerico(m_oAtributo.Minimo * g_oOfertaSeleccionada.Cambio) & " - " & sError
                            Else
                                sError = sIdiEntre & "  " & FormateoNumerico(m_oAtributo.Minimo) & " - " & sError
                            End If
                        End If
                        If CDbl(m_oAtributo.Minimo) > CDbl(vValorM) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                    End If
                Case TiposDeAtributos.TipoFecha
                    If IsDate(m_oAtributo.Maximo) Then
                        sError = m_oAtributo.Maximo
                        If CDate(m_oAtributo.Maximo) < CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    End If
                    If IsDate(m_oAtributo.Minimo) Then
                        If sError = "" Then
                            sError = sIdiMayor & " " & m_oAtributo.Minimo
                        Else
                            sError = sIdiEntre & "  " & m_oAtributo.Minimo & " - " & sError
                        End If
                        If CDate(m_oAtributo.Minimo) > CDate(sdbgAtributos.Columns("VALOR").Text) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sIdiMenor & " " & sError
                        End If
                    End If
            End Select
            If bSalir Then
                oMensajes.AtributoValorNoValido sError
                bModError = True
                If Me.Visible Then sdbgAtributos.SetFocus
                sdbgAtributos.col = 2
                Cancel = True
                Exit Sub
            End If
        End If
        
    End If 'Si hab�a valor
    
    'Si el atributo es num�rico y se est� aplicando al precio preguntamos si se van a borrar las adjudicaciones:
    bPreguntar = False
    If m_oAtributo.Tipo = TipoNumerico And Not IsNull(m_oAtributo.PrecioAplicarA) Then
        If m_oAtributo.PrecioAplicarA = TotalGrupo Then   'es de �mbito grupo.Miramos en la tabla USAR_GR_ATRIB para ver si se est� aplicando
            If m_oAtributo.AtribTotalGrupo Is Nothing Then
                m_oAtributo.CargarUsarPrecTotalGrupo
            End If
            If Not m_oAtributo.AtribTotalGrupo Is Nothing Then
                For Each oUsar In m_oAtributo.AtribTotalGrupo
                    If oUsar.UsarPrec = 1 Then
                        bPreguntar = True
                        Exit For
                    End If
                Next
            End If
        Else
            If m_oAtributo.UsarPrec = 1 Then bPreguntar = True
        End If
        If bPreguntar = True Then
            If m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then
                teserror = g_oOfertaSeleccionada.OfertaModificable(True, True)
                If teserror.NumError = TESOfertaAdjudicada Then
                    oMensajes.ImposibleModOfertaProcPedidos teserror.Arg1
                    If Me.Visible Then sdbgAtributos.SetFocus
                    sdbgAtributos.col = 2
                    Cancel = True
                    Exit Sub
                End If
            Else
                'comprueba si el proceso tiene adjudicaciones:
                teserror = g_oOfertaSeleccionada.OfertaModificable(True)
                If teserror.NumError = TESOfertaAdjudicada Then
                    irespuesta = oMensajes.PreguntaHayAdjudicacionesGuardadas
                    If irespuesta = vbNo Then
                        If Me.Visible Then sdbgAtributos.SetFocus
                        sdbgAtributos.col = 2
                        Cancel = True
                        Exit Sub
                    End If
                End If
            End If
        End If
    End If
    
    
    'Comprobamos si ha habido cambios en el valor de un atributo de oferta aplicado.
    bPreguntar = False
    If m_oAtributo.Tipo = TipoNumerico And Not IsNull(m_oAtributo.PrecioAplicarA) Then
        If m_oAtributo.PrecioAplicarA = TotalGrupo Then   'es de �mbito grupo.Miramos en la tabla USAR_GR_ATRIB para ver si se est� aplicando
            If m_oAtributo.AtribTotalGrupo Is Nothing Then
                m_oAtributo.CargarUsarPrecTotalGrupo
            End If
            If Not m_oAtributo.AtribTotalGrupo Is Nothing Then
                For Each oUsar In m_oAtributo.AtribTotalGrupo
                    If oUsar.UsarPrec = 1 Then
                        bPreguntar = True
                        Exit For
                    End If
                Next
            End If
        Else
            If m_oAtributo.UsarPrec = 1 Then bPreguntar = True
        End If
    End If
    
    If bPreguntar Then
        If g_oOfertaSeleccionada.portal = True Then  'Solo le sacamos el mensaje para las ofertas del portal.
            irespuesta = oMensajes.AvisoConConfirmacion(m_sMensajeModifAtrib)
            If irespuesta = vbNo Then
                If Me.Visible Then sdbgAtributos.SetFocus
                sdbgAtributos.col = 2
                Cancel = True
                Exit Sub
            End If
        End If
        If m_oAtribEnEdicion.valorNum <> StrToDblOrNull(sdbgAtributos.Columns("VALOR").Value) Then
            'Ha habido cambio en el valor de un atributo
            g_oOfertaSeleccionada.Recalcular = True
            Set g_oOfertaSeleccionada.proceso = m_oProcesoSeleccionado
        End If
    End If
    
End Sub

''' <summary>
''' Evento que salta al hacer cambios en el grid de atributos
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Tiempo m�ximo:0</remarks>

Private Sub sdbgatributos_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos

      If bModoEdicion Then

        DoEvents

        Set m_oAtribEnEdicion = Nothing

        If g_oGrupoOferSeleccionado Is Nothing Then
            If Not g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(sdbgAtributos.Columns("ID_P").Value)) Is Nothing Then
                Set m_oAtribEnEdicion = g_oOfertaSeleccionada.AtribProcOfertados.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
                sOperacion = "U"
            Else
                Set m_oAtribEnEdicion = oFSGSRaiz.Generar_CAtributoOfertado
                m_oAtribEnEdicion.idAtribProce = sdbgAtributos.Columns("ID_P").Value
                sOperacion = "I"
            End If
            Set m_oAtribEnEdicion.UltimaOferta = g_oOfertaSeleccionada
            Set m_oAtributo = m_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
        Else
            If Not g_oGrupoOferSeleccionado.AtribOfertados.Item(CStr(sdbgAtributos.Columns("ID_P").Value)) Is Nothing Then
                Set m_oAtribEnEdicion = g_oGrupoOferSeleccionado.AtribOfertados.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
                sOperacion = "U"
            Else
                Set m_oAtribEnEdicion = oFSGSRaiz.Generar_CAtributoOfertado
                m_oAtribEnEdicion.idAtribProce = sdbgAtributos.Columns("ID_P").Value
                sOperacion = "I"
            End If
            Set m_oAtribEnEdicion.UltimaOferta = g_oOfertaSeleccionada
            m_oAtribEnEdicion.Grupo = m_oGrupoSeleccionado.Codigo
            Set m_oAtributo = m_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(sdbgAtributos.Columns("ID_P").Value))
        End If
        
        If Not g_bAnyadirOfertaObl Then
        Accion = ACCRecOfeAtribMod
        cmdDeshacer.Enabled = True
        End If
    End If

End Sub

Private Sub sdbgAtributos_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
       If sdbgAtributos.DataChanged = False Then
            sdbgAtributos.CancelUpdate
            sdbgAtributos.DataChanged = False

            If Not m_oAtribEnEdicion Is Nothing Then
                Set oIBaseDatos = Nothing
                Set m_oAtribEnEdicion = Nothing
            End If

            cmdDeshacer.Enabled = False
            Accion = ACCRecOfeCon
        End If
    End If
End Sub

''' <summary>
''' Configura la grid de items de la oferta
''' </summary>
''' <param name="Tipo">Indica si son de grupo o de proceso o de item</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:MostrarItemsOfe;MostrarItemsGrupoOfe; Tiempo m�ximo:0</remarks>
Private Sub ConfigurarGridItems(ByVal Tipo As Integer)
Dim oAtribs As CAtributos
Dim oatrib  As CAtributo
Dim i As Integer
Dim oColumn As SSDataWidgets_B.Column
Dim bDest As Boolean
Dim bFecSum As Boolean
Dim bPago As Boolean
Dim vGrupo As Variant

    m_iTotalCols = 6

    If Tipo = 0 Then
        If m_oProcesoSeleccionado.AtributosItem Is Nothing Then
            vGrupo = Null
            If sdbcGrupo.Columns(0).Value = "**********" Then vGrupo = "#"
            m_oProcesoSeleccionado.CargarAtributos ambito:=AmbItem, vGrupo:=vGrupo
        End If

        Set oAtribs = m_oProcesoSeleccionado.AtributosItem
        If m_oProcesoSeleccionado.DefFechasSum = EnItem Then
            bFecSum = True
            m_iTotalCols = m_iTotalCols + 2
        End If
        If m_oProcesoSeleccionado.DefDestino = EnItem Then
            bDest = True
            m_iTotalCols = m_iTotalCols + 1
        End If
        If m_oProcesoSeleccionado.DefFormaPago = EnItem Then
            bPago = True
            m_iTotalCols = m_iTotalCols + 1
        End If
    Else
        If m_oGrupoSeleccionado.AtributosItem Is Nothing Then
            m_oGrupoSeleccionado.CargarAtributos
        End If
        
        Set oAtribs = m_oGrupoSeleccionado.AtributosItem
        If m_oProcesoSeleccionado.DefFechasSum = EnItem Or (m_oProcesoSeleccionado.DefFechasSum = EnGrupo And Not m_oGrupoSeleccionado.DefFechasSum) Then
            bFecSum = True
            m_iTotalCols = m_iTotalCols + 2
        End If
        If m_oProcesoSeleccionado.DefDestino = EnItem Or (m_oProcesoSeleccionado.DefDestino = EnGrupo And Not m_oGrupoSeleccionado.DefDestino) Then
            bDest = True
            m_iTotalCols = m_iTotalCols + 1
        End If
        If m_oProcesoSeleccionado.DefFormaPago = EnItem Or (m_oProcesoSeleccionado.DefFormaPago = EnGrupo And Not m_oGrupoSeleccionado.DefFormaPago) Then
           bPago = True
           m_iTotalCols = m_iTotalCols + 1
        End If
    End If

    i = sdbgPrecios.Columns.Count

    If Not oAtribs Is Nothing Then
    For Each oatrib In oAtribs

        sdbgPrecios.Columns.Add i
        m_iTotalCols = m_iTotalCols + 1
        Set oColumn = sdbgPrecios.Columns(i)
        oColumn.Name = "AT_" & CStr(oatrib.idAtribProce)

        oColumn.CaptionAlignment = ssColCapAlignCenter
        Select Case oatrib.Tipo
        Case TiposDeAtributos.TipoString
            oColumn.Alignment = ssCaptionAlignmentLeft
            If oatrib.TipoIntroduccion = IntroLibre Then
                oColumn.Style = ssStyleEdit
            End If
            oColumn.FieldLen = cLongAtribEsp_Texto
            
        Case TiposDeAtributos.TipoNumerico
            oColumn.Alignment = ssCaptionAlignmentRight
            oColumn.NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
            If oatrib.TipoIntroduccion = IntroLibre Then
                oColumn.Style = ssStyleEdit
            End If
            
        Case TiposDeAtributos.TipoFecha
            oColumn.Alignment = ssCaptionAlignmentRight
            If oatrib.TipoIntroduccion = IntroLibre Then
                oColumn.Style = ssStyleEdit
            End If
            
        Case TiposDeAtributos.TipoBoolean
            oColumn.Style = ssStyleEdit
            
        End Select
        If oatrib.Obligatorio Then
            oColumn.caption = "(*) " & oatrib.Cod
            oColumn.TagVariant = "1"
        Else
            oColumn.caption = oatrib.Cod
            oColumn.TagVariant = "0"
        End If
        
        oColumn.StyleSet = "Normal"
        oColumn.Visible = True
        oColumn.Locked = True
        Set oColumn = Nothing
        i = i + 1
    Next
    End If
    
    If m_oProcesoSeleccionado.AdjuntarAItems Then
        sdbgPrecios.Columns.Add i
        m_iTotalCols = m_iTotalCols + 1
        Set oColumn = sdbgPrecios.Columns(i)
        oColumn.Name = "C#_ADJ"

        oColumn.CaptionAlignment = ssColCapAlignCenter
        oColumn.caption = m_sIdiAdjuntos
        oColumn.Style = ssStyleButton
        oColumn.ButtonsAlways = True
        oColumn.caption = m_sIdiAdjuntos
    End If
    
    Set oColumn = Nothing

    If Not bFecSum Then
        sdbgPrecios.Columns("INI").Visible = False
        sdbgPrecios.Columns("FIN").Visible = False
    End If
    
    If Not bDest Then sdbgPrecios.Columns("DEST").Visible = False
    If Not bPago Then sdbgPrecios.Columns("PAG").Visible = False
        
    If m_oProcesoSeleccionado.PedirAlterPrecios Then
        sdbgPrecios.Columns("PREC2").Visible = True
        sdbgPrecios.Columns("COMENT2").Visible = True
        sdbgPrecios.Columns("PREC3").Visible = True
        sdbgPrecios.Columns("COMENT3").Visible = True
        sdbgPrecios.Columns("USAR").Visible = True
        m_iTotalCols = m_iTotalCols + 5
    Else
        sdbgPrecios.Columns("PREC2").Visible = False
        sdbgPrecios.Columns("COMENT2").Visible = False
        sdbgPrecios.Columns("COMENT2").Style = ssStyleEdit
        sdbgPrecios.Columns("PREC3").Visible = False
        sdbgPrecios.Columns("COMENT3").Visible = False
        sdbgPrecios.Columns("COMENT3").Style = ssStyleEdit
        sdbgPrecios.Columns("USAR").Visible = False
    End If
    If m_oProcesoSeleccionado.SolicitarCantMax Then
        sdbgPrecios.Columns("CANTMAX").Visible = True
        m_iTotalCols = m_iTotalCols + 1
    Else
        sdbgPrecios.Columns("CANTMAX").Visible = False
    End If
    m_bCargarLayout = True
    
    sdbgPrecios.AllowUpdate = True
    
    RedimensionarGridItems

    FormateoDecimales
End Sub

''' <summary>
''' Configura la grid de items de la oferta para escalados
''' </summary>
''' <param name="Tipo">Indica si son de grupo o de proceso o de item</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:MostrarItemsOfe;MostrarItemsGrupoOfe; Tiempo m�ximo:0</remarks>
''' <revision>JVS 02/12/2011</revision>
Private Sub ConfigurarGridItemsEscalados(ByVal Tipo As Integer)
    Dim oAtribs As CAtributos
    Dim oatrib  As CAtributo
    Dim i As Integer
    Dim oColumn As SSDataWidgets_B.Column
    Dim bDest As Boolean
    Dim bFecSum As Boolean
    Dim bPago As Boolean
    Dim vGrupo As Variant
    Dim iPosition As Integer
    Dim iGroupIndex As Integer
    Dim oEscalado As CEscalado
    Dim iAnchoCol As Integer
    Dim iNumAtrib As Integer
    Dim oGrupoSelecc As CGrupo
    Dim bGrupoAtrCreado As Boolean
    Dim ogroup As SSDataWidgets_B.Group

    m_iTotalCols = 6

    If Tipo = 0 Then
        If m_oProcesoSeleccionado.AtributosItem Is Nothing Then
            vGrupo = Null
            If sdbcGrupo.Columns(0).Value = "**********" Then vGrupo = "#"
            m_oProcesoSeleccionado.CargarAtributos ambito:=AmbItem, vGrupo:=vGrupo
        End If

        Set oAtribs = m_oProcesoSeleccionado.AtributosItem
        If m_oProcesoSeleccionado.DefFechasSum = EnItem Then
            bFecSum = True
            m_iTotalCols = m_iTotalCols + 2
        End If
        If m_oProcesoSeleccionado.DefDestino = EnItem Then
            bDest = True
            m_iTotalCols = m_iTotalCols + 1
        End If
        If m_oProcesoSeleccionado.DefFormaPago = EnItem Then
            bPago = True
            m_iTotalCols = m_iTotalCols + 1
        End If
    Else
        If m_oGrupoSeleccionado.AtributosItem Is Nothing Then
            m_oGrupoSeleccionado.CargarAtributos
        End If
        
        Set oAtribs = m_oGrupoSeleccionado.AtributosItem
        If m_oProcesoSeleccionado.DefFechasSum = EnItem Or (m_oProcesoSeleccionado.DefFechasSum = EnGrupo And Not m_oGrupoSeleccionado.DefFechasSum) Then
            bFecSum = True
            m_iTotalCols = m_iTotalCols + 2
        End If
        If m_oProcesoSeleccionado.DefDestino = EnItem Or (m_oProcesoSeleccionado.DefDestino = EnGrupo And Not m_oGrupoSeleccionado.DefDestino) Then
            bDest = True
            m_iTotalCols = m_iTotalCols + 1
        End If
        If m_oProcesoSeleccionado.DefFormaPago = EnItem Or (m_oProcesoSeleccionado.DefFormaPago = EnGrupo And Not m_oGrupoSeleccionado.DefFormaPago) Then
           bPago = True
           m_iTotalCols = m_iTotalCols + 1
        End If
    End If
    
    If Tipo = 0 Then
        Set oGrupoSelecc = m_oProcesoSeleccionado.Grupos.Item(1)
    Else
        Set oGrupoSelecc = m_oGrupoSeleccionado
    End If
    
    If oGrupoSelecc.Escalados Is Nothing Then oGrupoSelecc.CargarEscalados
        
    iPosition = oGrupoSelecc.Escalados.Count
    iGroupIndex = sdbgPreciosEscalados.Groups.Count
        
    i = 0
    
    'C�lculo del n�mero de costes/desc
    iNumAtrib = 0
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
            If oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "" Then
                iNumAtrib = iNumAtrib + 1
            End If
        Next
    End If

    For Each oEscalado In oGrupoSelecc.Escalados
        i = 0
        iPosition = iPosition + 1
        With sdbgPreciosEscalados
            .Groups.Add iGroupIndex
            Set ogroup = .Groups(iGroupIndex)
            
            ogroup.TagVariant = oEscalado.Id
            If oEscalado.final = "" Then
                ogroup.caption = FormateoNumerico(oEscalado.Inicial) & " " & m_oGrupoSeleccionado.UnidadEscalado
            Else
                ogroup.caption = m_sRango & " " & FormateoNumerico(oEscalado.Inicial) & " - " & FormateoNumerico(oEscalado.final) & " " & oGrupoSelecc.UnidadEscalado
            End If
            ogroup.AllowSizing = True
            ogroup.CaptionAlignment = ssCaptionAlignmentCenter
            ogroup.HeadStyleSet = "HeadBold"
            ogroup.Width = (iNumAtrib + 2) * 900
            iAnchoCol = Round(ogroup.Width / (iNumAtrib + 2))
            
            'Columnas
            ogroup.Columns.Add i
            Set oColumn = ogroup.Columns(i)
            oColumn.Name = "PresUni_" & oEscalado.Id
            oColumn.CaptionAlignment = ssColCapAlignCenter
            oColumn.Alignment = ssCaptionAlignmentRight
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            oColumn.caption = m_sPresUniAbrv
            oColumn.Style = ssStyleEdit
            oColumn.HeadStyleSet = "Normal"
            oColumn.ButtonsAlways = False
            oColumn.Visible = True
            oColumn.Locked = True
            oColumn.Backcolor = RGB(192, 255, 255)
            DoEvents
            i = i + 1
            ogroup.Columns.Add i
            Set oColumn = ogroup.Columns(i)
            oColumn.Name = "Precio_" & oEscalado.Id
            oColumn.CaptionAlignment = ssColCapAlignCenter
            oColumn.Alignment = ssCaptionAlignmentRight
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            oColumn.caption = sIdiPrecio
            oColumn.Style = ssStyleEdit
            oColumn.HeadStyleSet = "Normal"
            oColumn.ButtonsAlways = False
            oColumn.Visible = True
            oColumn.Locked = True
            DoEvents
            If Not oAtribs Is Nothing Then
                For Each oatrib In oAtribs
                    If oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "" Then
                        i = i + 1
                        ogroup.Columns.Add i
                        Set oColumn = ogroup.Columns(i)
                        oColumn.Name = "ATE_" & oEscalado.Id & "_" & CStr(oatrib.idAtribProce)
                        oColumn.caption = oatrib.Cod
                        oColumn.Alignment = ssCaptionAlignmentRight
                        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                        oColumn.Style = ssStyleEdit
                        oColumn.HeadStyleSet = "Normal"
                        oColumn.ButtonsAlways = False
                        oColumn.Visible = True
                        oColumn.Locked = True
                        DoEvents
                    End If
                Next
            End If
        End With
        For Each oColumn In ogroup.Columns
            oColumn.Width = iAnchoCol
        Next
        iGroupIndex = iGroupIndex + 1
    Next
    
    bGrupoAtrCreado = False
    
    If Not oAtribs Is Nothing Then
        If oAtribs.Count > 0 Then
            i = 0
            For Each oatrib In oAtribs
                If Not (oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "") Then
                    If Not bGrupoAtrCreado Then
                        sdbgPreciosEscalados.Groups.Add iGroupIndex
                        Set ogroup = sdbgPreciosEscalados.Groups(iGroupIndex)
                        ogroup.TagVariant = "ATRIBSNOCOSTESDESC"
                        ogroup.caption = m_sAtribItem
                        ogroup.AllowSizing = True
                        ogroup.CaptionAlignment = ssCaptionAlignmentCenter
                        ogroup.HeadStyleSet = "HeadBold"
                        bGrupoAtrCreado = True
                        iGroupIndex = iGroupIndex + 1
                    End If
                    ogroup.Columns.Add i
                    m_iTotalCols = m_iTotalCols + 1
                    Set oColumn = ogroup.Columns(i)
                    oColumn.Name = "AT_" & CStr(oatrib.idAtribProce)
                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                    Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoString
                        oColumn.Alignment = ssCaptionAlignmentLeft
                        If oatrib.TipoIntroduccion = IntroLibre Then
                            oColumn.Style = ssStyleEdit
                            oColumn.FieldLen = cLongAtribEsp_Texto
                        Else
                            oColumn.FieldLen = 800
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        oColumn.Alignment = ssCaptionAlignmentRight
                        oColumn.NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
                        If oatrib.TipoIntroduccion = IntroLibre Then
                            oColumn.Style = ssStyleEdit
                        End If
                        
                    Case TiposDeAtributos.TipoFecha
                        oColumn.Alignment = ssCaptionAlignmentRight
                        If oatrib.TipoIntroduccion = IntroLibre Then
                            oColumn.Style = ssStyleEdit
                        End If
                        
                    Case TiposDeAtributos.TipoBoolean
                        oColumn.Style = ssStyleEdit
                        
                    End Select
                    If oatrib.Obligatorio Then
                        oColumn.caption = "(*) " & oatrib.Cod
                        oColumn.TagVariant = "1"
                    Else
                        oColumn.caption = oatrib.Cod
                        oColumn.TagVariant = "0"
                    End If
                    oColumn.HeadStyleSet = "Normal"
                    oColumn.StyleSet = "Normal"
                    oColumn.Visible = True
                    oColumn.Locked = True
                    Set oColumn = Nothing
                    DoEvents
                    i = i + 1
                End If
            Next
        End If
    End If
    
    If m_oProcesoSeleccionado.AdjuntarAItems Then
        sdbgPreciosEscalados.Groups.Add iGroupIndex
        Set ogroup = sdbgPreciosEscalados.Groups(iGroupIndex)
        ogroup.TagVariant = "ADJUNTOS"
        i = ogroup.Columns.Count
        ogroup.Columns.Add i
        m_iTotalCols = m_iTotalCols + 1
        Set oColumn = ogroup.Columns(i)
        oColumn.Name = "C#_ADJ"
        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
        oColumn.caption = m_sIdiAdjuntos
        oColumn.Style = ssStyleButton
        oColumn.ButtonsAlways = True
        oColumn.caption = m_sIdiAdjuntos
        oColumn.Width = (ogroup.Width / ogroup.Columns.Count)
    End If
    
    Set oColumn = Nothing
    
    If Not bFecSum Then
        sdbgPreciosEscalados.Columns("INI").Visible = False
        sdbgPreciosEscalados.Columns("FIN").Visible = False
    End If
    If Not bDest Then sdbgPreciosEscalados.Columns("DEST").Visible = False
    If Not bPago Then sdbgPreciosEscalados.Columns("PAG").Visible = False
        
    sdbgPreciosEscalados.AllowUpdate = True
    
    RedimensionarGridItemsEscalados Tipo
    
    FormateoDecimales (iNumAtrib > 0)
End Sub

Private Function ComprobarValorAtributo(ByVal LastCol As Integer, ByRef sdbgGrid As SSDBGrid) As Boolean
Dim lIdAtrib As Long
Dim oatrib As CAtributo
Dim bSalir As Boolean
Dim bEncontrado As Boolean
Dim oElem As CValorPond
Dim sError As String
Dim vValorM As Variant
Dim bTratar As Boolean

bTratar = True
If sdbgGrid.Name = "sdbgPrecios" And LastCol <= 15 Then bTratar = False

If bTratar Then
    If Left(sdbgGrid.Columns(LastCol).Name, 2) = "AT" Then
        If Left(sdbgGrid.Columns(LastCol).Name, 3) = "AT_" Then
            lIdAtrib = Right(sdbgGrid.Columns(LastCol).Name, Len(sdbgGrid.Columns(LastCol).Name) - 3)
        Else
            lIdAtrib = Right(sdbgGrid.Columns(LastCol).Name, Len(sdbgGrid.Columns(LastCol).Name) - InStr(5, sdbgGrid.Columns(LastCol).Name, "_"))
        End If
        If sdbcGrupo.Text = m_sIdiTodosItems Then
            Set oatrib = m_oProcesoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
        Else
            Set oatrib = m_oGrupoSeleccionado.AtributosItem.Item(CStr(lIdAtrib))
        End If
        
        If sdbgGrid.Columns(LastCol).Text <> "" Then
            bSalir = False
            Select Case oatrib.Tipo
            Case TiposDeAtributos.TipoNumerico
                If Not IsNumeric(sdbgGrid.Columns(LastCol).Text) Then
                    sError = "TIPO2"
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoFecha
                If Not IsDate(sdbgGrid.Columns(LastCol).Text) Then
                    sError = "TIPO3"
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoBoolean
                If UCase(sdbgGrid.Columns(LastCol).Text) <> UCase(m_sIdiTrue) And UCase(sdbgGrid.Columns(LastCol).Text) <> UCase(m_sIdiFalse) Then
                    sError = "TIPO4"
                    bSalir = True
                End If
            End Select
            If bSalir Then
                oMensajes.AtributoValorNoValido sError
                ComprobarValorAtributo = False
                Exit Function
            End If
            
            bEncontrado = False
            If oatrib.TipoIntroduccion = Introselec Then
                For Each oElem In oatrib.ListaPonderacion
                    Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoString
                        If oElem.ValorLista = sdbgGrid.Columns(LastCol).Text Then
                            bEncontrado = True
                            Exit For
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                            If CDbl(oElem.ValorLista * g_oOfertaSeleccionada.Cambio) = CDbl(sdbgGrid.Columns(LastCol).Text) Then
                                bEncontrado = True
                                Exit For
                            End If
                        Else
                            If CDbl(oElem.ValorLista) = CDbl(sdbgGrid.Columns(LastCol).Text) Then
                                bEncontrado = True
                                Exit For
                            End If
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If CDate(oElem.ValorLista) = CDate(sdbgGrid.Columns(LastCol).Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    End Select
                Next
                If Not bEncontrado Then
                    oMensajes.AtributoValorNoValido "NO_LISTA"
                    ComprobarValorAtributo = False
                    Exit Function
                End If
            End If
            bSalir = False
            sError = ""
            If oatrib.TipoIntroduccion = IntroLibre Then
                Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoNumerico
                        If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                            vValorM = CDec(sdbgGrid.Columns(LastCol).Text) / g_oOfertaSeleccionada.Cambio
                        Else
                            vValorM = CDec(sdbgGrid.Columns(LastCol).Text)
                        End If
                        If IsNumeric(oatrib.Maximo) Then
                            If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                                sError = FormateoNumerico(oatrib.Maximo * g_oOfertaSeleccionada.Cambio)
                            Else
                                sError = FormateoNumerico(oatrib.Maximo)
                            End If
                            If CDbl(oatrib.Maximo) < CDbl(vValorM) Then
                                bSalir = True
                            End If
                        End If
                        If IsNumeric(oatrib.Minimo) Then
                            If sError = "" Then
                                If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                                    sError = sIdiMayor & " " & FormateoNumerico(oatrib.Minimo * g_oOfertaSeleccionada.Cambio)
                                Else
                                    sError = sIdiMayor & " " & FormateoNumerico(oatrib.Minimo)
                                End If
                            Else
                                If (oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-") And g_oOfertaSeleccionada.CodMon <> m_oProcesoSeleccionado.MonCod Then
                                    sError = sIdiEntre & "  " & FormateoNumerico(oatrib.Minimo * g_oOfertaSeleccionada.Cambio) & " - " & sError
                                Else
                                    sError = sIdiEntre & "  " & FormateoNumerico(oatrib.Minimo) & " - " & sError
                                End If
                            End If
                            If CDbl(oatrib.Minimo) > CDbl(vValorM) Then
                                bSalir = True
                            End If
                        Else
                            If sError <> "" Then
                                sError = sIdiMenor & " " & sError
                            End If
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If IsDate(oatrib.Maximo) Then
                            sError = oatrib.Maximo
                            If CDate(oatrib.Maximo) < CDate(sdbgGrid.Columns(LastCol).Text) Then
                                bSalir = True
                            End If
                        End If
                        If IsDate(oatrib.Minimo) Then
                            If sError = "" Then
                                sError = sIdiMayor & " " & oatrib.Minimo
                            Else
                                sError = sIdiEntre & "  " & oatrib.Minimo & " - " & sError
                            End If
                            If CDate(oatrib.Minimo) > CDate(sdbgGrid.Columns(LastCol).Text) Then
                                bSalir = True
                            End If
                        Else
                            If sError <> "" Then
                                sError = sIdiMenor & " " & sError
                            End If
                        End If
                End Select
                If bSalir Then
                    oMensajes.AtributoValorNoValido sError
                    ComprobarValorAtributo = False
                    Exit Function
                End If
            End If
                   
        End If 'Si hab�a valor
    End If
End If

ComprobarValorAtributo = True
End Function

Private Sub txtCambio_Change()
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    If txtCambio.Text = "" Then
        lblEquivalencia.caption = ""
    Else
        lblEquivalencia.caption = txtCambio.Text & " " & sIdiEquivaleA1 & " " & m_oProcesoSeleccionado.MonCod
    End If
End Sub

Private Sub txtNumDec_Validate(Cancel As Boolean)
    If Not IsInteger(txtNumDec.Text) Then
        txtNumDec.Text = UpDownDec.Value
        Exit Sub
    End If
    
    If val(txtNumDec.Text) < 0 Or val(txtNumDec.Text) > 20 Then
        txtNumDec.Text = UpDownDec.Value
        Exit Sub
    End If
        
    UpDownDec.Value = Trim(txtNumDec.Text)
End Sub

Private Sub txtObsAdjun_Change()
    If Not bRespetarCombo Then
        If Accion <> ACCRecOfeAdjunMod Then
            Accion = ACCRecOfeAdjunMod
        End If
    End If
End Sub

Private Sub txtObsAdjun_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit
Dim vObs As Variant

    If Accion = ACCRecOfeAdjunMod Then
    
        Screen.MousePointer = vbHourglass
        
        If g_oGrupoOferSeleccionado Is Nothing Then
            vObs = g_oOfertaSeleccionada.ObsAdjun
        Else
            vObs = g_oGrupoOferSeleccionado.ObsAdjun
        End If
        
        If StrComp(NullToStr(vObs), txtObsAdjun.Text, vbTextCompare) <> 0 Then
                
            If g_oGrupoOferSeleccionado Is Nothing Then
                 g_oOfertaSeleccionada.ObsAdjun = StrToNull(txtObsAdjun.Text)
                teserror = g_oOfertaSeleccionada.ModificarObsAdjunto
            Else
                g_oGrupoOferSeleccionado.ObsAdjun = StrToNull(txtObsAdjun.Text)
                teserror = g_oGrupoOferSeleccionado.ModificarObsAdjunto
            End If

            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCRecOfeCon
                Exit Sub
            End If
            
            If g_oGrupoOferSeleccionado Is Nothing Then
                If txtObsAdjun.Text = "" Then
                    If g_oOfertaSeleccionada.NumAdjuntos > 0 Then
                       tvwProce.selectedItem.Image = "Ficheros"
                    Else
                       tvwProce.selectedItem.Image = "FicherosGris"
                    End If
                Else
                    tvwProce.selectedItem.Image = "Ficheros"
                End If
            Else
                If txtObsAdjun.Text = "" Then
                    If g_oGrupoOferSeleccionado.NumAdjuntos > 0 Then
                       tvwProce.selectedItem.Image = "Ficheros"
                    Else
                       tvwProce.selectedItem.Image = "FicherosGris"
                    End If
                Else
                    tvwProce.selectedItem.Image = "Ficheros"
                End If
            End If
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, "Anyo:" & g_oOfertaSeleccionada.Anyo & "GMN1:" & g_oOfertaSeleccionada.GMN1Cod & "Proce:" & g_oOfertaSeleccionada.Proce & "Prove:" & g_oOfertaSeleccionada.Prove & "Oferta:" & g_oOfertaSeleccionada.Num

        End If
        Screen.MousePointer = vbNormal
        Accion = ACCRecOfeCon
    End If
End Sub

Private Sub Arrange()

    On Error Resume Next
    If Me.Height < 3000 Then Exit Sub
    If Me.Width < 3000 Then Exit Sub
    tvwProce.Height = Me.Height - 1470
    tvwProce.Width = (Me.Width - 225) * 0.34
    picPrincipal.Left = tvwProce.Width - 5
    picPrincipal.Height = tvwProce.Height
    picPrincipal.Width = (Me.Width - 225) * 0.66
    picApartado(0).Height = picPrincipal.Height - 240
    picApartado(0).Width = picPrincipal.Width - 330
    lblDatosGenProce.Width = picApartado(0).Width
    
    Line8.X2 = picApartado(0).Width
    Line7.X2 = picApartado(0).Width
    Line1.X2 = picApartado(0).Width
    picApartado(1).Height = picApartado(0).Height
    picApartado(1).Width = picApartado(0).Width
    fraFechas.Width = picApartado(1).Width
    lblTZ.Width = fraFechas.Width
    lblDatosGeneralesOferta.Width = picApartado(1).Width
    Line2.X2 = picApartado(1).Width
    Line3.X2 = picApartado(1).Width
    picApartado(2).Height = picApartado(0).Height
    picApartado(2).Width = picApartado(0).Width
    lblAtributosProceso.Width = picApartado(2).Width
    
    picApartado(3).Height = picApartado(0).Height
    picApartado(3).Width = picApartado(0).Width
    lblAdjuntos.Width = picApartado(3).Width
    picApartado(5).Height = picApartado(0).Height
    picApartado(5).Width = picApartado(0).Width
    Label11.Width = picApartado(5).Width
    picApartado(6).Height = picApartado(0).Height
    picApartado(6).Width = picApartado(0).Width
    Label18.Width = picApartado(6).Width
    Line6.X2 = picApartado(6).Width
    Line10.X2 = picApartado(6).Width
    picAtrib.Height = picApartado(2).Height - 380
    picAtrib.Width = picApartado(2).Width - 60
    sdbgAtributos.Height = picApartado(2).Height - 405
    sdbgAtributos.Width = picApartado(2).Width - 75
    sdbgAtributos.Columns(0).Width = sdbgAtributos.Width * 0.2
    sdbgAtributos.Columns(1).Width = sdbgAtributos.Width * 0.5
    sdbgAtributos.Columns(2).Width = sdbgAtributos.Width * 0.3 - 500
    sdbddValor.Width = sdbgAtributos.Columns(2).Width
    sdbddValor.Columns(0).Width = sdbgAtributos.Columns(2).Width
    
    'Adjuntos
    txtObsAdjun.Width = picApartado(3).Width - 295
    txtObsAdjun.Height = picApartado(3).Height * 0.4
    Label1.Top = txtObsAdjun.Top + txtObsAdjun.Height + 20
    sdbgAdjun.Top = Label1.Top + Label1.Height + 25
    sdbgAdjun.Width = picApartado(3).Width - 300
    sdbgAdjun.Height = picApartado(3).Height - txtObsAdjun.Height - 1200
    sdbgAdjun.Columns("FICHERO").Width = sdbgAdjun.Width * 0.3
    sdbgAdjun.Columns("TAMANYO").Width = sdbgAdjun.Width * 0.15
    sdbgAdjun.Columns("COMENTARIO").Width = sdbgAdjun.Width * 0.54
    
    picBarraAdjun.Left = (sdbgAdjun.Left + sdbgAdjun.Width) - 2458
    picBarraAdjun.Top = sdbgAdjun.Top + sdbgAdjun.Height + 50
    
    'Items
    picApartado(4).Width = Me.Width - 170
    picApartado(4).Height = Me.Height - 1410
    sdbgPreciosEscalados.Width = picApartado(4).Width - 265
    sdbgPreciosEscalados.Height = picApartado(4).Height - 750
    sdbgPrecios.Width = picApartado(4).Width - 265
    sdbgPrecios.Height = picApartado(4).Height - 750
    Line11.X2 = picApartado(4).Width
    If m_bGrupoConEscalados Then
        If sdbcGrupo.Columns(0).Value = "**********" Then
            RedimensionarGridItemsEscalados 0
        Else
            RedimensionarGridItemsEscalados 1
        End If
    Else
        If sdbcGrupo.Columns(0).Value = "**********" Then
            RedimensionarGridItems
        Else
            RedimensionarGridItems
        End If
    End If
    
    'Sobres
    picApartado(7).Height = picApartado(0).Height
    picApartado(7).Width = picApartado(0).Width
    lblCabeceraSobre.Width = picApartado(7).Width
    Line4.X2 = picApartado(7).Width
    
    'Barras
    cmdModoEdicion.Left = Me.Width - 1335
    cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 350
    cmdCancelar.Left = Me.Width / 2 + 127
    
    If m_bGrupoConEscalados Then
        lblNumDec.Left = Me.sdbgPreciosEscalados.Width - 1770
    Else
        lblNumDec.Left = Me.sdbgPrecios.Width - 1770
    End If
    txtNumDec.Left = lblNumDec.Left + lblNumDec.Width + 5
    UpDownDec.Left = txtNumDec.Left + txtNumDec.Width + 20
End Sub


''' <summary>Redimensiona la grid de �tems seg�n el n�mero de columnas visibles</summary>
''' <param name="Tipo">Indica si son de grupo o de proceso o de item</param>
''' <remarks>Llamada desde: ConfigurarGridItemsEscalados</remarks>
''' <revision>LTG 18/10/2012</revision>

Private Sub RedimensionarGridItemsEscalados(ByVal Tipo As Integer)
    Dim i As Integer
    Dim dblAncho As Double
    Dim oGrupoSelec As CGrupo
    Dim oAtribs As CAtributos
    Dim oatrib As CAtributo
    Dim iNumCDs As Integer
    Dim iNumAtrib As Integer
    Dim oEsc As CEscalado
    Dim iNumEsc As Integer
    Dim dblWidth As Double
 
    With sdbgPreciosEscalados
        'N� columnas de escalados (grupos de escalados)
        If Tipo = 0 Then
            Set oGrupoSelec = m_oProcesoSeleccionado.Grupos.Item(1)
            Set oAtribs = m_oProcesoSeleccionado.AtributosItem
        Else
            Set oGrupoSelec = m_oGrupoSeleccionado
            Set oAtribs = m_oGrupoSeleccionado.AtributosItem
        End If
        'C�lculo del n�mero de costes/desc
        iNumCDs = 0
        iNumAtrib = 0
        If Not oAtribs Is Nothing Then
            For Each oatrib In oAtribs
                If oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "" Then
                    iNumCDs = iNumCDs + 1
                Else
                    iNumAtrib = iNumAtrib + 1
                End If
            Next
        End If
        
        dblWidth = 0.95 * .Width
        
        'Redimensionamiento de todas las columnas del grid (visibles y no visibles)
        .Columns("COD").Width = dblWidth * 0.1
        .Columns("DEN").Width = dblWidth * 0.15
        .Columns("ITEM").Width = dblWidth * 0.1
        .Columns("INI").Width = dblWidth * 0.07
        .Columns("FIN").Width = dblWidth * 0.07
        .Columns("DEST").Width = dblWidth * 0.05
        .Columns("UNI").Width = dblWidth * 0.05
        .Columns("CANT").Width = dblWidth * 0.1
        .Columns("PREC").Width = dblWidth * 0.1
        .Columns("PAG").Width = dblWidth * 0.05
        .Columns("COMENT").Width = dblWidth * 0.1
        .Columns("CERRADO").Width = dblWidth * 0.06
        
        iNumEsc = 1
        For Each oEsc In oGrupoSelec.Escalados
            .Groups(iNumEsc).Width = dblWidth * 0.05 * (iNumCDs + 2)
            
            .Columns("PresUni_" & oEsc.Id).Width = dblWidth * 0.05
            .Columns("Precio_" & oEsc.Id).Width = dblWidth * 0.05
            For Each oatrib In oAtribs
                If oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "" Then
                    .Columns("ATE_" & oEsc.Id & "_" & CStr(oatrib.idAtribProce)).Width = dblWidth * 0.05
                End If
            Next
            iNumEsc = iNumEsc + 1
        Next
        If iNumAtrib > 0 Then
            .Groups(iNumEsc).Width = dblWidth * 0.05 * iNumAtrib
            For Each oatrib In oAtribs
                If Not (oatrib.ambito = AmbItem And oatrib.Tipo = TipoNumerico And NullToStr(oatrib.PrecioAplicarA) <> "") Then
                    .Columns("AT_" & CStr(oatrib.idAtribProce)).Width = dblWidth * 0.05
                End If
            Next
            iNumEsc = iNumEsc + 1
        End If
        If Not .Columns("C#_ADJ") Is Nothing Then
            .Groups(iNumEsc).Width = dblWidth * 0.05
            .Columns("C#_ADJ").Width = dblWidth * 0.05
        End If
                
        'Hace falta redimensionar el grupo 0 por las columnas no visibles
        'Ancho de las columnas visibles
        dblAncho = 0
        For i = 0 To .Groups(0).Columns.Count - 1
            If .Groups(0).Columns(i).Visible Then
                dblAncho = dblAncho + .Groups(0).Columns(i).Width
            End If
        Next
        
        'Redimensionamiento en funci�n de las visibles a todo el ancho del grid
         For i = 0 To .Groups(0).Columns.Count - 1
            If .Groups(0).Columns(i).Visible Then
                .Groups(0).Columns(i).Width = .Groups(0).Columns(i).Width * (.Groups(0).Width / dblAncho)
                
                'S�lo hace falta redimensionar la 1� col., el resto se redimensiona automaticamente
                Exit For
            End If
        Next
        
        Set oEsc = Nothing
        Set oatrib = Nothing
        Set oGrupoSelec = Nothing
        Set oAtribs = Nothing
    End With
End Sub

''' <summary>Redimensiona la grid de �tems seg�n el n�mero de columnas visibles.</summary>
''' <remarks>Llamada desde: ConfigurarGridItems</remarks>
''' <revision>LTG 09/05/2012</revision>

Private Sub RedimensionarGridItems()
    Dim i As Integer
    Dim dblAncho As Double
 
    With sdbgPrecios
        'Redimensionamiento de todas las columnas del grid (visibles y no visibles)
        .Columns("COD").Width = .Width * 0.06
        .Columns("DEN").Width = .Width * 0.1
        .Columns("INI").Width = .Width * 0.07
        .Columns("FIN").Width = .Width * 0.07
        .Columns("DEST").Width = .Width * 0.05
        .Columns("UNI").Width = .Width * 0.05
        .Columns("CANT").Width = .Width * 0.07
        .Columns("PREC").Width = .Width * 0.07
        .Columns("PAG").Width = .Width * 0.05
        .Columns("PREC1").Width = .Width * 0.06
        .Columns("COMENT1").Width = .Width * 0.05
        .Columns("PREC2").Width = .Width * 0.06
        .Columns("COMENT2").Width = .Width * 0.06
        .Columns("PREC3").Width = .Width * 0.06
        .Columns("COMENT3").Width = .Width * 0.05
        .Columns("ANYOIMPUTACION").Width = .Width * 0.05
        If Not .Columns("C#_ADJ") Is Nothing Then
            .Columns("C#_ADJ").Width = .Width * 0.05
        End If
        
        'Ancho de las columnas visibles
        dblAncho = 0
        For i = 0 To .Cols - 1
            If .Columns(i).Visible Then
                dblAncho = dblAncho + .Columns(i).Width
            End If
        Next
        
        'Redimensionamiento en funci�n de las visibles a todo el ancho del grid
        If dblAncho <= .Width Then
            For i = 0 To .Cols - 1
                If .Columns(i).Visible Then
                    .Columns(i).Width = (.Columns(i).Width / dblAncho) * (0.95 * .Width)
                End If
            Next
        End If
    End With
End Sub

Private Function OrdenarGridPrecios(ByVal sCol As String, oPrecios As CPrecioItems) As CPrecioItems
Dim oPrecios1 As CPrecioItems
Dim oPrecios2 As CPrecioItems
Dim oPrecio As CPrecioItem
Dim i As Integer
Dim iMid As Integer
  

If oPrecios.Count >= 2 Then
    Set oPrecios1 = oFSGSRaiz.Generar_CPrecioItems
    Set oPrecios2 = oFSGSRaiz.Generar_CPrecioItems
    iMid = (oPrecios.Count / 2)
    i = 1
    'Dividir en dos colecciones
    For Each oPrecio In oPrecios
        If i <= iMid Then
            oPrecios1.AddPrecio oPrecio
        Else
            oPrecios2.AddPrecio oPrecio
        End If
        i = i + 1
    Next
    'Se ordena recursivamente
    Set oPrecios1 = OrdenarGridPrecios(sCol, oPrecios1)
    Set oPrecios2 = OrdenarGridPrecios(sCol, oPrecios2)
    Set OrdenarGridPrecios = Fusionar(oPrecios1, oPrecios2, sCol)
    
Else
    Set OrdenarGridPrecios = oPrecios
End If

Set oPrecios1 = Nothing
Set oPrecios2 = Nothing

End Function

Private Function Fusionar(ByVal oPrecios1 As CPrecioItems, ByVal oPrecios2 As CPrecioItems, ByVal sDato As String) As CPrecioItems
Dim oPreciosOrdenada As CPrecioItems
Dim iI1 As Long
Dim iI2 As Long

    'Si alguna de las dos son vacias se devuelve la otra
    If oPrecios1.Count = 0 Then
        Set Fusionar = oPrecios2
        Exit Function
    End If
    If oPrecios2.Count = 0 Then
        Set Fusionar = oPrecios1
        Exit Function
    End If
    
    iI1 = 1
    iI2 = 1
    
    Set oPreciosOrdenada = oFSGSRaiz.Generar_CPrecioItems
    
    While iI1 <= oPrecios1.Count Or iI2 <= oPrecios2.Count
        If iI1 <= oPrecios1.Count And iI2 <= oPrecios2.Count Then
            If DevolverMenor(oPrecios1.Item(iI1), oPrecios2.Item(iI2), sDato) = 1 Then
                oPreciosOrdenada.AddPrecio oPrecios1.Item(iI1)
                iI1 = iI1 + 1
            Else
                oPreciosOrdenada.AddPrecio oPrecios2.Item(iI2)
                iI2 = iI2 + 1
            End If
        Else
            If iI1 <= oPrecios1.Count Then
                oPreciosOrdenada.AddPrecio oPrecios1.Item(iI1)
                iI1 = iI1 + 1
            Else
                If iI2 <= oPrecios2.Count Then
                    oPreciosOrdenada.AddPrecio oPrecios2.Item(iI2)
                    iI2 = iI2 + 1
                End If
            End If
        End If
    
    Wend
    
Set Fusionar = oPreciosOrdenada
Set oPreciosOrdenada = Nothing

End Function


''' <summary>Devuelve el menor de los 2 objetos de entrada en funci�n del campo sDato</summary>
''' <param name="oPrecio1">Objeto 1 a comparar</param>
''' <param name="oPrecio2">Objeto 2 a comparar</param>
''' <param name="sDato">Campo de comparaci�n</param>
''' <returns>1 o 2 indicando el objeto menor</returns>
''' <remarks>Llamada desde: </remarks>

Function DevolverMenor(ByVal oPrecio1 As CPrecioItem, ByVal oPrecio2 As CPrecioItem, ByVal sDato As String) As Integer
Dim lIDAtr As Long
Dim oatrib As CAtributo
Dim oAtributos As CAtributos
Dim bExiste  As Boolean
Dim scod1 As String
Dim scod2 As String

If m_bGrupoConEscalados Then
    DevolverMenor = DevolverMenorEsc(oPrecio1, oPrecio2, sDato)
Else
    Select Case sDato
    Case "COD"
        DevolverMenor = 2
        If NullToStr(oPrecio1.ArticuloCod) < NullToStr(oPrecio2.ArticuloCod) Then
            DevolverMenor = 1
        ElseIf NullToStr(oPrecio1.ArticuloCod) = NullToStr(oPrecio2.ArticuloCod) Then
            If NullToStr(oPrecio1.Descr) < NullToStr(oPrecio2.Descr) Then
                DevolverMenor = 1
            ElseIf NullToStr(oPrecio1.Descr) = NullToStr(oPrecio2.Descr) Then
                If NullToStr(oPrecio1.Id) < NullToStr(oPrecio2.Id) Then
                    DevolverMenor = 1
                End If
            End If
        End If
    Case "DEN"
        DevolverMenor = 2
        If NullToStr(oPrecio1.Descr) < NullToStr(oPrecio2.Descr) Then
            DevolverMenor = 1
        ElseIf NullToStr(oPrecio1.Descr) = NullToStr(oPrecio2.Descr) Then
            If NullToStr(oPrecio1.ArticuloCod) < NullToStr(oPrecio2.ArticuloCod) Then
                DevolverMenor = 1
            ElseIf NullToStr(oPrecio1.ArticuloCod) = NullToStr(oPrecio2.ArticuloCod) Then
                If NullToStr(oPrecio1.Id) < NullToStr(oPrecio2.Id) Then
                    DevolverMenor = 1
                End If
            End If
        End If
    Case "INI"
        DevolverMenor = 2
    
        If oPrecio1.FechaInicioSuministro < oPrecio2.FechaInicioSuministro Then
            DevolverMenor = 1
        ElseIf oPrecio1.FechaInicioSuministro = oPrecio2.FechaInicioSuministro Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "FIN"
        DevolverMenor = 2
        
        If oPrecio1.FechaFinSuministro < oPrecio2.FechaFinSuministro Then
            DevolverMenor = 1
        ElseIf oPrecio1.FechaFinSuministro = oPrecio2.FechaFinSuministro Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "DEST"
        DevolverMenor = 2
        
        If NullToStr(oPrecio1.DestCod) < NullToStr(oPrecio2.DestCod) Then
            DevolverMenor = 1
        ElseIf NullToStr(oPrecio1.DestCod) = NullToStr(oPrecio2.DestCod) Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "UNI"
        DevolverMenor = 2
        
        If oPrecio1.UniCod < oPrecio2.UniCod Then
            DevolverMenor = 1
        ElseIf oPrecio1.UniCod = oPrecio2.UniCod Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "CANT"
        DevolverMenor = 2
        
        If NullToDbl0(oPrecio1.Cantidad) < NullToDbl0(oPrecio2.Cantidad) Then
            DevolverMenor = 1
        ElseIf NullToDbl0(oPrecio1.Cantidad) = NullToDbl0(oPrecio2.Cantidad) Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "PREC"
        DevolverMenor = 2
        
        If NullToDbl0(oPrecio1.PrecioApertura) < NullToDbl0(oPrecio2.PrecioApertura) Then
            DevolverMenor = 1
        ElseIf NullToDbl0(oPrecio1.PrecioApertura) = NullToDbl0(oPrecio2.PrecioApertura) Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "PAG"
        DevolverMenor = 2
        
        If oPrecio1.PagCod < oPrecio2.PagCod Then
            DevolverMenor = 1
        ElseIf oPrecio1.PagCod = oPrecio2.PagCod Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "PREC1"
        DevolverMenor = 2
        
        If NullToDbl0(oPrecio1.Precio) < NullToDbl0(oPrecio2.Precio) Then
            DevolverMenor = 1
        ElseIf NullToDbl0(oPrecio1.Precio) = NullToDbl0(oPrecio2.Precio) Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "PREC2"
        DevolverMenor = 2
        
        If NullToDbl0(oPrecio1.Precio2) < NullToDbl0(oPrecio2.Precio2) Then
            DevolverMenor = 1
        ElseIf NullToDbl0(oPrecio1.Precio2) = NullToDbl0(oPrecio2.Precio2) Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "PREC3"
        DevolverMenor = 2
        
        If NullToDbl0(oPrecio1.Precio3) < NullToDbl0(oPrecio2.Precio3) Then
            DevolverMenor = 1
        ElseIf NullToDbl0(oPrecio1.Precio3) = NullToDbl0(oPrecio2.Precio3) Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "CANTMAX"
        DevolverMenor = 2
        
        If NullToDbl0(oPrecio1.CantidadMaxima) < NullToDbl0(oPrecio2.CantidadMaxima) Then
            DevolverMenor = 1
        ElseIf NullToDbl0(oPrecio1.CantidadMaxima) = NullToDbl0(oPrecio2.CantidadMaxima) Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "USAR"
        DevolverMenor = 2
        
        If oPrecio1.Usar < oPrecio2.Usar Then
            DevolverMenor = 1
        ElseIf oPrecio1.Usar = oPrecio2.Usar Then
            DevolverMenor = DevolverMenor(oPrecio1, oPrecio2, "COD")
        End If
    
    Case "C#_ADJ"
        If (Not IsNull(oPrecio1.ObsAdjun) And Not IsEmpty(oPrecio1.ObsAdjun)) Or oPrecio1.NumAdjuns > 0 Then
            DevolverMenor = 1
        ElseIf (Not IsNull(oPrecio2.ObsAdjun) And Not IsEmpty(oPrecio2.ObsAdjun)) Or oPrecio2.NumAdjuns > 0 Then
            DevolverMenor = 2
        Else
            DevolverMenor = 1
        End If
        
    Case "COMENT1", "COMENT2", "COMENT3"
        'No hace nada
        DevolverMenor = 1
        
    Case Else
        lIDAtr = CLng(Right(sDato, Len(sDato) - 3))
        If m_oGrupoSeleccionado Is Nothing Then
            Set oAtributos = m_oProcesoSeleccionado.AtributosItem
        Else
            Set oAtributos = m_oGrupoSeleccionado.AtributosItem
        End If
        bExiste = False
        If Not oAtributos Is Nothing Then
        For Each oatrib In oAtributos
            If oatrib.idAtribProce = lIDAtr Then
                scod1 = CStr(oPrecio1.Id) & "$" & CStr(oatrib.idAtribProce)
                scod2 = CStr(oPrecio2.Id) & "$" & CStr(oatrib.idAtribProce)
                Select Case oatrib.Tipo
                Case TiposDeAtributos.TipoString
                    If NullToStr(oPrecio1.AtribOfertados.Item(scod1).valorText) <= NullToStr(oPrecio2.AtribOfertados.Item(scod2).valorText) Then
                        DevolverMenor = 1
                    Else
                        DevolverMenor = 2
                    End If
                
                Case TiposDeAtributos.TipoNumerico
                    If NullToDbl0(oPrecio1.AtribOfertados.Item(scod1).valorNum) <= NullToDbl0(oPrecio2.AtribOfertados.Item(scod2).valorNum) Then
                        DevolverMenor = 1
                    Else
                        DevolverMenor = 2
                    End If
                Case TiposDeAtributos.TipoFecha
                    If NullToDbl0(oPrecio1.AtribOfertados.Item(scod1).valorFec) <= NullToDbl0(oPrecio2.AtribOfertados.Item(scod2).valorFec) Then
                        DevolverMenor = 1
                    Else
                        DevolverMenor = 2
                    End If
                
                Case TiposDeAtributos.TipoBoolean
                    If Not IsNull(oPrecio1.AtribOfertados.Item(scod1).valorBool) Then
                        If Not IsNull(oPrecio2.AtribOfertados.Item(scod2).valorBool) Then
                            If oPrecio1.AtribOfertados.Item(scod1).valorBool <= oPrecio2.AtribOfertados.Item(scod2).valorBool Then
                                DevolverMenor = 1
                            Else
                                DevolverMenor = 2
                            End If
                        Else
                            DevolverMenor = 2
                        End If
                    Else
                        DevolverMenor = 1
                    End If
                
                End Select
                bExiste = True
                Exit For
            End If
        Next
        End If
        If Not bExiste Then
            DevolverMenor = 1
        End If
        Set oAtributos = Nothing
    End Select
End If
End Function

''' <summary>Devuelve el menor de los 2 objetos de entrada en funci�n del campo sDato</summary>
''' <param name="oPrecio1">Objeto 1 a comparar</param>
''' <param name="oPrecio2">Objeto 2 a comparar</param>
''' <param name="sDato">Campo de comparaci�n</param>
''' <returns>1 o 2 indicando el objeto menor</returns>
''' <remarks>Llamada desde: </remarks>
Function DevolverMenorEsc(ByVal oPrecio1 As CPrecioItem, ByVal oPrecio2 As CPrecioItem, ByVal sDato As String) As Integer
Dim lIDAtr As Long
Dim oatrib As CAtributo
Dim oAtributos As CAtributos
Dim bExiste  As Boolean
Dim scod1 As String
Dim scod2 As String
Dim bOrdenado As Boolean
Dim i, j As Integer
Dim sEscalado As String
Dim ogroup As SSDataWidgets_B.Group

Select Case sDato
Case "COD"
    DevolverMenorEsc = 2
    If NullToStr(oPrecio1.ArticuloCod) < NullToStr(oPrecio2.ArticuloCod) Then
        DevolverMenorEsc = 1
    ElseIf NullToStr(oPrecio1.ArticuloCod) = NullToStr(oPrecio2.ArticuloCod) Then
        If NullToStr(oPrecio1.Descr) < NullToStr(oPrecio2.Descr) Then
            DevolverMenorEsc = 1
        ElseIf NullToStr(oPrecio1.Descr) = NullToStr(oPrecio2.Descr) Then
            If NullToStr(oPrecio1.Id) < NullToStr(oPrecio2.Id) Then DevolverMenorEsc = 1
        End If
    End If
Case "DEN"
    DevolverMenorEsc = 2
    If NullToStr(oPrecio1.Descr) < NullToStr(oPrecio2.Descr) Then
        DevolverMenorEsc = 1
    ElseIf NullToStr(oPrecio1.Descr) = NullToStr(oPrecio2.Descr) Then
        If NullToStr(oPrecio1.ArticuloCod) < NullToStr(oPrecio2.ArticuloCod) Then
            DevolverMenorEsc = 1
        ElseIf NullToStr(oPrecio1.ArticuloCod) = NullToStr(oPrecio2.ArticuloCod) Then
            If NullToStr(oPrecio1.Id) < NullToStr(oPrecio2.Id) Then DevolverMenorEsc = 1
        End If
    End If
Case "INI"
    DevolverMenorEsc = 2
    If oPrecio1.FechaInicioSuministro < oPrecio2.FechaInicioSuministro Then
        DevolverMenorEsc = 1
    ElseIf oPrecio1.FechaInicioSuministro = oPrecio2.FechaInicioSuministro Then
        DevolverMenorEsc = DevolverMenorEsc(oPrecio1, oPrecio2, "COD")
    End If

Case "FIN"
    DevolverMenorEsc = 2
    
    If oPrecio1.FechaFinSuministro < oPrecio2.FechaFinSuministro Then
        DevolverMenorEsc = 1
    ElseIf oPrecio1.FechaFinSuministro = oPrecio2.FechaFinSuministro Then
        DevolverMenorEsc = DevolverMenorEsc(oPrecio1, oPrecio2, "COD")
    End If

Case "DEST"
    DevolverMenorEsc = 2
    
    If NullToStr(oPrecio1.DestCod) < NullToStr(oPrecio2.DestCod) Then
        DevolverMenorEsc = 1
    ElseIf NullToStr(oPrecio1.DestCod) = NullToStr(oPrecio2.DestCod) Then
        DevolverMenorEsc = DevolverMenorEsc(oPrecio1, oPrecio2, "COD")
    End If

Case "UNI"
    DevolverMenorEsc = 2
    
    If oPrecio1.UniCod < oPrecio2.UniCod Then
        DevolverMenorEsc = 1
    ElseIf oPrecio1.UniCod = oPrecio2.UniCod Then
        DevolverMenorEsc = DevolverMenorEsc(oPrecio1, oPrecio2, "COD")
    End If

Case "CANT"
    DevolverMenorEsc = 2
    
    If NullToDbl0(oPrecio1.Cantidad) < NullToDbl0(oPrecio2.Cantidad) Then
        DevolverMenorEsc = 1
    ElseIf NullToDbl0(oPrecio1.Cantidad) = NullToDbl0(oPrecio2.Cantidad) Then
        DevolverMenorEsc = DevolverMenorEsc(oPrecio1, oPrecio2, "COD")
    End If

Case "PAG"
    DevolverMenorEsc = 2
    
    If oPrecio1.PagCod < oPrecio2.PagCod Then
        DevolverMenorEsc = 1
    ElseIf oPrecio1.PagCod = oPrecio2.PagCod Then
        DevolverMenorEsc = DevolverMenorEsc(oPrecio1, oPrecio2, "COD")
    End If

Case "C#_ADJ"
    If (Not IsNull(oPrecio1.ObsAdjun) And Not IsEmpty(oPrecio1.ObsAdjun)) Or oPrecio1.NumAdjuns > 0 Then
        DevolverMenorEsc = 1
    ElseIf (Not IsNull(oPrecio2.ObsAdjun) And Not IsEmpty(oPrecio2.ObsAdjun)) Or oPrecio2.NumAdjuns > 0 Then
        DevolverMenorEsc = 2
    Else
        DevolverMenorEsc = 1
    End If
    
Case "COMENT"
    'No hace nada
    DevolverMenorEsc = 1
    
Case "PREC"
    DevolverMenorEsc = 2
    
    If NullToDbl0(oPrecio1.PrecioApertura) < NullToDbl0(oPrecio2.PrecioApertura) Then
        DevolverMenorEsc = 1
    ElseIf NullToDbl0(oPrecio1.PrecioApertura) = NullToDbl0(oPrecio2.PrecioApertura) Then
        DevolverMenorEsc = DevolverMenorEsc(oPrecio1, oPrecio2, "COD")
    End If
    
Case Else
    
    bOrdenado = False
    For i = 1 To sdbgPreciosEscalados.Groups.Count - 1
        If sdbgPreciosEscalados.Groups(i).TagVariant <> "ATRIBSNOCOSTESDESC" And sdbgPreciosEscalados.Groups(i).TagVariant <> "ADJUNTOS" Then
            sEscalado = sdbgPreciosEscalados.Groups(i).TagVariant
            Set ogroup = sdbgPreciosEscalados.Groups(i)
            If sDato = "PresUni_" & sEscalado Then
                If NullToStr(oPrecio1.Escalados.Item(sEscalado).Presupuesto) <= NullToStr(oPrecio2.Escalados.Item(sEscalado).Presupuesto) Then
                    DevolverMenorEsc = 1
                    bOrdenado = True
                Else
                    DevolverMenorEsc = 2
                    bOrdenado = True
                End If
            End If
            If sDato = "Precio_" & sEscalado Then
                If NullToStr(oPrecio1.Escalados.Item(sEscalado).Precio) <= NullToStr(oPrecio2.Escalados.Item(sEscalado).Precio) Then
                    DevolverMenorEsc = 1
                    bOrdenado = True
                Else
                    DevolverMenorEsc = 2
                    bOrdenado = True
                End If
            End If
            For j = 2 To ogroup.Columns.Count - 1
                If Left(ogroup.Columns(j).Name, 3) = "ATE" Then
                    If sDato = ogroup.Columns(j).Name Then
                        If NullToStr(oPrecio1.Escalados.Item(sEscalado).valorNum) <= NullToStr(oPrecio2.Escalados.Item(sEscalado).valorNum) Then
                            DevolverMenorEsc = 1
                            bOrdenado = True
                        Else
                            DevolverMenorEsc = 2
                            bOrdenado = True
                        End If
                    End If
                End If
            Next j
        End If
    Next
        
    If Not bOrdenado Then
        lIDAtr = CLng(Right(sDato, Len(sDato) - 3))
        If m_oGrupoSeleccionado Is Nothing Then
            Set oAtributos = m_oProcesoSeleccionado.AtributosItem
        Else
            Set oAtributos = m_oGrupoSeleccionado.AtributosItem
        End If
        bExiste = False
        If Not oAtributos Is Nothing Then
        For Each oatrib In oAtributos
            If oatrib.idAtribProce = lIDAtr Then
                scod1 = CStr(oPrecio1.Id) & "$" & CStr(oatrib.idAtribProce)
                scod2 = CStr(oPrecio2.Id) & "$" & CStr(oatrib.idAtribProce)
                Select Case oatrib.Tipo
                Case TiposDeAtributos.TipoString
                    If NullToStr(oPrecio1.AtribOfertados.Item(scod1).valorText) <= NullToStr(oPrecio2.AtribOfertados.Item(scod2).valorText) Then
                        DevolverMenorEsc = 1
                    Else
                        DevolverMenorEsc = 2
                    End If
                
                Case TiposDeAtributos.TipoNumerico
                    If NullToDbl0(oPrecio1.AtribOfertados.Item(scod1).valorNum) <= NullToDbl0(oPrecio2.AtribOfertados.Item(scod2).valorNum) Then
                        DevolverMenorEsc = 1
                    Else
                        DevolverMenorEsc = 2
                    End If
                Case TiposDeAtributos.TipoFecha
                    If NullToDbl0(oPrecio1.AtribOfertados.Item(scod1).valorFec) <= NullToDbl0(oPrecio2.AtribOfertados.Item(scod2).valorFec) Then
                        DevolverMenorEsc = 1
                    Else
                        DevolverMenorEsc = 2
                    End If
                
                Case TiposDeAtributos.TipoBoolean
                    If Not IsNull(oPrecio1.AtribOfertados.Item(scod1).valorBool) Then
                        If Not IsNull(oPrecio2.AtribOfertados.Item(scod2).valorBool) Then
                            If oPrecio1.AtribOfertados.Item(scod1).valorBool <= oPrecio2.AtribOfertados.Item(scod2).valorBool Then
                                DevolverMenorEsc = 1
                            Else
                                DevolverMenorEsc = 2
                            End If
                        Else
                            DevolverMenorEsc = 2
                        End If
                    Else
                        DevolverMenorEsc = 1
                    End If
                
                End Select
                bExiste = True
                Exit For
    
            End If
        Next
        End If
        If Not bExiste Then
            DevolverMenorEsc = 1
        End If
        Set oAtributos = Nothing
    End If
End Select

End Function

Private Function MostrarSobre()
Dim oUsuarios  As CUsuarios

    With g_oSobreSeleccionado
        If .Estado = 1 Then
            picStop.Visible = False
            lblEstado = sIdiEstadoSobreAbierto
            lblMsjSobreCerrado.Visible = False
            lblFechaRealAperturaSobre.Visible = True
            txtFechaRealAperturaSobre.Visible = True
            cmdAbrirSobre.Visible = False
            cmdRestaurar(4).Left = Me.cmdAbrirSobre.Left
        Else
            picStop.Visible = True
            lblMsjSobreCerrado.Visible = True
            lblEstado = sIdiEstadoSobreCerrado
            lblFechaRealAperturaSobre.Visible = False
            txtFechaRealAperturaSobre.Visible = False
            
            If .Usuario = basOptimizacion.gvarCodUsuario And .FechaAperturaPrevista <= oFSGSRaiz.DevolverFechaServidor() Then
                cmdAbrirSobre.Visible = True
                cmdRestaurar(4).Left = Me.cmdAbrirSobre.Left + Me.cmdAbrirSobre.Width + 120
            Else
                cmdAbrirSobre.Visible = False
                cmdRestaurar(4).Left = Me.cmdAbrirSobre.Left
            End If
        End If
        
        lblCabeceraSobre.caption = sIdiSobre & g_oSobreSeleccionado.Sobre
        
        txtObsSobre = NullToStr(.Observaciones)
        
        Set oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios(.Usuario, , True, True)
        txtUsuAperSobre = .Usuario & ", " & oUsuarios.Item(.Usuario).Persona.nombre & " " & oUsuarios.Item(.Usuario).Persona.Apellidos
        Set oUsuarios = Nothing
        txtFechaAperturaSobre = .FechaAperturaPrevista
        txtFechaRealAperturaSobre = NullToStr(.FechaAperturaReal)
    End With
            
    MostrarApartado iAptSobre
End Function

Private Function BloquearProceso(Optional ByVal sDesde As String) As Boolean
Dim udtTeserror As TipoErrorSummit
'Bloquea el proceso
'sDesde desde donde se quiere bloquear, p.e. desde modificaci�n de items hay que comprobar si estamos en edici�n
'si es vacio no se comprueba ninguna variable
'"IT", miramos si g_bModoEdicionItem=False entonces bloqueamos
    Select Case sDesde
    Case "IT"
        If bModoEdicion Then
            BloquearProceso = True
            Exit Function
        End If
    End Select
    
    Screen.MousePointer = vbHourglass
    
    Set oIBaseBloqueo = m_oProcesoSeleccionado
    
    udtTeserror = oIBaseBloqueo.IniciarEdicion(True, basOptimizacion.gvarCodUsuario)
    If udtTeserror.NumError <> TESnoerror And udtTeserror.NumError <> TESInfActualModificada Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError udtTeserror
        oIBaseBloqueo.CancelarEdicion
        Set oIBaseBloqueo = Nothing
        BloquearProceso = False
        Exit Function
    End If

    Screen.MousePointer = vbNormal
    
    'A�ado a la variable de control de bloqueos el form y activo el timer del MDI
    If Not g_dicBloqueos.Exists("frmOFERec") Then g_dicBloqueos.Add "frmOFERec", "frmOFERec"
    g_iContadorBloqueo = 0
    MDI.Timer1.Enabled = True
    
    BloquearProceso = True
End Function

Public Sub DesbloquearProceso(Optional ByVal sDesde As String)
    
    Select Case sDesde
        Case "IT"
            If bModoEdicion Then Exit Sub
    End Select
    
    MDI.Timer1.Enabled = False
    If g_dicBloqueos.Exists("frmOFERec") Then g_dicBloqueos.Remove "frmOFERec"
    m_udtOrigBloqueo = OrigenBloqueo.NoBloqueo
    g_iContadorBloqueo = 0
    
    If Not oIBaseBloqueo Is Nothing Then
        oIBaseBloqueo.CancelarEdicion
        Set oIBaseBloqueo = Nothing
    End If

    Screen.MousePointer = vbNormal
End Sub

Public Sub TimeoutBloqueo()
Dim bMensaje As Boolean

    bMensaje = True
    frmMessageBox.g_bCancel = True
    Unload frmMessageBox
    Select Case m_udtOrigBloqueo
        Case Modificar
            Unload frmCalendar
            cmdCancelar_Click
        Case ModifItems
            Unload frmDetAtribProce
            Unload frmPROCEComFich
            Unload frmPROCEEspMod
            Unload frmOFERecAdj
            Unload frmItemDetalle
            If picApartado(2).Visible Then
                'Atributos
                If sdbgAtributos.DataChanged Then
                    sdbgAtributos.CancelUpdate
                    sdbgAtributos.DataChanged = False
                End If
            ElseIf picApartado(4).Visible Then
                'Items
                If m_bGrupoConEscalados Then
                    If sdbgPreciosEscalados.DataChanged Then
                        sdbgPreciosEscalados.CancelUpdate
                        sdbgPreciosEscalados.DataChanged = False
                    End If
                Else
                    If sdbgPrecios.DataChanged Then
                        sdbgPrecios.CancelUpdate
                        sdbgPrecios.DataChanged = False
                    End If
                End If
            End If
            cmdModoEdicion_Click
        Case Anyadir
            Unload frmCalendar
            cmdCancelar_Click
        Case Eliminar
        Case responsable
            Unload frmMensaje
            Unload frmSELPROVEResp
        Case Else
            bMensaje = False
    End Select

    If bMensaje Then
        Unload frmEsperaPortal
        If Not oIBaseBloqueo Is Nothing Then DesbloquearProceso
        g_iContadorBloqueo = 0
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Generacion del excel de una oferta
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:cmdgenerarXLS Tiempo m�ximo:0</remarks>

Private Sub GenerarExcel()

Dim xls() As String
Dim iNumFilasPorAtrib As Integer
Dim oProceso As cProceso
Dim oOferta As COferta
Dim oProve As CProveedor
Dim sTarget As String

On Error GoTo ERROR

iNumFilasPorAtrib = 13

Set oProceso = m_oProcesoSeleccionado
Set oProve = m_oProveSeleccionado

'Personalizado formLlamada
'Depende del formulario desde que se llama...

cmmdAdjun.CancelError = True
cmmdAdjun.FileTitle = ""
cmmdAdjun.DialogTitle = sIdiDialogoExcel
cmmdAdjun.Filter = sIdiFiltroExcel & "|*.xls"

cmmdAdjun.filename = ValidFilename(oProceso.Anyo & "_" & oProceso.GMN1Cod & "_" & oProceso.Cod & "_" & oProve.Cod & ".xls")
cmmdAdjun.ShowSave

If cmmdAdjun.FileTitle = "" Then Exit Sub

sTarget = cmmdAdjun.filename
Screen.MousePointer = vbHourglass

Set oOferta = oProceso.CargarUltimaOfertaProveedor(oProve.Cod)

Dim oExcel As CExcelOfertas
Set oExcel = New CExcelOfertas
If oExcel.FSXLSOfertaOffline(oProceso, oProve.Cod, m_oAsigs, sTarget, basParametros.gLongitudesDeCodigos.giLongCodPROVE, oFSGSRaiz, basPublic.gParametrosInstalacion.gIdioma, gParametrosGenerales.gbProveGrupos, _
        oGestorIdiomas, gParametrosInstalacion.giCargaMaximaCombos, g_bIncluir, gParametrosInstalacion.giAnyadirEspecArticulo, m_oOfertasProceso, lblTZ.caption) Then
    oMensajes.ExcelGeneradaCorrectamente (sTarget)
End If
Set oExcel = Nothing

Screen.MousePointer = vbNormal

fin:
Exit Sub

ERROR:
If err.Number = 9 Then
    ReDim Preserve xls(0 To UBound(xls, 1), 0 To UBound(xls, 2) + 10) As String
    Resume 0
ElseIf err.Number = 32755 Then
    Resume fin
Else
    Resume Next
    Resume 0
End If

End Sub

''' <summary>
''' Cargar una oferta desde un excel
''' </summary>
''' <remarks>Llamada desde: cmdCargarXLS_Click; Tiempo m�ximo:0,3</remarks>
Public Sub ObtenerDatosDeXLS()
    Dim sConnect As String
    Dim oExcelAdoConn As adodb.Connection
    Dim oExcelAdoRS As adodb.Recordset
    Dim codgrupo As String
    Dim xls()
    Dim oProceso As cProceso
    Dim oOferta As COferta
    Dim oPrecioItems As CPrecioItems
    Dim i As Long
    Dim j As Long
    Dim oAtribs As CAtributos
    Dim oAtribsOfertados As FSGSServer.CAtributosOfertados
    Dim oatrib As CAtributo
    Dim iNumItems As Integer
    Dim oGrupo As CGrupo
    Dim oOfeGrupo As COfertaGrupo
    Dim oPrecioItem As CPrecioItem
    Dim lFilaComienzoAtribsGrupo As Long
    Dim lFilaComienzoAtribsItem As Long
    Dim lfilacomienzoitems  As Long
    Dim lfilacomienzoEscalados  As Long '
    Dim iNumFilasPorAtrib As Integer
    Dim iNumFilasPorItem  As Integer
    Dim iNumFilasPorItemSinAtribs As Integer
    Dim IdItem As Integer
    Dim dCantidad As Variant
    Dim iEsc As Long
    Dim dPU As Variant
    Dim dPUS() As Variant
    Dim dCantMax As Variant
    Dim dPU2 As Variant
    Dim dPU3 As Variant
    Dim k As Integer
    Dim sComent1 As Variant
    Dim sComent2 As Variant
    Dim sComent3 As Variant
    Dim idAtrib As Long
    Dim TipoAtrib As Integer
    Dim aplicAtrib As Variant
    Dim sOp As Variant
    Dim vAtrib As Variant
    Dim sValor As Variant
    Dim vValor As Variant
    Dim sSource As String
    Dim AtribOblig As Integer
    Dim sAtribOblig As String
    Dim iEstado As Integer
    Dim lNumEscaladosGrupo As Long
    Dim fi As Long ' Fila actual en el procesado del Excel
    Dim vAtribs() As Variant
    Dim HayEscaladoAtrib As Boolean
    Dim desplazaAtrib As Long

    On Error GoTo ERROR:
    
    cmmdAdjun.CancelError = True
    cmmdAdjun.DialogTitle = sIdiDialogoCargarXls
    cmmdAdjun.Filter = sIdiFiltroExcel & "|*.xls"
    cmmdAdjun.filename = ValidFilename(m_oProcesoSeleccionado.Anyo & "_" & m_oProcesoSeleccionado.GMN1Cod & "_" & m_oProcesoSeleccionado.Cod & "_" & m_oProveSeleccionado.Cod & ".xls")
    cmmdAdjun.ShowOpen
    
    sSource = cmmdAdjun.filename
    
    If cmmdAdjun.FileTitle = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    iEstado = 0
    
    Dim sDecFmt As String
    Dim sDateFmt As String
    Dim teserror As TipoErrorSummit
    
    Set oExcelAdoConn = New adodb.Connection
    sConnect = "Provider=MSDASQL.1;" _
             & "Extended Properties=""DBQ=" & sSource & ";" _
             & "Driver={Microsoft Excel Driver (*.xls)};" _
             & "FIL=excel 8.0;" _
             & "ReadOnly=0;" _
             & "UID=admin;"""
             
    oExcelAdoConn.Open sConnect
    
    iEstado = 1 'Conexion a excel
    
    Set oExcelAdoRS = New adodb.Recordset
    
    oExcelAdoRS.Open "SELECT * from [Proceso$]", oExcelAdoConn
    
    xls = oExcelAdoRS.GetRows()
    
    iEstado = 2 'Filas recuperadas
    
    oExcelAdoRS.Close
    Set oExcelAdoRS = Nothing
    
    oExcelAdoConn.Close
    Set oExcelAdoConn = Nothing
    
    Dim iFilasPorDestino As Integer
    Dim iAdminPublica As Variant
    
    If xls(1, 7) = "1" Then
        iFilasPorDestino = 7
    Else
        iFilasPorDestino = 0
    End If
    If IsNull(xls(1, 19)) Then
        xls(1, 19) = 0
    End If
    
    iAdminPublica = xls(1, (22 + iFilasPorDestino + xls(1, 19) * 2))
    
    Dim sVersion As String
    Dim iVersion As Integer
    
    If Not IsNull(xls(UBound(xls, 1) - 3, 3)) Then iVersion = xls(UBound(xls, 1) - 3, 3)
    
    Select Case iVersion
        Case 3:
            sVersion = "2.12"
        Case 4:
            sVersion = "2.14"
        Case Else:
            If IsNull(iAdminPublica) Then
                sVersion = "2.10.5"
            Else
                sVersion = "2.11"
            End If
    End Select
    
    iEstado = 3 'Versi�n recuperada
    
    Set oProceso = oFSGSRaiz.Generar_CProceso
    
    oProceso.Anyo = xls(1, 0)
    oProceso.GMN1Cod = xls(1, 1)
    oProceso.Cod = xls(1, 2)
    
    sDecFmt = Replace(xls(0, 0), "0", "")
    sDateFmt = ObtenerFmtDesdeFecha(xls(0, 2))
    
    If oProceso.Anyo <> m_oProcesoSeleccionado.Anyo Or oProceso.GMN1Cod <> m_oProcesoSeleccionado.GMN1Cod Or oProceso.Cod <> m_oProcesoSeleccionado.Cod Then
        oMensajes.ErrorAlExportarExcel
        Screen.MousePointer = vbNormal
        Set oProceso = Nothing
        Exit Sub
    End If
    
    iEstado = 4 'El proceso es v�lido
    
    oProceso.CargarDatosGeneralesProceso
    
    If oProceso.Estado = 12 Or oProceso.Estado = 13 Or oProceso.Estado = 20 Then
        Screen.MousePointer = vbNormal
        Set oProceso = Nothing
        Exit Sub
    End If
    
    Set oOferta = oFSGSRaiz.Generar_COferta
    Set oOferta.Usuario = oUsuarioSummit
    
    Set oPrecioItems = oFSGSRaiz.Generar_CPrecioItems
    
    oOferta.Prove = m_oProveSeleccionado.Cod
    
    If xls(2, 0) = " " Then xls(2, 0) = Null
    oOferta.FechaHasta = Fecha(xls(2, 0), sDateFmt)
    oOferta.CodMon = Trim(Left(xls(2, 1), 6))
    
    'Comprobar la moneda de la oferta
    Dim sMonCodAdj As String
    If oProceso.HayAdjudicacionesPreviasProveOtraMon(oOferta.Prove, oOferta.CodMon, sMonCodAdj) Then
        oMensajes.OfertaIncorrectaMonedaDistintaAdj
        Screen.MousePointer = vbNormal
        Set oProceso = Nothing
        Set oOferta = Nothing
        Exit Sub
    End If
    
    With oOferta
        Dim oMonedas As CMonedas
        Set oMonedas = oFSGSRaiz.Generar_CMonedas
        oMonedas.CargarTodasLasMonedasDesde 1, .CodMon, , , , , True
        If oProceso.MonCod = oOferta.CodMon Then
            .Cambio = 1
        Else
            .Cambio = oMonedas.Item(1).Equiv / oProceso.Cambio
        End If
        Set oMonedas = Nothing
    End With
    
    oOferta.obs = IIf(IsNull(xls(2, 2)), "", xls(2, 2))
    
    Set oOferta.proceso = oProceso
    
    iEstado = 5 'Datos generales de la oferta
    
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
    Set oAtribsOfertados = oFSGSRaiz.Generar_CAtributosOfertados
    
    Select Case sVersion
        Case "2.12"
            iNumFilasPorAtrib = 12
        Case "2.14"
            iNumFilasPorAtrib = 13
        Case Else
            iNumFilasPorAtrib = 11
    End Select
    
    i = 0
    If IsNull(xls(2, 3)) Then
        xls(2, 3) = 0
    End If
    For j = 0 To xls(2, 3) - 1
        Set oatrib = oAtribs.Add(Id:=xls(2, i + 4), idAtribProce:=xls(2, i + 5), Cod:=xls(2, i + 6), Den:=xls(2, i + 7), TipoIntroduccion:=IntroLibre, Tipo:=xls(2, i + 10))
        
        sValor = IIf(IsNull(xls(2, i + 8)), "", xls(2, i + 8))
        TipoAtrib = xls(2, i + 10)
        AtribOblig = xls(2, i + 15)
        
        If AtribOblig = 1 And m_oProcesoSeleccionado.AtributosOfeObl And (sValor = "" Or IsNull(sValor)) Then
            sAtribOblig = sAtribOblig & vbCrLf & oatrib.Den
        End If
        Select Case TipoAtrib
            Case 1:
                vValor = sValor
                oAtribsOfertados.Add oatrib.idAtribProce, oOferta, , , , , , , vValor
            Case 2:
                vValor = Numero(sValor, sDecFmt)
                oAtribsOfertados.Add oatrib.idAtribProce, oOferta, , , , , vValor
            Case 3:
                vValor = Fecha(sValor, sDateFmt)
                oAtribsOfertados.Add oatrib.idAtribProce, oOferta, , , , vValor
            Case 4:
                If sValor = xls(0, 45) Then
                    vValor = True
                ElseIf sValor = xls(0, 46) Then
                    vValor = False
                Else
                    vValor = Null
                End If
                oAtribsOfertados.Add oatrib.idAtribProce, oOferta, , , vValor
        End Select
    
        i = i + iNumFilasPorAtrib
    Next j
    
    iEstado = 6 'Atributos a nivel de proceso de la oferta
    
    Set oOferta.AtribProcOfertados = oAtribsOfertados
    Set oProceso.Grupos = oFSGSRaiz.Generar_CGrupos
    Set oOferta.Grupos = oFSGSRaiz.Generar_COfertaGrupos
    
    i = 1
    j = 3
    
    For i = 0 To xls(1, 16) - 1
        codgrupo = xls(i + 3, 0)
    
        Set oGrupo = oProceso.Grupos.Add(oProceso, codgrupo, "no importa")
        
        iNumItems = xls(i + 3, 18)
        oGrupo.NumAtribsGrupo = xls(i + 3, 19)
        oGrupo.NumAtribsItem = xls(i + 3, 20)
    
        lFilaComienzoAtribsGrupo = xls(i + 3, 21) - 2
        lFilaComienzoAtribsItem = xls(i + 3, 24) - 2
        lfilacomienzoEscalados = xls(i + 3, 25) - 2
        lfilacomienzoitems = xls(i + 3, 26) - 2
        
        lNumEscaladosGrupo = xls(i + 3, lfilacomienzoEscalados)
        
        If sVersion = "2.10.5" Then
            iNumFilasPorItemSinAtribs = 20
        Else
            iNumFilasPorItemSinAtribs = 23
        End If
            
        iNumFilasPorItem = iNumFilasPorItemSinAtribs + oGrupo.NumAtribsItem + 1
            
        Set oOfeGrupo = oOferta.Grupos.Add(oOferta, oGrupo)
        Set oOfeGrupo.AtribOfertados = oFSGSRaiz.Generar_CAtributosOfertados
        
        Set oGrupo.ATRIBUTOS = oFSGSRaiz.Generar_CAtributos
        
        For j = 0 To oGrupo.NumAtribsGrupo - 1
            
            Set oatrib = oGrupo.ATRIBUTOS.Add(Id:=j, idAtribProce:=xls(i + 3, lFilaComienzoAtribsGrupo + (j * iNumFilasPorAtrib) + 1), Cod:="", Den:="", TipoIntroduccion:=IntroLibre, Tipo:=xls(i + 3, lFilaComienzoAtribsGrupo + (j * iNumFilasPorAtrib) + 6))
            sValor = xls(i + 3, lFilaComienzoAtribsGrupo + (j * iNumFilasPorAtrib) + 4)
            TipoAtrib = xls(i + 3, lFilaComienzoAtribsGrupo + (j * iNumFilasPorAtrib) + 6)
            AtribOblig = xls(i + 3, lFilaComienzoAtribsGrupo + (j * iNumFilasPorAtrib) + 11)
            
            If AtribOblig = 1 And m_oProcesoSeleccionado.AtributosOfeObl And (sValor = "" Or IsNull(sValor)) Then
                sAtribOblig = sAtribOblig & vbCrLf & oatrib.Den
            End If
    
            Select Case TipoAtrib
                Case 1:
                    vValor = sValor
                    oOfeGrupo.AtribOfertados.Add oatrib.idAtribProce, oOferta, , , , , , , vValor
                Case 2:
                    vValor = Numero(sValor, sDecFmt)
                    oOfeGrupo.AtribOfertados.Add oatrib.idAtribProce, oOferta, , , , , vValor
                Case 3:
                    vValor = Fecha(sValor, sDateFmt)
                    oOfeGrupo.AtribOfertados.Add oatrib.idAtribProce, oOferta, , , , vValor
                Case 4:
                    If sValor = xls(0, 45) Then
                        vValor = True
                    ElseIf sValor = xls(0, 46) Then
                        vValor = False
                    Else
                        vValor = Null
                    End If
                    oOfeGrupo.AtribOfertados.Add oatrib.idAtribProce, oOferta, , , vValor
            End Select
        Next j
    
        Set oGrupo.AtributosItem = oFSGSRaiz.Generar_CAtributos
        For j = 0 To oGrupo.NumAtribsItem - 1
            oGrupo.AtributosItem.Add j, idAtribProce:=xls(i + 3, lFilaComienzoAtribsItem + (j * iNumFilasPorAtrib) + 1), Cod:="", Den:="", TipoIntroduccion:=IntroLibre, Tipo:=xls(i + 3, lFilaComienzoAtribsItem + (j * iNumFilasPorAtrib) + 6)
        Next
    
        Set oOfeGrupo.Lineas = oFSGSRaiz.Generar_CPrecioItems
        Set oGrupo.Items = oFSGSRaiz.Generar_CItems
    
        For j = 0 To iNumItems - 1
            IdItem = xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 0)
            dCantidad = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 9), sDecFmt)
            
            fi = 12
            
            '@@DPD Precio Escalado
            If lNumEscaladosGrupo > 0 Then
                ReDim dPUS(lNumEscaladosGrupo)
                For iEsc = 1 To lNumEscaladosGrupo
                    dPUS(iEsc) = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + fi), sDecFmt)
                    fi = fi + 1
                Next iEsc
                dPU = Null 'Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 12), sDecFmt)
                
                If j = 0 Then
                    iNumFilasPorItem = iNumFilasPorItem + (3 * (lNumEscaladosGrupo - 1))
                    iNumFilasPorItemSinAtribs = iNumFilasPorItemSinAtribs + (3 * (lNumEscaladosGrupo - 1))
                End If
            Else
                dPU = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 12), sDecFmt)
            End If
            dCantMax = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 13), sDecFmt)
            sComent1 = xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 14)
            dPU2 = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 15), sDecFmt)
            sComent2 = xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 16)
            dPU3 = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 17), sDecFmt)
            sComent3 = xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + 18)
            
            Set oPrecioItem = oOfeGrupo.Lineas.Add(oOferta, IdItem, "", "", "", 0, #1/1/2001#, #1/1/2001#, , dCantMax, , , , , , , , dPU, dPU2, dPU3, , , , sComent1, sComent2, sComent3)
            
            'Escalados
            '@@DPD Precio Escalado
            If lNumEscaladosGrupo > 0 Then
                Set oPrecioItem.Escalados = oFSGSRaiz.Generar_CEscalados
                For iEsc = 1 To lNumEscaladosGrupo
                    oPrecioItem.Escalados.Add xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 1), xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 2), xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 3), , , dPUS(iEsc)
                    If oPrecioItem.Escalados.Usar(CStr(xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 1)), dCantidad) Then
                        If Not IsNull(dPUS(iEsc)) Then
                            oPrecioItem.Precio = CDbl(dPUS(iEsc))
                        End If
                    End If
                Next iEsc
            End If
            
            desplazaAtrib = 0
            Set oPrecioItem.AtribOfertados = oFSGSRaiz.Generar_CAtributosOfertados
            
            For k = 0 To oGrupo.NumAtribsItem - 1
                idAtrib = xls(i + 3, lFilaComienzoAtribsItem + (k * iNumFilasPorAtrib) + 1)
                TipoAtrib = xls(i + 3, lFilaComienzoAtribsItem + (k * iNumFilasPorAtrib) + 6)
                AtribOblig = xls(i + 3, lFilaComienzoAtribsItem + (k * iNumFilasPorAtrib) + 11)
                aplicAtrib = xls(i + 3, lFilaComienzoAtribsItem + (k * iNumFilasPorAtrib) + 10)
                sOp = xls(i + 3, lFilaComienzoAtribsItem + (k * iNumFilasPorAtrib) + 9)
                
                HayEscaladoAtrib = False
                
                Select Case TipoAtrib
                    Case 1:
                        vAtrib = xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + iNumFilasPorItemSinAtribs + desplazaAtrib)
                        If vAtrib = "" Then vAtrib = Null
                        oPrecioItem.AtribOfertados.Add idAtrib, oOferta, , , , , , , vAtrib
                    Case 2:
                        If lNumEscaladosGrupo > 0 Then
                            If Not IsNull(sOp) And aplicAtrib >= 2 Then
                                'Hay escalados
                                HayEscaladoAtrib = True
                                ReDim vAtribs(lNumEscaladosGrupo)
                                For iEsc = 1 To lNumEscaladosGrupo
                                    vAtribs(iEsc) = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + iNumFilasPorItemSinAtribs + desplazaAtrib + (iEsc - 1)), sDecFmt)
                                    'Atributo Escalado
                                Next iEsc
                                desplazaAtrib = desplazaAtrib + (lNumEscaladosGrupo - 1)
                            Else
                                vAtrib = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + iNumFilasPorItemSinAtribs + desplazaAtrib), sDecFmt)
                                If vAtrib = "" Then
                                    vAtrib = Null
                                End If
                                oPrecioItem.AtribOfertados.Add idAtrib, oOferta, , , , , vAtrib
                            End If
                        Else
                            vAtrib = Numero(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + iNumFilasPorItemSinAtribs + desplazaAtrib), sDecFmt)
                            If vAtrib = "" Then vAtrib = Null
                            oPrecioItem.AtribOfertados.Add idAtrib, oOferta, , , , , vAtrib
                        End If
                    Case 3:
                        vAtrib = Fecha(xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + iNumFilasPorItemSinAtribs + desplazaAtrib), sDateFmt)
                        If vAtrib = "" Then vAtrib = Null
                        oPrecioItem.AtribOfertados.Add idAtrib, oOferta, , , , vAtrib
                    Case 4:
                        vAtrib = xls(i + 3, lfilacomienzoitems + (j * iNumFilasPorItem) + iNumFilasPorItemSinAtribs + desplazaAtrib)
                        If vAtrib = xls(0, 45) Then
                            vAtrib = True
                        ElseIf vAtrib = xls(0, 46) Then
                            vAtrib = False
                        Else
                            vAtrib = Null
                        End If
                        If vAtrib = "" Then vAtrib = Null
                        oPrecioItem.AtribOfertados.Add idAtrib, oOferta, , , vAtrib
                End Select
                desplazaAtrib = desplazaAtrib + 1
                                    
                    If HayEscaladoAtrib Then
                        'Atributo Escalado
                        oPrecioItem.AtribOfertados.Add idAtrib, oOferta, , , , , Null
                        Set oPrecioItem.AtribOfertados.Item(CStr(idAtrib)).Escalados = oFSGSRaiz.Generar_CEscalados
                        For iEsc = 1 To lNumEscaladosGrupo
                            If vAtribs(iEsc) = "" Then vAtribs(iEsc) = Null
                            oPrecioItem.AtribOfertados.Item(CStr(idAtrib)).Escalados.Add xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 1), xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 2), xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 3), , , vAtribs(iEsc)
                            If oPrecioItem.AtribOfertados.Item(CStr(idAtrib)).Escalados.Usar(CStr(xls(i + 3, lfilacomienzoEscalados + (iEsc - 1) * 3 + 1)), dCantidad) Then
                                oPrecioItem.AtribOfertados.Item(CStr(idAtrib)).valorNum = vAtribs(iEsc)
                            End If
                        Next iEsc
                        If j = 0 Then
                            'la primera vez que recorramos los atributos de un item, sumamos las filas que ocupan los escalados de los atributos
                            iNumFilasPorItem = iNumFilasPorItem + (lNumEscaladosGrupo - 1)
                        End If
                    End If
                
                If Not IsNull(dPU) Or Not IsNull(dPU2) Or Not IsNull(dPU3) Then
                    If AtribOblig = 1 And m_oProcesoSeleccionado.AtributosOfeObl And IsNull(vAtrib) Then
                        sAtribOblig = sAtribOblig & vbCrLf & oatrib.Den
                    End If
                End If
            Next k
        Next j
    Next i
    
    If sAtribOblig <> "" Then
        oMensajes.ImposibleCargarExcel
        GoTo fin
    End If
    
    iEstado = 7 'Jerarquia de la oferta completa
    
    teserror = oOferta.AlmacenarOfertaXLS(m_sTemp)
    
    If teserror.NumError <> TESnoerror Then
        If teserror.NumError = TESFaltanDatos Then
            oMensajes.ErrorAlTransferirExcel
        Else
            oMensajes.OtroError teserror.NumError, teserror.Arg1
        End If
    Else
        If m_oOfertasProceso.Count = 0 Then
            basSeguridad.RegistrarAccion ACCRecOfeAnya, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & m_oProveSeleccionado.Cod & "Oferta cargada desde excel, �ltima: 1"
        Else
            basSeguridad.RegistrarAccion ACCRecOfeAnya, "Anyo:" & sdbcAnyo.Text & "GMN1:" & sdbcGMN1_4Cod.Text & "Proce:" & sdbcProceCod.Text & "Prove:" & m_oProveSeleccionado.Cod & "Oferta cargada desde excel, �ltima: " & CStr(m_oOfertasProceso.Item(m_oOfertasProceso.Count).Num + 1)
        End If
    
        ProcesoSeleccionado
        
        oMensajes.ExcelCargadaCorrectamnte
    End If
    
fin:
    Screen.MousePointer = vbNormal
    On Error Resume Next
    Set oAtribs = Nothing
    Set oAtribsOfertados = Nothing
    Set oPrecioItems = Nothing
    Set oPrecioItem = Nothing
    Set oOferta.Grupos = Nothing
    Set oOferta = Nothing
    Set oProceso.Grupos = Nothing
    Set oProceso = Nothing
    Exit Sub
ERROR:
    If err.Number <> cdlCancel Then oMensajes.ErrorAlCargarExcel (iEstado)
    Resume fin
    Resume 0
End Sub

Private Sub UpDownDec_Change()
    Dim teserror As TipoErrorSummit
    
    If UpDownDec.Tag = "C" Then Exit Sub
    
    'Asocia el valor del control Updown al texbox:
    txtNumDec.Text = UpDownDec.Value
    
    'Almacena en BD el valor de los decimales para el usuario y proceso:
    teserror = m_oProcesoSeleccionado.GuardarNumDecimalesOfe(oUsuarioSummit.Cod, Trim(txtNumDec.Text), False)
    
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Exit Sub
    Else
        FormateoDecimales True
    End If
End Sub

''' <summary>
''' Formatea los campos num�ricos con decimales (precios, atributos, cantidad) con y sin escalados
''' </summary>
''' <param name="bAtrib">Indica si se formatean los atributos o no</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: ConfigurarGridItems; ConfigurarGridItemsEscalados;
''' UpDownDec_Change()(); Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 02/12/2011</revision>
Private Sub FormateoDecimales(Optional ByVal bAtrib As Boolean)
    Dim oAtribs As CAtributos
    Dim oatrib  As CAtributo
    Dim ogroup As SSDataWidgets_B.Group
    Dim i, j As Integer
    Dim sEscalado As String

    '''***Formateo num�rico para la grid de precios***
    
    If m_bGrupoConEscalados Then
    ' Aplicar formateo a los precios y a los atributos de los escalados
        For i = 1 To sdbgPreciosEscalados.Groups.Count - 1
            If sdbgPreciosEscalados.Groups(i).TagVariant <> "ATRIBSNOCOSTESDESC" And sdbgPreciosEscalados.Groups(i).TagVariant <> "ADJUNTOS" Then
                sEscalado = sdbgPreciosEscalados.Groups(i).TagVariant
                Set ogroup = sdbgPreciosEscalados.Groups(i)
                sdbgPreciosEscalados.Columns("Precio_" & sEscalado).NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
                sdbgPreciosEscalados.Columns("PresUni_" & sEscalado).NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
                If bAtrib = True Then
                    For j = 2 To ogroup.Columns.Count - 1
                        If Left(ogroup.Columns(j).Name, 3) = "ATE" Then
                            ogroup.Columns(j).NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
                        End If
                    Next j
                End If
            End If
        Next
        sdbgPreciosEscalados.Columns("CANT").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
        sdbgPreciosEscalados.Columns("PREC").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
    Else
        sdbgPrecios.Columns("PREC").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
        sdbgPrecios.Columns("PREC1").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
        sdbgPrecios.Columns("PREC2").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
        sdbgPrecios.Columns("PREC3").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
        sdbgPrecios.Columns("CANT").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
        sdbgPrecios.Columns("CANTMAX").NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)

        'Formateo num�rico de los atributos:
        If bAtrib = True Then
            If g_oGrupoOferSeleccionado Is Nothing Then
                If Not m_oProcesoSeleccionado.AtributosItem Is Nothing Then
                    Set oAtribs = m_oProcesoSeleccionado.AtributosItem
                End If
            Else
                If Not m_oGrupoSeleccionado.AtributosItem Is Nothing Then
                    Set oAtribs = m_oGrupoSeleccionado.AtributosItem
                End If
            End If
        
            If Not oAtribs Is Nothing Then
                For Each oatrib In oAtribs
                    If oatrib.Tipo = TipoNumerico Then
                        If m_bGrupoConEscalados Then
                            sdbgPreciosEscalados.Columns("AT_" & CStr(oatrib.idAtribProce)).NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
                        Else
                            sdbgPrecios.Columns("AT_" & CStr(oatrib.idAtribProce)).NumberFormat = FormateoNumericoComp(m_oProcesoSeleccionado.NumDecimalesOfe)
                        End If
                    End If
                Next
                
                Set oAtribs = Nothing
            End If
        End If
    End If
End Sub

Private Sub ConfigurarPicProceInstalacion()
    lblFecIni(0).Visible = True
    lblFecIni(1).Visible = True
    lblFecFin(0).Visible = True
    lblFecFin(1).Visible = True
    lblDest(0).Visible = True
    lblDest(1).Visible = True
    lblDest(2).Visible = True
    lblPago(0).Visible = True
    lblPago(1).Visible = True
    lblPago(2).Visible = True
    lstMaterial.Visible = False
    lblProveProce(0).Visible = True
    lblProveProce(1).Visible = True
    lblProveProce(2).Visible = True
    lblFecIni(0).Top = 4380
    lblFecIni(1).Top = 4380
    lblFecFin(0).Top = 4380
    lblFecFin(1).Top = 4380
    lblDest(0).Top = 4830
    lblDest(1).Top = 4830
    lblDest(2).Top = 4830
    lblPago(0).Top = 5310
    lblPago(1).Top = 5310
    lblPago(2).Top = 5310
    lblProveProce(0).Top = 5790
    lblProveProce(1).Top = 5790
    lblProveProce(2).Top = 5790
        
    If gParametrosInstalacion.gCPSubasta = True Then
        lblFecIniSub(0).Visible = True
        lblFecIniSub(1).Visible = True
        lblHoraIniSub(0).Visible = True
        lblHoraIniSub(1).Visible = True
        lblHoraLimite(0).Visible = True
        lblHoraLimite(1).Visible = True
    Else
        lblFecIniSub(0).Visible = False
        lblFecIniSub(1).Visible = False
        lblHoraIniSub(0).Visible = False
        lblHoraIniSub(1).Visible = False
        lblHoraLimite(0).Visible = False
        lblHoraLimite(1).Visible = False
        lblFecLimit(0).caption = m_sIdiFecFinTradicional
    End If
        
    If gParametrosInstalacion.gCPDestino <> EnProceso And gParametrosInstalacion.gCPFechasSuministro <> EnProceso And gParametrosInstalacion.gCPPago <> EnProceso And gParametrosInstalacion.gCPProveActual <> EnProceso Then
        lblFecIni(0).Visible = False
        lblFecIni(1).Visible = False
        lblFecFin(0).Visible = False
        lblFecFin(1).Visible = False
        lblDest(0).Visible = False
        lblDest(1).Visible = False
        lblDest(2).Visible = False
        lblPago(0).Visible = False
        lblPago(1).Visible = False
        lblPago(2).Visible = False
        lblProveProce(0).Visible = False
        lblProveProce(1).Visible = False
        lblProveProce(2).Visible = False
        Exit Sub
    End If

    If Not gParametrosInstalacion.gCPFechasSuministro = EnProceso Then
        lblFecIni(0).Visible = False
        lblFecIni(1).Visible = False
        lblFecFin(0).Visible = False
        lblFecFin(1).Visible = False
        lblProveProce(0).Top = lblPago(1).Top
        lblProveProce(1).Top = lblPago(1).Top
        lblProveProce(2).Top = lblPago(1).Top
        lblPago(0).Top = lblDest(0).Top
        lblPago(1).Top = lblDest(0).Top
        lblPago(2).Top = lblDest(0).Top
        lblDest(0).Top = lblFecIni(0).Top
        lblDest(1).Top = lblFecIni(0).Top
        lblDest(2).Top = lblFecIni(0).Top
        If Not gParametrosInstalacion.gCPDestino = EnProceso Then
            lblDest(0).Visible = False
            lblDest(1).Visible = False
            lblDest(2).Visible = False
            lblProveProce(0).Top = lblPago(1).Top
            lblProveProce(1).Top = lblPago(1).Top
            lblProveProce(2).Top = lblPago(1).Top
            lblPago(0).Top = lblDest(0).Top
            lblPago(1).Top = lblDest(0).Top
            lblPago(2).Top = lblDest(0).Top
            If Not gParametrosInstalacion.gCPPago = EnProceso Then
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(0).Top = lblPago(1).Top
                    lblProveProce(1).Top = lblPago(1).Top
                    lblProveProce(2).Top = lblPago(1).Top
                End If
            Else
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                End If
            End If
        Else
            lblDest(0).Visible = True
            lblDest(1).Visible = True
            lblDest(2).Visible = True
            If Not gParametrosInstalacion.gCPPago = EnProceso Then
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(0).Top = lblPago(1).Top
                    lblProveProce(1).Top = lblPago(1).Top
                    lblProveProce(2).Top = lblPago(1).Top

                End If
            Else
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                End If
            End If
            
        End If
    Else
        lblFecIni(0).Visible = True
        lblFecIni(1).Visible = True
        lblFecFin(0).Visible = True
        lblFecFin(1).Visible = True

        If Not gParametrosInstalacion.gCPDestino = EnProceso Then
            lblDest(0).Visible = False
            lblDest(1).Visible = False
            lblDest(2).Visible = False
            lblProveProce(0).Top = lblPago(1).Top
            lblProveProce(1).Top = lblPago(1).Top
            lblProveProce(2).Top = lblPago(1).Top
            lblPago(0).Top = lblDest(0).Top
            lblPago(1).Top = lblDest(0).Top
            lblPago(2).Top = lblDest(0).Top
            If Not gParametrosInstalacion.gCPPago = EnProceso Then
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(0).Top = lblPago(1).Top
                    lblProveProce(1).Top = lblPago(1).Top
                    lblProveProce(2).Top = lblPago(1).Top
                End If
            Else
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                End If
            End If
        Else
            lblDest(0).Visible = True
            lblDest(1).Visible = True
            lblDest(2).Visible = True
            If Not gParametrosInstalacion.gCPPago = EnProceso Then
                lblPago(0).Visible = False
                lblPago(1).Visible = False
                lblPago(2).Visible = False
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                    lblProveProce(0).Top = lblPago(1).Top
                    lblProveProce(1).Top = lblPago(1).Top
                    lblProveProce(2).Top = lblPago(1).Top
                End If
            Else
                lblPago(0).Visible = True
                lblPago(1).Visible = True
                lblPago(2).Visible = True
                If Not gParametrosInstalacion.gCPProveActual = EnProceso Then
                    lblProveProce(0).Visible = False
                    lblProveProce(1).Visible = False
                    lblProveProce(2).Visible = False
                Else
                    lblProveProce(0).Visible = True
                    lblProveProce(1).Visible = True
                    lblProveProce(2).Visible = True
                End If
            End If
            
        End If
    End If
End Sub

Public Sub VerDetalleResponsable()
Dim oPer As CPersona
Dim teserror As TipoErrorSummit

    'Muestra el detalle del responsable del proceso
    If m_oProcesoSeleccionado.responsable Is Nothing Then
        oMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    If m_oProcesoSeleccionado.responsable.Cod = "" Then
        oMensajes.ProcesoSinResponsable
        Exit Sub
    End If
    
    Set oPer = oFSGSRaiz.Generar_CPersona
    oPer.Cod = m_oProcesoSeleccionado.responsable.Cod
            
    teserror = oPer.CargarTodosLosDatos
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Exit Sub
    End If
        
    Set frmESTRORGPersona.frmOrigen = Me
    frmESTRORGPersona.txtCod = oPer.Cod
    frmESTRORGPersona.txtNom = NullToStr(oPer.nombre)
    frmESTRORGPersona.txtApel = oPer.Apellidos
    frmESTRORGPersona.txtCargo = NullToStr(oPer.Cargo)
    frmESTRORGPersona.txtFax = NullToStr(oPer.Fax)
    frmESTRORGPersona.txtMail = NullToStr(oPer.mail)
    frmESTRORGPersona.txtTfno = NullToStr(oPer.Tfno)
    frmESTRORGPersona.txtTfno2 = NullToStr(oPer.Tfno2)
    frmESTRORGPersona.sdbcEquipo.Text = NullToStr(oPer.codEqp)
    If NullToStr(oPer.codEqp) <> "" Then frmESTRORGPersona.chkFunCompra.Value = vbChecked
    
    Screen.MousePointer = vbNormal
    frmESTRORGPersona.Show vbModal
    Set oPer = Nothing
End Sub

Public Sub SustituirResponsable()
    'Muestra el formulario para seleccionar el comprador responsable del proceso.
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If Not BloquearProceso Then Exit Sub
    m_udtOrigBloqueo = responsable
    
    Set frmSELPROVEResp.g_oOrigen = Me
    Set frmSELPROVEResp.g_oProcesoSeleccionado = m_oProcesoSeleccionado
    frmSELPROVEResp.g_udtAccion = AccionesSummit.ACCModRespOfeRec
    
    Screen.MousePointer = vbNormal
    frmSELPROVEResp.Show vbModal
    
    DesbloquearProceso
End Sub

Public Sub ResponsableSustituido(ByVal oPer As CPersona)
    If m_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    If m_oProcesoSeleccionado.responsable Is Nothing Then Set m_oProcesoSeleccionado.responsable = oFSGSRaiz.generar_CComprador
        
    m_oProcesoSeleccionado.responsable.Cod = oPer.Cod
    m_oProcesoSeleccionado.responsable.codEqp = oPer.codEqp
    m_oProcesoSeleccionado.responsable.nombre = NullToStr(oPer.nombre)
    m_oProcesoSeleccionado.responsable.Apel = oPer.Apellidos
    m_oProcesoSeleccionado.responsable.Cargo = NullToStr(oPer.Cargo)
    m_oProcesoSeleccionado.responsable.Fax = NullToStr(oPer.Fax)
    m_oProcesoSeleccionado.responsable.mail = NullToStr(oPer.mail)
    m_oProcesoSeleccionado.responsable.Tfno = NullToStr(oPer.Tfno)
    m_oProcesoSeleccionado.responsable.Tfno2 = NullToStr(oPer.Tfno2)
        
    Set oPer = Nothing
    
    If m_oProcesoSeleccionado.responsable.nombre <> "" Then
        cmdResponsable.ToolTipText = m_oProcesoSeleccionado.responsable.nombre & " " & m_oProcesoSeleccionado.responsable.Apel
    Else
        cmdResponsable.ToolTipText = m_oProcesoSeleccionado.responsable.Apel
    End If
End Sub

Private Function PermisoModificarOferta() As Boolean
    Dim bModificar As Boolean
    
    'Comprueba si tiene permisos para modificar las ofertas:
    
    bModificar = True
    
    If m_bModifOfeDeProv = True Or m_bModifOfeDeUsu = True Then
    
        'Permitir modificar las ofertas introducidas por los proveedores:
        If m_bModifOfeDeProv = True Then
            If g_oOfertaSeleccionada.portal = False And Not m_bModifOfeDeUsu Then bModificar = False
        End If
    
        'Permitir modificar las ofertas introducidas por los usuarios:
        If bModificar = True And m_bModifOfeDeUsu = True Then
            If g_oOfertaSeleccionada.portal = True And Not m_bModifOfeDeProv Then
                bModificar = False
            Else
                If m_bRestModifOfeUsu = True And g_oOfertaSeleccionada.portal = False Then 'Restringir la modificaci�n a las ofertas introducidas por el usuario. Esta restricci�n s�lo afectar� a las ofertas introducidas desde el GS.
                    If Not g_oOfertaSeleccionada.Usuario Is Nothing Then
                        If g_oOfertaSeleccionada.Usuario.Cod <> oUsuarioSummit.Cod Then bModificar = False
                    Else
                        bModificar = False
                    End If
                End If
            End If
            
            'Restringir la modificaci�n a las ofertas de los proveedores asignados al usuario
            If bModificar = True And m_bRestModifOfeProvUsu = True Then
                If Not m_bOfeAsigComp Then
                    If Not m_oProveedoresAsigUsu Is Nothing Then
                        If m_oProveedoresAsigUsu.Item(CStr(m_oProveSeleccionado.Cod)) Is Nothing Then bModificar = False
                    Else
                        bModificar = False
                    End If
                End If
            End If
        End If
    Else
        bModificar = False
    End If
    
    PermisoModificarOferta = bModificar
End Function

Private Function PermisoEliminarOferta() As Boolean
    Dim bEliminar As Boolean
    
    'Comprueba si tiene permisos para eliminar las ofertas
    bEliminar = True
    
    If m_bElimOfeDeProv = True Or m_bElimOfeDeUsu = True Then
    
        'Permitir eliminar ofertas introducidas por los proveedores:
        If m_bElimOfeDeProv = True Then
            If g_oOfertaSeleccionada.portal = False And Not m_bElimOfeDeUsu Then bEliminar = False
        End If
                
        'Permitir eliminar ofertas introducidas por los usuarios de GS:
        If bEliminar = True And m_bElimOfeDeUsu = True Then
            If g_oOfertaSeleccionada.portal = True And Not m_bElimOfeDeProv Then
                bEliminar = False
            Else
                If m_bRestElimOfeUsu = True Then 'Restringir la eliminaci�n a las ofertas introducidas por el usuario
                    If Not g_oOfertaSeleccionada.Usuario Is Nothing Then
                        If g_oOfertaSeleccionada.Usuario.Cod <> oUsuarioSummit.Cod Then bEliminar = False
                    Else
                        bEliminar = False
                    End If
                End If
            End If
            
            'Restringir la eliminaci�n a las ofertas de los proveedores asignados al usuario:
            If bEliminar = True And m_bRestElimOfeProvUsu = True Then
                If Not m_bOfeAsigComp Then
                    If Not m_oProveedoresAsigUsu Is Nothing Then
                        If m_oProveedoresAsigUsu.Item(CStr(m_oProveSeleccionado.Cod)) Is Nothing Then bEliminar = False
                    Else
                        bEliminar = False
                    End If
                End If
            End If
        End If
    Else
        bEliminar = False
    End If
    
    PermisoEliminarOferta = bEliminar
End Function

Private Function ComprobarExistenAdjudicaciones() As Boolean
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim oatrib As CAtributo
Dim bComprobar As Boolean
Dim sCod As String
Dim m_vEnPedidos() As Variant

    'Comprueba si tiene un escenario de adjudicaciones guardado:
    If m_oProcesoSeleccionado.Estado >= ConObjetivosSinNotificarYPreadjudicado And m_oProcesoSeleccionado.Estado < conadjudicaciones Then
        If Not g_oLineaEnEdicion Is Nothing Then
            bComprobar = False
            If NullToStr(g_oLineaEnEdicion.Precio) <> sdbgPrecios.Columns("PREC1").Value Then bComprobar = True
    
            If NullToStr(g_oLineaEnEdicion.Precio2) <> sdbgPrecios.Columns("PREC2").Value Then bComprobar = True
    
            If NullToStr(g_oLineaEnEdicion.Precio3) <> sdbgPrecios.Columns("PREC3").Value Then bComprobar = True
           
            Select Case sdbgPrecios.Columns("USAR").Text
                Case m_sIdiUsarPrec(1)
                    If g_oLineaEnEdicion.Usar <> 1 Then bComprobar = True
                Case m_sIdiUsarPrec(2)
                    If g_oLineaEnEdicion.Usar <> 2 Then bComprobar = True
                Case m_sIdiUsarPrec(3)
                    If g_oLineaEnEdicion.Usar <> 3 Then bComprobar = True
            End Select
        
            If bComprobar = False Then
                If m_oGrupoSeleccionado Is Nothing Then
                    bComprobar = True
                Else
                    For Each oatrib In m_oGrupoSeleccionado.AtributosItem
                        If oatrib.Tipo = TipoNumerico And oatrib.UsarPrec = 1 Then
                            sCod = CStr(sdbgPrecios.Columns("ITEM").Value) & "$" & CStr(oatrib.idAtribProce)
                            If sdbgPrecios.Columns("AT_" & CStr(oatrib.idAtribProce)).Value <> NullToStr(g_oLineaEnEdicion.AtribOfertados.Item(sCod).valorNum) Then
                                bComprobar = True
                                Exit For
                            End If
                        End If
                    Next
                End If
            End If
            
            'comprueba si el proceso tiene adjudicaciones:
            If bComprobar = True Then
                If m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then
                    teserror = g_oLineaEnEdicion.OfertaModificable
                    If teserror.NumError <> TESnoerror Then
                        ReDim Preserve m_vEnPedidos(3, 0)
                        m_vEnPedidos(0, 0) = Trim(sdbgPrecios.Columns("COD").Value & "  " & sdbgPrecios.Columns("DEN").Value)
                        m_vEnPedidos(1, 0) = teserror.Arg2
                        m_vEnPedidos(2, 0) = Trim(g_oOfertaSeleccionada.Prove & "  " & m_oProveedoresProceso.Item(g_oOfertaSeleccionada.Prove).Den)
                        m_vEnPedidos(3, 0) = teserror.Arg1
                        oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 4
                        ComprobarExistenAdjudicaciones = False
                        Exit Function
                    End If
                Else
                    teserror = g_oOfertaSeleccionada.OfertaModificable(True)
                    If teserror.NumError = TESOfertaAdjudicada Then
                        irespuesta = oMensajes.PreguntaHayAdjudicacionesGuardadas
                        If irespuesta = vbNo Then
                            ComprobarExistenAdjudicaciones = False
                            Exit Function
                        Else
                            g_oLineaEnEdicion.EliminarAdj = True
                            ComprobarExistenAdjudicaciones = True
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    If Not g_oLineaEnEdicion Is Nothing Then g_oLineaEnEdicion.EliminarAdj = False
    ComprobarExistenAdjudicaciones = True
End Function


Private Function ComprobarExistenAdjudicacionesEscalados() As Boolean
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim oatrib As CAtributo
Dim bComprobar As Boolean
Dim sCod As String
Dim sCodAdj As String
Dim m_vEnPedidos() As Variant
Dim iCountGroup As Integer
Dim iTotalCountGroup As Integer
Dim ogroup As SSDataWidgets_B.Group
Dim sEscalado As String
Dim i As Integer
Dim oEscalado As CEscalado

    'Comprueba si tiene un escenario de adjudicaciones guardado:
    If m_oProcesoSeleccionado.Estado >= ConObjetivosSinNotificarYPreadjudicado And m_oProcesoSeleccionado.Estado < conadjudicaciones Then
         'Carga las adjudicaciones
        If (m_bOfeAsigComp) Then
            m_oProcesoSeleccionado.CargarAdjudicacionesGrupos , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , m_bGrupoConEscalados
        Else
            If (m_bOfeAsigEqp) Then
                m_oProcesoSeleccionado.CargarAdjudicacionesGrupos , basOptimizacion.gCodEqpUsuario, , , m_bGrupoConEscalados
            Else
                m_oProcesoSeleccionado.CargarAdjudicacionesGrupos , , , , m_bGrupoConEscalados
            End If
        End If
        If Not g_oLineaEnEdicion Is Nothing Then
            bComprobar = False
            iCountGroup = 0
            iTotalCountGroup = 0
            ' JVS Comprueba el precio entre los escalados
            If m_oGrupoSeleccionado Is Nothing Then
                bComprobar = True
            Else
                For i = 1 To sdbgPreciosEscalados.Groups.Count - 1
                    If sdbgPreciosEscalados.Groups(i).TagVariant <> "ATRIBSNOCOSTESDESC" And sdbgPreciosEscalados.Groups(i).TagVariant <> "ADJUNTOS" Then
                        Set ogroup = sdbgPreciosEscalados.Groups(i)
                        sEscalado = ogroup.TagVariant
                        sCodAdj = KeyEscalado(m_oProveSeleccionado.Cod, sdbgPreciosEscalados.Groups(0).Columns.Item("ITEM").Value, sEscalado)
                        If Not m_oGrupoSeleccionado.Adjudicaciones.Item(sCodAdj) Is Nothing Then 'Comprueba que es el precio que corresponde al escalado que est� adjudicado
                            If NullToStr(g_oLineaEnEdicion.Escalados.Item(sEscalado).Precio) <> ogroup.Columns(1).Value Then
                                bComprobar = True
                                Exit For
                            End If
                        End If
                    End If
                Next
            End If
            
            If bComprobar = False Then
                If m_oGrupoSeleccionado Is Nothing Then
                    bComprobar = True
                Else
                    For Each oatrib In m_oGrupoSeleccionado.AtributosItem
                        If oatrib.Tipo = TipoNumerico And oatrib.UsarPrec = 1 Then
                            For Each oEscalado In m_oGrupoSeleccionado.Escalados
                                sCod = CStr(sdbgPreciosEscalados.Columns("ITEM").Value) & "$" & CStr(oatrib.idAtribProce)
                                sCodAdj = KeyEscalado(m_oProveSeleccionado.Cod, sdbgPreciosEscalados.Groups(0).Columns.Item("ITEM").Value, CStr(oEscalado.Id))
                                If Not m_oGrupoSeleccionado.Adjudicaciones.Item(sCodAdj) Is Nothing Then 'Comprueba que es el valor del atributo que corresponde al escalado que est� adjudicado
                                    If sdbgPreciosEscalados.Columns("ATE_" & CStr(oEscalado.Id) & "_" & CStr(oatrib.idAtribProce)).Value <> NullToStr(g_oLineaEnEdicion.AtribOfertados.Item(sCod).Escalados.Item(CStr(oEscalado.Id)).valorNum) Then
                                        bComprobar = True
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                    Next
                End If
            End If
            
            'comprueba si el proceso tiene adjudicaciones:
            If bComprobar = True Then
                If m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then
                    teserror = g_oLineaEnEdicion.OfertaModificable
                    If teserror.NumError <> TESnoerror Then
                        ReDim Preserve m_vEnPedidos(3, 0)
                        m_vEnPedidos(0, 0) = Trim(sdbgPreciosEscalados.Columns("COD").Value & "  " & sdbgPreciosEscalados.Columns("DEN").Value)
                        m_vEnPedidos(1, 0) = teserror.Arg2
                        m_vEnPedidos(2, 0) = Trim(g_oOfertaSeleccionada.Prove & "  " & m_oProveedoresProceso.Item(g_oOfertaSeleccionada.Prove).Den)
                        m_vEnPedidos(3, 0) = teserror.Arg1
                        oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 4
                        ComprobarExistenAdjudicacionesEscalados = False
                        Exit Function
                    End If
                Else
                    teserror = g_oOfertaSeleccionada.OfertaModificable(True)
                    If teserror.NumError = TESOfertaAdjudicada Then
                        irespuesta = oMensajes.PreguntaHayAdjudicacionesGuardadas
                        If irespuesta = vbNo Then
                            ComprobarExistenAdjudicacionesEscalados = False
                            Exit Function
                        Else
                            g_oLineaEnEdicion.EliminarAdj = True
                            ComprobarExistenAdjudicacionesEscalados = True
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    If Not g_oLineaEnEdicion Is Nothing Then g_oLineaEnEdicion.EliminarAdj = False
    ComprobarExistenAdjudicacionesEscalados = True
End Function

'CARGAR EL GMN1 DE MANERA AUTOM�TICA AL TRABAJAR EN MODO PYME
Private Sub CargarGMN1Automaticamente()
    Dim oGMN1s As CGruposMatNivel1
    Dim m_oIMAsig As IMaterialAsignado
    Set m_oIMAsig = oUsuarioSummit.comprador
    Set oGMN1s = m_oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
    If Not oGMN1s.Count = 0 Then
        bRespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4Cod.Columns(0).Text = sdbcGMN1_4Cod.Text
        bRespetarCombo = False
        GMN1Seleccionado
    End If
    Set oGMN1s = Nothing
End Sub

''' <summary>
''' Carga de la estructura de escalados
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: frmOFERec.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 19/10/2011</revision>
Private Sub CargarEscalados()
    Set m_oEscalados = oFSGSRaiz.Generar_CEscalados
    Set m_oEscalado = oFSGSRaiz.Generar_CEscalado
    
    'Inicializa la estructura
    m_oEscalados.clear
End Sub

''' <summary>
''' Muestra el progreso del total del guardado
''' </summary>
''' <param name="bLleva">array q indica q percentil ya ha sumado progreso</param>
''' <remarks>Llamada desde: cmdA�adirAdjun_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub ProgresoBarra(ByRef bLleva() As Boolean)
    Dim inum As Integer
    Dim bFin As Boolean
    Dim i As Integer
        
    inum = 10
    i = 0
    
    While Not bFin
        If (frmESPERA.ProgressBar2.Value >= inum) Then
            If Not bLleva(i) Then
                If frmESPERA.ProgressBar1.Value + 0.1 > frmESPERA.ProgressBar1.Max Then
                    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Max
                Else
                    frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 0.1
                End If
                bLleva(i) = True
            End If
        Else
            bFin = True
        End If
        
        inum = inum + 10
        i = i + 1
    Wend
End Sub
