VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmESPERAList 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Espere, por favor ++++"
   ClientHeight    =   2745
   ClientLeft      =   1155
   ClientTop       =   3240
   ClientWidth     =   5850
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2745
   ScaleWidth      =   5850
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Caption         =   "Elemento actual+++"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   60
      TabIndex        =   3
      Top             =   555
      Width           =   5730
      Begin MSComctlLib.ProgressBar ProgressBar2 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   540
         Width           =   5475
         _ExtentX        =   9657
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   1
      End
      Begin VB.Label lblContacto 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   120
         TabIndex        =   5
         Top             =   270
         Width           =   5415
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Total del proceso++++"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   60
      TabIndex        =   0
      Top             =   1560
      Width           =   5730
      Begin MSComctlLib.ProgressBar ProgressBar1 
         Height          =   315
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   5460
         _ExtentX        =   9631
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   1
         Max             =   12
      End
      Begin VB.Label lblDetalle 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   300
         Width           =   5415
      End
   End
   Begin VB.Label lblGeneral 
      Alignment       =   2  'Center
      Caption         =   "Generando peticiones de oferta...+++"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   45
      TabIndex        =   6
      Top             =   120
      Width           =   5730
   End
End
Attribute VB_Name = "frmESPERAList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESPERA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Frame2.Caption = Ador(0).Value
        Ador.MoveNext
        Frame3.Caption = Ador(0).Value
        Ador.MoveNext
        lblGeneral.Caption = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
            
End Sub


Private Sub Form_Load()
    CargarRecursos
End Sub

