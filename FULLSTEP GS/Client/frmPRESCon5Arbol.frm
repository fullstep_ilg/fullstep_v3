VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPRESCon5Arbol 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4650
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6045
   Icon            =   "frmPRESCon5Arbol.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4650
   ScaleWidth      =   6045
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   1200
      ScaleHeight     =   420
      ScaleWidth      =   3225
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   4080
      Width           =   3225
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   510
         TabIndex        =   14
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1650
         TabIndex        =   15
         Top             =   60
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   3795
      Left            =   1395
      ScaleHeight     =   3795
      ScaleWidth      =   4530
      TabIndex        =   0
      Top             =   0
      Width           =   4530
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         TabIndex        =   1
         Top             =   210
         Width           =   1530
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgDen 
         Height          =   495
         Left            =   60
         TabIndex        =   9
         Top             =   600
         Width           =   4365
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2170
         Columns(0).Caption=   "IDI"
         Columns(0).Name =   "IDI"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   14671839
         Columns(1).Width=   5503
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "COD_IDI"
         Columns(2).Name =   "COD_IDI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7708
         _ExtentY        =   873
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgNomNivel1 
         Height          =   495
         Left            =   60
         TabIndex        =   10
         Top             =   1200
         Width           =   4365
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2170
         Columns(0).Caption=   "IDI"
         Columns(0).Name =   "IDI"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   14671839
         Columns(1).Width=   5503
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "COD_IDI"
         Columns(2).Name =   "COD_IDI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7708
         _ExtentY        =   873
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgNomNivel2 
         Height          =   495
         Left            =   60
         TabIndex        =   11
         Top             =   1800
         Width           =   4365
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2170
         Columns(0).Caption=   "IDI"
         Columns(0).Name =   "IDI"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   14671839
         Columns(1).Width=   5503
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "COD_IDI"
         Columns(2).Name =   "COD_IDI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7708
         _ExtentY        =   873
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgNomNivel3 
         Height          =   495
         Left            =   60
         TabIndex        =   12
         Top             =   2400
         Width           =   4365
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2170
         Columns(0).Caption=   "IDI"
         Columns(0).Name =   "IDI"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   14671839
         Columns(1).Width=   5503
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "COD_IDI"
         Columns(2).Name =   "COD_IDI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7708
         _ExtentY        =   873
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgNomNivel4 
         Height          =   495
         Left            =   60
         TabIndex        =   13
         Top             =   3000
         Width           =   4365
         ScrollBars      =   0
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2170
         Columns(0).Caption=   "IDI"
         Columns(0).Name =   "IDI"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   14671839
         Columns(1).Width=   5503
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "COD_IDI"
         Columns(2).Name =   "COD_IDI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7708
         _ExtentY        =   873
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Label lblNomNivel3 
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre nivel 3:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   2445
      Width           =   1350
   End
   Begin VB.Label lblNomNivel4 
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre nivel 4:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   2985
      Width           =   1350
   End
   Begin VB.Label lblNomNivel2 
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre nivel 2:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1785
      Width           =   1350
   End
   Begin VB.Label lblDenominacion 
      BackStyle       =   0  'Transparent
      Caption         =   "Denominaci�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   705
      Width           =   1350
   End
   Begin VB.Label lblCodigo 
      BackStyle       =   0  'Transparent
      Caption         =   "C�digo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   285
      Width           =   1350
   End
   Begin VB.Label lblNomNivel1 
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre nivel 1:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   1245
      Width           =   1350
   End
End
Attribute VB_Name = "frmPRESCon5Arbol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public m_sAccion As String
Public m_sCod As String

'Variables privadas
Private m_sCodigo As String
Private m_sDenominacion As String
Private m_sNomNivel1 As String
Private m_oPres5 As cPresConcep5Nivel0

Private m_sAnyadir As String
Private m_sModificar As String
Private m_sTitulo As String

Private oIBaseDatos As IBaseDatos
Private g_oIdiomas As CIdiomas

''' <summary>
''' Guarda un nuevo arbol o modifica uno existente
''' </summary>
''' <remarks>Llamada desde Click del boton Aceptar; Tiempo m�ximo < 1 seg.</remarks>

Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim i As Integer
    Dim vbm As Variant
    
    If Trim(txtCod.Text) = "" Then
        oMensajes.NoValido m_sCodigo
        If Me.Visible Then txtCod.SetFocus
        Exit Sub
    End If
    
    sdbgDen.MoveFirst
    For i = 0 To sdbgDen.Rows - 1
        vbm = sdbgDen.AddItemBookmark(i)
            If sdbgDen.Columns("DEN").CellValue(vbm) = "" Then
                oMensajes.NoValido lblDenominacion.caption & " " & sdbgDen.Columns("IDI").CellValue(vbm)
                Exit Sub
            End If
    Next i
    
    Dim bTodosRellenos As Boolean
    Dim bTodosVacios  As Boolean
    Dim sMensaje As String
    
    sMensaje = ""
    
    bTodosRellenos = True
    bTodosVacios = True
    sdbgNomNivel1.MoveFirst
    For i = 0 To sdbgNomNivel1.Rows - 1
        vbm = sdbgNomNivel1.AddItemBookmark(i)
            If sdbgNomNivel1.Columns("DEN").CellValue(vbm) = "" Then
                If sMensaje = "" Then
                    sMensaje = lblNomNivel1.caption & " " & sdbgNomNivel1.Columns("IDI").CellValue(vbm)
                End If
                bTodosRellenos = False
            Else
                bTodosVacios = False
            End If
    Next i
    
    If Not (bTodosRellenos Or bTodosVacios) Then
        oMensajes.NoValido sMensaje
        Exit Sub
    End If
    
    
    sMensaje = ""
    bTodosRellenos = True
    bTodosVacios = True
    sdbgNomNivel2.MoveFirst
    For i = 0 To sdbgNomNivel2.Rows - 1
        vbm = sdbgNomNivel2.AddItemBookmark(i)
            If sdbgNomNivel2.Columns("DEN").CellValue(vbm) = "" Then
                If sMensaje = "" Then
                    sMensaje = lblNomNivel2.caption & " " & sdbgNomNivel2.Columns("IDI").CellValue(vbm)
                End If
                bTodosRellenos = False
            Else
                bTodosVacios = False
            End If
    Next i
    
    If Not (bTodosRellenos Or bTodosVacios) Then
        oMensajes.NoValido sMensaje
        Exit Sub
    End If
        
    sMensaje = ""
    bTodosRellenos = True
    bTodosVacios = True
    sdbgNomNivel3.MoveFirst
    For i = 0 To sdbgNomNivel3.Rows - 1
        vbm = sdbgNomNivel3.AddItemBookmark(i)
            If sdbgNomNivel3.Columns("DEN").CellValue(vbm) = "" Then
                If sMensaje = "" Then
                    sMensaje = lblNomNivel3.caption & " " & sdbgNomNivel3.Columns("IDI").CellValue(vbm)
                End If
                bTodosRellenos = False
            Else
                bTodosVacios = False
            End If
    Next i
    
    If Not (bTodosRellenos Or bTodosVacios) Then
        oMensajes.NoValido sMensaje
        Exit Sub
    End If
    
    
    sMensaje = ""
    bTodosRellenos = True
    bTodosVacios = True
    sdbgNomNivel4.MoveFirst
    For i = 0 To sdbgNomNivel4.Rows - 1
        vbm = sdbgNomNivel4.AddItemBookmark(i)
            If sdbgNomNivel4.Columns("DEN").CellValue(vbm) = "" Then
                If sMensaje = "" Then
                    sMensaje = lblNomNivel4.caption & " " & sdbgNomNivel4.Columns("IDI").CellValue(vbm)
                End If
                bTodosRellenos = False
            Else
                bTodosVacios = False
            End If
    Next i
    
    If Not (bTodosRellenos Or bTodosVacios) Then
        oMensajes.NoValido sMensaje
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPres5 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    
    m_oPres5.Cod = Trim(txtCod.Text)

    Dim oIdioma As CIdioma
    Dim oDenominaciones As CMultiidiomas
    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        For i = 0 To sdbgDen.Rows - 1
            vbm = sdbgDen.AddItemBookmark(i)
            If sdbgDen.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                If basPublic.gParametrosInstalacion.gIdioma = oIdioma.Cod Then
                    m_oPres5.Den = Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                End If
                    oDenominaciones.Add oIdioma.Cod, Trim(sdbgDen.Columns("DEN").CellValue(vbm))
                Exit For
                
            End If
        Next i
    Next
    
    Set m_oPres5.Denominaciones = oDenominaciones
    Set oDenominaciones = Nothing
    
    
    Dim oNomNivel1 As CMultiidiomas
    Set oNomNivel1 = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        For i = 0 To sdbgNomNivel1.Rows - 1
            vbm = sdbgNomNivel1.AddItemBookmark(i)
            If sdbgNomNivel1.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                oNomNivel1.Add oIdioma.Cod, Trim(sdbgNomNivel1.Columns("DEN").CellValue(vbm))
                Exit For
            End If
        Next i
    Next
    
    Set m_oPres5.NomNivel1 = oNomNivel1
    Set oNomNivel1 = Nothing

    Dim oNomNivel2 As CMultiidiomas
    Set oNomNivel2 = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        For i = 0 To sdbgNomNivel2.Rows - 1
            vbm = sdbgNomNivel2.AddItemBookmark(i)
            If sdbgNomNivel2.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                oNomNivel2.Add oIdioma.Cod, Trim(sdbgNomNivel2.Columns("DEN").CellValue(vbm))
                Exit For
            End If
        Next i
    Next
    
    Set m_oPres5.NomNivel2 = oNomNivel2
    Set oNomNivel2 = Nothing

    Dim oNomNivel3 As CMultiidiomas
    Set oNomNivel3 = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        For i = 0 To sdbgNomNivel3.Rows - 1
            vbm = sdbgNomNivel3.AddItemBookmark(i)
            If sdbgNomNivel3.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                oNomNivel3.Add oIdioma.Cod, Trim(sdbgNomNivel3.Columns("DEN").CellValue(vbm))
                Exit For
            End If
        Next i
    Next
    
    Set m_oPres5.NomNivel3 = oNomNivel3
    Set oNomNivel3 = Nothing
    
    
    Dim oNomNivel4 As CMultiidiomas
    Set oNomNivel4 = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        For i = 0 To sdbgNomNivel4.Rows - 1
            vbm = sdbgNomNivel4.AddItemBookmark(i)
            If sdbgNomNivel4.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                oNomNivel4.Add oIdioma.Cod, Trim(sdbgNomNivel4.Columns("DEN").CellValue(vbm))
                Exit For
            End If
        Next i
    Next
    
    Set m_oPres5.NomNivel4 = oNomNivel4
    Set oNomNivel4 = Nothing
    
    Set oIBaseDatos = m_oPres5
                        
    Select Case m_sAccion
        Case "Modificar"
            udtTeserror = oIBaseDatos.FinalizarEdicionModificando
            frmCONFGEN.sdbcPartidaPres.Text = m_oPres5.Den
            
        Case "Anyadir"
            udtTeserror = oIBaseDatos.AnyadirABaseDatos
            
    End Select
    
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        Exit Sub
    End If
    
    
    Set oIBaseDatos = Nothing

    
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    m_sAccion = ""
    Unload Me
End Sub

''' <summary>
''' Evento que se produce al activarse el formulario
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo < 1 seg.</remarks>
Private Sub Form_Activate()
    If m_sAccion = "Modificar" Then
        If Me.Visible Then sdbgDen.SetFocus
    Else
        If Me.Visible Then txtCod.SetFocus
    End If
End Sub


''' <summary>
''' Carga del formulario
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo < 1 seg.</remarks>

Private Sub Form_Load()
    CargarRecursos
    PonerFieldSeparator Me
    If m_sAccion = "Modificar" Then
        'Cargar Partida Contable
        Me.caption = m_sModificar & " " & m_sTitulo
        Me.Width = Me.Width + 100
        Set m_oPres5 = oFSGSRaiz.Generar_CPresConcep5Nivel0
        m_oPres5.Cod = m_sCod

        Set oIBaseDatos = m_oPres5
        oIBaseDatos.IniciarEdicion
        
        txtCod.Text = m_oPres5.Cod
                
        Set oIBaseDatos = Nothing
        txtCod.Enabled = False
    Else
        Me.caption = m_sAnyadir & " " & m_sTitulo
    End If
    txtCod.MaxLength = gLongitudesDeCodigos.giLongCodPres5Niv0

    CargarGridsIdiomas
    
    
    sdbgDen.Height = (sdbgDen.Rows) * sdbgDen.RowHeight + 10
    sdbgNomNivel1.Top = sdbgDen.Top + sdbgDen.Height + 100
    sdbgNomNivel1.Height = (sdbgNomNivel1.Rows) * sdbgNomNivel1.RowHeight + 20
    sdbgNomNivel2.Top = sdbgNomNivel1.Top + sdbgNomNivel1.Height + 100
    sdbgNomNivel2.Height = (sdbgNomNivel2.Rows) * sdbgNomNivel2.RowHeight + 20
    sdbgNomNivel3.Top = sdbgNomNivel2.Top + sdbgNomNivel2.Height + 100
    sdbgNomNivel3.Height = (sdbgNomNivel3.Rows) * sdbgNomNivel3.RowHeight + 20
    sdbgNomNivel4.Top = sdbgNomNivel3.Top + sdbgNomNivel3.Height + 100
    sdbgNomNivel4.Height = (sdbgNomNivel4.Rows) * sdbgNomNivel4.RowHeight + 20
    
    picDatos.Height = sdbgNomNivel4.Height + sdbgNomNivel3.Height + sdbgNomNivel2.Height + sdbgNomNivel1.Height + sdbgDen.Height + 1000
    picEdit.Top = picDatos.Top + picDatos.Height + 100
    Me.Height = picDatos.Height + picEdit.Height + 700
    
    lblDenominacion.Top = sdbgDen.Top + 100
    lblNomNivel1.Top = sdbgNomNivel1.Top + 100
    lblNomNivel2.Top = sdbgNomNivel2.Top + 100
    lblNomNivel3.Top = sdbgNomNivel3.Top + 100
    lblNomNivel4.Top = sdbgNomNivel4.Top + 100
    
End Sub

Private Sub CargarRecursos()
Dim rs As New ADODB.Recordset

    Set rs = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESCon5Arbol, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not rs Is Nothing Then
        m_sTitulo = rs(0).Value '1 �rbol de Partidas de aprovisionamiento
        rs.MoveNext
        lblCodigo.caption = rs(0).Value & ":" '2 C�digo
        m_sCodigo = rs(0).Value
        rs.MoveNext
        
        lblDenominacion.caption = rs(0).Value & ":" '3 Denominaci�n
        m_sDenominacion = rs(0).Value
        rs.MoveNext
        lblNomNivel1.caption = rs(0).Value & ":" '4 Nombre de nivel 1
        m_sNomNivel1 = rs(0).Value
        rs.MoveNext
        lblNomNivel2.caption = rs(0).Value & ":" '5 Nombre de nivel 2
        rs.MoveNext
        lblNomNivel3.caption = rs(0).Value & ":" '6 Nombre de nivel 3
        rs.MoveNext
        lblNomNivel4.caption = rs(0).Value & ":" '7 Nombre de nivel 4
        rs.MoveNext
        cmdAceptar.caption = rs(0).Value '8 Aceptar
        rs.MoveNext
        cmdCancelar.caption = rs(0).Value ' 9 Cancenlar
        rs.MoveNext
        
        m_sAnyadir = rs(0).Value & ":" '10 A�adir
        rs.MoveNext
        m_sModificar = rs(0).Value & ":" '11 Modificar
        
        
        rs.Close
    End If
    Set g_oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
End Sub


''' <summary>
''' Carga de las grids de denominacion y los distintos nombre de niveles.
''' </summary>
''' <remarks>Llamada desde FORM_Load; Tiempo m�ximo < 1 seg.</remarks>

Private Sub CargarGridsIdiomas()
Dim oIdioma As CIdioma
Dim sCadena As String

    Select Case m_sAccion
        Case "Anyadir":
    
            'Carga las denominaciones del grupo de datos generales en todos los idiomas:
            For Each oIdioma In g_oIdiomas
                sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
                sdbgNomNivel1.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
                sdbgNomNivel2.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
                sdbgNomNivel3.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
                sdbgNomNivel4.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgDen.MoveFirst
                        
        Case "Modificar"
        
                Dim oPres0 As cPresConcep5Nivel0
                Set oPres0 = oFSGSRaiz.Generar_CPresConcep5Nivel0
                oPres0.Cod = m_sCodigo
                
            
            For Each oIdioma In g_oIdiomas
                sCadena = ""
                If Not m_oPres5.Denominaciones.Item(oIdioma.Cod) Is Nothing Then
                    sCadena = NullToStr(m_oPres5.Denominaciones.Item(oIdioma.Cod).Den)
                End If
                sdbgDen.AddItem oIdioma.Den & Chr(m_lSeparador) & sCadena & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgDen.MoveFirst
                    
            For Each oIdioma In g_oIdiomas
                sCadena = ""
                If Not m_oPres5.Denominaciones.Item(oIdioma.Cod) Is Nothing Then
                    sCadena = NullToStr(m_oPres5.NomNivel1.Item(oIdioma.Cod).Den)
                End If
                sdbgNomNivel1.AddItem oIdioma.Den & Chr(m_lSeparador) & sCadena & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgNomNivel1.MoveFirst
            
            For Each oIdioma In g_oIdiomas
                sCadena = ""
                If Not m_oPres5.Denominaciones.Item(oIdioma.Cod) Is Nothing Then
                    sCadena = NullToStr(m_oPres5.NomNivel2.Item(oIdioma.Cod).Den)
                End If
                sdbgNomNivel2.AddItem oIdioma.Den & Chr(m_lSeparador) & sCadena & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgNomNivel2.MoveFirst
            
            For Each oIdioma In g_oIdiomas
                sCadena = ""
                If Not m_oPres5.Denominaciones.Item(oIdioma.Cod) Is Nothing Then
                    sCadena = NullToStr(m_oPres5.NomNivel3.Item(oIdioma.Cod).Den)
                End If
                sdbgNomNivel3.AddItem oIdioma.Den & Chr(m_lSeparador) & sCadena & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgNomNivel3.MoveFirst
            
            For Each oIdioma In g_oIdiomas
                sCadena = ""
                If Not m_oPres5.Denominaciones.Item(oIdioma.Cod) Is Nothing Then
                    sCadena = NullToStr(m_oPres5.NomNivel4.Item(oIdioma.Cod).Den)
                End If
                sdbgNomNivel4.AddItem oIdioma.Den & Chr(m_lSeparador) & sCadena & Chr(m_lSeparador) & oIdioma.Cod
            Next
            sdbgNomNivel4.MoveFirst
            
        End Select
        
End Sub

''' <summary>
''' Descarga del formulario
''' </summary>
''' <param name="Cancel">Indica si se para la descarga del formulario</param>
''' <remarks>Llamada desde cmdCancelar_click y cmdAceptar_Click; Tiempo m�ximo < 0,1 seg.</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Set g_oIdiomas = Nothing
End Sub
