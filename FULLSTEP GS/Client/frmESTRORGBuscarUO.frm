VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRORGBuscarUO 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar unidades organizativas"
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7575
   Icon            =   "frmESTRORGBuscarUO.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   7575
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3840
      TabIndex        =   7
      Top             =   3735
      Width           =   1125
   End
   Begin VB.CommandButton cmdAceptar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Seleccionar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2445
      TabIndex        =   6
      Top             =   3735
      Width           =   1125
   End
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7140
      Picture         =   "frmESTRORGBuscarUO.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Cargar"
      Top             =   570
      Width           =   315
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      Height          =   1155
      Left            =   120
      ScaleHeight     =   1095
      ScaleWidth      =   6915
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   135
      Width           =   6975
      Begin VB.TextBox txtDenCia 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1305
         MaxLength       =   150
         TabIndex        =   10
         Top             =   645
         Width           =   3150
      End
      Begin VB.TextBox txtCIF 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5490
         MaxLength       =   50
         TabIndex        =   9
         Top             =   645
         Width           =   1320
      End
      Begin VB.TextBox txtCod 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5490
         MaxLength       =   50
         TabIndex        =   1
         Top             =   180
         Width           =   1320
      End
      Begin VB.TextBox txtDen 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1305
         MaxLength       =   150
         TabIndex        =   0
         Top             =   180
         Width           =   3150
      End
      Begin VB.Label lblDenCia 
         BackColor       =   &H00808000&
         Caption         =   "Raz�n social:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   120
         TabIndex        =   12
         Top             =   705
         Width           =   1155
      End
      Begin VB.Label lblCIF 
         BackColor       =   &H00808000&
         Caption         =   "DUNS/VAT:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   4650
         TabIndex        =   11
         Top             =   705
         Width           =   870
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   4650
         TabIndex        =   8
         Top             =   240
         Width           =   750
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   1155
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgBuscar 
      Height          =   2085
      Left            =   120
      TabIndex        =   3
      Top             =   1515
      Width           =   7335
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   7
      stylesets.count =   1
      stylesets(0).Name=   "ActiveRowBlue"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8388608
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmESTRORGBuscarUO.frx":01D6
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Columns.Count   =   7
      Columns(0).Width=   1614
      Columns(0).Caption=   "UON1"
      Columns(0).Name =   "UON1"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1376
      Columns(1).Caption=   "UON2"
      Columns(1).Name =   "UON2"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1376
      Columns(2).Caption=   "UON3"
      Columns(2).Name =   "UON3"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1349
      Columns(3).Caption=   "UON4"
      Columns(3).Name =   "UON4"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3175
      Columns(4).Caption=   "Denominaci�n"
      Columns(4).Name =   "DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   1826
      Columns(5).Caption=   "CIF"
      Columns(5).Name =   "NIF"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3149
      Columns(6).Caption=   "Raz�n social"
      Columns(6).Name =   "DENCIA"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      TabNavigation   =   1
      _ExtentX        =   12938
      _ExtentY        =   3678
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmESTRORGBuscarUO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oUON1Encontradas As CUnidadesOrgNivel1
Private m_oUON2Encontradas As CUnidadesOrgNivel2
Private m_oUON3Encontradas As CUnidadesOrgNivel3
Private m_oUON4Encontradas As CUnidadesOrgNivel4
Private m_sCC As String

Public g_sOrigen As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub cmdAceptar_Click()
Dim nodx As node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm

    If sdbgBuscar.Rows = 0 Then
        Exit Sub
    End If
       
    Select Case g_sOrigen
    
        Case "frmPROCE"
    
                Screen.MousePointer = vbHourglass
                If Trim(sdbgBuscar.Columns("UON3").Value) <> "" Then
                     'uo de nivel 3
                     scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                     scod2 = sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                     scod3 = sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                     Set nodx = frmPROCE.tvwestrorg.Nodes.Item("UON1" & scod1 & "UON2" & scod2 & "UON3" & scod3)
                 Else
                     If Trim(sdbgBuscar.Columns("UON2").Value) <> "" Then
                         'uo de nivel 2
                         scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                         scod2 = sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                         Set nodx = frmPROCE.tvwestrorg.Nodes.Item("UON1" & scod1 & "UON2" & scod2)
                     Else
                         If Trim(sdbgBuscar.Columns("UON1").Value) <> "" Then
                             'uo de nivel 1
                             scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                             Set nodx = frmPROCE.tvwestrorg.Nodes.Item("UON1" & scod1)
                         End If
                     End If
                 End If
                Screen.MousePointer = vbNormal
                nodx.Parent.Expanded = True
                nodx.Selected = True
                Unload Me
                frmPROCE.ConfigurarInterfazUON nodx
    
    
        Case "frmPresupuestos1"
            
                Screen.MousePointer = vbHourglass
                If Trim(sdbgBuscar.Columns("UON3").Value) <> "" Then
                     'uo de nivel 3
                     scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                     scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                     scod3 = scod2 & sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                     Set nodx = frmPresupuestos1.tvwestrorg.Nodes.Item("UON3" & scod3)
                Else
                    If Trim(sdbgBuscar.Columns("UON2").Value) <> "" Then
                        'uo de nivel 2
                        scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                        scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                        Set nodx = frmPresupuestos1.tvwestrorg.Nodes.Item("UON2" & scod2)
                    Else
                        If Trim(sdbgBuscar.Columns("UON1").Value) <> "" Then
                            'uo de nivel 1
                            scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                            Set nodx = frmPresupuestos1.tvwestrorg.Nodes.Item("UON1" & scod1)
                        End If
                    End If
                End If
                Screen.MousePointer = vbNormal
                nodx.Parent.Expanded = True
                nodx.Selected = True
                If Not nodx Is Nothing Then
                    Select Case frmPresupuestos1.iUOBase
                        Case 0
                        Case 1
                            Select Case Left(nodx.Tag, 4)
                                Case "UON1", "UON2", "UON3"
                                    frmPresupuestos1.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos1.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 2
                            Select Case Left(nodx.Tag, 4)
                                Case "UON2", "UON3"
                                    frmPresupuestos1.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos1.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 3
                            Select Case Left(nodx.Tag, 4)
                                Case "UON3"
                                    frmPresupuestos1.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos1.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                    End Select
                End If
                Unload Me
                If frmPresupuestos1.Visible Then frmPresupuestos1.tvwestrorg.SetFocus
                
            
        Case "frmPresupuestos2"
        
                Screen.MousePointer = vbHourglass
                If Trim(sdbgBuscar.Columns("UON3").Value) <> "" Then
                     'uo de nivel 3
                     scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                     scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                     scod3 = scod2 & sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                     Set nodx = frmPresupuestos2.tvwestrorg.Nodes.Item("UON3" & scod3)
                Else
                    If Trim(sdbgBuscar.Columns("UON2").Value) <> "" Then
                        'uo de nivel 2
                        scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                        scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                        Set nodx = frmPresupuestos2.tvwestrorg.Nodes.Item("UON2" & scod2)
                    Else
                        If Trim(sdbgBuscar.Columns("UON1").Value) <> "" Then
                            'uo de nivel 1
                            scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                            Set nodx = frmPresupuestos2.tvwestrorg.Nodes.Item("UON1" & scod1)
                        End If
                    End If
                End If
                Screen.MousePointer = vbNormal
                nodx.Parent.Expanded = True
                nodx.Selected = True
                If Not nodx Is Nothing Then
                    Select Case frmPresupuestos2.iUOBase
                        Case 0
                        Case 1
                            Select Case Left(nodx.Tag, 4)
                                Case "UON1", "UON2", "UON3"
                                    frmPresupuestos2.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos2.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 2
                            Select Case Left(nodx.Tag, 4)
                                Case "UON2", "UON3"
                                    frmPresupuestos2.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos2.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 3
                            Select Case Left(nodx.Tag, 4)
                                Case "UON3"
                                    frmPresupuestos2.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos2.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                    End Select
                End If
                Unload Me
                If frmPresupuestos2.Visible Then frmPresupuestos2.tvwestrorg.SetFocus
                
            
        Case "frmPresupuestos3"
        
                Screen.MousePointer = vbHourglass
                If Trim(sdbgBuscar.Columns("UON3").Value) <> "" Then
                     'uo de nivel 3
                     scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                     scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                     scod3 = scod2 & sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                     Set nodx = frmPresupuestos3.tvwestrorg.Nodes.Item("UON3" & scod3)
                Else
                    If Trim(sdbgBuscar.Columns("UON2").Value) <> "" Then
                        'uo de nivel 2
                        scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                        scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                        Set nodx = frmPresupuestos3.tvwestrorg.Nodes.Item("UON2" & scod2)
                    Else
                        If Trim(sdbgBuscar.Columns("UON1").Value) <> "" Then
                            'uo de nivel 1
                            scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                            Set nodx = frmPresupuestos3.tvwestrorg.Nodes.Item("UON1" & scod1)
                        End If
                    End If
                End If
                Screen.MousePointer = vbNormal
                nodx.Parent.Expanded = True
                nodx.Selected = True
                If Not nodx Is Nothing Then
                    Select Case frmPresupuestos3.iUOBase
                        Case 0
                        Case 1
                            Select Case Left(nodx.Tag, 4)
                                Case "UON1", "UON2", "UON3"
                                    frmPresupuestos3.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos3.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 2
                            Select Case Left(nodx.Tag, 4)
                                Case "UON2", "UON3"
                                    frmPresupuestos3.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos3.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 3
                            Select Case Left(nodx.Tag, 4)
                                Case "UON3"
                                    frmPresupuestos3.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos3.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                    End Select
                End If
                Unload Me
                If frmPresupuestos3.Visible Then frmPresupuestos3.tvwestrorg.SetFocus
            
            
        Case "frmPresupuestos4"
        
                Screen.MousePointer = vbHourglass
                If Trim(sdbgBuscar.Columns("UON3").Value) <> "" Then
                     'uo de nivel 3
                     scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                     scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                     scod3 = scod2 & sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                     Set nodx = frmPresupuestos4.tvwestrorg.Nodes.Item("UON3" & scod3)
                Else
                    If Trim(sdbgBuscar.Columns("UON2").Value) <> "" Then
                        'uo de nivel 2
                        scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                        scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                        Set nodx = frmPresupuestos4.tvwestrorg.Nodes.Item("UON2" & scod2)
                    Else
                        If Trim(sdbgBuscar.Columns("UON1").Value) <> "" Then
                            'uo de nivel 1
                            scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                            Set nodx = frmPresupuestos4.tvwestrorg.Nodes.Item("UON1" & scod1)
                        End If
                    End If
                End If
                Screen.MousePointer = vbNormal
                nodx.Parent.Expanded = True
                nodx.Selected = True
                If Not nodx Is Nothing Then
                    Select Case frmPresupuestos4.iUOBase
                        Case 0
                        Case 1
                            Select Case Left(nodx.Tag, 4)
                                Case "UON1", "UON2", "UON3"
                                    frmPresupuestos4.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos4.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 2
                            Select Case Left(nodx.Tag, 4)
                                Case "UON2", "UON3"
                                    frmPresupuestos4.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos4.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                        Case 3
                            Select Case Left(nodx.Tag, 4)
                                Case "UON3"
                                    frmPresupuestos4.SSTabPresupuestos.TabVisible(1) = True
                                Case Else
                                    frmPresupuestos4.SSTabPresupuestos.TabVisible(1) = False
                            End Select
                    End Select
                End If
                Unload Me
                If frmPresupuestos4.Visible Then frmPresupuestos4.tvwestrorg.SetFocus
                 
        
        Case "frmPRESAsig"
    
                Screen.MousePointer = vbHourglass
                If Trim(sdbgBuscar.Columns("UON3").Value) <> "" Then
                     'uo de nivel 3
                     scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                     scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                     scod3 = scod2 & sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                     Set nodx = frmPRESAsig.tvwestrorg.Nodes.Item("UON3" & scod3)
                 Else
                     If Trim(sdbgBuscar.Columns("UON2").Value) <> "" Then
                         'uo de nivel 2
                         scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                         scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                         Set nodx = frmPRESAsig.tvwestrorg.Nodes.Item("UON2" & scod2)
                     Else
                         If Trim(sdbgBuscar.Columns("UON1").Value) <> "" Then
                             'uo de nivel 1
                             scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                             Set nodx = frmPRESAsig.tvwestrorg.Nodes.Item("UON1" & scod1)
                         End If
                     End If
                 End If
                Screen.MousePointer = vbNormal
                nodx.Parent.Expanded = True
                nodx.Selected = True
                Unload Me
                frmPRESAsig.ConfigurarInterfazSeguridadUON nodx
                If frmPRESAsig.Visible Then frmPRESAsig.tvwestrorg.SetFocus
        
        
        Case Else
                Screen.MousePointer = vbHourglass
                If Trim(sdbgBuscar.Columns("UON4").Value) <> "" Then
                     'uo de nivel 3
                     scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                     scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                     scod3 = scod2 & sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                     scod4 = scod3 & sdbgBuscar.Columns("UON4").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(sdbgBuscar.Columns("UON4").Value))
                     Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON4" & scod4)
                     frmESTRORG.sstabEstrorg.TabVisible(1) = True
                Else
                    If Trim(sdbgBuscar.Columns("UON3").Value) <> "" Then
                         'uo de nivel 3
                         scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                         scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                         scod3 = scod2 & sdbgBuscar.Columns("UON3").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgBuscar.Columns("UON3").Value))
                         Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON3" & scod3)
                    Else
                        If Trim(sdbgBuscar.Columns("UON2").Value) <> "" Then
                            'uo de nivel 2
                            scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                            scod2 = scod1 & sdbgBuscar.Columns("UON2").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgBuscar.Columns("UON2").Value))
                            Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON2" & scod2)
                        Else
                            If Trim(sdbgBuscar.Columns("UON1").Value) <> "" Then
                                'uo de nivel 1
                                scod1 = sdbgBuscar.Columns("UON1").Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgBuscar.Columns("UON1").Value))
                                Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("UON1" & scod1)
                            End If
                        End If
                    End If
                End If
                Screen.MousePointer = vbNormal
                nodx.Parent.Expanded = True
                nodx.Selected = True
                Unload Me
                If frmESTRORG.Visible Then frmESTRORG.tvwestrorg.SetFocus
                frmESTRORG.ConfigurarInterfazSeguridad nodx
    
    End Select
    
Exit Sub

ERROR_Frm:
    Screen.MousePointer = vbNormal
    oMensajes.DatosModificados
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "cmdAceptar_Click", err, Erl, Me)
      GoTo ERROR_Frm
      Exit Sub
   End If

End Sub

Private Sub cmdCancelar_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdCargar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    CargarUOS False
    CargarGrid
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "cmdCargar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
        Set m_oUON4Encontradas = oFSGSRaiz.Generar_CUnidadesOrgNivel4
        Me.sdbgBuscar.Columns("UON4").Visible = True
    Else
        Me.sdbgBuscar.Columns("UON4").Visible = False
    End If
    
    Select Case gParametrosGenerales.giNEO
                
        Case 1
                Set m_oUON1Encontradas = oFSGSRaiz.generar_CUnidadesOrgNivel1
        
        Case 2
                Set m_oUON1Encontradas = oFSGSRaiz.generar_CUnidadesOrgNivel1
                Set m_oUON2Encontradas = oFSGSRaiz.generar_CUnidadesOrgNivel2
        
        Case 3
                Set m_oUON1Encontradas = oFSGSRaiz.generar_CUnidadesOrgNivel1
                Set m_oUON2Encontradas = oFSGSRaiz.generar_CUnidadesOrgNivel2
                Set m_oUON3Encontradas = oFSGSRaiz.generar_CUnidadesOrgNivel3
        
    End Select
    
'    If Not basParametros.gParametrosGenerales.gbActivarCodProveErp Then
'        Me.txtCIF.Visible = False
'        Me.txtDenCia.Visible = False
'        Me.Picture1.Height = 700
'        Me.sdbgBuscar.Columns("NIF").Visible = False
'        Me.sdbgBuscar.Columns("DENCIA").Visible = False
'        Me.sdbgBuscar.Columns("DEN").Width = 4620
'        Me.sdbgBuscar.Top = Me.Picture1.Top + Me.Picture1.Height + 100
'        Me.cmdAceptar.Top = Me.sdbgBuscar.Top + Me.sdbgBuscar.Height + 100
'        Me.cmdCancelar.Top = Me.cmdAceptar.Top
'        Me.cmdCargar.Top = 345
'        Me.Height = 4000
'    End If

    CargarRecursos
    
    PonerFieldSeparator Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
oFSGSRaiz.pg_sFrmCargado Me.Name, False
    g_sOrigen = ""
    Set m_oUON1Encontradas = Nothing
    Set m_oUON2Encontradas = Nothing
    Set m_oUON3Encontradas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub sdbgBuscar_DblClick()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdAceptar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "sdbgBuscar_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgBuscar_InitColumnProps()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case gParametrosGenerales.giNEO
        
        Case 0
            
            sdbgBuscar.Columns("UON1").Visible = False
            sdbgBuscar.Columns("UON2").Visible = False
            sdbgBuscar.Columns("UON3").Visible = False
            sdbgBuscar.Columns("DEN").Width = sdbgBuscar.Width * 25 / 100
            sdbgBuscar.Columns("NIF").Width = sdbgBuscar.Width * 75 / 100
            
        Case 1
            sdbgBuscar.Columns("UON2").Visible = False
            sdbgBuscar.Columns("UON3").Visible = False
            sdbgBuscar.Columns("UON1").Width = sdbgBuscar.Width * 25 / 100
            sdbgBuscar.Columns("DEN").Width = sdbgBuscar.Width * 25 / 100
            sdbgBuscar.Columns("NIF").Width = sdbgBuscar.Width * 50 / 100
        Case 2
            sdbgBuscar.Columns("UON3").Visible = False
            sdbgBuscar.Columns("UON1").Width = sdbgBuscar.Width * 15 / 100
            sdbgBuscar.Columns("UON2").Width = sdbgBuscar.Width * 15 / 100
            sdbgBuscar.Columns("DEN").Width = sdbgBuscar.Width * 15 / 100
            sdbgBuscar.Columns("NIF").Width = sdbgBuscar.Width * 55 / 100
        
    End Select
    
    sdbgBuscar.Columns("UON1").caption = gParametrosGenerales.gsABR_UON1 & "."
    sdbgBuscar.Columns("UON2").caption = gParametrosGenerales.gsABR_UON2 & "."
    sdbgBuscar.Columns("UON3").caption = gParametrosGenerales.gsABR_UON3 & "."
    If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
        sdbgBuscar.Columns("UON4").caption = m_sCC
    End If
    
      
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "sdbgBuscar_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub CargarUOS(ByVal bOrdPorDen As Boolean)
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Select Case gParametrosGenerales.giNEO
        
        Case 0
                
        Case 1
                If g_sOrigen = "frmPROCE" Or g_sOrigen = "frmPRESAsig" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, , , basOptimizacion.gCodDepUsuario, frmPROCE.g_bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                ElseIf g_sOrigen = "frmPresupuestos1" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, , , , frmPresupuestos1.bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                ElseIf g_sOrigen = "frmPRESAsig" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1ParaPresPedido frmPRESAsig.g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, frmPRESAsig.g_bRUO, bOrdPorDen, Trim(txtCod.Text), Trim(txtDen.Text), False, True, Me.txtCIF, Me.txtDenCia
                Else
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, , , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                End If
        Case 2
                If g_sOrigen = "frmPROCE" Or g_sOrigen = "frmPRESAsig" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, frmPROCE.g_bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, frmPROCE.g_bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                ElseIf g_sOrigen = "frmPresupuestos1" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , , frmPresupuestos1.bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , , frmPresupuestos1.bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                ElseIf g_sOrigen = "frmPRESAsig" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1ParaPresPedido frmPRESAsig.g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, frmPRESAsig.g_bRUO, bOrdPorDen, Trim(txtCod.Text), Trim(txtDen.Text), False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2ParaPresPedido frmPRESAsig.g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, frmPRESAsig.g_bRUO, bOrdPorDen, Trim(txtCod.Text), Trim(txtDen.Text), False, True, Me.txtCIF, Me.txtDenCia
                Else
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                End If
        Case 3
                If g_sOrigen = "frmPROCE" Or g_sOrigen = "frmPRESAsig" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPROCE.g_bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPROCE.g_bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON3Encontradas.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmPROCE.g_bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                ElseIf g_sOrigen = "frmPresupuestos1" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , frmPresupuestos1.bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , frmPresupuestos1.bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON3Encontradas.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , frmPresupuestos1.bRUO, , Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                ElseIf g_sOrigen = "frmPRESAsig" Then
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1ParaPresPedido frmPRESAsig.g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, frmPRESAsig.g_bRUO, bOrdPorDen, Trim(txtCod.Text), Trim(txtDen.Text), False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2ParaPresPedido frmPRESAsig.g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, frmPRESAsig.g_bRUO, bOrdPorDen, Trim(txtCod.Text), Trim(txtDen.Text), False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON3Encontradas.CargarTodasLasUnidadesOrgNivel3ParaPresPedido frmPRESAsig.g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, frmPRESAsig.g_bRUO, bOrdPorDen, Trim(txtCod.Text), Trim(txtDen.Text), False, True, Me.txtCIF, Me.txtDenCia
                Else
                    m_oUON1Encontradas.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON2Encontradas.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    m_oUON3Encontradas.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                    If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
                        'm_oUON4Encontradas.CargarTodasLasUnidadesOrgNivel4 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, Trim(txtCod.Text), Trim(txtDen.Text), False, bOrdPorDen, False, True, Me.txtCIF, Me.txtDenCia
                        m_oUON4Encontradas.CargarTodasLasUnidadesOrgNivel4 , , , , , , Trim(txtCod.Text), Trim(txtDen.Text), False, True
                    End If
                End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "CargarUOS", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Cargar Grid Busqueda
''' </summary>
''' <remarks>Llamada desde: cmdCargar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGrid()
Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
Dim oUON4 As CUnidadOrgNivel4

Dim sCadena As String

Dim varUO As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgBuscar.RemoveAll
    
    If g_sOrigen = "frmPRESAsig" Then
        If m_oUON1Encontradas.Count = 0 And m_oUON2Encontradas.Count = 0 And m_oUON3Encontradas.Count = 0 Then
            oMensajes.MensajeOKOnly 727
        End If
    End If
    
    
    For Each oUON1 In m_oUON1Encontradas
        sCadena = oUON1.Cod & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
        
        sCadena = sCadena & Chr(m_lSeparador) & ""
        
        sCadena = sCadena & Chr(m_lSeparador) & oUON1.Den
        
        If Not oUON1.Empresa Is Nothing Then
            sCadena = sCadena & Chr(m_lSeparador) & oUON1.Empresa.NIF & Chr(m_lSeparador) & oUON1.Empresa.Den
        End If

        sdbgBuscar.AddItem sCadena
        
    Next
    
    If gParametrosGenerales.giNEO <= 1 Then Exit Sub
    
    For Each oUON2 In m_oUON2Encontradas
        sCadena = oUON2.CodUnidadOrgNivel1 & Chr(m_lSeparador) & oUON2.Cod & Chr(m_lSeparador) & ""
        
        sCadena = sCadena & Chr(m_lSeparador) & ""
        
        sCadena = sCadena & Chr(m_lSeparador) & oUON2.Den
         
        If Not oUON2.Empresa Is Nothing Then
            sCadena = sCadena & Chr(m_lSeparador) & oUON2.Empresa.NIF & Chr(m_lSeparador) & oUON2.Empresa.Den
        End If
        sdbgBuscar.AddItem sCadena
    Next
    
    If gParametrosGenerales.giNEO <= 2 Then Exit Sub
    
    For Each oUON3 In m_oUON3Encontradas
        sCadena = oUON3.CodUnidadOrgNivel1 & Chr(m_lSeparador) & oUON3.CodUnidadOrgNivel2 & Chr(m_lSeparador) & oUON3.Cod
        
        sCadena = sCadena & Chr(m_lSeparador) & ""
         
        sCadena = sCadena & Chr(m_lSeparador) & oUON3.Den
        If Not oUON3.Empresa Is Nothing Then
            sCadena = sCadena & Chr(m_lSeparador) & oUON3.Empresa.NIF & Chr(m_lSeparador) & oUON3.Empresa.Den
        End If
        sdbgBuscar.AddItem sCadena
    Next
    
    If Not gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then Exit Sub
    
    For Each oUON4 In m_oUON4Encontradas
        sCadena = oUON4.CodUnidadOrgNivel1 & Chr(m_lSeparador) & oUON4.CodUnidadOrgNivel2 & Chr(m_lSeparador) & oUON4.CodUnidadOrgNivel3 & Chr(m_lSeparador) & oUON4.Cod & Chr(m_lSeparador) & oUON4.Den
        sdbgBuscar.AddItem sCadena
    Next
    
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oUON4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "CargarGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG_BUSCARUO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblCod.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgBuscar.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
       
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblDenCia.caption = Ador(0).Value & ":"
        sdbgBuscar.Columns("DENCIA").caption = Ador(0).Value
        Ador.MoveNext
        sdbgBuscar.Columns("CIF").caption = Ador(0).Value
        Me.lblCIF.caption = Ador(0).Value & ":"
        Ador.MoveNext
        
        m_sCC = Ador(0).Value ' CC. (Abreviatura de Centro de coste)
        Ador.Close
        
    End If

    Set Ador = Nothing


'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRORGBuscarUO", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub




