VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPARTipoRelac 
   Caption         =   "Estados de ofertas (Consulta)"
   ClientHeight    =   3945
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5940
   Icon            =   "frmPARTipoRelac.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3945
   ScaleWidth      =   5940
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   5940
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   3390
      Width           =   5940
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2280
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4860
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdCodigo 
         Caption         =   "&C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3480
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRelacTipo 
      Height          =   2875
      Left            =   60
      TabIndex        =   8
      Top             =   60
      Width           =   5820
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   3
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPARTipoRelac.frx":014A
      stylesets(1).Name=   "chkBloqueado"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmPARTipoRelac.frx":0166
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   3200
      Columns(1).Caption=   "Permitir �rdenes"
      Columns(1).Name =   "ORD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   2
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   10266
      _ExtentY        =   5071
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPARTipoRelac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPARRelacTipo
''' *** Creacion: 5/08/2011 (JVS)


Option Explicit

''' Coleccion de Tipos de relacion
Public oRelacTipos As CRelacTipos
Public bOrdenListadoDen As Boolean
Public Accion As accionessummit
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean

''' RelacTipo en edicion
Private m_oRelacTipoEnEdicion As CRelacTipo
Private oIBAseDatosEnEdicion As IBaseDatos
''' Propiedades de seguridad
Private bModif  As Boolean
''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean
''' Variables de control
Private bCambioGeneral As Boolean
Private bModoEdicion As Boolean
''' Posicion para UnboundReadData
Private p As Long
Private sCod As String
'Multilenguaje
Private sIdiTitulos(1 To 4) As String
Private sIdiTipoRelCod As String
Private sIdiCodigo As String
Private sIdiConsulta As String
Private sIdiEdicion As String
Private sIdiLaRelacTipo As String
Private sIdiDenominacion As String
Private sIdiOrdenando As String
Private sIdiEliminacionMultiple As String
'Variables de idiomas:
Private m_sDato As String
Private oIdiomas As CIdiomas

''' <summary>Configuraci�n del formulario seg�n los permisos</summary>
''' <remarks>Llamada desde:frmPARTipoRelac.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub ConfigurarSeguridad()
    bModif = True
    cmdModoEdicion.Visible = True
End Sub

''' <summary>Ajustar tama�o y posici�n de los controles del formulario</summary>
''' <remarks>Llamada desde:frmPARTipoRelac.Form_Resize; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 5770 Then   'de tama�o de la ventana
            Me.Width = 5970        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 2300 Then  'cuando no se maximiza ni
            Me.Height = 2500       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width

    
    If Height >= 1475 Then sdbgRelacTipo.Height = Height - 1475
    If Width >= 250 Then sdbgRelacTipo.Width = Width - 250
    
    sdbgRelacTipo.Columns(0).Width = sdbgRelacTipo.Width * 15 / 100
    sdbgRelacTipo.Columns(1).Width = sdbgRelacTipo.Width * 15 / 100
   
    cmdModoEdicion.Left = sdbgRelacTipo.Left + sdbgRelacTipo.Width - cmdModoEdicion.Width
    
End Sub

''' <summary>Evento click bot�n de a�adir tipo de relaci�n </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub cmdA�adir_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgRelacTipo.Scroll 0, sdbgRelacTipo.Rows - sdbgRelacTipo.Row
    
    If sdbgRelacTipo.VisibleRows > 0 Then
        
        If sdbgRelacTipo.VisibleRows > sdbgRelacTipo.Rows Then
            sdbgRelacTipo.Row = sdbgRelacTipo.Rows
        Else
            sdbgRelacTipo.Row = sdbgRelacTipo.Rows - (sdbgRelacTipo.Rows - sdbgRelacTipo.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgRelacTipo.SetFocus
    
End Sub

''' <summary>Evento click bot�n de c�digo </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub cmdCodigo_Click()
    
    ''' * Objetivo: Cambiar de codigo la RelacTipo actual
    
    Dim teserror As TipoErrorSummit
    
    ''' Resaltar la RelacTipo actual
    
    If sdbgRelacTipo.Rows = 0 Then Exit Sub
    
    sdbgRelacTipo.SelBookmarks.Add sdbgRelacTipo.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    
    Set m_oRelacTipoEnEdicion = oRelacTipos.Item(CStr(sdbgRelacTipo.Bookmark))
    Set oIBAseDatosEnEdicion = m_oRelacTipoEnEdicion
   
    
    teserror = oIBAseDatosEnEdicion.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgRelacTipo.SetFocus
        Exit Sub
    End If
        
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
    frmMODCOD.caption = sIdiTipoRelCod
    frmMODCOD.Left = frmPARTipoRelac.Left + 500
    frmMODCOD.Top = frmPARTipoRelac.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodProveTipoRel
    frmMODCOD.txtCodAct.Text = m_oRelacTipoEnEdicion.Cod
    Set frmMODCOD.fOrigen = frmPARTipoRelac
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set m_oRelacTipoEnEdicion = Nothing
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(m_oRelacTipoEnEdicion.Cod) Then
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set m_oRelacTipoEnEdicion = Nothing
        Exit Sub
    End If
                
    ''' Cambiar el codigo
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
    Screen.MousePointer = vbNormal
             
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    cmdRestaurar_Click
    sdbgRelacTipo.MoveFirst
    sdbgRelacTipo.Refresh
    sdbgRelacTipo.Bookmark = sdbgRelacTipo.SelBookmarks(0)
    sdbgRelacTipo.SelBookmarks.RemoveAll
        
    Set oIBAseDatosEnEdicion = Nothing
    Set m_oRelacTipoEnEdicion = Nothing
    
End Sub

''' <summary>Evento click bot�n de deshacer</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la RelacTipo actual
    
    sdbgRelacTipo.CancelUpdate
    sdbgRelacTipo.DataChanged = False
    
    If Not m_oRelacTipoEnEdicion Is Nothing Then
        Set oIBAseDatosEnEdicion = Nothing
        Set m_oRelacTipoEnEdicion = Nothing
    End If
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    cmdCodigo.Enabled = True
    
    Accion = ACCRelacTCon
        
End Sub

''' <summary>Evento click bot�n de elminar tipo de relaci�n</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub cmdEliminar_Click()
'*********************************************************************
'*** Descripci�n: Elimina los tipos de relaci�n seleccionados.     ***
'***              En caso de no poder informa de los tipos de      ***
'***              relaci�n que no pudieron ser eliminados.         ***
'*** Par�metros:         ----------                                ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit

    If sdbgRelacTipo.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    Select Case sdbgRelacTipo.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
            irespuesta = oMensajes.PreguntaEliminar(sIdiLaRelacTipo & " " & sdbgRelacTipo.Columns(0).Value & " (" & sdbgRelacTipo.Columns(1).Value & ")")
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminarOfeEst(sIdiEliminacionMultiple)
    End Select
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    ReDim aIdentificadores(sdbgRelacTipo.SelBookmarks.Count)
    ReDim aBookmarks(sdbgRelacTipo.SelBookmarks.Count)
    
    i = 0
    While i < sdbgRelacTipo.SelBookmarks.Count
        aIdentificadores(i + 1) = oRelacTipos.Item(CStr(sdbgRelacTipo.SelBookmarks(i))).Id
        aBookmarks(i + 1) = sdbgRelacTipo.SelBookmarks(i)
        sdbgRelacTipo.Bookmark = sdbgRelacTipo.SelBookmarks(i)
        i = i + 1
    Wend
    udtTeserror = oRelacTipos.EliminarTiposDeRelacionDeBaseDatos(aIdentificadores)
    If udtTeserror.NumError <> TESnoerror Then
    
      If udtTeserror.NumError = TESImposibleEliminar Then
          iIndice = 1
          aAux = udtTeserror.Arg1
          inum = UBound(udtTeserror.Arg1, 2)

          For i = 0 To inum
              udtTeserror.Arg1(2, i) = oRelacTipos.Item(CStr(sdbgRelacTipo.SelBookmarks(aAux(2, i) - iIndice))).Descripciones.Item(gParametrosInstalacion.gIdioma).Den
              sdbgRelacTipo.SelBookmarks.Remove (aAux(2, i) - iIndice)
              iIndice = iIndice + 1
          Next i
          'Mensaje de cuales no se pudieron eliminar y porque
          oMensajes.ImposibleEliminacionMultiple 308, udtTeserror.Arg1
          If sdbgRelacTipo.SelBookmarks.Count > 0 Then
                sdbgRelacTipo.DeleteSelected
                DoEvents
                AnyadirCampos
          End If
      Else
          TratarError udtTeserror
          Screen.MousePointer = vbNormal
          Exit Sub
      End If
    Else
        sdbgRelacTipo.DeleteSelected
        DoEvents
        AnyadirCampos
    End If
        
    sdbgRelacTipo.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Evento click bot�n de emitir listado </summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub cmdlistado_Click()
    AbrirLstParametros "frmPARTipoRelac", bOrdenListadoDen
End Sub

''' <summary>Evento click bot�n de intercambir modo edici�n / modo consulta</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
                
        'cmdRestaurar_Click
        
        sdbgRelacTipo.AllowAddNew = True
        sdbgRelacTipo.AllowUpdate = True
        sdbgRelacTipo.AllowDelete = False
        
        frmPARTipoRelac.caption = sIdiTitulos(4)
        
        cmdModoEdicion.caption = sIdiConsulta
        
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCRelacTCon
        
        If oUsuarioSummit.Tipo = TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = True
        Else
            cmdCodigo.Visible = False
        End If
            
    
    Else
                
        If sdbgRelacTipo.DataChanged = True Then
        
            v = sdbgRelacTipo.ActiveCell.Value
            If Me.Visible Then sdbgRelacTipo.SetFocus
            If (v <> "") Then
                sdbgRelacTipo.ActiveCell.Value = v
            End If
                        
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbgRelacTipo.AllowAddNew = False
        sdbgRelacTipo.AllowUpdate = False
        sdbgRelacTipo.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdCodigo.Visible = False
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
        
        If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = False
        End If
    
        frmPARTipoRelac.caption = sIdiTitulos(3)
        
        cmdModoEdicion.caption = sIdiEdicion
        
        cmdRestaurar_Click
        
        bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgRelacTipo.SetFocus
    
End Sub

''' <summary>Funci�n que actualiza la grid de tipos de relaci�n</summary>
''' <returns>Boolean</returns>
''' <remarks>Llamada desde:frmPARTipoRelac.Form_Unload; frmPARTipoRelac.cmdModoEdicion_Click; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgRelacTipo.DataChanged = True Then
        bValError = False
        bAnyaError = False
        bModError = False
        If sdbgRelacTipo.Row = 0 Then
            sdbgRelacTipo.MoveNext
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgRelacTipo.MovePrevious
            End If
        Else
            sdbgRelacTipo.MovePrevious
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgRelacTipo.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

''' <summary>Evento click en bot�n de Restaurar la grid desde BD</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub cmdRestaurar_Click()

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
    Screen.MousePointer = vbHourglass
    
    Me.caption = sIdiTitulos(3)
       
    AnyadirCampos
    
    Screen.MousePointer = vbNormal
    
    If Me.Visible Then sdbgRelacTipo.SetFocus
    
End Sub

''' <summary>Carga el formulario y los tipos de relaci�n</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub Form_Load()

    ''' * Objetivo: Cargar las tipos de relaci�n e iniciar
    ''' * Objetivo: el formulario
    Dim oIdioma As CIdioma
    Dim i As Integer
    Dim iPosition As Integer
    
    Me.Width = 6060
    Me.Height = 4350
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    PonerFieldSeparator Me
    
    'Carga los tipos de relaci�n de proveedores
    Set oRelacTipos = oFSGSRaiz.Generar_CRelacTipos
    
    Screen.MousePointer = vbHourglass
    
    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
    Set oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    i = sdbgRelacTipo.Columns.Count
    iPosition = 1
    
    For Each oIdioma In oIdiomas
        If oIdioma.Cod = gParametrosInstalacion.gIdioma Then
            Exit For
        End If
    Next
    
    sdbgRelacTipo.Columns.Add i
    sdbgRelacTipo.Columns(i).Name = gParametrosInstalacion.gIdioma
    sdbgRelacTipo.Columns(i).caption = sIdiDenominacion & " (" & oIdioma.Den & ")"
    sdbgRelacTipo.Columns(i).Style = ssStyleEdit
    sdbgRelacTipo.Columns(i).ButtonsAlways = False
    sdbgRelacTipo.Columns(i).Position = iPosition
    sdbgRelacTipo.Columns(i).Visible = True
    sdbgRelacTipo.Columns(i).Locked = False
    
    For Each oIdioma In oIdiomas
        If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
            i = i + 1
            iPosition = iPosition + 1
            sdbgRelacTipo.Columns.Add i
            sdbgRelacTipo.Columns(i).Name = oIdioma.Cod
            sdbgRelacTipo.Columns(i).caption = sIdiDenominacion & " (" & oIdioma.Den & ")"
            sdbgRelacTipo.Columns(i).Style = ssStyleEdit
            sdbgRelacTipo.Columns(i).ButtonsAlways = False
            sdbgRelacTipo.Columns(i).Position = iPosition
            sdbgRelacTipo.Columns(i).Visible = True
            sdbgRelacTipo.Columns(i).Locked = False
        End If
    Next


    AnyadirCampos
    
    sdbgRelacTipo.Columns("COD").FieldLen = gLongitudesDeCodigos.giLongCodProveTipoRel
    Screen.MousePointer = vbNormal
    bModoEdicion = False
    
    Accion = ACCRelacTCon
        
    bOrdenListadoDen = False
    

    
End Sub

''' <summary>A�adir l�neas a la grid de tipos de relaci�n</summary>
''' <remarks>Llamada desde:frmPARTipoRelac.cmdEliminar_Click; frmPARTipoRelac.cmdRestaurar_Click; frmPARTipoRelac.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Public Sub AnyadirCampos()
    Dim sCadena As String
    Dim oIdioma As CIdioma
    
    Dim oRelacTip As CRelacTipo
    
    oRelacTipos.CargarTodosLosRelacTipos True
    
    If oRelacTipos Is Nothing Then Exit Sub
            
    sdbgRelacTipo.RemoveAll
    
    For Each oRelacTip In oRelacTipos
        
        sCadena = oRelacTip.Cod & Chr(m_lSeparador) & BooleanToSQLBinary(oRelacTip.PermitePedidos) & Chr(m_lSeparador) & oRelacTip.Id
        'Denominaci�n del campo en los diferentes idiomas:
        sCadena = sCadena & Chr(m_lSeparador) & oRelacTip.Descripciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den
        
        
        For Each oIdioma In oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                sCadena = sCadena & Chr(m_lSeparador) & oRelacTip.Descripciones.Item(CStr(oIdioma.Cod)).Den
            End If
        Next
               
        'a�ade la cadena a la grid:
        sdbgRelacTipo.AddItem sCadena
    Next

End Sub

''' <summary>Evento de cambiar tama�o al formulario</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub Form_Resize()
    
    ''' * Objetivo: Adecuar los controles
    
    
    Arrange
    
End Sub

''' <summary>Evento de descarga del formulario</summary>
''' <param name="Cancel">si se ha pulsado ESC</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub Form_Unload(Cancel As Integer)
    Dim v As Variant
    
    If sdbgRelacTipo.DataChanged Then
    
        v = sdbgRelacTipo.ActiveCell.Value
        If Me.Visible Then sdbgRelacTipo.SetFocus
        sdbgRelacTipo.ActiveCell.Value = v
        
        If actualizarYSalir() Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Set oRelacTipos = Nothing
    Set m_oRelacTipoEnEdicion = Nothing
    Set oIBAseDatosEnEdicion = Nothing
    Me.Visible = False
End Sub

''' <summary>Evento afterdelete de la grid de tipos de relaci�n</summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    RtnDispErrMsg = 0
    If (sdbgRelacTipo.Rows = 0) Then
        Exit Sub
    End If
    If Me.Visible Then sdbgRelacTipo.SetFocus
    sdbgRelacTipo.Bookmark = sdbgRelacTipo.RowBookmark(sdbgRelacTipo.Row)
    
End Sub

''' <summary>Evento afterinsert de la grid de tipos de relaci�n</summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub

''' <summary>Evento afterupdate de la grid de tipos de relaci�n</summary>
''' <param name="RtnDispErrMsg">Buffer con los datos y bookmark de la fila</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub

''' <summary>
''' BeforeDelete de la grid de tipos relacionados
''' </summary>
''' <param name="Cancel">Integer</param>
''' <param name="DispPromptMsg">Integer</param>
''' <remarks>Llamada desde:sistema ; Tiempo m�ximo: 1 sec</remarks>
''' <revision>JVS 31/08/2011</revision>

Private Sub sdbgRelacTipo_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>Evento beforeupdate de la grid de tipos de relaci�n</summary>
''' <param name="Cancel">si se ha pulsado ESC</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_BeforeUpdate(Cancel As Integer)
Dim oIdioma As CIdioma
Dim Accion As accionessummit
Dim teserror As TipoErrorSummit
    ''' * Objetivo: Validar los datos
    
    bValError = False
    Cancel = False
    
    If Trim(sdbgRelacTipo.Columns("COD").Value) = "" Then
        oMensajes.NoValido sIdiCodigo
        Cancel = True
        GoTo Salir
    End If
    
    For Each oIdioma In oIdiomas
        If Trim(sdbgRelacTipo.Columns(oIdioma.Cod).Value) = "" Then
            oMensajes.NoValida sIdiDenominacion
'            sdbgRelacTipo.col=sdbgRelacTipo.Columns(oIdioma.Cod)
            Cancel = True
            GoTo Salir
        End If
    Next
    
    If sdbgRelacTipo.IsAddRow Then
        Set m_oRelacTipoEnEdicion = oFSGSRaiz.Generar_CRelacTipo
        m_oRelacTipoEnEdicion.Cod = sdbgRelacTipo.Columns("COD").Value
        Set m_oRelacTipoEnEdicion.Descripciones = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdioma In oIdiomas
            m_oRelacTipoEnEdicion.Descripciones.Add oIdioma.Cod, sdbgRelacTipo.Columns(oIdioma.Cod).Value
        Next
        m_oRelacTipoEnEdicion.PermitePedidos = SQLBinaryToBoolean(IIf(sdbgRelacTipo.Columns("ORD").Value = "", 0, sdbgRelacTipo.Columns("ORD").Value))
        
        Accion = ACCRelacTAnya
        Set oIBAseDatosEnEdicion = m_oRelacTipoEnEdicion
        teserror = oIBAseDatosEnEdicion.AnyadirABaseDatos
    Else
        If m_oRelacTipoEnEdicion Is Nothing Then
            Set m_oRelacTipoEnEdicion = oRelacTipos.Item(CStr(sdbgRelacTipo.Bookmark))
        End If
        For Each oIdioma In oIdiomas
            If Not m_oRelacTipoEnEdicion.Descripciones.Item(oIdioma.Cod) Is Nothing Then
                m_oRelacTipoEnEdicion.Descripciones.Item(oIdioma.Cod).Den = sdbgRelacTipo.Columns(oIdioma.Cod).Value
            Else
                m_oRelacTipoEnEdicion.Descripciones.Add oIdioma.Cod, sdbgRelacTipo.Columns(oIdioma.Cod).Value
            End If
        Next
        m_oRelacTipoEnEdicion.PermitePedidos = SQLBinaryToBoolean(sdbgRelacTipo.Columns("ORD").Value)
        Accion = ACCRelacTMod
        Set oIBAseDatosEnEdicion = m_oRelacTipoEnEdicion
        teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
    End If
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgRelacTipo.CancelUpdate
        If Me.Visible Then sdbgRelacTipo.SetFocus
        sdbgRelacTipo.DataChanged = False
    Else
        ''' Registro de acciones
        basSeguridad.RegistrarAccion Accion, "Id" & m_oRelacTipoEnEdicion.Id
        Set oIBAseDatosEnEdicion = Nothing
        Set m_oRelacTipoEnEdicion = Nothing
    End If

Salir:
        
    bValError = Cancel
    If Me.Visible Then sdbgRelacTipo.SetFocus
        
End Sub

''' <summary>Evento change de la grid de tipos de relaci�n</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
        
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    
    End If
    
    If Accion = ACCRelacTCon And Not sdbgRelacTipo.IsAddRow Then
    
        Set m_oRelacTipoEnEdicion = Nothing
        Set m_oRelacTipoEnEdicion = oRelacTipos.Item(CStr(sdbgRelacTipo.Bookmark))
      
        Set oIBAseDatosEnEdicion = m_oRelacTipoEnEdicion
        
        teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            
            TratarError teserror
            sdbgRelacTipo.DataChanged = False
            teserror.NumError = TESnoerror
            
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgRelacTipo.SetFocus
        Else
            Accion = ACCRelacTMod
        End If
    End If
End Sub

''' <summary>Evento keydown de la grid de tipos de relaci�n</summary>
''' <param name="KeyCode">Codigo de la tecla presionada</param>
''' <param name="Shift">Si esta apretada la tecla shift</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_KeyDown(KeyCode As Integer, Shift As Integer)
'*********************************************************************
'*** Descripci�n: Captura la tecla Supr (Delete) y cuando          ***
'***              �sta es presionada se lanza el evento            ***
'***              Click asociado al bot�n eliminar para            ***
'***              borrar de la BD y de la grid las lineas          ***
'***              seleccionadas.                                   ***
'*** Par�metros:  KeyCode ::> Contiene el c�digo interno           ***
'***                          de la tecla pulsada.                 ***
'***              Shift   ::> Es la m�scara, sin uso en esta       ***
'***                          subrutina.                           ***
'*** Valor que devuelve: ----------                                ***
'*********************************************************************
    If KeyCode = vbKeyDelete Then
        If bModoEdicion And sdbgRelacTipo.SelBookmarks.Count > 0 Then
            cmdEliminar_Click
        End If
    End If
End Sub

''' <summary>Evento keypress de la grid de tipos de relaci�n</summary>
''' <param name="KeyAscii">Codigo ascii de la tecla presionada</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgRelacTipo.DataChanged = False Then
            
            sdbgRelacTipo.CancelUpdate
            sdbgRelacTipo.DataChanged = False
            
            If Not m_oRelacTipoEnEdicion Is Nothing Then
                Set oIBAseDatosEnEdicion = Nothing
                Set m_oRelacTipoEnEdicion = Nothing
            End If
           
            If Not sdbgRelacTipo.IsAddRow Then
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
            Else
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = False
            End If
            
            Accion = ACCRelacTCon
            
        Else
        
            If sdbgRelacTipo.IsAddRow Then
           
                cmdA�adir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
           
                Accion = ACCRelacTCon
            
            End If
            
        End If
        
    End If
    
End Sub

''' <summary>Evento rowcolchange de la grid de tipos de relaci�n</summary>
''' <param name="LastRow">�ltima fila</param>
''' <param name="LastCol">�ltima columna</param>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub sdbgRelacTipo_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgRelacTipo.IsAddRow Then
        sdbgRelacTipo.Columns("COD").Locked = True
        If sdbgRelacTipo.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
        End If
    Else
        If sdbgRelacTipo.DataChanged = True Then cmdDeshacer.Enabled = True
        sdbgRelacTipo.Columns("COD").Locked = False
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgRelacTipo.Row) Then
                sdbgRelacTipo.col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdCodigo.Enabled = False
        
    End If
        
End Sub

''' <summary>
''' Carga las cadenas de idiomas
''' </summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
''' <revision>JVS 01/09/2011</revision>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PARTIPO_REL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    sIdiTitulos(1) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(2) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(3) = Ador(0).Value
    caption = sIdiTitulos(3)
    Ador.MoveNext
    sIdiTitulos(4) = Ador(0).Value
    Ador.MoveNext
    sdbgRelacTipo.Columns(0).caption = Ador(0).Value 'C�digo
    Ador.MoveNext
    sdbgRelacTipo.Columns(1).caption = Ador(0).Value 'Permitir �rdenes
    Ador.MoveNext
    cmdA�adir.caption = Ador(0).Value
    Ador.MoveNext
    cmdCodigo.caption = Ador(0).Value
    sCod = cmdCodigo.caption & ":"
    Ador.MoveNext
    cmdDeshacer.caption = Ador(0).Value
    Ador.MoveNext
    cmdEliminar.caption = Ador(0).Value
    'Ador.MoveNext
    'cmdFiltrar.caption = Ador(0).Value
    Ador.MoveNext
    cmdListado.caption = Ador(0).Value
    Ador.MoveNext
    cmdModoEdicion.caption = Ador(0).Value
    Ador.MoveNext
    cmdRestaurar.caption = Ador(0).Value
    Ador.MoveNext
    
    
    sIdiTipoRelCod = Ador(0).Value
    Ador.MoveNext
    sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    sIdiConsulta = Ador(0).Value
    Ador.MoveNext
    sIdiEdicion = Ador(0).Value
    Ador.MoveNext
    sIdiLaRelacTipo = Ador(0).Value
    Ador.MoveNext
    sIdiDenominacion = Ador(0).Value
    Ador.MoveNext
    sIdiOrdenando = Ador(0).Value
    Ador.MoveNext
    sIdiEliminacionMultiple = Ador(0).Value '"m�ltiples estados de ofertas."
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub


