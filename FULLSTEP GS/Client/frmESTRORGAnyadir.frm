VERSION 5.00
Begin VB.Form frmESTRORGAnyadir 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "A�adir"
   ClientHeight    =   1455
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3285
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORGAnyadir.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1455
   ScaleWidth      =   3285
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.OptionButton optDep 
      BackColor       =   &H00808000&
      Caption         =   "A�adir departamento"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   180
      TabIndex        =   1
      Top             =   540
      Width           =   2985
   End
   Begin VB.OptionButton optUO 
      BackColor       =   &H00808000&
      Caption         =   "A�adir"
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   180
      TabIndex        =   0
      Top             =   225
      Value           =   -1  'True
      Width           =   3015
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   1665
      TabIndex        =   3
      Top             =   1050
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   555
      TabIndex        =   2
      Top             =   1050
      Width           =   1005
   End
End
Attribute VB_Name = "frmESTRORGAnyadir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private sIdiA�adir  As String

Private Sub cmdAceptar_Click()

Screen.MousePointer = vbHourglass

If optUO Then
    
    Select Case frmESTRORG.Accion

        Case ACCUON1Anya
    
            frmESTRORG.Accion = ACCUON1Anya
            'frmESTRORGDetalle.Caption = "A�adir " & LCase(gParametrosGenerales.gsDEN_UON1)
            frmESTRORGDetalle.caption = sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON1)
            Screen.MousePointer = vbNormal
            Me.Hide
            Screen.MousePointer = vbNormal
            frmESTRORGDetalle.Show 1
        
        Case ACCUON2Anya
        
            frmESTRORG.Accion = ACCUON2Anya
            'frmESTRORGDetalle.Caption = "A�adir " & LCase(gParametrosGenerales.gsDEN_UON2)
            frmESTRORGDetalle.caption = sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON2)
            Screen.MousePointer = vbNormal
            Me.Hide
            Screen.MousePointer = vbNormal
            frmESTRORGDetalle.Show 1
    
        Case ACCUON3Anya
            
            Screen.MousePointer = vbHourglass
            frmESTRORG.Accion = ACCUON3Anya
            'frmESTRORGDetalle.Caption = "A�adir " & LCase(gParametrosGenerales.gsDEN_UON3)
            frmESTRORGDetalle.caption = sIdiA�adir & " " & LCase(gParametrosGenerales.gsDEN_UON3)
            Screen.MousePointer = vbNormal
            Me.Hide
            Screen.MousePointer = vbNormal
            frmESTRORGDetalle.Show 1
            
        Case ACCUON4Anya
            Screen.MousePointer = vbHourglass
            frmESTRORG.Accion = ACCUON4Anya
            frmESTRORGDetalle.caption = sIdiA�adir & " " & gParametrosGenerales.gsDEN_UON4
            Screen.MousePointer = vbNormal
            Me.Hide
            Screen.MousePointer = vbNormal
            frmESTRORGDetalle.Show 1
        
    
    End Select

Else
    frmESTRORG.Accion = ACCDepAnya
    Screen.MousePointer = vbNormal
    Me.Hide
    frmESTRORGAnyaDep.Show 1
End If

Screen.MousePointer = vbNormal
Unload Me

End Sub

Private Sub cmdCancelar_Click()
    Screen.MousePointer = vbHourglass
    frmESTRORG.Accion = ACCUOCon
    Screen.MousePointer = vbNormal
    Unload Me
End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG_ANYADIR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        optUO.caption = Ador(0).Value
        Ador.MoveNext
        optDep.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiA�adir = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing

End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

CargarRecursos
End Sub
