VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmDetallePersona 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Detalle"
   ClientHeight    =   4365
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7140
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetallePersona.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4365
   ScaleWidth      =   7140
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3015
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   5318
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5415
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":107A
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":118E
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":14E2
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":1836
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":1B8A
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":1F1E
            Key             =   "COMP"
            Object.Tag             =   "COMP"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":2028
            Key             =   "PERSASIG"
            Object.Tag             =   "PERSASIG"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmDetallePersona.frx":234A
            Key             =   "COMPASIG"
            Object.Tag             =   "COMPASIG"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblTfno2 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Tfno. 2:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   3705
      TabIndex        =   12
      Top             =   3270
      Width           =   585
   End
   Begin VB.Label lblMail 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "E-mail:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   3705
      TabIndex        =   11
      Top             =   3630
      Width           =   480
   End
   Begin VB.Label lblFax 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Fax:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   10
      Top             =   3630
      Width           =   330
   End
   Begin VB.Label lblTfno 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Tfno. 1:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   9
      Top             =   3270
      Width           =   585
   End
   Begin VB.Label lblEquipo 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Equipo"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   8
      Top             =   3930
      Width           =   480
   End
   Begin VB.Label lblEqpDatos 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1080
      TabIndex        =   7
      Top             =   3960
      Width           =   1740
   End
   Begin VB.Label lblEmailDatos 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   4730
      TabIndex        =   6
      Top             =   3585
      Width           =   2220
   End
   Begin VB.Label lblFaxDatos 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1080
      TabIndex        =   5
      Top             =   3585
      Width           =   1755
   End
   Begin VB.Label lblTelefono2Datos 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   4730
      TabIndex        =   4
      Top             =   3225
      Width           =   2235
   End
   Begin VB.Label lblTelefono1Datos 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1080
      TabIndex        =   3
      Top             =   3225
      Width           =   1755
   End
   Begin VB.Label lblCargo 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "Cargo"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   3705
      TabIndex        =   2
      Top             =   3930
      Width           =   435
   End
   Begin VB.Label lblCargoDatos 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   4730
      TabIndex        =   1
      Top             =   3960
      Width           =   2220
   End
End
Attribute VB_Name = "frmDetallePersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sPersona As String
Private m_oPersona As CPersona

'Control de Errores
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not m_bActivado Then
        m_bActivado = True
   End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetallePersona", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    
    CargarRecursos
    
    'Obtiene los datos de la persona
    CargarDetallePersona
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetallePersona", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset


    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DETALLE_PERSONA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value       '1 Detalle
        Ador.MoveNext
        lblTfno.caption = Ador(0).Value  'Tfno1
        Ador.MoveNext
        lblTfno2.caption = Ador(0).Value   'Tfno2
        Ador.MoveNext
        lblFax.caption = Ador(0).Value  'Fax
        Ador.MoveNext
        lblMail.caption = Ador(0).Value    'E-mail
        Ador.MoveNext
        lblEquipo.caption = Ador(0).Value   'Equipo
        Ador.MoveNext
        lblCargo.caption = Ador(0).Value   'Cargo
        
        
        
        Ador.Close
    End If
    
    Set Ador = Nothing
           
End Sub


Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oPersona = Nothing
    g_sPersona = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetallePersona", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarDetallePersona()
    'Obtiene los datos de la persona
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oPersona = oFSGSRaiz.Generar_CPersona
    m_oPersona.Cod = g_sPersona
    m_oPersona.CargarTodosLosDatos
    
    'Genera la estructura del �rbol
    GenerarEstructuraArbol
    
    'Carga los datos en la pantalla
    Me.caption = Me.caption & Space(2) & m_oPersona.Cod & " - " & NullToStr(m_oPersona.nombre) & " " & NullToStr(m_oPersona.Apellidos)
    lblCargoDatos.caption = NullToStr(m_oPersona.Cargo)
    lblEmailDatos = NullToStr(m_oPersona.mail)
    If Not IsNull(m_oPersona.codEqp) Then
        If m_oPersona.codEqp <> "" Then
            lblEqpDatos = NullToStr(m_oPersona.codEqp) & " - " & NullToStr(m_oPersona.DenEqp)
        End If
    End If
    lblFaxDatos = NullToStr(m_oPersona.Fax)
    lblTelefono1Datos = NullToStr(m_oPersona.Tfno)
    lblTelefono2Datos = NullToStr(m_oPersona.Tfno2)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetallePersona", "CargarDetallePersona", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub GenerarEstructuraArbol()
Dim sUO As String
Dim nodx As node
Dim oUON1 As CUnidadesOrgNivel1
Dim oUON2 As CUnidadesOrgNivel2
Dim oUON3 As CUnidadesOrgNivel3
Dim sDen As String

    ' Unidades organizativas
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
    
    'Nivel 1
    If Not IsNull(m_oPersona.UON1) Then
        Set oUON1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
        sDen = oUON1.DevolverDenominacion(m_oPersona.UON1)
            
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1", CStr(m_oPersona.UON1) & " - " & sDen, "UON1")
        nodx.Tag = "UON1" & CStr(m_oPersona.UON1)
        sUO = "UON1"
    End If
            
            
    'Nivel 2
    If Not IsNull(m_oPersona.UON2) Then
        Set oUON2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
        sDen = oUON2.DevolverDenominacion(m_oPersona.UON1, m_oPersona.UON2)
            
        Set nodx = tvwestrorg.Nodes.Add("UON1", tvwChild, "UON2", CStr(m_oPersona.UON2) & " - " & sDen, "UON2")
        nodx.Tag = "UON2" & CStr(m_oPersona.UON2)
        sUO = "UON2"
    End If
            
            
    'Nivel 3
    If Not IsNull(m_oPersona.UON3) Then
        Set oUON3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
        sDen = oUON3.DevolverDenominacion(m_oPersona.UON1, m_oPersona.UON2, m_oPersona.UON3)

        Set nodx = tvwestrorg.Nodes.Add("UON2", tvwChild, "UON3", CStr(m_oPersona.UON3) & " - " & sDen, "UON3")
        nodx.Tag = "UON3" & CStr(m_oPersona.UON3)
        sUO = "UON3"
    End If
    
    'A�ade el departamento:
    If sUO = "" Then
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEP", CStr(m_oPersona.CodDep) & " - " & m_oPersona.DenDep, "Departamento")
    Else
        Set nodx = tvwestrorg.Nodes.Add(sUO, tvwChild, "DEP", CStr(m_oPersona.CodDep) & " - " & m_oPersona.DenDep, "Departamento")
    End If
    nodx.Tag = "DEP" & CStr(m_oPersona.CodDep)
    
        
    'Ahora a�ade la persona:
    If Not IsNull(m_oPersona.codEqp) Then
        Set nodx = tvwestrorg.Nodes.Add("DEP", tvwChild, "PERS" & CStr(m_oPersona.Cod), CStr(m_oPersona.Cod) & " - " & m_oPersona.Apellidos & " " & m_oPersona.nombre, "COMP")
    Else
        Set nodx = tvwestrorg.Nodes.Add("DEP", tvwChild, "PERS" & CStr(m_oPersona.Cod), CStr(m_oPersona.Cod) & " - " & m_oPersona.Apellidos & " " & m_oPersona.nombre, "Persona")
    End If
    nodx.Tag = "PER3" & CStr(m_oPersona.Cod)
    nodx.Selected = True

    Set nodx = Nothing
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetallePersona", "GenerarEstructuraArbol", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub
    
