VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstREU 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Reuniones (Opciones)"
   ClientHeight    =   6435
   ClientLeft      =   3810
   ClientTop       =   1395
   ClientWidth     =   8340
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstREU.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6435
   ScaleWidth      =   8340
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      Height          =   435
      Left            =   6915
      TabIndex        =   22
      Top             =   5910
      Width           =   1350
   End
   Begin TabDlg.SSTab sstabProce 
      Height          =   5790
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   8265
      _ExtentX        =   14579
      _ExtentY        =   10213
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstREU.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraComprador"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Timer1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraMat"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "fraDatosGenerales"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "fraFecha"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstREU.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraOrden"
      Tab(1).Control(1)=   "fraOpciones"
      Tab(1).ControlCount=   2
      Begin VB.Frame fraFecha 
         Caption         =   "Filtro por Fecha "
         Height          =   1170
         Left            =   120
         TabIndex        =   24
         Top             =   435
         Width           =   8025
         Begin VB.TextBox txtHoraHasta 
            Height          =   285
            Left            =   7020
            TabIndex        =   50
            Top             =   705
            Width           =   855
         End
         Begin VB.TextBox txtHoraDesde 
            Height          =   285
            Left            =   3015
            TabIndex        =   48
            Top             =   705
            Width           =   855
         End
         Begin VB.TextBox txtFecHasta 
            Height          =   285
            Left            =   4800
            TabIndex        =   1
            Top             =   705
            Width           =   1125
         End
         Begin VB.CommandButton cmdCalFecHasta 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5985
            Picture         =   "frmLstREU.frx":0CEA
            Style           =   1  'Graphical
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   705
            Width           =   315
         End
         Begin VB.TextBox txtFecDesde 
            Height          =   285
            Left            =   780
            TabIndex        =   0
            Top             =   705
            Width           =   1140
         End
         Begin VB.CommandButton cmdCalFecDesde 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1995
            Picture         =   "frmLstREU.frx":1274
            Style           =   1  'Graphical
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   705
            Width           =   315
         End
         Begin VB.Label lblHoraHasta 
            Alignment       =   1  'Right Justify
            Caption         =   "Hora:"
            Height          =   225
            Left            =   6300
            TabIndex        =   51
            Top             =   735
            Width           =   660
         End
         Begin VB.Label lblHoraDesde 
            Alignment       =   1  'Right Justify
            Caption         =   "Hora:"
            Height          =   225
            Left            =   2295
            TabIndex        =   49
            Top             =   735
            Width           =   660
         End
         Begin VB.Label lblFecHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   4095
            TabIndex        =   28
            Top             =   735
            Width           =   645
         End
         Begin VB.Label lblFecDesde 
            Caption         =   "Desde:"
            Height          =   225
            Left            =   120
            TabIndex        =   26
            Top             =   720
            Width           =   600
         End
         Begin VB.Label lblFecReu 
            Caption         =   "Fecha de reuni�n:"
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   345
            Width           =   1770
         End
      End
      Begin VB.Frame fraDatosGenerales 
         Caption         =   "Filtro por Datos Generales"
         Height          =   2025
         Left            =   120
         TabIndex        =   30
         Top             =   1650
         Width           =   8025
         Begin VB.TextBox txtSolicitud 
            Height          =   285
            Left            =   1125
            MaxLength       =   20
            TabIndex        =   6
            Top             =   1132
            Width           =   3930
         End
         Begin VB.TextBox txtDen 
            Height          =   285
            Left            =   1125
            MaxLength       =   100
            TabIndex        =   5
            Top             =   746
            Width           =   3930
         End
         Begin VB.TextBox txtCod 
            Height          =   285
            Left            =   6390
            MaxLength       =   20
            TabIndex        =   4
            Top             =   360
            Width           =   930
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEst 
            Height          =   285
            Left            =   1125
            TabIndex        =   7
            Top             =   1515
            Width           =   6210
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8361
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   10954
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   3990
            TabIndex        =   3
            Top             =   360
            Width           =   1065
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
            Height          =   285
            Left            =   1125
            TabIndex        =   2
            Top             =   360
            Width           =   930
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1640
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin VB.Label lblSolicitud 
            Caption         =   "Solicitud:"
            Height          =   225
            Left            =   75
            TabIndex        =   35
            Top             =   1155
            Width           =   1035
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "Cmd.:"
            Height          =   225
            Left            =   3060
            TabIndex        =   32
            Top             =   390
            Width           =   825
         End
         Begin VB.Label lblDen 
            Caption         =   "Den.:"
            Height          =   225
            Left            =   75
            TabIndex        =   34
            Top             =   780
            Width           =   1035
         End
         Begin VB.Label lblAnyo 
            Caption         =   "A�o:"
            ForeColor       =   &H00000000&
            Height          =   165
            Left            =   75
            TabIndex        =   31
            Top             =   420
            Width           =   1035
         End
         Begin VB.Label lblCod 
            Caption         =   "C�digo:"
            Height          =   225
            Left            =   5580
            TabIndex        =   33
            Top             =   390
            Width           =   750
         End
         Begin VB.Label lblEst 
            Caption         =   "Estado:"
            Height          =   225
            Left            =   75
            TabIndex        =   36
            Top             =   1545
            Width           =   1035
         End
      End
      Begin VB.Frame fraMat 
         Caption         =   "Filtro por Material"
         Height          =   825
         Left            =   120
         TabIndex        =   37
         Top             =   3720
         Width           =   8025
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   6540
            Picture         =   "frmLstREU.frx":17FE
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   285
            Width           =   315
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6915
            Picture         =   "frmLstREU.frx":18A3
            Style           =   1  'Graphical
            TabIndex        =   39
            TabStop         =   0   'False
            Top             =   285
            Width           =   345
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1110
            Locked          =   -1  'True
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   300
            Width           =   5310
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   5970
         Top             =   120
      End
      Begin VB.Frame fraComprador 
         Caption         =   "Filtro por Comprador y Equipo"
         Height          =   1095
         Left            =   120
         TabIndex        =   40
         Top             =   4545
         Width           =   8025
         Begin VB.OptionButton optResp 
            Caption         =   "Responsables"
            Height          =   195
            Left            =   210
            TabIndex        =   9
            Top             =   720
            Width           =   1770
         End
         Begin VB.OptionButton optComp 
            Caption         =   "Asignados"
            Height          =   195
            Left            =   210
            TabIndex        =   8
            Top             =   330
            Width           =   1710
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCompCod 
            Height          =   285
            Left            =   3105
            TabIndex        =   12
            Top             =   660
            Width           =   825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1111
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5001
            Columns(1).Caption=   "Apellido y nombre"
            Columns(1).Name =   "Ape"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1455
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcCompDen 
            Height          =   285
            Left            =   4065
            TabIndex        =   13
            Top             =   660
            Width           =   3750
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5424
            Columns(0).Caption=   "Apellido y nombre"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1296
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6615
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
            Height          =   285
            Left            =   4065
            TabIndex        =   11
            Top             =   270
            Width           =   3750
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1640
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6615
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
            Height          =   285
            Left            =   3105
            TabIndex        =   10
            Top             =   270
            Width           =   825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1296
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5345
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1455
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin VB.Label LblComp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   3105
            TabIndex        =   47
            Top             =   675
            Visible         =   0   'False
            Width           =   4710
         End
         Begin VB.Label lblCompCod 
            Caption         =   "Comprador:"
            Height          =   195
            Left            =   2130
            TabIndex        =   42
            Top             =   705
            Width           =   975
         End
         Begin VB.Label lblEqpCod 
            Caption         =   "Equipo:"
            Height          =   195
            Left            =   2130
            TabIndex        =   41
            Top             =   315
            Width           =   975
         End
         Begin VB.Label lblEqp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   3105
            TabIndex        =   46
            Top             =   270
            Visible         =   0   'False
            Width           =   4725
         End
      End
      Begin VB.Frame fraOpciones 
         Caption         =   "Opciones"
         Height          =   3120
         Left            =   -74760
         TabIndex        =   43
         Top             =   600
         Width           =   6300
         Begin VB.CheckBox chkResp 
            Caption         =   "Incluir Responsable"
            Height          =   285
            Left            =   555
            TabIndex        =   19
            Top             =   2565
            Value           =   1  'Checked
            Width           =   5565
         End
         Begin VB.OptionButton opLisReu 
            Caption         =   "Listado de Reuniones"
            Height          =   315
            Left            =   555
            TabIndex        =   17
            Top             =   1695
            Width           =   5535
         End
         Begin VB.OptionButton opLisPlan 
            Caption         =   "Listado de Planificaci�n de Fechas"
            Height          =   240
            Left            =   555
            TabIndex        =   14
            Top             =   405
            Value           =   -1  'True
            Width           =   5640
         End
         Begin VB.CheckBox chkConPlan 
            Caption         =   "Incluir Procesos Planificados"
            Height          =   255
            Left            =   1260
            TabIndex        =   16
            Top             =   1245
            Value           =   1  'Checked
            Width           =   4860
         End
         Begin VB.CheckBox chkSinPlan 
            Caption         =   "Incluir Procesos Sin Planificar"
            Height          =   255
            Left            =   1260
            TabIndex        =   15
            Top             =   810
            Value           =   1  'Checked
            Width           =   4860
         End
         Begin VB.CheckBox chkPer 
            Caption         =   "Incluir Personas Implicadas"
            Enabled         =   0   'False
            Height          =   255
            Left            =   1275
            TabIndex        =   18
            Top             =   2145
            Value           =   1  'Checked
            Width           =   4980
         End
      End
      Begin VB.Frame fraOrden 
         Caption         =   "Orden"
         Height          =   1440
         Left            =   -74760
         TabIndex        =   44
         Top             =   3810
         Width           =   6300
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   255
            Left            =   555
            TabIndex        =   20
            Top             =   630
            Value           =   -1  'True
            Width           =   1740
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   255
            Left            =   2940
            TabIndex        =   21
            Top             =   630
            Width           =   1740
         End
      End
   End
End
Attribute VB_Name = "frmLstREU"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de restricciones
Private m_bRMat As Boolean
Private m_bRAsig As Boolean
Private m_bRCompResp As Boolean
Private m_bREqpAsig As Boolean
Private m_bRUsuAper As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRPerfUON As Boolean

' Origen llamada a formulario
Public sOrigen As String
Private OrdenPorDen As Boolean

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4
Private sGMN1Cod As String
Private sGMN2Cod As String
Private sGMN3Cod As String
Private sGMN4Cod As String

' Variables para el manejo de combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean

Private CargarComboDesdeEqp As Boolean
Private CargarComboDesdeComp As Boolean
Private RespetarCombo As Boolean 'combo compradores

Private DesdeEst As TipoEstadoProceso
Private HastaEst As TipoEstadoProceso

Private oEqpSeleccionado As CEquipo
Private oComps As CCompradores
Private oEqps As CEquipos
Private oCompSeleccionado As CComprador
Private m_sSeleccion As String
'Multilenguaje

Private sIdiEstados(1 To 11) As String

Private sIdiMaterial As String
Private sIdiFecha As String
Private sIdiSeleccion As String
'Private sIdiTodosProce As String
Private sIdiTitulo1 As String
Private sIdiTitulo2 As String
Private sIdiGenerando1 As String
Private sIdiGenerando2 As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private stxtDesde As String
Private stxtHasta As String
Private stxtEstado As String
Private stxtCod As String
Private stxtDen As String
Private stxtAnio As String
Private stxtCompRes As String
Private stxtCompAs As String
Private stxtEqpRes As String
Private stxtEqpAs As String
Private stxtDeci As String
Private stxtNoDeci As String

Private srIdiTitulo As String
Private srIdiProceso As String
Private srIdiDenominacion As String
Private srIdiEstado As String
Private srIdiApertura As String
Private srIdiNecesidad As String
Private srIdiLimiteOfe As String
Private srIdiUltReunion As String
Private srIdiPlanificada As String
Private srIdiDecision As String
Private srIdiResponsable As String
Private srIdiTelefono As String
Private srIdiEMail As String

Private srIdiRTitulo As String
Private srIdiRReferencia As String
Private srIdiRProcesos As String
Private srIdiRHora As String
Private srIdiRResponsable As String
Private srIdiRCodigo As String
Private srIdiRTelefono As String
Private srIdiREMail As String
Private srIdiRSImplicadas As String
Private srIdiRSRol As String
Private srIdiRSCodigo As String
Private srIdiRSApelNom As String
Private srIdiRSUO As String
Private srIdiRSDept As String
Private srIdiRSTelefono As String
Private srIdiRSEMail As String

Private srIdiPag As String
Private srIdiDe As String


Private Sub CargarEstados()

Dim sEstados(1 To 11) As String
Dim i As Integer

sEstados(1) = sIdiEstados(1)
sEstados(2) = sIdiEstados(2)
sEstados(3) = sIdiEstados(3)
sEstados(4) = sIdiEstados(4)
sEstados(5) = sIdiEstados(5)
sEstados(6) = sIdiEstados(6)
sEstados(7) = sIdiEstados(7)
sEstados(8) = sIdiEstados(8)
sEstados(9) = sIdiEstados(9)
sEstados(10) = sIdiEstados(10)
sEstados(11) = sIdiEstados(11)

    For i = 1 To 11
        sdbcEst.AddItem sEstados(i) & Chr(m_lSeparador) & i
    Next
    DesdeEst = validado
    HastaEst = Cerrado
            
        
End Sub

Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
End Sub

Private Sub cmdObtener_Click()
    
    If txtCod.Text <> "" Then
        If txtCod.Text > giMaxProceCod Then
            txtCod.Text = ""
            Screen.MousePointer = vbNormal
            oMensajes.NoValido srIdiProceso
            Exit Sub
        End If
    End If
    
    If sdbcEqpCod.Value = "" Then
        optComp.Value = False
        optResp.Value = False
    End If
    If m_bRAsig Then
        optComp.Value = True
    ElseIf m_bRCompResp Then
        optResp.Value = True
    End If
    
    m_sSeleccion = GenerarTextoSeleccion
    
    If crs_Connected = False Then
        Exit Sub
    End If
        
    If opLisReu Then
        ObtenerListadoReuniones
    Else
        ObtenerListadoPlanReuniones
    End If
End Sub


Private Sub opLisPlan_Click()
If opLisPlan Then
    chkSinPlan.Enabled = True
    chkConPlan.Enabled = True
    chkPer.Enabled = False
End If
End Sub
Private Sub opLisReu_Click()
If opLisReu Then
    chkSinPlan.Enabled = False
    chkConPlan.Enabled = False
    chkPer.Enabled = True
End If
End Sub


Private Sub sdbcCompCod_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
         
        
    End If
    
End Sub


Private Sub sdbcCompCod_Click()
    If Not sdbcCompCod.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
    End If
End Sub

Private Sub sdbcCompCod_CloseUp()
    Dim sCod As String
    
    If sdbcCompCod.Value = "..." Then
        sdbcCompCod.Text = ""
        Exit Sub
    End If
    
    If sdbcCompCod.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompCod.Columns(1).Text
    sdbcCompCod.Text = sdbcCompCod.Columns(0).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompCod.Columns(0).Text)
    Screen.MousePointer = vbNormal
        
    CargarComboDesdeComp = False
    
    DoEvents
    
    'CompradorSeleccionado
    
End Sub

Private Sub sdbcCompCod_DropDown()
Dim EqpCod As String

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    Set oComps = Nothing
    Set oComps = oFSGSRaiz.generar_CCompradores
           
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcCompCod.Text), , , False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, False, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    'Set oEqpSeleccionado.Compradores = Nothing
  
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompCod.AddItem "..."
    End If

    sdbcCompCod.SelStart = 0
    sdbcCompCod.SelLength = Len(sdbcCompCod.Text)
    sdbcCompCod.Refresh
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcCompCod_InitColumnProps()
    sdbcCompCod.DataFieldList = "Column 0"
    sdbcCompCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCompCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompCod.Rows - 1
            bm = sdbcCompCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcCompCod_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If sdbcCompCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el comprador
       
    Screen.MousePointer = vbHourglass
    oEqpSeleccionado.CargarTodosLosCompradores sdbcCompCod.Text, , , True
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompCod.Text = ""
    Else
        RespetarCombo = True
        sdbcCompDen.Text = oCompradores.Item(1).Apel & ", " & oCompradores.Item(1).nombre
        sdbcCompCod.Columns(0).Value = sdbcCompCod.Text
        sdbcCompCod.Columns(1).Value = sdbcCompDen.Text
        RespetarCombo = False
        
        Set oCompSeleccionado = oCompradores.Item(1)
        CargarComboDesdeComp = False
        'CompradorSeleccionado
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcCompDen_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompCod.Text = ""
        RespetarCombo = False
        
        CargarComboDesdeComp = True
        Set oCompSeleccionado = Nothing
               
        
    End If
    
End Sub


Private Sub sdbcCompDen_Click()
    If Not sdbcCompDen.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
    End If
End Sub

Private Sub sdbcCompDen_CloseUp()
    Dim sCod As String
    
    If sdbcCompDen.Value = "..." Then
        sdbcCompDen.Text = ""
        Exit Sub
    End If
    
    If sdbcCompDen.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompDen.Columns(0).Text
    sdbcCompCod.Text = sdbcCompDen.Columns(1).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    
    Set oCompSeleccionado = oComps.Item(sCod & sdbcCompDen.Columns(1).Text)
    Screen.MousePointer = vbNormal
        
    CargarComboDesdeComp = False
    
    DoEvents
    
    'CompradorSeleccionado
       
End Sub

Private Sub sdbcCompDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Dim EqpCod As String
    
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    Set oComps = Nothing
    Set oComps = oFSGSRaiz.generar_CCompradores
      
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(Apellidos(sdbcEqpDen.Text)), True, False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, True, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    Set oEqpSeleccionado.Compradores = Nothing
        
    For i = 0 To UBound(Codigos.Cod) - 1
            sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompDen.AddItem "..."
    End If

    sdbcCompDen.SelStart = 0
    sdbcCompDen.SelLength = Len(sdbcCompDen.Text)
    sdbcCompDen.Refresh
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcCompDen_InitColumnProps()
    sdbcCompDen.DataFieldList = "Column 0"
    sdbcCompDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCompDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompDen.Rows - 1
            bm = sdbcCompDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcCompDen_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If sdbcCompDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
      
    Screen.MousePointer = vbHourglass
    oEqpSeleccionado.CargarTodosLosCompradores , nombre(sdbcCompDen.Text), Apellidos(sdbcCompDen.Text), True, False, False, False, True
    Set oCompradores = oEqpSeleccionado.Compradores
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompDen.Text = ""
    Else
        RespetarCombo = True
        sdbcCompCod.Text = oCompradores.Item(1).Cod
        sdbcCompDen.Columns(0).Value = sdbcCompDen.Text
        sdbcCompDen.Columns(1).Value = sdbcCompCod.Text
        RespetarCombo = False
        
        Set oCompSeleccionado = oCompradores.Item(1)
        CargarComboDesdeComp = False
        'CompradorSeleccionado
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpCod_Change()
     
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        RespetarCombo = False
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
        sdbcCompCod.Enabled = True
        sdbcCompDen.Enabled = True
        
    End If
    
End Sub
Private Sub sdbcEqpCod_CloseUp()
    
    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpCod.Value = "" Then Exit Sub
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
            
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    RespetarCombo = False
        
    Screen.MousePointer = vbHourglass
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(0).Text)
    Screen.MousePointer = vbNormal
    
    CargarComboDesdeEqp = False

End Sub
Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
       
    sdbcEqpCod.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcEqpCod_InitColumnProps()

    sdbcEqpCod.DataFieldList = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcEqpCod_Click()
    
    If Not sdbcEqpCod.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpCod.Rows - 1
            bm = sdbcEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        
    Else
        RespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        sdbcEqpCod.Columns(0).Value = sdbcEqpCod.Text
        sdbcEqpCod.Columns(1).Value = sdbcEqpDen.Text
        RespetarCombo = False
        
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
        sdbcCompCod.Enabled = True
        sdbcCompDen.Enabled = True
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_Change()
      
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpCod.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
                
    End If
          
End Sub

Private Sub sdbcEqpDen_Click()
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpDen_CloseUp()
    
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
        
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    RespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(1).Text)
    Screen.MousePointer = vbNormal
    
    CargarComboDesdeEqp = False
        
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpDen.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpDen.Rows - 1
            bm = sdbcEqpDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
    Else
        RespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        sdbcEqpDen.Columns(0).Value = sdbcEqpDen.Text
        sdbcEqpDen.Columns(1).Value = sdbcEqpCod.Text
        RespetarCombo = False
        
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
        sdbcCompCod.Enabled = True
        sdbcCompDen.Enabled = True
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcEst_Validate(Cancel As Boolean)
    
    If sdbcEst.Text <> "" Then
        sdbcEst.Text = sdbcEst.Columns(0).Text
    End If
    
End Sub

Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub

Private Sub cmdSelMat_Click()
    frmSELMAT.sOrigen = "frmLstREU"
    frmSELMAT.bRComprador = m_bRMat
    frmSELMAT.bRCompResponsable = m_bRCompResp
    frmSELMAT.bRUsuAper = m_bRUsuAper
    frmSELMAT.Show 1
End Sub

Private Sub Form_Load()

    ConfigurarSeguridad
    CargarRecursos
    
    PonerFieldSeparator Me

    Me.Height = 6810
    Me.Width = 8430
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    lblSolicitud = gParametrosGenerales.gsDenSolicitudCompra & ":"
    lblGMN1_4.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    
    If m_bREqpAsig Then
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " " & oUsuarioSummit.comprador.DenEqp
        sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
        Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
        oEqpSeleccionado.Cod = basOptimizacion.gCodEqpUsuario
        sdbcCompCod.Enabled = True
        sdbcCompDen.Enabled = True
        optComp.Enabled = True
        optResp.Enabled = True
   End If
    
   If m_bRAsig Or m_bRCompResp Then
        sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
        sdbcCompCod.Text = basOptimizacion.gCodCompradorUsuario
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        sdbcCompCod.Visible = False
        sdbcCompDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " " & oUsuarioSummit.comprador.DenEqp
        LblComp.Visible = True
        LblComp.caption = basOptimizacion.gCodCompradorUsuario & " " & oUsuarioSummit.comprador.Apel & "," & oUsuarioSummit.comprador.nombre
        optComp.Visible = False
        optResp.Visible = False
    End If
    
    CargarEstados
    CargarAnyos
        
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
 
    txtFecDesde = DateAdd("m", -1, Date)
    txtFecHasta = DateAdd("m", 6, Date)

End Sub
Private Sub sdbcEst_Change()
    
    DesdeEst = validado
    HastaEst = Cerrado
    
End Sub

Private Sub sdbcEst_InitColumnProps()
  
    sdbcEst.DataFieldList = "Column 0"
    sdbcEst.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_Change()
    
    If Not bRespetarCombo Then
    
        bCargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sGMN1Cod = "": sGMN2Cod = "": sGMN3Cod = "": sGMN4Cod = ""
        txtEstMat = ""

    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    bRespetarCombo = False
    
    GMN1Seleccionado
    bCargarComboDesde = False
    
    'Set oProcesoSeleccionado = Nothing
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
 

 
    If bCargarComboDesde Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
    
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
       
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bCargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    sGMN1Cod = "": sGMN2Cod = "": sGMN3Cod = "": sGMN4Cod = ""
    txtEstMat = ""
    
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
     
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False, , m_bRUsuAper, m_bRCompResp, basOptimizacion.gCodPersonaUsuario)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
'            oMensajes.NoValido "Material"
            oMensajes.NoValido sIdiMaterial
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
            sGMN1Cod = oGMN1Seleccionado.Cod
            bCargarComboDesde = False
            
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
   
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
'            oMensajes.NoValido "Material"
            oMensajes.NoValido sIdiMaterial
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
            sGMN1Cod = oGMN1Seleccionado.Cod
      
            bCargarComboDesde = False
            
        End If
    
    End If
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Public Sub PonerMatSeleccionado()

    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod = oGMN1Seleccionado.Cod
        sGMN1Cod = oGMN1Seleccionado.Cod
        txtEstMat = sGMN1Cod
    Else
         'sGMN1Cod = ""
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
    
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
End Sub

Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
 
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    sGMN1Cod = oGMN1Seleccionado.Cod
    sGMN2Cod = "": sGMN3Cod = "": sGMN4Cod = ""
    txtEstMat = ""
    
     
    
    
End Sub
Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    sdbcAnyo.AddItem ""
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = ""
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub txtFecDesde_Validate(Cancel As Boolean)
    
    If txtFecDesde <> "" Then
        If Not IsDate(txtFecDesde) Then
'            oMensajes.NoValida " fecha "
            oMensajes.NoValido sIdiFecha
            Cancel = True
        End If
    End If
End Sub

Private Sub txtFecHasta_Validate(Cancel As Boolean)
    If txtFecHasta <> "" Then
        If Not IsDate(txtFecHasta) Then
'            oMensajes.NoValida " fecha "
            oMensajes.NoValido sIdiFecha
            Cancel = True
        End If
    End If
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 14/01/2013</revision>

Private Sub ConfigurarSeguridad()
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestMat)) Is Nothing) Then
        m_bRMat = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestAsignado)) Is Nothing) Then
        m_bRAsig = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestResponsable)) Is Nothing) Then
        m_bRCompResp = True
    End If
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestEqpAsignado)) Is Nothing) Then
        m_bREqpAsig = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestUsuAper)) Is Nothing) Then
        m_bRUsuAper = True
    End If
    m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestUsuUON)) Is Nothing)
    m_bRUsuDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestUsuDep)) Is Nothing)
    m_bRPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestPerfUON)) Is Nothing)
End Sub

''' <summary>Obtiene el listado de reuniones</summary>
''' <remarks>Llamada desde: cmdObtener_Click</remarks>
''' <revision>LTG 14/01/2013</revision>

Private Sub ObtenerListadoReuniones()
    Dim oReport As Object
    Dim oSubReport As Object
    Dim oCRReunion As CRReunion
    Dim pv As Preview
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim FormulaSel As String
    Dim FormulaVPER As String
    Dim FormulaVRESP As String
    Dim lIdPerfil As Long

    Screen.MousePointer = vbHourglass
    
    Set oCRReunion = GenerarCRReunion
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptREU.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Set oFos = Nothing


    'Selecci�n que se lista, formula 1 - @SEL
    If m_sSeleccion = "" Then
        FormulaSel = ""
    Else
        FormulaSel = sIdiSeleccion & ": " & Trim(m_sSeleccion)
    End If
    
    FormulaVPER = "N"
    If chkPer = vbChecked Then
        FormulaVPER = "S"
    End If
    FormulaVRESP = "N"
    If chkResp = vbChecked Then
        FormulaVRESP = "S"
    End If
            
    'SE HABRE EL REPORT
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "VPER")).Text = """" & FormulaVPER & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "VRESP")).Text = """" & FormulaVRESP & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & srIdiRTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtREFERENCIA")).Text = """" & srIdiRReferencia & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESOS")).Text = """" & srIdiRProcesos & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtHORA")).Text = """" & srIdiRHora & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRESPONSABLE")).Text = """" & srIdiRResponsable & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCODIGO")).Text = """" & srIdiRCodigo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTELEFONO")).Text = """" & srIdiRTelefono & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEMAIL")).Text = """" & srIdiREMail & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDeci")).Text = """" & stxtDeci & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNoDeci")).Text = """" & stxtNoDeci & """"
    
    Set oSubReport = oReport.OpenSubreport("REU_PERSONAS")
    
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtIMPLICADAS")).Text = """" & srIdiRSImplicadas & """"
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtROL")).Text = """" & srIdiRSRol & """"
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtCODIGO")).Text = """" & srIdiRSCodigo & """"
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtAPELNOM")).Text = """" & srIdiRSApelNom & """"
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtUO")).Text = """" & srIdiRSUO & """"
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtDEPT")).Text = """" & srIdiRSDept & """"
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtTELEFONO")).Text = """" & srIdiRSTelefono & """"
    oSubReport.FormulaFields(crs_FormulaIndex(oSubReport, "txtEMAIL")).Text = """" & srIdiRSEMail & """"
    
    Set oSubReport = Nothing
    Dim sDesde As String
    Dim sHasta As String
    
    If txtHoraDesde <> "" Then
        sDesde = txtFecDesde & " " & txtHoraDesde
    Else
        sDesde = txtFecDesde
    End If
    If txtHoraHasta <> "" Then
        sHasta = txtFecHasta & " " & txtHoraHasta
    Else
        sHasta = txtFecHasta
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
    Dim rstUONsPerf As ADODB.Recordset
    If m_bRPerfUON Then
        If lIdPerfil > 0 Then
            Dim oPerfil As CPerfil
            Set oPerfil = oFSGSRaiz.Generar_CPerfil
            oPerfil.Id = lIdPerfil
            Set rstUONsPerf = oPerfil.DevolverUONsPerfil
            Set oPerfil = Nothing
        End If
    End If
    If Not oCRReunion.ListadoREU(oGestorInformes, oReport, sDesde, sHasta, val(sdbcAnyo.Text), sGMN1Cod, txtCod, txtDen, txtSolicitud, DesdeEst, HastaEst, sGMN2Cod, sGMN3Cod, _
                                 sGMN4Cod, optComp, sdbcEqpCod, sdbcCompCod, opOrdDen, chkPer, chkResp, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, _
                                 m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, m_bRPerfUON, lIdPerfil, rstUONsPerf) Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiGenerando1
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    pv.caption = sIdiTitulo1
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    

End Sub

''' <summary>Obtiene el listado de planificaci�n</summary>
''' <remarks>Llamada desde: cmdObtener_Click</remarks>
''' <revision>LTG 14/01/2013</revision>

Private Sub ObtenerListadoPlanReuniones()
    Dim oReport As Object
    Dim oCRReunion As CRReunion
    Dim RepPath As String
    Dim pv As Preview
    Dim oFos As FileSystemObject
    Dim FormulaSel As String
    Dim FormulaVRESP As String
    Dim i As Integer
    Dim lIdPerfil As Long
    
    Screen.MousePointer = vbHourglass
    
    Set oCRReunion = GenerarCRReunion
        
    If HastaEst > 10 Then HastaEst = 11
    
    If chkSinPlan = vbUnchecked And chkConPlan = vbUnchecked Then
        chkSinPlan = vbChecked
        chkConPlan = vbChecked
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptREUPlan.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    Set oFos = Nothing

    'Selecci�n que se lista, formula 1 - @SEL
    
    If m_sSeleccion = "" Then
        FormulaSel = ""
    Else
        FormulaSel = sIdiSeleccion & ": " & Trim(m_sSeleccion)
    End If
         
    FormulaVRESP = "N"
    If chkResp = vbChecked Then
        FormulaVRESP = "S"
    End If
    
    'SE HABRE EL REPORT
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "VRESP")).Text = """" & FormulaVRESP & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTITULO")).Text = """" & srIdiTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDENOMINACION")).Text = """" & srIdiDenominacion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtESTADO")).Text = """" & srIdiEstado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAPERTURA")).Text = """" & srIdiApertura & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNECESIDAD")).Text = """" & srIdiNecesidad & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLIMITEOFE")).Text = """" & srIdiLimiteOfe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtULTREUNION")).Text = """" & srIdiUltReunion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPLANIFICADA")).Text = """" & srIdiPlanificada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDECISION")).Text = """" & srIdiDecision & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRESPONSABLE")).Text = """" & srIdiResponsable & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTELEFONO")).Text = """" & srIdiTelefono & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEMAIL")).Text = """" & srIdiEMail & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    
    i = 1
    
    For i = 1 To 10
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEst" & CStr(i))).Text = """" & sIdiEstados(i) & """"
    Next

    Dim sDesde As String
    Dim sHasta As String
    
    If txtHoraDesde <> "" Then
        sDesde = txtFecDesde & " " & txtHoraDesde
    Else
        sDesde = txtFecDesde
    End If
    If txtHoraHasta <> "" Then
        sHasta = txtFecHasta & " " & txtHoraHasta
    Else
        sHasta = txtFecHasta
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    Dim rstUONsPerf As ADODB.Recordset
    If m_bRPerfUON Then
        If lIdPerfil > 0 Then
            Dim oPerfil As CPerfil
            Set oPerfil = oFSGSRaiz.Generar_CPerfil
            oPerfil.Id = lIdPerfil
            Set rstUONsPerf = oPerfil.DevolverUONsPerfil
            Set oPerfil = Nothing
        End If
    End If
    If Not oCRReunion.ListadoREUPlan(oGestorInformes, oReport, sDesde, sHasta, val(sdbcAnyo.Text), sGMN1Cod, txtCod, txtDen, txtSolicitud, DesdeEst, HastaEst, _
                                     sGMN2Cod, sGMN3Cod, sGMN4Cod, optComp, sdbcEqpCod, sdbcCompCod, opOrdDen, chkSinPlan, chkConPlan, chkResp, _
                                     m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, m_bRPerfUON, lIdPerfil, rstUONsPerf) Then
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiGenerando2
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    pv.caption = sIdiTitulo2
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
End Sub



Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTREU, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
'''        caption = Ador(0).Value '1    A partir de ahora se coger� del PARGEN_LIT
        Ador.MoveNext
        sstabProce.TabCaption(0) = Ador(0).Value
        sIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        sstabProce.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        fraFecha.caption = Ador(0).Value
        Ador.MoveNext
        lblFecReu.caption = Ador(0).Value '5
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value
        stxtDesde = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        stxtHasta = Ador(0).Value
        Ador.MoveNext
        fraDatosGenerales.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value
        'Ador.MoveNext
        'lblGMN1_4.Caption = Ador(0).Value '10
        Ador.MoveNext
        lblCod.caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value
        Ador.MoveNext
        lblSolicitud.caption = Ador(0).Value
        Ador.MoveNext
        lblEst.caption = Ador(0).Value
        stxtEstado = Ador(0).Value
        Ador.MoveNext
        
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value '15
        sdbcEqpCod.Columns(0).caption = Ador(0).Value
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        sdbcCompCod.Columns(0).caption = Ador(0).Value
        sdbcCompDen.Columns(1).caption = Ador(0).Value
        
        Ador.MoveNext
        
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value
        sdbcEst.Columns(0).caption = Ador(0).Value
        sdbcEqpCod.Columns(1).caption = Ador(0).Value
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        
        
        fraMat.caption = Ador(0).Value '17
        Ador.MoveNext
        fraComprador.caption = Ador(0).Value
        Ador.MoveNext
        optComp.caption = Ador(0).Value
        Ador.MoveNext
        optResp.caption = Ador(0).Value '20
        Ador.MoveNext
        lblEqpCod.caption = Ador(0).Value
        Ador.MoveNext
        lblCompCod.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        
        sdbcCompCod.Columns(1).caption = Ador(0).Value
        sdbcCompDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        
        fraOpciones.caption = Ador(0).Value '25
        Ador.MoveNext
        opLisPlan.caption = Ador(0).Value
        Ador.MoveNext
        chkSinPlan.caption = Ador(0).Value
        Ador.MoveNext
        chkConPlan.caption = Ador(0).Value
        Ador.MoveNext
        
'''        opLisReu.caption = Ador(0).Value    A partir de ahora se coger� del PARGEN_LIT
        Ador.MoveNext
        chkPer.caption = Ador(0).Value '30
        Ador.MoveNext
        
        chkResp.caption = Ador(0).Value
        Ador.MoveNext
        fraOrden.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        stxtCod = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value
        stxtDen = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value '35
        Ador.MoveNext
        
        sIdiEstados(1) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(2) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(3) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(4) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(5) = Ador(0).Value '40
        Ador.MoveNext
        sIdiEstados(6) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(7) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(8) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(9) = Ador(0).Value
        Ador.MoveNext
        sIdiEstados(10) = Ador(0).Value '45
        Ador.MoveNext
        sIdiEstados(11) = Ador(0).Value
        Ador.MoveNext
        sIdiMaterial = Ador(0).Value
        Ador.MoveNext
        sIdiFecha = Ador(0).Value
        'Ador.MoveNext
        'sIdiSeleccion = Ador(0).Value
        'Ador.MoveNext
        'sIdiTodosProce = Ador(0).Value
        Ador.MoveNext
'''        sIdiTitulo1 = Ador(0).Value '50
        Ador.MoveNext
        sIdiTitulo2 = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando1 = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando2 = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value '55
        Ador.MoveNext
        stxtAnio = Ador(0).Value '56
        Ador.MoveNext
        stxtCompRes = Ador(0).Value
        Ador.MoveNext
        stxtEqpRes = Ador(0).Value
        Ador.MoveNext
        stxtCompAs = Ador(0).Value
        Ador.MoveNext
        stxtEqpAs = Ador(0).Value
        Ador.MoveNext
        stxtDeci = Ador(0).Value
        Ador.MoveNext
        stxtNoDeci = Ador(0).Value
        'idioma del report
        Ador.MoveNext
        srIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiProceso = Ador(0).Value
        Ador.MoveNext
        srIdiDenominacion = Ador(0).Value
        Ador.MoveNext
        srIdiEstado = Ador(0).Value
        Ador.MoveNext
        srIdiApertura = Ador(0).Value
        Ador.MoveNext
        srIdiNecesidad = Ador(0).Value
        Ador.MoveNext
        srIdiLimiteOfe = Ador(0).Value
        Ador.MoveNext
        srIdiUltReunion = Ador(0).Value
        Ador.MoveNext
        srIdiPlanificada = Ador(0).Value
        Ador.MoveNext
        srIdiDecision = Ador(0).Value
        Ador.MoveNext
        srIdiResponsable = Ador(0).Value
        Ador.MoveNext
        srIdiTelefono = Ador(0).Value
        Ador.MoveNext
        srIdiEMail = Ador(0).Value
        Ador.MoveNext
        
'''        srIdiRTitulo = Ador(0).Value  A partir de ahora se coger� del PARGEN_LIT
        Ador.MoveNext
        srIdiRReferencia = Ador(0).Value
        Ador.MoveNext
        srIdiRProcesos = Ador(0).Value
        Ador.MoveNext
        srIdiRHora = Ador(0).Value
        lblHoraDesde.caption = Ador(0).Value
        lblHoraHasta.caption = Ador(0).Value
        
        Ador.MoveNext
        srIdiRResponsable = Ador(0).Value
        Ador.MoveNext
        srIdiRCodigo = Ador(0).Value
        Ador.MoveNext
        srIdiRTelefono = Ador(0).Value
        Ador.MoveNext
        srIdiREMail = Ador(0).Value
        Ador.MoveNext
        srIdiRSImplicadas = Ador(0).Value
        Ador.MoveNext
        srIdiRSRol = Ador(0).Value
        Ador.MoveNext
        srIdiRSCodigo = Ador(0).Value
        Ador.MoveNext
        srIdiRSApelNom = Ador(0).Value
        Ador.MoveNext
        srIdiRSUO = Ador(0).Value
        Ador.MoveNext
        srIdiRSDept = Ador(0).Value
        Ador.MoveNext
        srIdiRSTelefono = Ador(0).Value
        Ador.MoveNext
        srIdiRSEMail = Ador(0).Value
        Ador.MoveNext
        
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.Close
        
    
    End If


   Set Ador = Nothing
   
    'PARGEN_LIT
    Dim oLiterales As CLiterales
    Set oLiterales = oGestorParametros.DevolverLiterales(53, 54, basPublic.gParametrosInstalacion.gIdioma)
    Me.caption = oLiterales.Item(1).Den
    srIdiRTitulo = oLiterales.Item(2).Den
    opLisReu.caption = oLiterales.Item(2).Den
    sIdiTitulo1 = oLiterales.Item(2).Den


End Sub

Private Function GenerarTextoSeleccion() As String
Dim sSeleccion As String
Dim sUnion As String

    sSeleccion = ""
    sUnion = ""
    
    Dim sDesde As String
    Dim sHasta As String
    
    If txtHoraDesde <> "" Then
        sDesde = txtFecDesde & " " & txtHoraDesde
    Else
        sDesde = txtFecDesde
    End If
    If txtHoraHasta <> "" Then
        sHasta = txtFecHasta & " " & txtHoraHasta
    Else
        sHasta = txtFecHasta
    End If
    
    
    
    If IsDate(sDesde) Then
            sSeleccion = stxtDesde & " " & sDesde
    End If
    
    
    If IsDate(sHasta) Then
            If sSeleccion <> "" Then sUnion = "; "
            sSeleccion = sSeleccion & sUnion & stxtHasta & " " & sHasta
    End If
    
    If val(sdbcAnyo) <> 0 Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtAnio & ": " & Format(val(sdbcAnyo), "0000")
    End If
    
    If sdbcEst <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        Select Case sdbcEst.Columns("COD").Value
            Case 1
                'Pendiente de asignar proveedores
                DesdeEst = validado
                HastaEst = Conproveasignados
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(1)

            Case 2
                'Pendiente de enviar peticiones
                DesdeEst = conasignacionvalida
                HastaEst = conasignacionvalida
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(2)
                
            Case 3
                ' en rec ofertas
                DesdeEst = conpeticiones
                HastaEst = conofertas
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(3)
            
            Case 4
                'pendiente de comunicar objetivos
                DesdeEst = ConObjetivosSinNotificar
                HastaEst = ConObjetivosSinNotificarYPreadjudicado
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(4)
                    
            Case 5
                'Pendiente de adjudicar
                DesdeEst = PreadjYConObjNotificados
                HastaEst = PreadjYConObjNotificados
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(5)
                
            Case 6
            
                DesdeEst = ParcialmenteCerrado
                HastaEst = ParcialmenteCerrado
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(6)
            
            Case 7
                'Pendiente de notificar adjudicaciones
                DesdeEst = conadjudicaciones
                HastaEst = conadjudicaciones
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(7)
                
                
            Case 8
                'Notificado
                DesdeEst = ConAdjudicacionesNotificadas
                HastaEst = ConAdjudicacionesNotificadas
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(8)
                    
            Case 9
                'Anulado
                DesdeEst = Cerrado
                HastaEst = Cerrado
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(9)
    
            Case 10
                'Cerrados
                DesdeEst = conadjudicaciones
                HastaEst = ConAdjudicacionesNotificadas
                sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(10)
                
            Case 11
                'Abiertos
                DesdeEst = validado
                HastaEst = PreadjYConObjNotificados
                    sSeleccion = sSeleccion & sUnion & stxtEstado & " " & sIdiEstados(11)
           
        End Select
    
    Else
        If sdbcEst.Enabled = True Then
            DesdeEst = validado
            HastaEst = Cerrado
        End If
    End If
    
    If sSeleccion <> "" Then sUnion = "; "
    If Trim(sdbcEqpCod) <> "" Then
     If optResp Then
        
        If Trim(sdbcCompCod) <> "" Then
            sSeleccion = sSeleccion & sUnion & stxtCompRes & " " & Trim(sdbcEqpCod) & " - " & Trim(sdbcCompCod)
        Else
            sSeleccion = sSeleccion & sUnion & stxtEqpRes & " " & Trim(sdbcEqpCod)
        End If
     Else
        If Trim(sdbcCompCod) <> "" Then
            sSeleccion = sSeleccion & sUnion & stxtCompAs & " " & Trim(sdbcEqpCod) & " - " & Trim(sdbcCompCod)
        Else
            sSeleccion = sSeleccion & sUnion & stxtEqpAs & " " & Trim(sdbcEqpCod)
        End If
     End If
    End If
    
    If sGMN4Cod <> "" Or sGMN3Cod <> "" Or sGMN2Cod <> "" Or sGMN1Cod <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        If txtEstMat <> "" Then
            sSeleccion = sSeleccion & sUnion & sIdiMaterial & ": " & txtEstMat
        Else
            sSeleccion = sSeleccion & sUnion & sIdiMaterial & ": " & sGMN1Cod
        End If
    End If

    If sSeleccion <> "" Then sUnion = "; "
    If Not (val(txtCod) = 0) Then
        sSeleccion = sSeleccion & sUnion & stxtCod & ": " & txtCod
    Else
        If txtDen <> "" Then
            sSeleccion = sSeleccion & sUnion & stxtDen & ": '" & txtDen & "'"
        End If
    End If
    
    If sSeleccion <> "" Then sUnion = "; "
    If txtSolicitud <> "" Then
            sSeleccion = sSeleccion & sUnion & gParametrosGenerales.gsDenSolicitudCompra & ": " & txtSolicitud
    End If

    GenerarTextoSeleccion = sSeleccion

End Function






