VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSelCContables 
   BackColor       =   &H00808000&
   Caption         =   "frmSelCContables"
   ClientHeight    =   9045
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8505
   Icon            =   "frmSelCContables.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9045
   ScaleMode       =   0  'User
   ScaleWidth      =   8724.327
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   8070
      Picture         =   "frmSelCContables.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Cargar"
      Top             =   1320
      Width           =   336
   End
   Begin VB.CommandButton cmdSeleccion 
      Height          =   315
      Left            =   8070
      Picture         =   "frmSelCContables.frx":01D6
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   150
      Width           =   336
   End
   Begin VB.TextBox txtSeleccion 
      Height          =   285
      Left            =   1590
      TabIndex        =   0
      Top             =   150
      Width           =   6435
   End
   Begin VB.CheckBox chkNoVigentes 
      BackColor       =   &H00808000&
      Caption         =   "Ver no vigentes"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   6600
      TabIndex        =   9
      Top             =   1320
      Value           =   1  'Checked
      Width           =   1800
   End
   Begin VB.CommandButton cmdCalDesde 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2820
      Picture         =   "frmSelCContables.frx":0227
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   1320
      Width           =   336
   End
   Begin VB.TextBox txtFecDesde 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1590
      TabIndex        =   5
      Top             =   1320
      Width           =   1170
   End
   Begin VB.CommandButton cmdCalHasta 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   5670
      Picture         =   "frmSelCContables.frx":07B1
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Mantenimiento"
      Top             =   1320
      Width           =   336
   End
   Begin VB.TextBox txtFecHasta 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   4440
      TabIndex        =   7
      Top             =   1320
      Width           =   1170
   End
   Begin VB.TextBox txtCenCoste 
      Height          =   285
      Left            =   1590
      TabIndex        =   2
      Top             =   540
      Width           =   6435
   End
   Begin VB.CommandButton cmdSelCCoste 
      Height          =   315
      Left            =   8070
      Picture         =   "frmSelCContables.frx":0D3B
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   540
      Width           =   336
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "cmdCancelar"
      Height          =   315
      Left            =   4470
      TabIndex        =   13
      Top             =   8640
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "cmdAceptar"
      Height          =   315
      Left            =   3420
      TabIndex        =   12
      Top             =   8640
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwestrCC 
      Height          =   6765
      Left            =   60
      TabIndex        =   11
      Top             =   1770
      Width           =   8385
      _ExtentX        =   14790
      _ExtentY        =   11933
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   1350
      Top             =   2250
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCContables.frx":0DA7
            Key             =   "PP"
            Object.Tag             =   "PP"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCContables.frx":11F9
            Key             =   "CCC"
            Object.Tag             =   "CCC"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCContables.frx":1852
            Key             =   "CC3"
            Object.Tag             =   "CC3"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCContables.frx":18E4
            Key             =   "CC4"
            Object.Tag             =   "CC4"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCContables.frx":1977
            Key             =   "CC2"
            Object.Tag             =   "CC2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCContables.frx":1A27
            Key             =   "CC1"
            Object.Tag             =   "CC1"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCContables.frx":1AD7
            Key             =   "RAIZ"
            Object.Tag             =   "RAIZ"
         EndProperty
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcGestor 
      Height          =   285
      Left            =   1590
      TabIndex        =   4
      Top             =   930
      Width           =   6435
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2540
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6482
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   11351
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblGestor 
      BackStyle       =   0  'Transparent
      Caption         =   "DGestor:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   18
      Top             =   960
      Width           =   2085
   End
   Begin VB.Label lblSeleccion 
      BackStyle       =   0  'Transparent
      Caption         =   "DSeleccion"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   17
      Top             =   180
      Width           =   1185
   End
   Begin VB.Label lblFecDesde 
      BackStyle       =   0  'Transparent
      Caption         =   "DFecha inicio desde :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   150
      TabIndex        =   16
      Top             =   1350
      Width           =   1785
   End
   Begin VB.Label lblFecHasta 
      BackStyle       =   0  'Transparent
      Caption         =   "DFecha fin hasta :"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   3180
      TabIndex        =   15
      Top             =   1350
      Width           =   1575
   End
   Begin VB.Label lblCenCoste 
      BackStyle       =   0  'Transparent
      Caption         =   "DFiltro por centro de coste : "
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   14
      Top             =   570
      Width           =   2085
   End
End
Attribute VB_Name = "frmSelCContables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnVSep As Integer = 105

Public g_sOrigen As String
Public g_sPres0 As String
Public g_oOrigen As Form
Public m_sCadenaErrorNodo As String
Public g_lIdEmpresa As Long
Public g_iNivel As Integer
Private m_oCentrosCoste As CCentrosCoste
Public g_oCenCos As CCentroCoste
Public g_oPartida As CContratopres
Private m_bCenValidado As Boolean
Private m_bDobleClic As Boolean
Public g_bImputaPedido As Boolean
Public g_oGestor As CPersona

' Unidades organizativas
Dim oUnidadesOrg As CUnidadesOrgNivel1

Private m_lNodo As Long
Private nodx As node

Public m_bDescargarFrm As Boolean

Dim m_bActivado As Boolean

Private m_bUnload As Boolean

Private m_sMsgError As String

''' Se acepta una partida seleccionada en el �rbol de partidas.
Private Sub cmdAceptar_Click()

Dim nodx As MSComctlLib.node
Dim scodCC1 As String
Dim scodCC2 As String
Dim scodCC3 As String
Dim scodCC4 As String
Dim scodPP1 As String
Dim scodPP2 As String
Dim scodPP3 As String
Dim scodPP4 As String
Dim irespuesta As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrCC.selectedItem
                  
    If nodx Is Nothing Then
        oMensajes.DebeElegirUnNodoPresupuestario m_sCadenaErrorNodo
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    If Left(nodx.Tag, 4) = "Raiz" Then
        If Me.Visible Then tvwestrCC.SetFocus
        m_bDobleClic = False
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If nodx.Children <> 0 Then
        If Not m_bDobleClic Then
            oMensajes.DebeElegirUnNodoPresupuestario m_sCadenaErrorNodo
        End If
        m_bDobleClic = False
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Set g_oPartida = oFSGSRaiz.Generar_CContratoPres
    
    g_oPartida.Pres0 = g_sPres0
    
    If g_oCenCos Is Nothing Then
        Set g_oCenCos = oFSGSRaiz.Generar_CCentroCoste
    End If
    
    If Not nodx Is Nothing Then
        
        Screen.MousePointer = vbHourglass
        Select Case Left(nodx.Tag, 4)

            Case "CC1-"
                If Not m_bDobleClic Then
                    oMensajes.DebeElegirUnNodoPresupuestario m_sCadenaErrorNodo
                End If
                m_bDobleClic = False
                Screen.MousePointer = vbNormal
                Exit Sub
                
            Case "CC2-"
                If Not m_bDobleClic Then
                    oMensajes.DebeElegirUnNodoPresupuestario m_sCadenaErrorNodo
                End If
                m_bDobleClic = False
                Screen.MousePointer = vbNormal
                Exit Sub
                
            Case "CC3-"
                If Not m_bDobleClic Then
                    oMensajes.DebeElegirUnNodoPresupuestario m_sCadenaErrorNodo
                End If
                m_bDobleClic = False
                Screen.MousePointer = vbNormal
                Exit Sub
                
            Case "CC4-"
                If Not m_bDobleClic Then
                    oMensajes.DebeElegirUnNodoPresupuestario m_sCadenaErrorNodo
                End If
                m_bDobleClic = False
                Screen.MousePointer = vbNormal
                Exit Sub
        
            Case "P11-"
                 scodCC1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = ""
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).Den
                 g_oPartida.Pres1 = DevolverCod(nodx)
                 g_oPartida.Pres2 = ""
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).Gestor
    
            Case "P12-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = ""
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx)
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent)))
                 scodPP2 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Gestor
                 
            Case "P13-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = ""
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx)
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent)))
                 scodPP3 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodCC1 & scodPP1 & scodPP2 & scodPP3).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodCC1 & scodPP1 & scodPP2 & scodPP3).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodCC1 & scodPP1 & scodPP2 & scodPP3).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodCC1 & scodPP1 & scodPP2 & scodPP3).Gestor
                 
            Case "P14-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = ""
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres4 = DevolverCod(nodx)
                 scodPP1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP3 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx.Parent)))
                 scodPP4 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Gestor
                 
            Case "P21-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Den
                 g_oPartida.Pres1 = DevolverCod(nodx)
                 g_oPartida.Pres2 = ""
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).Gestor
                 
            Case "P22-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx)
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent)))
                 scodPP2 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Gestor
                 
            Case "P23-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx)
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent)))
                 scodPP3 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).Gestor
                 
            Case "P24-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = ""
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres4 = DevolverCod(nodx)
                 scodPP1 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP3 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx.Parent)))
                 scodPP4 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Gestor
                 
            Case "P31-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Den
                 g_oPartida.Pres1 = DevolverCod(nodx)
                 g_oPartida.Pres2 = ""
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).Gestor
                 
            Case "P32-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx)
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent)))
                 scodPP2 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Gestor
                 
            Case "P33-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx)
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent)))
                 scodPP3 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).Gestor
                 
            Case "P34-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = ""
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres4 = DevolverCod(nodx)
                 scodPP1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP3 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx.Parent)))
                 scodPP4 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Gestor
                 
            Case "P41-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodCC4 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(DevolverCod(nodx.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Den
                 g_oPartida.Pres1 = DevolverCod(nodx)
                 g_oPartida.Pres2 = ""
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).Gestor
                 
            Case "P42-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodCC4 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(DevolverCod(nodx.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx)
                 g_oPartida.Pres3 = ""
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent)))
                 scodPP2 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).Gestor
                 
            Case "P43-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 scodCC4 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx)
                 g_oPartida.Pres4 = ""
                 scodPP1 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent)))
                 scodPP3 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).Gestor
                 
            Case "P44-"
                 scodCC1 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent.Parent)))
                 scodCC2 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent.Parent)))
                 scodCC3 = DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent.Parent)))
                 scodCC4 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
                 g_oCenCos.UON1 = oUnidadesOrg.Item(scodCC1).Cod
                 g_oCenCos.UON2 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).Cod
                 g_oCenCos.UON3 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).Cod
                 g_oCenCos.UON4 = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Cod = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Cod
                 g_oCenCos.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).Den
                 g_oPartida.Pres1 = DevolverCod(nodx.Parent.Parent.Parent)
                 g_oPartida.Pres2 = DevolverCod(nodx.Parent.Parent)
                 g_oPartida.Pres3 = DevolverCod(nodx.Parent)
                 g_oPartida.Pres4 = DevolverCod(nodx)
                 scodPP1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                 scodPP2 = DevolverCod(nodx.Parent.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(DevolverCod(nodx.Parent.Parent)))
                 scodPP3 = DevolverCod(nodx.Parent) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(DevolverCod(nodx.Parent)))
                 scodPP4 = DevolverCod(nodx) & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(DevolverCod(nodx)))
                 g_oPartida.Den = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Den
                 g_oPartida.FecIni = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecIni
                 g_oPartida.FecFin = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).FecFin
                 g_oPartida.Gestor = oUnidadesOrg.Item(scodCC1).UnidadesOrgNivel2.Item(scodCC1 & scodCC2).UnidadesOrgNivel3.Item(scodCC1 & scodCC2 & scodCC3).UnidadesOrgNivel4.Item(scodCC1 & scodCC2 & scodCC3 & scodCC4).PartidasNivel1.Item(scodPP1).PresConceptos5Nivel2.Item(scodPP1 & scodPP2).PresConceptos5Nivel3.Item(scodPP1 & scodPP2 & scodPP3).PresConceptos5Nivel4.Item(scodPP1 & scodPP2 & scodPP3 & scodPP4).Gestor
            End Select
            
            g_oPartida.PRES5Id = g_oPartida.CargarPres5Id
                        
            If g_oCenCos.UON2 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1
                If g_oCenCos.UON3 <> "" Then
                    g_oCenCos.CodConcat = g_oCenCos.CodConcat & "-" & g_oCenCos.UON2
                    If g_oCenCos.UON4 <> "" Then
                        g_oCenCos.CodConcat = g_oCenCos.CodConcat & "-" & g_oCenCos.UON3
                    End If
                End If
            End If
            
            g_oPartida.Cod = DevolverCod(nodx)
            
    
    End If
    
    If g_sOrigen = "Pedidos" Then
        If g_oOrigen.m_oArbolCabecera.ImpModo = TipoModoImputacionSM.Nivellinea Then
            If Not g_oOrigen.g_oOrdenesTemporales Is Nothing Then
                If g_oOrigen.g_oOrdenesTemporales.Count > 0 Then 'Se comprueba que haya alguna linea seleccionada para pedido.
                    If g_oOrigen.txtContrato.Text <> "" Then
                        If g_oOrigen.m_oConPres.Pres0 <> g_oPartida.Pres0 Or g_oOrigen.m_oConPres.Pres1 <> g_oPartida.Pres1 Or g_oOrigen.m_oConPres.Pres2 <> g_oPartida.Pres2 Or g_oOrigen.m_oConPres.Pres3 <> g_oPartida.Pres3 Or g_oOrigen.m_oConPres.Pres4 <> g_oPartida.Pres4 Then
                            irespuesta = oMensajes.CambiarPartidaLineas(g_oOrigen.m_oArbolCabecera.NomNivelImputacion)
                        End If
                    Else
                        irespuesta = oMensajes.CambiarPartidaLineas(g_oOrigen.m_oArbolCabecera.NomNivelImputacion)
                    End If
                    If irespuesta = vbYes Then
                        Set g_oOrigen.m_oConPres = g_oPartida
                        Set g_oOrigen.m_oCenCoste = g_oCenCos
                        g_oOrigen.bSelectDeCon = True
                        g_oOrigen.txtContrato.Text = g_oPartida.Cod & " - " & g_oPartida.Den
                        If g_oCenCos.CodConcat <> "" Then
                            g_oOrigen.txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
                        Else
                            g_oOrigen.txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
                        End If
                    End If
                Else
                    Set g_oOrigen.m_oConPres = g_oPartida
                    Set g_oOrigen.m_oCenCoste = g_oCenCos
                    g_oOrigen.bSelectDeCon = True
                    g_oOrigen.txtContrato.Text = g_oPartida.Cod & " - " & g_oPartida.Den
                    If g_oCenCos.CodConcat <> "" Then
                        g_oOrigen.txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
                    Else
                        g_oOrigen.txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
                    End If
                End If
                g_oOrigen.m_bDatosSMCargados = True
            End If
        Else
            Set g_oOrigen.m_oConPres = g_oPartida
            Set g_oOrigen.m_oCenCoste = g_oCenCos
            g_oOrigen.bSelectDeCon = True
            g_oOrigen.txtContrato.Text = g_oPartida.Cod & " - " & g_oPartida.Den
            If g_oCenCos.CodConcat <> "" Then
                g_oOrigen.txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
            Else
                g_oOrigen.txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
        End If
    ElseIf g_sOrigen = "MisPedidos" Then
        Set g_oOrigen.m_oConPresSel = g_oPartida
        Set g_oOrigen.m_oCenCosteSel = g_oCenCos
    ElseIf g_sOrigen = "frmSeguimiento" Then
        Set g_oOrigen.oContratoSeleccionado = g_oPartida
        Set g_oOrigen.m_oCentroCosteSeleccionadoFiltro = g_oCenCos
        Set g_oOrigen.oCentroCosteSeleccionado = g_oCenCos
    ElseIf g_sOrigen = "frmLstPedidos" Then
        Set g_oOrigen.oContratoSeleccionado = g_oPartida
        Set g_oOrigen.m_oCentroCosteSeleccionadoFiltro = g_oCenCos
        Set g_oOrigen.oCentroCosteSeleccionado = g_oCenCos
    ElseIf g_sOrigen = "frmSolicitudes" Then
        Set frmSolicitudes.g_oPartida = g_oPartida
    ElseIf g_sOrigen = "frmSolicitudBuscar" Then
        Set frmSolicitudBuscar.g_oPartida = g_oPartida
    ElseIf g_sOrigen = "frmLstSolicitud" Then
        Set frmLstSolicitud.g_oPartida = g_oPartida
    ElseIf g_sOrigen = "frmFacturaBuscar" Then
        Set frmFacturaBuscar.g_oPartida = g_oPartida
        Set g_oOrigen.g_oCenCos = g_oCenCos
    End If
    
    m_bDobleClic = False
    Screen.MousePointer = vbNormal
         
   Unload Me
  
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
            
End Sub


''' <summary>
''' Bot�n que muestra el calendario para seleccionar la fecha Desde.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCalDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub


''' <summary>
''' Bot�n que muestra el calendario para seleccionar la fecha Hasta.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCalHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub


''' <summary>
''' Evento de cancelar.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Funci�n que carga los recursos de la ventana, etiquetas...
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEL_CCONTABLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value & " " & g_oParametrosSM.Item(g_sPres0).NomNivelImputacion
               
        Ador.MoveNext
        
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        
        m_sCadenaErrorNodo = Ador(0).Value
        Ador.MoveNext
        lblSeleccion.caption = Ador(0).Value
        Ador.MoveNext
        lblCenCoste.caption = Ador(0).Value
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value
        Ador.MoveNext
        chkNoVigentes.caption = Ador(0).Value
        Ador.MoveNext
'        cmdBuscar.caption = Ador(0).Value
        Ador.MoveNext
        cmdSeleccion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        lblGestor.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbcGestor.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbcGestor.Columns("DEN").caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub


''' <summary>
''' Bot�n que efectua una nueva carga del �rbol de partidas teniendo en cuenta los criterios de busqueda seleccionados.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCargar_Click()
Dim sFechaActual As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sFechaActual = Format(Now, "DD\/MM\/YYYY")
    
    If txtFecDesde.Text <> "" And txtFecHasta.Text <> "" Then
        If CDate(txtFecDesde.Text) < CDate(sFechaActual) And CDate(txtFecHasta.Text) < CDate(sFechaActual) Then
            chkNoVigentes.Value = 1
        ElseIf CDate(txtFecDesde.Text) > CDate(sFechaActual) And CDate(txtFecHasta.Text) > CDate(sFechaActual) Then
            chkNoVigentes.Value = 1
        End If
    End If
    
    
    'Cargamos la estructura de partidas.
    If Not g_oCenCos Is Nothing Then
         oUnidadesOrg.CargarEstructuraUONsPartidas_SM g_sPres0, g_iNivel, IIf((basOptimizacion.gTipoDeUsuario = Administrador), "", basOptimizacion.gvarCodUsuario), g_lIdEmpresa, g_oCenCos.UON1, g_oCenCos.UON2, g_oCenCos.UON3, g_oCenCos.UON4, txtFecDesde.Text, txtFecHasta.Text, chkNoVigentes.Value, IIf(sdbcGestor.Value <> "", sdbcGestor.Columns("COD").Value, "")
    Else
        oUnidadesOrg.CargarEstructuraUONsPartidas_SM g_sPres0, g_iNivel, IIf((basOptimizacion.gTipoDeUsuario = Administrador), "", basOptimizacion.gvarCodUsuario), g_lIdEmpresa, , , , , txtFecDesde.Text, txtFecHasta.Text, chkNoVigentes.Value, IIf(sdbcGestor.Value <> "", sdbcGestor.Columns("COD").Value, "")
    End If
    
    GenerarEstructura oUnidadesOrg
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "cmdCargar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Bot�n que muestra la pantalla de busqueda de centros de coste para su elecci�n.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdSelCCoste_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCenCoste.g_sOrigen = "SelContable"
    frmSelCenCoste.g_sPres0 = g_sPres0
    frmSelCenCoste.g_lIdEmpresa = g_lIdEmpresa
    frmSelCenCoste.Show 1
    If Not g_oCenCos Is Nothing Then
        If g_oCenCos.CodConcat <> "" Then
            txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
        Else
            txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
        End If
        m_bCenValidado = True
    End If
    If txtFecDesde.Visible Then txtFecDesde.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "cmdSelCCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Bot�n te va moviendo a trav�s del �rbol de partidas por las diferentes partidas de cuyo nombre forme parte el texto introducido por el usuario en la caja de texto de la partida.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdSeleccion_Click()
Dim nodx As MSComctlLib.node
Dim i As Integer
Dim bParar As Boolean


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_lNodo >= tvwestrCC.Nodes.Count Then m_lNodo = 1
bParar = False
For i = m_lNodo + 1 To tvwestrCC.Nodes.Count
        Set nodx = tvwestrCC.Nodes.Item(i)
        If InStr(1, nodx.Text, txtSeleccion.Text, vbTextCompare) > 0 Then
            nodx.Selected = True
            Set tvwestrCC.selectedItem = nodx
            m_lNodo = nodx.Index
            Exit For
        End If
        If i = tvwestrCC.Nodes.Count And Not bParar Then
            i = 2
            m_lNodo = 1
            bParar = True
        End If
Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "cmdSeleccion_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga del formulario.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    m_bDescargarFrm = False
    m_bActivado = False
    m_bUnload = False
    
    Screen.MousePointer = vbHourglass

    CargarRecursos
    PonerFieldSeparator Me

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    DoEvents
    
    m_bCenValidado = False
    chkNoVigentes.Value = False
    m_bDobleClic = False
    
    Set oUnidadesOrg = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set m_oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste

    'Cargamos la estructura.
    Set oUnidadesOrg = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    If Not g_oGestor Is Nothing Then
        sdbcGestor.Text = g_oGestor.nombre & " " & g_oGestor.Apellidos
        sdbcGestor.Value = g_oGestor.nombre & " " & g_oGestor.Apellidos
        sdbcGestor.Columns("COD").Value = g_oGestor.Cod
    End If
    If Not g_oCenCos Is Nothing Then
        If g_oCenCos.CodConcat <> "" Then
            txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
        Else
            If g_oCenCos.Cod <> "" Then
                txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            Else
                txtCenCoste.Text = ""
            End If
        End If
        m_bCenValidado = True
        oUnidadesOrg.CargarEstructuraUONsPartidas_SM g_sPres0, g_iNivel, basOptimizacion.gvarCodUsuario, g_lIdEmpresa, g_oCenCos.UON1, g_oCenCos.UON2, g_oCenCos.UON3, g_oCenCos.UON4, , , , IIf(sdbcGestor.Value <> "", sdbcGestor.Columns("COD").Value, "")
    Else
        oUnidadesOrg.CargarEstructuraUONsPartidas_SM g_sPres0, g_iNivel, basOptimizacion.gvarCodUsuario, g_lIdEmpresa, , , , , , , , IIf(sdbcGestor.Value <> "", sdbcGestor.Columns("COD").Value, "")
    End If
    
    GenerarEstructura oUnidadesOrg
    
    If g_oParametrosSM.Item(g_sPres0).Plurianual Then
        'Ocultamos los controlse de fechas y vigencia
        lblFecDesde.Visible = False
        lblFecHasta.Visible = False
        txtFecHasta.Visible = False
        txtFecDesde.Visible = False
        cmdCalDesde.Visible = False
        cmdCalHasta.Visible = False
        chkNoVigentes.Visible = False
        cmdCargar.Top = sdbcGestor.Top
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Genera la estructura de partidas.
''' </summary>
''' <param name="oUnidadesOrg">Colecci�n con la carga de centros de coste y partidas.</param>
''' <returns></returns>
''' <remarks>Llamada desde: frmSelCContables.Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 10seg </remarks>

Private Sub GenerarEstructura(ByVal oUnidadesOrg As CUnidadesOrgNivel1)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scodp1 As String
Dim scodp2 As String
Dim scodp3 As String
Dim scodp4 As String

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
Dim oUON4 As CUnidadOrgNivel4
Dim oPRES1 As CPresConcep5Nivel1
Dim oPRES2 As CPresConcep5Nivel2
Dim oPRES3 As CPresConcep5Nivel3
Dim oPRES4 As CPresConcep5Nivel4
' Otras
Dim nodx As node
Dim oCentrosCoste As CCentrosCoste
Dim sFechaActual As String
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
    
    sFechaActual = Format(Now, "DD\/MM\/YYYY")
    

   '************************************************************
    'Generamos la estructura arborea
        
    tvwestrCC.Nodes.clear
    tvwestrCC.ImageList = ImageList1
    Set nodx = tvwestrCC.Nodes.Add(, , "Raiz", "Partidas presupuestarias", "RAIZ")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    If oUnidadesOrg Is Nothing Then
        Exit Sub
    End If
        
    For Each oUON1 In oUnidadesOrg
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        If Not oUON1.CC Then
            Set nodx = tvwestrCC.Nodes.Add("Raiz", tvwChild, "CC1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "CC1")
        Else
            Set nodx = tvwestrCC.Nodes.Add("Raiz", tvwChild, "CC1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "CCC")
        End If
        nodx.Tag = "CC1-" & CStr(oUON1.Cod)
        If Not g_oCenCos Is Nothing Then
            nodx.Expanded = True
        End If
        If Not oUON1.PartidasNivel1 Is Nothing Then
            For Each oPRES1 In oUON1.PartidasNivel1
                scodp1 = scod1 & oPRES1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(oPRES1.Cod))
                Set nodx = tvwestrCC.Nodes.Add("CC1" & scod1, tvwChild, "P11" & scod1 & scodp1, CStr(oPRES1.Cod) & " - " & oPRES1.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES1.DenVigencia), "PP")
                nodx.Tag = "P11-" & CStr(oPRES1.Cod)
                If CDate(oPRES1.FecIni) >= CDate(sFechaActual) Or CDate(oPRES1.FecFin) <= CDate(sFechaActual) Then
                    nodx.Forecolor = vbRed
                End If
                If Not g_oCenCos Is Nothing Then
                    nodx.Expanded = True
                End If
                For Each oPRES2 In oPRES1.PresConceptos5Nivel2
                    scodp2 = scodp1 & oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                    Set nodx = tvwestrCC.Nodes.Add("P11" & scod1 & scodp1, tvwChild, "P12" & scod1 & scodp1 & scodp2, CStr(oPRES2.Cod) & " - " & oPRES2.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES2.DenVigencia), "PP")
                    nodx.Tag = "P12-" & CStr(oPRES2.Cod)
                    If CDate(oPRES2.FecIni) >= CDate(sFechaActual) Or CDate(oPRES2.FecFin) <= CDate(sFechaActual) Then
                        nodx.Forecolor = vbRed
                    End If
                    If Not g_oCenCos Is Nothing Then
                        nodx.Expanded = True
                    End If
                    For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                        scodp3 = scod1 & scod2 & scod3 & scodp1 & scodp2 & oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                        Set nodx = tvwestrCC.Nodes.Add("P12" & scod1 & scodp1 & scodp2, tvwChild, "P13" & scod1 & scodp1 & scodp2 & scodp3, CStr(oPRES3.Cod) & " - " & oPRES3.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES3.DenVigencia), "PP")
                        nodx.Tag = "P13-" & CStr(oPRES3.Cod)
                        If CDate(oPRES3.FecIni) >= CDate(sFechaActual) Or CDate(oPRES3.FecFin) <= CDate(sFechaActual) Then
                            nodx.Forecolor = vbRed
                        End If
                        If Not g_oCenCos Is Nothing Then
                            nodx.Expanded = True
                        End If
                        For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                            scodp4 = scod1 & scodp1 & scodp2 & scodp3 & oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                            Set nodx = tvwestrCC.Nodes.Add("P13" & scod1 & scodp1 & scodp2 & scodp3, tvwChild, "P14" & scod1 & scod2 & scod3 & scodp1 & scodp2 & scodp3 & scodp4, CStr(oPRES4.Cod) & " - " & oPRES4.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES4.DenVigencia), "PP")
                            nodx.Tag = "P14-" & CStr(oPRES4.Cod)
                            If CDate(oPRES4.FecIni) >= CDate(sFechaActual) Or CDate(oPRES4.FecFin) <= CDate(sFechaActual) Then
                                nodx.Forecolor = vbRed
                            End If
                            If Not g_oCenCos Is Nothing Then
                                nodx.Expanded = True
                            End If
                        Next
                    Next
                Next
            Next
        End If
        
        For Each oUON2 In oUON1.UnidadesOrgNivel2
            scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
            scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
            If Not oUON2.CC Then
                Set nodx = tvwestrCC.Nodes.Add("CC1" & scod1, tvwChild, "CC2" & scod1 & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "CC2")
            Else
                Set nodx = tvwestrCC.Nodes.Add("CC1" & scod1, tvwChild, "CC2" & scod1 & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "CCC")
            End If
            nodx.Tag = "CC2-" & CStr(oUON2.Cod)
            If Not g_oCenCos Is Nothing Then
                nodx.Expanded = True
            End If
            If Not oUON2.PartidasNivel1 Is Nothing Then
                For Each oPRES1 In oUON2.PartidasNivel1
                    scodp1 = scod2 & oPRES1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(oPRES1.Cod))
                    Set nodx = tvwestrCC.Nodes.Add("CC2" & scod1 & scod2, tvwChild, "P21" & scod1 & scod2 & scodp1, CStr(oPRES1.Cod) & " - " & oPRES1.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES1.DenVigencia), "PP")
                    nodx.Tag = "P21-" & CStr(oPRES1.Cod)
                    If CDate(oPRES1.FecIni) >= CDate(sFechaActual) Or CDate(oPRES1.FecFin) <= CDate(sFechaActual) Then
                        nodx.Forecolor = vbRed
                    End If
                    If Not g_oCenCos Is Nothing Then
                        nodx.Expanded = True
                    End If
                    For Each oPRES2 In oPRES1.PresConceptos5Nivel2
                        scodp2 = scod1 & scod2 & scodp1 & oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                        Set nodx = tvwestrCC.Nodes.Add("P21" & scod1 & scod2 & scodp1, tvwChild, "P22" & scod1 & scod2 & scodp1 & scodp2, CStr(oPRES2.Cod) & " - " & oPRES2.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES2.DenVigencia), "PP")
                        nodx.Tag = "P22-" & CStr(oPRES2.Cod)
                        If CDate(oPRES2.FecIni) >= CDate(sFechaActual) Or CDate(oPRES2.FecFin) <= CDate(sFechaActual) Then
                            nodx.Forecolor = vbRed
                        End If
                        If Not g_oCenCos Is Nothing Then
                            nodx.Expanded = True
                        End If
                        For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                            scodp3 = scod1 & scod2 & scod3 & scodp1 & scodp2 & oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                            Set nodx = tvwestrCC.Nodes.Add("P22" & scod1 & scod2 & scodp1 & scodp2, tvwChild, "P23" & scod1 & scod2 & scodp1 & scodp2 & scodp3, CStr(oPRES3.Cod) & " - " & oPRES3.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES3.DenVigencia), "PP")
                            nodx.Tag = "P23-" & CStr(oPRES3.Cod)
                            If CDate(oPRES3.FecIni) >= CDate(sFechaActual) Or CDate(oPRES3.FecFin) <= CDate(sFechaActual) Then
                                nodx.Forecolor = vbRed
                            End If
                            If Not g_oCenCos Is Nothing Then
                                nodx.Expanded = True
                            End If
                            For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                                scodp4 = scod1 & scod2 & scod3 & scodp1 & scodp2 & scodp3 & oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                                Set nodx = tvwestrCC.Nodes.Add("P23" & scod1 & scod2 & scodp1 & scodp2 & scodp3, tvwChild, "P24" & scod1 & scod2 & scodp1 & scodp2 & scodp3 & scodp4, CStr(oPRES4.Cod) & " - " & oPRES4.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES4.DenVigencia), "PP")
                                nodx.Tag = "P24-" & CStr(oPRES4.Cod)
                                If CDate(oPRES4.FecIni) >= CDate(sFechaActual) Or CDate(oPRES4.FecFin) <= CDate(sFechaActual) Then
                                    nodx.Forecolor = vbRed
                                End If
                                If Not g_oCenCos Is Nothing Then
                                    nodx.Expanded = True
                                End If
                            Next
                        Next
                    Next
                Next
            End If
            
            
            For Each oUON3 In oUON2.UnidadesOrgNivel3
                scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
                scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
                scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
                If Not oUON3.CC Then
                    Set nodx = tvwestrCC.Nodes.Add("CC2" & scod1 & scod2, tvwChild, "CC3" & scod1 & scod2 & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "CC3")
                Else
                    Set nodx = tvwestrCC.Nodes.Add("CC2" & scod1 & scod2, tvwChild, "CC3" & scod1 & scod2 & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "CCC")
                End If
                nodx.Tag = "CC3-" & CStr(oUON3.Cod)
                If Not g_oCenCos Is Nothing Then
                    nodx.Expanded = True
                End If
                If Not oUON3.PartidasNivel1 Is Nothing Then
                    For Each oPRES1 In oUON3.PartidasNivel1
                        scodp1 = scod1 & scod2 & scod3 & oPRES1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(oPRES1.Cod))
                        Set nodx = tvwestrCC.Nodes.Add("CC3" & scod1 & scod2 & scod3, tvwChild, "P31" & scod1 & scod2 & scod3 & scodp1, CStr(oPRES1.Cod) & " - " & oPRES1.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES1.DenVigencia), "PP")
                        nodx.Tag = "P31-" & CStr(oPRES1.Cod)
                        If CDate(oPRES1.FecIni) >= CDate(sFechaActual) Or CDate(oPRES1.FecFin) <= CDate(sFechaActual) Then
                            nodx.Forecolor = vbRed
                        End If
                        If Not g_oCenCos Is Nothing Then
                            nodx.Expanded = True
                        End If
                        For Each oPRES2 In oPRES1.PresConceptos5Nivel2
                            scodp2 = scod1 & scod2 & scod3 & scodp1 & oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                            Set nodx = tvwestrCC.Nodes.Add("P31" & scod1 & scod2 & scod3 & scodp1, tvwChild, "P32" & scod1 & scod2 & scod3 & scodp1 & scodp2, CStr(oPRES2.Cod) & " - " & oPRES2.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES2.DenVigencia), "PP")
                            nodx.Tag = "P32-" & CStr(oPRES2.Cod)
                            If CDate(oPRES2.FecIni) >= CDate(sFechaActual) Or CDate(oPRES2.FecFin) <= CDate(sFechaActual) Then
                                nodx.Forecolor = vbRed
                            End If
                            If Not g_oCenCos Is Nothing Then
                                nodx.Expanded = True
                            End If
                            For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                                scodp3 = scod1 & scod2 & scod3 & scodp1 & scodp2 & oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                                Set nodx = tvwestrCC.Nodes.Add("P32" & scod1 & scod2 & scod3 & scodp1 & scodp2, tvwChild, "P33" & scod1 & scod2 & scod3 & scodp1 & scodp2 & scodp3, CStr(oPRES3.Cod) & " - " & oPRES3.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES3.DenVigencia), "PP")
                                nodx.Tag = "P33-" & CStr(oPRES3.Cod)
                                If CDate(oPRES3.FecIni) >= CDate(sFechaActual) Or CDate(oPRES3.FecFin) <= CDate(sFechaActual) Then
                                    nodx.Forecolor = vbRed
                                End If
                                If Not g_oCenCos Is Nothing Then
                                    nodx.Expanded = True
                                End If
                                For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                                    scodp4 = scod1 & scod2 & scod3 & scodp1 & scodp2 & scodp3 & oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                                    Set nodx = tvwestrCC.Nodes.Add("P33" & scod1 & scod2 & scod3 & scodp1 & scodp2 & scodp3, tvwChild, "P34" & scod1 & scod2 & scod3 & scodp1 & scodp2 & scodp3 & scodp4, CStr(oPRES4.Cod) & " - " & oPRES4.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES4.DenVigencia), "PP")
                                    nodx.Tag = "P34-" & CStr(oPRES4.Cod)
                                    If CDate(oPRES4.FecIni) >= CDate(sFechaActual) Or CDate(oPRES4.FecFin) <= CDate(sFechaActual) Then
                                        nodx.Forecolor = vbRed
                                    End If
                                    If Not g_oCenCos Is Nothing Then
                                        nodx.Expanded = True
                                    End If
                                Next
                            Next
                        Next
                    Next
                End If
                
                
                For Each oUON4 In oUON3.UnidadesOrgNivel4
                    scod1 = oUON4.CodUnidadOrgNivel1 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON4.CodUnidadOrgNivel1))
                    scod2 = scod1 & oUON4.CodUnidadOrgNivel2 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON4.CodUnidadOrgNivel2))
                    scod3 = scod2 & oUON4.CodUnidadOrgNivel3 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON4.CodUnidadOrgNivel3))
                    scod4 = scod3 & oUON4.Cod & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(oUON4.Cod))
                    If Not oUON4.CC Then
                        Set nodx = tvwestrCC.Nodes.Add("CC3" & scod1 & scod2 & scod3, tvwChild, "CC4" & scod1 & scod2 & scod3 & scod4, CStr(oUON4.Cod) & " - " & oUON4.Den, "CC4")
                    Else
                        Set nodx = tvwestrCC.Nodes.Add("CC3" & scod1 & scod2 & scod3, tvwChild, "CC4" & scod1 & scod2 & scod3 & scod4, CStr(oUON4.Cod) & " - " & oUON4.Den, "CCC")
                    End If
                    nodx.Tag = "CC4-" & CStr(oUON4.Cod)
                    If Not g_oCenCos Is Nothing Then
                        nodx.Expanded = True
                    End If
                    If Not oUON4.PartidasNivel1 Is Nothing Then
                        For Each oPRES1 In oUON4.PartidasNivel1
                            scodp1 = scod1 & scod2 & scod3 & scod4 & oPRES1.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv1), 1, gLongitudesDeCodigos.giLongCodPres5Niv1 - Len(oPRES1.Cod))
                            Set nodx = tvwestrCC.Nodes.Add("CC4" & scod1 & scod2 & scod3 & scod4, tvwChild, "P41" & scod1 & scod2 & scod3 & scod4 & scodp1, CStr(oPRES1.Cod) & " - " & oPRES1.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES1.DenVigencia), "PP")
                            nodx.Tag = "P41-" & CStr(oPRES1.Cod)
                            If CDate(oPRES1.FecIni) >= CDate(sFechaActual) Or CDate(oPRES1.FecFin) <= CDate(sFechaActual) Then
                                nodx.Forecolor = vbRed
                            End If
                            If Not g_oCenCos Is Nothing Then
                                nodx.Expanded = True
                            End If
                            For Each oPRES2 In oPRES1.PresConceptos5Nivel2
                                scodp2 = scod1 & scod2 & scod3 & scod4 & scodp1 & oPRES2.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv2), 1, gLongitudesDeCodigos.giLongCodPres5Niv2 - Len(oPRES2.Cod))
                                Set nodx = tvwestrCC.Nodes.Add("P41" & scod1 & scod2 & scod3 & scod4 & scodp1, tvwChild, "P42" & scod1 & scod2 & scod3 & scod4 & scodp1 & scodp2, CStr(oPRES2.Cod) & " - " & oPRES2.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES2.DenVigencia), "PP")
                                nodx.Tag = "P42-" & CStr(oPRES2.Cod)
                                If CDate(oPRES2.FecIni) >= CDate(sFechaActual) Or CDate(oPRES2.FecFin) <= CDate(sFechaActual) Then
                                    nodx.Forecolor = vbRed
                                End If
                                If Not g_oCenCos Is Nothing Then
                                    nodx.Expanded = True
                                End If
                                For Each oPRES3 In oPRES2.PresConceptos5Nivel3
                                    scodp3 = scod1 & scod2 & scod3 & scod4 & scodp1 & scodp2 & oPRES3.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv3), 1, gLongitudesDeCodigos.giLongCodPres5Niv3 - Len(oPRES3.Cod))
                                    Set nodx = tvwestrCC.Nodes.Add("P42" & scod1 & scod2 & scod3 & scod4 & scodp1 & scodp2, tvwChild, "P43" & scod1 & scod2 & scod3 & scod4 & scodp1 & scodp2 & scodp3, CStr(oPRES3.Cod) & " - " & oPRES3.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES3.DenVigencia), "PP")
                                    nodx.Tag = "P43-" & CStr(oPRES3.Cod)
                                    If CDate(oPRES3.FecIni) >= CDate(sFechaActual) Or CDate(oPRES3.FecFin) <= CDate(sFechaActual) Then
                                        nodx.Forecolor = vbRed
                                    End If
                                    If Not g_oCenCos Is Nothing Then
                                        nodx.Expanded = True
                                    End If
                                    For Each oPRES4 In oPRES3.PresConceptos5Nivel4
                                        scodp4 = scod1 & scod2 & scod3 & scod4 & scodp1 & scodp2 & scodp3 & oPRES4.Cod & Mid$(Space(gLongitudesDeCodigos.giLongCodPres5Niv4), 1, gLongitudesDeCodigos.giLongCodPres5Niv4 - Len(oPRES4.Cod))
                                        Set nodx = tvwestrCC.Nodes.Add("P43" & scod1 & scod2 & scod3 & scod4 & scodp1 & scodp2 & scodp3, tvwChild, "P44" & scod1 & scod2 & scod3 & scod4 & scodp1 & scodp2 & scodp3 & scodp4, CStr(oPRES4.Cod) & " - " & oPRES4.Den & IIf(g_oParametrosSM.Item(g_sPres0).Plurianual, "", oPRES4.DenVigencia), "PP")
                                        nodx.Tag = "P44-" & CStr(oPRES4.Cod)
                                        If CDate(oPRES4.FecIni) >= CDate(sFechaActual) Or CDate(oPRES4.FecFin) <= CDate(sFechaActual) Then
                                            nodx.Forecolor = vbRed
                                        End If
                                        If Not g_oCenCos Is Nothing Then
                                            nodx.Expanded = True
                                        End If
                                    Next
                                Next
                            Next
                        Next
                    End If
                    
                Next
            Next
        Next
    Next
    
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oUON4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "GenerarEstructura", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Devuelve el c�digo de un nodo.
''' </summary>
''' <param name="node">Nodo</param>
''' <returns></returns>
''' <remarks>Llamada desde: frmSelCContables.cmdAceptar_Click</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Function DevolverCod(ByVal node As MSComctlLib.node) As Variant


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Function

DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function


''' <summary>
''' Resize del formulario.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If Me.ScaleHeight < (tvwestrCC.Top + cmdAceptar.Height + (2 * cnVSep)) Then
        Me.Height = 2700
        Exit Sub
    End If
    If Me.Width < 500 Then Exit Sub

    tvwestrCC.Width = Me.Width - 45

    tvwestrCC.Height = Me.ScaleHeight - tvwestrCC.Top - cmdAceptar.Height - (2 * cnVSep)
    cmdAceptar.Top = tvwestrCC.Top + tvwestrCC.Height + cnVSep
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (tvwestrCC.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (tvwestrCC.Width / 2) + 300
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub


''' <summary>
''' Descarga del formulario y las variables.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
    m_bDescargarFrm = False
    oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oUnidadesOrg = Nothing
    g_sOrigen = ""
    g_sPres0 = ""
    Set g_oOrigen = Nothing
    g_lIdEmpresa = 0
    Set g_oCenCos = Nothing
    Set g_oPartida = Nothing
    Set g_oGestor = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSELCContables", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcGestor_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbcGestor
        If .Value = "..." Then
            .Text = ""
            Exit Sub
        End If
                
        .Text = .Columns(1).Text
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "sdbcGestor_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGestor_DropDown()
    Dim oGestores As CPersonas
    Dim oGestor As CPersona
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbcGestor
        .RemoveAll
                
        Screen.MousePointer = vbHourglass
                
        'Obtener gestores
        Set oGestores = DevolverGestores
        If Not oGestores Is Nothing Then
            If oGestores.Count > 0 Then
                For Each oGestor In oGestores
                    .AddItem oGestor.Cod & Chr(m_lSeparador) & oGestor.nombre & " " & oGestor.Apellidos
                Next
                Set oGestor = Nothing
                Set oGestores = Nothing
            End If
        End If
        
        .SelStart = 0
        .SelLength = Len(.Text)
        .Refresh
        
        Screen.MousePointer = vbNormal
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "sdbcGestor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGestor_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGestor.DataFieldList = "Column 1"
    sdbcGestor.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "sdbcGestor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Devuelve los gestores asociados al centro de coste seleccionado.
''' Si no hay un centro de coste seleccionado se cargar�n todos los posibles gestores para la empresa del pedido
''' (gestores de partidas asociadas a centros de coste de la empresa del pedido)</summary>
''' <returns>Colecci�n de gestores</returns>
''' <remarks>Llamada desde: sdbcGestor_Validate y sdbcGestor_DropDown</remarks>
''' <revision>LTG 13/04/2012</revision>

Private Function DevolverGestores() As CPersonas
    Dim oGestores As CPersonas
    Dim oEmpresa As CEmpresa
    
                
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oCenCos Is Nothing Then
        If g_oCenCos.Cod <> "" Then
            'Gestores asociados al centro de coste seleccionado
            Set oGestores = g_oCenCos.DevolverGestoresAsociados
        End If
    End If
    
    If oGestores Is Nothing Then
        If g_lIdEmpresa <> 0 Then
            'Posibles gestores para la empresa del pedido
            Set oEmpresa = oFSGSRaiz.Generar_CEmpresa
            oEmpresa.Id = g_lIdEmpresa
            Set oGestores = oEmpresa.DevolverGestoresAsociados
        End If
    End If
    
    Set DevolverGestores = oGestores
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "DevolverGestores", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub sdbcGestor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    With sdbcGestor
        .MoveFirst
        
        If Text <> "" Then
            For i = 0 To .Rows - 1
                bm = .GetBookmark(i)
                If UCase(Text) = UCase(Mid(.Columns("DEN").CellText(bm), 1, Len(Text))) Then
                    .Bookmark = bm
                    Exit For
                End If
            Next i
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "sdbcGestor_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcGestor_Validate(Cancel As Boolean)
    Dim oGestores As CPersonas
    Dim oGestor As CPersona
    Dim bExiste As Boolean
    

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGestor.Text = "" Then
        sdbcGestor.Columns("COD").Text = ""
        sdbcGestor.Columns("DEN").Text = ""
    Else
        bExiste = False
        
        'Comprobar la existencia
        Set oGestores = DevolverGestores
        
        If Not oGestores Is Nothing Then
            If oGestores.Count > 0 Then
                For Each oGestor In oGestores
                    If sdbcGestor.Columns("COD").Text = oGestor.Cod Then
                        bExiste = True
                        Exit For
                    End If
                Next
            End If
        End If

        If bExiste Then
            sdbcGestor.Columns("COD").Text = oGestor.Cod
            sdbcGestor.Columns("DEN").Text = oGestor.nombre & " " & oGestor.Apellidos
            sdbcGestor.Text = oGestor.nombre & " " & oGestor.Apellidos
        Else
            sdbcGestor.Columns("COD").Text = ""
            sdbcGestor.Text = ""
        End If
        
        Set oGestores = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "sdbcGestor_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que salta cuando se hace doble click en alg�n nodo del �rbol de partidas.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub tvwestrCC_DblClick()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDobleClic = True
    cmdAceptar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "tvwestrCC_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Evento que salta cuando estando situado en el �rbol de partidas se presiona alguna tecla.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub tvwestrCC_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        If Not tvwestrCC.selectedItem Is Nothing Then
            cmdAceptar_Click
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "tvwestrCC_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Change de la caja de texto del centro de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub txtCenCoste_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCenValidado = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "txtCenCoste_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Validate del centro de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub txtCenCoste_Validate(Cancel As Boolean)


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtCenCoste.Text <> "" And Not m_bCenValidado Then
        m_oCentrosCoste.CargarTodosLosCentrosCoste g_sPres0, g_iNivel, basOptimizacion.gTipoDeUsuario, basOptimizacion.gvarCodUsuario, gParametrosInstalacion.gIdioma, , , , , g_lIdEmpresa, Trim(txtCenCoste.Text)
                
        If m_oCentrosCoste.Count >= 1 Then
            'Si existe ese centro de coste en varias ramas.
            If m_oCentrosCoste.Count > 1 Then
                oMensajes.MensajeOKOnly (1061)
                Exit Sub
            'Si s�lo est� en una rama.
            ElseIf m_oCentrosCoste.Count = 1 Then
                Set g_oCenCos = m_oCentrosCoste.Item(1)
                txtCenCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
                m_bCenValidado = True
            End If
        Else
            'El centro de coste introducido no es v�lido.
            txtCenCoste.Backcolor = &HC0C0FF
            oMensajes.MensajeOKOnly (1079)
            txtCenCoste.Text = ""
            txtCenCoste.Backcolor = &H80000005
            Exit Sub
        End If
    Else
        Set g_oCenCos = Nothing
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "txtCenCoste_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Change de la caja de texto en la que se introduce el c�digo de la partida.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub txtSeleccion_Change()
Dim nodx As MSComctlLib.node


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtSeleccion.Text = "" Then Exit Sub
    For Each nodx In tvwestrCC.Nodes
        If InStr(1, nodx.Text, txtSeleccion.Text, vbTextCompare) > 0 Then
            nodx.Selected = True
            Set tvwestrCC.selectedItem = nodx
            m_lNodo = nodx.Index
            Exit For
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "txtSeleccion_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Evento que salta cuando estando situado en la caja de texto de la partida el usuario presiona alguna tecla.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub txtSeleccion_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        If Not tvwestrCC.selectedItem Is Nothing Then
            cmdAceptar_Click
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCContables", "txtSeleccion_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
