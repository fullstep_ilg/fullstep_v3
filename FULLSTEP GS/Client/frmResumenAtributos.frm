VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmResumenAtributos 
   BackColor       =   &H00808000&
   Caption         =   "DResumen de atributos"
   ClientHeight    =   3735
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7980
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmResumenAtributos.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3735
   ScaleWidth      =   7980
   Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
      Height          =   3375
      Left            =   75
      TabIndex        =   0
      Top             =   60
      Width           =   7815
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   8
      BevelColorFrame =   0
      BevelColorHighlight=   16777215
      BevelColorShadow=   12632256
      BevelColorFace  =   12632256
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Caption=   "DC�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadBackColor=   32896
      Columns(0).BackColor=   12632256
      Columns(1).Width=   3200
      Columns(1).Caption=   "DNombre"
      Columns(1).Name =   "NOMBRE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Style=   1
      Columns(1).ButtonsAlways=   -1  'True
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).HeadBackColor=   32896
      Columns(1).BackColor=   12632256
      Columns(2).Width=   3200
      Columns(2).Caption=   "DGrupo"
      Columns(2).Name =   "GRUPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HasForeColor=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).HeadBackColor=   32896
      Columns(2).BackColor=   12632256
      Columns(3).Width=   3200
      Columns(3).Caption=   "DNivelIntro"
      Columns(3).Name =   "NIVELINTRO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasHeadForeColor=   -1  'True
      Columns(3).HasHeadBackColor=   -1  'True
      Columns(3).HasForeColor=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).HeadBackColor=   32896
      Columns(3).BackColor=   12632256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "IDATRIB"
      Columns(4).Name =   "IDATRIB"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).HasHeadBackColor=   -1  'True
      Columns(4).HasBackColor=   -1  'True
      Columns(4).HeadBackColor=   32896
      Columns(4).BackColor=   12632256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "IDATRIBPROCE"
      Columns(5).Name =   "IDATRIBPROCE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "PRIGRUPO"
      Columns(6).Name =   "PRIGRUPO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "GRUPOID"
      Columns(7).Name =   "GRUPOID"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   13785
      _ExtentY        =   5953
      _StockProps     =   79
      BackColor       =   12632256
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmResumenAtributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable de control de flujo de proceso
Public g_oProceso As CProceso
Public g_ogrupo As CGrupo
Public g_oAtributo As Catributo
Public g_adoRes As Ador.Recordset

Private m_sAmbito(2) As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_RESUMENATRIBUTOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        m_sAmbito(0) = Ador(0).Value
        Ador.MoveNext
        m_sAmbito(1) = Ador(0).Value
        Ador.MoveNext
        m_sAmbito(2) = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmResumenAtributos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Width = 9120
    Me.Height = 4365
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    CargarRecursos

    PonerFieldSeparator Me
    
    Set g_adoRes = g_oProceso.DevolverResumenAtributos()
    CargarGrid g_adoRes
    sdbgAtributos.Columns(0).Locked = True
    sdbgAtributos.Columns(1).Locked = True
    sdbgAtributos.Columns(2).Locked = True
    sdbgAtributos.Columns(3).Locked = True
    sdbgAtributos.Columns(4).Locked = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmResumenAtributos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub Form_Resize()

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 6000 Then   'de tama�o de la ventana
            Me.Width = 6300        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 2000 Then  'cuando no se maximiza ni
            Me.Height = 2100       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
    
    If Me.Width >= 250 Then sdbgAtributos.Width = Me.Width - 250
    If Me.Height >= 1200 Then sdbgAtributos.Height = Me.Height - 500

    sdbgAtributos.Top = 50
    sdbgAtributos.Columns("COD").Width = sdbgAtributos.Width * 0.15
    sdbgAtributos.Columns("NOMBRE").Width = sdbgAtributos.Width * 0.35
    sdbgAtributos.Columns("GRUPO").Width = sdbgAtributos.Width * 0.25
    sdbgAtributos.Columns("NIVELINTRO").Width = sdbgAtributos.Width * 0.22 '- 310
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub



Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmResumenAtributos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgAtributos_BtnClick()
Dim oGrupo As CGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmDetAtribProce.g_sOrigen = "frmPROCE"
    If sdbgAtributos.Columns("GRUPO").Value = m_sAmbito(0) Then
        If Not g_oProceso.ATRIBUTOS Is Nothing Then
            If g_oProceso.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value) Is Nothing Then
                g_oProceso.CargarTodosLosAtributos ("")
            End If
            Set frmDetAtribProce.g_oAtributo = g_oProceso.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value)
        Else
            g_oProceso.CargarTodosLosAtributos ("")
            Set frmDetAtribProce.g_oAtributo = g_oProceso.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value)
        End If
    Else
        If frmPROCE.sdbcGrupoA.Visible Then
            If Not g_ogrupo.ATRIBUTOS Is Nothing Then
                If g_ogrupo.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value) Is Nothing Then
                    Set oGrupo = oFSGSRaiz.generar_cgrupo
                    oGrupo.Codigo = StrToNull(sdbgAtributos.Columns("PRIGRUPO").Value)
                    oGrupo.Id = sdbgAtributos.Columns("GRUPOID").Value
                    Set oGrupo.proceso = g_oProceso
                    oGrupo.CargarTodosLosAtributos
                    Set frmDetAtribProce.g_oAtributo = oGrupo.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value)
                    Set frmDetAtribProce.g_oGrupoSeleccionado = oGrupo
                Else
                    Set frmDetAtribProce.g_oAtributo = g_ogrupo.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value)
                    Set frmDetAtribProce.g_oGrupoSeleccionado = g_ogrupo
                End If
            Else
                Set oGrupo = oFSGSRaiz.generar_cgrupo
                oGrupo.Codigo = StrToNull(sdbgAtributos.Columns("PRIGRUPO").Value)
                oGrupo.Id = sdbgAtributos.Columns("GRUPOID").Value
                Set oGrupo.proceso = g_oProceso
                oGrupo.CargarTodosLosAtributos
                Set frmDetAtribProce.g_oAtributo = oGrupo.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value)
                Set frmDetAtribProce.g_oGrupoSeleccionado = oGrupo
            End If
        Else
            Set oGrupo = oFSGSRaiz.generar_cgrupo
            oGrupo.Codigo = StrToNull(sdbgAtributos.Columns("PRIGRUPO").Value)
            oGrupo.Id = sdbgAtributos.Columns("GRUPOID").Value
            Set oGrupo.proceso = g_oProceso
            oGrupo.CargarTodosLosAtributos
            Set frmDetAtribProce.g_oAtributo = oGrupo.ATRIBUTOS.Item(sdbgAtributos.Columns("IDATRIBPROCE").Value)
            Set frmDetAtribProce.g_oGrupoSeleccionado = oGrupo
        End If
    End If

    Set frmDetAtribProce.g_oProceso = g_oProceso
    
    frmDetAtribProce.sdbcGrupos.Value = sdbgAtributos.Columns("PRIGRUPO").Value
    If sdbgAtributos.Columns("GRUPO").Value = m_sAmbito(0) Then
        frmDetAtribProce.sdbcGrupos.AddItem m_sAmbito(0)
    End If
    frmDetAtribProce.sstabGeneral.Tab = 0
    frmDetAtribProce.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmResumenAtributos", "sdbgAtributos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub CargarGrid(ByVal g_adoRes As Ador.Recordset)
Dim sGrupo As String
Dim sGrupoID As String
Dim sAmbito As String
Dim sIdAtrib As String
Dim sDen As String
Dim sProceAtrib As String
Dim sCod As String

    'Carga la grid
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
        
    While Not g_adoRes.EOF
        If Not IsNull(g_adoRes("GRUPO").Value) Then
            sGrupo = g_adoRes("GRUPO").Value
            sGrupoID = g_adoRes("GRUPOID").Value
        Else
            sGrupo = m_sAmbito(0)
        End If
        If g_adoRes("AMBITO").Value = 1 Then
            sAmbito = m_sAmbito(0)
        Else
            If g_adoRes("AMBITO").Value = 2 Then
                sAmbito = m_sAmbito(1)
            Else
                sAmbito = m_sAmbito(2)
            End If
        End If
        sIdAtrib = g_adoRes("ATRIB").Value
        
        sCod = g_adoRes("COD").Value
        sDen = g_adoRes("DEN").Value
        sProceAtrib = CStr(g_adoRes("ID").Value)
        
        g_adoRes.MoveNext
        While Not g_adoRes.EOF
            If sIdAtrib = g_adoRes("ATRIB").Value Then
                If sAmbito = m_sAmbito(0) Or sAmbito = m_sAmbito(1) Or sAmbito = m_sAmbito(2) Then
                    sAmbito = sGrupo & ":" & sAmbito
                End If
                
                sAmbito = sAmbito & ";" & g_adoRes("GRUPO").Value & ":" & IIf(g_adoRes("AMBITO").Value = 2, m_sAmbito(1), m_sAmbito(2))
                
                If Not IsNull(g_adoRes("GRUPO").Value) Then
                    sGrupo = sGrupo & ", " & g_adoRes("GRUPO").Value
                End If
                g_adoRes.MoveNext
            Else
                GoTo Seguir
            End If
        Wend
        
        
Seguir:
        sdbgAtributos.AddItem sCod & Chr(m_lSeparador) & sDen & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sAmbito & Chr(m_lSeparador) & sIdAtrib & Chr(m_lSeparador) & sProceAtrib & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & sGrupoID
        
    Wend
    sdbgAtributos.MoveFirst
    'Cierra el recordset
    g_adoRes.Close
    Set g_adoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmResumenAtributos", "CargarGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


