VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroApliUO 
   Caption         =   "Informe de ahorros aplicados por unidades organizativas"
   ClientHeight    =   5490
   ClientLeft      =   45
   ClientTop       =   2400
   ClientWidth     =   11715
   Icon            =   "frmInfAhorroApliUO.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5490
   ScaleWidth      =   11715
   Begin VB.PictureBox picTipoGrafico 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   5220
      ScaleHeight     =   465
      ScaleWidth      =   3690
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   150
      Visible         =   0   'False
      Width           =   3690
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   630
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   90
         Width           =   1755
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3096
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   10095
      Picture         =   "frmInfAhorroApliUO.frx":0CB2
      ScaleHeight     =   735
      ScaleWidth      =   1635
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   10095
      Picture         =   "frmInfAhorroApliUO.frx":49B4
      ScaleHeight     =   615
      ScaleWidth      =   1635
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   60
      Visible         =   0   'False
      Width           =   1635
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   0
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   5160
      Width           =   11700
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroApliUO.frx":7B52
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroApliUO.frx":7B6E
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroApliUO.frx":7B8A
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   20637
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   3735
      Left            =   30
      TabIndex        =   8
      Top             =   1380
      Width           =   11670
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroApliUO.frx":7BA6
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroApliUO.frx":7BC2
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroApliUO.frx":7BDE
      AllowUpdate     =   0   'False
      ActiveCellStyleSet=   "Normal"
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "Normal"
      SplitterPos     =   2
      SplitterVisible =   -1  'True
      Columns.Count   =   7
      Columns(0).Width=   1323
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   4207
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   3942
      Columns(2).Caption=   "Presupuesto"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   15400959
      Columns(3).Width=   4022
      Columns(3).Caption=   "Adjudicado"
      Columns(3).Name =   "ADJ"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15400959
      Columns(4).Width=   3334
      Columns(4).Caption=   "Ahorro"
      Columns(4).Name =   "AHO"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1296
      Columns(5).Caption=   "%"
      Columns(5).Name =   "PORCEN"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "0.0#\%"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1323
      Columns(6).Caption=   "HIST"
      Columns(6).Name =   "HIST"
      Columns(6).Alignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   4
      Columns(6).ButtonsAlways=   -1  'True
      _ExtentX        =   20585
      _ExtentY        =   6588
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraMostrarDesde 
      Caption         =   "Mostrar resultados de unidad organizativa"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      Left            =   0
      TabIndex        =   9
      Top             =   660
      Width           =   11700
      Begin VB.CommandButton cmdBorrar 
         Height          =   285
         Left            =   5325
         Picture         =   "frmInfAhorroApliUO.frx":7BFA
         Style           =   1  'Graphical
         TabIndex        =   25
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdSelProy 
         Height          =   285
         Left            =   5670
         Picture         =   "frmInfAhorroApliUO.frx":7C9F
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   240
         Width           =   315
      End
      Begin VB.Label lblUO 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   5130
      End
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3735
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroApliUO.frx":7D0B
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   1380
      Width           =   11700
   End
   Begin VB.Frame fraSel 
      Height          =   615
      Left            =   0
      TabIndex        =   17
      Top             =   30
      Width           =   10095
      Begin VB.OptionButton optDir 
         Caption         =   "Adj. directa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5250
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   360
         Width           =   1545
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6840
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   360
         Width           =   1155
      End
      Begin VB.OptionButton optReu 
         Caption         =   "Adj. en reuni�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5250
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   120
         Value           =   -1  'True
         Width           =   2520
      End
      Begin VB.CommandButton cmdActualizar 
         Height          =   285
         Left            =   9315
         Picture         =   "frmInfAhorroApliUO.frx":9731
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   210
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoDesde 
         Height          =   285
         Left            =   570
         TabIndex        =   0
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesDesde 
         Height          =   285
         Left            =   1380
         TabIndex        =   1
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoHasta 
         Height          =   285
         Left            =   3150
         TabIndex        =   2
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesHasta 
         Height          =   285
         Left            =   3960
         TabIndex        =   3
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   8055
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   210
         Width           =   795
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1402
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "SSDBCombo1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdGrafico 
         Height          =   285
         Left            =   8925
         Picture         =   "frmInfAhorroApliUO.frx":97BC
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   210
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         Height          =   285
         Left            =   8925
         Picture         =   "frmInfAhorroApliUO.frx":9AFE
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   210
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdImprimir 
         Height          =   285
         Left            =   9705
         Picture         =   "frmInfAhorroApliUO.frx":9C48
         Style           =   1  'Graphical
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   210
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.Label lblAnyoHasta 
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2670
         TabIndex        =   24
         Top             =   240
         Width           =   450
      End
      Begin VB.Label lblAnyoDesde 
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   23
         Top             =   240
         Width           =   555
      End
   End
End
Attribute VB_Name = "frmInfAhorroApliUO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables para func. combos
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

Private ADORs As Ador.Recordset

'Variable de seguridad
Private bRUO As Boolean
Private m_iNivelUO As Integer
Private sUON1 As String
Private sUON2 As String
Private sUON3 As String
Private sDen As String
' variable para listado
Public ofrmLstApliUO As frmLstINFAhorrosApl

Private sIdiTipoGrafico(5) As String
Private sIdiMeses(12) As String
Private sIdiTotal As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiMonCent As String
Private sIdiHistRes As String

Private Sub ConfigurarSeguridad()
    
    If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) Then
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplUORestPer)) Is Nothing) Then
            bRUO = True
        End If
        
    End If
    If bRUO Then
        m_iNivelUO = 0
        If Not IsNull(basOptimizacion.gUON3Usuario) Then
            m_iNivelUO = 3
        ElseIf IsNull(basOptimizacion.gUON2Usuario) Then
            m_iNivelUO = 2
        ElseIf IsNull(basOptimizacion.gUON1Usuario) Then
            m_iNivelUO = 1
        End If
    End If


End Sub

Private Sub cmdActualizar_Click()
    
    'Comprueba que la fecha de inicio no sea mayor que la de fin
    If val(sdbcAnyoDesde) > val(sdbcAnyoHasta) Then
        sdbgRes.RemoveAll
        oMensajes.PeriodoNoValido
        If Me.Visible Then sdbcAnyoDesde.SetFocus
        Exit Sub
    Else
        If val(sdbcAnyoDesde) = val(sdbcAnyoHasta) Then
            If sdbcMesHasta.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1 > sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1 Then
                sdbgRes.RemoveAll
                oMensajes.PeriodoNoValido
                If Me.Visible Then sdbcMesDesde.SetFocus
                Exit Sub
            End If
        End If
    End If

    If bRUO Then
        Select Case gParametrosGenerales.giNEO
    
            Case 1
                    If sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
            Case 2
                    If m_iNivelUO > 0 And sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 1 And sUON2 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
            Case 3
                    If m_iNivelUO > 0 And sUON1 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 1 And sUON2 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    ElseIf m_iNivelUO > 2 And sUON3 = "" Then
                        oMensajes.InfAhorroUONoValido2
                        Exit Sub
                    End If
        End Select
    End If

    Screen.MousePointer = vbHourglass
    If sUON3 <> "" Then
        Set ADORs = oGestorInformes.AhorroAplicadoUO(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sUON1, sUON2, sUON3, optReu.Value, optDir.Value, True)
    Else
        If sUON2 <> "" Then
            Set ADORs = oGestorInformes.AhorroAplicadoUO(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sUON1, sUON2, , optReu.Value, optDir.Value, True)
        Else
            If sUON1 <> "" Then
                Set ADORs = oGestorInformes.AhorroAplicadoUO(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sUON1, , , optReu.Value, optDir.Value, True)
            Else
                If bRUO Then
                    If basOptimizacion.gUON1Usuario <> "" Then
                        oMensajes.InfAhorroUONoValido
                    Else
                        Set ADORs = oGestorInformes.AhorroAplicadoUO(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, , , , optReu.Value, optDir.Value, True)
                    End If
                Else
                    Set ADORs = oGestorInformes.AhorroAplicadoUO(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, , , , optReu.Value, optDir.Value, True)
                End If
            End If
        End If
    End If

    Screen.MousePointer = vbNormal
    
    On Error GoTo Error:
    
    If ADORs Is Nothing Then Exit Sub
    
    CargarGrid
    
    If cmdGrid.Visible = True Then
        MSChart1.Visible = True
        MostrarGrafico sdbcTipoGrafico.Value
    End If
        
Error:
    
    Exit Sub
            
End Sub

Private Sub CargarGrid()
Dim dpres As Double
Dim dadj As Double

    sdbgRes.RemoveAll
    sdbgTotales.RemoveAll
        
    While Not ADORs.EOF
    
        sdbgRes.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & ADORs(5).Value
        dpres = dpres + dequivalencia * ADORs(2).Value
        dadj = dadj + dequivalencia * ADORs(3).Value
        ADORs.MoveNext
    Wend
    ADORs.Close
    Set ADORs = Nothing
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj
    
    
End Sub

Private Sub cmdBorrar_Click()
    lblUO = ""
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
End Sub

Private Sub cmdGrafico_Click()
            
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
        picTipoGrafico.Visible = True
        MostrarGrafico sIdiTipoGrafico(2)
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal

End Sub

Private Sub cmdGrid_Click()
    
    picTipoGrafico.Visible = False
    picLegend.Visible = False
    picLegend2.Visible = False
        
    cmdGrafico.Visible = True
    cmdGrid.Visible = False
    sdbgRes.Visible = True
    MSChart1.Visible = False
End Sub
Private Sub cmdImprimir_Click()
Dim iNivel As Integer
Dim i As Integer
Dim iInd As Integer

    Set ofrmLstApliUO = New frmLstINFAhorrosApl
    ofrmLstApliUO.sOrigen = "frmInfAhorroApliUO"
    
    ofrmLstApliUO.sdbcAnyoDesde.Text = sdbcAnyoDesde.Text
    ofrmLstApliUO.sdbcAnyoHasta.Text = sdbcAnyoHasta.Text
    
    ofrmLstApliUO.sdbcMesDesde.MoveFirst
    For iInd = 1 To sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark)
        ofrmLstApliUO.sdbcMesDesde.MoveNext
    Next
    ofrmLstApliUO.sdbcMesDesde.Text = ofrmLstApliUO.sdbcMesDesde.Columns(0).Value
    
    ofrmLstApliUO.sdbcMesHasta.MoveFirst
    For iInd = 1 To sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark)
        ofrmLstApliUO.sdbcMesHasta.MoveNext
    Next
    ofrmLstApliUO.sdbcMesHasta.Text = ofrmLstApliUO.sdbcMesHasta.Columns(0).Value
    
    ofrmLstApliUO.optReu.Value = optReu.Value
    ofrmLstApliUO.optTodos.Value = optTodos.Value
    ofrmLstApliUO.optDir.Value = optDir.Value
    ofrmLstApliUO.sdbcMon.Text = sdbcMon.Text
    ofrmLstApliUO.sdbcMon_Validate False
    
    iNivel = 1
    If sUON1 <> "" Then
        ofrmLstApliUO.sUON1 = sUON1
        iNivel = 1
    End If
    If sUON2 <> "" Then
        ofrmLstApliUO.sUON2 = sUON2
        iNivel = 2
    End If
    If sUON3 <> "" Then
        ofrmLstApliUO.sUON3 = sUON3
        iNivel = 3
    End If
    ofrmLstApliUO.txtEst = lblUO
    
    ofrmLstApliUO.cmbNivel.clear
    For i = gParametrosGenerales.giNEO To iNivel Step -1
        If iNivel = 0 Then Exit For
        ofrmLstApliUO.cmbNivel.AddItem i
    Next i
    ofrmLstApliUO.cmbNivel.ListIndex = 0
    If iNivel = 1 Then
        ofrmLstApliUO.cmbNivel.Text = "1"
    Else
        If iNivel = 3 Then
            ofrmLstApliUO.cmbNivel.Text = "3"
        Else
            ofrmLstApliUO.cmbNivel.Text = iNivel + 1
        End If
    End If
    
    ofrmLstApliUO.Show 1
End Sub

Private Sub cmdSelProy_Click()
    
    frmSELUO.sOrigen = "InfAhorroApliUO"
    frmSELUO.bRUO = bRUO
    frmSELUO.bMostrarBajas = True
    frmSELUO.Hide
    frmSELUO.Show 1
    
End Sub

Private Sub Form_Load()
    
    Me.Width = 11835
    Me.Height = 5895
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
   
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    PonerFieldSeparator Me
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If
     
    CargarRecursos
        
    ConfigurarSeguridad
    
    CargarAnyos
        
End Sub
Private Sub BorrarDatosTotales()
    
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh

End Sub

Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyoDesde.AddItem iInd
        sdbcAnyoHasta.AddItem iInd
    Next
    
    sdbcAnyoDesde.Text = iAnyoActual
    sdbcAnyoDesde.ListAutoPosition = True
    sdbcAnyoDesde.Scroll 1, 7
    sdbcAnyoHasta.Text = iAnyoActual
    sdbcAnyoHasta.ListAutoPosition = True
    sdbcAnyoHasta.Scroll 1, 7
    sdbcMesDesde.Text = "1"
    sdbcMesDesde.ListAutoPosition = True
    sdbcMesHasta.Text = Month(Date) - 1
    sdbcMesHasta.ListAutoPosition = True
    
    sdbcMesDesde.AddItem sIdiMeses(1)
    sdbcMesDesde.AddItem sIdiMeses(2)
    sdbcMesDesde.AddItem sIdiMeses(3)
    sdbcMesDesde.AddItem sIdiMeses(4)
    sdbcMesDesde.AddItem sIdiMeses(5)
    sdbcMesDesde.AddItem sIdiMeses(6)
    sdbcMesDesde.AddItem sIdiMeses(7)
    sdbcMesDesde.AddItem sIdiMeses(8)
    sdbcMesDesde.AddItem sIdiMeses(9)
    sdbcMesDesde.AddItem sIdiMeses(10)
    sdbcMesDesde.AddItem sIdiMeses(11)
    sdbcMesDesde.AddItem sIdiMeses(12)
    
    sdbcMesHasta.AddItem sIdiMeses(1)
    sdbcMesHasta.AddItem sIdiMeses(2)
    sdbcMesHasta.AddItem sIdiMeses(3)
    sdbcMesHasta.AddItem sIdiMeses(4)
    sdbcMesHasta.AddItem sIdiMeses(5)
    sdbcMesHasta.AddItem sIdiMeses(6)
    sdbcMesHasta.AddItem sIdiMeses(7)
    sdbcMesHasta.AddItem sIdiMeses(8)
    sdbcMesHasta.AddItem sIdiMeses(9)
    sdbcMesHasta.AddItem sIdiMeses(10)
    sdbcMesHasta.AddItem sIdiMeses(11)
    sdbcMesHasta.AddItem sIdiMeses(12)
    
    sdbcMesDesde.MoveFirst
    sdbcMesDesde.Text = sdbcMesDesde.Columns(0).Value
    
    sdbcMesHasta.MoveFirst
    
    For iInd = 1 To Month(Date) - 2
        sdbcMesHasta.MoveNext
    Next
    
    sdbcMesHasta.Text = sdbcMesHasta.Columns(0).Value
    

    
End Sub

Private Sub Form_Resize()
     
Dim W As Double
    
    If Me.Width > 150 Then
            
        W = sdbgRes.Width
        
        sdbgRes.Width = Me.Width - 150
        
        sdbgRes.Columns(0).Width = (sdbgRes.Width) * (sdbgRes.Columns(0).Width / W) '* 0.15
        sdbgRes.Columns(1).Width = (sdbgRes.Width) * (sdbgRes.Columns(1).Width / W) '* 0.2
        sdbgRes.Columns(2).Width = (sdbgRes.Width) * (sdbgRes.Columns(2).Width / W) '* 0.15
        sdbgRes.Columns(3).Width = (sdbgRes.Width) * (sdbgRes.Columns(3).Width / W) '* 0.15
        sdbgRes.Columns(4).Width = (sdbgRes.Width) * (sdbgRes.Columns(4).Width / W) '* 0.15
        sdbgRes.Columns(5).Width = (sdbgRes.Width) * (sdbgRes.Columns(5).Width / W) '* 0.1
        sdbgRes.Columns(6).Width = (sdbgRes.Width) * (sdbgRes.Columns(6).Width / W) '* 0.1 - 500
        
        
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Top = Me.Height - 780
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 7
        
        MSChart1.Width = Me.Width - 150
        
    End If
    
    If Me.Height > 2565 Then
        sdbgRes.Height = Me.Height - 2190
        MSChart1.Height = sdbgRes.Height + 300
    End If
    
  
End Sub

Public Sub MostrarUOSeleccionada()
    
    If frmSELUO.sUON3 <> "" Then
        lblUO = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = frmSELUO.sUON3
        
        Exit Sub
    End If
    
    If frmSELUO.sUON2 <> "" Then
        lblUO = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = ""
        Exit Sub
    End If
    
    If frmSELUO.sUON1 <> "" Then
        lblUO = frmSELUO.sUON1 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = ""
        sUON3 = ""
        Exit Sub
    End If
    
    lblUO = ""
    lblUO.Refresh
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oMonedas = Nothing
    Set oMon = Nothing
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    sDen = ""
    Me.Visible = False
    
End Sub

Private Sub lblUO_Change()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub optDir_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optReu_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optTodos_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_BtnClick()
        
Dim frm As frmInfAhorroApliDet
Dim ADORs As Ador.Recordset
            
    If sdbgRes.Rows = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If sUON3 <> "" Then
        Set ADORs = oGestorInformes.AhorroAplicadouoHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sUON1, sUON2, sUON3, optReu.Value, optDir.Value)
    Else
        If sUON2 <> "" Then
            Set ADORs = oGestorInformes.AhorroAplicadouoHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sUON1, sUON2, sdbgRes.Columns(0).Text, optReu.Value, optDir.Value)
        Else
            If sUON1 <> "" Then
                Set ADORs = oGestorInformes.AhorroAplicadouoHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sUON1, sdbgRes.Columns(0).Text, , optReu.Value, optDir.Value)
            Else
                Set ADORs = oGestorInformes.AhorroAplicadouoHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sdbgRes.Columns(0).Text, , , optReu.Value, optDir.Value)
            End If
        End If
    End If
    
    If Not ADORs Is Nothing Then
        Set frm = New frmInfAhorroApliDet
        sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
        frm.caption = sIdiHistRes
        
        While Not ADORs.EOF
            frm.sdbgRes.AddItem ADORs(0).Value & " - " & ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & ADORs(5).Value
            ADORs.MoveNext
        Wend
    End If
        
    frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 1000
    frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
        
    Screen.MousePointer = vbNormal
        
    frm.Show 1
    sdbgRes.SelBookmarks.RemoveAll
        
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
    

End Sub

Private Sub sdbgRes_DblClick()
    
    If sdbgRes.Row < 0 Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If sUON3 <> "" Then
            
    Else
        If sUON2 <> "" Then
            sUON3 = sdbgRes.Columns(0).Value
            lblUO = sUON1 & " - " & sUON2 & " - " & sUON3 & "  " & sdbgRes.Columns(1).Value
            cmdActualizar_Click
            Screen.MousePointer = vbNormal
        Else
            If sUON1 <> "" Then
                sUON2 = sdbgRes.Columns(0).Value
                lblUO = sUON1 & " - " & sUON2 & " - " & sdbgRes.Columns(1).Value
                cmdActualizar_Click
                Screen.MousePointer = vbNormal
            Else
                sUON1 = sdbgRes.Columns(0).Value
                lblUO = sUON1 & " - " & sdbgRes.Columns(1).Value
                cmdActualizar_Click
                Screen.MousePointer = vbNormal
            End If
        End If
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
    If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_InitColumnProps()
    
    sdbcTipoGrafico.DataFieldList = "Column 0"
    sdbcTipoGrafico.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
'               If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
        
    
    
End Sub
Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
        
End Sub

Private Sub sdbcMon_DropDown()
    
    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
    End If
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_APLIUO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        lblAnyoDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyoHasta.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value '5
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll '10
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        fraMostrarDesde.caption = Ador(0).Value '15
        Ador.MoveNext
        sdbgRes.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        sIdiPresup = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value '20
        sIdiAhor = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(1) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(2) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(3) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(4) = Ador(0).Value '25
        Ador.MoveNext
        sIdiMeses(5) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(6) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(7) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(8) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(9) = Ador(0).Value '30
        Ador.MoveNext
        sIdiMeses(10) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(11) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(12) = Ador(0).Value
        Ador.MoveNext
        sIdiTotal = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        sIdiHistRes = Ador(0).Value
       
        Ador.Close
    
    End If


    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_APLIUO_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_APLIUO_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)


    Set Ador = Nothing



End Sub
