VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmSOLCumplimentacion 
   Caption         =   "DCumplimentacion de formulario:"
   ClientHeight    =   7590
   ClientLeft      =   1395
   ClientTop       =   2655
   ClientWidth     =   10575
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLCumplimentacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7590
   ScaleWidth      =   10575
   Begin VB.CheckBox chkPermPedDirecto 
      Caption         =   "DNo permitir realizar pedidos hasta que haya finalizado el workflow"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Visible         =   0   'False
      Width           =   10335
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgGrupos 
      Height          =   5100
      Left            =   240
      TabIndex        =   4
      Top             =   1560
      Visible         =   0   'False
      Width           =   10095
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   14
      stylesets.count =   3
      stylesets(0).Name=   "Calculado"
      stylesets(0).BackColor=   16766421
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLCumplimentacion.frx":0CB2
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Gris"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSOLCumplimentacion.frx":0D19
      stylesets(2).Name=   "Amarillo"
      stylesets(2).BackColor=   12648447
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLCumplimentacion.frx":0D35
      UseGroups       =   -1  'True
      DividerType     =   0
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Groups.Count    =   3
      Groups(0).Width =   6112
      Groups(0).Caption=   "DATO"
      Groups(0).Columns.Count=   6
      Groups(0).Columns(0).Width=   3528
      Groups(0).Columns(0).Visible=   0   'False
      Groups(0).Columns(0).Caption=   "ID_CAMPO"
      Groups(0).Columns(0).Name=   "ID_CAMPO"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(1).Width=   6112
      Groups(0).Columns(1).Caption=   "DATO"
      Groups(0).Columns(1).Name=   "DATO"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(2).Width=   1535
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "TIPO"
      Groups(0).Columns(2).Name=   "TIPO"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(3).Width=   4048
      Groups(0).Columns(3).Visible=   0   'False
      Groups(0).Columns(3).Caption=   "CAMPO_GS"
      Groups(0).Columns(3).Name=   "CAMPO_GS"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   8
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(4).Width=   3069
      Groups(0).Columns(4).Visible=   0   'False
      Groups(0).Columns(4).Caption=   "SUBTIPO"
      Groups(0).Columns(4).Name=   "SUBTIPO"
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   8
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(5).Width=   3069
      Groups(0).Columns(5).Visible=   0   'False
      Groups(0).Columns(5).Caption=   "PESO"
      Groups(0).Columns(5).Name=   "PESO"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).DataType=   11
      Groups(0).Columns(5).FieldLen=   256
      Groups(1).Width =   3360
      Groups(1).Caption=   "SOLICITANTE@@"
      Groups(1).Columns.Count=   4
      Groups(1).Columns(0).Width=   1111
      Groups(1).Columns(0).Caption=   "VISIBLE_S"
      Groups(1).Columns(0).Name=   "VISIBLE_S"
      Groups(1).Columns(0).DataField=   "Column 6"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Style=   2
      Groups(1).Columns(1).Width=   1111
      Groups(1).Columns(1).Caption=   "ESCRITURA_S"
      Groups(1).Columns(1).Name=   "ESCRITURA_S"
      Groups(1).Columns(1).DataField=   "Column 7"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Style=   2
      Groups(1).Columns(2).Width=   1138
      Groups(1).Columns(2).Caption=   "OBL_S"
      Groups(1).Columns(2).Name=   "OBL_S"
      Groups(1).Columns(2).DataField=   "Column 8"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Style=   2
      Groups(1).Columns(3).Width=   1614
      Groups(1).Columns(3).Visible=   0   'False
      Groups(1).Columns(3).Caption=   "ID"
      Groups(1).Columns(3).Name=   "ID_S"
      Groups(1).Columns(3).DataField=   "Column 9"
      Groups(1).Columns(3).DataType=   8
      Groups(1).Columns(3).FieldLen=   256
      Groups(2).Width =   3360
      Groups(2).Visible=   0   'False
      Groups(2).Caption=   "PROVEEDORES@@"
      Groups(2).Columns.Count=   4
      Groups(2).Columns(0).Width=   1111
      Groups(2).Columns(0).Caption=   "VISIBLE"
      Groups(2).Columns(0).Name=   "VISIBLE_P"
      Groups(2).Columns(0).DataField=   "Column 10"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(0).Style=   2
      Groups(2).Columns(1).Width=   1111
      Groups(2).Columns(1).Caption=   "ESCRITURA"
      Groups(2).Columns(1).Name=   "ESCRITURA_P"
      Groups(2).Columns(1).DataField=   "Column 11"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(1).Style=   2
      Groups(2).Columns(2).Width=   1138
      Groups(2).Columns(2).Caption=   "OBL"
      Groups(2).Columns(2).Name=   "OBL_P"
      Groups(2).Columns(2).DataField=   "Column 12"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(2).Columns(2).Style=   2
      Groups(2).Columns(3).Width=   1614
      Groups(2).Columns(3).Visible=   0   'False
      Groups(2).Columns(3).Caption=   "ID_P"
      Groups(2).Columns(3).Name=   "ID_P"
      Groups(2).Columns(3).DataField=   "Column 13"
      Groups(2).Columns(3).DataType=   8
      Groups(2).Columns(3).FieldLen=   256
      _ExtentX        =   17815
      _ExtentY        =   8996
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPasos 
      Height          =   5100
      Left            =   240
      TabIndex        =   5
      Top             =   1560
      Width           =   10095
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   12
      stylesets.count =   4
      stylesets(0).Name=   "Calculado"
      stylesets(0).BackColor=   16766421
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSOLCumplimentacion.frx":0D51
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Gris"
      stylesets(1).BackColor=   -2147483633
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSOLCumplimentacion.frx":0DB8
      stylesets(2).Name=   "Calc"
      stylesets(2).BackColor=   16766421
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSOLCumplimentacion.frx":0DD4
      stylesets(3).Name=   "Amarillo"
      stylesets(3).BackColor=   12648447
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmSOLCumplimentacion.frx":0DF0
      DividerType     =   0
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   12
      Columns(0).Width=   3200
      Columns(0).Caption=   "GRUPO"
      Columns(0).Name =   "GRUPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(1).Width=   3200
      Columns(1).Caption=   "DATO"
      Columns(1).Name =   "DATO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1402
      Columns(2).Caption=   "DVisible"
      Columns(2).Name =   "VISIBLE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   2
      Columns(3).Width=   1614
      Columns(3).Caption=   "DEscritura"
      Columns(3).Name =   "ESCRITURA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   1799
      Columns(4).Caption=   "DObligatorio"
      Columns(4).Name =   "OBL"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID_GRUPO"
      Columns(5).Name =   "ID_GRUPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID_CAMPO"
      Columns(6).Name =   "ID_CAMPO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TIPO"
      Columns(7).Name =   "TIPO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "CAMPO_GS"
      Columns(8).Name =   "CAMPO_GS"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "ID"
      Columns(9).Name =   "ID"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "SUBTIPO"
      Columns(10).Name=   "SUBTIPO"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "PESO"
      Columns(11).Name=   "PESO"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   11
      Columns(11).FieldLen=   256
      _ExtentX        =   17815
      _ExtentY        =   8996
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.TabStrip ssTabPasos 
      Height          =   5775
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   10186
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   ""
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   400
      Left            =   120
      ScaleHeight     =   405
      ScaleWidth      =   10095
      TabIndex        =   8
      Top             =   6960
      Width           =   10095
      Begin VB.CommandButton cmdBajar 
         Height          =   315
         Left            =   1100
         Picture         =   "frmSOLCumplimentacion.frx":0E0C
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   1050
      End
      Begin VB.CommandButton cmdSubir 
         Height          =   315
         Left            =   0
         Picture         =   "frmSOLCumplimentacion.frx":114E
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   1050
      End
   End
   Begin VB.CheckBox chkProve 
      Caption         =   "DUsar una configuraci�n espec�fica para proveedores"
      Height          =   255
      Left            =   6240
      TabIndex        =   2
      Top             =   600
      Width           =   4215
   End
   Begin VB.OptionButton optGrupos 
      Caption         =   "DConfigurar por grupos"
      Height          =   255
      Left            =   3400
      TabIndex        =   1
      Top             =   600
      Width           =   2500
   End
   Begin VB.OptionButton optPasos 
      Caption         =   "DConfigurar por pasos"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Value           =   -1  'True
      Width           =   2500
   End
   Begin ComctlLib.TabStrip ssTabGrupos 
      Height          =   5775
      Left            =   120
      TabIndex        =   9
      Top             =   1080
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   10186
      _Version        =   327682
      BeginProperty Tabs {0713E432-850A-101B-AFC0-4210102A8DA7} 
         NumTabs         =   1
         BeginProperty Tab1 {0713F341-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   ""
            Key             =   ""
            Object.Tag             =   ""
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Line Line2 
      Visible         =   0   'False
      X1              =   0
      X2              =   10560
      Y1              =   480
      Y2              =   480
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   10560
      Y1              =   960
      Y2              =   960
   End
End
Attribute VB_Name = "frmSOLCumplimentacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_lWorkFlow As Long
Public g_lFormulario As Long
Public g_oSolicitud As CSolicitud
Public g_bModificar As Boolean

'Variables de idiomas
Private m_sIdiWork As String
Private m_sIdiVisible As String
Private m_sIdiEscritura As String
Private m_sIdiObl As String
Private m_sIdiProveedores As String

Private m_bCargando As Boolean
Private m_bCtrlNoMoverJunto As Boolean

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_CUMPLIMENTACION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value   '1 Cumplimentaci�n de formulario
        Ador.MoveNext
        optPasos.caption = Ador(0).Value  '2 Configurar por pasos
        Ador.MoveNext
        optGrupos.caption = Ador(0).Value  '3 Configurar por grupos
        Ador.MoveNext
        chkProve.caption = Ador(0).Value  '4 Usar una configuraci�n espec�fica para proveedores
        Ador.MoveNext
        sdbgPasos.Columns("DATO").caption = Ador(0).Value  '5 Dato
        sdbgGrupos.Columns("DATO").caption = Ador(0).Value
        sdbgGrupos.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPasos.Columns("GRUPO").caption = Ador(0).Value  '6 Grupo
        Ador.MoveNext
        sdbgPasos.Columns("VISIBLE").caption = Ador(0).Value '7 Visible
        Ador.MoveNext
        sdbgPasos.Columns("ESCRITURA").caption = Ador(0).Value '8 Escritura
        Ador.MoveNext
        sdbgPasos.Columns("OBL").caption = Ador(0).Value '9 Obligatorio
        Ador.MoveNext
        m_sIdiWork = Ador(0).Value '10 Sobre workflow:
        Ador.MoveNext
        m_sIdiProveedores = Ador(0).Value ' 11 -PROVEEDORES-
        sdbgGrupos.Groups(2).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiVisible = Ador(0).Value  '12 Vis.
        sdbgGrupos.Groups(2).Columns("VISIBLE_P").caption = Ador(0).Value
        sdbgGrupos.Groups(1).Columns("VISIBLE_S").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiEscritura = Ador(0).Value  '13 Esc.
        sdbgGrupos.Groups(2).Columns("ESCRITURA_P").caption = Ador(0).Value
        sdbgGrupos.Groups(1).Columns("ESCRITURA_S").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiObl = Ador(0).Value  '14 Obl.
        sdbgGrupos.Groups(2).Columns("OBL_P").caption = Ador(0).Value
        sdbgGrupos.Groups(1).Columns("OBL_S").caption = Ador(0).Value
        Ador.MoveNext
        sdbgGrupos.Groups(1).caption = Ador(0).Value  '15 Solicitante
        ssTabPasos.Tabs(1).caption = Ador(0).Value
#If VERSION >= 30400 Then
        Ador.MoveNext
        Ador.MoveNext
        chkPermPedDirecto.caption = Ador(0).Value  '17 No permitir realizar pedidos hasta que haya finalizado el workflow
#End If
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

#If VERSION >= 30400 Then
Private Sub chkPermPedDirecto_Click()
    Dim teserror As TipoErrorSummit

    If Not m_bCargando Then
        'Se permitir�n los pedidos directos:
        teserror = g_oSolicitud.GuardarConfiguracion(chkProve.Value = vbChecked, chkPermPedDirecto.Value <> vbChecked)
                    
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        Else
            frmSOLConfiguracion.m_bRespetar = True
            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("FECACT").Value = g_oSolicitud.FECACT
            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Update
            frmSOLConfiguracion.m_bRespetar = False
        End If
    End If
End Sub
#End If

Private Sub chkProve_Click()
Dim teserror As TipoErrorSummit

    If Not m_bCargando Then
        'usar� la configuraci�n del proveedor
        teserror = g_oSolicitud.GuardarConfiguracion(chkProve.Value = vbChecked, chkPermPedDirecto.Value <> vbChecked)

              
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        Else
            frmSOLConfiguracion.m_bRespetar = True
            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("FECACT").Value = g_oSolicitud.FECACT
            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Update
            frmSOLConfiguracion.m_bRespetar = False
        End If
    End If
        
    If chkProve.Value = vbChecked Then
        'a�ade un tab para los proveedores
        ssTabPasos.Tabs.Add ssTabPasos.Tabs.Count + 1, "PROVEEDORES@@", m_sIdiProveedores
        ssTabPasos.Tabs(ssTabPasos.Tabs.Count).Tag = "PROVEEDORES@@"
        
        'Hace visible el grupo de proveedores
        sdbgGrupos.Groups(sdbgGrupos.Groups.Count - 1).Visible = True
        
    Else
        'elimina el tab para los proveedores
        ssTabPasos.Tabs.Remove ("PROVEEDORES@@")
        ssTabPasos.Tabs(1).Selected = True
        
        'Hace invisible el grupo de proveedores
        sdbgGrupos.Groups(sdbgGrupos.Groups.Count - 1).Visible = False
        
    End If
    
End Sub

''' <summary>
''' Recolocar un campo en la solicitud bajandolo
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub cmdBajar_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    Dim lGrupo As Long
    Dim Quien As Integer
    Dim vbm As Variant
    Dim bHayMasPais As Boolean
    Dim bHayMasMat As Boolean
    
    Quien = 0

    If sdbgGrupos.Visible = True Then
    
        If sdbgGrupos.SelBookmarks.Count = 0 Then Exit Sub
        If sdbgGrupos.AddItemRowIndex(sdbgGrupos.SelBookmarks.Item(0)) = sdbgGrupos.Rows - 1 Then Exit Sub
        
        LockWindowUpdate Me.hWnd
        
        i = 0
        ReDim arrValores(sdbgGrupos.Columns.Count - 1)
        ReDim arrValores2(sdbgGrupos.Columns.Count - 1)
        
        vbm = sdbgGrupos.GetBookmark(1)
        
        If Not m_bCtrlNoMoverJunto Then
            Select Case sdbgGrupos.Columns("CAMPO_GS").Value
            Case TipoCampoGS.material, TipoCampoGS.Pais
                If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    If (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Then
                        Quien = 1
                    Else
                        Quien = -1
                    End If
                ElseIf (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
                    Quien = 1
                End If
                
                sdbgGrupos.MoveNext
                sdbgGrupos.MoveNext
                If (Quien = 0) Then sdbgGrupos.MoveNext
                
                If Not (Quien = -1) Then
                    If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                    And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdSubir_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MoveNext
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdSubir_Click 'pais
                        
                        sdbgGrupos.MoveNext
                        sdbgGrupos.MoveNext
                        sdbgGrupos.MoveNext
                        sdbgGrupos.MoveNext
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdSubir_Click 'provi
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MoveNext
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    ElseIf (Quien = 1) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                    And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdSubir_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MoveNext
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    ElseIf (Quien = 1) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdSubir_Click 'den
                        
                        sdbgGrupos.MoveNext
                        sdbgGrupos.MoveNext
                        sdbgGrupos.MoveNext
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdSubir_Click 'art
                        
                        sdbgGrupos.MoveNext
                        sdbgGrupos.MoveNext
                        sdbgGrupos.MoveNext
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdSubir_Click 'mat
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MoveNext
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    Else
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MovePrevious
                        If (Quien = 0) Then sdbgGrupos.MovePrevious
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    End If
                    LockWindowUpdate 0&
                    Exit Sub
                End If
            Case TipoCampoGS.NuevoCodArticulo, TipoCampoGS.CodArticulo, TipoCampoGS.provincia
                If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    Quien = 1
                ElseIf (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Then
                    Quien = 11
                End If
                
                sdbgGrupos.MoveNext
                If (Quien = 0) Then sdbgGrupos.MoveNext
                
                If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf (Quien = 1) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 1) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'la den
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'el art
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'la mat
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf (Quien = 11) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 11) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                End If
                
                LockWindowUpdate 0&
                Exit Sub
            Case TipoCampoGS.DenArticulo
                sdbgGrupos.MoveNext
                
                If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'pais
                    
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdSubir_Click 'provi
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                End If
                LockWindowUpdate 0&
                Exit Sub
            End Select
        End If
        
        For i = 0 To sdbgGrupos.Columns.Count - 1
            arrValores(i) = sdbgGrupos.Columns(i).Value
        Next i
        sdbgGrupos.MoveNext
        
        Quien = CInt(IIf(sdbgGrupos.Columns("CAMPO_GS").Value = "", 0, sdbgGrupos.Columns("CAMPO_GS").Value))
            
        For i = 0 To sdbgGrupos.Columns.Count - 1
            arrValores2(i) = sdbgGrupos.Columns(i).Value
            sdbgGrupos.Columns(i).Value = arrValores(i)
        Next i
        
        sdbgGrupos.MovePrevious
        
        For i = 0 To sdbgGrupos.Columns.Count - 1
            sdbgGrupos.Columns(i).Value = arrValores2(i)
        Next i
            
        sdbgGrupos.SelBookmarks.RemoveAll
        sdbgGrupos.MoveNext
        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
    
    Else
    
        If sdbgPasos.SelBookmarks.Count = 0 Then Exit Sub
        If sdbgPasos.AddItemRowIndex(sdbgPasos.SelBookmarks.Item(0)) = sdbgPasos.Rows - 1 Then Exit Sub
        
        LockWindowUpdate Me.hWnd
        
        i = 0
        ReDim arrValores(sdbgPasos.Columns.Count - 1)
        ReDim arrValores2(sdbgPasos.Columns.Count - 1)
        lGrupo = sdbgPasos.Columns("ID_GRUPO").Value
        
        vbm = sdbgPasos.GetBookmark(1)
        bHayMasMat = True
        bHayMasPais = True
        
        If Not m_bCtrlNoMoverJunto Then
            Select Case sdbgPasos.Columns("CAMPO_GS").Value
            Case TipoCampoGS.material, TipoCampoGS.Pais
                If (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    If (sdbgPasos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Then
                        Quien = 1
                    Else
                        Quien = -1
                    End If
                ElseIf (sdbgPasos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
                    Quien = 1
                End If
                
                sdbgPasos.MoveNext
                vbm = sdbgPasos.Row
                sdbgPasos.MoveNext
                If vbm = sdbgPasos.Row Then bHayMasPais = False 'Pais
                vbm = sdbgPasos.Row
                If (Quien = 0) Then sdbgPasos.MoveNext
                If vbm = sdbgPasos.Row Then bHayMasMat = False 'Mat
                
                If Not (Quien = -1) Then
                    If (Quien = 0) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                    And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdSubir_Click
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MoveNext
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    ElseIf (Quien = 0) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
                    And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdSubir_Click
                        
                        sdbgPasos.MoveNext
                        sdbgPasos.MoveNext
                        sdbgPasos.MoveNext
                        sdbgPasos.MoveNext
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdSubir_Click
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MoveNext
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    ElseIf (Quien = 1) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                    And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdSubir_Click
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MoveNext
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    ElseIf (Quien = 1) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                    And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdSubir_Click
                        
                        sdbgPasos.MoveNext
                        sdbgPasos.MoveNext
                        sdbgPasos.MoveNext
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdSubir_Click
                        
                        sdbgPasos.MoveNext
                        sdbgPasos.MoveNext
                        sdbgPasos.MoveNext
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdSubir_Click
                                                
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MoveNext
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    Else
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MovePrevious
                        If bHayMasPais Then sdbgPasos.MovePrevious
                        If (Quien = 0) And bHayMasMat Then sdbgPasos.MovePrevious
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    End If
                    
                    LockWindowUpdate 0&
                    Exit Sub
                End If
            Case TipoCampoGS.NuevoCodArticulo, TipoCampoGS.CodArticulo, TipoCampoGS.provincia
                If (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    Quien = 1
                ElseIf (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Then
                    Quien = 11
                End If
                
                sdbgPasos.MoveNext
                vbm = sdbgPasos.Row
                If (Quien = 0) Then sdbgPasos.MoveNext
                If vbm = sdbgPasos.Row Then
                    bHayMasMat = False
                    bHayMasPais = False
                End If
                
                If (Quien = 0) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                ElseIf (Quien = 0) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
                And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf (Quien = 1) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                ElseIf (Quien = 1) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf (Quien = 11) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) _
                And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                ElseIf (Quien = 11) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
                And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    If (Quien = 0) And bHayMasMat Then sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                End If
                            
                LockWindowUpdate 0&
                Exit Sub
            Case TipoCampoGS.DenArticulo
                sdbgPasos.MoveNext
                
                If (Quien = 0) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                ElseIf (Quien = 0) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
                And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdSubir_Click
                    
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                End If
                           
                LockWindowUpdate 0&
                Exit Sub
            End Select
        End If
        
        For i = 0 To sdbgPasos.Columns.Count - 1
            arrValores(i) = sdbgPasos.Columns(i).Value
        Next i
                
        sdbgPasos.MoveNext
            
        If lGrupo <> sdbgPasos.Columns("ID_GRUPO").Value Then
            sdbgPasos.SelBookmarks.RemoveAll
            sdbgPasos.MovePrevious
            sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
            LockWindowUpdate 0&
            Exit Sub
        End If
        
        Quien = CInt(IIf(sdbgPasos.Columns("CAMPO_GS").Value = "", 0, sdbgPasos.Columns("CAMPO_GS").Value))
            
        For i = 0 To sdbgPasos.Columns.Count - 1
            arrValores2(i) = sdbgPasos.Columns(i).Value
            sdbgPasos.Columns(i).Value = arrValores(i)
        Next i
    
        sdbgPasos.MovePrevious
            
        For i = 0 To sdbgPasos.Columns.Count - 1
            sdbgPasos.Columns(i).Value = arrValores2(i)
        Next i
            
        sdbgPasos.SelBookmarks.RemoveAll
        sdbgPasos.MoveNext
        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
        
    End If
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos sdbgPasos.Visible
    
    Select Case Quien
    Case TipoCampoGS.material
        cmdBajar_Click
        cmdBajar_Click
    Case TipoCampoGS.Pais
        cmdBajar_Click
    End Select
    
    LockWindowUpdate 0&
End Sub

''' <summary>
''' Recolocar un campo en la solicitud subiendolo
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub cmdSubir_Click()
    Dim i As Integer
    Dim arrValores() As Variant
    Dim arrValores2() As Variant
    Dim lGrupo As Long
    Dim Quien As Integer
    Dim vbm As Variant
    Dim bHayMasPais As Boolean
    Dim bHayMasMat As Boolean
    
    Quien = 0

    If sdbgGrupos.Visible = True Then
    
        If sdbgGrupos.SelBookmarks.Count = 0 Then Exit Sub
        If sdbgGrupos.AddItemRowIndex(sdbgGrupos.SelBookmarks.Item(0)) = 0 Then Exit Sub
            
        LockWindowUpdate Me.hWnd
            
        i = 0
        ReDim arrValores(sdbgGrupos.Columns.Count - 1)
        ReDim arrValores2(sdbgGrupos.Columns.Count - 1)
        
        vbm = sdbgGrupos.GetBookmark(1)
        bHayMasMat = True
        bHayMasPais = True
        
        If Not m_bCtrlNoMoverJunto Then
            Select Case sdbgGrupos.Columns("CAMPO_GS").Value
            Case TipoCampoGS.material, TipoCampoGS.Pais
                If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    If (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Then
                        Quien = 1
                    Else
                        Quien = -1
                    End If
                ElseIf (sdbgGrupos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
                    Quien = 11
                End If
                
                sdbgGrupos.MovePrevious
                
                If Not (Quien = -1) Then
                    If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                    And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        If (Quien = 0) Then sdbgGrupos.MovePrevious
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    ElseIf (Quien = 0) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    ElseIf (Quien = 1) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                    And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    ElseIf (Quien = 1) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    ElseIf (Quien = 11) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                    And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    ElseIf (Quien = 11) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                                    
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        cmdBajar_Click
                        
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.MovePrevious
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    Else
                        sdbgGrupos.SelBookmarks.RemoveAll
                        sdbgGrupos.MoveNext
                        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    End If
                    
                    LockWindowUpdate 0&
                    Exit Sub
                End If
            Case TipoCampoGS.NuevoCodArticulo, TipoCampoGS.CodArticulo, TipoCampoGS.provincia
                If (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    Quien = 1
                ElseIf (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Then
                    Quien = 11
                End If
                
                sdbgGrupos.MovePrevious
                vbm = sdbgPasos.Row
                sdbgGrupos.MovePrevious
                If vbm = sdbgPasos.Row Then
                    bHayMasMat = False
                    bHayMasPais = False
                End If
                
                If (Quien = 0 Or Quien = 11) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    If (Quien = 0) Then sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 0 Or Quien = 11) And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    If (Quien = 0) Then sdbgGrupos.MovePrevious
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    If (Quien = 0) Then sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                ElseIf Quien = 1 And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf Quien = 1 And (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                End If
                
                LockWindowUpdate 0&
                Exit Sub
            Case TipoCampoGS.DenArticulo
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                sdbgGrupos.MovePrevious
                
                If (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                ElseIf (Quien = 0) And Not (sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Then
                    m_bCtrlNoMoverJunto = True
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.MovePrevious
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    cmdBajar_Click
                    
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MovePrevious
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                    
                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgGrupos.SelBookmarks.RemoveAll
                    sdbgGrupos.MoveNext
                    sdbgGrupos.MoveNext
                    sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
                End If
                
                LockWindowUpdate 0&
                Exit Sub
            End Select
        End If
        
        For i = 0 To sdbgGrupos.Columns.Count - 1
            arrValores(i) = sdbgGrupos.Columns(i).Value
        Next i
        sdbgGrupos.MovePrevious
        
        Quien = CInt(IIf(sdbgGrupos.Columns("CAMPO_GS").Value = "", 0, sdbgGrupos.Columns("CAMPO_GS").Value))
            
        For i = 0 To sdbgGrupos.Columns.Count - 1
            arrValores2(i) = sdbgGrupos.Columns(i).Value
            sdbgGrupos.Columns(i).Value = arrValores(i)
        Next i
        
        sdbgGrupos.MoveNext
        
        For i = 0 To sdbgGrupos.Columns.Count - 1
            sdbgGrupos.Columns(i).Value = arrValores2(i)
        Next i
            
        sdbgGrupos.SelBookmarks.RemoveAll
        sdbgGrupos.MovePrevious
        sdbgGrupos.SelBookmarks.Add sdbgGrupos.Bookmark
    
    Else
        
        If sdbgPasos.SelBookmarks.Count = 0 Then Exit Sub
        If sdbgPasos.AddItemRowIndex(sdbgPasos.SelBookmarks.Item(0)) = 0 Then Exit Sub
        
        LockWindowUpdate Me.hWnd
    
        i = 0
        lGrupo = sdbgPasos.Columns("ID_GRUPO").Value
        
        ReDim arrValores(sdbgPasos.Columns.Count - 1)
        ReDim arrValores2(sdbgPasos.Columns.Count - 1)
        
        vbm = sdbgPasos.GetBookmark(1)
        bHayMasMat = True
        bHayMasPais = True
        
        If Not m_bCtrlNoMoverJunto Then
            Select Case sdbgPasos.Columns("CAMPO_GS").Value
            Case TipoCampoGS.material, TipoCampoGS.Pais
                If (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
                    If (sdbgPasos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Then
                        Quien = 1
                    Else
                        Quien = -1
                    End If
                ElseIf (sdbgPasos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Then
                    Quien = 11
                End If
                
                vbm = sdbgPasos.Row
                sdbgPasos.MovePrevious
                If vbm = sdbgPasos.Row Then
                    bHayMasMat = False 'Mat
                    bHayMasPais = False 'Pais
                End If
                
                If Not (Quien = -1) Then
                    If (Quien = 0) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                    And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        If (Quien = 0) Then sdbgPasos.MovePrevious
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    ElseIf (Quien = 0) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                    And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        m_bCtrlNoMoverJunto = True
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click
                        
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        If (Quien = 0) Then sdbgPasos.MovePrevious
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        
                        m_bCtrlNoMoverJunto = False
                    ElseIf (Quien = 1) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) _
                    And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        If (Quien = 0) Then sdbgPasos.MovePrevious
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    ElseIf (Quien = 1) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) _
                    And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        m_bCtrlNoMoverJunto = True

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark

                        m_bCtrlNoMoverJunto = False
                    ElseIf (Quien = 11) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                    And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        If (Quien = 0) Then sdbgPasos.MovePrevious
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    ElseIf (Quien = 11) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                    And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                        m_bCtrlNoMoverJunto = True

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                        cmdBajar_Click

                        sdbgPasos.SelBookmarks.RemoveAll
                        sdbgPasos.MovePrevious
                        sdbgPasos.MovePrevious
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark

                        m_bCtrlNoMoverJunto = False
                    Else
                        sdbgPasos.SelBookmarks.RemoveAll
                        If bHayMasPais Then sdbgPasos.MoveNext
                        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    End If
                End If
                
                LockWindowUpdate 0&
                Exit Sub
            Case TipoCampoGS.NuevoCodArticulo, TipoCampoGS.CodArticulo, TipoCampoGS.provincia
                If (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) Then
                    Quien = 1
                ElseIf (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo) Then
                    Quien = 11
                End If

                sdbgPasos.MovePrevious
                vbm = sdbgPasos.Row
                sdbgPasos.MovePrevious
                If vbm = sdbgPasos.Row Then
                    bHayMasMat = False
                    bHayMasPais = False
                End If

                If Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) And (Quien = 0 Or Quien = 11) _
                And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    If (Quien = 0) Then sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                ElseIf (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) And (Quien = 0 Or Quien = 11) _
                And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    m_bCtrlNoMoverJunto = True

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious
                   If (Quien = 0) Then sdbgPasos.MovePrevious

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    If (Quien = 0) Then sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark

                    m_bCtrlNoMoverJunto = False
                ElseIf Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) And (Quien = 1) _
                And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    If (Quien = 0) Then sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                ElseIf (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) And (Quien = 1) _
                And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    m_bCtrlNoMoverJunto = True

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click
                    
                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    If (Quien = 0) Then sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark

                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    If bHayMasPais Or bHayMasMat Then sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                End If

                LockWindowUpdate 0&
                Exit Sub
            Case TipoCampoGS.DenArticulo
                sdbgPasos.MovePrevious
                sdbgPasos.MovePrevious
                vbm = sdbgPasos.Row
                sdbgPasos.MovePrevious
                If vbm = sdbgPasos.Row Then bHayMasMat = False  'Mat

                If (Quien = 0) And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.material) _
                And Not (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                ElseIf (Quien = 0) And (sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia) _
                And (lGrupo = sdbgPasos.Columns("ID_GRUPO").Value) Then
                    m_bCtrlNoMoverJunto = True

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious
                    sdbgPasos.MovePrevious

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                    cmdBajar_Click

                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MovePrevious
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark

                    m_bCtrlNoMoverJunto = False
                Else
                    sdbgPasos.SelBookmarks.RemoveAll
                    sdbgPasos.MoveNext
                    sdbgPasos.MoveNext
                    If bHayMasMat Then sdbgPasos.MoveNext
                    sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
                End If

                LockWindowUpdate 0&
                Exit Sub
            End Select
        End If
        
        For i = 0 To sdbgPasos.Columns.Count - 1
            arrValores(i) = sdbgPasos.Columns(i).Value
        Next i
        
        sdbgPasos.MovePrevious
     
        If lGrupo <> sdbgPasos.Columns("ID_GRUPO").Value Then
            sdbgPasos.SelBookmarks.RemoveAll
            sdbgPasos.MoveNext
            sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
            LockWindowUpdate 0&
            Exit Sub
        End If
        
        Quien = CInt(IIf(sdbgPasos.Columns("CAMPO_GS").Value = "", 0, sdbgPasos.Columns("CAMPO_GS").Value))
        
        For i = 0 To sdbgPasos.Columns.Count - 1
            arrValores2(i) = sdbgPasos.Columns(i).Value
            sdbgPasos.Columns(i).Value = arrValores(i)
        Next i
        
        sdbgPasos.MoveNext
        
        For i = 0 To sdbgPasos.Columns.Count - 1
            sdbgPasos.Columns(i).Value = arrValores2(i)
        Next i
            
        sdbgPasos.SelBookmarks.RemoveAll
        sdbgPasos.MovePrevious
        sdbgPasos.SelBookmarks.Add sdbgPasos.Bookmark
        
    End If
    
    
    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:
    GuardarOrdenCampos sdbgPasos.Visible
    
    Select Case Quien
    Case TipoCampoGS.DenArticulo
        cmdSubir_Click
        cmdSubir_Click
    Case TipoCampoGS.provincia
        cmdSubir_Click
    End Select
    
    LockWindowUpdate 0&
End Sub

Private Sub Form_Load()

    Me.Height = 7780
    Me.Width = 10700
    
    Dim subirControles As Boolean
    subirControles = True
    
#If VERSION >= 30400 Then
    If g_lWorkFlow <> 0 Then
        subirControles = False
    End If
#End If

    If subirControles Then
        Me.Height = Me.Height - 400
        optPasos.Top = optPasos.Top - 400
        optGrupos.Top = optGrupos.Top - 400
        chkProve.Top = chkProve.Top - 400
        Line1.Y1 = Line1.Y1 - 400
        Line1.Y2 = Line1.Y2 - 400
        ssTabPasos.Top = ssTabPasos.Top - 400
        ssTabGrupos.Top = ssTabGrupos.Top - 400
        sdbgPasos.Top = sdbgPasos.Top - 400
        sdbgGrupos.Top = sdbgGrupos.Top - 400
        picNavigate.Top = picNavigate.Top - 400
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Me.caption = Me.caption & frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("FORM").GetText(ssMaskModeRaw) & ". "
    If g_lWorkFlow <> 0 Then
        Me.caption = Me.caption & m_sIdiWork & frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("WORKFL").GetText(ssMaskModeRaw)
    End If
    
    'Comprueba si se va a usar o no la configuraci�n del proveedor y si se permiten los pedidos sin terminar el workflow
    m_bCargando = True
    g_oSolicitud.CargarConfiguracion , , , , True
#If VERSION >= 30400 Then
    If g_lWorkFlow <> 0 Then
        Me.Line2.Visible = True
        Me.chkPermPedDirecto.Visible = True
        If g_oSolicitud.PermitirPedidoDirecto = False Then
            Me.chkPermPedDirecto.Value = vbChecked
        End If
    End If
#End If
    If g_oSolicitud.CumplProveedor = True Then
        Me.chkProve.Value = vbChecked
    End If
    m_bCargando = False
    
    'Carga los tabs con los pasos y grupos correspondientes
    CargarPasos
    CargarGrupos
    
    If g_bModificar = False Then
        picNavigate.Visible = False
        
        sdbgPasos.Columns("VISIBLE").Locked = True
        sdbgPasos.Columns("ESCRITURA").Locked = True
        sdbgPasos.Columns("OBL").Locked = True
        
        sdbgGrupos.Columns("VISIBLE_P").Locked = True
        sdbgGrupos.Columns("ESCRITURA_P").Locked = True
        sdbgGrupos.Columns("OBL_P").Locked = True
        
        sdbgGrupos.Columns("VISIBLE_S").Locked = True
        sdbgGrupos.Columns("ESCRITURA_S").Locked = True
        sdbgGrupos.Columns("OBL_S").Locked = True
    End If
End Sub


Private Sub Form_Resize()
Dim dGridWith As Double
On Error Resume Next


    If Me.Width < 2000 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    Line1.X2 = Me.Width - 135
#If VERSION >= 30400 Then
    Line2.X2 = Me.Width - 135
#End If

    ssTabPasos.Width = Me.Width - 360
    
    Dim correccion As Integer
    correccion = 0
#If VERSION >= 30400 Then
    If g_lWorkFlow <> 0 Then
        correccion = 400
    End If
#End If

    
    If g_bModificar = True Then
        ssTabPasos.Height = Me.Height - (1605 + correccion)
    Else
        ssTabPasos.Height = Me.Height - (1205 + correccion)
        
    End If
    ssTabGrupos.Width = ssTabPasos.Width
    ssTabGrupos.Height = ssTabPasos.Height
    
    sdbgPasos.Width = ssTabPasos.Width - 235
    sdbgPasos.Height = ssTabPasos.Height - 675
    sdbgGrupos.Width = sdbgPasos.Width
    sdbgGrupos.Height = sdbgPasos.Height
    
    dGridWith = sdbgPasos.Width - sdbgPasos.Columns("VISIBLE").Width - sdbgPasos.Columns("ESCRITURA").Width - sdbgPasos.Columns("OBL").Width
    sdbgPasos.Columns("GRUPO").Width = dGridWith * 0.33
    sdbgPasos.Columns("DATO").Width = dGridWith * 0.6
    
    picNavigate.Top = ssTabPasos.Top + ssTabPasos.Height + 80
End Sub

Private Sub Form_Unload(Cancel As Integer)
     g_lWorkFlow = 0
     g_lFormulario = 0
     Set g_oSolicitud = Nothing
End Sub

Private Sub optGrupos_Click()
    ssTabGrupos.Visible = True
    sdbgGrupos.Visible = True
    
    ssTabPasos.Visible = False
    sdbgPasos.Visible = False
    
    ssTabGrupos.Tabs(1).Selected = True
End Sub

Private Sub optPasos_Click()
    ssTabPasos.Visible = True
    sdbgPasos.Visible = True
    
    ssTabGrupos.Visible = False
    sdbgGrupos.Visible = False
    
    ssTabPasos.Tabs(1).Selected = True
End Sub

Private Sub CargarPasos()
Dim oWorkflow As cworkflow
Dim Ador As Ador.Recordset
Dim iTab As Integer

    'Carga los pasos del workflow de la solicitud en los tabs y grids correspondientes:
    
    ssTabPasos.Tabs(1).Tag = "SOLICITANTE@@"
    sdbgGrupos.Groups(1).TagVariant = "S"
    sdbgGrupos.Groups(2).TagVariant = "P"
    
    Set oWorkflow = oFSGSRaiz.Generar_CWorkflow
    oWorkflow.Id = g_lWorkFlow
    
    'Set Ador = oWorkflow.DevolverPasos
    
    If g_lWorkFlow <> 0 Then
        iTab = 2
        
        If Not Ador Is Nothing Then
            While Not Ador.EOF
                'Carga el tab con los pasos del workflow
                ssTabPasos.Tabs.Add iTab, "A" & Ador.Fields("ID").Value, Ador.Fields("DEN").Value
                ssTabPasos.Tabs(iTab).Tag = CStr(Ador.Fields("ID").Value)
            
                'ahora carga los pasos en la grid de grupos.Cada paso ser� un grupo de la grid:
                sdbgGrupos.Groups.Add iTab
                sdbgGrupos.Groups(iTab).caption = Ador.Fields("DEN").Value
                sdbgGrupos.Groups(iTab).TagVariant = "P" & Ador.Fields("ID").Value
                sdbgGrupos.Groups(iTab).Width = 1900
                 
                sdbgGrupos.Groups(iTab).Columns.Add 0
                sdbgGrupos.Groups(iTab).Columns(0).Name = "VISIBLE_P" & Ador.Fields("ID").Value
                sdbgGrupos.Groups(iTab).Columns(0).caption = m_sIdiVisible
                sdbgGrupos.Groups(iTab).Columns(0).Style = ssStyleCheckBox
                If g_bModificar = False Then
                    sdbgGrupos.Groups(iTab).Columns(0).Locked = True
                End If
                
                sdbgGrupos.Groups(iTab).Columns.Add 1
                sdbgGrupos.Groups(iTab).Columns(1).Name = "ESCRITURA_P" & Ador.Fields("ID").Value
                sdbgGrupos.Groups(iTab).Columns(1).caption = m_sIdiEscritura
                sdbgGrupos.Groups(iTab).Columns(1).Style = ssStyleCheckBox
                If g_bModificar = False Then
                    sdbgGrupos.Groups(iTab).Columns(1).Locked = True
                End If

                sdbgGrupos.Groups(iTab).Columns.Add 2
                sdbgGrupos.Groups(iTab).Columns(2).Name = "OBL_P" & Ador.Fields("ID").Value
                sdbgGrupos.Groups(iTab).Columns(2).caption = m_sIdiObl
                sdbgGrupos.Groups(iTab).Columns(2).Style = ssStyleCheckBox
                If g_bModificar = False Then
                    sdbgGrupos.Groups(iTab).Columns(2).Locked = True
                End If
                
                sdbgGrupos.Groups(iTab).Columns.Add 3
                sdbgGrupos.Groups(iTab).Columns(3).Name = "ID_P" & Ador.Fields("ID").Value
                sdbgGrupos.Groups(iTab).Columns(3).Visible = False
                
                sdbgGrupos.Groups(iTab).Columns(0).Width = 629
                sdbgGrupos.Groups(iTab).Columns(1).Width = 629
                sdbgGrupos.Groups(iTab).Columns(2).Width = 645
                
                iTab = iTab + 1
                Ador.MoveNext
            Wend
            Ador.Close
        End If
        
    End If
    
    ssTabPasos.Tabs(1).Selected = True
    
    Set Ador = Nothing
    Set oWorkflow = Nothing
        
End Sub

Private Sub CargarGrupos()
Dim oFormulario As CFormulario
Dim iTab As Integer
Dim oGrupo As CFormGrupo

    'Carga el tab con los pasos del workflow
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = g_lFormulario
    oFormulario.CargarTodosLosGrupos
    
    ssTabGrupos.Tabs.clear
    iTab = 1
    
    For Each oGrupo In oFormulario.Grupos
        ssTabGrupos.Tabs.Add iTab, "A" & oGrupo.Id, NullToStr(oGrupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        ssTabGrupos.Tabs(iTab).Tag = CStr(oGrupo.Id)
    
        iTab = iTab + 1
    Next
    
    Set oFormulario = Nothing
    
End Sub

Private Sub sdbgGrupos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oConf As CConfCumplimentacion
        
    Set oConf = oFSGSRaiz.Generar_CConfCumplimentacion
    oConf.Id = sdbgGrupos.Columns("ID_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value
    oConf.Visible = GridCheckToBoolean(sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value)
    oConf.ESCRITURA = GridCheckToBoolean(sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value)
    oConf.Obligatorio = GridCheckToBoolean(sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value)
    
    Set oIBaseDatos = oConf
    teserror = oIBaseDatos.FinalizarEdicionModificando
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgGrupos.CancelUpdate
        If Me.Visible Then sdbgGrupos.SetFocus
        sdbgGrupos.DataChanged = False
    End If
    
    Set oConf = Nothing
    Set oIBaseDatos = Nothing
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgGrupos_BtnClick()
    'Muestra la pantalla de configuraci�n para el desglose

    If g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgGrupos.Columns("ID_S").Value)).Campo Is Nothing Then Exit Sub
    
    Set frmSolCumplDesglose.g_oCampoPadre = g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgGrupos.Columns("ID_S").Value)).Campo
    frmSolCumplDesglose.g_bModificar = g_bModificar
    frmSolCumplDesglose.Show vbModal

End Sub

Private Sub sdbgGrupos_Change()
    Dim sCol As String

    If sdbgGrupos.col < 0 Then Exit Sub
    If sdbgGrupos.Grp < 1 Then Exit Sub
                
    sCol = Left(sdbgGrupos.Columns(sdbgGrupos.col).Name, Len(sdbgGrupos.Columns(sdbgGrupos.col).Name) - Len("_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant))
    
    Select Case sCol
        Case "VISIBLE"
            If GridCheckToBoolean(sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) = False Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Los campos de no conformidad "Motivo" tienen que tener visibilidad siempre checkeado
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Los campos de no conformidad marcados como Peso tienen que tener visibilidad siempre checkeado
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("PESO").Value Then
                        sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("PESO").Value Then
                        If Not GridCheckToBoolean(sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) Then
                            sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                            sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                        End If
                    End If
                End If
                
                sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
            End If
            
        Case "ESCRITURA"
            If (sdbgGrupos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado And Not sdbgGrupos.Columns("PESO").Value) Or _
                sdbgGrupos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                Exit Sub
            End If
            
            If GridCheckToBoolean(sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) = False Then
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Los campos de no conformidad "Motivo" tienen que tener escritura siempre checkeado
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Los campos de no conformidad marcados como Peso tienen que tener escritura siempre checkeado
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("PESO").Value Then
                        sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Para proveedores, si el campo marcado como Peso se desmarca de escritura no debe ser obligatorio
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("PESO").Value Then
                        If GridCheckToBoolean(sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) Then
                            sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                        End If
                    End If
                End If
                
                sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
            Else
                If sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = False Then
                    sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                    Exit Sub
                End If
                
                'Para proveedores, si el campo marcado como Peso se marca de escritura debe ser obligatorio
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("PESO").Value Then
                        If GridCheckToBoolean(sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) Then
                            sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        End If
                    End If
                End If
            End If
            
        Case "OBL"
            If (sdbgGrupos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado And Not sdbgGrupos.Columns("PESO").Value) Or _
                sdbgGrupos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                Exit Sub
            End If
            
            If GridCheckToBoolean(sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value) = True Then
                If sdbgGrupos.Columns("VISIBLE_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = False Or sdbgGrupos.Columns("ESCRITURA_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = False Then
                    sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "0"
                    Exit Sub
                End If
            Else
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "P" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Los campos de no conformidad "Motivo" tienen que tener obligatorio siempre checkeado
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Los campos de no conformidad marcados como Peso tienen que tener obligatorio siempre checkeado
                If sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant = "S" Then
                    If sdbgGrupos.Columns("PESO").Value Then
                        sdbgGrupos.Columns("OBL_" & sdbgGrupos.Groups(sdbgGrupos.Grp).TagVariant).Value = "1"
                        Exit Sub
                    End If
                End If
            End If
    End Select
    
    sdbgGrupos.Update
End Sub

Private Sub sdbgGrupos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    If sdbgGrupos.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or sdbgGrupos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
        sdbgGrupos.Columns("DATO").Style = ssStyleEditButton
        If Me.Visible Then Me.SetFocus
        If Me.Visible And sdbgGrupos.Visible Then sdbgGrupos.SetFocus
    Else
        sdbgGrupos.Columns("DATO").Style = ssStyleEdit
    End If
    
End Sub

Private Sub sdbgGrupos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgGrupos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgGrupos.Columns("DATO").CellStyleSet "Amarillo"
    ElseIf sdbgGrupos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgGrupos.Columns("DATO").CellStyleSet "Calculado"
    Else
        sdbgGrupos.Columns("DATO").CellStyleSet ""
    End If
End Sub

Private Sub sdbgPasos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oConf As CConfCumplimentacion
    
    Set oConf = oFSGSRaiz.Generar_CConfCumplimentacion
    oConf.Id = sdbgPasos.Columns("ID").Value
    oConf.Visible = GridCheckToBoolean(sdbgPasos.Columns("VISIBLE").Value)
    oConf.ESCRITURA = GridCheckToBoolean(sdbgPasos.Columns("ESCRITURA").Value)
    oConf.Obligatorio = GridCheckToBoolean(sdbgPasos.Columns("OBL").Value)
    
    Set oIBaseDatos = oConf
    teserror = oIBaseDatos.FinalizarEdicionModificando
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        sdbgPasos.CancelUpdate
        If Me.Visible Then sdbgPasos.SetFocus
        sdbgPasos.DataChanged = False
    End If
    
    Set oConf = Nothing
    Set oIBaseDatos = Nothing
        
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbgPasos_BtnClick()
    'Muestra la pantalla de configuraci�n para el desglose
    If g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgPasos.Columns("ID").Value)).Campo Is Nothing Then Exit Sub
    
    Set frmSolCumplDesglose.g_oCampoPadre = g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgPasos.Columns("ID").Value)).Campo
    frmSolCumplDesglose.g_bModificar = g_bModificar
    frmSolCumplDesglose.Show vbModal
End Sub

Private Sub sdbgPasos_Change()
    If sdbgPasos.col < 0 Then Exit Sub
        
    Select Case sdbgPasos.Columns(sdbgPasos.col).Name
        Case "VISIBLE"
            If GridCheckToBoolean(sdbgPasos.Columns("VISIBLE").Value) = False Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If Me.ssTabPasos.selectedItem.Tag = "PROVEEDORES@@" Then
                    If sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgPasos.Columns("VISIBLE").Value = "1"
                        Exit Sub
                    End If
                End If
                                
                If Me.ssTabPasos.selectedItem.Tag = "SOLICITANTE@@" Then
                    'si es un campo de tipo no conformidad motivo no se puede quitar la visibilidad:
                    If sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgPasos.Columns("VISIBLE").Value = "1"
                        Exit Sub
                    End If
                    
                    'si es un campo marcado como peso no se puede quitar la visibilidad:
                    If sdbgPasos.Columns("PESO").Value Then
                        sdbgPasos.Columns("VISIBLE").Value = "1"
                        Exit Sub
                    End If
                End If
                
                If Me.ssTabPasos.selectedItem.Tag = "PROVEEDORES@@" Then
                    'si es un campo marcado como peso no se puede quitar la visibilidad:
                    If sdbgPasos.Columns("PESO").Value Then
                        If Not GridCheckToBoolean(sdbgPasos.Columns("VISIBLE").Value) Then
                            sdbgPasos.Columns("ESCRITURA").Value = "0"
                            sdbgPasos.Columns("OBL").Value = "0"
                        End If
                    End If
                End If
                    
                sdbgPasos.Columns("ESCRITURA").Value = "0"
                sdbgPasos.Columns("OBL").Value = "0"
            End If
            
        Case "ESCRITURA"
            If (sdbgPasos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado And Not sdbgPasos.Columns("PESO").Value) Or _
                sdbgPasos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                sdbgPasos.Columns("ESCRITURA").Value = "0"
                Exit Sub
            End If
                
            If GridCheckToBoolean(sdbgPasos.Columns("ESCRITURA").Value) = False Then
                'Los campos de certificado "Certificado" tienen que tener obligatorio siempre checkeado:
                If Me.ssTabPasos.selectedItem.Tag = "PROVEEDORES@@" Then
                    If sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgPasos.Columns("ESCRITURA").Value = "1"
                        Exit Sub
                    End If
                End If
                                
                If Me.ssTabPasos.selectedItem.Tag = "SOLICITANTE@@" Then
                    'si es un campo de tipo no conformidad motivo no se puede quitar el permiso de escritura
                    If sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgPasos.Columns("ESCRITURA").Value = "1"
                        Exit Sub
                    End If
                    
                    'si es un campo marcado como peso no se puede quitar el permiso de escritura
                    If sdbgPasos.Columns("PESO").Value Then
                        sdbgPasos.Columns("ESCRITURA").Value = "1"
                        Exit Sub
                    End If
                End If
                
                'Para proveedores, si el campo marcado como Peso se desmarca de escritura no debe ser obligatorio
                If Me.ssTabPasos.selectedItem.Tag = "PROVEEDORES@@" Then
                    If sdbgPasos.Columns("PESO").Value Then
                        sdbgPasos.Columns("OBL").Value = "0"
                        Exit Sub
                    End If
                End If
                
                sdbgPasos.Columns("OBL").Value = "0"
            Else
                If GridCheckToBoolean(sdbgPasos.Columns("VISIBLE").Value) = False Then
                    sdbgPasos.Columns("ESCRITURA").Value = "0"
                    Exit Sub
                End If
                
                'Para proveedores, si el campo marcado como Peso se marca de escritura debe ser obligatorio
                If Me.ssTabPasos.selectedItem.Tag = "PROVEEDORES@@" Then
                    If sdbgPasos.Columns("PESO").Value Then
                        sdbgPasos.Columns("OBL").Value = "1"
                        Exit Sub
                    End If
                End If
            End If
            
        Case "OBL"
            If (sdbgPasos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado And Not sdbgPasos.Columns("PESO").Value) Or _
                sdbgPasos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                sdbgPasos.Columns("OBL").Value = "0"
                Exit Sub
            End If
            
            If GridCheckToBoolean(sdbgPasos.Columns("OBL").Value) = True Then
                If GridCheckToBoolean(sdbgPasos.Columns("VISIBLE").Value) = False Or GridCheckToBoolean(sdbgPasos.Columns("ESCRITURA").Value) = False Then
                    sdbgPasos.Columns("OBL").Value = "0"
                    Exit Sub
                End If
            Else
                If Me.ssTabPasos.selectedItem.Tag = "PROVEEDORES@@" Then
                    'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                    If sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Then
                        sdbgPasos.Columns("OBL").Value = "1"
                        Exit Sub
                    End If
                End If
                
                If Me.ssTabPasos.selectedItem.Tag = "SOLICITANTE@@" Then
                    'si es un campo de tipo no conformidad motivo tiene que tener obligatorio siempre checkeado:
                    If sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgPasos.Columns("OBL").Value = "1"
                        Exit Sub
                    End If
                    
                    'si es un campo marcado como peso tiene que tener obligatorio siempre checkeado:
                    If sdbgPasos.Columns("PESO").Value Then
                        sdbgPasos.Columns("OBL").Value = "1"
                        Exit Sub
                    End If
                End If
            End If
    End Select
    
    sdbgPasos.Update
End Sub

Private Sub sdbgPasos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgPasos.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or sdbgPasos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
        sdbgPasos.Columns("DATO").Style = ssStyleEditButton
        If Me.Visible Then Me.SetFocus
        If sdbgPasos.Visible Then sdbgPasos.SetFocus
    Else
        sdbgPasos.Columns("DATO").Style = ssStyleEdit
    End If
End Sub

Private Sub sdbgPasos_RowLoaded(ByVal Bookmark As Variant)
    
    If sdbgPasos.Columns("VISIBLE").CellValue(Bookmark) = False Then
        sdbgPasos.Columns("GRUPO").CellStyleSet "Gris"
        sdbgPasos.Columns("DATO").CellStyleSet "Gris"
        
    ElseIf sdbgPasos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgPasos.Columns("GRUPO").CellStyleSet "Amarillo"
        sdbgPasos.Columns("DATO").CellStyleSet "Amarillo"
    ElseIf sdbgPasos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgPasos.Columns("GRUPO").CellStyleSet "Calc"
        sdbgPasos.Columns("DATO").CellStyleSet "Calculado"
    Else
        sdbgPasos.Columns("GRUPO").CellStyleSet ""
        sdbgPasos.Columns("DATO").CellStyleSet ""
    End If

End Sub

Private Sub SSTabGrupos_Click()
Dim oConf As CConfCumplimentacion
Dim sCadena As String
Dim sCadenaS As String
Dim sCadenaP As String
Dim sCadenaPasos As String
Dim lCampo As Long
Dim ogroup As SSDataWidgets_B.Group

    'Carga la configuraci�n del paso seleccionado
    Screen.MousePointer = vbHourglass

    g_oSolicitud.CargarConfiguracion , ssTabGrupos.selectedItem.Tag

    sdbgGrupos.RemoveAll
    
    lCampo = 0
    For Each oConf In g_oSolicitud.Cumplimentaciones
        If oConf.Campo.Id <> lCampo Then
            lCampo = oConf.Campo.Id
            If sCadena <> "" Then sdbgGrupos.AddItem sCadena & sCadenaS & sCadenaPasos & sCadenaP
            
            sCadenaS = ""
            sCadenaP = ""
            sCadenaPasos = ""
            sCadena = oConf.Campo.Id & Chr(m_lSeparador) & oConf.Campo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sCadena = sCadena & Chr(m_lSeparador) & oConf.Campo.TipoPredef & Chr(m_lSeparador) & oConf.Campo.CampoGS & Chr(m_lSeparador) & oConf.Campo.Tipo & Chr(m_lSeparador) & oConf.Campo.Peso
        End If
        
        For Each ogroup In sdbgGrupos.Groups
            If ogroup.TagVariant = "S" And oConf.SolicitanteProv = ConfCumpliment.ConfSolicitante Then
                sCadenaS = sCadenaS & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.ESCRITURA) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                Exit For
            ElseIf ogroup.TagVariant = "P" And oConf.SolicitanteProv = ConfCumpliment.ConfProveedor Then
                sCadenaP = sCadenaP & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.ESCRITURA) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                Exit For
            ElseIf ogroup.TagVariant = "P" & oConf.Paso Then
                sCadenaPasos = sCadenaPasos & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.ESCRITURA) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio) & Chr(m_lSeparador) & oConf.Id
                Exit For
            End If
        Next
    Next
    
    If sCadena <> "" Then sdbgGrupos.AddItem sCadena & sCadenaS & sCadenaPasos & sCadenaP

    sdbgGrupos.MoveFirst
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub ssTabPasos_Click()
Dim oConf As CConfCumplimentacion
Dim sCadena As String

    'Carga la configuraci�n del paso seleccionado
    Screen.MousePointer = vbHourglass
    
    Select Case ssTabPasos.selectedItem.Tag
        Case "PROVEEDORES@@"
            g_oSolicitud.CargarConfiguracion , , ConfCumpliment.ConfProveedor
        Case "SOLICITANTE@@"
            g_oSolicitud.CargarConfiguracion , , ConfCumpliment.ConfSolicitante
        Case Else
            g_oSolicitud.CargarConfiguracion ssTabPasos.selectedItem.Tag
    End Select
    
    sdbgPasos.RemoveAll
    
    For Each oConf In g_oSolicitud.Cumplimentaciones
        sCadena = NullToStr(oConf.Campo.Grupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        sCadena = sCadena & Chr(m_lSeparador) & oConf.Campo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sCadena = sCadena & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Visible) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.ESCRITURA) & Chr(m_lSeparador) & BooleanToSQLBinary(oConf.Obligatorio)
        sCadena = sCadena & Chr(m_lSeparador) & oConf.Campo.Grupo.Id & Chr(m_lSeparador) & oConf.Campo.Id & Chr(m_lSeparador) & oConf.Campo.TipoPredef & Chr(m_lSeparador) & oConf.Campo.CampoGS
        sCadena = sCadena & Chr(m_lSeparador) & oConf.Id & Chr(m_lSeparador) & oConf.Campo.Tipo & Chr(m_lSeparador) & oConf.Campo.Peso
        
        sdbgPasos.AddItem sCadena
    Next

    sdbgPasos.MoveFirst
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub GuardarOrdenCampos(ByVal bPasos As Boolean)
Dim oConfs As CConfCumplimentaciones
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim vbm As Variant
Dim ogroup As SSDataWidgets_B.Group
Dim lID As Long

    'Comprueba los cambios de orden que se han producido y almacena en la base de datos:

    Set oConfs = oFSGSRaiz.Generar_CConfCumplimentaciones

    If bPasos = True Then
        For i = 0 To sdbgPasos.Rows - 1
            vbm = sdbgPasos.AddItemBookmark(i)
            'Guarda en BD solo el orden de los campos que han cambiado
            If NullToDbl0(g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgPasos.Columns("ID").CellValue(vbm))).Orden) <> i + 1 Then
                g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgPasos.Columns("ID").CellValue(vbm))).Orden = i + 1
                oConfs.Add sdbgPasos.Columns("ID").CellValue(vbm), g_oSolicitud, g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgPasos.Columns("ID").CellValue(vbm))).Campo, , , , , , , g_oSolicitud.Cumplimentaciones.Item(CStr(sdbgPasos.Columns("ID").CellValue(vbm))).Orden
            End If
        Next i
    
    Else
        For i = 0 To sdbgGrupos.Rows - 1
            vbm = sdbgGrupos.AddItemBookmark(i)
    
            For Each ogroup In sdbgGrupos.Groups
                If ogroup.TagVariant <> "" Then
                    lID = ogroup.Columns("ID_" & ogroup.TagVariant).CellValue(vbm)
                    'Guarda en BD solo el orden de los campos que han cambiado
                    If NullToDbl0(g_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Orden) <> i + 1 Then
                        g_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Orden = i + 1
                        oConfs.Add lID, g_oSolicitud, g_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Campo, , , , , , , g_oSolicitud.Cumplimentaciones.Item(CStr(lID)).Orden
                    End If
                End If
            Next
        Next i
    End If

    teserror = oConfs.GuardarOrdenCampos(bPasos)

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    Set oConfs = Nothing
End Sub


