VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSELPresAnuUON 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   5925
   ClientLeft      =   2175
   ClientTop       =   3210
   ClientWidth     =   7080
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELPresAnuUON.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5925
   ScaleWidth      =   7080
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3090
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":0CB2
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":107A
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":13CE
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":1722
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":1AB6
            Key             =   "UON1A"
            Object.Tag             =   "UON1A"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":1E08
            Key             =   "UON2A"
            Object.Tag             =   "UON2A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":215A
            Key             =   "UON3A"
            Object.Tag             =   "UON3A"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":24AC
            Key             =   "UON1B"
            Object.Tag             =   "UON1B"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":27FE
            Key             =   "UON2B"
            Object.Tag             =   "UON2B"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":2B50
            Key             =   "UON3B"
            Object.Tag             =   "UON3B"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdSeleccionar 
      Caption         =   "&Seleccionar"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2400
      TabIndex        =   14
      Top             =   5550
      Width           =   1005
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3480
      TabIndex        =   13
      Top             =   5550
      Width           =   1005
   End
   Begin TabDlg.SSTab SSTabPresupuestos 
      Height          =   5445
      Left            =   60
      TabIndex        =   0
      Top             =   30
      Width           =   6975
      _ExtentX        =   12303
      _ExtentY        =   9604
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   8421376
      TabCaption(0)   =   "DUnidad organizativa"
      TabPicture(0)   =   "frmSELPresAnuUON.frx":2EA2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblEstrorg1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "tvwestrorg"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "DPresupuestos"
      TabPicture(1)   =   "frmSELPresAnuUON.frx":2EBE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picSepar"
      Tab(1).Control(1)=   "tvwEstrPres"
      Tab(1).Control(2)=   "sdbcAnyo"
      Tab(1).Control(3)=   "lblAnyo"
      Tab(1).Control(4)=   "lblEstrorg2"
      Tab(1).ControlCount=   5
      Begin VB.PictureBox picSepar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   -74910
         ScaleHeight     =   375
         ScaleWidth      =   6675
         TabIndex        =   1
         Top             =   4905
         Width           =   6735
         Begin VB.TextBox txtPartida 
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   45
            Width           =   2775
         End
         Begin VB.TextBox txtObj 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5520
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   45
            Width           =   675
         End
         Begin VB.TextBox txtPres 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3360
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   45
            Width           =   1695
         End
         Begin VB.Label lblObj 
            Caption         =   "Obj.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   5100
            TabIndex        =   6
            Top             =   105
            Width           =   435
         End
         Begin VB.Label lblPres 
            Caption         =   "Pres.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   2835
            TabIndex        =   5
            Top             =   90
            Width           =   540
         End
      End
      Begin MSComctlLib.TreeView tvwEstrPres 
         Height          =   4185
         Left            =   -74880
         TabIndex        =   7
         Top             =   675
         Width           =   6720
         _ExtentX        =   11853
         _ExtentY        =   7382
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList2"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwestrorg 
         Height          =   4575
         Left            =   120
         TabIndex        =   8
         Top             =   660
         Width           =   6720
         _ExtentX        =   11853
         _ExtentY        =   8070
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   -68925
         TabIndex        =   9
         Top             =   360
         Width           =   765
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1349
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "2002"
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAnyo 
         BackColor       =   &H00FFC0C0&
         Caption         =   "DA�o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   -69510
         TabIndex        =   10
         Top             =   405
         Width           =   570
      End
      Begin VB.Label lblEstrorg2 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "DUnidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -74880
         TabIndex        =   12
         Top             =   360
         Width           =   6720
      End
      Begin VB.Label lblEstrorg1 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "DUnidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   6720
      End
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":2EDA
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":39A4
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":3DF6
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":4248
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPresAnuUON.frx":469A
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSELPresAnuUON"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bRUO As Boolean
Public bRuoRama As Boolean
Public iUOBase As Integer
Public g_iTipoPres As Integer
'Presupuestos seleccionados
Public g_sPRES1 As String
Public g_sPRES2 As String
Public g_sPRES3 As String
Public g_sPRES4 As String
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
Public g_sDenPres As String

Public g_vIdPRES1 As Variant
Public g_vIdPRES2 As Variant
Public g_vIdPRES3 As Variant
Public g_vIdPRES4 As Variant

Private oPresupuestos As Object 'Contendra toda la estructura de presupuestos

'Variable para mostrar la suma de presupuestos en la caption de la grid
Private dAcumulado As Double
'Variable que contendra el importe del presupuesto selecionado en el arbol
Private dPresGeneralSel As Double
Public sOrigen As String
Private bOrdenDen As Boolean ' orden listado

'Multilenguaje
Private sIdiSel As String

Public bMostrarBajas As Boolean
Private bBajasPres As Boolean


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPROY, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        sIdiSel = Ador(0).Value
        Ador.MoveNext
        
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value
        Ador.MoveNext
        lblObj.caption = Ador(0).Value
        Ador.MoveNext
        lblPres.caption = Ador(0).Value
        Ador.MoveNext
        lblEstrorg1.caption = Ador(0).Value
        lblEstrorg2.caption = Ador(0).Value
        SSTabPresupuestos.TabCaption(0) = Ador(0).Value
        Ador.Close
    End If
    Set Ador = Nothing
    If g_iTipoPres = 1 Then
        Me.caption = sIdiSel & " " & gParametrosGenerales.gsPlurPres1
        SSTabPresupuestos.TabCaption(1) = gParametrosGenerales.gsPlurPres1
    Else
        Me.caption = sIdiSel & " " & gParametrosGenerales.gsPlurPres2
        SSTabPresupuestos.TabCaption(1) = gParametrosGenerales.gsPlurPres2
    End If
End Sub

Private Sub Arrange()

    If Me.Width <= 1000 Then
        Exit Sub
    End If
    If Me.Height <= 1500 Then
        Exit Sub
    End If

    SSTabPresupuestos.Height = Me.Height - 900
    SSTabPresupuestos.Width = Me.Width - 240
    If SSTabPresupuestos.Tab = 0 Then
        lblEstrorg1.Width = SSTabPresupuestos.Width - 255
        tvwestrorg.Width = SSTabPresupuestos.Width - 255
        tvwestrorg.Height = SSTabPresupuestos.Height - 870
    Else
        sdbcAnyo.Left = lblEstrorg2.Width - sdbcAnyo.Width + 120
        lblAnyo.Left = sdbcAnyo.Left - lblAnyo.Width - 15
        lblEstrorg2.Width = SSTabPresupuestos.Width - 255
        tvwEstrPres.Width = SSTabPresupuestos.Width - 255
        tvwEstrPres.Height = SSTabPresupuestos.Height - 1260
        picSepar.Width = SSTabPresupuestos.Width - 255
        picSepar.Top = tvwEstrPres.Top + tvwEstrPres.Height + 45
    End If
    
    cmdSeleccionar.Top = SSTabPresupuestos.Top + SSTabPresupuestos.Height + 20
    cmdCerrar.Top = cmdSeleccionar.Top
    
End Sub


Private Sub cmdCerrar_Click()
Unload Me
End Sub

Private Sub cmdSeleccionar_Click()
Dim nodx As MSComctlLib.Node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
                               
    g_sPRES1 = ""
    g_sPRES2 = ""
    g_sPRES3 = ""
    g_sPRES4 = ""
    
    g_vIdPRES1 = Null
    g_vIdPRES2 = Null
    g_vIdPRES3 = Null
    g_vIdPRES4 = Null
    
    Screen.MousePointer = vbHourglass
    
    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        g_sDenPres = DevolverDen(nodx)
        Select Case Left(nodx.Tag, 5)
        
            Case "PRES1"
                
                g_sPRES1 = DevolverCod(nodx)
        
                If sOrigen = "frmSOLAbrirFaltan" Or sOrigen = "frmEST1" Or sOrigen = "frmEST2" Or sOrigen = "frmUsuarios" Or sOrigen = "frmFormularios" Or sOrigen = "frmDesgloseValores" Or sOrigen = "frmSolicitudDetalle" Or sOrigen = "frmSolicitudDesglose" Or sOrigen = "frmPROCEBuscar" Or InStr(sOrigen, "frmLstPROCE") > 0 Then
                    If g_iTipoPres = 1 Then
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(g_sPRES1))
                        g_vIdPRES1 = oPresupuestos.Item(scod1).Id
                    Else
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(g_sPRES1))
                        g_vIdPRES1 = oPresupuestos.Item(scod1).Id
                    End If
                End If
                
            Case "PRES2"
                
                g_sPRES1 = DevolverCod(nodx.Parent)
                g_sPRES2 = DevolverCod(nodx)
                
                If sOrigen = "frmSOLAbrirFaltan" Or sOrigen = "frmEST1" Or sOrigen = "frmEST2" Or sOrigen = "frmUsuarios" Or sOrigen = "frmFormularios" Or sOrigen = "frmDesgloseValores" Or sOrigen = "frmSolicitudDetalle" Or sOrigen = "frmSolicitudDesglose" Or sOrigen = "frmPROCEBuscar" Or InStr(sOrigen, "frmLstPROCE") > 0 Then
                    If g_iTipoPres = 1 Then
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(g_sPRES1))
                        scod2 = scod1 & g_sPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(g_sPRES2))
                        g_vIdPRES2 = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod2).Id
                    Else
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(g_sPRES1))
                        scod2 = scod1 & g_sPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(g_sPRES2))
                        g_vIdPRES2 = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod2).Id
                    End If
                End If
            
            Case "PRES3"
                g_sPRES1 = DevolverCod(nodx.Parent.Parent)
                g_sPRES2 = DevolverCod(nodx.Parent)
                g_sPRES3 = DevolverCod(nodx)
                
                If sOrigen = "frmSOLAbrirFaltan" Or sOrigen = "frmEST1" Or sOrigen = "frmEST2" Or sOrigen = "frmUsuarios" Or sOrigen = "frmFormularios" Or sOrigen = "frmDesgloseValores" Or sOrigen = "frmSolicitudDetalle" Or sOrigen = "frmSolicitudDesglose" Or sOrigen = "frmPROCEBuscar" Or InStr(sOrigen, "frmLstPROCE") > 0 Then
                    If g_iTipoPres = 1 Then
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(g_sPRES1))
                        scod2 = scod1 & g_sPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(g_sPRES2))
                        scod3 = scod2 & g_sPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(g_sPRES3))
                        g_vIdPRES3 = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod2).PresProyectosNivel3.Item(scod3).Id
                    Else
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(g_sPRES1))
                        scod2 = scod1 & g_sPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(g_sPRES2))
                        scod3 = scod2 & g_sPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(g_sPRES3))
                        g_vIdPRES3 = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod2).PresContablesNivel3.Item(scod3).Id
                    End If
                End If
                
            Case "PRES4"
                
                g_sPRES1 = DevolverCod(nodx.Parent.Parent.Parent)
                g_sPRES2 = DevolverCod(nodx.Parent.Parent)
                g_sPRES3 = DevolverCod(nodx.Parent)
                g_sPRES4 = DevolverCod(nodx)
                                    
                If sOrigen = "frmSOLAbrirFaltan" Or sOrigen = "frmEST1" Or sOrigen = "frmEST2" Or sOrigen = "frmUsuarios" Or sOrigen = "frmFormularios" Or sOrigen = "frmDesgloseValores" Or sOrigen = "frmSolicitudDetalle" Or sOrigen = "frmSolicitudDesglose" Or sOrigen = "frmPROCEBuscar" Or InStr(sOrigen, "frmLstPROCE") > 0 Then
                    If g_iTipoPres = 1 Then
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(g_sPRES1))
                        scod2 = scod1 & g_sPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(g_sPRES2))
                        scod3 = scod2 & g_sPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(g_sPRES3))
                        scod4 = scod3 & g_sPRES4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(g_sPRES4))
                        g_vIdPRES4 = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod2).PresProyectosNivel3.Item(scod3).PresProyectosNivel4.Item(scod4).Id
                    Else
                        scod1 = g_sPRES1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(g_sPRES1))
                        scod2 = scod1 & g_sPRES2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(g_sPRES2))
                        scod3 = scod2 & g_sPRES3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(g_sPRES3))
                        scod4 = scod3 & g_sPRES4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(g_sPRES4))
                        g_vIdPRES4 = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod2).PresContablesNivel3.Item(scod3).PresContablesNivel4.Item(scod4).Id
                    End If
                End If
                
        End Select
        
        Select Case sOrigen
    
            Case "InfAhorroApliProy"
            
                frmInfAhorroApliProy.MostrarProySeleccionado
                                
            Case "frmLstINFAhorroAplA4B2C3"
                ' Listado de ahorros aplicados por proyecto
                frmListados.ofrmLstApliProy.MostrarProySeleccionado
               
            Case "frmLstINFAhorroAplfrmPROY"
                ' Formulario de ahorros aplicados por proyecto
                frmInfAhorroApliProy.ofrmLstApliProy.MostrarProySeleccionado
            
            Case "InfEvolPres"
                MDI.g_ofrmInfEvolPres1.MostrarProySeleccionado
                    
            Case "LstInfEvolPres1"
                MDI.g_ofrmInfEvolPres1.ofrmLstInfEvolPres1.MostrarProySeleccionado
    
            Case "LstInfEvolPres3"
                frmListados.ofrmLstInfEvolPres3.MostrarProySeleccionado
            
            Case "InfAhorroApliPar"
                
                frmInfAhorroApliParCon.MostrarParConSeleccionada
            
            Case "frmLstPRESPorParCon"
                
                frmLstPRESPorParCon.MostrarParConSeleccionada
                
            Case "frmLstINFAhorroAplA4B2C4"
                frmListados.ofrmLstApliParCon.MostrarParConSeleccionada
                
            Case "frmLstINFAhorroAplfrmPARCON"
                frmInfAhorroApliParCon.ofrmLstApliParCon.MostrarParConSeleccionada
    
            Case "InfEvolPres1"
                MDI.g_ofrmInfEvolPres1.MostrarProySeleccionado
        
            Case "InfEvolPres2"
                MDI.g_ofrmInfEvolPres2.MostrarParConSeleccionada
    
            Case "LstInfEvolPres2"
                MDI.g_ofrmInfEvolPres2.ofrmLstInfEvolPres2.MostrarParConSeleccionada
    
            Case "LstInfEvolPres4"
                frmListados.ofrmLstInfEvolPres4.MostrarParConSeleccionada
    
            Case "frmEST1"
                frmEST.MostrarPresSeleccionado12 1
                
            Case "frmEST2"
                frmEST.MostrarPresSeleccionado12 2
                
            Case "frmPROCEBuscar"
                frmPROCEBuscar.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEA2B1"
                
                frmListados.ofrmLstProce1.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEA2B2"
                
                frmListados.ofrmlstProce2.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEA2B3"
                
                frmListados.ofrmLstProce3.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEA2B4C2"
            
                frmListados.ofrmLstProce4.MostrarPresSeleccionado12 g_iTipoPres
            
            Case "frmLstPROCEA2B5"
                
                frmListados.ofrmlstProce5.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEA6B3C1"
                
                frmListados.ofrmLstProce6.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEfrmPROCE"
                
                frmPROCE.g_ofrmLstPROCE.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEfrmSELPROVE"
                
                frmSELPROVE.ofrmLstPROCE.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEfrmOFEPet"
                
                frmOFEPet.ofrmLstPROCE.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEfrmOFERec"
                
                frmOFERec.ofrmLstPROCE.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmLstPROCEfrmOFEHistWeb"
                
                frmOFEHistWeb.ofrmLstPROCE.MostrarPresSeleccionado12 g_iTipoPres
            
''''            Case "frmFormularios"
''''
''''                frmFormularios.MostrarPresSeleccionado12 g_iTipoPres
''''
''''            Case "frmDesgloseValores"
''''                frmDesgloseValores.MostrarPresSeleccionado12 g_iTipoPres
''''
''''            Case "frmSolicitudDetalle"
''''                frmSolicitudes.g_ofrmDetalleSolic.MostrarPresSeleccionado12 g_iTipoPres
''''
''''            Case "frmSolicitudDesglose"
''''                frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.MostrarPresSeleccionado12 g_iTipoPres
''''
''''            Case "frmSOLAbrirFaltan"
''''                frmSOLAbrirFaltan.MostrarPresSeleccionado12 g_iTipoPres
                
            Case "frmUsuarios"
                If g_iTipoPres = 1 Then
                    If Mid(nodx.Image, 5, 1) < gParametrosGenerales.giNIVPP Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NivelAsignacionPresupuestos gParametrosGenerales.gsSingPres1, gParametrosGenerales.giNIVPP
                        Exit Sub
                    End If
                Else
                    If Mid(nodx.Image, 5, 1) < gParametrosGenerales.giNIVPC Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NivelAsignacionPresupuestos gParametrosGenerales.gsSingPres2, gParametrosGenerales.giNIVPC
                        Exit Sub
                    End If
                End If
                
                frmUSUARIOS.MostrarPresSeleccionado12 g_iTipoPres
        End Select
    
    End If
    
    Screen.MousePointer = vbNormal
    Unload Me
    

End Sub

'Private Sub Form_Activate()
'    tvwEstrPres_NodeClick tvwEstrPres.SelectedItem
'    tvwEstrPres.SetFocus
'
'End Sub

Private Sub Form_Load()
        
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    CargarAnyos
    
    ConfigurarTab0

    If SSTabPresupuestos.TabVisible(0) = False Then
        GenerarEstructuraPresupuestos False
        cmdSeleccionar.Enabled = True
    Else
        CargarEstructuraOrg False
    End If
    
End Sub

Private Sub GenerarEstructuraPresupuestos(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim oPRES1 As Object
    Dim oPRES2 As Object
    Dim oPRES3 As Object
    Dim oPRES4 As Object
    Dim oPresups2 As Object
    Dim oPresups3 As Object
    Dim oPresups4 As Object
    Dim iLongCod1 As Integer
    Dim iLongCod2 As Integer
    Dim iLongCod3 As Integer
    Dim iLongCod4 As Integer

    Dim nodx As MSComctlLib.Node
    
    tvwEstrPres.Nodes.clear
    
        
    If g_iTipoPres = 1 Then
        Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres1, "Raiz")
        Set oPresupuestos = oFSGSRaiz.generar_CPresProyectosNivel1
        iLongCod1 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1
        iLongCod2 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2
        iLongCod3 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3
        iLongCod4 = basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4
    Else
        Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres2, "Raiz")
        Set oPresupuestos = oFSGSRaiz.Generar_CPresContablesNivel1
        iLongCod1 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON1
        iLongCod2 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON2
        iLongCod3 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON3
        iLongCod4 = basParametros.gLongitudesDeCodigos.giLongCodPRESCON4
    End If
    nodx.Tag = "Raiz "
    nodx.Expanded = True
    
    'oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, , False
    oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, g_sUON1, g_sUON2, g_sUON3, , bBajasPres
    
    
    Select Case gParametrosGenerales.giNEPP
        
        Case 1
                
                For Each oPRES1 In oPresupuestos
                    scod1 = oPRES1.Cod & Mid$("                         ", 1, iLongCod1 - Len(oPRES1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oPRES1.Cod
                Next
        
        Case 2
            
            For Each oPRES1 In oPresupuestos
                scod1 = oPRES1.Cod & Mid$("                         ", 1, iLongCod1 - Len(oPRES1.Cod))
                Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                nodx.Tag = "PRES1" & oPRES1.Cod
                If g_iTipoPres = 1 Then
                    Set oPresups2 = oPRES1.PresProyectosNivel2
                Else
                    Set oPresups2 = oPRES1.PresContablesNivel2
                End If
                For Each oPRES2 In oPresups2
                    scod2 = oPRES2.Cod & Mid$("                         ", 1, iLongCod2 - Len(oPRES2.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                    nodx.Tag = "PRES2" & oPRES2.Cod
                Next
            Next
        
        Case 3
            
            For Each oPRES1 In oPresupuestos
                scod1 = oPRES1.Cod & Mid$("                         ", 1, iLongCod1 - Len(oPRES1.Cod))
                Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                nodx.Tag = "PRES1" & oPRES1.Cod
                If g_iTipoPres = 1 Then
                    Set oPresups2 = oPRES1.PresProyectosNivel2
                Else
                    Set oPresups2 = oPRES1.PresContablesNivel2
                End If
                
                For Each oPRES2 In oPresups2
                    scod2 = oPRES2.Cod & Mid$("                         ", 1, iLongCod2 - Len(oPRES2.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                    nodx.Tag = "PRES2" & oPRES2.Cod
                    If g_iTipoPres = 1 Then
                        Set oPresups3 = oPRES2.PresProyectosNivel3
                    Else
                        Set oPresups3 = oPRES2.PresContablesNivel3
                    End If
                        
                        For Each oPRES3 In oPresups3
                            scod3 = oPRES3.Cod & Mid$("                         ", 1, iLongCod3 - Len(oPRES3.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                            nodx.Tag = "PRES3" & oPRES3.Cod
                        Next
                Next
        Next
            
        Case 4
            
            For Each oPRES1 In oPresupuestos
                scod1 = oPRES1.Cod & Mid$("                         ", 1, iLongCod1 - Len(oPRES1.Cod))
                Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oPRES1.Cod & " - " & oPRES1.Den, "PRES1")
                nodx.Tag = "PRES1" & oPRES1.Cod
                If g_iTipoPres = 1 Then
                    Set oPresups2 = oPRES1.PresProyectosNivel2
                Else
                    Set oPresups2 = oPRES1.PresContablesNivel2
                End If
                
                For Each oPRES2 In oPresups2
                    scod2 = oPRES2.Cod & Mid$("                         ", 1, iLongCod2 - Len(oPRES2.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                    nodx.Tag = "PRES2" & oPRES2.Cod
                    If g_iTipoPres = 1 Then
                        Set oPresups3 = oPRES2.PresProyectosNivel3
                    Else
                        Set oPresups3 = oPRES2.PresContablesNivel3
                    End If
                        
                        For Each oPRES3 In oPresups3
                            scod3 = oPRES3.Cod & Mid$("                         ", 1, iLongCod3 - Len(oPRES3.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                            nodx.Tag = "PRES3" & oPRES3.Cod
                            If g_iTipoPres = 1 Then
                                Set oPresups4 = oPRES3.PresProyectosNivel4
                            Else
                                Set oPresups4 = oPRES3.PresContablesNivel4
                            End If
                            
                            For Each oPRES4 In oPresups4
                                scod4 = oPRES4.Cod & Mid$("                         ", 1, iLongCod4 - Len(oPRES4.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                nodx.Tag = "PRES4" & oPRES4.Cod
                            Next
                        Next
                Next
        Next
            
    End Select


    Set oPRES1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
    Set oPresups2 = Nothing
    Set oPresups3 = Nothing
    Set oPresups4 = Nothing
            
    Exit Sub
    
Error:
    Set nodx = Nothing
    Resume Next
End Sub

Private Sub ConfigurarSeguridad()
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona) Or (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador) Then
            If bRUO Or basOptimizacion.gPYMEUsuario <> 0 Then
                g_sUON1 = basOptimizacion.gUON1Usuario
                g_sUON2 = basOptimizacion.gUON2Usuario
                g_sUON3 = basOptimizacion.gUON3Usuario
            End If
        End If
    End If
End Sub

Public Function DevolverCod(ByVal Node As MSComctlLib.Node) As Variant

    If Node Is Nothing Then Exit Function
    
    Select Case Left(Node.Tag, 5)
        Case "PRES1", "PRES2", "PRES3", "PRES4"
            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 5)
    End Select
    
    Select Case Left(Node.Tag, 4)
        Case "UON1", "UON2", "UON3"
            DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    End Select
End Function
Private Function DevolverDen(ByVal Node As MSComctlLib.Node) As Variant

If Node Is Nothing Then Exit Function

Select Case Left(Node.Tag, 5)

Case "PRES1"
        
        DevolverDen = Right(Node.Text, (Len(Node.Text) - (Len(Node.Tag) - 5)))
    
Case "PRES2"
    
        DevolverDen = Right(Node.Text, (Len(Node.Text) - (Len(Node.Tag) - 5)))
    
Case "PRES3"
    
        DevolverDen = Right(Node.Text, (Len(Node.Text) - (Len(Node.Tag) - 5)))

Case "PRES4"
    
        DevolverDen = Right(Node.Text, (Len(Node.Text) - (Len(Node.Tag) - 5)))

End Select

End Function

Public Function DevolverAnyo(ByVal Node As MSComctlLib.Node) As Variant

If Node Is Nothing Then Exit Function

If Left(Node.Tag, 5) = "-ANYO-" Then
        
        DevolverAnyo = Int(val(Right(Node.Tag, Len(Node.Tag) - 5)))
Else
        DevolverAnyo = 0
End If

End Function


Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    iUOBase = 0
    g_iTipoPres = 0
    'Presupuestos seleccionados
    g_sPRES1 = ""
    g_sPRES2 = ""
    g_sPRES3 = ""
    g_sPRES4 = ""
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    g_sDenPres = ""
    
    g_vIdPRES1 = Null
    g_vIdPRES2 = Null
    g_vIdPRES3 = Null
    g_vIdPRES4 = Null
    
    dAcumulado = 0
    dPresGeneralSel = 0
    sOrigen = 0
    bOrdenDen = False
        
    bMostrarBajas = False
    bBajasPres = False
    
    Set oPresupuestos = Nothing
    
End Sub



Private Sub sdbcAnyo_Click()
    
    txtPartida = ""
    txtPres = ""
    txtObj = ""

    GenerarEstructuraPresupuestos False
    
End Sub

Private Sub SSTabPresupuestos_Click(PreviousTab As Integer)
    Arrange
    If SSTabPresupuestos.Tab = 1 Then
        cmdSeleccionar.Enabled = True
        GenerarEstructuraPresupuestos False
        txtObj.Text = ""
        txtPres.Text = ""
        txtPartida.Text = ""
    Else
        cmdSeleccionar.Enabled = False
    End If
End Sub

Private Sub tvwestrorg_NodeClick(ByVal Node As MSComctlLib.Node)
    lblEstrorg1.caption = Node.Text
    lblEstrorg2.caption = lblEstrorg1.caption
    
    Select Case Left(Node.Tag, 4)
        Case "UON1"
            g_sUON1 = DevolverCod(Node)
            g_sUON2 = ""
            g_sUON3 = ""
        Case "UON2"
            g_sUON1 = DevolverCod(Node.Parent)
            g_sUON2 = DevolverCod(Node)
            g_sUON3 = ""
        Case "UON3"
            g_sUON1 = DevolverCod(Node.Parent.Parent)
            g_sUON2 = DevolverCod(Node.Parent)
            g_sUON3 = DevolverCod(Node)
        Case "UON0"
            g_sUON1 = ""
            g_sUON2 = ""
            g_sUON3 = ""
    End Select
    
    Select Case Node.Image
        Case "UON1B", "UON2B", "UON3B"
            bBajasPres = True
        Case Else
            bBajasPres = False
    End Select
    
    If Not bRUO And SSTabPresupuestos.TabVisible(1) = False Then
        SSTabPresupuestos.TabVisible(1) = True
        Exit Sub
    End If
    
    Select Case iUOBase
        Case 1
            Select Case Left(Node.Tag, 4)
                Case "UON1", "UON2", "UON3"
                    SSTabPresupuestos.TabVisible(1) = True
                Case Else
                    SSTabPresupuestos.TabVisible(1) = False
            End Select
        Case 2
            Select Case Left(Node.Tag, 4)
                Case "UON2", "UON3"
                    SSTabPresupuestos.TabVisible(1) = True
                Case Else
                    SSTabPresupuestos.TabVisible(1) = False
            End Select
        Case 3
            Select Case Left(Node.Tag, 4)
                Case "UON3"
                    SSTabPresupuestos.TabVisible(1) = True
                Case Else
                    SSTabPresupuestos.TabVisible(1) = False
            End Select
    End Select
    
End Sub


Public Sub tvwEstrPres_NodeClick(ByVal Node As MSComctlLib.Node)
    MostrarDatosBarraInf
End Sub
Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer
    
    'Toma el a�o actual
    iAnyoActual = Year(Date)
    
    'A�ade a la combo desde 10 antes al actual hasta 10 despu�s del actual
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        sdbcAnyo.AddItem iInd
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub


Public Sub MostrarDatosBarraInf()
    Dim nodx As MSComctlLib.Node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim vImporte As Variant
    Dim vObjetivo As Variant
    
    

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 5)
            Case "Raiz "
                txtPartida = ""
                txtPres = ""
                txtObj = ""
                dPresGeneralSel = 0
            Case "PRES1"
                If g_iTipoPres = 1 Then
                    scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx)))
                Else
                    scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx)))
                End If
                txtPartida = DevolverCod(nodx) & " (" & oPresupuestos.Item(scod1).Den & ")"
                txtPres = DblToStr(oPresupuestos.Item(scod1).importe)
                dPresGeneralSel = NullToDbl0(oPresupuestos.Item(scod1).importe)
                txtObj = Format(DblToStr(oPresupuestos.Item(scod1).Objetivo), "0.0#\%")
            Case "PRES2"
                txtPartida = DevolverCod(nodx) & " (" & DevolverDen(nodx) & ")"
                If g_iTipoPres = 1 Then
                    scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent)))
                    scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx)))
                    vImporte = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).importe
                    vObjetivo = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).Objetivo
                Else
                    scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent)))
                    scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx)))
                    vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe
                    vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Objetivo
                End If
                txtPres = DblToStr(vImporte)
                dPresGeneralSel = NullToDbl0(vImporte)
                txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
            Case "PRES3"
                txtPartida = DevolverCod(nodx) & " (" & DevolverDen(nodx) & ")"
                If g_iTipoPres = 1 Then
                    scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent.Parent)))
                    scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx.Parent)))
                    scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(DevolverCod(nodx)))
                    vImporte = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).importe
                    vObjetivo = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).Objetivo
                Else
                    scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent)))
                    scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent)))
                    scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx)))
                    vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe
                    vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Objetivo
                End If
                txtPres = DblToStr(vImporte)
                dPresGeneralSel = NullToDbl0(vImporte)
                txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
            Case "PRES4"
                txtPartida = DevolverCod(nodx) & " (" & DevolverDen(nodx) & ")"
                If g_iTipoPres = 1 Then
                    scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                    scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY2 - Len(DevolverCod(nodx.Parent.Parent)))
                    scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY3 - Len(DevolverCod(nodx.Parent)))
                    scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESPROY4 - Len(DevolverCod(nodx)))
                    vImporte = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).PresProyectosNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                    vObjetivo = oPresupuestos.Item(scod1).PresProyectosNivel2.Item(scod1 & scod2).PresProyectosNivel3.Item(scod1 & scod2 & scod3).PresProyectosNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                Else
                    scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                    scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent.Parent)))
                    scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx.Parent)))
                    scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(DevolverCod(nodx)))
                    vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                    vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                End If
                txtPres = DblToStr(vImporte)
                dPresGeneralSel = NullToDbl0(vImporte)
                txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
        End Select
    
    End If

End Sub

Private Sub CargarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim bUON0 As Boolean

    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3

    ' Otras
    Dim nodx As Node
  
    tvwestrorg.Nodes.clear
    
    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
         
    If (oUsuarioSummit.Tipo = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (bRUO Or basOptimizacion.gPYMEUsuario <> 0) Then
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                bUON0 = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, bOrdenadoPorDen, , , , , , , bRuoRama, bMostrarBajas, basOptimizacion.gPYMEUsuario)
            Case 2
                bUON0 = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, bOrdenadoPorDen, , , , , , , bRuoRama, bMostrarBajas, basOptimizacion.gPYMEUsuario)
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, bOrdenadoPorDen, , , , , , , bRuoRama, bMostrarBajas, basOptimizacion.gPYMEUsuario
            Case 3
                bUON0 = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, bOrdenadoPorDen, , , , , , , bRuoRama, bMostrarBajas, basOptimizacion.gPYMEUsuario)
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, bOrdenadoPorDen, , , , , , , bRuoRama, bMostrarBajas, basOptimizacion.gPYMEUsuario
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, bOrdenadoPorDen, , , , , , , bMostrarBajas, basOptimizacion.gPYMEUsuario
        End Select
        
    Else
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                    bUON0 = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , bMostrarBajas)
            Case 2
                    bUON0 = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , bMostrarBajas)
                    oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , bMostrarBajas
            Case 3
                    bUON0 = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPresPedido(g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , bMostrarBajas)
                    oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , , bMostrarBajas
                    oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPresPedido g_iTipoPres, , , , , bOrdenadoPorDen, , , , , , , bMostrarBajas
        End Select
        
        
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    If bUON0 Then
        nodx.Tag = "UON0A"
    Else
        nodx.Tag = "UON0"
    End If
    nodx.Expanded = True
    
    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        If oUON1.ConPresup Then
            If oUON1.BajaLog Then
                nodx.Image = "UON1B"
                nodx.Expanded = True
            Else
                nodx.Image = "UON1A"
                nodx.Expanded = True
            End If
        Else
            If oUON1.BajaLog Then
                nodx.Image = "UON1B"
            End If
        End If
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        If oUON2.ConPresup Then
            If oUON2.BajaLog Then
                nodx.Image = "UON2B"
                nodx.Expanded = True
                nodx.Parent.Expanded = True
            Else
                nodx.Image = "UON2A"
                nodx.Expanded = True
                nodx.Parent.Expanded = True
            End If
        Else
            If oUON2.BajaLog Then
                nodx.Image = "UON2B"
            End If
        End If
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        If oUON3.ConPresup Then
            If oUON3.BajaLog Then
                nodx.Image = "UON3B"
                nodx.Expanded = True
                nodx.Parent.Expanded = True
                nodx.Parent.Parent.Expanded = True
            Else
                nodx.Image = "UON3A"
                nodx.Expanded = True
                nodx.Parent.Expanded = True
                nodx.Parent.Parent.Expanded = True
                
            End If
        Else
            If oUON3.BajaLog Then
                nodx.Image = "UON3B"
            End If
        End If
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
        
End Sub



Private Sub ConfigurarTab0()
Dim arPresup As Variant
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

    iUOBase = 0
    If bRUO Or bRuoRama Or basOptimizacion.gPYMEUsuario <> 0 Then
        If (CStr(basOptimizacion.gUON3Usuario) <> "") Then
            iUOBase = 3
            g_sUON3 = basOptimizacion.gUON3Usuario
            g_sUON2 = basOptimizacion.gUON2Usuario
            g_sUON1 = basOptimizacion.gUON1Usuario
        ElseIf (CStr(basOptimizacion.gUON2Usuario) <> "") Then
            iUOBase = 2
            g_sUON2 = basOptimizacion.gUON2Usuario
            g_sUON1 = basOptimizacion.gUON1Usuario
        ElseIf (CStr(basOptimizacion.gUON1Usuario) <> "") Then
            iUOBase = 1
            g_sUON1 = basOptimizacion.gUON1Usuario
        End If
        
    End If
    
    Select Case g_iTipoPres
    Case 1
        Set oPresupuestos = oFSGSRaiz.generar_CPresProyectosNivel1
        arPresup = oPresupuestos.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, bRUO, bRuoRama)
    Case 2
        Set oPresupuestos = oFSGSRaiz.Generar_CPresContablesNivel1
        arPresup = oPresupuestos.ExistenPresupuestos(g_sUON1, g_sUON2, g_sUON3, bRUO, bRuoRama)
    End Select
    
    Select Case arPresup(0)
    Case 0
        'No hay ningun presupuesto para las ramas permitidas
        oMensajes.MensajeOKOnly 728
    
    Case 1
        SSTabPresupuestos.TabVisible(1) = True
        SSTabPresupuestos.TabVisible(0) = False
        g_sUON1 = NullToStr(arPresup(1))
        g_sUON2 = NullToStr(arPresup(2))
        g_sUON3 = NullToStr(arPresup(3))
        If g_sUON3 <> "" Then
            Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
            lblEstrorg2.caption = g_sUON3 & " - " & oUnidadesOrgN3.DevolverDenominacion(g_sUON1, g_sUON2, g_sUON3)
        Else
            If g_sUON2 <> "" Then
                Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
                lblEstrorg2.caption = g_sUON2 & " - " & oUnidadesOrgN2.DevolverDenominacion(g_sUON1, g_sUON2)
            Else
                Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
                lblEstrorg2.caption = g_sUON1 & " - " & oUnidadesOrgN1.DevolverDenominacion(g_sUON1)
            End If
        End If
    Case Else
        SSTabPresupuestos.TabVisible(1) = False
    End Select
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    Set oPresupuestos = Nothing
End Sub

