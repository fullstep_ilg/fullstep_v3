VERSION 5.00
Begin VB.Form frmRESREUProx 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Posponer proceso a otra reuni�n"
   ClientHeight    =   1410
   ClientLeft      =   570
   ClientTop       =   405
   ClientWidth     =   4635
   Icon            =   "frmRESREUProx.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1410
   ScaleWidth      =   4635
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2370
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1020
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1170
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1020
      Width           =   1005
   End
   Begin VB.PictureBox picFec 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   60
      ScaleHeight     =   855
      ScaleWidth      =   4455
      TabIndex        =   2
      Top             =   60
      Width           =   4455
      Begin VB.OptionButton optPre 
         BackColor       =   &H00808000&
         Caption         =   "Preparatoria"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   2265
         TabIndex        =   7
         Top             =   480
         Width           =   2220
      End
      Begin VB.OptionButton optDec 
         BackColor       =   &H00808000&
         Caption         =   "Decisi�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   2265
         TabIndex        =   6
         Top             =   120
         Value           =   -1  'True
         Width           =   2220
      End
      Begin VB.TextBox txtFecProx 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   600
         TabIndex        =   4
         Top             =   300
         Width           =   1110
      End
      Begin VB.CommandButton cmdCalendar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1740
         Picture         =   "frmRESREUProx.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   300
         Width           =   315
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   60
         TabIndex        =   5
         Top             =   345
         Width           =   555
      End
   End
End
Attribute VB_Name = "frmRESREUProx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private sIdiFecha As String

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit

If Not IsDate(txtFecProx) Then
    oMensajes.NoValida sIdiFecha
    Exit Sub
End If

Screen.MousePointer = vbHourglass

With frmRESREU.m_oProcesoSeleccionado
    If Not IsNull(.FechaMinimoLimOfertas) Then
        If DateValue(.FechaMinimoLimOfertas) > CDate(txtFecProx) Then
            oMensajes.FechaMenorQueLimOfer
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    If Not IsNull(.FechaNecesidad) Then
        If .FechaNecesidad < CDate(txtFecProx) Then
            oMensajes.FechaMayorQueNecesidad
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
        
    If Not IsNull(.FechaApertura) Then
        If .FechaApertura > CDate(txtFecProx) Then
            oMensajes.FechaMenorQueApertura
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
End With

teserror = frmRESREU.m_oGestorReuniones.FijarProximaReunion(frmRESREU.sdbcProceCod.Columns("ANYO").Value, frmRESREU.sdbcProceCod.Columns("GMN1").Text, frmRESREU.sdbcProceCod.Columns("COD").Value, CDate(txtFecProx), optDec, CDate(frmRESREU.sdbcFecReu.Columns(0).Value))
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Screen.MousePointer = vbNormal
    Exit Sub
End If

frmRESREU.CargarComboProcesos
frmRESREU.sdbcProceCod.BackColor = 10867194
frmRESREU.lblHoraReu.BackColor = 10867194

Screen.MousePointer = vbNormal

Unload Me
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, txtFecProx
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_RESREU_PROX, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmRESREUProx.caption = Ador(0).Value
        Ador.MoveNext
        Label8.caption = Ador(0).Value & ":"
        sIdiFecha = Ador(0).Value
        Ador.MoveNext
        optDec.caption = Ador(0).Value
        Ador.MoveNext
        optPre.caption = Ador(0).Value
        
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
End Sub
