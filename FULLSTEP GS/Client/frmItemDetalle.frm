VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmItemDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Detalle de item"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6840
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmItemDetalle.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   6840
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picItem 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   6255
      Left            =   120
      ScaleHeight     =   6255
      ScaleWidth      =   6795
      TabIndex        =   2
      Top             =   360
      Width           =   6800
      Begin SSDataWidgets_B.SSDBGrid sdbgAdjudicaciones 
         Height          =   1800
         Left            =   0
         TabIndex        =   3
         Top             =   4320
         Width           =   6675
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         BorderStyle     =   0
         Col.Count       =   9
         BeveColorScheme =   1
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns.Count   =   9
         Columns(0).Width=   4207
         Columns(0).Caption=   "Nombre Proveedor"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777184
         Columns(1).Width=   1958
         Columns(1).Caption=   "Proveedor"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777184
         Columns(2).Width=   2143
         Columns(2).Caption=   "Precio"
         Columns(2).Name =   "PREC"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "standard"
         Columns(2).FieldLen=   256
         Columns(2).HasForeColor=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   14745599
         Columns(3).Width=   2143
         Columns(3).Caption=   "Cantidad"
         Columns(3).Name =   "CANT"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "Standard"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1244
         Columns(4).Caption=   "% Adj"
         Columns(4).Name =   "PORC"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "0.0#\%"
         Columns(4).FieldLen=   256
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   16777215
         Columns(5).Width=   1482
         Columns(5).Caption=   "F. Inicio"
         Columns(5).Name =   "FECINI"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   16777215
         Columns(6).Width=   1588
         Columns(6).Caption=   "F. Fin"
         Columns(6).Name =   "FECFIN"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   16777215
         Columns(7).Width=   820
         Columns(7).Caption=   "Des"
         Columns(7).Name =   "DEST"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   16777215
         Columns(8).Width=   741
         Columns(8).Caption=   "Uni"
         Columns(8).Name =   "UNI"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   16777215
         _ExtentX        =   11774
         _ExtentY        =   3175
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid ssdbArticulosAgregados 
         Height          =   1695
         Left            =   0
         TabIndex        =   26
         Top             =   2040
         Width           =   6705
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   3
         stylesets.count =   4
         stylesets(0).Name=   "Gris"
         stylesets(0).BackColor=   -2147483633
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmItemDetalle.frx":0CB2
         stylesets(1).Name=   "Yellow"
         stylesets(1).BackColor=   11862015
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmItemDetalle.frx":0CCE
         stylesets(2).Name=   "Normal"
         stylesets(2).ForeColor=   0
         stylesets(2).BackColor=   16777215
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmItemDetalle.frx":0CEA
         stylesets(3).Name=   "Header"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmItemDetalle.frx":0D06
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "UON"
         Columns(0).Name =   "UON"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "UONKEY"
         Columns(2).Name =   "UONKEY"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   11827
         _ExtentY        =   2990
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblArticulosPlanta 
         BackStyle       =   0  'Transparent
         Caption         =   "DArt�culos de planta:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   27
         Top             =   1680
         Width           =   3630
      End
      Begin VB.Label lblCodDest 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   840
         TabIndex        =   17
         Top             =   0
         Width           =   615
      End
      Begin VB.Label lblCodDestl 
         BackStyle       =   0  'Transparent
         Caption         =   "Destino:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   16
         Top             =   70
         Width           =   885
      End
      Begin VB.Label lblDenDest 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1470
         TabIndex        =   15
         Top             =   0
         Width           =   5205
      End
      Begin VB.Label lblCodUni 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   840
         TabIndex        =   14
         Top             =   390
         Width           =   615
      End
      Begin VB.Label lblCodUnil 
         BackStyle       =   0  'Transparent
         Caption         =   "Unidad:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   13
         Top             =   450
         Width           =   885
      End
      Begin VB.Label lblDenUni 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1470
         TabIndex        =   12
         Top             =   390
         Width           =   5205
      End
      Begin VB.Label lblCodPag 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   840
         TabIndex        =   11
         Top             =   790
         Width           =   615
      End
      Begin VB.Label lblCodPagl 
         BackStyle       =   0  'Transparent
         Caption         =   "Pago:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   10
         Top             =   850
         Width           =   885
      End
      Begin VB.Label lblDenPag 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1470
         TabIndex        =   9
         Top             =   790
         Width           =   5205
      End
      Begin VB.Label lblFecIni 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   840
         TabIndex        =   8
         Top             =   1180
         Width           =   1125
      End
      Begin VB.Label lblFecInil 
         BackStyle       =   0  'Transparent
         Caption         =   "Inicio:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   7
         Top             =   1240
         Width           =   885
      End
      Begin VB.Label lblFecFin 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2800
         TabIndex        =   6
         Top             =   1180
         Width           =   1125
      End
      Begin VB.Label lblFecFinl 
         BackStyle       =   0  'Transparent
         Caption         =   "Fin:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   2200
         TabIndex        =   5
         Top             =   1240
         Width           =   650
      End
      Begin VB.Label lblAdjudicaciones 
         BackStyle       =   0  'Transparent
         Caption         =   "Adjudicaciones anteriores:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   4
         Top             =   3960
         Width           =   3630
      End
   End
   Begin VB.PictureBox picEsp 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   3855
      Left            =   0
      ScaleHeight     =   3855
      ScaleWidth      =   6795
      TabIndex        =   18
      Top             =   360
      Visible         =   0   'False
      Width           =   6800
      Begin VB.TextBox txtEsp 
         Height          =   1450
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   25
         Top             =   240
         Width           =   6640
      End
      Begin VB.CommandButton cmdAbrirEsp 
         Height          =   300
         Left            =   6400
         Picture         =   "frmItemDetalle.frx":0D22
         Style           =   1  'Graphical
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   3520
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin VB.CommandButton cmdSalvarEsp 
         Height          =   300
         Left            =   5950
         Picture         =   "frmItemDetalle.frx":1064
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   3520
         UseMaskColor    =   -1  'True
         Width           =   420
      End
      Begin MSComctlLib.ListView lstvwEsp 
         Height          =   1395
         Left            =   135
         TabIndex        =   21
         Top             =   2050
         Width           =   6640
         _ExtentX        =   11721
         _ExtentY        =   2461
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Comentario"
            Object.Width           =   6588
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Fecha"
            Object.Width           =   2867
         EndProperty
      End
      Begin MSComDlg.CommonDialog cmmdEsp 
         Left            =   2880
         Top             =   1800
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
      End
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   3000
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   1
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmItemDetalle.frx":13A6
               Key             =   "ESP"
               Object.Tag             =   "ESP"
            EndProperty
         EndProperty
      End
      Begin VB.Label lblEspecificaciones 
         BackColor       =   &H00808000&
         Caption         =   "DEspecificaciones"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   23
         Top             =   0
         Width           =   2235
      End
      Begin VB.Label lblArchivosAdj 
         BackColor       =   &H00808000&
         Caption         =   "DArchivos adjuntos"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   180
         TabIndex        =   22
         Top             =   1750
         Width           =   2235
      End
   End
   Begin VB.CommandButton cmdDatGen 
      Height          =   315
      Left            =   0
      Picture         =   "frmItemDetalle.frx":1500
      Style           =   1  'Graphical
      TabIndex        =   24
      ToolTipText     =   "Datos generales"
      Top             =   0
      Width           =   375
   End
   Begin VB.CommandButton cmdEsp 
      Height          =   315
      Left            =   360
      Picture         =   "frmItemDetalle.frx":1A8A
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Pulse para ver las especificaciones del  �tem"
      Top             =   0
      Width           =   375
   End
   Begin VB.Label lblItem 
      Alignment       =   2  'Center
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "                                              "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   720
      TabIndex        =   0
      Top             =   0
      Width           =   6100
   End
End
Attribute VB_Name = "frmItemDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oItemSeleccionado As CItem
Public g_oProcesoSeleccionado As CProceso

Private m_sayFileNames() As String

Private m_sIdiGuardar As String
Private m_sIdiTipoOrig As String
Private m_sIdiNombre As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String

'Separadores
Private Const lSeparador = 127

Private m_oArticulo As CArticulo
Private m_oUons As CUnidadesOrganizativas
Private m_oAtributos As CAtributos
Private m_oArticulos As CArticulos
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean

Private m_sMsgError As String
Public Sub cargarArticulo()
    
    Dim oArticulos As CArticulos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    oArticulos.DevolverArticulosDesde 1, , , , , , NullToStr(g_oItemSeleccionado.ArticuloCod), NullToStr(g_oItemSeleccionado.Descr)
    Set m_oArticulo = oArticulos.Item(1)
    Set oArticulos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "cargarArticulo", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ITEMDETALLE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value '1
        Ador.MoveNext
        lblCodDestl.caption = Ador(0).Value
        Ador.MoveNext
        
        lblCodUnil.caption = Ador(0).Value
        Ador.MoveNext
        lblCodPagl.caption = Ador(0).Value
        Ador.MoveNext
        lblFecInil.caption = Ador(0).Value '5
        Ador.MoveNext
        lblFecFinl.caption = Ador(0).Value
        Ador.MoveNext
        lblAdjudicaciones.caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgAdjudicaciones.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(2).caption = Ador(0).Value '10
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(7).caption = Ador(0).Value '14
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        cmdDatGen.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEsp.ToolTipText = Ador(0).Value
        Ador.MoveNext
        lblEspecificaciones.caption = Ador(0).Value
        Ador.MoveNext
        lblArchivosAdj.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiGuardar = Ador(0).Value
        Ador.MoveNext
        m_sIdiTipoOrig = Ador(0).Value
        Ador.MoveNext
        m_sIdiNombre = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        lstvwEsp.ColumnHeaders(3).Text = Ador(0).Value
        Ador.MoveNext
        Me.ssdbArticulosAgregados.Columns("UON").caption = Ador(0).Value
        Ador.MoveNext
        Me.ssdbArticulosAgregados.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.Close
    
    End If


    Set Ador = Nothing

    'pargen lit
    Dim oLiterales As CLiterales
    Set oLiterales = oGestorParametros.DevolverLiterales(48, 50, basPublic.gParametrosInstalacion.gIdioma)
    Me.lblArticulosPlanta.caption = oLiterales.Item(1).Den    'Art de planta o agregado
    Set oLiterales = Nothing

End Sub




Private Sub cmdAbrirEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oItem As CItem
    
On Error GoTo Cancelar:

    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        
    Else
        
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
        
        Screen.MousePointer = vbHourglass
        
        ' Cargamos el contenido en la esp.
        Screen.MousePointer = vbHourglass
        Set oEsp = g_oItemSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspItem)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oItem = Nothing
            Set oEsp = Nothing
            Exit Sub
        End If
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspItem, oEsp.DataSize, sFileName
        
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
Cancelar:
    If err.Number = 70 Then Resume Next
    
    Set oEsp = Nothing
    Set oItem = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdDatGen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picEsp.Visible = False
    picItem.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "cmdDatGen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdEsp_Click()
Dim oEsp As CEspecificacion
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picEsp.Visible = True
    picItem.Visible = False
    
    If g_oItemSeleccionado.especificaciones Is Nothing Then
        g_oItemSeleccionado.CargarTodasLasEspecificaciones , True
    End If
    
    'Limpia la lista
    lstvwEsp.ListItems.clear
    'A�ade las especificaciones a la lista
    For Each oEsp In g_oItemSeleccionado.especificaciones
        lstvwEsp.ListItems.Add , "ESP" & CStr(oEsp.indice), oEsp.nombre, , "ESP"
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).Tag = CStr(oEsp.indice)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , "ESP" & CStr(oEsp.indice), NullToStr(oEsp.Comentario)
        lstvwEsp.ListItems.Item("ESP" & CStr(oEsp.indice)).ListSubItems.Add , , oEsp.Fecha
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "cmdEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdSalvarEsp_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oItem As CItem
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        cmmdEsp.DialogTitle = m_sIdiGuardar
        cmmdEsp.Filter = m_sIdiTipoOrig
    
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdiNombre
            Exit Sub
        End If
        
        ' Cargamos el contenido en la esp.
        Screen.MousePointer = vbHourglass
        
        Set oEsp = g_oItemSeleccionado.especificaciones.Item(CStr(lstvwEsp.selectedItem.Tag))
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspItem)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oItem = Nothing
            Set oEsp = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspItem, oEsp.DataSize, sFileName
    End If
    
Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set oItem = Nothing
    End If
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "cmdSalvarEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo Cancelar
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Initialize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
    Set m_oArticulos = oFSGSRaiz.Generar_CArticulos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "Form_Initialize", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    CargarRecursos
    ReDim m_sayFileNames(0)

    lstvwEsp.ColumnHeaders(1).Width = lstvwEsp.Width * 0.25
    lstvwEsp.ColumnHeaders(2).Width = lstvwEsp.Width * 0.5
    lstvwEsp.ColumnHeaders(3).Width = lstvwEsp.Width * 0.25 - 75
    
    PonerFieldSeparator Me
    'inicializar variables de colecci�n de uon y de atributos
    Set m_oUons = Nothing
    Set m_oAtributos = Nothing
    Set m_oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
    
    cargarArticulo
    montarColumnasGridAtributosUon
    CargarArticulosAgregados
    
    cargarValoresGridArticulosAgregados
    
    If Not m_oArticulo Is Nothing Then
        If Not m_oArticulo.isCentral Then
            Me.ssdbArticulosAgregados.Columns("COD").Visible = False
        End If
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub Form_Terminate()
    Set m_oArticulo = Nothing
    Set m_oUons = Nothing
    Set g_oProcesoSeleccionado = Nothing
    Set g_oItemSeleccionado = Nothing
    Set m_oAtributos = Nothing
    Set m_oArticulos = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim FOSFile As Scripting.FileSystemObject
Dim bErroresEnElBorrado As Boolean
Dim i As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oArticulo = Nothing
    m_oUons.clear
On Error GoTo ERROR_Frm

    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject

    i = 0
    While i < UBound(m_sayFileNames)

        If FOSFile.FileExists(m_sayFileNames(i)) Then
            bErroresEnElBorrado = True
            FOSFile.DeleteFile m_sayFileNames(i), True
            bErroresEnElBorrado = False
            
        End If

        i = i + 1
    Wend
    
    Set FOSFile = Nothing
    Exit Sub

ERROR_Frm:

    Set FOSFile = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemDetalle", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        GoTo ERROR_Frm
        Exit Sub
    End If
    
End Sub


Public Function HabilitarBotonesInvitado()
' Funci�n para mostrar los botones que puede ver un invitado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 cmdAbrirEsp.Visible = False
 cmdSalvarEsp.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "HabilitarBotonesInvitado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Function DesHabilitarBotonesInvitado()
' Funci�n para mostrar los botones que puede ver un invitado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 cmdAbrirEsp.Visible = True
 cmdSalvarEsp.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "DesHabilitarBotonesInvitado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub CargarArticulosAgregados()
    Dim oUON As IUon
    Dim oArticuloAgr As CArticulo
    Dim oUonDistribuida As Variant
    Dim oUonsDistribuidas As CUnidadesOrganizativas
    
    Dim sLinea As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.ssdbArticulosAgregados.RemoveAll
    
    Set oUonsDistribuidas = g_oItemSeleccionado.proceso.getUonsDistribuidas(g_oItemSeleccionado.Id)
    
    If Not m_oArticulo Is Nothing Then
        If m_oArticulo.isCentral Then
            'caso 1: El art�culo est� por encima de la uon distribuida
            For Each oArticuloAgr In m_oArticulo.articulosAgregados
                For Each oUON In oArticuloAgr.uons
                    If oUonsDistribuidas.CONTIENE(oUON) Then
                        If Not m_oUons.existe(oUON.key) Then
                            m_oArticulos.addArticulo oArticuloAgr
                            m_oUons.Add oUON, oUON.key
                            sLinea = oUON.titulo & Chr(lSeparador) 'Codigo de unidadorganizativa del articulo
                            sLinea = sLinea & oArticuloAgr.Cod & Chr(lSeparador) ' codigo de art�culo de planta
                            sLinea = sLinea & oUON.key ' codificacion de uon
                            ssdbArticulosAgregados.AddItem sLinea
                        End If
                    End If
                Next
            Next
            
            'caso 2: el art�culo est� por debajo de la uon distribuida
            
            For Each oArticuloAgr In m_oArticulo.articulosAgregados
                For Each oUonDistribuida In oUonsDistribuidas
                    For Each oUON In oArticuloAgr.uons
                        If oUonDistribuida.incluye(oUON) And Not oUonDistribuida.equals(oUON) Then
                            If Not m_oUons.existe(oUON.key) Then
                                m_oArticulos.addArticulo oArticuloAgr
                                m_oUons.Add oUON, oUON.key
                                sLinea = oUON.titulo & Chr(lSeparador) 'Codigo de unidadorganizativa del articulo
                                sLinea = sLinea & oArticuloAgr.Cod & Chr(lSeparador) ' codigo de art�culo de planta
                                sLinea = sLinea & oUON.key ' codificacion de uon
                                ssdbArticulosAgregados.AddItem sLinea
                            End If
                        End If
                    Next
                Next
            Next
        Else
            'caso 1_ art�culo por debajo de la distribucion
            For Each oUON In m_oArticulo.uons
                If oUonsDistribuidas.CONTIENE(oUON) Then
                    m_oUons.Add oUON, oUON.key
                    sLinea = oUON.titulo
                    sLinea = sLinea & Chr(lSeparador) & "" 'en caso de no ser central no mostramos el c�digo de planta
                    sLinea = sLinea & Chr(lSeparador) & oUON.key
                    Me.ssdbArticulosAgregados.AddItem sLinea
                End If
            Next
            'caso 2: el art�culo est� por encima de la uon distribuida
            
            For Each oUonDistribuida In oUonsDistribuidas
                For Each oUON In m_oArticulo.uons
                    If oUON.incluye(oUonDistribuida) And Not oUON.equals(oUonDistribuida) Then 'no igual para evitar repetidos en caso de que coincidan
                        If Not m_oUons.existe(oUON.key) Then
                            m_oUons.Add oUON, oUON.key
                            sLinea = oUON.titulo & Chr(lSeparador)
                            sLinea = sLinea & "" & Chr(lSeparador) 'La columna de codigo de planta se deja vac�a y luego se oculta
                            sLinea = sLinea & oUON.key ' codificacion de uon
                            ssdbArticulosAgregados.AddItem sLinea
                        End If
                    End If
                Next
            Next
        End If
    End If
    
    Set oUON = Nothing
    Set oArticuloAgr = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "CargarArticulosAgregados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub montarColumnasGridAtributosUon()
    Dim icolumns As Integer
    Dim oArticulo As CArticulo
    Dim oColumn As SSDataWidgets_B.Column
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    icolumns = ssdbArticulosAgregados.Columns.Count
    Dim oAtributo As CAtributo
    If Not m_oArticulo Is Nothing Then
        If m_oArticulo.isCentral Then
            For Each oArticulo In m_oArticulo.articulosAgregados
                For Each oAtributo In oArticulo.getAtributosUon
                    If Not m_oAtributos.existe(oAtributo.Id) Then
                        ssdbArticulosAgregados.Columns.Add icolumns
                        Set oColumn = ssdbArticulosAgregados.Columns(icolumns)
                        oColumn.Name = oAtributo.Id
                        oColumn.caption = oAtributo.Den
                        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                        oColumn.Alignment = ssCaptionAlignmentLeft
                        oColumn.Style = ssStyleEdit
                        oColumn.Locked = True
                        oColumn.FieldLen = 100
                        icolumns = icolumns + 1
                        m_oAtributos.addAtributo oAtributo
                        Set oColumn = Nothing
                    End If
                Next
            Next
        Else
            For Each oAtributo In m_oArticulo.getAtributosUon
                If Not m_oAtributos.existe(oAtributo.Id) Then
                    ssdbArticulosAgregados.Columns.Add icolumns
                    Set oColumn = ssdbArticulosAgregados.Columns(icolumns)
                    oColumn.Name = oAtributo.Id
                    oColumn.caption = oAtributo.Den
                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                    oColumn.Alignment = ssCaptionAlignmentLeft
                    oColumn.Style = ssStyleEdit
                    oColumn.Locked = True
                    oColumn.FieldLen = 100
                    m_oAtributos.addAtributo oAtributo
                    icolumns = icolumns + 1
                    Set oColumn = Nothing
                End If
            Next
        End If
    End If
    Set oAtributo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "montarColumnasGridAtributosUon", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub cargarValoresGridArticulosAgregados()
    Dim oAtributo As CAtributo
    Dim oAtributos As CAtributos
    Dim oArticulo As CArticulo
    Dim i, j As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate Me.hWnd
    If Not m_oArticulo Is Nothing Then
        If m_oArticulo.isCentral Then
            Me.ssdbArticulosAgregados.MoveFirst
            For i = 0 To ssdbArticulosAgregados.Rows - 1
                Set oArticulo = m_oArticulo.articulosAgregados.Item(ssdbArticulosAgregados.Columns("COD").Value)
                Set oAtributos = oArticulo.AtributosUonValorados
                For j = 3 To ssdbArticulosAgregados.Cols - 1
                    'Si el atributo de uon de la columna tiene valor para la uon de la fila correspondiente existe...
                    If oAtributos.existe(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value) Then
                        Set oAtributo = oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value)
                        If oAtributo.Tipo = TipoBoolean Then
                            If oAtributo.getValor = 1 Then
                                ssdbArticulosAgregados.Columns(j).Value = m_sIdiTrue
                            Else
                                ssdbArticulosAgregados.Columns(j).Value = m_sIdiFalse
                            End If
                        Else
                            ssdbArticulosAgregados.Columns(j).Value = oAtributo.getValor
                        End If
                    Else
                        'si no existe le damos el valor por defecto (guardado en atributosuon)
                        Set oAtributo = m_oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name)
                        If oAtributo.uons.CONTIENE(m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)) Then
                            If oAtributo.Tipo = TipoBoolean Then
                                If oAtributo.getValor = 1 Then
                                    ssdbArticulosAgregados.Columns(j).Text = m_sIdiTrue
                                Else
                                    ssdbArticulosAgregados.Columns(j).Text = m_sIdiFalse
                                End If
                            Else
                                ssdbArticulosAgregados.Columns(j).Text = NullToStr(oAtributo.getValor)
                            End If
                        End If
                        Set oAtributo = Nothing
                    End If
                    
                Next
                
                ssdbArticulosAgregados.MoveNext
            Next
        Else
            Me.ssdbArticulosAgregados.MoveFirst
            Set oAtributos = m_oArticulo.AtributosUonValorados
            For i = 0 To ssdbArticulosAgregados.Rows - 1
                
                For j = 3 To ssdbArticulosAgregados.Cols - 1
                    If oAtributos.existe(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value) Then
                        Set oAtributo = oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value)
                        If oAtributo.Tipo = TipoBoolean Then
                            If oAtributo.getValor = 1 Then
                                ssdbArticulosAgregados.Columns(j).Value = m_sIdiTrue
                            Else
                                ssdbArticulosAgregados.Columns(j).Value = m_sIdiFalse
                            End If
                        Else
                            ssdbArticulosAgregados.Columns(j).Value = oAtributo.getValor
                        End If
                    Else
                        'si no existe le damos el valor por defecto (guardado en atributosuon)
                        Set oAtributo = m_oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name)
                        If oAtributo.uons.CONTIENE(m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)) Then
                            If oAtributo.Tipo = TipoBoolean Then
                                If oAtributo.getValor = 1 Then
                                    ssdbArticulosAgregados.Columns(j).Text = m_sIdiTrue
                                Else
                                    ssdbArticulosAgregados.Columns(j).Text = m_sIdiFalse
                                End If
                            Else
                                ssdbArticulosAgregados.Columns(j).Text = NullToStr(oAtributo.getValor)
                            End If
                        End If
                        Set oAtributo = Nothing
                    End If
                    
                Next
                
                ssdbArticulosAgregados.MoveNext
            Next
        End If
    End If
    
    ssdbArticulosAgregados.MoveFirst
    LockWindowUpdate 0&
    Set oAtributo = Nothing
    Set oAtributos = Nothing
    Set oArticulo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "cargarValoresGridArticulosAgregados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub ssdbArticulosAgregados_RowLoaded(ByVal Bookmark As Variant)
    Dim oUON As IUon
    Dim oatrib As CAtributo
    Dim oArticulo As CArticulo
    'celdas editables o no
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oArticulo.isCentral Then
        'tomamos el art�culo agregado y la uon que se corresponden con la fila que estamos cargando
        Set oArticulo = m_oArticulo.articulosAgregados.Item(ssdbArticulosAgregados.Columns("COD").Value)
        If m_oUons.Count > 0 Then
            Set oUON = oArticulo.uons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)
            For Each oatrib In oArticulo.getAtributosUon
                If Not oatrib.uons.CONTIENE(oUON) Then
                    ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Gris", ssdbArticulosAgregados.Row
                Else
                    ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Normal", ssdbArticulosAgregados.Row
                End If
            Next
        End If
    Else
        'uon de la fila
        Set oUON = m_oArticulo.uons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)
        'para cada atributo(columna atributo)
        For Each oatrib In m_oArticulo.getAtributosUon
            If Not ssdbArticulosAgregados.Columns(CStr(oatrib.Id)) Is Nothing Then
                'si las uons del atributo contienen a la uon del art�culo puden tener valor , si no "no aplica"
                If Not oatrib.uons.CONTIENE(oUON) Then
                    ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Gris", ssdbArticulosAgregados.Row
                Else
                    ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Normal", ssdbArticulosAgregados.Row
                End If
            End If
        Next
       
    End If
 
    Set oUON = Nothing
    Set oatrib = Nothing
    Set oArticulo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmItemDetalle", "ssdbArticulosAgregados_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
