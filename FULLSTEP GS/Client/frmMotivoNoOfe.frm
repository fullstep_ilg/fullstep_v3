VERSION 5.00
Begin VB.Form frmMotivoNoOfe 
   BackColor       =   &H00808000&
   ClientHeight    =   4650
   ClientLeft      =   2145
   ClientTop       =   2190
   ClientWidth     =   7365
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMotivoNoOfe.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4650
   ScaleWidth      =   7365
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCalendarFec 
      Height          =   315
      Left            =   3495
      Picture         =   "frmMotivoNoOfe.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   360
      Width           =   315
   End
   Begin VB.TextBox txtFec 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   2385
      MaxLength       =   10
      TabIndex        =   5
      Top             =   375
      Width           =   1080
   End
   Begin VB.TextBox txtDescr 
      BackColor       =   &H00FFFFFF&
      Height          =   2835
      Left            =   75
      MaxLength       =   4000
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   1335
      Width           =   7200
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   315
      Left            =   2535
      TabIndex        =   1
      Top             =   4260
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3735
      TabIndex        =   2
      Top             =   4260
      Width           =   1005
   End
   Begin VB.Label lblFec 
      BackColor       =   &H00808000&
      Caption         =   "Persona"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   2
      Left            =   90
      TabIndex        =   9
      Top             =   720
      Width           =   7245
   End
   Begin VB.Label lblMotivo 
      BackColor       =   &H00808000&
      Caption         =   "Motivo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   90
      TabIndex        =   8
      Top             =   1035
      Width           =   2205
   End
   Begin VB.Label lblFec 
      BackColor       =   &H00808000&
      Caption         =   "Fecha de comunicaci�n:"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   1
      Left            =   90
      TabIndex        =   7
      Top             =   405
      Width           =   2205
   End
   Begin VB.Label lblFec 
      BackColor       =   &H00808000&
      Caption         =   "Fecha de comunicaci�n: 26/5/2004"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Index           =   0
      Left            =   105
      TabIndex        =   4
      Top             =   405
      Width           =   7200
   End
   Begin VB.Label lblComent 
      BackColor       =   &H00808000&
      Caption         =   "El proveedor ha comunicado que no ofertar� en el proceso actual"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   105
      TabIndex        =   3
      Top             =   90
      Width           =   8640
   End
End
Attribute VB_Name = "frmMotivoNoOfe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_bEdicion As Boolean
Public g_sOrigen As String
Private sIdiProceso As String
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean


Private m_sMsgError As String
Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim vUsu As Variant
Dim vNomUsu As Variant
Dim vFecha As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
With frmOFERec.m_oProveSeleccionado

    If txtFec.Text = "" Then
        oMensajes.DatoObligatorio Left(lblFec(1).caption, Len(lblFec(1).caption) - 1)
        Exit Sub
    End If
    If Not IsDate(txtFec.Text) Then
        oMensajes.NoValido Left(lblFec(1).caption, Len(lblFec(1).caption) - 1)
        Exit Sub
    End If
    
    If IsNull(.NoOfeUsu) Then
        .NoOfeUsu = oUsuarioSummit.Cod
        If Not oUsuarioSummit.Persona Is Nothing Then
            If Not IsNull(oUsuarioSummit.Persona.nombre) Then
                .NoOfeNomUsu = oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos
            Else
                .NoOfeNomUsu = oUsuarioSummit.Persona.Apellidos
            End If
        Else
            .NoOfeNomUsu = Null
        End If
    End If
    If .NoOfe Then
        If Format(.NoOfeFecha, "Short date") <> Format(txtFec.Text, "Short date") Then
            .NoOfeFecha = CDate(txtFec.Text)
        End If
    Else
        .NoOfeFecha = CDate(txtFec.Text)
        .NoOfePortal = False
    End If
    .NoOfe = True
    .NoOfeMotivo = StrToNull(txtDescr.Text)
    teserror = .MarcarProveedorNoOferta(frmOFERec.m_oProcesoSeleccionado.Anyo, frmOFERec.m_oProcesoSeleccionado.GMN1Cod, frmOFERec.m_oProcesoSeleccionado.Cod)
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
End With
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMotivoNoOfe", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
 
End Sub

''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendarFec_Click()
    AbrirFormCalendar Me, txtFec
End Sub

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMotivoNoOfe", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub


Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMotivoNoOfe", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
Dim teserror As TipoErrorSummit
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    Me.Height = 5055
    Me.Width = 7480
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarRecursos
    
    If g_bEdicion Then
        lblFec(0).Visible = False
        lblFec(1).Visible = True
        txtFec.Visible = True
        cmdCalendarFec.Visible = True
        cmdAceptar.Visible = True
        cmdCancelar.Visible = True
        txtDescr.Locked = False
    Else
        lblFec(0).Visible = True
        lblFec(1).Visible = False
        txtFec.Visible = False
        cmdCalendarFec.Visible = False
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        txtDescr.Locked = True
        txtDescr.BackColor = &HE1FFFF
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMotivoNoOfe", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_MOFIVO_NO_OFE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblComent.caption = Ador(0).Value
        Ador.MoveNext
        Me.lblFec(0).caption = Ador(0).Value
        Me.lblFec(1).caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblMotivo.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmMotivoNoOfe", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    If g_bEdicion Then
        txtDescr.Height = Me.Height - 2220
        txtDescr.Width = Me.Width - 315
        
        cmdAceptar.Top = txtDescr.Height + txtDescr.Top + 70
        cmdCancelar.Top = cmdAceptar.Top
        cmdAceptar.Left = txtDescr.Width / 2 - cmdAceptar.Width - 50
        cmdCancelar.Left = txtDescr.Width / 2 + 50
    Else
        txtDescr.Height = Me.Height - 1900
        txtDescr.Width = Me.Width - 315
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMotivoNoOfe", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


