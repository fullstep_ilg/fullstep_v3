VERSION 5.00
Begin VB.Form frmCATDatExtImagenes3 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cargar imagenes"
   ClientHeight    =   2355
   ClientLeft      =   4065
   ClientTop       =   3795
   ClientWidth     =   6480
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtImagenes3.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2355
   ScaleWidth      =   6480
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2880
      TabIndex        =   7
      Top             =   1980
      Width           =   1155
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   ">>"
      Default         =   -1  'True
      Height          =   315
      Left            =   1590
      TabIndex        =   6
      Top             =   1980
      Width           =   1155
   End
   Begin VB.CheckBox chkNoMostrar 
      BackColor       =   &H00808000&
      Caption         =   "No mostrar imagenes de ""vista previa"" en el proceso de carga"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   60
      TabIndex        =   5
      Top             =   1500
      Width           =   6195
   End
   Begin VB.ComboBox cmbSiNo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      ItemData        =   "frmCATDatExtImagenes3.frx":0CB2
      Left            =   5220
      List            =   "frmCATDatExtImagenes3.frx":0CB4
      TabIndex        =   4
      Top             =   60
      Width           =   915
   End
   Begin VB.OptionButton opSimple 
      BackColor       =   &H00808000&
      Caption         =   "Simple"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   2340
      TabIndex        =   2
      Top             =   1080
      Width           =   1335
   End
   Begin VB.OptionButton opInter 
      BackColor       =   &H00808000&
      Caption         =   "Interpolaci�n"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   840
      TabIndex        =   1
      Top             =   1080
      Value           =   -1  'True
      Width           =   1335
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Seleccione el m�todo de redimensionamiento (Interpolaci�n es m�s lento pero puede obtener mejores resultados.)"
      ForeColor       =   &H8000000E&
      Height          =   495
      Left            =   360
      TabIndex        =   3
      Top             =   540
      Width           =   5775
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Generar las imagenes de ""vista previa"" para el cat�logo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5115
   End
End
Attribute VB_Name = "frmCATDatExtImagenes3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sPath As String
Public g_oProve As CProveedor
Public g_bImagenes As Boolean 'Han pulsado el bot�n de cargar imagenes
Private m_sSi As String
Private m_sNo As String




Private Sub cmdAceptar_Click()
    
    If cmbSiNo.Text = m_sSi Then
        frmCATDatExtImagenes2.bThumbnails = True
    Else
        frmCATDatExtImagenes2.bThumbnails = False
    End If
    frmCATDatExtImagenes2.g_bImagenes = g_bImagenes
    frmCATDatExtImagenes2.sPath = sPath
    Set frmCATDatExtImagenes2.g_oProve = g_oProve
    Me.Hide
    frmCATDatExtImagenes2.Show 1

End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_Imagenes1, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Label2.Caption = Ador(0).Value
        Ador.MoveNext
        opInter.Caption = Ador(0).Value
        Ador.MoveNext
        opSimple.Caption = Ador(0).Value
        Ador.MoveNext
        chkNoMostrar.Caption = Ador(0).Value
        Ador.MoveNext
        m_sSi = Ador(0).Value
        Ador.MoveNext
        m_sNo = Ador(0).Value
        Ador.Close
            
    End If
    
    Set Ador = Nothing
End Sub


Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub Form_Load()
Me.Height = 2760
Me.Width = 6600

CargarRecursos

cmbSiNo.AddItem m_sSi, 0
cmbSiNo.AddItem m_sNo, 1
cmbSiNo.ListIndex = 0

End Sub

Private Sub Form_Unload(Cancel As Integer)
Set g_oProve = Nothing
End Sub
