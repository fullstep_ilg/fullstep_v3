VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmFormAdjuntos 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   3255
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12285
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormAdjuntos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3255
   ScaleWidth      =   12285
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog cmmdAdjun 
      Left            =   4080
      Top             =   2760
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdRestaurar 
      Caption         =   "&Restaurar"
      Height          =   345
      Left            =   1200
      TabIndex        =   2
      Top             =   2860
      Width           =   1005
   End
   Begin VB.CommandButton cmdAnyadir 
      Caption         =   "&Anyadir"
      Height          =   345
      Left            =   120
      TabIndex        =   1
      Top             =   2860
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAdjuntos 
      Height          =   2700
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   12045
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   12
      stylesets.count =   4
      stylesets(0).Name=   "Eliminar"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormAdjuntos.frx":0CB2
      stylesets(1).Name=   "Sust"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFormAdjuntos.frx":0D48
      stylesets(2).Name=   "Abrir"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFormAdjuntos.frx":0DF9
      stylesets(3).Name=   "Guardar"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFormAdjuntos.frx":0E85
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      SelectByCell    =   -1  'True
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   12
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2196
      Columns(1).Caption=   "ARCHIVO"
      Columns(1).Name =   "ARCHIVO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Caption=   "RUTA"
      Columns(2).Name =   "RUTA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1588
      Columns(3).Caption=   "IDIOMA"
      Columns(3).Name =   "IDIOMA"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   1296
      Columns(4).Caption=   "SIZE"
      Columns(4).Name =   "SIZE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3043
      Columns(5).Caption=   "FECHA"
      Columns(5).Name =   "FECHA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2619
      Columns(6).Caption=   "USU"
      Columns(6).Name =   "USU"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   1588
      Columns(7).Caption=   "COMENT"
      Columns(7).Name =   "COMENT"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   4
      Columns(7).ButtonsAlways=   -1  'True
      Columns(8).Width=   1588
      Columns(8).Caption=   "SALVAR"
      Columns(8).Name =   "SALVAR"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   4
      Columns(8).ButtonsAlways=   -1  'True
      Columns(9).Width=   1588
      Columns(9).Caption=   "ABRIR"
      Columns(9).Name =   "ABRIR"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Style=   4
      Columns(9).ButtonsAlways=   -1  'True
      Columns(10).Width=   1588
      Columns(10).Caption=   "SUST"
      Columns(10).Name=   "SUST"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).Style=   4
      Columns(10).ButtonsAlways=   -1  'True
      Columns(11).Width=   1482
      Columns(11).Caption=   "ELIMINAR"
      Columns(11).Name=   "ELIMINAR"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).Style=   4
      Columns(11).ButtonsAlways=   -1  'True
      _ExtentX        =   21246
      _ExtentY        =   4762
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFormAdjuntos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

'Variables p�blicas
Public g_bModif As Boolean
Public g_oSolicitudSeleccionada As CSolicitud
Public g_oCampoSeleccionado As CFormItem
Public g_oLineaSeleccionada As CLineaDesglose
Public g_oInstanciaSeleccionada As CInstancia
Public g_sOrigen As String

'Para las especificaciones
Public g_bCancelarEsp As Boolean
Public g_sComentario As String
Public g_bSoloRuta As Boolean

'Variables privadas
Private m_oIBaseDatos As IBaseDatos
Private m_aSayFileNames() As String
Private m_oIdiomas As CIdiomas

'Variables de idiomas:
Private m_sIdiForm As String
Private m_sIdiAdjun As String
Private m_sIdiKB As String
Private m_sIdiSelecAdjunto As String
Private m_sIdiTodosArchivos As String
Private m_sIdiElArchivo As String
Private m_sIdiGuardar As String
Private m_sIdiTipoOrig As String
Private m_sIdiSolicitud As String

''' <summary>
''' A�ade un nuevo archivo adjunto al campo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAnyadir_Click()
Dim DataFile As Integer
Dim oAdjunto As CCampoAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim TESError As TipoErrorSummit
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject
Dim sRuta As String
Dim sdrive As String
Dim sdrive2 As String
Dim oDrive As Scripting.Drive

On Error GoTo Cancelar:

    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
            Set m_oIBaseDatos = g_oSolicitudSeleccionada
        
        Case "frmFormularios"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
            Set m_oIBaseDatos = g_oCampoSeleccionado
            
        Case "frmDesgloseValores"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
            Set m_oIBaseDatos = g_oLineaSeleccionada
        
        Case "frmSolicitudDetalle"
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
            Set m_oIBaseDatos = g_oCampoSeleccionado
            
        Case "frmSolicitudDesglose", "frmSolicitudDesgloseP"
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
            Set m_oIBaseDatos = g_oLineaSeleccionada
    End Select
    
    Screen.MousePointer = vbHourglass
        
    TESError = m_oIBaseDatos.IniciarEdicion
    If TESError.NumError <> TESnoerror And TESError.NumError <> TESInfActualModificada Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError TESError
        m_oIBaseDatos.CancelarEdicion
        Set m_oIBaseDatos = Nothing
        Exit Sub
    End If
    
    Screen.MousePointer = vbNormal

    cmmdAdjun.DialogTitle = m_sIdiSelecAdjunto
    cmmdAdjun.Filter = m_sIdiTodosArchivos & "|*.*"
    cmmdAdjun.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    cmmdAdjun.filename = ""
    cmmdAdjun.ShowOpen

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle
    If sFileName = "" Then
        Screen.MousePointer = vbNormal
        If Not m_oIBaseDatos Is Nothing Then
            m_oIBaseDatos.CancelarEdicion
            Set m_oIBaseDatos = Nothing
        End If
        Exit Sub
    End If

    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para el adjunto
    frmPROCEComFich.sOrigen = "frmFormAdjuntos"
    frmPROCEComFich.chkProcFich.Visible = False
    
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    g_bCancelarEsp = False
    Screen.MousePointer = vbNormal
    frmPROCEComFich.Show 1

    Screen.MousePointer = vbHourglass

    If g_bSoloRuta = False Then 'Si es adjunto no pasa por aqui
    
        If Not g_bCancelarEsp Then
            
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
            
                Set oAdjunto = oFSGSRaiz.Generar_CCampoAdjunto
                
                Select Case g_sOrigen
                    Case "frmSOLConfiguracion"
                        Set oAdjunto.Solicitud = g_oSolicitudSeleccionada
                    Case "frmFormularios"
                        Set oAdjunto.Campo = g_oCampoSeleccionado
                    Case "frmDesgloseValores"
                        Set oAdjunto.Linea = g_oLineaSeleccionada
                    Case "frmSolicitudDetalle"
                        Set oAdjunto.Campo = g_oCampoSeleccionado
                        Set oAdjunto.Instancia = g_oInstanciaSeleccionada
                    Case "frmSolicitudDesglose", "frmSolicitudDesgloseP"
                        Set oAdjunto.Linea = g_oLineaSeleccionada
                        Set oAdjunto.Instancia = g_oInstanciaSeleccionada
                End Select
        
                oAdjunto.nombre = sFileTitle
                oAdjunto.Comentario = g_sComentario
                oAdjunto.Fecha = CDate(Date & " " & Time)
                oAdjunto.idioma = gParametrosInstalacion.gIdioma
                oAdjunto.Ruta = Null
                                
                Set oAdjunto.Persona = oFSGSRaiz.Generar_CPersona
                oAdjunto.Persona.Cod = basOptimizacion.gCodPersonaUsuario
                                    
                Dim sAdjunto As String
                Dim ArrayAdjunto() As String
                Dim oFile As File
                Dim bites As Long
                
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = oAdjunto.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites)
                
                'Creamos un array, cada "substring" se asignar�
                'a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oAdjunto.Id = ArrayAdjunto(0)
                oAdjunto.DataSize = bites
            
            Next iFile
        End If
    
    Else   ' Guardamos la ruta en lugar del archivo
        If Not g_bCancelarEsp Then
            Set oFos = New FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                
                sdrive = oFos.GetDriveName(sFileName)
                sRuta = Left(sFileName, Len(sFileName) - Len(sFileTitle))
                sRuta = Right(sRuta, Len(sRuta) - Len(sdrive))
                Set oDrive = oFos.GetDrive(sdrive)
                sdrive2 = oDrive.ShareName
                
                If sdrive2 = "" Then
                    sRuta = sdrive & sRuta
                Else
                    sRuta = sdrive2 & sRuta
                End If
                If sRuta = "" Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                Set oAdjunto = oFSGSRaiz.Generar_CCampoAdjunto
                
                Select Case g_sOrigen
                    Case "frmSOLConfiguracion"
                        Set oAdjunto.Solicitud = g_oSolicitudSeleccionada
                    Case "frmFormularios"
                        Set oAdjunto.Campo = g_oCampoSeleccionado
                    Case "frmDesgloseValores"
                        Set oAdjunto.Linea = g_oLineaSeleccionada
                    Case "frmSolicitudDetalle"
                        Set oAdjunto.Campo = g_oCampoSeleccionado
                        Set oAdjunto.Instancia = g_oInstanciaSeleccionada
                    Case "frmSolicitudDesglose", "frmSolicitudDesgloseP"
                        Set oAdjunto.Linea = g_oLineaSeleccionada
                        Set oAdjunto.Instancia = g_oInstanciaSeleccionada
                End Select
    
            
                oAdjunto.nombre = sFileTitle
                oAdjunto.Comentario = g_sComentario
                oAdjunto.Ruta = sRuta
                oAdjunto.DataSize = FileLen(sFileName)
                oAdjunto.idioma = gParametrosInstalacion.gIdioma
                
                Set oAdjunto.Persona = oFSGSRaiz.Generar_CPersona
                oAdjunto.Persona.Cod = basOptimizacion.gCodPersonaUsuario
                
                
                Set m_oIBaseDatos = oAdjunto
                
                TESError = m_oIBaseDatos.AnyadirABaseDatos
                If TESError.NumError <> TESnoerror Then
                    basErrores.TratarError TESError
                    Set oAdjunto = Nothing
                    Set oDrive = Nothing
                    Set oFos = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                Set oDrive = Nothing
            Next
        End If
        
    End If
    
    If Not g_bCancelarEsp Then
        oAdjunto.ObtenerID
        Select Case g_sOrigen
            Case "frmSOLConfiguracion"
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolConfAnyaAdjun, "Id:" & g_oSolicitudSeleccionada.Id & "Archivo:" & sFileTitle
                
                If g_oSolicitudSeleccionada.Adjuntos Is Nothing Then
                    Set g_oSolicitudSeleccionada.Adjuntos = oFSGSRaiz.Generar_CCampoAdjuntos
                End If
                g_oSolicitudSeleccionada.Adjuntos.Add oAdjunto.Id, oAdjunto.Fecha, oAdjunto.nombre, oAdjunto.DataSize, oAdjunto.Comentario, oAdjunto.Ruta, oAdjunto.idioma, oAdjunto.Persona, , , , , g_oSolicitudSeleccionada
                
            Case "frmFormularios"
                basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAnyaAdjun, "Id:" & g_oCampoSeleccionado.Id & "Archivo:" & sFileTitle
                
                If g_oCampoSeleccionado.Adjuntos Is Nothing Then
                    Set g_oCampoSeleccionado.Adjuntos = oFSGSRaiz.Generar_CCampoAdjuntos
                End If
                g_oCampoSeleccionado.Adjuntos.Add oAdjunto.Id, oAdjunto.Fecha, oAdjunto.nombre, oAdjunto.DataSize, oAdjunto.Comentario, oAdjunto.Ruta, oAdjunto.idioma, oAdjunto.Persona, g_oCampoSeleccionado
            
            Case "frmDesgloseValores"
                basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAnyaAdjun, "Linea:" & g_oLineaSeleccionada.Linea & "Archivo:" & sFileTitle
                
                If g_oLineaSeleccionada.Adjuntos Is Nothing Then
                    Set g_oLineaSeleccionada.Adjuntos = oFSGSRaiz.Generar_CCampoAdjuntos
                End If
                g_oLineaSeleccionada.Adjuntos.Add oAdjunto.Id, oAdjunto.Fecha, oAdjunto.nombre, oAdjunto.DataSize, oAdjunto.Comentario, oAdjunto.Ruta, oAdjunto.idioma, oAdjunto.Persona, , g_oLineaSeleccionada
            
            Case "frmSolicitudDetalle"
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAnyaAdjun, "Id:" & g_oCampoSeleccionado.Id & "Archivo:" & sFileTitle
                
                If g_oCampoSeleccionado.Adjuntos Is Nothing Then
                    Set g_oCampoSeleccionado.Adjuntos = oFSGSRaiz.Generar_CCampoAdjuntos
                End If
                g_oCampoSeleccionado.Adjuntos.Add oAdjunto.Id, oAdjunto.Fecha, oAdjunto.nombre, oAdjunto.DataSize, oAdjunto.Comentario, oAdjunto.Ruta, oAdjunto.idioma, oAdjunto.Persona, g_oCampoSeleccionado, , , , , oAdjunto.Instancia
                
            Case "frmSolicitudDesglose", "frmSolicitudDesgloseP"
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAnyaAdjun, "Linea:" & g_oLineaSeleccionada.Linea & "Archivo:" & sFileTitle
                
                If g_oLineaSeleccionada.Adjuntos Is Nothing Then
                    Set g_oLineaSeleccionada.Adjuntos = oFSGSRaiz.Generar_CCampoAdjuntos
                End If
                g_oLineaSeleccionada.Adjuntos.Add oAdjunto.Id, oAdjunto.Fecha, oAdjunto.nombre, oAdjunto.DataSize, oAdjunto.Comentario, oAdjunto.Ruta, oAdjunto.idioma, oAdjunto.Persona, , g_oLineaSeleccionada, , , , oAdjunto.Instancia
        End Select
    
    
        'A�ade el adjunto a la grid:
        
        oAdjunto.Persona.nombre = oUsuarioSummit.Persona.nombre
        oAdjunto.Persona.Apellidos = oUsuarioSummit.Persona.Apellidos
        
                
        RellenarGrid oAdjunto
    End If
    
    Set oFos = Nothing
    Set oAdjunto = Nothing
    m_oIBaseDatos.CancelarEdicion
    Set m_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal

    Exit Sub

Cancelar:

    Screen.MousePointer = vbNormal
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Close DataFile
    End If
    Set oAdjunto = Nothing

    If Not m_oIBaseDatos Is Nothing Then
        m_oIBaseDatos.CancelarEdicion
        Set m_oIBaseDatos = Nothing
    End If
End Sub

Private Sub cmdRestaurar_Click()
    CargarGridAdjuntos
End Sub

Private Sub Form_Load()
    Me.Height = 3765
    Me.Width = 12405
    
    CargarRecursos
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            Me.caption = m_sIdiSolicitud & ": " & NullToStr(g_oSolicitudSeleccionada.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        Case "frmFormularios"
            Me.caption = m_sIdiForm & g_oCampoSeleccionado.Grupo.Formulario.Den & " " & NullToStr(g_oCampoSeleccionado.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        Case "frmDesgloseValores"
            Me.caption = m_sIdiForm & g_oLineaSeleccionada.CampoHijo.Grupo.Formulario.Den & " " & NullToStr(g_oLineaSeleccionada.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        Case "frmSolicitudDetalle"
            Me.caption = m_sIdiSolicitud & ":" & NullToStr(g_oCampoSeleccionado.Grupo.Instancia.DescrBreve) & " " & NullToStr(g_oCampoSeleccionado.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            sdbgAdjuntos.Columns("RUTA").Visible = False
        Case "frmSolicitudDesglose", "frmSolicitudDesgloseP", "frmSOLAbrirProc"
            Me.caption = m_sIdiSolicitud & ":" & NullToStr(g_oLineaSeleccionada.CampoHijo.Grupo.Instancia.DescrBreve) & " " & NullToStr(g_oLineaSeleccionada.CampoHijo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            sdbgAdjuntos.Columns("RUTA").Visible = False
    End Select
    
    'Inicializar el array de ficheros
    ReDim m_aSayFileNames(0)

    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    'si no tiene permisos de modificaci�n solo podr� consultar los adjuntos
    If g_bModif = False Then
        sdbgAdjuntos.Columns("SUST").Visible = False
        sdbgAdjuntos.Columns("ELIMINAR").Visible = False
        
        cmdAnyadir.Visible = False
        cmdRestaurar.Left = cmdAnyadir.Left
    End If
    
    CargarGridAdjuntos
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FORM_ADJUN, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        m_sIdiForm = Ador(0).Value & ":"   '1 Formulario
        Ador.MoveNext
        m_sIdiAdjun = Ador(0).Value '2 Archivos adjuntos
        Ador.MoveNext
        sdbgAdjuntos.Columns("ARCHIVO").caption = Ador(0).Value  '3 Archivo
        Ador.MoveNext
        sdbgAdjuntos.Columns("RUTA").caption = Ador(0).Value  '4 Ruta
        Ador.MoveNext
        sdbgAdjuntos.Columns("IDIOMA").caption = Ador(0).Value  '5 Idioma
        Ador.MoveNext
        sdbgAdjuntos.Columns("SIZE").caption = Ador(0).Value  '6 Tama�o
        Ador.MoveNext
        sdbgAdjuntos.Columns("FECHA").caption = Ador(0).Value  '7 Fecha
        Ador.MoveNext
        sdbgAdjuntos.Columns("USU").caption = Ador(0).Value  '8 Usuario
        Ador.MoveNext
        sdbgAdjuntos.Columns("COMENT").caption = Ador(0).Value  '9 Comentario
        Ador.MoveNext
        sdbgAdjuntos.Columns("SALVAR").caption = Ador(0).Value  '10 Salvar
        Ador.MoveNext
        sdbgAdjuntos.Columns("ABRIR").caption = Ador(0).Value  '11 Abrir
        Ador.MoveNext
        sdbgAdjuntos.Columns("SUST").caption = Ador(0).Value  '12 Sustituir
        Ador.MoveNext
        sdbgAdjuntos.Columns("ELIMINAR").caption = Ador(0).Value  '13 Eliminar
        Ador.MoveNext
        cmdAnyadir.caption = Ador(0).Value   '14 &A�adir
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value  '15 &Restaurar
        Ador.MoveNext
        m_sIdiKB = Ador(0).Value  '16 KB
        Ador.MoveNext
        m_sIdiSelecAdjunto = Ador(0).Value  '17 Seleccionar archivo adjunto
        Ador.MoveNext
        m_sIdiElArchivo = Ador(0).Value   '18 Archivo
        Ador.MoveNext
        m_sIdiTodosArchivos = Ador(0).Value  '19  Todos los archivos
        Ador.MoveNext
        m_sIdiGuardar = Ador(0).Value  '20  Guardar archivo adjunto
        Ador.MoveNext
        m_sIdiTipoOrig = Ador(0).Value  '21  Tipo original
        Ador.MoveNext
        m_sIdiSolicitud = Ador(0).Value   '22 Tipo de solicitud
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Resize()
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 1100 Then Exit Sub
    
    sdbgAdjuntos.Width = Me.Width - 360
    sdbgAdjuntos.Height = Me.Height - 1065
    
    If g_bModif = True Then
        If sdbgAdjuntos.Columns("RUTA").Visible = True Then
            sdbgAdjuntos.Columns("ARCHIVO").Width = sdbgAdjuntos.Width / 9
            sdbgAdjuntos.Columns("RUTA").Width = sdbgAdjuntos.Width / 9
            sdbgAdjuntos.Columns("USU").Width = sdbgAdjuntos.Width / 9
        Else
            sdbgAdjuntos.Columns("ARCHIVO").Width = sdbgAdjuntos.Width / 6
            sdbgAdjuntos.Columns("USU").Width = sdbgAdjuntos.Width / 6
        End If
        sdbgAdjuntos.Columns("IDIOMA").Width = sdbgAdjuntos.Width / 14
        sdbgAdjuntos.Columns("SIZE").Width = sdbgAdjuntos.Width / 13
    Else
        If sdbgAdjuntos.Columns("RUTA").Visible = True Then
            sdbgAdjuntos.Columns("ARCHIVO").Width = sdbgAdjuntos.Width / 7
            sdbgAdjuntos.Columns("RUTA").Width = sdbgAdjuntos.Width / 7
            sdbgAdjuntos.Columns("USU").Width = sdbgAdjuntos.Width / 7
        Else
            sdbgAdjuntos.Columns("ARCHIVO").Width = sdbgAdjuntos.Width / 4
            sdbgAdjuntos.Columns("USU").Width = sdbgAdjuntos.Width / 6
        End If
        sdbgAdjuntos.Columns("IDIOMA").Width = sdbgAdjuntos.Width / 12
        sdbgAdjuntos.Columns("SIZE").Width = sdbgAdjuntos.Width / 12
    End If
    sdbgAdjuntos.Columns("FECHA").Width = sdbgAdjuntos.Width / 7
    sdbgAdjuntos.Columns("COMENT").Width = sdbgAdjuntos.Width / 12
    sdbgAdjuntos.Columns("SALVAR").Width = sdbgAdjuntos.Width / 16
    sdbgAdjuntos.Columns("ABRIR").Width = sdbgAdjuntos.Width / 16
    sdbgAdjuntos.Columns("SUST").Width = sdbgAdjuntos.Width / 16
    sdbgAdjuntos.Columns("ELIMINAR").Width = sdbgAdjuntos.Width / 16
    
    cmdAnyadir.Top = sdbgAdjuntos.Top + sdbgAdjuntos.Height + 55
    cmdRestaurar.Top = cmdAnyadir.Top
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim FOSFile As Scripting.FileSystemObject
Dim i As Integer

    'Borramos los archivos temporales que hayamos creado
On Error GoTo ERROR:
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(m_aSayFileNames)
        If FOSFile.FileExists(m_aSayFileNames(i)) Then
            FOSFile.DeleteFile m_aSayFileNames(i), True
        End If
        i = i + 1
    Wend
    Set FOSFile = Nothing
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If Not ((IsNull(frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_ADJUNTOS").Value) And Me.sdbgAdjuntos.Rows = 0) Or (NullToDbl0(frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_ADJUNTOS").Value) = Me.sdbgAdjuntos.Rows)) Then
                frmSOLConfiguracion.m_bRespetar = True
                frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_ADJUNTOS").Value = Me.sdbgAdjuntos.Rows
                frmSOLConfiguracion.ssSOLConfig.Update
                frmSOLConfiguracion.ssSOLConfig.Refresh
                frmSOLConfiguracion.m_bRespetar = False
            End If
        Case "frmFormularios"
            frmFormularios.ModificarAdjuntos g_oCampoSeleccionado
        Case "frmDesgloseValores"
            frmDesgloseValores.ModificarAdjuntos g_oLineaSeleccionada
        Case "frmSolicitudDetalle"
            frmSolicitudes.g_ofrmDetalleSolic.ModificarAdjuntos g_oCampoSeleccionado
        Case "frmSolicitudDesglose"
            frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ModificarAdjuntos g_oLineaSeleccionada
        Case "frmSolicitudDesgloseP"
            frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ModificarAdjuntos g_oLineaSeleccionada
    End Select
    
    Set g_oInstanciaSeleccionada = Nothing
    Set g_oSolicitudSeleccionada = Nothing
    Set g_oCampoSeleccionado = Nothing
    Set g_oLineaSeleccionada = Nothing
    Set m_oIdiomas = Nothing
    g_sOrigen = ""
    g_bModif = False
    
ERROR:
    Resume Next
End Sub


Private Sub CargarGridAdjuntos()
    Dim oAdjunto As CCampoAdjunto

    sdbgAdjuntos.RemoveAll
      
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            g_oSolicitudSeleccionada.CargarAdjuntos
            
            If g_oSolicitudSeleccionada.Adjuntos Is Nothing Then Exit Sub
            If g_oSolicitudSeleccionada.Adjuntos.Count = 0 Then Exit Sub
            
            For Each oAdjunto In g_oSolicitudSeleccionada.Adjuntos
                RellenarGrid oAdjunto
            Next
        
        Case "frmFormularios"
            g_oCampoSeleccionado.CargarAdjuntos
            
            If g_oCampoSeleccionado.Adjuntos Is Nothing Then Exit Sub
            If g_oCampoSeleccionado.Adjuntos.Count = 0 Then Exit Sub
            
            For Each oAdjunto In g_oCampoSeleccionado.Adjuntos
                RellenarGrid oAdjunto
            Next
          
        Case "frmDesgloseValores"
            g_oLineaSeleccionada.CargarAdjuntos
            
            If g_oLineaSeleccionada.Adjuntos Is Nothing Then Exit Sub
            If g_oLineaSeleccionada.Adjuntos.Count = 0 Then Exit Sub
            
            For Each oAdjunto In g_oLineaSeleccionada.Adjuntos
                RellenarGrid oAdjunto
            Next
        
        Case "frmSolicitudDetalle"
            g_oCampoSeleccionado.CargarAdjuntos
            
            If g_oCampoSeleccionado.Adjuntos Is Nothing Then Exit Sub
            If g_oCampoSeleccionado.Adjuntos.Count = 0 Then Exit Sub
            
            For Each oAdjunto In g_oCampoSeleccionado.Adjuntos
                RellenarGrid oAdjunto
            Next
            
        Case "frmSolicitudDesglose", "frmSolicitudDesgloseP", "frmSOLAbrirProc"
            g_oLineaSeleccionada.CargarAdjuntos
            
            If g_oLineaSeleccionada.Adjuntos Is Nothing Then Exit Sub
            If g_oLineaSeleccionada.Adjuntos.Count = 0 Then Exit Sub
            
            For Each oAdjunto In g_oLineaSeleccionada.Adjuntos
                RellenarGrid oAdjunto
            Next
    End Select

End Sub


Private Sub sdbgAdjuntos_BtnClick()
    If sdbgAdjuntos.col < 0 Then Exit Sub
    
    If sdbgAdjuntos.Rows = 0 Then
        oMensajes.SeleccioneFichero
    End If
            
    Select Case sdbgAdjuntos.Columns(sdbgAdjuntos.col).Name
        Case "SALVAR"
            GuardarAdjunto
            
        Case "ABRIR"
            AbrirAdjunto
            
        Case "SUST"
            SustituirAdjunto
            
        Case "ELIMINAR"
            EliminarAdjunto
            
        Case "COMENT"
            ModificarComentario
    End Select
End Sub

Private Sub sdbgAdjuntos_RowLoaded(ByVal Bookmark As Variant)
    sdbgAdjuntos.Columns("SALVAR").CellStyleSet "Guardar"
    sdbgAdjuntos.Columns("ABRIR").CellStyleSet "Abrir"
    sdbgAdjuntos.Columns("SUST").CellStyleSet "Sust"
    sdbgAdjuntos.Columns("ELIMINAR").CellStyleSet "Eliminar"
    
End Sub

''' <summary>
''' Abre un archivo adjunto
''' </summary>
''' <remarks>Llamada desde: sdbgAdjuntos_BtnClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub AbrirAdjunto()
Dim DataFile As Integer
Dim oAdjun As CCampoAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim FOSFile As Scripting.FileSystemObject
Dim oFos As Scripting.FileSystemObject
Dim oFile As Scripting.File

On Error GoTo Cancelar:

    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
        Case "frmFormularios"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
        Case "frmDesgloseValores"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
        Case "frmSolicitudDetalle"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
        Case "frmSolicitudDesglose", "frmSolicitudDesgloseP", "frmSOLAbrirProc"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
    End Select
    
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & sdbgAdjuntos.Columns("ARCHIVO").Value
    sFileTitle = sdbgAdjuntos.Columns("ARCHIVO").Value
    
    'Comprueba si existe el fichero y si existe se borra:
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing

    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            Set oAdjun = g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Solicitud = g_oSolicitudSeleccionada
        Case "frmFormularios"
            Set oAdjun = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Campo = g_oCampoSeleccionado
        Case "frmDesgloseValores"
            Set oAdjun = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Linea = g_oLineaSeleccionada
        Case "frmSolicitudDetalle"
            Set oAdjun = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Campo = g_oCampoSeleccionado
            Set oAdjun.Instancia = g_oInstanciaSeleccionada
        Case "frmSolicitudDesglose", "frmSOLAbrirProc"
            Set oAdjun = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Linea = g_oLineaSeleccionada
            Set oAdjun.Instancia = g_oInstanciaSeleccionada
    End Select
    
    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If


    If Not IsNull(oAdjun.Ruta) Then 'los archivos son vinculados
        Set oFos = New FileSystemObject
        ''''estas cuatro lineas te devuelven un error debido a que no tiene permiso de acceso pero no devuelve el numero de
        Set oFile = oFos.GetFile(oAdjun.Ruta & oAdjun.nombre)
        oFile.OpenAsTextStream.ReadAll ''error

        If oFos.FolderExists(oAdjun.Ruta) And oFos.FileExists(oAdjun.Ruta & oAdjun.nombre) Then
            ShellExecute MDI.hWnd, "Open", oAdjun.Ruta & oAdjun.nombre, 0&, oAdjun.Ruta, 1
        Else
            oMensajes.ImposibleModificarEsp (oAdjun.nombre & " - " & oAdjun.Ruta)
        End If
        
    Else ' los archivos son adjuntos, se leen de base de datos
        Dim arrData() As Byte
        arrData = oAdjun.LeerAdjunto(oAdjun.Id)
        WriteByteArray sFileName, arrData
        
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
    
    Set oAdjun = Nothing
    Set oFos = Nothing
    Set oFile = Nothing
    
    Exit Sub

Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    If DataFile <> 0 Then
        Close DataFile
        DataFile = 0
    End If

    Set oAdjun = Nothing
    Set oFos = Nothing
End Sub

''' <summary>
''' A�ade un nuevo archivo adjunto al campo
''' </summary>
''' <remarks>Llamada desde: sdbgAdjuntos_BtnClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarAdjunto()
Dim DataFile As Integer
Dim oAdjun As CCampoAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim oFos As Scripting.FileSystemObject

On Error GoTo Cancelar:

    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
        Case "frmFormularios"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
        Case "frmDesgloseValores"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
        Case "frmSolicitudDetalle"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
        Case "frmSolicitudDesglose", "frmSOLAbrirProc"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
    End Select

    cmmdAdjun.DialogTitle = m_sIdiGuardar
    cmmdAdjun.CancelError = True
    cmmdAdjun.Filter = m_sIdiTipoOrig & "|*.*"
    cmmdAdjun.filename = sdbgAdjuntos.Columns("ARCHIVO").Value
    cmmdAdjun.ShowSave

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdiElArchivo
        Exit Sub
    End If
    DataFile = 1

    ' Cargamos el contenido en la esp.
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            Set oAdjun = g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Solicitud = g_oSolicitudSeleccionada
            
        Case "frmFormularios"
            Set oAdjun = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Campo = g_oCampoSeleccionado
            
        Case "frmDesgloseValores"
            Set oAdjun = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Linea = g_oLineaSeleccionada
            
        Case "frmSolicitudDetalle"
            Set oAdjun = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Campo = g_oCampoSeleccionado
            Set oAdjun.Instancia = g_oInstanciaSeleccionada
            
        Case "frmSolicitudDesglose", "frmSOLAbrirProc"
            Set oAdjun = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Linea = g_oLineaSeleccionada
            Set oAdjun.Instancia = g_oInstanciaSeleccionada
    End Select
    
    If IsNull(oAdjun.Ruta) Then 'Los archivos son adjuntos
        Dim arrData() As Byte
        arrData = oAdjun.LeerAdjunto(oAdjun.Id)
        WriteByteArray sFileName, arrData
    Else
        ' los archivos son vinculados
        Set oFos = New FileSystemObject
        If oFos.FolderExists(oAdjun.Ruta) And oFos.FileExists(oAdjun.Ruta & oAdjun.nombre) Then
            oFos.CopyFile oAdjun.Ruta & oAdjun.nombre, sFileName, False
            Exit Sub
        Else
            oMensajes.ImposibleModificarEsp (oAdjun.nombre & " - " & oAdjun.Ruta)
        End If
        
        Set oFos = Nothing
        Set oAdjun = Nothing
    End If
    
    
Cancelar:
    On Error Resume Next

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Close DataFile
        Set oAdjun = Nothing
    End If
End Sub

Private Sub EliminarAdjunto()
Dim oAdjun As CCampoAdjunto
Dim TESError As TipoErrorSummit
Dim irespuesta As Integer

On Error GoTo Cancelar:

    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
        Case "frmFormularios"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
        Case "frmDesgloseValores"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
        Case "frmSolicitudDetalle"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
        Case "frmSolicitudDesglose"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
    End Select


    irespuesta = oMensajes.PreguntaEliminar(m_sIdiElArchivo & ": " & sdbgAdjuntos.Columns("ARCHIVO").Value)

    If irespuesta = vbNo Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            Set oAdjun = g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Solicitud = g_oSolicitudSeleccionada
        Case "frmFormularios"
            Set oAdjun = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Campo = g_oCampoSeleccionado
        Case "frmDesgloseValores"
            Set oAdjun = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Linea = g_oLineaSeleccionada
        Case "frmSolicitudDetalle"
            Set oAdjun = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Campo = g_oCampoSeleccionado
            Set oAdjun.Instancia = g_oInstanciaSeleccionada
        Case "frmSolicitudDesglose"
            Set oAdjun = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            Set oAdjun.Linea = g_oLineaSeleccionada
            Set oAdjun.Instancia = g_oInstanciaSeleccionada
    End Select
    
    Set m_oIBaseDatos = oAdjun

    TESError = m_oIBaseDatos.EliminarDeBaseDatos
    If TESError.NumError <> TESnoerror Then
        basErrores.TratarError TESError
    Else
        'Elimina el adjunto de la colecci�n:
        Select Case g_sOrigen
            Case "frmSOLConfiguracion"
                g_oSolicitudSeleccionada.Adjuntos.Remove (CStr(sdbgAdjuntos.Columns("ID").Value))
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolConfElimAdjun, "Id:" & g_oSolicitudSeleccionada.Id & "Archivo:" & sdbgAdjuntos.Columns("ARCHIVO").Value
                
            Case "frmFormularios"
                g_oCampoSeleccionado.Adjuntos.Remove (CStr(sdbgAdjuntos.Columns("ID").Value))
                basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAdjunEli, "Id:" & g_oCampoSeleccionado.Id & "Archivo:" & sdbgAdjuntos.Columns("ARCHIVO").Value
                
            Case "frmDesgloseValores"
                g_oLineaSeleccionada.Adjuntos.Remove (CStr(sdbgAdjuntos.Columns("ID").Value))
                basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAdjunEli, "Linea:" & g_oLineaSeleccionada.Linea & "Archivo:" & sdbgAdjuntos.Columns("ARCHIVO").Value
                
            Case "frmSolicitudDetalle"
                g_oCampoSeleccionado.Adjuntos.Remove (CStr(sdbgAdjuntos.Columns("ID").Value))
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAdjunEli, "Id:" & g_oCampoSeleccionado.Id & "Archivo:" & sdbgAdjuntos.Columns("ARCHIVO").Value
                
            Case "frmSolicitudDesglose"
                g_oLineaSeleccionada.Adjuntos.Remove (CStr(sdbgAdjuntos.Columns("ID").Value))
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAdjunEli, "Linea:" & g_oLineaSeleccionada.Linea & "Archivo:" & sdbgAdjuntos.Columns("ARCHIVO").Value
                
        End Select
        
        'Elimina el adjunto de la grid
        If sdbgAdjuntos.AddItemRowIndex(sdbgAdjuntos.Bookmark) > -1 Then
            sdbgAdjuntos.RemoveItem (sdbgAdjuntos.AddItemRowIndex(sdbgAdjuntos.Bookmark))
        Else
            sdbgAdjuntos.RemoveItem (0)
        End If
        If sdbgAdjuntos.Rows > 0 Then
            If IsEmpty(sdbgAdjuntos.RowBookmark(sdbgAdjuntos.Row)) Then
                sdbgAdjuntos.Bookmark = sdbgAdjuntos.RowBookmark(sdbgAdjuntos.Row - 1)
            Else
                sdbgAdjuntos.Bookmark = sdbgAdjuntos.RowBookmark(sdbgAdjuntos.Row)
            End If
        End If
        
    End If

    Set oAdjun = Nothing
    Set m_oIBaseDatos = Nothing

    Screen.MousePointer = vbNormal

Cancelar:

    Set oAdjun = Nothing
    Set m_oIBaseDatos = Nothing
End Sub


Private Sub ModificarComentario()
    Dim TESError As TipoErrorSummit

    If g_bModif = True Then
        Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
            Set g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Solicitud = g_oSolicitudSeleccionada
            Set m_oIBaseDatos = g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            
            TESError = m_oIBaseDatos.IniciarEdicion
            If TESError.NumError <> TESnoerror Then
                basErrores.TratarError TESError
                Set m_oIBaseDatos = Nothing
                Exit Sub
            End If
            
            basSeguridad.RegistrarAccion AccionesSummit.ACCSolConfModifAdjun, "Id:" & g_oSolicitudSeleccionada.Id
                
            frmPROCEEspMod.g_sOrigen = "frmFormAdjuntos"
    
        Case "frmFormularios"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
            Set g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Campo = g_oCampoSeleccionado
            Set m_oIBaseDatos = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            
            TESError = m_oIBaseDatos.IniciarEdicion
            If TESError.NumError <> TESnoerror Then
                basErrores.TratarError TESError
                Set m_oIBaseDatos = Nothing
                Exit Sub
            End If
            basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAdjunMod, "Id:" & g_oCampoSeleccionado.Id
                
            frmPROCEEspMod.g_sOrigen = "frmFormAdjuntos"
            
        Case "frmDesgloseValores"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
            Set g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Linea = g_oLineaSeleccionada
            Set m_oIBaseDatos = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
         
            TESError = m_oIBaseDatos.IniciarEdicion
            If TESError.NumError <> TESnoerror Then
                basErrores.TratarError TESError
                Set m_oIBaseDatos = Nothing
                Exit Sub
            End If
            basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAdjunMod, "Linea:" & g_oLineaSeleccionada.Linea
            
            frmPROCEEspMod.g_sOrigen = "frmDesgloseAdjuntos"
            
        Case "frmSolicitudDetalle"
            If g_oCampoSeleccionado Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
            Set g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Campo = g_oCampoSeleccionado
            Set m_oIBaseDatos = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
            
            TESError = m_oIBaseDatos.IniciarEdicion
            If TESError.NumError <> TESnoerror Then
                basErrores.TratarError TESError
                Set m_oIBaseDatos = Nothing
                Exit Sub
            End If
            basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAdjunMod, "Id:" & g_oCampoSeleccionado.Id
                
            frmPROCEEspMod.g_sOrigen = "frmFormAdjuntos"
            
        Case "frmSolicitudDesglose"
            If g_oLineaSeleccionada Is Nothing Then Exit Sub
            If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
            Set g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Linea = g_oLineaSeleccionada
            Set m_oIBaseDatos = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
         
            TESError = m_oIBaseDatos.IniciarEdicion
            If TESError.NumError <> TESnoerror Then
                basErrores.TratarError TESError
                Set m_oIBaseDatos = Nothing
                Exit Sub
            End If
            basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAdjunMod, "Linea:" & g_oLineaSeleccionada.Linea
            
            frmPROCEEspMod.g_sOrigen = "frmDesgloseAdjuntos"
        End Select
        
        Set frmPROCEEspMod.g_oIBaseDatos = m_oIBaseDatos
        frmPROCEEspMod.Show 1
    
        Set m_oIBaseDatos = Nothing
        
    Else
        Select Case g_sOrigen
            Case "frmSOLConfiguracion"
                If g_oSolicitudSeleccionada Is Nothing Then Exit Sub
                MostrarFormFormCampoDescr oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgAdjuntos.Columns("COMENT").caption, False, -1, _
                                          NullToStr(g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Comentario)
            Case "frmFormularios"
                If g_oCampoSeleccionado Is Nothing Then Exit Sub
                MostrarFormFormCampoDescr oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgAdjuntos.Columns("COMENT").caption, False, -1, _
                                          NullToStr(g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Comentario)
            Case "frmDesgloseValores"
                If g_oLineaSeleccionada Is Nothing Then Exit Sub
                MostrarFormFormCampoDescr oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgAdjuntos.Columns("COMENT").caption, False, -1, _
                                          NullToStr(g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Comentario)
            Case "frmSolicitudDetalle"
                If g_oCampoSeleccionado Is Nothing Then Exit Sub
                If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
                MostrarFormFormCampoDescr oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgAdjuntos.Columns("COMENT").caption, False, -1, _
                                          NullToStr(g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Comentario)
            Case "frmSolicitudDesglose"
                If g_oLineaSeleccionada Is Nothing Then Exit Sub
                If g_oInstanciaSeleccionada Is Nothing Then Exit Sub
                MostrarFormFormCampoDescr oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgAdjuntos.Columns("COMENT").caption, False, -1, _
                                          NullToStr(g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Comentario)
        End Select
    End If
End Sub

''' <summary>
''' A�ade un nuevo archivo adjunto al campo
''' </summary>
''' <remarks>Llamada desde: sdbgAdjuntos_BtnClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub SustituirAdjunto()
Dim DataFile As Integer
Dim oAdjunto As CCampoAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim TESError As TipoErrorSummit
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject
Dim sRuta As String
Dim sdrive As String
Dim sdrive2 As String
Dim oDrive As Scripting.Drive
    
On Error GoTo Cancelar

    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmSOLConfiguracion"
            Set m_oIBaseDatos = g_oSolicitudSeleccionada
        Case "frmFormularios"
            Set m_oIBaseDatos = g_oCampoSeleccionado
        Case "frmDesgloseValores"
            Set m_oIBaseDatos = g_oLineaSeleccionada
        Case "frmSolicitudDetalle"
            Set m_oIBaseDatos = g_oCampoSeleccionado
        Case "frmSolicitudDesglose"
            Set m_oIBaseDatos = g_oLineaSeleccionada
    End Select

    TESError = m_oIBaseDatos.IniciarEdicion
    If TESError.NumError <> TESnoerror And TESError.NumError <> TESInfActualModificada Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError TESError
        m_oIBaseDatos.CancelarEdicion
        Set m_oIBaseDatos = Nothing
        Exit Sub
    End If
    Screen.MousePointer = vbNormal

    cmmdAdjun.DialogTitle = m_sIdiSelecAdjunto
    cmmdAdjun.Filter = m_sIdiTodosArchivos & "|*.*"
    cmmdAdjun.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    cmmdAdjun.filename = ""
    cmmdAdjun.ShowOpen

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle
    If sFileName = "" Then
        Screen.MousePointer = vbNormal
        m_oIBaseDatos.CancelarEdicion
        Set m_oIBaseDatos = Nothing
        Exit Sub
    End If

    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para el adjunto
    frmPROCEComFich.sOrigen = "frmFormAdjuntos"
    frmPROCEComFich.chkProcFich.Visible = False
    
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    g_bCancelarEsp = False
    Screen.MousePointer = vbNormal
    frmPROCEComFich.Show 1
    
    Screen.MousePointer = vbHourglass

    If g_bSoloRuta = False Then 'Si es adjunto no pasa por aqui
    
        If Not g_bCancelarEsp Then
            
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
            
                Select Case g_sOrigen
                    Case "frmSOLConfiguracion"
                        Set oAdjunto = g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                        Set oAdjunto.Solicitud = g_oSolicitudSeleccionada
                    Case "frmFormularios"
                        Set oAdjunto = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                    Case "frmDesgloseValores"
                        Set oAdjunto = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                    Case "frmSolicitudDetalle"
                        Set oAdjunto = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                    Case "frmSolicitudDesglose"
                        Set oAdjunto = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                End Select
                oAdjunto.nombre = sFileTitle
                oAdjunto.Comentario = g_sComentario
                oAdjunto.Ruta = Null
                Dim oFile As File
                Dim bites As Long
    
                Dim sAdjunto As String
                Dim ArrayAdjunto() As String
                
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = oAdjunto.SustituirAdjuntoGs(oAdjunto.Id, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile), sFileTitle, bites)
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oAdjunto.DataSize = bites
                
            Next iFile
    
        End If
    
    Else   ' Guardamos la ruta en lugar del archivo
        If Not g_bCancelarEsp Then
            Set oFos = New FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                
                sdrive = oFos.GetDriveName(sFileName)
                sRuta = Left(sFileName, Len(sFileName) - Len(sFileTitle))
                sRuta = Right(sRuta, Len(sRuta) - Len(sdrive))
                Set oDrive = oFos.GetDrive(sdrive)
                sdrive2 = oDrive.ShareName
                
                If sdrive2 = "" Then
                    sRuta = sdrive & sRuta
                Else
                    sRuta = sdrive2 & sRuta
                End If
                If sRuta = "" Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                Select Case g_sOrigen
                    Case "frmSOLConfiguracion"
                        Set oAdjunto = g_oSolicitudSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                        Set oAdjunto.Solicitud = g_oSolicitudSeleccionada
                    Case "frmFormularios"
                        Set oAdjunto = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                    Case "frmDesgloseValores"
                        Set oAdjunto = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                    Case "frmSolicitudDetalle"
                        Set oAdjunto = g_oCampoSeleccionado.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                    Case "frmSolicitudDesglose"
                        Set oAdjunto = g_oLineaSeleccionada.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
                End Select
                oAdjunto.nombre = sFileTitle
                oAdjunto.Comentario = g_sComentario
                oAdjunto.Ruta = sRuta
                oAdjunto.DataSize = FileLen(sFileName)
                
                TESError = oAdjunto.SustituirAdjunto
                If TESError.NumError <> TESnoerror Then
                    basErrores.TratarError TESError
                    Set oAdjunto = Nothing
                    Set oDrive = Nothing
                    Set oFos = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                Set oDrive = Nothing
            Next
        End If
    End If
    
    
    If Not g_bCancelarEsp Then
        Select Case g_sOrigen
            Case "frmSOLConfiguracion"
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolConfModifAdjun, "Id:" & g_oSolicitudSeleccionada.Id & "Archivo:" & sFileTitle
            Case "frmFormularios"
                basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAdjunMod, "Id:" & g_oCampoSeleccionado.Id & "Archivo:" & sFileTitle
            Case "frmDesgloseValores"
                basSeguridad.RegistrarAccion AccionesSummit.ACCFormItemAdjunMod, "Linea:" & g_oLineaSeleccionada.Linea & "Archivo:" & sFileTitle
            Case "frmSolicitudDetalle"
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAdjunMod, "Id:" & g_oCampoSeleccionado.Id & "Archivo:" & sFileTitle
            Case "frmSolicitudDesglose"
                basSeguridad.RegistrarAccion AccionesSummit.ACCSolicAdjunMod, "Linea:" & g_oLineaSeleccionada.Linea & "Archivo:" & sFileTitle
        End Select
    
        'Actualiza la grid:
        sdbgAdjuntos.Columns("ARCHIVO").Value = NullToStr(oAdjunto.nombre)
        sdbgAdjuntos.Columns("RUTA").Value = NullToStr(oAdjunto.Ruta)
        sdbgAdjuntos.Columns("SIZE").Value = TamanyoAdjuntos(oAdjunto.DataSize / 1024) & " " & m_sIdiKB
        sdbgAdjuntos.Update
    End If
    
    Set oFos = Nothing
    Set oAdjunto = Nothing
    m_oIBaseDatos.CancelarEdicion
    Set m_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal

    Exit Sub

Cancelar:

    Screen.MousePointer = vbNormal
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Close DataFile
    End If
    Set oAdjunto = Nothing

    m_oIBaseDatos.CancelarEdicion
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub RellenarGrid(ByVal oAdjunto As CCampoAdjunto)
Dim sCadena As String

    'A�ade un adjunto a la grid:

    sCadena = oAdjunto.Id & Chr(9) & oAdjunto.nombre & Chr(9) & oAdjunto.Ruta & Chr(9) & m_oIdiomas.Item(CStr(oAdjunto.idioma)).Den
    sCadena = sCadena & Chr(9) & TamanyoAdjuntos(oAdjunto.DataSize / 1024) & " " & m_sIdiKB
    sCadena = sCadena & Chr(9) & Format(oAdjunto.Fecha, "General Date")
    
    If Not oAdjunto.Persona Is Nothing Then
        sCadena = sCadena & Chr(9) & NullToStr(oAdjunto.Persona.nombre) & " " & NullToStr(oAdjunto.Persona.Apellidos)
    End If
        
    sdbgAdjuntos.AddItem sCadena
End Sub
