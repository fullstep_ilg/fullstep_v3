VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosRolesCampoAsigna 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "DCampo que referencia al siguiente ROL:"
   ClientHeight    =   2010
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6195
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosRolesCampoAsigna.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2010
   ScaleWidth      =   6195
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   1680
      ScaleHeight     =   420
      ScaleWidth      =   2625
      TabIndex        =   6
      Top             =   1440
      Width           =   2625
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   195
         TabIndex        =   4
         Top             =   60
         Width           =   1050
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   1410
         TabIndex        =   5
         Top             =   60
         Width           =   1050
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcGrupos 
      Height          =   285
      Left            =   1500
      TabIndex        =   1
      Top             =   360
      Width           =   4215
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   7435
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   7435
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcCampos 
      Height          =   285
      Left            =   1500
      TabIndex        =   3
      Top             =   840
      Width           =   4215
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   7435
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   7435
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblGrupo 
      BackColor       =   &H00808000&
      Caption         =   "DGrupo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   420
      Width           =   1095
   End
   Begin VB.Label lblCampo 
      BackColor       =   &H00808000&
      Caption         =   "DCampo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   900
      Width           =   1095
   End
End
Attribute VB_Name = "frmFlujosRolesCampoAsigna"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public lIdFormulario As Long
Public sRol As String
Public iTipoRol As TipoPMRol
Public lIdCampo As Long
Public m_bModifFlujo As Boolean

Private m_lIdGrupo As Long

Private Sub cmdAceptar_Click()
    If lIdCampo > 0 Then
        Me.Hide
    End If
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = frmFlujosRoles.Top + (frmFlujosRoles.Height / 2) - (Me.Height / 2)
    If Me.Top < 0 Then
        Me.Top = 0
    End If
    Me.Left = frmFlujosRoles.Left + (frmFlujosRoles.Width / 2) - (Me.Width / 2)
    If Me.Left < 0 Then
        Me.Left = 0
    End If
    CargarRecursos
    ConfigurarSeguridad
    PreseleccionarCampo
End Sub

Private Sub ConfigurarSeguridad()
    If Not m_bModifFlujo Then
        sdbcGrupos.Enabled = False
        sdbcCampos.Enabled = False
        picEdit.Visible = False
        Me.Height = Me.Height - picEdit.Height
    End If
End Sub

Private Sub CargarRecursos()
   Dim ador As ador.Recordset
   
    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSROLESCAMPOASIGNA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
            
        Me.caption = ador(0).Value & " " & sRol '1
        ador.MoveNext
        lblGrupo.caption = ador(0).Value
        ador.MoveNext
        lblCampo.caption = ador(0).Value
        ador.MoveNext
        cmdAceptar.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value '5
                
        ador.Close
    End If
End Sub

Private Sub PreseleccionarCampo()
    Dim oCampo As CFormItem
    
    If lIdFormulario > 0 And lIdCampo > 0 Then
        Set oCampo = oFSGSRaiz.Generar_CFormCampo
        oCampo.Id = lIdCampo
        oCampo.CargarDatosFormCampo
    
        CargarGrupos
        sdbcGrupos.Value = oCampo.Grupo.Id
        m_lIdGrupo = oCampo.Grupo.Id
        CargarCampos
        sdbcCampos.Value = oCampo.Id
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    lIdCampo = 0
    m_lIdGrupo = 0
End Sub

Private Sub sdbcCampos_CloseUp()
    If sdbcCampos.Value = "" Then
        lIdCampo = 0
    Else
        lIdCampo = sdbcCampos.Value
    End If
End Sub

Private Sub sdbcCampos_DropDown()
    CargarCampos
End Sub

Private Sub CargarCampos()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem

    Screen.MousePointer = vbHourglass
    sdbcCampos.RemoveAll
    If m_lIdGrupo > 0 Then
        Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
        oGrupo.Id = m_lIdGrupo
        Set oGrupo.Formulario = oFSGSRaiz.Generar_CFormulario 'Lo pongo para enga�ar al objeto y que cargue los datos de FORM_CAMPO
        oGrupo.CargarTodosLosCampos
        If iTipoRol = TipoPMRol.RolComprador Or iTipoRol = TipoPMRol.RolUsuario Or iTipoRol = TipoPMRol.RolObservador Then
            For Each oCampo In oGrupo.Campos
                If oCampo.CampoGS = TipoCampoGS.CampoPersona Then
                    sdbcCampos.AddItem oCampo.Id & Chr(9) & oCampo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
                End If
            Next
        ElseIf iTipoRol = TipoPMRol.RolProveedor Then
            For Each oCampo In oGrupo.Campos
                If oCampo.CampoGS = TipoCampoGS.Proveedor Then
                    sdbcCampos.AddItem oCampo.Id & Chr(9) & oCampo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
                End If
            Next
        End If
    End If
    If sdbcCampos.Rows = 0 Then
        sdbcCampos.AddItem 0 & Chr(9) & ""
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCampos_InitColumnProps()
    sdbcCampos.DataFieldList = "Column 0"
    sdbcCampos.DataFieldToDisplay = "Column 1"

End Sub

Private Sub sdbcCampos_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCampos.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCampos.Rows - 1
            bm = sdbcCampos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCampos.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcCampos.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcGrupos_CloseUp()
    If sdbcGrupos.Value = "" Then
        m_lIdGrupo = 0
    Else
        m_lIdGrupo = sdbcGrupos.Value
    End If
   
End Sub

Private Sub sdbcGrupos_DropDown()
    CargarGrupos
End Sub

Private Sub CargarGrupos()
    Dim oFormulario As CFormulario
    Dim oGrupo As CFormGrupo

    Screen.MousePointer = vbHourglass
    
    sdbcGrupos.RemoveAll
    
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = lIdFormulario
    oFormulario.CargarTodosLosGrupos
    
    For Each oGrupo In oFormulario.Grupos
        sdbcGrupos.AddItem oGrupo.Id & Chr(9) & oGrupo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
    Next
    
    Set oGrupo = Nothing
    Set oFormulario = Nothing
        
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGrupos_InitColumnProps()
    sdbcGrupos.DataFieldList = "Column 0"
    sdbcGrupos.DataFieldToDisplay = "Column 1"

End Sub

Private Sub sdbcGrupoS_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGrupos.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGrupos.Rows - 1
            bm = sdbcGrupos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGrupos.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcGrupos.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
