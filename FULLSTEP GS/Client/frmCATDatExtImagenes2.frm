VERSION 5.00
Object = "{1E47E980-2496-11D3-ACB1-C0A64FC10000}#1.52#0"; "Pajant.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCATDatExtImagenes2 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cargando imagenes"
   ClientHeight    =   1695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6795
   Icon            =   "frmCATDatExtImagenes2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1695
   ScaleWidth      =   6795
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   660
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   767
      _Version        =   393216
      Appearance      =   1
   End
   Begin PAJANTLibCtl.PajantImageCtrl PajantImagen 
      Height          =   1575
      Left            =   5160
      OleObjectBlob   =   "frmCATDatExtImagenes2.frx":030A
      TabIndex        =   3
      Top             =   60
      Width           =   1575
   End
   Begin VB.Label lblImagen 
      BackColor       =   &H00808000&
      Caption         =   "Imagen: ART1.jpg"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   180
      Width           =   3915
   End
   Begin VB.Label lblNumImagenes 
      BackColor       =   &H00808000&
      Caption         =   "Archivo 1 de 20"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   1380
      Width           =   4995
   End
End
Attribute VB_Name = "frmCATDatExtImagenes2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_oProve As CProveedor
Public sPath As String
Public g_bImagenes As Boolean
Public bThumbnails As Boolean
Private m_sIdiImagen As String
Private m_sIdiArchivo As String

Private Sub Form_Activate()
    CargarImagenes
End Sub

Private Sub CargarImagenes()
Dim oFos As FileSystemObject
Dim oFolder As Scripting.Folder
Dim oFiles As Scripting.Files
Dim oFile As Scripting.File
Dim pi As PajantImage
Dim pi2 As PajantImage
Dim imgProve As Variant
Dim imgThumb As Variant
Dim oArticulos As CArticulos
Dim oArt As CArticulo
Dim oArts As CArticulos
Dim sArticulo As String
Dim arNoExisten() As String
Dim arErrorFormato() As String
Dim i As Integer
Dim j As Integer
Dim iContError As Integer
Dim iTotFiles As Integer
Dim teserror As TipoErrorSummit
Dim sCod As String
Dim sExtension As String
Dim dblPorcen As Double
Dim bGIF As Boolean
Dim sFileName As String
Dim sFilePath As String
Dim sFileNameSinEx As String
Dim s As Variant
Dim bContinuar As Boolean

On Error GoTo ERROR

Set oFos = New FileSystemObject
Set oFolder = oFos.GetFolder(sPath)
Set oFiles = oFolder.Files
Set oArticulos = oFSGSRaiz.Generar_CArticulos

Screen.MousePointer = vbHourglass

PajantImagen.pi.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"
i = 0
j = 0
iContError = 0
iTotFiles = oFiles.Count
dblPorcen = (100 \ iTotFiles)
m_sIdiArchivo = Replace(m_sIdiArchivo, "Y", CStr(iTotFiles))
For Each oFile In oFiles
    If ProgressBar1.Value = 100 Then ProgressBar1.Value = 0
    ProgressBar1.Value = ProgressBar1.Value + dblPorcen
    s = DevolverNombreExtension(oFile.Name)
    sFileNameSinEx = s(1)
    sExtension = s(2)
    'sExtension = Right(oFile.Name, Len(oFile.Name) - InStr(1, oFile.Name, "."))
    sFileName = oFile.Name
    sFilePath = oFile.Path
    bGIF = False
    bContinuar = True
    
    If UCase(sExtension) = "GIF" Then
        If FSGSLibrary.ConvertirGIFaJPEG(sFilePath, sExtension) Then
            sFilePath = Left(sFilePath, Len(sFilePath) - Len(sFileName)) & sFileNameSinEx & ".jpeg"
            sFileName = sFileNameSinEx & ".jpeg"
            sExtension = "JPEG"
            bGIF = True
        Else
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleCargarGIF
            Exit Sub
        End If
    End If
    If UCase(sExtension) = "JPG" Or UCase(sExtension) = "JPEG" Or UCase(sExtension) = "BMP" _
    Or UCase(sExtension) = "TIFF" Or UCase(sExtension) = "PNG" Or UCase(sExtension) = "MNG" Then
    
        lblImagen.caption = m_sIdiImagen & ": " & sFileName
        j = j + 1
        lblNumImagenes.caption = Replace(m_sIdiArchivo, "X", CStr(j))
        sArticulo = sFileNameSinEx
        Set oArts = oArticulos.CargarArticuloEnProveedor(g_oProve.Cod, sArticulo)
        If oArts Is Nothing Then
            'Si no existe el art�culo en el prove lo meto a un array
            ReDim Preserve arNoExisten(i + 1)
            arNoExisten(i + 1) = sFileName
            i = i + 1
        Else 'Si exist�a el art�culo
            If g_bImagenes Then
                Set pi = New PajantImage
                pi.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"
                pi.Load sFilePath, 0
                If IsEmpty(pi.ImageData) Then
                    iContError = iContError + 1
                    ReDim Preserve arErrorFormato(iContError)
                    arErrorFormato(iContError) = sFileName
                    oArticulos.Remove oArticulos.Count
                    bContinuar = False
                Else
                    If Not bThumbnails And PajantImagen.Visible = True Then
                        PajantImagen.pi.Copy pi, 0, 0, PJT_IMAGE Or PJT_TONEWFRAME 'PJT_TONEWIMAGE
                        DoEvents
                    End If
                    'Guarda la imagen en imgProve para a�adirla despu�s a la base de datos
                    pi.ExportTo PJT_MEMPNG, 0, 0, imgProve, PJT_FRAME
                    DoEvents
                End If
            End If
            If bThumbnails And bContinuar Then
                Set pi2 = New PajantImage
                pi2.Register "FULLSTEP NETWORKS S.L.", "3C91D6B17E242021664AB9C6"
                If g_bImagenes Then
                    pi2.Copy pi, 0, 0, PJT_IMAGE Or PJT_TONEWFRAME
                Else
                    pi2.Load sFilePath, 0
                End If
                If IsEmpty(pi2.ImageData) Then
                    iContError = iContError + 1
                    ReDim Preserve arErrorFormato(iContError)
                    arErrorFormato(iContError) = sFileName
                    oArticulos.Remove oArticulos.Count
                    bContinuar = False
                Else
                    If frmCATDatExtImagenes3.opInter.Value = True Then
                        pi2.ResizeImage 100, 100, PJT_INTERPOLATE
                    Else
                        pi2.ResizeImage 100, 100, PJT_SIMPLE
                    End If
                    If PajantImagen.Visible = True Then
                        PajantImagen.pi.Copy pi2, 0, 0, PJT_IMAGE Or PJT_TONEWFRAME 'PJT_TONEWIMAGE
                        DoEvents
                    End If
                    
                    DoEvents
                    pi2.ExportTo PJT_MEMJPEG, 0, 0, imgThumb, PJT_FRAME
                    DoEvents
                End If

            End If
            If bContinuar Then
                For Each oArt In oArts
                    sCod = oArt.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oArt.GMN1Cod))
                    sCod = sCod & oArt.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oArt.GMN2Cod))
                    sCod = sCod & oArt.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oArt.GMN3Cod))
                    sCod = sCod & oArt.GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oArt.GMN4Cod))
                    sCod = sCod & oArt.Cod
                    ' La colecci�n de art�culos utiliza la clave de art�culo simple, sin GMNs
                    If Not oArticulos.Item(oArt.Cod) Is Nothing Then
                        If g_bImagenes Then
                            oArticulos.Item(oArt.Cod).imagen = imgProve
                        End If
                        If bThumbnails Then
                            oArticulos.Item(oArt.Cod).Thumbnail = imgThumb
                        End If
                    End If
                Next
            End If
        End If
    End If
    If bGIF Then
        oFos.DeleteFile sFilePath
    End If
Next
Set pi = Nothing
Set pi2 = Nothing
Set oFos = Nothing
Set oFolder = Nothing
Set oFiles = Nothing

teserror = g_oProve.GuardarDatosExternosArticulos(oArticulos)
If teserror.NumError <> TESnoerror Then
    TratarError teserror
    Screen.MousePointer = vbNormal
    Unload Me
    Exit Sub
End If
Screen.MousePointer = vbNormal
If i > 0 Or iContError > 0 Then
    oMensajes.ImposibleCargarImagenes arNoExisten, arErrorFormato
End If
Dim vbook As Variant
Dim vBook1 As Variant
If frmCatalogoDatosExt.sdbgArt.Rows > 0 And Not frmCatalogoDatosExt.m_oArticulos Is Nothing Then
    vBook1 = frmCatalogoDatosExt.sdbgArt.Bookmark
    For Each oArt In oArticulos
        sCod = oArt.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oArt.GMN1Cod))
        sCod = sCod & oArt.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oArt.GMN2Cod))
        sCod = sCod & oArt.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oArt.GMN3Cod))
        sCod = sCod & oArt.GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oArt.GMN4Cod))
        sCod = sCod & oArt.Cod
        
        If Not frmCatalogoDatosExt.m_oArticulos.Item(sCod) Is Nothing Then
            frmCatalogoDatosExt.m_oArticulos.Item(sCod).ConImagen = True
            vbook = frmCatalogoDatosExt.sdbgArt.AddItemBookmark(frmCatalogoDatosExt.m_oArticulos.Item(sCod).indice)
            frmCatalogoDatosExt.sdbgArt.Bookmark = vbook
            frmCatalogoDatosExt.sdbgArt.Columns("CONIMAGEN").Value = "1"
            frmCatalogoDatosExt.sdbgArt.Update
        End If
    Next
    frmCatalogoDatosExt.sdbgArt.Bookmark = vBook1
End If

Set oArticulos = Nothing
Unload Me
Exit Sub

ERROR:
    If err.Number = 53 Or err.Number = 70 Or err.Number = 76 Then
        oMensajes.ImposibleAccederAFichero
    Else
        MsgBox err.Description, vbCritical + vbOKOnly, "FULLSTEP"
    End If
    Screen.MousePointer = vbNormal
    Unload Me
End Sub

Private Sub Form_Load()

Me.Height = 2100
If frmCATDatExtImagenes3.chkNoMostrar.Value = vbChecked Then
    PajantImagen.Visible = False
    Me.Width = 5370
Else
    PajantImagen.Visible = True
    Me.Width = 6915
End If

Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

CargarRecursos

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_Imagenes1, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sIdiArchivo = Ador(0).Value
        Ador.MoveNext
        m_sIdiImagen = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        Ador.Close
            
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Resize()

If PajantImagen.Visible Then

Else

End If

End Sub

