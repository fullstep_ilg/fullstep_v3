VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "*\AUserControls\FSGSUserControls.vbp"
Begin VB.Form frmCONFGEN 
   Caption         =   " "
   ClientHeight    =   10095
   ClientLeft      =   2895
   ClientTop       =   1905
   ClientWidth     =   11610
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONFGEN.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10095
   ScaleWidth      =   11610
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   0
      ScaleHeight     =   405
      ScaleWidth      =   11610
      TabIndex        =   372
      TabStop         =   0   'False
      Top             =   9345
      Width           =   11610
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "D&Aceptar"
         Default         =   -1  'True
         Height          =   345
         Left            =   4065
         TabIndex        =   134
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "DCancelar"
         Height          =   345
         Left            =   5205
         TabIndex        =   135
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   350
      Left            =   0
      ScaleHeight     =   345
      ScaleWidth      =   11610
      TabIndex        =   144
      TabStop         =   0   'False
      Top             =   9750
      Width           =   11610
      Begin VB.CommandButton cmdEdicion 
         Caption         =   "D&Edici�n"
         Height          =   345
         Left            =   9750
         TabIndex        =   0
         Top             =   0
         Width           =   1170
      End
   End
   Begin TabDlg.SSTab tabCONFGEN 
      Height          =   9195
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   10875
      _ExtentX        =   19182
      _ExtentY        =   16219
      _Version        =   393216
      Style           =   1
      Tabs            =   9
      TabsPerRow      =   9
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DPersonalizaci�n"
      TabPicture(0)   =   "frmCONFGEN.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "SSTab2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "DMantenimiento"
      TabPicture(1)   =   "frmCONFGEN.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraManten(0)"
      Tab(1).Control(1)=   "fraManten(1)"
      Tab(1).Control(2)=   "fraManten(2)"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "DGesti�n"
      TabPicture(2)   =   "frmCONFGEN.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "SSTabGestion"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "DOfertas"
      TabPicture(3)   =   "frmCONFGEN.frx":0D06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "SSTabOferta"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "DReuniones"
      TabPicture(4)   =   "frmCONFGEN.frx":0D22
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "sstabReu"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "DPedidos"
      TabPicture(5)   =   "frmCONFGEN.frx":0D3E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "ctlConfPed"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "DSeguridad"
      TabPicture(6)   =   "frmCONFGEN.frx":0D5A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Frame11(0)"
      Tab(6).Control(1)=   "Frame11(1)"
      Tab(6).ControlCount=   2
      TabCaption(7)   =   "DCarpetas"
      TabPicture(7)   =   "frmCONFGEN.frx":0D76
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "picListados"
      Tab(7).ControlCount=   1
      TabCaption(8)   =   "DOpciones"
      TabPicture(8)   =   "frmCONFGEN.frx":0D92
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "tabOpciones"
      Tab(8).ControlCount=   1
      Begin FSGSUserControls.ctlConfGenPedidos ctlConfPed 
         Height          =   8775
         Left            =   -74880
         TabIndex        =   514
         Top             =   360
         Width           =   9375
         _ExtentX        =   16536
         _ExtentY        =   15478
      End
      Begin VB.Frame Frame11 
         Caption         =   "DPol�tica de seguridad de contrase�as"
         Height          =   4905
         Index           =   1
         Left            =   -74880
         TabIndex        =   406
         Top             =   1800
         Width           =   8085
         Begin VB.PictureBox picSeg 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   4665
            Index           =   1
            Left            =   60
            ScaleHeight     =   4665
            ScaleWidth      =   7935
            TabIndex        =   407
            TabStop         =   0   'False
            Top             =   180
            Width           =   7935
            Begin VB.TextBox txtGrupoDefecto 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   1
               EndProperty
               Height          =   315
               Index           =   3
               Left            =   3000
               MaxLength       =   100
               TabIndex        =   128
               Text            =   "0"
               Top             =   240
               Width           =   600
            End
            Begin VB.TextBox txtGrupoDefecto 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   1
               EndProperty
               Height          =   315
               Index           =   4
               Left            =   3000
               MaxLength       =   100
               TabIndex        =   129
               Text            =   "0"
               Top             =   1320
               Width           =   600
            End
            Begin VB.TextBox txtGrupoDefecto 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   1
               EndProperty
               Height          =   315
               Index           =   5
               Left            =   3000
               MaxLength       =   100
               TabIndex        =   130
               Text            =   "0"
               Top             =   2400
               Width           =   600
            End
            Begin VB.TextBox txtGrupoDefecto 
               BeginProperty DataFormat 
                  Type            =   1
                  Format          =   "0"
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   3082
                  SubFormatType   =   1
               EndProperty
               Height          =   315
               Index           =   6
               Left            =   3000
               MaxLength       =   100
               TabIndex        =   133
               Text            =   "0"
               Top             =   4200
               Width           =   600
            End
            Begin VB.OptionButton optRutaTS 
               Caption         =   "DHabilitado"
               Height          =   240
               Index           =   2
               Left            =   5160
               TabIndex        =   131
               Top             =   3120
               Width           =   3570
            End
            Begin VB.OptionButton optRutaTS 
               Caption         =   "DDeshabilitado"
               Height          =   240
               Index           =   3
               Left            =   5160
               TabIndex        =   132
               Top             =   3600
               Value           =   -1  'True
               Width           =   3570
            End
            Begin MSComCtl2.UpDown UpDown1 
               Height          =   315
               Index           =   0
               Left            =   3600
               TabIndex        =   408
               Top             =   240
               Width           =   255
               _ExtentX        =   450
               _ExtentY        =   556
               _Version        =   393216
               BuddyControl    =   "txtGrupoDefecto(3)"
               BuddyDispid     =   196616
               BuddyIndex      =   3
               OrigLeft        =   4200
               OrigTop         =   600
               OrigRight       =   4455
               OrigBottom      =   1005
               Max             =   24
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin MSComCtl2.UpDown UpDown1 
               Height          =   315
               Index           =   1
               Left            =   3600
               TabIndex        =   409
               Top             =   1320
               Width           =   255
               _ExtentX        =   450
               _ExtentY        =   556
               _Version        =   393216
               BuddyControl    =   "txtGrupoDefecto(4)"
               BuddyDispid     =   196616
               BuddyIndex      =   4
               OrigLeft        =   4200
               OrigTop         =   1680
               OrigRight       =   4455
               OrigBottom      =   2085
               Max             =   998
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin MSComCtl2.UpDown UpDown1 
               Height          =   315
               Index           =   2
               Left            =   3600
               TabIndex        =   410
               Top             =   2400
               Width           =   255
               _ExtentX        =   450
               _ExtentY        =   556
               _Version        =   393216
               BuddyControl    =   "txtGrupoDefecto(5)"
               BuddyDispid     =   196616
               BuddyIndex      =   5
               OrigLeft        =   4200
               OrigTop         =   2760
               OrigRight       =   4455
               OrigBottom      =   3165
               Max             =   999
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin MSComCtl2.UpDown UpDown1 
               Height          =   315
               Index           =   3
               Left            =   3600
               TabIndex        =   411
               Top             =   4200
               Width           =   255
               _ExtentX        =   450
               _ExtentY        =   556
               _Version        =   393216
               BuddyControl    =   "txtGrupoDefecto(6)"
               BuddyDispid     =   196616
               BuddyIndex      =   6
               OrigLeft        =   4200
               OrigTop         =   4560
               OrigRight       =   4455
               OrigBottom      =   4965
               Max             =   14
               SyncBuddy       =   -1  'True
               BuddyProperty   =   65547
               Enabled         =   -1  'True
            End
            Begin VB.Label label2 
               Caption         =   "DForzar el historial de contrase�as:"
               Height          =   195
               Index           =   12
               Left            =   120
               TabIndex        =   423
               Top             =   240
               Width           =   3000
            End
            Begin VB.Label label2 
               Caption         =   "DNo mantener el hist�rico de contrase�as"
               Height          =   195
               Index           =   13
               Left            =   3000
               TabIndex        =   422
               Top             =   0
               Width           =   3720
            End
            Begin VB.Label label2 
               Caption         =   "Dcontrase�as recordadas"
               Height          =   195
               Index           =   14
               Left            =   3960
               TabIndex        =   421
               Top             =   360
               Width           =   2640
            End
            Begin VB.Label label2 
               Caption         =   "DVigencia m�nima de la contrase�a:"
               Height          =   195
               Index           =   15
               Left            =   120
               TabIndex        =   420
               Top             =   1320
               Width           =   3000
            End
            Begin VB.Label label2 
               Caption         =   "DVigencia m�xima de la contrase�a:"
               Height          =   195
               Index           =   16
               Left            =   120
               TabIndex        =   419
               Top             =   2400
               Width           =   3000
            End
            Begin VB.Label label2 
               Caption         =   "Dd�as"
               Height          =   195
               Index           =   17
               Left            =   3960
               TabIndex        =   418
               Top             =   1440
               Width           =   1920
            End
            Begin VB.Label label2 
               Caption         =   "Dd�as"
               Height          =   195
               Index           =   18
               Left            =   3960
               TabIndex        =   417
               Top             =   2520
               Width           =   1920
            End
            Begin VB.Label label2 
               Caption         =   "DLa contrase�a puede cambiar despu�s de:"
               Height          =   195
               Index           =   19
               Left            =   3000
               TabIndex        =   416
               Top             =   1080
               Width           =   3720
            End
            Begin VB.Label label2 
               Caption         =   "DLa contrase�a expirar� en:"
               Height          =   195
               Index           =   20
               Left            =   3000
               TabIndex        =   415
               Top             =   2160
               Width           =   3720
            End
            Begin VB.Label label2 
               Caption         =   "DLongitud m�nima de contrase�a:"
               Height          =   195
               Index           =   21
               Left            =   120
               TabIndex        =   414
               Top             =   4200
               Width           =   3000
            End
            Begin VB.Label label2 
               Caption         =   "Dcar�cteres"
               Height          =   195
               Index           =   22
               Left            =   3960
               TabIndex        =   413
               Top             =   4320
               Width           =   3000
            End
            Begin VB.Label label2 
               Caption         =   "DLas contrase�as deben cumplir los requisitos de complejidad:"
               Height          =   195
               Index           =   23
               Left            =   120
               TabIndex        =   412
               Top             =   3360
               Width           =   5385
            End
         End
      End
      Begin TabDlg.SSTab tabOpciones 
         Height          =   6435
         Left            =   -74850
         TabIndex        =   303
         Top             =   480
         Width           =   9285
         _ExtentX        =   16378
         _ExtentY        =   11351
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         TabCaption(0)   =   "Opciones por defecto de correo electr�nico"
         TabPicture(0)   =   "frmCONFGEN.frx":0DAE
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "picOpciones(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Idiomas"
         TabPicture(1)   =   "frmCONFGEN.frx":0DCA
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "picOpciones(1)"
         Tab(1).ControlCount=   1
         Begin VB.PictureBox picOpciones 
            BorderStyle     =   0  'None
            Height          =   4185
            Index           =   1
            Left            =   -74910
            ScaleHeight     =   4185
            ScaleWidth      =   8805
            TabIndex        =   305
            Top             =   360
            Width           =   8805
            Begin VB.Frame fraLenguaje 
               Caption         =   "DZona horaria"
               Height          =   1095
               Index           =   2
               Left            =   180
               TabIndex        =   439
               Top             =   2880
               Width           =   8490
               Begin SSDataWidgets_B.SSDBCombo sdbcIdiomas 
                  Height          =   285
                  Index           =   2
                  Left            =   600
                  TabIndex        =   440
                  Top             =   450
                  Width           =   7275
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   12832
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
            End
            Begin VB.Frame fraLenguaje 
               Caption         =   "DIdioma para los datos del portal"
               Height          =   1095
               Index           =   1
               Left            =   180
               TabIndex        =   320
               Top             =   1500
               Width           =   6420
               Begin SSDataWidgets_B.SSDBCombo sdbcIdiomas 
                  Height          =   285
                  Index           =   1
                  Left            =   600
                  TabIndex        =   318
                  Top             =   450
                  Width           =   4935
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   8705
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
            End
            Begin VB.Frame fraLenguaje 
               Caption         =   "DIdioma de la aplicaci�n"
               Height          =   1095
               Index           =   0
               Left            =   210
               TabIndex        =   319
               Top             =   150
               Width           =   6420
               Begin SSDataWidgets_B.SSDBCombo sdbcIdiomas 
                  Height          =   285
                  Index           =   0
                  Left            =   600
                  TabIndex        =   317
                  Top             =   450
                  Width           =   4935
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   8705
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
            End
         End
         Begin VB.PictureBox picOpciones 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   5865
            Index           =   0
            Left            =   90
            ScaleHeight     =   5865
            ScaleWidth      =   9120
            TabIndex        =   304
            Top             =   420
            Width           =   9120
            Begin VB.Frame fraOpciones 
               Height          =   2370
               Index           =   0
               Left            =   0
               TabIndex        =   345
               Top             =   2680
               Width           =   3340
               Begin VB.OptionButton optFormatoEMail 
                  Caption         =   "TXT"
                  Height          =   195
                  Index           =   1
                  Left            =   1800
                  TabIndex        =   316
                  Top             =   1400
                  Value           =   -1  'True
                  Width           =   1095
               End
               Begin VB.OptionButton optFormatoEMail 
                  Caption         =   "HTML"
                  Height          =   195
                  Index           =   0
                  Left            =   240
                  TabIndex        =   315
                  Top             =   1400
                  Width           =   975
               End
               Begin VB.CheckBox chkCorreo 
                  Caption         =   "DPedir acuse de recibo"
                  Height          =   195
                  Index           =   5
                  Left            =   100
                  TabIndex        =   314
                  Top             =   640
                  Width           =   3100
               End
               Begin VB.CheckBox chkCorreo 
                  Caption         =   "DMostrar los mensajes antes de enviarlos"
                  Height          =   315
                  Index           =   4
                  Left            =   100
                  TabIndex        =   313
                  Top             =   240
                  Width           =   3100
               End
               Begin VB.Label lblFormatoMail 
                  Caption         =   "(Se aplicar� a los emais de:Solicitudes de compra, aprovisionamiento y aviso de disposici�n a no ofertar)"
                  BeginProperty Font 
                     Name            =   "Small Fonts"
                     Size            =   6.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   560
                  Index           =   1
                  Left            =   60
                  TabIndex        =   347
                  Top             =   1700
                  Width           =   3140
               End
               Begin VB.Label lblFormatoMail 
                  Caption         =   "DRecibir los emails en formato:"
                  Height          =   255
                  Index           =   0
                  Left            =   100
                  TabIndex        =   346
                  Top             =   1080
                  Width           =   2295
               End
            End
            Begin VB.CheckBox chkCorreo 
               Caption         =   "SMTP"
               Height          =   225
               Index           =   1
               Left            =   3600
               TabIndex        =   309
               Top             =   60
               Width           =   765
            End
            Begin VB.CheckBox chkCorreo 
               Caption         =   "MAPI"
               Height          =   225
               Index           =   0
               Left            =   150
               TabIndex        =   306
               Top             =   60
               Width           =   765
            End
            Begin VB.Frame fraOpciones 
               Height          =   4995
               Index           =   2
               Left            =   3450
               TabIndex        =   322
               Top             =   60
               Width           =   5610
               Begin VB.Frame fraOpciones 
                  Enabled         =   0   'False
                  Height          =   1230
                  Index           =   3
                  Left            =   150
                  TabIndex        =   444
                  Top             =   1200
                  Width           =   5355
                  Begin VB.OptionButton optAutenticacion 
                     Caption         =   "anonima"
                     Height          =   195
                     Index           =   2
                     Left            =   240
                     TabIndex        =   455
                     Top             =   240
                     Width           =   3060
                  End
                  Begin VB.OptionButton optAutenticacion 
                     Caption         =   "Segura"
                     Height          =   195
                     Index           =   1
                     Left            =   240
                     TabIndex        =   454
                     Top             =   570
                     Width           =   3060
                  End
                  Begin VB.OptionButton optAutenticacion 
                     Caption         =   "B�sica"
                     Height          =   225
                     Index           =   0
                     Left            =   240
                     TabIndex        =   453
                     Top             =   900
                     Width           =   2610
                  End
                  Begin VB.Label lblSMTP 
                     Caption         =   "dControl de  acceso"
                     Height          =   225
                     Index           =   6
                     Left            =   120
                     TabIndex        =   456
                     Top             =   0
                     Width           =   1515
                  End
               End
               Begin VB.TextBox txtSMTP 
                  Height          =   285
                  Index           =   1
                  Left            =   1530
                  TabIndex        =   311
                  Top             =   600
                  Width           =   1065
               End
               Begin VB.CheckBox chkCorreo 
                  Caption         =   "Requiere SSL"
                  Height          =   225
                  Index           =   2
                  Left            =   1530
                  TabIndex        =   312
                  Top             =   960
                  Width           =   3135
               End
               Begin VB.TextBox txtSMTP 
                  Height          =   285
                  Index           =   0
                  Left            =   1530
                  TabIndex        =   310
                  Top             =   270
                  Width           =   3345
               End
               Begin VB.Frame fraOpciones 
                  Enabled         =   0   'False
                  Height          =   1695
                  Index           =   4
                  Left            =   150
                  TabIndex        =   323
                  Top             =   2640
                  Width           =   5355
                  Begin VB.TextBox txtSMTP 
                     Height          =   285
                     IMEMode         =   3  'DISABLE
                     Index           =   5
                     Left            =   1620
                     TabIndex        =   450
                     Top             =   1320
                     Width           =   3585
                  End
                  Begin VB.TextBox txtSMTP 
                     Height          =   285
                     Index           =   2
                     Left            =   1620
                     TabIndex        =   446
                     Top             =   600
                     Width           =   3585
                  End
                  Begin VB.TextBox txtSMTP 
                     Height          =   285
                     IMEMode         =   3  'DISABLE
                     Index           =   3
                     Left            =   1620
                     PasswordChar    =   "*"
                     TabIndex        =   445
                     Top             =   960
                     Width           =   3585
                  End
                  Begin VB.TextBox txtSMTP 
                     Height          =   285
                     IMEMode         =   3  'DISABLE
                     Index           =   4
                     Left            =   1620
                     TabIndex        =   449
                     Top             =   240
                     Width           =   3585
                  End
                  Begin VB.Label lblSMTP 
                     Caption         =   "dServicio plataforma"
                     Height          =   225
                     Index           =   7
                     Left            =   120
                     TabIndex        =   457
                     Top             =   0
                     Width           =   1575
                  End
                  Begin VB.Label lblSMTP 
                     Caption         =   "dDominio:"
                     Height          =   225
                     Index           =   5
                     Left            =   120
                     TabIndex        =   452
                     Top             =   1350
                     Width           =   1125
                  End
                  Begin VB.Label lblSMTP 
                     Caption         =   "Usuario:"
                     Height          =   225
                     Index           =   2
                     Left            =   120
                     TabIndex        =   448
                     Top             =   660
                     Width           =   1065
                  End
                  Begin VB.Label lblSMTP 
                     Caption         =   "Contrase�a:"
                     Height          =   225
                     Index           =   3
                     Left            =   120
                     TabIndex        =   447
                     Top             =   990
                     Width           =   1125
                  End
                  Begin VB.Label lblSMTP 
                     Caption         =   "dCuenta de correo:"
                     Height          =   225
                     Index           =   4
                     Left            =   120
                     TabIndex        =   451
                     Top             =   240
                     Width           =   1425
                  End
               End
               Begin VB.Label lblSMTP 
                  Caption         =   "Servidor:"
                  Height          =   225
                  Index           =   0
                  Left            =   210
                  TabIndex        =   325
                  Top             =   330
                  Width           =   1185
               End
               Begin VB.Label lblSMTP 
                  Caption         =   "Puerto:"
                  Height          =   225
                  Index           =   1
                  Left            =   210
                  TabIndex        =   324
                  Top             =   630
                  Width           =   1185
               End
            End
            Begin VB.Frame fraOpciones 
               Height          =   2600
               Index           =   1
               Left            =   0
               TabIndex        =   321
               Top             =   60
               Width           =   3340
               Begin VB.OptionButton optMapi 
                  Caption         =   "DCliente Microsoft (Outlook, Outlook Express...)"
                  Height          =   585
                  Index           =   0
                  Left            =   180
                  TabIndex        =   307
                  Top             =   660
                  Width           =   3060
               End
               Begin VB.OptionButton optMapi 
                  Caption         =   "DCliente Lotus Notes"
                  Height          =   405
                  Index           =   1
                  Left            =   180
                  TabIndex        =   308
                  Top             =   2010
                  Width           =   2940
               End
            End
         End
      End
      Begin VB.Frame fraManten 
         Caption         =   "DAlta de art�culos"
         Height          =   1065
         Index           =   2
         Left            =   -74820
         TabIndex        =   295
         Top             =   3600
         Width           =   8130
         Begin VB.PictureBox picSugerir 
            BorderStyle     =   0  'None
            Height          =   465
            Left            =   120
            ScaleHeight     =   465
            ScaleWidth      =   7395
            TabIndex        =   296
            Top             =   360
            Width           =   7395
            Begin VB.CheckBox chkSugerir 
               Caption         =   "Sugerir c�digos de art�culo"
               Height          =   345
               Left            =   240
               TabIndex        =   31
               Top             =   30
               Width           =   6615
            End
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "DOpciones"
         Height          =   1185
         Index           =   0
         Left            =   -74880
         TabIndex        =   254
         Top             =   480
         Width           =   8085
         Begin VB.PictureBox picSeg 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   945
            Index           =   0
            Left            =   60
            ScaleHeight     =   945
            ScaleWidth      =   7935
            TabIndex        =   255
            TabStop         =   0   'False
            Top             =   180
            Width           =   7935
            Begin VB.CheckBox chkActivar 
               Caption         =   "DActivar el bloqueo de cuentas de usuario tras"
               Height          =   330
               Index           =   0
               Left            =   60
               TabIndex        =   125
               Top             =   120
               Width           =   4140
            End
            Begin VB.CheckBox chkActivar 
               Caption         =   "DActivar el reporte de actividad del sistema"
               Height          =   255
               Index           =   1
               Left            =   60
               TabIndex        =   127
               Top             =   525
               Width           =   5310
            End
            Begin MSComctlLib.Slider sldLogPreBloq 
               Height          =   450
               Left            =   4170
               TabIndex        =   126
               Top             =   0
               Width           =   1755
               _ExtentX        =   3096
               _ExtentY        =   794
               _Version        =   393216
               LargeChange     =   1
               Min             =   3
               SelStart        =   3
               Value           =   3
            End
            Begin VB.Label lblIntentos 
               Caption         =   "D3 intentos fallidos"
               Height          =   780
               Left            =   5970
               TabIndex        =   256
               Top             =   120
               Width           =   1935
            End
         End
      End
      Begin VB.PictureBox picListados 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6690
         Left            =   -74865
         ScaleHeight     =   6690
         ScaleWidth      =   8565
         TabIndex        =   252
         Top             =   405
         Width           =   8565
         Begin VB.Frame fraCarpetas 
            Caption         =   "Ruta por defecto para gesti�n de archivos desde Terminal Services"
            Height          =   1770
            Index           =   2
            Left            =   0
            TabIndex        =   353
            Top             =   4815
            Width           =   8160
            Begin VB.ComboBox cmbTSUnidad 
               Height          =   315
               Left            =   855
               TabIndex        =   358
               Text            =   "Z:"
               Top             =   1215
               Width           =   735
            End
            Begin VB.TextBox txtRuta 
               Height          =   315
               Index           =   3
               Left            =   2250
               MaxLength       =   255
               TabIndex        =   357
               Top             =   1215
               Width           =   5580
            End
            Begin VB.TextBox txtRuta 
               Height          =   315
               Index           =   2
               Left            =   855
               MaxLength       =   255
               TabIndex        =   356
               Top             =   540
               Width           =   6975
            End
            Begin VB.OptionButton optRutaTS 
               Caption         =   "Conectar unidad:"
               Height          =   240
               Index           =   1
               Left            =   180
               TabIndex        =   355
               Top             =   900
               Value           =   -1  'True
               Width           =   3570
            End
            Begin VB.OptionButton optRutaTS 
               Caption         =   "Ruta local"
               Height          =   285
               Index           =   0
               Left            =   180
               TabIndex        =   354
               Top             =   270
               Width           =   3390
            End
            Begin VB.Label Label6 
               Alignment       =   2  'Center
               Caption         =   "a"
               Height          =   285
               Left            =   1710
               TabIndex        =   359
               Top             =   1260
               Width           =   420
            End
         End
         Begin VB.Frame fraCarpetas 
            Caption         =   "DRuta para los archivos de emails de solicitudes de oferta"
            Height          =   2370
            Index           =   1
            Left            =   0
            TabIndex        =   294
            Top             =   2385
            Width           =   8160
            Begin VB.CommandButton cmdDrive 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   7770
               Picture         =   "frmCONFGEN.frx":0DE6
               Style           =   1  'Graphical
               TabIndex        =   140
               Top             =   360
               Width           =   315
            End
            Begin VB.TextBox txtRuta 
               Height          =   315
               Index           =   1
               Left            =   100
               TabIndex        =   139
               Top             =   350
               Width           =   7605
            End
            Begin VB.DirListBox dirRPT 
               Height          =   1440
               Index           =   1
               Left            =   105
               TabIndex        =   141
               Top             =   735
               Width           =   7995
            End
         End
         Begin VB.Frame fraCarpetas 
            Caption         =   "DRuta para los archivos de dise�o de listados (RPT)"
            Height          =   2370
            Index           =   0
            Left            =   0
            TabIndex        =   253
            Top             =   -45
            Width           =   8160
            Begin VB.CommandButton cmdDrive 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   7770
               Picture         =   "frmCONFGEN.frx":0EA5
               Style           =   1  'Graphical
               TabIndex        =   137
               Top             =   360
               Width           =   315
            End
            Begin VB.DirListBox dirRPT 
               Height          =   1440
               Index           =   0
               Left            =   105
               TabIndex        =   138
               Top             =   750
               Width           =   7995
            End
            Begin VB.TextBox txtRuta 
               Height          =   315
               Index           =   0
               Left            =   100
               TabIndex        =   136
               Top             =   350
               Width           =   7605
            End
         End
      End
      Begin TabDlg.SSTab SSTab2 
         Height          =   5355
         Left            =   90
         TabIndex        =   2
         Top             =   360
         Width           =   9285
         _ExtentX        =   16378
         _ExtentY        =   9446
         _Version        =   393216
         Style           =   1
         Tabs            =   5
         TabsPerRow      =   5
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "DMateriales"
         TabPicture(0)   =   "frmCONFGEN.frx":0F64
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraMAT"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DUnidades organizativas"
         TabPicture(1)   =   "frmCONFGEN.frx":0F80
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraUO"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "DPresupuestos"
         TabPicture(2)   =   "frmCONFGEN.frx":0F9C
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame19"
         Tab(2).Control(1)=   "Picture1"
         Tab(2).ControlCount=   2
         TabCaption(3)   =   "DCampos para proveedores"
         TabPicture(3)   =   "frmCONFGEN.frx":0FB8
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "fraCodERP"
         Tab(3).ControlCount=   1
         TabCaption(4)   =   "DOtros"
         TabPicture(4)   =   "frmCONFGEN.frx":0FD4
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "Frame14"
         Tab(4).ControlCount=   1
         Begin VB.PictureBox Picture1 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   615
            Left            =   -74880
            ScaleHeight     =   615
            ScaleWidth      =   7935
            TabIndex        =   424
            Top             =   4560
            Width           =   7935
            Begin VB.Frame FraPartidasPresupuestarias 
               BorderStyle     =   0  'None
               Height          =   495
               Left            =   120
               TabIndex        =   425
               Top             =   240
               Visible         =   0   'False
               Width           =   7815
               Begin VB.CommandButton cmdControlPresupuestario 
                  Enabled         =   0   'False
                  Height          =   285
                  Index           =   2
                  Left            =   7275
                  Picture         =   "frmCONFGEN.frx":0FF0
                  Style           =   1  'Graphical
                  TabIndex        =   428
                  Top             =   0
                  Width           =   315
               End
               Begin VB.CommandButton cmdControlPresupuestario 
                  Enabled         =   0   'False
                  Height          =   285
                  Index           =   1
                  Left            =   6870
                  Picture         =   "frmCONFGEN.frx":10D7
                  Style           =   1  'Graphical
                  TabIndex        =   427
                  Top             =   0
                  Width           =   315
               End
               Begin VB.CommandButton cmdControlPresupuestario 
                  Height          =   285
                  Index           =   0
                  Left            =   6480
                  Picture         =   "frmCONFGEN.frx":1169
                  Style           =   1  'Graphical
                  TabIndex        =   426
                  Top             =   0
                  Width           =   315
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPartidaPres 
                  Height          =   285
                  Left            =   1920
                  TabIndex        =   429
                  Top             =   0
                  Width           =   4395
                  DataFieldList   =   "Column 0"
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2672
                  Columns(0).Caption=   "COD"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   7064
                  Columns(1).Caption=   "DEN"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   7752
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblPresupuesto 
                  AutoSize        =   -1  'True
                  Caption         =   "Partidas presupuestarias:"
                  Height          =   195
                  Index           =   4
                  Left            =   0
                  TabIndex        =   430
                  Top             =   0
                  Width           =   1845
               End
            End
         End
         Begin VB.Frame fraCodERP 
            Height          =   4350
            Left            =   -74880
            TabIndex        =   342
            Top             =   465
            Width           =   8955
            Begin VB.PictureBox picNomRel 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   975
               Left            =   4560
               ScaleHeight     =   975
               ScaleWidth      =   3945
               TabIndex        =   436
               Top             =   2880
               Width           =   3945
               Begin VB.TextBox txtDenPres 
                  Height          =   285
                  Index           =   3
                  Left            =   120
                  MaxLength       =   50
                  TabIndex        =   438
                  Top             =   480
                  Width           =   3495
               End
               Begin VB.TextBox txtDenPres 
                  Height          =   285
                  Index           =   2
                  Left            =   120
                  MaxLength       =   50
                  TabIndex        =   437
                  Top             =   120
                  Width           =   3495
               End
            End
            Begin SSDataWidgets_B.SSDBGrid sdbgCamposERP 
               Height          =   1530
               Left            =   75
               TabIndex        =   343
               Top             =   825
               Width           =   8775
               ScrollBars      =   1
               _Version        =   196617
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               HeadLines       =   0
               Col.Count       =   2
               stylesets.count =   2
               stylesets(0).Name=   "Normal"
               stylesets(0).HasFont=   -1  'True
               BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(0).Picture=   "frmCONFGEN.frx":11EB
               stylesets(1).Name=   "ProvPortal"
               stylesets(1).BackColor=   10079487
               stylesets(1).HasFont=   -1  'True
               BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets(1).Picture=   "frmCONFGEN.frx":1207
               UseGroups       =   -1  'True
               AllowDelete     =   -1  'True
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowColumnSwapping=   0
               AllowGroupShrinking=   0   'False
               AllowColumnShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeRow   =   3
               SelectByCell    =   -1  'True
               HeadStyleSet    =   "Normal"
               StyleSet        =   "Normal"
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Groups(0).Width =   4842
               Groups(0).Caption=   "Activar"
               Groups(0).HasHeadForeColor=   -1  'True
               Groups(0).HeadBackColor=   12632256
               Groups(0).Columns.Count=   2
               Groups(0).Columns(0).Width=   1482
               Groups(0).Columns(0).Caption=   "Activar"
               Groups(0).Columns(0).Name=   "ACT"
               Groups(0).Columns(0).DataField=   "Column 0"
               Groups(0).Columns(0).DataType=   8
               Groups(0).Columns(0).FieldLen=   256
               Groups(0).Columns(0).Style=   2
               Groups(0).Columns(0).HasBackColor=   -1  'True
               Groups(0).Columns(0).BackColor=   -2147483633
               Groups(0).Columns(1).Width=   3360
               Groups(0).Columns(1).Caption=   "Campo"
               Groups(0).Columns(1).Name=   "CAMPO"
               Groups(0).Columns(1).DataField=   "Column 1"
               Groups(0).Columns(1).DataType=   8
               Groups(0).Columns(1).FieldLen=   256
               Groups(0).Columns(1).Locked=   -1  'True
               Groups(0).Columns(1).HasHeadForeColor=   -1  'True
               Groups(0).Columns(1).HasBackColor=   -1  'True
               Groups(0).Columns(1).HeadBackColor=   12632256
               Groups(0).Columns(1).BackColor=   -2147483633
               _ExtentX        =   15478
               _ExtentY        =   2699
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
               Height          =   285
               Index           =   8
               Left            =   1080
               TabIndex        =   433
               Top             =   3120
               Width           =   1695
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   3
               Columns(0).Width=   6773
               Columns(0).Caption=   "Denominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "ID"
               Columns(1).Name =   "ID"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               Columns(2).Width=   3200
               Columns(2).Visible=   0   'False
               Columns(2).Caption=   "OFFSET"
               Columns(2).Name =   "OFFSET"
               Columns(2).DataField=   "Column 2"
               Columns(2).DataType=   8
               Columns(2).FieldLen=   256
               _ExtentX        =   2990
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblRel 
               Caption         =   "DPlural:"
               Height          =   255
               Index           =   3
               Left            =   3600
               TabIndex        =   435
               Top             =   3360
               Width           =   645
            End
            Begin VB.Label lblRel 
               Caption         =   "DSingular:"
               Height          =   255
               Index           =   2
               Left            =   3600
               TabIndex        =   434
               Top             =   3000
               Width           =   765
            End
            Begin VB.Label lblRel 
               Caption         =   "DIdioma:"
               Height          =   255
               Index           =   1
               Left            =   240
               TabIndex        =   432
               Top             =   3120
               Width           =   645
            End
            Begin VB.Label Label5 
               Caption         =   "DNombres de los campos personalizados"
               Height          =   255
               Left            =   0
               TabIndex        =   431
               Top             =   0
               Width           =   7725
            End
            Begin VB.Label lblRel 
               Caption         =   "DNombre para las relaciones entre proveedores (Distribuidores...)"
               Height          =   255
               Index           =   0
               Left            =   360
               TabIndex        =   344
               Top             =   2520
               Width           =   7725
            End
         End
         Begin VB.Frame Frame14 
            Caption         =   "DDenominaci�n del documento que inicia un proceso (Solicitud...)"
            Height          =   1725
            Left            =   -74640
            TabIndex        =   339
            Top             =   600
            Width           =   7005
            Begin VB.PictureBox picDenSolicitud 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1215
               Left            =   120
               ScaleHeight     =   1215
               ScaleWidth      =   6705
               TabIndex        =   340
               Top             =   420
               Width           =   6705
               Begin VB.TextBox txtDenSolicitud 
                  Height          =   285
                  Left            =   480
                  MaxLength       =   50
                  TabIndex        =   341
                  Top             =   720
                  Width           =   4095
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
                  Height          =   285
                  Index           =   3
                  Left            =   1680
                  TabIndex        =   393
                  Top             =   0
                  Width           =   2895
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   5106
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblIdiPers 
                  Caption         =   "DIdioma"
                  Height          =   195
                  Index           =   3
                  Left            =   495
                  TabIndex        =   394
                  Top             =   60
                  Width           =   1095
               End
               Begin VB.Line Line4 
                  X1              =   480
                  X2              =   6300
                  Y1              =   480
                  Y2              =   480
               End
            End
         End
         Begin VB.Frame fraMAT 
            Caption         =   "DDenominaci�n de los niveles de estructura de material"
            Height          =   3285
            Left            =   120
            TabIndex        =   194
            Top             =   840
            Width           =   7920
            Begin VB.PictureBox picMat 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   2655
               Left            =   60
               ScaleHeight     =   2655
               ScaleWidth      =   7665
               TabIndex        =   195
               TabStop         =   0   'False
               Top             =   480
               Width           =   7665
               Begin VB.TextBox txtAbrMat 
                  Height          =   285
                  Index           =   4
                  Left            =   6810
                  MaxLength       =   3
                  TabIndex        =   10
                  Top             =   2160
                  Width           =   675
               End
               Begin VB.TextBox txtDenMat 
                  Height          =   285
                  Index           =   4
                  Left            =   1290
                  MaxLength       =   50
                  TabIndex        =   9
                  Top             =   2160
                  Width           =   3705
               End
               Begin VB.TextBox txtAbrMat 
                  Height          =   285
                  Index           =   3
                  Left            =   6810
                  MaxLength       =   3
                  TabIndex        =   8
                  Top             =   1740
                  Width           =   675
               End
               Begin VB.TextBox txtDenMat 
                  Height          =   285
                  Index           =   3
                  Left            =   1290
                  MaxLength       =   50
                  TabIndex        =   7
                  Top             =   1740
                  Width           =   3705
               End
               Begin VB.TextBox txtAbrMat 
                  Height          =   285
                  Index           =   2
                  Left            =   6810
                  MaxLength       =   3
                  TabIndex        =   6
                  Top             =   1320
                  Width           =   675
               End
               Begin VB.TextBox txtDenMat 
                  Height          =   285
                  Index           =   2
                  Left            =   1290
                  MaxLength       =   50
                  TabIndex        =   5
                  Top             =   1320
                  Width           =   3705
               End
               Begin VB.TextBox txtAbrMat 
                  Height          =   285
                  Index           =   1
                  Left            =   6810
                  MaxLength       =   3
                  TabIndex        =   4
                  Top             =   900
                  Width           =   675
               End
               Begin VB.TextBox txtDenMat 
                  Height          =   285
                  Index           =   1
                  Left            =   1275
                  MaxLength       =   50
                  TabIndex        =   3
                  Top             =   900
                  Width           =   3705
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
                  Height          =   285
                  Index           =   0
                  Left            =   1800
                  TabIndex        =   384
                  Top             =   0
                  Width           =   2895
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   5106
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblIdiPers 
                  Caption         =   "DIdioma"
                  Height          =   195
                  Index           =   0
                  Left            =   600
                  TabIndex        =   385
                  Top             =   60
                  Width           =   1155
               End
               Begin VB.Line Line1 
                  X1              =   600
                  X2              =   6420
                  Y1              =   480
                  Y2              =   480
               End
               Begin VB.Label lblAbrMat 
                  Caption         =   "DAbreviatura:"
                  Height          =   195
                  Index           =   4
                  Left            =   5625
                  TabIndex        =   203
                  Top             =   2220
                  Width           =   1215
               End
               Begin VB.Label lblMat 
                  Caption         =   "DNivel 4:"
                  Height          =   195
                  Index           =   4
                  Left            =   60
                  TabIndex        =   202
                  Top             =   2220
                  Width           =   1125
               End
               Begin VB.Label lblAbrMat 
                  Caption         =   "DAbreviatura:"
                  Height          =   195
                  Index           =   3
                  Left            =   5625
                  TabIndex        =   201
                  Top             =   1800
                  Width           =   1215
               End
               Begin VB.Label lblMat 
                  Caption         =   "DNivel 3:"
                  Height          =   195
                  Index           =   3
                  Left            =   60
                  TabIndex        =   200
                  Top             =   1800
                  Width           =   1125
               End
               Begin VB.Label lblAbrMat 
                  Caption         =   "DAbreviatura:"
                  Height          =   195
                  Index           =   2
                  Left            =   5625
                  TabIndex        =   199
                  Top             =   1380
                  Width           =   1215
               End
               Begin VB.Label lblMat 
                  Caption         =   "DNivel 2:"
                  Height          =   195
                  Index           =   2
                  Left            =   60
                  TabIndex        =   198
                  Top             =   1365
                  Width           =   1125
               End
               Begin VB.Label lblAbrMat 
                  Caption         =   "DAbreviatura:"
                  Height          =   195
                  Index           =   1
                  Left            =   5625
                  TabIndex        =   197
                  Top             =   960
                  Width           =   1215
               End
               Begin VB.Label lblMat 
                  Caption         =   "DNivel 1:"
                  Height          =   195
                  Index           =   1
                  Left            =   60
                  TabIndex        =   196
                  Top             =   960
                  Width           =   1125
               End
            End
         End
         Begin VB.Frame fraUO 
            Caption         =   "DAbreviatura:"
            Height          =   3195
            Left            =   -74880
            TabIndex        =   188
            Top             =   840
            Width           =   7920
            Begin VB.PictureBox picUO 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   2535
               Left            =   45
               ScaleHeight     =   2535
               ScaleWidth      =   7755
               TabIndex        =   189
               TabStop         =   0   'False
               Top             =   480
               Width           =   7755
               Begin VB.TextBox txtDenUO 
                  Height          =   285
                  Index           =   1
                  Left            =   1530
                  MaxLength       =   50
                  TabIndex        =   12
                  Top             =   1320
                  Width           =   3495
               End
               Begin VB.TextBox txtAbrUO 
                  Height          =   285
                  Index           =   1
                  Left            =   6840
                  MaxLength       =   3
                  TabIndex        =   13
                  Top             =   1320
                  Width           =   675
               End
               Begin VB.TextBox txtDenUO 
                  Height          =   285
                  Index           =   2
                  Left            =   1530
                  MaxLength       =   50
                  TabIndex        =   14
                  Top             =   1740
                  Width           =   3495
               End
               Begin VB.TextBox txtAbrUO 
                  Height          =   285
                  Index           =   2
                  Left            =   6840
                  MaxLength       =   3
                  TabIndex        =   15
                  Top             =   1740
                  Width           =   675
               End
               Begin VB.TextBox txtDenUO 
                  Height          =   285
                  Index           =   3
                  Left            =   1530
                  MaxLength       =   50
                  TabIndex        =   16
                  Top             =   2160
                  Width           =   3495
               End
               Begin VB.TextBox txtAbrUO 
                  Height          =   285
                  Index           =   3
                  Left            =   6840
                  MaxLength       =   3
                  TabIndex        =   17
                  Top             =   2160
                  Width           =   675
               End
               Begin VB.TextBox txtDenUO 
                  Height          =   285
                  Index           =   0
                  Left            =   1530
                  MaxLength       =   50
                  TabIndex        =   11
                  Top             =   900
                  Width           =   3495
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
                  Height          =   285
                  Index           =   1
                  Left            =   1830
                  TabIndex        =   386
                  Top             =   0
                  Width           =   2895
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   5106
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Line Line2 
                  X1              =   600
                  X2              =   6420
                  Y1              =   480
                  Y2              =   480
               End
               Begin VB.Label lblIdiPers 
                  Caption         =   "DIdioma"
                  Height          =   195
                  Index           =   1
                  Left            =   600
                  TabIndex        =   387
                  Top             =   60
                  Width           =   1155
               End
               Begin VB.Label lblUOAbr 
                  Caption         =   "DAbreviatura:"
                  Height          =   195
                  Index           =   1
                  Left            =   5490
                  TabIndex        =   232
                  Top             =   1380
                  Width           =   1335
               End
               Begin VB.Label lblUOAbr 
                  Height          =   195
                  Index           =   0
                  Left            =   4320
                  TabIndex        =   231
                  Top             =   150
                  Width           =   975
               End
               Begin VB.Label lblUO 
                  Caption         =   "DNivel 1:"
                  Height          =   195
                  Index           =   1
                  Left            =   60
                  TabIndex        =   230
                  Top             =   1380
                  Width           =   1410
               End
               Begin VB.Label lblUO 
                  Caption         =   "DRaiz:"
                  Height          =   195
                  Index           =   0
                  Left            =   60
                  TabIndex        =   229
                  Top             =   960
                  Width           =   1410
               End
               Begin VB.Label lblUO 
                  Caption         =   "DNivel 2:"
                  Height          =   195
                  Index           =   2
                  Left            =   60
                  TabIndex        =   193
                  Top             =   1800
                  Width           =   1410
               End
               Begin VB.Label lblUOAbr 
                  Caption         =   "DAbreviatura:"
                  Height          =   195
                  Index           =   2
                  Left            =   5490
                  TabIndex        =   192
                  Top             =   1800
                  Width           =   1335
               End
               Begin VB.Label lblUO 
                  Caption         =   "DNivel 3:"
                  Height          =   195
                  Index           =   3
                  Left            =   60
                  TabIndex        =   191
                  Top             =   2220
                  Width           =   1410
               End
               Begin VB.Label lblUOAbr 
                  Caption         =   "DAbreviatura:"
                  Height          =   195
                  Index           =   3
                  Left            =   5490
                  TabIndex        =   190
                  Top             =   2220
                  Width           =   1335
               End
            End
         End
         Begin VB.Frame Frame19 
            Caption         =   "DPresupuestos por ... "
            Height          =   3795
            Left            =   -74850
            TabIndex        =   204
            Top             =   780
            Width           =   7155
            Begin VB.PictureBox picPres3 
               BorderStyle     =   0  'None
               Height          =   1215
               Left            =   60
               ScaleHeight     =   1215
               ScaleWidth      =   6735
               TabIndex        =   388
               Top             =   480
               Width           =   6735
               Begin SSDataWidgets_B.SSDBCombo sdbcConPres 
                  Height          =   315
                  Left            =   1800
                  TabIndex        =   389
                  Top             =   720
                  Width           =   2865
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "DEN"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "USAR"
                  Columns(2).Name =   "USAR"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   11
                  Columns(2).FieldLen=   256
                  _ExtentX        =   5054
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
                  Height          =   315
                  Index           =   2
                  Left            =   1800
                  TabIndex        =   390
                  Top             =   0
                  Width           =   2865
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   5054
                  _ExtentY        =   556
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Line Line3 
                  X1              =   600
                  X2              =   6420
                  Y1              =   480
                  Y2              =   480
               End
               Begin VB.Label lblIdiPers 
                  Caption         =   "DIdioma"
                  Height          =   195
                  Index           =   2
                  Left            =   600
                  TabIndex        =   392
                  Top             =   60
                  Width           =   1095
               End
               Begin VB.Label lblPresupuesto 
                  Caption         =   "DConcepto"
                  Height          =   315
                  Index           =   0
                  Left            =   600
                  TabIndex        =   391
                  Top             =   720
                  Width           =   1140
               End
            End
            Begin VB.PictureBox PicConPres 
               BorderStyle     =   0  'None
               Height          =   555
               Left            =   825
               ScaleHeight     =   555
               ScaleWidth      =   2415
               TabIndex        =   250
               Top             =   1620
               Width           =   2415
               Begin VB.CheckBox chkConPres 
                  Caption         =   "usar presupuesto"
                  Height          =   315
                  Left            =   75
                  TabIndex        =   18
                  Top             =   120
                  Width           =   2265
               End
            End
            Begin VB.PictureBox picPres1 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   855
               Left            =   300
               ScaleHeight     =   855
               ScaleWidth      =   6030
               TabIndex        =   205
               Top             =   2280
               Width           =   6030
               Begin VB.TextBox txtDenPres 
                  Height          =   285
                  Index           =   1
                  Left            =   975
                  MaxLength       =   50
                  TabIndex        =   20
                  Top             =   480
                  Width           =   3495
               End
               Begin VB.TextBox txtDenPres 
                  Height          =   285
                  Index           =   0
                  Left            =   975
                  MaxLength       =   50
                  TabIndex        =   19
                  Top             =   60
                  Width           =   3495
               End
               Begin VB.Label Label37 
                  Caption         =   "DPlural:"
                  Height          =   255
                  Left            =   60
                  TabIndex        =   207
                  Top             =   540
                  Width           =   930
               End
               Begin VB.Label lblPresupuesto 
                  Caption         =   "DSingular:"
                  Height          =   255
                  Index           =   2
                  Left            =   60
                  TabIndex        =   206
                  Top             =   120
                  Width           =   930
               End
            End
            Begin VB.PictureBox picPres2 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   795
               Left            =   180
               ScaleHeight     =   795
               ScaleWidth      =   6915
               TabIndex        =   233
               Top             =   2760
               Width           =   6915
               Begin VB.Label Label7 
                  Caption         =   "Label7"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   15
                  Left            =   120
                  TabIndex        =   234
                  Top             =   0
                  Width           =   1035
               End
            End
            Begin VB.Label lblPresupuesto 
               Caption         =   "D(concepto)"
               Height          =   255
               Index           =   3
               Left            =   3300
               TabIndex        =   251
               Top             =   1800
               Width           =   2175
            End
         End
      End
      Begin TabDlg.SSTab SSTabOferta 
         Height          =   5760
         Left            =   -74880
         TabIndex        =   176
         Top             =   450
         Width           =   8160
         _ExtentX        =   14393
         _ExtentY        =   10160
         _Version        =   393216
         Style           =   1
         Tabs            =   6
         TabsPerRow      =   6
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "DPetici�n"
         TabPicture(0)   =   "frmCONFGEN.frx":1223
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTabHabSub"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DComunicaci�n de objetivos"
         TabPicture(1)   =   "frmCONFGEN.frx":123F
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "Frame18"
         Tab(1).Control(1)=   "Frame17"
         Tab(1).Control(2)=   "Frame16"
         Tab(1).ControlCount=   3
         TabCaption(2)   =   "DComparativa"
         TabPicture(2)   =   "frmCONFGEN.frx":125B
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame28(2)"
         Tab(2).Control(1)=   "Frame28(1)"
         Tab(2).Control(2)=   "Frame28(0)"
         Tab(2).ControlCount=   3
         TabCaption(3)   =   "DAdjudicaci�n"
         TabPicture(3)   =   "frmCONFGEN.frx":1277
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "picAdj4"
         Tab(3).Control(1)=   "frameAdj(3)"
         Tab(3).Control(2)=   "frameAdj(1)"
         Tab(3).Control(3)=   "frameAdj(2)"
         Tab(3).ControlCount=   4
         TabCaption(4)   =   "DNo adjudicaci�n"
         TabPicture(4)   =   "frmCONFGEN.frx":1293
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "Frame22"
         Tab(4).Control(1)=   "Frame23"
         Tab(4).ControlCount=   2
         TabCaption(5)   =   "DOpciones"
         TabPicture(5)   =   "frmCONFGEN.frx":12AF
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "fraManten(3)"
         Tab(5).Control(1)=   "picBuzPeriodo"
         Tab(5).Control(2)=   "picLeerOfer"
         Tab(5).Control(3)=   "picMaxAdjun"
         Tab(5).ControlCount=   4
         Begin VB.Frame Frame28 
            Caption         =   "DHoja comparativa de calidad"
            Height          =   1125
            Index           =   2
            Left            =   -74820
            TabIndex        =   380
            Top             =   2940
            Width           =   7890
            Begin VB.PictureBox picComp 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   855
               Index           =   2
               Left            =   120
               ScaleHeight     =   855
               ScaleWidth      =   7740
               TabIndex        =   381
               TabStop         =   0   'False
               Top             =   240
               Width           =   7740
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   42
                  Left            =   1425
                  MaxLength       =   255
                  TabIndex        =   81
                  Top             =   60
                  Width           =   5880
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   43
                  Left            =   7320
                  Picture         =   "frmCONFGEN.frx":12CB
                  Style           =   1  'Graphical
                  TabIndex        =   82
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtLongCabComp 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   2
                  Left            =   3150
                  TabIndex        =   83
                  Top             =   420
                  Width           =   615
               End
               Begin VB.Label lblHojaComp 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Index           =   4
                  Left            =   75
                  TabIndex        =   383
                  Top             =   120
                  Width           =   1215
               End
               Begin VB.Label lblHojaComp 
                  Caption         =   "DComienzo de comparativa en la fila:"
                  Height          =   225
                  Index           =   5
                  Left            =   75
                  TabIndex        =   382
                  Top             =   450
                  Width           =   2985
               End
            End
         End
         Begin VB.Frame fraManten 
            Caption         =   "DAviso de recepci�n de ofertas del portal"
            Height          =   990
            Index           =   3
            Left            =   -74835
            TabIndex        =   376
            Top             =   4200
            Width           =   7890
            Begin VB.PictureBox picConf 
               BorderStyle     =   0  'None
               Height          =   465
               Index           =   3
               Left            =   240
               ScaleHeight     =   465
               ScaleWidth      =   7395
               TabIndex        =   377
               Top             =   360
               Width           =   7395
               Begin VB.CheckBox chkCorreo 
                  Caption         =   "DS� soy el comprador asignado"
                  Height          =   225
                  Index           =   7
                  Left            =   3480
                  TabIndex        =   379
                  Top             =   0
                  Width           =   2715
               End
               Begin VB.CheckBox chkCorreo 
                  Caption         =   "DS� soy el comprador responsable"
                  Height          =   225
                  Index           =   6
                  Left            =   240
                  TabIndex        =   378
                  Top             =   0
                  Width           =   2715
               End
            End
         End
         Begin VB.PictureBox picAdj4 
            BorderStyle     =   0  'None
            Height          =   1520
            Left            =   -74820
            ScaleHeight     =   1515
            ScaleWidth      =   7920
            TabIndex        =   297
            Top             =   480
            Width           =   7915
            Begin VB.CheckBox chkPlantAdj 
               Caption         =   "Plantilla para fichero adjunto de notificaci�n de adjudicaci�n"
               Height          =   240
               Index           =   1
               Left            =   90
               TabIndex        =   352
               Top             =   765
               Width           =   4700
            End
            Begin VB.CheckBox chkPlantAdj 
               Caption         =   "Plantilla para fichero adjunto de orden de compra"
               Height          =   240
               Index           =   0
               Left            =   90
               TabIndex        =   351
               Top             =   0
               Width           =   4700
            End
            Begin VB.Frame frameAdj 
               Caption         =   " "
               Height          =   735
               Index           =   0
               Left            =   0
               TabIndex        =   299
               Top             =   0
               Width           =   7905
               Begin VB.PictureBox PicAdj1 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  Enabled         =   0   'False
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   400
                  Left            =   120
                  ScaleHeight     =   405
                  ScaleWidth      =   7710
                  TabIndex        =   300
                  Top             =   240
                  Width           =   7710
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   8
                     Left            =   7395
                     Picture         =   "frmCONFGEN.frx":138A
                     Style           =   1  'Graphical
                     TabIndex        =   85
                     Top             =   60
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   8
                     Left            =   1545
                     MaxLength       =   255
                     TabIndex        =   84
                     Top             =   60
                     Width           =   5805
                  End
                  Begin VB.Label lblAdjudi 
                     Caption         =   "DPlantilla:"
                     Height          =   195
                     Index           =   0
                     Left            =   0
                     TabIndex        =   301
                     Top             =   120
                     Width           =   1185
                  End
               End
            End
            Begin VB.Frame frameAdj 
               Caption         =   " "
               Height          =   735
               Index           =   4
               Left            =   0
               TabIndex        =   298
               Top             =   780
               Width           =   7905
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   39
                  Left            =   7515
                  Picture         =   "frmCONFGEN.frx":1449
                  Style           =   1  'Graphical
                  TabIndex        =   87
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   38
                  Left            =   1665
                  TabIndex        =   86
                  Top             =   300
                  Width           =   5805
               End
               Begin VB.Label lblAdjudi 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Index           =   5
                  Left            =   120
                  TabIndex        =   302
                  Top             =   360
                  Width           =   975
               End
            End
         End
         Begin VB.Frame Frame28 
            Caption         =   "DHoja comparativa de item"
            Height          =   1125
            Index           =   1
            Left            =   -74820
            TabIndex        =   290
            Top             =   1680
            Width           =   7890
            Begin VB.PictureBox picComp 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   855
               Index           =   1
               Left            =   120
               ScaleHeight     =   855
               ScaleWidth      =   7740
               TabIndex        =   291
               TabStop         =   0   'False
               Top             =   240
               Width           =   7740
               Begin VB.TextBox txtLongCabComp 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   1
                  Left            =   3150
                  TabIndex        =   80
                  Top             =   420
                  Width           =   615
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   38
                  Left            =   7320
                  Picture         =   "frmCONFGEN.frx":1508
                  Style           =   1  'Graphical
                  TabIndex        =   79
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   37
                  Left            =   1425
                  MaxLength       =   255
                  TabIndex        =   78
                  Top             =   60
                  Width           =   5880
               End
               Begin VB.Label lblHojaComp 
                  Caption         =   "DComienzo de comparativa en la fila:"
                  Height          =   225
                  Index           =   3
                  Left            =   75
                  TabIndex        =   293
                  Top             =   450
                  Width           =   2985
               End
               Begin VB.Label lblHojaComp 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Index           =   2
                  Left            =   75
                  TabIndex        =   292
                  Top             =   120
                  Width           =   1215
               End
            End
         End
         Begin TabDlg.SSTab SSTabHabSub 
            Height          =   5325
            Left            =   45
            TabIndex        =   262
            Top             =   360
            Width           =   8025
            _ExtentX        =   14155
            _ExtentY        =   9393
            _Version        =   393216
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "DPetici�n habitual"
            TabPicture(0)   =   "frmCONFGEN.frx":15C7
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "Frame2"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "Frame1"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "Frame10"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "Frame9"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).ControlCount=   4
            TabCaption(1)   =   "DModo Subasta"
            TabPicture(1)   =   "frmCONFGEN.frx":15E3
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "fraTiempoAnte(1)"
            Tab(1).Control(1)=   "fraModSub(2)"
            Tab(1).Control(2)=   "fraModSub(1)"
            Tab(1).Control(3)=   "fraModSub(0)"
            Tab(1).ControlCount=   4
            Begin VB.Frame fraTiempoAnte 
               Caption         =   "DEnvio de notificaciones de subasta"
               Height          =   1455
               Index           =   1
               Left            =   -74910
               TabIndex        =   286
               Top             =   3690
               Width           =   7890
               Begin VB.PictureBox picModSub 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  ForeColor       =   &H80000008&
                  Height          =   1215
                  Index           =   3
                  Left            =   180
                  ScaleHeight     =   1215
                  ScaleWidth      =   7590
                  TabIndex        =   287
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7590
                  Begin VB.PictureBox picModSub 
                     BorderStyle     =   0  'None
                     Height          =   420
                     Index           =   4
                     Left            =   3690
                     ScaleHeight     =   420
                     ScaleWidth      =   3885
                     TabIndex        =   288
                     Top             =   90
                     Width           =   3885
                     Begin VB.ComboBox cboTAntelacion 
                        Height          =   315
                        ItemData        =   "frmCONFGEN.frx":15FF
                        Left            =   2340
                        List            =   "frmCONFGEN.frx":1687
                        TabIndex        =   63
                        Top             =   0
                        Width           =   1455
                     End
                     Begin VB.Label lblModSub 
                        Caption         =   "DTiempo de antelaci�n"
                        Height          =   255
                        Index           =   5
                        Left            =   225
                        TabIndex        =   289
                        Top             =   45
                        Width           =   2040
                     End
                  End
                  Begin VB.CheckBox chkInicio 
                     Caption         =   "DInicio de subasta"
                     Height          =   195
                     Left            =   180
                     TabIndex        =   64
                     Top             =   495
                     Width           =   3500
                  End
                  Begin VB.CheckBox chkRecordatorio 
                     Caption         =   "DRecordatorio de inicio de subasta"
                     Height          =   195
                     Left            =   180
                     TabIndex        =   62
                     Top             =   135
                     Width           =   3500
                  End
                  Begin VB.CheckBox chkCierre 
                     Caption         =   "DCierre de subasta"
                     Height          =   195
                     Left            =   180
                     TabIndex        =   65
                     Top             =   855
                     Width           =   3500
                  End
               End
            End
            Begin VB.Frame fraModSub 
               Caption         =   "DE-mail de petici�n de oferta"
               Height          =   735
               Index           =   2
               Left            =   -74910
               TabIndex        =   283
               Top             =   2835
               Width           =   7890
               Begin VB.PictureBox picModSub 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  ForeColor       =   &H80000008&
                  Height          =   495
                  Index           =   2
                  Left            =   120
                  ScaleHeight     =   495
                  ScaleWidth      =   7665
                  TabIndex        =   284
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7665
                  Begin VB.TextBox txtMailSubject 
                     Height          =   285
                     Index           =   8
                     Left            =   1530
                     MaxLength       =   255
                     TabIndex        =   61
                     Top             =   60
                     Width           =   6135
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DAsunto:"
                     Height          =   255
                     Index           =   4
                     Left            =   0
                     TabIndex        =   285
                     Top             =   120
                     Width           =   1125
                  End
               End
            End
            Begin VB.Frame fraModSub 
               Caption         =   "DPlantillas para peticiones de oferta"
               Height          =   1575
               Index           =   1
               Left            =   -74910
               TabIndex        =   278
               Top             =   1215
               Width           =   7890
               Begin VB.PictureBox picModSub 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  ForeColor       =   &H80000008&
                  Height          =   1275
                  Index           =   1
                  Left            =   60
                  ScaleHeight     =   1275
                  ScaleWidth      =   7755
                  TabIndex        =   279
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7755
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   33
                     Left            =   2145
                     MaxLength       =   255
                     TabIndex        =   59
                     Top             =   960
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     Height          =   285
                     Index           =   34
                     Left            =   7410
                     Picture         =   "frmCONFGEN.frx":1840
                     Style           =   1  'Graphical
                     TabIndex        =   60
                     Top             =   960
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   34
                     Left            =   2145
                     MaxLength       =   255
                     TabIndex        =   57
                     Top             =   540
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     Height          =   285
                     Index           =   35
                     Left            =   7410
                     Picture         =   "frmCONFGEN.frx":18FF
                     Style           =   1  'Graphical
                     TabIndex        =   58
                     Top             =   540
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   35
                     Left            =   2145
                     MaxLength       =   255
                     TabIndex        =   55
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     Height          =   285
                     Index           =   36
                     Left            =   7410
                     Picture         =   "frmCONFGEN.frx":19BE
                     Style           =   1  'Graphical
                     TabIndex        =   56
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla web:"
                     Height          =   315
                     Index           =   3
                     Left            =   60
                     TabIndex        =   282
                     Top             =   975
                     Width           =   2235
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla e-mail: "
                     Height          =   315
                     Index           =   2
                     Left            =   60
                     TabIndex        =   281
                     Top             =   555
                     Width           =   2235
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla carta:"
                     Height          =   255
                     Index           =   1
                     Left            =   60
                     TabIndex        =   280
                     Top             =   135
                     Width           =   2235
                  End
               End
            End
            Begin VB.Frame fraModSub 
               Caption         =   "DFormulario de oferta"
               Height          =   795
               Index           =   0
               Left            =   -74910
               TabIndex        =   275
               Top             =   390
               Width           =   7890
               Begin VB.PictureBox picModSub 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  ForeColor       =   &H80000008&
                  Height          =   555
                  Index           =   0
                  Left            =   135
                  ScaleHeight     =   555
                  ScaleWidth      =   7680
                  TabIndex        =   276
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7680
                  Begin VB.CommandButton cmdPlantilla 
                     Height          =   285
                     Index           =   37
                     Left            =   7350
                     Picture         =   "frmCONFGEN.frx":1A7D
                     Style           =   1  'Graphical
                     TabIndex        =   54
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   36
                     Left            =   2070
                     MaxLength       =   255
                     TabIndex        =   53
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.Label lblModSub 
                     Caption         =   "DPlantilla:"
                     Height          =   240
                     Index           =   0
                     Left            =   0
                     TabIndex        =   277
                     Top             =   135
                     Width           =   735
                  End
               End
            End
            Begin VB.Frame Frame9 
               Caption         =   "DFormulario de oferta"
               Height          =   795
               Left            =   90
               TabIndex        =   272
               Top             =   420
               Width           =   7890
               Begin VB.PictureBox picPetOfe 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   555
                  Index           =   1
                  Left            =   120
                  ScaleHeight     =   555
                  ScaleWidth      =   7680
                  TabIndex        =   273
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7680
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   0
                     Left            =   7350
                     Picture         =   "frmCONFGEN.frx":1B3C
                     Style           =   1  'Graphical
                     TabIndex        =   44
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   0
                     Left            =   2100
                     MaxLength       =   255
                     TabIndex        =   43
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.Label lblOfePet 
                     Caption         =   "DPlantilla:"
                     Height          =   240
                     Index           =   0
                     Left            =   0
                     TabIndex        =   274
                     Top             =   135
                     Width           =   1815
                  End
               End
            End
            Begin VB.Frame Frame10 
               Caption         =   "DPlantillas para peticiones de oferta"
               Height          =   1575
               Left            =   90
               TabIndex        =   267
               Top             =   1290
               Width           =   7890
               Begin VB.PictureBox picPetOfe 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   1275
                  Index           =   2
                  Left            =   60
                  ScaleHeight     =   1275
                  ScaleWidth      =   7755
                  TabIndex        =   268
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7755
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   3
                     Left            =   2145
                     MaxLength       =   255
                     TabIndex        =   49
                     Top             =   960
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   3
                     Left            =   7410
                     Picture         =   "frmCONFGEN.frx":1BFB
                     Style           =   1  'Graphical
                     TabIndex        =   50
                     Top             =   960
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   2
                     Left            =   2145
                     MaxLength       =   255
                     TabIndex        =   47
                     Top             =   540
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   2
                     Left            =   7410
                     Picture         =   "frmCONFGEN.frx":1CBA
                     Style           =   1  'Graphical
                     TabIndex        =   48
                     Top             =   540
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   1
                     Left            =   2145
                     MaxLength       =   255
                     TabIndex        =   45
                     Top             =   120
                     Width           =   5235
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   1
                     Left            =   7410
                     Picture         =   "frmCONFGEN.frx":1D79
                     Style           =   1  'Graphical
                     TabIndex        =   46
                     Top             =   120
                     Width           =   315
                  End
                  Begin VB.Label lblOfePet 
                     Caption         =   "DPlantilla web:"
                     Height          =   315
                     Index           =   3
                     Left            =   60
                     TabIndex        =   271
                     Top             =   975
                     Width           =   2235
                  End
                  Begin VB.Label lblOfePet 
                     Caption         =   "DPlantilla e-mail: "
                     Height          =   315
                     Index           =   2
                     Left            =   60
                     TabIndex        =   270
                     Top             =   555
                     Width           =   2235
                  End
                  Begin VB.Label lblOfePet 
                     Caption         =   "DPlantilla carta:"
                     Height          =   255
                     Index           =   1
                     Left            =   60
                     TabIndex        =   269
                     Top             =   135
                     Width           =   2235
                  End
               End
            End
            Begin VB.Frame Frame1 
               Caption         =   "DE-mail de petici�n de oferta"
               Height          =   795
               Left            =   90
               TabIndex        =   265
               Top             =   2970
               Width           =   7890
               Begin VB.PictureBox picPetOfe 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   495
                  Index           =   3
                  Left            =   120
                  ScaleHeight     =   495
                  ScaleWidth      =   7665
                  TabIndex        =   95
                  TabStop         =   0   'False
                  Top             =   240
                  Width           =   7665
                  Begin VB.TextBox txtMailSubject 
                     Height          =   285
                     Index           =   0
                     Left            =   1560
                     MaxLength       =   255
                     TabIndex        =   51
                     Top             =   60
                     Width           =   6135
                  End
                  Begin VB.Label lblOfePet 
                     Caption         =   "DAsunto:"
                     Height          =   255
                     Index           =   4
                     Left            =   0
                     TabIndex        =   266
                     Top             =   120
                     Width           =   1125
                  End
               End
            End
            Begin VB.Frame Frame2 
               Caption         =   "DDirecci�n (URL) del sitio web de presentaci�n de ofertas"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   735
               Left            =   90
               TabIndex        =   263
               Top             =   3840
               Width           =   7890
               Begin VB.PictureBox picURL 
                  Appearance      =   0  'Flat
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   495
                  Left            =   60
                  ScaleHeight     =   495
                  ScaleWidth      =   7725
                  TabIndex        =   264
                  TabStop         =   0   'False
                  Top             =   180
                  Width           =   7725
                  Begin VB.TextBox txtURL 
                     Height          =   285
                     Left            =   210
                     MaxLength       =   255
                     TabIndex        =   52
                     Top             =   120
                     Width           =   7515
                  End
               End
            End
         End
         Begin VB.PictureBox picBuzPeriodo 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1440
            Left            =   -74835
            ScaleHeight     =   1440
            ScaleWidth      =   7965
            TabIndex        =   239
            Top             =   420
            Width           =   7965
            Begin VB.Frame fraBuzPeriodo 
               Caption         =   "DPeriodo de fechas a mostrar por defecto en buz�n de ofertas"
               Height          =   1410
               Left            =   0
               TabIndex        =   240
               Top             =   0
               Width           =   7890
               Begin SSDataWidgets_B.SSDBCombo sdbcAntigOfer 
                  Height          =   285
                  Left            =   3735
                  TabIndex        =   103
                  Top             =   540
                  Width           =   2565
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  MaxDropDownItems=   9
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   -2147483640
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   4524
                  Columns(0).Caption=   "PERIODO"
                  Columns(0).Name =   "PERIODO"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "BUZPER"
                  Columns(1).Name =   "BUZPER"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4524
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblbuzperiodo 
                  Caption         =   "DAntig�edad de las ofertas a mostrar:"
                  Height          =   300
                  Index           =   0
                  Left            =   315
                  TabIndex        =   241
                  Top             =   600
                  Width           =   3375
               End
            End
         End
         Begin VB.PictureBox picLeerOfer 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1065
            Left            =   -74850
            ScaleHeight     =   1065
            ScaleWidth      =   7935
            TabIndex        =   237
            Top             =   1905
            Width           =   7935
            Begin VB.Frame fraLeerOfer 
               Caption         =   "DMostrar por defecto los siguientes datos en el buz�n de ofertas"
               Height          =   990
               Left            =   0
               TabIndex        =   238
               Top             =   0
               Width           =   7890
               Begin VB.OptionButton opLeidas 
                  Caption         =   "DS�lo le�das"
                  Height          =   300
                  Left            =   390
                  TabIndex        =   104
                  Top             =   375
                  Width           =   2055
               End
               Begin VB.OptionButton opNoLeidas 
                  Caption         =   "DS�lo no le�das"
                  Height          =   300
                  Left            =   2782
                  TabIndex        =   105
                  Top             =   375
                  Width           =   2350
               End
               Begin VB.OptionButton opLeerTodas 
                  Caption         =   "DTodas"
                  Height          =   300
                  Left            =   5500
                  TabIndex        =   106
                  Top             =   375
                  Width           =   2055
               End
            End
         End
         Begin VB.PictureBox picMaxAdjun 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1065
            Left            =   -74835
            ScaleHeight     =   1065
            ScaleWidth      =   7950
            TabIndex        =   235
            Top             =   3060
            Width           =   7950
            Begin VB.Frame fraMAXADJUN 
               Caption         =   "DTama�o m�ximo en kilobytes del total de archivos adjuntos de una oferta"
               Height          =   990
               Left            =   0
               TabIndex        =   236
               Top             =   0
               Width           =   7890
               Begin VB.TextBox txtMAXADJUN 
                  Height          =   285
                  Left            =   405
                  TabIndex        =   107
                  Top             =   390
                  Width           =   1770
               End
            End
         End
         Begin VB.Frame Frame28 
            Caption         =   "DHoja comparativa"
            Height          =   1125
            Index           =   0
            Left            =   -74820
            TabIndex        =   224
            Top             =   420
            Width           =   7890
            Begin VB.PictureBox picComp 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   855
               Index           =   0
               Left            =   120
               ScaleHeight     =   855
               ScaleWidth      =   7740
               TabIndex        =   225
               TabStop         =   0   'False
               Top             =   240
               Width           =   7740
               Begin VB.TextBox txtLongCabComp 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   0
                  Left            =   3150
                  TabIndex        =   77
                  Top             =   420
                  Width           =   615
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   19
                  Left            =   7320
                  Picture         =   "frmCONFGEN.frx":1E38
                  Style           =   1  'Graphical
                  TabIndex        =   76
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   19
                  Left            =   1425
                  MaxLength       =   255
                  TabIndex        =   75
                  Top             =   60
                  Width           =   5880
               End
               Begin VB.Label lblHojaComp 
                  Caption         =   "DComienzo de comparativa en la fila:"
                  Height          =   225
                  Index           =   1
                  Left            =   75
                  TabIndex        =   242
                  Top             =   450
                  Width           =   2985
               End
               Begin VB.Label lblHojaComp 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Index           =   0
                  Left            =   75
                  TabIndex        =   226
                  Top             =   120
                  Width           =   1215
               End
            End
         End
         Begin VB.Frame frameAdj 
            Caption         =   "DHoja de adjudicaci�n directa"
            Height          =   735
            Index           =   3
            Left            =   -74820
            TabIndex        =   222
            Top             =   4040
            Width           =   7905
            Begin VB.PictureBox picAdjDir 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   375
               Left            =   60
               ScaleHeight     =   375
               ScaleWidth      =   7785
               TabIndex        =   227
               Top             =   240
               Width           =   7785
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   14
                  Left            =   1635
                  MaxLength       =   255
                  TabIndex        =   93
                  Top             =   60
                  Width           =   5790
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   14
                  Left            =   7470
                  Picture         =   "frmCONFGEN.frx":1EF7
                  Style           =   1  'Graphical
                  TabIndex        =   94
                  Top             =   60
                  Width           =   315
               End
               Begin VB.Label lblAdjudi 
                  Caption         =   "DPlantilla:"
                  Height          =   255
                  Index           =   4
                  Left            =   60
                  TabIndex        =   228
                  Top             =   75
                  Width           =   945
               End
            End
            Begin VB.CommandButton cmdPlantilla 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   20
               Left            =   6060
               Picture         =   "frmCONFGEN.frx":1FB6
               Style           =   1  'Graphical
               TabIndex        =   223
               Top             =   840
               Width           =   375
            End
         End
         Begin VB.Frame frameAdj 
            Caption         =   "DPlantillas para comunicaci�n de adjudicaci�n "
            Height          =   1155
            Index           =   1
            Left            =   -74820
            TabIndex        =   218
            Top             =   2060
            Width           =   7905
            Begin VB.PictureBox PicAdj2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   855
               Left            =   90
               ScaleHeight     =   855
               ScaleWidth      =   7755
               TabIndex        =   219
               Top             =   240
               Width           =   7755
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   9
                  Left            =   7425
                  Picture         =   "frmCONFGEN.frx":22F8
                  Style           =   1  'Graphical
                  TabIndex        =   89
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   9
                  Left            =   1590
                  MaxLength       =   255
                  TabIndex        =   88
                  Top             =   60
                  Width           =   5790
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   10
                  Left            =   7425
                  Picture         =   "frmCONFGEN.frx":23B7
                  Style           =   1  'Graphical
                  TabIndex        =   91
                  Top             =   480
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   10
                  Left            =   1590
                  MaxLength       =   255
                  TabIndex        =   90
                  Top             =   480
                  Width           =   5790
               End
               Begin VB.Label lblAdjudi 
                  Caption         =   "DPlantilla carta:"
                  Height          =   210
                  Index           =   1
                  Left            =   15
                  TabIndex        =   221
                  Top             =   75
                  Width           =   1560
               End
               Begin VB.Label lblAdjudi 
                  Caption         =   "DPlantilla e-mail:"
                  Height          =   180
                  Index           =   2
                  Left            =   15
                  TabIndex        =   220
                  Top             =   540
                  Width           =   1575
               End
            End
         End
         Begin VB.Frame frameAdj 
            Caption         =   "DE-mail de adjudicaci�n "
            Height          =   735
            Index           =   2
            Left            =   -74820
            TabIndex        =   215
            Top             =   3260
            Width           =   7905
            Begin VB.PictureBox PicAdj3 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   375
               Left            =   120
               ScaleHeight     =   375
               ScaleWidth      =   7755
               TabIndex        =   216
               TabStop         =   0   'False
               Top             =   240
               Width           =   7755
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   2
                  Left            =   1560
                  MaxLength       =   255
                  TabIndex        =   92
                  Top             =   60
                  Width           =   6150
               End
               Begin VB.Label lblAdjudi 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Index           =   3
                  Left            =   0
                  TabIndex        =   217
                  Top             =   75
                  Width           =   1260
               End
            End
         End
         Begin VB.Frame Frame22 
            Caption         =   "DComunicaci�n de NO adjudicaci�n"
            Height          =   1155
            Left            =   -74820
            TabIndex        =   211
            Top             =   420
            Width           =   7905
            Begin VB.PictureBox PicAdjNo1 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   855
               Left            =   120
               ScaleHeight     =   855
               ScaleWidth      =   7710
               TabIndex        =   212
               Top             =   240
               Width           =   7710
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   11
                  Left            =   7395
                  Picture         =   "frmCONFGEN.frx":2476
                  Style           =   1  'Graphical
                  TabIndex        =   99
                  Top             =   60
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   11
                  Left            =   1935
                  MaxLength       =   255
                  TabIndex        =   98
                  Top             =   60
                  Width           =   5445
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   12
                  Left            =   7395
                  Picture         =   "frmCONFGEN.frx":2535
                  Style           =   1  'Graphical
                  TabIndex        =   101
                  Top             =   480
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   12
                  Left            =   1935
                  MaxLength       =   255
                  TabIndex        =   100
                  Top             =   480
                  Width           =   5445
               End
               Begin VB.Label Label50 
                  Caption         =   "DPlantilla carta:"
                  Height          =   195
                  Left            =   0
                  TabIndex        =   214
                  Top             =   60
                  Width           =   1920
               End
               Begin VB.Label Label51 
                  Caption         =   "DPlantilla e-mail:"
                  Height          =   255
                  Left            =   0
                  TabIndex        =   213
                  Top             =   540
                  Width           =   1920
               End
            End
         End
         Begin VB.Frame Frame23 
            Caption         =   "DE-mail de NO Adjudicaci�n"
            Height          =   795
            Left            =   -74805
            TabIndex        =   208
            Top             =   1620
            Width           =   7875
            Begin VB.PictureBox PicAdjNo2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               ForeColor       =   &H80000008&
               Height          =   450
               Left            =   120
               ScaleHeight     =   450
               ScaleWidth      =   7695
               TabIndex        =   209
               TabStop         =   0   'False
               Top             =   240
               Width           =   7695
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   3
                  Left            =   1275
                  MaxLength       =   255
                  TabIndex        =   102
                  Top             =   60
                  Width           =   6405
               End
               Begin VB.Label lblbuzperiodo 
                  Caption         =   "DAsunto:"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   0
                  TabIndex        =   210
                  Top             =   120
                  Width           =   1185
               End
            End
         End
         Begin VB.Frame Frame18 
            Caption         =   "DE-mail de comunicaci�n de objetivos"
            Height          =   795
            Left            =   -74820
            TabIndex        =   185
            Top             =   2880
            Width           =   7845
            Begin VB.PictureBox picObj3 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   435
               Left            =   120
               ScaleHeight     =   435
               ScaleWidth      =   7635
               TabIndex        =   186
               TabStop         =   0   'False
               Top             =   240
               Width           =   7635
               Begin VB.TextBox txtMailSubject 
                  Height          =   285
                  Index           =   1
                  Left            =   1185
                  MaxLength       =   255
                  TabIndex        =   74
                  Top             =   60
                  Width           =   6450
               End
               Begin VB.Label lblComObj 
                  Caption         =   "DAsunto:"
                  Height          =   255
                  Index           =   4
                  Left            =   0
                  TabIndex        =   187
                  Top             =   120
                  Width           =   1095
               End
            End
         End
         Begin VB.Frame Frame17 
            Caption         =   "DPlantillas para comunicaci�n de objetivos"
            Height          =   1575
            Left            =   -74820
            TabIndex        =   180
            Top             =   1260
            Width           =   7845
            Begin VB.PictureBox picObj2 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1275
               Left            =   60
               ScaleHeight     =   1275
               ScaleWidth      =   7695
               TabIndex        =   181
               TabStop         =   0   'False
               Top             =   180
               Width           =   7695
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   7
                  Left            =   1995
                  MaxLength       =   255
                  TabIndex        =   72
                  Top             =   960
                  Width           =   5355
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   7
                  Left            =   7380
                  Picture         =   "frmCONFGEN.frx":25F4
                  Style           =   1  'Graphical
                  TabIndex        =   73
                  Top             =   960
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   6
                  Left            =   1995
                  MaxLength       =   255
                  TabIndex        =   70
                  Top             =   540
                  Width           =   5355
               End
               Begin VB.CommandButton cmdPlantilla 
                  Height          =   285
                  Index           =   6
                  Left            =   7380
                  Picture         =   "frmCONFGEN.frx":26B3
                  Style           =   1  'Graphical
                  TabIndex        =   71
                  Top             =   540
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   5
                  Left            =   1995
                  MaxLength       =   255
                  TabIndex        =   68
                  Top             =   120
                  Width           =   5355
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   5
                  Left            =   7380
                  Picture         =   "frmCONFGEN.frx":2772
                  Style           =   1  'Graphical
                  TabIndex        =   69
                  Top             =   120
                  Width           =   315
               End
               Begin VB.Label lblComObj 
                  Caption         =   "DPlantilla web:"
                  Height          =   315
                  Index           =   3
                  Left            =   60
                  TabIndex        =   184
                  Top             =   945
                  Width           =   1920
               End
               Begin VB.Label lblComObj 
                  Caption         =   "DPlantilla e-mail:"
                  Height          =   240
                  Index           =   2
                  Left            =   60
                  TabIndex        =   183
                  Top             =   600
                  Width           =   1920
               End
               Begin VB.Label lblComObj 
                  Caption         =   "DPlantilla carta:"
                  Height          =   195
                  Index           =   1
                  Left            =   60
                  TabIndex        =   182
                  Top             =   180
                  Width           =   1920
               End
            End
         End
         Begin VB.Frame Frame16 
            Caption         =   "DFormulario de oferta"
            Height          =   795
            Left            =   -74820
            TabIndex        =   177
            Top             =   420
            Width           =   7845
            Begin VB.PictureBox picObj1 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   555
               Left            =   120
               ScaleHeight     =   555
               ScaleWidth      =   7650
               TabIndex        =   178
               TabStop         =   0   'False
               Top             =   180
               Width           =   7650
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   4
                  Left            =   7320
                  Picture         =   "frmCONFGEN.frx":2831
                  Style           =   1  'Graphical
                  TabIndex        =   67
                  Top             =   120
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   4
                  Left            =   1335
                  MaxLength       =   255
                  TabIndex        =   66
                  Top             =   120
                  Width           =   5940
               End
               Begin VB.Label lblComObj 
                  Caption         =   "DPlantilla:"
                  Height          =   195
                  Index           =   0
                  Left            =   0
                  TabIndex        =   179
                  Top             =   180
                  Width           =   1260
               End
            End
         End
      End
      Begin TabDlg.SSTab sstabReu 
         Height          =   7035
         Left            =   -74820
         TabIndex        =   153
         Top             =   480
         Width           =   8100
         _ExtentX        =   14288
         _ExtentY        =   12409
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "DValores por defecto"
         TabPicture(0)   =   "frmCONFGEN.frx":28F0
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraConv(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraConv(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "fraConv(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "DPlantillas"
         TabPicture(1)   =   "frmCONFGEN.frx":290C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "picReuPlantillas"
         Tab(1).ControlCount=   1
         Begin VB.PictureBox picReuPlantillas 
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   6375
            Left            =   -74880
            ScaleHeight     =   6375
            ScaleWidth      =   7185
            TabIndex        =   165
            Top             =   360
            Width           =   7185
            Begin VB.Frame Frame15 
               Caption         =   "DActa"
               Height          =   1500
               Left            =   30
               TabIndex        =   174
               Top             =   3840
               Width           =   7065
               Begin VB.OptionButton optGenerarProc 
                  Caption         =   "DActaWord"
                  Height          =   255
                  Index           =   2
                  Left            =   855
                  TabIndex        =   375
                  Top             =   225
                  Width           =   2460
               End
               Begin VB.OptionButton optGenerarProc 
                  Caption         =   "DActaCrystal"
                  Height          =   255
                  Index           =   3
                  Left            =   3330
                  TabIndex        =   374
                  Top             =   195
                  Width           =   2460
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   40
                  Left            =   6630
                  Picture         =   "frmCONFGEN.frx":2928
                  Style           =   1  'Graphical
                  TabIndex        =   124
                  Top             =   1020
                  Width           =   315
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   18
                  Left            =   6630
                  Picture         =   "frmCONFGEN.frx":29E7
                  Style           =   1  'Graphical
                  TabIndex        =   122
                  Top             =   660
                  Width           =   315
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
                  Height          =   285
                  Index           =   11
                  Left            =   840
                  TabIndex        =   442
                  Top             =   660
                  Width           =   5745
                  DataFieldList   =   "Column 1"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "ID"
                  Columns(0).Name =   "ID"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   10134
                  Columns(1).Caption=   "NOM"
                  Columns(1).Name =   "NOM"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   10134
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
                  Height          =   285
                  Index           =   12
                  Left            =   840
                  TabIndex        =   443
                  Top             =   1020
                  Width           =   5745
                  DataFieldList   =   "Column 1"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "ID"
                  Columns(0).Name =   "ID"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   10134
                  Columns(1).Caption=   "NOM"
                  Columns(1).Name =   "NOM"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   10134
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   39
                  Left            =   840
                  TabIndex        =   123
                  Top             =   1020
                  Width           =   5745
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   18
                  Left            =   840
                  MaxLength       =   255
                  TabIndex        =   121
                  Top             =   660
                  Width           =   5745
               End
               Begin VB.Label lblReunionPlantillas 
                  Caption         =   "DCarta2:"
                  Height          =   255
                  Index           =   5
                  Left            =   120
                  TabIndex        =   338
                  Top             =   1080
                  Width           =   735
               End
               Begin VB.Label lblReunionPlantillas 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   4
                  Left            =   120
                  TabIndex        =   175
                  Top             =   720
                  Width           =   825
               End
            End
            Begin VB.Frame Frame13 
               Caption         =   "DAgenda"
               Height          =   675
               Left            =   30
               TabIndex        =   168
               Top             =   3120
               Width           =   7065
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   17
                  Left            =   6630
                  Picture         =   "frmCONFGEN.frx":2AA6
                  Style           =   1  'Graphical
                  TabIndex        =   120
                  Top             =   240
                  Width           =   315
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPlantilla 
                  Height          =   285
                  Index           =   10
                  Left            =   840
                  TabIndex        =   441
                  Top             =   240
                  Width           =   5745
                  DataFieldList   =   "Column 1"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "ID"
                  Columns(0).Name =   "ID"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   10134
                  Columns(1).Caption=   "NOM"
                  Columns(1).Name =   "NOM"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   10134
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblReunionPlantillas 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   3
                  Left            =   120
                  TabIndex        =   171
                  Top             =   300
                  Width           =   465
               End
            End
            Begin VB.Frame Frame12 
               Caption         =   "DConvocatoria"
               Height          =   2175
               Left            =   30
               TabIndex        =   167
               Top             =   900
               Width           =   7065
               Begin VB.Frame frmConvEmail 
                  Caption         =   "DEmail"
                  Height          =   1455
                  Left            =   120
                  TabIndex        =   397
                  Top             =   600
                  Width           =   6855
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   43
                     Left            =   840
                     MaxLength       =   255
                     TabIndex        =   404
                     Top             =   1080
                     Width           =   5625
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   44
                     Left            =   6480
                     Picture         =   "frmCONFGEN.frx":2B65
                     Style           =   1  'Graphical
                     TabIndex        =   403
                     Top             =   1080
                     Width           =   315
                  End
                  Begin VB.TextBox txtPlantilla 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Index           =   16
                     Left            =   840
                     MaxLength       =   255
                     TabIndex        =   401
                     Top             =   720
                     Width           =   5625
                  End
                  Begin VB.CommandButton cmdPlantilla 
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   285
                     Index           =   16
                     Left            =   6480
                     Picture         =   "frmCONFGEN.frx":2C24
                     Style           =   1  'Graphical
                     TabIndex        =   400
                     Top             =   720
                     Width           =   315
                  End
                  Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
                     Height          =   285
                     Index           =   6
                     Left            =   840
                     TabIndex        =   398
                     Top             =   360
                     Width           =   2070
                     DataFieldList   =   "Column 0"
                     ListAutoValidate=   0   'False
                     ListAutoPosition=   0   'False
                     AllowInput      =   0   'False
                     _Version        =   196617
                     DataMode        =   2
                     ColumnHeaders   =   0   'False
                     ForeColorEven   =   0
                     BackColorOdd    =   16777215
                     RowHeight       =   423
                     Columns.Count   =   3
                     Columns(0).Width=   6773
                     Columns(0).Caption=   "Denominaci�n"
                     Columns(0).Name =   "DEN"
                     Columns(0).DataField=   "Column 0"
                     Columns(0).DataType=   8
                     Columns(0).FieldLen=   256
                     Columns(1).Width=   3200
                     Columns(1).Visible=   0   'False
                     Columns(1).Caption=   "ID"
                     Columns(1).Name =   "ID"
                     Columns(1).DataField=   "Column 1"
                     Columns(1).DataType=   8
                     Columns(1).FieldLen=   256
                     Columns(2).Width=   3200
                     Columns(2).Visible=   0   'False
                     Columns(2).Caption=   "OFFSET"
                     Columns(2).Name =   "OFFSET"
                     Columns(2).DataField=   "Column 2"
                     Columns(2).DataType=   8
                     Columns(2).FieldLen=   256
                     _ExtentX        =   3651
                     _ExtentY        =   503
                     _StockProps     =   93
                     BackColor       =   -2147483643
                  End
                  Begin VB.Label lblReunionPlantillas 
                     Caption         =   "DE-Mail:"
                     Height          =   195
                     Index           =   6
                     Left            =   120
                     TabIndex        =   405
                     Top             =   1140
                     Width           =   1335
                  End
                  Begin VB.Label lblReunionPlantillas 
                     Caption         =   "DE-Mail:"
                     Height          =   195
                     Index           =   2
                     Left            =   120
                     TabIndex        =   402
                     Top             =   780
                     Width           =   1335
                  End
                  Begin VB.Label lblIdiConv 
                     Caption         =   "DIdioma"
                     Height          =   195
                     Left            =   120
                     TabIndex        =   399
                     Top             =   390
                     Width           =   1155
                  End
               End
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   15
                  Left            =   6630
                  Picture         =   "frmCONFGEN.frx":2CE3
                  Style           =   1  'Graphical
                  TabIndex        =   119
                  Top             =   240
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   15
                  Left            =   840
                  MaxLength       =   255
                  TabIndex        =   118
                  Top             =   240
                  Width           =   5745
               End
               Begin VB.Label lblReunionPlantillas 
                  Caption         =   "DCarta:"
                  Height          =   255
                  Index           =   1
                  Left            =   150
                  TabIndex        =   170
                  Top             =   300
                  Width           =   825
               End
            End
            Begin VB.Frame Frame4 
               Caption         =   "DHojas de adjudicaci�n"
               Height          =   765
               Left            =   30
               TabIndex        =   166
               Top             =   60
               Width           =   7065
               Begin VB.CommandButton cmdPlantilla 
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Index           =   13
                  Left            =   6630
                  Picture         =   "frmCONFGEN.frx":2DA2
                  Style           =   1  'Graphical
                  TabIndex        =   117
                  Top             =   300
                  Width           =   315
               End
               Begin VB.TextBox txtPlantilla 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Index           =   13
                  Left            =   840
                  MaxLength       =   255
                  TabIndex        =   116
                  Top             =   300
                  Width           =   5745
               End
               Begin VB.Label lblReunionPlantillas 
                  Caption         =   "DImpreso:"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   169
                  Top             =   360
                  Width           =   765
               End
            End
         End
         Begin VB.Frame fraConv 
            Caption         =   "DConvocatoria"
            Height          =   1575
            Index           =   0
            Left            =   120
            TabIndex        =   164
            Top             =   2880
            Width           =   7875
            Begin VB.PictureBox picConvocatoria 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1305
               Left            =   75
               ScaleHeight     =   1305
               ScaleWidth      =   7725
               TabIndex        =   172
               Top             =   180
               Width           =   7725
               Begin VB.TextBox txtConvMailSubject 
                  Height          =   315
                  Left            =   2085
                  MaxLength       =   255
                  TabIndex        =   114
                  Top             =   480
                  Width           =   5595
               End
               Begin VB.CheckBox chkSoloConvResp 
                  Caption         =   "DEntre los compradores solo convocar a los responsables de cada proceso"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   115
                  Top             =   900
                  Width           =   7500
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcIdioma 
                  Height          =   285
                  Index           =   7
                  Left            =   2085
                  TabIndex        =   395
                  Top             =   60
                  Width           =   2070
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  AllowInput      =   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   6773
                  Columns(0).Caption=   "Denominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "ID"
                  Columns(1).Name =   "ID"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "OFFSET"
                  Columns(2).Name =   "OFFSET"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   3651
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblbuzperiodo 
                  Caption         =   "DIdioma"
                  Height          =   195
                  Index           =   4
                  Left            =   120
                  TabIndex        =   396
                  Top             =   90
                  Width           =   1155
               End
               Begin VB.Label lblReunionValDef 
                  Caption         =   "DAsunto para e-mail:"
                  Height          =   225
                  Index           =   4
                  Left            =   120
                  TabIndex        =   173
                  Top             =   540
                  Width           =   1455
               End
            End
         End
         Begin VB.Frame fraConv 
            Caption         =   "DOrden habitual"
            Height          =   1155
            Index           =   2
            Left            =   120
            TabIndex        =   160
            Top             =   1620
            Width           =   7875
            Begin VB.PictureBox picOrden 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   915
               Left            =   90
               ScaleHeight     =   915
               ScaleWidth      =   7695
               TabIndex        =   161
               Top             =   180
               Width           =   7695
               Begin VB.ComboBox cmbCriterio 
                  Height          =   315
                  Index           =   0
                  Left            =   2100
                  Style           =   2  'Dropdown List
                  TabIndex        =   112
                  Top             =   60
                  Width           =   2835
               End
               Begin VB.ComboBox cmbCriterio 
                  Height          =   315
                  Index           =   1
                  Left            =   2085
                  Style           =   2  'Dropdown List
                  TabIndex        =   113
                  Top             =   480
                  Width           =   2835
               End
               Begin VB.Label lblReunionValDef 
                  Caption         =   "DPrimer criterio:"
                  Height          =   255
                  Index           =   2
                  Left            =   60
                  TabIndex        =   163
                  Top             =   120
                  Width           =   2025
               End
               Begin VB.Label lblReunionValDef 
                  Caption         =   "DSegundo criterio:"
                  Height          =   255
                  Index           =   3
                  Left            =   60
                  TabIndex        =   162
                  Top             =   540
                  Width           =   2025
               End
            End
         End
         Begin VB.Frame fraConv 
            Caption         =   "DCronolog�a habitual"
            Height          =   1125
            Index           =   1
            Left            =   120
            TabIndex        =   154
            Top             =   420
            Width           =   7875
            Begin VB.PictureBox picCrono 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   795
               Left            =   90
               ScaleHeight     =   795
               ScaleWidth      =   7740
               TabIndex        =   155
               Top             =   210
               Width           =   7740
               Begin VB.ComboBox cmbDiaSemana 
                  Height          =   315
                  Left            =   2085
                  Style           =   2  'Dropdown List
                  TabIndex        =   108
                  Top             =   60
                  Width           =   1335
               End
               Begin VB.ComboBox cmbNumSemanas 
                  Height          =   315
                  Left            =   5535
                  Style           =   2  'Dropdown List
                  TabIndex        =   109
                  Top             =   60
                  Width           =   2190
               End
               Begin VB.TextBox txtHoraReu 
                  Height          =   285
                  Left            =   2100
                  MaxLength       =   5
                  TabIndex        =   110
                  Top             =   510
                  Width           =   795
               End
               Begin VB.TextBox txtTiempoReu 
                  Height          =   285
                  Left            =   6915
                  MaxLength       =   3
                  TabIndex        =   111
                  Top             =   480
                  Width           =   810
               End
               Begin VB.Label lblReunionValDef 
                  Caption         =   "DD�a de la semana:"
                  Height          =   255
                  Index           =   0
                  Left            =   60
                  TabIndex        =   159
                  Top             =   120
                  Width           =   2025
               End
               Begin VB.Label lblReunionValDef 
                  Caption         =   "DPeriodicidad:"
                  Height          =   255
                  Index           =   5
                  Left            =   3855
                  TabIndex        =   158
                  Top             =   120
                  Width           =   1665
               End
               Begin VB.Label lblReunionValDef 
                  Caption         =   "DHora de inicio (hh:mm):"
                  Height          =   255
                  Index           =   1
                  Left            =   60
                  TabIndex        =   157
                  Top             =   510
                  Width           =   2025
               End
               Begin VB.Label lblReunionValDef 
                  Caption         =   "DTiempo dedicado por proceso (min):"
                  Height          =   195
                  Index           =   6
                  Left            =   3555
                  TabIndex        =   156
                  Top             =   540
                  Width           =   3345
               End
            End
         End
      End
      Begin VB.Frame fraManten 
         Caption         =   "DDestino ""sin transporte"" (ST)"
         Height          =   735
         Index           =   1
         Left            =   -74820
         TabIndex        =   143
         Top             =   2700
         Width           =   8130
         Begin VB.PictureBox picDestST 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   435
            Left            =   60
            ScaleHeight     =   435
            ScaleWidth      =   6615
            TabIndex        =   151
            TabStop         =   0   'False
            Top             =   240
            Width           =   6615
            Begin VB.TextBox txtDestSTDen 
               Height          =   285
               Left            =   1485
               MaxLength       =   50
               TabIndex        =   29
               Top             =   60
               Width           =   3030
            End
            Begin VB.CommandButton cmdDestCod 
               Caption         =   "DCambiar c�digo"
               Height          =   285
               Left            =   4620
               TabIndex        =   30
               Top             =   60
               Width           =   1335
            End
            Begin VB.Label lblTabMantenimiento 
               Caption         =   "DDenominaci�n:"
               Height          =   255
               Index           =   4
               Left            =   60
               TabIndex        =   152
               Top             =   120
               Width           =   1380
            End
         End
      End
      Begin VB.Frame fraManten 
         Caption         =   "DValores por defecto"
         Height          =   2055
         Index           =   0
         Left            =   -74820
         TabIndex        =   142
         Top             =   480
         Width           =   8130
         Begin VB.PictureBox picDef 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   1755
            Left            =   60
            ScaleHeight     =   1755
            ScaleWidth      =   6615
            TabIndex        =   145
            TabStop         =   0   'False
            Top             =   240
            Width           =   6615
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiCod 
               Height          =   285
               Left            =   1815
               TabIndex        =   21
               Top             =   0
               Width           =   855
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
               Height          =   285
               Left            =   2715
               TabIndex        =   22
               Top             =   0
               Width           =   3075
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5424
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonCod 
               Height          =   285
               Left            =   1815
               TabIndex        =   96
               Top             =   0
               Visible         =   0   'False
               Width           =   855
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcMonDen 
               Height          =   285
               Left            =   2715
               TabIndex        =   97
               Top             =   0
               Visible         =   0   'False
               Width           =   3075
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5424
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviCod 
               Height          =   285
               Left            =   1815
               TabIndex        =   23
               Top             =   420
               Width           =   855
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
               Height          =   285
               Left            =   2715
               TabIndex        =   24
               Top             =   420
               Width           =   3075
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5424
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
               Height          =   285
               Left            =   1815
               TabIndex        =   25
               Top             =   840
               Width           =   855
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
               Height          =   285
               Left            =   2715
               TabIndex        =   26
               Top             =   840
               Width           =   3075
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5424
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcUniCod 
               Height          =   285
               Left            =   1815
               TabIndex        =   27
               Top             =   1260
               Width           =   855
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   2540
               Columns(0).Caption=   "DCod"
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   6482
               Columns(1).Caption=   "DDenominaci�n"
               Columns(1).Name =   "DEN"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1508
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcUniDen 
               Height          =   285
               Left            =   2715
               TabIndex        =   28
               Top             =   1260
               Width           =   3075
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               ListAutoPosition=   0   'False
               _Version        =   196617
               DataMode        =   2
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   6165
               Columns(0).Caption=   "DDenominaci�n"
               Columns(0).Name =   "DEN"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2117
               Columns(1).Caption=   "DCod"
               Columns(1).Name =   "COD"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5424
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
            End
            Begin VB.Label lblTabMantenimiento 
               Caption         =   "DPa�s:"
               Height          =   315
               Index           =   0
               Left            =   75
               TabIndex        =   149
               Top             =   60
               Width           =   1605
            End
            Begin VB.Label lblTabMantenimiento 
               Caption         =   "DProvincia:"
               Height          =   195
               Index           =   1
               Left            =   60
               TabIndex        =   148
               Top             =   480
               Width           =   1605
            End
            Begin VB.Label lblTabMantenimiento 
               Caption         =   "DDestino:"
               Height          =   255
               Index           =   2
               Left            =   60
               TabIndex        =   147
               Top             =   900
               Width           =   1605
            End
            Begin VB.Label lblTabMantenimiento 
               Caption         =   "DUnidad:"
               Height          =   195
               Index           =   3
               Left            =   60
               TabIndex        =   146
               Top             =   1320
               Width           =   1605
            End
            Begin VB.Label lblbuzperiodo 
               Caption         =   "DMoneda central:"
               Height          =   195
               Index           =   2
               Left            =   60
               TabIndex        =   150
               Top             =   60
               Visible         =   0   'False
               Width           =   1065
            End
         End
      End
      Begin TabDlg.SSTab SSTabGestion 
         Height          =   8595
         Left            =   -75000
         TabIndex        =   243
         Top             =   360
         Width           =   10740
         _ExtentX        =   18944
         _ExtentY        =   15161
         _Version        =   393216
         Style           =   1
         Tabs            =   6
         TabsPerRow      =   6
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Valores por defecto"
         TabPicture(0)   =   "frmCONFGEN.frx":2E61
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Frame6"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraGrupoDefecto"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Configuraci�n de proceso por defecto"
         TabPicture(1)   =   "frmCONFGEN.frx":2E7D
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraConfProce(4)"
         Tab(1).Control(1)=   "fraPonderacion"
         Tab(1).Control(2)=   "fraAmbitoDatos"
         Tab(1).Control(3)=   "picConf(2)"
         Tab(1).ControlCount=   4
         TabCaption(2)   =   "Reglas de apertura de procesos"
         TabPicture(2)   =   "frmCONFGEN.frx":2E99
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "Frame5"
         Tab(2).Control(1)=   "fraAdjEspec"
         Tab(2).ControlCount=   2
         TabCaption(3)   =   "Otras reglas"
         TabPicture(3)   =   "frmCONFGEN.frx":2EB5
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "fraOpciones(5)"
         Tab(3).Control(1)=   "Frame3"
         Tab(3).ControlCount=   2
         TabCaption(4)   =   "Avisos"
         TabPicture(4)   =   "frmCONFGEN.frx":2ED1
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "ctrlConfGenAvisos"
         Tab(4).ControlCount=   1
         TabCaption(5)   =   "Solicitudes de compras"
         TabPicture(5)   =   "frmCONFGEN.frx":2EED
         Tab(5).ControlEnabled=   0   'False
         Tab(5).Control(0)=   "fraSolicit(0)"
         Tab(5).Control(1)=   "fraSolicit(1)"
         Tab(5).ControlCount=   2
         Begin VB.Frame fraOpciones 
            Height          =   2000
            Index           =   5
            Left            =   -74460
            TabIndex        =   507
            Top             =   3000
            Width           =   8150
            Begin VB.PictureBox picOtrasReglas 
               BorderStyle     =   0  'None
               Height          =   1500
               Index           =   1
               Left            =   50
               ScaleHeight     =   1500
               ScaleWidth      =   7995
               TabIndex        =   508
               Top             =   340
               Width           =   8000
               Begin VB.TextBox txtNumOfe 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   7125
                  TabIndex        =   513
                  Top             =   1000
                  Width           =   780
               End
               Begin VB.OptionButton optCalcLinBase 
                  Caption         =   "DCalcular la media de las X mejores ofertas vigentes. (Si X vale 0 son todas las ofertas)  X:"
                  Height          =   225
                  Index           =   2
                  Left            =   240
                  TabIndex        =   512
                  Top             =   1000
                  Width           =   6900
               End
               Begin VB.OptionButton optCalcLinBase 
                  Caption         =   "DCalcular la mediana de las primeras ofertas de cada proveedor"
                  Height          =   195
                  Index           =   1
                  Left            =   240
                  TabIndex        =   511
                  Top             =   700
                  Width           =   7620
               End
               Begin VB.OptionButton optCalcLinBase 
                  Caption         =   "DNo calcular"
                  Height          =   195
                  Index           =   0
                  Left            =   240
                  TabIndex        =   510
                  Top             =   400
                  Width           =   3060
               End
               Begin VB.Label label2 
                  Caption         =   "DRecalcular presupuesto unitario en los items con las ofertas recibidas"
                  Height          =   250
                  Index           =   28
                  Left            =   240
                  TabIndex        =   509
                  Top             =   0
                  Width           =   6975
               End
            End
         End
         Begin FSGSUserControls.ctrlConfGenAvisos ctrlConfGenAvisos 
            Height          =   8085
            Left            =   -74520
            TabIndex        =   506
            Top             =   390
            Width           =   9240
            _ExtentX        =   16298
            _ExtentY        =   14261
            General         =   -1  'True
         End
         Begin VB.Frame fraAdjEspec 
            Caption         =   "DAdjuntar especificaciones del art�culo a items"
            Height          =   1635
            Left            =   -74460
            TabIndex        =   480
            Top             =   5640
            Width           =   8200
            Begin VB.PictureBox picAdjEspec 
               BorderStyle     =   0  'None
               Height          =   660
               Left            =   200
               ScaleHeight     =   660
               ScaleWidth      =   7575
               TabIndex        =   484
               Top             =   255
               Width           =   7570
               Begin VB.OptionButton opAdjAutomat 
                  Caption         =   "DPreguntar si adjuntar"
                  Height          =   210
                  Index           =   1
                  Left            =   3000
                  TabIndex        =   488
                  Top             =   420
                  Width           =   2400
               End
               Begin VB.OptionButton opAdjAutomat 
                  Caption         =   "DNo adjuntar"
                  Height          =   270
                  Index           =   2
                  Left            =   5550
                  TabIndex        =   487
                  Top             =   420
                  Width           =   2000
               End
               Begin VB.OptionButton opAdjAutomat 
                  Caption         =   "DAdjuntar especificaciones del art�culo autom�ticamente"
                  Height          =   340
                  Index           =   0
                  Left            =   -20
                  TabIndex        =   486
                  Top             =   330
                  Width           =   2650
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DPermitir abrir lineas de �tems sin que correspondan a art�culos codificados"
                  Height          =   405
                  Index           =   1
                  Left            =   -20
                  TabIndex        =   485
                  Top             =   -50
                  Width           =   7550
               End
            End
            Begin VB.PictureBox picRegGest 
               BorderStyle     =   0  'None
               Height          =   375
               Left            =   200
               ScaleHeight     =   375
               ScaleWidth      =   7500
               TabIndex        =   481
               Top             =   1155
               Width           =   7500
               Begin VB.OptionButton opAdjAutomat 
                  Caption         =   "DCargar con presupuesto planificado"
                  Height          =   525
                  Index           =   4
                  Left            =   3000
                  TabIndex        =   483
                  Top             =   -120
                  Width           =   3405
               End
               Begin VB.OptionButton opAdjAutomat 
                  Caption         =   "DCargar presupuesto unitario con precio de �ltima adjudicaci�n"
                  Height          =   585
                  Index           =   3
                  Left            =   -20
                  TabIndex        =   482
                  Top             =   -120
                  Width           =   2800
               End
            End
            Begin VB.Line Line6 
               Index           =   0
               X1              =   240
               X2              =   7800
               Y1              =   1005
               Y2              =   1005
            End
         End
         Begin VB.Frame Frame5 
            Height          =   5145
            Left            =   -74460
            TabIndex        =   458
            Top             =   345
            Width           =   8200
            Begin VB.PictureBox picProcesos 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               ForeColor       =   &H80000008&
               Height          =   4785
               Left            =   120
               ScaleHeight     =   4785
               ScaleWidth      =   7995
               TabIndex        =   459
               TabStop         =   0   'False
               Top             =   120
               Width           =   8000
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DPermitir suministro retroactivo"
                  Height          =   240
                  Index           =   28
                  Left            =   30
                  TabIndex        =   504
                  Top             =   4440
                  Width           =   7860
               End
               Begin VB.TextBox txtImpMaxAdjDir 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   5325
                  MaxLength       =   25
                  TabIndex        =   490
                  Top             =   450
                  Width           =   2355
               End
               Begin VB.CheckBox chkRegOtraGest 
                  Caption         =   "DVolumen m�ximo para adjudicaci�n directa:"
                  Height          =   255
                  Left            =   45
                  TabIndex        =   489
                  Top             =   465
                  Width           =   5265
               End
               Begin VB.TextBox txtVolMaxAdjDir 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   5325
                  MaxLength       =   25
                  TabIndex        =   474
                  Top             =   105
                  Width           =   2355
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DVolumen m�ximo para adjudicaci�n directa:"
                  Height          =   255
                  Index           =   0
                  Left            =   45
                  TabIndex        =   473
                  Top             =   120
                  Width           =   5265
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DObligar a indicar unidad presupuestaria de proyecto, al nivel"
                  Height          =   195
                  Index           =   3
                  Left            =   45
                  TabIndex        =   472
                  Top             =   1170
                  Width           =   6495
               End
               Begin VB.ComboBox cmbOblDist 
                  Height          =   315
                  Index           =   1
                  Left            =   6825
                  Style           =   2  'Dropdown List
                  TabIndex        =   471
                  Top             =   1425
                  Width           =   855
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DObligar a indicar unidad presupuestaria contable, al nivel"
                  Height          =   255
                  Index           =   4
                  Left            =   30
                  TabIndex        =   470
                  Top             =   1725
                  Width           =   6645
               End
               Begin VB.ComboBox cmbOblDist 
                  Height          =   315
                  Index           =   2
                  IntegralHeight  =   0   'False
                  Left            =   6825
                  Style           =   2  'Dropdown List
                  TabIndex        =   469
                  Top             =   1980
                  Width           =   855
               End
               Begin VB.ComboBox cmbOblDist 
                  Height          =   315
                  Index           =   3
                  IntegralHeight  =   0   'False
                  Left            =   6825
                  Style           =   2  'Dropdown List
                  TabIndex        =   468
                  Top             =   2535
                  Width           =   855
               End
               Begin VB.ComboBox cmbOblDist 
                  Height          =   315
                  Index           =   4
                  IntegralHeight  =   0   'False
                  Left            =   6825
                  Style           =   2  'Dropdown List
                  TabIndex        =   467
                  Top             =   3090
                  Width           =   855
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DObligar a indicar unidad presupuestaria concepto 3, al nivel"
                  Height          =   255
                  Index           =   5
                  Left            =   30
                  TabIndex        =   466
                  Top             =   2280
                  Width           =   6690
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DObligar a indicar unidad presupuestaria concepto 4, al nivel"
                  Height          =   255
                  Index           =   6
                  Left            =   30
                  TabIndex        =   465
                  Top             =   2835
                  Width           =   6690
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DUtilizar ponderaci�n de atributos"
                  Height          =   240
                  Index           =   10
                  Left            =   30
                  TabIndex        =   464
                  Top             =   3645
                  Width           =   6900
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "Mantener la distribuci�n porcentual de presupuestos autom�ticamente "
                  Height          =   255
                  Index           =   11
                  Left            =   30
                  TabIndex        =   463
                  Top             =   3345
                  Width           =   6615
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DObligar a usar la unidad de medida por defecto de los art�culos"
                  Height          =   240
                  Index           =   23
                  Left            =   30
                  TabIndex        =   462
                  Top             =   3960
                  Width           =   7860
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DRestringir la asignaci�n de presupuestos a la unidad organizativa de los art�culos"
                  Height          =   240
                  Index           =   26
                  Left            =   30
                  TabIndex        =   461
                  Top             =   4200
                  Width           =   7620
               End
               Begin VB.ComboBox cmbOblDist 
                  Height          =   315
                  Index           =   0
                  IntegralHeight  =   0   'False
                  Left            =   6825
                  Style           =   2  'Dropdown List
                  TabIndex        =   460
                  Top             =   810
                  Width           =   855
               End
               Begin VB.Label lblOblDist 
                  Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de concepto 4"
                  Height          =   255
                  Index           =   4
                  Left            =   300
                  TabIndex        =   479
                  Top             =   3090
                  Width           =   6315
               End
               Begin VB.Label lblOblDist 
                  Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de concepto 3"
                  Height          =   255
                  Index           =   3
                  Left            =   300
                  TabIndex        =   478
                  Top             =   2535
                  Width           =   6315
               End
               Begin VB.Label lblOblDist 
                  Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de contable"
                  Height          =   255
                  Index           =   2
                  Left            =   300
                  TabIndex        =   477
                  Top             =   1980
                  Width           =   6315
               End
               Begin VB.Label lblOblDist 
                  Caption         =   "Nivel m�nimo de asignaci�n presupuestaria de proyecto"
                  Height          =   255
                  Index           =   1
                  Left            =   300
                  TabIndex        =   476
                  Top             =   1425
                  Width           =   6315
               End
               Begin VB.Label lblOblDist 
                  Caption         =   "Nivel m�nimo de asignaci�n a unidades organizativas"
                  Height          =   255
                  Index           =   0
                  Left            =   45
                  TabIndex        =   475
                  Top             =   840
                  Width           =   6315
               End
            End
         End
         Begin VB.Frame fraSolicit 
            Caption         =   "DGeneraci�n de ofertas desde solicitud"
            Height          =   1215
            Index           =   1
            Left            =   -74460
            TabIndex        =   365
            Top             =   2600
            Width           =   8600
            Begin VB.PictureBox picGenSolicit 
               BorderStyle     =   0  'None
               Height          =   720
               Index           =   1
               Left            =   240
               ScaleHeight     =   720
               ScaleWidth      =   7905
               TabIndex        =   367
               Top             =   300
               Width           =   7900
               Begin SSDataWidgets_B.SSDBCombo sdbcPeriodoSolic 
                  Height          =   285
                  Left            =   3120
                  TabIndex        =   371
                  Top             =   90
                  Width           =   2415
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Caption=   "PERIODO"
                  Columns(0).Name =   "PERIODO"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Visible=   0   'False
                  Columns(1).Caption=   "BUZPER"
                  Columns(1).Name =   "BUZPER"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   4260
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblPeriodoSolic 
                  Caption         =   "DPer�odo de validez:"
                  Height          =   255
                  Index           =   0
                  Left            =   0
                  TabIndex        =   370
                  Top             =   120
                  Width           =   2895
               End
            End
         End
         Begin VB.Frame fraSolicit 
            Caption         =   "DGeneraci�n de procesos desde solicitud"
            Height          =   1700
            Index           =   0
            Left            =   -74460
            TabIndex        =   360
            Top             =   700
            Width           =   8600
            Begin VB.PictureBox picGenSolicit 
               BorderStyle     =   0  'None
               Height          =   1300
               Index           =   0
               Left            =   240
               ScaleHeight     =   1305
               ScaleWidth      =   7905
               TabIndex        =   361
               Top             =   300
               Width           =   7900
               Begin VB.OptionButton optGenerarProc 
                  Caption         =   "DGenerar procesos de adjudicaci�n directa"
                  Height          =   255
                  Index           =   0
                  Left            =   0
                  TabIndex        =   364
                  Top             =   0
                  Width           =   4695
               End
               Begin VB.OptionButton optGenerarProc 
                  Caption         =   "DGenerar procesos de reuni�n"
                  Height          =   255
                  Index           =   1
                  Left            =   0
                  TabIndex        =   369
                  Top             =   900
                  Width           =   4695
               End
               Begin VB.PictureBox picSolicImporte 
                  BorderStyle     =   0  'None
                  Height          =   500
                  Left            =   480
                  ScaleHeight     =   495
                  ScaleWidth      =   7335
                  TabIndex        =   362
                  Top             =   360
                  Width           =   7335
                  Begin VB.TextBox txtAdjDirImp 
                     Alignment       =   1  'Right Justify
                     Height          =   285
                     Left            =   3200
                     TabIndex        =   368
                     Top             =   0
                     Width           =   1575
                  End
                  Begin VB.CheckBox chkAdjDirImp 
                     Caption         =   "DPara importes menores de:"
                     Height          =   255
                     Left            =   0
                     TabIndex        =   366
                     Top             =   20
                     Width           =   3075
                  End
                  Begin VB.Label lblPeriodoSolic 
                     Caption         =   "EUR"
                     Height          =   255
                     Index           =   1
                     Left            =   5000
                     TabIndex        =   363
                     Top             =   40
                     Width           =   855
                  End
               End
            End
         End
         Begin VB.Frame fraConfProce 
            Caption         =   "Pedidos"
            Height          =   520
            Index           =   4
            Left            =   -74460
            TabIndex        =   348
            Top             =   6540
            Width           =   7620
            Begin VB.PictureBox picDatosConf 
               BorderStyle     =   0  'None
               Height          =   255
               Index           =   7
               Left            =   100
               ScaleHeight     =   255
               ScaleWidth      =   3855
               TabIndex        =   349
               Top             =   240
               Width           =   3855
               Begin VB.CheckBox chkUnSoloPedido 
                  Caption         =   "Permitir un s�lo pedido directo"
                  Height          =   195
                  Left            =   -20
                  TabIndex        =   350
                  Top             =   -20
                  Width           =   3855
               End
            End
         End
         Begin VB.Frame Frame3 
            Height          =   2100
            Left            =   -74460
            TabIndex        =   332
            Top             =   660
            Width           =   8150
            Begin VB.PictureBox picOtrasReglas 
               BorderStyle     =   0  'None
               Height          =   1500
               Index           =   0
               Left            =   40
               ScaleHeight     =   1500
               ScaleWidth      =   8055
               TabIndex        =   333
               Top             =   345
               Width           =   8055
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DControl de cambios en las fechas de suministro al traspasar informaci�n al ERP"
                  Height          =   375
                  Index           =   25
                  Left            =   240
                  TabIndex        =   373
                  Top             =   1150
                  Width           =   7500
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DNo utilizar la selecci�n de proveedores por equipos de compra"
                  Height          =   255
                  Index           =   7
                  Left            =   240
                  TabIndex        =   337
                  Top             =   888
                  Width           =   7500
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DSelecci�n positiva en la selecci�n de proveedores"
                  Height          =   255
                  Index           =   12
                  Left            =   240
                  TabIndex        =   336
                  Top             =   592
                  Width           =   7500
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DObligar a asignar material a los proveedores del portal"
                  Height          =   255
                  Index           =   9
                  Left            =   240
                  TabIndex        =   335
                  Top             =   296
                  Width           =   7500
               End
               Begin VB.CheckBox chkRegGest 
                  Caption         =   "DPermitir adjudicar a proveedores no homologados"
                  Height          =   255
                  Index           =   8
                  Left            =   240
                  TabIndex        =   334
                  Top             =   0
                  Width           =   7500
               End
            End
         End
         Begin VB.Frame fraPonderacion 
            Caption         =   "DPonderaci�n"
            Height          =   510
            Left            =   -74460
            TabIndex        =   329
            Top             =   5940
            Width           =   7620
            Begin VB.PictureBox picConf 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   90
               ScaleHeight     =   240
               ScaleWidth      =   7485
               TabIndex        =   330
               Top             =   225
               Width           =   7485
               Begin VB.CheckBox chkPonderacion 
                  Caption         =   "DHabilitar ponderaci�n en atributos"
                  Height          =   195
                  Index           =   0
                  Left            =   0
                  TabIndex        =   331
                  Top             =   0
                  Width           =   5010
               End
            End
         End
         Begin VB.Frame fraAmbitoDatos 
            Caption         =   "DAmbito de datos"
            Height          =   2475
            Left            =   -74460
            TabIndex        =   326
            Top             =   360
            Width           =   7620
            Begin VB.PictureBox picAmbitoDatos 
               BorderStyle     =   0  'None
               Height          =   2130
               Left            =   135
               ScaleHeight     =   2130
               ScaleWidth      =   7350
               TabIndex        =   327
               Top             =   225
               Width           =   7350
               Begin SSDataWidgets_B.SSDBGrid sdbgAmbitoDatos 
                  Height          =   2085
                  Left            =   120
                  TabIndex        =   328
                  Top             =   0
                  Width           =   7170
                  ScrollBars      =   2
                  _Version        =   196617
                  DataMode        =   2
                  RecordSelectors =   0   'False
                  Col.Count       =   7
                  stylesets.count =   3
                  stylesets(0).Name=   "Yellow"
                  stylesets(0).BackColor=   11862015
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCONFGEN.frx":2F09
                  stylesets(1).Name=   "Normal"
                  stylesets(1).ForeColor=   0
                  stylesets(1).BackColor=   14671839
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCONFGEN.frx":2F25
                  stylesets(2).Name=   "Header"
                  stylesets(2).HasFont=   -1  'True
                  BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(2).Picture=   "frmCONFGEN.frx":2F41
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowColumnSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   0
                  HeadStyleSet    =   "Header"
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   7
                  Columns(0).Width=   979
                  Columns(0).Caption=   "DUsar"
                  Columns(0).Name =   "Usar"
                  Columns(0).AllowSizing=   0   'False
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Style=   2
                  Columns(1).Width=   6800
                  Columns(1).Caption=   "DDato"
                  Columns(1).Name =   "Dato"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   1455
                  Columns(2).Caption=   "DProceso"
                  Columns(2).Name =   "Proceso"
                  Columns(2).AllowSizing=   0   'False
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Style=   2
                  Columns(3).Width=   1455
                  Columns(3).Caption=   "DGrupo"
                  Columns(3).Name =   "Grupo"
                  Columns(3).AllowSizing=   0   'False
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(3).Style=   2
                  Columns(4).Width=   1455
                  Columns(4).Caption=   "DItem"
                  Columns(4).Name =   "Item"
                  Columns(4).AllowSizing=   0   'False
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(4).Style=   2
                  Columns(5).Width=   3200
                  Columns(5).Visible=   0   'False
                  Columns(5).Caption=   "Obligatoria"
                  Columns(5).Name =   "Obligatoria"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(6).Width=   3200
                  Columns(6).Visible=   0   'False
                  Columns(6).Caption=   "Fila"
                  Columns(6).Name =   "Fila"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  _ExtentX        =   12647
                  _ExtentY        =   3678
                  _StockProps     =   79
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
         End
         Begin VB.Frame fraGrupoDefecto 
            Caption         =   "DGrupo por defecto para procesos"
            Height          =   1875
            Left            =   540
            TabIndex        =   257
            Top             =   2760
            Width           =   7890
            Begin VB.PictureBox picGrupoDefecto 
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1590
               Left            =   45
               ScaleHeight     =   1590
               ScaleWidth      =   7800
               TabIndex        =   258
               Top             =   225
               Width           =   7800
               Begin VB.TextBox txtGrupoDefecto 
                  Height          =   285
                  Index           =   0
                  Left            =   990
                  TabIndex        =   40
                  Top             =   90
                  Width           =   1140
               End
               Begin VB.TextBox txtGrupoDefecto 
                  Height          =   285
                  Index           =   1
                  Left            =   3825
                  MaxLength       =   100
                  TabIndex        =   41
                  Top             =   90
                  Width           =   3030
               End
               Begin VB.TextBox txtGrupoDefecto 
                  Height          =   735
                  Index           =   2
                  Left            =   225
                  MaxLength       =   500
                  MultiLine       =   -1  'True
                  TabIndex        =   42
                  Top             =   720
                  Width           =   6630
               End
               Begin VB.Label lblGrupoDef 
                  Caption         =   "DC�digo"
                  Height          =   240
                  Index           =   0
                  Left            =   180
                  TabIndex        =   261
                  Top             =   105
                  Width           =   825
               End
               Begin VB.Label lblGrupoDef 
                  Caption         =   "DDenominaci�n"
                  Height          =   240
                  Index           =   1
                  Left            =   2475
                  TabIndex        =   260
                  Top             =   105
                  Width           =   1365
               End
               Begin VB.Label lblGrupoDef 
                  Caption         =   "DDescripci�n del grupo"
                  Height          =   240
                  Index           =   2
                  Left            =   225
                  TabIndex        =   259
                  Top             =   450
                  Width           =   1815
               End
            End
         End
         Begin VB.Frame Frame6 
            Caption         =   "DValores por defecto"
            Height          =   1935
            Left            =   540
            TabIndex        =   244
            Top             =   750
            Width           =   7890
            Begin VB.PictureBox picGesDef 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   1695
               Left            =   270
               ScaleHeight     =   1695
               ScaleWidth      =   7575
               TabIndex        =   245
               TabStop         =   0   'False
               Top             =   195
               Width           =   7575
               Begin SSDataWidgets_B.SSDBCombo sdbcRolCod 
                  Height          =   285
                  Left            =   2670
                  TabIndex        =   32
                  Top             =   60
                  Width           =   855
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "DCod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "DDenominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1508
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcRolDen 
                  Height          =   285
                  Left            =   3585
                  TabIndex        =   33
                  Top             =   60
                  Width           =   3075
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "DCod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5424
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPagCod 
                  Height          =   285
                  Left            =   2670
                  TabIndex        =   34
                  Top             =   480
                  Width           =   855
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "DCod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "DDenominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1508
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcPagDen 
                  Height          =   285
                  Left            =   3585
                  TabIndex        =   35
                  Top             =   480
                  Width           =   3075
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "DCod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5424
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcEstCod 
                  Height          =   285
                  Index           =   0
                  Left            =   2670
                  TabIndex        =   36
                  Top             =   900
                  Width           =   855
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "DCod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "DDenominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1508
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcEstDen 
                  Height          =   285
                  Index           =   0
                  Left            =   3585
                  TabIndex        =   37
                  Top             =   900
                  Width           =   3075
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "DCod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5424
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcEstCod 
                  Height          =   285
                  Index           =   1
                  Left            =   2670
                  TabIndex        =   38
                  Top             =   1320
                  Width           =   855
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2540
                  Columns(0).Caption=   "DCod"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   6482
                  Columns(1).Caption=   "DDenominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   1508
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin SSDataWidgets_B.SSDBCombo sdbcEstDen 
                  Height          =   285
                  Index           =   1
                  Left            =   3585
                  TabIndex        =   39
                  Top             =   1320
                  Width           =   3075
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   6165
                  Columns(0).Caption=   "DDenominaci�n"
                  Columns(0).Name =   "DEN"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   2117
                  Columns(1).Caption=   "DCod"
                  Columns(1).Name =   "COD"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5424
                  _ExtentY        =   503
                  _StockProps     =   93
                  BackColor       =   -2147483643
               End
               Begin VB.Label lblValDef 
                  Caption         =   "DEstado ofertas web:"
                  Height          =   225
                  Index           =   3
                  Left            =   0
                  TabIndex        =   249
                  Top             =   1380
                  Width           =   2400
               End
               Begin VB.Label lblValDef 
                  Caption         =   "DEstado inicial ofertas:"
                  Height          =   210
                  Index           =   2
                  Left            =   0
                  TabIndex        =   248
                  Top             =   960
                  Width           =   2400
               End
               Begin VB.Label lblValDef 
                  Caption         =   "DForma de pago:"
                  Height          =   225
                  Index           =   1
                  Left            =   0
                  TabIndex        =   247
                  Top             =   540
                  Width           =   2400
               End
               Begin VB.Label lblValDef 
                  Caption         =   "DRol persona / proceso:"
                  Height          =   210
                  Index           =   0
                  Left            =   0
                  TabIndex        =   246
                  Top             =   120
                  Width           =   2400
               End
            End
         End
         Begin VB.PictureBox picConf 
            BorderStyle     =   0  'None
            Height          =   3015
            Index           =   2
            Left            =   -75000
            ScaleHeight     =   3015
            ScaleWidth      =   8445
            TabIndex        =   491
            Top             =   2880
            Width           =   8445
            Begin VB.Frame fraSolOfe 
               Caption         =   "DSolicitudes de ofertas"
               Height          =   2370
               Left            =   540
               TabIndex        =   494
               Top             =   600
               Width           =   7620
               Begin VB.CommandButton cmdConfigSubasta 
                  Caption         =   "..."
                  Height          =   300
                  Left            =   4380
                  TabIndex        =   505
                  Top             =   180
                  Width           =   300
               End
               Begin VB.PictureBox picConf 
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   2130
                  Index           =   1
                  Left            =   45
                  ScaleHeight     =   2130
                  ScaleWidth      =   7530
                  TabIndex        =   495
                  Top             =   180
                  Width           =   7530
                  Begin VB.CheckBox chkSolOfe 
                     Caption         =   "Ofertar en la moneda del proceso"
                     Height          =   240
                     Index           =   6
                     Left            =   45
                     TabIndex        =   503
                     Top             =   810
                     Width           =   7035
                  End
                  Begin VB.CheckBox chkSolOfe 
                     Caption         =   "DSolicitar cantidades m�ximas de suministro"
                     Height          =   240
                     Index           =   2
                     Left            =   45
                     TabIndex        =   502
                     Top             =   555
                     Width           =   7035
                  End
                  Begin VB.CheckBox chkSolOfe 
                     Caption         =   "DPedir alternativas de precios"
                     Height          =   240
                     Index           =   1
                     Left            =   45
                     TabIndex        =   501
                     Top             =   285
                     Width           =   7035
                  End
                  Begin VB.CheckBox chkSolOfe 
                     Caption         =   "DRecepci�n de ofertas en modo subasta"
                     Height          =   240
                     Index           =   0
                     Left            =   45
                     TabIndex        =   500
                     Top             =   30
                     Width           =   7035
                  End
                  Begin VB.CheckBox chkSolOfe 
                     Caption         =   "DPermitir adjudicar archivos a nivel de proceso"
                     Height          =   240
                     Index           =   3
                     Left            =   45
                     TabIndex        =   499
                     Top             =   1080
                     Width           =   7035
                  End
                  Begin VB.CheckBox chkSolOfe 
                     Caption         =   "DPermitir adjudicar archivos a nivel de grupo"
                     Height          =   240
                     Index           =   4
                     Left            =   45
                     TabIndex        =   498
                     Top             =   1365
                     Width           =   7035
                  End
                  Begin VB.CheckBox chkSolOfe 
                     Caption         =   "DPermitir adjudicar archivos a nivel de �tem"
                     Height          =   240
                     Index           =   5
                     Left            =   45
                     TabIndex        =   497
                     Top             =   1635
                     Width           =   7035
                  End
                  Begin VB.CheckBox chkNoPublicarFinSum 
                     Caption         =   "No publicar fecha de fin de suministro"
                     Height          =   330
                     Left            =   45
                     TabIndex        =   496
                     Top             =   1860
                     Width           =   7035
                  End
               End
            End
            Begin VB.Frame fraAdminPub 
               Caption         =   "DAdministraci�n p�blica"
               Height          =   540
               Left            =   540
               TabIndex        =   492
               Top             =   0
               Width           =   7620
               Begin VB.CheckBox chkPonderacion 
                  Caption         =   "DProceso de administraci�n p�blica"
                  Height          =   195
                  Index           =   1
                  Left            =   120
                  TabIndex        =   493
                  Top             =   240
                  Width           =   5010
               End
            End
         End
      End
   End
   Begin MSComDlg.CommonDialog cdlArchivo 
      Left            =   75
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "DSeleccione plantilla"
      Filter          =   "Plantillas de Word (*.dot)|*.dot"
   End
End
Attribute VB_Name = "frmCONFGEN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Valores par�metros subasta
Private m_iSubTipo As Integer
Private m_iSubModo As Integer
Private m_vSubDuracion As Variant
Private m_iSubastaEspera As Integer
Private m_bSubPublicar As Boolean
Private m_bSubNotifEventos As Boolean
Private m_bSubastaProve As Boolean
Private m_bSubVerDesdePrimPuja As Boolean
Private m_bSubastaPrecioPuja As Boolean
Private m_bSubastaPujas As Boolean
Private m_bSubastaBajMinPuja As Boolean
Private m_iSubBajMinGanTipo As Integer
Private m_vSubBajMinGanProcVal As Variant
Private m_vSubBajMinGanGrupoVal As Variant
Private m_vSubBajMinGanItemVal As Variant
Private m_bSubBajMinProve As Boolean
Private m_iSubBajMinProveTipo As Integer
Private m_vSubBajMinProveProcVal As Variant
Private m_vSubBajMinProveGrupoVal As Variant
Private m_vSubBajMinProveItemVal As Variant
Private m_dcSubTextosFin As Dictionary

Private oFos As FileSystemObject
Private iNEM As Integer
Private iNEO As Integer
Private MonRespetarCombo As Boolean

Private MonCargarComboDesde As Boolean
Private PaiRespetarCombo As Boolean
Private PaiCargarComboDesde As Boolean
Private ProviRespetarCombo As Boolean
Private ProviCargarComboDesde As Boolean
Private DestRespetarCombo As Boolean
Private DestCargarComboDesde As Boolean
Private UniRespetarCombo As Boolean
Private UniCargarComboDesde As Boolean
Private RolRespetarCombo As Boolean
Private RolCargarComboDesde As Boolean
Private PagRespetarCombo As Boolean
Private PagCargarComboDesde As Boolean
Private EstRespetarCombo(0 To 1) As Boolean
Private EstCargarComboDesde(0 To 1) As Boolean
Private bRespetarControl As Boolean
Private bChangeTxt As Boolean

Private sParte() As String
Private EstUsarPres(0 To 3) As Boolean
Private m_bCambiarThres As Boolean

Private oIdiomas As CIdiomas
Private oIdiomasPortal As CIdiomas
Private oMonedas As CMonedas
Private oPaises As CPaises
Private oPaisSeleccionado As CPais
Private oDestinos As CDestinos
Private oUnidades As CUnidades
Private oRoles As CRoles
Private oPagos As CPagos
Private oOfeEstados As COfeEstados

Private sCodDestSTCurrent As String
Private sDenDestSTCurrent As String
Private sCodDestSTNew As String
Private sDenDestSTNew As String

Private Criterios(0 To 4) As String
Private ClickControl As Boolean
Private sCap(1 To 59) As String

Public oLiteralesAModificar As CLiterales

Public g_bCodigoCancelar As Boolean
Public g_sCodigoNuevo As String

Private arrERP() As Variant

Private idiUORow As Variant
Private idiMatRow As Variant
Private idiPresRow As Variant
Private idiOtrosRow As Variant
Private idiAprovRow As Variant
Private idiPedAbiertoRow As Variant
Private idiConvocatoriaRow As Variant
Private idiConvocatoriaSubjectRow As Variant
Private idiRelacionRow As Variant
Private idiIdiomasRow As Variant
Private idiIdiomasPortalRow As Variant
Private idiRecepRow As Variant
Private sIdiAvisoAdj As String
Private sIdiAvisoDepub As String
Private sIdiAntelacion  As String
Private arRecursosConfPedidosCom(9) As String
Private arRecursosConfGenPedidos(90) As String

Private ConPresupuRow As Variant
Private sDia As String
Private sDias As String
Private sSemana As String
Private sSemanas As String
Private sMes As String
Private sMeses As String
Private sDatos(10) As String
Private sFormatoTextoTAntelacion(7) As String
Private m_bComboTAntelacion As Boolean
Private m_sIdiCodGrupo As String
Private m_sIdiDenGrupo As String
Private m_sIdiCampo As String
Private m_sAnyo As String
Private m_sAnyos As String

'Indica si se mostrar� o no la solicitud de compra
Private m_bMostrarSolicitud As Boolean
Private m_bMostrarCarpetas As Boolean

Private Const Matrix = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
Private strMatrix(26) As String

Private m_oIdiomas As CIdiomas
Private m_sLitPartida As String

Private mbExitPorBasica As Boolean

''' <summary>
''' Funci�n que verifica todos los parametros que se han cambiado del formulario de configuracion general
''' </summary>
''' <returns>Una variable booleana que determinar� si los par�metros son correctos</returns>
''' <remarks>Llamada desde: CmdAceptar_click
''' Tiempo estimado: 0</remarks>
Private Function VerificarParametros() As Boolean
    Dim oIdioma As CIdioma
    Dim sFileName As String
    Dim iCasoVerif As Integer
    
    VerificarParametros = False
    
    If txtDenUO(0) = "" Then
        oMensajes.ImposibleActualizarParam sCap(1)
        Exit Function
    End If
    If txtDenUO(1) = "" Then
        oMensajes.ImposibleActualizarParam sCap(2)
        Exit Function
    End If
    If txtAbrUO(1) = "" Then
        oMensajes.ImposibleActualizarParam sCap(3)
        Exit Function
    End If
    If gParametrosGenerales.giNEO > 1 Then
        If txtDenUO(2) = "" Then
            oMensajes.ImposibleActualizarParam sCap(4)
            Exit Function
        End If
        If txtAbrUO(2) = "" Then
            oMensajes.ImposibleActualizarParam sCap(5)
            Exit Function
        End If
        If gParametrosGenerales.giNEO > 2 Then
            If txtDenUO(3) = "" Then
                oMensajes.ImposibleActualizarParam sCap(6)
                Exit Function
            End If
            If txtAbrUO(3) = "" Then
                oMensajes.ImposibleActualizarParam sCap(7)
                Exit Function
            End If
        End If
    End If
    If txtDenMat(1) = "" Then
        oMensajes.ImposibleActualizarParam sCap(8)
        Exit Function
    End If
    If txtAbrMat(1) = "" Then
        oMensajes.ImposibleActualizarParam sCap(9)
        Exit Function
    End If
    If gParametrosGenerales.giNEM > 1 Then
        If txtDenMat(2) = "" Then
            oMensajes.ImposibleActualizarParam sCap(10)
            Exit Function
        End If
        If txtAbrMat(2) = "" Then
            oMensajes.ImposibleActualizarParam sCap(11)
            Exit Function
        End If
        If gParametrosGenerales.giNEM > 2 Then
            If txtDenMat(3) = "" Then
                oMensajes.ImposibleActualizarParam sCap(12)
                Exit Function
            End If
            If txtAbrMat(3) = "" Then
                oMensajes.ImposibleActualizarParam sCap(13)
                Exit Function
            End If
            If gParametrosGenerales.giNEM > 3 Then
                If txtDenMat(4) = "" Then
                    oMensajes.ImposibleActualizarParam sCap(14)
                    Exit Function
                End If
                If txtAbrMat(4) = "" Then
                    oMensajes.ImposibleActualizarParam sCap(15)
                    Exit Function
                End If
            End If
        End If
    End If
    sdbcMonCod_Validate False
    If sdbcMonCod = "" Then
        oMensajes.ImposibleActualizarParam sCap(16)
        Exit Function
    End If
    If sdbcMonCod <> "" Then
        sdbcMonCod_Validate False
        If sdbcMonCod = "" Then
            oMensajes.ImposibleActualizarParam sCap(17)
            Exit Function
        End If
    End If
    If sdbcPaiCod <> "" Then
        sdbcPaiCod_Validate False
        If sdbcPaiCod = "" Then
            oMensajes.ImposibleActualizarParam sCap(18)
            Exit Function
        End If
    End If
    If sdbcProviCod <> "" Then
        sdbcProviCod_Validate False
        If sdbcProviCod = "" Then
            oMensajes.ImposibleActualizarParam sCap(19)
            Exit Function
        End If
    End If
    If sdbcDestCod <> "" Then
        sdbcDestCod_Validate False
        If sdbcDestCod = "" Then
            oMensajes.ImposibleActualizarParam sCap(20)
            Exit Function
        End If
    End If
    If sdbcUniCod <> "" Then
        sdbcUniCod_Validate False
        If sdbcUniCod = "" Then
           oMensajes.ImposibleActualizarParam sCap(21)
            Exit Function
        End If
    End If
    If sCodDestSTNew = "" Then
        oMensajes.ImposibleActualizarParam sCap(22)
        Exit Function
    End If
    If sDenDestSTNew = "" Then
        oMensajes.ImposibleActualizarParam sCap(23)
        Exit Function
    End If
    If chkRegGest(0).Value = vbChecked Then
        If Not IsNumeric(txtVolMaxAdjDir) Then
            oMensajes.ImposibleActualizarParam sCap(24)
            Exit Function
        Else
            If CDbl(txtVolMaxAdjDir) <= 0 Then
                oMensajes.ImposibleActualizarParam sCap(24)
                Exit Function
            End If
        End If
    End If
    If chkRegOtraGest.Value = vbChecked Then
        If Not IsNumeric(txtImpMaxAdjDir) Then
            oMensajes.ImposibleActualizarParam sCap(59)
            Exit Function
        Else
            If CDbl(txtImpMaxAdjDir) <= 0 Then
                oMensajes.ImposibleActualizarParam sCap(59)
                Exit Function
            End If
        End If
    End If
    If sdbcRolCod <> "" Then
        sdbcRolCod_Validate False
        If sdbcRolCod = "" Then
            oMensajes.ImposibleActualizarParam sCap(25)
            Exit Function
        End If
    End If
    If sdbcPagCod <> "" Then
        sdbcPagCod_Validate False
        If sdbcPagCod = "" Then
            oMensajes.ImposibleActualizarParam sCap(26)
            Exit Function
        End If
    End If
    If sdbcEstCod(0) <> "" Then
        sdbcEstCod_Validate 0, False
        If sdbcEstCod(0) = "" Then
            oMensajes.ImposibleActualizarParam sCap(27)
            Exit Function
        End If
    Else
        oMensajes.ImposibleActualizarParam sCap(27)
        Exit Function
    End If
    
    If sdbcEstCod(1) <> "" Then
        sdbcEstCod_Validate 1, False
        If sdbcEstCod(1) = "" Then
            oMensajes.ImposibleActualizarParam sCap(28)
            Exit Function
        End If
    Else
        oMensajes.ImposibleActualizarParam sCap(28)
        Exit Function
    End If
        
    If txtHoraReu <> "" Then
        If Not IsTime(txtHoraReu) Then
            oMensajes.ImposibleActualizarParam sCap(29)
            Exit Function
        End If
    Else
        oMensajes.ImposibleActualizarParam sCap(29)
        Exit Function
    End If
    
    If txtTiempoReu <> "" Then
        If Not IsInteger(txtTiempoReu) Then
            oMensajes.ImposibleActualizarParam sCap(30)
            Exit Function
        End If
        If CInt(txtTiempoReu) <= 0 Then
            oMensajes.ImposibleActualizarParam sCap(30)
            Exit Function
        End If
    End If
    
    If gParametrosGenerales.giINSTWEB Then
        If txtURL = "" Then
            oMensajes.ImposibleActualizarParam sCap(31)
            Exit Function
        End If
    End If
    
    If Not IsNumeric(txtLongCabComp(0)) Or val(txtLongCabComp(0)) <= 0 Then
        oMensajes.ImposibleActualizarParam sCap(32)
        Exit Function
    End If
    
    If Not IsNumeric(txtLongCabComp(1)) Or val(txtLongCabComp(1)) <= 0 Then
        oMensajes.ImposibleActualizarParam sCap(32)
        Exit Function
    End If
    
    If Not IsNumeric(txtLongCabComp(2)) Or val(txtLongCabComp(2)) <= 0 Then
        oMensajes.ImposibleActualizarParam sCap(32)
        Exit Function
    End If
    
    If Not oFos.FolderExists(txtRuta(0).Text) Then
        oMensajes.RutaNoValida ("RPT")
        Exit Function
    End If
            
    If m_bMostrarCarpetas Then
        If Not oFos.FolderExists(txtRuta(1).Text) Then
            oMensajes.RutaNoValida ("Solicit")
            Exit Function
        End If
    End If
    
    If optRutaTS(0).Value = True Then
        If Not oFos.FolderExists(txtRuta(2).Text) Then
            oMensajes.RutaNoValida ("TSRuta")
            Exit Function
        End If
    ElseIf optRutaTS(1).Value = True Then
        If UCase(Left(txtRuta(3).Text, 11)) <> UCase("\\tsclient\") Then
            oMensajes.RutaNoValida ("TSRuta")
            Exit Function
        End If
    End If
    
    If sdbcAntigOfer = "" Then
        oMensajes.ImposibleActualizarParam lblbuzperiodo(0).caption
        Exit Function
    End If
 
    If Me.ctrlConfGenAvisos.AvisoAdjSoloSiResp = True Then
        If Me.ctrlConfGenAvisos.AntelacionAdjudicacion = 0 Then
            oMensajes.ImposibleActualizarParam sIdiAvisoAdj & vbLf & sIdiAntelacion
            Exit Function
        End If
    End If

    If Me.ctrlConfGenAvisos.AvisoVisorGSSoloSiResp = True Then
        If Me.ctrlConfGenAvisos.AntelacionVisorGS = 0 Then
            oMensajes.ImposibleActualizarParam sIdiAvisoDepub & vbLf & sIdiAntelacion
            Exit Function
        End If
    End If
    
    'Nunca deber�a pasar puesto que por defecto PARGEN_GEST.BUZLEC=2
    If ((opLeidas.Value = False) And (opNoLeidas = False) And (opLeerTodas = False)) Then
        oMensajes.ImposibleActualizarParam fraLeerOfer.caption
        Exit Function
    End If
    
    If Not IsNumeric(txtMAXADJUN) Then
        oMensajes.ImposibleActualizarParam fraMAXADJUN.caption
        Exit Function
    End If
    
    If CDbl(txtMAXADJUN) <= 0 Then
        oMensajes.ImposibleActualizarParam fraMAXADJUN.caption
        Exit Function
    End If
    
    If ((opAdjAutomat(0).Value = False) And (opAdjAutomat(1).Value = False) And (opAdjAutomat(2).Value = False)) Or _
       ((opAdjAutomat(3).Value = False) And (opAdjAutomat(4).Value = False)) Then
        oMensajes.ImposibleActualizarParam fraAdjEspec.caption
        Exit Function
    End If
    
    If chkRecordatorio.Value = vbChecked Then
        If cboTAntelacion.Text = "" Then
            oMensajes.ImposibleActualizarParam lblModSub(5).caption
            Exit Function
        End If
    End If
    
    If txtGrupoDefecto(0).Text = "" Then
        oMensajes.ImposibleActualizarParam m_sIdiCodGrupo
        Exit Function
    Else
        If Not NombreDeGrupoValido(txtGrupoDefecto(0).Text) Then
            oMensajes.CodigoGrupoNoValido m_sIdiCodGrupo
            Exit Function
        End If
    End If
    
    If txtGrupoDefecto(1).Text = "" Then
        oMensajes.ImposibleActualizarParam m_sIdiDenGrupo
        Exit Function
    End If
    
    If Not ctlConfPed.VerificarParametros(oMensajes, iCasoVerif) Then
        If iCasoVerif = 1 Then
            tabCONFGEN.Tab = 5
            ctlConfPed.txtMailSubject_SetFocus
        End If
                
        Exit Function
    End If
    
    If gParametrosGenerales.gbPedidosDirectos Then
        For Each oIdioma In oIdiomas
            If Not oLiteralesAModificar.Item(oIdioma.Cod & "37") Is Nothing Then
                If IsNull(oLiteralesAModificar.Item(oIdioma.Cod & "37").Den) Or oLiteralesAModificar.Item(oIdioma.Cod & "37").Den = "" Then
                    oMensajes.FaltanReferencias
                    Exit Function
                End If
            Else
                oMensajes.FaltanReferencias
                Exit Function
            End If
        Next
    End If
    If chkCorreo(1).Value = vbChecked Then
        If optAutenticacion(2).Value = True Then 'Anonima
            If txtSMTP(4).Text = "" Then
                oMensajes.ImposibleActualizarParamFalta 140
                Exit Function
            End If
        ElseIf optAutenticacion(1).Value = True Then 'Windows
            If txtSMTP(2).Text = "" Or txtSMTP(3).Text = "" Or txtSMTP(4).Text = "" Then
                oMensajes.ImposibleActualizarParamFalta 141
                Exit Function
            End If
        Else 'basica
            If (txtSMTP(4).Text = "" Or txtSMTP(3).Text = "") Then
                oMensajes.ImposibleActualizarParamFalta 142
                Exit Function
            End If
        End If
    End If
    
    If m_bMostrarSolicitud Then
        If Trim(txtAdjDirImp.Text) <> "" Then
            If Not IsNumeric(txtAdjDirImp.Text) Or val(txtLongCabComp(0).Text) <= 0 Then
                oMensajes.ImposibleActualizarParam sCap(57)
                Exit Function
            End If
        End If
        
        If sdbcPeriodoSolic.Text = "" Then
            oMensajes.ImposibleActualizarParam fraSolicit(1).caption
            Exit Function
        End If
    End If
    If Not optGenerarProc(2).Value = True Then
        sFileName = oFos.GetFileName(Trim(txtPlantilla(18).Text))
        If UCase(Right(sFileName, 3)) <> "RPT" Then
            oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(180), TipoIconoMensaje.Information
            Exit Function
        End If
        sFileName = oFos.GetFileName(Trim(txtPlantilla(39).Text))
        If UCase(Right(sFileName, 3)) <> "RPT" Then
            oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(180), TipoIconoMensaje.Information
            Exit Function
        End If
    End If
    'Verificaci�n de los par�metros de Seguridad
    If txtGrupoDefecto(3).Text > 24 Or txtGrupoDefecto(3).Text < 0 Then
        oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(1147), Critical
        tabCONFGEN.Tab = 6
        If Me.Visible Then txtGrupoDefecto(3).SetFocus
        Exit Function
    End If
    If txtGrupoDefecto(4).Text > 998 Or txtGrupoDefecto(4).Text < 0 Then
    
        oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(1148), Critical
        tabCONFGEN.Tab = 6
        If Me.Visible Then txtGrupoDefecto(4).SetFocus
        Exit Function
    End If
    If txtGrupoDefecto(5).Text > 999 Or txtGrupoDefecto(5).Text < 0 Then
        oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(1149), Critical
        tabCONFGEN.Tab = 6
        If Me.Visible Then txtGrupoDefecto(5).SetFocus
        Exit Function
    End If
    If txtGrupoDefecto(6).Text > 14 Or txtGrupoDefecto(6).Text < 0 Then
        oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(1150), Critical
        tabCONFGEN.Tab = 6
        If Me.Visible Then txtGrupoDefecto(6).SetFocus
        Exit Function
    End If
    If CInt(txtGrupoDefecto(5).Text) <> 0 And CInt(txtGrupoDefecto(4).Text) > CInt(txtGrupoDefecto(5).Text) Then
        oMensajes.MensajeOKOnly err.Description + vbCrLf + oMensajes.CargarTextoMensaje(1151), Critical
        tabCONFGEN.Tab = 6
        If Me.Visible Then txtGrupoDefecto(4).SetFocus
        Exit Function
    End If
    If sdbcIdiomas(2).Text = "" Then
        oMensajes.ImposibleActualizarParam fraLenguaje(2).caption
        Exit Function
    End If
    
    If optCalcLinBase(2).Value = True Then
        If Not IsNumeric(txtNumOfe.Text) Then
            oMensajes.MensajeOKOnly err.Description + oMensajes.CargarTextoMensaje(1530)
            Exit Function
        ElseIf Not txtNumOfe.Text >= 0 Then
            oMensajes.MensajeOKOnly err.Description + oMensajes.CargarTextoMensaje(1530)
            Exit Function
        ElseIf (InStr(1, txtNumOfe.Text, ".") > 0 Or InStr(1, txtNumOfe.Text, ",") > 0) Then
            oMensajes.MensajeOKOnly err.Description + oMensajes.CargarTextoMensaje(1530)
            Exit Function
        End If
    End If
    
    sdbgCamposERP.Update
    
    VerificarParametros = True
End Function

''' <summary>
''' Procedimiento que configura los controles del formulario en modo consulta
''' </summary>
''' <remarks>Llamada desde: Frm_Load
''' Tiempo m�ximo: 0 seg</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub ModoConsulta()
    Dim i As Integer
    
    For i = 0 To 3
        picModSub(i).Enabled = False
    Next i
    picSugerir.Enabled = False
    picConf(1).Enabled = False
    picConf(0).Enabled = False
    picDatosConf(7).Enabled = False
    picGrupoDefecto.Enabled = False
    picObj1.Enabled = False
    picObj2.Enabled = False
    picObj3.Enabled = False
    picPres2.Enabled = False
    picPres3.Enabled = False
    picListados.Enabled = False
    picUO.Enabled = False
    picMat.Enabled = False
    picAdjEspec.Enabled = False
    picRegGest.Enabled = False
    picDef.Enabled = False
    picDestST.Enabled = False
    cmdDestCod.Visible = False
    picProcesos.Enabled = False
    picGesDef.Enabled = False
    picPetOfe(1).Enabled = False
    picPetOfe(2).Enabled = False
    picPetOfe(3).Enabled = False
    picURL.Enabled = False
    picCrono.Enabled = False
    picOrden.Enabled = False
    picSeg(0).Enabled = False
    If oUsuarioSummit.Tipo = Administrador Then
        picSeg(1).Enabled = False 'Lo nuevo de configuracion general
        Frame11(1).Visible = True
    Else
        Frame11(1).Visible = False
    End If
    picReuPlantillas.Enabled = False
    picConvocatoria.Enabled = False
    picEdit.Visible = False
    picNavigate.Visible = True
    picDenSolicitud.Enabled = False
    picPres1.Enabled = False
    PicAdj1.Enabled = False
    PicAdj2.Enabled = False
    PicAdj3.Enabled = False
    PicAdjNo1.Enabled = False
    PicAdjNo2.Enabled = False
    picOpciones(0).Enabled = False
    picOpciones(1).Enabled = False
    picComp(0).Enabled = False
    picComp(1).Enabled = False
    picComp(2).Enabled = False
    picAdjDir.Enabled = False
    picBuzPeriodo.Enabled = False
    picLeerOfer.Enabled = False
    picMaxAdjun.Enabled = False
    picConf(3).Enabled = False
    PicConPres.Enabled = False
                        
    Me.ctrlConfGenAvisos.ModoConsulta = True
    picOtrasReglas(0).Enabled = False
    picOtrasReglas(1).Enabled = False
    picAmbitoDatos.Enabled = True
    
    picNomRel.Enabled = False
    
    Picture1.Enabled = False
    
    For i = 0 To sdbgCamposERP.Columns.Count - 1
        sdbgCamposERP.Columns(i).Locked = True
    Next
        
    For i = 0 To sdbgAmbitoDatos.Columns.Count - 1
        sdbgAmbitoDatos.Columns(i).Locked = True
    Next i
    
    picConf(2).Enabled = False
    picAdj4.Enabled = False
    
    ctrlConfGenAvisos.ModoConsulta = True
    
    If m_bMostrarSolicitud Then
        picGenSolicit(0).Enabled = False
        picGenSolicit(1).Enabled = False
    End If
    
    ctlConfPed.ModoEdicion False
End Sub

''' <summary>
''' Procedimiento que configura los controles del formulario en modo edici�n
''' </summary>
''' <remarks>Llamada desde: cmdEdicion_click
''' Tiempo m�ximo: 0 seg</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub ModoEdicion()
    Dim oLiterales As CLiterales
    Dim lValor As Long
    Dim i As Integer
    Dim bytUT As Byte
    Dim oIdioma As CIdioma
    
    For i = 0 To 3
        picModSub(i).Enabled = True
    Next i
    picSugerir.Enabled = True
    If (chkRecordatorio.Value = vbChecked) Then
        picModSub(4).Enabled = True
        FormatearTiempoAntelacionCombo gParametrosGenerales.glSubastaTiempAnt, lValor, bytUT
        cboTAntelacion.Text = lValor & " " & sFormatoTextoTAntelacion(bytUT)
    Else
        picModSub(4).Enabled = False
        cboTAntelacion.clear
    End If
    
    picAmbitoDatos.Enabled = True
    sdbgAmbitoDatos.Columns(0).Locked = False
    For i = 2 To sdbgAmbitoDatos.Columns.Count - 1
        sdbgAmbitoDatos.Columns(i).Locked = False
    Next i
    
    picConf(1).Enabled = True
    picConf(0).Enabled = True
    picDatosConf(7).Enabled = True
    picGrupoDefecto.Enabled = True
    
    picObj1.Enabled = True
    picObj2.Enabled = True
    picObj3.Enabled = True
    picPres2.Enabled = True
    picPres3.Enabled = True
    picListados.Enabled = True
    picPres1.Enabled = True
    picUO.Enabled = True
    picMat.Enabled = True
    picAdjEspec.Enabled = True
    picRegGest.Enabled = True
    picDef.Enabled = True
    picDestST.Enabled = True
    cmdDestCod.Visible = True
    picProcesos.Enabled = True
    picGesDef.Enabled = True
    picPetOfe(1).Enabled = True
    picPetOfe(2).Enabled = True
    picPetOfe(3).Enabled = True
    picURL.Enabled = True
    picCrono.Enabled = True
    picOrden.Enabled = True
    picSeg(0).Enabled = True
    If oUsuarioSummit.Tipo = Administrador Then
        Frame11(1).Visible = True
        picSeg(1).Enabled = True 'Lo nuevo de configuracion general
    Else
        Frame11(1).Visible = False
    End If
    picDenSolicitud.Enabled = True
    picReuPlantillas.Enabled = True
    picConvocatoria.Enabled = True
    picNavigate.Visible = False
    picEdit.Visible = True
    picEdit.Top = picNavigate.Top
    PicAdj1.Enabled = True
    PicAdj2.Enabled = True
    PicAdj3.Enabled = True
    PicAdjNo1.Enabled = True
    PicAdjNo2.Enabled = True
    
    picNomRel.Enabled = True
    
    picOpciones(0).Enabled = True
    If chkCorreo(0).Value = vbChecked Then
        fraOpciones(1).Enabled = True
        fraOpciones(2).Enabled = False
    ElseIf chkCorreo(1).Value = vbChecked Then
        fraOpciones(1).Enabled = False
        fraOpciones(2).Enabled = True
        fraOpciones(3).Enabled = True
        fraOpciones(4).Enabled = True
        
        If optAutenticacion(2).Value = True Then 'anonima
            Me.txtSMTP(4).Visible = True
            
            Me.lblSMTP(4).Visible = True
            
            Me.fraOpciones(4).Height = 645
            
            Me.txtSMTP(2).Visible = False
            Me.txtSMTP(3).Visible = False
            Me.txtSMTP(5).Visible = False
            
            Me.lblSMTP(2).Visible = False
            Me.lblSMTP(3).Visible = False
            Me.lblSMTP(5).Visible = False
                    
        ElseIf optAutenticacion(1).Value = True Then 'windows
            Me.txtSMTP(4).Visible = True
            Me.txtSMTP(2).Visible = True
            Me.txtSMTP(3).Visible = True
            Me.txtSMTP(5).Visible = True
            
            Me.lblSMTP(4).Visible = True
            Me.lblSMTP(2).Visible = True
            Me.lblSMTP(3).Visible = True
            Me.lblSMTP(5).Visible = True
            
            Me.fraOpciones(4).Height = 1695
        Else 'basica
            Me.txtSMTP(4).Visible = True
            Me.txtSMTP(3).Visible = True
            
            Me.lblSMTP(4).Visible = True
            Me.lblSMTP(3).Visible = True
            
            Me.fraOpciones(4).Height = 975
            
            Me.txtSMTP(2).Visible = False
            Me.txtSMTP(5).Visible = False
            
            Me.lblSMTP(2).Visible = False
            Me.lblSMTP(5).Visible = False
        End If
    Else
        fraOpciones(1).Enabled = False
        fraOpciones(2).Enabled = False
    End If
    picOpciones(1).Enabled = True
    
    picComp(0).Enabled = True
    picComp(1).Enabled = True
    picComp(2).Enabled = True
    picAdjDir.Enabled = True
    picBuzPeriodo.Enabled = True
    picLeerOfer.Enabled = True
    picMaxAdjun.Enabled = True
    picConf(3).Enabled = True
    PicConPres.Enabled = True
                        
    Me.ctrlConfGenAvisos.ModoConsulta = False
    picConf(2).Enabled = True
    picAdj4.Enabled = True
    picOtrasReglas(0).Enabled = True
    picOtrasReglas(1).Enabled = True
    
    Picture1.Enabled = True
    
    'Tengo que rellenar los literales
    Set oLiteralesAModificar = Nothing
    Set oLiteralesAModificar = New CLiterales
    
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(1).Text, 8
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(1).Text, 9
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(2).Text, 10
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(2).Text, 11
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(3).Text, 12
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(3).Text, 13
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(4).Text, 14
    oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(4).Text, 15
    oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(0).Text, 1
    oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(1).Text, 2
    oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(1).Text, 3
    oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(2).Text, 4
    oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(2).Text, 5
    oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(3).Text, 6
    oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(3).Text, 7
     
    oLiteralesAModificar.Add "SPA", gParametrosGenerales.gsCONVMAILHTMLSPA, 10008
    oLiteralesAModificar.Add "SPA", gParametrosGenerales.gsCONVMAILTEXTSPA, 10009
    oLiteralesAModificar.Add "SPA", gParametrosGenerales.gsCONVMAILSUBJECTSPA, 10010
    
    oLiteralesAModificar.Add "ENG", gParametrosGenerales.gsCONVMAILHTMLENG, 10008
    oLiteralesAModificar.Add "ENG", gParametrosGenerales.gsCONVMAILTEXTENG, 10009
    oLiteralesAModificar.Add "ENG", gParametrosGenerales.gsCONVMAILSUBJECTENG, 10010
    
    oLiteralesAModificar.Add "GER", gParametrosGenerales.gsCONVMAILHTMLGER, 10008
    oLiteralesAModificar.Add "GER", gParametrosGenerales.gsCONVMAILTEXTGER, 10009
    oLiteralesAModificar.Add "GER", gParametrosGenerales.gsCONVMAILSUBJECTGER, 10010
     
    oLiteralesAModificar.Add "FRA", gParametrosGenerales.gsCONVMAILHTMLFRA, 10008
    oLiteralesAModificar.Add "FRA", gParametrosGenerales.gsCONVMAILTEXTFRA, 10009
    oLiteralesAModificar.Add "FRA", gParametrosGenerales.gsCONVMAILSUBJECTFRA, 10010
     
    Select Case sdbcConPres.Columns("ID").Value
       Case 1, 2:
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(20, 23, sdbcIdioma(2).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
             Select Case sdbcConPres.Columns("ID").Value
                Case 1:
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 21
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 23
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 20
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 22
               Case 2:
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 20
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 22
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 21
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 23
             End Select
    Case 3, 4:
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(27, 30, sdbcIdioma(2).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
             Select Case sdbcConPres.Columns("ID").Value
                Case 3:
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 28
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 30
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 27
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 29
               Case 4:
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 27
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 29
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 28
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 30
              End Select
    End Select
     
    oLiteralesAModificar.Add sdbcIdioma(3).Columns("ID").Value, txtDenSolicitud.Text, 19
     
    ' Modificar las cajas de texto por el idioma
    Screen.MousePointer = vbHourglass
    Set oLiterales = oGestorParametros.DevolverLiterales(45, 46, sdbcIdioma(8).Columns("ID").Value)
    Screen.MousePointer = vbNormal
    txtDenPres(2).Text = oLiterales.Item(sdbcIdioma(8).Columns("ID").Value & CStr(45)).Den
    txtDenPres(3).Text = oLiterales.Item(sdbcIdioma(8).Columns("ID").Value & CStr(46)).Den
    oLiteralesAModificar.Add sdbcIdioma(8).Columns("ID").Value, txtDenPres(2).Text, 45
    oLiteralesAModificar.Add sdbcIdioma(8).Columns("ID").Value, txtDenPres(3).Text, 46
    
    'Carga los literales  para la codificaci�n personalizada de pedidos:
    For Each oIdioma In oIdiomas
        Set oLiterales = oGestorParametros.DevolverLiterales(31, 47, oIdioma.Cod)
        If oLiterales.Count > 0 Then
            oLiteralesAModificar.Add oIdioma.Cod, oLiterales.Item(oIdioma.Cod & "31").Den, 31
            oLiteralesAModificar.Add oIdioma.Cod, oLiterales.Item(oIdioma.Cod & "36").Den, 36
            oLiteralesAModificar.Add oIdioma.Cod, oLiterales.Item(oIdioma.Cod & "37").Den, 37
            oLiteralesAModificar.Add oIdioma.Cod, oLiterales.Item(oIdioma.Cod & "47").Den, 47 'Recepciones
       End If
    Next
    
    Set oLiterales = Nothing
    
    For i = 0 To sdbgCamposERP.Columns.Count - 1
        If i <> 1 Then
            sdbgCamposERP.Columns(i).Locked = False
        End If
    Next
        
    If m_bMostrarSolicitud Then
        picGenSolicit(0).Enabled = True
        picGenSolicit(1).Enabled = True
    End If
    
    ctlConfPed.ModoEdicion True
End Sub

''' <summary>
''' Procedimiento que muestra los par�metros generales en los campos del formulario que correspondan
''' </summary>
''' <remarks>
''' Llamada desde: cmdAceptar_click,cmdCancelar_click,Form_Load
''' Tiempo m�ximo: 0 seg</remarks>
''' <revision>JVS 07/09/2011</revision>

Private Sub MostrarParametros()
    Dim dcListaZonasHorarias As Dictionary
    Dim oDestinoST As CDestino
    Dim i As Integer
    Dim j As Integer
    Dim lValor As Long
    Dim bytUT As Byte
    Dim oIdioma As CIdioma
    Dim iNumPres As Integer

    On Error GoTo Error
     
    txtDenUO(0) = gParametrosGenerales.gsDEN_UON0
    txtDenUO(1) = gParametrosGenerales.gsDEN_UON1
    txtAbrUO(1) = gParametrosGenerales.gsABR_UON1
    txtDenUO(2) = gParametrosGenerales.gsDEN_UON2
    txtAbrUO(2) = gParametrosGenerales.gsABR_UON2
    txtDenUO(3) = gParametrosGenerales.gsDEN_UON3
    txtAbrUO(3) = gParametrosGenerales.gsABR_UON3
    txtDenMat(1) = gParametrosGenerales.gsDEN_GMN1
    txtAbrMat(1) = gParametrosGenerales.gsabr_GMN1
    txtDenMat(2) = gParametrosGenerales.gsDEN_GMN2
    txtAbrMat(2) = gParametrosGenerales.gsABR_GMN2
    txtDenMat(3) = gParametrosGenerales.gsDEN_GMN3
    txtAbrMat(3) = gParametrosGenerales.gsABR_GMN3
    txtDenMat(4) = gParametrosGenerales.gsDEN_GMN4
    txtAbrMat(4) = gParametrosGenerales.gsABR_GMN4
    
    txtDenSolicitud = gParametrosGenerales.gsDenSolicitudCompra
    
    ' Asignar los valores de los par�metros de Nombre de relaci�n a los campos de texto
    txtDenPres(2) = gParametrosGenerales.gsSingNombreRel
    txtDenPres(3) = gParametrosGenerales.gsPlurNombreRel
            
    sdbcMonCod.Text = gParametrosGenerales.gsMONCEN
    sdbcMonCod_Validate False
    sdbcPaiCod.Text = gParametrosGenerales.gsPAIDEF
    sdbcPaiCod_Validate False
    sdbcProviCod.Text = gParametrosGenerales.gsPROVIDEF
    sdbcProviCod_Validate False
    sdbcDestCod.Text = gParametrosGenerales.gsDESTDEF
    sdbcDestCod_Validate False
    sdbcUniCod.Text = gParametrosGenerales.gsUNIDEF
    sdbcUniCod_Validate False
    
    Set oDestinoST = oFSGSRaiz.Generar_CDestino
    Set oDestinoST.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In m_oIdiomas
        oDestinoST.Denominaciones.Add oIdioma.Cod, ""
    Next
    oDestinoST.CargarDestinoSinTransporte
    
    sCodDestSTCurrent = oDestinoST.Cod
    sDenDestSTCurrent = oDestinoST.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    sCodDestSTNew = oDestinoST.Cod
    sDenDestSTNew = oDestinoST.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    fraManten(1).caption = sCap(33) & " (" & oDestinoST.Cod & ")"
    txtDestSTDen = oDestinoST.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    
    chkSugerir.Value = IIf(gParametrosGenerales.gbSugerirCodArticulos, vbChecked, vbUnchecked)
    
    If gParametrosGenerales.gdVOL_MAX_ADJ_DIR > 0 Then
        chkRegGest(0).Value = vbChecked
        txtVolMaxAdjDir.Enabled = True
        txtVolMaxAdjDir = Format(gParametrosGenerales.gdVOL_MAX_ADJ_DIR, "standard")
    Else
        chkRegGest(0).Value = vbUnchecked
        txtVolMaxAdjDir = 0
        txtVolMaxAdjDir.Enabled = False
    End If
    If gParametrosGenerales.gdIMP_MAX_ADJ_DIR > 0 Then
        chkRegOtraGest.Value = vbChecked
        txtImpMaxAdjDir.Enabled = True
        txtImpMaxAdjDir = Format(gParametrosGenerales.gdIMP_MAX_ADJ_DIR, "standard")
    Else
        chkRegOtraGest.Value = vbUnchecked
        txtImpMaxAdjDir = 0
        txtImpMaxAdjDir.Enabled = False
    End If
    ''''''''''''''''''''''''''''''
    iNumPres = 0
    If gParametrosGenerales.gbUsarPres1 Then
        chkRegGest(3).Visible = True
        lblOblDist(1).Visible = True
        cmbOblDist(1).Visible = True
        cmbOblDist(1).clear
        cmbOblDist(1).AddItem ""
        For i = 1 To gParametrosGenerales.giNEPP
            cmbOblDist(1).AddItem i
        Next i
        chkRegGest(4).Top = chkRegGest(3).Top + 551
        lblOblDist(2).Top = lblOblDist(1).Top + 551
        cmbOblDist(2).Top = cmbOblDist(1).Top + 551
        iNumPres = iNumPres + 1
    Else
        chkRegGest(3).Visible = False
        lblOblDist(1).Visible = False
        cmbOblDist(1).Visible = False
        chkRegGest(3).Value = 0
        chkRegGest(4).Top = chkRegGest(3).Top
        lblOblDist(2).Top = lblOblDist(1).Top
        cmbOblDist(2).Top = cmbOblDist(1).Top
    End If
    
    If gParametrosGenerales.gbUsarPres2 Then
        chkRegGest(4).Visible = True
        lblOblDist(2).Visible = True
        cmbOblDist(2).Visible = True
        cmbOblDist(2).clear
        cmbOblDist(2).AddItem ""
        For i = 1 To gParametrosGenerales.giNEPC
            cmbOblDist(2).AddItem i
        Next i
        chkRegGest(5).Top = chkRegGest(4).Top + 551
        lblOblDist(3).Top = lblOblDist(2).Top + 551
        cmbOblDist(3).Top = cmbOblDist(2).Top + 551
        iNumPres = iNumPres + 1
    Else
        chkRegGest(4).Visible = False
        lblOblDist(2).Visible = False
        cmbOblDist(2).Visible = False
        chkRegGest(4).Value = 0
        chkRegGest(5).Top = chkRegGest(4).Top
        lblOblDist(3).Top = lblOblDist(2).Top
        cmbOblDist(3).Top = cmbOblDist(2).Top
    
    End If
    
    If gParametrosGenerales.gbUsarPres3 Then
        chkRegGest(5).Visible = True
        lblOblDist(3).Visible = True
        cmbOblDist(3).Visible = True
        cmbOblDist(3).clear
        cmbOblDist(3).AddItem ""
        For i = 1 To gParametrosGenerales.giNEP3
            cmbOblDist(3).AddItem i
        Next i
        chkRegGest(6).Top = chkRegGest(5).Top + 551
        lblOblDist(4).Top = lblOblDist(3).Top + 551
        cmbOblDist(4).Top = cmbOblDist(3).Top + 551
        iNumPres = iNumPres + 1
    Else
        chkRegGest(5).Visible = False
        lblOblDist(3).Visible = False
        cmbOblDist(3).Visible = False
        chkRegGest(5).Value = 0
        chkRegGest(6).Top = chkRegGest(5).Top
        lblOblDist(4).Top = lblOblDist(3).Top
        cmbOblDist(4).Top = cmbOblDist(3).Top
    End If
        
    If gParametrosGenerales.gbUsarPres4 Then
        chkRegGest(6).Visible = True
        lblOblDist(4).Visible = True
        cmbOblDist(4).Visible = True
        cmbOblDist(4).clear
        cmbOblDist(4).AddItem ""
        For i = 1 To gParametrosGenerales.giNEP4
            cmbOblDist(4).AddItem i
        Next i
        chkRegGest(11).Top = chkRegGest(6).Top + 551
        iNumPres = iNumPres + 1
    Else
        chkRegGest(6).Visible = False
        lblOblDist(4).Visible = False
        cmbOblDist(4).Visible = False
        chkRegGest(6).Value = 0
        chkRegGest(11).Top = chkRegGest(6).Top
    End If
    
    If gParametrosGenerales.gbPresupuestosAut Then
        chkRegGest(11).Value = vbChecked
    Else
        chkRegGest(11).Value = vbUnchecked
    End If
    
    chkRegGest(10).Top = chkRegGest(11).Top + 300
    chkRegGest(23).Top = chkRegGest(10).Top + 300
    chkRegGest(26).Top = chkRegGest(23).Top + 300
                
    If gParametrosGenerales.gbOblAsigPresUonArtAper Then chkRegGest(26).Value = vbChecked
        
    picProcesos.Height = chkRegGest(28).Top + chkRegGest(28).Height + 50

    Frame5.Height = picProcesos.Height + 150
    fraAdjEspec.Top = Frame5.Top + Frame5.Height + 50
    
    cmbOblDist(0).ListIndex = IIf(gParametrosGenerales.giNIVDIST > 0, gParametrosGenerales.giNIVDIST, -1)
    chkRegGest(3).Value = IIf(gParametrosGenerales.gbOBLPP, vbChecked, vbUnchecked)
        
    If gParametrosGenerales.giNIVPP > 0 And gParametrosGenerales.gbUsarPres1 Then
        cmbOblDist(1).ListIndex = gParametrosGenerales.giNIVPP
    Else
        cmbOblDist(1).ListIndex = -1
        If Not gParametrosGenerales.gbUsarPres1 Then
           chkRegGest(3).Visible = False
           gParametrosGenerales.giNIVPP = 0
        End If
    End If
    
    chkRegGest(4).Value = IIf(gParametrosGenerales.gbOBLPC, vbChecked, vbUnchecked)
        
    If gParametrosGenerales.giNIVPC > 0 And gParametrosGenerales.gbUsarPres2 Then
        cmbOblDist(2).ListIndex = gParametrosGenerales.giNIVPC
    Else
        cmbOblDist(2).ListIndex = -1
        If Not gParametrosGenerales.gbUsarPres2 Then
           chkRegGest(4).Visible = False
           gParametrosGenerales.giNIVPC = 0
        End If
    End If

    chkRegGest(5).Value = IIf(gParametrosGenerales.gbOBLPres3, vbChecked, vbUnchecked)
        
    If gParametrosGenerales.giNIVPres3 > 0 And gParametrosGenerales.gbUsarPres3 Then
        cmbOblDist(3).ListIndex = gParametrosGenerales.giNIVPres3
    Else
        cmbOblDist(3).ListIndex = -1
        If Not gParametrosGenerales.gbUsarPres3 Then
           chkRegGest(5).Visible = False
           gParametrosGenerales.giNIVPres3 = 0
        End If
    End If
    
    chkRegGest(6).Value = IIf(gParametrosGenerales.gbOBLPres4, vbChecked, vbUnchecked)
        
    If gParametrosGenerales.giNIVPres4 > 0 And gParametrosGenerales.gbUsarPres4 Then
        cmbOblDist(4).ListIndex = gParametrosGenerales.giNIVPres4
    Else
        cmbOblDist(4).ListIndex = -1
        If Not gParametrosGenerales.gbUsarPres4 Then
           chkRegGest(6).Visible = False
           gParametrosGenerales.giNIVPres4 = 0
        End If
    End If
    
    chkRegGest(8).Value = IIf(gParametrosGenerales.gbPermAdjSinHom, vbChecked, vbUnchecked)
    chkRegGest(10).Value = IIf(gParametrosGenerales.gbUsarPonderacion, vbChecked, vbUnchecked)
    chkRegGest(1).Value = IIf(gParametrosGenerales.gbPermAbrirItemsSinArtCod, vbChecked, vbUnchecked)
    chkRegGest(7).Value = IIf(gParametrosGenerales.gbOblProveEqp, vbUnchecked, vbChecked)
    chkRegGest(12).Value = IIf(gParametrosGenerales.gbSeleccionPositiva, vbChecked, vbUnchecked)
    
    sdbcRolCod.Text = gParametrosGenerales.gsROLDEF
    sdbcRolCod_Validate False
    
    sdbcPagCod.Text = gParametrosGenerales.gsPAGDEF
    sdbcPagCod_Validate False
        
    sdbcEstCod(0).Text = gParametrosGenerales.gsESTINI
    sdbcEstCod_Validate 0, False
        
    sdbcEstCod(1).Text = gParametrosGenerales.gsESTWEB
    sdbcEstCod_Validate 1, False
        
    txtPlantilla(0) = gParametrosGenerales.gsPETOFEDOT
    txtPlantilla(1) = gParametrosGenerales.gsPETOFECARTADOT
    txtPlantilla(2) = gParametrosGenerales.gsPETOFEMAILDOT
    txtPlantilla(3) = gParametrosGenerales.gsPETOFEWEBDOT
    txtPlantilla(4) = gParametrosGenerales.gsOBJDOT
    txtPlantilla(5) = gParametrosGenerales.gsOBJCARTADOT
    txtPlantilla(6) = gParametrosGenerales.gsOBJMAILDOT
    txtPlantilla(7) = gParametrosGenerales.gsOBJWEBDOT
        
    chkPlantAdj(0).Value = IIf(gParametrosGenerales.gbAdjOrden, vbChecked, vbUnchecked)
    chkPlantAdj(1).Value = IIf(gParametrosGenerales.gbAdjNotif, vbChecked, vbUnchecked)
    
    txtPlantilla(8) = gParametrosGenerales.gsadjDot
    txtPlantilla(38) = gParametrosGenerales.gsADJNOTIFDOT
    txtPlantilla(9) = gParametrosGenerales.gsAdjCartaDot
    txtPlantilla(10) = gParametrosGenerales.gsAdjMailDot
    txtPlantilla(11) = gParametrosGenerales.gsAdjNoCartaDot
    txtPlantilla(12) = gParametrosGenerales.gsAdjNoMailDot
    
    txtPlantilla(13) = gParametrosGenerales.gsHOJAADJDETPROCEDOT
    txtPlantilla(14) = gParametrosGenerales.gsHOJAADJDIRDOT
    txtPlantilla(15) = gParametrosGenerales.gsCONVDOT
    Select Case UCase(basPublic.gParametrosInstalacion.gIdioma)
        Case "SPA"
            txtPlantilla(16) = gParametrosGenerales.gsCONVMAILHTMLSPA
            txtPlantilla(43) = gParametrosGenerales.gsCONVMAILTEXTSPA
            txtConvMailSubject = gParametrosGenerales.gsCONVMAILSUBJECTSPA
        Case "ENG"
            txtPlantilla(16) = gParametrosGenerales.gsCONVMAILHTMLENG
            txtPlantilla(43) = gParametrosGenerales.gsCONVMAILTEXTENG
            txtConvMailSubject = gParametrosGenerales.gsCONVMAILSUBJECTENG
        Case "GER"
            txtPlantilla(16) = gParametrosGenerales.gsCONVMAILHTMLGER
            txtPlantilla(43) = gParametrosGenerales.gsCONVMAILTEXTGER
            txtConvMailSubject = gParametrosGenerales.gsCONVMAILSUBJECTGER
        Case "FRA"
            txtPlantilla(16) = gParametrosGenerales.gsCONVMAILHTMLFRA
            txtPlantilla(43) = gParametrosGenerales.gsCONVMAILTEXTFRA
            txtConvMailSubject = gParametrosGenerales.gsCONVMAILSUBJECTFRA
    End Select
    
    CargarDatosPlantillas
    
    txtPlantilla(19) = gParametrosGenerales.gsCOMPARATIVADOT
    txtPlantilla(37) = gParametrosGenerales.gsCOMPARATIVAITEMDOT
    txtPlantilla(42) = gParametrosGenerales.gsComparativaQA
    
    txtMailSubject(0) = gParametrosGenerales.gsPetOfeMailSubject
    txtMailSubject(1) = gParametrosGenerales.gsObjMailSubject
    txtMailSubject(2) = gParametrosGenerales.gsAdjMailSubject
    txtMailSubject(3) = gParametrosGenerales.gsAdjNoMailSubject
    
    txtURL = gParametrosGenerales.gsURLSUMMIT
    
    cmbDiaSemana.ListIndex = gParametrosGenerales.giDIAREU - 1
    cmbNumSemanas.ListIndex = (gParametrosGenerales.giPERIODICIREU \ 7) - 1
    txtHoraReu = Format(gParametrosGenerales.gdCOMIENZOREU, "short time")
    txtTiempoReu = gParametrosGenerales.giTIEMPOREU
    
    cmbCriterio(0).clear
    cmbCriterio(1).clear
    
    For i = 0 To 1
        With cmbCriterio(i)
            For j = 0 To 4
                .AddItem Criterios(j)
            Next j
        End With
    Next i
        
    cmbCriterio(0).ListIndex = gParametrosGenerales.giCRITEORDREU1 - 1
    For i = 0 To 4
        If cmbCriterio(1).List(i) = Criterios(gParametrosGenerales.giCRITEORDREU2 - 1) Then
            cmbCriterio(1).ListIndex = i
        End If
    Next i
    
    If gParametrosGenerales.giLOGPREBLOQ > 0 Then
        chkActivar(0).Value = vbChecked
        sldLogPreBloq.Enabled = True
        sldLogPreBloq.Value = gParametrosGenerales.giLOGPREBLOQ
        sParte = DividirFrase(sCap(53))
        lblIntentos = sParte(1) + CStr(sldLogPreBloq.Value) + sParte(2)
    Else
        chkActivar(0).Value = vbUnchecked
        sldLogPreBloq.Value = 3
        sldLogPreBloq.Enabled = False
        lblIntentos = sCap(34)
    End If
    'Nuevos parametros de seguridad
    txtGrupoDefecto(3).Text = gParametrosGenerales.giHIST_PWD
    txtGrupoDefecto(4).Text = gParametrosGenerales.giEDAD_MIN_PWD
    txtGrupoDefecto(5).Text = gParametrosGenerales.giEDAD_MAX_PWD
    optRutaTS(2).Value = (gParametrosGenerales.gbCOMPLEJIDAD_PWD)
    optRutaTS(3).Value = Not (gParametrosGenerales.gbCOMPLEJIDAD_PWD)
    txtGrupoDefecto(6).Text = gParametrosGenerales.giMIN_SIZE_PWD
    
    chkActivar(1).Value = IIf(gParametrosGenerales.gbACTIVLOG, vbChecked, vbUnchecked)
        
    tabCONFGEN.Tab = 0
    
    chkSoloConvResp.Value = IIf(basParametros.gParametrosGenerales.gbCONVSOLORESP, vbChecked, vbUnchecked)
    chkRegGest(1).Value = IIf(gParametrosGenerales.gbPermAbrirItemsSinArtCod, vbChecked, vbUnchecked)
    chkRegGest(8).Value = IIf(gParametrosGenerales.gbPermAdjSinHom, vbChecked, vbUnchecked)
        
    txtDenPres(0) = gParametrosGenerales.gsSingPres1
    txtDenPres(1) = gParametrosGenerales.gsPlurPres1
    lblPresupuesto(3).caption = ConPresupuRow
    sdbcConPres.Columns("ID").Value = 1
    sdbcConPres.Columns(0).Value = ConPresupuRow
    sdbcConPres.Bookmark = 0
    
    If gParametrosGenerales.gbUsarPres1 Then
        chkConPres.Value = vbChecked
        EstUsarPres(0) = True
    Else
        chkConPres.Value = vbUnchecked
        EstUsarPres(0) = False
    End If
    
    EstUsarPres(1) = (gParametrosGenerales.gbUsarPres2)
    EstUsarPres(2) = (gParametrosGenerales.gbUsarPres3)
    EstUsarPres(3) = (gParametrosGenerales.gbUsarPres4)
    
    If gParametrosGenerales.gsRPTPATH <> "" Then
        dirRPT(0).Path = gParametrosGenerales.gsRPTPATH
        txtRuta(0).Text = gParametrosGenerales.gsRPTPATH
    End If
    
    If m_bMostrarCarpetas Then
        If gParametrosGenerales.gsPathSolicitudes <> "" Then
            dirRPT(1).Path = gParametrosGenerales.gsPathSolicitudes
            txtRuta(1).Text = gParametrosGenerales.gsPathSolicitudes
        End If
    End If
    
    If gParametrosGenerales.gsTSHomeFolderRutaLocal <> "" Then
        optRutaTS(0).Value = True
        txtRuta(2).Text = gParametrosGenerales.gsTSHomeFolderRutaLocal
        cmbTSUnidad.ListIndex = -1
    ElseIf gParametrosGenerales.gsTSHomeFolderRutaUnidad <> "" Then
        optRutaTS(1).Value = True
        txtRuta(3).Text = gParametrosGenerales.gsTSHomeFolderRutaUnidad
        cmbTSUnidad.ListIndex = BuscarUnidad(gParametrosGenerales.gsTSHomeFolderUnidad & ":")
    End If

    txtLongCabComp(0).Text = gParametrosGenerales.giLongCabComp
    txtLongCabComp(1).Text = gParametrosGenerales.giLongCabCompItem
    txtLongCabComp(2).Text = gParametrosGenerales.giLongCabCompQA
    
    sdbcIdiomas(0).Text = oIdiomas.Item(CStr(basParametros.gParametrosGenerales.gIdioma)).Den
    sdbcIdiomas(0).Bookmark = idiIdiomasRow
    
    If gParametrosGenerales.giINSTWEB = ConPortal Then
        sdbcIdiomas(1).Text = oIdiomasPortal.Item(CStr(basParametros.gParametrosGenerales.gIdiomaPortal)).Den
        sdbcIdiomas(1).Bookmark = idiIdiomasPortalRow
    End If
    
    'Zona horaria
    Set dcListaZonasHorarias = ObtenerZonasHorarias
    sdbcIdiomas(2).Text = dcListaZonasHorarias.Item(basParametros.gParametrosGenerales.gvTimeZone)
    sdbcIdiomas(2).Columns(0).Value = dcListaZonasHorarias.Item(basParametros.gParametrosGenerales.gvTimeZone)
    sdbcIdiomas(2).Columns(1).Value = basParametros.gParametrosGenerales.gvTimeZone
    Set dcListaZonasHorarias = Nothing
        
    sdbcIdioma(1).Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdioma(0).Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdioma(2).Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdioma(3).Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdioma(6).Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdioma(7).Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    sdbcIdioma(8).Text = oIdiomas.Item(CStr(basPublic.gParametrosInstalacion.gIdioma)).Den
    
    sdbcIdioma(1).Bookmark = idiUORow
    sdbcIdioma(0).Bookmark = idiMatRow
    sdbcIdioma(2).Bookmark = idiPresRow
    sdbcIdioma(3).Bookmark = idiOtrosRow
    sdbcIdioma(6).Bookmark = idiConvocatoriaRow
    sdbcIdioma(7).Bookmark = idiConvocatoriaSubjectRow
    sdbcIdioma(8).Bookmark = idiRelacionRow
    
    sdbcConPres.Text = ConPresupuRow
    
    txtSMTP(0).Text = ""
    txtSMTP(1).Text = ""
    txtSMTP(2).Text = ""
    txtSMTP(3).Text = ""
    txtSMTP(4).Text = ""
    txtSMTP(5).Text = ""
    chkCorreo(2).Value = vbUnchecked
    Select Case gParametrosGenerales.giMail
        Case 0
            chkCorreo(0).Value = vbUnchecked
            chkCorreo(1).Value = vbUnchecked
            fraOpciones(1).Enabled = False
            fraOpciones(2).Enabled = False
        Case 3 'MS
            chkCorreo(0).Value = vbChecked
            fraOpciones(1).Enabled = True
            optMapi(0).Value = True
        Case 2 'Lotus
            chkCorreo(0).Value = vbChecked
            fraOpciones(1).Enabled = True
            optMapi(1).Value = True
        Case 1 'SMTP
            chkCorreo(1).Value = vbChecked
            fraOpciones(2).Enabled = True
            txtSMTP(0).Text = gParametrosGenerales.gsSMTPServer
            txtSMTP(1).Text = gParametrosGenerales.giSMTPPort
            chkCorreo(2).Value = IIf(gParametrosGenerales.gbSMTPSSL, vbChecked, vbUnchecked)
            txtSMTP(4).Text = gParametrosGenerales.gsSMTPCuentaMail
            txtSMTP(2).Text = gParametrosGenerales.gsSMTPUser
            txtSMTP(3).Text = gParametrosGenerales.gsSMTPPwd
            txtSMTP(5).Text = gParametrosGenerales.gsSMTPDominio
            
            mbExitPorBasica = True
                   
            Select Case gParametrosGenerales.giSMTPAutent
                Case 0 'anonima
                    optAutenticacion(2).Value = True
                    txtSMTP(4).Locked = False
                Case 1 'basica
                    optAutenticacion(0).Value = True
                    txtSMTP(4).Locked = False
                    txtSMTP(3).Locked = False
                Case 2 'windows
                    optAutenticacion(1).Value = True
                    txtSMTP(4).Locked = False
                    txtSMTP(2).Locked = False
                    txtSMTP(3).Locked = False
                    txtSMTP(5).Locked = False
            End Select
            
            mbExitPorBasica = False
    End Select
    
    chkCorreo(4).Value = IIf(gParametrosGenerales.gbMostrarMail = True, vbChecked, vbUnchecked)
    chkCorreo(5).Value = IIf(gParametrosGenerales.gbAcuseRecibo = True, vbChecked, vbUnchecked)
    
    If gParametrosGenerales.gbTipoMail Then
        optFormatoEMail(0).Value = True
        optFormatoEMail(1).Value = False
    Else
        optFormatoEMail(1).Value = True
        optFormatoEMail(0).Value = False
    End If
    
    Dim bm As Variant
   
    sdbcAntigOfer.MoveFirst
    
    For i = 0 To sdbcAntigOfer.Rows - 1
        bm = sdbcAntigOfer.GetBookmark(i)
        If gParametrosGenerales.giBuzonPeriodoDef = sdbcAntigOfer.Columns(1).Value Then
            sdbcAntigOfer.Text = sdbcAntigOfer.Columns(0).Value
            Exit For
        End If
        sdbcAntigOfer.MoveNext
    Next i
                  
    Select Case gParametrosGenerales.giBuzonOfeLeidas
        Case 0:     opNoLeidas.Value = True
        Case 1:     opLeidas.Value = True
        Case 2:     opLeerTodas.Value = True
    End Select
    
    txtMAXADJUN.Text = gParametrosGenerales.glMAXADJUN / 1024
    
    Select Case gParametrosGenerales.giAnyadirEspecArticulo
        Case 1:     opAdjAutomat(0).Value = True
        Case 2:     opAdjAutomat(1).Value = True
        Case 3:     opAdjAutomat(2).Value = True
    End Select
    
    chkCorreo(6).Value = IIf(gParametrosGenerales.gbAvisoPortalCompResp = True, vbChecked, vbUnchecked)
    chkCorreo(7).Value = IIf(gParametrosGenerales.gbAvisoPortalCompAsign = True, vbChecked, vbUnchecked)
    
    chkRegGest(9).Value = IIf(gParametrosGenerales.gbOblProveMat, vbChecked, vbUnchecked)

    Me.ctrlConfGenAvisos.AvisoAdjSoloSiResp = gParametrosGenerales.gbAvisoAdj
    Me.ctrlConfGenAvisos.AvisoVisorGSSoloSiResp = gParametrosGenerales.gbAvisoDespublica
    Me.ctrlConfGenAvisos.AntelacionAdjudicacion = gParametrosGenerales.giAvisoAdj
    Me.ctrlConfGenAvisos.AntelacionVisorGS = gParametrosGenerales.giAvisoDespublica
            
    If Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosAprov Then
        tabCONFGEN.TabVisible(5) = False
    Else
        tabCONFGEN.TabVisible(5) = True
    End If
    
    With gParametrosGenerales
        txtGrupoDefecto(0).Text = .gsGrupoCod
        txtGrupoDefecto(1).Text = .gsGrupoDen
        txtGrupoDefecto(2).Text = .gsGrupoDescrip
        fraPonderacion.Enabled = True
        
        chkPonderacion(0).Value = IIf(.gCPPonderar = True, vbChecked, vbUnchecked)
        Me.chkUnSoloPedido.Value = IIf(.gbUnSoloPedido = True, vbChecked, vbUnchecked)
        
        fraPonderacion.Visible = (.gbUsarPonderacion = True)
        
        chkSolOfe(1).Value = IIf(.gCPAlternativasPrec = True, vbChecked, vbUnchecked)
        chkSolOfe(6).Value = IIf(.gCPCambiarMonOferta = True, vbUnchecked, vbChecked)
        chkSolOfe(2).Value = IIf(.gCPSolCantMax = True, vbChecked, vbUnchecked)
        
        If .gbSubasta Then
            chkSolOfe(0) = IIf(.gCPSubasta = True, vbChecked, vbUnchecked)
        Else
            chkSolOfe(0).Visible = False
            cmdConfigSubasta.Visible = False
        End If
        
        chkSolOfe(3).Value = IIf(.gCPAdjunOfe = True, vbChecked, vbUnchecked)
        chkSolOfe(4).Value = IIf(.gCPAdjunGrupo = True, vbChecked, vbUnchecked)
        chkSolOfe(5).Value = IIf(.gCPAdjunItem = True, vbChecked, vbUnchecked)
           
        Me.chkNoPublicarFinSum.Value = IIf(.gCPNoPublicarFinSum = True, vbChecked, vbUnchecked)
        
        txtPlantilla(36).Text = .gsPETSUBDOT
        txtPlantilla(35).Text = .gsPETSUBCARTADOT
        txtPlantilla(34).Text = .gsPETSUBMAILDOT
        txtPlantilla(33).Text = .gsPETSUBWEBDOT
        txtMailSubject(8).Text = .gsPETSUBMAILSUBJECT
        
        If .gbRecordatorioInicioSubasta Then
            chkRecordatorio.Value = vbChecked
            FormatearTiempoAntelacionCombo .glSubastaTiempAnt, lValor, bytUT
            m_bComboTAntelacion = True
            cboTAntelacion.Text = lValor & " " & sFormatoTextoTAntelacion(bytUT)
            m_bComboTAntelacion = False
        Else
            chkRecordatorio.Value = vbUnchecked
            cboTAntelacion.clear
        End If
        
        chkInicio.Value = IIf(.gbInicioSubasta, vbChecked, vbUnchecked)
        chkCierre.Value = IIf(.gbCierreSubasta, vbChecked, vbUnchecked)
        
        'chkAdminPub.Value = IIf(.gCPProcesoAdminPub = True, vbChecked, vbUnchecked)
        chkPonderacion(1).Value = IIf(.gCPProcesoAdminPub = True, vbChecked, vbUnchecked)
        
        If .gbPresupuestoPlanificado = True Then
            opAdjAutomat(4).Value = True
        Else
            opAdjAutomat(3).Value = True
        End If
    End With
    
    If gParametrosGenerales.giINSTWEB = SinWeb Or Not gParametrosGenerales.gbSubasta Then SSTabHabSub.TabVisible(1) = False
        
    If gParametrosGenerales.gbActivarCodProveErp Then
        i = 1
        Dim ogroup As SSDataWidgets_B.Group
        Dim oColumn As SSDataWidgets_B.Column
        Dim oLiterales As CLiterales
        Dim oLiteral As CLiteral
        Dim k As Integer
        Dim bCrearGrupos As Boolean
        
        ReDim arrERP(0 To 3, 0 To oIdiomas.Count - 1)
        Dim sFila As String
        
        If sdbgCamposERP.Groups.Count = 1 Then bCrearGrupos = True
        
        For Each oIdioma In oIdiomas
            If bCrearGrupos Then
                sdbgCamposERP.Groups.Add i
                Set ogroup = sdbgCamposERP.Groups(i)
                ogroup.caption = oIdioma.Cod
                ogroup.Columns.Add 0
                Set oColumn = ogroup.Columns(0)
                oColumn.caption = ""
            End If
            Set oLiterales = oGestorParametros.DevolverLiterales(32, 35, oIdioma.Cod)
            j = 0
            For Each oLiteral In oLiterales
                arrERP(j, i - 1) = oLiteral.Den
                j = j + 1
            Next
            i = i + 1
            Set oLiterales = Nothing
        Next
        
        sdbgCamposERP.RemoveAll
        For j = 0 To 3
            Select Case j
                Case 0
                    sFila = IIf(basParametros.gParametrosGenerales.gbCampo1ERPAct, "1", "0") & Chr(m_lSeparador) & m_sIdiCampo & " 1"
                Case 1
                    sFila = IIf(basParametros.gParametrosGenerales.gbCampo2ERPAct, "1", "0") & Chr(m_lSeparador) & m_sIdiCampo & " 2"
                Case 2
                    sFila = IIf(basParametros.gParametrosGenerales.gbCampo3ERPAct, "1", "0") & Chr(m_lSeparador) & m_sIdiCampo & " 3"
                Case 3
                    sFila = IIf(basParametros.gParametrosGenerales.gbCampo4ERPAct, "1", "0") & Chr(m_lSeparador) & m_sIdiCampo & " 4"
            End Select
            
            For k = 0 To UBound(arrERP, 2)
                sFila = sFila & Chr(m_lSeparador) & arrERP(j, k)
            Next k
            sdbgCamposERP.AddItem sFila
        Next j
    Else
        Label5.Visible = False
        sdbgCamposERP.Visible = False
        
        lblRel(0).Left = Label5.Left
        lblRel(0).Top = Label5.Top
        lblRel(2).Top = lblRel(0).Top + 450
        lblRel(1).Top = lblRel(2).Top + 120
        sdbcIdioma(8).Top = lblRel(1).Top
        lblRel(3).Top = lblRel(2).Top + 360
        picNomRel.Top = lblRel(2).Top - 120
        fraCodERP.Height = picNomRel.Top + picNomRel.Height + 100
    End If
        
    
    Me.ctrlConfGenAvisos.AvisoProveDespub = gParametrosGenerales.gbAvisoProveDespublicar
    Me.ctrlConfGenAvisos.AvisoNoHanOfertado = gParametrosGenerales.gbAvisoProveSoloNoOfertado
    Me.ctrlConfGenAvisos.AvisoNoVanOfertar = gParametrosGenerales.gbExcluirAProveNoOfertan
    Me.ctrlConfGenAvisos.AntelacionDespublicacion = NullToDbl0(gParametrosGenerales.giAntelacionAvisoProve)
    Me.ctrlConfGenAvisos.PlantillaDespub(0) = gParametrosGenerales.gsOFENotifAvisoDespubHTML
    Me.ctrlConfGenAvisos.PlantillaDespub(1) = gParametrosGenerales.gsOFENotifAvisoDespubTXT
    Me.ctrlConfGenAvisos.AsuntoAvisoDespub = gParametrosGenerales.gsOFENotifAvisoDespubSubject
    
    'Avisos a los participantes
    ctrlConfGenAvisos.EnviarEmailApertura = gParametrosGenerales.gbActApeMail
    ctrlConfGenAvisos.EnviarEmailPreAdj = gParametrosGenerales.gbActPreAdjMail
    ctrlConfGenAvisos.EnviarEmailAdj = gParametrosGenerales.gbActAdjMail
    ctrlConfGenAvisos.EnviarEmailAnulacion = gParametrosGenerales.gbActAnulacionMail
    
    If m_bMostrarSolicitud Then
        If gParametrosGenerales.gbSolicProcAdjDir = True Then
            optGenerarProc(0).Value = True
        Else
            optGenerarProc(1).Value = True
        End If
    End If
    
    If m_bMostrarSolicitud Then
        If Not IsNull(gParametrosGenerales.gvImporteSolicProcAdjDir) Then
            chkAdjDirImp.Value = vbChecked
            txtAdjDirImp.Text = gParametrosGenerales.gvImporteSolicProcAdjDir
        Else
            chkAdjDirImp.Value = vbUnchecked
            txtAdjDirImp.Text = ""
        End If
    End If
    
    If m_bMostrarSolicitud Then
        sdbcPeriodoSolic.MoveFirst
        For i = 0 To sdbcPeriodoSolic.Rows - 1
            bm = sdbcPeriodoSolic.GetBookmark(i)
            If gParametrosGenerales.giValidezSolicOfe = sdbcPeriodoSolic.Columns(1).Value Then
                sdbcPeriodoSolic.Text = sdbcPeriodoSolic.Columns(0).Value
                Exit For
            End If
            sdbcPeriodoSolic.MoveNext
        Next i
    End If
     
    ctlConfPed.MostrarParametros gParametrosGenerales, iNumPres, idiAprovRow, idiRecepRow, idiPedAbiertoRow

    chkRegGest(23).Value = IIf(gParametrosGenerales.gbOBLUnidadMedida, vbChecked, vbUnchecked)
    chkRegGest(25).Value = IIf(gParametrosGenerales.gbControlCambiosFechaSuministro, vbChecked, vbUnchecked)
    chkRegGest(28).Value = IIf(gParametrosGenerales.gbPermitSuminRetroactivo, vbChecked, vbUnchecked)
      
    optCalcLinBase(gParametrosGenerales.giCalculoLineaBase).Value = True
    txtNumOfe.Text = gParametrosGenerales.gvNumMejoresOfertas
    Exit Sub
Error:
    If err.Number = 76 Or err.Number = 68 Then Resume Next
End Sub

Private Sub cboTAntelacion_Change()
    If Not m_bComboTAntelacion Then
        m_bComboTAntelacion = True
        cboTAntelacion.Text = ""
        m_bComboTAntelacion = False
    End If
End Sub

Private Sub cboTAntelacion_DropDown()
    Dim i As Integer
    
    cboTAntelacion.clear
    For i = 5 To 55 Step 5
        cboTAntelacion.AddItem i & " " & sFormatoTextoTAntelacion(0)
    Next i
    cboTAntelacion.AddItem "1 " & sFormatoTextoTAntelacion(1)
    For i = 2 To 23
        cboTAntelacion.AddItem i & " " & sFormatoTextoTAntelacion(2)
    Next i
    cboTAntelacion.AddItem "1 " & sFormatoTextoTAntelacion(3)
    For i = 2 To 6
        cboTAntelacion.AddItem i & " " & sFormatoTextoTAntelacion(4)
    Next i
    cboTAntelacion.AddItem "1 " & sFormatoTextoTAntelacion(5)
    For i = 2 To 3
        cboTAntelacion.AddItem i & " " & sFormatoTextoTAntelacion(6)
    Next i
    cboTAntelacion.AddItem "1 " & sFormatoTextoTAntelacion(7)
End Sub

Private Sub chkActivar_Click(Index As Integer)
If Index = 0 Then
    If chkActivar(0).Value = vbChecked Then
            sldLogPreBloq.Enabled = True
            If gParametrosGenerales.giLOGPREBLOQ > 0 Then
                sldLogPreBloq.Value = gParametrosGenerales.giLOGPREBLOQ
            Else
                sldLogPreBloq.Value = 10
            End If
            sParte = DividirFrase(sCap(53))
            lblIntentos = sParte(1) + CStr(sldLogPreBloq.Value) + sParte(2)
    Else
            sldLogPreBloq.Value = 3
            sldLogPreBloq.Enabled = False
            lblIntentos = sCap(34)
    End If
End If

End Sub

Private Sub chkPonderacion_Click(Index As Integer)
    Dim i As Integer
    Dim vbm As Variant
    
    'If chkAdminPub.Value = vbChecked Then
    If chkPonderacion(1).Value = vbChecked Then
    
        'Desactiva la recepci�n en modo subasta
        chkSolOfe(0).Value = vbUnchecked
        chkSolOfe(0).Visible = False
        
        'Desactiva Pedir alternativas de precio
        chkSolOfe(1).Value = vbUnchecked
        chkSolOfe(1).Visible = False
        
        'Activa ofertar en la moneda del proceso
        chkSolOfe(6).Value = vbChecked
        chkSolOfe(6).Visible = False
        
        'Desactiva las cantidades m�ximas
        chkSolOfe(2).Value = vbUnchecked
        chkSolOfe(2).Visible = False
        
        'permitir adjuntar archivos a nivel de proceso
        chkSolOfe(3).Value = vbUnchecked
        chkSolOfe(3).Visible = False
        
        chkSolOfe(4).Top = 0
        chkSolOfe(5).Top = 270
        Me.chkNoPublicarFinSum.Top = 540
        
        
        'Elimina los �mbitos de datos correspondientes de la grid
        i = sdbgAmbitoDatos.Rows - 1
        While i >= 0
            vbm = sdbgAmbitoDatos.AddItemBookmark(i)
        
            Select Case sdbgAmbitoDatos.Columns("Fila").CellValue(vbm)
                Case 0
                    'Destino
                    sdbgAmbitoDatos.RemoveItem (sdbgAmbitoDatos.AddItemRowIndex(vbm))
                Case 1
                    'forma de pago
                    sdbgAmbitoDatos.RemoveItem (sdbgAmbitoDatos.AddItemRowIndex(vbm))
                Case 3
                    'Proveedor actual
                    sdbgAmbitoDatos.RemoveItem (sdbgAmbitoDatos.AddItemRowIndex(vbm))
                Case 10
                    'Solicitud de compra
                    sdbgAmbitoDatos.RemoveItem (sdbgAmbitoDatos.AddItemRowIndex(vbm))
            End Select
            i = i - 1
        Wend
                        
    Else
        'Vuelve a hacer visibles las checks
        chkSolOfe(0).Visible = True
        chkSolOfe(1).Visible = True
        chkSolOfe(6).Visible = True
        chkSolOfe(2).Visible = True
        chkSolOfe(3).Visible = True
        
        chkSolOfe(4).Top = 1365
        chkSolOfe(5).Top = 1635
        Me.chkNoPublicarFinSum.Top = 1860
        
        'Carga otra vez la grid
        CargarAmbitoDatos
    End If
End Sub

''' <summary>
''' Dependiendo de si se pulsa Mapi o Smtp, activa unos campos u otros
''' </summary>
''' <param name="Index">Cual de los checks se ha pulsado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub chkCorreo_Click(Index As Integer)
Select Case Index
Case 0
    If chkCorreo(0).Value = vbChecked Then
        chkCorreo(1).Value = vbUnchecked
        fraOpciones(1).Enabled = True
        optMapi(0).Value = True
    Else
        fraOpciones(1).Enabled = False
        optMapi(0).Value = False
        optMapi(1).Value = False
    End If
Case 1 'Stmp
    If chkCorreo(1).Value = vbChecked Then
        chkCorreo(0).Value = vbUnchecked
        fraOpciones(2).Enabled = True
    Else
        fraOpciones(2).Enabled = False
        txtSMTP(0).Text = ""
        txtSMTP(1).Text = ""
        chkCorreo(2).Value = vbUnchecked
    End If
End Select
End Sub

Private Sub chkRecordatorio_Click()
    Dim lValor As Long
    Dim bytUT As Byte
    
    Select Case chkRecordatorio.Value
        Case vbChecked
            picModSub(4).Enabled = True
            FormatearTiempoAntelacionCombo gParametrosGenerales.glSubastaTiempAnt, lValor, bytUT
            m_bComboTAntelacion = True
            cboTAntelacion.Text = lValor & " " & sFormatoTextoTAntelacion(bytUT)
            m_bComboTAntelacion = False
        Case vbUnchecked
            picModSub(4).Enabled = False
            cboTAntelacion.clear
    End Select
End Sub


Private Sub chkConPres_validate(Cancel As Boolean)
        Select Case sdbcConPres.Columns("ID").Value
        Case 1:
             If chkConPres.Value = vbChecked Then
                EstUsarPres(0) = True
                MDI.mnuInformes(2).Visible = True
                MDI.mnuPresAnu(1).Visible = True
                If MDI.mnuPresAnuCab.Visible = False Then
                    MDI.mnuPresAnuCab.Visible = True
                    MDI.mnuPresAnu(2).Visible = False
                End If
                MDI.mnuInfApl(3).Visible = True
             Else
                EstUsarPres(0) = False
                MDI.mnuInformes(2).Visible = False
                If MDI.mnuPresAnu(2).Visible = False Then
                   MDI.mnuPresAnuCab.Visible = False
                Else
                    MDI.mnuPresAnu(1).Visible = False
                End If
                MDI.mnuInfApl(3).Visible = False
             End If
             If chkConPres.Value = vbChecked Then
                sdbcConPres.Columns("USAR").Value = True
             Else
                sdbcConPres.Columns("USAR").Value = False
             End If
        Case 2:
             If chkConPres.Value = vbChecked Then
                EstUsarPres(1) = True
                MDI.mnuInformes(3).Visible = True
                MDI.mnuPresAnu(2).Visible = True
                If MDI.mnuPresAnuCab.Visible = False Then
                    MDI.mnuPresAnuCab.Visible = True
                    MDI.mnuPresAnu(1).Visible = False
                End If
                
                MDI.mnuInfApl(4).Visible = True
             Else
                EstUsarPres(1) = False
                MDI.mnuInformes(3).Visible = False
                If MDI.mnuPresAnu(1).Visible = False Then
                   MDI.mnuPresAnuCab.Visible = False
                Else
                    MDI.mnuPresAnu(2).Visible = False
                End If
                MDI.mnuInfApl(4).Visible = False
             End If
             If chkConPres.Value = vbChecked Then
                sdbcConPres.Columns("USAR").Value = True
             Else
                sdbcConPres.Columns("USAR").Value = False
             End If
        Case 3:
            If chkConPres.Value = vbChecked Then
                 EstUsarPres(2) = True
                 MDI.mnuConcepto3.Visible = True
                 MDI.mnuInfConcep3.Visible = True
                 MDI.mnuInfApl(5).Visible = True
             Else
                 EstUsarPres(2) = False
                 MDI.mnuConcepto3.Visible = False
                 MDI.mnuInfConcep3.Visible = False
                 MDI.mnuInfApl(5).Visible = False
             End If
             If chkConPres.Value = vbChecked Then
                sdbcConPres.Columns("USAR").Value = True
             Else
                sdbcConPres.Columns("USAR").Value = False
             End If
        Case 4:
             If chkConPres.Value = vbChecked Then
                EstUsarPres(3) = True
                MDI.mnuConcepto4.Visible = True
                MDI.mnuInfConcep4.Visible = True
                MDI.mnuInfApl(6).Visible = True
             Else
                EstUsarPres(3) = False
                MDI.mnuConcepto4.Visible = False
                MDI.mnuInfConcep4.Visible = False
                MDI.mnuInfApl(6).Visible = False
             End If
             If chkConPres.Value = vbChecked Then
                sdbcConPres.Columns("USAR").Value = True
             Else
                sdbcConPres.Columns("USAR").Value = False
             End If
        End Select
End Sub

Private Sub chkRegGest_Click(Index As Integer)

With gParametrosGenerales
    Select Case Index
        Case 0
            If chkRegGest(0).Value = vbUnchecked Then
                txtVolMaxAdjDir = 0
                txtVolMaxAdjDir.Enabled = False
            Else
                txtVolMaxAdjDir.Enabled = True
                If .gdVOL_MAX_ADJ_DIR Then
                    txtVolMaxAdjDir = Format(.gdVOL_MAX_ADJ_DIR, "standard")
                End If
            End If
        
        Case 3
            'Si se obliga a distribuir un PRESUP la fila de la grid de �mbito tiene que estar seleccionada
            If (chkRegGest(3).Value = vbChecked) Then
                sdbgAmbitoDatos.MoveFirst
                sdbgAmbitoDatos.MoveRecords 6
                
                If sdbgAmbitoDatos.Columns(0).Value = "0" Then
                    sdbgAmbitoDatos.Columns(0).Value = "-1"
                    sdbgAmbitoDatos.Columns("Item").Value = "-1"
                    sdbgAmbitoDatos.Refresh
                End If
            End If
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.chkRGest_SetValue 13, IIf(chkRegGest(3).Value = vbChecked, True, False)

        Case 4
            'Si se obliga a distribuir un PRESUP la fila de la grid de �mbito tiene que estar seleccionada
            If (chkRegGest(4).Value = vbChecked) Then
                sdbgAmbitoDatos.MoveFirst
                sdbgAmbitoDatos.MoveRecords 7
                
                If sdbgAmbitoDatos.Columns(0).Value = "0" Then
                    sdbgAmbitoDatos.Columns(0).Value = "-1"
                    sdbgAmbitoDatos.Columns("Item").Value = "-1"
                    sdbgAmbitoDatos.Refresh
                End If
            End If
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.chkRGest_SetValue 14, IIf(chkRegGest(4).Value = vbChecked, True, False)
        
        Case 5
            'Si se obliga a distribuir un PRESUP la fila de la grid de �mbito tiene que estar seleccionada
            If (chkRegGest(5).Value = vbChecked) Then
                sdbgAmbitoDatos.MoveFirst
                sdbgAmbitoDatos.MoveRecords 8
                
                If sdbgAmbitoDatos.Columns(0).Value = "0" Then
                    sdbgAmbitoDatos.Columns(0).Value = "-1"
                    sdbgAmbitoDatos.Columns("Item").Value = "-1"
                    sdbgAmbitoDatos.Refresh
                End If
            End If
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.chkRGest_SetValue 15, IIf(chkRegGest(5).Value = vbChecked, True, False)
        
        Case 6
            'Si se obliga a distribuir un PRESUP la fila de la grid de �mbito tiene que estar seleccionada
            If (chkRegGest(6).Value = vbChecked) Then
                sdbgAmbitoDatos.MoveFirst
                sdbgAmbitoDatos.MoveRecords 9
                
                If sdbgAmbitoDatos.Columns(0).Value = "0" Then
                    sdbgAmbitoDatos.Columns(0).Value = "-1"
                    sdbgAmbitoDatos.Columns("Item").Value = "-1"
                    sdbgAmbitoDatos.Refresh
                End If
            End If
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.chkRGest_SetValue 16, IIf(chkRegGest(6).Value = vbChecked, True, False)
            
        Case 10
            If (chkRegGest(10).Value = vbChecked) Then
                fraPonderacion.Visible = True
                fraPonderacion.Enabled = True
            Else
                fraPonderacion.Visible = False
                fraPonderacion.Enabled = False
            End If
            
        Case 24 'Asignacion de unidades organizativas de los articulos
            
            
        'Restringir la asignaci�n de presupuestos a la unidad organizativa de los art�culos
        Case 26
            .gbOblAsigPresUonArtAper = IIf(chkRegGest(26).Value = vbChecked, True, False)
                    
        Case 28
            'Permitir suministro retroactivo
            .gbPermitSuminRetroactivo = IIf(chkRegGest(28).Value = vbChecked, True, False)
    End Select
End With
End Sub

''' <summary>
''' Habilitar/Desabilitar Importe adjudicado m�ximo para adjudicaci�n directa
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub chkRegOtraGest_Click()
    If chkRegOtraGest.Value = vbUnchecked Then
        txtImpMaxAdjDir = 0
        txtImpMaxAdjDir.Enabled = False
    Else
        txtImpMaxAdjDir.Enabled = True
        If gParametrosGenerales.gdIMP_MAX_ADJ_DIR Then
            txtImpMaxAdjDir = Format(gParametrosGenerales.gdIMP_MAX_ADJ_DIR, "standard")
        End If
    End If
End Sub


Private Sub chkSolOfe_Click(Index As Integer)
    If Index = 0 Then
        'Si el proceso es de subasta no se debe permitir seleccionar "Solicitar cantidades m�ximas ..."
        If chkSolOfe(0).Value = vbChecked Then
            chkSolOfe(2).Value = vbUnchecked
        End If
        chkSolOfe(2).Enabled = (chkSolOfe(0).Value = vbUnchecked)
    End If
End Sub

Private Sub cmbCriterio_Click(Index As Integer)

    Dim i As Integer
    Dim tmp As String
    
    If ClickControl Then Exit Sub
    
    If Index = 0 Then
        With cmbCriterio(1)
            tmp = .Text
            .clear
            For i = 0 To 4
                If Criterios(i) <> cmbCriterio(0) Then
                    .AddItem Criterios(i)
                End If
            Next i
            For i = 0 To .ListCount - 1
                If tmp = .List(i) Then
                    ClickControl = True
                    .ListIndex = i
                    ClickControl = False
                End If
            Next i
        End With
    Else
        With cmbCriterio(0)
            tmp = .Text
            .clear
            For i = 0 To 4
                If Criterios(i) <> cmbCriterio(1) Then
                    .AddItem Criterios(i)
                End If
            Next i
            For i = 0 To .ListCount - 1
                If tmp = .List(i) Then
                    ClickControl = True
                    .ListIndex = i
                    ClickControl = False
                End If
            Next i
        End With
    End If
End Sub

Private Sub cmbOblDist_Validate(Index As Integer, Cancel As Boolean)
    Select Case Index
        Case 1
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.cmbOblDist_SetListIndex 5, cmbOblDist(1).ListIndex
        Case 2
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.cmbOblDist_SetListIndex 6, cmbOblDist(2).ListIndex
        Case 3
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.cmbOblDist_SetListIndex 7, cmbOblDist(3).ListIndex
        Case 4
            If ctlConfPed.chkRGest_GetValue(22) Then ctlConfPed.cmbOblDist_SetListIndex 8, cmbOblDist(4).ListIndex
    End Select
End Sub


Private Sub cmdAceptar_Click()
    Dim oDestino As CDestino
    Dim CodigoAntiguo As String
    Dim oIBaseDatos As IBaseDatos
    Dim lParametrosGenerales As ParametrosGenerales
    Dim oGestorParametros As CGestorParametros
    Dim teserror As TipoErrorSummit
    
    If Not VerificarParametros Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    PrepararParametros lParametrosGenerales
    Screen.MousePointer = vbNormal
    
    Set oGestorParametros = oFSGSRaiz.Generar_CGestorParametros
    
    teserror.NumError = TESnoerror
        
    Screen.MousePointer = vbHourglass
    teserror = oGestorParametros.GuardarParametrosGenerales(lParametrosGenerales, oLiteralesAModificar, m_bCambiarThres)
    Screen.MousePointer = vbNormal
    If teserror.NumError <> TESnoerror Then
        oMensajes.ImposibleGuardarParametros
        Exit Sub
    End If
        
    Set oDestino = oFSGSRaiz.Generar_CDestino
    
    oDestino.CargarDestinoSinTransporte
    Set oIBaseDatos = oDestino
    oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        oMensajes.ImposibleActualizarDestinoSinTransporte
    Else
        CodigoAntiguo = oDestino.Cod
        oDestino.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den = sDenDestSTNew
        teserror = oIBaseDatos.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            oMensajes.ImposibleActualizarDestinoSinTransporte
        Else
            If CodigoAntiguo <> sCodDestSTNew Then
                Screen.MousePointer = vbHourglass
                teserror = oIBaseDatos.CambiarCodigo(sCodDestSTNew)
                Screen.MousePointer = vbNormal
                If teserror.NumError <> TESnoerror Then
                    oMensajes.ImposibleActualizarDestinoSinTransporte
                End If
            End If
        End If
    End If
        
    Screen.MousePointer = vbHourglass
    
    gParametrosGenerales = oGestorParametros.DevolverParametrosGenerales(basPublic.gParametrosInstalacion.gIdioma)
    If ADMIN_OBLIGATORIO Then
        ConfigurarAdminPublicaObligatoria
    End If

    If gParametrosGenerales.giINSTWEB = ConPortal Then
        If gParametrosGenerales.glMAXADJUN < CDbl(txtMAXADJUN) * 1024 Then
            oMensajes.MaxAdjunPortalMenor
        End If
    End If
    
    MostrarParametros
    sdbgAmbitoDatos_InitColumnProps
    ModoConsulta
        
    'Si se ha cambiado la denominaci�n de los presupuestos actualiza los menus:
    MDI.mnuPopUpModifPresup(0).caption = gParametrosGenerales.gsSingPres1
    MDI.mnuPopUpModifPresup(1).caption = gParametrosGenerales.gsSingPres2
    MDI.mnuPopUpModifPresup(2).caption = gParametrosGenerales.gsSingPres3
    MDI.mnuPopUpModifPresup(3).caption = gParametrosGenerales.gsSingPres4

    MDI.mnuCamposGS(11).caption = gParametrosGenerales.gsSingPres1
    MDI.mnuCamposGS(12).caption = gParametrosGenerales.gsSingPres2
    MDI.mnuCamposGS(13).caption = gParametrosGenerales.gsSingPres3
    MDI.mnuCamposGS(14).caption = gParametrosGenerales.gsSingPres4
    
    MDI.mnuAnyaCampoGS(11).caption = gParametrosGenerales.gsSingPres1
    MDI.mnuAnyaCampoGS(12).caption = gParametrosGenerales.gsSingPres2
    MDI.mnuAnyaCampoGS(13).caption = gParametrosGenerales.gsSingPres3
    MDI.mnuAnyaCampoGS(14).caption = gParametrosGenerales.gsSingPres4
    
    Screen.MousePointer = vbNormal
    
    tabCONFGEN.Tab = 0
    
    oMensajes.ConfiguracionParametrosActualizada
End Sub

''' <summary>procedimiento de preparaci�n de par�metros</summary>
''' <param name="pParametrosGenerales">objeto del tipo de ParametrosGenerales</param>
''' <remarks>Llamada desde: frmCONFGEN.cmdAceptar_Click; Tiempo m�ximo: 1 sec</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub PrepararParametros(pParametrosGenerales As ParametrosGenerales)
    Dim i As Integer
    Dim lValor As Long
    Dim bytUT As Byte
        
    With pParametrosGenerales
    .giNEO = gParametrosGenerales.giNEO
    .giNEM = gParametrosGenerales.giNEM
    .giNEPP = gParametrosGenerales.giNEPP
    .giNEPC = gParametrosGenerales.giNEPC
    .giINSTWEB = gParametrosGenerales.giINSTWEB
    .gsDEN_UON0 = txtDenUO(0)
    .gsDEN_UON1 = txtDenUO(1)
    .gsABR_UON1 = txtAbrUO(1)
    .gsDEN_UON2 = txtDenUO(2)
    .gsABR_UON2 = txtAbrUO(2)
    .gsDEN_UON3 = txtDenUO(3)
    .gsABR_UON3 = txtAbrUO(3)
    .gsDEN_GMN1 = txtDenMat(1)
    .gsabr_GMN1 = txtAbrMat(1)
    .gsDEN_GMN2 = txtDenMat(2)
    .gsABR_GMN2 = txtAbrMat(2)
    .gsDEN_GMN3 = txtDenMat(3)
    .gsABR_GMN3 = txtAbrMat(3)
    .gsDEN_GMN4 = txtDenMat(4)
    .gsABR_GMN4 = txtAbrMat(4)
    
    .gsMONCEN = sdbcMonCod
    .gsPAIDEF = sdbcPaiCod
    .gsPROVIDEF = sdbcProviCod
    .gsDESTDEF = sdbcDestCod
    .gsUNIDEF = sdbcUniCod
    .gbSugerirCodArticulos = IIf(chkSugerir.Value = vbChecked, True, False)
        
    .gdVOL_MAX_ADJ_DIR = txtVolMaxAdjDir
    .gdIMP_MAX_ADJ_DIR = txtImpMaxAdjDir
                
    If cmbOblDist(0).Text = "" Then
        .giNIVDIST = 0
    Else
        If cmbOblDist(0).ListIndex > -1 Then
            .giNIVDIST = cmbOblDist(0).Text
        Else
            .giNIVDIST = 0
        End If
    End If
    
    If EstUsarPres(0) Then
        .gbOBLPP = CBool(chkRegGest(3).Value)
        If cmbOblDist(1).Text = "" Then
            .giNIVPP = 0
        Else
            If cmbOblDist(1).ListIndex > -1 Then
                .giNIVPP = cmbOblDist(1).Text
            Else
                .giNIVPP = 0
            End If
        End If
    Else
        .gbOBLPP = False
        .giNIVPP = 0
    End If
    
    If EstUsarPres(1) Then
        .gbOBLPC = CBool(chkRegGest(4).Value)
        If cmbOblDist(2).Text = "" Then
            .giNIVPC = 0
        Else
            If cmbOblDist(2).ListIndex > -1 Then
                .giNIVPC = cmbOblDist(2).Text
            Else
                .giNIVPC = 0
            End If
        End If
    Else
        .gbOBLPC = False
        .giNIVPC = 0
    End If
    
    If EstUsarPres(2) Then
        .gbOBLPres3 = CBool(chkRegGest(5).Value)
        If cmbOblDist(3).Text = "" Then
            .giNIVPres3 = 0
        Else
            If cmbOblDist(3).ListIndex > -1 Then
                .giNIVPres3 = cmbOblDist(3).Text
            Else
                .giNIVPres3 = 0
            End If
        End If
    Else
        .gbOBLPres3 = False
        .giNIVPres3 = 0
    End If
    
    If EstUsarPres(3) Then
        .gbOBLPres4 = CBool(chkRegGest(6).Value)
        If cmbOblDist(4).Text = "" Then
            .giNIVPres4 = 0
        Else
            If cmbOblDist(4).ListIndex > -1 Then
                .giNIVPres4 = cmbOblDist(4).Text
            Else
                .giNIVPres4 = 0
            End If
        End If
    Else
        .gbOBLPres4 = False
        .giNIVPres4 = 0
    End If
    
    .gbOblProveEqp = Not (chkRegGest(7).Value = vbChecked)
    .gsROLDEF = sdbcRolCod
    .gsPAGDEF = sdbcPagCod
    .gsESTINI = sdbcEstCod(0)
    .gsESTWEB = sdbcEstCod(1)
    
    .giDIAREU = cmbDiaSemana.ListIndex + 1
    .giPERIODICIREU = (cmbNumSemanas.ListIndex + 1) * 7
    
    .gdCOMIENZOREU = txtHoraReu
    
    For i = 0 To 4
        If Criterios(i) = cmbCriterio(0) Then
            .giCRITEORDREU1 = i + 1
        End If
    Next i
    
    For i = 0 To 4
        If Criterios(i) = cmbCriterio(1) Then
            .giCRITEORDREU2 = i + 1
        End If
    Next i
    
    If txtTiempoReu = "" Then
        .giTIEMPOREU = 0
    Else
        .giTIEMPOREU = txtTiempoReu
    End If
    
    .gsPETOFEDOT = txtPlantilla(0)
    .gsPETOFECARTADOT = txtPlantilla(1)
    .gsPETOFEMAILDOT = txtPlantilla(2)
    .gsPETOFEWEBDOT = txtPlantilla(3)
    .gsOBJDOT = txtPlantilla(4)
    .gsOBJCARTADOT = txtPlantilla(5)
    .gsOBJMAILDOT = txtPlantilla(6)
    .gsOBJWEBDOT = txtPlantilla(7)
    
    If chkPlantAdj(0).Value = vbUnchecked Then
        .gbAdjOrden = False
    Else
        .gbAdjOrden = True
    End If
    
    If chkPlantAdj(1).Value = vbUnchecked Then
        .gbAdjNotif = False
    Else
        .gbAdjNotif = True
    End If
    
    .gsadjDot = txtPlantilla(8)
    .gsADJNOTIFDOT = txtPlantilla(38)
    .gsAdjCartaDot = txtPlantilla(9)
    .gsAdjMailDot = txtPlantilla(10)
    .gsAdjNoCartaDot = txtPlantilla(11)
    .gsAdjNoMailDot = txtPlantilla(12)
    
    .gsHOJAADJDETPROCEDOT = txtPlantilla(13)
    .gsHOJAADJDIRDOT = txtPlantilla(14)
    .gsCONVDOT = txtPlantilla(15)
    Select Case UCase(Me.sdbcIdioma(6).Columns("ID").Value)
    Case "SPA"
        .gsCONVMAILHTMLSPA = txtPlantilla(16)
        .gsCONVMAILTEXTSPA = txtPlantilla(43)
        
        .gsCONVMAILHTMLENG = gParametrosGenerales.gsCONVMAILHTMLENG
        .gsCONVMAILTEXTENG = gParametrosGenerales.gsCONVMAILTEXTENG
        
        .gsCONVMAILHTMLGER = gParametrosGenerales.gsCONVMAILHTMLGER
        .gsCONVMAILTEXTGER = gParametrosGenerales.gsCONVMAILTEXTGER
        
        .gsCONVMAILHTMLFRA = gParametrosGenerales.gsCONVMAILHTMLFRA
        .gsCONVMAILTEXTFRA = gParametrosGenerales.gsCONVMAILTEXTFRA
        
    Case "ENG"
        .gsCONVMAILHTMLSPA = gParametrosGenerales.gsCONVMAILHTMLSPA
        .gsCONVMAILTEXTSPA = gParametrosGenerales.gsCONVMAILTEXTSPA
    
        .gsCONVMAILHTMLENG = txtPlantilla(16)
        .gsCONVMAILTEXTENG = txtPlantilla(43)
                
        .gsCONVMAILHTMLGER = gParametrosGenerales.gsCONVMAILHTMLGER
        .gsCONVMAILTEXTGER = gParametrosGenerales.gsCONVMAILTEXTGER
        
        .gsCONVMAILHTMLFRA = gParametrosGenerales.gsCONVMAILHTMLFRA
        .gsCONVMAILTEXTFRA = gParametrosGenerales.gsCONVMAILTEXTFRA
        
    Case "GER"
        .gsCONVMAILHTMLSPA = gParametrosGenerales.gsCONVMAILHTMLSPA
        .gsCONVMAILTEXTSPA = gParametrosGenerales.gsCONVMAILTEXTSPA
        
        .gsCONVMAILHTMLENG = gParametrosGenerales.gsCONVMAILHTMLENG
        .gsCONVMAILTEXTENG = gParametrosGenerales.gsCONVMAILTEXTENG
    
        .gsCONVMAILHTMLGER = txtPlantilla(16)
        .gsCONVMAILTEXTGER = txtPlantilla(43)
        
        .gsCONVMAILHTMLFRA = gParametrosGenerales.gsCONVMAILHTMLFRA
        .gsCONVMAILTEXTFRA = gParametrosGenerales.gsCONVMAILTEXTFRA
        
    Case "FRA"
        .gsCONVMAILHTMLSPA = gParametrosGenerales.gsCONVMAILHTMLSPA
        .gsCONVMAILTEXTSPA = gParametrosGenerales.gsCONVMAILTEXTSPA
        
        .gsCONVMAILHTMLENG = gParametrosGenerales.gsCONVMAILHTMLENG
        .gsCONVMAILTEXTENG = gParametrosGenerales.gsCONVMAILTEXTENG
    
        .gsCONVMAILHTMLGER = gParametrosGenerales.gsCONVMAILHTMLGER
        .gsCONVMAILTEXTGER = gParametrosGenerales.gsCONVMAILTEXTGER
        
        .gsCONVMAILHTMLFRA = txtPlantilla(16)
        .gsCONVMAILTEXTFRA = txtPlantilla(43)
    
    End Select
    If optGenerarProc(2).Value = True Then
        'Acta en word
        .giActaTipo = ActaWord
    Else
        'Acta en crystal reports
        .giActaTipo = ActaCrystal
    End If
    .giIDAGENDADEFECTO = IIf(sdbcPlantilla(10).Value = "", 0, sdbcPlantilla(10).Value)
    If .giActaTipo = ActaWord Then
        .giIDACTA1DEFECTO = IIf(sdbcPlantilla(11).Value = "", 0, sdbcPlantilla(11).Value)
        .giIDACTA2DEFECTO = IIf(sdbcPlantilla(12).Value = "", 0, sdbcPlantilla(12).Value)
    Else
        .gsACTADOT = txtPlantilla(18)
        .gsACTADOT2 = txtPlantilla(39)
    End If
    
    .gsCOMPARATIVADOT = txtPlantilla(19)
    .gsCOMPARATIVAITEMDOT = txtPlantilla(37)
    .gsComparativaQA = txtPlantilla(42)
    
    Select Case UCase(Me.sdbcIdioma(7).Columns("ID").Value)
    Case "SPA"
        .gsCONVMAILSUBJECTSPA = txtConvMailSubject
        .gsCONVMAILSUBJECTENG = gParametrosGenerales.gsCONVMAILSUBJECTENG
        .gsCONVMAILSUBJECTGER = gParametrosGenerales.gsCONVMAILSUBJECTGER
        .gsCONVMAILSUBJECTFRA = gParametrosGenerales.gsCONVMAILSUBJECTFRA
    Case "ENG"
        .gsCONVMAILSUBJECTSPA = gParametrosGenerales.gsCONVMAILSUBJECTSPA
        .gsCONVMAILSUBJECTENG = txtConvMailSubject
        .gsCONVMAILSUBJECTGER = gParametrosGenerales.gsCONVMAILSUBJECTGER
        .gsCONVMAILSUBJECTFRA = gParametrosGenerales.gsCONVMAILSUBJECTFRA
    Case "GER"
        .gsCONVMAILSUBJECTSPA = gParametrosGenerales.gsCONVMAILSUBJECTSPA
        .gsCONVMAILSUBJECTENG = gParametrosGenerales.gsCONVMAILSUBJECTENG
        .gsCONVMAILSUBJECTGER = txtConvMailSubject
        .gsCONVMAILSUBJECTFRA = gParametrosGenerales.gsCONVMAILSUBJECTFRA
    Case "FRA"
        .gsCONVMAILSUBJECTSPA = gParametrosGenerales.gsCONVMAILSUBJECTSPA
        .gsCONVMAILSUBJECTENG = gParametrosGenerales.gsCONVMAILSUBJECTENG
        .gsCONVMAILSUBJECTGER = gParametrosGenerales.gsCONVMAILSUBJECTGER
        .gsCONVMAILSUBJECTFRA = txtConvMailSubject
    End Select
    
    .gsPetOfeMailSubject = txtMailSubject(0)
    .gsObjMailSubject = txtMailSubject(1)
    .gsAdjMailSubject = txtMailSubject(2)
    .gsAdjNoMailSubject = txtMailSubject(3)
    
    .gsDenSolicitudCompra = txtDenSolicitud
    .gsURLSUMMIT = txtURL
    
    'Asignar a los par�metros de nombre de relaci�n los contenidos de las cajas de texto
    .gsSingNombreRel = txtDenPres(2)
    .gsPlurNombreRel = txtDenPres(3)
    
    m_bCambiarThres = False
    If chkActivar(0).Value = vbChecked Then
        .giLOGPREBLOQ = sldLogPreBloq.Value
    Else
        .giLOGPREBLOQ = 0
    End If
    
    If chkActivar(1).Value = vbChecked Then
        .gbACTIVLOG = True
    Else
        .gbACTIVLOG = False
    End If
     
    .gbPermAbrirItemsSinArtCod = (chkRegGest(1).Value = vbChecked)
    
    .gbPermAdjSinHom = (chkRegGest(8).Value = vbChecked)
    
    .gsDEN_CAL1 = gParametrosGenerales.gsDEN_CAL1
    .gsDEN_CAL2 = gParametrosGenerales.gsDEN_CAL2
    .gsDEN_CAL3 = gParametrosGenerales.gsDEN_CAL3
    
    .gbSENT_ORD_CAL1 = gParametrosGenerales.gbSENT_ORD_CAL1
    .gbSENT_ORD_CAL1 = gParametrosGenerales.gbSENT_ORD_CAL1
    .gbSENT_ORD_CAL1 = gParametrosGenerales.gbSENT_ORD_CAL1
    
    .gbCONVSOLORESP = (chkSoloConvResp.Value = vbChecked)
    .gbPermAbrirItemsSinArtCod = (chkRegGest(1).Value = vbChecked)
    .gbPermAdjSinHom = (chkRegGest(8).Value = vbChecked)
    
    Select Case sdbcConPres.Columns("ID").Value
        Case 1
            .gsSingPres1 = txtDenPres(0).Text
            .gsPlurPres1 = txtDenPres(1).Text
            sDatos(6) = .gsPlurPres1
        Case 2
            .gsSingPres2 = txtDenPres(0).Text
            .gsPlurPres2 = txtDenPres(1).Text
            sDatos(7) = .gsPlurPres2
        Case 3
            .gsSingPres3 = txtDenPres(0).Text
            .gsPlurPres3 = txtDenPres(1).Text
            sDatos(8) = .gsPlurPres3
        Case 4
            .gsSingPres4 = txtDenPres(0).Text
            .gsPlurPres4 = txtDenPres(1).Text
            sDatos(9) = .gsPlurPres4
    End Select
    
    .gbUsarPres1 = EstUsarPres(0)
    .gbUsarPres2 = EstUsarPres(1)
    .gbUsarPres3 = EstUsarPres(2)
    .gbUsarPres4 = EstUsarPres(3)
        
    .gsRPTPATH = txtRuta(0).Text
    
    If m_bMostrarCarpetas Then
        .gsPathSolicitudes = txtRuta(1).Text
    End If
    
    If optRutaTS(0).Value = True Then
        .gsTSHomeFolderRutaLocal = txtRuta(2).Text
        .gsTSHomeFolderRutaUnidad = ""
        .gsTSHomeFolderUnidad = ""
    ElseIf optRutaTS(1).Value = True Then
        .gsTSHomeFolderUnidad = Left(strMatrix(cmbTSUnidad.ListIndex), 1)
        .gsTSHomeFolderRutaUnidad = txtRuta(3).Text
        .gsTSHomeFolderRutaLocal = ""
    End If
    
    .gsSMTPServer = ""
    .giSMTPPort = 0
    .gbSMTPSSL = False
    .giSMTPAutent = 0
    .gbSMTPBasicaGS = False
    .gsSMTPUser = ""
    .gsSMTPPwd = ""
    .gbSMTPUsarCuenta = False
    .gsSMTPCuentaMail = ""
    .gsSMTPDominio = ""
    If chkCorreo(0).Value = vbUnchecked And chkCorreo(1).Value = vbUnchecked Then
        .giMail = 0
    ElseIf chkCorreo(0).Value = vbChecked Then
        If optMapi(0).Value = True Then
            .giMail = 3
        ElseIf optMapi(1).Value = True Then
            .giMail = 2
        End If
    ElseIf chkCorreo(1).Value = vbChecked Then
        .giMail = 1
        .gsSMTPServer = Trim(txtSMTP(0).Text)
        .giSMTPPort = Trim(txtSMTP(1).Text)
        .gbSMTPSSL = chkCorreo(2).Value
        If optAutenticacion(2).Value = True Then 'anonima
            .giSMTPAutent = 0
            .gsSMTPCuentaMail = Trim(txtSMTP(4).Text)
            .gbSMTPBasicaGS = False
        ElseIf optAutenticacion(0).Value = True Then 'basica
            .giSMTPAutent = 1
            .gbSMTPBasicaGS = False
            .gsSMTPUser = Trim(txtSMTP(4).Text) 'para la basica meto la cuenta en el campo de usuario
            .gsSMTPCuentaMail = Trim(txtSMTP(4).Text)
            .gsSMTPPwd = Trim(txtSMTP(3).Text)
        Else 'windows
            .giSMTPAutent = 2

            .gsSMTPCuentaMail = Trim(txtSMTP(4).Text)
            .gbSMTPBasicaGS = False
            .gsSMTPUser = Trim(txtSMTP(2).Text)
            .gsSMTPPwd = Trim(txtSMTP(3).Text)
            .gsSMTPDominio = Trim(txtSMTP(5).Text)
        End If
    End If
    
    .gbMostrarMail = IIf(chkCorreo(4).Value = vbChecked, True, False)
    .gbAcuseRecibo = IIf(chkCorreo(5).Value = vbChecked, True, False)
       
    .gbTipoMail = (optFormatoEMail(0).Value = True)
    
    .giLongCabComp = val(txtLongCabComp(0))
    .giLongCabCompItem = val(txtLongCabComp(1))
    .giLongCabCompQA = val(txtLongCabComp(2))
    
    pParametrosGenerales.gIdioma = sdbcIdiomas(0).Columns("ID").Value
    idiIdiomasRow = sdbcIdiomas(0).Bookmark
    
    .gIdiomaOffSet = sdbcIdiomas(0).Columns("OFFSET").Value
    .gIdiomaPortal = sdbcIdiomas(1).Columns("ID").Value
    idiIdiomasPortalRow = sdbcIdioma(1).Bookmark
    
    'Zona horaria
    .gvTimeZone = sdbcIdiomas(2).Columns(1).Value
    
    .gsFSP_BD = gParametrosGenerales.gsFSP_BD
    .giPortal = gParametrosGenerales.giPortal
    .gsFSP_CIA = gParametrosGenerales.gsFSP_CIA
    .gsFSP_SRV = gParametrosGenerales.gsFSP_SRV
        
    .giBuzonPeriodoDef = sdbcAntigOfer.Columns(1).Value

    If opLeidas.Value = True Then
        .giBuzonOfeLeidas = TipoBuzonOfertas.Leidas
    ElseIf opNoLeidas.Value = True Then
        .giBuzonOfeLeidas = TipoBuzonOfertas.NoLeidas
    Else
        .giBuzonOfeLeidas = TipoBuzonOfertas.Todas
    End If
    
    .glMAXADJUN = CDbl(txtMAXADJUN) * 1024
    
    If opAdjAutomat(0).Value = True Then
        .giAnyadirEspecArticulo = Adjuntar
    ElseIf opAdjAutomat(1).Value = True Then
        .giAnyadirEspecArticulo = PreguntarAdj
    Else
        .giAnyadirEspecArticulo = NoAdjuntar
    End If
        
    .gbAvisoPortalCompResp = IIf(chkCorreo(6).Value = vbUnchecked, False, True)
    .gbAvisoPortalCompAsign = IIf(chkCorreo(7).Value = vbUnchecked, False, True)
    .gbOblProveMat = IIf(chkRegGest(9).Value = vbUnchecked, False, True)
    
    ctlConfPed.PrepararParametros pParametrosGenerales
    
    .gbAvisoAdj = Me.ctrlConfGenAvisos.AvisoAdjSoloSiResp
    
    .giAvisoAdj = Me.ctrlConfGenAvisos.AntelacionAdjudicacion
    
    .gbAvisoDespublica = Me.ctrlConfGenAvisos.AvisoVisorGSSoloSiResp
    .giAvisoDespublica = Me.ctrlConfGenAvisos.AntelacionVisorGS
            
    .gsGrupoCod = txtGrupoDefecto(0).Text
    .gsGrupoDen = txtGrupoDefecto(1).Text
    .gsGrupoDescrip = txtGrupoDefecto(2).Text
    
    .gbUsarPonderacion = (chkRegGest(10).Value = vbChecked)
    .gCPPonderar = (chkPonderacion(0).Value = vbChecked)
    .gbUnSoloPedido = (Me.chkUnSoloPedido.Value = vbChecked)
    
    Select Case chkSolOfe(0).Value
        Case vbChecked
            .gCPSubasta = True
        Case vbUnchecked
            .gCPSubasta = False
    End Select
    
    .gCPAlternativasPrec = (chkSolOfe(1).Value = vbChecked)
    .gCPCambiarMonOferta = (chkSolOfe(6).Value = vbUnchecked)

    Select Case chkSolOfe(2).Value
        Case vbChecked
            .gCPSolCantMax = True
        Case vbUnchecked
            .gCPSolCantMax = False
    End Select
        
    'Ahora preparamos los par�metros de la grid de �mbito de datos

    sdbgAmbitoDatos.MoveFirst
     For i = 0 To sdbgAmbitoDatos.Rows - 1
        Select Case sdbgAmbitoDatos.Columns("Fila").Value
            Case 0
                'Destino
                Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                    Case True
                        .gCPDestino = EnProceso
                    Case False
                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                            Case True
                                .gCPDestino = EnGrupo
                            Case False
                                .gCPDestino = EnItem
                        End Select
                End Select
    
            Case 1
                'Forma de pago
                Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                    Case True
                        .gCPPago = EnProceso
                    Case False
                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                            Case True
                                .gCPPago = EnGrupo
                            Case False
                                .gCPPago = EnItem
                        End Select
                End Select
    
            Case 2
                'Fechas de suministro
                Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                    Case True
                        .gCPFechasSuministro = EnProceso
                    Case False
                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                            Case True
                                .gCPFechasSuministro = EnGrupo
                            Case False
                                .gCPFechasSuministro = EnItem
                        End Select
                End Select
    
            Case 3
                'Proveedor actual
                Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                            Case True
                                .gCPProveActual = EnProceso
                            Case False
                                Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                                    Case True
                                        .gCPProveActual = EnGrupo
                                    Case False
                                        .gCPProveActual = EnItem
                                End Select
                        End Select
                    Case False
                        .gCPProveActual = NoDefinido
                End Select
    
            Case 4
                'Especificaciones
                Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                            Case True
                                .gCPEspProce = True
                            Case False
                                .gCPEspProce = False
                        End Select
                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                            Case True
                                .gCPEspGrupo = True
                            Case False
                                .gCPEspGrupo = False
                        End Select
                        Select Case sdbgAmbitoDatos.Columns.Item(4).Value
                            Case True
                                .gCPEspItem = True
                            Case False
                                .gCPEspItem = False
                        End Select
                    Case False
                        .gCPEspProce = False
                        .gCPEspGrupo = False
                        .gCPEspItem = False
                End Select
                
            Case 5
                'Distribuci�n en unidades organizativas
                Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                            Case True
                                .gCPDistUON = EnProceso
                            Case False
                                Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                                    Case True
                                        .gCPDistUON = EnGrupo
                                    Case False
                                        .gCPDistUON = EnItem
                                End Select
                        End Select
                    Case False
                        .gCPDistUON = NoDefinido
                End Select
    
            Case 6
                'Pres.Anu1
                Select Case .gbUsarPres1
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                            Case True
                                Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                                    Case True
                                        .gCPPresAnu1 = EnProceso
                                    Case False
                                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                                            Case True
                                                .gCPPresAnu1 = EnGrupo
                                            Case False
                                                .gCPPresAnu1 = EnItem
                                        End Select
                                End Select
                            Case False
                                .gCPPresAnu1 = NoDefinido
                        End Select
                End Select
    
            Case 7
                'Pres.Anu2
                Select Case .gbUsarPres2
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                            Case True
                                Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                                    Case True
                                        .gCPPresAnu2 = EnProceso
                                    Case False
                                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                                            Case True
                                                .gCPPresAnu2 = EnGrupo
                                            Case False
                                                .gCPPresAnu2 = EnItem
                                        End Select
                                End Select
                            Case False
                                .gCPPresAnu2 = NoDefinido
                        End Select
                End Select
    
            Case 8
                'Pres.Anu3
                Select Case .gbUsarPres3
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                            Case True
                                Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                                    Case True
                                        .gCPPres1 = EnProceso
                                    Case False
                                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                                            Case True
                                                .gCPPres1 = EnGrupo
                                            Case False
                                                .gCPPres1 = EnItem
                                        End Select
                                End Select
                            Case False
                                .gCPPres1 = NoDefinido
                        End Select
                End Select
    
            Case 9
                'Pres.Anu4
                Select Case .gbUsarPres4
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                            Case True
                                Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                                    Case True
                                        .gCPPres2 = EnProceso
                                    Case False
                                        Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                                            Case True
                                                .gCPPres2 = EnGrupo
                                            Case False
                                                .gCPPres2 = EnItem
                                        End Select
                                End Select
                            Case False
                                .gCPPres2 = NoDefinido
                        End Select
                End Select
    
            Case 10
                'Solicitudes de compras
                Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                    Case True
                        Select Case sdbgAmbitoDatos.Columns.Item(2).Value
                            Case True
                                .gCPSolicitud = EnProceso
                            Case False
                                Select Case sdbgAmbitoDatos.Columns.Item(3).Value
                                    Case True
                                        .gCPSolicitud = EnGrupo
                                    Case False
                                        .gCPSolicitud = EnItem
                                End Select
                        End Select
                    Case False
                        .gCPSolicitud = NoDefinido
                End Select
    
        End Select
        sdbgAmbitoDatos.MoveNext
    Next i
    sdbgAmbitoDatos.MoveFirst
    
    'If chkAdminPub.Value = vbChecked Then
    If chkPonderacion(1).Value = vbChecked Then
        .gCPDestino = EnProceso
        .gCPPago = EnProceso
        .gCPProveActual = NoDefinido
        .gCPSolicitud = NoDefinido
    End If
        
    Select Case chkSolOfe(3).Value
        Case vbChecked
            .gCPAdjunOfe = True
        Case vbUnchecked
            .gCPAdjunOfe = False
    End Select
    
    Select Case chkSolOfe(4).Value
        Case vbChecked
            .gCPAdjunGrupo = True
        Case vbUnchecked
            .gCPAdjunGrupo = False
    End Select
    
    Select Case chkSolOfe(5).Value
        Case vbChecked
            .gCPAdjunItem = True
        Case vbUnchecked
            .gCPAdjunItem = False
    End Select
    
    If Me.chkNoPublicarFinSum.Value = vbChecked Then
        .gCPNoPublicarFinSum = True
    Else
        .gCPNoPublicarFinSum = False
    End If
    
    If ADMIN_OBLIGATORIO Then
        .gCPProcesoAdminPub = True
    Else
        'If (chkAdminPub.Value = vbChecked) Then
        If (chkPonderacion(1).Value = vbChecked) Then
            .gCPProcesoAdminPub = True
        Else
            .gCPProcesoAdminPub = False
        End If
    End If

    .gsPETSUBDOT = txtPlantilla(36).Text
    .gsPETSUBCARTADOT = txtPlantilla(35).Text
    .gsPETSUBMAILDOT = txtPlantilla(34).Text
    .gsPETSUBWEBDOT = txtPlantilla(33).Text
    .gsPETSUBMAILSUBJECT = txtMailSubject(8).Text
        
    If chkRecordatorio.Value = vbChecked Then
        .gbRecordatorioInicioSubasta = True
        ExtraerValorYUnidadDeTiempo cboTAntelacion.Text, lValor, bytUT
        FormatearTiempoAntelacionMinutos lValor, bytUT, .glSubastaTiempAnt
    Else
        .gbRecordatorioInicioSubasta = False
        .glSubastaTiempAnt = gParametrosGenerales.glSubastaTiempAnt
    End If
    
    If chkInicio.Value = vbChecked Then
        .gbInicioSubasta = True
    Else
        .gbInicioSubasta = False
    End If
    
    If chkCierre.Value = vbChecked Then
        .gbCierreSubasta = True
    Else
        .gbCierreSubasta = False
    End If
    
    .gbPresupuestosAut = CBool(chkRegGest(11).Value)
    .gbSeleccionPositiva = CBool(chkRegGest(12).Value)
    .gbPresupuestoPlanificado = (opAdjAutomat(4).Value = True)
    
    Dim vbm As Variant
    For i = 0 To sdbgCamposERP.Rows - 1
        vbm = sdbgCamposERP.AddItemBookmark(i)
        
        Select Case i
            Case 0
                .gbCampo1ERPAct = sdbgCamposERP.Columns("ACT").CellValue(vbm)
            Case 1
                .gbCampo2ERPAct = sdbgCamposERP.Columns("ACT").CellValue(vbm)
            Case 2
                .gbCampo3ERPAct = sdbgCamposERP.Columns("ACT").CellValue(vbm)
            Case 3
                .gbCampo4ERPAct = sdbgCamposERP.Columns("ACT").CellValue(vbm)
        End Select
    Next
        
    .gbAvisoProveDespublicar = ctrlConfGenAvisos.AvisoProveDespub
    .gbAvisoProveSoloNoOfertado = ctrlConfGenAvisos.AvisoNoHanOfertado
    .gbExcluirAProveNoOfertan = ctrlConfGenAvisos.AvisoNoVanOfertar
    .giAntelacionAvisoProve = ctrlConfGenAvisos.AntelacionDespublicacion
    .gsOFENotifAvisoDespubHTML = ctrlConfGenAvisos.PlantillaDespub(0)
    .gsOFENotifAvisoDespubTXT = ctrlConfGenAvisos.PlantillaDespub(1)
    .gsOFENotifAvisoDespubSubject = ctrlConfGenAvisos.AsuntoAvisoDespub

    'Avisos a los participantes
    .gbActApeMail = ctrlConfGenAvisos.EnviarEmailApertura
    .gbActPreAdjMail = ctrlConfGenAvisos.EnviarEmailPreAdj
    .gbActAdjMail = ctrlConfGenAvisos.EnviarEmailAdj
    .gbActAnulacionMail = ctrlConfGenAvisos.EnviarEmailAnulacion

    If m_bMostrarSolicitud Then
        If optGenerarProc(0).Value Then
            .gbSolicProcAdjDir = True
        Else
            .gbSolicProcAdjDir = False
        End If
    End If
    
    If m_bMostrarSolicitud Then
        If Trim(txtAdjDirImp.Text) = "" Then
            .gvImporteSolicProcAdjDir = Null
        Else
            .gvImporteSolicProcAdjDir = Trim(txtAdjDirImp.Text)
        End If
    End If
    
    If m_bMostrarSolicitud Then .giValidezSolicOfe = sdbcPeriodoSolic.Columns(1).Value
    
    .gbOBLUnidadMedida = CBool(chkRegGest(23).Value)
    .gbControlCambiosFechaSuministro = CBool(chkRegGest(25).Value)
    .gbOblAsigPresUonArtAper = CBool(chkRegGest(26).Value)
    .gbPermitSuminRetroactivo = CBool(chkRegGest(28).Value)
        
    'Ahora los nuevos par�metros generales referentes a seguridad
    If oUsuarioSummit.Tipo = Administrador Then
        .giHIST_PWD = CInt(txtGrupoDefecto(3).Text)
        .giEDAD_MAX_PWD = CInt(txtGrupoDefecto(5).Text)
        .giEDAD_MIN_PWD = CInt(txtGrupoDefecto(4).Text)
        .gbCOMPLEJIDAD_PWD = optRutaTS(2).Value
        .giMIN_SIZE_PWD = CInt(txtGrupoDefecto(6).Text)
    Else
        .giHIST_PWD = gParametrosGenerales.giHIST_PWD
        .giEDAD_MAX_PWD = gParametrosGenerales.giEDAD_MAX_PWD
        .giEDAD_MIN_PWD = gParametrosGenerales.giEDAD_MIN_PWD
        .gbCOMPLEJIDAD_PWD = gParametrosGenerales.gbCOMPLEJIDAD_PWD
        .giMIN_SIZE_PWD = gParametrosGenerales.giMIN_SIZE_PWD
    End If
    
    'Par�metros subasta
    .gCPSubTipo = m_iSubTipo
    .gCPSubModo = m_iSubModo
    If Not IsEmpty(m_vSubDuracion) Then .gCPSubDuracion = m_vSubDuracion
    .gCPSubastaEspera = m_iSubastaEspera
    .gCPSubPublicar = m_bSubPublicar
    .gCPSubNotifEventos = m_bSubNotifEventos
    .gCPSubastaProve = m_bSubastaProve
    .gCPSubVerDesdePrimPuja = m_bSubVerDesdePrimPuja
    .gCPSubastaPrecioPuja = m_bSubastaPrecioPuja
    .gCPSubastaPujas = m_bSubastaPujas
    .gCPSubastaBajMinPuja = m_bSubastaBajMinPuja
    .gCPSubBajMinProveTipo = m_iSubBajMinGanTipo
    .gCPSubBajMinGanProcVal = m_vSubBajMinGanProcVal
    .gCPSubBajMinGanGrupoVal = m_vSubBajMinGanGrupoVal
    .gCPSubBajMinGanItemVal = m_vSubBajMinGanItemVal
    .gCPSubBajMinProve = m_bSubBajMinProve
    .gCPSubBajMinProveTipo = m_iSubBajMinProveTipo
    .gCPSubBajMinProveProcVal = m_vSubBajMinProveProcVal
    .gCPSubBajMinProveGrupoVal = m_vSubBajMinProveGrupoVal
    .gCPSubBajMinProveItemVal = m_vSubBajMinProveItemVal
    
    If optCalcLinBase(0).Value = True Then
            .giCalculoLineaBase = 0
            .gvNumMejoresOfertas = Null
    ElseIf optCalcLinBase(1).Value = True Then
            .giCalculoLineaBase = 1
            .gvNumMejoresOfertas = Null
    ElseIf optCalcLinBase(2).Value = True Then
            .giCalculoLineaBase = 2
            .gvNumMejoresOfertas = txtNumOfe.Text
    End If
    
    End With
    
    Set pParametrosGenerales.gCPSubTextoFin = m_dcSubTextosFin
End Sub

Private Sub cmdCancelar_Click()
    MostrarParametros
    InicializarValoresSubasta
    ModoConsulta
End Sub

''' <summary>Abre el formulario de configuraci�n de subasta.</summary>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: 1sg</remarks>

Private Sub cmdConfigSubasta_Click()
    On Error GoTo Error
    
    Dim oConfSubasta As New frmCONFSubasta
    oConfSubasta.g_sOrigen = "frmCONFGEN"
    oConfSubasta.g_bModoEdicion = picConf(1).Enabled
    
    oConfSubasta.g_iSubTipo = m_iSubTipo
    oConfSubasta.g_iSubModo = m_iSubModo
    oConfSubasta.g_vSubDuracion = m_vSubDuracion
    oConfSubasta.g_iSubastaEspera = m_iSubastaEspera
    oConfSubasta.g_bSubPublicar = m_bSubPublicar
    oConfSubasta.g_bSubNotifEventos = m_bSubNotifEventos
    oConfSubasta.g_bSubastaProve = m_bSubastaProve
    oConfSubasta.g_bSubVerDesdePrimPuja = m_bSubVerDesdePrimPuja
    oConfSubasta.g_bSubastaPrecioPuja = m_bSubastaPrecioPuja
    oConfSubasta.g_bSubastaPujas = m_bSubastaPujas
    oConfSubasta.g_bSubastaBajMinPuja = m_bSubastaBajMinPuja
    oConfSubasta.g_iSubBajMinGanTipo = m_iSubBajMinGanTipo
    oConfSubasta.g_vSubBajMinGanProcVal = m_vSubBajMinGanProcVal
    oConfSubasta.g_vSubBajMinGanGrupoVal = m_vSubBajMinGanGrupoVal
    oConfSubasta.g_vSubBajMinGanItemVal = m_vSubBajMinGanItemVal
    oConfSubasta.g_bSubBajMinProve = m_bSubBajMinProve
    oConfSubasta.g_iSubBajMinProveTipo = m_iSubBajMinProveTipo
    oConfSubasta.g_vSubBajMinProveProcVal = m_vSubBajMinProveProcVal
    oConfSubasta.g_vSubBajMinProveGrupoVal = m_vSubBajMinProveGrupoVal
    oConfSubasta.g_vSubBajMinProveItemVal = m_vSubBajMinProveItemVal
    Set oConfSubasta.g_dcSubTextosFin = m_dcSubTextosFin
    oConfSubasta.Show vbModal
    If oConfSubasta.g_bOK Then
        m_iSubTipo = oConfSubasta.g_iSubTipo
        m_iSubModo = oConfSubasta.g_iSubModo
        m_vSubDuracion = oConfSubasta.g_vSubDuracion
        m_iSubastaEspera = oConfSubasta.g_iSubastaEspera
        m_bSubPublicar = oConfSubasta.g_bSubPublicar
        m_bSubNotifEventos = oConfSubasta.g_bSubNotifEventos
        m_bSubastaProve = oConfSubasta.g_bSubastaProve
        m_bSubVerDesdePrimPuja = oConfSubasta.g_bSubVerDesdePrimPuja
        m_bSubastaPrecioPuja = oConfSubasta.g_bSubastaPrecioPuja
        m_bSubastaPujas = oConfSubasta.g_bSubastaPujas
        m_bSubastaBajMinPuja = oConfSubasta.g_bSubastaBajMinPuja
        m_iSubBajMinGanTipo = oConfSubasta.g_iSubBajMinGanTipo
        m_vSubBajMinGanProcVal = oConfSubasta.g_vSubBajMinGanProcVal
        m_vSubBajMinGanGrupoVal = oConfSubasta.g_vSubBajMinGanGrupoVal
        m_vSubBajMinGanItemVal = oConfSubasta.g_vSubBajMinGanItemVal
        m_bSubBajMinProve = oConfSubasta.g_bSubBajMinProve
        m_iSubBajMinProveTipo = oConfSubasta.g_iSubBajMinProveTipo
        m_vSubBajMinProveProcVal = oConfSubasta.g_vSubBajMinProveProcVal
        m_vSubBajMinProveGrupoVal = oConfSubasta.g_vSubBajMinProveGrupoVal
        m_vSubBajMinProveItemVal = oConfSubasta.g_vSubBajMinProveItemVal
        Set m_dcSubTextosFin = oConfSubasta.g_dcSubTextosFin
    End If
    
Salir:
    Unload oConfSubasta
    Set oConfSubasta = Nothing
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
    Resume Salir
End Sub

Private Sub cmdDestCod_Click()

    frmMODCOD.caption = sCap(35)
    frmMODCOD.Left = frmCONFGEN.Left + 500
    frmMODCOD.Top = frmCONFGEN.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodDEST
    frmMODCOD.txtCodAct.Text = sCodDestSTCurrent
    Set frmMODCOD.fOrigen = frmCONFGEN
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido sCap(36)
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(sCodDestSTCurrent) Then
        oMensajes.NoValido sCap(36)
        Exit Sub
    End If
        
    sCodDestSTNew = g_sCodigoNuevo
    sCodDestSTCurrent = g_sCodigoNuevo
    fraManten(1).caption = sCap(33) & " (" & sCodDestSTNew & ")"
        
End Sub



Private Sub cmdEdicion_Click()
    ModoEdicion
End Sub

''' <summary>Muestra una pantalla de seleccion de archivos</summary>
''' <param name="Index">Indicedel boton pulsado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPlantilla_Click(Index As Integer)

On Error GoTo Error
    cdlArchivo.filename = ""

    If Index = 19 Or Index = 38 Or Index = 43 Then
        cdlArchivo.Filter = sCap(37) & " (*.xlt)|*.xlt"
        cdlArchivo.DefaultExt = "xlt"
    ElseIf Index = 16 Or Index = 2 Or Index = 3 Or Index = 6 Or Index = 7 Or Index = 10 Or Index = 12 Or Index = 34 Or Index = 35 Then
        cdlArchivo.Filter = sCap(56) & " (*.htm)|*.htm"
        cdlArchivo.DefaultExt = "htm"
    ElseIf Index = 44 Then
        cdlArchivo.Filter = sCap(55) & " (*.txt)|*.txt"
        cdlArchivo.DefaultExt = "txt"
    ElseIf Index = 18 Or Index = 40 Then
        If optGenerarProc(2).Value = True Then
            MostrarVisorPlantillas
            Exit Sub
        Else
            cdlArchivo.Filter = sCap(58) & " (*.rpt)|*.rpt"
            cdlArchivo.DefaultExt = "rpt"
        End If
    ElseIf Index = 17 Then
        MostrarVisorPlantillas
        Exit Sub
    Else
        cdlArchivo.Filter = sCap(38) & " (*.dot)|*.dot"
        cdlArchivo.DefaultExt = "dot"
    End If
    
    cdlArchivo.Action = 1
    If Index > 20 Then
        txtPlantilla(Index - 1) = cdlArchivo.filename
    Else
        txtPlantilla(Index) = cdlArchivo.filename
    End If
    
    If Index = 16 Then
        oLiteralesAModificar.Item(sdbcIdioma(6).Columns("ID").Value & CStr(10008)).Den = txtPlantilla(16).Text
    ElseIf Index = 44 Then
        oLiteralesAModificar.Item(sdbcIdioma(6).Columns("ID").Value & CStr(10009)).Den = txtPlantilla(43).Text
    End If
    
    Exit Sub
Error:
    If err.Number = cdlCancel Then
        Exit Sub
    Else
        Resume Next
    End If
    
End Sub

''' <summary>carga de textos multiidioma</summary>
''' <remarks>Llamada desde: frmCONFGEN.CargarRecursos; Tiempo m�ximo: 1 sec</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer

    On Error Resume Next
        
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFGEN, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        For i = 1 To 53
            sCap(i) = Ador(0).Value
            Ador.MoveNext
        Next
        arRecursosConfGenPedidos(87) = sCap(37)
        arRecursosConfGenPedidos(88) = sCap(56)
        arRecursosConfGenPedidos(89) = sCap(55)
        arRecursosConfGenPedidos(90) = sCap(38)
        
        SSTab2.TabCaption(0) = Ador(0).Value '54
        Ador.MoveNext
        SSTab2.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        SSTab2.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        SSTab2.TabCaption(4) = Ador(0).Value
        Ador.MoveNext
        fraMAT.caption = Ador(0).Value
        Ador.MoveNext
        lblMat(1).caption = Ador(0).Value
        lblUO(1).caption = Ador(0).Value
        Ador.MoveNext
        lblMat(2).caption = Ador(0).Value '60
        lblUO(2).caption = Ador(0).Value
        Ador.MoveNext
        lblMat(3).caption = Ador(0).Value
        lblUO(3).caption = Ador(0).Value
        Ador.MoveNext
        lblMat(4).caption = Ador(0).Value
        Ador.MoveNext
        lblAbrMat(1).caption = Ador(0).Value '63 Abreviatura
        lblAbrMat(2).caption = Ador(0).Value
        lblAbrMat(3).caption = Ador(0).Value
        lblAbrMat(4).caption = Ador(0).Value
        fraUO.caption = Ador(0).Value
        lblUOAbr(1).caption = Ador(0).Value
        lblUOAbr(2).caption = Ador(0).Value
        lblUOAbr(3).caption = Ador(0).Value
        Ador.MoveNext
        lblUO(0).caption = Ador(0).Value & ":"
        Ador.MoveNext
        Frame19.caption = Ador(0).Value '65
        Ador.MoveNext
        lblPresupuesto(2).caption = Ador(0).Value '67 Singular
        Ador.MoveNext
        Label37.caption = Ador(0).Value '68 Plural
        Ador.MoveNext
        Frame14.caption = Ador(0).Value
        Ador.MoveNext
        fraManten(0).caption = Ador(0).Value '70 Valores por defecto
        Frame6.caption = Ador(0).Value
        sstabReu.TabCaption(0) = Ador(0).Value
        SSTabGestion.TabCaption(0) = Ador(0).Value

        Ador.MoveNext
        lblbuzperiodo(2).caption = Ador(0).Value
        Ador.MoveNext
        lblTabMantenimiento(0).caption = Ador(0).Value
        Ador.MoveNext
        lblTabMantenimiento(1).caption = Ador(0).Value
        Ador.MoveNext
        lblTabMantenimiento(2).caption = Ador(0).Value
        Ador.MoveNext
        lblTabMantenimiento(3).caption = Ador(0).Value  '75
        Ador.MoveNext
        sdbcMonCod.Columns(0).caption = Ador(0).Value '76 Cod
        sdbcPaiCod.Columns(0).caption = Ador(0).Value
        sdbcProviCod.Columns(0).caption = Ador(0).Value
        sdbcDestCod.Columns(0).caption = Ador(0).Value
        sdbcUniCod.Columns(0).caption = Ador(0).Value
        sdbcRolCod.Columns(0).caption = Ador(0).Value
        sdbcPagCod.Columns(0).caption = Ador(0).Value
        sdbcEstCod(0).Columns(0).caption = Ador(0).Value
        sdbcEstCod(1).Columns(0).caption = Ador(0).Value
        sdbcMonDen.Columns(1).caption = Ador(0).Value
        sdbcPaiDen.Columns(1).caption = Ador(0).Value
        sdbcProviDen.Columns(1).caption = Ador(0).Value
        sdbcDestDen.Columns(1).caption = Ador(0).Value
        sdbcUniDen.Columns(1).caption = Ador(0).Value
        sdbcRolDen.Columns(1).caption = Ador(0).Value
        sdbcPagDen.Columns(1).caption = Ador(0).Value
        sdbcEstDen(0).Columns(1).caption = Ador(0).Value
        sdbcEstDen(1).Columns(1).caption = Ador(0).Value
        lblGrupoDef(0).caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbcMonDen.Columns(0).caption = Ador(0).Value '77 Denominaci�n
        sdbcPaiDen.Columns(0).caption = Ador(0).Value
        sdbcProviDen.Columns(0).caption = Ador(0).Value
        sdbcDestDen.Columns(0).caption = Ador(0).Value
        sdbcUniDen.Columns(0).caption = Ador(0).Value
        sdbcRolDen.Columns(0).caption = Ador(0).Value
        sdbcPagDen.Columns(0).caption = Ador(0).Value
        sdbcEstDen(0).Columns(0).caption = Ador(0).Value
        sdbcEstDen(1).Columns(0).caption = Ador(0).Value
        sdbcMonCod.Columns(1).caption = Ador(0).Value
        sdbcPaiCod.Columns(1).caption = Ador(0).Value
        sdbcProviCod.Columns(1).caption = Ador(0).Value
        sdbcDestCod.Columns(1).caption = Ador(0).Value
        sdbcUniCod.Columns(1).caption = Ador(0).Value
        sdbcRolCod.Columns(1).caption = Ador(0).Value
        sdbcPagCod.Columns(1).caption = Ador(0).Value
        sdbcEstCod(0).Columns(1).caption = Ador(0).Value
        sdbcEstCod(1).Columns(1).caption = Ador(0).Value
        lblTabMantenimiento(4).caption = Ador(0).Value & ":"
        sdbcIdiomas(0).caption = Ador(0).Value
        lblGrupoDef(1).caption = Ador(0).Value & ":"
        Ador.MoveNext
        cmdDestCod.caption = Ador(0).Value
        Ador.MoveNext
        chkRegGest(0).caption = Replace(Ador(0).Value, "###", gParametrosGenerales.gsMONCEN) '79 "Presupuesto m�ximo para adjudicaci�n directa (###):"
        Ador.MoveNext
        chkRegGest(1).caption = Ador(0).Value '80
        Ador.MoveNext
        Ador.MoveNext
        chkRegGest(7).caption = Ador(0).Value
        Ador.MoveNext
        chkRegGest(8).caption = Ador(0).Value
        Ador.MoveNext
        SSTabGestion.TabCaption(2) = Ador(0).Value  '84 Reglas de apertura de procesos
        Ador.MoveNext
        lblValDef(0).caption = Ador(0).Value '85
        Ador.MoveNext
        lblValDef(1).caption = Ador(0).Value
        Ador.MoveNext
        lblValDef(2).caption = Ador(0).Value
        Ador.MoveNext
        lblValDef(3).caption = Ador(0).Value
        Ador.MoveNext
        SSTabOferta.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTabOferta.TabCaption(1) = Ador(0).Value '90
        Ador.MoveNext
        SSTabOferta.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        SSTabOferta.TabCaption(3) = Ador(0).Value
        Ador.MoveNext
        SSTabOferta.TabCaption(4) = Ador(0).Value
        Ador.MoveNext
        Frame9.caption = Ador(0).Value
        fraModSub(0).caption = Ador(0).Value
        Frame16.caption = Ador(0).Value
        Ador.MoveNext
        lblOfePet(0).caption = Ador(0).Value '95 Plantilla
        lblComObj(0).caption = Ador(0).Value
        lblModSub(0).caption = Ador(0).Value
        lblHojaComp(0).caption = Ador(0).Value
        lblHojaComp(2).caption = lblHojaComp(0).caption
        lblHojaComp(4).caption = lblHojaComp(0).caption
        lblAdjudi(0).caption = Ador(0).Value
        lblAdjudi(4).caption = Ador(0).Value
        lblAdjudi(5).caption = Ador(0).Value
        Ador.MoveNext
        Frame10.caption = Ador(0).Value
        fraModSub(1).caption = Ador(0).Value
        Ador.MoveNext
        lblOfePet(1).caption = Ador(0).Value '97
        lblComObj(1).caption = Ador(0).Value
        lblModSub(1).caption = Ador(0).Value
        lblAdjudi(1).caption = Ador(0).Value
        Label50.caption = Ador(0).Value

        Ador.MoveNext
        Frame18.caption = Ador(0).Value '98 E-mail de comunicaci�n de objetivos
        Ador.MoveNext
        lblOfePet(3).caption = Ador(0).Value '99 Plantilla web
        lblComObj(3).caption = Ador(0).Value
        lblModSub(3).caption = Ador(0).Value
        '##############################################################################
        Ador.MoveNext
        Frame1.caption = Ador(0).Value '100 E-mail de petici�n de oferta
        fraModSub(2).caption = Ador(0).Value
        Ador.MoveNext
        lblOfePet(4).caption = Ador(0).Value '101 Asunto
        lblComObj(4).caption = Ador(0).Value
        lblModSub(4).caption = Ador(0).Value
        lblAdjudi(3).caption = Ador(0).Value
        lblbuzperiodo(1).caption = Ador(0).Value
        
        Ador.MoveNext
        Frame2.caption = Ador(0).Value
        Ador.MoveNext
        Frame17.caption = Ador(0).Value
        Ador.MoveNext
        Frame28(0).caption = Ador(0).Value
        Ador.MoveNext
        lblHojaComp(1).caption = Ador(0).Value '105
        lblHojaComp(3).caption = lblHojaComp(1).caption
        lblHojaComp(5).caption = lblHojaComp(1).caption
        Ador.MoveNext
        frameAdj(1).caption = Ador(0).Value '108
        Ador.MoveNext
        lblAdjudi(2).caption = Ador(0).Value '110
        Label51.caption = Ador(0).Value
        lblOfePet(2).caption = Ador(0).Value
        lblModSub(2).caption = Ador(0).Value
        lblComObj(2).caption = Ador(0).Value
        Ador.MoveNext
        frameAdj(2).caption = Ador(0).Value
        Ador.MoveNext
        frameAdj(3).caption = Ador(0).Value
        Ador.MoveNext
        Frame22.caption = Ador(0).Value
        Ador.MoveNext
        Frame23.caption = Ador(0).Value
        Ador.MoveNext
        sstabReu.TabCaption(1) = Ador(0).Value '115
        arRecursosConfGenPedidos(3) = Ador(0).Value
'        fraNotiPed.Caption = Ador(0).Value
        Ador.MoveNext
        fraConv(1).caption = Ador(0).Value
        Ador.MoveNext
        lblReunionValDef(0).caption = Ador(0).Value
        Ador.MoveNext
        lblReunionValDef(1).caption = Ador(0).Value
        Ador.MoveNext
        lblReunionValDef(6).caption = Ador(0).Value
        Ador.MoveNext
        fraConv(2).caption = Ador(0).Value '120
        Ador.MoveNext
        lblReunionValDef(2).caption = Ador(0).Value
        Ador.MoveNext
        lblReunionValDef(3).caption = Ador(0).Value
        Ador.MoveNext
        fraConv(0).caption = Ador(0).Value '123 Convocatoria
        Frame12.caption = Ador(0).Value
        Ador.MoveNext
        lblReunionValDef(4).caption = Ador(0).Value
        label2(0).caption = Ador(0).Value
        arRecursosConfGenPedidos(63) = Ador(0).Value
        arRecursosConfGenPedidos(74) = Ador(0).Value
        arRecursosConfGenPedidos(85) = Ador(0).Value
        arRecursosConfGenPedidos(71) = Ador(0).Value
        Ador.MoveNext
        chkSoloConvResp.caption = Ador(0).Value '125
        Ador.MoveNext
        Frame4.caption = Ador(0).Value
        Ador.MoveNext
        lblReunionPlantillas(0).caption = Ador(0).Value
        Ador.MoveNext
        lblReunionPlantillas(1).caption = Ador(0).Value  '128 Carta
        lblReunionPlantillas(3).caption = Ador(0).Value
        ' plantillas pedidos
        arRecursosConfGenPedidos(61) = Ador(0).Value
        arRecursosConfGenPedidos(72) = Ador(0).Value
        arRecursosConfGenPedidos(77) = Ador(0).Value
        arRecursosConfGenPedidos(80) = Ador(0).Value
        Ador.MoveNext
        ' plantillas pedidos
        arRecursosConfGenPedidos(62) = Ador(0).Value
        arRecursosConfGenPedidos(73) = Ador(0).Value
        arRecursosConfGenPedidos(78) = Ador(0).Value
        arRecursosConfGenPedidos(81) = Ador(0).Value
        arRecursosConfGenPedidos(83) = Ador(0).Value
        Ador.MoveNext
        Frame13.caption = Ador(0).Value '130
        Ador.MoveNext
        Frame15.caption = Ador(0).Value
        Ador.MoveNext
        Frame11(0).caption = Ador(0).Value
        tabCONFGEN.TabCaption(8) = Ador(0).Value
        SSTabOferta.TabCaption(5) = Ador(0).Value
        Ador.MoveNext
        chkActivar(0).caption = Ador(0).Value
        Ador.MoveNext
        chkActivar(1).caption = Ador(0).Value
        Ador.MoveNext
        fraCarpetas(0).caption = Ador(0).Value '135
        Ador.MoveNext
        tabOpciones.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        chkCorreo(0).caption = Ador(0).Value
        Ador.MoveNext
        chkCorreo(1).caption = Ador(0).Value
        Ador.MoveNext
        optMapi(0).caption = Ador(0).Value
        Ador.MoveNext
        optMapi(1).caption = Ador(0).Value '140
        Ador.MoveNext
        chkCorreo(4).caption = Ador(0).Value
        Ador.MoveNext
        fraLenguaje(0).caption = Ador(0).Value ' idioma
        Ador.MoveNext
        tabCONFGEN.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        tabCONFGEN.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        tabCONFGEN.TabCaption(2) = Ador(0).Value '145
        Ador.MoveNext
        tabCONFGEN.TabCaption(3) = Ador(0).Value
        Ador.MoveNext
        tabCONFGEN.TabCaption(4) = Ador(0).Value
        Ador.MoveNext
        tabCONFGEN.TabCaption(6) = Ador(0).Value
        Ador.MoveNext
        tabCONFGEN.TabCaption(7) = Ador(0).Value
        Ador.MoveNext
        cmdEdicion.caption = Ador(0).Value '150
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmCONFGEN.caption = Ador(0).Value
        Ador.MoveNext
        cdlArchivo.DialogTitle = Ador(0).Value
        arRecursosConfGenPedidos(86) = Ador(0).Value
        Ador.MoveNext
        lblReunionValDef(5).caption = Ador(0).Value '155
        Ador.MoveNext
        fraLenguaje(1).caption = Ador(0).Value
        Ador.MoveNext
        fraLenguaje(0).caption = Ador(0).Value
        Ador.MoveNext
        lblIdiPers(0).caption = Ador(0).Value
        lblIdiPers(2).caption = Ador(0).Value
        lblIdiPers(1).caption = Ador(0).Value
        lblIdiPers(3).caption = Ador(0).Value
        arRecursosConfGenPedidos(38) = Ador(0).Value
                
        arRecursosConfGenPedidos(9) = Ador(0).Value
        lblIdiConv.caption = Ador(0).Value
        lblbuzperiodo(4).caption = Ador(0).Value
        arRecursosConfGenPedidos(51) = Ador(0).Value
        Ador.MoveNext
        sdbcConPres.AddItem Ador(0).Value & Chr(m_lSeparador) & CStr(1)
        ConPresupuRow = Ador(0)
        lblPresupuesto(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbcConPres.AddItem Ador(0).Value & Chr(m_lSeparador) & CStr(2) '160
        Ador.MoveNext
        fraBuzPeriodo.caption = Ador(0).Value '162
        Ador.MoveNext
        lblbuzperiodo(0).caption = Ador(0).Value
        Ador.MoveNext
        fraLeerOfer.caption = Ador(0).Value
        Ador.MoveNext
        opLeidas.caption = Ador(0).Value
        Ador.MoveNext
        opNoLeidas.caption = Ador(0).Value
        Ador.MoveNext
        opLeerTodas.caption = Ador(0).Value
        Ador.MoveNext
        sSemana = Ador(0).Value
        Ador.MoveNext
        sSemanas = Ador(0).Value
        Ador.MoveNext
        sMes = Ador(0).Value '170
        Ador.MoveNext
        sMeses = Ador(0).Value
        Ador.MoveNext
        fraMAXADJUN.caption = Ador(0).Value
        Ador.MoveNext
        fraAdjEspec.caption = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(43) = Ador(0).Value
        arRecursosConfGenPedidos(46) = Ador(0).Value
        Ador.MoveNext
        opAdjAutomat(1).caption = Ador(0).Value '175
        arRecursosConfGenPedidos(44) = Ador(0).Value
        arRecursosConfGenPedidos(47) = Ador(0).Value
        Ador.MoveNext
        opAdjAutomat(2).caption = Ador(0).Value
        arRecursosConfGenPedidos(45) = Ador(0).Value
        arRecursosConfGenPedidos(48) = Ador(0).Value
        Ador.MoveNext
        sdbcConPres.AddItem Ador(0).Value & Chr(m_lSeparador) & CStr(3)
        Ador.MoveNext
        sdbcConPres.AddItem Ador(0).Value & Chr(m_lSeparador) & CStr(4)
        Ador.MoveNext
        chkConPres.caption = Ador(0).Value '179
        Ador.MoveNext
        lblPresupuesto(0).caption = Ador(0).Value '185
        Ador.MoveNext
        chkRegGest(9).caption = Ador(0).Value '186
        Ador.MoveNext
        tabCONFGEN.TabCaption(5) = Ador(0).Value '188
        Me.fraConfProce(4).caption = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(0) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(39) = Ador(0).Value '190
        Ador.MoveNext
        arRecursosConfGenPedidos(1) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(34) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(53) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(55) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(56) = Ador(0).Value  '195
        Ador.MoveNext
        arRecursosConfGenPedidos(76) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(79) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(57) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(82) = Ador(0).Value
        '##############################################################################
        Ador.MoveNext
        arRecursosConfGenPedidos(84) = Ador(0).Value & ":" '200
        Ador.MoveNext
        arRecursosConfGenPedidos(58) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(75) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(60) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(59) = Ador(0).Value
        Ador.MoveNext
        fraGrupoDefecto.caption = Ador(0).Value
        Ador.MoveNext
        lblGrupoDef(2).caption = Ador(0).Value '206
        Ador.MoveNext
        chkRegGest(10).caption = Ador(0) '208
        Ador.MoveNext
        SSTabGestion.TabCaption(1) = Ador(0).Value  '209 Configuraci�n de proceso por defecto
        Ador.MoveNext
        SSTabGestion.TabCaption(4) = Ador(0).Value '210 Avisos
        Ador.MoveNext
        fraPonderacion.caption = Ador(0).Value
        Ador.MoveNext
        chkPonderacion(0).caption = Ador(0).Value
        Ador.MoveNext
        chkSolOfe(0).caption = Ador(0).Value
        Ador.MoveNext
        chkSolOfe(1).caption = Ador(0).Value
        Ador.MoveNext
        chkSolOfe(2).caption = Ador(0).Value '215
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        fraSolOfe.caption = Ador(0).Value
        Ador.MoveNext
        fraAmbitoDatos.caption = Ador(0).Value '220
        Ador.MoveNext
        For i = 0 To 5
            sDatos(i) = Ador(0).Value
            Ador.MoveNext
        Next i
        sDatos(6) = gParametrosGenerales.gsPlurPres1
        sDatos(7) = gParametrosGenerales.gsPlurPres2
        sDatos(8) = gParametrosGenerales.gsPlurPres3
        sDatos(9) = gParametrosGenerales.gsPlurPres4
        For i = 0 To 4
            sdbgAmbitoDatos.Columns.Item(i).caption = Ador(0).Value '227 - 231
            Ador.MoveNext
        Next i
        For i = 3 To 5
            chkSolOfe(i).caption = Ador(0).Value '232 - 234
            Ador.MoveNext
        Next i
        SSTabHabSub.TabCaption(0) = Ador(0).Value '235
        Ador.MoveNext
        SSTabHabSub.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblModSub(5).caption = Ador(0).Value
        Ador.MoveNext
        chkInicio.caption = Ador(0).Value '238
        Ador.MoveNext
        For i = 0 To 7
            If i = 4 Then '243
                label2(17).caption = Ador(0).Value
                label2(18).caption = Ador(0).Value
            End If
            sFormatoTextoTAntelacion(i) = Ador(0).Value  '239 - 246
            Ador.MoveNext
        Next i
        
        sIdiAvisoAdj = Ador(0).Value '247
        Ador.MoveNext
        sIdiAvisoDepub = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        
        sIdiAntelacion = Ador(0).Value
        
        Ador.MoveNext
        sDia = Ador(0).Value
        Ador.MoveNext
        sDias = Ador(0).Value
        Ador.MoveNext
        sCap(54) = Ador(0).Value
        Ador.MoveNext
        lblOblDist(0).caption = Ador(0).Value
        Ador.MoveNext
        chkRegGest(11).caption = Ador(0).Value '255
        
        Ador.MoveNext
        chkRecordatorio.caption = Ador(0).Value
        Ador.MoveNext
        chkCierre.caption = Ador(0).Value
        Ador.MoveNext
        fraTiempoAnte(1).caption = Ador(0).Value
        Ador.MoveNext
        Frame28(1).caption = Ador(0).Value
        Ador.MoveNext
        chkCorreo(5).caption = Ador(0).Value '260
        Ador.MoveNext
        fraCarpetas(1).caption = Ador(0).Value '262
        Ador.MoveNext
        sDatos(10) = Ador(0).Value 'solicitud de compras
        Ador.MoveNext
        fraAdminPub.caption = Ador(0).Value
        Ador.MoveNext
        'chkAdminPub.caption = Ador(0).Value '265
        chkPonderacion(1).caption = Ador(0).Value  '265
        Ador.MoveNext
        fraManten(2).caption = Ador(0).Value
        Ador.MoveNext
        chkSugerir.caption = Ador(0).Value
        
        Ador.MoveNext
        Ador.MoveNext
        chkPlantAdj(1).caption = Ador(0).Value
        Ador.MoveNext
        chkPlantAdj(0).caption = Ador(0).Value '270
        
        Ador.MoveNext
        arRecursosConfGenPedidos(7) = Ador(0).Value  '271 Habilitar campo para codificaci�n personalizada de pedidos
        Ador.MoveNext
        arRecursosConfGenPedidos(10) = Ador(0).Value   '272 Nombre
        arRecursosConfGenPedidos(52) = Ador(0).Value   '272 Nombre
        Ador.MoveNext
        
        'El 273 se elimino de la BBDD y de la aplicaci�n era el limite en KB para archivos adjuntos a pedidos
                
        arRecursosConfGenPedidos(40) = Ador(0).Value '274
        Ador.MoveNext
        arRecursosConfGenPedidos(41) = Ador(0).Value '275
        Ador.MoveNext
        lblSMTP(0).caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(1).caption = Ador(0).Value
        Ador.MoveNext
        chkCorreo(2).caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        optAutenticacion(0).caption = Ador(0).Value '280
        Ador.MoveNext
        optAutenticacion(1).caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        lblSMTP(2).caption = Ador(0).Value
        Ador.MoveNext
        lblSMTP(3).caption = Ador(0).Value '285
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        tabOpciones.TabCaption(1) = Ador(0).Value    '288 Idiomas
        Ador.MoveNext
        chkNoPublicarFinSum.caption = Ador(0).Value
        
        Ador.MoveNext
        chkRegGest(12).caption = Ador(0).Value
        
        Ador.MoveNext
        SSTabGestion.TabCaption(3) = Ador(0).Value '291 Otras reglas
        
        Ador.MoveNext
        opAdjAutomat(3).caption = Ador(0).Value
        Ador.MoveNext
        opAdjAutomat(4).caption = Ador(0).Value
        
        Ador.MoveNext
        opAdjAutomat(0).caption = Ador(0).Value
        
        Ador.MoveNext
        lblReunionPlantillas(4).caption = Ador(0).Value & "1:"
        lblReunionPlantillas(5).caption = Ador(0).Value & "2:"
        
        Ador.MoveNext
        arRecursosConfGenPedidos(32) = Ador(0).Value
        
        Ador.MoveNext
        arRecursosConfGenPedidos(54) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(68) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(69) = Ador(0).Value
        lblReunionPlantillas(2).caption = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(70) = Ador(0).Value
        lblReunionPlantillas(6).caption = Ador(0).Value
        '##############################################################################
        Ador.MoveNext
        sCap(55) = Ador(0).Value  'Plantillas de texto
        Ador.MoveNext
        sCap(56) = Ador(0).Value  'Plantillas de html
        
        Ador.MoveNext
        SSTab2.TabCaption(3) = Ador(0).Value 'Campos para proveedores 303
        Ador.MoveNext
        Label5.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiCampo = Ador(0).Value
        Ador.MoveNext
        sdbgCamposERP.Groups(0).caption = Ador(0).Value
        
        Ador.MoveNext
        lblFormatoMail(0).caption = Ador(0).Value  '307 Recibir los emails en formato:
        Ador.MoveNext
        lblFormatoMail(1).caption = Ador(0).Value  '308
        Ador.MoveNext
        optFormatoEMail(0).caption = Ador(0).Value '309 HTML
        Ador.MoveNext
        optFormatoEMail(1).caption = Ador(0).Value '310 txt
        Ador.MoveNext
        Me.chkUnSoloPedido.caption = Ador(0).Value
        Ador.MoveNext
        fraCarpetas(2).caption = Ador(0).Value
        Ador.MoveNext
        optRutaTS(0).caption = Ador(0).Value
        Ador.MoveNext
        optRutaTS(1).caption = Ador(0).Value
        Ador.MoveNext
        Label6.caption = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(8) = Ador(0).Value '316
        Ador.MoveNext
        arRecursosConfGenPedidos(23) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(24) = Ador(0).Value
        If m_bMostrarSolicitud Then
            Ador.MoveNext
            SSTabGestion.TabCaption(5) = Ador(0).Value  '319 Solicitudes de compra
            Ador.MoveNext
            fraSolicit(0).caption = Ador(0).Value  '320 Generaci�n de procesos desde solicitud
            Ador.MoveNext
            fraSolicit(1).caption = Ador(0).Value  '321 Generaci�n de ofertas desde solicitud
            Ador.MoveNext
            optGenerarProc(0).caption = Ador(0).Value  '322 Generar procesos de adjudicaci�n directa
            Ador.MoveNext
            optGenerarProc(1).caption = Ador(0).Value  '323 Generar procesos de reuni�n
            Ador.MoveNext
            chkAdjDirImp.caption = Ador(0).Value  '324 Para importes menores de:
            Ador.MoveNext
            lblPeriodoSolic(0).caption = Ador(0).Value   '325 Per�odo de validez
        Else
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
        End If
        Ador.MoveNext
        sCap(57) = Ador(0).Value  '326
        Ador.MoveNext
        m_sAnyo = Ador(0).Value  '327 a�o
        Ador.MoveNext
        m_sAnyos = Ador(0).Value '328 a�os
        Ador.MoveNext
        chkSolOfe(6).caption = Ador(0).Value '329
        Ador.MoveNext
        arRecursosConfGenPedidos(5) = Ador(0).Value '330
        Ador.MoveNext
        arRecursosConfGenPedidos(11) = Ador(0).Value '331
        Ador.MoveNext
        chkRegGest(23).caption = Ador(0).Value '332
        Ador.MoveNext
        '333
        Ador.MoveNext
        '334
        Ador.MoveNext
        arRecursosConfGenPedidos(27) = Ador(0).Value '335
        Ador.MoveNext
        arRecursosConfGenPedidos(35) = Ador(0).Value '336
        Ador.MoveNext
        'chkRegGest(24).caption = Ador(0).Value '337
        
        Ador.MoveNext
        arRecursosConfGenPedidos(28) = Ador(0).Value '338
        Ador.MoveNext
        '339
        Ador.MoveNext
        chkRegGest(25).caption = Ador(0).Value '340 Control de cambios en las fechas de suministro al traspasar la informaci�n al ERP
        
        Ador.MoveNext
        optGenerarProc(2).caption = Ador(0).Value  '341 Acta en Crystal
        Ador.MoveNext
        optGenerarProc(3).caption = Ador(0).Value '342 Acta en crystal report
        Ador.MoveNext
        sCap(58) = Ador(0).Value '343 Plantillas crystal
        Ador.MoveNext
        fraManten(3).caption = Ador(0).Value    '344
        Ador.MoveNext
        chkCorreo(6).caption = Ador(0).Value    '345
        Ador.MoveNext
        chkCorreo(7).caption = Ador(0).Value    '346
        '___________________________________________
        Ador.MoveNext
        arRecursosConfGenPedidos(30) = Ador(0).Value     '347 Incluir v�as de pago en el pedido
        '�������������������������������������������
        
        Ador.MoveNext
        Frame28(2).caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        arRecursosConfGenPedidos(6) = Ador(0).Value
        Ador.MoveNext
        chkRegGest(26).caption = Ador(0).Value '351 Restringir la asignaci�n de presupuestos a la unidad organizativa de los art�culos
        arRecursosConfGenPedidos(25) = Ador(0).Value '351 Restringir la asignaci�n de presupuestos a la unidad organizativa de los art�culos
        Ador.MoveNext
        frmConvEmail.caption = Ador(0).Value
        'Opciones de seguridad
        Ador.MoveNext
        Frame11(1).caption = Ador(0).Value '353
        Ador.MoveNext
        label2(12).caption = Ador(0).Value '354
        Ador.MoveNext
        label2(15).caption = Ador(0).Value '355
        Ador.MoveNext
        label2(16).caption = Ador(0).Value '356
        Ador.MoveNext
        label2(23).caption = Ador(0).Value '357
        Ador.MoveNext
        label2(21).caption = Ador(0).Value '358
        Ador.MoveNext
        label2(13).caption = Ador(0).Value '359
        Ador.MoveNext
        Ador.MoveNext
        label2(14).caption = Ador(0).Value '361
        Ador.MoveNext
        label2(19).caption = Ador(0).Value '362
        Ador.MoveNext
        label2(22).caption = Ador(0).Value '363
        Ador.MoveNext
        optRutaTS(2).caption = Ador(0).Value '364
        Ador.MoveNext
        optRutaTS(3).caption = Ador(0).Value '365
        Ador.MoveNext
        label2(20).caption = Ador(0).Value '366
        Ador.MoveNext
        lblPresupuesto(4).caption = Ador(0).Value '367
        m_sLitPartida = Ador(0).Value
        Ador.MoveNext
        lblRel(0).caption = Ador(0).Value '368  Nombre para las relaciones entre proveedores (Distribuidores...)
        Ador.MoveNext
        lblRel(1).caption = Ador(0).Value '369  Idioma
        Ador.MoveNext
        lblRel(2).caption = Ador(0).Value '370  Singular
        Ador.MoveNext
        lblRel(3).caption = Ador(0).Value '371  Plural
        Ador.MoveNext
        arRecursosConfGenPedidos(50) = Ador(0).Value '372 Habilitar campo personalizado codificaci�n recepciones
        Ador.MoveNext
        arRecursosConfGenPedidos(2) = Ador(0).Value '373 Recepciones
        Ador.MoveNext
        fraLenguaje(2).caption = Ador(0).Value  '374    Zona horaria
        Ador.MoveNext
        Ador.MoveNext
        optAutenticacion(2).caption = Ador(0).Value '376 Autenticaci�n an�nima
        Ador.MoveNext
        Ador.MoveNext
        Me.lblSMTP(4).caption = Ador(0).Value
        Ador.MoveNext
        Me.lblSMTP(5).caption = Ador(0).Value
        Ador.MoveNext
        Me.lblSMTP(6).caption = Ador(0).Value
        Ador.MoveNext
        Me.lblSMTP(7).caption = Ador(0).Value
        Ador.MoveNext
        chkRegOtraGest.caption = Replace(Ador(0).Value, "###", gParametrosGenerales.gsMONCEN) '"Importe adjudicado m�ximo para adjudicaci�n directa (###):"
        Ador.MoveNext
        sCap(59) = Ador(0).Value
        Ador.MoveNext
        chkRegGest(28).caption = Ador(0).Value
        Ador.MoveNext
        fraCarpetas(1).caption = Ador(0).Value      'TEXTOS: Modulo=9, ID=385   'Ruta para los archivos de emails
        Ador.MoveNext
        arRecursosConfGenPedidos(4) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(0) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(1) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(2) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(3) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(4) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(5) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(6) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(7) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfPedidosCom(8) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(14) = Ador(0).Value 'Activar el Pedido Abierto
        Ador.MoveNext
        arRecursosConfPedidosCom(9) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(64) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(65) = Ador(0).Value & ":"
        Ador.MoveNext
        arRecursosConfGenPedidos(66) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(67) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(12) = Ador(0).Value & ":"
        Ador.MoveNext
        arRecursosConfGenPedidos(13) = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(29) = Ador(0).Value
        Ador.MoveNext
        label2(28).caption = Ador(0).Value
        Ador.MoveNext
        optCalcLinBase(0).caption = Ador(0).Value
        Ador.MoveNext
        optCalcLinBase(1).caption = Ador(0).Value
        Ador.MoveNext
        optCalcLinBase(2).caption = Ador(0).Value
        Ador.MoveNext
        arRecursosConfGenPedidos(49) = Ador(0).Value 'M�dulo 9 - ID: 409
        Ador.MoveNext
        arRecursosConfGenPedidos(42) = Ador(0).Value 'M�dulo 9 - ID: 410
        Ador.MoveNext
        arRecursosConfGenPedidos(33) = Ador(0).Value 'Hitos de facturaci�n
        
        Ador.Close
    End If
    
    m_sIdiCodGrupo = fraGrupoDefecto & " (" & lblGrupoDef(0).caption & ")"
    m_sIdiDenGrupo = fraGrupoDefecto & " (" & lblGrupoDef(1).caption & ")"
        
    arRecursosConfGenPedidos(19) = gParametrosGenerales.gsPlurPres1
    arRecursosConfGenPedidos(20) = gParametrosGenerales.gsPlurPres2
    arRecursosConfGenPedidos(21) = gParametrosGenerales.gsPlurPres3
    arRecursosConfGenPedidos(22) = gParametrosGenerales.gsPlurPres4
    
    sParte = DividirFrase(sCap(52))
    chkRegGest(3).caption = sParte(1) & gParametrosGenerales.gsSingPres1
    arRecursosConfGenPedidos(15) = sParte(1) & gParametrosGenerales.gsSingPres1
    chkRegGest(4).caption = sParte(1) & gParametrosGenerales.gsSingPres2
    arRecursosConfGenPedidos(16) = sParte(1) & gParametrosGenerales.gsSingPres2
    chkRegGest(5).caption = sParte(1) & gParametrosGenerales.gsSingPres3
    arRecursosConfGenPedidos(17) = sParte(1) & gParametrosGenerales.gsSingPres3
    chkRegGest(6).caption = sParte(1) & gParametrosGenerales.gsSingPres4
    arRecursosConfGenPedidos(18) = sParte(1) & gParametrosGenerales.gsSingPres4
    
    sParte = DividirFrase(sCap(54))
    lblOblDist(1).caption = sParte(1) & gParametrosGenerales.gsSingPres1
    arRecursosConfGenPedidos(26) = sParte(1) & gParametrosGenerales.gsSingPres1
    lblOblDist(2).caption = sParte(1) & gParametrosGenerales.gsSingPres2
    arRecursosConfGenPedidos(31) = sParte(1) & gParametrosGenerales.gsSingPres2
    lblOblDist(3).caption = sParte(1) & gParametrosGenerales.gsSingPres3
    arRecursosConfGenPedidos(36) = sParte(1) & gParametrosGenerales.gsSingPres3
    lblOblDist(4).caption = sParte(1) & gParametrosGenerales.gsSingPres4
    arRecursosConfGenPedidos(37) = sParte(1) & gParametrosGenerales.gsSingPres4
        
    ctlConfPed.CargarRecursos arRecursosConfGenPedidos, arRecursosConfPedidosCom
    
    m_bComboTAntelacion = False
    Set Ador = Nothing
End Sub

Private Sub ctrlConfGenAvisos_AbrirDialogoArchivo(Index As Integer)
    On Error GoTo Error:
    
    If Index = 0 Then
        cdlArchivo.Filter = sCap(56) & " (*.htm)|*.htm"
        cdlArchivo.DefaultExt = "htm"
    ElseIf Index = 1 Then
        cdlArchivo.Filter = sCap(55) & " (*.txt)|*.txt"
        cdlArchivo.DefaultExt = "txt"
    End If
    
    cdlArchivo.Action = 1
    Me.ctrlConfGenAvisos.PlantillaDespub(Index) = cdlArchivo.filename
            
Error:
    If err.Number = cdlCancel Then
        Exit Sub
    Else
        Resume Next
    End If
End Sub

''' <summary>procedimiento de carga inicial del formulario</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1 sec</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub Form_Load()
    Dim i As Integer, j As Integer
    Dim oIdioma As CIdioma
    
    Set oIdiomas = oGestorParametros.DevolverIdiomas
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    'Me.Height = 8190
    Me.Height = 8895
    Me.Width = 11085
    
    'Inicializaci�n controles
    ctrlConfGenAvisos.idioma = gParametrosInstalacion.gIdioma
    Set ctrlConfGenAvisos.GestorIdiomas = oGestorIdiomas
    Set oLiteralesAModificar = New CLiterales
    
    ctrlConfGenAvisos.Initialize
    ctlConfPed.Initialize oIdiomas, oGestorParametros, gParametrosGenerales, basPublic.gParametrosInstalacion.gIdioma, (gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) Or gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_Aprov)), _
        gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Aprov) Or gParametrosIntegracion.gaExportar(EntidadIntegracion.Rec_Directo) Or gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Aprov) Or gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Rec_Directo)
    
    m_bMostrarSolicitud = True
        
    'Si no se tiene acceso a GS ocultar las pesta�as Gesti�n, Ofertas y Reuniones
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        tabCONFGEN.TabVisible(2) = False
        tabCONFGEN.TabVisible(3) = False
        tabCONFGEN.TabVisible(4) = False
    End If
    
    'SE DEJA VISIBLE PQ EN ESA RUTA ESTA TB PLANTILLA DE CAMBIO DE RESPONSABLE
    'Comprueba si se mostrar� o no la solicitud de compra
    If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
        m_bMostrarSolicitud = False
        fraCarpetas(1).Visible = True
        m_bMostrarCarpetas = True
        SSTabGestion.TabVisible(5) = False
    Else
        fraCarpetas(1).Visible = True
        m_bMostrarSolicitud = True
        m_bMostrarCarpetas = True
        SSTabGestion.TabVisible(5) = True
    End If
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas(False, False, True)
    
    PonerFieldSeparator Me
    
    CargarRecursos
    CargarComboTZ
    
    If gParametrosGenerales.giINSTWEB <> ConPortal Then
        fraLenguaje(1).Visible = False
        chkRegGest(9).Visible = False
        chkRegGest(10).Top = chkRegGest(9).Top
    Else
        Set oIdiomasPortal = oGestorParametros.DevolverIdiomas(True)
        i = 0
        
        For Each oIdioma In oIdiomasPortal
            sdbcIdiomas(1).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
            If oIdioma.Cod = gParametrosGenerales.gIdiomaPortal Then
                idiIdiomasPortalRow = sdbcIdiomas(1).GetBookmark(i)
            End If
            
            i = i + 1
        Next
    End If
    
    If gParametrosGenerales.giINSTWEB = 0 Then
        lblValDef(3).Visible = False
        sdbcEstCod(1).Visible = False
        sdbcEstDen(1).Visible = False
        SSTabHabSub.TabVisible(1) = False
        lblComObj(3).Visible = False
        lblOfePet(3).Visible = False
        cmdPlantilla(7).Visible = False
        cmdPlantilla(3).Visible = False
        txtPlantilla(7).Visible = False
        txtPlantilla(3).Visible = False
        Frame2.Visible = False
        Frame17.Height = Frame17.Height - 330
        picPetOfe(2).Height = picPetOfe(2).Height - 330
        picObj2.Height = picObj2.Height - 330
        Frame18.Top = Frame18.Top - 325
        Frame10.Height = Frame10.Height - 330
        Frame1.Top = Frame1.Top - 325
    End If
    
    
    If gParametrosGenerales.gbPedidosDirectos Then
        Me.fraConfProce(4).Visible = True
    Else
        Me.fraConfProce(4).Visible = False
    End If
    
    If ADMIN_OBLIGATORIO = True Then
        SSTabHabSub.TabVisible(1) = False
    End If
        
    'LDAP, usuarios locales y windows no se ve el control pq lo controla el propio sistema
    If Not (gParametrosGenerales.giWinSecurity = Fullstep Or gParametrosGenerales.giWinSecurityWeb = Fullstep) Then
        'chkLogPreBloq.Visible = False
        chkActivar(0).Visible = False
        sldLogPreBloq.Visible = False
        lblIntentos.Visible = False
    End If
    
    
    Set oFos = New FileSystemObject
    
    cmdEdicion.Left = tabCONFGEN.Left + tabCONFGEN.Width - cmdEdicion.Width
    
    ''' Dependiendo del n�mero de niveles, quitar denominaciones
    
    iNEO = gParametrosGenerales.giNEO
    
    For i = iNEO + 1 To 3
        txtDenUO(i).Visible = False
        txtAbrUO(i).Visible = False
        lblUO(i).Visible = False
        lblUOAbr(i).Visible = False
        fraUO.Height = fraUO.Height - 450
    Next
        
    cmbOblDist(0).AddItem ""
    For i = 1 To gParametrosGenerales.giNEO
        cmbOblDist(0).AddItem i
    Next i
    
    iNEM = gParametrosGenerales.giNEM
    
    For i = iNEM + 1 To 4
        
        txtDenMat(i).Visible = False
        txtAbrMat(i).Visible = False
        lblMat(i).Visible = False
        lblAbrMat(i).Visible = False
        fraMAT.Height = fraMAT.Height - 450
        
    Next
        
    cdlArchivo.FLAGS = cdlOFNFileMustExist & cdlOFNHideReadOnly
    
    With cmbDiaSemana
        .AddItem sCap(39) 'Lunes
        .AddItem sCap(40)
        .AddItem sCap(41)
        .AddItem sCap(42)
        .AddItem sCap(43)
        .AddItem sCap(44)
        .AddItem sCap(45)
    End With
    
    With cmbNumSemanas
        .AddItem sCap(46)
        sParte = DividirFrase(sCap(47))
        For i = 2 To 12
            .AddItem sParte(1) & i & sParte(2)
        Next i
    End With
    
    Criterios(0) = gParametrosGenerales.gsDEN_GMN1
    Criterios(1) = sCap(48)
    Criterios(2) = sCap(49)
    Criterios(3) = sCap(50)
    Criterios(4) = sCap(51)
    
    For i = 0 To 1
        With cmbCriterio(i)
            For j = 0 To 4
                .AddItem Criterios(j)
            Next j
        End With
    Next i
    
    i = 0
    
    For Each oIdioma In oIdiomas
        sdbcIdiomas(0).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(1).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(0).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(2).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(3).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(6).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(7).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        sdbcIdioma(8).AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
        
        If oIdioma.Cod = basPublic.gParametrosInstalacion.gIdioma Then
            
            idiUORow = sdbcIdioma(1).GetBookmark(i)
            idiMatRow = sdbcIdioma(0).GetBookmark(i)
            idiPresRow = sdbcIdioma(2).GetBookmark(i)
            idiOtrosRow = sdbcIdioma(3).GetBookmark(i)
            idiAprovRow = ctlConfPed.sdbcIdioma4.GetBookmark(i)
            idiPedAbiertoRow = ctlConfPed.sdbcIdioma5.GetBookmark(i)
            idiConvocatoriaRow = sdbcIdioma(6).GetBookmark(i)
            idiConvocatoriaSubjectRow = sdbcIdioma(7).GetBookmark(i)
            idiRelacionRow = sdbcIdioma(8).GetBookmark(i)
            idiRecepRow = ctlConfPed.sdbcIdioma9.GetBookmark(i)
        End If
        
        If gParametrosGenerales.gIdioma = oIdioma.Cod Then
            idiIdiomasRow = sdbcIdiomas(0).GetBookmark(i)
        End If
        i = i + 1
        
    Next
    
    'Rellenar combo de Antig�edad de las ofertas a mostrar
    Dim iBuzper As Integer
    With sdbcAntigOfer
        iBuzper = 7
        i = 1
        .AddItem i & " " & sSemana & Chr(m_lSeparador) & iBuzper
        For i = 2 To 3
           .AddItem i & " " & sSemanas & Chr(m_lSeparador) & iBuzper * i
        Next i

        iBuzper = 30
        i = 1
        .AddItem i & " " & sMes & Chr(m_lSeparador) & iBuzper
        For i = 2 To 6
            .AddItem i & " " & sMeses & Chr(m_lSeparador) & iBuzper * i
        Next i
    End With
    
    If m_bMostrarSolicitud Then
        With sdbcPeriodoSolic
            iBuzper = 7
            i = 1
            .AddItem i & " " & sSemana & Chr(m_lSeparador) & iBuzper
            For i = 2 To 3
               .AddItem i & " " & sSemanas & Chr(m_lSeparador) & iBuzper * i
            Next i
    
            iBuzper = 30
            i = 1
            .AddItem i & " " & sMes & Chr(m_lSeparador) & iBuzper
            For i = 2 To 6
                .AddItem i & " " & sMeses & Chr(m_lSeparador) & iBuzper * i
            Next i
            
            iBuzper = 365
            i = 1
            .AddItem i & " " & m_sAnyo & Chr(m_lSeparador) & iBuzper
            For i = 2 To 3
                .AddItem i & " " & m_sAnyos & Chr(m_lSeparador) & iBuzper * i
            Next i
            
        End With
    End If
    
    RellenarComboUnidad
    
    MostrarParametros
    
    ModoConsulta
        
    If gParametrosGenerales.gbPedidosAprov = False And gParametrosGenerales.gbPedidosDirectos = False Then tabCONFGEN.TabVisible(5) = False
   
    txtGrupoDefecto(0).MaxLength = basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE
        
    If ADMIN_OBLIGATORIO Then
        'No aparece la check de "Proceso de admin.pub" porque ser�n obligatoriamente de admin.p�blica
        fraAmbitoDatos.Height = 3100
        picAmbitoDatos.Height = 2700
        sdbgAmbitoDatos.Height = 2700
        fraAdminPub.Visible = False
        fraSolOfe.Top = fraSolOfe.Top - fraAdminPub.Height
    Else
        If basParametros.gParametrosGenerales.gbAdminPublica = True Then
            'Check para indicar si ser� o no un proceso de administraci�n p�blica:
            fraAdminPub.Visible = True
        Else
            fraAmbitoDatos.Height = 3100
            picAmbitoDatos.Height = 2700
            sdbgAmbitoDatos.Height = 2700
            fraAdminPub.Visible = False
            fraSolOfe.Top = fraSolOfe.Top - fraAdminPub.Height
        End If
    End If
    
    'Tarea 3376 - Obligar a indicar solicitud de compras
    Me.picConf(2).Top = Me.fraAmbitoDatos.Top + fraAmbitoDatos.Height + 10
        
    If gParametrosIntegracion.gaExportar(EntidadIntegracion.Adj) Or gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Adj) Then
        chkRegGest(25).Visible = True
    Else
        chkRegGest(25).Visible = False
    End If
    SSTab2.Tab = 0
    
    InicializarValoresSubasta
End Sub

''' <summary>Carga los combos de zonas horarias</summary>
Private Sub CargarComboTZ()
    Dim dcListaZonasHorarias As Dictionary  'TIME_ZONE_INFO
    Dim iIndex As Integer
        
    On Error GoTo Error

    Set dcListaZonasHorarias = ObtenerZonasHorarias
    
    For iIndex = 0 To dcListaZonasHorarias.Count - 1
        sdbcIdiomas(2).AddItem dcListaZonasHorarias.Items(iIndex) & Chr(m_lSeparador) & dcListaZonasHorarias.Keys(iIndex)
    Next
    sdbcIdiomas(2).ListAutoPosition = True
    
    Set dcListaZonasHorarias = Nothing
    
    Exit Sub
Error:
    oMensajes.MensajeOKOnly err.Number & " - " & err.Description
End Sub

''' <summary>Inicializa los valores de las variables de par�metros de subasta</summary>
''' <remarks>Llamada desde Form_Load, cmdCancelar_Click</remarks>
Private Sub InicializarValoresSubasta()
    m_iSubTipo = gParametrosGenerales.gCPSubTipo
    m_iSubModo = gParametrosGenerales.gCPSubModo
    m_vSubDuracion = gParametrosGenerales.gCPSubDuracion
    m_iSubastaEspera = gParametrosGenerales.gCPSubastaEspera
    m_bSubPublicar = gParametrosGenerales.gCPSubPublicar
    m_bSubNotifEventos = gParametrosGenerales.gCPSubNotifEventos
    m_bSubastaProve = gParametrosGenerales.gCPSubastaProve
    m_bSubVerDesdePrimPuja = gParametrosGenerales.gCPSubVerDesdePrimPuja
    m_bSubastaPrecioPuja = gParametrosGenerales.gCPSubastaPrecioPuja
    m_bSubastaPujas = gParametrosGenerales.gCPSubastaPujas
    m_bSubastaBajMinPuja = gParametrosGenerales.gCPSubastaBajMinPuja
    m_iSubBajMinGanTipo = gParametrosGenerales.gCPSubBajMinProveTipo
    m_vSubBajMinGanProcVal = gParametrosGenerales.gCPSubBajMinGanProcVal
    m_vSubBajMinGanGrupoVal = gParametrosGenerales.gCPSubBajMinGanGrupoVal
    m_vSubBajMinGanItemVal = gParametrosGenerales.gCPSubBajMinGanItemVal
    m_bSubBajMinProve = gParametrosGenerales.gCPSubBajMinProve
    m_iSubBajMinProveTipo = gParametrosGenerales.gCPSubBajMinProveTipo
    m_vSubBajMinProveProcVal = gParametrosGenerales.gCPSubBajMinProveProcVal
    m_vSubBajMinProveGrupoVal = gParametrosGenerales.gCPSubBajMinProveGrupoVal
    m_vSubBajMinProveItemVal = gParametrosGenerales.gCPSubBajMinProveItemVal
    Set m_dcSubTextosFin = gParametrosGenerales.gCPSubTextoFin
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oLiteralesAModificar = Nothing
    Set oFos = Nothing
    Set oIdiomas = Nothing
    Set oIdiomasPortal = Nothing
    Set oUnidades = Nothing
    Set oPagos = Nothing
    Set oMonedas = Nothing
    Set oDestinos = Nothing
    Set oPaises = Nothing
    Set m_dcSubTextosFin = Nothing
    
    Me.Visible = False
End Sub

''' <summary>
''' Dependiendo de si se pulsa basica o windows o anonima, activa unos campos u otros
''' </summary>
''' <param name="Index">Cual de los radiobutton se ha pulsado</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub optAutenticacion_Click(Index As Integer)
    If Index = 0 Then 'BASICA
        If optAutenticacion(0).Value = True Then
            Me.txtSMTP(4).Visible = True
            Me.txtSMTP(3).Visible = True
            
            Me.lblSMTP(4).Visible = True
            Me.lblSMTP(3).Visible = True
            
            Me.txtSMTP(2).Visible = False
            Me.txtSMTP(5).Visible = False
            
            Me.lblSMTP(2).Visible = False
            Me.lblSMTP(5).Visible = False
                        
            Me.txtSMTP(3).Top = Me.txtSMTP(2).Top
            Me.lblSMTP(3).Top = Me.lblSMTP(2).Top
            
            fraOpciones(4).Height = 1000
                   
        End If
    
    ElseIf Index = 1 Then 'windows
        If optAutenticacion(1).Value = True Then
            
            Me.txtSMTP(4).Visible = True
            Me.txtSMTP(2).Visible = True
            Me.txtSMTP(3).Visible = True
            Me.txtSMTP(5).Visible = True
            
            Me.txtSMTP(4).Text = gParametrosGenerales.gsSMTPCuentaMail
            Me.txtSMTP(2).Text = gParametrosGenerales.gsSMTPUser
            Me.txtSMTP(3).Text = gParametrosGenerales.gsSMTPPwd
            Me.txtSMTP(5).Text = gParametrosGenerales.gsSMTPDominio
            
            Me.lblSMTP(4).Visible = True
            Me.lblSMTP(2).Visible = True
            Me.lblSMTP(3).Visible = True
            Me.lblSMTP(5).Visible = True
            
            Me.txtSMTP(3).Top = 960
            Me.lblSMTP(3).Top = 990
                        
            fraOpciones(4).Height = 1800
            
        End If
    Else 'anonima
        If optAutenticacion(2).Value = True Then
            Me.txtSMTP(4).Visible = True
            
            Me.txtSMTP(4).Text = gParametrosGenerales.gsSMTPCuentaMail
            
            Me.lblSMTP(4).Visible = True
            
            Me.fraOpciones(4).Height = 645
            
            Me.txtSMTP(2).Visible = False
            Me.txtSMTP(3).Visible = False
            Me.txtSMTP(5).Visible = False
            
            Me.lblSMTP(2).Visible = False
            Me.lblSMTP(3).Visible = False
            Me.lblSMTP(5).Visible = False
            
        End If
    End If
    
    Me.fraOpciones(4).Top = Me.fraOpciones(3).Top + Me.fraOpciones(3).Height + 100
    
End Sub

Private Sub optCalcLinBase_Click(Index As Integer)
If Index = 2 Then
    txtNumOfe.Enabled = True
Else
    txtNumOfe.Text = ""
    txtNumOfe.Enabled = False
End If
End Sub

Private Sub optMapi_Click(Index As Integer)
    chkCorreo(4).Enabled = True
    chkCorreo(5).Enabled = True
End Sub

Private Sub chkPlantAdj_Click(Index As Integer)
    Select Case Index
        Case 0:
            If chkPlantAdj(0).Value = vbChecked And chkPlantAdj(1).Value = vbChecked Then
                chkPlantAdj(1).Value = vbUnchecked
            End If
        Case 1:
            If chkPlantAdj(0).Value = vbChecked And chkPlantAdj(1).Value = vbChecked Then
                chkPlantAdj(0).Value = vbUnchecked
            End If

    End Select
End Sub

Private Sub sdbcAntigOfer_Validate(Cancel As Boolean)
    Dim bm As Variant
    Dim bExiste As Boolean
    Dim i As Integer
    Dim Fila As Integer
    
    Fila = sdbcAntigOfer.Row
    
    If picMat.Enabled Then
        'Estamos en modo consulta
        bExiste = False
        sdbcAntigOfer.MoveFirst
        
        For i = 0 To sdbcAntigOfer.Rows - 1
                bm = sdbcAntigOfer.GetBookmark(i)
                If sdbcAntigOfer.Text = sdbcAntigOfer.Columns(0).Value Then
                    bExiste = True
                    sdbcAntigOfer.Bookmark = bm
                    Exit For
                End If
                sdbcAntigOfer.MoveNext
        Next i
        
        If Not bExiste Then
            sdbcAntigOfer.Text = ""
        Else
            sdbcAntigOfer.Row = Fila
            sdbcAntigOfer.Columns(0).Value = sdbcAntigOfer.Text
        End If
    End If
End Sub

Private Sub sdbcConPres_DropDown()
    If oIdiomas Is Nothing Then
     Exit Sub
    End If
End Sub

Private Sub sdbcIdiMat_CloseUp()
Dim oLiterales As CLiterales

    If picMat.Enabled = False Then
        'Estamos en modo consulta
        If sdbcIdioma(0).Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(8, 15, sdbcIdioma(0).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenMat(1).Text = oLiterales.Item(1).Den
                txtAbrMat(1).Text = oLiterales.Item(2).Den
                txtDenMat(2).Text = oLiterales.Item(3).Den
                txtAbrMat(2).Text = oLiterales.Item(4).Den
                txtDenMat(3).Text = oLiterales.Item(5).Den
                txtAbrMat(3).Text = oLiterales.Item(6).Den
                txtDenMat(4).Text = oLiterales.Item(7).Den
                txtAbrMat(4).Text = oLiterales.Item(8).Den
            Else
                txtDenMat(1).Text = ""
                txtAbrMat(1).Text = ""
                txtDenMat(2).Text = ""
                txtAbrMat(2).Text = ""
                txtDenMat(3).Text = ""
                txtAbrMat(3).Text = ""
                txtDenMat(4).Text = ""
                txtAbrMat(4).Text = ""
            End If
        End If
            
        Set oLiterales = Nothing
    Else
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
        
        If oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(8)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(8, 15, sdbcIdioma(0).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
            
                txtDenMat(1).Text = oLiterales.Item(1).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(1).Text, 8
                txtAbrMat(1).Text = oLiterales.Item(2).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(1).Text, 9
               
                txtDenMat(2).Text = oLiterales.Item(3).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(2).Text, 10
                
                txtAbrMat(2).Text = oLiterales.Item(4).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(2).Text, 11
               
                txtDenMat(3).Text = oLiterales.Item(5).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(3).Text, 12
               
                txtAbrMat(3).Text = oLiterales.Item(6).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(3).Text, 13
               
                txtDenMat(4).Text = oLiterales.Item(7).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(4).Text, 14
               
                txtAbrMat(4).Text = oLiterales.Item(8).Den
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(4).Text, 15
               
            Else
                txtDenMat(1).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(1).Text, 8
                txtAbrMat(1).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(1).Text, 9
               
                txtDenMat(2).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(2).Text, 10
                
                txtAbrMat(2).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(2).Text, 11
               
                txtDenMat(3).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(3).Text, 12
               
                txtAbrMat(3).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(3).Text, 13
               
                txtDenMat(4).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtDenMat(4).Text, 14
               
                txtAbrMat(4).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(0).Columns("ID").Value, txtAbrMat(4).Text, 15
               
                
            End If
            
        Else
            
            'Cargamos desde oLiteralesAModificar
            txtDenMat(1).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(8)).Den
            txtDenMat(2).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(10)).Den
            txtDenMat(3).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(12)).Den
            txtDenMat(4).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(14)).Den
            txtAbrMat(1).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(9)).Den
            txtAbrMat(2).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(11)).Den
            txtAbrMat(3).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(13)).Den
            txtAbrMat(4).Text = oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(15)).Den
            
    End If
End If

End Sub

Private Sub sdbcIdioma_CloseUp(Index As Integer)
    Select Case Index
        Case 0
            sdbcIdiMat_CloseUp
        Case 1
            sdbcIdiUo_CloseUp
        Case 2
            sdbcIdiPres_CloseUp
        Case 3
            sdbcIdiOtros_CloseUp
        Case 6
            sdbcIdiConvocatoria_CloseUp
        Case 7
            sdbcIdiConvocatoriaSubject_CloseUp
        Case 8
            sdbcIdiNombreRel_CloseUp
    End Select
End Sub

''' <summary>
''' Cambia los textos relacionados a los correspondientes para el nuevo idioma seleccionado
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbcIdiNombreRel_CloseUp()
 Dim oLiterales As CLiterales

    If picMat.Enabled = False Then
        
        'Estamos en modo consulta
        
        If sdbcIdioma(8).Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(45, 46, sdbcIdioma(8).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenPres(2).Text = oLiterales.Item(1).Den
                txtDenPres(3).Text = oLiterales.Item(2).Den
            Else
                txtDenPres(2).Text = ""
                txtDenPres(3).Text = ""
            End If
        End If
        Set oLiterales = Nothing
    Else
           
        'MODO EDICION
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
        
        If oLiteralesAModificar.Item(sdbcIdioma(8).Columns("ID").Value & CStr(45)) Is Nothing And oLiteralesAModificar.Item(sdbcIdioma(8).Columns("ID").Value & CStr(46)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(45, 46, sdbcIdioma(8).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenPres(2).Text = oLiterales.Item(1).Den
                oLiteralesAModificar.Add sdbcIdioma(8).Columns("ID").Value, txtDenPres(2).Text, 45
                txtDenPres(3).Text = oLiterales.Item(2).Den
                oLiteralesAModificar.Add sdbcIdioma(8).Columns("ID").Value, txtDenPres(3).Text, 46
            Else
                txtDenPres(2).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(8).Columns("ID").Value, "", 45
                txtDenPres(3).Text = ""
                oLiteralesAModificar.Add sdbcIdioma(8).Columns("ID").Value, "", 46
                
            End If
        Else
            txtDenPres(2).Text = oLiteralesAModificar.Item(sdbcIdioma(8).Columns("ID").Value & CStr(45)).Den
            txtDenPres(3).Text = oLiteralesAModificar.Item(sdbcIdioma(8).Columns("ID").Value & CStr(46)).Den
        End If
    
    End If
End Sub


Private Sub sdbcIdiOtros_CloseUp()
 Dim oLiterales As CLiterales

    If picMat.Enabled = False Then
        
        'Estamos en modo consulta
        
        If sdbcIdioma(3).Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(19, 19, sdbcIdioma(3).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenSolicitud.Text = oLiterales.Item(1).Den
            Else
                txtDenSolicitud.Text = ""
            End If
        End If
        Set oLiterales = Nothing
    Else
           
        'MODO EDICION
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
        
        If oLiteralesAModificar.Item(sdbcIdioma(3).Columns("ID").Value & CStr(19)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(19, 19, sdbcIdioma(3).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenSolicitud.Text = oLiterales.Item(1).Den
                oLiteralesAModificar.Add sdbcIdioma(3).Columns("ID").Value, txtDenSolicitud.Text, 19
            Else
                txtDenSolicitud.Text = ""
                oLiteralesAModificar.Add sdbcIdioma(3).Columns("ID").Value, "", 19
                
            End If
        Else
            txtDenSolicitud.Text = oLiteralesAModificar.Item(sdbcIdioma(3).Columns("ID").Value & CStr(19)).Den
        End If
    End If
End Sub

Private Sub sdbcIdiPres_CloseUp()
    Dim oLiterales As CLiterales

    If picPres1.Enabled = False Then
        
        'Estamos en modo consulta
        
        If sdbcIdioma(2).Value <> "" Then
            Screen.MousePointer = vbHourglass
            If sdbcConPres.Columns("ID").Value < 3 Then
               Set oLiterales = oGestorParametros.DevolverLiterales(20, 23, sdbcIdioma(2).Columns("ID").Value)
            Else
               Set oLiterales = oGestorParametros.DevolverLiterales(27, 30, sdbcIdioma(2).Columns("ID").Value)
            End If
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                
             Select Case sdbcConPres.Columns("ID").Value
             
                Case 1:
                
                 If EstUsarPres(0) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(1).Den
                 txtDenPres(1).Text = oLiterales.Item(3).Den
                 sdbcConPres.Text = sdbcConPres.Columns("DEN").Value
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                 
                Case 2:
                
                 If EstUsarPres(1) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(2).Den
                 txtDenPres(1).Text = oLiterales.Item(4).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                 sdbcConPres.Text = sdbcConPres.Columns("DEN").Value

                
                Case 3:
                
                 If EstUsarPres(2) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(1).Den
                 txtDenPres(1).Text = oLiterales.Item(3).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                 sdbcConPres.Text = sdbcConPres.Columns("DEN").Value
                 
               Case 4:
                 If EstUsarPres(3) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(2).Den
                 txtDenPres(1).Text = oLiterales.Item(4).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                 sdbcConPres.Text = sdbcConPres.Columns("DEN").Value
                 
                Case Else
                
                 If EstUsarPres(0) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 sdbcIdioma(2).Text = sdbcIdioma(2).Column(0).Value
                 txtDenPres(0).Text = oLiterales.Item(1).Den
                 txtDenPres(1).Text = oLiterales.Item(3).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                 sdbcConPres.Text = sdbcConPres.Columns("DEN").Value
            End Select
            Else
                txtDenPres(0).Text = ""
                txtDenPres(1).Text = ""
            End If
        End If
            
        Set oLiterales = Nothing
    Else
        'MODO EDICION
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
     If sdbcConPres.Columns("ID").Value < 3 Then
        If oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)) Is Nothing Or oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(20, 23, sdbcIdioma(2).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
             Select Case sdbcConPres.Columns("ID").Value
                Case 1:
                 If EstUsarPres(0) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 21
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 23
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 20
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 22
               
                Case 2:
                 If EstUsarPres(1) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 20
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 22
                
                 txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                 txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 21
                 oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 23
              End Select
                 
            Else
            
              Select Case sdbcConPres.Columns("ID").Value
                  Case 1:
                    If EstUsarPres(0) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    
                    oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, "", 20
                    oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, "", 22
                    txtDenPres(0).Text = ""
                    txtDenPres(1).Text = ""
                  Case 2:
                    If EstUsarPres(1) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, "", 21
                    oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, "", 23
                    txtDenPres(0).Text = ""
                    txtDenPres(1).Text = ""
                
                
              End Select
           
            End If
            
        Else
            
            'Cargamos desde oLiteralesAModificar
            Select Case sdbcConPres.Columns("ID").Value
                  Case 1:
                    If EstUsarPres(0) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                  Case 2:
                    If EstUsarPres(1) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                  End Select
    End If
 Else
           If oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)) Is Nothing Or oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)) Is Nothing Then
    
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(27, 30, sdbcIdioma(2).Columns("ID").Value)
          
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                Select Case sdbcConPres.Columns("ID").Value
                Case 3:
                If EstUsarPres(2) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 28
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 30
                
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 27
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 29
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                Case 4:
                If EstUsarPres(3) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 27
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 29
                
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 28
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 30
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                End Select
            Else
               txtDenPres(0).Text = ""
               txtDenPres(1).Text = ""
               Select Case sdbcConPres.Columns("ID").Value
                Case 3:
                If EstUsarPres(2) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 27
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 29
                Case 4:
                If EstUsarPres(3) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 28
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 30
             End Select
             
            End If
            
        Else
            
            'Cargamos desde oLiteralesAModificar
            
               Select Case sdbcConPres.Columns("ID").Value
                    Case 3:
                    If EstUsarPres(2) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                    Case 4:
                    If EstUsarPres(3) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
               End Select
    End If

    End If
    End If
End Sub
Private Sub sdbcConPres_Validate(Cancel As Boolean)
    If oIdiomas Is Nothing Then
     Exit Sub
    End If
End Sub
Private Sub sdbcConPres_CloseUp()
    Dim oLiterales As CLiterales

    If picPres1.Enabled = False Then
        
        'Estamos en modo consulta
        
        If sdbcIdioma(2).Value <> "" Then
            Screen.MousePointer = vbHourglass
            If sdbcConPres.Columns("ID").Value < 3 Then
               Set oLiterales = oGestorParametros.DevolverLiterales(20, 23, sdbcIdioma(2).Columns("ID").Value)
            Else
               Set oLiterales = oGestorParametros.DevolverLiterales(27, 30, sdbcIdioma(2).Columns("ID").Value)
            End If
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                Select Case sdbcConPres.Columns("ID").Value
                Case 1:
                 If EstUsarPres(0) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(1).Den
                 txtDenPres(1).Text = oLiterales.Item(3).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                 
                Case 2:
                 If EstUsarPres(1) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(2).Den
                 txtDenPres(1).Text = oLiterales.Item(4).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                
                Case 3:
                 If EstUsarPres(2) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 txtDenPres(0).Text = oLiterales.Item(1).Den
                 txtDenPres(1).Text = oLiterales.Item(3).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                 
               Case 4:
                 If EstUsarPres(3) Then
                    chkConPres.Value = vbChecked
                 Else
                    chkConPres.Value = vbUnchecked
                 End If
                 'sdbcConPres.Columns(0).Value =
                 txtDenPres(0).Text = oLiterales.Item(2).Den
                 txtDenPres(1).Text = oLiterales.Item(4).Den
                 lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
            End Select
                
                
            Else
                txtDenPres(0).Text = ""
                txtDenPres(1).Text = ""
                
            End If
            
        End If
            
        Set oLiterales = Nothing
    Else
        'MODO EDICION
        'Tenemos que cargar oLiteralesAModificar, si es que todav�a no se hab�a cargado
        
       If (sdbcConPres.Columns("ID").Value < 3) Then
        If oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)) Is Nothing Or oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)) Is Nothing Then
    
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(20, 23, sdbcIdioma(2).Columns("ID").Value)
          
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                Select Case sdbcConPres.Columns("ID").Value
                Case 1:
                If EstUsarPres(0) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 21
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 23
                
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 20
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 22
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                Case 2:
                If EstUsarPres(1) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 20
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 22
                
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 21
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 23
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                End Select
            Else
               txtDenPres(0).Text = ""
               txtDenPres(1).Text = ""
               Select Case sdbcConPres.Columns("ID").Value
                Case 1:
                If EstUsarPres(0) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 20
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 22
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                Case 2:
                If EstUsarPres(1) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 21
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 23
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
              
              End Select
             
            End If
            
        Else
            
            'Cargamos desde oLiteralesAModificar
            
               Select Case sdbcConPres.Columns("ID").Value
                    Case 1:
                    If EstUsarPres(0) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den
                    lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
  
                    Case 2:
                    If EstUsarPres(1) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den
                    lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
  
               End Select
            End If
         Else
           If oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)) Is Nothing Or oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)) Is Nothing Then
    
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(27, 30, sdbcIdioma(2).Columns("ID").Value)
          
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                Select Case sdbcConPres.Columns("ID").Value
                Case 3:
                If EstUsarPres(2) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 28
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 30
                
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 27
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 29
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                Case 4:
                If EstUsarPres(3) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 27
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 29
                
                txtDenPres(0).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                txtDenPres(1).Text = oLiterales.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(0).Text, 28
                oLiteralesAModificar.Add sdbcIdioma(2).Columns("ID").Value, txtDenPres(1).Text, 30
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
                End Select
            Else
               txtDenPres(0).Text = ""
               txtDenPres(1).Text = ""
               Select Case sdbcConPres.Columns("ID").Value
                Case 3:
                If EstUsarPres(2) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 27
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 29
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
  
                Case 4:
                If EstUsarPres(3) Then
                    chkConPres.Value = vbChecked
                Else
                    chkConPres.Value = vbUnchecked
                End If
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 28
                oLiteralesAModificar.Add sdbcConPres.Columns("ID").Value, "", 30
                lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
  
             End Select
             
            End If
            
        Else
            
            'Cargamos desde oLiteralesAModificar
            
               Select Case sdbcConPres.Columns("ID").Value
                    Case 3:
                    If EstUsarPres(2) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den
                    lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
  
                    Case 4:
                    If EstUsarPres(3) Then
                       chkConPres.Value = vbChecked
                    Else
                       chkConPres.Value = vbUnchecked
                    End If
                    txtDenPres(0).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den
                    txtDenPres(1).Text = oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den
                    lblPresupuesto(3).caption = sdbcConPres.Columns("DEN").Value
  
               End Select
    End If

    End If
    End If
End Sub

Private Sub sdbcIdiUo_CloseUp()
    Dim oLiterales As CLiterales

    If picMat.Enabled = False Then
        'Estamos en modo consulta
        If sdbcIdioma(1).Value <> "" Then
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(1, 7, sdbcIdioma(1).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                
                txtDenUO(0).Text = oLiterales.Item(1).Den
                txtDenUO(1).Text = oLiterales.Item(2).Den
                txtAbrUO(1).Text = oLiterales.Item(3).Den
                txtDenUO(2).Text = oLiterales.Item(4).Den
                txtAbrUO(2).Text = oLiterales.Item(5).Den
                txtDenUO(3).Text = oLiterales.Item(6).Den
                txtAbrUO(3).Text = oLiterales.Item(7).Den
            Else
                txtDenUO(0).Text = ""
                txtDenUO(1).Text = ""
                txtAbrUO(1).Text = ""
                txtDenUO(2).Text = ""
                txtAbrUO(2).Text = ""
                txtDenUO(3).Text = ""
                txtAbrUO(3).Text = ""
            End If
        End If
        Set oLiterales = Nothing
    Else
        'MODO EDICI�N
         If oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(1)) Is Nothing Then
            'Todav�a no se hab�a cargado
            'Cargamos desde la BD
            Screen.MousePointer = vbHourglass
            Set oLiterales = oGestorParametros.DevolverLiterales(1, 7, sdbcIdioma(1).Columns("ID").Value)
            Screen.MousePointer = vbNormal
            
            If oLiterales.Count > 0 Then
                txtDenUO(0).Text = oLiterales.Item(1).Den
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(0).Text, 1
                
                txtDenUO(1).Text = oLiterales.Item(2).Den
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(1).Text, 2
                
                txtAbrUO(1).Text = oLiterales.Item(3).Den
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(1).Text, 3
                
                txtDenUO(2).Text = oLiterales.Item(4).Den
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(2).Text, 4
                
                txtAbrUO(2).Text = oLiterales.Item(5).Den
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(2).Text, 5
                
                txtDenUO(3).Text = oLiterales.Item(6).Den
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(3).Text, 6
                
                txtAbrUO(3).Text = oLiterales.Item(7).Den
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(3).Text, 7
            Else
                txtDenUO(0).Text = ""
                txtDenUO(1).Text = ""
                txtAbrUO(1).Text = ""
                txtDenUO(2).Text = ""
                txtAbrUO(2).Text = ""
                txtDenUO(3).Text = ""
                txtAbrUO(3).Text = ""
                txtDenUO(0).Text = oLiterales.Item(1).Den
                
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(0).Text, 1
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(1).Text, 2
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(1).Text, 3
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(2).Text, 4
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(2).Text, 5
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(3).Text, 6
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtAbrUO(3).Text, 7
            End If
    Else
        txtDenUO(0).Text = oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(1)).Den
        txtDenUO(1).Text = oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(2)).Den
        txtAbrUO(1).Text = oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(3)).Den
        txtDenUO(2).Text = oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(4)).Den
        txtAbrUO(2).Text = oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(5)).Den
        txtDenUO(3).Text = oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(6)).Den
        txtAbrUO(3).Text = oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(7)).Den
    End If
End If
   
End Sub

Private Sub sdbcIdiomas_InitColumnProps(Index As Integer)
    If Index = 2 Then
        sdbcIdiomas(Index).DataFieldList = "Column 0,Column 1"
    Else
        sdbcIdiomas(Index).DataFieldList = "Column 0"
    End If
    sdbcIdiomas(Index).DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcIdiomas_PositionList(Index As Integer, ByVal Text As String)
    If Index = 2 Then
        PositionList sdbcIdiomas(2), Text
    End If
End Sub

Private Sub sdbcMonDen_Validate(Cancel As Boolean)
    If sdbcMonDen.Forecolor = RGB(255, 0, 0) Then
        sdbcMonDen = ""
    End If
End Sub

Private Sub sdbcPaiCod_Change()
    If Not PaiRespetarCombo Then
    
        PaiRespetarCombo = True
        sdbcPaiDen.Text = ""
        PaiRespetarCombo = False
        
        PaiCargarComboDesde = True
        Set oPaisSeleccionado = Nothing
        
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
End Sub

Private Sub sdbcPaiCod_CloseUp()
    If sdbcPaiCod.Value = "..." Then
        sdbcPaiCod.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiDen.Text = sdbcPaiCod.Columns(1).Text
    sdbcPaiCod.Text = sdbcPaiCod.Columns(0).Text
    PaiRespetarCombo = False
    
    Set oPaisSeleccionado = oPaises.Item(sdbcPaiCod.Columns(0).Text)
    
    PaiCargarComboDesde = False
End Sub

Private Sub sdbcPaiCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oPaises = Nothing
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    sdbcPaiCod.RemoveAll
    
    If PaiCargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPaiCod.Text), , , False
    Else
        oPaises.CargarTodosLosPaises , , , , , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If PaiCargarComboDesde And Not oPaises.EOF Then
        sdbcPaiCod.AddItem "..."
    End If

    sdbcPaiCod.SelStart = 0
    sdbcPaiCod.SelLength = Len(sdbcPaiCod.Text)
    sdbcPaiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiCod_InitColumnProps()
    sdbcPaiCod.DataFieldList = "Column 0"
    sdbcPaiCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcPaiCod_PositionList(ByVal Text As String)
PositionList sdbcPaiCod, Text
End Sub
Private Sub sdbcPaiCod_Validate(Cancel As Boolean)
    Dim oPaises As CPaises
    Dim bExiste As Boolean
    
    Set oPaises = oFSGSRaiz.Generar_CPaises
    
    If sdbcPaiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    oPaises.CargarTodosLosPaises sdbcPaiCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPaises.Count = 0)
    
    If Not bExiste Then
        sdbcPaiCod.Text = ""
    Else
        PaiRespetarCombo = True
        sdbcPaiDen.Text = oPaises.Item(1).Den
        sdbcPaiCod.Columns(0).Value = sdbcPaiCod.Text
        sdbcPaiCod.Columns(1).Value = sdbcPaiDen.Text
        PaiRespetarCombo = False
        
        Set oPaisSeleccionado = oPaises.Item(1)
        PaiCargarComboDesde = False
    End If
    
    Set oPaises = Nothing
End Sub
Private Sub sdbcPaiDen_Change()
    If Not PaiRespetarCombo Then
        PaiRespetarCombo = True
        sdbcPaiCod.Text = ""
        PaiRespetarCombo = False
        PaiCargarComboDesde = True
        Set oPaisSeleccionado = Nothing
    End If
    
    sdbcProviCod = ""
    sdbcProviDen = ""
End Sub
Private Sub sdbcPaiDen_CloseUp()
    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If
    
    PaiRespetarCombo = True
    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    PaiRespetarCombo = False
    
    Set oPaisSeleccionado = oPaises.Item(sdbcPaiDen.Columns(1).Text)
    
    PaiCargarComboDesde = False
End Sub

Private Sub sdbcPaiDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Set oPaises = Nothing
    Set oPaises = oFSGSRaiz.Generar_CPaises
   
    Screen.MousePointer = vbHourglass
    
    sdbcPaiDen.RemoveAll
    
    If PaiCargarComboDesde Then
        oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPaiDen.Text), True, False
    Else
        oPaises.CargarTodosLosPaises , , , True, , False
    End If
    
    Codigos = oPaises.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPaiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If PaiCargarComboDesde And Not oPaises.EOF Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
    sdbcPaiCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiDen_InitColumnProps()
    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcPaiDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcPaiCod.DroppedDown = False
        sdbcPaiCod.Text = ""
        sdbcPaiCod.RemoveAll
        sdbcPaiDen.DroppedDown = False
        sdbcPaiDen.Text = ""
        sdbcPaiDen.RemoveAll
        Set oPaisSeleccionado = Nothing
    End If
End Sub
Private Sub sdbcMonCod_Change()
    If Not MonRespetarCombo Then
    
        MonRespetarCombo = True
        sdbcMonCod.Forecolor = RGB(255, 0, 0)
        sdbcMonDen.Text = ""
        MonRespetarCombo = False
        
        MonCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcMonCod_CloseUp()
    If sdbcMonCod.Value = "..." Then
        sdbcMonCod.Text = ""
        Exit Sub
    End If
    
    MonRespetarCombo = True
    sdbcMonCod.Forecolor = RGB(0, 0, 0)
    sdbcMonDen.Forecolor = RGB(0, 0, 0)
    sdbcMonDen.Text = sdbcMonCod.Columns(1).Text
    sdbcMonCod.Text = sdbcMonCod.Columns(0).Text
    MonRespetarCombo = False
    
    MonCargarComboDesde = False
End Sub

Private Sub sdbcMonCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Set oMonedas = Nothing
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
   
    
    sdbcMonCod.RemoveAll
    
    If MonCargarComboDesde Then
        oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcMonCod.Text), , , True
    Else
        oMonedas.CargarTodasLasMonedas , , , , , False, True
    End If
    
    Codigos = oMonedas.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If MonCargarComboDesde And Not oMonedas.EOF Then
        sdbcMonCod.AddItem "..."
    End If

    sdbcMonCod.SelStart = 0
    sdbcMonCod.SelLength = Len(sdbcMonCod.Text)
    sdbcMonCod.Refresh
End Sub

Private Sub sdbcMonCod_InitColumnProps()
    sdbcMonCod.DataFieldList = "Column 0"
    sdbcMonCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcMonCod_PositionList(ByVal Text As String)
PositionList sdbcMonCod, Text
End Sub
Private Sub sdbcMonCod_Validate(Cancel As Boolean)
    Dim oMonedas As CMonedas
    Dim bExiste As Boolean
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    If sdbcMonCod.Text = "" Then Exit Sub
    
    oMonedas.CargarTodasLasMonedas sdbcMonCod.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMonCod.Text = ""
    Else
        MonRespetarCombo = True
        sdbcMonCod.Forecolor = RGB(0, 0, 0)
        sdbcMonDen.Forecolor = RGB(0, 0, 0)
        sdbcMonDen.Text = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcMonCod.Columns(0).Value = sdbcMonCod.Text
        sdbcMonCod.Columns(1).Value = sdbcMonDen.Text
        MonRespetarCombo = False
        MonCargarComboDesde = False
    End If
    
    Set oMonedas = Nothing
End Sub
Private Sub sdbcMonden_Change()
    If Not MonRespetarCombo Then
        MonRespetarCombo = True
        sdbcMonDen.Forecolor = RGB(255, 0, 0)
        sdbcMonCod.Text = ""
        MonRespetarCombo = False
        MonCargarComboDesde = True
    End If
End Sub
Private Sub sdbcMonDen_CloseUp()
    If sdbcMonDen.Value = "..." Then
        sdbcMonDen.Text = ""
        Exit Sub
    End If
    
    MonRespetarCombo = True
    sdbcMonCod.Forecolor = RGB(0, 0, 0)
    sdbcMonDen.Forecolor = RGB(0, 0, 0)
    sdbcMonCod.Text = sdbcMonDen.Columns(1).Text
    sdbcMonDen.Text = sdbcMonDen.Columns(0).Text
    MonRespetarCombo = False
    
    MonCargarComboDesde = False
End Sub

Private Sub sdbcMonDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Set oMonedas = Nothing
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    
    sdbcMonDen.RemoveAll
    
    If MonCargarComboDesde Then
        oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcMonDen.Text), True, True
    Else
        oMonedas.CargarTodasLasMonedas , , , True, , False, True
    End If
    
    Codigos = oMonedas.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcMonDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If MonCargarComboDesde And Not oMonedas.EOF Then
        sdbcMonDen.AddItem "..."
    End If

    sdbcMonDen.SelStart = 0
    sdbcMonDen.SelLength = Len(sdbcMonDen.Text)
    sdbcMonCod.Refresh
End Sub

Private Sub sdbcMonDen_InitColumnProps()
    sdbcMonDen.DataFieldList = "Column 0"
    sdbcMonDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProviCod_Change()
    If Not ProviRespetarCombo Then
    
        ProviRespetarCombo = True
        sdbcProviDen.Text = ""
        ProviRespetarCombo = False
        
        ProviCargarComboDesde = True
    End If
End Sub

Private Sub sdbcProviCod_CloseUp()
    If sdbcProviCod.Value = "..." Then
        sdbcProviCod.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviDen.Text = sdbcProviCod.Columns(1).Text
    sdbcProviCod.Text = sdbcProviCod.Columns(0).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
End Sub

Private Sub sdbcProviCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    sdbcProviCod.RemoveAll
        
    If Not oPaisSeleccionado Is Nothing Then

        If ProviCargarComboDesde Then
            oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProviCod.Text), , , False
        Else
            oPaisSeleccionado.CargarTodasLasProvincias , , , , False
        End If
        
        Codigos = oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
        Next
        
        If ProviCargarComboDesde And Not oPaisSeleccionado.Provincias.EOF Then
            sdbcProviCod.AddItem "..."
        End If

    End If
    
    sdbcProviCod.SelStart = 0
    sdbcProviCod.SelLength = Len(sdbcProviCod.Text)
    sdbcProviCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcProviCod_InitColumnProps()
    sdbcProviCod.DataFieldList = "Column 0"
    sdbcProviCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcProviCod_PositionList(ByVal Text As String)
PositionList sdbcProviCod, Text
End Sub
Private Sub sdbcProviCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcProviCod.Text = "" Then Exit Sub
   
    If Not oPaisSeleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        oPaisSeleccionado.CargarTodasLasProvincias sdbcProviCod.Text, , True, , False
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oPaisSeleccionado.Provincias.Count = 0)
    Else
        bExiste = False
    End If
        
    If Not bExiste Then
        sdbcProviCod.Text = ""
    Else
        ProviRespetarCombo = True
        sdbcProviDen.Text = oPaisSeleccionado.Provincias.Item(1).Den
        sdbcProviCod.Columns(0).Value = sdbcProviCod.Text
        sdbcProviCod.Columns(1).Value = sdbcProviDen.Text
        
        ProviRespetarCombo = False
        ProviCargarComboDesde = False
    End If
End Sub
Private Sub sdbcProviDen_Change()
    If Not ProviRespetarCombo Then
        ProviRespetarCombo = True
        sdbcProviCod.Text = ""
        ProviRespetarCombo = False
        ProviCargarComboDesde = True
    End If
End Sub
Private Sub sdbcProviDen_CloseUp()
    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If
    
    ProviRespetarCombo = True
    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    ProviRespetarCombo = False
    
    ProviCargarComboDesde = False
End Sub
Private Sub sdbcProviDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    sdbcProviDen.RemoveAll
    
    If Not oPaisSeleccionado Is Nothing Then
        
        If ProviCargarComboDesde Then
            oPaisSeleccionado.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProviDen.Text), True, False
        Else
            oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If
        
        Codigos = oPaisSeleccionado.DevolverLosCodigosDeProvincias
        
        For i = 0 To UBound(Codigos.Cod) - 1
            sdbcProviDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
        Next
        
        If ProviCargarComboDesde And Not oPaisSeleccionado.Provincias.EOF Then
            sdbcProviDen.AddItem "..."
        End If

    End If
    
    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProviDen_InitColumnProps()
    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProviDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProviCod.DroppedDown = False
        sdbcProviCod.Text = ""
        sdbcProviCod.RemoveAll
        sdbcProviDen.DroppedDown = False
        sdbcProviDen.Text = ""
        sdbcProviDen.RemoveAll
    End If
End Sub

Private Sub sdbcDestCod_Change()
    If Not DestRespetarCombo Then
    
        DestRespetarCombo = True
        sdbcDestDen.Text = ""
        DestRespetarCombo = False
        
        DestCargarComboDesde = True
        
    End If
End Sub
Private Sub sdbcDestCod_CloseUp()
    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    
    DestRespetarCombo = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    DestRespetarCombo = False
    
    DestCargarComboDesde = False
End Sub

Private Sub sdbcDestCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oDestinos = Nothing
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    
    sdbcDestCod.RemoveAll
    
    If DestCargarComboDesde Then
        oDestinos.CargarTodosLosDestinosDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcDestCod), , , True, False
    Else
        oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, False
    End If
    
    Codigos = oDestinos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcDestCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If DestCargarComboDesde And Not oDestinos.EOF Then
        sdbcDestCod.AddItem "..."
    End If

    sdbcDestCod.SelStart = 0
    sdbcDestCod.SelLength = Len(sdbcDestCod.Text)
    sdbcDestCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcDestCod_InitColumnProps()
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub
Private Sub sdbcDestCod_Validate(Cancel As Boolean)
    Dim oDestinos As CDestinos
    Dim bExiste As Boolean
    
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    
    If sdbcDestCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oDestinos.CargarTodosLosDestinos sdbcDestCod.Text, , True, , , , , , , False, True, False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oDestinos.Count = 0)
    
    If Not bExiste Then
        sdbcDestCod.Text = ""
    Else
        DestRespetarCombo = True
        sdbcDestDen.Text = oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcDestCod.Columns(0).Value = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Value = sdbcDestDen.Text
        DestRespetarCombo = False
        
        DestCargarComboDesde = False
    End If
    
    Set oDestinos = Nothing
End Sub

Private Sub sdbcDestDen_Change()
    If Not DestRespetarCombo Then
    
        DestRespetarCombo = True
        sdbcDestCod.Text = ""
        DestRespetarCombo = False
        DestCargarComboDesde = True
    
    End If
End Sub
Private Sub sdbcDestDen_CloseUp()
    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    
    DestRespetarCombo = True
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    DestRespetarCombo = False
    
    DestCargarComboDesde = False
End Sub

Private Sub sdbcDestDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oDestinos = Nothing
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    
    sdbcDestDen.RemoveAll
    
    If DestCargarComboDesde Then
        oDestinos.CargarTodosLosDestinosDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcDestDen), True, True, False
    Else
        oDestinos.CargarTodosLosDestinos , , , True, , , , , , False, True, False
    End If
    
    Codigos = oDestinos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcDestDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If DestCargarComboDesde And Not oDestinos.EOF Then
        sdbcDestDen.AddItem "..."
    End If

    sdbcDestDen.SelStart = 0
    sdbcDestDen.SelLength = Len(sdbcDestDen.Text)
    sdbcDestCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcDestDen_InitColumnProps()
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDestDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcDestCod.DroppedDown = False
        sdbcDestCod.Text = ""
        sdbcDestCod.RemoveAll
        sdbcDestDen.DroppedDown = False
        sdbcDestDen.Text = ""
        sdbcDestDen.RemoveAll
    End If
End Sub
Private Sub sdbcUniCod_Change()
    If Not UniRespetarCombo Then
        UniRespetarCombo = True
        sdbcUniDen.Text = ""
        UniRespetarCombo = False
        
        UniCargarComboDesde = True
    End If
End Sub

Private Sub sdbcUniCod_CloseUp()
    If sdbcUniCod.Value = "..." Then
        sdbcUniCod.Text = ""
        Exit Sub
    End If
    
    UniRespetarCombo = True
    sdbcUniDen.Text = sdbcUniCod.Columns(1).Text
    sdbcUniCod.Text = sdbcUniCod.Columns(0).Text
    UniRespetarCombo = False
    
    UniCargarComboDesde = False
End Sub

Private Sub sdbcUniCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oUnidades = Nothing
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    
    sdbcUniCod.RemoveAll
    
    If UniCargarComboDesde Then
        oUnidades.CargarTodasLasUnidadesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcUniCod.Text), , , True
    Else
        oUnidades.CargarTodasLasUnidades , , , , False
    End If
    
    Codigos = oUnidades.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcUniCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If UniCargarComboDesde And Not oUnidades.EOF Then
        sdbcUniCod.AddItem "..."
    End If

    sdbcUniCod.SelStart = 0
    sdbcUniCod.SelLength = Len(sdbcUniCod.Text)
    sdbcUniCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcUniCod_InitColumnProps()
    sdbcUniCod.DataFieldList = "Column 0"
    sdbcUniCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcUniCod_PositionList(ByVal Text As String)
PositionList sdbcUniCod, Text
End Sub
Private Sub sdbcUniCod_Validate(Cancel As Boolean)
    Dim oUnidades As CUnidades
    Dim bExiste As Boolean
    
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    
    If sdbcUniCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oUnidades.CargarTodasLasUnidades sdbcUniCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oUnidades.Count = 0)
    
    If Not bExiste Then
        sdbcUniCod.Text = ""
    Else
        UniRespetarCombo = True
        sdbcUniDen.Text = oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcUniCod.Columns(0).Value = sdbcUniCod.Text
        sdbcUniCod.Columns(1).Value = sdbcUniDen.Text
        
        UniRespetarCombo = False
        UniCargarComboDesde = False
    End If
    
    Set oUnidades = Nothing
End Sub
Private Sub sdbcUniden_Change()
    If Not UniRespetarCombo Then
        UniRespetarCombo = True
        sdbcUniCod.Text = ""
        UniRespetarCombo = False
        UniCargarComboDesde = True
    End If
End Sub

Private Sub sdbcUniDen_CloseUp()
    If sdbcUniDen.Value = "..." Then
        sdbcUniDen.Text = ""
        Exit Sub
    End If
    
    UniRespetarCombo = True
    sdbcUniCod.Text = sdbcUniDen.Columns(1).Text
    sdbcUniDen.Text = sdbcUniDen.Columns(0).Text
    UniRespetarCombo = False
    
    UniCargarComboDesde = False
End Sub

Private Sub sdbcUniDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oUnidades = Nothing
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    
    sdbcUniDen.RemoveAll
    
    If UniCargarComboDesde Then
        oUnidades.CargarTodasLasUnidadesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcUniDen.Text), True, False
    Else
        oUnidades.CargarTodasLasUnidades , , , True, False
    End If
    
    Codigos = oUnidades.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcUniDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If UniCargarComboDesde And Not oUnidades.EOF Then
        sdbcUniDen.AddItem "..."
    End If

    sdbcUniDen.SelStart = 0
    sdbcUniDen.SelLength = Len(sdbcUniDen.Text)
    sdbcUniCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcUniDen_InitColumnProps()
    sdbcUniDen.DataFieldList = "Column 0"
    sdbcUniDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcUniDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcUniCod.DroppedDown = False
        sdbcUniCod.Text = ""
        sdbcUniCod.RemoveAll
        sdbcUniDen.DroppedDown = False
        sdbcUniDen.Text = ""
        sdbcUniDen.RemoveAll
    End If
End Sub
Private Sub sdbcRolCod_Change()
    If Not RolRespetarCombo Then
    
        RolRespetarCombo = True
        sdbcRolDen.Text = ""
        RolRespetarCombo = False
        
        RolCargarComboDesde = True
    End If
End Sub

Private Sub sdbcRolCod_CloseUp()
    If sdbcRolCod.Value = "..." Then
        sdbcRolCod.Text = ""
        Exit Sub
    End If
    
    RolRespetarCombo = True
    sdbcRolDen.Text = sdbcRolCod.Columns(1).Text
    sdbcRolCod.Text = sdbcRolCod.Columns(0).Text
    RolRespetarCombo = False
    
    RolCargarComboDesde = False
End Sub

Private Sub sdbcRolCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    Set oRoles = Nothing
    Set oRoles = oFSGSRaiz.Generar_CRoles
    
    sdbcRolCod.RemoveAll
    
    If RolCargarComboDesde Then
        oRoles.CargarTodosLosRolesDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcRolCod.Text), , , True, , basPublic.gParametrosInstalacion.gIdioma
    Else
        oRoles.CargarTodosLosRoles , , , , False, , basPublic.gParametrosInstalacion.gIdioma
    End If
    
    Codigos = oRoles.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcRolCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If RolCargarComboDesde And Not oRoles.EOF Then
        sdbcRolCod.AddItem "..."
    End If

    sdbcRolCod.SelStart = 0
    sdbcRolCod.SelLength = Len(sdbcRolCod.Text)
    sdbcRolCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcRolCod_InitColumnProps()
    sdbcRolCod.DataFieldList = "Column 0"
    sdbcRolCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcRolCod_PositionList(ByVal Text As String)
PositionList sdbcRolCod, Text
End Sub
Private Sub sdbcRolCod_Validate(Cancel As Boolean)
    Dim oRoles As CRoles
    Dim bExiste As Boolean
    
    Set oRoles = oFSGSRaiz.Generar_CRoles
    
    If sdbcRolCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oRoles.CargarTodosLosRoles sdbcRolCod.Text, , True, , False, , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oRoles.Count = 0)
    
    If Not bExiste Then
        sdbcRolCod.Text = ""
    Else
        RolRespetarCombo = True
        sdbcRolDen.Text = oRoles.Item(1).Den
        sdbcRolCod.Columns(0).Value = sdbcRolCod.Text
        sdbcRolCod.Columns(1).Value = sdbcRolDen.Text
        
        RolRespetarCombo = False
        RolCargarComboDesde = False
    End If
    
    Set oRoles = Nothing
End Sub
Private Sub sdbcRolden_Change()
    If Not RolRespetarCombo Then
        RolRespetarCombo = True
        sdbcRolCod.Text = ""
        RolRespetarCombo = False
        RolCargarComboDesde = True
    End If
End Sub
Private Sub sdbcRolDen_CloseUp()
    If sdbcRolDen.Value = "..." Then
        sdbcRolDen.Text = ""
        Exit Sub
    End If
    
    RolRespetarCombo = True
    sdbcRolCod.Text = sdbcRolDen.Columns(1).Text
    sdbcRolDen.Text = sdbcRolDen.Columns(0).Text
    RolRespetarCombo = False
    
    RolCargarComboDesde = False
End Sub

Private Sub sdbcRolDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    Set oRoles = Nothing
    Set oRoles = oFSGSRaiz.Generar_CRoles
   
    sdbcRolDen.RemoveAll
    
    If RolCargarComboDesde Then
        oRoles.CargarTodosLosRolesDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcRolDen.Text), True, False, , basPublic.gParametrosInstalacion.gIdioma
    Else
        oRoles.CargarTodosLosRoles , , , True, False, , basPublic.gParametrosInstalacion.gIdioma
    End If
    
    Codigos = oRoles.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcRolDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If RolCargarComboDesde And Not oRoles.EOF Then
        sdbcRolDen.AddItem "..."
    End If

    sdbcRolDen.SelStart = 0
    sdbcRolDen.SelLength = Len(sdbcRolDen.Text)
    sdbcRolCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcRolDen_InitColumnProps()
    sdbcRolDen.DataFieldList = "Column 0"
    sdbcRolDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcRolDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcRolCod.DroppedDown = False
        sdbcRolCod.Text = ""
        sdbcRolCod.RemoveAll
        sdbcRolDen.DroppedDown = False
        sdbcRolDen.Text = ""
        sdbcRolDen.RemoveAll
    End If
End Sub
Private Sub sdbcPagCod_Change()
    If Not PagRespetarCombo Then
        PagRespetarCombo = True
        sdbcPagDen.Text = ""
        PagRespetarCombo = False
        PagCargarComboDesde = True
    End If
End Sub

Private Sub sdbcPagCod_CloseUp()
    If sdbcPagCod.Value = "..." Then
        sdbcPagCod.Text = ""
        Exit Sub
    End If
    
    PagRespetarCombo = True
    sdbcPagDen.Text = sdbcPagCod.Columns(1).Text
    sdbcPagCod.Text = sdbcPagCod.Columns(0).Text
    PagRespetarCombo = False
    
    PagCargarComboDesde = False
End Sub

Private Sub sdbcPagCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oPagos = Nothing
    Set oPagos = oFSGSRaiz.generar_CPagos
   
    sdbcPagCod.RemoveAll
    
    If PagCargarComboDesde Then
        oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcPagCod.Text), , , True
    Else
        oPagos.CargarTodosLosPagos , , , , False
    End If
    
    Codigos = oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If PagCargarComboDesde And Not oPagos.EOF Then
        sdbcPagCod.AddItem "..."
    End If

    sdbcPagCod.SelStart = 0
    sdbcPagCod.SelLength = Len(sdbcPagCod.Text)
    sdbcPagCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPagCod_InitColumnProps()
    sdbcPagCod.DataFieldList = "Column 0"
    sdbcPagCod.DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcPagCod_PositionList(ByVal Text As String)
PositionList sdbcPagCod, Text
End Sub
Private Sub sdbcPagCod_Validate(Cancel As Boolean)
    Dim oPagos As CPagos
    Dim bExiste As Boolean
    
    Set oPagos = oFSGSRaiz.generar_CPagos
    
    If sdbcPagCod.Text = "" Then Exit Sub
   
    Screen.MousePointer = vbHourglass
    oPagos.CargarTodosLosPagos sdbcPagCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPagos.Count = 0)
    
    If Not bExiste Then
        sdbcPagCod.Text = ""
    Else
        PagRespetarCombo = True
        sdbcPagDen.Text = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcPagCod.Columns(0).Value = sdbcPagCod.Text
        sdbcPagCod.Columns(1).Value = sdbcPagDen.Text
        
        PagRespetarCombo = False
        PagCargarComboDesde = False
    End If
    
    Set oPagos = Nothing
End Sub
Private Sub sdbcPagden_Change()
    If Not PagRespetarCombo Then
        PagRespetarCombo = True
        sdbcPagCod.Text = ""
        PagRespetarCombo = False
        PagCargarComboDesde = True
    End If
End Sub
Private Sub sdbcPagDen_CloseUp()
    If sdbcPagDen.Value = "..." Then
        sdbcPagDen.Text = ""
        Exit Sub
    End If
    
    PagRespetarCombo = True
    sdbcPagCod.Text = sdbcPagDen.Columns(1).Text
    sdbcPagDen.Text = sdbcPagDen.Columns(0).Text
    PagRespetarCombo = False
    
    PagCargarComboDesde = False
End Sub

Private Sub sdbcPagDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oPagos = Nothing
    Set oPagos = oFSGSRaiz.generar_CPagos
    
    sdbcPagDen.RemoveAll
    
    If PagCargarComboDesde Then
        oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcPagDen.Text), True, False
    Else
        oPagos.CargarTodosLosPagos , , , True, False
    End If
    
    Codigos = oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If PagCargarComboDesde And Not oPagos.EOF Then
        sdbcPagDen.AddItem "..."
    End If

    sdbcPagDen.SelStart = 0
    sdbcPagDen.SelLength = Len(sdbcPagDen.Text)
    sdbcPagCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPagDen_InitColumnProps()
    sdbcPagDen.DataFieldList = "Column 0"
    sdbcPagDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcPagDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcPagCod.DroppedDown = False
        sdbcPagCod.Text = ""
        sdbcPagCod.RemoveAll
        sdbcPagDen.DroppedDown = False
        sdbcPagDen.Text = ""
        sdbcPagDen.RemoveAll
    End If
End Sub
Private Sub sdbcEstCod_Change(Index As Integer)
    If Not EstRespetarCombo(Index) Then
        EstRespetarCombo(Index) = True
        sdbcEstDen(Index).Text = ""
        EstRespetarCombo(Index) = False
        EstCargarComboDesde(Index) = True
    End If
End Sub

Private Sub sdbcEstCod_CloseUp(Index As Integer)
    If sdbcEstCod(Index).Value = "..." Then
        sdbcEstCod(Index).Text = ""
        Exit Sub
    End If
    
    EstRespetarCombo(Index) = True
    sdbcEstDen(Index).Text = sdbcEstCod(Index).Columns(1).Text
    sdbcEstCod(Index).Text = sdbcEstCod(Index).Columns(0).Text
    EstRespetarCombo(Index) = False
    
    EstCargarComboDesde(Index) = False
End Sub

Private Sub sdbcEstCod_DropDown(Index As Integer)
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    Set oOfeEstados = Nothing
    Set oOfeEstados = oFSGSRaiz.Generar_COfeEstados
 
    sdbcEstCod(Index).RemoveAll
    
    If EstCargarComboDesde(Index) Then
        oOfeEstados.CargarTodosLosOfeEstadosDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEstCod(Index).Text), , , True, basPublic.gParametrosInstalacion.gIdioma
    Else
        oOfeEstados.CargarTodosLosOfeEstados , , , , False, , , basPublic.gParametrosInstalacion.gIdioma
    End If
    
    Codigos = oOfeEstados.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEstCod(Index).AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If EstCargarComboDesde(Index) And Not oOfeEstados.EOF Then
        sdbcEstCod(Index).AddItem "..."
    End If

    sdbcEstCod(Index).SelStart = 0
    sdbcEstCod(Index).SelLength = Len(sdbcEstCod(Index).Text)
    sdbcEstCod(Index).Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEstCod_InitColumnProps(Index As Integer)
    sdbcEstCod(Index).DataFieldList = "Column 0"
    sdbcEstCod(Index).DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcEstCod_PositionList(Index As Integer, ByVal Text As String)
PositionList sdbcEstCod(Index), Text
End Sub
Private Sub sdbcEstCod_Validate(Index As Integer, Cancel As Boolean)
    Dim oOfeEstados As COfeEstados
    Dim bExiste As Boolean
    
    Set oOfeEstados = oFSGSRaiz.Generar_COfeEstados
    
    If sdbcEstCod(Index).Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oOfeEstados.CargarTodosLosOfeEstados sdbcEstCod(Index).Text, , True, , False, , , basPublic.gParametrosInstalacion.gIdioma
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oOfeEstados.Count = 0)
    
    If Not bExiste Then
        sdbcEstCod(Index).Text = ""
    Else
        EstRespetarCombo(Index) = True
        sdbcEstDen(Index).Text = oOfeEstados.Item(1).Den
        sdbcEstCod(Index).Columns(0).Value = sdbcEstCod(Index).Text
        sdbcEstCod(Index).Columns(1).Value = sdbcEstDen(Index).Text
        EstRespetarCombo(Index) = False
        EstCargarComboDesde(Index) = False
    End If
    
    Set oOfeEstados = Nothing
End Sub
Private Sub sdbcEstden_Change(Index As Integer)
    If Not EstRespetarCombo(Index) Then
    
        EstRespetarCombo(Index) = True
        sdbcEstCod(Index).Text = ""
        EstRespetarCombo(Index) = False
        EstCargarComboDesde(Index) = True
    
    End If
End Sub
Private Sub sdbcEstDen_CloseUp(Index As Integer)
    
    If sdbcEstDen(Index).Value = "..." Then
        sdbcEstDen(Index).Text = ""
        Exit Sub
    End If
    
    EstRespetarCombo(Index) = True
    sdbcEstCod(Index).Text = sdbcEstDen(Index).Columns(1).Text
    sdbcEstDen(Index).Text = sdbcEstDen(Index).Columns(0).Text
    EstRespetarCombo(Index) = False
    
    EstCargarComboDesde(Index) = False
        
End Sub

Private Sub sdbcEstDen_DropDown(Index As Integer)
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set oOfeEstados = Nothing
    Set oOfeEstados = oFSGSRaiz.Generar_COfeEstados

    sdbcEstDen(Index).RemoveAll
    
    If EstCargarComboDesde(Index) Then
        oOfeEstados.CargarTodosLosOfeEstadosDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEstDen(Index).Text), True, False, basPublic.gParametrosInstalacion.gIdioma
    Else
        oOfeEstados.CargarTodosLosOfeEstados , , , True, False, , , basPublic.gParametrosInstalacion.gIdioma
    End If
    
    Codigos = oOfeEstados.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEstDen(Index).AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If EstCargarComboDesde(Index) And Not oOfeEstados.EOF Then
        sdbcEstDen(Index).AddItem "..."
    End If

    sdbcEstDen(Index).SelStart = 0
    sdbcEstDen(Index).SelLength = Len(sdbcEstDen(Index).Text)
    sdbcEstCod(Index).Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEstDen_InitColumnProps(Index As Integer)
    sdbcEstDen(Index).DataFieldList = "Column 0"
    sdbcEstDen(Index).DataFieldToDisplay = "Column 0"
End Sub
Private Sub sdbcEstDen_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcEstCod(Index).DroppedDown = False
        sdbcEstCod(Index).Text = ""
        sdbcEstCod(Index).RemoveAll
        sdbcEstDen(Index).DroppedDown = False
        sdbcEstDen(Index).Text = ""
        sdbcEstDen(Index).RemoveAll
    End If
End Sub


Private Sub sdbgAmbitoDatos_Change()
Dim i As Integer
    
    Select Case sdbgAmbitoDatos.Columns.Item("Obligatoria").Value
        Case "1"
            Select Case sdbgAmbitoDatos.col
                Case 0 'Clic en la columna Usar
                    sdbgAmbitoDatos.Columns.Item("Usar").Value = True
                Case 2, 3, 4 'Clic en una columna de ambito
                    sdbgAmbitoDatos.Columns.Item(sdbgAmbitoDatos.col).Value = True
                    For i = 2 To 4
                        If (i <> sdbgAmbitoDatos.col) Then
                            sdbgAmbitoDatos.Columns.Item(i).Value = False
                        End If
                    Next i
            End Select
        Case Else
            Select Case sdbgAmbitoDatos.col
                Case 0 'Clic en la columna Usar
                    Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                        Case True  'Checkbox Usar activado
                            sdbgAmbitoDatos.Columns.Item("Item").Value = True
                            sdbgAmbitoDatos.Columns(0).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
                            sdbgAmbitoDatos.Columns(1).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
                        Case False 'Checkbox Usar desactivado
                            'Si la DIST o los PRESUP son obligatorios no se pueden descheckear
                            Select Case sdbgAmbitoDatos.Columns("Fila").Value
                                Case 5 'DIST
                                        sdbgAmbitoDatos.Columns(0).Value = "-1"
                                        Exit Sub
                                Case 6 'PRES1
                                        If chkRegGest(3).Value = vbChecked Then
                                            sdbgAmbitoDatos.Columns(0).Value = "-1"
                                            Exit Sub
                                        End If
                                Case 7 'PRES2
                                        If chkRegGest(4).Value = vbChecked Then
                                            sdbgAmbitoDatos.Columns(0).Value = "-1"
                                            Exit Sub
                                        End If
                                Case 8 'PRES3
                                        If chkRegGest(5).Value = vbChecked Then
                                            sdbgAmbitoDatos.Columns(0).Value = "-1"
                                            Exit Sub
                                        End If
                                Case 9 'PRES4
                                        If chkRegGest(6).Value = vbChecked Then
                                            sdbgAmbitoDatos.Columns(0).Value = "-1"
                                            Exit Sub
                                        End If
                            
                            End Select
                            
                            sdbgAmbitoDatos.Columns.Item("Proceso").Value = False
                            sdbgAmbitoDatos.Columns.Item("Grupo").Value = False
                            sdbgAmbitoDatos.Columns.Item("Item").Value = False
                            sdbgAmbitoDatos.Columns(0).CellStyleSet "Normal", sdbgAmbitoDatos.Row
                            sdbgAmbitoDatos.Columns(1).CellStyleSet "Normal", sdbgAmbitoDatos.Row
                    End Select
                    'si la fila seleccionada es Solicitud de compra mostraru o cultar frame correspondiente seg�n estado de check
                    'Tarea 3376, se ha eliminado esta opci�n por la tarea 3425 GFA
                Case 2, 3, 4 'Clic en una columna de ambito
                    Select Case sdbgAmbitoDatos.Columns.Item(0).Value
                        Case True  'Checkbox Usar activado
                            If ((sdbgAmbitoDatos.Columns.Item(2).Value = False) And (sdbgAmbitoDatos.Columns.Item(3).Value = False) And (sdbgAmbitoDatos.Columns.Item(4).Value = False)) Then
                            'Si la DIST o los PRESUP son obligatorios no se pueden descheckear
                                Select Case sdbgAmbitoDatos.Columns("Fila").Value
                                    Case 5 'DIST
                                            sdbgAmbitoDatos.Columns(0).Value = "-1"
                                            sdbgAmbitoDatos.Columns(sdbgAmbitoDatos.col).Value = "-1"
                                            Exit Sub
                                    Case 6 'PRES1
                                            If chkRegGest(3).Value = vbChecked Then
                                                sdbgAmbitoDatos.Columns(0).Value = "-1"
                                                sdbgAmbitoDatos.Columns(sdbgAmbitoDatos.col).Value = "-1"
                                                Exit Sub
                                            End If
                                    Case 7 'PRES2
                                            If chkRegGest(4).Value = vbChecked Then
                                                sdbgAmbitoDatos.Columns(0).Value = "-1"
                                                sdbgAmbitoDatos.Columns(sdbgAmbitoDatos.col).Value = "-1"
                                                Exit Sub
                                            End If
                                    Case 8 'PRES3
                                            If chkRegGest(5).Value = vbChecked Then
                                                sdbgAmbitoDatos.Columns(0).Value = "-1"
                                                sdbgAmbitoDatos.Columns(sdbgAmbitoDatos.col).Value = "-1"
                                                Exit Sub
                                            End If
                                    Case 9 'PRES4
                                            If chkRegGest(6).Value = vbChecked Then
                                                sdbgAmbitoDatos.Columns(0).Value = "-1"
                                                sdbgAmbitoDatos.Columns(sdbgAmbitoDatos.col).Value = "-1"
                                                Exit Sub
                                            End If
                                
                                End Select
                                sdbgAmbitoDatos.Columns.Item("Usar").Value = False
                                sdbgAmbitoDatos.Columns(0).CellStyleSet "Normal", sdbgAmbitoDatos.Row
                                sdbgAmbitoDatos.Columns(1).CellStyleSet "Normal", sdbgAmbitoDatos.Row
                            Else
                                If (sdbgAmbitoDatos.Columns.Item(1).Value <> sDatos(4)) Then
                                    sdbgAmbitoDatos.Columns.Item(sdbgAmbitoDatos.col).Value = True
                                    For i = 2 To 4
                                        If (i <> sdbgAmbitoDatos.col) Then
                                            sdbgAmbitoDatos.Columns.Item(i).Value = False
                                        End If
                                    Next i
                                End If
                            End If
                        Case False 'Checkbox Usar desactivado
                            sdbgAmbitoDatos.Columns.Item("Usar").Value = True
                            sdbgAmbitoDatos.Columns(0).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
                            sdbgAmbitoDatos.Columns(1).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
                    End Select
            End Select
    End Select
End Sub

Private Sub sdbgAmbitoDatos_InitColumnProps()
    Dim sEspProce As String
    Dim sEspGrupo As String
    Dim sEspItem As String
    Dim sEspUsada As String

    sdbgAmbitoDatos.RemoveAll
    'Primero se cargan los 3 datos obligatorios
    If gParametrosGenerales.gCPProcesoAdminPub = False Then
    
        Select Case gParametrosGenerales.gCPDestino
            Case 0
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
            Case 1
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
            Case 2
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
            Case 3
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        End Select
        
        Select Case gParametrosGenerales.gCPPago
            Case 0
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
            Case 1
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
            Case 2
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
            Case 3
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        End Select
    End If
    
    Select Case gParametrosGenerales.gCPFechasSuministro
        Case 0
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 1
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 2
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 3
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
    End Select
    
    'Ahora se cargan el resto de datos no obligatorios
    If gParametrosGenerales.gCPProcesoAdminPub = False Then
        Select Case gParametrosGenerales.gCPProveActual
            Case 0
                sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
            Case 1
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
            Case 2
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
            Case 3
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        End Select
    End If
    
    Select Case gParametrosGenerales.gCPEspProce Or gParametrosGenerales.gCPEspGrupo Or gParametrosGenerales.gCPEspItem
        Case True
            sEspUsada = "-1"
        Case False
            sEspUsada = "0"
    End Select
    Select Case gParametrosGenerales.gCPEspProce
        Case True
            sEspProce = "-1"
        Case False
            sEspProce = "0"
    End Select
    Select Case gParametrosGenerales.gCPEspGrupo
        Case True
            sEspGrupo = "-1"
        Case False
            sEspGrupo = "0"
    End Select
    Select Case gParametrosGenerales.gCPEspItem
        Case True
            sEspItem = "-1"
        Case False
            sEspItem = "0"
    End Select
    sdbgAmbitoDatos.AddItem sEspUsada & Chr(m_lSeparador) & sDatos(4) & Chr(m_lSeparador) & sEspProce & Chr(m_lSeparador) & sEspGrupo & Chr(m_lSeparador) & sEspItem & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "4"
    
    Select Case gParametrosGenerales.gCPDistUON
        Case 0
            sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 1
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 2
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 3
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
    End Select
    
    Select Case gParametrosGenerales.gbUsarPres1
        Case True
            Select Case gParametrosGenerales.gCPPresAnu1
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            End Select
    End Select
    
    Select Case gParametrosGenerales.gbUsarPres2
        Case True
            Select Case gParametrosGenerales.gCPPresAnu2
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            End Select
    End Select
    
    Select Case gParametrosGenerales.gbUsarPres3
        Case True
            Select Case gParametrosGenerales.gCPPres1
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            End Select
    End Select
    
    Select Case gParametrosGenerales.gbUsarPres4
        Case True
            Select Case gParametrosGenerales.gCPPres2
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            End Select
    End Select
    
    If m_bMostrarSolicitud = True Then
        If gParametrosGenerales.gCPProcesoAdminPub = False Then
            Select Case gParametrosGenerales.gCPSolicitud
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            End Select
        End If
    End If
    
    sdbgAmbitoDatos.Columns.Item(1).Locked = True
End Sub

Private Sub sdbgAmbitoDatos_RowLoaded(ByVal Bookmark As Variant)
    Select Case CStr(Bookmark)
        Case "0", "1", "2"
            sdbgAmbitoDatos.Columns(0).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
            sdbgAmbitoDatos.Columns(1).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
        Case Else
            Select Case sdbgAmbitoDatos.Columns.Item("Usar").Value
                Case True
                    sdbgAmbitoDatos.Columns(0).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
                    sdbgAmbitoDatos.Columns(1).CellStyleSet "Yellow", sdbgAmbitoDatos.Row
            End Select
    End Select
End Sub

Private Sub sdbgCamposERP_AfterColUpdate(ByVal ColIndex As Integer)

If ColIndex = 0 Then
    Exit Sub
Else
    If oLiteralesAModificar.Item(sdbgCamposERP.Groups(ColIndex - 1).caption & CStr(sdbgCamposERP.Row + 32)) Is Nothing Then
        oLiteralesAModificar.Add sdbgCamposERP.Groups(ColIndex - 1).caption, sdbgCamposERP.Columns(ColIndex).Value, sdbgCamposERP.Row + 32
    Else
        oLiteralesAModificar.Item(sdbgCamposERP.Groups(ColIndex - 1).caption & CStr(sdbgCamposERP.Row + 32)).Den = sdbgCamposERP.Columns(ColIndex).Value
    End If
End If

End Sub

Private Sub sldLogPreBloq_Change()
    sParte = DividirFrase(sCap(53))
    lblIntentos = sParte(1) + CStr(sldLogPreBloq.Value) + sParte(2)
End Sub

Private Sub SSTab2_Click(PreviousTab As Integer)
    FraPartidasPresupuestarias.Visible = (SSTab2.Tab = 2)
End Sub

Private Sub txtAbrMat_Validate(Index As Integer, Cancel As Boolean)
    Select Case Index
        Case 1
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(9)).Den = txtAbrMat(1).Text
        Case 2
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(11)).Den = txtAbrMat(2).Text
        Case 3
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(13)).Den = txtAbrMat(3).Text
        Case 4
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(15)).Den = txtAbrMat(4).Text
    End Select
End Sub

Private Sub txtAbrUO_Validate(Index As Integer, Cancel As Boolean)
    Select Case Index
        Case 1
               oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(3)).Den = txtAbrUO(1).Text
        Case 2
               oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(5)).Den = txtAbrUO(2).Text
        Case 3
                oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(7)).Den = txtAbrUO(3).Text
    End Select
End Sub

Private Sub txtDenMat_Validate(Index As Integer, Cancel As Boolean)
    Select Case Index
        Case 1
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(8)).Den = txtDenMat(1).Text
        Case 2
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(10)).Den = txtDenMat(2).Text
        Case 3
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(12)).Den = txtDenMat(3).Text
        Case 4
               oLiteralesAModificar.Item(sdbcIdioma(0).Columns("ID").Value & CStr(14)).Den = txtDenMat(4).Text
    End Select
End Sub

Private Sub txtDenPres_Validate(Index As Integer, Cancel As Boolean)
      If Index > 1 Then
            Select Case Index
            Case 2
                    oLiteralesAModificar.Item(sdbcIdioma(8).Columns("ID").Value & CStr(45)).Den = txtDenPres(2).Text
            Case 3
                    oLiteralesAModificar.Item(sdbcIdioma(8).Columns("ID").Value & CStr(46)).Den = txtDenPres(3).Text
            End Select
      Else
          Select Case sdbcConPres.Columns("ID").Value
           Case 1:
           
            Select Case Index
            Case 0
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(20)).Den = txtDenPres(0).Text
            Case 1
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(22)).Den = txtDenPres(1).Text
            End Select
          Case 2:
            Select Case Index
              Case 0
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(21)).Den = txtDenPres(0).Text
              Case 1
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(23)).Den = txtDenPres(1).Text
            End Select
          Case 3:
            Select Case Index
              Case 0
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(27)).Den = txtDenPres(0).Text
              Case 1
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(29)).Den = txtDenPres(1).Text
            End Select
          Case 4:
            Select Case Index
              Case 0
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(28)).Den = txtDenPres(0).Text
              Case 1
                    oLiteralesAModificar.Item(sdbcIdioma(2).Columns("ID").Value & CStr(30)).Den = txtDenPres(1).Text
            End Select
        End Select
    End If
End Sub

Private Sub txtDenSolicitud_Validate(Cancel As Boolean)
    oLiteralesAModificar.Item(sdbcIdioma(3).Columns("ID").Value & CStr(19)).Den = txtDenSolicitud.Text
End Sub

Private Sub txtDenUO_Validate(Index As Integer, Cancel As Boolean)
    Select Case Index
        Case 0
            If oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(1)) Is Nothing Then
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(0).Text, 1
            Else
               oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(1)).Den = txtDenUO(0).Text
            End If
        Case 1
            If oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(2)) Is Nothing Then
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(1).Text, 2
            Else
               oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(2)).Den = txtDenUO(1).Text
            End If
        Case 2
            If oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(4)) Is Nothing Then
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(2).Text, 4
            Else
               oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(4)).Den = txtDenUO(2).Text
            End If
        Case 3
            If oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(6)) Is Nothing Then
                oLiteralesAModificar.Add sdbcIdioma(1).Columns("ID").Value, txtDenUO(3).Text, 6
            Else
               oLiteralesAModificar.Item(sdbcIdioma(1).Columns("ID").Value & CStr(6)).Den = txtDenUO(3).Text
            End If
    End Select
End Sub

Private Sub txtDestSTDen_Change()
    sDenDestSTNew = txtDestSTDen
End Sub

Private Sub txtRuta_Change(Index As Integer)
    If Not bRespetarControl And Index < 2 Then
        bChangeTxt = True
    End If
End Sub

Private Sub txtRuta_Validate(Index As Integer, Cancel As Boolean)

On Error GoTo Error

    If Index < 2 Then
        If txtRuta(Index).Text <> dirRPT(Index).Path Then
            bRespetarControl = True
            dirRPT(Index).Path = txtRuta(Index).Text
            bChangeTxt = False
            bRespetarControl = False
        End If
    End If
    Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    
    txtRuta(Index).Text = ""
    bChangeTxt = False
    bRespetarControl = False
End Sub

Private Sub txtVolMaxAdjDir_LostFocus()
    If IsNumeric(txtVolMaxAdjDir) Then
        txtVolMaxAdjDir = Format(txtVolMaxAdjDir, "standard")
    End If
End Sub

Private Sub FormatearTiempoAntelacionMinutos(ByVal lTiempoCombo As Long, ByVal bytFormat As Byte, ByRef lTiempoMinutos As Long)
    Select Case bytFormat
        Case 0 'minutos
            lTiempoMinutos = lTiempoCombo
        Case 1 'hora
            lTiempoMinutos = 60
        Case 2 'horas
            lTiempoMinutos = lTiempoCombo * 60
        Case 3 'd�a
            lTiempoMinutos = 1440
        Case 4 'd�as
            lTiempoMinutos = lTiempoCombo * 1440
        Case 5 'semana
            lTiempoMinutos = 10080
        Case 6 'semanas
            lTiempoMinutos = lTiempoCombo * 10080
        Case 7 'mes
            lTiempoMinutos = 40320
    End Select
End Sub

Private Sub FormatearTiempoAntelacionCombo(ByVal lTiempo As Long, ByRef lTiempoConvertido As Long, ByRef bytFormat As Byte)
    Select Case lTiempo
        Case 5 To 55
            lTiempoConvertido = lTiempo
            bytFormat = 0 'minutos
        Case 60
            lTiempoConvertido = 1
            bytFormat = 1 'hora
        Case 120 To 1380
            lTiempoConvertido = lTiempo / 60
            bytFormat = 2 'horas
        Case 1440
            lTiempoConvertido = 1
            bytFormat = 3 'dia
        Case 2880 To 8640
            lTiempoConvertido = lTiempo / 1440
            bytFormat = 4 'd�as
        Case 10080
            lTiempoConvertido = 1
            bytFormat = 5 'semana
        Case 20160 To 30240
            lTiempoConvertido = lTiempo / 10080
            bytFormat = 6 'semanas
        Case 40320
            lTiempoConvertido = 1
            bytFormat = 7 'mes
    End Select
End Sub

Private Sub ExtraerValorYUnidadDeTiempo(ByVal sCombo As String, ByRef lValor As Long, ByRef bytFormat As Byte)
    Dim sUT As String
    Dim i As Byte
    
    lValor = val(Left(sCombo, (InStr(1, sCombo, " ", vbTextCompare)) - 1))
    sUT = Mid(sCombo, InStr(1, sCombo, " ", vbTextCompare) + 1)
    
    For i = 0 To 7
        If sUT = sFormatoTextoTAntelacion(i) Then
            bytFormat = i
            Exit For
        End If
    Next i
End Sub

Private Sub CargarAmbitoDatos()
    Dim sEspProce As String
    Dim sEspGrupo As String
    Dim sEspItem As String
    Dim sEspUsada As String

    sdbgAmbitoDatos.RemoveAll
    
    'Primero se cargan los 3 datos obligatorios
    
    Select Case gParametrosGenerales.gCPDestino
        Case 0
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        Case 1
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        Case 2
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
        Case 3
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(0) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "0"
    End Select
    
    Select Case gParametrosGenerales.gCPPago
        Case 0
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        Case 1
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        Case 2
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
        Case 3
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(1) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "1"
    End Select

    Select Case gParametrosGenerales.gCPFechasSuministro
        Case 0
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 1
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 2
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
        Case 3
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(2) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & "2"
    End Select
    
    'Ahora se cargan el resto de datos no obligatorios
    Select Case gParametrosGenerales.gCPProveActual
        Case 0
            sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        Case 1
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        Case 2
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
        Case 3
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(3) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "3"
    End Select
    
    Select Case gParametrosGenerales.gCPEspProce Or gParametrosGenerales.gCPEspGrupo Or gParametrosGenerales.gCPEspItem
        Case True
            sEspUsada = "-1"
        Case False
            sEspUsada = "0"
    End Select
    Select Case gParametrosGenerales.gCPEspProce
        Case True
            sEspProce = "-1"
        Case False
            sEspProce = "0"
    End Select
    Select Case gParametrosGenerales.gCPEspGrupo
        Case True
            sEspGrupo = "-1"
        Case False
            sEspGrupo = "0"
    End Select
    Select Case gParametrosGenerales.gCPEspItem
        Case True
            sEspItem = "-1"
        Case False
            sEspItem = "0"
    End Select
    sdbgAmbitoDatos.AddItem sEspUsada & Chr(m_lSeparador) & sDatos(4) & Chr(m_lSeparador) & sEspProce & Chr(m_lSeparador) & sEspGrupo & Chr(m_lSeparador) & sEspItem & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "4"
    
    Select Case gParametrosGenerales.gCPDistUON
        Case 0
            sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 1
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 2
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
        Case 3
            sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(5) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "5"
    End Select

    Select Case gParametrosGenerales.gbUsarPres1
        Case True
            Select Case gParametrosGenerales.gCPPresAnu1
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(6) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "6"
            End Select
    End Select
    
    Select Case gParametrosGenerales.gbUsarPres2
        Case True
            Select Case gParametrosGenerales.gCPPresAnu2
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(7) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "7"
            End Select
    End Select
    
    Select Case gParametrosGenerales.gbUsarPres3
        Case True
            Select Case gParametrosGenerales.gCPPres1
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(8) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "8"
            End Select
    End Select
    
    Select Case gParametrosGenerales.gbUsarPres4
        Case True
            Select Case gParametrosGenerales.gCPPres2
                Case 0
                    sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
                Case 1
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
                Case 2
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
                Case 3
                    sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(9) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "9"
            End Select
    End Select
    
    If m_bMostrarSolicitud = True Then
        Select Case gParametrosGenerales.gCPSolicitud
            Case 0
                sdbgAmbitoDatos.AddItem "0" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            Case 1
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            Case 2
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
            Case 3
                sdbgAmbitoDatos.AddItem "-1" & Chr(m_lSeparador) & sDatos(10) & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "-1" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "10"
        End Select
    End If
    
    sdbgAmbitoDatos.Columns.Item(1).Locked = True
End Sub
Private Sub cmdDrive_Click(Index As Integer)
Dim sBuffer As String
Dim sTit As String
Dim sInicio As String

On Error GoTo Error

sTit = fraCarpetas(Index).caption & ":"
sInicio = txtRuta(Index).Text
If sInicio = "" Then sInicio = CurDir$

sBuffer = BrowseForFolder(Me.hWnd, sTit, sInicio)

If sBuffer <> "" Then
    bRespetarControl = True
    dirRPT(Index).Path = sBuffer
    txtRuta(Index).Text = sBuffer
    bChangeTxt = False
    bRespetarControl = False
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description

    txtRuta(Index).Text = ""
    bChangeTxt = False
    bRespetarControl = False
End Sub


Private Sub DirRPT_Change(Index As Integer)
    If Not bRespetarControl Then
        bRespetarControl = True
        txtRuta(Index).Text = dirRPT(Index).Path
        bRespetarControl = False
    End If
End Sub

Private Sub RellenarComboUnidad()
Dim i As Integer
Dim Str1 As String
Dim Str2 As String

    cmbTSUnidad.clear
    cmbTSUnidad.AddItem ""
    
    Str1 = Matrix
    
    For i = 1 To Len(Matrix)
        Str2 = Left(Str1, 1)   'First Character
        Str1 = Right(Str1, (Len(Str1) - 1))   'All but First Character
        strMatrix(i) = Str2 & ":"  'Makes up each row of the Array
        cmbTSUnidad.AddItem Str2 & ":", i
    Next i
    
End Sub


Private Function BuscarUnidad(ByVal sLetra As String) As Integer
Dim i As Integer
Dim iRes As Integer

iRes = -1
For i = 1 To UBound(strMatrix)
    If sLetra = strMatrix(i) Then
        iRes = i
        Exit For
    End If
Next
BuscarUnidad = iRes
End Function

''' <summary>
''' Evento que se genera al seleccionar el optionBox de la secci�n de seguridad, deseleccionando el otro
''' </summary>
''' <remarks>Llamada desde: Autom�tico, cuando se selecciona una opcion del optionBox
''' Tiempo m�ximo: 0 seg</remarks>
Private Sub optRutaTS_Click(Index As Integer)
If Index = 2 Then
    optRutaTS(2).Value = True
    optRutaTS(3).Value = False
ElseIf Index = 3 Then
    optRutaTS(2).Value = False
    optRutaTS(3).Value = True
ElseIf Index = 0 Then
    txtRuta(2).Locked = False
    txtRuta(2).Text = gParametrosGenerales.gsTSHomeFolderRutaLocal
    cmbTSUnidad.ListIndex = -1
    cmbTSUnidad.Locked = True
    txtRuta(3).Text = ""
    txtRuta(3).Locked = True
Else
    cmbTSUnidad.Locked = False
    txtRuta(3).Locked = False
    cmbTSUnidad.ListIndex = BuscarUnidad(gParametrosGenerales.gsTSHomeFolderUnidad & ":")
    txtRuta(3).Text = gParametrosGenerales.gsTSHomeFolderRutaUnidad
    txtRuta(2).Locked = True
    txtRuta(2).Text = ""
End If

End Sub

Private Sub optGenerarProc_Click(Index As Integer)
    If optGenerarProc(0).Value = True Then
        picSolicImporte.Enabled = True
    Else
        picSolicImporte.Enabled = False
        chkAdjDirImp.Value = vbUnchecked
        txtAdjDirImp.Text = ""
    End If
    
    If optGenerarProc(2).Value = True Then
        'Acta en word
        optGenerarProc(3).Value = False
        txtPlantilla(18).Visible = False
        txtPlantilla(39).Visible = False
        sdbcPlantilla(11).Visible = True
        sdbcPlantilla(12).Visible = True
    Else
        'Acta en Crystal
        optGenerarProc(2).Value = False
        txtPlantilla(18).Visible = True
        txtPlantilla(39).Visible = True
        sdbcPlantilla(11).Visible = False
        sdbcPlantilla(12).Visible = False
    End If
    
End Sub

Private Sub txtConvMailSubject_Validate(Cancel As Boolean)
    oLiteralesAModificar.Item(sdbcIdioma(7).Columns("ID").Value & CStr(10010)).Den = txtConvMailSubject.Text
End Sub

Private Sub txtPlantilla_Validate(Index As Integer, Cancel As Boolean)
    If Index = 16 Then
        oLiteralesAModificar.Item(sdbcIdioma(6).Columns("ID").Value & CStr(10008)).Den = txtPlantilla(Index).Text
    ElseIf Index = 43 Then
        oLiteralesAModificar.Item(sdbcIdioma(6).Columns("ID").Value & CStr(10009)).Den = txtPlantilla(Index).Text
    End If
End Sub

Private Sub sdbcIdiConvocatoria_CloseUp()

    If picReuPlantillas.Enabled = False Then
        'Estamos en modo consulta
        
        If sdbcIdioma(6).Value <> "" Then
            Select Case UCase(sdbcIdioma(6).Columns("ID").Value)
            Case "SPA"
                txtPlantilla(16).Text = gParametrosGenerales.gsCONVMAILHTMLSPA
                txtPlantilla(43).Text = gParametrosGenerales.gsCONVMAILTEXTSPA
            Case "ENG"
                txtPlantilla(16).Text = gParametrosGenerales.gsCONVMAILHTMLENG
                txtPlantilla(43).Text = gParametrosGenerales.gsCONVMAILTEXTENG
            Case "GER"
                txtPlantilla(16).Text = gParametrosGenerales.gsCONVMAILHTMLGER
                txtPlantilla(43).Text = gParametrosGenerales.gsCONVMAILTEXTGER
            Case "FRA"
                txtPlantilla(16).Text = gParametrosGenerales.gsCONVMAILHTMLFRA
                txtPlantilla(43).Text = gParametrosGenerales.gsCONVMAILTEXTFRA
            End Select
        End If
            
    Else
        txtPlantilla(16).Text = oLiteralesAModificar.Item(sdbcIdioma(6).Columns("ID").Value & CStr(10008)).Den
        txtPlantilla(43).Text = oLiteralesAModificar.Item(sdbcIdioma(6).Columns("ID").Value & CStr(10009)).Den
    End If
End Sub

Private Sub sdbcIdiConvocatoriaSubject_CloseUp()

    If picConvocatoria.Enabled = False Then
        'Estamos en modo consulta
        
        If sdbcIdioma(7).Value <> "" Then
            Select Case UCase(sdbcIdioma(6).Columns("ID").Value)
            Case "SPA"
                txtConvMailSubject.Text = gParametrosGenerales.gsCONVMAILSUBJECTSPA
            Case "ENG"
                txtConvMailSubject.Text = gParametrosGenerales.gsCONVMAILSUBJECTENG
            Case "GER"
                txtConvMailSubject.Text = gParametrosGenerales.gsCONVMAILSUBJECTGER
            Case "FRA"
                txtConvMailSubject.Text = gParametrosGenerales.gsCONVMAILSUBJECTFRA
            End Select
        End If
    Else
        txtConvMailSubject.Text = oLiteralesAModificar.Item(sdbcIdioma(7).Columns("ID").Value & CStr(10010)).Den
    End If
End Sub

''' <summary>
''' Dar formato al campo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub txtImpMaxAdjDir_LostFocus()
    If IsNumeric(txtImpMaxAdjDir) Then txtImpMaxAdjDir = Format(txtImpMaxAdjDir, "standard")
End Sub

''' <summary>
''' Evento que se genera cuando se cambia el valor del hist�rico de los passwords
''' </summary>
''' <param name="Indice">Indice del control UpDown1</param>
''' <remarks>Tiempo estimado: 0.3</remarks>
Private Sub UpDown1_Change(Index As Integer)
    Dim Ador As Ador.Recordset
    Dim i As Integer
    i = 0
    On Error Resume Next
        
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFGEN, basPublic.gParametrosInstalacion.gIdioma)
    If Index = 0 Then
        label2(13).caption = IIf(UpDown1(Index).Value <> 0, Ador(360).Value, Ador(359).Value)
    End If
    Select Case Index
        Case 0
            If CInt(txtGrupoDefecto(3).Text) > 23 Or CInt(txtGrupoDefecto(3).Text) < 0 Then Exit Sub
        Case 1
            If CInt(txtGrupoDefecto(4).Text) > 997 Or CInt(txtGrupoDefecto(4).Text) < 0 Then Exit Sub
        Case 2
            If CInt(txtGrupoDefecto(5).Text) > 998 Or CInt(txtGrupoDefecto(5).Text) < 0 Then Exit Sub
        Case 3
            If CInt(txtGrupoDefecto(6).Text) > 13 Or CInt(txtGrupoDefecto(6).Text) < 0 Then Exit Sub
    End Select
End Sub

''' <summary>
''' Evento que se genera cuando se escribe algo en un textBox del formulario de seguridad, comprobando que son n�meros para no habilitar si no es asi
''' </summary>
''' <remarks>
''' Llamada desde: Automatico, cuando se pulse una tecla estando el foco en el textBox
''' Tiempo m�ximo: 0 seg</remarks>
Private Sub txtGrupoDefecto_KeyPress(Index As Integer, KeyAscii As Integer)
    If Index > 2 And Index < 7 Then
        If KeyAscii = 13 Then
            KeyAscii = 0        ' Para que no "pite"
            SendKeys "{tab}"    ' Env�a una pulsaci�n TAB
        ElseIf KeyAscii <> 8 Then   ' El 8 es la tecla de borrar (backspace)
        If Not IsNumeric(Chr(KeyAscii)) Then
            ' ... se desecha esa tecla y se avisa de que no es correcta
                Beep
                KeyAscii = 0
            End If
        End If
    End If
End Sub

Private Sub cmdControlPresupuestario_Click(Index As Integer)

If Index = 0 Then
    cmdAnyaArbol
ElseIf Index = 1 Then
    cmdElimArbol
ElseIf Index = 2 Then
    cmdModifArbol
End If

End Sub
Private Sub cmdAnyaArbol()
    frmPRESCon5Arbol.m_sAccion = "Anyadir"
    frmPRESCon5Arbol.WindowState = vbNormal
    frmPRESCon5Arbol.Show vbModal
    
    If sdbcPartidaPres = "" Then Exit Sub
    'Permisos usuario
    cmdControlPresupuestario(1).Enabled = True
    cmdControlPresupuestario(2).Enabled = True
End Sub


Private Sub cmdElimArbol()
Dim oIBaseDatos As IBaseDatos
Dim opres5 As cPresConcep5Nivel0
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit

 irespuesta = oMensajes.PreguntaEliminar(m_sLitPartida & " " & sdbcPartidaPres.Text)
 If irespuesta = vbYes Then
    Set opres5 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    opres5.Cod = sdbcPartidaPres.Value
    Set oIBaseDatos = opres5
    
    teserror = oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
    Else
        sdbcPartidaPres = ""
        sdbcPartidaPres.Value = ""
    End If
 End If
 
 Set opres5 = Nothing
 Set oIBaseDatos = Nothing
End Sub

''' <summary>
''' Modificacion de arbol de nivel0
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Click del boton; Tiempo m�ximo:0</remarks>
Private Sub cmdModifArbol()
    frmPRESCon5Arbol.m_sAccion = "Modificar"
    frmPRESCon5Arbol.m_sCod = sdbcPartidaPres.Value
    frmPRESCon5Arbol.WindowState = vbNormal
    frmPRESCon5Arbol.Show vbModal
    
    
    If sdbcPartidaPres = "" Then Exit Sub
    'Permisos usuario
    cmdControlPresupuestario(1).Enabled = True
    cmdControlPresupuestario(2).Enabled = True

    
End Sub

Private Sub sdbcPartidaPres_InitColumnProps()
    sdbcPartidaPres.DataFieldList = "Column 0"
    sdbcPartidaPres.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcPartidaPres_Change()
    cmdControlPresupuestario(1).Enabled = False
    cmdControlPresupuestario(2).Enabled = False
End Sub

Private Sub sdbcPartidaPres_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyBack Then
        cmdControlPresupuestario(1).Enabled = False
        cmdControlPresupuestario(2).Enabled = False
    End If
End Sub

Private Sub sdbcPartidaPres_PositionList(ByVal Text As String)
PositionList sdbcPartidaPres, Text
End Sub

Private Sub sdbcPartidaPres_DropDown()
    Dim oPartida As cPresConcep5Nivel0
    Dim sUsu As String
    Dim sPerfilCod As String
    
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
        
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESConcepto5CrearArboles)) Is Nothing) Then
            'No tiene permiso para crear arboles presupuestarios
            cmdControlPresupuestario(0).Visible = False
            cmdControlPresupuestario(1).Visible = False
            cmdControlPresupuestario(2).Visible = False
            
            sUsu = oUsuarioSummit.Cod
        Else
            cmdControlPresupuestario(0).Visible = True
            cmdControlPresupuestario(1).Visible = True
            cmdControlPresupuestario(2).Visible = True
        End If
        
        If Not oUsuarioSummit.Perfil Is Nothing Then sPerfilCod = oUsuarioSummit.Perfil.Cod
    End If
    
    
    Screen.MousePointer = vbHourglass
    
    Dim oPartidas As cPresConceptos5Nivel0
    Set oPartidas = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    
    sdbcPartidaPres.RemoveAll

    oPartidas.CargarPresupuestosConceptos5 , , , , , , , , sUsu, sPerfilCod


    For Each oPartida In oPartidas
        sdbcPartidaPres.AddItem oPartida.Cod & Chr(m_lSeparador) & oPartida.Den
    Next
    
    Set oPartidas = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPartidaPres_CloseUp()
    If sdbcPartidaPres.Value = "" Then Exit Sub
    If sdbcPartidaPres = "" Then Exit Sub
    'Permisos usuario
    cmdControlPresupuestario(1).Enabled = True
    cmdControlPresupuestario(2).Enabled = True
End Sub

''' <summary>
''' Realiza la llamada web a Visor Plantillas
''' </summary>
''' <remarks>Llamada desde: cmdPlantilla_Click ; Tiempo m�ximo:0,8seg.</remarks>
Private Sub MostrarVisorPlantillas()
    Dim strSessionId As String
       
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    With frmInternet
        .g_sOrigen = "frmCONFGEN"
        .g_sNombre = "Visor Plantillas"
        .g_sRuta = gParametrosGenerales.gsRutasFullstepWeb & "/App_Pages/GS/Reuniones/VisorPlantillasReunion.aspx?desdeGS=1&SessionId=" & strSessionId
        If MDI.ActiveForm Is Nothing Then
            .WindowState = vbNormal
        Else
            If MDI.ActiveForm.WindowState = vbMaximized Then
                .WindowState = vbMaximized
            Else
                .WindowState = vbNormal
            End If
        End If
        .SetFocus
    End With
End Sub

Private Sub sdbcPlantilla_InitColumnProps(Index As Integer)
    sdbcPlantilla(Index).DataFieldList = "Column 0"
    sdbcPlantilla(Index).DataFieldToDisplay = "Column 1"
End Sub

Public Sub CargarDatosPlantillas()
    Dim Ador As Ador.Recordset
    Set Ador = oGestorParametros.DevolverPlantillasAgenda(oUsuarioSummit.idioma)
    sdbcPlantilla(10).RemoveAll
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            sdbcPlantilla(10).AddItem Ador("ID").Value & Chr(m_lSeparador) & Ador("DEN").Value & ": " & Ador("NOM").Value
            If CBool(Ador("DEFECTO").Value) Then
                gParametrosGenerales.giIDAGENDADEFECTO = Ador("ID").Value
                sdbcPlantilla(10).Value = Ador("ID").Value
            End If
            Ador.MoveNext
        Wend
    End If
    
    If gParametrosGenerales.giActaTipo = ActaWord Then
        optGenerarProc(2).Value = True
        txtPlantilla(18).Visible = False
    Else
        optGenerarProc(3).Value = True
        txtPlantilla(18).Visible = True
    End If

    Set Ador = oGestorParametros.DevolverPlantillasActa(oUsuarioSummit.idioma, 1)
    sdbcPlantilla(11).RemoveAll
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            sdbcPlantilla(11).AddItem Ador("ID").Value & Chr(m_lSeparador) & Ador("DEN").Value & ": " & Ador("NOM").Value
            If CBool(Ador("DEFECTO").Value) Then
                gParametrosGenerales.giIDACTA1DEFECTO = Ador("ID").Value
                sdbcPlantilla(11).Value = Ador("ID").Value
            End If
            Ador.MoveNext
        Wend
    End If
    
    Set Ador = oGestorParametros.DevolverPlantillasActa(oUsuarioSummit.idioma, 2)
    sdbcPlantilla(12).RemoveAll
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            sdbcPlantilla(12).AddItem Ador("ID").Value & Chr(m_lSeparador) & Ador("DEN").Value & ": " & Ador("NOM").Value
            If CBool(Ador("DEFECTO").Value) Then
                gParametrosGenerales.giIDACTA2DEFECTO = Ador("ID").Value
                sdbcPlantilla(12).Value = Ador("ID").Value
            End If
            Ador.MoveNext
        Wend
    End If

    txtPlantilla(18) = gParametrosGenerales.gsACTADOT
    txtPlantilla(39) = gParametrosGenerales.gsACTADOT2
End Sub


