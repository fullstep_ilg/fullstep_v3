VERSION 5.00
Object = "{2864468C-765F-4EF3-B8CF-42666C92B545}#1.0#0"; "scvncctrl.dll"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmReunionVirtual 
   Caption         =   "Participar en reuni�n virtual"
   ClientHeight    =   8745
   ClientLeft      =   1650
   ClientTop       =   1800
   ClientWidth     =   12795
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmReunionVirtual.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   8745
   ScaleWidth      =   12795
   Begin VB.PictureBox PicAbrirProce 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   165
      Left            =   12540
      Picture         =   "frmReunionVirtual.frx":0442
      ScaleHeight     =   165
      ScaleWidth      =   165
      TabIndex        =   6
      Top             =   30
      Visible         =   0   'False
      Width           =   165
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   120
      Top             =   930
   End
   Begin ViewerXCtl.CSC_ViewerXControl viewerReuniones 
      Height          =   7815
      Left            =   60
      TabIndex        =   4
      Top             =   900
      Width           =   12675
      HostIP          =   "localhost"
      Port            =   5900
      Password        =   ""
      CustomCompression=   0   'False
      CustomCompressionLevel=   6
      JPEGCompression =   -1  'True
      JPEGCompressionLevel=   9
      CopyRect        =   -1  'True
      EmulateThreeButton=   -1  'True
      SwapMouseButtons=   0   'False
      Encoding        =   8
      ViewOnly        =   0   'False
      RestrictPixel   =   0   'False
      ScaleNum        =   100
      ScaleDen        =   100
      ScaleEnable     =   0   'False
      FullScreen      =   0   'False
      LocalCursor     =   1
      MessageBoxes    =   0   'False
      DisableClipboard=   -1  'True
      ThumbnailMode   =   0   'False
      LoginType       =   0
      MsUser          =   ""
      MsDomain        =   ""
      MsPassword      =   ""
      ProxyIP         =   ""
      ProxyPort       =   -1
      ProxyUser       =   ""
      ProxyPassword   =   ""
      ProxyType       =   0
      ConnectionBar   =   1
      StretchMode     =   0
      ListenPort      =   5500
      DisconnectedText=   ""
      ConnectingText  =   ""
      ListeningText   =   ""
      CtrlKeyPressed  =   0   'False
      AltKeyPressed   =   0   'False
      MouseCursorMode =   0
      UVNCS_EnableSecurity=   0   'False
      UVNCS_KeyFilePath=   "rc4.key"
      _cx             =   5080
      _cy             =   5080
   End
   Begin VB.PictureBox picReunion 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   825
      Left            =   30
      ScaleHeight     =   765
      ScaleWidth      =   12675
      TabIndex        =   1
      Top             =   0
      Width           =   12735
      Begin VB.PictureBox picCerrarProce 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   220
         Left            =   12450
         Picture         =   "frmReunionVirtual.frx":07A9
         ScaleHeight     =   225
         ScaleWidth      =   225
         TabIndex        =   5
         Top             =   0
         Width           =   220
      End
      Begin VB.CommandButton cmdReuniones 
         Caption         =   "Entrar en la reuni�n"
         Height          =   345
         Left            =   8490
         TabIndex        =   0
         Top             =   200
         Width           =   1725
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcReuniones 
         Height          =   285
         Left            =   1740
         TabIndex        =   3
         Top             =   240
         Width           =   6600
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   8
         Columns(0).Width=   11642
         Columns(0).Caption=   "TEXTO"
         Columns(0).Name =   "TEXTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "REU"
         Columns(1).Name =   "REU"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ANYO"
         Columns(2).Name =   "ANYO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "GMN1"
         Columns(3).Name =   "GMN1"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "PROCE"
         Columns(4).Name =   "PROCE"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "PWD"
         Columns(5).Name =   "PWD"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "FECPWD"
         Columns(6).Name =   "FECPWD"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "PUERTO"
         Columns(7).Name =   "PUERTO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   11642
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblReunion 
         Caption         =   "Reuniones Virtuales :"
         Height          =   255
         Left            =   150
         TabIndex        =   2
         Top             =   270
         Width           =   1575
      End
   End
End
Attribute VB_Name = "frmReunionVirtual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables de seguridad
Private m_bRMat As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bSoloInvitado As Boolean

Private m_oGestorReuniones As CGestorReuniones

Private m_sEntrarReu As String
Private m_sSalirReu As String

Private m_bReunion As Boolean
Private m_bOculto As Boolean
Private m_bReuFinalizada As Boolean

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_REUNIONVIRTUAL, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        frmReunionVirtual.caption = Ador(0).Value ' 1 Participar en reuni�n virtual
        Ador.MoveNext
        lblReunion.caption = Ador(0).Value ' 2 reuniones virtuales
        Ador.MoveNext
        cmdReuniones.caption = Ador(0).Value ' 3 entrar en la reunion
        m_sEntrarReu = Ador(0).Value
        Ador.MoveNext
        m_sSalirReu = Ador(0).Value ' 4 salir de la reunion
        
        Ador.Close

    End If
    
    Set Ador = Nothing
        
End Sub


Private Sub Arrange()

If Me.WindowState = 0 Then
    If Me.Height <= 3960 Then
        Me.Height = 3960
    End If
    If Me.Width <= 4890 Then
        Me.Width = 4890
    End If
End If
If Me.WindowState <> 1 Then
    If m_bOculto Then
        ' ajusto el tama�o del visor al tama�o del formulario
        viewerReuniones.Width = Me.Width - 260
        viewerReuniones.Height = Me.Height - 620
        viewerReuniones.Top = 150
        
        PicAbrirProce.Left = viewerReuniones.Left + viewerReuniones.Width - 260
        PicAbrirProce.Top = viewerReuniones.Top - 160
    Else
        ' ajusto el tama�o del visor al tama�o del formulario
        viewerReuniones.Width = Me.Width - 260
        viewerReuniones.Height = Me.Height - picReunion.Height - 520
        viewerReuniones.Top = 900
        ' ajusto el tama�o del picture y los botones para que se adapten al formulario
        picReunion.Width = viewerReuniones.Width + 30
        
        picCerrarProce.Left = picReunion.Left + picReunion.Width - 260
        picCerrarProce.Top = picReunion.Top
        
        If cmdReuniones.Left >= picReunion.Left + picReunion.Width - cmdReuniones.Width Or cmdReuniones.Left < 8850 Then
            If sdbcReuniones.Width < 1150 Then
                sdbcReuniones.Width = 1150
            End If
            cmdReuniones.Left = picReunion.Left + picReunion.Width - cmdReuniones.Width - 100
            sdbcReuniones.Width = cmdReuniones.Left - lblReunion.Width - 300
        End If
        sdbcReuniones.Columns("TEXTO").Width = sdbcReuniones.Width
    End If
End If



End Sub

''' <summary>
''' Entrar en una reuni�n
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,4</remarks>
Private Sub cmdReuniones_Click()
Dim sContrasenia As String
Dim teserror As TipoErrorSummit


If m_bReunion Then
    viewerReuniones.Disconnect
    Timer1.Enabled = False
    teserror = m_oGestorReuniones.GuardarHistAsistenteReunion(sdbcReuniones.Columns("REU").Value, CInt(sdbcReuniones.Columns("ANYO").Value), sdbcReuniones.Columns("GMN1").Value, CLng(sdbcReuniones.Columns("PROCE").Value), oUsuarioSummit.Cod)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    teserror = m_oGestorReuniones.EliminarAsistenteReunion(sdbcReuniones.Columns("REU").Value, oUsuarioSummit.Cod, True, m_bReuFinalizada)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    m_bReuFinalizada = False
    sdbcReuniones.Text = ""
    sdbcReuniones.RemoveAll
    
    PonerInterfazReunion
    m_bReunion = False
Else
    If sdbcReuniones.Columns("REU").Value <> "" Then
        sContrasenia = FSGSLibrary.EncriptarAES("VNC_PWD", CStr(sdbcReuniones.Columns("PWD").Value), False, CDate(sdbcReuniones.Columns("FECPWD").Value), 1, TipoDeUsuario.Persona)
        
        teserror = m_oGestorReuniones.GuardarAsistenteReunion(sdbcReuniones.Columns("REU").Value, CInt(sdbcReuniones.Columns("ANYO").Value), sdbcReuniones.Columns("GMN1").Value, CLng(sdbcReuniones.Columns("PROCE").Value), oUsuarioSummit.Cod)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        viewerReuniones.Port = CInt(sdbcReuniones.Columns("PUERTO").Value)
        viewerReuniones.Password = sContrasenia
        viewerReuniones.Connect
        
        Timer1.Enabled = True
        
        PonerInterfazReunion
        m_bReunion = True
    Else
        ' mensaje diciendo que debe seleccionar una reunion del combo
        oMensajes.MensajeOKOnly 830, TipoIconoMensaje.Information
    End If
End If

End Sub


Private Sub PonerInterfazReunion()
If m_bReunion Then
    picReunion.BackColor = &H8000000F
    lblReunion.BackColor = &H8000000F
    cmdReuniones.caption = m_sEntrarReu
    sdbcReuniones.Enabled = True
    picCerrarProce.BackColor = &H8000000F
    
    lblReunion.ForeColor = RGB(0, 0, 0)
    picCerrarProce.ForeColor = RGB(0, 0, 0)
Else
    picReunion.BackColor = RGB(128, 0, 0)
    lblReunion.BackColor = RGB(128, 0, 0)
    cmdReuniones.caption = m_sSalirReu
    sdbcReuniones.Enabled = False
    picCerrarProce.BackColor = RGB(128, 0, 0)
    
    lblReunion.ForeColor = RGB(255, 255, 255)
    picCerrarProce.ForeColor = RGB(255, 255, 255)
End If

End Sub

Private Sub Form_Load()
    ConfigurarSeguridad
    
    m_bSoloInvitado = False
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        If oUsuarioSummit.EsInvitado Then
            If oUsuarioSummit.Acciones Is Nothing Then
                m_bSoloInvitado = True
            Else
                If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREUConsultar)) Is Nothing Then
                    m_bSoloInvitado = True
                End If
            End If
        End If
    End If
    
    
    CargarRecursos
    PonerFieldSeparator Me
    Arrange
    
    Set m_oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
    m_bReunion = False
    m_bOculto = False
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If m_bReunion Then
        cmdReuniones_Click
    End If
    m_bOculto = False
End Sub

Private Sub PicAbrirProce_Click()
    m_bOculto = False
    picReunion.Visible = True
    PicAbrirProce.Visible = False
    picCerrarProce.Visible = True
    
    Arrange
End Sub
Private Sub PicAbrirProce_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    PicAbrirProce.BorderStyle = 1
    PicAbrirProce.BorderStyle = 0
End Sub

Private Sub picCerrarProce_Click()
    m_bOculto = True
    picReunion.Visible = False
    picCerrarProce.Visible = False
    PicAbrirProce.Visible = True
    
    Arrange
End Sub
Private Sub picCerrarProce_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picCerrarProce.BorderStyle = 1
    picCerrarProce.BorderStyle = 0
End Sub

Private Sub sdbcReuniones_DropDown()
Dim sTextoCombo As String
Dim adoProcesos As Ador.Recordset
    
    
    sdbcReuniones.RemoveAll
        
    Screen.MousePointer = vbHourglass
    
    If m_bRMat Then
        Set adoProcesos = m_oGestorReuniones.CargarProcesosDeReunionVirtual(, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True, m_bSoloInvitado)
    Else
        Set adoProcesos = m_oGestorReuniones.CargarProcesosDeReunionVirtual(, , basOptimizacion.gCodPersonaUsuario, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True, m_bSoloInvitado)
    End If
    While Not adoProcesos.EOF
        sTextoCombo = adoProcesos("REU").Value & " " & adoProcesos("REF").Value & " Tema : " & CStr(adoProcesos("ANYO").Value) & "/" & adoProcesos("GMN1").Value & "/" & CStr(adoProcesos("COD").Value) & " " & adoProcesos("DEN").Value
        sdbcReuniones.AddItem sTextoCombo & Chr(m_lSeparador) & adoProcesos("REU").Value & Chr(m_lSeparador) & adoProcesos.Fields("ANYO").Value & Chr(m_lSeparador) & adoProcesos.Fields("GMN1").Value & Chr(m_lSeparador) & adoProcesos.Fields("COD").Value & Chr(m_lSeparador) & adoProcesos.Fields("PWD").Value & Chr(m_lSeparador) & adoProcesos.Fields("FECPWD").Value & Chr(m_lSeparador) & adoProcesos.Fields("PUERTO").Value
        adoProcesos.MoveNext
    Wend
    adoProcesos.Close
    Set adoProcesos = Nothing

    Screen.MousePointer = vbNormal

End Sub

Private Sub ConfigurarSeguridad()
Dim i As Integer

    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
    
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestMatComp)) Is Nothing) Then
            m_bRMat = True
        End If
        
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestUsuUON)) Is Nothing) Then
            m_bRUsuUON = True
        End If
        
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREURestUsuDep)) Is Nothing) Then
            m_bRUsuDep = True
        End If
        
        
    End If

    
        
End Sub

''' <summary>
''' Verifica el usuario en la reuni�n si puede ver el proceso de la reu, tb actualiza la fecha de control de conexion
''' </summary>
''' <remarks>Llamada desde: Es un timer, se llama solo cada 5 segundos; Tiempo m�ximo:1ms</remarks>
Private Sub Timer1_Timer()
Dim adoRecordset As ADODB.Recordset
Dim iReunionComprobada As Integer
Dim sMensaje As String


Set adoRecordset = m_oGestorReuniones.DevolverDatosReunion(sdbcReuniones.Columns("REU").Value, sdbcReuniones.Columns("ANYO").Value, sdbcReuniones.Columns("GMN1").Value, sdbcReuniones.Columns("PROCE").Value, True)

If adoRecordset.EOF Then
    iReunionComprobada = ComprobarReunion
    Select Case iReunionComprobada
    Case 1
        m_bReuFinalizada = True
        cmdReuniones_Click
        Screen.MousePointer = vbNormal
        oMensajes.MensajeOKOnly 829, TipoIconoMensaje.Information
        Exit Sub
    Case 2
        m_bReuFinalizada = False
        cmdReuniones_Click
        Screen.MousePointer = vbNormal
        sMensaje = oMensajes.CargarTextoMensaje(828) & vbCrLf & oMensajes.CargarTextoMensaje(835)
        oMensajes.MensajeOKOnly sMensaje, TipoIconoMensaje.Information
        Exit Sub
    End Select
    
End If

If CInt("0" & Timer1.Tag) >= 4 Then
    Set adoRecordset = m_oGestorReuniones.DevolverDatosReunion(sdbcReuniones.Columns("REU").Value, , , , True)

    If Not adoRecordset.EOF Then
        m_oGestorReuniones.ActualizarAsistenteReunion sdbcReuniones.Columns("REU").Value, basOptimizacion.gvarCodUsuario
        Timer1.Tag = 0
    End If
Else
    Timer1.Tag = CInt("0" & Timer1.Tag) + 1
End If


End Sub

Private Function ComprobarReunion() As Integer
'1 - Si se ha finalizado la reu, 2- Si no tiene permisos para ver el proceso,
'3- Si puede ver el proceso y sigue conectado, 4-Error al guardar en BD
Dim bReunionActiva As Boolean
Dim adoDatosReunion As Ador.Recordset
Dim iAnyo As Integer
Dim sGmn1 As String
Dim iProce As Long
Dim teserror As TipoErrorSummit
Dim sTextoCombo As String

    Screen.MousePointer = vbHourglass
    
    Set adoDatosReunion = m_oGestorReuniones.DevolverDatosReunion(sdbcReuniones.Columns("REU").Value, , , , True)
    If adoDatosReunion.EOF Then
        ComprobarReunion = 1
        adoDatosReunion.Close
        Set adoDatosReunion = Nothing
        Exit Function
    Else
        iAnyo = adoDatosReunion("ANYO").Value
        sGmn1 = adoDatosReunion("GMN1").Value
        iProce = adoDatosReunion("PROCE").Value
    End If
    adoDatosReunion.Close
    Set adoDatosReunion = Nothing

                
    If m_bRMat Then
        Set adoDatosReunion = m_oGestorReuniones.CargarProcesosDeReunionVirtual(sdbcReuniones.Columns("REU").Value, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodPersonaUsuario, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True, m_bSoloInvitado, iAnyo, sGmn1, iProce)
    Else
        Set adoDatosReunion = m_oGestorReuniones.CargarProcesosDeReunionVirtual(sdbcReuniones.Columns("REU").Value, , basOptimizacion.gCodPersonaUsuario, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, True, m_bSoloInvitado, iAnyo, sGmn1, iProce)
    End If
        
    
    If Not adoDatosReunion.EOF Then
        sTextoCombo = adoDatosReunion("REU").Value & " " & adoDatosReunion("REF").Value & " Tema : " & CStr(iAnyo) & "/" & sGmn1 & "/" & CStr(iProce) & " " & adoDatosReunion("DEN").Value
        adoDatosReunion.Close
        Set adoDatosReunion = Nothing

        teserror = m_oGestorReuniones.GuardarHistAsistenteReunion(sdbcReuniones.Columns("REU").Value, CInt(sdbcReuniones.Columns("ANYO").Value), sdbcReuniones.Columns("GMN1").Value, CLng(sdbcReuniones.Columns("PROCE").Value), oUsuarioSummit.Cod)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            ComprobarReunion = 4
            Exit Function
        End If
                                                    
        sdbcReuniones.Text = sTextoCombo
        sdbcReuniones.Columns("ANYO").Value = iAnyo
        sdbcReuniones.Columns("GMN1").Value = sGmn1
        sdbcReuniones.Columns("PROCE").Value = iProce

        teserror = m_oGestorReuniones.GuardarAsistenteReunion(sdbcReuniones.Columns("REU").Value, iAnyo, sGmn1, iProce, basOptimizacion.gvarCodUsuario)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            ComprobarReunion = 4
            Exit Function
        End If
        Screen.MousePointer = vbNormal
        ComprobarReunion = 3

    Else
        ComprobarReunion = 2
    End If


    Screen.MousePointer = vbNormal

End Function
