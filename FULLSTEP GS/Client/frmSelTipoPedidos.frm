VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSelTipoPedidos 
   Caption         =   "DSeleccion Tipo de Pedidos"
   ClientHeight    =   4545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11025
   Icon            =   "frmSelTipoPedidos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4545
   ScaleWidth      =   11025
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBGrid sdbgSelTipoPedidos 
      Height          =   4170
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10845
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSelTipoPedidos.frx":0562
      stylesets(1).Name=   "StringTachado"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(1).Picture=   "frmSelTipoPedidos.frx":057E
      stylesets(2).Name=   "Bloqueado"
      stylesets(2).BackColor=   12632256
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSelTipoPedidos.frx":059A
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   1005
      Columns(0).Name =   "SEL"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   1773
      Columns(1).Caption=   "COD"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   6244
      Columns(2).Caption=   "DEN"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Caption=   "CONCEPTO"
      Columns(3).Name =   "CONCEPTO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   3200
      Columns(4).Caption=   "RECEPCIONABLE"
      Columns(4).Name =   "RECEPCIONABLE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   3200
      Columns(5).Caption=   "ALMACENABLE"
      Columns(5).Name =   "ALMACENABLE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID"
      Columns(6).Name =   "ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   19129
      _ExtentY        =   7355
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSelTipoPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Variables privadas
Private m_sCadenaTipoPedidos As String
Private m_bCambiosRealizados As Boolean
Private m_sGasto As String
Private m_sInversion As String
Private m_sGastoInversion As String
Private m_sNoAlmacenable As String
Private m_sObligatorioAlmacenar As String
Private m_sOpcionalAlmacenar As String
Private m_sNoRecepcionar As String
Private m_sObligatorioRecepcionar As String
Private m_sOpcionalRecepcionar As String

'Variables publicas
Public g_sTipoPedidos As String
Public g_bModoEdicion As Boolean


Private Sub Form_Load()
    m_sCadenaTipoPedidos = g_sTipoPedidos
    PonerFieldSeparator Me
    CargarRecursos
    'Configura las columnas bloqueadas del grid
    ConfigurarGrid
    'Cargamos los tipos de pedido en la grid
    CargarTipoPedidos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_sTipoPedidos = m_sCadenaTipoPedidos
End Sub

''' <summary>
''' Configura las columnas bloqueadas del grid en funcion de si esta en modo consulta o no
''' </summary>
Private Sub ConfigurarGrid()
    If g_bModoEdicion Then
        sdbgSelTipoPedidos.Columns("SEL").Locked = False
    Else
        sdbgSelTipoPedidos.Columns("SEL").Locked = True
    End If
End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELTIPOSPEDIDO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        
        Me.caption = ador(0).Value
        ador.MoveNext
        sdbgSelTipoPedidos.Columns("COD").caption = ador(0).Value
        ador.MoveNext
        sdbgSelTipoPedidos.Columns("DEN").caption = ador(0).Value
        ador.MoveNext
        sdbgSelTipoPedidos.Columns("ALMACENABLE").caption = ador(0).Value
        ador.MoveNext
        sdbgSelTipoPedidos.Columns("RECEPCIONABLE").caption = ador(0).Value
        ador.MoveNext
        sdbgSelTipoPedidos.Columns("CONCEPTO").caption = ador(0).Value
        ador.MoveNext
        m_sGasto = ador(0).Value
        ador.MoveNext
        m_sInversion = ador(0).Value
        ador.MoveNext
        m_sGastoInversion = ador(0).Value
        ador.MoveNext
        m_sNoAlmacenable = ador(0).Value
        ador.MoveNext
        m_sObligatorioAlmacenar = ador(0).Value
        ador.MoveNext
        m_sOpcionalAlmacenar = ador(0).Value
        ador.MoveNext
        m_sNoRecepcionar = ador(0).Value
        ador.MoveNext
        m_sObligatorioRecepcionar = ador(0).Value
        ador.MoveNext
        m_sOpcionalRecepcionar = ador(0).Value
        
        ador.Close
        
    End If
    
    Set ador = Nothing
    
End Sub



''' <summary>
''' Carga todos los tipos de pedido
''' </summary>
''' <remarks>Llamada desde:Form_load; Tiempo m�ximo:0,3seg.</remarks>
Private Sub CargarTipoPedidos()
    Dim oTiposPedido As CTiposPedido
    Dim oTipoPedido As CTipoPedido
    Set oTiposPedido = oFSGSRaiz.Generar_CTiposPedido
    Dim bAsignado As Boolean
    
    'Cargo todos los tipos de pedidos
    oTiposPedido.CargarTodosLosTiposPedidos (gParametrosGenerales.gIdioma)
    
    Dim i As Integer
    Dim arrAux() As String
    arrAux = Split(g_sTipoPedidos, ",")
    
    For Each oTipoPedido In oTiposPedido
        'Por cada tipo de pedido miro si en este atributo ya esta seleccionado
        bAsignado = False
        For i = 0 To UBound(arrAux)
            If arrAux(i) = "-1" Then
                'Se marcaran todos los tipos de pedido
                bAsignado = True
                m_sCadenaTipoPedidos = oTipoPedido.Id & ","
            ElseIf arrAux(i) <> "" Then
                If oTipoPedido.Id = arrAux(i) Then
                    bAsignado = True
                    Exit For
                End If
            End If
        Next i
        
        sdbgSelTipoPedidos.AddItem bAsignado & Chr(m_lSeparador) & oTipoPedido.Cod & Chr(m_lSeparador) & oTipoPedido.Den & Chr(m_lSeparador) & oTipoPedido.CodConcep & Chr(m_lSeparador) & oTipoPedido.CodRecep & Chr(m_lSeparador) & oTipoPedido.CodAlmac & Chr(m_lSeparador) & oTipoPedido.Id
    Next

End Sub
''' <summary>
''' Evento que salta al seleccionar un tipo de pedido en el grid
''' Actualiza la cadena de tipo de pedidos seleccionados
''' </summary>
''' <param name="Cancel">Si ha cancelado el formulario</param>
''' <remarks>Tiempo m�ximo:0,1seg.</remarks>
Private Sub sdbgSelTipoPedidos_Change()
    If sdbgSelTipoPedidos.col = -1 Then Exit Sub
    
    m_bCambiosRealizados = True
    
    If sdbgSelTipoPedidos.Columns("SEL").Value = "-1" Or sdbgSelTipoPedidos.Columns("SEL").Value = "1" Then
        'Seleccionar Empresa
        If Right(m_sCadenaTipoPedidos, 1) = "," Then
            m_sCadenaTipoPedidos = m_sCadenaTipoPedidos & sdbgSelTipoPedidos.Columns("ID").Value & ","
        Else
            m_sCadenaTipoPedidos = m_sCadenaTipoPedidos & "," & sdbgSelTipoPedidos.Columns("ID").Value
        End If
        
    Else
        'Deseleccionar Tipo Pedido
        Dim sCadenaABuscar As String
        Dim sCadenaNueva As String
        Dim arrAux() As String
        arrAux = Split(m_sCadenaTipoPedidos, ",")
        sCadenaABuscar = sdbgSelTipoPedidos.Columns("ID").Value

        Dim i As Integer
        For i = 0 To UBound(arrAux)
            If sCadenaABuscar <> arrAux(i) And arrAux(i) <> "" Then
                sCadenaNueva = sCadenaNueva & arrAux(i) & ","
            End If
        Next i
        m_sCadenaTipoPedidos = sCadenaNueva
        
    End If
    sdbgSelTipoPedidos.Update

End Sub
''' <summary>
''' Evento que salta cada vez que se carga una fila en el grid
''' </summary>
''' <param name="Bookmark">Bookmark de la fila que se carga</param>
Private Sub sdbgSelTipoPedidos_RowLoaded(ByVal Bookmark As Variant)
    'Se le ponen los textos a los valores de CONCEPTO, ALMACENABLE,RECEPCIONABLE de los tipos de pedido
    Select Case sdbgSelTipoPedidos.Columns("CONCEPTO").Value
        Case TipoConcepto.Gasto
            sdbgSelTipoPedidos.Columns("CONCEPTO").Value = m_sGasto
        Case TipoConcepto.Inversion
            sdbgSelTipoPedidos.Columns("CONCEPTO").Value = m_sInversion
        Case TipoConcepto.Ambos
            sdbgSelTipoPedidos.Columns("CONCEPTO").Value = m_sGastoInversion
    End Select
    
    Select Case sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value
        Case TipoArtRecepcionable.NoRececpionar
            sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value = m_sNoRecepcionar
        Case TipoArtRecepcionable.ObligatorioRececpionar
            sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value = m_sObligatorioRecepcionar
        Case TipoArtRecepcionable.OpcionalRececpionar
            sdbgSelTipoPedidos.Columns("RECEPCIONABLE").Value = m_sOpcionalRecepcionar
    End Select
    
    Select Case sdbgSelTipoPedidos.Columns("ALMACENABLE").Value
        Case TipoArtAlmacenable.NoAlmacenable
            sdbgSelTipoPedidos.Columns("ALMACENABLE").Value = m_sNoAlmacenable
        Case TipoArtAlmacenable.ObligatorioAlmacenar
            sdbgSelTipoPedidos.Columns("ALMACENABLE").Value = m_sObligatorioAlmacenar
        Case TipoArtAlmacenable.OpcionalAlmacenar
            sdbgSelTipoPedidos.Columns("ALMACENABLE").Value = m_sOpcionalAlmacenar
    End Select
End Sub
