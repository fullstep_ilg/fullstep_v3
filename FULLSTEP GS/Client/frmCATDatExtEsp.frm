VERSION 5.00
Begin VB.Form frmCATDatExtEsp 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DCargar especificaciones"
   ClientHeight    =   5505
   ClientLeft      =   1815
   ClientTop       =   4020
   ClientWidth     =   6795
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtEsp.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5505
   ScaleWidth      =   6795
   Begin VB.CommandButton cmdDrive 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6345
      Picture         =   "frmCATDatExtEsp.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   450
      Width           =   315
   End
   Begin VB.TextBox txtCaracter 
      Height          =   285
      Left            =   3560
      TabIndex        =   3
      Text            =   " _ "
      Top             =   4640
      Width           =   285
   End
   Begin VB.CheckBox chlSoloRuta 
      BackColor       =   &H00808000&
      Caption         =   "DGuardar s�lo ruta"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   4200
      Width           =   5295
   End
   Begin VB.TextBox txtRuta 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   420
      Width           =   6105
   End
   Begin VB.DirListBox lstDirIma 
      Height          =   3240
      Left            =   120
      TabIndex        =   1
      Top             =   855
      Width           =   6555
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3300
      TabIndex        =   5
      Top             =   5160
      Width           =   1155
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "DAceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1980
      TabIndex        =   4
      Top             =   5160
      Width           =   1155
   End
   Begin VB.Label lblCaracter 
      BackColor       =   &H00808000&
      Caption         =   "DCar�cter separador de c�digo de art�culo:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   4680
      Width           =   3255
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      X1              =   120
      X2              =   6660
      Y1              =   4560
      Y2              =   4560
   End
   Begin VB.Label lblSelCarpeta 
      BackColor       =   &H00808000&
      Caption         =   "DSeleccione la carpeta donde se encuentran las especificaciones:"
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   5775
   End
End
Attribute VB_Name = "frmCATDatExtEsp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oProve As CProveedor
Private bRespetarControl As Boolean
Private bChangeTxt As Boolean

Private Sub cmdAceptar_Click()
    Dim oFos As FileSystemObject
    Dim oFolder As Folder
    Dim oFiles As Files

    If Trim(txtRuta.Text) = "" Then
        MsgBox lblSelCarpeta.caption, vbExclamation, "FULLSTEP GS"
        Exit Sub
    End If
    
    Set oFos = New FileSystemObject
    If Not oFos.FolderExists(Trim(txtRuta.Text)) Then
        'si la carpeta no existe:
        oMensajes.CarpetaNoExiste
        Set oFos = Nothing
        If Me.Visible Then txtRuta.SetFocus
        Exit Sub
    Else
        'Comprueba que la carpeta contenga archivos:
        Set oFolder = oFos.GetFolder(txtRuta.Text)
        Set oFiles = oFolder.Files
        If oFiles.Count = 0 Then
            oMensajes.NoExistenArchivos
            Set oFiles = Nothing
            Set oFolder = Nothing
            Set oFos = Nothing
            If Me.Visible Then txtRuta.SetFocus
            Exit Sub
        End If
    End If
    
    Set oFiles = Nothing
    Set oFolder = Nothing
    Set oFos = Nothing
    
    'LLama a la pantalla de progreso,que es la que va cargando las especificaciones
    frmCATDatExtEsp2.g_sPath = Trim(txtRuta.Text)
    frmCATDatExtEsp2.g_sCaracter = Trim$(txtCaracter.Text)
    frmCATDatExtEsp2.g_bSoloRuta = chlSoloRuta.Value
    Set frmCATDatExtEsp2.g_oProve = g_oProve
    Me.Hide
    frmCATDatExtEsp2.Show 1
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub




Private Sub Form_Load()

    Me.Height = 5880
    Me.Width = 6885
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    txtRuta.Text = lstDirIma.Path

    txtCaracter.Text = " _ "
    
End Sub

Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_ESP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value
        ador.MoveNext
        lblSelCarpeta.caption = ador(0).Value
        ador.MoveNext
        cmdAceptar.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value
        ador.MoveNext
        chlSoloRuta.caption = ador(0).Value
        ador.MoveNext
        lblCaracter.caption = ador(0).Value
        
        ador.Close
    End If
    
    Set ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oProve = Nothing
End Sub
Private Sub cmdDrive_Click()
Dim sBuffer As String
Dim sTit As String
Dim sInicio As String

On Error GoTo Error

sTit = lblSelCarpeta.caption
sInicio = txtRuta.Text
If sInicio = "" Then sInicio = CurDir$
sBuffer = BrowseForFolder(Me.hWnd, sTit, sInicio)

If sBuffer <> "" Then
    bRespetarControl = True
    lstDirIma.Path = sBuffer
    txtRuta.Text = sBuffer
    bChangeTxt = False
    bRespetarControl = False
End If
Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    
    txtRuta.Text = ""
    bChangeTxt = False
    bRespetarControl = False
    
End Sub

Private Sub lstDirIma_Change()
    If Not bRespetarControl Then
        bRespetarControl = True
        txtRuta.Text = lstDirIma.Path
        bRespetarControl = False
    End If
    
End Sub

Private Sub txtRuta_Change()
    
    If Not bRespetarControl Then
        bChangeTxt = True
    End If
End Sub

Private Sub txtRuta_Validate(Cancel As Boolean)

On Error GoTo Error

    If txtRuta.Text <> lstDirIma.Path Then
        bRespetarControl = True
        lstDirIma.Path = txtRuta.Text
        bChangeTxt = False
        bRespetarControl = False
    End If
    Exit Sub

Error:
    TratarErrorBrowse err.Number, err.Description
    
    txtRuta.Text = ""
    bChangeTxt = False
    bRespetarControl = False
    
End Sub
