VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmREU 
   Caption         =   "Administrador de reuniones"
   ClientHeight    =   6165
   ClientLeft      =   1590
   ClientTop       =   1995
   ClientWidth     =   11070
   Icon            =   "frmREU.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   6165
   ScaleWidth      =   11070
   Begin TabDlg.SSTab sstabReu 
      Height          =   6075
      Left            =   0
      TabIndex        =   0
      Top             =   75
      Width           =   11055
      _ExtentX        =   19500
      _ExtentY        =   10716
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Planificaci�n de fechas"
      TabPicture(0)   =   "frmREU.frx":014A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(1)=   "picReuPla"
      Tab(0).Control(2)=   "picNavigate"
      Tab(0).Control(3)=   "picEdit"
      Tab(0).Control(4)=   "fraFechas"
      Tab(0).Control(5)=   "lblHint"
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Gesti�n de reuniones"
      TabPicture(1)   =   "frmREU.frx":0166
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "picGestEdit"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "picGestNavigate"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "picGest"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "fraGest"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   -64680
         Top             =   120
      End
      Begin VB.PictureBox picReuPla 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4275
         Left            =   -74880
         ScaleHeight     =   4275
         ScaleWidth      =   10755
         TabIndex        =   1
         Top             =   1260
         Width           =   10755
         Begin SSDataWidgets_B.SSDBGrid sdbgReuPla 
            Height          =   4215
            Left            =   30
            TabIndex        =   2
            Top             =   30
            Width           =   10695
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   15
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmREU.frx":0182
            UseGroups       =   -1  'True
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Groups.Count    =   3
            Groups(0).Width =   10795
            Groups(0).Caption=   "Procesos"
            Groups(0).HasHeadForeColor=   -1  'True
            Groups(0).HasHeadBackColor=   -1  'True
            Groups(0).HeadForeColor=   16777215
            Groups(0).HeadBackColor=   8421504
            Groups(0).Columns.Count=   5
            Groups(0).Columns(0).Width=   1058
            Groups(0).Columns(0).Caption=   "A�o"
            Groups(0).Columns(0).Name=   "A�O"
            Groups(0).Columns(0).DataField=   "Column 0"
            Groups(0).Columns(0).DataType=   8
            Groups(0).Columns(0).FieldLen=   256
            Groups(0).Columns(0).Locked=   -1  'True
            Groups(0).Columns(0).HasBackColor=   -1  'True
            Groups(0).Columns(0).BackColor=   16777184
            Groups(0).Columns(1).Width=   1111
            Groups(0).Columns(1).Caption=   "Comm"
            Groups(0).Columns(1).Name=   "COMM"
            Groups(0).Columns(1).DataField=   "Column 1"
            Groups(0).Columns(1).DataType=   8
            Groups(0).Columns(1).FieldLen=   256
            Groups(0).Columns(1).Locked=   -1  'True
            Groups(0).Columns(1).HasBackColor=   -1  'True
            Groups(0).Columns(1).BackColor=   16777184
            Groups(0).Columns(2).Width=   1296
            Groups(0).Columns(2).Caption=   "Cod"
            Groups(0).Columns(2).Name=   "COD"
            Groups(0).Columns(2).DataField=   "Column 2"
            Groups(0).Columns(2).DataType=   8
            Groups(0).Columns(2).FieldLen=   256
            Groups(0).Columns(2).Locked=   -1  'True
            Groups(0).Columns(2).HasBackColor=   -1  'True
            Groups(0).Columns(2).BackColor=   16777184
            Groups(0).Columns(3).Width=   3651
            Groups(0).Columns(3).Caption=   "Descripci�n"
            Groups(0).Columns(3).Name=   "DESCR"
            Groups(0).Columns(3).DataField=   "Column 3"
            Groups(0).Columns(3).DataType=   8
            Groups(0).Columns(3).FieldLen=   256
            Groups(0).Columns(3).Locked=   -1  'True
            Groups(0).Columns(3).HasBackColor=   -1  'True
            Groups(0).Columns(3).BackColor=   16777184
            Groups(0).Columns(4).Width=   3678
            Groups(0).Columns(4).Caption=   "Estado"
            Groups(0).Columns(4).Name=   "EST"
            Groups(0).Columns(4).DataField=   "Column 4"
            Groups(0).Columns(4).DataType=   8
            Groups(0).Columns(4).FieldLen=   256
            Groups(0).Columns(4).Locked=   -1  'True
            Groups(0).Columns(4).HasBackColor=   -1  'True
            Groups(0).Columns(4).BackColor=   16777184
            Groups(1).Width =   10583
            Groups(1).Caption=   "Fechas"
            Groups(1).HasHeadForeColor=   -1  'True
            Groups(1).HasHeadBackColor=   -1  'True
            Groups(1).HeadForeColor=   16777215
            Groups(1).HeadBackColor=   8421504
            Groups(1).Columns.Count=   6
            Groups(1).Columns(0).Width=   1905
            Groups(1).Columns(0).Caption=   "Apertura"
            Groups(1).Columns(0).Name=   "APE"
            Groups(1).Columns(0).DataField=   "Column 5"
            Groups(1).Columns(0).DataType=   8
            Groups(1).Columns(0).FieldLen=   256
            Groups(1).Columns(0).Locked=   -1  'True
            Groups(1).Columns(0).HasBackColor=   -1  'True
            Groups(1).Columns(0).BackColor=   14745599
            Groups(1).Columns(1).Width=   1905
            Groups(1).Columns(1).Caption=   "L�m Ofer"
            Groups(1).Columns(1).Name=   "LIM"
            Groups(1).Columns(1).DataField=   "Column 6"
            Groups(1).Columns(1).DataType=   8
            Groups(1).Columns(1).FieldLen=   256
            Groups(1).Columns(1).Locked=   -1  'True
            Groups(1).Columns(1).HasBackColor=   -1  'True
            Groups(1).Columns(1).BackColor=   14745599
            Groups(1).Columns(2).Width=   1905
            Groups(1).Columns(2).Caption=   "Ultima"
            Groups(1).Columns(2).Name=   "ULTREU"
            Groups(1).Columns(2).DataField=   "Column 7"
            Groups(1).Columns(2).DataType=   8
            Groups(1).Columns(2).FieldLen=   256
            Groups(1).Columns(2).Locked=   -1  'True
            Groups(1).Columns(2).HasBackColor=   -1  'True
            Groups(1).Columns(2).BackColor=   14745599
            Groups(1).Columns(3).Width=   1905
            Groups(1).Columns(3).Caption=   "Presentaci�n"
            Groups(1).Columns(3).Name=   "PLA"
            Groups(1).Columns(3).DataField=   "Column 8"
            Groups(1).Columns(3).DataType=   8
            Groups(1).Columns(3).FieldLen=   256
            Groups(1).Columns(3).Locked=   -1  'True
            Groups(1).Columns(3).HasBackColor=   -1  'True
            Groups(1).Columns(3).BackColor=   16777215
            Groups(1).Columns(4).Width=   1905
            Groups(1).Columns(4).Caption=   "Necesidad"
            Groups(1).Columns(4).Name=   "NEC"
            Groups(1).Columns(4).DataField=   "Column 9"
            Groups(1).Columns(4).DataType=   8
            Groups(1).Columns(4).FieldLen=   256
            Groups(1).Columns(4).Locked=   -1  'True
            Groups(1).Columns(4).HasBackColor=   -1  'True
            Groups(1).Columns(4).BackColor=   14745599
            Groups(1).Columns(5).Width=   1058
            Groups(1).Columns(5).Caption=   "Decis."
            Groups(1).Columns(5).Name=   "DEC"
            Groups(1).Columns(5).DataField=   "Column 10"
            Groups(1).Columns(5).DataType=   8
            Groups(1).Columns(5).FieldLen=   256
            Groups(1).Columns(5).Locked=   -1  'True
            Groups(1).Columns(5).Style=   2
            Groups(2).Width =   10716
            Groups(2).Caption=   "Responsable"
            Groups(2).Columns.Count=   4
            Groups(2).Columns(0).Width=   3254
            Groups(2).Columns(0).Caption=   "Apellidos"
            Groups(2).Columns(0).Name=   "RESP"
            Groups(2).Columns(0).DataField=   "Column 11"
            Groups(2).Columns(0).DataType=   8
            Groups(2).Columns(0).FieldLen=   256
            Groups(2).Columns(0).Locked=   -1  'True
            Groups(2).Columns(0).HasBackColor=   -1  'True
            Groups(2).Columns(0).BackColor=   9562111
            Groups(2).Columns(1).Width=   1614
            Groups(2).Columns(1).Caption=   "Nombre"
            Groups(2).Columns(1).Name=   "RESPNOM"
            Groups(2).Columns(1).DataField=   "Column 12"
            Groups(2).Columns(1).DataType=   8
            Groups(2).Columns(1).FieldLen=   256
            Groups(2).Columns(1).Locked=   -1  'True
            Groups(2).Columns(1).HasBackColor=   -1  'True
            Groups(2).Columns(1).BackColor=   9562111
            Groups(2).Columns(2).Width=   2011
            Groups(2).Columns(2).Caption=   "Tfno"
            Groups(2).Columns(2).Name=   "RESPTFNO"
            Groups(2).Columns(2).DataField=   "Column 13"
            Groups(2).Columns(2).DataType=   8
            Groups(2).Columns(2).FieldLen=   256
            Groups(2).Columns(2).Locked=   -1  'True
            Groups(2).Columns(2).HasBackColor=   -1  'True
            Groups(2).Columns(2).BackColor=   9562111
            Groups(2).Columns(3).Width=   3836
            Groups(2).Columns(3).Caption=   "Mail"
            Groups(2).Columns(3).Name=   "EMAIL"
            Groups(2).Columns(3).DataField=   "Column 14"
            Groups(2).Columns(3).DataType=   8
            Groups(2).Columns(3).FieldLen=   256
            Groups(2).Columns(3).Locked=   -1  'True
            Groups(2).Columns(3).HasBackColor=   -1  'True
            Groups(2).Columns(3).BackColor=   9562111
            _ExtentX        =   18865
            _ExtentY        =   7435
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.PictureBox picCrearReu 
            BorderStyle     =   0  'None
            Height          =   975
            Left            =   0
            ScaleHeight     =   975
            ScaleWidth      =   9795
            TabIndex        =   35
            Top             =   0
            Width           =   9795
            Begin VB.TextBox txtFecha 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   720
               TabIndex        =   39
               Top             =   120
               Width           =   1110
            End
            Begin VB.CommandButton Command3 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1860
               Picture         =   "frmREU.frx":019E
               Style           =   1  'Graphical
               TabIndex        =   38
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   120
               Width           =   315
            End
            Begin VB.TextBox txtHora 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2880
               TabIndex        =   37
               Top             =   120
               Width           =   930
            End
            Begin VB.TextBox txtRef 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4830
               TabIndex        =   36
               Top             =   120
               Width           =   3150
            End
            Begin VB.Label Label4 
               BackStyle       =   0  'Transparent
               Caption         =   "Fecha:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   165
               Left            =   180
               TabIndex        =   42
               Top             =   180
               Width           =   495
            End
            Begin VB.Label Label5 
               BackStyle       =   0  'Transparent
               Caption         =   "Hora:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   2430
               TabIndex        =   41
               Top             =   180
               Width           =   435
            End
            Begin VB.Label Label6 
               BackStyle       =   0  'Transparent
               Caption         =   "Referencia:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   240
               Left            =   3960
               TabIndex        =   40
               Top             =   180
               Width           =   825
            End
         End
      End
      Begin VB.Frame fraGest 
         Height          =   675
         Left            =   150
         TabIndex        =   3
         Top             =   360
         Width           =   10755
         Begin VB.CommandButton cmdCalendar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3015
            Picture         =   "frmREU.frx":0728
            Style           =   1  'Graphical
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   240
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFecReu 
            Height          =   285
            Left            =   720
            TabIndex        =   5
            Top             =   240
            Width           =   2250
            DataFieldList   =   "Column 0"
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Fecha"
            Columns(0).Name =   "FECHA"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3228
            Columns(1).Caption=   "Fecha"
            Columns(1).Name =   "FECHACORTA"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   4075
            Columns(2).Caption=   "Referencia"
            Columns(2).Name =   "REF"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   3969
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   0
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblRef 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4650
            TabIndex        =   43
            Top             =   240
            Width           =   4530
         End
         Begin VB.Label Label2 
            Caption         =   "Referencia:"
            Height          =   255
            Left            =   3780
            TabIndex        =   6
            Top             =   300
            Width           =   945
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   150
            TabIndex        =   4
            Top             =   300
            Width           =   555
         End
      End
      Begin VB.PictureBox picGest 
         BackColor       =   &H00808080&
         Height          =   4395
         Left            =   165
         ScaleHeight     =   4335
         ScaleWidth      =   10695
         TabIndex        =   8
         Top             =   1140
         Width           =   10755
         Begin VB.CommandButton cmdHora 
            Enabled         =   0   'False
            Height          =   435
            Left            =   60
            Picture         =   "frmREU.frx":0CB2
            Style           =   1  'Graphical
            TabIndex        =   54
            Top             =   90
            UseMaskColor    =   -1  'True
            Width           =   435
         End
         Begin VB.CommandButton cmdEliminarProceso 
            Enabled         =   0   'False
            Height          =   435
            Left            =   60
            Picture         =   "frmREU.frx":1041
            Style           =   1  'Graphical
            TabIndex        =   53
            Top             =   2445
            UseMaskColor    =   -1  'True
            Width           =   435
         End
         Begin VB.CommandButton cmdA�adirProceso 
            Enabled         =   0   'False
            Height          =   435
            Left            =   60
            Picture         =   "frmREU.frx":10D3
            Style           =   1  'Graphical
            TabIndex        =   52
            Top             =   1854
            UseMaskColor    =   -1  'True
            Width           =   435
         End
         Begin VB.CommandButton cmdBajar 
            Enabled         =   0   'False
            Height          =   435
            Left            =   60
            Picture         =   "frmREU.frx":1155
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   1266
            UseMaskColor    =   -1  'True
            Width           =   435
         End
         Begin VB.CommandButton cmdSubir 
            Enabled         =   0   'False
            Height          =   435
            Left            =   60
            Picture         =   "frmREU.frx":1497
            Style           =   1  'Graphical
            TabIndex        =   44
            Top             =   678
            UseMaskColor    =   -1  'True
            Width           =   435
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgReuGest 
            Height          =   4095
            Left            =   600
            TabIndex        =   9
            Top             =   120
            Width           =   9990
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   13
            stylesets.count =   2
            stylesets(0).Name=   "Anulado"
            stylesets(0).BackColor=   11513775
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmREU.frx":17D9
            stylesets(1).Name=   "Normal"
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmREU.frx":17F5
            UseGroups       =   -1  'True
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Groups.Count    =   2
            Groups(0).Width =   10504
            Groups(0).Caption=   "Proceso"
            Groups(0).HasHeadForeColor=   -1  'True
            Groups(0).HasHeadBackColor=   -1  'True
            Groups(0).HeadForeColor=   16777215
            Groups(0).HeadBackColor=   8421504
            Groups(0).Columns.Count=   9
            Groups(0).Columns(0).Width=   1005
            Groups(0).Columns(0).Caption=   "A�o"
            Groups(0).Columns(0).Name=   "ANYO"
            Groups(0).Columns(0).DataField=   "Column 0"
            Groups(0).Columns(0).DataType=   8
            Groups(0).Columns(0).FieldLen=   256
            Groups(0).Columns(0).Locked=   -1  'True
            Groups(0).Columns(0).HasBackColor=   -1  'True
            Groups(0).Columns(0).BackColor=   16777184
            Groups(0).Columns(1).Width=   1085
            Groups(0).Columns(1).Caption=   "Comm"
            Groups(0).Columns(1).Name=   "COMM"
            Groups(0).Columns(1).DataField=   "Column 1"
            Groups(0).Columns(1).DataType=   8
            Groups(0).Columns(1).FieldLen=   256
            Groups(0).Columns(1).Locked=   -1  'True
            Groups(0).Columns(1).HasBackColor=   -1  'True
            Groups(0).Columns(1).BackColor=   16777184
            Groups(0).Columns(2).Width=   1191
            Groups(0).Columns(2).Caption=   "Cod"
            Groups(0).Columns(2).Name=   "PROCE"
            Groups(0).Columns(2).DataField=   "Column 2"
            Groups(0).Columns(2).DataType=   8
            Groups(0).Columns(2).FieldLen=   256
            Groups(0).Columns(2).Locked=   -1  'True
            Groups(0).Columns(2).HasBackColor=   -1  'True
            Groups(0).Columns(2).BackColor=   16777184
            Groups(0).Columns(3).Width=   4022
            Groups(0).Columns(3).Caption=   "Descripci�n"
            Groups(0).Columns(3).Name=   "DESCR"
            Groups(0).Columns(3).DataField=   "Column 3"
            Groups(0).Columns(3).DataType=   8
            Groups(0).Columns(3).FieldLen=   256
            Groups(0).Columns(3).Locked=   -1  'True
            Groups(0).Columns(3).HasBackColor=   -1  'True
            Groups(0).Columns(3).BackColor=   16777184
            Groups(0).Columns(4).Width=   900
            Groups(0).Columns(4).Caption=   "Dec"
            Groups(0).Columns(4).Name=   "DEC"
            Groups(0).Columns(4).DataField=   "Column 4"
            Groups(0).Columns(4).DataType=   8
            Groups(0).Columns(4).FieldLen=   256
            Groups(0).Columns(4).Style=   2
            Groups(0).Columns(5).Width=   661
            Groups(0).Columns(5).Visible=   0   'False
            Groups(0).Columns(5).Caption=   "Hora"
            Groups(0).Columns(5).Name=   "HORA"
            Groups(0).Columns(5).DataField=   "Column 5"
            Groups(0).Columns(5).DataType=   8
            Groups(0).Columns(5).FieldLen=   256
            Groups(0).Columns(6).Width=   1191
            Groups(0).Columns(6).Caption=   "Hora"
            Groups(0).Columns(6).Name=   "HORACORTA"
            Groups(0).Columns(6).DataField=   "Column 6"
            Groups(0).Columns(6).DataType=   8
            Groups(0).Columns(6).FieldLen=   256
            Groups(0).Columns(7).Width=   1111
            Groups(0).Columns(7).Caption=   "Pers"
            Groups(0).Columns(7).Name=   "PERS"
            Groups(0).Columns(7).DataField=   "Column 7"
            Groups(0).Columns(7).DataType=   8
            Groups(0).Columns(7).FieldLen=   256
            Groups(0).Columns(7).Locked=   -1  'True
            Groups(0).Columns(7).Style=   4
            Groups(0).Columns(7).ButtonsAlways=   -1  'True
            Groups(0).Columns(8).Width=   5054
            Groups(0).Columns(8).Visible=   0   'False
            Groups(0).Columns(8).Caption=   "EST"
            Groups(0).Columns(8).Name=   "EST"
            Groups(0).Columns(8).DataField=   "Column 8"
            Groups(0).Columns(8).DataType=   8
            Groups(0).Columns(8).FieldLen=   256
            Groups(1).Width =   10319
            Groups(1).Caption=   "Responsable"
            Groups(1).HasHeadForeColor=   -1  'True
            Groups(1).HasHeadBackColor=   -1  'True
            Groups(1).HeadForeColor=   16777215
            Groups(1).HeadBackColor=   8421504
            Groups(1).Columns.Count=   4
            Groups(1).Columns(0).Width=   2223
            Groups(1).Columns(0).Caption=   "Apellidos"
            Groups(1).Columns(0).Name=   "APE"
            Groups(1).Columns(0).DataField=   "Column 9"
            Groups(1).Columns(0).DataType=   8
            Groups(1).Columns(0).FieldLen=   256
            Groups(1).Columns(0).Locked=   -1  'True
            Groups(1).Columns(0).HasBackColor=   -1  'True
            Groups(1).Columns(0).BackColor=   9562111
            Groups(1).Columns(1).Width=   1799
            Groups(1).Columns(1).Caption=   "Nombre"
            Groups(1).Columns(1).Name=   "NOM"
            Groups(1).Columns(1).DataField=   "Column 10"
            Groups(1).Columns(1).DataType=   8
            Groups(1).Columns(1).FieldLen=   256
            Groups(1).Columns(1).Locked=   -1  'True
            Groups(1).Columns(1).HasBackColor=   -1  'True
            Groups(1).Columns(1).BackColor=   9562111
            Groups(1).Columns(2).Width=   1879
            Groups(1).Columns(2).Caption=   "Tfno"
            Groups(1).Columns(2).Name=   "TFNO"
            Groups(1).Columns(2).DataField=   "Column 11"
            Groups(1).Columns(2).DataType=   8
            Groups(1).Columns(2).FieldLen=   256
            Groups(1).Columns(2).Locked=   -1  'True
            Groups(1).Columns(2).HasBackColor=   -1  'True
            Groups(1).Columns(2).BackColor=   9562111
            Groups(1).Columns(3).Width=   4419
            Groups(1).Columns(3).Caption=   "Mail"
            Groups(1).Columns(3).Name=   "EMAIL"
            Groups(1).Columns(3).DataField=   "Column 12"
            Groups(1).Columns(3).DataType=   8
            Groups(1).Columns(3).FieldLen=   256
            Groups(1).Columns(3).Locked=   -1  'True
            Groups(1).Columns(3).HasBackColor=   -1  'True
            Groups(1).Columns(3).BackColor=   9562111
            _ExtentX        =   17621
            _ExtentY        =   7223
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   -74925
         ScaleHeight     =   435
         ScaleWidth      =   9915
         TabIndex        =   10
         Top             =   5580
         Width           =   9915
         Begin VB.CommandButton cmdCrearReu 
            Caption         =   "&Crear reuni�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1185
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   60
            Width           =   1380
         End
         Begin VB.CommandButton cmdModificar 
            Caption         =   "&Modificar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   60
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2715
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   3870
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   -74940
         ScaleHeight     =   435
         ScaleWidth      =   9675
         TabIndex        =   30
         Top             =   5580
         Visible         =   0   'False
         Width           =   9675
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4410
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Cancelar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5610
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.PictureBox picGestNavigate 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   120
         ScaleHeight     =   435
         ScaleWidth      =   10875
         TabIndex        =   24
         Top             =   5580
         Width           =   10875
         Begin VB.CommandButton cmdAgenda 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   2
            Left            =   8685
            Picture         =   "frmREU.frx":1811
            Style           =   1  'Graphical
            TabIndex        =   57
            TabStop         =   0   'False
            Top             =   60
            Width           =   250
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   9010
            TabIndex        =   55
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdOrdenar 
            Caption         =   "&Ordenar"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5205
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   60
            Width           =   885
         End
         Begin VB.CommandButton cmdModifReu 
            Caption         =   "&Modificar reuni�n"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   45
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   60
            Width           =   1365
         End
         Begin VB.CommandButton cmdFecha 
            Caption         =   "&Cambiar fecha"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3225
            TabIndex        =   33
            TabStop         =   0   'False
            Top             =   60
            Width           =   1890
         End
         Begin VB.CommandButton cmdAgenda 
            Caption         =   "&Terminkalender"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   7470
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   60
            Width           =   1215
         End
         Begin VB.CommandButton cmdEliReu 
            Caption         =   "&Versammlung l�schen"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1500
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   60
            Width           =   1650
         End
         Begin VB.CommandButton cmdRestaurarGest 
            Caption         =   "&Aktualisier."
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   10105
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   60
            Width           =   975
         End
         Begin VB.CommandButton cmdConv 
            Caption         =   "Ausschreibu&ng"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   6165
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   60
            Width           =   1215
         End
      End
      Begin VB.PictureBox picGestEdit 
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   60
         ScaleHeight     =   435
         ScaleWidth      =   10935
         TabIndex        =   46
         Top             =   5580
         Visible         =   0   'False
         Width           =   10935
         Begin VB.CommandButton cmdCancelarGest 
            Caption         =   "&Cancelar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5430
            TabIndex        =   48
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptarGest 
            Caption         =   "&Aceptar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4230
            TabIndex        =   47
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.Frame fraFechas 
         Caption         =   "Incluir procesos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   675
         Left            =   -74865
         TabIndex        =   15
         Top             =   360
         Width           =   10785
         Begin VB.CommandButton cmdCargar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   8040
            Picture         =   "frmREU.frx":185F
            Style           =   1  'Graphical
            TabIndex        =   50
            ToolTipText     =   "Cargar"
            Top             =   240
            Width           =   315
         End
         Begin VB.CheckBox chkSinPla 
            Caption         =   "Sin planificar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   16
            Top             =   285
            Value           =   1  'Checked
            Width           =   1800
         End
         Begin VB.PictureBox picPla 
            BorderStyle     =   0  'None
            Height          =   435
            Left            =   1380
            ScaleHeight     =   435
            ScaleWidth      =   6375
            TabIndex        =   17
            Top             =   180
            Width           =   6375
            Begin VB.CheckBox chkPla 
               Caption         =   "Planificados"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   660
               TabIndex        =   49
               Top             =   120
               Value           =   1  'Checked
               Width           =   1215
            End
            Begin VB.TextBox txtFecHasta 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4710
               TabIndex        =   22
               Top             =   60
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5850
               Picture         =   "frmREU.frx":18EA
               Style           =   1  'Graphical
               TabIndex        =   21
               TabStop         =   0   'False
               ToolTipText     =   "Mantenimiento"
               Top             =   60
               Width           =   315
            End
            Begin VB.TextBox txtFecDesde 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2490
               TabIndex        =   19
               Top             =   60
               Width           =   1110
            End
            Begin VB.CommandButton cmdCalDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   3630
               Picture         =   "frmREU.frx":1E74
               Style           =   1  'Graphical
               TabIndex        =   18
               TabStop         =   0   'False
               Top             =   60
               Width           =   315
            End
            Begin VB.Label Label3 
               BackStyle       =   0  'Transparent
               Caption         =   "Hasta:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   4200
               TabIndex        =   23
               Top             =   120
               Width           =   495
            End
            Begin VB.Label Label8 
               BackStyle       =   0  'Transparent
               Caption         =   "Desde:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   1920
               TabIndex        =   20
               Top             =   120
               Width           =   555
            End
         End
      End
      Begin VB.Label lblHint 
         Alignment       =   2  'Center
         Caption         =   "Puede crear una reuni�n seleccionando los procesos a incluir y haciendo clic en 'Crear reuni�n'."
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   -74880
         TabIndex        =   51
         Top             =   1080
         Width           =   10755
      End
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   30
      Left            =   0
      TabIndex        =   56
      Top             =   0
      Width           =   30
      ExtentX         =   53
      ExtentY         =   53
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmREU"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de seguridad
Private bModifPla As Boolean
Private bModifGest As Boolean
Private bCrearReu As Boolean
Private bConvocatoria As Boolean
Public g_bRMat As Boolean
Private m_bRAsig As Boolean
Private m_bRCompResp As Boolean
Private m_bREqpAsig As Boolean
Private m_bRUsuAper As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private m_bRPerfUON As Boolean
Private bNoModificarHoraReu As Boolean

Private m_bRestProvMatComp As Boolean

Private bModError As Boolean

'Variables para idioma
Private sIdioma() As String
'Variables para control de combo
Private bCargarComboDesde As Boolean
Private bRespetarCombo As Boolean

' Objeto para manejo de reuniones
Public oGestorReuniones As CGestorReuniones

Public Accion As AccionesSummit
' Arrays para contener los datos de fechas a actualizar
Private Fechas As TipoDatosPlaReu

Public oProcesoSeleccionado As CProceso

'Colecci�n de reuniones
Private oReuniones As CReuniones
Public oReuSeleccionada As CReunion

'variable para ayudarnos en la ordenaci�n a la hora de modificar
Private Hora As String
Private bOrdenando As Boolean
Public mdtHoraReunion As Variant

'variables para ayudarnos en el ajuste de las horas de los procesos en una reunion
Private m_bAjustarHoras As Boolean
Private m_bAjustandoHoras As Boolean
Private m_sReunionConPreadjudicaciones As String
Private m_sConfiramarEliminacion As String


Private Sub ConfigurarSeguridad()
    
    If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
    
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestMat)) Is Nothing) Then
            g_bRMat = True
        End If
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestAsignado)) Is Nothing) Then
            m_bRAsig = True
        End If
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestResponsable)) Is Nothing) Then
            m_bRCompResp = True
        End If
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestEqpAsignado)) Is Nothing) Then
            m_bREqpAsig = True
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestUsuAper)) Is Nothing) Then
            m_bRUsuAper = True
        End If
        
        m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestUsuUON)) Is Nothing)
        m_bRUsuDep = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestUsuDep)) Is Nothing)
        m_bRPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestPerfUON)) Is Nothing)
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREUModFechas)) Is Nothing) Then
            bModifPla = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREUModReu)) Is Nothing) Then
            bModifGest = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREUConvAge)) Is Nothing) Then
            bConvocatoria = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREUEstReu)) Is Nothing) Then
            bCrearReu = True
        End If
        
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREURestProvMatComp)) Is Nothing Then
            m_bRestProvMatComp = True
        Else
            m_bRestProvMatComp = False
        End If
        
    Else
        bModifPla = True
        bModifGest = True
        bCrearReu = True
        bConvocatoria = True
        g_bRMat = False
        m_bRAsig = False
        m_bRCompResp = False
        m_bREqpAsig = False
        m_bRUsuAper = False
        m_bRUsuUON = False
        m_bRUsuDep = False
    End If

    If Not bModifPla Then
        cmdModificar.Visible = False
        cmdCrearReu.Left = cmdModificar.Left
        cmdRestaurar.Left = cmdCrearReu.Left + cmdCrearReu.Width + 135
        cmdListado(0).Left = cmdRestaurar.Left + cmdRestaurar.Width + 135
    End If
    
    If Not bCrearReu Then
        cmdCrearReu.Visible = False
        cmdRestaurar.Left = cmdCrearReu.Left
        cmdListado(0).Left = cmdRestaurar.Left + cmdRestaurar.Width + 135
    End If
    
    If Not bModifGest Then
        sdbgReuGest.Left = 120
        cmdHora.Visible = False
        cmdSubir.Visible = False
        cmdBajar.Visible = False
        cmdEliminarProceso.Visible = False
        cmdA�adirProceso.Visible = False
        cmdModifReu.Visible = False
        cmdFecha.Visible = False
        cmdOrdenar.Visible = False
        cmdEliReu.Visible = False
        
        cmdConv.Left = cmdA�adirProceso.Left
        cmdAgenda(0).Left = cmdConv.Left + cmdConv.Width + 75
        cmdAgenda(2).Left = cmdAgenda(0).Left + cmdAgenda(0).Width
        cmdListado(1).Left = cmdAgenda(0).Left + cmdAgenda(0).Width + cmdAgenda(2).Width + 75
        cmdRestaurarGest.Left = cmdListado(1).Left + cmdListado(1).Width + 75
        
    End If
    
    If Not bConvocatoria Then
        cmdConv.Visible = False
        cmdAgenda(0).Left = cmdConv.Left
        cmdAgenda(2).Left = cmdAgenda(0).Left + cmdAgenda(0).Width
        cmdListado(1).Left = cmdAgenda(0).Left + cmdAgenda(0).Width + cmdAgenda(2).Width + 75
        cmdRestaurarGest.Left = cmdListado(1).Left + cmdListado(1).Width + 75
    End If
    
    If bCrearReu Then
        lblHint.Visible = True
    Else
        lblHint.Visible = False
    End If
    
End Sub

Private Sub RestaurarDatos()
    Dim ADORs As Ador.Recordset
    Dim sestado As String
    Dim lIdPerfil As Long
    
    txtFecDesde = DateAdd("m", -1, Date)
    txtFecHasta = DateAdd("m", 6, Date)
        
    sdbgReuPla.RemoveAll
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    Set ADORs = oGestorReuniones.DevolverProcesos(True, True, TipoOrdenacionProcesos.OrdPorCod, CDate(txtFecDesde), CDate(txtFecHasta), g_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gvarCodUsuario, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
    
    While Not ADORs.EOF
        sestado = DevolverEstado(ADORs(4).Value)
                
        '                  ANYO                      GMN1                      COD                       DEN                       EST                FECAPE                    FECLIMOFE                 FECULTREU                 FECPRES                   FECNEC                    REUDEC
        sdbgReuPla.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & ADORs(3).Value & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & ADORs(5).Value & Chr(m_lSeparador) & ADORs(7).Value & Chr(m_lSeparador) & ADORs(8).Value & Chr(m_lSeparador) & ADORs(9).Value & Chr(m_lSeparador) & ADORs(6).Value & Chr(m_lSeparador) & Int(ADORs(10).Value) & Chr(m_lSeparador) & ADORs(11).Value & Chr(m_lSeparador) & ADORs(12).Value & Chr(m_lSeparador) & ADORs(13).Value & Chr(m_lSeparador) & ADORs(14).Value
        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing
         
    Screen.MousePointer = vbNormal
End Sub

Private Sub chkPla_Click()
If chkPla.Value = vbUnchecked Then
    Me.txtFecDesde.Enabled = False
    Me.txtFecHasta.Enabled = False
    Me.cmdCalDesde.Enabled = False
    Me.cmdCalHasta.Enabled = False
Else
    Me.txtFecDesde.Enabled = True
    Me.txtFecHasta.Enabled = True
    Me.cmdCalDesde.Enabled = True
    Me.cmdCalHasta.Enabled = True
End If

End Sub

Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim oProces As CProcesos
Dim iIndice As Integer

    If Accion = ACCPlaReuCrear Then
                
        If Not IsDate(txtFecha) Then
            oMensajes.NoValido sIdioma(1)
            If Me.Visible Then txtFecha.SetFocus
            Exit Sub
        End If
        
        If Not IsDate(txtHora) Then
            oMensajes.NoValido sIdioma(1)
            If Me.Visible Then txtHora.SetFocus
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        Set oProces = oFSGSRaiz.generar_CProcesos
        Screen.MousePointer = vbNormal
                
        iIndice = 0
        
        While iIndice < sdbgReuPla.SelBookmarks.Count
            
            sdbgReuPla.Bookmark = sdbgReuPla.SelBookmarks(iIndice)
            oProces.Add sdbgReuPla.Columns(0).Value, sdbgReuPla.Columns(2).Value, sdbgReuPla.Columns(1).Value, sdbgReuPla.Columns(3).Value, , , , , , , , , , , , , , , , , , , , CBool(val(sdbgReuPla.Columns("DEC").Value))
            iIndice = iIndice + 1
        
        Wend
        
        Screen.MousePointer = vbHourglass
        teserror = oGestorReuniones.CrearReunion(txtFecha, txtHora, txtRef, oProces)
        Screen.MousePointer = vbNormal
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
            
        Set oProces = Nothing
        
        picNavigate.Visible = True
        picEdit.Visible = False
    
        picReuPla.Height = picReuPla.Height - 700
        picReuPla.Top = picReuPla.Top + 875
        sdbgReuPla.Height = sdbgReuPla.Height - 200
        sdbgReuPla.Top = sdbgReuPla.Top - 500
    
        Accion = ACCPlaReuCon
        
        RegistrarAccion ACCPlaReuCrear, " Nueva reunion: " & txtFecha & " " & txtHora & " " & txtRef
    
    Else

        sdbgReuPla.Update
        DoEvents
        If bModError Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oGestorReuniones.ModificarFechaProxReunionProcesos(Fechas)
        Screen.MousePointer = vbNormal

        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        End If
        
        picNavigate.Visible = True
        picEdit.Visible = False
        
        sdbgReuPla.Columns(8).Locked = True
        sdbgReuPla.Columns(10).Locked = True
        sdbgReuPla.AllowUpdate = False
        sdbgReuPla.col = 1
        
        Accion = ACCPlaReuCon
    
    End If
    
    
End Sub

Private Sub cmdAceptarGest_Click()
Dim teserror As TipoErrorSummit
Dim dFecha As Date
Dim dFechaCorta As Date
Dim vbm As Variant

    sdbgReuGest.Update
    DoEvents
    
    If bModError Then
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    oReuSeleccionada.FinalizarBloqueoEnBD
    
    vbm = sdbgReuGest.AddItemBookmark(0)
    dFechaCorta = Format(sdbcFecReu.Columns(0).Value, "short date") & " " & Format(sdbgReuGest.Columns("HORACORTA").CellValue(vbm), "short time")
    dFecha = Format(sdbcFecReu.Columns(0).Value, "short date") & " " & sdbgReuGest.Columns("HORA").CellValue(vbm)
   
    If sdbcFecReu.Text <> dFechaCorta Then
        If bNoModificarHoraReu Then
            oMensajes.MensajeOKOnly 626
        End If
    End If
    
    If bNoModificarHoraReu Then
        teserror = oReuSeleccionada.ModificarProcesosEnBD(False)
    Else
        teserror = oReuSeleccionada.ModificarProcesosEnBD
    End If
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    Screen.MousePointer = vbNormal
    
    ' Registrar accion
    
    Accion = ACCGestReuCon
    
    RegistrarAccion ACCPlaReuMod, "Reunion: " & CStr(oReuSeleccionada.Fecha) & " Proceso: " & sdbgReuGest.Columns(0).Value & "/" & sdbgReuGest.Columns(1).Value & "/" & sdbgReuGest.Columns(2).Value
    
    cmdSubir.Enabled = True
    cmdBajar.Enabled = True
    
    picGestEdit.Visible = False
    picGestNavigate.Visible = True
    Me.cmdA�adirProceso.Enabled = True
    Me.cmdEliminarProceso.Enabled = True
    Me.cmdHora.Enabled = True
    fraGest.Enabled = True
    sdbgReuGest.AllowUpdate = False
    
    If sdbcFecReu.Text = "" Then Exit Sub
    bRespetarCombo = True
    If CDate(sdbcFecReu.Text) <> dFechaCorta Then
        If bNoModificarHoraReu = False Then
            sdbcFecReu.Value = oReuSeleccionada.Fecha
            sdbcFecReu.Text = Format(oReuSeleccionada.Fecha, "Short date") & " " & Format(oReuSeleccionada.Fecha, "Short time")
            sdbcFecReu.Columns(0).Value = oReuSeleccionada.Fecha
            bRespetarCombo = False
            sdbcFecReu_Validate (False)
        End If
    End If
    bRespetarCombo = False
    cmdRestaurarGest_Click
        
End Sub

Public Sub CargarProcesoConBusqueda()
    Dim oProce As CProceso
    Dim sHora As String
    Dim dHora As Date
    Dim sCod As String
    Dim teserror As TipoErrorSummit
    Dim oIasig As IAsignaciones
    Dim inSec As Integer
    Dim bSinProcesos As Boolean

    sdbgReuGest.MoveLast
    If IsNull(mdtHoraReunion) Or IsEmpty(mdtHoraReunion) Then
        inSec = Second(sdbcFecReu.Columns("FECHA").Value)
        sHora = Format(sdbcFecReu.Columns("FECHA").Value, "short date") & " " & TimeValue(sdbgReuGest.Columns("HORA").Value)
        dHora = DateAdd("n", gParametrosGenerales.giTIEMPOREU, CDate(sHora))
        bSinProcesos = False
    Else
        sHora = Format(sdbcFecReu.Columns("FECHA").Value, "short date") & " " & TimeValue(mdtHoraReunion)
        dHora = CDate(sHora)
        bSinProcesos = True
    End If
        
    If sHora = "" And sdbgReuGest.Rows > 0 Then
        Exit Sub
    Else
        If sHora = "" Then
            dHora = CDate(sdbcFecReu.Columns(0).Value)
        End If
    End If
    
    For Each oProce In frmPROCEBuscar.oProceEncontrados
    
        sCod = oProce.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oProce.GMN1Cod))
        sCod = CStr(oProce.Anyo) & sCod & CStr(oProce.Cod)
    
        If oReuSeleccionada.Procesos.Item(sCod) Is Nothing Then
                        
            teserror = oReuSeleccionada.IncluirProceso(oProce.Anyo, oProce.GMN1Cod, oProce.Cod, dHora)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                'A traves de la funcion oReuSeleccionada.IncluirProceso
                'pasamos el dato "Dec" al proceso a traves de la
                'variable teserror.Arg1
                oProce.Reudec = teserror.Arg1
                'Busca el responsable de cada proceso
                Set oIasig = oProce
                oIasig.DevolverResponsable
                
                oReuSeleccionada.Procesos.Add oProce.Anyo, oProce.Cod, oProce.GMN1Cod, oProce.Den, , , , , , oProce.Estado, , , , , , , , , , , , dHora, oProce.responsable, oProce.Reudec
                
                If oProce.responsable Is Nothing Then
                    sdbgReuGest.AddItem oProce.Anyo & Chr(m_lSeparador) & oProce.GMN1Cod & Chr(m_lSeparador) & oProce.Cod & Chr(m_lSeparador) & oProce.Den & Chr(m_lSeparador) & Int(oProce.Reudec) & Chr(m_lSeparador) & TimeValue(dHora) & Chr(m_lSeparador) & Format(TimeValue(dHora), "short time")
                Else
                    sdbgReuGest.AddItem oProce.Anyo & Chr(m_lSeparador) & oProce.GMN1Cod & Chr(m_lSeparador) & oProce.Cod & Chr(m_lSeparador) & oProce.Den & Chr(m_lSeparador) & Int(oProce.Reudec) & Chr(m_lSeparador) & TimeValue(dHora) & Chr(m_lSeparador) & Format(TimeValue(dHora), "short time") & Chr(m_lSeparador) & Chr(m_lSeparador) & oProce.Estado & Chr(m_lSeparador) & oProce.responsable.Apel & Chr(m_lSeparador) & oProce.responsable.nombre & Chr(m_lSeparador) & oProce.responsable.Tfno & Chr(m_lSeparador) & oProce.responsable.mail
                End If
                
                dHora = DateAdd("n", gParametrosGenerales.giTIEMPOREU, dHora)
                
                Set oIasig = Nothing
                RegistrarAccion ACCPlaReuMod, "Reunion: " & oReuSeleccionada.Fecha & " Nuevo proceso:" & oProce.Anyo & "/" & oProce.GMN1Cod & "/" & oProce.Cod
            End If
        End If
    
    Next
           
    Set oProce = Nothing
    Set frmPROCEBuscar.oProceEncontrados = Nothing
End Sub

''' <summary>
''' A�adir Procesos a Reuni�n
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdA�adirProceso_Click()
    
    If oReuSeleccionada.Procesos.Count = 0 Then
        mdtHoraReunion = TimeValue(sdbcFecReu.Columns("FECHA").Value)
    Else
        mdtHoraReunion = Null
    End If
    
    frmPROCEBuscar.sOrigen = "frmREU"
    
    frmPROCEBuscar.bRDest = False
    frmPROCEBuscar.m_bProveAsigComp = False
    frmPROCEBuscar.m_bProveAsigEqp = False
    frmPROCEBuscar.bRMat = g_bRMat
    frmPROCEBuscar.bRAsig = m_bRAsig
    frmPROCEBuscar.bRCompResponsable = m_bRCompResp
    frmPROCEBuscar.bREqpAsig = m_bREqpAsig
    frmPROCEBuscar.bRUsuAper = m_bRUsuAper
    frmPROCEBuscar.bRUsuDep = m_bRUsuDep
    frmPROCEBuscar.bRUsuUON = m_bRUsuUON
    frmPROCEBuscar.bRPerfUON = m_bRPerfUON
    frmPROCEBuscar.bRestProvMatComp = m_bRestProvMatComp
    frmPROCEBuscar.bRestProvEquComp = False
    frmPROCEBuscar.sdbcAnyo = Year(CDate(sdbcFecReu))
    frmPROCEBuscar.Show 1
    
End Sub

Private Sub cmdBajar_Click()
Dim varActual As Variant
Dim varSiguiente As Variant
Dim sCod As String
Dim Hora1 As Variant
Dim Hora2 As Variant
Dim ayContenidoFila1() As Variant
Dim ayContenidoFila2() As Variant
Dim i As Integer
    
    
    If sdbgReuGest.SelBookmarks.Count = 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    bOrdenando = True
    
    varActual = sdbgReuGest.SelBookmarks(0)
    'Comprobamos que tenga una fila posterior
    varSiguiente = sdbgReuGest.AddItemBookmark(sdbgReuGest.Row + 1)
    
    If IsNull(varSiguiente) Or IsEmpty(varSiguiente) Then
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        sdbgReuGest.MoveNext
    End If
        
    'Cambiamos las filas entre s�, menos la hora
    ReDim ayContenidoFila1(sdbgReuGest.Columns.Count)
    ReDim ayContenidoFila2(sdbgReuGest.Columns.Count)
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        ayContenidoFila1(i) = sdbgReuGest.Columns(i).Value
    Next
    
    Hora1 = Format(sdbcFecReu.Columns(0).Value, "short date") & " " & sdbgReuGest.Columns("HORA").Value
    
    sdbgReuGest.MovePrevious
    
    Hora2 = Format(sdbcFecReu.Columns(0).Value, "short date") & " " & sdbgReuGest.Columns("HORA").Value
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        ayContenidoFila2(i) = sdbgReuGest.Columns(i).Value
    Next
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        sdbgReuGest.Columns(i).Value = ayContenidoFila1(i)
    Next
    
    sdbgReuGest.Columns("HORA").Value = TimeValue(Hora2)
    sdbgReuGest.Columns("HORACORTA").Value = Format(TimeValue(Hora2), "short time")
    
    sdbgReuGest.MoveNext
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        sdbgReuGest.Columns(i).Value = ayContenidoFila2(i)
    Next
    
    sdbgReuGest.Columns("HORA").Value = TimeValue(Hora1)
    sdbgReuGest.Columns("HORACORTA").Value = Format(TimeValue(Hora1), "short time")
    
    'Cambiamos la hora del proceso
    sCod = sdbgReuGest.Columns(1).Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgReuGest.Columns(1).Text))
    sCod = sdbgReuGest.Columns("ANYO").Text & sCod & sdbgReuGest.Columns("PROCE").Text
    oReuSeleccionada.Procesos.Item(sCod).HoraEnReunion = Hora1
    sdbgReuGest.MovePrevious
    sCod = sdbgReuGest.Columns(1).Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgReuGest.Columns(1).Text))
    sCod = sdbgReuGest.Columns("ANYO").Text & sCod & sdbgReuGest.Columns("PROCE").Text
    oReuSeleccionada.Procesos.Item(sCod).HoraEnReunion = Hora2
    sdbgReuGest.MoveNext
    sdbgReuGest.SelBookmarks.RemoveAll
    sdbgReuGest.SelBookmarks.Add sdbgReuGest.Bookmark
    
    Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    oReuSeleccionada.FinalizarBloqueoEnBD
    
    teserror = oReuSeleccionada.ModificarProcesosEnBD(False)
    
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    bRespetarCombo = True
    
    sdbcFecReu.Columns(0).Text = oReuSeleccionada.Fecha
    sdbcFecReu = Format(oReuSeleccionada.Fecha, "short date") & " " & Format(oReuSeleccionada.Fecha, "short time")
    bRespetarCombo = False
    
    sdbcFecReu_Validate False
    
    ' Registrar accion
    
    Accion = ACCGestReuCon
    
    RegistrarAccion ACCPlaReuMod, "Reunion: " & CStr(oReuSeleccionada.Fecha) & " Hora modificada para el proceso: " & sdbgReuGest.Columns(0).Value & "/" & sdbgReuGest.Columns(1).Value & "/" & sdbgReuGest.Columns(2).Value
      
    
    Screen.MousePointer = vbNormal
    bOrdenando = False
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalDesde_Click()
    AbrirFormCalendar Me, txtFecDesde, "PlanReu"
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalendar_Click()
    AbrirFormCalendar Me, sdbcFecReu
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalHasta_Click()
    AbrirFormCalendar Me, txtFecHasta, "PlanReu"
End Sub

Private Sub cmdCancelar_Click()
    
    If Accion = ACCPlaReuMod Then
        picNavigate.Visible = True
        picEdit.Visible = False
    
        RestaurarDatos
        sdbgReuPla.Columns(8).Locked = True
        sdbgReuPla.Columns(10).Locked = True
        sdbgReuPla.AllowUpdate = False
        sdbgReuPla.col = 1
    
    Else
        picNavigate.Visible = True
        picEdit.Visible = False
        
        picReuPla.Height = picReuPla.Height - 700
        picReuPla.Top = picReuPla.Top + 875
        sdbgReuPla.Height = sdbgReuPla.Height - 200
        sdbgReuPla.Top = sdbgReuPla.Top - 500
    
    End If
    
        Accion = ACCPlaReuCon
    
    
End Sub

Private Sub cmdCancelarGest_Click()
            
    Screen.MousePointer = vbHourglass
    oReuSeleccionada.FinalizarBloqueoEnBD
    Screen.MousePointer = vbNormal
    
    Accion = ACCGestReuCon
    
    picGestEdit.Visible = False
    picGestNavigate.Visible = True
    fraGest.Enabled = True
    
    sdbgReuGest.CancelUpdate
    sdbgReuGest.Columns("HORACORTA").Value = Format(sdbgReuGest.Columns("HORA").Value, "short time")
    sdbgReuGest.Update
    bModError = False
    sdbgReuGest.AllowUpdate = False
    cmdSubir.Enabled = True
    cmdBajar.Enabled = True
    cmdRestaurarGest_Click
    
     
End Sub

Private Sub cmdCargar_Click()
    Dim ADORs As Ador.Recordset
    Dim sestado As String
    Dim lIdPerfil As Long
    
    Screen.MousePointer = vbHourglass
    sdbgReuPla.RemoveAll
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If chkPla.Value = vbChecked Then
        If Not IsDate(txtFecDesde) Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdioma(2)
            If Me.Visible Then txtFecDesde.SetFocus
            Exit Sub
        End If
        
        If Not IsDate(txtFecHasta) Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdioma(3)
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
            
        Set ADORs = oGestorReuniones.DevolverProcesos((chkSinPla.Value = vbChecked), True, TipoOrdenacionDatosPlanifReu.OrdPorCod, CDate(txtFecDesde), CDate(txtFecHasta), g_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gvarCodUsuario, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
    Else
        Set ADORs = oGestorReuniones.DevolverProcesos((chkSinPla.Value = vbChecked), False, TipoOrdenacionDatosPlanifReu.OrdPorCod, , , g_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gvarCodUsuario, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
    End If

    
    While Not ADORs.EOF
        sestado = DevolverEstado(ADORs(4).Value)
        
        '                  ANYO                      GMN1                      COD                       DEN                       EST                FECAPE                    FECLIMOFE                 FECULTREU                 FECPRES                   FECNEC                    REUDEC
        sdbgReuPla.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & ADORs(3).Value & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & ADORs(5).Value & Chr(m_lSeparador) & ADORs(7).Value & Chr(m_lSeparador) & ADORs(8).Value & Chr(m_lSeparador) & ADORs(9).Value & Chr(m_lSeparador) & ADORs(6).Value & Chr(m_lSeparador) & Int(ADORs(10).Value) & Chr(m_lSeparador) & ADORs(11).Value & Chr(m_lSeparador) & ADORs(12).Value & Chr(m_lSeparador) & ADORs(13).Value & Chr(m_lSeparador) & ADORs(14).Value

        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing
    sdbgReuPla.MoveFirst
    sdbgReuPla.Scroll -100, -100
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdConv_Click()
Dim ADORs As Ador.Recordset
Dim i As Integer
Dim vbm As Variant
Dim sMailHtml As String
Dim sMailText As String

    If oReuSeleccionada Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    'Se cargan por si han cambiado el orden para que est�n ordernados en la colecci�n
    oReuSeleccionada.CargarProcesos TipoOrdenacionDatosPlanifReu.OrdPorHoraProceso
    
    
    If gParametrosInstalacion.gsConvocatoria = "" Then
        gParametrosInstalacion.gsConvocatoria = gParametrosGenerales.gsCONVDOT
        g_GuardarParametrosIns = True
    End If
    
    If gParametrosInstalacion.gsConvocatoriaMailHtmlSPA = "" Then
        gParametrosInstalacion.gsConvocatoriaMailHtmlSPA = gParametrosGenerales.gsCONVMAILHTMLSPA
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailTextSPA = "" Then
        gParametrosInstalacion.gsConvocatoriaMailTextSPA = gParametrosGenerales.gsCONVMAILTEXTSPA
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailSubjectSPA = "" Then
        gParametrosInstalacion.gsConvocatoriaMailSubjectSPA = gParametrosGenerales.gsCONVMAILSUBJECTSPA
        g_GuardarParametrosIns = True
    End If
    
    If gParametrosInstalacion.gsConvocatoriaMailHtmlENG = "" Then
        gParametrosInstalacion.gsConvocatoriaMailHtmlENG = gParametrosGenerales.gsCONVMAILHTMLENG
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailTextENG = "" Then
        gParametrosInstalacion.gsConvocatoriaMailTextENG = gParametrosGenerales.gsCONVMAILTEXTENG
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailSubjectENG = "" Then
        gParametrosInstalacion.gsConvocatoriaMailSubjectENG = gParametrosGenerales.gsCONVMAILSUBJECTENG
        g_GuardarParametrosIns = True
    End If
    
    If gParametrosInstalacion.gsConvocatoriaMailHtmlGER = "" Then
        gParametrosInstalacion.gsConvocatoriaMailHtmlGER = gParametrosGenerales.gsCONVMAILHTMLGER
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailTextGER = "" Then
        gParametrosInstalacion.gsConvocatoriaMailTextGER = gParametrosGenerales.gsCONVMAILTEXTGER
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailSubjectGER = "" Then
        gParametrosInstalacion.gsConvocatoriaMailSubjectGER = gParametrosGenerales.gsCONVMAILSUBJECTGER
        g_GuardarParametrosIns = True
    End If
    
    If gParametrosInstalacion.gsConvocatoriaMailHtmlFRA = "" Then
        gParametrosInstalacion.gsConvocatoriaMailHtmlFRA = gParametrosGenerales.gsCONVMAILHTMLFRA
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailTextFRA = "" Then
        gParametrosInstalacion.gsConvocatoriaMailTextFRA = gParametrosGenerales.gsCONVMAILTEXTFRA
        g_GuardarParametrosIns = True
    End If
    If gParametrosInstalacion.gsConvocatoriaMailSubjectFRA = "" Then
        gParametrosInstalacion.gsConvocatoriaMailSubjectFRA = gParametrosGenerales.gsCONVMAILSUBJECTFRA
        g_GuardarParametrosIns = True
    End If
    
    frmREUConvocatoria.iNumTotal = 0
    
    'Cargar los compradores
    'Dependiendo de un parametro, seleccionaremos por defecto todos los compradores, o solo los responsables
    Set ADORs = oGestorReuniones.DevolverCompradoresReunion(CDate(sdbcFecReu.Columns(0).Value), TipoOrdenacionCompradores.OrdPorCod)
    
    If Not ADORs Is Nothing Then
        While Not ADORs.EOF
            frmREUConvocatoria.iNumTotal = frmREUConvocatoria.iNumTotal + 1
            frmREUConvocatoria.sdbgComp.AddItem Abs(ADORs("BAJALOG").Value - 1) & Chr(m_lSeparador) & ADORs("EQP").Value & Chr(m_lSeparador) & ADORs("COD").Value & Chr(m_lSeparador) & ADORs("APE").Value & Chr(m_lSeparador) & ADORs("NOM").Value & Chr(m_lSeparador) & ADORs("TFNO").Value & Chr(m_lSeparador) & ADORs("FAX").Value & Chr(m_lSeparador) & ADORs("EMAIL").Value & Chr(m_lSeparador) & ADORs("BAJALOG").Value & Chr(m_lSeparador) & ADORs("IDIOMA").Value & Chr(m_lSeparador) & ADORs("TIPOEMAIL").Value
            ADORs.MoveNext
        Wend
    End If
    
    'Primero los responsables pq si haces cargar las personas implicadas primero y una de ellas es persona no implicada
    'luego por muy responsable que sea no sale implicada.
    Set ADORs = oGestorReuniones.DevolverPersonasImplicadasReunion(CDate(sdbcFecReu.Columns(0).Value), TipoOrdenacionPersonas.OrdPorCod)
    
    If Not ADORs Is Nothing Then
        While Not ADORs.EOF
            If ADORs("CONV").Value <> 0 Then
                For i = 0 To frmREUConvocatoria.sdbgComp.Rows - 1
                    vbm = frmREUConvocatoria.sdbgComp.AddItemBookmark(i)
                    If frmREUConvocatoria.sdbgComp.Columns("COD").CellValue(vbm) = ADORs.Fields("COD") Then
                        Exit For
                    End If
                Next i
                
                If i = frmREUConvocatoria.sdbgComp.Rows Then
                    frmREUConvocatoria.iNumTotal = frmREUConvocatoria.iNumTotal + 1
                    frmREUConvocatoria.sdbgPer.AddItem (ADORs("CONV").Value And Abs(ADORs("BAJALOG").Value - 1)) & Chr(m_lSeparador) & ADORs("COD").Value & Chr(m_lSeparador) & ADORs("APE").Value & Chr(m_lSeparador) & ADORs("NOM").Value & Chr(m_lSeparador) & ADORs("CAR").Value & Chr(m_lSeparador) & ADORs("TFNO").Value & Chr(m_lSeparador) & ADORs("FAX").Value & Chr(m_lSeparador) & ADORs("EMAIL").Value & Chr(m_lSeparador) & ADORs("DEP").Value & Chr(m_lSeparador) & ADORs("UON1").Value & Chr(m_lSeparador) & ADORs("UON2").Value & Chr(m_lSeparador) & ADORs("UON3").Value & Chr(m_lSeparador) & ADORs("BAJALOG").Value & Chr(m_lSeparador) & ADORs("IDIOMA").Value & Chr(m_lSeparador) & ADORs("TIPOEMAIL").Value & Chr(m_lSeparador) & ADORs("DEP_DEN").Value
                End If
            End If
            ADORs.MoveNext
        Wend
    End If
    
    If Not ADORs Is Nothing Then
        ADORs.Close
        Set ADORs = Nothing
    End If
    
    Select Case UCase(basPublic.gParametrosInstalacion.gIdioma)
    Case "SPA"
        sMailHtml = gParametrosInstalacion.gsConvocatoriaMailHtmlSPA
        sMailText = gParametrosInstalacion.gsConvocatoriaMailTextSPA
'        sMailSubject = gParametrosInstalacion.gsConvocatoriaMailSubjectSPA
    Case "ENG"
        sMailHtml = gParametrosInstalacion.gsConvocatoriaMailHtmlENG
        sMailText = gParametrosInstalacion.gsConvocatoriaMailTextENG
'        sMailSubject = gParametrosInstalacion.gsConvocatoriaMailSubjectENG
    Case "GER"
        sMailHtml = gParametrosInstalacion.gsConvocatoriaMailHtmlGER
        sMailText = gParametrosInstalacion.gsConvocatoriaMailTextGER
'        sMailSubject = gParametrosInstalacion.gsConvocatoriaMailSubjectGER
    Case "FRA"
        sMailHtml = gParametrosInstalacion.gsConvocatoriaMailHtmlFRA
        sMailText = gParametrosInstalacion.gsConvocatoriaMailTextFRA
'        sMailSubject = gParametrosInstalacion.gsConvocatoriaMailSubjectFRA
    End Select
    Screen.MousePointer = vbNormal
    
    frmREUConvocatoria.txtMailHtml = sMailHtml
    frmREUConvocatoria.txtMailText = sMailText

'    frmREUConvocatoria.txtMailSubject = sMailSubject
    
    frmREUConvocatoria.txtDOT = gParametrosInstalacion.gsConvocatoria
    
    frmREUConvocatoria.Show
    frmREUConvocatoria.SetFocus
    
    
End Sub

Private Sub cmdCrearReu_Click()
            
    Screen.MousePointer = vbHourglass
    Accion = ACCPlaReuCrear
    picReuPla.Height = picReuPla.Height + 700
    picReuPla.Top = picReuPla.Top - 870
    sdbgReuPla.Height = sdbgReuPla.Height + 200
    sdbgReuPla.Top = sdbgReuPla.Top + 500
    picNavigate.Visible = False
    picEdit.Visible = True
            
    'Cargar par�metros por defecto
    txtFecha = oGestorReuniones.DevolverPosibleFechaReunion
    txtHora = Format(gParametrosGenerales.gdCOMIENZOREU, "Short Time")
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdEliminarProceso_Click()
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim sCod As String
Dim oProceso As CProceso

    If sdbgReuGest.Rows = 0 Then Exit Sub
    
    sdbgReuGest.SelBookmarks.Add sdbgReuGest.AddItemBookmark(sdbgReuGest.Row)
    sCod = sdbgReuGest.Columns(1).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgReuGest.Columns(1).Value))
    sCod = CStr(sdbgReuGest.Columns(0).Value) & sCod & CStr(sdbgReuGest.Columns(2).Value)
   
    irespuesta = oMensajes.PreguntaQuitarProceso(sdbgReuGest.Columns(0).Value & "/" & sdbgReuGest.Columns(1).Value & "/" & sdbgReuGest.Columns(2).Value)
    If irespuesta = vbYes Then
        Screen.MousePointer = vbHourglass
        Set oProceso = proceso
        irespuesta = vbYes
        If oProceso.res And oReuSeleccionada.TienePreadjudicaciones Then
            irespuesta = oMensajes.MensajeYesNo(m_sReunionConPreadjudicaciones & vbCrLf & m_sConfiramarEliminacion)
        End If
        If irespuesta = vbYes Then
            teserror = oReuSeleccionada.ExcluirProceso(val(sdbgReuGest.Columns(0).Value), sdbgReuGest.Columns(1).Value, val(sdbgReuGest.Columns(2).Value))
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                sdbgReuGest.SelBookmarks.RemoveAll
                Exit Sub
            Else
                RegistrarAccion ACCGestReuQuitarProc, "Reunion: " & CStr(oReuSeleccionada.Fecha) & " Proceso: " & sdbgReuGest.Columns(0).Value & "/" & sdbgReuGest.Columns(1).Value & "/" & sdbgReuGest.Columns(2).Value
                sdbgReuGest.DeleteSelected
                sdbgReuGest.MovePrevious
                oReuSeleccionada.Procesos.Remove sCod
            End If
        End If
        Screen.MousePointer = vbNormal
    Else
        sdbgReuGest.SelBookmarks.RemoveAll
        
    End If
    
End Sub

Private Sub cmdEliReu_Click()
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit

    irespuesta = oMensajes.PreguntaEliminar(sIdioma(15))
    If irespuesta = vbYes Then
        Screen.MousePointer = vbHourglass
        teserror = oGestorReuniones.EliminarReunion(sdbcFecReu.Columns("FECHA").Value)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If
        
        RegistrarAccion ACCGestReuEliReu, "Reunion:" & sdbcFecReu.Columns("FECHA").Value
        
        lblref = ""
        sdbcFecReu = ""
        sdbcFecReu.RemoveAll
        Screen.MousePointer = vbNormal
    End If
    
End Sub

Private Sub cmdFecha_Click()
        
    frmREUCambiarFecha.Show 1
    
End Sub


Private Sub cmdHora_Click()
    Dim inSec As Integer
            
    bRespetarCombo = True
    mdtHoraReunion = Null
    
    mdtHoraReunion = MostrarFormHora(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, Me.sdbcFecReu.Columns(0).Value)
    
    If IsNull(mdtHoraReunion) Then
        Exit Sub
    Else
        If mdtHoraReunion = CDate(Me.sdbcFecReu.Value) Then
            Exit Sub
        End If
            
        inSec = Second(sdbcFecReu.Columns("FECHA").Value)
        mdtHoraReunion = CDate(Format(sdbcFecReu.Columns("FECHA").Value, "short date") & " " & Format(mdtHoraReunion, "short time"))
    End If
    
    Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    teserror = frmREU.oReuSeleccionada.CambiarFecha(mdtHoraReunion)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    RegistrarAccion ACCPlaReuMod, "Reunion:" & oReuSeleccionada.Fecha & " Hora:" & mdtHoraReunion
    
    sdbcFecReu.Value = mdtHoraReunion
    sdbcFecReu.Text = Format(oReuSeleccionada.Fecha, "short date") & " " & Format(oReuSeleccionada.Fecha, "HH:nn")
    sdbcFecReu.Columns(0).Value = oReuSeleccionada.Fecha
    bRespetarCombo = False
    bCargarComboDesde = False

    sdbcFecReu_Validate False
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdlistado_Click(Index As Integer)
If Index = 0 Then
    
    frmLstREU.sOrigen = "frmREU"
    frmLstREU.txtFecDesde.Text = txtFecDesde.Text
    frmLstREU.txtFecHasta.Text = txtFecHasta.Text
    frmLstREU.chkSinPlan.Value = chkSinPla.Value
    frmLstREU.chkConPlan.Value = chkPla.Value
    frmLstREU.Show vbModal
Else
    frmLstREU.sOrigen = "frmREU"
    frmLstREU.txtFecDesde.Text = Format(sdbcFecReu.Columns("FECHACORTA").Value, "short date")
    frmLstREU.txtHoraDesde.Text = Format(sdbcFecReu.Columns("FECHACORTA").Value, "short time")
    
    
    frmLstREU.txtFecHasta.Text = Format(sdbcFecReu.Columns("FECHACORTA").Value, "short date")
    frmLstREU.txtHoraHasta.Text = Format(sdbcFecReu.Columns("FECHACORTA").Value, "short time")
    
    frmLstREU.opLisReu.Value = True
    frmLstREU.chkPer.Value = vbChecked
    frmLstREU.chkResp.Value = vbChecked
    
    frmLstREU.Show vbModal
End If
End Sub

Private Sub cmdModificar_Click()
        
    ReDim Fechas.Anyo(0)
    ReDim Fechas.GMN1(0)
    ReDim Fechas.Cod(0)
    ReDim Fechas.Dec(0)
    ReDim Fechas.Fecha(0)
    
    
    picNavigate.Visible = False
    picEdit.Visible = True
    
    sdbgReuPla.Columns(8).Locked = False
    sdbgReuPla.Columns(10).Locked = False
    sdbgReuPla.AllowUpdate = True
    sdbgReuPla.col = 8
    If Me.Visible Then sdbgReuPla.SetFocus
    Accion = ACCPlaReuMod
    
End Sub

Private Sub cmdModifReu_Click()
    Dim teserror As TipoErrorSummit
    
    If oReuSeleccionada Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    teserror = oReuSeleccionada.IniciarBloqueoEnBD
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    'Solo para ajustar las horas de los procesos en la reunion
    m_bAjustandoHoras = False
    m_bAjustarHoras = False
    
    sdbgReuGest.AllowUpdate = True
    sdbgReuGest.Columns(4).Locked = False
    
    Accion = ACCGestReuMod
   
    cmdSubir.Enabled = False
    cmdBajar.Enabled = False
    
    picGestEdit.Visible = True
    picGestNavigate.Visible = False
    
    fraGest.Enabled = False
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdOrdenar_Click()
    Dim teserror As TipoErrorSummit
    
    If oReuSeleccionada Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    teserror = oReuSeleccionada.IniciarBloqueoEnBD
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    Accion = ACCGestReuMod
    
    Screen.MousePointer = vbNormal
    
    'frmREUOrd.Show 1
    Dim ValoresOrdenacion As Variant
    ValoresOrdenacion = MostrarFormREUOrd(oGestorIdiomas, gParametrosInstalacion.gIdioma, oGestorParametros.DevolverIdiomas(False, False, True), gParametrosGenerales)
    
    If ValoresOrdenacion(0) = True Then
        teserror = frmREU.oReuSeleccionada.Ordenar(ValoresOrdenacion(1), ValoresOrdenacion(2)) '(frmREUOrd.Criterio1, frmREUOrd.Criterio2)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If
        
        RegistrarAccion ACCPlaReuMod, "Reunion:" & frmREU.oReuSeleccionada.Fecha & " Orden1:" & ValoresOrdenacion(3) & " Orden2:" & ValoresOrdenacion(4) '" Orden1:" & sdbcCrit1 & " Orden2:" & sdbcCrit2
    End If
    
    frmREU.oReuSeleccionada.FinalizarBloqueoEnBD
    frmREU.Accion = ACCGestReuCon
    
    If ValoresOrdenacion(0) = True Then frmREU.cmdRestaurarGest_Click
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdRestaurar_Click()
    
    cmdCargar_Click
    
End Sub


Public Sub cmdRestaurarGest_Click()
    Screen.MousePointer = vbHourglass
    ReunionSeleccionada
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdSubir_Click()
Dim varActual As Variant
Dim varAnterior As Variant
Dim sCod As String
Dim Hora1 As Variant
Dim Hora2 As Variant
Dim ayContenidoFila1() As Variant
Dim ayContenidoFila2() As Variant
Dim i As Integer

    If sdbgReuGest.SelBookmarks.Count = 0 Then Exit Sub
    
    bOrdenando = True
    Screen.MousePointer = vbHourglass
    
    varActual = sdbgReuGest.SelBookmarks(0)
    'Comprobamos que tenga una fila anterior
    varAnterior = sdbgReuGest.AddItemBookmark(sdbgReuGest.Row - 1)
    
    If IsNull(varAnterior) Or IsEmpty(varAnterior) Then
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        sdbgReuGest.MovePrevious
    End If
        
    'Cambiamos las filas entre s�, menos la hora
    ReDim ayContenidoFila1(sdbgReuGest.Columns.Count)
    ReDim ayContenidoFila2(sdbgReuGest.Columns.Count)
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        ayContenidoFila1(i) = sdbgReuGest.Columns(i).Value
    Next
    
    Hora1 = Format(sdbcFecReu.Columns(0).Value, "short date") & " " & sdbgReuGest.Columns("HORA").Value
    
    sdbgReuGest.MoveNext
    
    Hora2 = Format(sdbcFecReu.Columns(0).Value, "short date") & " " & sdbgReuGest.Columns("HORA").Value
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        ayContenidoFila2(i) = sdbgReuGest.Columns(i).Value
    Next
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        sdbgReuGest.Columns(i).Value = ayContenidoFila1(i)
    Next
    
    sdbgReuGest.Columns("HORA").Value = TimeValue(Hora2)
    sdbgReuGest.Columns("HORACORTA").Value = Format(TimeValue(Hora2), "short time")
    
    sdbgReuGest.MovePrevious
    
    For i = 0 To sdbgReuGest.Columns.Count - 1
        sdbgReuGest.Columns(i).Value = ayContenidoFila2(i)
    Next
    
    sdbgReuGest.Columns("HORA").Value = TimeValue(Hora1)
    sdbgReuGest.Columns("HORACORTA").Value = Format(TimeValue(Hora1), "short time")
    
    'Cambiamos la hora del proceso
    sCod = sdbgReuGest.Columns(1).Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgReuGest.Columns(1).Text))
    sCod = sdbgReuGest.Columns("ANYO").Text & sCod & sdbgReuGest.Columns("PROCE").Text
    oReuSeleccionada.Procesos.Item(sCod).HoraEnReunion = Hora1
    sdbgReuGest.MoveNext
    sCod = sdbgReuGest.Columns(1).Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgReuGest.Columns(1).Text))
    sCod = sdbgReuGest.Columns("ANYO").Text & sCod & sdbgReuGest.Columns("PROCE").Text
    oReuSeleccionada.Procesos.Item(sCod).HoraEnReunion = Hora2
    sdbgReuGest.MovePrevious
    sdbgReuGest.SelBookmarks.RemoveAll
    sdbgReuGest.SelBookmarks.Add sdbgReuGest.Bookmark
    
    Dim teserror As TipoErrorSummit

    Screen.MousePointer = vbHourglass
    oReuSeleccionada.FinalizarBloqueoEnBD
    
    teserror = oReuSeleccionada.ModificarProcesosEnBD(False)
    
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    bRespetarCombo = True
    sdbcFecReu.Columns(0).Text = oReuSeleccionada.Fecha
    sdbcFecReu = Format(oReuSeleccionada.Fecha, "short date") & " " & Format(oReuSeleccionada.Fecha, "short time")
    bRespetarCombo = False
    
    sdbcFecReu_Validate False
    
    ' Registrar accion
    
    Accion = ACCGestReuCon
    
    RegistrarAccion ACCPlaReuMod, "Reunion: " & CStr(oReuSeleccionada.Fecha) & " Hora modificada para el proceso: " & sdbgReuGest.Columns(0).Value & "/" & sdbgReuGest.Columns(1).Value & "/" & sdbgReuGest.Columns(2).Value
    
    Screen.MousePointer = vbNormal
    bOrdenando = False
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub Command3_Click()
    AbrirFormCalendar Me, txtFecha, "PlanReu"
End Sub

Private Sub Form_Activate()
    
    Unload frmREUConvocatoria
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_REU, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 22)
        For i = 1 To 22
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        Label1.caption = sIdioma(1) & ":"
        Label4.caption = sIdioma(1) & ":"
        sdbcFecReu.Columns(1).caption = sIdioma(1)
        sdbgReuGest.Groups(0).caption = sIdioma(14)
        sdbgReuGest.Groups(0).Columns(6).caption = sIdioma(16)
        Label5.caption = sIdioma(16) & ":"
        chkPla.caption = Ador(0).Value '22
        Ador.MoveNext
        chkSinPla.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        cmdAceptarGest.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        cmdCancelarGest.caption = Ador(0).Value '25
        Ador.MoveNext
        cmdAgenda(0).caption = Ador(0).Value
        Ador.MoveNext
        cmdA�adirProceso.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdConv.caption = Ador(0).Value
        Ador.MoveNext
        cmdCrearReu.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminarProceso.ToolTipText = Ador(0).Value  '30
        Ador.MoveNext
        cmdEliReu.caption = Ador(0).Value
        Ador.MoveNext
        cmdFecha.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado(0).caption = Ador(0).Value
        cmdListado(1).caption = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdModifReu.caption = Ador(0).Value '35
        Ador.MoveNext
        cmdOrdenar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        cmdRestaurarGest.caption = Ador(0).Value
        Ador.MoveNext
        fraFechas.caption = Ador(0).Value
        Ador.MoveNext
        frmREU.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value & ":" '40
        Label6.caption = Ador(0).Value & ":"
        sdbcFecReu.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Ador(0).Value
        Ador.MoveNext
        Label8.caption = Ador(0).Value
        Ador.MoveNext
        lblHint.caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(1).caption = Ador(0).Value
        sdbgReuPla.Groups(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(0).Columns(0).caption = Ador(0).Value '45
        sdbgReuPla.Groups(0).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(0).Columns(2).caption = Ador(0).Value
        sdbgReuPla.Groups(0).Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(0).Columns(3).caption = Ador(0).Value
        sdbgReuPla.Groups(0).Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(0).Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(0).Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(1).Columns(0).caption = Ador(0).Value '50
        sdbgReuPla.Groups(2).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(1).Columns(1).caption = Ador(0).Value
        sdbgReuPla.Groups(2).Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(1).Columns(2).caption = Ador(0).Value
        sdbgReuPla.Groups(2).Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuGest.Groups(1).Columns(3).caption = Ador(0).Value
        sdbgReuPla.Groups(2).Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuPla.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuPla.Groups(1).caption = Ador(0).Value '55
        Ador.MoveNext
        sdbgReuPla.Groups(0).Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuPla.Groups(1).Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuPla.Groups(1).Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuPla.Groups(1).Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuPla.Groups(1).Columns(2).caption = Ador(0).Value '60
        Ador.MoveNext
        sdbgReuPla.Groups(1).Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgReuPla.Groups(1).Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sstabReu.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabReu.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        m_sReunionConPreadjudicaciones = Ador(0).Value
        Ador.MoveNext
        m_sConfiramarEliminacion = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
    Dim ADORs As Ador.Recordset
    Dim sestado As String
    Dim lIdPerfil As Long
    
    Me.Height = 6570
    Me.Width = 11100
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    Screen.MousePointer = vbHourglass
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Accion = ACCPlaReuCon
    
    
    ConfigurarSeguridad
    Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
        
    txtFecDesde = DateAdd("m", -1, Date)
    txtFecHasta = DateAdd("m", 6, Date)
    sdbgReuPla.Groups(0).Columns(1).caption = gParametrosGenerales.gsabr_GMN1
    sdbgReuGest.Groups(0).Columns(1).caption = gParametrosGenerales.gsabr_GMN1
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    Set ADORs = oGestorReuniones.DevolverProcesos(True, True, TipoOrdenacionProcesos.OrdPorCod, CDate(txtFecDesde), CDate(txtFecHasta), g_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gvarCodUsuario, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
    
    While Not ADORs.EOF
        sestado = DevolverEstado(ADORs(4).Value)
                
        '                  ANYO                      GMN1                      COD                       DEN                       EST                FECAPE                    FECLIMOFE                 FECULTREU                 FECPRES                   FECNEC                    REUDEC
        sdbgReuPla.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & ADORs(3).Value & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & ADORs(5).Value & Chr(m_lSeparador) & ADORs(7).Value & Chr(m_lSeparador) & ADORs(8).Value & Chr(m_lSeparador) & ADORs(9).Value & Chr(m_lSeparador) & ADORs(6).Value & Chr(m_lSeparador) & Int(ADORs(10).Value) & Chr(m_lSeparador) & ADORs(11).Value & Chr(m_lSeparador) & ADORs(12).Value & Chr(m_lSeparador) & ADORs(13).Value & Chr(m_lSeparador) & ADORs(14).Value
        
        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing
    
    If basPublic.gParametrosInstalacion.giLockTimeOut <= 0 Then
        basPublic.gParametrosInstalacion.giLockTimeOut = basParametros.gParametrosGenerales.giLockTimeOut
    End If
    
    'oGestorReuniones.EstablecerTiempoDeBloqueo basPublic.gParametrosInstalacion.giLockTimeOut
    Set oReuniones = oFSGSRaiz.Generar_CReuniones
    
    oReuniones.EstablecerTiempoDeBloqueo basPublic.gParametrosInstalacion.giLockTimeOut
     
   Screen.MousePointer = vbNormal
   

End Sub

Private Sub Form_Resize()
    
    On Error Resume Next
    
    If Me.Height < 2500 Then Exit Sub
    If Me.Width < 1500 Then Exit Sub
    
    sstabReu.Height = Me.Height - 500
    sstabReu.Width = Me.Width - 45
    picReuPla.Height = sstabReu.Height - 1800
    picReuPla.Width = sstabReu.Width - 300
    picNavigate.Top = sstabReu.Height - 490
    picGestNavigate.Width = Me.Width
    picEdit.Top = picNavigate.Top
    picGestEdit.Top = picNavigate.Top
    picGestNavigate.Top = sstabReu.Height - 490
    picCrearReu.Width = picReuPla.Width
    picCrearReu.Height = picReuPla.Height
    picGest.Height = picReuPla.Height
    picGest.Width = picReuPla.Width
    sdbgReuPla.Height = picReuPla.Height
    sdbgReuPla.Width = picReuPla.Width
    sdbgReuGest.Height = picGest.Height - 200
    If bModifGest Then
        sdbgReuGest.Width = picGest.Width - 800
    Else
        sdbgReuGest.Width = picGest.Width - 300
    End If
    fraFechas.Width = picReuPla.Width
    fraGest.Width = fraFechas.Width
    lblHint.Width = sdbgReuPla.Width
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim teserror As TipoErrorSummit

    If Accion = ACCGestReuMod Then
        If oReuSeleccionada Is Nothing Then Exit Sub
        teserror = oReuSeleccionada.FinalizarBloqueoEnBD
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
    End If
    
    Accion = ACCGestReuCon
    Set oGestorReuniones = Nothing
    Set oProcesoSeleccionado = Nothing
    Set oReuSeleccionada = Nothing
    Set oReuniones = Nothing
    
    Me.Visible = False
    
End Sub




Private Sub sdbcFecReu_Change()
 
    If Not bRespetarCombo Then
        Set oReuSeleccionada = Nothing
        
        bCargarComboDesde = True
        sdbgReuGest.RemoveAll
        sdbcFecReu.Columns(0).Value = ""
        cmdAgenda(0).Enabled = False
        cmdAgenda(2).Enabled = False
        cmdConv.Enabled = False
        cmdEliReu.Enabled = False
        cmdRestaurarGest.Enabled = False
        cmdFecha.Enabled = False
        cmdModifReu.Enabled = False
        cmdA�adirProceso.Enabled = False
        cmdEliminarProceso.Enabled = False
        cmdOrdenar.Enabled = False
        cmdSubir.Enabled = False
        cmdBajar.Enabled = False
        cmdHora.Enabled = False
        Me.lblref.caption = ""
    End If
    
End Sub

Private Sub sdbcFecReu_CloseUp()
    
    If sdbcFecReu.Value = "" Then
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    bRespetarCombo = True
    sdbcFecReu.Text = sdbcFecReu.Columns(1).Text
    lblref = sdbcFecReu.Columns(2).Text
    bRespetarCombo = False
    
    ReunionSeleccionada
    bCargarComboDesde = False
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFecReu_DropDown()
    Dim oReu As CReunion

    Screen.MousePointer = vbHourglass
    sdbcFecReu.RemoveAll
        
    If bCargarComboDesde Then
        If IsDate(sdbcFecReu) Then
            oReuniones.CargarReuniones sdbcFecReu
        Else
            sdbcFecReu = ""
            oReuniones.CargarReuniones
        End If
        bCargarComboDesde = False
    Else
        oReuniones.CargarReuniones
        
    End If
    For Each oReu In oReuniones
        
        sdbcFecReu.AddItem Format(oReu.Fecha, "General Date") & Chr(m_lSeparador) & Format(oReu.Fecha, "Short Date") & " " & Format(oReu.Fecha, "short time") & Chr(m_lSeparador) & oReu.Referencia
        
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFecReu_InitColumnProps()
    
    sdbcFecReu.DataFieldList = "Column 0"
    sdbcFecReu.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcFecReu_PositionList(ByVal Text As String)
 ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    If Not oReuSeleccionada Is Nothing Then
        Text = oReuSeleccionada.Fecha
    End If
    sdbcFecReu.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcFecReu.Rows - 1
            bm = sdbcFecReu.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcFecReu.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcFecReu.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub

Public Sub sdbcFecReu_Validate(Cancel As Boolean)

    If Trim(sdbcFecReu.Text = "") Then Exit Sub
    
    If Not IsDate(sdbcFecReu) Then
        oMensajes.NoValido sIdioma(1)
        Exit Sub
    End If
    
    If sdbcFecReu.Text = sdbcFecReu.Columns(1).Text Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    If sdbcFecReu.Columns(0).Text = "" Then
        sdbcFecReu.Columns(0).Text = sdbcFecReu.Text
    End If
    
    
    oReuniones.CargarReuniones sdbcFecReu.Columns(0).Text, DateAdd("d", 1, sdbcFecReu.Columns(0).Text), , , True
    
    If oReuniones.Count = 0 Then
        sdbcFecReu.Text = ""
        oMensajes.NoValido sIdioma(15)
    Else
        bRespetarCombo = True
        sdbcFecReu.Columns(0).Value = oReuniones.Item(1).Fecha
        sdbcFecReu.Columns(1).Text = Format(oReuniones.Item(1).Fecha, "short date") & " " & Format(oReuniones.Item(1).Fecha, "short time")
        sdbcFecReu.Text = sdbcFecReu.Columns(1).Text
        lblref = oReuniones.Item(1).Referencia
        bRespetarCombo = False
        ReunionSeleccionada
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgReuGest_AfterUpdate(RtnDispErrMsg As Integer)
Dim sCod As String
Dim bDec As Boolean
Dim lMinutos As Long
Dim dHora1 As Date
Dim bkMarkInicial As Variant

Screen.MousePointer = vbHourglass
    If Not bOrdenando Then
        sCod = sdbgReuGest.Columns("COMM").Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgReuGest.Columns("COMM").Text))
        sCod = sdbgReuGest.Columns("ANYO").Text & sCod & sdbgReuGest.Columns("PROCE").Text
    
        If Not oReuSeleccionada.Procesos.Item(sCod) Is Nothing Then
            'Si no estamos ajustando horas y hay que ajustar horas podemos entrar
            If Not m_bAjustandoHoras And m_bAjustarHoras Then
                'Ajustando la hora de los procesos de la reunion
                m_bAjustandoHoras = True
                'Calculamos la diferencia entre la hora anterior y la actual
                lMinutos = DateDiff("n", TimeValue(oReuSeleccionada.Procesos.Item(sCod).HoraEnReunion), sdbgReuGest.Columns("HORACORTA").Value)
                'Ajustamos las horas de los procesos posteriores de la reunion
                bkMarkInicial = sdbgReuGest.Bookmark
                While Not sdbgReuGest.AddItemRowIndex(sdbgReuGest.Bookmark) = sdbgReuGest.Rows - 1
                    sdbgReuGest.MoveNext
                    dHora1 = DateAdd("n", lMinutos, sdbgReuGest.Columns("HORACORTA").Value)
                    sdbgReuGest.Columns("HORACORTA").Value = Format(dHora1, "short time")
                Wend
                sdbgReuGest.Bookmark = bkMarkInicial
                'Terminamos de ajustar y reseteamos m_bAjustandoHoras y m_bAjustarHoras
                m_bAjustandoHoras = False
                m_bAjustarHoras = False
            End If
            If sdbgReuGest.Columns("DEC").Value = "" Then
                bDec = False
            Else
                bDec = CBool(sdbgReuGest.Columns("DEC").Value)
                'oReuSeleccionada.Procesos.Item(scod).Reudec = CBool(Int(sdbgReuGest.Columns("DEC").Value))
            End If
            oReuSeleccionada.Procesos.Item(sCod).Reudec = bDec
            oReuSeleccionada.Procesos.Item(sCod).HoraEnReunion = CDate(Format(sdbcFecReu.Columns(1).Text, "short date") & " " & sdbgReuGest.Columns("HORA").Value)
        End If
    End If
Screen.MousePointer = vbNormal
End Sub


Private Sub sdbgReuGest_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    
    DispPromptMsg = 0
    
End Sub

Private Sub sdbgReuGest_BeforeUpdate(Cancel As Integer)
Dim inDec As Integer
Dim bkMark As Variant
Dim irespuesta As Integer

    bModError = False
    
    If Not IsDate(sdbgReuGest.Columns("HORACORTA").Value) Then
        oMensajes.NoValido sIdioma(16)
        bModError = True
        If Me.Visible Then sdbgReuGest.SetFocus
        Cancel = True
        Exit Sub
    End If
    
    If Not bOrdenando And sdbgReuGest.Columns("HORACORTA").ColChanged And Not m_bAjustandoHoras Then
        m_bAjustarHoras = False
        If Not CStr(sdbgReuGest.Bookmark) = CStr(sdbgReuGest.AddItemBookmark(0)) Then
            'En caso de que no estemos en el primer proceso de la reuni�n
            'comprobamos que la hora del proceso seleccionado
            'es mayor que la del proceso anterior
            bkMark = sdbgReuGest.GetBookmark(-1)
            If sdbgReuGest.Columns("HORACORTA").Value < sdbgReuGest.Columns("HORACORTA").CellValue(bkMark) Then
                oMensajes.HoraProcesoReunionNoValida 850
                bModError = True
                If Me.Visible Then sdbgReuGest.SetFocus
                Cancel = True
                Exit Sub
            End If
        End If
        'preguntamos al usuario si desea ajustar las horas
        If Not sdbgReuGest.AddItemRowIndex(sdbgReuGest.Bookmark) = sdbgReuGest.Rows - 1 Then
            irespuesta = oMensajes.PreguntaAjustarHoraProcesosReunion
            If irespuesta = vbNo Then
                m_bAjustarHoras = False
                'Me da el BookMark (indice) del proceso posterior
                bkMark = sdbgReuGest.GetBookmark(1)
                If sdbgReuGest.Columns("HORACORTA").Value > sdbgReuGest.Columns("HORACORTA").CellValue(bkMark) Then
                    'no se puede adelantar ese proceso por que el que le sigue tiene una hora menor
                    oMensajes.HoraProcesoReunionNoValida 854
                    If Me.Visible Then sdbgReuGest.SetFocus
                    Cancel = True
                    Exit Sub
                End If
            Else 'respuesta = vbYes
                m_bAjustarHoras = True
            End If
        End If
    End If
    Hora = sdbgReuGest.Columns("HORACORTA").Text
    inDec = Second(sdbcFecReu.Columns("FECHA").Value)
    sdbgReuGest.Columns("HORA").Text = DateAdd("s", inDec, sdbgReuGest.Columns("HORACORTA").Text)
End Sub

Private Sub sdbgReuGest_BtnClick()
Dim sCod As String


    Set oProcesoSeleccionado = Nothing
    sCod = sdbgReuGest.Columns(1).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbgReuGest.Columns(1).Value))
    sCod = sdbgReuGest.Columns(0).Text & sCod & sdbgReuGest.Columns(2).Text
    
    Set oProcesoSeleccionado = oReuSeleccionada.Procesos.Item(sCod)
    
    
    If oProcesoSeleccionado Is Nothing Then Exit Sub
        
    frmREUPer.CargarGridConPersonas
    
    frmREUPer.Show 1
    
End Sub

Private Sub sdbgReuGest_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    If sdbgReuGest.Columns("EST").Value = TipoEstadoProceso.Cerrado Then
        For i = 0 To sdbgReuGest.Cols - 1
            sdbgReuGest.Columns(i).CellStyleSet "Anulado"
        Next
    End If
End Sub

Private Sub sdbgReuPla_AfterUpdate(RtnDispErrMsg As Integer)
Dim iPosicion As Integer
    
Screen.MousePointer = vbHourglass
    iPosicion = BuscarEnArrayProce(Fechas, sdbgReuPla.Columns(0).Value, sdbgReuPla.Columns(1).Value, sdbgReuPla.Columns(2).Value) = -1
    
    If iPosicion = -1 Then
        'No lo hab�amos modificado todav�a
        ReDim Preserve Fechas.Fecha(UBound(Fechas.Fecha) + 1)
        ReDim Preserve Fechas.Anyo(UBound(Fechas.Anyo) + 1)
        ReDim Preserve Fechas.Cod(UBound(Fechas.Cod) + 1)
        ReDim Preserve Fechas.Dec(UBound(Fechas.Dec) + 1)
        ReDim Preserve Fechas.GMN1(UBound(Fechas.GMN1) + 1)
        iPosicion = UBound(Fechas.Fecha)
    End If
    
    
    Fechas.Fecha(iPosicion) = sdbgReuPla.Columns(8).Value
    
    Fechas.Anyo(iPosicion) = sdbgReuPla.Columns(0).Value
    Fechas.GMN1(iPosicion) = sdbgReuPla.Columns(1).Value
    Fechas.Cod(iPosicion) = sdbgReuPla.Columns(2).Value
    
    If sdbgReuPla.Columns(10).Value = "1" Or sdbgReuPla.Columns(10).Value = "-1" Then
        Fechas.Dec(iPosicion) = True
    Else
        Fechas.Dec(iPosicion) = False
    End If
Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgReuPla_BeforeUpdate(Cancel As Integer)
'@@@todos cambiados
bModError = False
    If sdbgReuPla.Columns(8).Value <> "" Then
        If Not IsDate(sdbgReuPla.Columns(8).Value) Then
            oMensajes.NoValido sIdioma(1)
            If Me.Visible Then sdbgReuPla.SetFocus
            bModError = True
            Cancel = True
            Exit Sub
        Else
            If sdbgReuPla.Columns(6).Value <> "" Then
                If CDate(sdbgReuPla.Columns(6).Value) > CDate(sdbgReuPla.Columns(8).Value) Then
                    oMensajes.FechaMenorQueLimOfer
                    If Me.Visible Then sdbgReuPla.SetFocus
                    bModError = True
                    Cancel = True
                    Exit Sub
                End If
            End If
            If sdbgReuPla.Columns(9).Value <> "" Then
                If CDate(sdbgReuPla.Columns(9).Value) < CDate(sdbgReuPla.Columns(8).Value) Then
                    oMensajes.FechaMayorQueNecesidad
                    If Me.Visible Then sdbgReuPla.SetFocus
                    bModError = True
                    Cancel = True
                    Exit Sub
                End If
            End If
            
            If sdbgReuPla.Columns(5).Value <> "" Then
                If CDate(sdbgReuPla.Columns(5).Value) > CDate(sdbgReuPla.Columns(8).Value) Then
                    oMensajes.FechaMenorQueApertura
                    If Me.Visible Then sdbgReuPla.SetFocus
                    bModError = True
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
    End If
    
End Sub

Private Sub sdbgReuPla_HeadClick(ByVal ColIndex As Integer)
    Dim ADORs As Ador.Recordset
    Dim sestado As String
    Dim udtOrdenar As TipoOrdenacionDatosPlanifReu
    Dim lIdPerfil As Long
        
    If Accion = ACCPlaReuCon Or Accion = ACCGestReuCon Then
        Select Case ColIndex
            Case 0
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorAnyo
            Case 1  'Por gmn1
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorMat
            Case 2  'Por c�digo
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorCod
            Case 3
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorDescrProceso
            Case 4
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorEstadoProceso
            Case 5
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorfechaapertura
            Case 6
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorFechaValidezOfertas
            Case 7
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorFechaUltimaReunion
            Case 8
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorFechaProximaReunion
            Case 9
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorfechanecesidad
            Case 10
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorTipoReu
            Case 11
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorapeResponsable
            Case 12
                udtOrdenar = TipoOrdenacionDatosPlanifReu.OrdPorNombreResponsable
        End Select
            
        sdbgReuPla.RemoveAll
        
        If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
        If chkPla.Value = vbChecked Then
            If Not IsDate(txtFecDesde) Then
                oMensajes.NoValido sIdioma(2)
                If Me.Visible Then txtFecDesde.SetFocus
                Exit Sub
            End If
            
            If Not IsDate(txtFecHasta) Then
                oMensajes.NoValido sIdioma(3)
                If Me.Visible Then txtFecHasta.SetFocus
                Exit Sub
            End If
        
            Screen.MousePointer = vbHourglass
            Set ADORs = oGestorReuniones.DevolverProcesos((chkSinPla = vbChecked), True, udtOrdenar, CDate(txtFecDesde), CDate(txtFecHasta), g_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, basOptimizacion.gCodEqpUsuario, gCodCompradorUsuario, basOptimizacion.gvarCodUsuario, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
            Screen.MousePointer = vbNormal
        Else
            Screen.MousePointer = vbHourglass
            Set ADORs = oGestorReuniones.DevolverProcesos((chkSinPla = vbChecked), False, udtOrdenar, , , g_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, basOptimizacion.gCodEqpUsuario, gCodCompradorUsuario, basOptimizacion.gvarCodUsuario, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, m_bRPerfUON, lIdPerfil)
            Screen.MousePointer = vbNormal
        End If
        
    
        If ADORs Is Nothing Then Exit Sub
        
        While Not ADORs.EOF
            sestado = DevolverEstado(ADORs(4).Value)
            '                  ANYO                      GMN1                      COD                       DEN                       EST                FECAPE                    FECLIMOFE                 FECULTREU                 FECPRES                   FECNEC                    REUDEC
            sdbgReuPla.AddItem ADORs(0).Value & Chr(m_lSeparador) & ADORs(1).Value & Chr(m_lSeparador) & ADORs(2).Value & Chr(m_lSeparador) & ADORs(3).Value & Chr(m_lSeparador) & sestado & Chr(m_lSeparador) & ADORs(5).Value & Chr(m_lSeparador) & ADORs(7).Value & Chr(m_lSeparador) & ADORs(8).Value & Chr(m_lSeparador) & ADORs(9).Value & Chr(m_lSeparador) & ADORs(6).Value & Chr(m_lSeparador) & Int(ADORs(10).Value) & Chr(m_lSeparador) & ADORs(11).Value & Chr(m_lSeparador) & ADORs(12).Value & Chr(m_lSeparador) & ADORs(13).Value & Chr(m_lSeparador) & ADORs(14).Value
            ADORs.MoveNext
        Wend
    
        Set ADORs = Nothing
    End If
End Sub

Private Function BuscarEnArrayProce(ByVal ay As Variant, ByVal Anyo As Integer, ByVal GMN1 As String, ByVal Cod As Long) As Integer
Dim bEncontrado As Boolean
Dim iIndice As Integer
On Error GoTo Error

' Si encuentra el proceso en el array devuelve la posici�n,
' si no lo encuentra devuelve -1
' La posicion 0 no contiene un dato v�lido
        
        iIndice = 0
        BuscarEnArrayProce = -1
        While iIndice < UBound(ay.Anyo) And Not bEncontrado
            
            If ay.Anyo(iIndice) = Anyo And ay.GMN1(iIndice) = GMN1 And ay.Cod(iIndice) = Cod Then
                BuscarEnArrayProce = iIndice
                bEncontrado = True
            End If
            
            iIndice = iIndice + 1
            
        Wend
    
    Exit Function
    
Error:
    
    BuscarEnArrayProce = -1

End Function

''' <summary>
''' Cargar los proceso de la reuni�n y la botonera.
''' </summary>
''' <remarks>Llamada desde: cmdRestaurarGest_Click      sdbcFecReu_CloseUp     sdbcFecReu_Validate; Tiempo m�ximo: 0,2</remarks>
Public Sub ReunionSeleccionada()
Dim oProce As CProceso
    
    sdbgReuGest.RemoveAll
    
    Set oReuSeleccionada = Nothing
    bNoModificarHoraReu = False

    oReuniones.CargarReuniones sdbcFecReu
    
    Set oReuSeleccionada = oReuniones.Item(CStr(CDate((sdbcFecReu.Columns(0).Value))))
    
    If oReuSeleccionada Is Nothing Then
        Exit Sub
    Else
        'Se sacan todos los procesos de la reuni�n independientemente de las restricciones
        oReuSeleccionada.CargarProcesos TipoOrdenacionDatosPlanifReu.OrdPorHoraProceso, True
        
        For Each oProce In oReuSeleccionada.Procesos
            If oProce.Estado >= 11 Then 'Si hay procesos cerrados no puedo modificar la hora de la reuni�n
                bNoModificarHoraReu = True
            End If
            If oProce.responsable Is Nothing Then
                
                sdbgReuGest.AddItem oProce.Anyo & Chr(m_lSeparador) & oProce.GMN1Cod & Chr(m_lSeparador) & oProce.Cod & Chr(m_lSeparador) & oProce.Den & Chr(m_lSeparador) & Int(oProce.Reudec) & Chr(m_lSeparador) & Format(oProce.HoraEnReunion, "long Time") & Chr(m_lSeparador) & Format(oProce.HoraEnReunion, "Short Time") & Chr(m_lSeparador) & Chr(m_lSeparador) & oProce.Estado
            Else
                sdbgReuGest.AddItem oProce.Anyo & Chr(m_lSeparador) & oProce.GMN1Cod & Chr(m_lSeparador) & oProce.Cod & Chr(m_lSeparador) & oProce.Den & Chr(m_lSeparador) & Int(oProce.Reudec) & Chr(m_lSeparador) & Format(oProce.HoraEnReunion, "long Time") & Chr(m_lSeparador) & Format(oProce.HoraEnReunion, "Short Time") & Chr(m_lSeparador) & Chr(m_lSeparador) & oProce.Estado & Chr(m_lSeparador) & oProce.responsable.Apel & Chr(m_lSeparador) & oProce.responsable.nombre & Chr(m_lSeparador) & oProce.responsable.Tfno & Chr(m_lSeparador) & oProce.responsable.mail
            End If
        Next
        
        cmdAgenda(0).Enabled = (oReuSeleccionada.Procesos.Count > 0)
        cmdAgenda(2).Enabled = (oReuSeleccionada.Procesos.Count > 0)
        cmdConv.Enabled = True
        cmdEliReu.Enabled = True
        cmdFecha.Enabled = True
        cmdRestaurarGest.Enabled = True
        cmdModifReu.Enabled = True
        cmdBajar.Enabled = True
        cmdSubir.Enabled = True
        cmdHora.Enabled = True
        cmdA�adirProceso.Enabled = True
        cmdEliminarProceso.Enabled = True
        cmdOrdenar.Enabled = True
        
        sdbgReuGest.Scroll -100, -100
    End If
End Sub

Private Function DevolverEstado(ByVal vEst As Variant)
    Dim sestado As String
    Select Case vEst
        Case TipoEstadoProceso.sinitems, TipoEstadoProceso.ConItemsSinValidar
            sestado = sIdioma(4)
        Case TipoEstadoProceso.validado, TipoEstadoProceso.Conproveasignados
            sestado = sIdioma(5)
        Case TipoEstadoProceso.conasignacionvalida
            sestado = sIdioma(6)
        Case TipoEstadoProceso.conpeticiones, TipoEstadoProceso.conofertas
            sestado = sIdioma(7)
        Case TipoEstadoProceso.ConObjetivosSinNotificar, TipoEstadoProceso.ConObjetivosSinNotificarYPreadjudicado
            sestado = sIdioma(8)
        Case TipoEstadoProceso.PreadjYConObjNotificados
            sestado = sIdioma(9)
        Case TipoEstadoProceso.ParcialmenteCerrado
            sestado = sIdioma(10)
        Case TipoEstadoProceso.conadjudicaciones
            sestado = sIdioma(11)
        Case TipoEstadoProceso.ConAdjudicacionesNotificadas
            sestado = sIdioma(12)
        Case TipoEstadoProceso.Cerrado
            sestado = sIdioma(13)
    End Select
    
    DevolverEstado = sestado
End Function

Private Sub cmdAgenda_Click(Index As Integer)
    Select Case Index
        Case 0
            Dim ADORs As Ador.Recordset
            Set ADORs = oGestorReuniones.ObtenerDatosGenerarDocumentoAgenda(oUsuarioSummit.Cod)
            If Not ADORs Is Nothing Then
                If Not ADORs.EOF Then
                    GenerarDocumentoAgenda ADORs("ID").Value, ADORs("FORMATO").Value
                    ADORs.Close
                Else
                    MostrarFormREUAgenda Year(frmREU.sdbcFecReu.Columns(0).Value), Month(frmREU.sdbcFecReu.Columns(0).Value), Day(frmREU.sdbcFecReu.Columns(0).Value), Hour(frmREU.sdbcFecReu.Columns(0).Value), _
                        Minute(frmREU.sdbcFecReu.Columns(0).Value), Second(frmREU.sdbcFecReu.Columns(0).Value), gParametrosGenerales.gsRutasFullstepWeb, gParametrosGenerales.gsURLSessionService, oUsuarioSummit
                End If
            End If
        Case 2
            MostrarFormREUAgenda Year(frmREU.sdbcFecReu.Columns(0).Value), Month(frmREU.sdbcFecReu.Columns(0).Value), Day(frmREU.sdbcFecReu.Columns(0).Value), Hour(frmREU.sdbcFecReu.Columns(0).Value), _
                Minute(frmREU.sdbcFecReu.Columns(0).Value), Second(frmREU.sdbcFecReu.Columns(0).Value), gParametrosGenerales.gsRutasFullstepWeb, gParametrosGenerales.gsURLSessionService, oUsuarioSummit
    End Select
End Sub

Public Sub GenerarDocumentoAgenda(ByVal IDPlantilla As Integer, ByVal Formato As Integer)
    Timer1.Enabled = True
    frmADJCargar.Show
    frmADJCargar.Refresh
    With WebBrowser1
        .Width = 1
        .Height = 1
        .Top = -5
    End With
    Dim Y, m, d, h, min, Seg As Integer
    Y = Year(frmREU.oReuSeleccionada.Fecha)
    m = Month(frmREU.oReuSeleccionada.Fecha)
    d = Day(frmREU.oReuSeleccionada.Fecha)
    h = Hour(frmREU.oReuSeleccionada.Fecha)
    min = Minute(frmREU.oReuSeleccionada.Fecha)
    Seg = Second(frmREU.oReuSeleccionada.Fecha)

    Dim params, strSessionId As String
    params = "&a=" & Y & "&m=" & m & "&d=" & d & "&h=" & h & "&mn=" & min & "&s=" & Seg
    params = params & "&IdPlantilla=" & IDPlantilla & "&Formato=" & Formato
    ' Se crea el id de sesion de fullstep web
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    Dim sURL As String
    sURL = gParametrosGenerales.gsRutasFullstepWeb & "/App_Pages/GS/Reuniones/agendaExport.aspx"
    sURL = sURL & "?desdeGS=1&SessionId=" & strSessionId & params
    WebBrowser1.Navigate2 sURL, 4
End Sub

Private Sub Timer1_Timer()
    If WebBrowser1.ReadyState = READYSTATE_COMPLETE Then
        frmADJCargar.Hide
        Timer1.Enabled = False
    End If
End Sub

Private Function proceso() As CProceso
    Set proceso = oReuSeleccionada.Procesos.Item(Me.sdbgReuGest.Row + 1)
End Function
