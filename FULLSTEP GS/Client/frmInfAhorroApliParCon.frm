VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroApliParCon 
   Caption         =   "Informe de ahorros aplicados por partidas contables"
   ClientHeight    =   5460
   ClientLeft      =   1800
   ClientTop       =   3015
   ClientWidth     =   11715
   Icon            =   "frmInfAhorroApliParCon.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5460
   ScaleWidth      =   11715
   Begin VB.Frame fraAnyoPres 
      Caption         =   "Considerar el presupuesto en los siguientes a�os"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   7080
      TabIndex        =   27
      Top             =   660
      Width           =   4575
      Begin SSDataWidgets_B.SSDBCombo sdbcADesdePres 
         Height          =   285
         Left            =   870
         TabIndex        =   13
         Top             =   270
         Width           =   975
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAHastaPres 
         Height          =   285
         Left            =   2850
         TabIndex        =   14
         Top             =   270
         Width           =   975
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblHastaPres 
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2130
         TabIndex        =   29
         Top             =   300
         Width           =   660
      End
      Begin VB.Label lblDesdePres 
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   150
         TabIndex        =   28
         Top             =   300
         Width           =   705
      End
   End
   Begin VB.PictureBox picLegend 
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   10095
      Picture         =   "frmInfAhorroApliParCon.frx":0CB2
      ScaleHeight     =   735
      ScaleWidth      =   1635
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.PictureBox picLegend2 
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   10080
      Picture         =   "frmInfAhorroApliParCon.frx":49B4
      ScaleHeight     =   615
      ScaleWidth      =   1635
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   60
      Visible         =   0   'False
      Width           =   1635
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   315
      Left            =   0
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   5130
      Width           =   11715
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   7
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroApliParCon.frx":7B52
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4741885
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroApliParCon.frx":7B6E
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroApliParCon.frx":7B8A
      DividerType     =   1
      DividerStyle    =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   503
      ExtraHeight     =   344
      Columns.Count   =   7
      Columns(0).Width=   2275
      Columns(0).Caption=   "CAPTION"
      Columns(0).Name =   "CAPTION"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2646
      Columns(1).Caption=   "PRESCAPTION"
      Columns(1).Name =   "PRESCAPTION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "PRES"
      Columns(2).Name =   "PRES"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "#,#"
      Columns(2).FieldLen=   256
      Columns(3).Width=   2646
      Columns(3).Caption=   "ADJCAPTION"
      Columns(3).Name =   "ADJCAPTION"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "ADJ"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "#,#"
      Columns(4).FieldLen=   256
      Columns(5).Width=   2090
      Columns(5).Caption=   "AHOCAPTION"
      Columns(5).Name =   "AHOCAPTION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "AHO"
      Columns(6).Name =   "AHO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "#,#"
      Columns(6).FieldLen=   256
      _ExtentX        =   20664
      _ExtentY        =   556
      _StockProps     =   79
      ForeColor       =   0
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fraMostrarDesde 
      Caption         =   "Mostrar resultados desde partida"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   0
      TabIndex        =   17
      Top             =   660
      Width           =   7035
      Begin VB.TextBox lblParCon 
         BackColor       =   &H80000018&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   240
         Width           =   6135
      End
      Begin VB.CommandButton cmdBorrar 
         Height          =   285
         Left            =   6270
         Picture         =   "frmInfAhorroApliParCon.frx":7BA6
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   240
         Width           =   315
      End
      Begin VB.CommandButton cmdSelProy 
         Height          =   285
         Left            =   6615
         Picture         =   "frmInfAhorroApliParCon.frx":7C4B
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   240
         Width           =   315
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRes 
      Height          =   2865
      Left            =   0
      TabIndex        =   15
      Top             =   1380
      Width           =   11625
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   9
      stylesets.count =   3
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmInfAhorroApliParCon.frx":7CB7
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   4744445
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmInfAhorroApliParCon.frx":7CD3
      stylesets(2).Name=   "Green"
      stylesets(2).BackColor=   10409634
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmInfAhorroApliParCon.frx":7CEF
      AllowUpdate     =   0   'False
      ActiveCellStyleSet=   "Normal"
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "Normal"
      SplitterPos     =   2
      SplitterVisible =   -1  'True
      Columns.Count   =   9
      Columns(0).Width=   1032
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   3757
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16776960
      Columns(2).Width=   2910
      Columns(2).Caption=   "Presupuesto total"
      Columns(2).Name =   "PRESTOT"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).NumberFormat=   "Standard"
      Columns(2).FieldLen=   256
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   15400959
      Columns(3).Width=   2963
      Columns(3).Caption=   "Consumido"
      Columns(3).Name =   "PRES"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).NumberFormat=   "Standard"
      Columns(3).FieldLen=   256
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   15400959
      Columns(4).Width=   2725
      Columns(4).Caption=   "Adjudicado"
      Columns(4).Name =   "ADJ"
      Columns(4).Alignment=   1
      Columns(4).CaptionAlignment=   2
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).NumberFormat=   "Standard"
      Columns(4).FieldLen=   256
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   15400959
      Columns(5).Width=   2434
      Columns(5).Caption=   "Ahorro"
      Columns(5).Name =   "AHO"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   2
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "Standard"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1349
      Columns(6).Caption=   "%"
      Columns(6).Name =   "PORCEN"
      Columns(6).Alignment=   1
      Columns(6).CaptionAlignment=   2
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "0.0#\%"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1349
      Columns(7).Caption=   "Obj."
      Columns(7).Name =   "OBJ"
      Columns(7).Alignment=   1
      Columns(7).CaptionAlignment=   2
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).NumberFormat=   "0.0#\%"
      Columns(7).FieldLen=   256
      Columns(7).HasBackColor=   -1  'True
      Columns(7).BackColor=   15400959
      Columns(8).Width=   1085
      Columns(8).Caption=   "Hist..."
      Columns(8).Name =   "HIST"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   4
      Columns(8).ButtonsAlways=   -1  'True
      _ExtentX        =   20505
      _ExtentY        =   5054
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   3705
      Left            =   0
      OleObjectBlob   =   "frmInfAhorroApliParCon.frx":7D0B
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1380
      Width           =   11715
   End
   Begin VB.Frame fraSel 
      Height          =   615
      Left            =   0
      TabIndex        =   24
      Top             =   30
      Width           =   10080
      Begin VB.OptionButton optDir 
         Caption         =   "Adj. directa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5250
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   1560
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6900
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   1155
      End
      Begin VB.OptionButton optReu 
         Caption         =   "Adj. en reuni�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   5250
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Value           =   -1  'True
         Width           =   2730
      End
      Begin VB.CommandButton cmdActualizar 
         Height          =   285
         Left            =   9330
         Picture         =   "frmInfAhorroApliParCon.frx":9731
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   210
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoDesde 
         Height          =   285
         Left            =   570
         TabIndex        =   0
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesDesde 
         Height          =   285
         Left            =   1380
         TabIndex        =   1
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyoHasta 
         Height          =   285
         Left            =   3150
         TabIndex        =   2
         Top             =   210
         Width           =   825
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1455
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMesHasta 
         Height          =   285
         Left            =   3960
         TabIndex        =   3
         Top             =   210
         Width           =   1260
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   2222
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcMon 
         Height          =   285
         Left            =   8055
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   210
         Width           =   795
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1164
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1879
         Columns(2).Caption=   "Equivalencia"
         Columns(2).Name =   "EQUIV"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   1402
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "SSDBCombo1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdImprimir 
         Height          =   285
         Left            =   9720
         Picture         =   "frmInfAhorroApliParCon.frx":97BC
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   210
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CommandButton cmdGrafico 
         Height          =   285
         Left            =   8940
         Picture         =   "frmInfAhorroApliParCon.frx":98BE
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   210
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         Height          =   285
         Left            =   8940
         Picture         =   "frmInfAhorroApliParCon.frx":9C00
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   210
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.Label lblAnyoHasta 
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   2670
         TabIndex        =   26
         Top             =   240
         Width           =   450
      End
      Begin VB.Label lblAnyoDesde 
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   60
         TabIndex        =   25
         Top             =   240
         Width           =   555
      End
   End
   Begin VB.PictureBox picTipoGrafico 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      FillStyle       =   0  'Solid
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   5220
      ScaleHeight     =   465
      ScaleWidth      =   3660
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   150
      Visible         =   0   'False
      Width           =   3660
      Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
         Height          =   285
         Left            =   630
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   90
         Width           =   1755
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Row.Count       =   5
         Row(0)          =   "Barras 2D"
         Row(1)          =   "Barras 3D"
         Row(2)          =   "Lineas 2D"
         Row(3)          =   "Lineas 3D"
         Row(4)          =   "Tarta"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).Caption=   "TIPO"
         Columns(0).Name =   "TIPO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3096
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmInfAhorroApliParCon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables para func. combos
Private bRespetarCombo As Boolean
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oMonedas As CMonedas
Private dequivalencia As Double
Private oMon As CMoneda

Private arResultados() As Variant

Private sParCon1 As String
Private sParCon2 As String
Private sParCon3 As String
Private sParCon4 As String
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_bRuo As String
Private m_sLblUO As String
' variable para listado
Public ofrmLstApliParCon As frmLstINFAhorrosApl

Private sIdiTipoGrafico(5) As String
Private sIdiMeses(12) As String
Private sIdiTotal As String
Private sIdiPresup As String
Private sIdiAdj As String
Private sIdiAhor As String
Private sIdiAnyoInic As String
Private sIdiInfAhorAplic As String
'Private sIdiMosResultDesde As String
Private sIdiMonCent As String
Private sIdiHistRes As String
Private sIdiAnyo As String

Private Sub cmdActualizar_Click()
Dim vacio As Integer

    
    'Comprueba que la fecha de inicio no sea mayor que la de fin
    If val(sdbcAnyoDesde) > val(sdbcAnyoHasta) Then
        sdbgRes.RemoveAll
        oMensajes.PeriodoNoValido
        If Me.Visible Then sdbcAnyoDesde.SetFocus
        Exit Sub
    Else
        If val(sdbcAnyoDesde) = val(sdbcAnyoHasta) Then
            If sdbcMesHasta.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1 > sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1 Then
                sdbgRes.RemoveAll
                oMensajes.PeriodoNoValido
                If Me.Visible Then sdbcMesDesde.SetFocus
                Exit Sub
            End If
        End If
    End If
    
    
    If sdbcADesdePres.Text <> "" Then
        If Len(sdbcADesdePres.Text) <> 4 Then
            oMensajes.NoValido sIdiAnyo
            If Me.Visible Then sdbcADesdePres.SetFocus
            Exit Sub
        Else
            If Not IsNumeric(sdbcADesdePres.Text) Then
                oMensajes.NoValido sIdiAnyo
                If Me.Visible Then sdbcADesdePres.SetFocus
                Exit Sub
            End If
        End If
        If sdbcAHastaPres.Text = "" Then
            bRespetarCombo = True
            sdbcAHastaPres.Text = sdbcADesdePres.Text
            bRespetarCombo = False
        End If
    End If
    If sdbcAHastaPres.Text <> "" Then
        If Len(sdbcAHastaPres.Text) <> 4 Then
            oMensajes.NoValido sIdiAnyo
            If Me.Visible Then sdbcAHastaPres.SetFocus
            Exit Sub
        Else
            If Not IsNumeric(sdbcAHastaPres.Text) Then
                oMensajes.NoValido sIdiAnyo
                If Me.Visible Then sdbcAHastaPres.SetFocus
                Exit Sub
            End If
        End If
        If sdbcADesdePres.Text = "" Then
            bRespetarCombo = True
            sdbcADesdePres.Text = sdbcAHastaPres.Text
            bRespetarCombo = False
        End If
    End If
    If val(sdbcADesdePres.Text) > val(sdbcAHastaPres.Text) Then
        oMensajes.PeriodoNoValido
        If Me.Visible Then sdbcAHastaPres.SetFocus
        Exit Sub
    End If
    If m_bRuo Then
        If basOptimizacion.gUON1Usuario <> "" And lblParCon.Text = "" Then
            ColocarUODeUsuario
        End If
    End If

    Screen.MousePointer = vbHourglass
    arResultados = oGestorInformes.AhorroAplicadoParContable(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, sParCon3, sParCon4, optReu.Value, optDir.Value, True, , , , , val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
        
    On Error GoTo Error:
  
    Screen.MousePointer = vbNormal
    If LBound(arResultados, 2) < 0 Then
        Exit Sub
    End If
    
    CargarGrid
    
    If cmdGrid.Visible = True Then
        MSChart1.Visible = True
        MostrarGrafico sdbcTipoGrafico.Value
    End If
        
    Exit Sub

Error:
    vacio = 0
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & vacio & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & vacio & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & vacio
    Exit Sub

End Sub

Private Sub CargarGrid()
Dim i As Integer
Dim dpres As Double
Dim dadj As Double

    sdbgRes.RemoveAll
    sdbgTotales.RemoveAll
    
    i = 0
    
    While i <= UBound(arResultados, 2)
        
        sdbgRes.AddItem arResultados(0, i) & Chr(m_lSeparador) & arResultados(1, i) & Chr(m_lSeparador) & dequivalencia * arResultados(2, i) & Chr(m_lSeparador) & dequivalencia * arResultados(4, i) & Chr(m_lSeparador) & dequivalencia * arResultados(5, i) & Chr(m_lSeparador) & dequivalencia * arResultados(6, i) & Chr(m_lSeparador) & arResultados(7, i) & Chr(m_lSeparador) & arResultados(3, i)
        dpres = dpres + dequivalencia * arResultados(4, i)
        dadj = dadj + dequivalencia * arResultados(5, i)
        i = i + 1
    
    Wend
    
    sdbgTotales.AddItem sIdiTotal & Chr(m_lSeparador) & sIdiPresup & Chr(m_lSeparador) & dpres & Chr(m_lSeparador) & sIdiAdj & Chr(m_lSeparador) & dadj & Chr(m_lSeparador) & sIdiAhor & Chr(m_lSeparador) & dpres - dadj
    
    Erase arResultados

End Sub

Private Sub cmdBorrar_Click()
    lblParCon = ""
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    m_sUON1 = ""
    m_sUON2 = ""
    m_sUON3 = ""
    
End Sub

Private Sub cmdGrafico_Click()
            
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass

'        sdbcTipoGrafico.Value = "Barras 3D"
        sdbcTipoGrafico.Value = sIdiTipoGrafico(2)
        picTipoGrafico.Visible = True
'        MostrarGrafico "Barras 3D"
        MostrarGrafico sIdiTipoGrafico(2)
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal

End Sub

Private Sub cmdGrid_Click()
    
    picTipoGrafico.Visible = False
    picLegend.Visible = False
    picLegend2.Visible = False
        
    cmdGrafico.Visible = True
    cmdGrid.Visible = False
    sdbgRes.Visible = True
    MSChart1.Visible = False
End Sub

Private Sub cmdImprimir_Click()
Dim iNivel As Integer
Dim i As Integer
Dim iInd As Integer
'zzz
    Set ofrmLstApliParCon = New frmLstINFAhorrosApl
    ofrmLstApliParCon.sOrigen = "frmPARCON"
  
    ofrmLstApliParCon.WindowState = vbNormal
    
    ofrmLstApliParCon.sdbcAnyoDesde.Text = sdbcAnyoDesde.Text
    ofrmLstApliParCon.sdbcAnyoHasta.Text = sdbcAnyoHasta.Text
    ofrmLstApliParCon.sdbcADesdePres.Text = sdbcADesdePres.Text
    ofrmLstApliParCon.sdbcAHastaPres.Text = sdbcAHastaPres.Text

    
    ofrmLstApliParCon.sdbcMesDesde.MoveFirst
    For iInd = 1 To sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark)
        ofrmLstApliParCon.sdbcMesDesde.MoveNext
    Next
    ofrmLstApliParCon.sdbcMesDesde.Text = ofrmLstApliParCon.sdbcMesDesde.Columns(0).Value
    
    ofrmLstApliParCon.sdbcMesHasta.MoveFirst
    For iInd = 1 To sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark)
        ofrmLstApliParCon.sdbcMesHasta.MoveNext
    Next
    ofrmLstApliParCon.sdbcMesHasta.Text = ofrmLstApliParCon.sdbcMesHasta.Columns(0).Value
    
    ofrmLstApliParCon.optReu.Value = optReu.Value
    ofrmLstApliParCon.optDir.Value = optDir.Value
    ofrmLstApliParCon.optTodos.Value = optTodos.Value
    ofrmLstApliParCon.sdbcMon.Text = sdbcMon.Text
    ofrmLstApliParCon.sdbcMon_Validate False
    ofrmLstApliParCon.sUON1 = m_sUON1
    ofrmLstApliParCon.sUON2 = m_sUON2
    ofrmLstApliParCon.sUON3 = m_sUON3
    
    iNivel = 1
    If sParCon1 <> "" Then
        ofrmLstApliParCon.sParCon1 = sParCon1
        iNivel = 1
    End If
    If sParCon2 <> "" Then
        ofrmLstApliParCon.sParCon2 = sParCon2
        iNivel = 2
    End If
    If sParCon3 <> "" Then
        ofrmLstApliParCon.sParCon3 = sParCon3
        iNivel = 3
    End If
    If sParCon4 <> "" Then
        ofrmLstApliParCon.sParCon4 = sParCon4
        iNivel = 4
    End If
    
    ofrmLstApliParCon.cmbNivel.clear
    ofrmLstApliParCon.txtEst = lblParCon
    
    Screen.MousePointer = vbHourglass
    For i = gParametrosGenerales.giNEPC To iNivel Step -1
         If iNivel = 0 Then Exit For
         ofrmLstApliParCon.cmbNivel.AddItem i
    Next
    ofrmLstApliParCon.cmbNivel.ListIndex = 0
    If iNivel = 1 Then
        If sParCon1 <> "" Then
            ofrmLstApliParCon.cmbNivel.Text = iNivel + 1
        Else
            ofrmLstApliParCon.cmbNivel.Text = "1"
        End If
    Else
        If iNivel = 4 Then
            ofrmLstApliParCon.cmbNivel.Text = "4"
        Else
            ofrmLstApliParCon.cmbNivel.Text = iNivel + 1
        End If
    End If
    
    Screen.MousePointer = vbNormal
    ofrmLstApliParCon.Show 1
    
End Sub

Private Sub cmdSelProy_Click()
       
    frmSELPresAnuUON.sOrigen = "InfAhorroApliPar"
    frmSELPresAnuUON.bRUO = m_bRuo
    frmSELPresAnuUON.g_iTipoPres = 2
    frmSELPresAnuUON.bMostrarBajas = True
    frmSELPresAnuUON.Show 1
    
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    m_bRuo = False
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplPartRestUO)) Is Nothing) Then
        m_bRuo = True
        ColocarUODeUsuario
    End If
End Sub

Private Sub ColocarUODeUsuario()
        m_sUON1 = NullToStr(basOptimizacion.gUON1Usuario)
        m_sUON2 = NullToStr(basOptimizacion.gUON2Usuario)
        m_sUON3 = NullToStr(basOptimizacion.gUON3Usuario)
        m_sLblUO = ""
        If m_sUON1 <> "" Then
            m_sLblUO = "(" & m_sUON1
            If m_sUON2 <> "" Then
                m_sLblUO = m_sLblUO & " - " & m_sUON2
                If m_sUON3 <> "" Then
                    m_sLblUO = m_sLblUO & " - " & m_sUON3 & ") "
                Else
                    m_sLblUO = m_sLblUO & ") "
                End If
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        End If
        lblParCon.Text = m_sLblUO

End Sub


Private Sub Form_Load()
    
    Me.Width = 11835
    Me.Height = 5865
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    m_sUON1 = ""
    m_sUON2 = ""
    m_sUON3 = ""
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    Me.caption = sIdiInfAhorAplic & " - " & basParametros.gParametrosGenerales.gsPlurPres2
    fraMostrarDesde.caption = basParametros.gParametrosGenerales.gsSingPres2

    CargarAnyos
    
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
   
    oMonedas.CargarTodasLasMonedas basParametros.gParametrosGenerales.gsMONCEN, , True, , , , True
    
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiMonCent
    Else
        For Each oMon In oMonedas
            sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
        Next
        sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
        dequivalencia = oMonedas.Item(1).Equiv
    End If

    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    
End Sub
Private Sub BorrarDatosTotales()
    
    sdbgTotales.RemoveAll
    sdbgTotales.Columns(6).CellStyleSet "Normal"
    sdbgTotales.Refresh

End Sub

Private Sub CargarAnyos()
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyoDesde.AddItem iInd
        sdbcAnyoHasta.AddItem iInd
        sdbcADesdePres.AddItem iInd
        sdbcAHastaPres.AddItem iInd
        
    Next
    
    sdbcAnyoDesde.Text = iAnyoActual
    sdbcAnyoDesde.ListAutoPosition = True
    sdbcAnyoDesde.Scroll 1, 7
    sdbcAnyoHasta.Text = iAnyoActual
    sdbcAnyoHasta.ListAutoPosition = True
    sdbcAnyoHasta.Scroll 1, 7
    sdbcADesdePres.Scroll 1, 7
    sdbcAHastaPres.Scroll 1, 7

    sdbcMesDesde.Text = "1"
    sdbcMesDesde.ListAutoPosition = True
    sdbcMesHasta.Text = Month(Date) - 1
    sdbcMesHasta.ListAutoPosition = True
    
    sdbcMesDesde.AddItem sIdiMeses(1)
    sdbcMesDesde.AddItem sIdiMeses(2)
    sdbcMesDesde.AddItem sIdiMeses(3)
    sdbcMesDesde.AddItem sIdiMeses(4)
    sdbcMesDesde.AddItem sIdiMeses(5)
    sdbcMesDesde.AddItem sIdiMeses(6)
    sdbcMesDesde.AddItem sIdiMeses(7)
    sdbcMesDesde.AddItem sIdiMeses(8)
    sdbcMesDesde.AddItem sIdiMeses(9)
    sdbcMesDesde.AddItem sIdiMeses(10)
    sdbcMesDesde.AddItem sIdiMeses(11)
    sdbcMesDesde.AddItem sIdiMeses(12)
    
    sdbcMesHasta.AddItem sIdiMeses(1)
    sdbcMesHasta.AddItem sIdiMeses(2)
    sdbcMesHasta.AddItem sIdiMeses(3)
    sdbcMesHasta.AddItem sIdiMeses(4)
    sdbcMesHasta.AddItem sIdiMeses(5)
    sdbcMesHasta.AddItem sIdiMeses(6)
    sdbcMesHasta.AddItem sIdiMeses(7)
    sdbcMesHasta.AddItem sIdiMeses(8)
    sdbcMesHasta.AddItem sIdiMeses(9)
    sdbcMesHasta.AddItem sIdiMeses(10)
    sdbcMesHasta.AddItem sIdiMeses(11)
    sdbcMesHasta.AddItem sIdiMeses(12)
    
    sdbcMesDesde.MoveFirst
    sdbcMesDesde.Text = sdbcMesDesde.Columns(0).Value
    
    sdbcMesHasta.MoveFirst
    
    For iInd = 1 To Month(Date) - 2
        sdbcMesHasta.MoveNext
    Next
    
    sdbcMesHasta.Text = sdbcMesHasta.Columns(0).Value
    

End Sub

Private Sub Form_Resize()
        
Dim W As Double
    
    If Me.Width > 150 Then
            
        W = sdbgRes.Width
        
        sdbgRes.Width = Me.Width - 150
        
        sdbgRes.Columns(0).Width = (sdbgRes.Width) * (sdbgRes.Columns(0).Width / W)
        sdbgRes.Columns(1).Width = (sdbgRes.Width) * (sdbgRes.Columns(1).Width / W)
        sdbgRes.Columns(2).Width = (sdbgRes.Width) * (sdbgRes.Columns(2).Width / W)
        sdbgRes.Columns(3).Width = (sdbgRes.Width) * (sdbgRes.Columns(3).Width / W)
        sdbgRes.Columns(4).Width = (sdbgRes.Width) * (sdbgRes.Columns(4).Width / W)
        sdbgRes.Columns(5).Width = (sdbgRes.Width) * (sdbgRes.Columns(5).Width / W)
        sdbgRes.Columns(6).Width = (sdbgRes.Width) * (sdbgRes.Columns(6).Width / W)
        sdbgRes.Columns(7).Width = (sdbgRes.Width) * (sdbgRes.Columns(7).Width / W)
        sdbgRes.Columns(8).Width = (sdbgRes.Width) * (sdbgRes.Columns(8).Width / W)
        
        sdbgTotales.Top = Me.Height - 780
        sdbgTotales.Width = sdbgRes.Width
        
        sdbgTotales.Columns(0).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(1).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(2).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(3).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(4).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(5).Width = sdbgTotales.Width / 7
        sdbgTotales.Columns(6).Width = sdbgTotales.Width / 7
        
        MSChart1.Width = Me.Width - 150
        
    End If
    
    If Me.Height > 2565 Then
        sdbgRes.Height = Me.Height - 2190
        MSChart1.Height = sdbgRes.Height + 300
    End If
    


End Sub

Public Sub MostrarParConSeleccionada()
    
    sParCon1 = frmSELPresAnuUON.g_sPRES1
    sParCon2 = frmSELPresAnuUON.g_sPRES2
    sParCon3 = frmSELPresAnuUON.g_sPRES3
    sParCon4 = frmSELPresAnuUON.g_sPRES4
    m_sUON1 = frmSELPresAnuUON.g_sUON1
    m_sUON2 = frmSELPresAnuUON.g_sUON2
    m_sUON3 = frmSELPresAnuUON.g_sUON3
    
    m_sLblUO = ""
    If m_sUON1 <> "" Then
        m_sLblUO = "(" & m_sUON1
        If m_sUON2 <> "" Then
            m_sLblUO = m_sLblUO & " - " & m_sUON2
            If m_sUON3 <> "" Then
                m_sLblUO = m_sLblUO & " - " & m_sUON3 & ") "
            Else
                m_sLblUO = m_sLblUO & ") "
            End If
        Else
            m_sLblUO = CStr(m_sLblUO) & ") "
        End If
    End If
    
    If sParCon4 <> "" Then
        lblParCon.Text = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " - " & sParCon4 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sParCon3 <> "" Then
        lblParCon.Text = m_sLblUO & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sParCon2 <> "" Then
        lblParCon.Text = m_sLblUO & sParCon1 & " - " & sParCon2 & " " & frmSELPresAnuUON.g_sDenPres
    ElseIf sParCon1 <> "" Then
        lblParCon.Text = m_sLblUO & sParCon1 & " " & frmSELPresAnuUON.g_sDenPres
    Else
        lblParCon.Text = m_sLblUO
    End If
        
    
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oMon = Nothing
    Set oMonedas = Nothing
    Me.Visible = False
    
End Sub

Private Sub lblParCon_Change()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub optDir_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optReu_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub optTodos_Click()
    sdbgRes.RemoveAll
    BorrarDatosTotales
End Sub

Private Sub sdbcADesdePres_Change()
    If Not bRespetarCombo Then
        If cmdGrafico.Visible Then
            sdbgRes.RemoveAll
        Else
            MSChart1.Visible = False
        End If
        BorrarDatosTotales
    End If
End Sub


Private Sub sdbcADesdePres_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcADesdePres_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
    sdbcAHastaPres.Text = sdbcADesdePres.Text
End Sub

Private Sub sdbcADesdePres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcADesdePres.Text = "" Then
        sdbcAHastaPres.Text = ""
    End If
    bRespetarCombo = False

End Sub

Private Sub sdbcAHastaPres_Change()
    If Not bRespetarCombo Then
        If cmdGrafico.Visible Then
            sdbgRes.RemoveAll
        Else
            MSChart1.Visible = False
        End If
        BorrarDatosTotales
    End If

End Sub

Private Sub sdbcAHastaPres_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAHastaPres_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAHastaPres_Validate(Cancel As Boolean)
    bRespetarCombo = True
    If sdbcAHastaPres.Text = "" Then
        sdbcAHastaPres.Text = sdbcADesdePres.Text
    End If
    bRespetarCombo = False

End Sub

Private Sub sdbcAnyoDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcAnyoHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesDesde_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesDesde_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub


Private Sub sdbcMesHasta_Click()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales
End Sub

Private Sub sdbcMesHasta_CloseUp()
    If cmdGrafico.Visible Then
        sdbgRes.RemoveAll
    Else
        MSChart1.Visible = False
    End If
    BorrarDatosTotales

End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbgRes_BtnClick()
Dim frm As frmInfAhorroApliDet
Dim ADORs As Ador.Recordset
        
        Screen.MousePointer = vbHourglass
        
        If sParCon3 <> "" Then
            Set ADORs = oGestorInformes.AhorroAplicadoParContableHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, sParCon3, sdbgRes.Columns(0).Text, optReu.Value, optDir.Value, , , , , , val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
        Else
            If sParCon2 <> "" Then
                Set ADORs = oGestorInformes.AhorroAplicadoParContableHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sParCon2, sdbgRes.Columns(0).Text, , optReu.Value, optDir.Value, , , , , , val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
            Else
                If sParCon1 <> "" Then
                    Set ADORs = oGestorInformes.AhorroAplicadoParContableHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sParCon1, sdbgRes.Columns(0).Text, , , optReu.Value, optDir.Value, , , , , , val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                Else
                    Set ADORs = oGestorInformes.AhorroAplicadoParContableHistorico(val(sdbcAnyoDesde.Text), sdbcMesDesde.AddItemRowIndex(sdbcMesDesde.Bookmark) + 1, val(sdbcAnyoHasta.Text), sdbcMesHasta.AddItemRowIndex(sdbcMesHasta.Bookmark) + 1, sdbgRes.Columns(0).Text, , , , optReu.Value, optDir.Value, , , , , , val(sdbcADesdePres.Text), val(sdbcAHastaPres.Text), m_sUON1, m_sUON2, m_sUON3)
                End If
            End If
        End If
        
        If Not ADORs Is Nothing Then
            Set frm = New frmInfAhorroApliDet
            sdbgRes.SelBookmarks.Add sdbgRes.Bookmark
            frm.caption = sIdiHistRes
            
            While Not ADORs.EOF
                frm.sdbgRes.AddItem ADORs(0).Value & " - " & ADORs(1).Value & Chr(m_lSeparador) & dequivalencia * ADORs(2).Value & Chr(m_lSeparador) & dequivalencia * ADORs(3).Value & Chr(m_lSeparador) & dequivalencia * ADORs(4).Value & Chr(m_lSeparador) & ADORs(5).Value
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing
        End If
            
        frm.Top = sdbgRes.Top + Me.Top + MDI.Top + sdbgRes.RowTop(sdbgRes.Row) + sdbgRes.RowHeight + 1000
        frm.Left = sdbgRes.Left + Left + 400 + MDI.Left
            
        Screen.MousePointer = vbNormal
            
        frm.Show 1
        sdbgRes.SelBookmarks.RemoveAll
            
        
End Sub

Private Sub sdbgRes_DblClick()
    
    If sdbgRes.Rows = 0 Then Exit Sub
    If sdbgRes.Row < 0 Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If sParCon3 <> "" Then
        sParCon4 = sdbgRes.Columns(0).Value
        lblParCon = m_sLblUO & " " & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " - " & sParCon4 & "  " & sdbgRes.Columns(1).Value
        cmdActualizar_Click
        Screen.MousePointer = vbNormal
            
    Else
        If sParCon2 <> "" Then
            sParCon3 = sdbgRes.Columns(0).Value
            lblParCon = m_sLblUO & " " & sParCon1 & " - " & sParCon2 & " - " & sParCon3 & "  " & sdbgRes.Columns(1).Value
            cmdActualizar_Click
            Screen.MousePointer = vbNormal
        Else
            If sParCon1 <> "" Then
                sParCon2 = sdbgRes.Columns(0).Value
                lblParCon = m_sLblUO & " " & sParCon1 & " - " & sParCon2 & " - " & sdbgRes.Columns(1).Value
                cmdActualizar_Click
                Screen.MousePointer = vbNormal
            Else
                sParCon1 = sdbgRes.Columns(0).Value
                lblParCon = m_sLblUO & " " & sParCon1 & " - " & sdbgRes.Columns(1).Value
                cmdActualizar_Click
                Screen.MousePointer = vbNormal
            End If
        End If
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    If sdbgRes.Columns("AHO").Value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").Value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbgTotales_RowLoaded(ByVal Bookmark As Variant)
    If sdbgTotales.Columns(6).Value = "" Then Exit Sub
    
    If sdbgTotales.Columns(6).Value < 0 Then
        sdbgTotales.Columns(6).CellStyleSet "Red"
    Else
        If sdbgTotales.Columns(6).Value > 0 Then
            sdbgTotales.Columns(6).CellStyleSet "Green"
        End If
    End If
End Sub


Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.Value
End Sub

Private Sub sdbcTipoGrafico_InitColumnProps()
    
    sdbcTipoGrafico.DataFieldList = "Column 0"
    sdbcTipoGrafico.DataFieldToDisplay = "Column 0"
    
End Sub


Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                picLegend.Visible = True
                picLegend2.Visible = False
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                picLegend.Visible = False
                picLegend2.Visible = True
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").Value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").Value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                picLegend.Visible = True
                picLegend2.Visible = False
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").Value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").Value) > CDbl(sdbgRes.Columns("ADJ").Value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value) - CDbl(sdbgRes.Columns("ADJ").Value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").Value) - CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").Value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").Value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").Value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").Value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
    
    
End Sub


Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
    sdbgRes.RemoveAll
    BorrarDatosTotales
        
End Sub

Private Sub sdbcMon_DropDown()
    
    sdbcMon.RemoveAll
    
    oMonedas.CargarTodasLasMonedas , , , , , False, True
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
        cmdActualizar_Click
    End If
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_APLIPARCON, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        lblAnyoDesde.caption = Ador(0).Value
        lblDesdePres.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyoHasta.caption = Ador(0).Value
        lblHastaPres.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value '5
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
       
        sdbcTipoGrafico.RemoveAll '10
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(2) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(3) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(4) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).Value
        sIdiTipoGrafico(5) = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).Value
        sIdiPresup = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(4).caption = Ador(0).Value '20
        sIdiAdj = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(5).caption = Ador(0).Value
        sIdiAhor = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        sdbgRes.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiMeses(1) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(2) = Ador(0).Value '25
        Ador.MoveNext
        sIdiMeses(3) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(4) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(5) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(6) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(7) = Ador(0).Value '30
        Ador.MoveNext
        sIdiMeses(8) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(9) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(10) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(11) = Ador(0).Value
        Ador.MoveNext
        sIdiMeses(12) = Ador(0).Value '35
        Ador.MoveNext
        sIdiTotal = Ador(0).Value
        Ador.MoveNext
        sIdiAnyoInic = Ador(0).Value '40
        Ador.MoveNext
        sIdiInfAhorAplic = Ador(0).Value
        Ador.MoveNext
        sIdiMonCent = Ador(0).Value
        Ador.MoveNext
        sIdiHistRes = Ador(0).Value
        Ador.MoveNext
        fraAnyoPres.caption = Ador(0).Value
        Ador.MoveNext
        sIdiAnyo = Ador(0).Value
       
        Ador.Close
    
    End If

    picLegend.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_APLIPARCON_PICLEGEND + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)
    picLegend2.Picture = LoadResPicture(FSClientResource.FRM_INFAHORRO_APLIPARCON_PICLEGEND2 + basPublic.gParametrosInstalacion.gIdiomaOffSet, vbResBitmap)

    Set Ador = Nothing

End Sub
