VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPROCEItemArticuloEsp 
   BackColor       =   &H00808000&
   Caption         =   "Especificaciones"
   ClientHeight    =   4305
   ClientLeft      =   825
   ClientTop       =   3090
   ClientWidth     =   9585
   Icon            =   "frmPROCEItemArticuloEsp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4305
   ScaleWidth      =   9585
   Begin VB.CommandButton cmdExcluirTodosFichArt 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   4560
      Picture         =   "frmPROCEItemArticuloEsp.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   3450
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirTodosFichArt 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   4560
      Picture         =   "frmPROCEItemArticuloEsp.frx":0D04
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2900
      Width           =   495
   End
   Begin VB.Frame Frame2 
      Height          =   4020
      Left            =   5160
      TabIndex        =   13
      Top             =   0
      Width           =   4455
      Begin VB.PictureBox picEditEspItem 
         BorderStyle     =   0  'None
         Height          =   300
         Left            =   2040
         ScaleHeight     =   300
         ScaleWidth      =   2340
         TabIndex        =   16
         Top             =   3600
         Width           =   2340
         Begin VB.CommandButton cmdModificarEspItem 
            Height          =   300
            Left            =   960
            Picture         =   "frmPROCEItemArticuloEsp.frx":0D55
            Style           =   1  'Graphical
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirEspItem 
            Height          =   300
            Left            =   0
            Picture         =   "frmPROCEItemArticuloEsp.frx":0E9F
            Style           =   1  'Graphical
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarEspItem 
            Height          =   300
            Left            =   480
            Picture         =   "frmPROCEItemArticuloEsp.frx":0F00
            Style           =   1  'Graphical
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEspItem 
            Height          =   300
            Left            =   1425
            Picture         =   "frmPROCEItemArticuloEsp.frx":0F86
            Style           =   1  'Graphical
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAbrirEspItem 
            Height          =   300
            Left            =   1905
            Picture         =   "frmPROCEItemArticuloEsp.frx":1007
            Style           =   1  'Graphical
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.TextBox txtItemEsp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000012&
         Height          =   1275
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         Top             =   420
         Width           =   4260
      End
      Begin MSComctlLib.ListView lstvwItemEsp 
         Height          =   1455
         Left            =   120
         TabIndex        =   9
         Top             =   1980
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   2566
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin VB.Label lblItemFich 
         Caption         =   "Archivos adjuntos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   15
         Top             =   1740
         Width           =   2775
      End
      Begin VB.Label lblItemEsp 
         Caption         =   "Especificaciones del �tem:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   180
         Width           =   4035
      End
   End
   Begin VB.CommandButton cmdExcluirEspArt 
      Height          =   435
      Left            =   4560
      Picture         =   "frmPROCEItemArticuloEsp.frx":1083
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   1200
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirEspArt 
      Height          =   435
      Left            =   4560
      Picture         =   "frmPROCEItemArticuloEsp.frx":10D5
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   600
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirFichEspArt 
      Height          =   345
      Left            =   4560
      Picture         =   "frmPROCEItemArticuloEsp.frx":1126
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2410
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirFichEspArt 
      Height          =   360
      Left            =   4560
      Picture         =   "frmPROCEItemArticuloEsp.frx":1178
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2010
      Width           =   495
   End
   Begin VB.Frame Frame1 
      Height          =   4020
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   4455
      Begin VB.PictureBox picEditEspArt 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   1960
         ScaleHeight     =   375
         ScaleWidth      =   2340
         TabIndex        =   17
         Top             =   3600
         Width           =   2340
         Begin VB.CommandButton cmdModificarEspArt 
            Height          =   300
            Left            =   960
            Picture         =   "frmPROCEItemArticuloEsp.frx":11C9
            Style           =   1  'Graphical
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirEspArt 
            Height          =   300
            Left            =   0
            Picture         =   "frmPROCEItemArticuloEsp.frx":1313
            Style           =   1  'Graphical
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarEspArt 
            Height          =   300
            Left            =   480
            Picture         =   "frmPROCEItemArticuloEsp.frx":1374
            Style           =   1  'Graphical
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEspArt 
            Height          =   300
            Left            =   1425
            Picture         =   "frmPROCEItemArticuloEsp.frx":13FA
            Style           =   1  'Graphical
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAbrirEspArt 
            Height          =   300
            Left            =   1905
            Picture         =   "frmPROCEItemArticuloEsp.frx":147B
            Style           =   1  'Graphical
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.TextBox txtArticuloEsp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000012&
         Height          =   1275
         Left            =   60
         MaxLength       =   800
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   0
         Top             =   420
         Width           =   4260
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   1455
         Left            =   60
         TabIndex        =   1
         Top             =   1980
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   2566
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Path"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin VB.Label lblArticuloEsp 
         Caption         =   "Especificaciones del art�culo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   180
         Width           =   3795
      End
      Begin VB.Label lblArticuloFich 
         Caption         =   "Archivos adjuntos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   11
         Top             =   1740
         Width           =   1410
      End
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEItemArticuloEsp.frx":14F7
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
   Begin VB.Line Line1 
      BorderColor     =   &H8000000F&
      BorderWidth     =   10
      X1              =   4410
      X2              =   5250
      Y1              =   45
      Y2              =   45
   End
End
Attribute VB_Name = "frmPROCEItemArticuloEsp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oItem As CItem
Public g_oArticulo As CArticulo
Public g_oIBaseDatos As IBaseDatos
Public g_bCancelarEsp As Boolean
Public g_sComentario As String
Private sayFileNames() As String
Private Accion As accionessummit
Public g_bRespetarCombo As Boolean
Private m_sIdioma() As String
Public g_sArticulo As String
Public g_sItem As String
Public g_VEstado As Integer
Public g_VCerrado As Boolean
Public g_bVisualizarTodos As Boolean

Public g_bSoloRuta As Boolean

Private m_skb As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Public Sub AnyadirEspsAListaArt()
Dim oEsp As CEspecificacion

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    lstvwArticuloEsp.ListItems.clear
    
    For Each oEsp In g_oArticulo.especificaciones
        lstvwArticuloEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", NullToStr(oEsp.Ruta)
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    
    If g_oArticulo.especificaciones.Count = 0 And txtArticuloEsp.Locked Then
        cmdSalvarEspArt.Enabled = False
        cmdAbrirEspArt.Enabled = False
    End If
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "AnyadirEspsAListaArt", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub AnyadirEspsAListaItem()
Dim oEsp As CEspecificacion

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    lstvwItemEsp.ListItems.clear
    
    For Each oEsp In g_oItem.especificaciones
        lstvwItemEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next

    If g_oItem.especificaciones.Count = 0 And txtItemEsp.Locked = True Then
        cmdSalvarEspItem.Enabled = False
        cmdAbrirEspItem.Enabled = False
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "AnyadirEspsAListaItem", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Public Sub CargarRecursos()

Dim Ador As Ador.Recordset
Dim i As Integer

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ARTICULOITEMESP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        lblArticuloEsp.caption = Ador(0).Value & " " & g_sArticulo
        Ador.MoveNext
        lblItemEsp.caption = Ador(0).Value & " " & g_sItem
        Ador.MoveNext
        lblArticuloFich.caption = Ador(0).Value
        lblItemFich.caption = Ador(0).Value
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(1).Text = Ador(0).Value
        lstvwItemEsp.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(4).Text = Ador(0).Value
        lstvwItemEsp.ColumnHeaders(3).Text = Ador(0).Value
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(5).Text = Ador(0).Value
        lstvwItemEsp.ColumnHeaders(4).Text = Ador(0).Value
        Ador.MoveNext
        ReDim m_sIdioma(1 To 13)
        For i = 1 To 12
            m_sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        lstvwArticuloEsp.ColumnHeaders(3).Text = Ador(0).Value
        
        
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(2).Text = Ador(0).Value
        lstvwItemEsp.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        
        Ador.Close
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub
Public Sub ConfigurarSeguridad()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
            cmdAnyadirEspArt.Enabled = False
            cmdEliminarEspArt.Enabled = False
            cmdModificarEspArt.Enabled = False
            cmdExcluirFichEspArt.Enabled = False
            cmdExcluirEspArt.Enabled = False
            cmdExcluirTodosFichArt.Enabled = False
            txtArticuloEsp.Locked = True
        End If
    End If

    If Not frmPROCE.g_bModifEsp Then
        cmdAnyadirEspItem.Enabled = False
        cmdEliminarEspItem.Enabled = False
        cmdModificarEspItem.Enabled = False
        txtItemEsp.Locked = True
        cmdIncluirEspArt.Enabled = False
        cmdIncluirFichEspArt.Enabled = False
        cmdIncluirTodosFichArt.Enabled = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
Public Sub DeshabilitarBotones()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdAnyadirEspArt.Enabled = False
    cmdAnyadirEspItem.Enabled = False
    cmdEliminarEspArt.Enabled = False
    cmdEliminarEspItem.Enabled = False
    cmdModificarEspArt.Enabled = False
    cmdModificarEspItem.Enabled = False
    cmdIncluirFichEspArt.Enabled = False
    cmdIncluirEspArt.Enabled = False
    cmdIncluirTodosFichArt.Enabled = False
    cmdExcluirEspArt.Enabled = False
    cmdExcluirFichEspArt.Enabled = False
    cmdExcluirTodosFichArt.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "DeshabilitarBotones", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
Private Sub cmdAbrirEspArt_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oFos As FileSystemObject
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        
    Else
        
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
        
        ' Cargamos el contenido en la esp.
        Set oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        
        If IsNull(oEsp.Ruta) Then 'Archivo adjunto, se lee de la base de datos
            
            Screen.MousePointer = vbHourglass
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspArticulo)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Exit Sub
            End If
            
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspArticulo, oEsp.DataSize, sFileName
                        
            'Lanzamos la aplicacion
            ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        
        Else 'Archivo vinculado
        
            Set oFos = New FileSystemObject
            If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
                ShellExecute MDI.hWnd, "Open", oEsp.Ruta & oEsp.nombre, 0&, oEsp.Ruta, 1
            Else
                oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
            End If
        End If
    End If
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
    If err.Number = 70 Then
        Resume Next
    End If

    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdAbrirEspArt_Click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
   End If

End Sub

Private Sub cmdAbrirEspItem_Click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwItemEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        
    Else
        
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
        
        ' Cargamos el contenido en la esp.
        Screen.MousePointer = vbHourglass
        Set oEsp = g_oItem.especificaciones.Item(CStr(lstvwItemEsp.selectedItem.Tag))
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspItem)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspItem, oEsp.DataSize, sFileName
                
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        
        Screen.MousePointer = vbNormal
        
    End If
    
Cancelar:
    If err.Number = 70 Then
        Resume Next
    End If
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number = 70 Then
        Resume Next
    End If

    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdAbrirEspItem_Click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
    End If
End Sub


Private Sub cmdAnyadirEspArt_Click()
Dim DataFile As Integer
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim sdrive As String
Dim sdrive2 As String
Dim sRuta As String
Dim oFos As FileSystemObject
Dim oDrive As Scripting.Drive
Dim arrFileNames As Variant
Dim iFile As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    
    cmmdEsp.filename = ""
    cmmdEsp.DialogTitle = m_sIdioma(1)
    cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    
    cmmdEsp.ShowOpen
    
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then Exit Sub
    
    
    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.sOrigen = "frmPROCEItemArticuloEspA"
    frmPROCEComFich.chkProcFich.Visible = True
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.chkProcFich.Top
        frmPROCEComFich.chkProcFich.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2000
    End If
    frmPROCEComFich.Show 1
    
    If g_bSoloRuta = False Then 'Si es adjunto no pasa por aqui
    
        If Not g_bCancelarEsp Then
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
        
                Set oEsp = oFSGSRaiz.generar_CEspecificacion
                Set oEsp.Articulo = g_oArticulo
                oEsp.nombre = sFileTitle
                oEsp.Comentario = g_sComentario
                
                Screen.MousePointer = vbHourglass
                teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspArticulo)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Exit Sub
                End If
                Dim sAdjunto As String
                Dim ArrayAdjunto() As String
                Dim oFile As File
                Dim bites As Long
                
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspArticulo)
                
                'Creamos un array, cada "substring" se asignar�
                'a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oEsp.Id = ArrayAdjunto(0)
                oEsp.DataSize = bites
               
                g_oArticulo.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , oEsp.Articulo, , , , , oEsp.DataSize
                
                g_oArticulo.EspAdj = 1
                lstvwArticuloEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", ""
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
                
            Next
            basSeguridad.RegistrarAccion accionessummit.ACCArtMod, "Cod:" & g_oArticulo.Cod
        End If
        
        lstvwArticuloEsp.Refresh
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    
    Else ' Guardamos la ruta en lugar del archivo
    
        If Not g_bCancelarEsp Then
            
            Set oFos = New Scripting.FileSystemObject
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
                sdrive = oFos.GetDriveName(sFileName)
                sRuta = Left(sFileName, Len(sFileName) - Len(sFileTitle))
                sRuta = Right(sRuta, Len(sRuta) - Len(sdrive))
                Set oDrive = oFos.GetDrive(sdrive)
                sdrive2 = oDrive.ShareName
                
                If sdrive2 = "" Then
                    sRuta = sdrive & sRuta
                Else
                    sRuta = sdrive2 & sRuta
                End If
                
                If sRuta = "" Then Exit Sub
                  
                Set oEsp = oFSGSRaiz.generar_CEspecificacion
                Set oEsp.Articulo = g_oArticulo
                oEsp.nombre = sFileTitle
                oEsp.Comentario = g_sComentario
                oEsp.Ruta = sRuta
                oEsp.DataSize = FileLen(sFileName)
                
                Screen.MousePointer = vbHourglass
                Set g_oIBaseDatos = oEsp
                teserror = g_oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Set oDrive = Nothing
                    Set oFos = Nothing
                    Exit Sub
                End If
            
                g_oArticulo.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , , oEsp.Comentario, , oEsp.Articulo, , , oEsp.Ruta, , oEsp.DataSize
            
                g_oArticulo.EspAdj = 1
                lstvwArticuloEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", oEsp.Ruta
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
                lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
            
            Next
            basSeguridad.RegistrarAccion accionessummit.ACCArtMod, "Cod:" & g_oArticulo.Cod
            
        End If
        
        lstvwArticuloEsp.Refresh
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
        Set oFos = Nothing
        Set oDrive = Nothing
        Screen.MousePointer = vbNormal
        Exit Sub
    
    End If
    
Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        If oFSGSRaiz.fg_bProgramando Then
            MsgBox err.Description
            Close DataFile
            Set oEsp = Nothing
            Set g_oIBaseDatos = Nothing
        End If
    End If
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        MsgBox err.Description
        Close DataFile
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If

   If err.Number <> 0 Then
        If err.Number <> 32755 Then m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdAnyadirEspArt_Click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
   End If
End Sub

Private Sub cmdAnyadirEspItem_Click()
Dim DataFile As Integer
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit

Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:
    'Bloqueamos el proceso si no lo est� ya
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    cmmdEsp.filename = ""
    cmmdEsp.DialogTitle = m_sIdioma(1)
    cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer

    cmmdEsp.ShowOpen
    
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then
        frmPROCE.DesbloquearProceso ("IT")
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    arrFileNames = ExtraerFicheros(sFileName)
    
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.sOrigen = "frmPROCEItemArticuloEspI"
    frmPROCEComFich.chkProcFich.Visible = False
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    frmPROCEComFich.Show 1
    
    If Not g_bCancelarEsp Then
        Set oFos = New Scripting.FileSystemObject
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            Set oEsp = oFSGSRaiz.generar_CEspecificacion
            Set oEsp.Item = g_oItem
            oEsp.nombre = sFileTitle
            oEsp.Comentario = g_sComentario
            
            Screen.MousePointer = vbHourglass
            teserror = oEsp.ComenzarEscrituraData(TipoEspecificacion.EspItem)
            If teserror.NumError <> TESnoerror Then
                frmPROCE.DesbloquearProceso ("IT")
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set oEsp = Nothing
                Exit Sub
            End If
            Dim sAdjunto As String
            Dim ArrayAdjunto() As String
            Dim oFile As File
            Dim bites As Long
                
            Set oFile = oFos.GetFile(sFileName)
            bites = oFile.Size
            oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
            sAdjunto = oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspItem)
                
            'Creamos un array, cada "substring" se asignar�
            'a un elemento del array
            ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
            oEsp.Id = ArrayAdjunto(0)
            oEsp.DataSize = bites
            
            'g_oItem.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , g_oItem, oEsp.Comentario
            g_oItem.especificaciones.Add oEsp.nombre, oEsp.Fecha, oEsp.Id, , g_oItem, oEsp.Comentario, , , , , , , oEsp.DataSize
            
            g_oItem.EspAdj = 1
            
            lstvwItemEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), sFileTitle, , "ESP"
            lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = oEsp.Comentario
            lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
            lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", oEsp.Comentario
            lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", Date & " " & Time
            lstvwItemEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
        Next iFile
        basSeguridad.RegistrarAccion accionessummit.ACCArtMod, "Cod:" & g_oArticulo.Cod
        
    End If
    lstvwItemEsp.Refresh
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    frmPROCE.DesbloquearProceso ("IT")
    Screen.MousePointer = vbNormal
    Exit Sub
    
Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        If oFSGSRaiz.fg_bProgramando Then
            MsgBox err.Description
            Close DataFile
            Set oEsp = Nothing
            Set g_oIBaseDatos = Nothing
        End If
    End If
    
    frmPROCE.DesbloquearProceso ("IT")
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        MsgBox err.Description
        Close DataFile
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
        
    If err.Number <> 0 Then
        If err.Number <> 32755 Then m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdAnyadirEspItem_Click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
    End If
End Sub

Private Sub cmdeliminarespart_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(3) & ": " & lstvwArticuloEsp.selectedItem.Text)
        If irespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        Set oEsp = g_oArticulo.especificaciones.Item(lstvwArticuloEsp.selectedItem.Index)
        Set g_oIBaseDatos = oEsp
        teserror = g_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            g_oArticulo.especificaciones.Remove lstvwArticuloEsp.ListItems.Item((CStr(lstvwArticuloEsp.selectedItem.key))).Tag
            
            If g_oArticulo.especificaciones.Count = 0 Then
                If txtArticuloEsp = "" Then
                    g_oArticulo.EspAdj = 0
                End If
            End If
            basSeguridad.RegistrarAccion accionessummit.ACCProceItemMod, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(lstvwArticuloEsp.selectedItem.Tag)
            lstvwArticuloEsp.ListItems.Remove (CStr(lstvwArticuloEsp.selectedItem.key))
        End If
        
    End If
    
Cancelar:
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdeliminarespart_click", err, Erl, Me)
      GoTo Cancelar
      Exit Sub
   End If

End Sub
Private Sub cmdIncluirFichEspArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer

        
    'Bloqueo el proceso si no lo est�
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    Set Item = lstvwArticuloEsp.selectedItem
    
    If Not Item Is Nothing Then
        
        'se a�aden las especificaciones del art�culo al �tem
        Screen.MousePointer = vbHourglass
        Set oEsp = oFSGSRaiz.generar_CEspecificacion
        Set oEsp.Item = g_oItem
        teserror = oEsp.Item.CopiarEspecificacionesAItem(g_oArticulo.Cod, , , g_oArticulo.especificaciones.Item(lstvwArticuloEsp.selectedItem.Index).Id)
        If teserror.NumError <> TESnoerror Then
            frmPROCE.DesbloquearProceso ("IT")
            basErrores.TratarError teserror
            Accion = ACCProceItemMod
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
            Exit Sub
        End If
                 
        'Buscamos el mayor �ndice de las especificaciones de los �tems
        For i = 1 To lstvwItemEsp.ListItems.Count
            If g_oItem.especificaciones.Item(i).Id > maxId Then
                maxId = g_oItem.especificaciones.Item(i).Id
            End If
        Next i
        
        maxId = maxId + 1
        lstvwItemEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
        lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
        lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(2) = Item.SubItems(3)
        lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Now
        lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
         
        g_oItem.especificaciones.Add Item.Text, Now, maxId, , g_oItem, Item.SubItems(3)
        g_oItem.EspAdj = 1
         
        Screen.MousePointer = vbNormal
    
    End If
    Screen.MousePointer = vbNormal
    frmPROCE.DesbloquearProceso ("IT")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdIncluirFichEspArt_click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub cmdExcluirFichEspArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set Item = lstvwItemEsp.selectedItem
    
    If Item Is Nothing Then Exit Sub
    
    'se a�aden las especificaciones del �tem al art�culo
    Screen.MousePointer = vbHourglass
    Set oEsp = oFSGSRaiz.generar_CEspecificacion
    Set oEsp.Articulo = g_oArticulo
    teserror = oEsp.Articulo.CopiarEspecificacionAArticulo(g_oItem.proceso.Anyo, g_oItem.proceso.GMN1Cod, g_oItem.proceso.Cod, g_oItem.Id, g_oArticulo.Cod, g_oItem.especificaciones.Item(lstvwItemEsp.selectedItem.Index).Id)
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Accion = ACCProceItemMod
        oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
        Exit Sub
    End If
        
    'Buscamos el mayor �ndice de las especificaciones de los art�culos
    For i = 1 To lstvwArticuloEsp.ListItems.Count
        If g_oArticulo.especificaciones.Item(i).Id > maxId Then
            maxId = g_oArticulo.especificaciones.Item(i).Id
        End If
    Next i
                                                
    maxId = maxId + 1
    lstvwArticuloEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Item.SubItems(2)
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(4) = Now
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
    g_oArticulo.especificaciones.Add Item.Text, Now, maxId, , , Item.SubItems(2), , g_oArticulo
    g_oArticulo.EspAdj = 1

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdExcluirFichEspArt_click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub cmdIncluirTodosFichArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems

    If lstvwArticuloEsp.ListItems.Count = 0 Then
        frmPROCE.DesbloquearProceso ("IT")
        Exit Sub
    End If
    
    Set Item = lstvwArticuloEsp.ListItems.Item(1)
    If Item Is Nothing Then
        frmPROCE.DesbloquearProceso ("IT")
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    'se a�aden las especificaciones del art�culo al �tem
    Set oEsp = oFSGSRaiz.generar_CEspecificacion
    Set oEsp.Item = g_oItem
    teserror = oEsp.Item.CopiarEspecificacionesAItem(g_oArticulo.Cod)
    If teserror.NumError <> TESnoerror Then
        frmPROCE.DesbloquearProceso ("IT")
        basErrores.TratarError teserror
        Accion = ACCProceItemMod
        Screen.MousePointer = vbNormal
        oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
        Exit Sub
    End If
    
    'Buscamos el mayor �ndice de las especificaciones de los �tems
    For i = 1 To lstvwItemEsp.ListItems.Count
        If g_oItem.especificaciones.Item(i).Id > maxId Then
            maxId = g_oItem.especificaciones.Item(i).Id
        End If
    Next
        
    For i = 1 To lstvwArticuloEsp.ListItems.Count
            
        Set Item = lstvwArticuloEsp.ListItems.Item(i)
        
        If Not Item Is Nothing Then
        
            maxId = maxId + 1
            lstvwItemEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
            lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
            lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(2) = Item.SubItems(3)
            lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Now
            lstvwItemEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
            g_oItem.especificaciones.Add Item.Text, Now, maxId, , g_oItem, Item.SubItems(3)
        
        End If
        
    Next
    
    g_oItem.EspAdj = 1
    
    frmPROCE.DesbloquearProceso ("IT")
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdIncluirTodosFichArt_click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub cmdExcluirTodosFichArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If lstvwItemEsp.ListItems.Count = 0 Then Exit Sub
    
    Set Item = lstvwItemEsp.ListItems.Item(1)
    If Item Is Nothing Then Exit Sub
    
    'Buscamos el mayor �ndice de las especificaciones de los art�culos
    For i = 1 To lstvwArticuloEsp.ListItems.Count
        If g_oArticulo.especificaciones.Item(i).Id > maxId Then
            maxId = g_oArticulo.especificaciones.Item(i).Id
        End If
    Next i

    For i = 1 To lstvwItemEsp.ListItems.Count

        Set Item = lstvwItemEsp.ListItems.Item(i)

        If Not Item Is Nothing Then

            Screen.MousePointer = vbHourglass
            'se a�aden las especificaciones de los �tems a los art�culos
            Set oEsp = oFSGSRaiz.generar_CEspecificacion
            Set oEsp.Articulo = g_oArticulo
            teserror = oEsp.Articulo.CopiarEspecificacionAArticulo(g_oItem.proceso.Anyo, g_oItem.proceso.GMN1Cod, g_oItem.proceso.Cod, g_oItem.Id, g_oArticulo.Cod, g_oItem.especificaciones.Item(i).Id)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Accion = ACCProceItemMod
                oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
                Exit Sub
            End If

            maxId = maxId + 1
            lstvwArticuloEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(2) = ""
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Item.SubItems(2)
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(4) = Now
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
            g_oArticulo.especificaciones.Add Item.Text, Now, maxId, , , Item.SubItems(2), , g_oArticulo

        End If

    Next
    
    g_oArticulo.EspAdj = 1

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdExcluirTodosFichArt_click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdSalvarEspArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oFos As FileSystemObject
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        cmmdEsp.DialogTitle = m_sIdioma(6)
        cmmdEsp.Filter = m_sIdioma(7) & "|*.*"
    
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdioma(8)
            Exit Sub
        End If
                
        ' Cargamos el contenido en la esp.
        Set oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        
        If IsNull(oEsp.Ruta) Then 'Archivo adjunto
            Screen.MousePointer = vbHourglass
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspArticulo)
            If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Exit Sub
            End If
                
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspArticulo, oEsp.DataSize, sFileName
        Else 'Archivo vinculado
            Set oFos = New FileSystemObject
            If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
                oFos.CopyFile oEsp.Ruta & oEsp.nombre, sFileName, False
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
            End If
            
            Set oFos = Nothing
            Set oEsp = Nothing
        End If
    End If
    
Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If

   If err.Number <> 0 Then
        If err.Number <> 32755 Then m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdSalvarEspArt_click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
   End If

End Sub

Private Sub cmdsalvarespitem_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    Set Item = lstvwItemEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        cmmdEsp.DialogTitle = m_sIdioma(6)
        cmmdEsp.Filter = m_sIdioma(7) & "|*.*"
    
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdioma(8)
            Exit Sub
        End If
                     
        ' Cargamos el contenido en la esp.
        Screen.MousePointer = vbHourglass
        Set oEsp = g_oItem.especificaciones.Item(CStr(lstvwItemEsp.selectedItem.Tag))
        teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspItem)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set oEsp = Nothing
            Exit Sub
        End If
        
        oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspItem, oEsp.DataSize, sFileName
        
    End If
    
Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    Screen.MousePointer = vbNormal
    If Not oFSGSRaiz.fg_bProgramando Then
        Unload Me
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If

   If err.Number <> 0 Then
        If err.Number <> 32755 Then m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdsalvarespitem_click", err, Erl, Me)
        GoTo Cancelar
        Exit Sub
   End If
    
End Sub
Private Sub cmdModificarEspArt_click()
Dim teserror As TipoErrorSummit
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If lstvwArticuloEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        
        Screen.MousePointer = vbHourglass
        Set g_oIBaseDatos = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        teserror = g_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
        
        basSeguridad.RegistrarAccion accionessummit.ACCProceItemMod, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(lstvwArticuloEsp.selectedItem.Tag)
        
        Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
        frmPROCEEspMod.g_sOrigen = "frmPROCEItemArticuloEspA"
        Screen.MousePointer = vbNormal
        frmPROCEEspMod.Show 1
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdModificarEspArt_click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Private Sub cmdmodificarespitem_click()
Dim teserror As TipoErrorSummit

 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems

    If lstvwItemEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        
        Screen.MousePointer = vbHourglass
        Set g_oIBaseDatos = g_oItem.especificaciones.Item(CStr(lstvwItemEsp.selectedItem.Tag))
        teserror = g_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            frmPROCE.DesbloquearProceso ("IT")
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
        
        basSeguridad.RegistrarAccion accionessummit.ACCProceItemMod, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(lstvwItemEsp.selectedItem.Tag)
        
        Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
        frmPROCEEspMod.g_sOrigen = "frmPROCEItemArticuloEspI"
        Screen.MousePointer = vbNormal
        frmPROCEEspMod.Show 1
    
    End If

    frmPROCE.DesbloquearProceso ("IT")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdmodificarespitem_click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub cmdEliminarEspItem_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo Cancelar:

    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    Set Item = lstvwItemEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(3) & ": " & lstvwItemEsp.selectedItem.Text)
        If irespuesta = vbNo Then
            frmPROCE.DesbloquearProceso ("IT")
            Exit Sub
        End If
        Screen.MousePointer = vbHourglass
        Set oEsp = g_oItem.especificaciones.Item(lstvwItemEsp.selectedItem.Index)
        Set g_oIBaseDatos = oEsp
        teserror = g_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            g_oItem.especificaciones.Remove lstvwItemEsp.ListItems.Item((CStr(lstvwItemEsp.selectedItem.key))).Tag
            If g_oItem.especificaciones.Count = 0 Then
                If txtItemEsp = "" Then
                    g_oItem.EspAdj = 0
                End If
            End If
            basSeguridad.RegistrarAccion accionessummit.ACCProceItemMod, "Anyo:" & CStr(frmPROCE.sdbcAnyo) & "GMN1:" & CStr(frmPROCE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmPROCE.sdbcProceCod) & "Item:" & CStr(lstvwItemEsp.selectedItem.Tag)
            lstvwItemEsp.ListItems.Remove (CStr(lstvwItemEsp.selectedItem.key))
        End If
        
    End If
    
Cancelar:
    frmPROCE.DesbloquearProceso ("IT")
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdEliminarEspItem_click", err, Erl, Me)
      GoTo Cancelar
      Exit Sub
   End If
End Sub

Private Sub cmdExcluirEspArt_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtItemEsp.Text <> "" Then
        txtArticuloEsp.Text = txtItemEsp.Text
        Accion = ACCArtMod
        txtArticuloEsp_Validate False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdExcluirEspArt_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : cmdIncluirEspArt_Click
' Author    : gfa
' Date      : 18/03/2015
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub cmdIncluirEspArt_Click()

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    g_bRespetarCombo = True
    txtItemEsp.Text = txtArticuloEsp.Text
    g_bRespetarCombo = False
    Accion = ACCProceEspMod
    txtItemEsp_Validate False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "cmdIncluirEspArt_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
If Not m_bActivado Then
    m_bActivado = True
End If
    If txtItemEsp.Enabled And Me.Visible Then txtItemEsp.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Width = 9705
    Me.Height = 4710
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    'Array que contendra los ficheros eliminados
    ReDim sayFileNames(0)
    CargarRecursos
    Accion = ACCProceCon
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub Form_Resize()

On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 3150 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    Frame1.Width = Me.Width / 2 - 397
    Frame1.Height = Me.Height - 690
    
    Frame2.Width = Frame1.Width
    Frame2.Height = Frame1.Height
    Frame2.Left = Frame1.Width + 660
    
    txtArticuloEsp.Width = Frame1.Width - 180
    lstvwArticuloEsp.Width = Frame1.Width - 180
    lstvwArticuloEsp.Height = Frame1.Height * 0.32
    txtArticuloEsp.Height = Frame1.Height - lstvwArticuloEsp.Height - 1200
    lblArticuloFich.Top = txtArticuloEsp.Top + txtArticuloEsp.Height + 45
    lstvwArticuloEsp.Top = lblArticuloFich.Top + 240
 
    txtItemEsp.Width = txtArticuloEsp.Width
    lstvwItemEsp.Width = lstvwArticuloEsp.Width
    lstvwItemEsp.Height = lstvwArticuloEsp.Height
    txtItemEsp.Height = txtArticuloEsp.Height
    lblItemFich.Top = lblArticuloFich.Top
    lstvwItemEsp.Top = lstvwArticuloEsp.Top
    

    cmdIncluirEspArt.Left = Frame1.Width + 100
    cmdIncluirFichEspArt.Left = cmdIncluirEspArt.Left
    cmdExcluirEspArt.Left = cmdIncluirEspArt.Left
    cmdExcluirFichEspArt.Left = cmdIncluirEspArt.Left
    cmdIncluirTodosFichArt.Left = cmdIncluirEspArt.Left
    cmdExcluirTodosFichArt.Left = cmdIncluirEspArt.Left
        
    cmdIncluirFichEspArt.Top = lstvwArticuloEsp.Top
    cmdExcluirFichEspArt.Top = cmdIncluirFichEspArt.Top + cmdIncluirFichEspArt.Height + 40
    cmdIncluirTodosFichArt.Top = cmdExcluirFichEspArt.Top + cmdExcluirFichEspArt.Height + 145
    cmdExcluirTodosFichArt.Top = cmdIncluirTodosFichArt.Top + cmdIncluirTodosFichArt.Height + 40
    
    picEditEspArt.Top = Me.Height - 1110
    picEditEspArt.Left = lstvwArticuloEsp.Left + lstvwArticuloEsp.Width - picEditEspArt.Width - 15
    picEditEspItem.Top = Me.Height - 1110
    picEditEspItem.Left = lstvwItemEsp.Left + lstvwItemEsp.Width - picEditEspItem.Width - 15
    
    lstvwArticuloEsp.ColumnHeaders(1).Width = lstvwArticuloEsp.Width * 0.25
    lstvwArticuloEsp.ColumnHeaders(2).Width = lstvwArticuloEsp.Width * 0.2
    lstvwArticuloEsp.ColumnHeaders(3).Width = lstvwArticuloEsp.Width * 0.25
    lstvwArticuloEsp.ColumnHeaders(4).Width = lstvwArticuloEsp.Width * 0.28

    lstvwItemEsp.ColumnHeaders(1).Width = lstvwItemEsp.Width * 0.25
    lstvwItemEsp.ColumnHeaders(2).Width = lstvwItemEsp.Width * 0.45
    lstvwItemEsp.ColumnHeaders(3).Width = lstvwItemEsp.Width * 0.28
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim irespuesta As Integer
    Dim i As Integer
    Dim FOSFile As Scripting.FileSystemObject
    Dim bBorrando As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bDescargarFrm = False Then
    Select Case Accion
        Case accionessummit.ACCProceEspMod
                txtItemEsp_Validate (irespuesta)
        Case accionessummit.ACCArtMod
                txtArticuloEsp_Validate (irespuesta)
    End Select
End If
m_bDescargarFrm = False

    frmPROCE.DesbloquearProceso ("IT")
    
    On Error GoTo ERROR_Frm: 'Este error se tiene  que ejecuta estemos en DES o no
    
    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    
    Exit Sub

ERROR_Frm:
    
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "Form_Unload", err, Erl, Me)
      GoTo ERROR_Frm
      Exit Sub
   End If
    
End Sub

Private Sub lstvwArticuloEsp_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_VEstado = ParcialmenteCerrado And g_VCerrado Then
        oMensajes.ImposibleModItem
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "lstvwArticuloEsp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub txtArticuloEsp_Change()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_bRespetarCombo Then
        If Accion <> ACCArtMod Then
            Accion = ACCArtMod
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "txtArticuloEsp_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub txtArticuloEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Accion = ACCArtMod Then
    
        If StrComp(NullToStr(g_oArticulo.esp), txtArticuloEsp.Text, vbTextCompare) <> 0 Then
                
            g_oArticulo.esp = StrToNull(txtArticuloEsp.Text)
            If txtArticuloEsp <> "" Then
                g_oArticulo.EspAdj = 1
            Else
                If g_oArticulo.especificaciones.Count = 0 Then
                    g_oArticulo.EspAdj = 0
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            ''usuario para la integraci�n
            g_oArticulo.Usuario = basOptimizacion.gvarCodUsuario
            teserror = g_oArticulo.ModificarEspecificacion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCArtCon
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, " Gmn1:" & g_oArticulo.GMN1Cod & " Gmn2:" & g_oArticulo.GMN2Cod & " Gmn3:" & g_oArticulo.GMN3Cod & " Gmn4:" & g_oArticulo.GMN4Cod & " Cod:" & g_oArticulo.Cod & " Esp:" & Left(g_oArticulo.esp, 50)
            Accion = ACCArtCon
            Screen.MousePointer = vbNormal
        
        End If
    
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "txtArticuloEsp_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub txtItemEsp_Change()

   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bRespetarCombo Then Exit Sub
    
    If Not frmPROCE.BloquearProceso("IT") Then Exit Sub
    frmPROCE.m_udtOrigBloqueo = ModificarItems
    
    g_bRespetarCombo = True
        
    If Accion <> ACCProceEspMod Then
        Accion = ACCProceEspMod
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "txtItemEsp_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub txtItemEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Accion = ACCProceEspMod Then
    
        If StrComp(NullToStr(g_oItem.esp), txtItemEsp.Text, vbTextCompare) <> 0 Then
                
            g_oItem.esp = StrToNull(txtItemEsp.Text)
            If txtItemEsp.Text <> "" Then
                g_oItem.EspAdj = 1
            Else
                If g_oItem.especificaciones.Count = 0 Then
                    g_oItem.EspAdj = 0
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            teserror = g_oItem.ModificarEspecificacion
            If teserror.NumError <> TESnoerror Then
                frmPROCE.DesbloquearProceso ("IT")
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCProceCon
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, "Anyo:" & g_oItem.proceso.Anyo & " Gmn1:" & g_oItem.proceso.GMN1Cod & " Proce:" & g_oItem.proceso.Cod & " Item:" & g_oItem.Id & " Esp:" & Left(g_oItem.esp, 50)
            Accion = ACCProceCon
            Screen.MousePointer = vbNormal
            g_bRespetarCombo = False
        
        End If
    End If

    frmPROCE.DesbloquearProceso ("IT")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEItemArticuloEsp", "txtItemEsp_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


