VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmCONFItems 
   Caption         =   "Inclusi�n / exclusi�n de items+"
   ClientHeight    =   6150
   ClientLeft      =   795
   ClientTop       =   2955
   ClientWidth     =   8880
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONFGItems.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6150
   ScaleWidth      =   8880
   Begin VB.PictureBox picGreen 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5655
      Left            =   0
      ScaleHeight     =   5595
      ScaleWidth      =   8775
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   0
      Width           =   8835
      Begin SSDataWidgets_B.SSDBGrid sdbgIncluir 
         Height          =   4815
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   8595
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   9
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).BackColor=   16777162
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCONFGItems.frx":014A
         stylesets(1).Name=   "EL"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCONFGItems.frx":0166
         stylesets(2).Name=   "Tan"
         stylesets(2).BackColor=   9934847
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCONFGItems.frx":0182
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         BalloonHelp     =   0   'False
         CellNavigation  =   1
         MaxSelectedRows =   0
         HeadStyleSet    =   "EL"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterPos     =   1
         Columns.Count   =   9
         Columns(0).Width=   1693
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   100
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   6324
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   200
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1429
         Columns(2).Caption=   "Destino"
         Columns(2).Name =   "DEST"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   100
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1720
         Columns(3).Caption=   "Fec. Ini."
         Columns(3).Name =   "FECINI"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   1640
         Columns(4).Caption=   "Fec. Fin."
         Columns(4).Name =   "FECFIN"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   20
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   1349
         Columns(5).Caption=   "Exclu�do"
         Columns(5).Name =   "EXC"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(5).Style=   2
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "PORTAL"
         Columns(6).Name =   "PORTAL"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "GRUPO"
         Columns(7).Name =   "GRUPO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "IDGRUPO"
         Columns(8).Name =   "IDGRUPO"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   15161
         _ExtentY        =   8493
         _StockProps     =   79
         BackColor       =   8421376
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcIncluir 
         Height          =   285
         Left            =   120
         TabIndex        =   0
         Top             =   120
         Width           =   4005
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777215
         Columns(1).Width=   6482
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777215
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7064
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   8880
      TabIndex        =   6
      Top             =   5655
      Width           =   8880
      Begin VB.CommandButton cmdEdicion 
         Caption         =   "&Edici�n"
         Height          =   315
         Left            =   7320
         TabIndex        =   2
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar+"
         Default         =   -1  'True
         Height          =   345
         Left            =   3360
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar+"
         Height          =   345
         Left            =   4560
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.Label lblAviso 
         Caption         =   "Al aceptar una inclusi�n/exclusi�n se grabara la adjudicaci�n actual."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Width           =   7095
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   1
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmCONFGItems.frx":019E
            Key             =   "Item"
            Object.Tag             =   "Item"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCONFItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oIconfItems As iconfirmacionItems

Private bClick As Boolean

Private m_aIdentificadores As Collection
Private bModError As Boolean
Public udtAccion As accionessummit
Private bGrupo As Boolean
Private m_sTodos As String
Private m_bModoEdicion  As Boolean
Private m_oProcesoSeleccionado As CProceso
Private m_udtOrden As TipoOrdenacionItems
'Variable publicas
Public g_oOrigen As Form
Public g_bConsulta As Boolean

Private m_vEnPedidos() As Variant
Private m_iInd As Integer
Private m_sMensajeExInc As String
Private m_sLitRecalculando As String

'form:control de errores
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean

Private m_sMsgError As String
Private Sub ModoConsulta()
    m_bModoEdicion = False
    sdbcIncluir.Enabled = True
    cmdEdicion.Visible = True
    cmdAceptar.Visible = False
    cmdCancelar.Visible = False
    sdbgIncluir.Columns(5).Locked = True
    sdbgIncluir.SelectTypeRow = ssSelectionTypeNone
End Sub

Private Sub ModoEdicion()
    m_bModoEdicion = True
    sdbcIncluir.Enabled = False
    cmdEdicion.Visible = False
    cmdAceptar.Visible = True
    cmdCancelar.Visible = True
    sdbgIncluir.Columns(5).Locked = False
    sdbgIncluir.SelectTypeRow = ssSelectionTypeMultiSelectRange
End Sub

Private Sub cmdAceptar_Click()
''***************************************************************
''  En funcion de si es un item perteneciente a un grupo o a un proceso
''      guarda los valores en la coleccion y confirma si se ha hecho
''          correctamente
''
''*****************************************************************
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim vbm As Variant
Dim oItem As CItem
Dim irespuesta As Integer
Dim oOfer As COferta

Screen.MousePointer = vbHourglass
cmdAceptar.Enabled = False
sdbgIncluir.Update


'Si hay ofertas
If Not m_oProcesoSeleccionado.Ofertas Is Nothing Then
    irespuesta = oMensajes.AvisoConConfirmacion(m_sMensajeExInc)
    If irespuesta = vbNo Then
        cmdCancelar_Click
        Screen.MousePointer = vbNormal
        cmdAceptar.Enabled = True
        Exit Sub
    Else
        For Each oOfer In m_oProcesoSeleccionado.Ofertas
            Set oOfer.proceso = m_oProcesoSeleccionado
        Next
    End If
End If


teserror = oIconfItems.RealizarConfirmacion(m_oProcesoSeleccionado.Ofertas)


If teserror.NumError <> TESnoerror Then
    If teserror.NumError = TESImposibleExcluirItem Then
        oMensajes.ImposibleExcluirItem teserror.Arg1
        bModError = True
        Screen.MousePointer = vbNormal
        cmdAceptar.Enabled = True
        Exit Sub
    Else
        basErrores.TratarError teserror
        bModError = True
        Screen.MousePointer = vbNormal
        cmdAceptar.Enabled = True
        Exit Sub
    End If
End If

For i = 0 To sdbgIncluir.Rows - 1
    vbm = sdbgIncluir.AddItemBookmark(i)
    If bGrupo Then
        Set oItem = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sdbcIncluir.Columns("COD").Value).Items.Item(sdbgIncluir.Columns(6).CellValue(vbm))
        If sdbgIncluir.Columns(5).CellValue(vbm) = -1 Or sdbgIncluir.Columns(5).CellValue(vbm) = 1 Then
            oItem.Confirmado = False
        Else
            oItem.Confirmado = True
        End If
        oItem.ActualizarFechas
        Set oItem = Nothing
    Else
        Set oItem = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sdbgIncluir.Columns("GRUPO").CellValue(vbm)).Items.Item(sdbgIncluir.Columns(6).CellValue(vbm))
        If sdbgIncluir.Columns(5).CellValue(vbm) = -1 Or sdbgIncluir.Columns(5).CellValue(vbm) = 1 Then
            oItem.Confirmado = False
        Else
            oItem.Confirmado = True
        End If
        oItem.ActualizarFechas
        Set oItem = Nothing
    End If
Next i

If g_oOrigen.Name = "frmRESREU" Then
    teserror = g_oOrigen.m_oProcesoSeleccionado.ActualicarFecUltimaReunion(g_oOrigen.m_oReuSeleccionada.Fecha)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        bModError = True
        Screen.MousePointer = vbNormal
        cmdAceptar.Enabled = True
        Exit Sub
    End If
    frmRESREU.CargarComboProcesos
    frmRESREU.sdbcProceCod.BackColor = 10867194
    frmRESREU.lblHoraReu.BackColor = 10867194

End If

    
Set oIconfItems = Nothing

basSeguridad.RegistrarAccion accionessummit.AccConfItemsMod, "Anyo:" & CStr(g_oOrigen.m_oProcesoSeleccionado.Anyo) & "GMN1:" & g_oOrigen.m_oProcesoSeleccionado.GMN1Cod & "Proce:" & g_oOrigen.m_oProcesoSeleccionado.Cod

udtAccion = ACCConfItemsCon

g_oOrigen.CargarExcluirIncluir

Screen.MousePointer = vbNormal
cmdAceptar.Enabled = True


Unload Me
End Sub

Private Sub cmdCancelar_Click()
    
udtAccion = ACCConfItemsCon
Unload Me
    
End Sub

''' <summary>
''' Establecer al modo de edici�n/consulta al pulsar el bot�n de Edici�n
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema (bt cmdEdicion); Tiempo m�ximo: 0</remarks>
Private Sub cmdEdicion_Click()

lblAviso.Visible = False

If m_bModoEdicion Then
    ModoConsulta
Else
    ModoEdicion
End If

End Sub
Public Function HabilitarBotonesInvitado()
' funci�n para ocultar/mostrar los botones si se es invitado
 If frmRESREU.sdbcProceCod.Columns("INVI").Value Then
 
    cmdEdicion.Visible = False
 Else
 
    cmdEdicion.Visible = True
 End If

End Function

''' <summary>
''' Cargar la pantalla y establecer todo lo necesario para incluir/excluir.
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0</remarks>
Private Sub Form_Load()
    Dim oGrupo As CGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False

    Me.Height = 6555
    Me.Width = 9120
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If g_bConsulta = True Then
        lblAviso.Visible = False
        cmdEdicion.Enabled = False
    End If
    
    ModoConsulta
    m_udtOrden = OrdItemPorOrden
    udtAccion = ACCConfItemsCon
    
    Set m_oProcesoSeleccionado = oFSGSRaiz.Generar_CProceso
    m_oProcesoSeleccionado.Cod = g_oOrigen.m_oProcesoSeleccionado.Cod
    m_oProcesoSeleccionado.GMN1Cod = g_oOrigen.m_oProcesoSeleccionado.GMN1Cod
    m_oProcesoSeleccionado.Anyo = g_oOrigen.m_oProcesoSeleccionado.Anyo
    m_oProcesoSeleccionado.Estado = g_oOrigen.m_oProcesoSeleccionado.Estado
    Set m_oProcesoSeleccionado.Ofertas = oFSGSRaiz.Generar_COfertas
    Set m_oProcesoSeleccionado.Ofertas = g_oOrigen.m_oProcesoSeleccionado.Ofertas
    
    Set m_oProcesoSeleccionado.Grupos = oFSGSRaiz.Generar_CGrupos
    
    For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
        m_oProcesoSeleccionado.Grupos.Add m_oProcesoSeleccionado, oGrupo.Codigo, oGrupo.Den, , , , , , , , , , , , , , , , , , , , , , , , , , , , oGrupo.Cerrado, , , oGrupo.NumItems, lID:=oGrupo.Id
    Next
   
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Resize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "Form_Resize", err, Erl)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
    m_bDescargarFrm = False
    oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If udtAccion = AccConfItemsMod Then
        bModError = False
        cmdAceptar_Click
        If bModError Then
            Cancel = True
            Exit Sub
        End If
    End If

    m_bDescargarFrm = False
    udtAccion = ACCConfItemsCon
    sdbcIncluir.RemoveAll
    Set oIconfItems = Nothing
    Set m_oProcesoSeleccionado = Nothing
    g_bConsulta = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Aplicar el multiling�ismo en la pantalla
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFITEMS, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        cmdAceptar.caption = Ador(0).Value '1
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmCONFItems.caption = Ador(0).Value
        Ador.MoveNext
        sdbgIncluir.Columns(0).caption = Ador(0).Value
        sdbcIncluir.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgIncluir.Columns(1).caption = Ador(0).Value '5
        sdbcIncluir.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgIncluir.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgIncluir.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgIncluir.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgIncluir.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        m_sTodos = Ador(0).Value '10
        Ador.MoveNext
        cmdEdicion.caption = Ador(0).Value
        Ador.MoveNext
        lblAviso.caption = Ador(0).Value
        Ador.MoveNext
        m_sLitRecalculando = Ador(0).Value
        Ador.MoveNext
        m_sMensajeExInc = Ador(0).Value
        Ador.MoveNext
        m_sMensajeExInc = m_sMensajeExInc & vbCrLf & Ador(0).Value
        Ador.MoveNext
        m_sMensajeExInc = m_sMensajeExInc & vbCrLf & Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

''' <summary>
''' Recalcula los tama�os de los objetos de pantalla en funci�n del ancho y alto de la propia pantalla
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: Form_Resize; Tiempo m�ximo: 0</remarks>
Private Sub Arrange()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.WindowState = 0 Then
        If Me.Height < 2475 Then
            Me.Height = 2475
        End If
        If Me.Width < 4425 Then
            Me.Width = 4425
        End If
    End If

    picGreen.Width = Width - 120
    picGreen.Height = Height - 895
    sdbgIncluir.Height = picGreen.Height - 645
    sdbgIncluir.Width = picGreen.Width - 285

    picEdit.Top = picGreen.Top + picGreen.Height + 100
    cmdEdicion.Left = picGreen.Width - cmdEdicion.Width - 105
    cmdCancelar.Left = picEdit.Width / 2 + 50
    cmdAceptar.Left = (picEdit.Width / 2) - cmdAceptar.Width - 50
    sdbgIncluir.Columns(0).Width = sdbgIncluir.Width * 15 / 100
    sdbgIncluir.Columns(1).Width = sdbgIncluir.Width * 35 / 100
    sdbgIncluir.Columns(2).Width = sdbgIncluir.Width * 10 / 100
    sdbgIncluir.Columns(3).Width = sdbgIncluir.Width * 12 / 100
    sdbgIncluir.Columns(4).Width = sdbgIncluir.Width * 12 / 100
    sdbgIncluir.Columns(5).Width = sdbgIncluir.Width * 10 / 100
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "Arrange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Public Sub GrupoSeleccionado(Optional ByVal bNoRecargar As Boolean)
''******************************************************************
''*  Lee los item del grupo o proceso elegido en la combo y carga la grid
''*  Parametros: udtOrden
''*
''*******************************************************************
Dim i As Integer
Dim sCodigo As String
Dim oItem As CItem
Dim oGrupo As CGrupo
Dim iCheckExcluido As Integer

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbgIncluir.RemoveAll

'Relleno un proceso nuevo distinto porque se puede ordenar por las columns y para no cambiar el del origen

If UCase(sdbcIncluir.Text) = UCase(m_sTodos) Then
    If g_bConsulta = False Then
        cmdEdicion.Enabled = True
    End If
    
    bGrupo = False
    
    If Not bNoRecargar Then
        Set oIconfItems = m_oProcesoSeleccionado
        
        m_oProcesoSeleccionado.CargarTodosLosItems m_udtOrden
    End If
    
    For Each oItem In m_oProcesoSeleccionado.Items
            If oItem.Confirmado Then
                iCheckExcluido = 0
            Else
                iCheckExcluido = -1
            End If
            If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
                If Not oItem.Cerrado Then
                    sdbgIncluir.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.DestCod & Chr(m_lSeparador) & oItem.FechaInicioSuministro & Chr(m_lSeparador) & oItem.FechaFinSuministro & Chr(m_lSeparador) & iCheckExcluido & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & oItem.GrupoCod & Chr(m_lSeparador) & oItem.GrupoID
                End If
            Else
                sdbgIncluir.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.DestCod & Chr(m_lSeparador) & oItem.FechaInicioSuministro & Chr(m_lSeparador) & oItem.FechaFinSuministro & Chr(m_lSeparador) & iCheckExcluido & Chr(m_lSeparador) & oItem.Id & Chr(m_lSeparador) & oItem.GrupoCod & Chr(m_lSeparador) & oItem.GrupoID
            End If
            
        Next
    

Else
    bGrupo = True
    
    Set oGrupo = m_oProcesoSeleccionado.Grupos.Item(sdbcIncluir.Columns("COD").Value)
    
    'Si el grupo est� cerrado no deja excluir ni incluir items
    If g_bConsulta = False Then
        If oGrupo.Cerrado = 1 Then
            cmdEdicion.Enabled = False
        Else
            cmdEdicion.Enabled = True
        End If
    End If
    
    If Not bNoRecargar Then
        oGrupo.CargarTodosLosItems m_udtOrden
        
        Set oIconfItems = oGrupo
    End If
    
    For Each oItem In oGrupo.Items
        If oItem.Confirmado Then
            iCheckExcluido = 0
        Else
            iCheckExcluido = -1
        End If
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            If Not oItem.Cerrado Then
                sdbgIncluir.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.DestCod & Chr(m_lSeparador) & oItem.FechaInicioSuministro & Chr(m_lSeparador) & oItem.FechaFinSuministro & Chr(m_lSeparador) & iCheckExcluido & Chr(m_lSeparador) & oItem.Id
            End If
        Else
            sdbgIncluir.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.DestCod & Chr(m_lSeparador) & oItem.FechaInicioSuministro & Chr(m_lSeparador) & oItem.FechaFinSuministro & Chr(m_lSeparador) & iCheckExcluido & Chr(m_lSeparador) & oItem.Id
        End If
    Next
End If

sdbgIncluir.MoveFirst
Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "GrupoSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub sdbcIncluir_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbcIncluir.Columns(0).Value = "**********" Then
    sdbcIncluir.Text = sdbcIncluir.Columns(1).Value
Else
    sdbcIncluir.Text = sdbcIncluir.Columns(0).Value & " - " & sdbcIncluir.Columns(1).Value
End If
GrupoSeleccionado

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "sdbcIncluir_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcIncluir_DropDown()
''****************************************************************
''** Carga el combo con los grupos seleccionados y le a�ade un elemento
''**    nuevo "TODOS" que incluye a todos los items
''******************************************************************
Dim oGrupo As CGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sdbcIncluir.RemoveAll
      
For Each oGrupo In m_oProcesoSeleccionado.Grupos
    sdbcIncluir.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den & Chr(m_lSeparador) & oGrupo.Id
Next

If sdbcIncluir.Rows > 1 Then
    sdbcIncluir.AddItem "**********" & Chr(m_lSeparador) & m_sTodos & Chr(m_lSeparador) & "0"
End If
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "sdbcIncluir_DropDown", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcIncluir_InitColumnProps()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcIncluir.DataFieldList = "Column 0"
    sdbcIncluir.DataFieldToDisplay = "Column 0"

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "sdbcIncluir_InitColumnProps", err, Erl, , m_bActivado)
      Exit Sub
   End If
        
End Sub


Private Sub sdbcIncluir_PositionList(ByVal Text As String)
''********************************************************
''* Posicionarse en el combo segun la seleccion
'*********************************************************
Dim i As Long
Dim vbm As Variant

On Error Resume Next

sdbcIncluir.MoveFirst

If Text <> "" Then
    For i = 0 To sdbcIncluir.Rows - 1
        vbm = sdbcIncluir.GetBookmark(i)
        If UCase(Text) = UCase(Mid(sdbcIncluir.Columns(0).CellText(vbm), 1, Len(Text))) Then
            sdbcIncluir.Bookmark = vbm
            Exit For
        End If
    Next i
End If
End Sub

Private Sub sdbgIncluir_Change()
''*************************************************************
''*  En la coleccion m_aIdentificadores estan guardados los items seleccionados en
''*     la grid para tratarlos en grupo. Si esta coleecion esta vacia se trata
''*         individualmente a los items
''*
''******************************************************************

Dim bk As Variant
Dim i As Integer
Dim bUltimoCheck As Boolean

'Si el item pertenece a un grupo que est� cerrado no deja incluirlo ni excluirlo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbcIncluir.Text = m_sTodos Then
    If sdbgIncluir.Columns("GRUPO").Value <> "" Then
        If m_oProcesoSeleccionado.Grupos.Item(CStr(sdbgIncluir.Columns("GRUPO").Value)).Cerrado = 1 Then
            oMensajes.ImposibleIncluirExcluir
            sdbgIncluir.Columns.Item(5).Value = Not sdbgIncluir.Columns.Item(5).Value
            Exit Sub
        End If
    End If
End If

If m_aIdentificadores Is Nothing Then

    If sdbgIncluir.Columns.Item(5).Value = 1 Or sdbgIncluir.Columns.Item(5).Value = -1 Then
            m_iInd = 0
            Erase m_vEnPedidos
            If bGrupo Then
                If ComprobarEnPedido(sdbcIncluir.Columns("COD").Value, sdbgIncluir.Columns(6).Value) Then
                    oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 3
                    sdbgIncluir.Columns.Item(5).Value = Not sdbgIncluir.Columns.Item(5).Value
                    Exit Sub
                End If
            Else
                If ComprobarEnPedido(m_oProcesoSeleccionado.Items.Item(sdbgIncluir.Columns(6).Value).GrupoCod, sdbgIncluir.Columns(6).Value) Then
                    oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 3
                    sdbgIncluir.Columns.Item(5).Value = Not sdbgIncluir.Columns.Item(5).Value
                    Exit Sub
                End If
            End If
            sdbgIncluir.Columns(0).CellStyleSet "Tan", sdbgIncluir.Row
            sdbgIncluir.Columns(1).CellStyleSet "Tan", sdbgIncluir.Row
            sdbgIncluir.Columns(2).CellStyleSet "Tan", sdbgIncluir.Row
            sdbgIncluir.Columns(3).CellStyleSet "Tan", sdbgIncluir.Row
            sdbgIncluir.Columns(4).CellStyleSet "Tan", sdbgIncluir.Row
            sdbgIncluir.Columns(5).CellStyleSet "Tan", sdbgIncluir.Row
            If bGrupo Then
                m_oProcesoSeleccionado.Grupos.Item(sdbcIncluir.Columns("COD").Value).Items.Item(sdbgIncluir.Columns(6).Value).Confirmado = False
            Else
                m_oProcesoSeleccionado.Items.Item(sdbgIncluir.Columns(6).Value).Confirmado = False
            End If
    Else
            sdbgIncluir.Columns(0).CellStyleSet "Normal", sdbgIncluir.Row
            sdbgIncluir.Columns(1).CellStyleSet "Normal", sdbgIncluir.Row
            sdbgIncluir.Columns(2).CellStyleSet "Normal", sdbgIncluir.Row
            sdbgIncluir.Columns(3).CellStyleSet "Normal", sdbgIncluir.Row
            sdbgIncluir.Columns(4).CellStyleSet "Normal", sdbgIncluir.Row
            sdbgIncluir.Columns(5).CellStyleSet "Normal", sdbgIncluir.Row
            If bGrupo Then
                m_oProcesoSeleccionado.Grupos.Item(sdbcIncluir.Columns("COD").Value).Items.Item(sdbgIncluir.Columns(6).Value).Confirmado = True
            Else
                m_oProcesoSeleccionado.Items.Item(sdbgIncluir.Columns(6).Value).Confirmado = True
            End If
            
    End If
               
Else
    
    If sdbgIncluir.Columns(5).Value = -1 Then
        bUltimoCheck = True
    Else
        bUltimoCheck = False
    End If
    m_iInd = 0
    Erase m_vEnPedidos
    
    For Each bk In m_aIdentificadores
    
        If bUltimoCheck Then
            If bGrupo Then
                If ComprobarEnPedido(sdbcIncluir.Columns("COD").Value, sdbgIncluir.Columns(6).CellValue(bk)) Then
                    sdbgIncluir.Columns.Item(5).Value = 0
                Else
                    m_oProcesoSeleccionado.Grupos.Item(sdbcIncluir.Columns("COD").Value).Items.Item(sdbgIncluir.Columns(6).CellValue(bk)).Confirmado = False
                End If
            Else
                If ComprobarEnPedido(m_oProcesoSeleccionado.Items.Item(sdbgIncluir.Columns(6).CellValue(bk)).GrupoCod, sdbgIncluir.Columns(6).CellValue(bk)) Then
                    sdbgIncluir.Columns.Item(5).Value = 0
                Else
                    m_oProcesoSeleccionado.Items.Item(sdbgIncluir.Columns(6).CellValue(bk)).Confirmado = False
                End If
            End If
        Else
            If bGrupo Then
                m_oProcesoSeleccionado.Grupos.Item(sdbcIncluir.Columns("COD").Value).Items.Item(sdbgIncluir.Columns(6).CellValue(bk)).Confirmado = True
            Else
                m_oProcesoSeleccionado.Items.Item(sdbgIncluir.Columns(6).CellValue(bk)).Confirmado = True
            End If
        End If
    Next
    
    Set m_aIdentificadores = Nothing
    If m_iInd > 0 Then
        oMensajes.ImposibleCambiarAdjudicacion m_vEnPedidos, 3
    End If
    GrupoSeleccionado True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "sdbgIncluir_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgIncluir_Click()
''******************************************************************
''*   Guardamos los items que se han seleccionado en una coleccion
''*     para tratar las modificaciones sobre ellos en conjunto
''*
''*******************************************************************
Dim i As Integer
Dim bk As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgIncluir.Col = -1 Then
    Set m_aIdentificadores = New Collection
    
    For Each bk In sdbgIncluir.SelBookmarks
        m_aIdentificadores.Add bk
    Next
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "sdbgIncluir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbgIncluir_HeadClick(ByVal ColIndex As Integer)
''*******************************************************************
''* Ordena la grid dependiendo de la columna en la que se pulse
''*******************************************************************
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bModoEdicion Then Exit Sub

Select Case ColIndex
    Case 0:
        m_udtOrden = OrdItemPorCodArt
    Case 1:
        m_udtOrden = OrdItemPorDen
    Case 2:
        m_udtOrden = OrdItemPorDest
    Case 3:
        m_udtOrden = OrdItemPorFecIni
    Case 4:
        m_udtOrden = OrdItemPorFecFin

End Select
GrupoSeleccionado

Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "sdbgIncluir_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgIncluir_RowLoaded(ByVal Bookmark As Variant)
''**************************************************************
''* Segun el valor del check modificamos el color de cada fila
''**************************************************************

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgIncluir.Columns(5).Value = True Then

    sdbgIncluir.Columns(0).CellStyleSet "Tan", sdbgIncluir.Row
    sdbgIncluir.Columns(1).CellStyleSet "Tan", sdbgIncluir.Row
    sdbgIncluir.Columns(2).CellStyleSet "Tan", sdbgIncluir.Row
    sdbgIncluir.Columns(3).CellStyleSet "Tan", sdbgIncluir.Row
    sdbgIncluir.Columns(4).CellStyleSet "Tan", sdbgIncluir.Row
    sdbgIncluir.Columns(5).CellStyleSet "Tan", sdbgIncluir.Row
Else
    sdbgIncluir.Columns(0).CellStyleSet "Normal", sdbgIncluir.Row
    sdbgIncluir.Columns(1).CellStyleSet "Normal", sdbgIncluir.Row
    sdbgIncluir.Columns(2).CellStyleSet "Normal", sdbgIncluir.Row
    sdbgIncluir.Columns(3).CellStyleSet "Normal", sdbgIncluir.Row
    sdbgIncluir.Columns(4).CellStyleSet "Normal", sdbgIncluir.Row
    sdbgIncluir.Columns(5).CellStyleSet "Normal", sdbgIncluir.Row
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "sdbgIncluir_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub


Private Function ComprobarEnPedido(ByVal sGrupo As String, ByVal lItem As Long) As Boolean
'Devuelve:  el porcentaje eliminado
Dim oProv As CProveedor
Dim oGrupo As CGrupo
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarEnPedido = False

    If g_oOrigen.m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then

        Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sGrupo)
        For Each oProv In g_oOrigen.m_oProvesAsig
            sCod = oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            sCod = CStr(lItem) & sCod
            If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                'Si est� en pedidos o catalogo no se puede cambiar la adjudicaci�n
                If Not IsNull(oGrupo.Adjudicaciones.Item(sCod).pedido) Then
                    ReDim Preserve m_vEnPedidos(3, m_iInd)
                    m_vEnPedidos(0, m_iInd) = Trim(NullToStr(oGrupo.Items.Item(CStr(lItem)).ArticuloCod) & "  " & NullToStr(oGrupo.Items.Item(CStr(lItem)).Descr))
                    m_vEnPedidos(1, m_iInd) = oGrupo.Adjudicaciones.Item(sCod).pedido
                    m_vEnPedidos(2, m_iInd) = Trim(oProv.Cod & "  " & oProv.Den)
                    m_vEnPedidos(3, m_iInd) = 174 'El �tem esta incluido en los pedidos
                    m_iInd = m_iInd + 1
                    ComprobarEnPedido = True
                ElseIf oGrupo.Adjudicaciones.Item(sCod).Catalogo = 1 Then
                    ReDim Preserve m_vEnPedidos(3, m_iInd)
                    m_vEnPedidos(0, m_iInd) = Trim(NullToStr(oGrupo.Items.Item(CStr(lItem)).ArticuloCod) & "  " & NullToStr(oGrupo.Items.Item(CStr(lItem)).Descr))
                    m_vEnPedidos(1, m_iInd) = oGrupo.Adjudicaciones.Item(sCod).pedido
                    m_vEnPedidos(2, m_iInd) = Trim(oProv.Cod & "  " & oProv.Den)
                    m_vEnPedidos(3, m_iInd) = 175 'El �tem esta incluido en los pedidos
                    m_iInd = m_iInd + 1
                    ComprobarEnPedido = True
                End If
            End If
        Next
        
        Set oGrupo = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONFItems", "ComprobarEnPedido", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function






