VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{75F920A0-2673-11D0-BDCB-0020A90B183A}#8.0#0"; "PVShape.ocx"
Begin VB.Form frmADJGraficos 
   BackColor       =   &H80000005&
   Caption         =   "DGraficos de adjudicaciones"
   ClientHeight    =   5835
   ClientLeft      =   945
   ClientTop       =   4245
   ClientWidth     =   8775
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmADJGraficos.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5835
   ScaleWidth      =   8775
   Begin MSChart20Lib.MSChart MSChart3 
      Height          =   5055
      Left            =   6840
      OleObjectBlob   =   "frmADJGraficos.frx":0442
      TabIndex        =   16
      Top             =   720
      Visible         =   0   'False
      Width           =   1980
   End
   Begin VB.PictureBox PicPrecios 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5745
      Left            =   0
      ScaleHeight     =   5745
      ScaleWidth      =   8745
      TabIndex        =   4
      Top             =   30
      Width           =   8745
      Begin VB.CommandButton cmdZoomOut 
         Height          =   285
         Left            =   1410
         Picture         =   "frmADJGraficos.frx":28C6
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   360
         Width           =   285
      End
      Begin VB.PictureBox picProveedores 
         BackColor       =   &H00A56E3A&
         Height          =   2055
         Left            =   720
         ScaleHeight     =   1995
         ScaleWidth      =   7395
         TabIndex        =   20
         Top             =   610
         Visible         =   0   'False
         Width           =   7455
         Begin SSDataWidgets_B.SSDBGrid sdbgProveedores 
            Height          =   1455
            Left            =   120
            TabIndex        =   22
            Top             =   360
            Width           =   7020
            _Version        =   196617
            DataMode        =   2
            RecordSelectors =   0   'False
            HeadLines       =   0
            Col.Count       =   3
            DividerType     =   0
            DividerStyle    =   2
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            ForeColorEven   =   0
            RowHeight       =   423
            ExtraHeight     =   53
            Columns.Count   =   3
            Columns(0).Width=   714
            Columns(0).Name =   "VISIBLE"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(0).HasHeadBackColor=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).HeadBackColor=   12615680
            Columns(0).BackColor=   16367808
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasHeadForeColor=   -1  'True
            Columns(1).HasHeadBackColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).HeadForeColor=   16777215
            Columns(1).HeadBackColor=   12615680
            Columns(1).BackColor=   16367808
            Columns(2).Width=   11033
            Columns(2).Caption=   "Proveedor"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).HasHeadForeColor=   -1  'True
            Columns(2).HasHeadBackColor=   -1  'True
            Columns(2).HasBackColor=   -1  'True
            Columns(2).HeadForeColor=   16777215
            Columns(2).HeadBackColor=   12615680
            Columns(2).BackColor=   16367808
            _ExtentX        =   12382
            _ExtentY        =   2566
            _StockProps     =   79
            BackColor       =   16367808
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblMostrarProveedor 
            BackColor       =   &H00A56E3A&
            Caption         =   "Mostrar Proveedores"
            ForeColor       =   &H8000000E&
            Height          =   285
            Left            =   240
            TabIndex        =   21
            Top             =   120
            Width           =   5175
         End
      End
      Begin VB.CheckBox Chkzoomin 
         Height          =   285
         Left            =   1080
         Picture         =   "frmADJGraficos.frx":2C08
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   360
         Width           =   285
      End
      Begin VB.CheckBox ChkProveedores 
         Height          =   285
         Left            =   750
         Picture         =   "frmADJGraficos.frx":2F4A
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   360
         Width           =   285
      End
      Begin PVSHAPE3DLib.PVShape3D picLupa 
         Height          =   375
         Left            =   2520
         TabIndex        =   17
         Top             =   1320
         Visible         =   0   'False
         Width           =   975
         _Version        =   524288
         _ExtentX        =   1720
         _ExtentY        =   661
         _StockProps     =   1
         BackColor       =   16777215
         Shape           =   2
         BackColor       =   16777215
         Transparent     =   -1  'True
         TransparentBackground=   -1  'True
      End
      Begin MSChart20Lib.MSChart MSChart1 
         Height          =   5055
         Left            =   120
         OleObjectBlob   =   "frmADJGraficos.frx":2FF0
         TabIndex        =   7
         Top             =   690
         Width           =   6960
      End
      Begin VB.OptionButton OptTotal 
         Caption         =   "DAdjudicado al 100%"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   4575
         TabIndex        =   3
         Top             =   420
         Width           =   3090
      End
      Begin VB.OptionButton OptActual 
         Caption         =   "DAdjudicaci�n actual"
         Height          =   195
         Left            =   1050
         TabIndex        =   2
         Top             =   420
         Value           =   -1  'True
         Width           =   3090
      End
      Begin VB.CheckBox chkLabels 
         DownPicture     =   "frmADJGraficos.frx":69AF
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   420
         Picture         =   "frmADJGraficos.frx":6CF1
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   360
         Width           =   285
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   60
         Picture         =   "frmADJGraficos.frx":6D6B
         Style           =   1  'Graphical
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGrupo 
         Height          =   285
         Left            =   60
         TabIndex        =   0
         Top             =   60
         Width           =   4395
         DataFieldList   =   "Column 1"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   47031
         Columns(1).Width=   6482
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   47031
         _ExtentX        =   7752
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   49344
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcItem 
         Height          =   285
         Left            =   4485
         TabIndex        =   1
         Top             =   60
         Width           =   4185
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2037
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   11468799
         Columns(1).Width=   6033
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   11468799
         Columns(2).Width=   1746
         Columns(2).Caption=   "Grupo"
         Columns(2).Name =   "CODGRUPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   11468799
         Columns(3).Width=   1905
         Columns(3).Caption=   "Anyo/Dest"
         Columns(3).Name =   "DEST"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   11468799
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "ID"
         Columns(4).Name =   "ID"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   7382
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   12648447
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtItem 
         BackColor       =   &H00C0FFFF&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   4485
         Locked          =   -1  'True
         TabIndex        =   13
         Text            =   "DItem"
         Top             =   60
         Width           =   4185
      End
      Begin VB.TextBox txtGrupo 
         BackColor       =   &H0000C0C0&
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   75
         Locked          =   -1  'True
         TabIndex        =   12
         Text            =   "DGrupo"
         Top             =   60
         Width           =   4395
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcEscalado 
         Height          =   285
         Left            =   6045
         TabIndex        =   24
         Top             =   360
         Visible         =   0   'False
         Width           =   2625
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Escalado"
         Columns(1).Name =   "ESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   11468799
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "INI"
         Columns(2).Name =   "INI"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "FIN"
         Columns(3).Name =   "FIN"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   4630
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   12648447
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblRango 
         Caption         =   "DRango de escalado"
         Height          =   255
         Left            =   4485
         TabIndex        =   25
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.PictureBox picAtributos 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5835
      Left            =   0
      ScaleHeight     =   5835
      ScaleWidth      =   8775
      TabIndex        =   8
      Top             =   30
      Width           =   8775
      Begin TabDlg.SSTab sstabGrafico 
         Height          =   5715
         Left            =   60
         TabIndex        =   9
         Top             =   30
         Width           =   8655
         _ExtentX        =   15266
         _ExtentY        =   10081
         _Version        =   393216
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "DSelecci�n de atributos"
         TabPicture(0)   =   "frmADJGraficos.frx":6DF0
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "sdbgAtributos"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "DGr�fico"
         TabPicture(1)   =   "frmADJGraficos.frx":6E0C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "MSChart2"
         Tab(1).Control(1)=   "chkLabels2"
         Tab(1).Control(2)=   "cmdImprimir2"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).ControlCount=   3
         Begin VB.CommandButton cmdImprimir2 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   -74910
            Picture         =   "frmADJGraficos.frx":6E28
            Style           =   1  'Graphical
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   360
            UseMaskColor    =   -1  'True
            Width           =   315
         End
         Begin VB.CheckBox chkLabels2 
            DownPicture     =   "frmADJGraficos.frx":6F2A
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   -74550
            Picture         =   "frmADJGraficos.frx":726C
            Style           =   1  'Graphical
            TabIndex        =   14
            Top             =   360
            Width           =   285
         End
         Begin MSChart20Lib.MSChart MSChart2 
            Height          =   4935
            Left            =   -74940
            OleObjectBlob   =   "frmADJGraficos.frx":75AE
            TabIndex        =   11
            Top             =   690
            Width           =   8490
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   5055
            Left            =   180
            TabIndex        =   10
            Top             =   480
            Width           =   8325
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            Row.Count       =   3
            Col.Count       =   7
            Row(0).Col(0)   =   "-1"
            Row(0).Col(1)   =   "ENTREGA"
            Row(0).Col(2)   =   "Tiempo de retardo en la entrega"
            Row(0).Col(3)   =   "Proceso"
            Row(1).Col(0)   =   "-1"
            Row(1).Col(1)   =   "REV"
            Row(1).Col(2)   =   "N�mero de revisiones gratuitas"
            Row(1).Col(3)   =   "EXC;AP;"
            Row(2).Col(0)   =   "-1"
            Row(2).Col(1)   =   "KM"
            Row(2).Col(2)   =   "Kilometros antes del cambio de aceite."
            Row(2).Col(3)   =   "EXC;AP;..."
            stylesets.count =   19
            stylesets(0).Name=   "ProvActual"
            stylesets(0).ForeColor=   0
            stylesets(0).BackColor=   9693431
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmADJGraficos.frx":F614
            stylesets(1).Name=   "NoHomologado"
            stylesets(1).ForeColor=   192
            stylesets(1).BackColor=   13170165
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmADJGraficos.frx":F630
            stylesets(1).AlignmentText=   1
            stylesets(2).Name=   "Yellow"
            stylesets(2).ForeColor=   0
            stylesets(2).BackColor=   13170165
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmADJGraficos.frx":F64C
            stylesets(2).AlignmentText=   1
            stylesets(3).Name=   "Bold"
            stylesets(3).BackColor=   16777215
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmADJGraficos.frx":F668
            stylesets(4).Name=   "Normal"
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmADJGraficos.frx":F684
            stylesets(5).Name=   "YellowHead"
            stylesets(5).ForeColor=   0
            stylesets(5).BackColor=   13170165
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmADJGraficos.frx":F6A0
            stylesets(6).Name=   "WhiteHead"
            stylesets(6).ForeColor=   0
            stylesets(6).BackColor=   16777215
            stylesets(6).HasFont=   -1  'True
            BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(6).Picture=   "frmADJGraficos.frx":F6BC
            stylesets(7).Name=   "BlueHead"
            stylesets(7).ForeColor=   12582912
            stylesets(7).BackColor=   16777215
            stylesets(7).HasFont=   -1  'True
            BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(7).Picture=   "frmADJGraficos.frx":F6D8
            stylesets(8).Name=   "Red"
            stylesets(8).BackColor=   4744445
            stylesets(8).HasFont=   -1  'True
            BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(8).Picture=   "frmADJGraficos.frx":F6F4
            stylesets(8).AlignmentText=   1
            stylesets(9).Name=   "Proveedor"
            stylesets(9).BackColor=   -2147483633
            stylesets(9).HasFont=   -1  'True
            BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(9).Picture=   "frmADJGraficos.frx":F710
            stylesets(9).AlignmentText=   0
            stylesets(9).AlignmentPicture=   1
            stylesets(10).Name=   "Blue"
            stylesets(10).ForeColor=   12582912
            stylesets(10).BackColor=   16777215
            stylesets(10).HasFont=   -1  'True
            BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(10).Picture=   "frmADJGraficos.frx":F72C
            stylesets(10).AlignmentText=   1
            stylesets(11).Name=   "Adjudicado"
            stylesets(11).BackColor=   10079487
            stylesets(11).HasFont=   -1  'True
            BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(11).Picture=   "frmADJGraficos.frx":F748
            stylesets(12).Name=   "Atrib1"
            stylesets(12).ForeColor=   16711680
            stylesets(12).BackColor=   16777215
            stylesets(12).HasFont=   -1  'True
            BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(12).Picture=   "frmADJGraficos.frx":F764
            stylesets(13).Name=   "Atrib2"
            stylesets(13).ForeColor=   128
            stylesets(13).BackColor=   16777215
            stylesets(13).HasFont=   -1  'True
            BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(13).Picture=   "frmADJGraficos.frx":F780
            stylesets(14).Name=   "ItemCerrado"
            stylesets(14).ForeColor=   16777215
            stylesets(14).BackColor=   12632256
            stylesets(14).HasFont=   -1  'True
            BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(14).Picture=   "frmADJGraficos.frx":F79C
            stylesets(15).Name=   "YellowLeft"
            stylesets(15).ForeColor=   0
            stylesets(15).BackColor=   13170165
            stylesets(15).HasFont=   -1  'True
            BeginProperty stylesets(15).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(15).Picture=   "frmADJGraficos.frx":F7B8
            stylesets(15).AlignmentText=   0
            stylesets(16).Name=   "Green"
            stylesets(16).BackColor=   10409634
            stylesets(16).HasFont=   -1  'True
            BeginProperty stylesets(16).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(16).Picture=   "frmADJGraficos.frx":F7D4
            stylesets(16).AlignmentText=   1
            stylesets(17).Name=   "Atrib3"
            stylesets(17).ForeColor=   8421376
            stylesets(17).BackColor=   16777215
            stylesets(17).HasFont=   -1  'True
            BeginProperty stylesets(17).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(17).Picture=   "frmADJGraficos.frx":F7F0
            stylesets(18).Name=   "GrayHead"
            stylesets(18).ForeColor=   0
            stylesets(18).BackColor=   -2147483633
            stylesets(18).HasFont=   -1  'True
            BeginProperty stylesets(18).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(18).Picture=   "frmADJGraficos.frx":F80C
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            BevelColorShadow=   8421504
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   26
            Columns.Count   =   7
            Columns(0).Width=   1482
            Columns(0).Caption=   "DIncluir"
            Columns(0).Name =   "INCLUIR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(0).HasHeadForeColor=   -1  'True
            Columns(0).HasHeadBackColor=   -1  'True
            Columns(0).HeadForeColor=   16777215
            Columns(0).HeadBackColor=   32896
            Columns(1).Width=   1905
            Columns(1).Caption=   "DC�digo"
            Columns(1).Name =   "CODIGO"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasHeadForeColor=   -1  'True
            Columns(1).HasHeadBackColor=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).HeadForeColor=   16777215
            Columns(1).HeadBackColor=   32896
            Columns(1).BackColor=   48059
            Columns(2).Width=   7038
            Columns(2).Caption=   "DNombre"
            Columns(2).Name =   "NOMBRE"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(2).Style=   1
            Columns(2).ButtonsAlways=   -1  'True
            Columns(2).HasHeadForeColor=   -1  'True
            Columns(2).HasHeadBackColor=   -1  'True
            Columns(2).HasBackColor=   -1  'True
            Columns(2).HeadForeColor=   16777215
            Columns(2).HeadBackColor=   32896
            Columns(2).BackColor=   48059
            Columns(3).Width=   3201
            Columns(3).Caption=   "DIntroducci�n"
            Columns(3).Name =   "INTRODUCCION"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Locked=   -1  'True
            Columns(3).HasHeadForeColor=   -1  'True
            Columns(3).HasHeadBackColor=   -1  'True
            Columns(3).HasForeColor=   -1  'True
            Columns(3).HasBackColor=   -1  'True
            Columns(3).HeadForeColor=   16777215
            Columns(3).HeadBackColor=   32896
            Columns(3).BackColor=   48059
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "IDATRIB"
            Columns(4).Name =   "IDATRIB"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(4).HasHeadForeColor=   -1  'True
            Columns(4).HasHeadBackColor=   -1  'True
            Columns(4).HasForeColor=   -1  'True
            Columns(4).HeadForeColor=   16777215
            Columns(4).HeadBackColor=   32896
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "DGrupo"
            Columns(5).Name =   "CODGRUPO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "DAmbito"
            Columns(6).Name =   "AMBITO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   14684
            _ExtentY        =   8916
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
End
Attribute VB_Name = "frmADJGraficos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum NivelGrafico
    proceso = 0
    Grupo = 1
End Enum

'VARIABLES P�BLICAS
Public g_oProcesoSeleccionado As CProceso
Public g_oProvesAsig As CProveedores
Public g_oAdjs As CAdjsGrupo
Public g_oOrigen As Form
Public g_oEstOfes As COfeEstados
Public g_iGrafico As Integer
Public m_iNumProv As Integer
Public g_oAtribsFormulas As CAtributos
'Para ver si se coge el prove en funci�n de las asignaciones a grupos
Private bCargar As Boolean
Private scodProve As String
'VARIABLES PRIVADAS
Private m_lItem As Long
Private m_sCodGrupo As String
Private m_lIdGrupo As Long
Private m_oGrupoSeleccionado As CGrupo

'Idiomas
Private m_sOferta As String
Private m_sEje As String
Private m_stxtTitulo As String
Private m_stxtPag As String
Private m_stxtDe As String
Private m_sIncompleto As String
Private m_sObjetivo As String
Private m_sPresupuesto As String
Private m_sCaption(9) As String
Private m_sProceso As String
Private m_sGrupo As String
Private m_sItem As String
Private m_sTodos As String
Private m_sImporte As String
Private m_sArticulo As String
Private m_sAhorr As String
Private m_sAhorrNegativo As String
Private m_sAdjud As String
Private m_sConsu As String
Private m_sAbier As String
Private m_sIdiPtsTotales As String
Private m_sRango As String

Private m_arEvol() As Variant
Private m_ar() As Variant
Private m_arLis() As Variant
Private m_ArItem As New ADODB.Recordset
Private m_sIdiMon As String
Private m_sCaptionList(1 To 8) As String
Private m_sALL As String

'Variables para func. combos
Private m_bRespetarCombo As Boolean

' Interface para obtener las ofertas
Private m_oIOfertas As iOfertas

'Variables para el objetivo y el presupuesto totales
Private m_iPresupuesto As Double
Private m_iObjetivo As Double

'Formato para el n�mero de decimales
Private m_sFormatoNumber As String

Private m_bSoloAbiertos  As Boolean
Private m_arProveRep() As Variant

Private m_EscalaMax As Double
Private m_EscalaMin As Double
Private m_Divisiones As Integer
Private m_bPrimeraVez As Boolean

Private m_arEvol2() As Variant
'Private m_arProves() As Variant

Private m_xini As Double
Private m_yini As Double

Private m_yAntes As Long
Private m_xAntes As Long

Private m_bzoomin As Boolean
Private m_arZoom() As Variant
Private m_inumZoom As Integer
Private m_bCambioProveedor As Boolean

Private m_ArColores() As Variant

Private m_ArProveVisibles() As Variant

Private m_bPrimeraCargaEvolucion As Boolean
Private m_bGraficoEsc As Boolean

Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String
 
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ADJGRAFICOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sCaption(1) = Ador(0).Value '1
        Ador.MoveNext
        m_sCaption(2) = Ador(0).Value
        Ador.MoveNext
        m_sCaption(3) = Ador(0).Value
        Ador.MoveNext
        m_sCaption(4) = Ador(0).Value
        Ador.MoveNext
        m_sCaption(5) = Ador(0).Value '5
        m_sCaption(8) = Ador(0).Value
        Ador.MoveNext
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        sdbcItem.Columns("DEN").caption = Ador(0).Value
        m_sItem = Ador(0).Value
        Ador.MoveNext
        m_sArticulo = Ador(0).Value
        Ador.MoveNext
        m_sImporte = Ador(0).Value
        Ador.MoveNext
        m_stxtTitulo = Ador(0).Value '10
        Ador.MoveNext
        m_stxtPag = Ador(0).Value
        Ador.MoveNext
        m_stxtDe = Ador(0).Value
        Ador.MoveNext
        m_sAdjud = Ador(0).Value
        Ador.MoveNext
        m_sAhorr = Ador(0).Value
        Ador.MoveNext
        m_sConsu = Ador(0).Value '15
        Ador.MoveNext
        m_sAbier = Ador(0).Value
        Ador.MoveNext
        OptActual.caption = Ador(0).Value
        Ador.MoveNext
        OptTotal.caption = Ador(0).Value
        Ador.MoveNext
         m_sOferta = Ador(0).Value
        Ador.MoveNext
        m_sEje = Ador(0).Value '20
        m_sEje = m_sEje & "(" & g_oProcesoSeleccionado.MonCod & ")"
        Ador.MoveNext
        m_sIncompleto = Ador(0).Value
        Ador.MoveNext
        m_sObjetivo = Ador(0).Value
        Ador.MoveNext
        m_sPresupuesto = Ador(0).Value
        Ador.MoveNext
        sstabGrafico.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabGrafico.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("INCLUIR").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("CODIGO").caption = Ador(0).Value
        sdbcGrupo.Columns("COD").caption = Ador(0).Value
        sdbcItem.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("NOMBRE").caption = Ador(0).Value
        sdbcGrupo.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("INTRODUCCION").caption = Ador(0).Value
        Ador.MoveNext
        m_sTodos = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        sdbcItem.Columns("CODGRUPO").caption = Ador(0).Value
        Ador.MoveNext
        m_sCaption(6) = Ador(0).Value
        Ador.MoveNext
        m_sCaption(7) = Ador(0).Value
        Ador.MoveNext
        m_sIdiPtsTotales = Ador(0).Value
        Ador.MoveNext
        m_sIdiMon = Ador(0).Value
        Ador.MoveNext
        m_sCaptionList(5) = Ador(0).Value '36
        m_sCaptionList(8) = Ador(0).Value '36
        Ador.MoveNext
        m_sALL = Ador(0).Value
        Ador.MoveNext
        m_sCaptionList(1) = Ador(0).Value
        Ador.MoveNext
        m_sCaptionList(6) = Ador(0).Value
        Ador.MoveNext
        m_sCaptionList(3) = Ador(0).Value
        Ador.MoveNext
        m_sCaptionList(2) = Ador(0).Value
        Ador.MoveNext
        m_sCaptionList(7) = Ador(0).Value
        Ador.MoveNext
        m_sCaptionList(4) = Ador(0).Value
        Ador.MoveNext
        m_sAhorrNegativo = Ador(0).Value
        Ador.MoveNext
        sdbgProveedores.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        lblMostrarProveedor.caption = Ador(0).Value
        Ador.MoveNext
        sdbcItem.Columns("DEST").caption = Ador(0).Value
        Ador.MoveNext
        sdbcEscalado.Columns("ESC").caption = Ador(0).Value
        Ador.MoveNext
        lblRango.caption = Ador(0).Value & ":"
        m_sRango = Ador(0).Value
        Ador.MoveNext
        m_sCaption(9) = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "CargarRecursos", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub



Private Sub chkLabels_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TratarLabelsNumericas (g_iGrafico)
    
    If Me.Visible Then PicPrecios.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "chkLabels_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub chkLabels2_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    TratarLabelsNumericas (g_iGrafico)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "chkLabels2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub ChkProveedores_Click()
Dim oProve As CProveedor

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgProveedores.RemoveAll
    
    picProveedores.Visible = True
    
    For Each oProve In g_oProvesAsig
    sdbgProveedores.AddItem oProve.VisibleEnGrafico & Chr(m_lSeparador) & oProve.Cod & Chr(m_lSeparador) & oProve.Cod & "-" & oProve.Den
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ChkProveedores_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub cmdZoomOut_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If m_bzoomin Then
        Chkzoomin.Value = 0
    End If

    m_inumZoom = UBound(m_arZoom, 2)
    
    If m_inumZoom > 1 Then
        If m_inumZoom - 1 > 1 Then
            cmdZoomOut.Enabled = True
        Else
            cmdZoomOut.Enabled = False
        End If
        ReDim Preserve m_arZoom(1, m_inumZoom - 1)
        If m_bGraficoEsc Then
            ObtenerGraficoUltimaOfertaEscZoom m_arZoom(0, m_inumZoom - 1), m_arZoom(1, m_inumZoom - 1), m_Divisiones
        Else
            ObtenerGraficoProcesoEvolucion2 m_arZoom(0, m_inumZoom - 1), m_arZoom(1, m_inumZoom - 1), m_Divisiones
        End If
    Else
        cmdZoomOut.Enabled = False
        If m_bGraficoEsc Then
            ObtenerGraficoUltimaOfertaEscZoom m_arZoom(0, m_inumZoom), m_arZoom(1, m_inumZoom), m_Divisiones
        Else
            ObtenerGraficoProcesoEvolucion2 m_arZoom(0, m_inumZoom), m_arZoom(1, m_inumZoom), m_Divisiones
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "cmdZoomOut_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    picProveedores.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "Form_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Obtiene el gr�fico de la �ltima oferta para el item y el escalado seleccionado</summary>
''' <remarks>Llamada desde: Form_Load; m�ximo: 0</remarks>

Private Sub ObtenerGraficoUltimaOfertaEsc()
    Dim lbl As MSChart20Lib.Label
    Dim i As Integer
    Dim Ador As Ador.Recordset
    Dim bIncompleto As Boolean
    Dim k As Integer
    Dim oProve As CProveedor
    Dim iNumEsc As Long
    Dim dblPrec As Double
    Dim dblPrecio As Double
    Dim oGrupo As CGrupo
    Dim lMax As Double
    Dim lMin As Double
    Dim oEsc As CEscalado
    Dim oproce As CProceso
    Dim icolores As Integer
    Dim bPrimeraVez As Boolean
    Dim iCarga As Integer
    Dim sCodGrupo As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bGraficoEsc = True
    
    lMax = 0
    lMin = 0
    
    bPrimeraVez = True

    PicPrecios.Visible = True
    picAtributos.Visible = False

    MSChart1.ShowLegend = True
    MSChart3.Visible = False

    If m_oIOfertas Is Nothing Then Exit Sub

    If g_oProvesAsig.Count = 0 Then
        If m_lItem <> 0 Then
            oMensajes.NoExistenOfertasProc 47
        ElseIf m_sCodGrupo <> m_sProceso Then
            oMensajes.NoExistenOfertasProc 120
        Else
            oMensajes.NoExistenOfertasProc 44
        End If
        Exit Sub
    End If

    MSChart1.chartType = VtChChartType2dLine
    
    If m_sCodGrupo <> m_sProceso Then
        If m_sCodGrupo = "ALL" Then
            sCodGrupo = g_oProcesoSeleccionado.grupos.Item(1).Codigo
        Else
            sCodGrupo = m_sCodGrupo
        End If
    Else
        sCodGrupo = sdbcGrupo.Columns("COD").Value
    End If
    Set oGrupo = g_oProcesoSeleccionado.grupos.Item(sCodGrupo)
    
    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    ReDim m_arEvol(1 To oGrupo.Escalados.Count, 1)
    MSChart1.ColumnCount = m_iNumProv

    i = 1
    For Each oEsc In oGrupo.Escalados
        Select Case oGrupo.TipoEscalados
            Case TModoEscalado.ModoDirecto
                m_arEvol(i, 1) = CStr(oEsc.Inicial) & " "   'Si no es un string no lo coge como base del eje horizontal
            Case TModoEscalado.ModoRangos
                m_arEvol(i, 1) = oEsc.Inicial & " - " & oEsc.final
        End Select
        
        i = i + 1
    Next
    Set oEsc = Nothing

    Erase m_ArColores
    ReDim m_ArColores(g_oProvesAsig.Count, 2)
    Erase m_ArProveVisibles
    ReDim m_ArProveVisibles(g_oProvesAsig.Count)
    
    For iCarga = 1 To g_oProvesAsig.Count
        m_ArProveVisibles(iCarga) = 1
        iCarga = iCarga + 1
    Next
    
    i = 1
    icolores = 1
    For Each oProve In g_oProvesAsig
        If oProve.VisibleEnGrafico Then

            bIncompleto = False
            'Obtiene las ofertas para cada proveedor
            Set oproce = oFSGSRaiz.Generar_CProceso
            Set Ador = oproce.DevolverUltimasOfertasItemEsc(g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, m_lItem, oProve.Cod)
            Set oproce = Nothing
            
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If g_oOrigen.Name = "frmADJItem" Then
                    If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sCodGrupo) Is Nothing Then
                        bCargar = False
                    End If
                Else
                    If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sCodGrupo) Is Nothing Then
                        bCargar = False
                    End If
                End If
            End If

            'Las cantidades est�n ya cambiadas a moneda del proceso
            If bCargar Then
                ReDim Preserve m_arEvol(1 To oGrupo.Escalados.Count, i + 1)

                If Not Ador.EOF Then
                    dblPrecio = 0
                    
                    bIncompleto = False
                    k = 1
                    While Not Ador.EOF
                        dblPrec = NullToDbl0(Ador("PREC_VALIDO").Value)
                                                                                                
                        iNumEsc = 0
                        For Each oEsc In oGrupo.Escalados
                            iNumEsc = iNumEsc + 1
                            If oEsc.Id = Ador("ESC") Then
                                If k < iNumEsc Then bIncompleto = True
                                Exit For
                            End If
                        Next
                        m_arEvol(iNumEsc, i + 1) = dblPrec

                        If bPrimeraVez Then
                            lMax = dblPrecio
                            lMin = dblPrecio
                            bPrimeraVez = False
                        Else
                            If dblPrecio > lMax Then
                                lMax = dblPrecio
                            End If

                            If dblPrecio < lMin Then
                                lMin = dblPrecio
                            End If
                        End If
                        
                        dblPrecio = dblPrec
                        
                        k = k + 1
                        Ador.MoveNext
                    Wend

                    If dblPrecio > lMax Then
                        lMax = dblPrecio
                    End If

                    If dblPrecio < lMin Then
                        lMin = dblPrecio
                    End If

                    Ador.Close
                    Set Ador = Nothing
                End If

                'Leyenda de los proveedores
                If bIncompleto Then
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod) & " " & m_sIncompleto
                Else
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod)
                End If
                
                m_ArColores(i, 0) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red
                m_ArColores(i, 1) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green
                m_ArColores(i, 2) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue

                i = i + 1
                icolores = icolores + 1
            End If
        Else
            icolores = icolores + 1
        End If
    Next

    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    MSChart1.ColumnCount = i - 1
    
    TratarLabelsNumericas (g_iGrafico)

    'A�ade los valores del array al gr�fico
    MSChart1.ChartData = m_arEvol

    'leyenda
    MSChart1.Legend.TextLayout.Orientation = VtOrientationUp
    MSChart1.Legend.Location.LocationType = VtChLocationTypeTop

    'Etiquetas, fuentes y orientaci�n de los ejes
    MSChart1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = m_sEje
    MSChart1.Plot.Axis(VtChAxisIdX).AxisTitle.Text = m_sRango & " (" & oGrupo.UnidadEscalado & ")"

    'MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = True
    MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = False
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MajorDivision = 11
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MinorDivision = 1

    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = lMax
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = lMin

    m_EscalaMax = MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum
    m_EscalaMin = MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum

    ReDim Preserve m_arZoom(1, 1)
    m_inumZoom = 1

    m_arZoom(0, m_inumZoom) = lMin
    m_arZoom(1, m_inumZoom) = lMax
    m_Divisiones = 10
    cmdZoomOut.Enabled = False

    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
        lbl.VtFont.Style = VtFontStyleBold
        lbl.VtFont.Size = 8
        lbl.TextLayout.Orientation = VtOrientationHorizontal
    Next

    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
        lbl.VtFont.Size = 8
        lbl.VtFont.Style = VtFontStyleBold
        lbl.Format = m_sFormatoNumber
    Next
    
    Set oGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoUltimaOfertaEsc", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub sdbcEscalado_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcEscalado.DroppedDown Then
        sdbcEscalado = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcEscalado_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcEscalado_CloseUp()
    Dim sCodGrupo As String
    Dim bMostrarGrafico As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbcEscalado
        If .Value = "..." Or .Value = "" Then
            .Text = ""
            Exit Sub
        End If
        
        m_bRespetarCombo = True
        .Text = .Columns("ESC").Text
        m_bRespetarCombo = False
        
        If g_iGrafico = 1 Then
             ObtenerGraficoItem
        Else
            If m_sCodGrupo <> vbNullString And m_sCodGrupo <> m_sProceso Then
                sCodGrupo = m_sCodGrupo
            Else
                sCodGrupo = sdbcItem.Columns("CODGRUPO").Value
            End If
            
            bMostrarGrafico = True
            If g_oProcesoSeleccionado.grupos.Item(sCodGrupo).UsarEscalados Then
                If m_sCodGrupo <> vbNullString And m_sCodGrupo <> m_sProceso Then
                    If txtItem.Text = vbNullString Then bMostrarGrafico = False
                Else
                    If sdbcItem.Text = vbNullString Then bMostrarGrafico = False
                End If
            End If
            
            If bMostrarGrafico Then
                ObtenerGraficoProcesoEvolucion
            End If
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcEscalado_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcEscalado_DropDown()
    Dim oEsc As CEscalado
    Dim sEsc As String
    Dim sCodGrupo As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbcEscalado
        .RemoveAll
        If sdbcGrupo.Text <> m_sProceso Then
            If sdbcGrupo.Text <> vbNullString Then
                sCodGrupo = sdbcGrupo.Columns(0).Value
            Else
                sCodGrupo = m_sCodGrupo
            End If
        Else
            sCodGrupo = sdbcItem.Columns("CODGRUPO").Value
        End If
        
        Screen.MousePointer = vbHourglass

        For Each oEsc In g_oProcesoSeleccionado.grupos.Item(sCodGrupo).Escalados
            If g_oProcesoSeleccionado.grupos.Item(sCodGrupo).TipoEscalados = ModoDirecto Then
                sEsc = oEsc.Inicial
            Else
                sEsc = oEsc.Inicial & " - " & oEsc.final
            End If
            sEsc = sEsc & " (" & g_oProcesoSeleccionado.grupos.Item(sCodGrupo).UnidadEscalado & ")"
            .AddItem oEsc.Id & Chr(m_lSeparador) & sEsc & Chr(m_lSeparador) & oEsc.Inicial & Chr(m_lSeparador) & oEsc.final
        Next
        
        If g_oProcesoSeleccionado.grupos.Item(sCodGrupo).Escalados.Count = 0 Then
            .AddItem "..."
        End If
        
        Set oEsc = Nothing
        
        .SelStart = 0
        .Refresh
    End With
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcEscalado_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcEscalado_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcEscalado.DataFieldList = "Column 1"
    sdbcEscalado.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcEscalado_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgProveedores_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bCambioProveedor = True

Dim oProve As CProveedor
    If sdbgProveedores.Columns(sdbgProveedores.Col).Name = "VISIBLE" Then
            For Each oProve In g_oProvesAsig
                If sdbgProveedores.Columns("COD").Text = oProve.Cod Then
                    If sdbgProveedores.Columns("VISIBLE").Value = "-1" Or sdbgProveedores.Columns("VISIBLE").Value = "1" Then
                        oProve.VisibleEnGrafico = True
                    Else
                        oProve.VisibleEnGrafico = False
                    End If
                    Exit For
                End If
            
            Next
        
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbgProveedores_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub Chkzoomin_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bzoomin Then
    m_bzoomin = False
Else
    m_bzoomin = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "Chkzoomin_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub cmdImprimir_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ObtenerInforme
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "cmdImprimir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub cmdImprimir2_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ObtenerInforme
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "cmdImprimir2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim oProve As CProveedor
    Dim aSymbols As Variant
    Dim bHayEscalados As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_yAntes = 0
    m_xAntes = 0
    m_inumZoom = 0
    m_bDescargarFrm = False
    m_bActivado = False
    m_bPrimeraCargaEvolucion = True


    Chkzoomin.Visible = False
    cmdZoomOut.Visible = False
    ChkProveedores.Visible = False
    

    Me.Height = 6015
    Me.Width = 8920
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    Set g_oProcesoSeleccionado = g_oOrigen.m_oProcesoSeleccionado
    Set g_oAdjs = g_oOrigen.m_oAdjs
        
    CargarRecursos
    
    PonerFieldSeparator Me
    
    Me.caption = m_sCaption(g_iGrafico)
    
    CargarComboGrupos
        
    'Obtiene el n� de decimales a mostrar en esta pantalla
    aSymbols = oGestorParametros.DevolverSettingsSymbols
    
    If g_oOrigen.Name = "frmADJItem" Then
        If g_oOrigen.g_oVistaSeleccionada.DecResult > 0 Then
            m_sFormatoNumber = "#" & aSymbols(1) & "##0" & aSymbols(2) & "0"
        Else
            m_sFormatoNumber = "#" & aSymbols(1) & "##0"
        End If
        For i = 1 To g_oOrigen.g_oVistaSeleccionada.DecResult - 1
            m_sFormatoNumber = m_sFormatoNumber & "#"
        Next i
        
        Set g_oEstOfes = g_oOrigen.g_oOrigen.m_oEstOfes
        
    Else
        Select Case g_oOrigen.sstabComparativa.selectedItem.Tag
            Case "General"
                If g_oOrigen.m_oVistaSeleccionada.DecResult > 0 Then
                    m_sFormatoNumber = "#" & aSymbols(1) & "##0" & aSymbols(2) & "0"
                Else
                    m_sFormatoNumber = "#" & aSymbols(1) & "##0"
                End If
                For i = 1 To g_oOrigen.m_oVistaSeleccionada.DecResult - 1
                    m_sFormatoNumber = m_sFormatoNumber & "#"
                Next i
            Case "ALL"
                If g_oOrigen.m_oVistaSeleccionadaAll.DecResult > 0 Then
                    m_sFormatoNumber = "#" & aSymbols(1) & "##0" & aSymbols(2) & "0"
                Else
                    m_sFormatoNumber = "#" & aSymbols(1) & "##0"
                End If
                For i = 1 To g_oOrigen.m_oVistaSeleccionadaAll.DecResult - 1
                    m_sFormatoNumber = m_sFormatoNumber & "#"
                Next i
            Case Else
                If g_oOrigen.m_oVistaSeleccionadaGr.DecResult > 0 Then
                    m_sFormatoNumber = "#" & aSymbols(1) & "##0" & aSymbols(2) & "0"
                Else
                    m_sFormatoNumber = "#" & aSymbols(1) & "##0"
                End If
                For i = 1 To g_oOrigen.m_oVistaSeleccionadaGr.DecResult - 1
                    m_sFormatoNumber = m_sFormatoNumber & "#"
                Next i
        End Select
        
        Set g_oEstOfes = g_oOrigen.m_oEstOfes
    End If
    
    'Obtiene los proveedores con la misma descripci�n
    Dim oProveRep As CProveedor
    ReDim m_arProveRep(0)
    
    Dim iNumProves As Integer
    iNumProves = 0
    
    For Each oProve In g_oProvesAsig
        For Each oProveRep In g_oProvesAsig
            If oProve.Cod <> oProveRep.Cod Then
                If Mid(oProve.Den, 1, 17) = Mid(oProveRep.Den, 1, 17) Then
                    ReDim Preserve m_arProveRep(UBound(m_arProveRep) + 1)
                    m_arProveRep(UBound(m_arProveRep)) = oProve.Cod
                    Exit For
                End If
            End If
        Next
        'ReDim Preserve m_arProves(2, iNumProves)
        'm_arProves(0, iNumProves) = oProve.Cod
        'm_arProves(1, iNumProves) = oProve.Den
        'm_arProves(2, iNumProves) = 1 'visibilidad
        iNumProves = iNumProves + 1
    Next
    
    
    'Obtiene el gr�fico:
    Select Case g_iGrafico
        Case 1:
            ObtenerGraficoProceso
        Case 2:
            PicPrecios.Visible = False
            picAtributos.Visible = True
            If Not g_oOrigen.g_PonderacionCalculada Then
                g_oOrigen.CalcularPonderacionAtributosOfertados
            End If
            CargarGrid
        Case 3:
            PicPrecios.Visible = True
            picAtributos.Visible = False
            sdbcGrupo.Visible = True
            sdbcItem.Visible = True
            lblRango.Visible = False
            sdbcEscalado.Visible = False
            txtGrupo.Visible = False
            txtItem.Visible = False
            OptActual.Visible = True
            OptTotal.Visible = True
            ObtenerGraficoGrupoProveedor
        Case 4:
            PicPrecios.Visible = False
            picAtributos.Visible = True
            If Not g_oOrigen.g_PonderacionCalculada Then
                g_oOrigen.CalcularPonderacionAtributosOfertados
            End If
            CargarGrid
        Case 5:
            bHayEscalados = HayEscalados
            
            PicPrecios.Visible = True
            picAtributos.Visible = False
            OptActual.Visible = False
            OptTotal.Visible = False
            sdbcGrupo.Visible = True
            sdbcItem.Visible = True
            lblRango.Visible = bHayEscalados
            sdbcEscalado.Visible = bHayEscalados
            txtGrupo.Visible = False
            txtItem.Visible = False
            Set m_oIOfertas = g_oProcesoSeleccionado
            sdbcGrupo.Text = m_sProceso
            m_sCodGrupo = m_sProceso
            m_lItem = 0
            
            Chkzoomin.Visible = True
            cmdZoomOut.Visible = True
            ChkProveedores.Visible = True
                
            ObtenerGraficoProcesoEvolucion
        Case 6:
            PicPrecios.Visible = True
            picAtributos.Visible = False
            sdbcGrupo.Visible = False
            sdbcItem.Visible = False
            lblRango.Visible = False
            sdbcEscalado.Visible = False
            txtGrupo.Visible = True
            txtItem.Visible = True
            OptActual.Visible = True
            OptTotal.Visible = True
            If g_oOrigen.Name = "frmADJItem" Then
                ConfigurarDesdeItem (6)
            Else
                txtGrupo.Text = g_oOrigen.sstabComparativa.selectedItem
                If g_oOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
                    m_sCodGrupo = g_oOrigen.sdbgAll.Columns("GRUPO").Value
                    txtItem.Text = g_oOrigen.sdbgAll.Columns("DESCR").Value
                    m_lItem = g_oOrigen.sdbgAll.Columns("ID").Value
                Else
                    m_sCodGrupo = g_oOrigen.sstabComparativa.selectedItem.Tag
                    If g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UsarEscalados Then
                        txtItem.Text = g_oOrigen.ctlADJEsc.GetCurrentItemCodArt & " - " & g_oOrigen.ctlADJEsc.GetCurrentItemDesc
                        m_lItem = g_oOrigen.ctlADJEsc.GetCurrentItemCod
                    Else
                        txtItem.Text = g_oOrigen.sdbgAdj.Columns("DESCR").Value
                        m_lItem = g_oOrigen.sdbgAdj.Columns("ID").Value
                    End If
                End If
            End If
            ObtenerGraficoItem
        Case 7:
            PicPrecios.Visible = False
            picAtributos.Visible = True
            If g_oOrigen.Name = "frmADJItem" Then
                ConfigurarDesdeItem (7)
            Else
                If Not g_oOrigen.g_PonderacionCalculada Then
                    g_oOrigen.CalcularPonderacionAtributosOfertados
                End If
                If g_oOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
                    m_sCodGrupo = g_oOrigen.sdbgAll.Columns("GRUPO").Value
                    m_lItem = g_oOrigen.sdbgAll.Columns("ID").Value
                Else
                    m_sCodGrupo = g_oOrigen.sstabComparativa.selectedItem.Tag
                    m_lItem = g_oOrigen.sdbgAdj.Columns("ID").Value
                End If

            End If
            
            CargarGrid
        Case 8:
            PicPrecios.Visible = True
            picAtributos.Visible = False
            sdbcGrupo.Visible = False
            sdbcItem.Visible = False
            txtGrupo.Visible = True
            txtItem.Visible = True
            OptActual.Visible = False
            OptTotal.Visible = False
            lblRango.Visible = False
            sdbcEscalado.Visible = False
            Set m_oIOfertas = g_oProcesoSeleccionado
            If g_oOrigen.Name = "frmADJItem" Then
                m_sCodGrupo = g_oOrigen.g_ogrupo.Codigo
                ConfigurarDesdeItem (8)
            Else
                txtGrupo.Text = g_oOrigen.sstabComparativa.selectedItem
                If g_oOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
                    txtItem.Text = g_oOrigen.sdbgAll.Columns("DESCR").Value
                    m_sCodGrupo = g_oOrigen.sdbgAll.Columns("GRUPO").Value
                    m_lItem = g_oOrigen.sdbgAll.Columns("ID").Value
                Else
                    m_sCodGrupo = g_oOrigen.sstabComparativa.selectedItem.Tag
                    If g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UsarEscalados Then
                        lblRango.Visible = True
                        sdbcEscalado.Visible = True
                        
                        txtItem.Text = g_oOrigen.ctlADJEsc.GetCurrentItemCodArt & " - " & g_oOrigen.ctlADJEsc.GetCurrentItemDesc
                        m_lItem = g_oOrigen.ctlADJEsc.GetCurrentItemCod
                    Else
                        txtItem.Text = g_oOrigen.sdbgAdj.Columns("DESCR").Value
                        m_lItem = g_oOrigen.sdbgAdj.Columns("ID").Value
                    End If
                End If
            End If
            Chkzoomin.Visible = True
            cmdZoomOut.Visible = True
            ChkProveedores.Visible = True
            ObtenerGraficoProcesoEvolucion
        Case 9
            PicPrecios.Visible = True
            picAtributos.Visible = False
            sdbcGrupo.Visible = False
            txtGrupo.Visible = True
            OptActual.Visible = False
            OptTotal.Visible = False
            lblRango.Visible = False
            sdbcEscalado.Visible = False
            Set m_oIOfertas = g_oProcesoSeleccionado
                        
            If g_oOrigen.Name = "frmADJItem" Then
                sdbcItem.Visible = False
                txtItem.Visible = True
                ConfigurarDesdeItem (9)
            Else
                sdbcItem.Visible = True
                txtItem.Visible = False
                
                txtGrupo.Text = g_oOrigen.sstabComparativa.selectedItem
                m_sCodGrupo = g_oOrigen.sstabComparativa.selectedItem.Tag
                m_lItem = g_oOrigen.ctlADJEsc.GetCurrentItemCod
                m_bRespetarCombo = True
                sdbcItem.Text = g_oOrigen.ctlADJEsc.GetCurrentItemCodArt & " - " & g_oOrigen.ctlADJEsc.GetCurrentItemDesc
                m_bRespetarCombo = False
                m_bPrimeraCargaEvolucion = True
            End If
            Chkzoomin.Visible = True
            cmdZoomOut.Visible = True
            ChkProveedores.Visible = True
            m_bGraficoEsc = True
            ObtenerGraficoUltimaOfertaEsc
     End Select
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub ObtenerGraficoProceso()
    'Obtiene el gr�fico asociado al proceso e item seleccionados
    Dim i As Integer
    Dim j As Integer
    Dim oProve As CProveedor
    Dim dblImporte As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    PicPrecios.Visible = True
    picAtributos.Visible = False
    sdbcGrupo.Visible = True
    sdbcItem.Visible = True
    lblRango.Visible = False
    sdbcEscalado.Visible = False
    txtGrupo.Visible = False
    txtItem.Visible = False
    OptActual.Visible = True
    OptTotal.Visible = True
    
    sdbcGrupo.Text = m_sProceso
    
    'Precios por proceso / proveedor
    Me.caption = m_sCaption(g_iGrafico)
    
    ReDim m_ar(1 To m_iNumProv, 1 To 5)
    ReDim m_arLis(1 To m_iNumProv, 1 To 5)
    
    i = 1
    If OptActual.Value Then
        For Each oProve In g_oProvesAsig
                m_ar(i, 1) = Space(1) & DenominacionProve(oProve.Den, oProve.Cod) & Space(1)
                If g_oOrigen.sdbgTotalesProve.Rows > 0 Then
                    
                    m_ar(i, 2) = g_oOrigen.sdbgTotalesProve.Columns("Consumido" & oProve.Cod).Value
                    dblImporte = g_oOrigen.sdbgTotalesProve.Columns("Adjudicado" & oProve.Cod).Value
                    m_ar(i, 3) = AplicarAtributosOferta(oProve.Cod, dblImporte, False)
                    If g_oOrigen.sdbgTotalesProve.Columns("Ahorrado" & oProve.Cod).Value >= 0 Then
                         m_ar(i, 4) = g_oOrigen.sdbgTotalesProve.Columns("Ahorrado" & oProve.Cod).Value
                         m_ar(i, 5) = Null
                    Else
                         m_ar(i, 4) = Null
                         m_ar(i, 5) = g_oOrigen.sdbgTotalesProve.Columns("Ahorrado" & oProve.Cod).Value
                    End If
                    
                End If
                For j = 1 To 5
                    m_arLis(i, j) = m_ar(i, j)
                Next
                i = i + 1
        Next
    Else
        For Each oProve In g_oProvesAsig
                m_ar(i, 1) = Space(1) & DenominacionProve(oProve.Den, oProve.Cod) & Space(1)
                If g_oOrigen.sdbgTotalesProve.Rows > 0 Then
                    m_ar(i, 2) = StrToDbl0(g_oOrigen.sdbgTotalesProve.Columns("Importe" & oProve.Cod).Value) + StrToDbl0(g_oOrigen.sdbgTotalesProve.Columns("AhorroOferta" & oProve.Cod).Value)
                    dblImporte = g_oOrigen.sdbgTotalesProve.Columns("Importe" & oProve.Cod).Value
                    m_ar(i, 3) = AplicarAtributosOferta(oProve.Cod, dblImporte, True)
                    If g_oOrigen.sdbgTotalesProve.Columns("AhorroOferta" & oProve.Cod).Value >= 0 Then
                         m_ar(i, 4) = g_oOrigen.sdbgTotalesProve.Columns("AhorroOferta" & oProve.Cod).Value
                         m_ar(i, 5) = Null
                    Else
                         m_ar(i, 4) = Null
                         m_ar(i, 5) = g_oOrigen.sdbgTotalesProve.Columns("AhorroOferta" & oProve.Cod).Value
                    End If
                End If
                For j = 1 To 5
                    m_arLis(i, j) = m_ar(i, j)
                Next
                i = i + 1
        Next
        
    End If

    
    
    m_ar = ApilarValores(m_ar)
    GraficoPrecioProcesoProveedor
    
'    For i = 1 To MSChart1.Plot.SeriesCollection.Count
'        Debug.Print MSChart1.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.Text
'    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoProceso", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

Private Sub ObtenerGraficoGrupo()
    'Obtiene el gr�fico asociado al grupo e item seleccionados
    Dim i As Integer
    Dim j As Integer
    Dim oProve As CProveedor
    Dim sCod As String
    Dim m_aux1() As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    PicPrecios.Visible = True
    picAtributos.Visible = False

    'Precios por proceso / proveedor

     i = 1
     If sdbcItem.Text = "" Then
         
         For Each oProve In g_oProvesAsig
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sdbcGrupo.Columns(0).Value) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
                ReDim Preserve m_aux1(5, i)
                m_aux1(1, i) = Space(1) & DenominacionProve(oProve.Den, oProve.Cod) & Space(1)
                sCod = sdbcGrupo.Columns(0).Value & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If OptActual.Value Then
                    If Not g_oAdjs.Item(sCod) Is Nothing Then
                        m_aux1(2, i) = g_oAdjs.Item(sCod).Consumido / g_oAdjs.Item(sCod).Cambio
                        m_aux1(3, i) = g_oAdjs.Item(sCod).Adjudicado / g_oAdjs.Item(sCod).Cambio
                        If g_oAdjs.Item(sCod).Ahorrado >= 0 Then
                            m_aux1(4, i) = g_oAdjs.Item(sCod).Ahorrado / g_oAdjs.Item(sCod).Cambio
                            m_aux1(5, i) = Null
                        Else
                            m_aux1(4, i) = Null
                            m_aux1(5, i) = g_oAdjs.Item(sCod).Ahorrado / g_oAdjs.Item(sCod).Cambio
                        End If
                    Else
                        m_aux1(2, i) = 0
                        m_aux1(3, i) = 0
                        m_aux1(4, i) = 0
                        m_aux1(5, i) = 0
                    End If
                Else
                    If Not g_oAdjs.Item(sCod) Is Nothing Then
                        m_aux1(2, i) = NullToDbl0(g_oAdjs.Item(sCod).AbiertoOfe) / g_oAdjs.Item(sCod).Cambio
                        m_aux1(3, i) = g_oAdjs.Item(sCod).importe / g_oAdjs.Item(sCod).Cambio
                        m_aux1(4, i) = g_oAdjs.Item(sCod).AhorroOfe / g_oAdjs.Item(sCod).Cambio
                        If g_oAdjs.Item(sCod).AhorroOfe >= 0 Then
                            m_aux1(4, i) = g_oAdjs.Item(sCod).AhorroOfe / g_oAdjs.Item(sCod).Cambio
                            m_aux1(5, i) = Null
                        Else
                            m_aux1(4, i) = Null
                            m_aux1(5, i) = g_oAdjs.Item(sCod).AhorroOfe / g_oAdjs.Item(sCod).Cambio
                        End If
                    Else
                        m_aux1(2, i) = 0
                        m_aux1(3, i) = 0
                        m_aux1(4, i) = 0
                        m_aux1(5, i) = 0
                    End If
                End If
                i = i + 1
            End If
         Next
         ReDim m_ar(1 To i - 1, 1 To 5)
         ReDim m_arLis(1 To i - 1, 1 To 5)
         For i = 1 To UBound(m_aux1, 2)
            m_ar(i, 1) = m_aux1(1, i)
            m_ar(i, 2) = m_aux1(2, i)
            m_ar(i, 3) = m_aux1(3, i)
            m_ar(i, 4) = m_aux1(4, i)
            m_ar(i, 5) = m_aux1(5, i)
            For j = 1 To 5
                m_arLis(i, j) = m_ar(i, j)
            Next
         Next
         
    End If
    
    m_ar = ApilarValores(m_ar)

    GraficoPrecioProcesoProveedor
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoGrupo", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Genera el gr�fico para el item seleccionado</summary>
''' <remarks>Llamada desde: OptActual_Click,OptTotal_Click,Form_Load ;m�ximo: 0</remarks>
''' <revision>LTG 10/11/2011</revision>

Private Sub ObtenerGraficoItem()
    'Obtiene el gr�fico asociado al proceso o grupo e item seleccionados
    Dim oAsig As COferta
    Dim i As Integer
    Dim j As Integer
    Dim sCod As String
    Dim oItem As CItem
    Dim dporcentaje As Double
    Dim dImporte As Double
    Dim dCantidadAdj As Double
    Dim oProve As CProveedor
    Dim oGrupo As CGrupo
    Dim ar_aux() As Variant
    Dim oAdjsProveEsc As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oAdjTotProveEsc As CAdjudicacion

    'Comprobamos que hay un proceso y grupo seleccionado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProcesoSeleccionado Is Nothing Then
        Exit Sub
    End If
    
    If g_iGrafico = 1 Then
        Set oGrupo = g_oProcesoSeleccionado.grupos.Item(sdbcItem.Columns("CODGRUPO").Value)
        m_lIdGrupo = oGrupo.Id
        Set oItem = oGrupo.Items.Item(sdbcItem.Columns("ID").Value)
    Else
        If g_oOrigen.Name = "frmADJItem" Then
            Set oGrupo = g_oOrigen.g_ogrupo
            Set oItem = g_oOrigen.g_oItemSeleccionado
        Else
            If g_oOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
                Set oGrupo = g_oProcesoSeleccionado.grupos.Item(g_oOrigen.sdbgAll.Columns("GRUPO").Value)
                m_lIdGrupo = oGrupo.Id
                Set oItem = oGrupo.Items.Item(g_oOrigen.sdbgAll.Columns("ID").Value)
            Else
                Set oGrupo = g_oProcesoSeleccionado.grupos.Item(g_oOrigen.sstabComparativa.selectedItem.Tag)
                m_lIdGrupo = oGrupo.Id
                If oGrupo.UsarEscalados Then
                    Set oItem = oGrupo.Items.Item(g_oOrigen.ctlADJEsc.GetCurrentItemCod)
                Else
                    Set oItem = oGrupo.Items.Item(g_oOrigen.sdbgAdj.Columns("ID").Value)
                End If
            End If
        End If
    End If
    
    'Si hay escalados se calculan las adjudicaciones para los grupos con escalados
    If oGrupo.UsarEscalados Then
        Set oAdjsProveEsc = CalcularAdjudicacionesGruposEscalados(g_oProcesoSeleccionado, g_oProcesoSeleccionado.grupos, g_oProvesAsig, g_oOrigen.m_oAtribsFormulas)
    End If
    
    i = 1

    For Each oProve In g_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If g_oOrigen.Name = "frmADJItem" Then
                If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(oGrupo.Codigo) Is Nothing Then
                    bCargar = False
                End If
            Else
                If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(oGrupo.Codigo) Is Nothing Then
                    bCargar = False
                End If
            End If
        End If
        If bCargar Then
            ReDim Preserve ar_aux(5, i)
            ar_aux(1, i) = Space(1) & DenominacionProve(oProve.Den, oProve.Cod) & Space(1)
            If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                Set oAsig = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                
                'Carga las adjudicaciones
                sCod = oAsig.Prove & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAsig.Prove))
                sCod = CStr(oItem.Id) & sCod
                
                If oGrupo.UsarEscalados Then
                    Set oAdj = oAdjsProveEsc.Item(sCod)
                Else
                    Set oAdj = oGrupo.adjudicaciones.Item(sCod)
                End If
                
                dCantidadAdj = 0
                If Not oAdj Is Nothing Then
                    dporcentaje = oAdj.Porcentaje
            
                    If oGrupo.UsarEscalados Then
                        dCantidadAdj = oAdj.Adjudicado
                    Else
                        If dporcentaje <> 0 Then
                            dCantidadAdj = (dporcentaje / 100) * oItem.Cantidad
                        End If
                    End If
                End If
                
                'Va calculando el consumido y el preadjudicado del item con los valores de cada proveedor:
                
                If OptActual.Value Then
                    If oGrupo.UsarEscalados Then
                        If Not oAdj Is Nothing Then
                            ar_aux(2, i) = ConsumidoProveedor(oGrupo, oAdj.ProveCod, oItem.Id, oAdj.Adjudicado)
                        Else
                            ar_aux(2, i) = 0
                        End If
                    Else
                        ar_aux(2, i) = (oItem.Precio * dCantidadAdj) 'Consumido
                    End If
                    
                    'Rellena el importe adjudicado del proveedor sdbgAdj.Columns("IMPADJ" & oAsig.Prove)
                    If dCantidadAdj = 0 Then 'Adjudicado
                        ar_aux(3, i) = 0
                    Else
                        If oGrupo.UsarEscalados Then
                            dImporte = oAdj.ImporteAdjTot
                            ar_aux(3, i) = dImporte
                        Else
                            dImporte = (NullToDbl0(oAsig.Lineas.Item(CStr(oItem.Id)).PrecioOferta) * dCantidadAdj)
                            ar_aux(3, i) = AplicarAtributosItem(oAsig.Prove, oItem, dImporte, False) / oAsig.Cambio 'Al importe del item le aplico los atributos de total de item
                        End If
                    End If
                Else
                    If oGrupo.UsarEscalados Then
                        'Asignar todo al escalado con mejor oferta
                        'g_oOrigen.ImporteOptimoItemEsc oGrupo.TipoEscalados, oProve.Cod, oItem, oAsig, lIdEscalado, dImporte, dCantidadAdj
                        Set oAdjTotProveEsc = CalcularAdjTotalProveedorEsc(oProve.Cod, oItem.Id, g_oProcesoSeleccionado, oGrupo, oAsig, g_oProvesAsig, g_oOrigen.m_oAtribsFormulas)
                        If Not oAdjTotProveEsc Is Nothing Then
                            ar_aux(2, i) = oAdjTotProveEsc.importe
                            ar_aux(3, i) = oAdjTotProveEsc.ImporteAdj
                        End If
                    Else
                        If Not IsNull(oAsig.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                            ar_aux(2, i) = (oItem.Precio * oItem.Cantidad)
                        Else
                            ar_aux(2, i) = 0
                        End If
                        dImporte = oItem.Cantidad * (NullToDbl0(oAsig.Lineas.Item(CStr(oItem.Id)).PrecioOferta))
                        ar_aux(3, i) = AplicarAtributosItem(oAsig.Prove, oItem, dImporte, True) / oAsig.Cambio 'Al importe del item le aplico los atributos de total de item
                    End If
                End If
                If NullToDbl0(ar_aux(2, i)) - NullToDbl0(ar_aux(3, i)) >= 0 Then
                     ar_aux(4, i) = NullToDbl0(ar_aux(2, i)) - NullToDbl0(ar_aux(3, i))
                     ar_aux(5, i) = Null
                Else
                     ar_aux(4, i) = Null
                     ar_aux(5, i) = NullToDbl0(ar_aux(2, i)) - NullToDbl0(ar_aux(3, i))
                End If

                i = i + 1
            Else
                ar_aux(2, i) = 0
                ar_aux(3, i) = 0
                ar_aux(4, i) = 0
                ar_aux(5, i) = 0

                i = i + 1
            End If
        End If
    Next   'Pasa al siguiente proveedor
    ReDim m_ar(1 To i - 1, 1 To 5)
    ReDim m_arLis(1 To i - 1, 1 To 5)
    For i = 1 To UBound(ar_aux, 2)
        m_ar(i, 1) = ar_aux(1, i)
        m_ar(i, 2) = ar_aux(2, i)
        m_ar(i, 3) = ar_aux(3, i)
        m_ar(i, 4) = ar_aux(4, i)
        m_ar(i, 5) = ar_aux(5, i)
        For j = 1 To 5
            m_arLis(i, j) = m_ar(i, j)
        Next
    Next
    m_ar = ApilarValores(m_ar)
    
    GraficoPrecioProcesoProveedor

    Set oAdjsProveEsc = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoItem", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Indica si el proceso seleccionado tiene escalados</summary>
''' <returns>Booleano indicando si hay escalados o no</returns>
''' <remarks>Llamada desde: ObtenerGraficoItem</remarks>
''' <revision>LTG 10/11/2011</revision>

Private Function HayEscalados() As Boolean
    Dim oGrupo As CGrupo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    HayEscalados = False
    
    For Each oGrupo In g_oProcesoSeleccionado.grupos
        If oGrupo.UsarEscalados = 1 Then
            HayEscalados = True
            Exit For
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "HayEscalados", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Sub GraficoPrecioProcesoProveedor()
Dim lbl As MSChart20Lib.Label

 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    MSChart1.Visible = True
    MSChart3.Visible = True
    MSChart2.Visible = False
    
    MSChart1.chartType = VtChChartType2dBar
    MSChart1.Stacking = True

    MSChart1.SeriesType = VtChSeriesType2dBar
    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
         lbl.VtFont.Size = 12
         lbl.VtFont.Style = VtFontStyleBold
         lbl.Format = m_sFormatoNumber
    Next
     
    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
         lbl.VtFont.Size = 12
         lbl.VtFont.Style = VtFontStyleBold
         lbl.Format = m_sFormatoNumber
    Next
    
    'Consumido 255,255,168
    'Adjudicado 0,163,240
    'Ahorrado 162,214,158
    'Ahorro negativo 255,0,0
    
    MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 162, 214, 158
    MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
    MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 163, 240
    MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 255, 255, 168
    MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 0, 163, 240
    MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 255, 255, 168
    MSChart1.Plot.SeriesCollection.Item(7).DataPoints.Item(-1).Brush.FillColor.Set 162, 214, 158
    MSChart1.Plot.SeriesCollection.Item(8).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
    MSChart1.Plot.SeriesCollection.Item(9).DataPoints.Item(-1).Brush.FillColor.Set 255, 255, 168
    MSChart1.Plot.SeriesCollection.Item(10).DataPoints.Item(-1).Brush.FillColor.Set 0, 163, 240
     
    MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Marker.Style = VtMarkerStyleX
    MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(7).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(8).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(9).DataPoints.Item(-1).Marker.Visible = False
    MSChart1.Plot.SeriesCollection.Item(10).DataPoints.Item(-1).Marker.Visible = False
        
    MSChart1.Plot.SeriesCollection.Item(5).LegendText = ""
    MSChart1.Plot.SeriesCollection.Item(6).LegendText = ""
    MSChart1.Plot.SeriesCollection.Item(7).LegendText = ""
    MSChart1.Plot.SeriesCollection.Item(8).LegendText = ""
    MSChart1.Plot.SeriesCollection.Item(9).LegendText = ""
    MSChart1.Plot.SeriesCollection.Item(10).LegendText = ""
    
    MSChart3.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 162, 214, 158
    MSChart3.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
    MSChart3.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 163, 240
    MSChart3.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 255, 255, 168
    
    If OptActual Then
        MSChart1.Plot.SeriesCollection.Item(1).LegendText = m_sAhorr
        MSChart1.Plot.SeriesCollection.Item(2).LegendText = m_sAhorrNegativo
        MSChart1.Plot.SeriesCollection.Item(3).LegendText = m_sAdjud
        MSChart1.Plot.SeriesCollection.Item(4).LegendText = m_sConsu
        MSChart3.Plot.SeriesCollection.Item(1).LegendText = m_sAhorr
        MSChart3.Plot.SeriesCollection.Item(2).LegendText = m_sAhorrNegativo
        MSChart3.Plot.SeriesCollection.Item(3).LegendText = m_sAdjud
        MSChart3.Plot.SeriesCollection.Item(4).LegendText = m_sConsu
    Else
        MSChart1.Plot.SeriesCollection.Item(1).LegendText = m_sAhorr
        MSChart1.Plot.SeriesCollection.Item(2).LegendText = m_sAhorrNegativo
        MSChart1.Plot.SeriesCollection.Item(3).LegendText = m_sImporte
        MSChart1.Plot.SeriesCollection.Item(4).LegendText = m_sConsu
        MSChart3.Plot.SeriesCollection.Item(1).LegendText = m_sAhorr
        MSChart3.Plot.SeriesCollection.Item(2).LegendText = m_sAhorrNegativo
        MSChart3.Plot.SeriesCollection.Item(3).LegendText = m_sImporte
        MSChart3.Plot.SeriesCollection.Item(4).LegendText = m_sConsu
        
    End If
 
    TratarLabelsNumericas (1)
    
    MSChart1.ChartData = m_ar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "GraficoPrecioProcesoProveedor", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>Obtiene el gr�fico asociado a los grupos / proveedor</summary>
''' <remarks>Llamada desde: optActual_Click, optTotal_Click, Form_Load; m�ximo: 0</remarks>
''' <revision>LTG 14/11/2011</revision>

Private Sub ObtenerGraficoGrupoProveedor()
    Dim i As Integer
    Dim j As Integer
    Dim oProve As CProveedor
    Dim oGrupo As CGrupo
    Dim sCod As String
    Dim lbl As MSChart20Lib.Label
    Dim bMostrarGrupo As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    PicPrecios.Visible = True
    picAtributos.Visible = False
    
    'Precios por grupo / proveedor

     i = 1
     
     ReDim m_ar(1 To g_oProcesoSeleccionado.grupos.Count, 1 To m_iNumProv + 1)
     
     For Each oGrupo In g_oProcesoSeleccionado.grupos
        bMostrarGrupo = True
        If g_oProcesoSeleccionado.AdminPublica Then
            If g_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                bMostrarGrupo = False
            End If
        End If
        
        If bMostrarGrupo Then
            j = 1
            m_ar(i, j) = Space(1) & oGrupo.Codigo & Space(1)
            j = j + 1
            For Each oProve In g_oProvesAsig
                sCod = oGrupo.Codigo & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                
                If Not g_oAdjs.Item(sCod) Is Nothing Then
                    If g_oEstOfes.Item(g_oAdjs.Item(sCod).Estado).Comparable Then
                        If OptActual.Value Then
                            m_ar(i, j) = g_oAdjs.Item(sCod).Adjudicado / g_oAdjs.Item(sCod).Cambio
                        Else
                            m_ar(i, j) = g_oAdjs.Item(sCod).importe / g_oAdjs.Item(sCod).Cambio
                        End If
                    Else
                        m_ar(i, j) = 0
                    End If
                Else
                    m_ar(i, j) = 0
                End If
                
                j = j + 1
            Next
       End If
       i = i + 1
    Next

    sdbcGrupo.Visible = False
    sdbcItem.Visible = False
    
    MSChart1.Visible = True
    MSChart1.ShowLegend = True
    MSChart3.Visible = False
    
    MSChart3.Visible = False
    MSChart1.chartType = VtChChartType2dLine
    MSChart1.SeriesType = VtChSeriesType2dBar
    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
         lbl.VtFont.Size = 12
         lbl.VtFont.Style = VtFontStyleBold
         lbl.Format = m_sFormatoNumber
     Next
     
     For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
         lbl.VtFont.Size = 12
         lbl.VtFont.Style = VtFontStyleBold
         lbl.Format = m_sFormatoNumber
     Next
     
     MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Marker.Style = VtMarkerStyleX
   
   
     MSChart1.ChartData = m_ar
     
     TratarLabelsNumericas (3)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoGrupoProveedor", err, Erl, , m_bActivado)
        Exit Sub
    End If
    
End Sub

''' <summary>
''' Formatea las caracteristicas de las lineas que aparecen en el grafico
''' </summary>
''' <param name="Tip">Tipo del grafico</param>
''' <remarks>Llamada desde: sistema; chkLabels_Click; chkLabels2_ClickTiempo;ObtenerGraficoProcesoEvolucion;ObtenerGraficoProcesoEvolucion2;sstabGrafico_Click;GraficoPrecioProcesoProveedor m�ximo: 0</remarks>

Private Sub TratarLabelsNumericas(ByVal Tip As Integer)
Dim i As Integer
Dim oProve As CProveedor
 
Dim maslineas As Integer
Dim l As Integer
Dim provesnovisibles As Integer
 
Dim Ador As Ador.Recordset
Dim bIncompleto  As Boolean
Dim bCargar  As Boolean
 
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case Tip
'Case 1, 5, 6, 8
Case 1, 6, 8, 9
    For i = 1 To MSChart1.Plot.SeriesCollection.Count
        If chkLabels.Value = 0 And Tip <> 1 Then
            MSChart1.Plot.SeriesCollection.Item(i).SeriesMarker.Auto = False
            MSChart1.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.ValueFormat = m_sFormatoNumber
            MSChart1.Plot.SeriesCollection.Item(i).DataPoints(-1).Marker.Style = VtMarkerStyleSquare
            MSChart1.Plot.SeriesCollection.Item(i).DataPoints(-1).Marker.Size = 100
            MSChart1.Plot.SeriesCollection.Item(i).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeLeft
        Else
            MSChart1.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
            If Tip = 1 Then
                Me.chkLabels.Visible = False
                MSChart1.Plot.SeriesCollection.Item(i).ShowGuideLine(VtChAxisIdY) = True
            End If
            
        End If
    Next
Case 2, 4, 7
    For i = 1 To MSChart2.Plot.SeriesCollection.Count
        If chkLabels2.Value = 0 Then
            MSChart2.Plot.SeriesCollection.Item(i).SeriesMarker.Auto = False
            MSChart2.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.ValueFormat = m_sFormatoNumber
            MSChart2.Plot.SeriesCollection.Item(i).DataPoints(-1).Marker.Style = VtMarkerStyleSquare
            MSChart2.Plot.SeriesCollection.Item(i).DataPoints(-1).Marker.Size = 100
            MSChart2.Plot.SeriesCollection.Item(i).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeLeft
        Else
            MSChart2.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
        End If
    Next
Case 3
     i = 1
     For Each oProve In g_oProvesAsig
'        Select Case g_oOrigen.sstabComparativa.SelectedItem.Tag
'            Case "General"
'                bVisible = EsProveedorVisible("General", oProve.Cod)
'            Case "ALL"
'                bVisible = EsProveedorVisible("ALL", oProve.Cod)
'
'            Case Else
'                bVisible = EsProveedorVisible("GR", oProve.Cod)
'        End Select
'
'        If bVisible = True Then
            MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod)
            If chkLabels.Value = 0 Then
                MSChart1.Plot.SeriesCollection.Item(i).SeriesMarker.Auto = False
                MSChart1.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.ValueFormat = m_sFormatoNumber
                MSChart1.Plot.SeriesCollection.Item(i).DataPoints(-1).Marker.Style = VtMarkerStyleSquare
                MSChart1.Plot.SeriesCollection.Item(i).DataPoints(-1).Marker.Size = 100
                MSChart1.Plot.SeriesCollection.Item(i).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeLeft
            Else
                MSChart1.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
            End If
            
            i = i + 1
'        End If
     Next
     
Case 5
provesnovisibles = 0
    i = 1
        
    For Each oProve In g_oProvesAsig
        If oProve.VisibleEnGrafico Then
            If m_sCodGrupo = m_sProceso Then
                Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, , m_lItem, m_bSoloAbiertos)
                bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, , m_lItem, m_bSoloAbiertos)
                bCargar = True
                If gParametrosGenerales.gbProveGrupos And m_lItem <> 0 Then
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sdbcItem.Columns("CODGRUPO").Value) Is Nothing Then
                        bCargar = False
                    End If
                End If
    
            Else
                Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos)
                bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos)
                bCargar = True
                If gParametrosGenerales.gbProveGrupos Then
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If g_oOrigen.Name = "frmADJItem" Then
                        If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(m_sCodGrupo) Is Nothing Then
                            bCargar = False
                        End If
                    Else
                        If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(m_sCodGrupo) Is Nothing Then
                            bCargar = False
                        End If
                    End If
                End If
            End If

            If bCargar Then
                       
            
                If chkLabels.Value = 0 Then
                    MSChart1.Plot.SeriesCollection.Item(i - provesnovisibles).SeriesMarker.Auto = False
                    MSChart1.Plot.SeriesCollection.Item(i - provesnovisibles).DataPoints.Item(-1).DataPointLabel.ValueFormat = m_sFormatoNumber
                    MSChart1.Plot.SeriesCollection.Item(i - provesnovisibles).DataPoints(-1).Marker.Style = VtMarkerStyleSquare
                    MSChart1.Plot.SeriesCollection.Item(i - provesnovisibles).DataPoints(-1).Marker.Size = 100
                    MSChart1.Plot.SeriesCollection.Item(i - provesnovisibles).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeLeft
                    MSChart1.Plot.SeriesCollection.Item(i - provesnovisibles).DataPoints(-1).Marker.Pen.VtColor.Set m_ArColores(i, 0), m_ArColores(i, 1), m_ArColores(i, 2)
                    
                Else
                    MSChart1.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
                End If
                
                i = i + 1
            End If
        Else
        
            provesnovisibles = provesnovisibles + 1
            i = i + 1
        End If
    Next
    
    'Ahora hacemos presupuesto y objetivo si hay
    Set Ador = Nothing
    
    If m_iObjetivo > 0 And m_iPresupuesto > 0 Then
        maslineas = 2
    ElseIf m_iObjetivo > 0 Or m_iPresupuesto > 0 Then
        maslineas = 1
    Else
        maslineas = 0
    End If
        
    l = UBound(m_ArColores, 1) - provesnovisibles
    
    If maslineas = 2 Then
                
            If chkLabels.Value = 0 Then
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count - 1).SeriesMarker.Auto = False
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count - 1).DataPoints.Item(-1).DataPointLabel.ValueFormat = m_sFormatoNumber
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count - 1).DataPoints(-1).Marker.Style = VtMarkerStyleSquare
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count - 1).DataPoints(-1).Marker.Size = 100
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count - 1).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeLeft
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count - 1).DataPoints(-1).Marker.Pen.VtColor.Set m_ArColores(l + provesnovisibles - 1, 0), m_ArColores(l + provesnovisibles - 1, 1), m_ArColores(l + provesnovisibles - 1, 2)
                
            Else
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count - 1).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
            End If
    
            If chkLabels.Value = 0 Then
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).SeriesMarker.Auto = False
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints.Item(-1).DataPointLabel.ValueFormat = m_sFormatoNumber
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).Marker.Style = VtMarkerStyleSquare
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).Marker.Size = 100
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeLeft
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).Marker.Pen.VtColor.Set m_ArColores(l + provesnovisibles, 0), m_ArColores(l + provesnovisibles, 1), m_ArColores(l + provesnovisibles, 2)
                
            Else
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
            End If
    
            
            
    Else
        If maslineas = 1 Then
        
            If chkLabels.Value = 0 Then
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).SeriesMarker.Auto = False
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints.Item(-1).DataPointLabel.ValueFormat = m_sFormatoNumber
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).Marker.Style = VtMarkerStyleSquare
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).Marker.Size = 100
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).DataPointLabel.LocationType = VtChLabelLocationTypeLeft
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints(-1).Marker.Pen.VtColor.Set m_ArColores(l + provesnovisibles, 0), m_ArColores(l + provesnovisibles, 1), m_ArColores(l + provesnovisibles, 2)
                
            Else
                MSChart1.Plot.SeriesCollection.Item(MSChart1.Plot.SeriesCollection.Count).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
            End If
        
        End If
    
    End If
    
    
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "TratarLabelsNumericas", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub


Private Sub CargarItemsProceso()
Dim oItem As CItem
Dim oGrupo As CGrupo
Dim bMostrarGrupo As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcItem.RemoveAll
    
    For Each oGrupo In g_oProcesoSeleccionado.grupos
        bMostrarGrupo = True
        If g_oProcesoSeleccionado.AdminPublica = True Then
            If g_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                bMostrarGrupo = False
            End If
        End If
        If bMostrarGrupo = True Then
            For Each oItem In oGrupo.Items
                 'Muestra todos los items
                If oItem.Confirmado = True Then
                    sdbcItem.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.GrupoCod & Chr(m_lSeparador) & CStr(Year(oItem.FechaInicioSuministro)) & "/" & oItem.DestCod & Chr(m_lSeparador) & oItem.Id
                End If
            Next
        End If
    Next
    Set oGrupo = Nothing
    Set oItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "CargarItemsProceso", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Sub Form_Resize()
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub


Private Sub Arrange()
    'Redimensiona el formulario
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height <= 1800 Then Exit Sub
    If Me.Width <= 6200 Then Exit Sub
    
    PicPrecios.Height = Me.Height - 525
    PicPrecios.Width = Me.Width - 150
    sdbcGrupo.Width = PicPrecios.Width / 2 + 22
    sdbcItem.Left = sdbcGrupo.Width + 90
    sdbcItem.Width = PicPrecios.Width - sdbcItem.Left - 75
    lblRango.Left = sdbcGrupo.Width + 90
    lblRango.Width = (sdbcItem.Width / 4) - 90
    sdbcEscalado.Left = lblRango.Left + lblRango.Width + 90
    sdbcEscalado.Width = 3 * (sdbcItem.Width / 4)
    txtGrupo.Width = sdbcGrupo.Width
    txtItem.Left = sdbcItem.Left
    txtItem.Width = sdbcItem.Width
    
    MSChart1.Height = PicPrecios.Height - 660
    MSChart3.Height = PicPrecios.Height - 660
    If MSChart3.Visible Then
    MSChart1.Width = PicPrecios.Width - 105 - MSChart3.Width
    Else
    MSChart1.Width = PicPrecios.Width - 105
    End If
    
    MSChart3.Left = MSChart1.Left + MSChart1.Width
    
    
    picAtributos.Height = Me.Height - 405
    picAtributos.Width = Me.Width - 120
    
    sstabGrafico.Width = picAtributos.Width - 120
    sstabGrafico.Height = picAtributos.Height - 120
    sdbgAtributos.Width = picAtributos.Width - 450
    sdbgAtributos.Height = picAtributos.Height - 600
    sdbgAtributos.Columns(0).Width = sdbgAtributos.Width * 0.1
    sdbgAtributos.Columns(1).Width = sdbgAtributos.Width * 0.15
    sdbgAtributos.Columns(2).Width = sdbgAtributos.Width * 0.55
    sdbgAtributos.Columns(3).Width = sdbgAtributos.Width * 0.2
    MSChart2.Height = picAtributos.Height - 900
    MSChart2.Width = picAtributos.Width - 285
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oProcesoSeleccionado = Nothing
    Set g_oProvesAsig = Nothing
    Set m_oIOfertas = Nothing
    Set g_oAdjs = Nothing
    Set m_oGrupoSeleccionado = Nothing
    g_oOrigen.PopupDescargarGrafico
    Set g_oOrigen = Nothing
    Set g_oEstOfes = Nothing
    Me.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub MSChart1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bzoomin Then
    
        If Button = vbLeftButton Then

        picLupa.Width = 1

        picLupa.Height = 1

        picLupa.Top = MSChart1.Top + Y

        picLupa.Left = MSChart1.Left + X

        picLupa.Visible = True

        m_xini = X

        m_yini = Y

        End If
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "MSChart1_MouseDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub MSChart1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
   
   Dim ValorMinimo As Double
   Dim ValorMaximo As Double
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   picProveedores.Visible = False
   ChkProveedores.Value = 0
   
   If m_bCambioProveedor Then
        m_bCambioProveedor = False
        
        m_inumZoom = UBound(m_arZoom, 2)
        ValorMinimo = m_arZoom(0, m_inumZoom)
        ValorMaximo = m_arZoom(1, m_inumZoom)
        
        If m_bGraficoEsc Then
            ObtenerGraficoUltimaOfertaEscZoom ValorMinimo, ValorMaximo, m_Divisiones, True
        Else
            ObtenerGraficoProcesoEvolucion2 ValorMinimo, ValorMaximo, m_Divisiones, True
        End If
        
        Exit Sub

   End If
   
    If X > 2500 Then
        If m_bzoomin Then
        MSChart1.MousePointer = VtMousePointerCross
        Else
            MSChart1.MousePointer = VtMousePointerDefault
        End If
    Else
        MSChart1.MousePointer = VtMousePointerDefault
    End If
    
    If Button = vbLeftButton And picLupa.Visible Then

            If (Abs(m_xAntes - X) > 100 Or Abs(m_yAntes - Y) > 100) Then

                m_xAntes = X
                m_yAntes = Y
                
                picLupa.Width = Abs(m_xini - X)

                picLupa.Height = Abs(m_yini - Y)

                If X < m_xini Then picLupa.Left = MSChart1.Left + X

                If Y < m_yini Then picLupa.Top = MSChart1.Top + Y

                DoEvents
            End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "MSChart1_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub MSChart1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

Dim iarriba As Double
Dim iAbajo As Double

Dim ValorMaximo As Double
Dim ValorMinimo As Double

Dim bredimension As Boolean
Dim iAlturaGrafico As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Button = vbLeftButton And picLupa.Visible Then

        picLupa.Visible = False
    

        If Not m_bPrimeraVez Then
            m_bPrimeraVez = True
            bredimension = True
            m_EscalaMax = MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum
            m_EscalaMin = MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum
            m_Divisiones = MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MajorDivision
        
            'ReDim Preserve m_arZoom(1, 1)
            'm_inumZoom = 1
        Else
            'm_inumZoom = UBound(m_arZoom, 0) + 1
            'ReDim Preserve m_arZoom(m_inumZoom, 1)
        
        End If
    
    
        iAlturaGrafico = MSChart1.Height - 660 - 1290
               
        If m_yini > Y Then
                   
            iAbajo = ((MSChart1.Height - 660 - m_yini) * (MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum - MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum)) / (MSChart1.Height - 660 - 1290) + MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum
            iarriba = ((MSChart1.Height - 660 - Y) * (MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum - MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum)) / (MSChart1.Height - 660 - 1290) + MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum
            
        Else
            iAbajo = ((MSChart1.Height - 660 - Y) * (MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum - MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum)) / (MSChart1.Height - 660 - 1290) + MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum
            iarriba = ((MSChart1.Height - 660 - m_yini) * (MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum - MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum)) / (MSChart1.Height - 660 - 1290) + MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum
        
        End If
    
        
        If Shift = 0 Then
        
                    
            If Y = m_yini Then Exit Sub
            
            If iarriba > m_EscalaMax Then
                ValorMaximo = m_EscalaMax
            Else
                ValorMaximo = iarriba
            End If
    
            If iarriba < m_EscalaMin Then
                ValorMinimo = m_EscalaMin
            Else
                ValorMinimo = iAbajo
            End If
                
            'If bredimension Then
            '    ReDim Preserve m_arZoom(1, 1)
            '    m_inumZoom = 1
            'Else
                m_inumZoom = UBound(m_arZoom, 2) + 1
                ReDim Preserve m_arZoom(1, m_inumZoom)
            'End If
                        
            m_arZoom(0, m_inumZoom) = ValorMinimo
            m_arZoom(1, m_inumZoom) = ValorMaximo
            
            If m_bGraficoEsc Then
                ObtenerGraficoUltimaOfertaEscZoom ValorMinimo, ValorMaximo, m_Divisiones
            Else
                ObtenerGraficoProcesoEvolucion2 ValorMinimo, ValorMaximo, m_Divisiones
            End If
            cmdZoomOut.Enabled = True
        'Else
        '    ObtenerGraficoProcesoEvolucion
        End If
        

    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "MSChart1_MouseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub OptActual_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_iGrafico = 1 Then
        If sdbcGrupo.Value = m_sProceso Then
            If sdbcItem.Value = "" Then
                ObtenerGraficoProceso
            Else
                ObtenerGraficoItem
            End If
        Else
            If sdbcItem.Value = "" Then
                ObtenerGraficoGrupo
            Else
                ObtenerGraficoItem
            End If
        End If
    Else
        If g_iGrafico = 6 Then
            ObtenerGraficoItem
        Else
            ObtenerGraficoGrupoProveedor
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "OptActual_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub OptTotal_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_iGrafico = 1 Then
        If sdbcGrupo.Value = m_sProceso Then
            If sdbcItem.Value = "" Then
                ObtenerGraficoProceso
            Else
                ObtenerGraficoItem
            End If
        Else
            If sdbcItem.Value = "" Then
                ObtenerGraficoGrupo
            Else
                ObtenerGraficoItem
            End If
        End If
    Else
        If g_iGrafico = 6 Then
            ObtenerGraficoItem
        Else
            ObtenerGraficoGrupoProveedor
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "OptTotal_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupo_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupo.Value = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcGrupo_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGrupo.Columns(0).Value = "************" Then
        m_sCodGrupo = m_sProceso
        m_lItem = 0
        sdbcGrupo.Text = sdbcGrupo.Columns(1).Value
        m_bRespetarCombo = True
         sdbcItem.Value = ""
        m_bRespetarCombo = False
                
        lblRango.Visible = False
        sdbcEscalado.Visible = False
        
        If g_iGrafico = 1 Then
            ObtenerGraficoProceso
        Else
            If Not HayEscalados Then
                ObtenerGraficoProcesoEvolucion
            End If
        End If
    Else
        m_sCodGrupo = sdbcGrupo.Columns(0).Value
        m_lItem = 0
        sdbcGrupo.Text = sdbcGrupo.Columns(0).Value
        If sdbcGrupo.Columns(1).Value <> "" Then
            sdbcGrupo.Text = sdbcGrupo.Columns(0).Value & " - " & sdbcGrupo.Columns(1).Value
        End If
        m_bRespetarCombo = True
        sdbcItem.Value = ""
        m_bRespetarCombo = False
           
        Set m_oGrupoSeleccionado = g_oProcesoSeleccionado.grupos.Item(sdbcGrupo.Columns(0).Value)
        
        If m_oGrupoSeleccionado.UsarEscalados Then
            lblRango.Visible = True
            sdbcEscalado.Visible = True
            
            m_bRespetarCombo = True
            sdbcEscalado.Value = ""
            m_bRespetarCombo = False
        Else
            lblRango.Visible = False
            sdbcEscalado.Visible = False
            
            If g_iGrafico = 1 Then
                ObtenerGraficoGrupo
            Else
                ObtenerGraficoProcesoEvolucion
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcGrupo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbcGrupo_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcGrupo.DataFieldList = "Column 0"
    sdbcGrupo.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcGrupo_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcGrupo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    sdbcGrupo.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcGrupo.Rows - 1
            bm = sdbcGrupo.GetBookmark(i)
            If UCase(sdbcGrupo.Text) = UCase(Mid(sdbcGrupo.Columns(0).CellText(bm), 1, Len(sdbcGrupo.Text))) Or sdbcGrupo.Text = m_sProceso Then
                sdbcGrupo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcGrupo_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub


Private Sub sdbcItem_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarCombo Then
        m_lItem = 0
        If sdbcItem.Text = "" Then
            If g_iGrafico = 1 Then
                ObtenerGraficoProceso
            Else
                ObtenerGraficoProcesoEvolucion
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcItem_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcItem_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcItem.DroppedDown Then
        sdbcItem = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcItem_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcItem_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcItem.Value = "..." Or sdbcItem.Value = "" Then
        sdbcItem.Text = ""
        m_lItem = 0
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    If sdbcItem.Columns(0).Text <> "" Then
        sdbcItem.Text = sdbcItem.Columns(0).Text & " - " & sdbcItem.Columns(1).Text
    Else
        sdbcItem.Text = sdbcItem.Columns(1).Text
    End If
    m_lItem = sdbcItem.Columns("ID").Value
    m_bRespetarCombo = False
    
    If sdbcGrupo.Text = m_sProceso Then
        If g_iGrafico = 9 Then
            lblRango.Visible = False
            sdbcEscalado.Visible = False
            ObtenerGraficoUltimaOfertaEsc
        Else
            If g_oProcesoSeleccionado.grupos.Item(sdbcItem.Columns("CODGRUPO").Value).UsarEscalados Then
                lblRango.Visible = True
                sdbcEscalado.Visible = True
                
                m_bRespetarCombo = True
                sdbcEscalado.Value = ""
                m_bRespetarCombo = False
            Else
                lblRango.Visible = False
                sdbcEscalado.Visible = False
            End If
        End If
    End If
    
    If g_iGrafico = 1 Then
         ObtenerGraficoItem
    Else
        If g_iGrafico = 9 Then
            ObtenerGraficoUltimaOfertaEsc
        Else
            ObtenerGraficoProcesoEvolucion
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcItem_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbcItem_DropDown()
    Dim oItem As CItem
    Dim sCodGrupo As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcItem.RemoveAll
    m_lItem = 0
    If sdbcGrupo.Text = m_sProceso Then
        CargarItemsProceso
    Else
        Screen.MousePointer = vbHourglass
'        If g_oProcesoSeleccionado.Grupos.Item(sdbcGrupo.Columns(0).Value).Items Is Nothing Then
'            'Carga los items
'            g_oProcesoSeleccionado.Grupos.Item(sdbcGrupo.Columns(0).Value).CargarTodosLosItems OrdItemPorCodArt, , , , , True
'            'Carga las adjudicaciones
'            g_oProcesoSeleccionado.Grupos.Item(sdbcGrupo.Columns(0).Value).CargarAdjudicaciones
'        End If

        If m_sCodGrupo = m_sProceso Then
            sCodGrupo = sdbcItem.Columns("CODGRUPO").Value
        Else
            sCodGrupo = m_sCodGrupo
        End If

        For Each oItem In g_oProcesoSeleccionado.grupos.Item(sCodGrupo).Items
            If oItem.Confirmado = True Then
                sdbcItem.AddItem oItem.ArticuloCod & Chr(m_lSeparador) & oItem.Descr & Chr(m_lSeparador) & oItem.GrupoCod & Chr(m_lSeparador) & CStr(Year(oItem.FechaInicioSuministro)) & "/" & oItem.DestCod & Chr(m_lSeparador) & oItem.Id
            End If
        Next
        
        If g_oProcesoSeleccionado.grupos.Item(sdbcGrupo.Columns(0).Value).Items.Count = 0 Then
            sdbcItem.AddItem "..."
        End If
    End If
    
    Set oItem = Nothing
    
    sdbcItem.SelStart = 0
    sdbcItem.Refresh
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcItem_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub sdbcItem_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcItem.DataFieldList = "Column 1"
    sdbcItem.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcItem_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub ObtenerInforme()
    'Generar el informe del gr�fico
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim Table As CRAXDRT.DatabaseTable

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If crs_Connected = False Then
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
            Set oReport = Nothing
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    Select Case g_iGrafico
    Case 5, 8  'Grafico de evoluci�n
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptADJGraficoEvol.rpt"
    Case 1, 6  'Precios por proceso o item
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptADJGraficoProceProve.rpt"
    Case 3 'Precios por grupo
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptADJGraficoGrupoProve.rpt"
    Case 2, 7 'Puntos por proceso, item
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptADJGraficoPuntos.rpt"
    Case 4 'Puntos por grupo
        RepPath = gParametrosInstalacion.gsRPTPATH & "\rptADJGraficoPuntosGrupo.rpt"
        
    End Select
        
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    For Each Table In oReport.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
        
    SelectionText = m_sProceso & ": " & g_oProcesoSeleccionado.Anyo & "/" & g_oProcesoSeleccionado.GMN1Cod & "/" & CStr(g_oProcesoSeleccionado.Cod)
    
    Select Case g_iGrafico
    Case 5, 8  'Grafico de evoluci�n
        PrepararListadoEvolucion oReport, SelectionText
        
    Case 1, 6
        PrepararListadoPreciosProceso oReport, SelectionText
            
    Case 3
        PrepararListadoPreciosGrupo oReport, SelectionText
    Case 2, 7
        PrepararListadoPuntosProceso oReport, SelectionText
    Case 4
        PrepararListadoPuntosGrupo oReport, SelectionText
    
    End Select
    
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
   'Me.Hide
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.caption = m_sCaption(g_iGrafico)
    pv.crViewer.ReportSource = oReport
    pv.crViewer.ViewReport
    pv.Show
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerInforme", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub CargarComboGrupos()
Dim oGrupo As CGrupo
Dim bMostrarGrupo As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProcesoSeleccionado Is Nothing Then Exit Sub
    
    sdbcGrupo.RemoveAll
    
    For Each oGrupo In g_oProcesoSeleccionado.grupos
        bMostrarGrupo = True
        If g_oProcesoSeleccionado.AdminPublica = True Then
            If g_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                bMostrarGrupo = False
            End If
        End If
        
        If bMostrarGrupo = True Then
            sdbcGrupo.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
        End If
    Next
    sdbcGrupo.AddItem "************" & Chr(m_lSeparador) & m_sProceso

    Set oGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "CargarComboGrupos", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub sdbcItem_PositionList(ByVal Text As String)
 ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcItem.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcItem.Rows - 1
            bm = sdbcItem.GetBookmark(i)
            If UCase(sdbcItem.Text) = UCase(Mid(sdbcItem.Columns(0).CellText(bm), 1, Len(sdbcItem.Text))) Then
                sdbcItem.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbcItem_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>Obtiene el objetivo y el presupuesto</summary>
''' <remarks>Llamada desde: frmADJGraficos m�ximo: 0</remarks>
''' <revision>LTG 17/11/2011</revision>

Private Sub ObtenerPresYObjetivo()
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim bUsarEscalados As Boolean
    Dim vPrecio As Variant
    Dim vObjetivo As Variant
    Dim vCantidad As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcGrupo.Text <> m_sProceso Then
        bUsarEscalados = (g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UsarEscalados = 1)
    Else
        If sdbcItem.Columns("CODGRUPO").Value <> vbNullString Then
            bUsarEscalados = (g_oProcesoSeleccionado.grupos.Item(sdbcItem.Columns("CODGRUPO").Value).UsarEscalados = 1)
        End If
    End If
    
    m_iPresupuesto = 0
    m_iObjetivo = 0
    If m_lItem <> 0 Then
        'Obtiene el presupuesto y objetivo para el item
        If m_sCodGrupo = m_sProceso Then
            Set oGrupo = g_oProcesoSeleccionado.grupos.Item(sdbcItem.Columns("CODGRUPO").Value)
        Else
            Set oGrupo = g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo)
        End If
        Set oItem = oGrupo.Items.Item(CStr(m_lItem))
                
        vCantidad = oItem.Cantidad
                
        m_lIdGrupo = oItem.GrupoID
        If bUsarEscalados And sdbcEscalado.Columns("ID").Value <> vbNullString Then
            If sdbcEscalado.Text <> vbNullString Then
                PresupuestoYObjetivoItemConEscalados oGrupo, oItem, g_oProvesAsig, vPrecio, vObjetivo, CLng(sdbcEscalado.Columns("ID").Value)
            Else
                PresupuestoYObjetivoItemConEscalados oGrupo, oItem, g_oProvesAsig, vPrecio, vObjetivo
            End If
            m_iPresupuesto = NullToDbl0(vPrecio) * NullToDbl0(vCantidad)
            m_iObjetivo = NullToDbl0(vObjetivo) * NullToDbl0(vCantidad)
        Else
            m_iPresupuesto = NullToDbl0(oItem.Precio) * NullToDbl0(oItem.Cantidad)
            m_iObjetivo = NullToDbl0(oItem.Objetivo) * NullToDbl0(oItem.Cantidad)
        End If
    Else
        If m_sCodGrupo = m_sProceso Then
            'Obtiene el presupuesto y objetivo para el proceso
            For Each oGrupo In g_oProcesoSeleccionado.grupos
                For Each oItem In oGrupo.Items
                    If oItem.Confirmado Then
                        If oGrupo.UsarEscalados Then
                            vCantidad = oItem.Cantidad
                            
                            PresupuestoYObjetivoItemConEscalados oGrupo, oItem, g_oProvesAsig, vPrecio, vObjetivo
            
                            m_iPresupuesto = m_iPresupuesto + (NullToDbl0(vPrecio) * NullToDbl0(vCantidad))
                            m_iObjetivo = m_iObjetivo + (NullToDbl0(vObjetivo) * NullToDbl0(vCantidad))
                        Else
                            m_iPresupuesto = m_iPresupuesto + (NullToDbl0(oItem.Precio) * NullToDbl0(oItem.Cantidad))
                            m_iObjetivo = m_iObjetivo + (NullToDbl0(oItem.Objetivo) * NullToDbl0(oItem.Cantidad))
                        End If
                    End If
                Next
            Next
        Else
            'Obtiene el presupuesto y objetivo para el grupo
            Set oGrupo = g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo)
            For Each oItem In oGrupo.Items
                If oItem.Confirmado Then
                    If oGrupo.UsarEscalados Then
                        vCantidad = oItem.Cantidad
                        
                        PresupuestoYObjetivoItemConEscalados oGrupo, oItem, g_oProvesAsig, vPrecio, vObjetivo
        
                        m_iPresupuesto = m_iPresupuesto + (NullToDbl0(vPrecio) * NullToDbl0(vCantidad))
                        m_iObjetivo = m_iObjetivo + (NullToDbl0(vObjetivo) * NullToDbl0(vCantidad))
                    Else
                        m_iPresupuesto = m_iPresupuesto + (NullToDbl0(oItem.Precio) * NullToDbl0(oItem.Cantidad))
                        m_iObjetivo = m_iObjetivo + (NullToDbl0(oItem.Objetivo) * NullToDbl0(oItem.Cantidad))
                    End If
                End If
            Next
            m_lIdGrupo = oGrupo.Id
        End If
    End If
        
    Set oGrupo = Nothing
    Set oItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerPresYObjetivo", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>Genera el grafico de evolucion de ofertas por proceso e item</summary>
''' <remarks>Llamada desde: frmADJGraficos m�ximo: 0</remarks>
''' <revision>LTG 14/11/2011</revision>

Public Sub ObtenerGraficoProcesoEvolucion()
    Dim lbl As MSChart20Lib.Label
    Dim i As Integer
    Dim Ador As Ador.Recordset
    Dim intNumOfertas As Integer
    Dim bIncompleto As Boolean
    Dim j As Integer
    Dim k As Integer
    Dim oProve As CProveedor
    Dim iNumOfe As Long
    Dim dblPrec As Double
    Dim dblPrecio As Double
    Dim lMax As Double
    Dim lMin As Double
    Dim bUsarEscalados As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bGraficoEsc = False
    
    lMax = 0
    lMin = 0
    
    Dim bPrimeraVez As Boolean
    bPrimeraVez = True
                
    PicPrecios.Visible = True
    picAtributos.Visible = False
    
    ObtenerPresYObjetivo
    
    MSChart1.ShowLegend = True
    MSChart3.Visible = False
    
    If m_oIOfertas Is Nothing Then Exit Sub
    'Obtiene el n�mero de ofertas para el proceso
    intNumOfertas = m_oIOfertas.DevolverNumOfertasDelProceso(m_lItem)
    
    If intNumOfertas = 0 Then
        'No existen ofertas.Sacar un mensaje indic�ndolo
        If m_lItem <> 0 Then
            oMensajes.NoExistenOfertasProc 47
        ElseIf m_sCodGrupo <> m_sProceso Then
            oMensajes.NoExistenOfertasProc 120
        Else
            oMensajes.NoExistenOfertasProc 44
        End If
        Exit Sub
    End If
    
    If g_oProvesAsig.Count = 0 Then
        'No existen ofertas.Sacar un mensaje indic�ndolo
        If m_lItem <> 0 Then
            oMensajes.NoExistenOfertasProc 47
        ElseIf m_sCodGrupo <> m_sProceso Then
            oMensajes.NoExistenOfertasProc 120
        Else
            oMensajes.NoExistenOfertasProc 44
        End If
        Exit Sub
    End If
    
    Dim iUbo As Integer
    MSChart1.chartType = VtChChartType2dLine
    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    If m_iObjetivo > 0 And m_iPresupuesto > 0 Then
        ReDim m_arEvol(1 To intNumOfertas, 3)
        MSChart1.ColumnCount = m_iNumProv + 2
        iUbo = 3
    ElseIf m_iObjetivo > 0 Or m_iPresupuesto > 0 Then
        ReDim m_arEvol(1 To intNumOfertas, 2)
        MSChart1.ColumnCount = m_iNumProv + 1
        iUbo = 2
    Else
        ReDim m_arEvol(1 To intNumOfertas, 1)
        MSChart1.ColumnCount = m_iNumProv
        iUbo = 1
    End If
    
    i = 1
    While i <= intNumOfertas
        m_arEvol(i, 1) = m_sOferta & i
        i = i + 1
    Wend
        
    'Para cada proveedor
    i = 1
    Dim icolores As Integer
    icolores = 1
    
    Dim maslineas As Integer
    
    If m_iObjetivo > 0 And m_iPresupuesto > 0 Then
        maslineas = 2
    ElseIf m_iObjetivo > 0 Or m_iPresupuesto > 0 Then
        maslineas = 1
    Else
        maslineas = 0
    End If
    
    If m_bPrimeraCargaEvolucion Then
        Erase m_ArColores
        ReDim m_ArColores(g_oProvesAsig.Count + maslineas, 2)
    Else
        If UBound(m_ArColores, 1) < g_oProvesAsig.Count + maslineas Then
            ReDim Preserve m_ArColores(g_oProvesAsig.Count + maslineas, 2)
        End If
    End If
    ReDim Preserve m_ArProveVisibles(g_oProvesAsig.Count)
    Dim iCarga As Integer
    
    If m_bPrimeraCargaEvolucion Then
        For iCarga = 1 To g_oProvesAsig.Count
        m_ArProveVisibles(iCarga) = 1
        iCarga = iCarga + 1
        Next
    End If
    
    If sdbcGrupo.Text <> m_sProceso Then
        bUsarEscalados = (g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UsarEscalados = 1)
    Else
        If sdbcItem.Columns("CODGRUPO").Value <> vbNullString Then bUsarEscalados = (g_oProcesoSeleccionado.grupos.Item(sdbcItem.Columns("CODGRUPO").Value).UsarEscalados = 1)
    End If
    
    For Each oProve In g_oProvesAsig
        
        If oProve.VisibleEnGrafico Then
            
            bIncompleto = False
            'Obtiene las ofertas para cada proveedor
            If m_sCodGrupo = m_sProceso Then
                If bUsarEscalados And sdbcEscalado.Columns("ID").Value <> vbNullString Then
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, , m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, , m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                Else
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, , m_lItem, m_bSoloAbiertos)
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, , m_lItem, m_bSoloAbiertos)
                End If
                bCargar = True
                If gParametrosGenerales.gbProveGrupos And m_lItem <> 0 Then
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sdbcItem.Columns("CODGRUPO").Value) Is Nothing Then
                        bCargar = False
                    End If
                End If
    
            Else
                If bUsarEscalados And sdbcEscalado.Columns("ID").Value <> vbNullString Then
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                Else
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos)
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos)
                End If
                bCargar = True
                If gParametrosGenerales.gbProveGrupos Then
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If g_oOrigen.Name = "frmADJItem" Then
                        If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(m_sCodGrupo) Is Nothing Then
                            bCargar = False
                        End If
                    Else
                        If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(m_sCodGrupo) Is Nothing Then
                            bCargar = False
                        End If
                    End If
                End If
            End If
            'Las cantidades est�n ya cambiadas a moneda del proceso
            If bCargar Then
                ReDim Preserve m_arEvol(1 To intNumOfertas, iUbo + i)
                
                If Not Ador.EOF Then
                
                    iNumOfe = Ador("NUM").Value
                    dblPrecio = 0
                    While Not Ador.EOF
                        dblPrec = NullToDbl0(Ador("PREC_VALIDO").Value)
                        
                        If iNumOfe <> Ador("NUM").Value Then
                            'If dblPrecio <> 0 Then
                                m_arEvol(iNumOfe, i + 1) = dblPrecio
                                
                                If bPrimeraVez Then
                                    lMax = dblPrecio
                                    lMin = dblPrecio
                                    bPrimeraVez = False
                                Else
                                
                                    If dblPrecio > lMax Then
                                        lMax = dblPrecio
                                    End If
                                    
                                    If dblPrecio < lMin Then
                                        lMin = dblPrecio
                                    End If
                                    
                                End If
                            'End If
                            dblPrecio = dblPrec
                            iNumOfe = Ador("NUM").Value
                        Else
                        
                            If bPrimeraVez Then
                                lMax = dblPrec
                                lMin = dblPrec
                                bPrimeraVez = False
                            Else
                            
                                If dblPrec > lMax Then
                                    lMax = dblPrec
                                End If
                                
                                If dblPrec < lMin Then
                                    lMin = dblPrec
                                End If
                                
                            End If
                        
                            dblPrecio = dblPrecio + dblPrec
                        End If
                        Ador.MoveNext
                    Wend
                    'If dblPrecio <> 0 Then
                        m_arEvol(iNumOfe, i + 1) = dblPrecio 'Es la ultima del while
                        
                        If dblPrecio > lMax Then
                            lMax = dblPrecio
                        End If
                        
                        If dblPrecio < lMin Then
                            lMin = dblPrecio
                        End If
                        
                    'End If
                
                    Ador.Close
                    Set Ador = Nothing
                End If
            
                'Leyenda de los proveedores
                If bIncompleto = True Then
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod) & " " & m_sIncompleto
                Else
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod)
                End If
                            
                If m_bPrimeraCargaEvolucion Then
                    m_ArColores(i, 0) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red
                    m_ArColores(i, 1) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green
                    m_ArColores(i, 2) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue
                Else
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red = m_ArColores(icolores, 0)
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green = m_ArColores(icolores, 1)
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue = m_ArColores(icolores, 2)
                End If
                                                                    
                i = i + 1
                icolores = icolores + 1
            End If
        Else
            icolores = icolores + 1
        End If
    Next
    
    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    If m_iObjetivo > 0 And m_iPresupuesto > 0 Then
        MSChart1.ColumnCount = i - 1 + 2
    ElseIf m_iObjetivo > 0 Or m_iPresupuesto > 0 Then
        MSChart1.ColumnCount = i - 1 + 1
    Else
        MSChart1.ColumnCount = i - 1
    End If
    
    Dim l As Integer
    
    For j = 1 To intNumOfertas
        'Objetivo
        If m_iObjetivo > 0 Then
            m_arEvol(j, i + 1) = m_iObjetivo
            MSChart1.Plot.SeriesCollection.Item(i).LegendText = m_sObjetivo
            
            l = UBound(m_ArColores, 1)
            
            If m_bPrimeraCargaEvolucion Then
            
                If m_iPresupuesto > 0 Then
                    m_ArColores(l - 1, 0) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red
                    m_ArColores(l - 1, 1) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green
                    m_ArColores(l - 1, 2) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue
    
                Else
                    m_ArColores(l, 0) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red
                    m_ArColores(l, 1) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green
                    m_ArColores(l, 2) = MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue
                End If
                
            Else
            
                If m_iPresupuesto > 0 Then
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red = m_ArColores(l - 1, 0)
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green = m_ArColores(l - 1, 1)
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue = m_ArColores(l - 1, 2)
    
                Else
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red = m_ArColores(l, 0)
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green = m_ArColores(l, 1)
                    MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue = m_ArColores(l, 2)
                End If
            
            End If
                        
            If m_iObjetivo > lMax Then
                lMax = m_iObjetivo
            End If
                                
            If m_iObjetivo < lMin Then
                lMin = m_iObjetivo
            End If
            
        End If
        
        'Presupuesto
        If m_iPresupuesto > 0 Then
            If m_iObjetivo > 0 Then
                k = i + 1
            Else
                k = i
            End If
            m_arEvol(j, k + 1) = m_iPresupuesto
            MSChart1.Plot.SeriesCollection.Item(k).LegendText = m_sPresupuesto
            
            l = UBound(m_ArColores, 1)
            
            If m_bPrimeraCargaEvolucion Then
                m_ArColores(l, 0) = MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Red
                m_ArColores(l, 1) = MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Green
                m_ArColores(l, 2) = MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Blue
            Else
                MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Red = m_ArColores(l, 0)
                MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Green = m_ArColores(l, 1)
                MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Blue = m_ArColores(l, 2)
            
            End If
            
                        
            If m_iPresupuesto > lMax Then
                lMax = m_iPresupuesto
            End If
                                
            If m_iPresupuesto < lMin Then
                lMin = m_iPresupuesto
            End If
            
        End If
    Next j
    
'    Dim p
'    p = 1
    
'    For Each oProve In g_oProvesAsig
'        ReDim Preserve m_ArColores(2, p)
'        'MSChart1.Plot.SeriesCollection.Item(p).Pen.VtColor
'            m_ArColores(0, p) = MSChart1.Plot.SeriesCollection.Item(p).Pen.VtColor.Blue
'            m_ArColores(1, p) = MSChart1.Plot.SeriesCollection.Item(p).Pen.VtColor.Red
'            m_ArColores(2, p) = MSChart1.Plot.SeriesCollection.Item(p).Pen.VtColor.Green
'
'        p = p + 1
'    Next
'
    TratarLabelsNumericas (g_iGrafico)
    
    'A�ade los valores del array al gr�fico
    MSChart1.ChartData = m_arEvol
    
    'leyenda
    MSChart1.Legend.TextLayout.Orientation = VtOrientationUp
    MSChart1.Legend.Location.LocationType = VtChLocationTypeTop
        
    'Etiquetas, fuentes y orientaci�n de los ejes
    MSChart1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = m_sEje
    
    'MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = True
    MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = False
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MajorDivision = 11
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MinorDivision = 1
    
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = lMax
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = lMin

    m_EscalaMax = MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum
    m_EscalaMin = MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum
    
    ReDim Preserve m_arZoom(1, 1)
    m_inumZoom = 1
    
    m_arZoom(0, m_inumZoom) = lMin
    m_arZoom(1, m_inumZoom) = lMax
    m_Divisiones = 10
    cmdZoomOut.Enabled = False
    
    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
        lbl.VtFont.Style = VtFontStyleBold
        lbl.VtFont.Size = 8
        lbl.TextLayout.Orientation = VtOrientationHorizontal
    Next

    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
        lbl.VtFont.Size = 8
        lbl.VtFont.Style = VtFontStyleBold
        lbl.Format = m_sFormatoNumber
    Next
  
    m_bPrimeraCargaEvolucion = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoProcesoEvolucion", err, Erl, , m_bActivado)
        Exit Sub
    End If
  
End Sub


Private Sub sdbgAtributos_BtnClick()
Dim sIdAtrib As String
Dim bExiste As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sIdAtrib = sdbgAtributos.Columns("IDATRIB").Value
        
    Set frmDetAtribProce.g_oProceso = g_oProcesoSeleccionado
     bExiste = False
    'Comprueba si es un atributo de proceso
    If Not g_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        If Not g_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib) Is Nothing Then
            'Es un atributo de proceso
            Set frmDetAtribProce.g_oAtributo = g_oProcesoSeleccionado.ATRIBUTOS.Item(sIdAtrib)
            frmDetAtribProce.sdbcGrupos.Value = m_sProceso
            frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            bExiste = True
        End If
    End If
    'Comprueba si es un atributo de grupo
    If Not g_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
        If Not g_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib) Is Nothing Then
            Set frmDetAtribProce.g_oAtributo = g_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib)
        
            If Not IsNull(g_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib).codgrupo) Then
                'Comprueba si est� definido a nivel de grupo o de proceso
                Set frmDetAtribProce.g_oGrupoSeleccionado = g_oProcesoSeleccionado.grupos.Item(g_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib).codgrupo)
                frmDetAtribProce.sdbcGrupos.Value = g_oProcesoSeleccionado.AtributosGrupo.Item(sIdAtrib).codgrupo
            Else
                frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            End If
            bExiste = True
        End If
    End If
    
    'Comprueba si es un atributo de item
    If Not g_oProcesoSeleccionado.AtributosItem Is Nothing Then
        If Not g_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib) Is Nothing Then
            Set frmDetAtribProce.g_oAtributo = g_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib)
        
            If Not IsNull(g_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib).codgrupo) Then
                'Comprueba si est� definido a nivel de grupo o de proceso
                Set frmDetAtribProce.g_oGrupoSeleccionado = g_oProcesoSeleccionado.grupos.Item(g_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib).codgrupo)
                frmDetAtribProce.sdbcGrupos.Value = g_oProcesoSeleccionado.AtributosItem.Item(sIdAtrib).codgrupo
            Else
                frmDetAtribProce.sdbcGrupos.Value = m_sProceso
                frmDetAtribProce.sdbcGrupos.Text = m_sProceso
            End If
            bExiste = True
        End If
    End If
    
    'Si no es un atributo de item no muestra la pantalla
    If Not bExiste Then Exit Sub
    
    
    frmDetAtribProce.sstabGeneral.Tab = 0
    frmDetAtribProce.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sdbgAtributos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub CargarGrid()
 'A�ade a la grid de aplicar precio todos los items con f�rmulas
    Dim oatrib As Catributo
    Dim sPertenece As String
    Dim vValor As Variant
    

    'Carga los atributos de proceso
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
    If Not g_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
        For Each oatrib In g_oProcesoSeleccionado.ATRIBUTOS
            vValor = Null
            If oatrib.TipoPonderacion <> SinPonderacion Then
                If oatrib.TipoPonderacion = Manual Then
                    vValor = BuscarValorPonderacion(oatrib.idAtribProce, 1)
                Else
                    vValor = 1
                End If
                If Not IsNull(vValor) Then
                    sdbgAtributos.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & m_sProceso & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & NullToStr(oatrib.codgrupo) & Chr(m_lSeparador) & oatrib.ambito
                End If
            End If
        Next
    End If
    
    If g_iGrafico = 2 Or g_iGrafico = 4 Then
        
        'Carga los atributos de grupo
        If Not g_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
            For Each oatrib In g_oProcesoSeleccionado.AtributosGrupo
                vValor = Null
                If oatrib.TipoPonderacion <> SinPonderacion Then
                    If oatrib.TipoPonderacion = Manual Then
                        If IsNull(oatrib.codgrupo) Then
                            vValor = BuscarValorPonderacion(oatrib.idAtribProce, 2)
                        Else
                            vValor = BuscarValorPonderacion(oatrib.idAtribProce, 2, oatrib.codgrupo)
                        End If
                    Else
                        vValor = 1
                    End If
                    If Not IsNull(vValor) Then
                        sPertenece = m_sGrupo & ":"
                        If IsNull(oatrib.codgrupo) Then
                            sPertenece = sPertenece & m_sTodos
                        Else
                            sPertenece = sPertenece & oatrib.codgrupo
                        End If
                        sdbgAtributos.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & NullToStr(oatrib.codgrupo) & Chr(m_lSeparador) & oatrib.ambito
                    End If
                End If
            Next
        End If
        
        'Carga los atributos de item
        If Not g_oProcesoSeleccionado.AtributosItem Is Nothing Then
            For Each oatrib In g_oProcesoSeleccionado.AtributosItem
                vValor = Null
                If oatrib.TipoPonderacion <> SinPonderacion Then
                    If oatrib.TipoPonderacion = Manual Then
                        vValor = BuscarValorPonderacion(oatrib.idAtribProce, 3)
                    Else
                        vValor = 1
                    End If
                    If Not IsNull(vValor) Then
                        sPertenece = m_sItem & ":"
                        If IsNull(oatrib.codgrupo) Then
                            sPertenece = sPertenece & m_sTodos
                        Else
                            sPertenece = sPertenece & oatrib.codgrupo
                        End If
                        sdbgAtributos.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & NullToStr(oatrib.codgrupo) & Chr(m_lSeparador) & oatrib.ambito
                    End If
                End If
            Next
        End If
    Else
        
        'Carga los atributos de grupo
        If Not g_oProcesoSeleccionado.AtributosGrupo Is Nothing Then
            For Each oatrib In g_oProcesoSeleccionado.AtributosGrupo
                If IsNull(oatrib.codgrupo) Or oatrib.codgrupo = m_sCodGrupo Then
                    vValor = Null
                    If oatrib.TipoPonderacion <> SinPonderacion Then
                        If oatrib.TipoPonderacion = Manual Then
                            If IsNull(oatrib.codgrupo) Then
                                vValor = BuscarValorPonderacion(oatrib.idAtribProce, 2)
                            Else
                                vValor = BuscarValorPonderacion(oatrib.idAtribProce, 2, oatrib.codgrupo)
                            End If
                        Else
                            vValor = 1
                        End If
                        If Not IsNull(vValor) Then
                            sPertenece = m_sGrupo & ":"
                            If IsNull(oatrib.codgrupo) Then
                                sPertenece = sPertenece & m_sTodos
                            Else
                                sPertenece = sPertenece & oatrib.codgrupo
                            End If
                            sdbgAtributos.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & NullToStr(oatrib.codgrupo) & Chr(m_lSeparador) & oatrib.ambito
                        End If
                    End If
                End If
            Next
        End If

        'Carga los atributos del item
        If Not g_oProcesoSeleccionado.AtributosItem Is Nothing Then
            For Each oatrib In g_oProcesoSeleccionado.AtributosItem
                If IsNull(oatrib.codgrupo) Or oatrib.codgrupo = m_sCodGrupo Then
                    vValor = Null
                    If oatrib.TipoPonderacion <> SinPonderacion Then
                        If oatrib.TipoPonderacion = Manual Then
                            vValor = BuscarValorPonderacion(oatrib.idAtribProce, 3)
                        Else
                            vValor = 1
                        End If
                        If Not IsNull(vValor) Then
                            sPertenece = m_sItem & ":"
                            If IsNull(oatrib.codgrupo) Then
                                sPertenece = sPertenece & m_sTodos
                            Else
                                sPertenece = sPertenece & oatrib.codgrupo
                            End If
                            sdbgAtributos.AddItem "" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & oatrib.idAtribProce & Chr(m_lSeparador) & NullToStr(oatrib.codgrupo) & Chr(m_lSeparador) & oatrib.ambito
                        End If
                    End If
                End If
            Next
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "CargarGrid", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

''' <summary>
''' Buscar Valor Ponderacion
''' </summary>
''' <param name="idAtribProce">id Atrib Proce</param>
''' <param name="iColec">indice</param>
''' <param name="codgrupo">grupo</param>
''' <returns>Valor Ponderacion</returns>
''' <remarks>Llamada desde: CargarGrid ; Tiempo m�ximo: 0,2</remarks>
Private Function BuscarValorPonderacion(ByVal idAtribProce As Long, iColec As Integer, Optional ByVal codgrupo As String) As Variant
Dim oProve As CProveedor
Dim vValor As Variant
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    vValor = Null
    Select Case iColec
    Case 1
        For Each oProve In g_oProvesAsig
            If Not g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                If Not g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados Is Nothing Then
                    If Not g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(idAtribProce)) Is Nothing Then
                        If Not IsNull(g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(idAtribProce)).ValorPond) Then
                           vValor = g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribProcOfertados.Item(CStr(idAtribProce)).ValorPond
                           Exit For
                        End If
                    End If
                End If
            End If
        Next
    Case 2
        For Each oProve In g_oProvesAsig
            If Not g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod) Is Nothing Then
                If Not g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados Is Nothing Then
                    sCod = codgrupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(codgrupo))
                    sCod = sCod & CStr(idAtribProce)
                     If Not g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(sCod) Is Nothing Then
                         If Not IsNull(g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(sCod).ValorPond) Then
                            vValor = CDbl(g_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).AtribGrOfertados.Item(sCod).ValorPond)
                            Exit For
                         End If
                     End If
                End If
            End If
        Next
    Case Else
         For Each oProve In g_oProvesAsig
            If codgrupo <> "" Then
                 If Not g_oProcesoSeleccionado.grupos.Item(codgrupo).UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                     If Not g_oProcesoSeleccionado.grupos.Item(codgrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados Is Nothing Then
                        For Each oItem In g_oProcesoSeleccionado.grupos.Item(codgrupo).Items
                            If oItem.Confirmado = True Then
                                If Not g_oProcesoSeleccionado.grupos.Item(codgrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(idAtribProce)) Is Nothing Then
                                  If Not IsNull(g_oProcesoSeleccionado.grupos.Item(codgrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(idAtribProce)).ValorPond) Then
                                     vValor = g_oProcesoSeleccionado.grupos.Item(codgrupo).UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(idAtribProce)).ValorPond
                                     Exit For
                                  End If
                                End If
                            End If
                        Next
                     End If
                 End If
             Else
                 For Each oGrupo In g_oProcesoSeleccionado.grupos
                     If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                         If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados Is Nothing Then
                            For Each oItem In oGrupo.Items
                                If oItem.Confirmado = True Then
                                    If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(idAtribProce)) Is Nothing Then
                                      If Not IsNull(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(idAtribProce)).ValorPond) Then
                                         vValor = NullToDbl0(vValor) + CDbl(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(idAtribProce)).ValorPond)
                                         Exit For
                                      End If
                                    End If
                                End If
                            Next
                         End If
                     End If
                 Next
             End If
        Next
    End Select

    BuscarValorPonderacion = vValor
                       
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "BuscarValorPonderacion", err, Erl, , m_bActivado)
        Exit Function
    End If
    
End Function

Private Sub sstabGrafico_Click(PreviousTab As Integer)
Dim oProve As CProveedor
Dim iCount As Integer
Dim i As Integer
Dim lbl As MSChart20Lib.Label
    
       
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If PreviousTab = 0 And sstabGrafico.Tab = 1 Then

    If g_iGrafico = 2 Or g_iGrafico = 7 Then
                
        sdbgAtributos.MoveFirst
        iCount = 0
        MSChart2.ColumnCount = sdbgAtributos.Rows + 1
        For i = 1 To sdbgAtributos.Rows
            If sdbgAtributos.Columns("INCLUIR").Value = "-1" Then
               
                MSChart2.Plot.SeriesCollection.Item(iCount + 1).DataPoints.Item(-1).Marker.Visible = False
                MSChart2.Plot.SeriesCollection.Item(iCount + 1).LegendText = sdbgAtributos.Columns("CODIGO").Value & "-" & sdbgAtributos.Columns("NOMBRE").Value
            
                iCount = iCount + 1
            End If
            sdbgAtributos.MoveNext
        Next
        If iCount = 0 Then
            sstabGrafico.Tab = 0
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        MSChart2.Plot.SeriesCollection.Item(iCount + 1).DataPoints.Item(-1).Marker.Visible = False
        MSChart2.Plot.SeriesCollection.Item(iCount + 1).LegendText = m_sIdiPtsTotales
        MSChart2.ColumnCount = iCount + 2
        
        'ReDim m_ar(1 To g_oProvesAsig.Count, 1 To iCount + 2)
        ReDim m_ar(1 To m_iNumProv, 1 To iCount + 2)
        
        GenerarGraficoPuntos
        
        MSChart1.Visible = False
        MSChart3.Visible = False
        MSChart2.Visible = True
        MSChart2.chartType = VtChChartType2dBar
        MSChart2.SeriesType = VtChSeriesType2dBar
        MSChart2.Legend.Location.LocationType = VtChLocationTypeTop
        For Each lbl In MSChart2.Plot.Axis(VtChAxisIdY).Labels
             lbl.VtFont.Size = 12
             lbl.VtFont.Style = VtFontStyleBold
             lbl.Format = m_sFormatoNumber
         Next
         
         For Each lbl In MSChart2.Plot.Axis(VtChAxisIdY2).Labels
             lbl.VtFont.Size = 12
             lbl.VtFont.Style = VtFontStyleBold
             lbl.Format = m_sFormatoNumber
         Next
         
         MSChart2.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Marker.Style = VtMarkerStyleX

         MSChart2.ChartData = m_ar
         Screen.MousePointer = vbNormal
    
    Else
        sdbgAtributos.MoveFirst
        iCount = 0
        For i = 1 To sdbgAtributos.Rows
            If sdbgAtributos.Columns("INCLUIR").Value = "-1" Then
                iCount = iCount + 1
            End If
            sdbgAtributos.MoveNext
        Next
        If iCount = 0 Then
            sstabGrafico.Tab = 0
            Exit Sub
        End If
    
        Screen.MousePointer = vbHourglass
        
        'ReDim m_ar(1 To g_oProcesoSeleccionado.grupos.Count, 1 To g_oProvesAsig.Count + 1)
        ReDim m_ar(1 To g_oProcesoSeleccionado.grupos.Count, 1 To m_iNumProv + 1)
        
        GenerarGraficoPuntosGrupo
        
        MSChart2.Visible = True
        MSChart2.chartType = VtChChartType2dLine
        MSChart2.SeriesType = VtChSeriesType2dBar
        For Each lbl In MSChart2.Plot.Axis(VtChAxisIdY).Labels
             lbl.VtFont.Size = 12
             lbl.VtFont.Style = VtFontStyleBold
             lbl.Format = m_sFormatoNumber
         Next
         
         For Each lbl In MSChart2.Plot.Axis(VtChAxisIdY2).Labels
             lbl.VtFont.Size = 12
             lbl.VtFont.Style = VtFontStyleBold
             lbl.Format = m_sFormatoNumber
         Next
         
         MSChart2.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Marker.Style = VtMarkerStyleX
       
         i = 1
         For Each oProve In g_oProvesAsig
'            If g_oOrigen.Name = "frmADJItem" Then
'                bVisible = EsProveedorVisible("Item", oProve.Cod)
'            Else
'                Select Case g_oOrigen.sstabComparativa.SelectedItem.Tag
'                    Case "General"
'                        bVisible = EsProveedorVisible("General", oProve.Cod)
'
'                    Case "ALL"
'                        bVisible = EsProveedorVisible("ALL", oProve.Cod)
'
'                    Case Else
'                        bVisible = EsProveedorVisible("GR", oProve.Cod)
'                End Select
'            End If
'            If bVisible = True Then
                MSChart2.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod)
                MSChart2.Plot.SeriesCollection.Item(i).DataPoints.Item(-1).DataPointLabel.LocationType = VtChLabelLocationTypeNone
                i = i + 1
'            End If
         Next
         
         MSChart2.ChartData = m_ar
         
         Screen.MousePointer = vbNormal
    End If
    TratarLabelsNumericas (g_iGrafico)
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "sstabGrafico_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If

End Sub

Private Sub GenerarGraficoPuntosGrupo()
Dim oProve As CProveedor
Dim vValor As Double
Dim i As Integer
Dim j As Integer
Dim m As Integer
Dim oGrupo As CGrupo
Dim bMostrarGrupo As Boolean

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    i = 1
    For Each oGrupo In g_oProcesoSeleccionado.grupos
      bMostrarGrupo = True
      If g_oProcesoSeleccionado.AdminPublica = True Then
        If g_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGrupo = False
        End If
      End If
      
      If bMostrarGrupo = True Then
         j = 1
         m_ar(i, j) = Space(1) & oGrupo.Codigo & Space(1)
         j = j + 1
         m_sCodGrupo = oGrupo.Codigo
         For Each oProve In g_oProvesAsig
'            Select Case g_oOrigen.sstabComparativa.SelectedItem.Tag
'                Case "General"
'                    bVisible = EsProveedorVisible("General", oProve.Cod)
'
'                Case "ALL"
'                    bVisible = EsProveedorVisible("ALL", oProve.Cod)
'
'                Case Else
'                    bVisible = EsProveedorVisible("GR", oProve.Cod)
'            End Select
'            If bVisible = True Then
                 vValor = 0
                 sdbgAtributos.MoveFirst
                 For m = 1 To sdbgAtributos.Rows
                     
                     If sdbgAtributos.Columns("INCLUIR").Value = "-1" Then
                         vValor = vValor + DevolverPuntosDelAtributo(oProve.Cod, sdbgAtributos.Columns("IDATRIB").Value, sdbgAtributos.Columns("AMBITO").Value)
                     End If
                     sdbgAtributos.MoveNext
                 Next
                 m_ar(i, j) = vValor
                
                 j = j + 1
'            End If
         Next
       End If
       i = i + 1
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "GenerarGraficoPuntosGrupo", err, Erl, , m_bActivado)
        Exit Sub
    End If

End Sub

Private Sub GenerarGraficoPuntos()
Dim oProve As CProveedor
Dim vValor As Double
Dim i As Integer
Dim j As Integer
Dim m As Integer
Dim dblTotal As Double
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    i = 1
    For Each oProve In g_oProvesAsig
     
            m_ar(i, 1) = Space(1) & DenominacionProve(oProve.Den, oProve.Cod) & Space(1)
            sdbgAtributos.MoveFirst
            j = 2
            dblTotal = 0
            For m = 1 To sdbgAtributos.Rows
                vValor = 0
                If sdbgAtributos.Columns("INCLUIR").Value = "-1" Then
        
                    vValor = DevolverPuntosDelAtributo(oProve.Cod, sdbgAtributos.Columns("IDATRIB").Value, sdbgAtributos.Columns("AMBITO").Value)
                    
                    m_ar(i, j) = NullToDbl0(vValor)
                    j = j + 1
                End If
                dblTotal = dblTotal + vValor
                m_ar(i, j) = dblTotal
                sdbgAtributos.MoveNext
            Next
            i = i + 1
'        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "GenerarGraficoPuntos", err, Erl, , m_bActivado)
        Exit Sub
    End If
        
End Sub

''' <summary>
''' Devolver Puntos Del Atributo
''' </summary>
''' <param name="sProve">Prove</param>
''' <param name="lAtrib">Atrib</param>
''' <param name="udtAmbito">Ambito</param>
''' <returns>Puntos Del Atributo</returns>
''' <remarks>Llamada desde: GenerarGraficoPuntos ; Tiempo m�ximo: 0,2</remarks>
Private Function DevolverPuntosDelAtributo(ByVal sProve As String, ByVal lAtrib As Long, ByVal udtAmbito As TipoAmbitoProceso) As Double
Dim dblValor As Double
Dim oAtribO As CAtributoOfertado
Dim oGrupo As CGrupo
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    DevolverPuntosDelAtributo = 0
    If g_oProcesoSeleccionado.Ofertas Is Nothing Then Exit Function
    If g_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then Exit Function

    Select Case udtAmbito
    Case AmbProceso
        If g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados Is Nothing Then Exit Function
        If g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(lAtrib)) Is Nothing Then Exit Function
        DevolverPuntosDelAtributo = NullToDbl0(g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(lAtrib)).ValorPond)
                                               
    Case AmbGrupo
        If g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados Is Nothing Then Exit Function
        If g_iGrafico <> 2 Then
            
            With g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados
                sCod = m_sCodGrupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(m_sCodGrupo))
                sCod = sCod & CStr(lAtrib)

                If Not .Item(sCod) Is Nothing Then
                    dblValor = .Item(sCod).ValorPond
                Else
                    dblValor = 0
                End If
            End With
        Else
            For Each oAtribO In g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados
                If oAtribO.idAtribProce = lAtrib Then
                    dblValor = dblValor + NullToDbl0(oAtribO.ValorPond)
                End If
            Next
        End If
        DevolverPuntosDelAtributo = dblValor
        
    Case AmbItem
        Select Case g_iGrafico
        Case 7
            If Not HayAtribItemOfertados(m_sCodGrupo, sProve) Then Exit Function
            
            With g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UltimasOfertas.Item(Trim(sProve)).AtribItemOfertados
                If Not .Item(CStr(m_lItem) & "$" & CStr(lAtrib)) Is Nothing Then
                    dblValor = NullToDbl0(.Item(CStr(m_lItem) & "$" & CStr(lAtrib)).ValorPond)
                Else
                    dblValor = 0
                End If
            End With
        Case 2
            For Each oGrupo In g_oProcesoSeleccionado.grupos
                If HayAtribItemOfertados(oGrupo.Codigo, sProve) Then
                    For Each oAtribO In oGrupo.UltimasOfertas.Item(Trim(sProve)).AtribItemOfertados
                        If oAtribO.idAtribProce = lAtrib Then
                            dblValor = dblValor + NullToDbl0(oAtribO.ValorPond)
                        End If
                    Next
                End If
            Next
        Case 4
            If HayAtribItemOfertados(m_sCodGrupo, sProve) Then
                With g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UltimasOfertas.Item(Trim(sProve))
                    For Each oAtribO In .AtribItemOfertados
                        If oAtribO.idAtribProce = lAtrib Then
                            dblValor = dblValor + NullToDbl0(oAtribO.ValorPond)
                        End If
                    Next
                End With
            End If
        End Select
        DevolverPuntosDelAtributo = dblValor
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "DevolverPuntosDelAtributo", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

''' <summary>
''' Determina si Hay Atrib Item Ofertados
''' </summary>
''' <param name="sGrupo">Grupo</param>
''' <param name="sProve">Prove</param>
''' <returns>si Hay Atrib Item Ofertados</returns>
''' <remarks>Llamada desde: DevolverPuntosDelAtributo ; Tiempo m�ximo: 0,2</remarks>
Private Function HayAtribItemOfertados(ByVal sGrupo As String, sProve As String) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProcesoSeleccionado.grupos.Item(sGrupo).UltimasOfertas Is Nothing Then
        HayAtribItemOfertados = False
        Exit Function
    End If
    If g_oProcesoSeleccionado.grupos.Item(sGrupo).UltimasOfertas.Item(Trim(sProve)) Is Nothing Then
        HayAtribItemOfertados = False
        Exit Function
    End If
    If g_oProcesoSeleccionado.grupos.Item(sGrupo).UltimasOfertas.Item(Trim(sProve)).AtribItemOfertados Is Nothing Then
        HayAtribItemOfertados = False
        Exit Function
    End If
    HayAtribItemOfertados = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "HayAtribItemOfertados", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>
''' Calcula el importe adj. aplicando las f�rmulas de total del item
''' </summary>
''' <param name="sProve">Prove</param>
''' <param name="oItem">Item</param>
''' <param name="dblImporte">Importe</param>
''' <param name="bAplicar">Aplicar f�rmula</param>
''' <returns>el importe adj. aplicando las f�rmulas de total del item</returns>
''' <remarks>Llamada desde: ObtenerGraficoItem ; Tiempo m�ximo: 0,2</remarks>
Private Function AplicarAtributosItem(ByVal sProve As String, ByVal oItem As CItem, ByVal dblImporte As Double, ByVal bAplicar As Boolean) As Double
    Dim oGrupo As CGrupo
    Dim bSiguiente As Boolean
    Dim oAsig As COferta
    Dim dImporteAdj As Double
    Dim bAplicTotalItem As Boolean
    Dim lngMinimo As Long
    Dim oatrib As Catributo
    Dim iOrden As Integer
    Dim dValorAtrib As Double
    Dim lAtribAnterior As Long
    Dim oAtributos As CAtributos
    Dim sCod As String
    
    'dblImporte llega en moneda de oferta y se devuelve en moneda oferta
    'Calcula el importe adj. aplicando las f�rmulas de total del item
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGrupo = g_oProcesoSeleccionado.grupos.Item(oItem.GrupoCod)
    
    dImporteAdj = dblImporte
    
    If Not bAplicar Then
        If g_oOrigen.Name = "frmADJItem" Then
            'If g_oOrigen.ComprobarAplicarAtribsItem(lItem:=m_lItem) <> "" Then
                bAplicTotalItem = True
            'Else
            '    bAplicTotalItem = False
            'End If
        Else
            If ComprobarAplicarAtribsItem(g_oAtribsFormulas, oItem.Id, oItem.GrupoCod) Then
                bAplicTotalItem = True
            Else
                bAplicTotalItem = False
            End If
        End If
    Else
        bAplicTotalItem = True
    End If
    
    Set oAtributos = g_oOrigen.m_oAtribsFormulas
    Set oAsig = oGrupo.UltimasOfertas.Item(Trim(sProve))
        
    If bAplicTotalItem = True And Not oAsig Is Nothing Then
        If Not IsNull(oAsig.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
            lAtribAnterior = 0
            bSiguiente = False
            iOrden = 1
            While bSiguiente = False
                lngMinimo = 0
                For Each oatrib In oAtributos
                        'si el atributo aplica la f�rmula al total del item
                    If oatrib.codgrupo = oItem.GrupoCod Or IsNull(oatrib.codgrupo) Then
                        If oatrib.PrecioAplicarA = 2 And oatrib.UsarPrec = 1 And oatrib.FlagAplicar = True Then
                            If oatrib.Orden = iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                lngMinimo = oatrib.idAtribProce
                                Exit For
                            Else
                                If oatrib.Orden > iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                    If lngMinimo = 0 Then
                                        lngMinimo = oatrib.idAtribProce
                                    ElseIf (oatrib.Orden < oAtributos.Item(CStr(lngMinimo)).Orden) Then
                                        lngMinimo = oatrib.idAtribProce
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
                        
                If (lngMinimo = 0) Then
                    bSiguiente = True
                ElseIf (oAtributos.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                    bSiguiente = True
                Else
                    dValorAtrib = 0
                    Set oatrib = oAtributos.Item(CStr(lngMinimo))
                    lAtribAnterior = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                    Select Case oatrib.ambito
                        Case 1
                            'Proceso
                            If Not g_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
                             If Not g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados Is Nothing Then
                                If Not g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                    dValorAtrib = NullToDbl0(g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                End If
                              End If
                            End If
                                    
                        Case 2
                            'Grupo
                            If Not g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados Is Nothing Then
                                'aplica las formulas que tengan efecto sobre el precio total del item:
                                If oGrupo.Codigo = oatrib.codgrupo Or IsNull(oatrib.codgrupo) Then
                                    sCod = oGrupo.Codigo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(oGrupo.Codigo))
                                    sCod = sCod & CStr(oatrib.idAtribProce)
                                    
                                    If Not g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados.Item(sCod) Is Nothing Then
                                        dValorAtrib = NullToDbl0(g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribGrOfertados.Item(sCod).valorNum)
                                    End If
                                End If
                            End If
                        
                        Case 3  'Item
                            If Not oAsig.AtribItemOfertados Is Nothing Then
                                If Not oAsig.AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                                    dValorAtrib = NullToDbl0(oAsig.AtribItemOfertados.Item(CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)).valorNum)
                                End If
                            End If
                    End Select
                    If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                        dImporteAdj = AplicarFormula(dImporteAdj, dValorAtrib, oatrib.PrecioFormula)
                    End If
                    Set oatrib = Nothing
                    bSiguiente = False
                    iOrden = iOrden + 1
                            
                End If
            Wend
        End If
        
    End If
            
    AplicarAtributosItem = dImporteAdj
    
    Set oGrupo = Nothing
    Set oAsig = Nothing
    Set oatrib = Nothing
    Set oAtributos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "AplicarAtributosItem", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function


Private Function AplicarFormula(ByVal dblImporte As Double, ByVal dblValor As Double, ByVal sOperacion As String) As Double
Dim dblImp As Double


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case sOperacion
    Case "+"
        dblImp = dblImporte + dblValor
        
    Case "-"
        dblImp = dblImporte - dblValor
        
    Case "/"
        dblImp = dblImporte / dblValor
        
    Case "*"
        dblImp = dblImporte * dblValor
        
    Case "+%"
        dblImp = dblImporte + (dblImporte * (dblValor / 100))
        
    Case "-%"
        dblImp = dblImporte - (dblImporte * (dblValor / 100))
        
End Select

AplicarFormula = dblImp
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "AplicarFormula", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Function AplicarAtributosOferta(ByVal sProve As String, ByVal dblImporte As Double, ByVal bAplicar As Boolean) As Double

    Dim oatrib As Catributo
    Dim bSiguiente As Boolean
    Dim lngMinimo As Long
    Dim dValorAtrib As Double
    Dim dblImporteAdj As Double
    Dim iOrden As Integer
    Dim lAtribAnterior As Long
    Dim bAplicTotal As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dblImporteAdj = dblImporte
    'dblImporte llega en moneda del proceso y hay que aplicar los atribs en moneda de la oferta
    If dblImporte <> 0 Then
        'Comprueba si existen atributos aplicables al total del proceso y en ese caso los aplica.
        If Not g_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
            'Si bAplicar es True es porque estoy haciendo el grafico adjudicacdo al 100%
            If Not bAplicar Then
            'comprueba si se aplica o no atributos para �ste proveedor.S�lo se aplicar� si los items
            'de los grupos est�n todos asignados al mismo proveedor.
                If g_oOrigen.ComprobarAplicarAtribsProceso(sProve) = True Then
                    bAplicTotal = True
                    If Not g_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
                        dblImporteAdj = dblImporteAdj * g_oProcesoSeleccionado.Ofertas.Item(sProve).Cambio
                    End If
                Else
                    bAplicTotal = False
                End If
            Else
                If Not g_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
                    dblImporteAdj = dblImporteAdj * g_oProcesoSeleccionado.Ofertas.Item(sProve).Cambio
                End If
                bAplicTotal = True
            End If
            lAtribAnterior = 0
            iOrden = 1
            bSiguiente = False
            
            If bAplicTotal Then
            
                While bSiguiente = False
                    lngMinimo = 0
                    For Each oatrib In g_oOrigen.m_oAtribsFormulas
                        If Not IsNull(oatrib.PrecioFormula) And oatrib.PrecioAplicarA = 0 And oatrib.UsarPrec = 1 And oatrib.FlagAplicar = True Then
                            If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                lngMinimo = oatrib.idAtribProce
                                Exit For
                            Else
                                If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                    If lngMinimo = 0 Then
                                        lngMinimo = oatrib.idAtribProce
                                    ElseIf (oatrib.Orden < g_oOrigen.m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                        lngMinimo = oatrib.idAtribProce
                                    ElseIf oatrib.idAtribProce = lngMinimo Then
                                        bSiguiente = True
                                        Exit For
                                    End If
                                End If
                            End If
                        End If
                    Next
                    
                    If (lngMinimo = 0) Then
                        bSiguiente = True
                    Else
                        dValorAtrib = 0
                        Set oatrib = g_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(lngMinimo))
                        lAtribAnterior = oatrib.idAtribProce
                        If Not g_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
                            If Not g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados Is Nothing Then
                                If Not g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                    dValorAtrib = NullToDbl0(g_oProcesoSeleccionado.Ofertas.Item(sProve).AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                                    If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                                        dblImporteAdj = AplicarFormula(dblImporteAdj, dValorAtrib, oatrib.PrecioFormula)
                                    End If
                                End If
                            End If
                        End If
                        
                        Set oatrib = Nothing
                        bSiguiente = False
                        iOrden = iOrden + 1
                    End If
                Wend
                If Not g_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
                    dblImporteAdj = dblImporteAdj / g_oProcesoSeleccionado.Ofertas.Item(sProve).Cambio
                End If
            End If
        End If
    End If
    
    AplicarAtributosOferta = dblImporteAdj
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "AplicarAtributosOferta", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Prepara el informe para su visualizaci�n</summary>
''' <param name="oReport">Informe</param>
''' <param name="SelectionText">SelectionText</param>
''' <remarks>Llamada desde: ObtenerInforme</remarks>
''' <revision>LTG 11/06/2012</revision>

Private Function PrepararListadoEvolucion(oReport As CRAXDRT.Report, ByVal SelectionText As String)
    Dim i As Integer
    Dim j As Integer
    Dim oProve As CProveedor

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    SelectionText = m_sIdiMon & " " & g_oProcesoSeleccionado.MonCod
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & m_sCaptionList(g_iGrafico) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & m_stxtDe & """"

    Set m_ArItem = New Ador.Recordset
    
    m_ArItem.Fields.Append "OFE", adVarChar, 100
    m_ArItem.Fields.Append "PROVE", adVarChar, 300
    m_ArItem.Fields.Append "PRECIOTOTAL", adDouble
    
    m_ArItem.Open
    
    For i = 1 To UBound(m_arEvol)
        j = 2
        For Each oProve In g_oProvesAsig
'          If g_oOrigen.Name = "frmADJItem" Then
'            bVisible = EsProveedorVisible("Item", oProve.Cod)
'          Else
'            Select Case g_oOrigen.sstabComparativa.SelectedItem.Tag
'                Case "General"
'                    bVisible = EsProveedorVisible("General", oProve.Cod)
'
'                Case "ALL"
'                    bVisible = EsProveedorVisible("ALL", oProve.Cod)
'
'                Case Else
'                    bVisible = EsProveedorVisible("GR", oProve.Cod)
'            End Select
'           End If
'
'           If bVisible = True Then
                m_ArItem.AddNew
                m_ArItem("OFE").Value = m_arEvol(i, 1)
                m_ArItem("PROVE").Value = DenominacionProve(oProve.Den, oProve.Cod)
                If Not IsNull(m_arEvol(i, j)) And Not IsEmpty(m_arEvol(i, j)) Then m_ArItem("PRECIOTOTAL").Value = StrToDbl0(m_arEvol(i, j))
                j = j + 1
'           End If
        Next
    Next
    oReport.Database.SetDataSource m_ArItem
        
    Set m_ArItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "PrepararListadoEvolucion", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Function PrepararListadoPreciosProceso(oReport As CRAXDRT.Report, ByVal SelectionText As String)
Dim i As Integer
Dim oProve As CProveedor

    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_iGrafico = 1 Then
        If sdbcGrupo.Value <> m_sProceso Then
            SelectionText = SelectionText & Space(5) & sdbcGrupo.Value
        End If
        If Me.sdbcItem.Text <> "" Then
            SelectionText = SelectionText & Space(5)
            SelectionText = SelectionText & sdbcItem.Text
        End If
    Else
        SelectionText = SelectionText & Space(5) & m_sCodGrupo & " - " & g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).Den
        If txtItem.Text <> "" Then
            SelectionText = SelectionText & Space(5) & txtItem.Text
        End If
    
    End If
    
   
    oReport.FormulaFields(crs_FormulaIndex(oReport, "PROCESO")).Text = """" & SelectionText & """"
    SelectionText = m_sIdiMon & " " & g_oProcesoSeleccionado.MonCod
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    
    If OptTotal.Value = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLCONSUMIDO")).Text = """" & m_sConsu & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLAHORRADO")).Text = """" & m_sAhorr & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & m_sImporte & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLAHORRADONEG")).Text = """" & m_sAhorrNegativo & """"
        SelectionText = OptTotal.caption
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLCONSUMIDO")).Text = """" & m_sConsu & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLAHORRADO")).Text = """" & m_sAhorr & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & m_sAdjud & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLAHORRADONEG")).Text = """" & m_sAhorrNegativo & """"
        SelectionText = OptActual.caption
    End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TIPOADJU")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & m_sCaptionList(g_iGrafico) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & m_stxtDe & """"

    Set m_ArItem = New Ador.Recordset

    m_ArItem.Fields.Append "PROVE", adVarChar, 100
    m_ArItem.Fields.Append "CONSU", adDouble
    m_ArItem.Fields.Append "ADJU", adDouble
    m_ArItem.Fields.Append "AHORR", adDouble
    m_ArItem.Open
    i = 1
    For Each oProve In g_oProvesAsig
            m_ArItem.AddNew
            m_ArItem("PROVE").Value = m_arLis(i, 1)
            m_ArItem("CONSU").Value = StrToDbl0(m_arLis(i, 2))
            m_ArItem("ADJU").Value = StrToDbl0(m_arLis(i, 3))
            If Not IsNull(m_arLis(i, 4)) Then
                m_ArItem("AHORR").Value = StrToDbl0(m_arLis(i, 4))
            Else
                m_ArItem("AHORR").Value = StrToDbl0(m_arLis(i, 5))
            End If
            i = i + 1
    Next
    
    oReport.Database.SetDataSource m_ArItem
    
    Set m_ArItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "PrepararListadoPreciosProceso", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Function PrepararListadoPreciosGrupo(oReport As CRAXDRT.Report, ByVal SelectionText As String)
Dim i As Integer
Dim j As Integer
Dim oGrupo As CGrupo
Dim oProve As CProveedor
Dim bMostrarGrupo As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        SelectionText = SelectionText & " - " & g_oProcesoSeleccionado.Den
        oReport.FormulaFields(crs_FormulaIndex(oReport, "PROCESO")).Text = """" & SelectionText & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & m_sCaptionList(g_iGrafico) & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & m_stxtPag & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & m_stxtDe & """"
        SelectionText = m_sIdiMon & " " & g_oProcesoSeleccionado.MonCod
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & SelectionText & """"
        If OptActual.Value Then
            oReport.FormulaFields(crs_FormulaIndex(oReport, "TIPOADJU")).Text = """" & OptActual.caption & """"
        Else
            oReport.FormulaFields(crs_FormulaIndex(oReport, "TIPOADJU")).Text = """" & OptTotal.caption & """"
        End If
        
        Set m_ArItem = New Ador.Recordset
    
        m_ArItem.Fields.Append "GRUPO", adVarChar, 50
        m_ArItem.Fields.Append "PROVE", adVarChar, 100
        m_ArItem.Fields.Append "VALORPROVE", adDouble
        
        m_ArItem.Open
        i = 1
        For Each oGrupo In g_oProcesoSeleccionado.grupos
          bMostrarGrupo = True
          If g_oProcesoSeleccionado.AdminPublica = True Then
            If g_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                bMostrarGrupo = False
            End If
          End If
          If bMostrarGrupo = True Then
            j = 2
            For Each oProve In g_oProvesAsig
'                Select Case g_oOrigen.sstabComparativa.SelectedItem.Tag
'                    Case "General"
'                        bVisible = EsProveedorVisible("General", oProve.Cod)
'
'                    Case "ALL"
'                        bVisible = EsProveedorVisible("ALL", oProve.Cod)
'                    Case Else
'                        bVisible = EsProveedorVisible("GR", oProve.Cod)
'                End Select
'                If bVisible = True Then
                    m_ArItem.AddNew
                    m_ArItem("GRUPO").Value = oGrupo.Codigo
                    m_ArItem("PROVE").Value = DenominacionProve(oProve.Den, oProve.Cod)
                    m_ArItem("VALORPROVE").Value = StrToDbl0(m_ar(i, j))
                    j = j + 1
'                End If
            Next
          End If
          i = i + 1
        Next
        
        oReport.Database.SetDataSource m_ArItem
        
        Set m_ArItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "PrepararListadoPreciosGrupo", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Function PrepararListadoPuntosProceso(oReport As CRAXDRT.Report, ByVal SelectionText As String)
Dim i As Integer
Dim j As Integer
Dim oProve As CProveedor
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_iGrafico = 7 Then
        
        SelectionText = SelectionText & Space(5) & m_sCodGrupo & " - " & g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).Den & Space(5)
        If Not IsNull(g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).Items.Item(CStr(m_lItem)).ArticuloCod) And g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).Items.Item(CStr(m_lItem)).ArticuloCod <> "" Then
            SelectionText = SelectionText & g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).Items.Item(CStr(m_lItem)).ArticuloCod & " - "
        End If
        SelectionText = SelectionText & g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).Items.Item(CStr(m_lItem)).Descr
    End If
    
   
    oReport.FormulaFields(crs_FormulaIndex(oReport, "PROCESO")).Text = """" & SelectionText & """"
    SelectionText = m_sIdiMon & " " & g_oProcesoSeleccionado.MonCod
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & SelectionText & """"

    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & m_sCaptionList(g_iGrafico) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & m_stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & m_stxtDe & """"

    Set m_ArItem = New Ador.Recordset

    m_ArItem.Fields.Append "PROVE", adVarChar, 300
    m_ArItem.Fields.Append "ATRIB", adVarChar, 300
    m_ArItem.Fields.Append "VALOR", adDouble
    m_ArItem.Fields.Append "ORDEN", adBigInt
    m_ArItem.Open
    i = 1
    For Each oProve In g_oProvesAsig
'        If g_oOrigen.Name = "frmADJItem" Then
'            bVisible = EsProveedorVisible("Item", oProve.Cod)
'        Else
'        Select Case g_oOrigen.sstabComparativa.SelectedItem.Tag
'           Case "General"
'                bVisible = EsProveedorVisible("General", oProve.Cod)
'
'           Case "ALL"
'                bVisible = EsProveedorVisible("ALL", oProve.Cod)
'
'           Case Else
'                bVisible = EsProveedorVisible("GR", oProve.Cod)
'       End Select
'       End If
'
'       If bVisible = True Then
            For j = 2 To UBound(m_ar, 2)
                m_ArItem.AddNew
                m_ArItem("PROVE").Value = m_ar(i, 1)
                m_ArItem("ATRIB").Value = MSChart2.Plot.SeriesCollection.Item(j - 1).LegendText
                m_ArItem("VALOR").Value = StrToDbl0(m_ar(i, j))
                m_ArItem("ORDEN").Value = j - 1
            Next
            i = i + 1
'        End If
    Next
    
    oReport.Database.SetDataSource m_ArItem
    
    Set m_ArItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "PrepararListadoPuntosProceso", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Function PrepararListadoPuntosGrupo(oReport As CRAXDRT.Report, ByVal SelectionText As String)
Dim i As Integer
Dim j As Integer
Dim oGrupo As CGrupo
Dim oProve As CProveedor
Dim bMostrarGrupo As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        SelectionText = SelectionText & " - " & g_oProcesoSeleccionado.Den
        oReport.FormulaFields(crs_FormulaIndex(oReport, "PROCESO")).Text = """" & SelectionText & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & m_sCaptionList(g_iGrafico) & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & m_stxtPag & """"
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & m_stxtDe & """"
        SelectionText = m_sIdiMon & " " & g_oProcesoSeleccionado.MonCod
        oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & SelectionText & """"
        
        Set m_ArItem = New Ador.Recordset
    
        m_ArItem.Fields.Append "PROVE", adVarChar, 300
        m_ArItem.Fields.Append "GRUPO", adVarChar, 300
        m_ArItem.Fields.Append "VALOR", adDouble
        
        m_ArItem.Open
        i = 1
        For Each oGrupo In g_oProcesoSeleccionado.grupos
          bMostrarGrupo = True
          If g_oProcesoSeleccionado.AdminPublica = True Then
            If g_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                bMostrarGrupo = False
            End If
          End If
          If bMostrarGrupo = True Then
            j = 2
            For Each oProve In g_oProvesAsig
'                Select Case g_oOrigen.sstabComparativa.SelectedItem.Tag
'                    Case "General"
'                        bVisible = EsProveedorVisible("General", oProve.Cod)
'
'                    Case "ALL"
'                        bVisible = EsProveedorVisible("ALL", oProve.Cod)
'
'                    Case Else
'                        bVisible = EsProveedorVisible("GR", oProve.Cod)
'                End Select
'                If bVisible = True Then
                    m_ArItem.AddNew
                    m_ArItem("GRUPO").Value = oGrupo.Codigo
                    m_ArItem("PROVE").Value = DenominacionProve(oProve.Den, oProve.Cod)
                    m_ArItem("VALOR").Value = StrToDbl0(m_ar(i, j))
                    j = j + 1
'                End If
            Next
            
           End If
           
           i = i + 1
           
        Next
        
        oReport.Database.SetDataSource m_ArItem
        
        Set m_ArItem = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "PrepararListadoPuntosGrupo", err, Erl, , m_bActivado)
        Exit Function
    End If

End Function

Private Sub ConfigurarDesdeItem(ByVal iTipo As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sCodGrupo = g_oOrigen.g_ogrupo.Codigo
    m_lItem = g_oOrigen.g_oItemSeleccionado.Id
    
    Select Case iTipo
        Case 6, 8
            If g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UsarEscalados Then
                lblRango.Visible = True
                sdbcEscalado.Visible = True
            Else
                lblRango.Visible = False
                sdbcEscalado.Visible = False
            End If
            
            txtGrupo.Text = g_oOrigen.g_ogrupo.Codigo & " - " & g_oOrigen.g_ogrupo.Den
            If IsNull(g_oOrigen.g_oItemSeleccionado.ArticuloCod) Then
                txtItem.Text = g_oOrigen.g_oItemSeleccionado.Descr
            Else
                txtItem.Text = g_oOrigen.g_oItemSeleccionado.ArticuloCod & " - " & g_oOrigen.g_oItemSeleccionado.Descr
            End If
        Case 7
            If Not g_oOrigen.g_oOrigen.g_PonderacionCalculada Then
                g_oOrigen.g_oOrigen.CalcularPonderacionAtributosOfertados
            End If
        Case 9
            lblRango.Visible = False
            sdbcEscalado.Visible = False
            
            txtGrupo.Text = g_oOrigen.g_ogrupo.Codigo & " - " & g_oOrigen.g_ogrupo.Den
            If IsNull(g_oOrigen.g_oItemSeleccionado.ArticuloCod) Then
                txtItem.Text = g_oOrigen.g_oItemSeleccionado.Descr
            Else
                txtItem.Text = g_oOrigen.g_oItemSeleccionado.ArticuloCod & " - " & g_oOrigen.g_oItemSeleccionado.Descr
            End If
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ConfigurarDesdeItem", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

Private Function DenominacionProve(ByVal ProveDen As String, ByVal ProveCod As String) As String
    Dim sIndice As String
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sIndice = ""
    If UBound(m_arProveRep) > 0 Then
        For i = 1 To UBound(m_arProveRep)
            If m_arProveRep(i) = ProveCod Then
                sIndice = "(" & i & ")"
                Exit For
            End If
        Next i
    End If
    
    If Len(ProveDen) > 17 Then
        DenominacionProve = Mid(ProveDen, 1, 17) & "..." & sIndice
    Else
        DenominacionProve = ProveDen & sIndice
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "DenominacionProve", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

Private Function ApilarValores(ByVal Arr As Variant) As Variant
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim l As Integer
    Dim vAuxArr() As Variant
    Dim vNewArr() As Variant
    Dim vTmpArr() As Variant
    Dim vMayor As Variant
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ReDim vAuxArr(0 To UBound(Arr, 2))
    ReDim vNewArr(1 To UBound(Arr, 1), 1 To 11) As Variant
    ReDim vTmpArr(1 To UBound(Arr, 1), 1 To UBound(Arr, 2)) As Variant
    
    For i = 1 To UBound(Arr, 1)
        For j = 2 To UBound(Arr, 2)
            Arr(i, j) = Numero(Arr(i, j), garSimbolos(2))
        Next
    Next
            
        
    For i = 1 To UBound(Arr, 1)
    
        For j = 2 To UBound(Arr, 2)
            If Arr(i, j) > 0 Then
                vAuxArr(j) = Arr(i, j)
            Else
                vAuxArr(j) = 0
            End If
            
        Next j
        
        For j = 2 To UBound(Arr, 2)
            vMayor = 0
            l = 2
            For k = 2 To UBound(Arr, 2)
                If vMayor < vAuxArr(k) Then
                    vMayor = vAuxArr(k)
                    l = k
                End If
            Next
            If vMayor > 0 Then
                vMayor = 0
                For k = 2 To UBound(Arr, 2)
                    If k <> l Then
                        If vMayor < vAuxArr(k) Then
                            vMayor = vAuxArr(k)
                        End If
                    End If
                Next
                vTmpArr(i, l) = Arr(i, l) - vMayor
                vAuxArr(l) = 0
            End If
        Next
    Next
    
    
    For i = 1 To UBound(Arr, 1)
    
        For j = 2 To UBound(Arr, 2)
            If Arr(i, j) < 0 Then
                vAuxArr(j) = Numero(Arr(i, j), garSimbolos(2))
            Else
                vAuxArr(j) = 0
            End If
            
        Next j
        
        For j = 2 To UBound(Arr, 2)
            vMayor = 0
            l = 2
            For k = 2 To UBound(Arr, 2)
                If vMayor < Abs(vAuxArr(k)) Then
                    vMayor = Abs(vAuxArr(k))
                    l = k
                End If
            Next
            If vMayor > 0 Then
                vMayor = 0
                For k = 2 To UBound(Arr, 2)
                    If k <> l Then
                        If vMayor < Abs(vAuxArr(k)) Then
                            vMayor = Abs(vAuxArr(k))
                        End If
                    End If
                Next
                vTmpArr(i, l) = Arr(i, l) + vMayor
                vAuxArr(l) = 0
            End If
        Next
    Next
            
    
    For i = 1 To UBound(Arr, 1)
        For j = 2 To 11
            vNewArr(i, j) = Null
        Next
        vNewArr(i, 1) = Arr(i, 1)
        If Arr(i, 2) > Arr(i, 3) Then 'consumido > adjudicado
            If Arr(i, 3) > IIf(IsNull(Arr(i, 4)), Arr(i, 5), Arr(i, 4)) Then 'adjudicado > Ahorrado
                vNewArr(i, 2) = vTmpArr(i, 4)
                vNewArr(i, 3) = vTmpArr(i, 5)
                vNewArr(i, 4) = vTmpArr(i, 3)
                vNewArr(i, 5) = vTmpArr(i, 2)
            Else
                If Arr(i, 2) > IIf(IsNull(Arr(i, 4)), Arr(i, 5), Arr(i, 4)) Then 'Consumido > Ahorrado
                    vNewArr(i, 4) = vTmpArr(i, 3)
                    vNewArr(i, 8) = vTmpArr(i, 4)
                    vNewArr(i, 9) = vTmpArr(i, 5)
                    vNewArr(i, 10) = vTmpArr(i, 2)
                Else
                    vNewArr(i, 4) = vTmpArr(i, 3)
                    vNewArr(i, 7) = vTmpArr(i, 2)
                    vNewArr(i, 8) = vTmpArr(i, 4)
                    vNewArr(i, 9) = vTmpArr(i, 5)
                End If
            End If
        Else
            If Arr(i, 2) > IIf(IsNull(Arr(i, 4)), Arr(i, 5), Arr(i, 4)) Then 'Consumido > Ahorrado
                vNewArr(i, 2) = vTmpArr(i, 4)
                vNewArr(i, 3) = vTmpArr(i, 5)
                vNewArr(i, 5) = vTmpArr(i, 2)
                vNewArr(i, 6) = vTmpArr(i, 3)
            Else
                If Arr(i, 3) > IIf(IsNull(Arr(i, 4)), Arr(i, 5), Arr(i, 4)) Then 'adjudicado > Ahorrado
                    vNewArr(i, 5) = vTmpArr(i, 2)
                    vNewArr(i, 8) = vTmpArr(i, 4)
                    vNewArr(i, 9) = vTmpArr(i, 5)
                    vNewArr(i, 11) = vTmpArr(i, 3)
                Else
                    vNewArr(i, 5) = vTmpArr(i, 2)
                    vNewArr(i, 6) = vTmpArr(i, 3)
                    vNewArr(i, 8) = vTmpArr(i, 4)
                    vNewArr(i, 9) = vTmpArr(i, 5)
                End If
            End If
        End If
                
    Next
    
    ApilarValores = vNewArr
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ApilarValores", err, Erl, , m_bActivado)
        Exit Function
    End If
End Function

''' <summary>Obtiene el gr�fico asociado al proceso e item seleccionados</summary>
''' <param name="ValorMin">valor m�nimo</param>
''' <param name="ValorMax">valor m�ximo</param>
''' <param name="Divisiones">Divisiones</param>
''' <param name="bSaltarseMaximoyMinimo">saltarse m�x. y min.</param>
''' <remarks>Llamada desde: cmdZoomOut_Click,MSChart1_MouseMove,MSChart1_MouseUp ;m�ximo: 0</remarks>
''' <revision>LTG 17/11/2011</revision>

Private Sub ObtenerGraficoProcesoEvolucion2(ByVal ValorMin As Double, ByVal ValorMax As Double, ByVal Divisiones As Integer, Optional ByVal bSaltarseMaximoyMinimo As Boolean)
    Dim lbl As MSChart20Lib.Label
    Dim i As Integer
    Dim Ador As Ador.Recordset
    Dim intNumOfertas As Integer
    Dim bIncompleto As Boolean
    Dim j As Integer
    Dim k As Integer
    Dim oProve As CProveedor
    Dim iNumOfe As Long
    Dim dblPrec As Double
    Dim dblPrecio As Double
    Dim lMax As Double
    Dim lMin As Double
    Dim bPrimeraVez As Boolean
    Dim bCasoProveedor As Boolean
    Dim bUsarEscalados As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lMax = ValorMax
    lMin = ValorMin
    bPrimeraVez = True
            
    PicPrecios.Visible = True
    picAtributos.Visible = False
    
    ObtenerPresYObjetivo
    
    MSChart1.ShowLegend = True
    MSChart3.Visible = False
    
    
    If m_oIOfertas Is Nothing Then Exit Sub
    'Obtiene el n�mero de ofertas para el proceso
    intNumOfertas = m_oIOfertas.DevolverNumOfertasDelProceso(m_lItem)
    
    If intNumOfertas = 0 Then
        'No existen ofertas.Sacar un mensaje indic�ndolo
        If m_lItem <> 0 Then
            oMensajes.NoExistenOfertasProc 47
        ElseIf m_sCodGrupo <> m_sProceso Then
            oMensajes.NoExistenOfertasProc 120
        Else
            oMensajes.NoExistenOfertasProc 44
        End If
        Exit Sub
    End If
    
    If g_oProvesAsig.Count = 0 Then
        'No existen ofertas.Sacar un mensaje indic�ndolo
        If m_lItem <> 0 Then
            oMensajes.NoExistenOfertasProc 47
        ElseIf m_sCodGrupo <> m_sProceso Then
            oMensajes.NoExistenOfertasProc 120
        Else
            oMensajes.NoExistenOfertasProc 44
        End If
        Exit Sub
    End If
    
Dim iUbo As Integer
    MSChart1.chartType = VtChChartType2dLine
    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    If m_iObjetivo > 0 And m_iPresupuesto > 0 Then
        ReDim m_arEvol2(1 To intNumOfertas, 3)
        MSChart1.ColumnCount = m_iNumProv + 2
        iUbo = 3
    ElseIf m_iObjetivo > 0 Or m_iPresupuesto > 0 Then
        ReDim m_arEvol2(1 To intNumOfertas, 2)
        MSChart1.ColumnCount = m_iNumProv + 1
        iUbo = 2
    Else
        ReDim m_arEvol2(1 To intNumOfertas, 1)
        MSChart1.ColumnCount = m_iNumProv
        iUbo = 1
    End If
    
    i = 1
    While i <= intNumOfertas
        m_arEvol2(i, 1) = m_sOferta & i
        i = i + 1
    Wend
    
    If sdbcGrupo.Text <> m_sProceso Then
        bUsarEscalados = (g_oProcesoSeleccionado.grupos.Item(m_sCodGrupo).UsarEscalados = 1)
    Else
        bUsarEscalados = False
    End If
    
    Dim icolores As Integer
    'Para cada proveedor
    i = 1
    icolores = 1
    For Each oProve In g_oProvesAsig
        
        If oProve.VisibleEnGrafico Then
            bIncompleto = False
            'Obtiene las ofertas para cada proveedor
            If m_sCodGrupo = m_sProceso Then
                If bUsarEscalados And sdbcEscalado.Columns("ID").Value <> vbNullString Then
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, , m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, , m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                Else
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, , m_lItem, m_bSoloAbiertos)
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, , m_lItem, m_bSoloAbiertos)
                End If
                bCargar = True
                If gParametrosGenerales.gbProveGrupos And m_lItem <> 0 Then
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sdbcItem.Columns("CODGRUPO").Value) Is Nothing Then
                        bCargar = False
                    End If
                End If
    
            Else
                If bUsarEscalados And sdbcEscalado.Columns("ID").Value <> vbNullString Then
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos, CLng(sdbcEscalado.Columns("ID").Value))
                Else
                    Set Ador = m_oIOfertas.DevolverOfertasDelProceso(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos)
                    bIncompleto = m_oIOfertas.DevolverOfertasDelProcesoSinPrecio(oProve.Cod, m_lIdGrupo, m_lItem, m_bSoloAbiertos)
                End If
                bCargar = True
                If gParametrosGenerales.gbProveGrupos Then
                    scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If g_oOrigen.Name = "frmADJItem" Then
                        If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(m_sCodGrupo) Is Nothing Then
                            bCargar = False
                        End If
                    Else
                        If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(m_sCodGrupo) Is Nothing Then
                            bCargar = False
                        End If
                    End If
                End If
            End If
                'Las cantidades est�n ya cambiadas a moneda del proceso
            If bCargar Then
                ReDim Preserve m_arEvol2(1 To intNumOfertas, iUbo + i)
                
                If Not Ador.EOF Then
                
                    iNumOfe = Ador("NUM").Value
                    dblPrecio = 0
                    While Not Ador.EOF
                        dblPrec = NullToDbl0(Ador("PREC_VALIDO").Value)
                        
                        If iNumOfe <> Ador("NUM").Value Then
                            If (dblPrecio >= ValorMin And dblPrecio <= ValorMax) Or (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then '+ 1 Then
                                If (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then
                                    bCasoProveedor = True
                                End If
                                
                                m_arEvol2(iNumOfe, i + 1) = dblPrecio
                                
                                If bPrimeraVez Then
                                    lMax = dblPrecio
                                    lMin = dblPrecio
                                    bPrimeraVez = False
                                Else
                                    
                                    If dblPrecio > lMax Then
                                        lMax = dblPrecio
                                    End If
                                    
                                    If dblPrecio < lMin Then
                                        lMin = dblPrecio
                                    End If
                                    
                                End If
                                
                                
                            End If
                        End If
                            dblPrecio = dblPrec
                            iNumOfe = Ador("NUM").Value
                        Ador.MoveNext
                    Wend
                    If (dblPrecio >= ValorMin And dblPrecio <= ValorMax) Or (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then
                    
                        If (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then
                            bCasoProveedor = True
                        End If
                    
                        If dblPrecio > lMax Then
                            lMax = dblPrecio
                        End If
                        
                        If dblPrecio < lMin Then
                            lMin = dblPrecio
                        End If
                    
                        m_arEvol2(iNumOfe, i + 1) = dblPrecio 'Es la ultima del while
                    End If
                
                    Ador.Close
                    Set Ador = Nothing
                End If
            
                'Leyenda de los proveedores
                If bIncompleto = True Then
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod) & " " & m_sIncompleto
                Else
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod)
                End If
                
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Set m_ArColores(icolores, 0), m_ArColores(icolores, 1), m_ArColores(icolores, 2)

                m_ArProveVisibles(icolores) = 1
                
                icolores = icolores + 1
                i = i + 1
            End If
        Else
            m_ArProveVisibles(icolores) = 0
            icolores = icolores + 1
        End If
    Next
        
    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    If m_iObjetivo > 0 And m_iPresupuesto > 0 Then
        MSChart1.ColumnCount = i - 1 + 2
    ElseIf m_iObjetivo > 0 Or m_iPresupuesto > 0 Then
        MSChart1.ColumnCount = i - 1 + 1
    Else
        MSChart1.ColumnCount = i - 1
    End If
    
    Dim l As Integer
    
    For j = 1 To intNumOfertas
        'Objetivo
        If m_iObjetivo > 0 Then
            If (m_iObjetivo >= ValorMin And m_iObjetivo <= ValorMax) Or bCasoProveedor Then
            
                m_arEvol2(j, i + 1) = m_iObjetivo
                If m_iObjetivo > lMax Then
                    lMax = m_iObjetivo
                End If
                                    
                If m_iObjetivo < lMin Then
                    lMin = m_iObjetivo
                End If
                
            End If
            MSChart1.Plot.SeriesCollection.Item(i).LegendText = m_sObjetivo
            
            l = UBound(m_ArColores, 1)
            
            If m_iPresupuesto > 0 Then
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red = m_ArColores(l - 1, 0)
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green = m_ArColores(l - 1, 1)
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue = m_ArColores(l - 1, 2)

            Else
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Red = m_ArColores(l, 0)
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Green = m_ArColores(l, 1)
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Blue = m_ArColores(l, 1)
            End If
            
        End If
        
        'Presupuesto
        If m_iPresupuesto > 0 Then
            If m_iObjetivo > 0 Then
                k = i + 1
            Else
                k = i
            End If
            If (m_iPresupuesto >= ValorMin And m_iPresupuesto <= ValorMax) Or bCasoProveedor Then
                m_arEvol2(j, k + 1) = m_iPresupuesto
                If m_iPresupuesto > lMax Then
                    lMax = m_iPresupuesto
                End If
                                    
                If m_iPresupuesto < lMin Then
                    lMin = m_iPresupuesto
                End If
                
            End If
            MSChart1.Plot.SeriesCollection.Item(k).LegendText = m_sPresupuesto
            
            l = UBound(m_ArColores, 1)
            
            MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Red = m_ArColores(l, 0)
            MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Green = m_ArColores(l, 1)
            MSChart1.Plot.SeriesCollection.Item(k).Pen.VtColor.Blue = m_ArColores(l, 2)
        End If
    Next j
    
    TratarLabelsNumericas (g_iGrafico)
    
    'A�ade los valores del array al gr�fico
    MSChart1.ChartData = m_arEvol2
    
    'leyenda
    MSChart1.Legend.TextLayout.Orientation = VtOrientationUp
    MSChart1.Legend.Location.LocationType = VtChLocationTypeTop
    
    'Etiquetas, fuentes y orientaci�n de los ejes
    MSChart1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = m_sEje
    
    'MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = True
    MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = False
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MajorDivision = Divisiones
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MinorDivision = 1
    
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = lMax
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = lMin

    If bSaltarseMaximoyMinimo Then
    
        If lMax > ValorMax Or lMin > ValorMin Then
        
            ReDim Preserve m_arZoom(1, 1)
            m_inumZoom = 1
            
            m_arZoom(0, m_inumZoom) = lMin
            m_arZoom(1, m_inumZoom) = lMax
        
            cmdZoomOut.Enabled = False
            
        End If
    End If
    
    
    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
        lbl.VtFont.Style = VtFontStyleBold
        lbl.VtFont.Size = 8
        lbl.TextLayout.Orientation = VtOrientationHorizontal
    Next

    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
        lbl.VtFont.Size = 8
        lbl.VtFont.Style = VtFontStyleBold
        lbl.Format = m_sFormatoNumber
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoProcesoEvolucion2", err, Erl, , m_bActivado)
        Exit Sub
    End If
  
End Sub

''' <summary>Obtiene el gr�fico asociado a la �ltima oferta con escalados</summary>
''' <param name="ValorMin">valor m�nimo</param>
''' <param name="ValorMax">valor m�ximo</param>
''' <param name="Divisiones">Divisiones</param>
''' <param name="bSaltarseMaximoyMinimo">saltarse m�x. y min.</param>
''' <remarks>Llamada desde: cmdZoomOut_Click,MSChart1_MouseMove,MSChart1_MouseUp ;m�ximo: 0</remarks>

Private Sub ObtenerGraficoUltimaOfertaEscZoom(ByVal ValorMin As Double, ByVal ValorMax As Double, ByVal Divisiones As Integer, Optional ByVal bSaltarseMaximoyMinimo As Boolean)
    Dim lbl As MSChart20Lib.Label
    Dim i As Integer
    Dim Ador As Ador.Recordset
    Dim bIncompleto As Boolean
    Dim k As Integer
    Dim oProve As CProveedor
    Dim dblPrec As Double
    Dim dblPrecio As Double
    Dim oGrupo As CGrupo
    Dim lMax As Double
    Dim lMin As Double
    Dim bPrimeraVez As Boolean
    Dim bCasoProveedor As Boolean
    Dim sCodGrupo As String
    Dim oEsc As CEscalado
    Dim oproce As CProceso
    Dim iNumEsc As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lMax = ValorMax
    lMin = ValorMin
    bPrimeraVez = True
            
    PicPrecios.Visible = True
    picAtributos.Visible = False
    
    MSChart1.ShowLegend = True
    MSChart3.Visible = False
        
    If m_oIOfertas Is Nothing Then Exit Sub
    
    If g_oProvesAsig.Count = 0 Then
        'No existen ofertas.Sacar un mensaje indic�ndolo
        If m_lItem <> 0 Then
            oMensajes.NoExistenOfertasProc 47
        ElseIf m_sCodGrupo <> m_sProceso Then
            oMensajes.NoExistenOfertasProc 120
        Else
            oMensajes.NoExistenOfertasProc 44
        End If
        Exit Sub
    End If
    
    MSChart1.chartType = VtChChartType2dLine
    
    If m_sCodGrupo <> m_sProceso Then
        sCodGrupo = m_sCodGrupo
    Else
        sCodGrupo = sdbcGrupo.Columns("COD").Value
    End If
    Set oGrupo = g_oProcesoSeleccionado.grupos.Item(sCodGrupo)
    
    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    ReDim m_arEvol2(1 To oGrupo.Escalados.Count, 1)
    MSChart1.ColumnCount = m_iNumProv
    
    i = 1
    For Each oEsc In oGrupo.Escalados
        Select Case oGrupo.TipoEscalados
            Case TModoEscalado.ModoDirecto
                m_arEvol2(i, 1) = CStr(oEsc.Inicial) & " "   'Si no es un string no lo coge como base del eje horizontal
            Case TModoEscalado.ModoRangos
                m_arEvol2(i, 1) = oEsc.Inicial & " - " & oEsc.final
        End Select
        
        i = i + 1
    Next
    Set oEsc = Nothing
    
    Dim icolores As Integer
    'Para cada proveedor
    i = 1
    icolores = 1
    For Each oProve In g_oProvesAsig
        
        If oProve.VisibleEnGrafico Then
            bIncompleto = False
            'Obtiene las ofertas para cada proveedor
            Set oproce = oFSGSRaiz.Generar_CProceso
            Set Ador = oproce.DevolverUltimasOfertasItemEsc(g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, m_lItem, oProve.Cod)
            Set oproce = Nothing
                        
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If g_oOrigen.Name = "frmADJItem" Then
                    If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sCodGrupo) Is Nothing Then
                        bCargar = False
                    End If
                Else
                    If g_oOrigen.m_oAsigs.Item(scodProve).grupos.Item(sCodGrupo) Is Nothing Then
                        bCargar = False
                    End If
                End If
            End If
                            
            If bCargar Then
                ReDim Preserve m_arEvol2(1 To oGrupo.Escalados.Count, i + 1)
                
                If Not Ador.EOF Then
                    dblPrecio = 0
                    bIncompleto = False
                    k = 1
                    While Not Ador.EOF
                        dblPrec = NullToDbl0(Ador("PREC_VALIDO").Value)
                                                
                        If (dblPrec >= ValorMin And dblPrec <= ValorMax) Or (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then '+ 1 Then
                            If (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then
                                bCasoProveedor = True
                            End If
                            
                            iNumEsc = 0
                            For Each oEsc In oGrupo.Escalados
                                iNumEsc = iNumEsc + 1
                                If oEsc.Id = Ador("ESC") Then
                                    If k < iNumEsc Then bIncompleto = True
                                    Exit For
                                End If
                            Next
                            m_arEvol2(iNumEsc, i + 1) = dblPrec
                                
                            If dblPrec > lMax Then
                                lMax = dblPrec
                            End If

                            If dblPrec < lMin Then
                                lMin = dblPrec
                            End If
                        End If
                        
                        dblPrecio = dblPrec
                            
                        k = k + 1
                        Ador.MoveNext
                    Wend
                    
'                    If (dblPrecio >= ValorMin And dblPrecio <= ValorMax) Or (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then
'                        If (bSaltarseMaximoyMinimo And m_ArProveVisibles(icolores) = 0) Then
'                            bCasoProveedor = True
'                        End If
'
'                        If dblPrecio > lMax Then
'                            lMax = dblPrecio
'                        End If
'
'                        If dblPrecio < lMin Then
'                            lMin = dblPrecio
'                        End If
'
'                        m_arEvol2(iNumEsc, i + 1) = dblPrecio 'Es la ultima del while
'                    End If
                
                    Ador.Close
                    Set Ador = Nothing
                End If
            
                'Leyenda de los proveedores
                If bIncompleto Then
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod) & " " & m_sIncompleto
                Else
                    MSChart1.Plot.SeriesCollection.Item(i).LegendText = DenominacionProve(oProve.Den, oProve.Cod)
                End If
                
                MSChart1.Plot.SeriesCollection.Item(i).Pen.VtColor.Set m_ArColores(icolores, 0), m_ArColores(icolores, 1), m_ArColores(icolores, 2)

                m_ArProveVisibles(icolores) = 1
                
                icolores = icolores + 1
                i = i + 1
            End If
        Else
            m_ArProveVisibles(icolores) = 0
            icolores = icolores + 1
        End If
    Next
        
    'Necesitaremos tantas series como proveedores hayan realizado ofertas
    MSChart1.ColumnCount = i - 1
            
    TratarLabelsNumericas (g_iGrafico)
    
    'A�ade los valores del array al gr�fico
    MSChart1.ChartData = m_arEvol2
    
    'leyenda
    MSChart1.Legend.TextLayout.Orientation = VtOrientationUp
    MSChart1.Legend.Location.LocationType = VtChLocationTypeTop
    
    'Etiquetas, fuentes y orientaci�n de los ejes
    MSChart1.Plot.Axis(VtChAxisIdY).AxisTitle.Text = m_sEje
    
    'MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = True
    MSChart1.Plot.Axis(VtChAxisIdY).CategoryScale.Auto = False
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MajorDivision = Divisiones
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.MinorDivision = 1
    
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = lMax
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = lMin

    If bSaltarseMaximoyMinimo Then
    
        If lMax > ValorMax Or lMin > ValorMin Then
        
            ReDim Preserve m_arZoom(1, 1)
            m_inumZoom = 1
            
            m_arZoom(0, m_inumZoom) = lMin
            m_arZoom(1, m_inumZoom) = lMax
        
            cmdZoomOut.Enabled = False
            
        End If
    End If
    
    
    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
        lbl.VtFont.Style = VtFontStyleBold
        lbl.VtFont.Size = 8
        lbl.TextLayout.Orientation = VtOrientationHorizontal
    Next

    For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
        lbl.VtFont.Size = 8
        lbl.VtFont.Style = VtFontStyleBold
        lbl.Format = m_sFormatoNumber
    Next
    
    Set oGrupo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmADJGraficos", "ObtenerGraficoUltimaOfertaEscZoom", err, Erl, , m_bActivado)
        Exit Sub
    End If
End Sub

