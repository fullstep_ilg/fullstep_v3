VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPRESPorCon5 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   5445
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6600
   Icon            =   "frmLstPRESPorCon5.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5445
   ScaleWidth      =   6600
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   6600
      TabIndex        =   0
      Top             =   5025
      Width           =   6600
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   5240
         TabIndex        =   1
         Top             =   20
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5040
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6570
      _ExtentX        =   11589
      _ExtentY        =   8890
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPRESPorCon5.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame2"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame4"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      Begin VB.Frame Frame4 
         Height          =   1080
         Left            =   120
         TabIndex        =   16
         Top             =   480
         Width           =   6375
         Begin SSDataWidgets_B.SSDBCombo sdbcPartidaPres 
            Height          =   285
            Left            =   360
            TabIndex        =   17
            Top             =   600
            Width           =   5595
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2672
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7064
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   9869
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblFiltroArbol 
            AutoSize        =   -1  'True
            Caption         =   "DFiltro por �rbol de partidas de control de aprovisionamiento:"
            Height          =   195
            Left            =   405
            TabIndex        =   18
            Top             =   315
            Width           =   4275
         End
      End
      Begin VB.Frame Frame2 
         Height          =   1095
         Left            =   120
         TabIndex        =   11
         Top             =   1560
         Width           =   6375
         Begin VB.CommandButton cmdBorrar 
            Height          =   285
            Left            =   5490
            Picture         =   "frmLstPRESPorCon5.frx":0CCE
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   480
            Width           =   315
         End
         Begin VB.CommandButton cmdSelCon5 
            Height          =   285
            Left            =   5850
            Picture         =   "frmLstPRESPorCon5.frx":0D73
            Style           =   1  'Graphical
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   480
            Width           =   315
         End
         Begin VB.Label lblParCon 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   420
            TabIndex        =   15
            Top             =   480
            Width           =   5010
         End
         Begin VB.Label lblNomPar 
            AutoSize        =   -1  'True
            Caption         =   "Partida presupuestaria:"
            Height          =   195
            Left            =   450
            TabIndex        =   14
            Top             =   240
            Width           =   1620
         End
      End
      Begin VB.Frame Frame3 
         Height          =   1270
         Left            =   120
         TabIndex        =   7
         Top             =   2640
         Width           =   6375
         Begin VB.ComboBox cmbOblPC 
            Height          =   315
            IntegralHeight  =   0   'False
            Left            =   2760
            Style           =   2  'Dropdown List
            TabIndex        =   9
            Top             =   775
            Width           =   855
         End
         Begin VB.CheckBox chkBajaLog 
            Caption         =   "Incluir bajas l�gicas"
            Height          =   225
            Left            =   480
            TabIndex        =   8
            Top             =   325
            Width           =   2000
         End
         Begin VB.Label lblOblPC 
            AutoSize        =   -1  'True
            Caption         =   "dNivel de desglose de listado"
            Height          =   195
            Left            =   480
            TabIndex        =   10
            Top             =   825
            Width           =   2070
         End
      End
      Begin VB.Frame Frame1 
         Height          =   960
         Left            =   120
         TabIndex        =   3
         Top             =   3915
         Width           =   6345
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   240
            Left            =   450
            TabIndex        =   5
            Top             =   570
            Value           =   -1  'True
            Width           =   2235
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   240
            Left            =   3225
            TabIndex        =   4
            Top             =   570
            Width           =   2205
         End
         Begin VB.Label lblOrdenar 
            AutoSize        =   -1  'True
            Caption         =   "Ordenar por:"
            Height          =   195
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Width           =   885
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4815
         Top             =   15
      End
   End
End
Attribute VB_Name = "frmLstPRESPorCon5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables privadas
Private m_oPres0 As cPresConcep5Nivel0
Private m_oPartidas As cPresConceptos5Nivel0
Private m_bCargarComboDesde As Boolean
Private oIBaseDatos As IBaseDatos
Private m_sUsu As String
Private m_sPerfilCod As String
Private m_bSaltar As Boolean

Private m_sTituloInf As String
Private m_sFiltro As String
Private m_sArbolPresupuestario As String
Private m_sCodigo As String
Private m_sDenominacion As String
Private m_sBaja As String
Private m_sSeleccionando As String
Private m_sVisualizando As String
Private m_sPartidaPresupuestaria As String

Public m_sPres As String
Public sParCon1 As String
Public sParCon2 As String
Public sParCon3 As String
Public sParCon4 As String
Public sDen As String
Public m_bajaLog As Boolean



Private Sub cmdBorrar_Click()
lblParCon.caption = ""
sParCon1 = ""
sParCon2 = ""
sParCon3 = ""
sParCon4 = ""

CargarNivelesDeDesglose (1)
End Sub

Private Sub cmdObtener_Click()
   ''' * Objetivo: Obtener un listado de Presupuestos por partidas contables
      
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    Dim SelectionTextRpt(1 To 2, 1 To 2) As String
    Dim oReport As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim pv As Preview
    Dim i As Integer
    Dim sNivel As String
    Dim Table As CRAXDRT.DatabaseTable
    Dim SubListado As CRAXDRT.Report
    Dim adoresDG As ADODB.Recordset
    Dim sPres0, sDenPres0, sArbol, sFiltro As String
    
    
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
           oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPRESPorPres5.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Set oReport = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    '*********** FORMULA FIELDS REPORT
    

    If Me.sdbcPartidaPres.Text <> "" Then
        m_bSaltar = True
        sDenPres0 = sdbcPartidaPres.Text
        sPres0 = sdbcPartidaPres.Value
        sdbcPartidaPres.Text = sDenPres0
        m_bSaltar = False
        sArbol = m_sArbolPresupuestario & ":  "
        sFiltro = m_sFiltro
    Else
        sPres0 = ""
        sDenPres0 = ""
        sArbol = ""
        sFiltro = ""
    End If
               
    'Nivel
    SelectionTextRpt(2, 1) = "NIVEL"
    SelectionTextRpt(1, 1) = cmbOblPC.Text
    sNivel = SelectionTextRpt(1, 1)
    
    'Titulo
    SelectionTextRpt(2, 2) = "TITULO"
    SelectionTextRpt(1, 2) = m_sTituloInf
    
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCodigo")).Text = """" & m_sCodigo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDenominacion")).Text = """" & m_sDenominacion & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFiltro")).Text = """" & sFiltro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtArbol")).Text = """" & sArbol & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFiltroDenPRES0")).Text = """" & sDenPres0 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBaja")).Text = """" & m_sBaja & """"
    

    Set adoresDG = oGestorInformes.ListadoParaPresCon5(sPres0, sNivel, sParCon1, sParCon2, sParCon3, sParCon4, opOrdDen, opOrdCod, CBool(chkBajaLog.Value))     ', m_sCodigo, m_sDenominacion, m_sBaja

   

   
   
'   For Each Table In oReport.Database.Tables
'        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
'    Next
    
    Select Case sNivel
        Case "1"
            Set SubListado = oReport.OpenSubreport("prtPROY1")
        Case "2"
            Set SubListado = oReport.OpenSubreport("rptPROY2")
        Case "3"
            Set SubListado = oReport.OpenSubreport("rptPROY3")
        Case "4"
            Set SubListado = oReport.OpenSubreport("rptPROY4")
    End Select

    '    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFiltro")).Text = """" & sFiltro & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCodigo")).Text = """" & m_sCodigo & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDenominacion")).Text = """" & m_sDenominacion & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtBaja")).Text = """" & m_sBaja & """"
    
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
    Next
    
    If Not adoresDG Is Nothing Then
        SubListado.Database.SetDataSource adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    Me.Hide
    
    frmESPERA.lblGeneral.caption = m_sSeleccionando & " - " & m_sPartidaPresupuestaria
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = m_sSeleccionando
    frmESPERA.lblDetalle = m_sVisualizando
    
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    
    pv.caption = m_sTituloInf
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
End Sub

Private Sub cmdSelCon5_Click()
    m_bSaltar = True
    Dim sDen As String
    
    If Me.sdbcPartidaPres.Value <> "" Then
        If Not m_oPres0 Is Nothing Then
            sDen = m_oPres0.Den
        End If
        frmSelCon5.m_sPres0 = sdbcPartidaPres.Value
        'sdbcPartidaPres.Value = m_oPres0.Cod
        sdbcPartidaPres.Text = sDen
        frmSelCon5.m_bVerBajaLog = chkBajaLog.Value
        frmSelCon5.Show 1
        
    End If
    m_bSaltar = False
End Sub

Private Sub Form_Load()
    CargarRecursos
    CargarSeguridad
    

    If sParCon4 <> "" Then
        lblParCon.caption = sParCon1 & " - " & sParCon2 & " - " & sParCon3 & " - " & sParCon4 & sDen
        CargarNivelesDeDesglose 4
        cmbOblPC.Text = 4
    ElseIf sParCon3 <> "" Then
        lblParCon.caption = sParCon1 & " - " & sParCon2 & " - " & sParCon3 & sDen
        CargarNivelesDeDesglose 3
        cmbOblPC.Text = 3
    ElseIf sParCon2 <> "" Then
        lblParCon.caption = sParCon1 & " - " & sParCon2 & sDen
        CargarNivelesDeDesglose 2
        cmbOblPC.Text = 2
    ElseIf sParCon1 <> "" Then
        lblParCon.caption = sParCon1 & sDen
        CargarNivelesDeDesglose 1
        cmbOblPC.Text = 1
    Else
        CargarNivelesDeDesglose 1
        cmbOblPC.Text = 1
    End If
    
    If m_sPres <> "" Then
        m_bSaltar = True
        sdbcPartidaPres.Value = m_sPres
        PartidaSeleccionada m_sPres
        sdbcPartidaPres.Text = m_oPres0.Den
        m_bSaltar = False
       
        cmdSelCon5.Enabled = True
    Else
        cmdSelCon5.Enabled = False
    End If
    If m_bajaLog Then
        chkBajaLog.Value = vbChecked
    End If
End Sub


Private Sub CargarRecursos()
Dim sTitulo As String
Dim Ador As Ador.Recordset


' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LstPRESPorCon5, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        sTitulo = Ador(0).Value '1 Listado de partidas de control de aprovisionamiento
        Ador.MoveNext
        Me.caption = sTitulo & " " & Ador(0).Value '2 (Opciones)
        Ador.MoveNext
        Me.SSTab1.TabCaption(1) = Ador(0).Value '3 Selecci�n
        Ador.MoveNext
        lblFiltroArbol.caption = Ador(0).Value & ":" '4 Filtro por �rbol de partidas de control de aprovisionamiento
        Ador.MoveNext
        Me.lblNomPar.caption = Ador(0).Value & ":" '5 Partida presupuestaria
        m_sPartidaPresupuestaria = Ador(0).Value
        Ador.MoveNext
        Me.chkBajaLog.caption = Ador(0).Value '6 Incluir bajas l�gicas
        Ador.MoveNext
        lblOblPC.caption = Ador(0).Value & ":" '7 Nivel de desglose del listado
        Ador.MoveNext
        lblOrdenar.caption = Ador(0).Value & ":" '8 Ordenar por
        Ador.MoveNext
        Me.opOrdCod.caption = Ador(0).Value '9 C�digo
        m_sCodigo = Ador(0).Value
        Ador.MoveNext
        Me.opOrdDen.caption = Ador(0).Value '10 Denominaci�n
        m_sDenominacion = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value '11 Obtener
        Ador.MoveNext
        m_sTituloInf = Ador(0).Value '12 Listados de partidas presupuestarias
        Ador.MoveNext
        m_sFiltro = Ador(0).Value & ":"   '13 FILTRO
        Ador.MoveNext
        m_sArbolPresupuestario = Ador(0).Value  '14 �rbol presupuestario
        Ador.MoveNext
        m_sBaja = Ador(0).Value ' 15 Baja
        Ador.MoveNext
        m_sSeleccionando = Ador(0).Value '16 Seleccionando registros...
        Ador.MoveNext
        m_sVisualizando = Ador(0).Value '17 Visualizando registros...
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
End Sub
Private Sub Form_Unload(Cancel As Integer)
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    sdbcPartidaPres = ""
    m_bajaLog = False
    m_sPres = ""
End Sub

Private Sub sdbcPartidaPres_Change()
    If m_bSaltar Then Exit Sub
    m_bCargarComboDesde = True
    Set m_oPres0 = Nothing
End Sub

Private Sub sdbcPartidaPres_InitColumnProps()
    sdbcPartidaPres.DataFieldList = "Column 0"
    sdbcPartidaPres.DataFieldToDisplay = "Column 1"
End Sub


Private Sub sdbcPartidaPres_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyBack Then
        Set m_oPres0 = Nothing
        sdbcPartidaPres.Value = ""
        lblParCon.caption = ""
        sParCon1 = ""
        sParCon2 = ""
        sParCon3 = ""
        sParCon4 = ""
        m_bCargarComboDesde = False
        cmdSelCon5.Enabled = False
    End If
End Sub

Private Sub sdbcPartidaPres_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPartidaPres.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPartidaPres.Rows - 1
            bm = sdbcPartidaPres.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPartidaPres.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPartidaPres.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcPartidaPres_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oPres0 As cPresConcep5Nivel0
    
    Screen.MousePointer = vbHourglass
    
    Set m_oPartidas = Nothing
    Set m_oPartidas = oFSGSRaiz.Generar_CPresConceptos5Nivel0
    
    sdbcPartidaPres.RemoveAll
    
'    If m_bCargarComboDesde Then
'        m_oPartidas.CargarPresupuestosConceptos5 Trim(sdbcPartidaPres.Text), , , , , , , m_sUsu, m_sPerfilCod
'    Else
        m_oPartidas.CargarPresupuestosConceptos5 , , , , , , , , m_sUsu, m_sPerfilCod
    'End If
'
    For Each oPres0 In m_oPartidas
        sdbcPartidaPres.AddItem oPres0.Cod & Chr(9) & oPres0.Den
    Next
    
    Set oPres0 = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPartidaPres_CloseUp()
    If sdbcPartidaPres.Value = "" Then Exit Sub
    If sdbcPartidaPres = "" Then Exit Sub

    If Not m_oPres0 Is Nothing Then If sdbcPartidaPres.Value = m_oPres0.Cod Then Exit Sub
    PartidaSeleccionada sdbcPartidaPres.Value
    Me.lblParCon.caption = ""
    cmdSelCon5.Enabled = True
End Sub

Private Sub PartidaSeleccionada(ByVal sPres0 As String)
    
    Set m_oPres0 = oFSGSRaiz.Generar_CPresConcep5Nivel0
    m_oPres0.Cod = sPres0
    Set oIBaseDatos = m_oPres0
    oIBaseDatos.IniciarEdicion
    Set oIBaseDatos = Nothing

End Sub


Public Sub MostrarParConSeleccionada()
  'Dim iNivelDesglose As Integer
    
    If Not frmSelCon5.oPRES4 Is Nothing Then
        lblParCon.caption = frmSelCon5.oPRES4.Pres1 & " - " & frmSelCon5.oPRES4.Pres2 & " - " & frmSelCon5.oPRES4.Pres3 & " - " & frmSelCon5.oPRES4.Cod & " - " & frmSelCon5.oPRES4.Den
        sParCon1 = frmSelCon5.oPRES4.Pres1
        sParCon2 = frmSelCon5.oPRES4.Pres2
        sParCon3 = frmSelCon5.oPRES4.Pres3
        sParCon4 = frmSelCon5.oPRES4.Cod
        'iNivelDesglose = 4
        CargarNivelesDeDesglose (4)
        cmbOblPC.Text = 4
        Exit Sub
    End If
    
    If Not frmSelCon5.oPRES3 Is Nothing Then
        lblParCon.caption = frmSelCon5.oPRES3.Pres1 & " - " & frmSelCon5.oPRES3.Pres2 & " - " & frmSelCon5.oPRES3.Cod & " - " & frmSelCon5.oPRES3.Den
        sParCon1 = frmSelCon5.oPRES3.Pres1
        sParCon2 = frmSelCon5.oPRES3.Pres2
        sParCon3 = frmSelCon5.oPRES3.Cod
        sParCon4 = ""
'        iNivelDesglose = 3
        CargarNivelesDeDesglose (3)
        cmbOblPC.Text = 3
        Exit Sub
    End If
    
    If Not frmSelCon5.oPRES2 Is Nothing Then
        lblParCon.caption = frmSelCon5.oPRES2.Pres1 & " - " & frmSelCon5.oPRES2.Cod & " - " & frmSelCon5.oPRES2.Den
        sParCon1 = frmSelCon5.oPRES2.Pres1
        sParCon2 = frmSelCon5.oPRES2.Cod
        sParCon3 = ""
        sParCon4 = ""
'        iNivelDesglose = 2
        CargarNivelesDeDesglose (2)
        cmbOblPC.Text = 2
        Exit Sub
    End If
    
    If Not frmSelCon5.oPRES1 Is Nothing Then
        lblParCon.caption = frmSelCon5.oPRES1.Cod & " - " & frmSelCon5.oPRES1.Den
        sParCon1 = frmSelCon5.oPRES1.Cod
        sParCon2 = ""
        sParCon3 = ""
        sParCon4 = ""
'        iNivelDesglose = 1
        CargarNivelesDeDesglose (1)
        cmbOblPC.Text = 1
        Exit Sub
    End If
    
    lblParCon = ""
    sParCon1 = ""
    sParCon2 = ""
    sParCon3 = ""
    sParCon4 = ""
    
End Sub

Function CargarNivelesDeDesglose(iNivel As Integer)
    Dim i As Integer
    
    cmbOblPC.clear
    
    For i = 4 To iNivel Step -1
        If i = 0 Then Exit For
        cmbOblPC.AddItem i
    Next i

End Function

Private Sub CargarSeguridad()
    If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
        If (oUsuarioSummit.Acciones.Item(CStr(accionesdeseguridad.PRESConcepto5CrearArboles)) Is Nothing) Then
            'No tiene permiso para crear arboles presupuestarios
            'picNavigate1.Visible = False
            m_sUsu = oUsuarioSummit.Cod
        End If
        
        If Not oUsuarioSummit.Perfil Is Nothing Then m_sPerfilCod = oUsuarioSummit.Perfil.Cod
    End If
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub
