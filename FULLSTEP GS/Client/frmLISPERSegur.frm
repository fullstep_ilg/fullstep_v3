VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLISPERSegur 
   Caption         =   "Seguridad - Listados personalizados"
   ClientHeight    =   7635
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12015
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLISPERSegur.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   7635
   ScaleWidth      =   12015
   Begin VB.PictureBox picEdit 
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      Height          =   420
      Left            =   3960
      ScaleHeight     =   420
      ScaleWidth      =   2535
      TabIndex        =   19
      Top             =   6840
      Visible         =   0   'False
      Width           =   2535
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "DAceptar"
         Height          =   345
         Left            =   75
         TabIndex        =   21
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "DCancelar"
         Height          =   345
         Left            =   1200
         TabIndex        =   20
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.PictureBox picModif 
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      Height          =   420
      Left            =   120
      ScaleHeight     =   420
      ScaleWidth      =   2685
      TabIndex        =   16
      Top             =   6870
      Width           =   2685
      Begin VB.CommandButton cmdModificar 
         Caption         =   "DModificar"
         Height          =   345
         Left            =   75
         TabIndex        =   18
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "DListado"
         Height          =   345
         Left            =   1200
         TabIndex        =   17
         Top             =   30
         Width           =   1005
      End
   End
   Begin TabDlg.SSTab sstabListados 
      Height          =   7470
      Left            =   60
      TabIndex        =   0
      Top             =   45
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   13176
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabMaxWidth     =   3528
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Usuarios"
      TabPicture(0)   =   "frmLISPERSegur.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ssTabUsuarios"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Perfiles"
      TabPicture(1)   =   "frmLISPERSegur.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sstabPerfiles"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin TabDlg.SSTab ssTabUsuarios 
         Height          =   6915
         Left            =   0
         TabIndex        =   1
         Top             =   405
         Width           =   11625
         _ExtentX        =   20505
         _ExtentY        =   12197
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Usuarios por listados"
         TabPicture(0)   =   "frmLISPERSegur.frx":0182
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "sstabUsuList"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraUsu"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lstListUsuL"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Listados por usuarios"
         TabPicture(1)   =   "frmLISPERSegur.frx":019E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraList"
         Tab(1).Control(1)=   "tvwListUsu"
         Tab(1).ControlCount=   2
         Begin MSComctlLib.ListView lstListUsuL 
            Height          =   4875
            Left            =   660
            TabIndex        =   13
            Top             =   960
            Width           =   5340
            _ExtentX        =   9419
            _ExtentY        =   8599
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "DEN"
               Text            =   "Listados"
               Object.Width           =   8820
            EndProperty
         End
         Begin VB.Frame fraUsu 
            Caption         =   "Usuarios"
            Height          =   5745
            Left            =   6690
            TabIndex        =   5
            Top             =   480
            Width           =   4695
            Begin MSComctlLib.TreeView tvwUsuLisMod 
               Height          =   5265
               Left            =   210
               TabIndex        =   10
               Top             =   300
               Visible         =   0   'False
               Width           =   4350
               _ExtentX        =   7673
               _ExtentY        =   9287
               _Version        =   393217
               LabelEdit       =   1
               Style           =   7
               Checkboxes      =   -1  'True
               HotTracking     =   -1  'True
               ImageList       =   "ImageList1"
               Appearance      =   1
            End
            Begin MSComctlLib.TreeView tvwUsuLis 
               Height          =   5265
               Left            =   180
               TabIndex        =   9
               Top             =   300
               Width           =   4350
               _ExtentX        =   7673
               _ExtentY        =   9287
               _Version        =   393217
               LabelEdit       =   1
               Style           =   7
               HotTracking     =   -1  'True
               ImageList       =   "ImageList1"
               Appearance      =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraList 
            Caption         =   "Listados personalizados"
            Height          =   5655
            Left            =   -69690
            TabIndex        =   3
            Top             =   460
            Width           =   6165
            Begin VB.ListBox lstListUsu 
               Height          =   4545
               ItemData        =   "frmLISPERSegur.frx":01BA
               Left            =   240
               List            =   "frmLISPERSegur.frx":01BC
               TabIndex        =   11
               Top             =   690
               Width           =   5610
            End
            Begin VB.ListBox lstListUsuCH 
               Height          =   4335
               Left            =   300
               Style           =   1  'Checkbox
               TabIndex        =   4
               Top             =   780
               Visible         =   0   'False
               Width           =   5490
            End
            Begin MSComctlLib.TabStrip sstabListUsu 
               Height          =   5160
               Left            =   150
               TabIndex        =   26
               Top             =   270
               Width           =   5835
               _ExtentX        =   10292
               _ExtentY        =   9102
               _Version        =   393216
               BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
                  NumTabs         =   1
                  BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
                     ImageVarType    =   2
                  EndProperty
               EndProperty
            End
         End
         Begin MSComctlLib.TreeView tvwListUsu 
            Height          =   5595
            Left            =   -74880
            TabIndex        =   2
            Top             =   570
            Width           =   5070
            _ExtentX        =   8943
            _ExtentY        =   9869
            _Version        =   393217
            HideSelection   =   0   'False
            LabelEdit       =   1
            Style           =   7
            HotTracking     =   -1  'True
            ImageList       =   "ImageList1"
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComctlLib.TabStrip sstabUsuList 
            Height          =   5670
            Left            =   150
            TabIndex        =   28
            Top             =   540
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   10001
            _Version        =   393216
            BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
               NumTabs         =   1
               BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
                  ImageVarType    =   2
               EndProperty
            EndProperty
         End
      End
      Begin TabDlg.SSTab sstabPerfiles 
         Height          =   6900
         Left            =   -75000
         TabIndex        =   6
         Top             =   420
         Width           =   11625
         _ExtentX        =   20505
         _ExtentY        =   12171
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Perfiles por listados"
         TabPicture(0)   =   "frmLISPERSegur.frx":01BE
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "sstabPerfList"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "framePerfiles"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lstPerfiles"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "Listados por perfiles"
         TabPicture(1)   =   "frmLISPERSegur.frx":01DA
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "lstLisPerfiles"
         Tab(1).Control(1)=   "frameListPerfiles"
         Tab(1).ControlCount=   2
         Begin MSComctlLib.ListView lstPerfiles 
            Height          =   3795
            Left            =   570
            TabIndex        =   14
            Top             =   1320
            Width           =   5070
            _ExtentX        =   8943
            _ExtentY        =   6694
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "DEN"
               Text            =   "Listados"
               Object.Width           =   8820
            EndProperty
         End
         Begin VB.Frame frameListPerfiles 
            Caption         =   "Listados personalizados"
            Height          =   5715
            Left            =   -69720
            TabIndex        =   22
            Top             =   480
            Width           =   6195
            Begin VB.ListBox lstListLPerfiles 
               Height          =   4545
               Left            =   330
               TabIndex        =   23
               Top             =   780
               Width           =   5490
            End
            Begin VB.ListBox lstListLPerfilesCH 
               Height          =   4560
               ItemData        =   "frmLISPERSegur.frx":01F6
               Left            =   330
               List            =   "frmLISPERSegur.frx":01F8
               Style           =   1  'Checkbox
               TabIndex        =   24
               TabStop         =   0   'False
               Top             =   780
               Visible         =   0   'False
               Width           =   5520
            End
            Begin MSComctlLib.TabStrip sstabListPerf 
               Height          =   5160
               Left            =   180
               TabIndex        =   27
               Top             =   300
               Width           =   5835
               _ExtentX        =   10292
               _ExtentY        =   9102
               _Version        =   393216
               BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
                  NumTabs         =   1
                  BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
                     ImageVarType    =   2
                  EndProperty
               EndProperty
            End
         End
         Begin MSComctlLib.ListView lstLisPerfiles 
            Height          =   5625
            Left            =   -74850
            TabIndex        =   15
            Top             =   540
            Width           =   5070
            _ExtentX        =   8943
            _ExtentY        =   9922
            View            =   3
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            _Version        =   393217
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   1
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "PERF"
               Text            =   "Perfiles"
               Object.Width           =   8820
            EndProperty
         End
         Begin VB.Frame framePerfiles 
            Caption         =   "Perfiles"
            Height          =   5685
            Left            =   6660
            TabIndex        =   7
            Top             =   510
            Width           =   4755
            Begin VB.ListBox lstPerfLisCod 
               Height          =   1035
               Left            =   360
               TabIndex        =   25
               Top             =   720
               Visible         =   0   'False
               Width           =   735
            End
            Begin VB.ListBox lstPerfLis 
               Height          =   4740
               ItemData        =   "frmLISPERSegur.frx":01FA
               Left            =   180
               List            =   "frmLISPERSegur.frx":01FC
               TabIndex        =   12
               Top             =   300
               Width           =   4350
            End
            Begin VB.ListBox lstPerfLisCH 
               Height          =   4785
               ItemData        =   "frmLISPERSegur.frx":01FE
               Left            =   180
               List            =   "frmLISPERSegur.frx":0200
               Style           =   1  'Checkbox
               TabIndex        =   8
               TabStop         =   0   'False
               Top             =   300
               Visible         =   0   'False
               Width           =   4350
            End
         End
         Begin MSComctlLib.TabStrip sstabPerfList 
            Height          =   5670
            Left            =   150
            TabIndex        =   29
            Top             =   510
            Width           =   6225
            _ExtentX        =   10980
            _ExtentY        =   10001
            _Version        =   393216
            BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
               NumTabs         =   1
               BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
                  ImageVarType    =   2
               EndProperty
            EndProperty
         End
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLISPERSegur.frx":0202
            Key             =   "GrupoDeUsuarios"
            Object.Tag             =   "GrupoDeUsuarios"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLISPERSegur.frx":0554
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLISPERSegur.frx":0666
            Key             =   "Usuario"
            Object.Tag             =   "Usuario"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmLISPERSegur.frx":09B8
            Key             =   "Candado"
            Object.Tag             =   "Candado"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmLISPERSegur"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private bEdicion As Boolean

Private bCargando As Boolean
Private bModificado As Boolean

Private oUsuarios As CUsuarios

'Idiomas
Private sIdiPersonalizado As String
Private sIdiUsuarios As String

'Colecciones para la modificaci�n
Private oSeleccionados As CLispers
Private oDeseleccionados As CLispers
Private oExiste As CLispers

Private bMouseDown As Boolean
Private SelNode As MSComctlLib.node
Private bUsulist As Boolean

Private Function CargarTabListUsu(ByVal sUsu As String) As String
    Dim oUsuario As CUsuario
    Dim bUsuario As Boolean
    Dim sTabAnterior As String
    Dim bAccesoFSGS As Boolean
    Dim bAccesoFSEP As Boolean
    Dim bAccesoFSWS As Boolean
    Dim bAccesoFSQA As Boolean
    
    If sstabListUsu.Tabs.Count > 0 Then
        sTabAnterior = sstabListUsu.selectedItem.caption
    End If
    
    sstabListUsu.Tabs.clear
    
    Set oUsuario = oFSGSRaiz.generar_cusuario
    
    oUsuario.Cod = sUsu
    
    bUsuario = oUsuario.ExpandirUsuario
    
    If bUsuario Then
        If oUsuario.Perfil Is Nothing Then
            bAccesoFSGS = oUsuario.AccesoFSGS
            bAccesoFSEP = oUsuario.AccesoFSEP
            bAccesoFSWS = oUsuario.AccesoFSWS
            bAccesoFSQA = oUsuario.AccesoFSQA
        Else
            bAccesoFSGS = oUsuario.Perfil.FSGS
            bAccesoFSEP = oUsuario.Perfil.FSEP
            bAccesoFSWS = oUsuario.Perfil.FSPM
            bAccesoFSQA = oUsuario.Perfil.FSQA
        End If
        
        If bAccesoFSGS Then
            sstabListUsu.Tabs.Add , , "GS"
            If sTabAnterior = "" Then
                sTabAnterior = "GS"
            End If
        End If
        If bAccesoFSEP Then
            sstabListUsu.Tabs.Add , , "EP"
            If sTabAnterior = "" Then
                sTabAnterior = "EP"
            End If
        End If
        If bAccesoFSWS Then
            sstabListUsu.Tabs.Add , , "PM"
            If sTabAnterior = "" Then
                sTabAnterior = "PM"
            End If
        End If
        If bAccesoFSQA Then
            sstabListUsu.Tabs.Add , , "QA"
            If sTabAnterior = "" Then
                sTabAnterior = "QA"
            End If
        End If
    End If
        
    If sstabListUsu.Tabs.Count > 0 Then
        CargarTabListUsu = sstabListUsu.selectedItem.caption
    Else
        CargarTabListUsu = ""
    End If
End Function

Private Function CargarTabUsuList() As String
Dim sTabAnterior As String


    
If sstabUsuList.Tabs.Count > 0 Then
    sTabAnterior = sstabUsuList.selectedItem.caption
Else
    sTabAnterior = "GS"
End If

sstabUsuList.Tabs.clear


If gParametrosGenerales.gbAccesoFSGS <> TipoAccesoFSGS.SinAcceso Then
    sstabUsuList.Tabs.Add , , "GS"
End If
If gParametrosGenerales.gbPedidosAprov = True Then
    sstabUsuList.Tabs.Add , , "EP"
End If
If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then
    sstabUsuList.Tabs.Add , , "PM"
End If
If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
   sstabUsuList.Tabs.Add , , "QA"
End If

CargarTabUsuList = sTabAnterior

End Function


Private Function CargarTabPerfList() As String
Dim sTabAnterior As String
    
If sstabPerfList.Tabs.Count > 0 Then
    sTabAnterior = sstabPerfList.selectedItem.caption
Else
    sTabAnterior = "GS"
End If

sstabPerfList.Tabs.clear

If gParametrosGenerales.gbAccesoFSGS <> TipoAccesoFSGS.SinAcceso Then sstabPerfList.Tabs.Add , , "GS"
If gParametrosGenerales.gbPedidosAprov = True Then sstabPerfList.Tabs.Add , , "EP"
If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then sstabPerfList.Tabs.Add , , "PM"
If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then sstabPerfList.Tabs.Add , , "QA"

CargarTabPerfList = sTabAnterior

End Function
Private Function CargarTabListPerf() As String
    Dim sTabAnterior As String
    
    If sstabListPerf.Tabs.Count > 0 Then sTabAnterior = sstabListPerf.selectedItem.caption
        
    sstabListPerf.Tabs.clear
        
    If gParametrosGenerales.gbAccesoFSGS <> TipoAccesoFSGS.SinAcceso Then
        sstabListPerf.Tabs.Add , , "GS"
        If sTabAnterior = "" Then
            sTabAnterior = "GS"
        End If
    End If
    If gParametrosGenerales.gbPedidosAprov = True Then
        sstabListPerf.Tabs.Add , , "EP"
        If sTabAnterior = "" Then
            sTabAnterior = "EP"
        End If
    End If
    If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then
        sstabListPerf.Tabs.Add , , "PM"
        If sTabAnterior = "" Then
            sTabAnterior = "PM"
        End If
    End If
    If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
        sstabListPerf.Tabs.Add , , "QA"
        If sTabAnterior = "" Then
            sTabAnterior = "QA"
        End If
    End If
    
    
    If sstabListPerf.Tabs.Count > 0 Then
        sstabListPerf.selectedItem.Tag = sTabAnterior
        CargarTabListPerf = sstabListPerf.selectedItem.Tag
    Else
        CargarTabListPerf = ""
    End If

End Function


Private Sub cmdAceptar_Click()
    'Guarda los cambios en la BD
    GuardarBD
End Sub

Private Sub cmdCancelar_Click()
    Set oSeleccionados = Nothing
    Set oDeseleccionados = Nothing
    
    ModoConsulta
End Sub

Private Sub cmdlistado_Click()
    If sstabListados.Tab = 0 Then 'Usuarios
        If ssTabUsuarios.Tab = 0 Then  'Usuarios por listados
            frmLstLISPERUsu.bListUsu = False
            frmLstLISPERUsu.WindowState = vbNormal
            frmLstLISPERUsu.Show 1
            
        Else  'Listados por usuario
            frmLstLISPERUsu.bListUsu = True
            frmLstLISPERUsu.WindowState = vbNormal
            frmLstLISPERUsu.Show 1
        End If
        
    Else  'Perfiles
        If sstabPerfiles.Tab = 0 Then  'Perfiles por listado
            frmLstLISPERPerf.bListPerf = False
            frmLstLISPERPerf.WindowState = vbNormal
            frmLstLISPERPerf.Show 1
             
        Else  'Listados por perfiles
            frmLstLISPERPerf.bListPerf = True
            frmLstLISPERPerf.WindowState = vbNormal
            frmLstLISPERPerf.Show 1
        End If
        
    End If
End Sub

Private Sub cmdModificar_Click()
    ModoEdicion
    
    Set oSeleccionados = oFSGSRaiz.Generar_CLisPers
    Set oDeseleccionados = oFSGSRaiz.Generar_CLisPers
End Sub

Private Sub Form_Load()
    Me.Width = 10560
    Me.Height = 6075
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    Screen.MousePointer = vbHourglass
    
    GenerarEstructuraListados lstListUsuL
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LISPER_SEGUR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        sstabListados.TabCaption(0) = Ador(0).Value
        fraUsu.caption = Ador(0).Value
        sIdiUsuarios = Ador(0).Value
        Ador.MoveNext
        sstabListados.TabCaption(1) = Ador(0).Value
        framePerfiles.caption = Ador(0).Value
        lstLisPerfiles.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        ssTabUsuarios.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        ssTabUsuarios.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sstabPerfiles.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabPerfiles.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        Me.cmdListado.caption = Ador(0).Value
        
        Ador.MoveNext
        fraList.caption = Ador(0).Value
        frameListPerfiles.caption = Ador(0).Value
        lstListUsuL.ColumnHeaders.Item(1).Text = Ador(0).Value
        lstPerfiles.ColumnHeaders.Item(1).Text = Ador(0).Value
        
        Ador.MoveNext
        sIdiPersonalizado = Ador(0).Value
        
        Ador.Close
    
    End If

   Set Ador = Nothing

End Sub

Private Sub Arrange()
    'Redimensiona el formulario
    If Me.Width < 2000 Then Exit Sub
    If Me.Height < 2000 Then Exit Sub
    
    sstabListados.Width = Me.Width - 255
    sstabListados.Height = Me.Height - 555
    
    sstabPerfiles.Height = sstabListados.Height - 585
    sstabPerfiles.Width = sstabListados.Width - 210
    
    ssTabUsuarios.Height = sstabListados.Height - 585
    ssTabUsuarios.Width = sstabListados.Width - 210
    
    'Botones
    picModif.Top = ssTabUsuarios.Height - 15
    picEdit.Top = ssTabUsuarios.Height - 15
    picEdit.Left = ssTabUsuarios.Width / 2 - 1000
   
    'Usuarios por listados
    sstabUsuList.Width = ssTabUsuarios.Width / 2
    sstabUsuList.Height = ssTabUsuarios.Height - 1100
    lstListUsuL.Width = sstabUsuList.Width - 250
    lstListUsuL.Height = sstabUsuList.Height - 600
    lstListUsuL.Left = sstabUsuList.Left + 150
    lstListUsuL.ColumnHeaders(1).Width = lstListUsuL.Width - 69
    
    fraUsu.Height = ssTabUsuarios.Height - 1000
    fraUsu.Width = ssTabUsuarios.Width / 2 - 350
    fraUsu.Left = sstabUsuList.Width + 210

    tvwUsuLisMod.Height = fraUsu.Height - 460
    tvwUsuLisMod.Width = fraUsu.Width - 345
    tvwUsuLis.Height = fraUsu.Height - 460
    tvwUsuLis.Width = fraUsu.Width - 345
    
    'Listados por usuarios
    tvwListUsu.Width = ssTabUsuarios.Width / 2 + 23
    tvwListUsu.Height = ssTabUsuarios.Height - 1100
    
    fraList.Height = ssTabUsuarios.Height - 1060
    fraList.Width = ssTabUsuarios.Width / 2 - 352
    fraList.Left = tvwListUsu.Width + 210
    
    lstListUsu.Height = fraList.Height - 950
    lstListUsu.Width = fraList.Width - 420
    lstListUsuCH.Top = lstListUsu.Top
    lstListUsuCH.Left = lstListUsu.Left
    lstListUsuCH.Height = lstListUsu.Height
    lstListUsuCH.Width = lstListUsu.Width
    
    
    sstabListUsu.Height = lstListUsu.Height + 575
    sstabListUsu.Width = lstListUsu.Width + 160
    
    'Perfiles por listados
    sstabPerfList.Width = ssTabUsuarios.Width / 2
    sstabPerfList.Height = ssTabUsuarios.Height - 1100
    
    lstPerfiles.Width = sstabPerfList.Width - 250
    lstPerfiles.Height = sstabPerfList.Height - 600
    lstPerfiles.Left = sstabPerfList.Left + 150
    lstPerfiles.Top = sstabPerfList.Top + 500
    lstPerfiles.ColumnHeaders(1).Width = lstPerfiles.Width - 69
    
    framePerfiles.Height = ssTabUsuarios.Height - 1000
    framePerfiles.Width = ssTabUsuarios.Width / 2 - 350
    framePerfiles.Left = sstabPerfList.Width + 210
    
    lstPerfLis.Height = framePerfiles.Height - 400
    lstPerfLis.Width = framePerfiles.Width - 345
    lstPerfLisCH.Height = framePerfiles.Height - 400
    lstPerfLisCH.Width = framePerfiles.Width - 345
    
    'Listados por perfiles
    lstLisPerfiles.Width = sstabPerfiles.Width / 2 + 23
    lstLisPerfiles.Height = sstabPerfiles.Height - 1100
    

    frameListPerfiles.Height = sstabPerfiles.Height - 1060
    frameListPerfiles.Width = sstabPerfiles.Width / 2 - 352
    frameListPerfiles.Left = lstLisPerfiles.Width + 220

    lstListLPerfiles.Height = lstLisPerfiles.Height - 800
    lstListLPerfiles.Width = lstLisPerfiles.Width - 900
    lstListLPerfilesCH.Height = lstListLPerfiles.Height
    lstListLPerfilesCH.Width = lstListLPerfiles.Width
    
    sstabListPerf.Height = lstListLPerfiles.Height + 575
    sstabListPerf.Width = lstListLPerfiles.Width + 250
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub GenerarEstructuraUsuList(ByVal key As String)
    Dim Cod As Integer
    Dim Ador As Ador.Recordset
    Dim nodx As node
    Dim sDenPerfil As String
    Dim sProducto As String
        
    Screen.MousePointer = vbHourglass
    
    Cod = CInt(Mid(key, 2))  'C�digo del listado
    sProducto = CargarTabUsuList
        
    Set Ador = oGestorListadosPers.DevolverUsuariosPorListado(Cod, oUsuarioSummit.idioma, , sProducto)
    
    tvwUsuLis.Nodes.clear
    
    'Genera la estructura del �rbol
    Set nodx = tvwUsuLis.Nodes.Add(, , "Root", sIdiUsuarios, "GrupoDeUsuarios")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    Set nodx = Nothing
        
    'Carga el treview
    If Not Ador Is Nothing Then
        Ador.MoveFirst

        While Not Ador.EOF
            If Ador("DEN") Is Nothing Or IsNull(Ador("DEN")) Then
                sDenPerfil = " " & sIdiPersonalizado
            Else
                sDenPerfil = Ador("DEN")
                sDenPerfil = " (" & sDenPerfil & ")"
                
            End If
            Set nodx = tvwUsuLis.Nodes.Add("Root", tvwChild, "Cod" & Ador("COD"), Ador("COD") & sDenPerfil, "Usuario")
            Ador.MoveNext
        Wend
                
        Ador.Close
        Set Ador = Nothing
    End If
        
    'Se posiciona en el nodo ra�z del �rbol
    tvwUsuLis.Nodes("Root").Selected = True
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub GenerarEstructuraListados(ByVal lstListados As ListView)
    'Carga todos los listados personalizados
    Dim Ador As Ador.Recordset
    Dim sProducto As String
    
    Screen.MousePointer = vbHourglass
    
    'Limpia la lista de los listados
    lstListados.ListItems.clear
    
    If sstabListados.Tab = 0 Then
        sProducto = CargarTabUsuList
    Else
        sProducto = CargarTabPerfList
    End If
    
    If sProducto = "" Then sProducto = sstabUsuList.selectedItem.caption
    
    Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , sProducto)
    
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        
        While Not Ador.EOF
            lstListados.ListItems.Add , "L" & Ador("COD"), Ador("DEN"), , 2
            Ador.MoveNext
        Wend
        
        Ador.Close
        Set Ador = Nothing
        If lstListUsuL.ListItems.Count > 0 Then lstListUsuL_ItemClick lstListUsuL.selectedItem
        If lstPerfiles.ListItems.Count > 0 Then lstPerfiles_ItemClick lstPerfiles.selectedItem
    End If

    Screen.MousePointer = vbNormal
End Sub


Private Sub Form_Unload(Cancel As Integer)
    bEdicion = False
    Me.Visible = False
End Sub

Private Sub lstLisPerfiles_ItemClick(ByVal Item As MSComctlLib.listItem)
    If bEdicion Then
        GenerarEstructuraListPorPerfMod lstLisPerfiles.selectedItem.key
    Else
        GenerarEstructuraListPorPerf lstLisPerfiles.selectedItem.key
    End If
End Sub


Private Sub lstListLPerfilesCH_ItemCheck(Item As Integer)
    Dim intLis As Integer
    Dim strPerfil As String
    
    If bCargando Then Exit Sub
    
    If lstListLPerfilesCH.ListCount = 0 Then Exit Sub
    
    intLis = lstListLPerfilesCH.ItemData(Item)
    strPerfil = Mid(lstLisPerfiles.selectedItem.key, 2)
    
    If lstListLPerfilesCH.Selected(Item) = True Then
        'Selecciona el listado
        If Not oDeseleccionados.Item(intLis & "$" & strPerfil & "$" & CStr(sstabListPerf.selectedItem.Tag)) Is Nothing Then
            oDeseleccionados.Remove (intLis & "$" & strPerfil & "$" & CStr(sstabListPerf.selectedItem.Tag))
        End If
        
        'Comprueba si es un listado ya existente en PERF_LIS.En ese caso no lo a�ade a la
        'colecci�n
        If Not oExiste Is Nothing Then
            If oExiste.Item(intLis & "$" & strPerfil & "$" & CStr(sstabListPerf.selectedItem.Tag)) Is Nothing Then
                oSeleccionados.Add intLis, strPerfil, sstabListPerf.selectedItem.Tag
            End If
        Else
            oSeleccionados.Add intLis, strPerfil, sstabListPerf.selectedItem.Tag
        End If
        
    Else  'Deselecciona el listado
        If Not oSeleccionados.Item(intLis & "$" & strPerfil & "$" & CStr(sstabListPerf.selectedItem.Tag)) Is Nothing Then
            oSeleccionados.Remove (intLis & "$" & strPerfil & "$" & CStr(sstabListPerf.selectedItem.Tag))
        End If
        oDeseleccionados.Add intLis, strPerfil, sstabListPerf.selectedItem.Tag
    End If
    
    bModificado = True

End Sub

Private Sub lstListUsuCH_ItemCheck(Item As Integer)
    Dim intLis As Integer
    Dim strUsu As String
    
    If bCargando Then Exit Sub
    
    If lstListUsuCH.ListCount = 0 Then Exit Sub
    
    intLis = lstListUsuCH.ItemData(Item)
    strUsu = Mid(tvwListUsu.selectedItem.key, 4)
    
    
    If lstListUsuCH.Selected(Item) = True Then
        'Selecciona el listado
        If Not oDeseleccionados.Item(intLis & "$" & strUsu & "$" & CStr(sstabListUsu.selectedItem.caption)) Is Nothing Then
            oDeseleccionados.Remove (intLis & "$" & strUsu & "$" & CStr(sstabListUsu.selectedItem.caption))
        End If
        
        'Comprueba si es un listado ya existente en USU_LIS.En ese caso no lo a�ade a la colecci�n
        If Not oExiste Is Nothing Then
            If oExiste.Item(intLis & "$" & strUsu & "$" & CStr(sstabListUsu.selectedItem.caption)) Is Nothing Then
                oSeleccionados.Add intLis, strUsu, sstabListUsu.selectedItem.caption
            End If
        Else
            oSeleccionados.Add intLis, strUsu, sstabListUsu.selectedItem.caption
        End If
        
    Else  'Deselecciona el listado
        'Comprueba si la asignaci�n se ha realizado por p�rfil.En ese caso no deja desasignarlo
        Dim Ador As Ador.Recordset
        Set Ador = oGestorListadosPers.DevolverExisteListadoPorPerfil(intLis, tvwListUsu.selectedItem.Tag, sstabListUsu.selectedItem.caption)
        If Not Ador Is Nothing Then
            lstListUsuCH.Selected(Item) = True
            oMensajes.ImposibleDesasignarListado
            Ador.Close
            Set Ador = Nothing
            Exit Sub
        End If
        
        If Not oSeleccionados.Item(intLis & "$" & strUsu & "$" & CStr(sstabListUsu.selectedItem.caption)) Is Nothing Then
            oSeleccionados.Remove (intLis & "$" & strUsu & "$" & CStr(sstabListUsu.selectedItem.caption))
        End If
        oDeseleccionados.Add intLis, strUsu, sstabListUsu.selectedItem.caption
    End If
    
    bModificado = True
End Sub

Private Sub lstListUsuL_ItemClick(ByVal Item As MSComctlLib.listItem)
    If bEdicion Then  'Modificaci�n
        Set oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios(, , , , sstabUsuList.selectedItem.caption)
        If Not oUsuarios Is Nothing Then
            GenerarEstructuraUsuarios tvwUsuLisMod
            Set oUsuarios = Nothing
            GenerarEstructuraUsuListMod lstListUsuL.selectedItem.key
        End If
        
    Else  'Consulta
        If Not lstListUsuL.selectedItem Is Nothing Then
            GenerarEstructuraUsuList lstListUsuL.selectedItem.key
        End If
    End If
End Sub

Private Sub lstPerfiles_ItemClick(ByVal Item As MSComctlLib.listItem)
    If lstPerfiles.selectedItem Is Nothing Then Exit Sub
    If bEdicion Then
        'Carga todos los perfiles y selecciona los del listado
        GenerarEstructuraPerfilesPorListMod lstPerfiles.selectedItem.key
    Else
        'Carga los perfiles que tienen permisos para el informe seleccionado
        GenerarEstructuraPerfilesPorList lstPerfiles.selectedItem.key
    End If
End Sub


Private Sub lstPerfLisCH_ItemCheck(Item As Integer)
    Dim intLis As Integer
    Dim sProducto As String
    
    If bCargando Then Exit Sub
    
    If lstPerfLisCH.ListCount = 0 Then Exit Sub
    
    intLis = Mid(lstPerfiles.selectedItem.key, 2)
    sProducto = CargarTabPerfList
    
    If lstPerfLisCH.Selected(Item) = True Then
    
        'Selecciona el perfil
        If Not oDeseleccionados.Item(intLis & "$" & lstPerfLisCod.List(Item) & "$" & sProducto) Is Nothing Then
            oDeseleccionados.Remove (intLis & "$" & lstPerfLisCod.List(Item) & "$" & sProducto)
        End If
        
        'Comprueba si es un perfil ya existente en PERF_LIS.En ese caso no lo a�ade a la colecci�n
        If Not oExiste Is Nothing Then
            If oExiste.Item(intLis & "$" & lstPerfLisCod.List(Item) & "$" & sProducto) Is Nothing Then
                oSeleccionados.Add intLis, lstPerfLisCod.List(Item), sProducto
            End If
        Else
            oSeleccionados.Add intLis, lstPerfLisCod.List(Item), sProducto
        End If
        
    Else  'Deselecciona el perfil
        If Not oSeleccionados.Item(intLis & "$" & lstPerfLisCod.List(Item) & "$" & sProducto) Is Nothing Then
            oSeleccionados.Remove (intLis & "$" & lstPerfLisCod.List(Item) & "$" & sProducto)
        End If
        oDeseleccionados.Add intLis, lstPerfLisCod.List(Item), sProducto
    End If
    
    bModificado = True
    
End Sub

Private Sub sstabListados_Click(PreviousTab As Integer)
    If sstabListados.Tab = 0 Then   'Usuarios
        ssTabUsuarios.Tab = 0
        
        GenerarEstructuraListados lstListUsuL
        If lstListUsuL.ListItems.Count > 0 Then
            lstListUsuL_ItemClick lstListUsuL.selectedItem
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
            tvwUsuLisMod.Nodes.clear
        End If
        
    Else 'Perfiles
        sstabPerfiles.Tab = 0
        Me.framePerfiles.Visible = True
        Me.frameListPerfiles.Visible = False
        
        GenerarEstructuraListados lstPerfiles
        If lstPerfiles.ListItems.Count > 0 Then
            lstPerfiles_ItemClick lstPerfiles.selectedItem
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
        End If
    End If
End Sub


Private Sub sstabListPerf_Click()
Dim Ador As Ador.Recordset
    If bEdicion Then
        GenerarEstructuraListPorPerfMod lstLisPerfiles.selectedItem.key
    Else
        GenerarEstructuraListPorPerf lstLisPerfiles.selectedItem.key
    End If
    If lstListLPerfiles.ListCount > 0 Then
        cmdModificar.Enabled = True
    Else
        Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , sstabListPerf.selectedItem.caption)
        If Not Ador Is Nothing Then
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
        End If
    End If
End Sub

Private Sub sstabListUsu_Click()
    Dim Ador As Ador.Recordset
    
    If bEdicion Then
        GenerarEstructuraListUsuMod tvwListUsu.selectedItem
    Else
        GenerarEstructuraListUsu tvwListUsu.selectedItem
    End If
    If lstListUsu.ListCount > 0 Then
        cmdModificar.Enabled = True
    Else
        Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , sstabListUsu.selectedItem.caption)
        If Not Ador Is Nothing Then
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
        End If
    End If
End Sub

Private Sub sstabPerfiles_Click(PreviousTab As Integer)
Dim Ador As Ador.Recordset
    If sstabPerfiles.Tab = 0 Then
        'Perfiles por listados
        GenerarEstructuraListados lstPerfiles
        If lstPerfiles.ListItems.Count > 0 Then
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
        End If
        
        Me.framePerfiles.Visible = True
        Me.frameListPerfiles.Visible = False
        
        
    Else
        'Listados por perfiles
        GenerarEstructuraPerfiles
        
        Me.framePerfiles.Visible = False
        Me.frameListPerfiles.Visible = True
        If lstListLPerfiles.ListCount > 0 Then
            cmdModificar.Enabled = True
        Else
            Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , sstabListPerf.selectedItem.caption)
            If Not Ador Is Nothing Then
                cmdModificar.Enabled = True
            Else
                cmdModificar.Enabled = False
            End If
        End If
    End If
    
    If lstListUsuL.selectedItem Is Nothing Then Exit Sub
    
    If lstLisPerfiles.ListItems.Count > 0 Then GenerarEstructuraListPorPerf lstLisPerfiles.selectedItem.key
    If lstPerfiles.ListItems.Count > 0 Then GenerarEstructuraPerfilesPorList lstPerfiles.selectedItem.key
End Sub

    
Private Sub sstabPerfList_Click()
    GenerarEstructuraListados lstPerfiles
    If lstPerfiles.ListItems.Count > 0 Then
        lstPerfiles_ItemClick lstPerfiles.selectedItem
        cmdModificar.Enabled = True
    Else
        cmdModificar.Enabled = False
        lstPerfiles.ListItems.clear
        lstPerfLisCod.clear
        lstPerfLis.clear
        lstPerfLisCH.clear
    End If
End Sub


Private Sub ssTabUsuarios_Click(PreviousTab As Integer)
    If ssTabUsuarios.Tab = 0 Then
        'Usuarios por listados
        GenerarEstructuraListados lstListUsuL
        
        If lstListUsuL.ListItems.Count > 0 Then
            lstListUsuL_ItemClick lstListUsuL.selectedItem
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
        End If
        Me.fraUsu.Visible = True
        Me.fraList.Visible = False
        lstListUsu.clear
    Else
        'Listados por usuarios
        
        Set oUsuarios = oGestorSeguridad.DevolverTodosLosUsuarios
        GenerarEstructuraUsuarios Me.tvwListUsu
        Set oUsuarios = Nothing
        
        If Me.tvwListUsu.Nodes.Count > 0 Then
            tvwListUsu_NodeClick tvwListUsu.Nodes.Item(2)
            tvwListUsu.Nodes.Item(2).Selected = True
        End If
        cmdModificar.Enabled = True
               
        Me.fraUsu.Visible = False
        Me.fraList.Visible = True
    End If
    
End Sub


Public Sub GenerarEstructuraUsuarios(ByVal treArbol As TreeView)

Dim oUsuario As CUsuario
Dim nodx As node
Dim sDenPerfil As String

Screen.MousePointer = vbHourglass

'Limpia el �rbol
treArbol.Nodes.clear

Set nodx = treArbol.Nodes.Add(, , "Root", sIdiUsuarios, "GrupoDeUsuarios")
nodx.Tag = "Raiz"
nodx.Expanded = True

If Not oUsuarios Is Nothing Then
    
    For Each oUsuario In oUsuarios
        
        If oUsuario.Perfil Is Nothing Then
            sDenPerfil = " " & sIdiPersonalizado

        Else
            sDenPerfil = oUsuario.Perfil.Den
            sDenPerfil = " (" & sDenPerfil & ")"
        End If
       
        Set nodx = treArbol.Nodes.Add("Root", tvwChild, "Cod" & oUsuario.Cod, oUsuario.Cod & sDenPerfil, "Usuario")
        If Not oUsuario.Perfil Is Nothing Then
            nodx.Tag = oUsuario.Perfil.Cod
        End If
    Next
End If

Set nodx = Nothing

Screen.MousePointer = vbNormal

End Sub


Private Sub sstabUsuList_Click()
    GenerarEstructuraListados lstListUsuL
    If lstListUsuL.ListItems.Count > 0 Then
        lstListUsuL_ItemClick lstListUsuL.selectedItem
        cmdModificar.Enabled = True
    Else
        cmdModificar.Enabled = False
        tvwUsuLis.Nodes.clear
        tvwUsuLisMod.Nodes.clear
    End If
End Sub

Private Sub tvwListUsu_NodeClick(ByVal node As MSComctlLib.node)
Dim Ador As Ador.Recordset
    If bEdicion Then
        GenerarEstructuraListUsuMod node
    Else
        GenerarEstructuraListUsu node
    End If
    If lstListUsu.ListCount > 0 Then
        cmdModificar.Enabled = True
    Else
        Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , sstabListUsu.selectedItem.caption)
        If Not Ador Is Nothing Then
            cmdModificar.Enabled = True
        Else
            cmdModificar.Enabled = False
        End If
    End If
End Sub


Private Sub GenerarEstructuraListUsu(ByVal node As node)
    Dim sUsu As String
    Dim Ador As Ador.Recordset
    Dim sProducto As String
    
    'Limpia la lista de los informes
    lstListUsu.clear
    
    'Al posicionarse en un usuario obtenemos los listados personalizados
    'asociados a dicho usuario
        
    Screen.MousePointer = vbHourglass
    
    
    Select Case node.Tag
        Case "Raiz"
            Screen.MousePointer = vbNormal
            Exit Sub
        Case ""
            sUsu = Mid(node.key, 4)
            sProducto = CargarTabListUsu(sUsu)
            Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(sUsu, , , sProducto)
        Case Else
            sUsu = Mid(node.key, 4)
            sProducto = CargarTabListUsu(sUsu)
            Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(sUsu, node.Tag, , sProducto)
    End Select
        
    If Not Ador Is Nothing Then
        While Not Ador.EOF
            lstListUsu.AddItem Ador("DEN")
            Ador.MoveNext
        Wend
    
        Ador.Close
        Set Ador = Nothing
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub GenerarEstructuraListUsuMod(ByVal node As node)
    'Al posicionarse en un usuario obtenemos los listados personalizados
    'asociados a dicho usuario
    
    Dim sUsu As String
    Dim Ador As Ador.Recordset
    Dim adorUsu As Ador.Recordset
    Dim i As Integer
    Dim sProducto As String
    
    
    'Limpia la lista de los informes
    Me.lstListUsuCH.clear
    
    'si se posiciona en los nodos raices no hace nada
    If node.Tag = "Raiz" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    bCargando = True
    
    sUsu = Mid(node.key, 4)
    sProducto = CargarTabListUsu(sUsu)
    
    'Primero carga todos los listados personalizados existentes
    Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , sProducto)
    
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        i = 0
        
        While Not Ador.EOF
            lstListUsuCH.AddItem Ador("DEN")
            lstListUsuCH.ItemData(lstListUsuCH.NewIndex) = Ador("COD")
            
            Ador.MoveNext
        Wend
    
        Ador.Close
        Set Ador = Nothing
        
        
        'Marca los listados asociados al usuario
        Select Case node.Tag
            Case ""
                sUsu = Mid(node.key, 4)
                Set adorUsu = oGestorListadosPers.DevolverListadosPersonalizados(sUsu, , , sProducto)
            Case Else
                sUsu = Mid(node.key, 4)
                Set adorUsu = oGestorListadosPers.DevolverListadosPersonalizados(sUsu, node.Tag, , sProducto)
        End Select
        
        Set oExiste = Nothing
        If Not adorUsu Is Nothing Then
            adorUsu.MoveFirst
            Set oExiste = oFSGSRaiz.Generar_CLisPers
            
            While Not adorUsu.EOF
                For i = 0 To lstListUsuCH.ListCount - 1
                    If lstListUsuCH.ItemData(i) = adorUsu("COD") Then
                        lstListUsuCH.Selected(i) = True
                        oExiste.Add lstListUsuCH.ItemData(i), Mid(node.key, 4)
                        Exit For
                    End If
                Next i
                adorUsu.MoveNext
            Wend
            
            adorUsu.Close
            Set adorUsu = Nothing
        End If
        
        'Marca los listados de la colecci�n
        If bModificado Then
            For i = 0 To lstListUsuCH.ListCount - 1
                If Not oSeleccionados.Item(lstListUsuCH.ItemData(i) & "$" & Mid(node.key, 4) & "$" & sstabListUsu.selectedItem.caption) Is Nothing Then
                    lstListUsuCH.Selected(i) = True
                End If
                
                If Not oDeseleccionados.Item(lstListUsuCH.ItemData(i) & "$" & Mid(node.key, 4) & "$" & sstabListUsu.selectedItem.caption) Is Nothing Then
                    lstListUsuCH.Selected(i) = False
                End If
            Next i
        End If
        
    End If
    bUsulist = False
    bCargando = False
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub GenerarEstructuraPerfilesPorList(ByVal key As String)
    Dim Cod As Integer
    Dim Ador As Ador.Recordset
    Dim sProducto As String

    Screen.MousePointer = vbHourglass
    
    Cod = CInt(Mid(key, 2))  'C�digo del listado
    
    sProducto = CargarTabPerfList
    
    Set Ador = oGestorListadosPers.DevolverUsuariosPorListado(Cod, oUsuarioSummit.idioma, True, sProducto)
    
    lstPerfLis.clear
    
    'Carga la lista de los perfiles
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        While Not Ador.EOF
            lstPerfLis.AddItem Ador("DEN")
            
            Ador.MoveNext
        Wend
        
        Ador.Close
        Set Ador = Nothing
    End If

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub GenerarEstructuraPerfiles()
    'Carga todos los perfiles existentes en la aplicaci�n
    Dim oPerfiles As CPerfiles
    Dim i As Integer
    
    'Limpia la lista de los listados
    lstLisPerfiles.ListItems.clear
    
    Screen.MousePointer = vbHourglass
    
    Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(, , , True)
    
    For i = 1 To oPerfiles.Count
        lstLisPerfiles.ListItems.Add , "P" & oPerfiles.Item(i).Cod, oPerfiles.Item(i).Den, , 3
    Next i
    
    Set oPerfiles = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub GenerarEstructuraListPorPerf(ByVal key As String)
    
    Dim Ador As Ador.Recordset
    Dim sProducto As String

    Screen.MousePointer = vbHourglass
    
    'Limpia la lista
    lstListLPerfiles.clear
    lstListLPerfilesCH.clear
    
    
    key = Mid(key, 2)  'C�digo del listado
    
    sProducto = CargarTabListPerf
    
    Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, key, , sProducto)
    
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        
        While Not Ador.EOF
            lstListLPerfiles.AddItem Ador("DEN")
            Ador.MoveNext
        Wend
        
        Ador.Close
        Set Ador = Nothing
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub ModoEdicion()
    bEdicion = True
    
    'Bloquea los tabs
    sstabListados.TabEnabled(0) = False
    sstabListados.TabEnabled(1) = False
    ssTabUsuarios.TabEnabled(0) = False
    ssTabUsuarios.TabEnabled(1) = False
    sstabPerfiles.TabEnabled(0) = False
    sstabPerfiles.TabEnabled(1) = False
    
    picModif.Visible = False
    picEdit.Visible = True
    
    If sstabListados.Tab = 0 Then  'Usuarios
        If ssTabUsuarios.Tab = 0 Then
            tvwUsuLisMod.Visible = True
            tvwUsuLis.Visible = False
            lstListUsuL_ItemClick lstListUsuL.selectedItem
            
        Else
            lstListUsuCH.Visible = True
            lstListUsu.Visible = False
            If Not tvwListUsu.selectedItem Is Nothing Then
                tvwListUsu_NodeClick tvwListUsu.selectedItem
            End If
        End If
        
    Else  'Perfiles
        If sstabPerfiles.Tab = 0 Then
            lstPerfLisCH.Visible = True
            lstPerfLis.Visible = False
            lstPerfiles_ItemClick lstPerfiles.selectedItem
        Else
            lstListLPerfilesCH.Visible = True
            lstListLPerfiles.Visible = False
            lstLisPerfiles_ItemClick lstLisPerfiles.selectedItem
        End If
    End If
    
End Sub

Private Sub ModoConsulta()
    bEdicion = False
    bModificado = False
    
    'Bloquea los tabs
    sstabListados.TabEnabled(0) = True
    sstabListados.TabEnabled(1) = True
    ssTabUsuarios.TabEnabled(0) = True
    ssTabUsuarios.TabEnabled(1) = True
    sstabPerfiles.TabEnabled(0) = True
    sstabPerfiles.TabEnabled(1) = True
    
    
    picModif.Visible = True
    picEdit.Visible = False
    
    If sstabListados.Tab = 0 Then  'Usuarios
        If ssTabUsuarios.Tab = 0 Then
            tvwUsuLis.Visible = True
            tvwUsuLisMod.Visible = False
            lstListUsuL_ItemClick lstListUsuL.selectedItem
            
        Else
            lstListUsu.Visible = True
            lstListUsuCH.Visible = False
            If Not tvwListUsu.selectedItem Is Nothing Then
                tvwListUsu_NodeClick tvwListUsu.selectedItem
            End If
        End If
        
    Else  'Perfiles
        If sstabPerfiles.Tab = 0 Then
            lstPerfLis.Visible = True
            lstPerfLisCH.Visible = False
            lstPerfiles_ItemClick lstPerfiles.selectedItem
            
        Else
            lstListLPerfiles.Visible = True
            lstListLPerfilesCH.Visible = False
            lstLisPerfiles_ItemClick lstLisPerfiles.selectedItem
        End If
    End If
    
    
    
End Sub


Private Sub GuardarBD()
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    Select Case sstabListados.Tab
      Case 0  'Usuarios
            'A�ade a USU_LIS
            For i = 1 To oSeleccionados.Count
                teserror = oGestorListadosPers.A�adirListadoAUsuario(oSeleccionados.Item(i).Lis, oSeleccionados.Item(i).PerfLis, oSeleccionados.Item(i).Producto, bUsulist)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
            Next i

            'Elimina de USU_LIS
            For i = 1 To oDeseleccionados.Count
                teserror = oGestorListadosPers.EliminarListadoDeUsuario(oDeseleccionados.Item(i).Lis, oDeseleccionados.Item(i).PerfLis, oDeseleccionados.Item(i).Producto, bUsulist)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
            Next i
        
      Case 1  'Perfiles
            'A�ade a PERF_LIS
            For i = 1 To oSeleccionados.Count
                teserror = oGestorListadosPers.A�adirListadoAPerfil(oSeleccionados.Item(i).Lis, oSeleccionados.Item(i).PerfLis, oSeleccionados.Item(i).Producto)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
            Next i
            
            'Elimina de PERF_LIS
            For i = 1 To oDeseleccionados.Count
                teserror = oGestorListadosPers.EliminarListadoDePerfil(oDeseleccionados.Item(i).Lis, oDeseleccionados.Item(i).PerfLis, oDeseleccionados.Item(i).Producto)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
            Next i
        
    End Select
    
    ModoConsulta
    
    Set oSeleccionados = Nothing
    Set oDeseleccionados = Nothing
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub GenerarEstructuraPerfilesPorListMod(ByVal key As String)
    Dim Cod As Integer
    Dim Ador As Ador.Recordset
    Dim oPerfiles As CPerfiles
    Dim i As Integer
    Dim sProducto As String
    
    Screen.MousePointer = vbHourglass
    
    'Limpia la lista de perfiles
    lstPerfLisCH.clear
    lstPerfLisCod.clear
    
    bCargando = True
    
    'Cargamos todos los perfiles existentes en la aplicaci�n
    Set oPerfiles = oGestorSeguridad.DevolverTodosLosPerfiles(, , , True)
    
    If oPerfiles Is Nothing Then
        Screen.MousePointer = vbNormal
        bCargando = False
        Exit Sub
    End If
    
    For i = 1 To oPerfiles.Count
        lstPerfLisCH.AddItem oPerfiles.Item(i).Den
        lstPerfLisCod.AddItem oPerfiles.Item(i).Cod
    Next i
    
    Set oPerfiles = Nothing
    
    Set oExiste = Nothing
    
    'A continuaci�n seleccionamos los perfiles que tienen acceso al listado seleccionado
    Cod = CInt(Mid(key, 2))  'C�digo del listado
    sProducto = CargarTabPerfList
    Set Ador = oGestorListadosPers.DevolverUsuariosPorListado(Cod, oUsuarioSummit.idioma, True, sProducto)
    
    If Not Ador Is Nothing Then
        Set oExiste = oFSGSRaiz.Generar_CLisPers
        
        Ador.MoveFirst
        
        Do While Not Ador.EOF
            For i = 0 To lstPerfLisCod.ListCount - 1
                If Ador("COD") = lstPerfLisCod.List(i) Then
                    lstPerfLisCH.Selected(i) = True
                    oExiste.Add Cod, lstPerfLisCod.List(i)
                    Exit For
                End If
            Next i
            
            Ador.MoveNext
        Loop
    
        Ador.Close
        Set Ador = Nothing
    End If
    
    'Marca o desmarca las de la colecci�n
    If bModificado Then
        For i = 0 To lstPerfLisCod.ListCount - 1
            If Not oSeleccionados.Item(Cod & "$" & lstPerfLisCod.List(i) & "$" & sstabPerfList.selectedItem.caption) Is Nothing Then
                lstPerfLisCH.Selected(i) = True
            End If
            
            If Not oDeseleccionados.Item(Cod & "$" & lstPerfLisCod.List(i) & "$" & sstabPerfList.selectedItem.caption) Is Nothing Then
                lstPerfLisCH.Selected(i) = False
            End If
        Next i
    End If
    
    bCargando = False
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub GenerarEstructuraListPorPerfMod(ByVal key As String)
    Dim Ador As Ador.Recordset
    Dim i As Integer
    Dim sProducto As String
    
    Screen.MousePointer = vbHourglass
    
    'Limpia la lista
    lstListLPerfilesCH.clear
    
    bCargando = True
    
    sProducto = CargarTabListPerf
    
    'Primero carga todos los listados personalizados existentes
    Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , , sProducto)
    
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        i = 0
        
        While Not Ador.EOF
            lstListLPerfilesCH.AddItem Ador("DEN")
            lstListLPerfilesCH.ItemData(lstListLPerfilesCH.NewIndex) = Ador("COD")
            
            Ador.MoveNext
        Wend

        'Marca los listados asociados al perfil
        MarcarListados (key)
        
        If bModificado Then
            MarcarListadosMod (key)
        End If
        
        Ador.Close
        Set Ador = Nothing
    End If
    
    bCargando = False
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub GenerarEstructuraUsuListMod(ByVal key As String)
    Dim Ador As Ador.Recordset
    Dim Cod As Integer
    Dim i As Integer
    Dim j As Integer
    Dim sProducto As String
    
        
    Screen.MousePointer = vbHourglass
    
    'Marca los usuarios asociados a un determinado listado
    Cod = CInt(Mid(key, 2))  'C�digo del listado
    sProducto = CargarTabUsuList

    Set oExiste = Nothing
    
    Set Ador = oGestorListadosPers.DevolverUsuariosPorListado(Cod, oUsuarioSummit.idioma, , sProducto)
    
    If sProducto = "GS" Then
        Cod = Cod & 1
    ElseIf sProducto = "EP" Then
        Cod = Cod & 2
    ElseIf sProducto = "PM" Then
        Cod = Cod & 3
    ElseIf sProducto = "QA" Then
        Cod = Cod & 4
    End If

    If Not Ador Is Nothing Then
        Ador.MoveFirst
        Set oExiste = oFSGSRaiz.Generar_CLisPers
        
        While Not Ador.EOF
            tvwUsuLisMod.Nodes.Item("Cod" & Ador("COD")).Checked = True
            oExiste.Add Cod, Mid(tvwUsuLisMod.Nodes.Item("Cod" & Ador("COD")).key, 4)
            Ador.MoveNext
        Wend
        
        Ador.Close
        Set Ador = Nothing
    End If

    'Marca los nodos existentes en la colecci�n
    If bModificado Then
        For i = 1 To oSeleccionados.Count
            If oSeleccionados.Item(i).Lis = Cod Then
                tvwUsuLisMod.Nodes.Item("Cod" & oSeleccionados.Item(i).PerfLis).Checked = True
            End If
        Next i
        
        For i = 1 To oDeseleccionados.Count
            If oDeseleccionados.Item(i).Lis = Cod Then
                tvwUsuLisMod.Nodes.Item("Cod" & oDeseleccionados.Item(i).PerfLis).Checked = False
            End If
        Next i
    End If
    
    'Marcar los padres si est�n marcados todos sus hijos
    For i = 2 To tvwUsuLisMod.Nodes(1).Children + 1
        If tvwUsuLisMod.Nodes(i).Children > 0 Then
            tvwUsuLisMod.Nodes(i).Checked = True
            j = tvwUsuLisMod.Nodes(i).Child.Index
            If tvwUsuLisMod.Nodes(j).Checked = False Then
                tvwUsuLisMod.Nodes(i).Checked = False
            End If
            Do While j <> tvwUsuLisMod.Nodes(i).Child.LastSibling.Index
                j = tvwUsuLisMod.Nodes(j).Next.Index
                If tvwUsuLisMod.Nodes(j).Checked = False Then
                    tvwUsuLisMod.Nodes(i).Checked = False
                    Exit Do
                End If
            Loop
        End If
    Next i
    
    bUsulist = True
    'Se posiciona en el nodo ra�z del �rbol
    tvwUsuLisMod.Nodes("Root").Selected = True
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub MarcarListados(ByVal key)
    Dim AdorPerf As Ador.Recordset
    Dim i As Integer
    
    'Comprueba si existen listados para el perfil seleccionado
    'En caso afirmativo se selecciona en la lista
    
    Set oExiste = Nothing
        
    key = Mid(key, 2)  'C�digo del listado
    Set AdorPerf = oGestorListadosPers.DevolverListadosPersonalizados(, key, , sstabListPerf.selectedItem.Tag)
    
    If Not AdorPerf Is Nothing Then
        Set oExiste = oFSGSRaiz.Generar_CLisPers
        
        AdorPerf.MoveFirst
        
        While Not AdorPerf.EOF
            
            For i = 0 To lstListLPerfilesCH.ListCount - 1
                If lstListLPerfilesCH.ItemData(i) = AdorPerf("COD") Then
                    lstListLPerfilesCH.Selected(i) = True
                    oExiste.Add lstListLPerfilesCH.ItemData(i), key
                    Exit For
                End If
            Next i
            
            AdorPerf.MoveNext
        Wend
        
        AdorPerf.Close
        Set AdorPerf = Nothing
    End If
    
End Sub

Private Sub MarcarListadosMod(ByVal key)
    Dim i As Integer
    
    'comprueba si existen listados para el perfil seleccionado
    'En caso afirmativo se selecciona en la lista
    
    key = Mid(key, 2)  'C�digo del listado
    
    For i = 0 To lstListLPerfilesCH.ListCount - 1
        If Not oSeleccionados.Item(lstListLPerfilesCH.ItemData(i) & "$" & key & "$" & sstabListPerf.selectedItem.caption) Is Nothing Then
            lstListLPerfilesCH.Selected(i) = True
        End If
        
        If Not oDeseleccionados.Item(lstListLPerfilesCH.ItemData(i) & "$" & key & "$" & sstabListPerf.selectedItem.caption) Is Nothing Then
            lstListLPerfilesCH.Selected(i) = False
        End If
    Next i
    
End Sub

Private Sub tvwUsuLisMod_KeyUp(KeyCode As Integer, Shift As Integer)
Dim Ador As Ador.Recordset
Dim j As Integer

    If bMouseDown Then

        bMouseDown = False

        If SelNode.Tag = "Raiz" Then
            If SelNode.Checked = True Then 'Marca todos los hijos
                'Marca todos los hijos
                MarcarTodosLosHijos
            Else
                'Desmarca todos los hijos
                DesmarcarTodosLosHijos
            End If

        Else  'Si es un nodo hijo
            'comprueba si se puede desasignar.Si la asignaci�n se ha realizado
            'por perfil no se podr�
            If SelNode.Checked = False Then
                Set Ador = oGestorListadosPers.DevolverExisteListadoPorPerfil(CInt(Mid(lstListUsuL.selectedItem.key, 2)), SelNode.Tag, sstabUsuList.selectedItem.caption)
                If Not Ador Is Nothing Then
                    SelNode.Checked = True
                    oMensajes.ImposibleDesasignarListado
                    Ador.Close
                    Set Ador = Nothing
                    Exit Sub
                End If
                
                SelNode.Parent.Checked = False
                EliminarDeColeccion (Mid(SelNode.key, 4))
                
            Else
                'A�ade a la colecci�n
                A�adirAColeccion (Mid(SelNode.key, 4))
                
                'Si todos los hijos est�n marcados marca el padre
                If SelNode.Parent.Children > 0 Then
                    SelNode.Parent.Checked = True
                    j = SelNode.Parent.Child.Index
                    If tvwUsuLisMod.Nodes(j).Checked = False Then
                        SelNode.Parent.Checked = False
                        Exit Sub
                    End If
                    Do While j <> SelNode.Child.LastSibling.Index
                        j = tvwUsuLisMod.Nodes(j).Next.Index
                        If tvwUsuLisMod.Nodes(j).Checked = False Then
                            SelNode.Parent.Checked = False
                            Exit Do
                        End If
                    Loop
                End If
            End If
        End If
    End If
End Sub

Private Sub tvwUsuLisMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nod As MSComctlLib.node
Dim tvhti As TVHITTESTINFO

    If (Button = vbLeftButton) Then
        'Determinar si se ha pulsado sobre el nodo
        Set nod = tvwUsuLisMod.HitTest(X, Y)
        If Not nod Is Nothing Then
            'Determinar si se ha pulsado sobre la checkbox

            tvhti.pt.X = X / Screen.TwipsPerPixelX
            tvhti.pt.Y = Y / Screen.TwipsPerPixelY
            If (SendMessage(tvwUsuLisMod.hWnd, TVM_HITTEST, 0, tvhti)) Then

                If (tvhti.FLAGS And TVHT_ONITEMSTATEICON) Then
                 'El estado del nodo cambiar� al finalizar este procedimiento

                bMouseDown = True
                Set SelNode = nod

                End If
            End If
        End If
    End If
End Sub

Private Sub tvwUsuLisMod_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Ador As Ador.Recordset
Dim j As Integer
Dim sProducto As String

    If bMouseDown Then

        bMouseDown = False

        If SelNode.Tag = "Raiz" Then
            If SelNode.Checked = True Then
                'Marca todos los hijos
                MarcarTodosLosHijos
            Else
                'Desmarca todos los hijos
                DesmarcarTodosLosHijos
            End If

        Else  'Si es un nodo hijo
            'comprueba si se puede desasignar.Si la asignaci�n se ha realizado
            'por perfil no se podr�
            If SelNode.Checked = False Then
                sProducto = CargarTabUsuList
                Set Ador = oGestorListadosPers.DevolverExisteListadoPorPerfil(CInt(Mid(lstListUsuL.selectedItem.key, 2)), SelNode.Tag, sProducto)
                If Not Ador Is Nothing Then
                    SelNode.Checked = True
                    oMensajes.ImposibleDesasignarListado
                    Ador.Close
                    Set Ador = Nothing
                    Exit Sub
                End If
                
                SelNode.Parent.Checked = False  'Quita la marca del padre
                EliminarDeColeccion (Mid(SelNode.key, 4))
            Else
                'A�ade a la colecci�n
                A�adirAColeccion (Mid(SelNode.key, 4))
                
                'Si todos los hijos est�n marcados marca el padre
                If SelNode.Parent.Children > 0 Then
                    SelNode.Parent.Checked = True
                    j = SelNode.Parent.Child.Index
                    If tvwUsuLisMod.Nodes(j).Checked = False Then
                        SelNode.Parent.Checked = False
                        Exit Sub
                    End If
                    Do While j <> SelNode.Parent.Child.LastSibling.Index
                        j = tvwUsuLisMod.Nodes(j).Next.Index
                        If tvwUsuLisMod.Nodes(j).Checked = False Then
                            SelNode.Parent.Checked = False
                            Exit Do
                        End If
                    Loop
                End If
            End If
            
        End If
        
    End If
End Sub

Private Sub tvwUsuLisMod_NodeCheck(ByVal node As MSComctlLib.node)
    
    If bMouseDown Then
        Exit Sub
    End If

    bMouseDown = True
    Set SelNode = node
        
End Sub

Private Sub A�adirAColeccion(ByVal strUsu As String)
    Dim intLis As Integer
    Dim sProducto As String
    
    intLis = CInt(Mid(lstListUsuL.selectedItem.key, 2))
    sProducto = CargarTabUsuList
    'Selecciona el listado
    
    If sProducto = "GS" Then
        intLis = intLis & 1
    ElseIf sProducto = "EP" Then
        intLis = intLis & 2
    ElseIf sProducto = "PM" Then
        intLis = intLis & 3
    ElseIf sProducto = "QA" Then
        intLis = intLis & 4
    End If
    If Not oDeseleccionados.Item(intLis & "$" & strUsu & "$" & sProducto) Is Nothing Then
        oDeseleccionados.Remove (intLis & "$" & strUsu & "$" & sProducto)
    End If
    
    'Comprueba si es un listado ya existente en USU_LIS.En ese caso no lo a�ade a la
    'colecci�n
    If Not oExiste Is Nothing Then
        If oExiste.Item(intLis & "$" & strUsu & "$" & sProducto) Is Nothing Then
            oSeleccionados.Add intLis, strUsu, sProducto
        End If
    Else
        oSeleccionados.Add intLis, strUsu, sProducto
    End If
        
    bModificado = True
    
End Sub

Private Sub EliminarDeColeccion(ByVal strUsu As String)
    Dim intLis As Integer
    Dim sProducto As String
    
    intLis = CInt(Mid(lstListUsuL.selectedItem.key, 2))
    
    sProducto = CargarTabUsuList
    If sProducto = "GS" Then
        intLis = intLis & 1
    ElseIf sProducto = "EP" Then
        intLis = intLis & 2
    ElseIf sProducto = "PM" Then
        intLis = intLis & 3
    ElseIf sProducto = "QA" Then
        intLis = intLis & 4
    End If
    
    'A�ade el usuario a la colecci�n para eliminarlo de USU_LIS
    If Not oSeleccionados.Item(intLis & "$" & strUsu & "$" & sProducto) Is Nothing Then
        oSeleccionados.Remove (intLis & "$" & strUsu & "$" & sProducto)
    End If
    oDeseleccionados.Add intLis, strUsu, sProducto
    
    bModificado = True
End Sub

Private Sub MarcarTodosLosHijos()
    Dim i As Integer

    'Marca todos los hijos de un tipo de usuario
    If SelNode.Children > 0 Then
        i = SelNode.Child.Index
        tvwUsuLisMod.Nodes(i).Checked = True
        A�adirAColeccion (Mid(SelNode.Child.key, 4))
        While i <> SelNode.Child.LastSibling.Index
             i = tvwUsuLisMod.Nodes(i).Next.Index
             If Not tvwUsuLisMod.Nodes(i).Checked Then
                 'lo a�ade a la colecci�n si no est� ya checked
                 tvwUsuLisMod.Nodes(i).Checked = True
                A�adirAColeccion (Mid(tvwUsuLisMod.Nodes(i).key, 4))
            End If
        Wend
    End If
End Sub

Private Sub DesmarcarTodosLosHijos()
    Dim i As Integer

    'Desmarca todos los hijos de un tipo de usuario
    If SelNode.Children > 0 Then
        i = SelNode.Child.Index
        If tvwUsuLisMod.Nodes(i).Checked Then
        'lo a�ade a la colecci�n si no est� ya descheckeado
            tvwUsuLisMod.Nodes(i).Checked = False
            If Not ListadoAsignadoporPerfil(tvwUsuLisMod.Nodes(i).Tag) Then
                'lo a�ade a la colecci�n
                EliminarDeColeccion (Mid(tvwUsuLisMod.Nodes(i).key, 4))
            Else
                tvwUsuLisMod.Nodes(i).Checked = True
            End If
        End If
        While i <> SelNode.Child.LastSibling.Index
            i = tvwUsuLisMod.Nodes(i).Next.Index
            If tvwUsuLisMod.Nodes(i).Checked Then
            'lo a�ade a la colecci�n si no est� ya descheckeado
                tvwUsuLisMod.Nodes(i).Checked = False
                If Not ListadoAsignadoporPerfil(tvwUsuLisMod.Nodes(i).Tag) Then
                    EliminarDeColeccion (Mid(tvwUsuLisMod.Nodes(i).key, 4))
                Else
                    tvwUsuLisMod.Nodes(i).Checked = True
                End If
            End If
        Wend

    End If
End Sub

Private Function ListadoAsignadoporPerfil(ByVal strPerf As String) As Boolean
    Dim Ador As Ador.Recordset
    
    Set Ador = oGestorListadosPers.DevolverExisteListadoPorPerfil(CInt(Mid(lstListUsuL.selectedItem.key, 2)), strPerf, sstabUsuList.selectedItem.caption)
    If Not Ador Is Nothing Then
        ListadoAsignadoporPerfil = True
        Ador.Close
        Set Ador = Nothing
        Exit Function
    Else
        ListadoAsignadoporPerfil = False
    End If
End Function
