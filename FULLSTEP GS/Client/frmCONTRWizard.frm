VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCONTRWizard 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Generaci�n de contrato"
   ClientHeight    =   4215
   ClientLeft      =   2085
   ClientTop       =   5220
   ClientWidth     =   7380
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCONTRWizard.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4215
   ScaleWidth      =   7380
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   2400
      TabIndex        =   10
      Top             =   3480
      Width           =   1095
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   ">>"
      Default         =   -1  'True
      Height          =   315
      Left            =   3720
      TabIndex        =   9
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   555
      Left            =   0
      TabIndex        =   5
      Top             =   2880
      Width           =   7365
      Begin VB.OptionButton opGrupo 
         Caption         =   "Seleccionar grupos"
         Height          =   315
         Left            =   2670
         TabIndex        =   8
         Top             =   120
         Width           =   2145
      End
      Begin VB.OptionButton opItem 
         Caption         =   "Seleccionar items"
         Height          =   315
         Left            =   5340
         TabIndex        =   7
         Top             =   120
         Width           =   2145
      End
      Begin VB.OptionButton opProce 
         Caption         =   "Proceso completo"
         Height          =   315
         Left            =   360
         TabIndex        =   6
         Top             =   120
         Value           =   -1  'True
         Width           =   2145
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcTipoCod 
      Height          =   285
      Left            =   1950
      TabIndex        =   3
      Top             =   1080
      Width           =   1335
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      Row.Count       =   3
      Col.Count       =   2
      Row(0).Col(0)   =   "ELEC"
      Row(0).Col(1)   =   "Contrato de Electricidad"
      Row(1).Col(0)   =   "CONS"
      Row(1).Col(1)   =   "Contrato de Construcci�n"
      Row(2).Col(0)   =   "COCH"
      Row(2).Col(1)   =   "Contrato de Alquiler de Veh�culos"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2170
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7276
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "Denominaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2355
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.TextBox txtNom 
      Height          =   285
      Left            =   1950
      TabIndex        =   0
      Top             =   630
      Width           =   5355
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcTipoDen 
      Height          =   285
      Left            =   3300
      TabIndex        =   4
      Top             =   1080
      Width           =   4005
      _Version        =   196617
      DataMode        =   2
      Row.Count       =   3
      Col.Count       =   2
      Row(0).Col(0)   =   "Contrato de Electricidad"
      Row(0).Col(1)   =   "ELEC"
      Row(1).Col(0)   =   "Contrato de Construcci�n"
      Row(1).Col(1)   =   "CONS"
      Row(2).Col(0)   =   "Contrato de Alquiler de Veh�culos"
      Row(2).Col(1)   =   "COCH"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5689
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "Denominaci�n"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1614
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "C�digo"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   7064
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcIdiomas 
      Height          =   285
      Left            =   3570
      TabIndex        =   12
      Top             =   1500
      Width           =   3735
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   6773
      Columns(0).Caption=   "DDenominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "OFFSET"
      Columns(2).Name =   "OFFSET"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcContacto 
      Height          =   285
      Left            =   1920
      TabIndex        =   14
      Top             =   2280
      Width           =   5355
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   10451
      Columns(0).Caption=   "Nombre"
      Columns(0).Name =   "NOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9446
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProveedores 
      Height          =   285
      Left            =   1920
      TabIndex        =   17
      Top             =   1920
      Width           =   5355
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   10451
      Columns(0).Caption=   "Nombre"
      Columns(0).Name =   "NOM"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9446
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblProveedor 
      Caption         =   "Proveedor:"
      ForeColor       =   &H80000007&
      Height          =   225
      Left            =   150
      TabIndex        =   16
      Top             =   1920
      Width           =   1635
   End
   Begin VB.Label lblContac 
      Caption         =   "Contacto  proveedor:"
      ForeColor       =   &H80000007&
      Height          =   225
      Left            =   150
      TabIndex        =   15
      Top             =   2280
      Width           =   1635
   End
   Begin VB.Label lblIdi 
      Caption         =   "Idioma para el documento de contrato:"
      ForeColor       =   &H80000007&
      Height          =   225
      Left            =   150
      TabIndex        =   13
      Top             =   1530
      Width           =   3285
   End
   Begin VB.Line Line3 
      BorderColor     =   &H00808080&
      BorderWidth     =   2
      X1              =   0
      X2              =   9330
      Y1              =   510
      Y2              =   510
   End
   Begin VB.Label lblSel 
      Caption         =   "Introduzca los datos para crear un contrato nuevo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   150
      TabIndex        =   11
      Top             =   90
      Width           =   6225
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00808080&
      BorderWidth     =   2
      X1              =   0
      X2              =   9480
      Y1              =   2760
      Y2              =   2760
   End
   Begin VB.Label lblTipo 
      Caption         =   "Tipo de contrato:"
      ForeColor       =   &H80000007&
      Height          =   225
      Left            =   150
      TabIndex        =   2
      Top             =   1110
      Width           =   1665
   End
   Begin VB.Label lblNom 
      Caption         =   "Nombre de contrato:"
      ForeColor       =   &H80000007&
      Height          =   225
      Left            =   150
      TabIndex        =   1
      Top             =   660
      Width           =   1665
   End
End
Attribute VB_Name = "frmCONTRWizard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public m_oProcesoSeleccionado As CProceso
Public g_oProveSeleccionado As CProveedor
Public g_oProveedores As CProveedores
Public g_sCodProveedor As String
Public g_sDenProveedor As String


Private m_sFormatoNumber As String
Private m_sComentario As String
Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean

Private m_oTiposContrato As CSolicitudes
Private m_oTipoSeleccionado As CSolicitud
Private m_oIdiomas  As CIdiomas
Private m_oProce As CProceso
Private m_oProve As CProveedor
Private m_oAtribsFormulas As CAtributos
Private m_oOferta As COferta
Private m_oAdjudicaciones As CAdjudicaciones
Private m_oAdjs As CAdjudicaciones 'son las adjudicaciones que se incluyen en el contrato
Private m_oProvesAsig As CProveedores
Public g_oGruposProce As CGrupos 'Grupos elegidos si seleccionan grupos
Public g_oItemsProce As CItems 'Grupos elegidos si seleccionan items
Public g_sCadenaItems As String
Private m_sCarpeta As String
Private m_sArchivo As String
Private g_sPlantilla As String
Private m_iAplicarTotalProce As Integer
Private oFos As Scripting.FileSystemObject
Private m_iWordVer As Integer

Private m_sIdiNombre As String
Private m_sIdiIdioma As String
Private m_sIdiPlantilla As String
Private m_sIdiCaption As String
Private m_stxtImp As String
Private m_lIdContacto As Long
Private m_sLiteralGeneracionContrato As String

'Para guardar las fechas de inicio y fin de suministro si se seleccionan grupos o items
Public g_dFechaInicioSuministro As Variant
Public g_dFechaFinSuministro As Variant

Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean
Private m_sMsgError As String

''' <summary>Llama a la creacion del contrato y la realiza la llamada web</summary>
''' <returns>Error si lo ha habido</returns>
''' <remarks>Llamada desde:=cmdContinuar_click(); Tiempo m�ximo:1,2seg (No incluye la el tiempo de la llamada)</remarks>
Public Function WizardContrato() As TipoErrorSummit
    Dim teserror As TipoErrorSummit
    Dim sProcesoContrato As String
    Dim lIdSolicitud As Long
    Dim sproveedor As String
    Dim sDenProveedor As String
    Dim sCadenaItems As String
    Dim sCadenaGrupos As String
    Dim dFechaInicioSuministro As Variant
    Dim dFechaFinSuministro As Variant
    Dim oItem As CItem
    Dim oGrupo As CGrupo
    Dim oIBaseDatos As IBaseDatos
    Dim oPersona As CPersona
    Dim bBuscarFechasEnItems As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dFechaInicioSuministro = Null
    dFechaFinSuministro = Null

    teserror = GenerarArchivoContrato()
    
    Screen.MousePointer = vbNormal
    If Not m_oTipoSeleccionado Is Nothing Then
        lIdSolicitud = m_oTipoSeleccionado.Id
    End If
    sproveedor = sdbcProveedores.Value
    sDenProveedor = sdbcProveedores.Text
    
    If g_sCadenaItems <> "" Then
        sCadenaItems = g_sCadenaItems
        dFechaInicioSuministro = g_dFechaInicioSuministro
        dFechaFinSuministro = g_dFechaFinSuministro
        sCadenaGrupos = vbNullString
    ElseIf Not g_oGruposProce Is Nothing Then
        sCadenaItems = vbNullString
        
        For Each oGrupo In g_oGruposProce
            If oGrupo.FechaInicioSuministro <> "" Then
                If IsNull(dFechaInicioSuministro) Then
                    dFechaInicioSuministro = oGrupo.FechaInicioSuministro
                Else
                    If CDate(oGrupo.FechaInicioSuministro) < CDate(dFechaInicioSuministro) Then
                        dFechaInicioSuministro = oGrupo.FechaInicioSuministro
                    End If
                End If
            End If
            
            If oGrupo.FechaFinSuministro <> "" Then
                If IsNull(dFechaFinSuministro) Then
                    dFechaFinSuministro = oGrupo.FechaFinSuministro
                Else
                    If CDate(oGrupo.FechaFinSuministro) > CDate(dFechaFinSuministro) Then
                        dFechaFinSuministro = oGrupo.FechaFinSuministro
                    End If
                End If
            End If
                                            
            sCadenaGrupos = sCadenaGrupos & oGrupo.Id & "_"
        Next
        sCadenaGrupos = Left(sCadenaGrupos, Len(sCadenaGrupos) - 1)
        
        If IsNull(dFechaInicioSuministro) And Not m_oProcesoSeleccionado Is Nothing Then
            dFechaInicioSuministro = m_oProcesoSeleccionado.FechaInicioSuministro
            dFechaFinSuministro = m_oProcesoSeleccionado.FechaFinSuministro
        End If
    Else
        sCadenaItems = vbNullString
        sCadenaGrupos = vbNullString
        
        If Not m_oProcesoSeleccionado Is Nothing Then
            dFechaInicioSuministro = m_oProcesoSeleccionado.FechaInicioSuministro
            dFechaFinSuministro = m_oProcesoSeleccionado.FechaFinSuministro
            If Not m_oProcesoSeleccionado.Items Is Nothing Then
                If IsNull(dFechaInicioSuministro) Then bBuscarFechasEnItems = True
                                    
                For Each oItem In m_oProcesoSeleccionado.Items
                    If bBuscarFechasEnItems Then
                        If IsNull(dFechaInicioSuministro) Then
                            dFechaInicioSuministro = oItem.FechaInicioSuministro
                        ElseIf oItem.FechaInicioSuministro < dFechaInicioSuministro Then
                            dFechaInicioSuministro = oItem.FechaInicioSuministro
                        End If
                        If IsNull(dFechaFinSuministro) Then
                            dFechaFinSuministro = oItem.FechaFinSuministro
                        ElseIf oItem.FechaFinSuministro > dFechaFinSuministro Then
                            dFechaFinSuministro = oItem.FechaFinSuministro
                        End If
                    End If
                Next
            End If
        End If
    End If
    Dim lIdEmpresa As Long
    Dim sComprador_Usuario As String
    
    If Not m_oProcesoSeleccionado Is Nothing Then
        sProcesoContrato = m_oProcesoSeleccionado.Anyo & "_" & m_oProcesoSeleccionado.GMN1Cod & "_" & m_oProcesoSeleccionado.Cod
        lIdEmpresa = ObtenerEmpresa(m_oProcesoSeleccionado.Anyo, m_oProcesoSeleccionado.GMN1Cod, m_oProcesoSeleccionado.Cod)
        If Not m_oProcesoSeleccionado.responsable Is Nothing Then
            
            If m_oProcesoSeleccionado.responsable.Cod <> "" Then
                Set oPersona = oFSGSRaiz.Generar_CPersona
                oPersona.Cod = m_oProcesoSeleccionado.responsable.Cod
                Set oIBaseDatos = oPersona
                oIBaseDatos.ComprobarExistenciaEnBaseDatos
                sComprador_Usuario = oPersona.Cod & "_" & oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
                Set oPersona = Nothing
                Set oIBaseDatos = Nothing
            End If
        Else
            If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
                sComprador_Usuario = oUsuarioSummit.Persona.Cod & "_" & oUsuarioSummit.Persona.nombre & " " & oUsuarioSummit.Persona.Apellidos & " (" & oUsuarioSummit.Persona.mail & ")"
            End If
        End If
     
    End If
    
    
    ''Llamar a parte Web
    LlamadaInternet m_sCarpeta, m_sArchivo, sProcesoContrato, sCadenaGrupos, sCadenaItems, lIdSolicitud, sproveedor, lIdEmpresa, m_lIdContacto, sComprador_Usuario, sDenProveedor, dFechaInicioSuministro, dFechaFinSuministro
    
    Unload Me
Set oItem = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "WizardContrato", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Nos lanza la llamada web, nos envia a la seleccion de items o a la seleccion de grupos. (Segun corresponda)
''' </summary>
''' <remarks>Llamada desde:=Evento que salta al pulsar en el boton; Tiempo m�ximo:=0,4seg.</remarks>
Private Sub cmdContinuar_Click()
    Dim bEncontrado As Boolean
    Dim teserror As TipoErrorSummit
        
    Dim oSolicitPlantillas As CSolicitPlantillas
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtNom.Text = Trim(txtNom.Text)
    If txtNom.Text = "" Then
        oMensajes.NoValido m_sIdiNombre
        Exit Sub
    End If
    If Not FSGSLibrary.NombreDeArchivoValido(txtNom.Text) Then
        oMensajes.NombreArchivoNoValido
        If Me.Visible Then txtNom.SetFocus
        Exit Sub
    End If
    
    If sdbcTipoCod.Text = "" Then
        oMensajes.NoValido 123
        Exit Sub
    End If
    If sdbcIdiomas.Text = "" Then
        oMensajes.NoValido m_sIdiIdioma
        Exit Sub
    End If
    If sdbcProveedores.Text = "" Then
        oMensajes.NoValido 28
        Exit Sub
    End If
    
    
    If sdbcTipoCod.Value = "" Then
        oMensajes.NoValido 123
        Exit Sub
    Else
    Dim sDen As String
    
    sDen = sdbcTipoDen.Text


    Set oSolicitPlantillas = oFSGSRaiz.Generar_CSolicitPlantillas
    oSolicitPlantillas.CargarTodasLasPlantillas m_oTipoSeleccionado.Id
    Dim oSolicPlantilla As CSolicitPlantilla
    
    bEncontrado = False
    For Each oSolicPlantilla In oSolicitPlantillas
        If oSolicPlantilla.idioma = sdbcIdiomas.Columns(1).Value Then
            If oSolicPlantilla.Ruta = "" Then
                oMensajes.FaltanDatos m_sIdiPlantilla
                Exit Sub
            End If
            
            bEncontrado = True
            g_sPlantilla = oSolicPlantilla.Ruta
            Exit For
        End If
    Next

    
    If Not bEncontrado Then
        oMensajes.FaltanDatos m_sIdiPlantilla
        Exit Sub
    End If
    If Right(g_sPlantilla, 3) <> "dot" Then
        oMensajes.NoValido m_sIdiPlantilla & ": " & g_sPlantilla
        Exit Sub
    End If
    If Not oFos.FileExists(g_sPlantilla) Then
        oMensajes.PlantillaNoEncontrada g_sPlantilla
        Exit Sub
    End If
    

End If

'Comprobar que el usuario tiene acceso
'Si se trata del administrator, comprobar que el usuario que se ha introducido
'en la tabla PARGEN_PM se encuentra en la solicitud

If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
    Dim oContrato As CContrato
    Set oContrato = oFSGSRaiz.Generar_CContrato
    Dim bResultado As Boolean
    
    bResultado = oContrato.ComprobarPermisoSolicitud(sdbcTipoCod.Text)
    If Not bResultado Then
        oMensajes.PermisoDenegadoEnSolicitud sDen
        Exit Sub
    End If
End If

If opProce.Value = True Then
    teserror = WizardContrato
    
ElseIf opGrupo.Value = True Then
    Me.Hide
    frmCONTRWizardGru.g_sCodProve = Me.sdbcProveedores.Value
    frmCONTRWizardGru.Show
Else
    Me.Hide
    Set g_oProveSeleccionado = m_oProve
    frmCONTRWizardItem.g_sCodProve = Me.sdbcProveedores.Value
    frmCONTRWizardItem.Show
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "cmdContinuar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Realiza la llamada web --> Alta de contratos</summary>
''' <param name="sPath">Path de la carpeta donde se almacena esta temporalmente el archivo del contrato</param>
''' <param name="sNombreContrato">Nombre del contrato</param>
''' <param name="sProcesoContrato">Proceso contrato (a�o_gmn1_cod)</param>
''' <param name="sCadenaGrupos">Cadena de grupos del contrato</param>
''' <param name="sCadenaItems">Cadena de Items del contrato</param>
''' <param name="lSolicitud">ID de la solicitud del que se crea el contrato</param>
''' <param name="sproveedor">Codigo proveedor</param>
''' <param name="lEmpresa">ID Empresa</param>
''' <param name="lIdContacto">ID del contacto del proveedor</param>
''' <param name="sPersona">Codigo de persona y Nombre (codPersona#Nombre y apellido)</param>
''' <param name="sDenProveedor">Denominacion proveedor </param>
''' <remarks>Llamada desde; Tiempo m�ximo:0,2seg.</remarks>
Private Sub LlamadaInternet(ByVal sPath As String, ByVal sNombreContrato As String, ByVal sProcesoContrato As String, ByVal sCadenaGrupos As String, _
                            ByVal sCadenaItems As String, ByVal lSolicitud As Long, ByVal sproveedor As String, ByVal lEmpresa As Long, _
                            ByVal lIdContacto As Long, ByVal sPersona As String, ByVal sDenProveedor As String, Optional ByVal dFechaInicioSuministro As Variant, _
                            Optional ByVal dFechaFinSuministro As Variant)
    Dim sURL As String
    Dim sParametros As String
    Dim strSessionId As String
    Dim bites As Double
    Dim oFile As File
    Dim fso As Scripting.FileSystemObject
    Dim sFicheroLogin As String
    Dim AdjuntoContrato As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    Set fso = New Scripting.FileSystemObject
    Set oFile = fso.GetFile(sPath & "\" & sNombreContrato)
    bites = oFile.Size / 1024
    Set oFile = Nothing
    Set fso = Nothing
       
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    Dim oWebSvc As FSGSLibrary.CWebService
    Set oWebSvc = New FSGSLibrary.CWebService
    AdjuntoContrato = oWebSvc.LlamarWebServiceGrabarAdjunto(gParametrosGenerales.gsURLWebServiceQA, sPath & "\" & sNombreContrato, sproveedor, sNombreContrato)
    Set oWebSvc = Nothing
    
    Dim ArrayAdjunto() As String
        
    'Creamos un array, cada "substring" se asignar�
    'a un elemento del array
    ArrayAdjunto = Split(AdjuntoContrato, "#", , vbTextCompare)
    
    sParametros = "?NombreContrato=" & sNombreContrato
    sParametros = sParametros & "*PathContrato=" & sPath
    sParametros = sParametros & "*DesdeWizardGS=1"
    sParametros = sParametros & "*idAdjuntoContrato=" & ArrayAdjunto(0)
    sParametros = sParametros & "*SizeAdjuntoContrato=" & ArrayAdjunto(1)
    sParametros = sParametros & "*ProcesoContrato=" & sProcesoContrato
    sParametros = sParametros & "*GruposContrato=" & sCadenaGrupos
    sParametros = sParametros & "*ItemsContrato=" & sCadenaItems
    If Not IsNull(dFechaInicioSuministro) Then
        sParametros = sParametros & "*FecInicio=" & Format(dFechaInicioSuministro, "dd/MM/yyyy")
    End If
    If Not IsNull(dFechaFinSuministro) Then
        sParametros = sParametros & "*FecFin=" & Format(dFechaFinSuministro, "dd/MM/yyyy")
    End If
    sParametros = sParametros & "*Solicitud=" & lSolicitud
    sParametros = sParametros & "*Proveedor=" & sproveedor
    sParametros = sParametros & "*DenProveedor=" & sDenProveedor
    sParametros = sParametros & "*Empresa=" & lEmpresa
    sParametros = sParametros & "*IdContacto=" & lIdContacto
    sParametros = sParametros & "*PersonaNotificadora=" & sPersona
    sParametros = sParametros & "*kb=" & bites
    sParametros = sParametros & "*desdeGS=1"
    sParametros = sParametros & "*configuracionGS=1"

    sURL = gParametrosGenerales.gsURLPMAlta & "sessionId=" & strSessionId & "&desdeGS=1&" & gParametrosGenerales.gcolRutas("AltaContratos") & sParametros
    
    With frmInternet
        .g_sOrigen = Me.Name
        .g_sFileLogin = sFicheroLogin
        .g_sPathContrato = sPath
        .g_sNombre = m_sLiteralGeneracionContrato '"Generaci�n de contrato"
        .g_sRuta = sURL
        '.Show
        If MDI.ActiveForm Is Nothing Then
            Me.WindowState = vbNormal
        Else
            If MDI.ActiveForm.WindowState = vbMaximized Then
                Me.WindowState = vbMaximized
            Else
                Me.WindowState = vbNormal
            End If
        End If
        .SetFocus
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "LlamadaInternet", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Realiza la operaciones de carga del formulario
''' Cargar los tipos de solicitudes, proveedores...
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then

      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bDescargarFrm = False
Dim oProv As CProveedor
Dim oIdioma As CIdioma
Dim idiIdiomasRow  As Variant
Dim i As Integer

Me.Height = 4450
Me.Width = 7500
Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

'Obtiene el n� de decimales a mostrar en esta pantalla
m_sFormatoNumber = "#,##0."
For i = 1 To gParametrosInstalacion.giNumDecimalesComp
    m_sFormatoNumber = m_sFormatoNumber & "0"
Next i


CargarRecursos

PonerFieldSeparator Me

i = 0
Set m_oIdiomas = oGestorParametros.DevolverIdiomas
For Each oIdioma In m_oIdiomas
    sdbcIdiomas.AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
    If gParametrosInstalacion.gIdioma = oIdioma.Cod Then
        idiIdiomasRow = sdbcIdiomas.GetBookmark(i)
    End If
    i = i + 1
        
Next
sdbcIdiomas.Text = m_oIdiomas.Item(CStr(gParametrosInstalacion.gIdioma)).Den
sdbcIdiomas.Bookmark = idiIdiomasRow


If Not g_oProveedores Is Nothing Then
    For Each oProv In g_oProveedores
        sdbcProveedores.AddItem oProv.Cod & " " & oProv.Den & Chr(m_lSeparador) & oProv.Cod
        If g_oProveedores.Count = 1 Then
            sdbcProveedores.Text = oProv.Den
            sdbcProveedores.Value = oProv.Cod
            sdbcProveedores.Bookmark = sdbcProveedores.GetBookmark(0)
            Set m_oProve = g_oProveedores.Item(sdbcProveedores.Value)
            Set g_oProveSeleccionado = g_oProveedores.Item(sdbcProveedores.Value)
        End If
    Next
ElseIf Not g_oProveSeleccionado Is Nothing Then
    sdbcProveedores.AddItem g_oProveSeleccionado.Cod & " " & g_oProveSeleccionado.Den & Chr(m_lSeparador) & g_oProveSeleccionado.Cod
    sdbcProveedores.Text = g_oProveSeleccionado.Den
    sdbcProveedores.Value = g_oProveSeleccionado.Cod
    sdbcProveedores.Bookmark = sdbcProveedores.GetBookmark(0)
    Set m_oProve = g_oProveSeleccionado
    Set g_oProveSeleccionado = Nothing
Else
    sdbcProveedores.AddItem g_sCodProveedor & " " & g_sDenProveedor & Chr(m_lSeparador) & g_sCodProveedor
    sdbcProveedores.Text = g_sDenProveedor
    sdbcProveedores.Value = g_sCodProveedor
    sdbcProveedores.Bookmark = sdbcProveedores.GetBookmark(0)
End If

Set g_oGruposProce = Nothing
Set g_oItemsProce = Nothing
Set m_oProce = m_oProcesoSeleccionado
Set m_oTiposContrato = oFSGSRaiz.Generar_CSolicitudes


CargarContratos
'Si hay un unico contrato seleccionarlo
If Not m_oTiposContrato Is Nothing Then
    If m_oTiposContrato.Count = 1 Then
        m_bRespetarCombo = True
        sdbcTipoDen.Value = m_oTiposContrato.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcTipoCod.Value = m_oTiposContrato.Item(1).Codigo
        sdbcTipoCod.Text = m_oTiposContrato.Item(1).Codigo
        Set m_oTipoSeleccionado = m_oTiposContrato.Item(sdbcTipoCod.Text)
        m_bRespetarCombo = False
    End If
End If

Set oFos = New Scripting.FileSystemObject
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Descarga las variables
''' </summary>
''' <param name="Cancel">del evento</param>
''' <remarks>Llamada desde; Tiempo m�ximo:0seg.</remarks>
Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set m_oTiposContrato = Nothing
Set m_oTipoSeleccionado = Nothing
Set m_oIdiomas = Nothing
Set m_oProce = Nothing
Set m_oProve = Nothing
Set m_oAtribsFormulas = Nothing
Set m_oOferta = Nothing
Set m_oAdjudicaciones = Nothing
Set m_oProvesAsig = Nothing
Set g_oGruposProce = Nothing
Set g_oItemsProce = Nothing
Set oFos = Nothing

Set m_oProcesoSeleccionado = Nothing
Set g_oProveedores = Nothing
g_sCadenaItems = ""

Unload frmCONTRWizardGru
Unload frmCONTRWizardItem
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Crear el documento word del contrato a generar.
''' Se genera como Microsoft Office 97-2003 para q el TXTextControl lo pueda mostrar sin problemas.
''' </summary>
''' <returns>Error de haberlo/returns>
''' <remarks>Llamada desde: WizardContrato; Tiempo m�ximo: 2sg</remarks>
Private Function GenerarArchivoContrato() As TipoErrorSummit
Dim teserror As TipoErrorSummit
Dim sTemp As String
Dim appword As Object
Dim docword As Object
Dim blankword As Object
Dim rangeword As Object
Dim rangewordIT As Object
Dim rangewordGR As Object
Dim Destino As Boolean
Dim Pago As Boolean
Dim Fecha As Boolean
Dim oItem As CItem
Dim oEsp As CEspecificacion
Dim oIAdjudicaciones As IAdjudicaciones
Dim oAdjsAux As CAdjudicaciones
Dim oAdj As CAdjudicacion
Dim oEquipo As CEquipo
Dim oiasignaciones As IAsignaciones
Dim oAsignaciones As CAsignaciones
Dim oCon As CContacto
Dim oDestinos As CDestinos
Dim oPagos As CPagos
Dim oUnidades As CUnidades
Dim bSalvar As Boolean
Dim sGrupo As String
Dim bCerrados As Boolean
Dim bVisibleWord As Boolean
Dim iNumEsp As Integer
Dim iNumPago As Integer
Dim INUMDEST As Integer
Dim ayDest() As String
Dim ayPago() As String
Dim iNumFich As Integer
Dim iNumFich2 As Integer
Dim dTotalItem As Double
Dim dTotalGrupo As Double
Dim dTotalGen As Double
Dim sVersion As String
Dim dtFechaOferta As Date
Dim dtHoraOferta As Date
Dim tzInfo As TIME_ZONE_INFO
Dim oGruposProce As CGrupos
Dim oGrupo As CGrupo
Dim oProves As CProveedores

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
        
        Screen.MousePointer = vbHourglass
        teserror.NumError = TESnoerror
        frmESPERA.lblGeneral = m_sIdiCaption
        frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
        frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
        frmESPERA.Show
        DoEvents
        frmESPERA.ProgressBar1.Value = 1
        frmESPERA.lblDetalle = txtNom.Text
        
        DoEvents
        frmESPERA.ProgressBar2.Value = 2
        frmESPERA.ProgressBar1.Value = 2
        
        ' DATOS
        If m_oProce Is Nothing Then
            Set m_oProce = m_oProcesoSeleccionado
        End If
        
        If m_oProve Is Nothing Then
            Set m_oProve = oFSGSRaiz.generar_CProveedor
            m_oProve.Cod = sdbcProveedores.Value
        End If
        Set oProves = Nothing
        Set oProves = oFSGSRaiz.generar_CProveedores
        oProves.Add m_oProve.Cod, m_oProve.Den
        oProves.CargarDatosProveedor m_oProve.Cod, , False
       
        Set oiasignaciones = m_oProce
        Set oAsignaciones = oiasignaciones.DevolverAsignaciones(, , , m_oProve.Cod)
        Set oEquipo = oFSGSRaiz.generar_CEquipo
        oEquipo.Cod = oAsignaciones.Item(1).codEqp
        oEquipo.CargarTodosLosCompradoresDesde 1, oAsignaciones.Item(1).CodComp, , , , True
                  
        DoEvents
        If frmESPERA.ProgressBar2.Value < 90 Then
           frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
        End If
        If frmESPERA.ProgressBar1.Value < 10 Then
            frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
        End If
                  
        If sdbcContacto.Text <> "" Then
            Set oCon = m_oProve.Contactos.Item(sdbcContacto.Columns(1).Value)
        Else
            Set oCon = Nothing
        End If
        
        Set m_oOferta = m_oProce.DevolverUltimaOfertaProveedor(m_oProve.Cod, False, True, , sdbcIdiomas.Columns(1).Value)
        
        CargarAtributos
        
        Set oDestinos = oFSGSRaiz.Generar_CDestinos
        Set oUnidades = oFSGSRaiz.Generar_CUnidades
        Set oPagos = oFSGSRaiz.generar_CPagos
        oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, True
        oUnidades.CargarTodasLasUnidades
        oPagos.CargarTodosLosPagos
        
        DoEvents
        If frmESPERA.ProgressBar2.Value < 90 Then
           frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
        End If
        If frmESPERA.ProgressBar1.Value < 10 Then
            frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
        End If
        
        If appword Is Nothing Then

            DoEvents
            Set appword = CreateObject("Word.Application")
            bSalvar = appword.Options.SavePropertiesPrompt
            appword.Options.SavePropertiesPrompt = False
        End If
        bVisibleWord = appword.Visible
        appword.WindowState = 2
        sVersion = appword.VERSION
        If InStr(1, sVersion, ".") > 1 Then
            sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
            m_iWordVer = CInt(sVersion)
        Else
            m_iWordVer = 9
        End If
        
        DoEvents
        If frmESPERA.ProgressBar2.Value < 90 Then
           frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
        End If
        If frmESPERA.ProgressBar1.Value < 10 Then
            frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
        End If
        
        Set docword = appword.Documents.Add(g_sPlantilla)
        Set blankword = appword.Documents.Add(g_sPlantilla)
        appword.Visible = True
        
        If m_iWordVer >= 9 Then
            If appword.Visible Then docword.activewindow.WindowState = 2
            If appword.Visible Then blankword.activewindow.WindowState = 2
        Else
            appword.Windows(docword.Name).WindowState = 1
            appword.Windows(blankword.Name).WindowState = 1
        End If
        
        INUMDEST = 0
        ReDim ayDest(0)
        iNumPago = 0
        ReDim ayPago(0)
        
        With docword
            .Bookmarks("\StartOfDoc").Select
            If .Bookmarks.Exists("NUM_PROCESO") Then
                DatoAWord docword, "NUM_PROCESO", m_oProce.Anyo & "/" & m_oProce.GMN1Cod & "/" & m_oProce.Cod
            End If
            If .Bookmarks.Exists("DEN_PROCESO") Then
                DatoAWord docword, "DEN_PROCESO", m_oProce.Den
            End If
            If .Bookmarks.Exists("MAT1_PROCESO") Then
                DatoAWord docword, "MAT1_PROCESO", m_oProce.GMN1Cod
            End If
            If .Bookmarks.Exists("MONEDA") Then
                DatoAWord docword, "MONEDA", m_oProce.MonCod
            End If
            If .Bookmarks.Exists("CAMBIO") Then
                DatoAWord docword, "CAMBIO", FormateoNumerico(m_oProce.Cambio, m_sFormatoNumber)
            End If
            If .Bookmarks.Exists("SOLICITUD") Then
                DatoAWord docword, "SOLICITUD", NullToStr(m_oProce.Referencia)
            End If
            If .Bookmarks.Exists("FECHA_CIERRE") Then
                DatoAWord docword, "FECHA_CIERRE", NullToStr(m_oProce.FechaCierre)
            End If
            ' DATOS OFERTA
            If .Bookmarks.Exists("FEC_OFERTA") Then
                'Pasar la fecha al TZ del usuario
                ConvertirUTCaTZ DateValue(m_oOferta.FechaRec), TimeValue(m_oOferta.FechaRec), oUsuarioSummit.TimeZone, dtFechaOferta, dtHoraOferta
                tzInfo = GetTimeZoneByKey(oUsuarioSummit.TimeZone)
                DatoAWord docword, "FEC_OFERTA", CStr(dtFechaOferta + dtHoraOferta) & " " & Mid(tzInfo.DisplayName, 1, 11)
            End If
            If .Bookmarks.Exists("NUM_OFERTA") Then
                DatoAWord docword, "NUM_OFERTA", m_oOferta.Num
            End If
            If .Bookmarks.Exists("OBS_OFERTA") Then
                DatoAWord docword, "OBS_OFERTA", NullToStr(m_oOferta.obs)
            End If
            ' DATOS PROVEEDOR
            If .Bookmarks.Exists("DEN_PROVE") Then
                DatoAWord docword, "DEN_PROVE", oProves.Item(1).Den
            End If
            If .Bookmarks.Exists("COD_PROVE") Then
                DatoAWord docword, "COD_PROVE", oProves.Item(1).Cod
            End If
            If .Bookmarks.Exists("DIR_PROVE") Then
                DatoAWord docword, "DIR_PROVE", NullToStr(oProves.Item(1).Direccion)
            End If
            If .Bookmarks.Exists("POBL_PROVE") Then
                DatoAWord docword, "POBL_PROVE", NullToStr(oProves.Item(1).Poblacion)
            End If
            If .Bookmarks.Exists("CP_PROVE") Then
                DatoAWord docword, "CP_PROVE", NullToStr(oProves.Item(1).cP)
            End If
            If .Bookmarks.Exists("PROV_PROVE") Then
                DatoAWord docword, "PROV_PROVE", NullToStr(oProves.Item(1).DenProvi)
            End If
            If .Bookmarks.Exists("PAIS_PROVE") Then
                DatoAWord docword, "PAIS_PROVE", NullToStr(oProves.Item(1).DenPais)
            End If
            If .Bookmarks.Exists("NIF_PROVE") Then
                DatoAWord docword, "NIF_PROVE", NullToStr(oProves.Item(1).NIF)
            End If
            ' DATOS CONTACTO
            If Not oCon Is Nothing Then
                If .Bookmarks.Exists("NOM_CONTACTO") Then
                    DatoAWord docword, "NOM_CONTACTO", NullToStr(oCon.nombre)
                End If
                If .Bookmarks.Exists("APE_CONTACTO") Then
                    DatoAWord docword, "APE_CONTACTO", oCon.Apellidos
                End If
                If .Bookmarks.Exists("TFNO_CONTACTO") Then
                    DatoAWord docword, "TFNO_CONTACTO", NullToStr(oCon.Tfno)
                End If
                If .Bookmarks.Exists("FAX_CONTACTO") Then
                    DatoAWord docword, "FAX_CONTACTO", NullToStr(oCon.Fax)
                End If
                If .Bookmarks.Exists("TFNO_MOVIL_CONTACTO") Then
                    DatoAWord docword, "TFNO_MOVIL_CONTACTO", NullToStr(oCon.tfnomovil)
                End If
            End If
            
            ' DATOS COMPRADOR ASIGNADO A PROVEEDOR
            If .Bookmarks.Exists("APE_COMPRADOR") Then
                DatoAWord docword, "APE_COMPRADOR", oEquipo.Compradores.Item(1).Apel
            End If
            If .Bookmarks.Exists("NOM_COMPRADOR") Then
                DatoAWord docword, "NOM_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).nombre)
            End If
            If .Bookmarks.Exists("TFNO_COMPRADOR") Then
                DatoAWord docword, "TFNO_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Tfno)
            End If
            If .Bookmarks.Exists("FAX_COMPRADOR") Then
                DatoAWord docword, "FAX_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).Fax)
            End If
            If .Bookmarks.Exists("MAIL_COMPRADOR") Then
                DatoAWord docword, "MAIL_COMPRADOR", NullToStr(oEquipo.Compradores.Item(1).mail)
            End If

            If m_oProce.DefDestino = EnProceso Then
                If .Bookmarks.Exists("COD_DEST_PROCE") Then
                    .Bookmarks("COD_DEST_PROCE").Range.Text = NullToStr(m_oProce.DestCod)
                End If
                If .Bookmarks.Exists("DEN_DEST_PROCE") Then
                    .Bookmarks("DEN_DEST_PROCE").Range.Text = NullToStr(oDestinos.Item(m_oProce.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                End If
            Else
                If .Bookmarks.Exists("DEST_PROCE") Then
                    .Bookmarks("DEST_PROCE").Select
                    .Application.Selection.cut
                End If
            End If
            
            If m_oProce.DefFechasSum = EnProceso Then
                If .Bookmarks.Exists("FINI_SUM_PROCE") Then
                    .Bookmarks("FINI_SUM_PROCE").Range.Text = NullToStr(m_oProce.FechaInicioSuministro)
                End If
                If .Bookmarks.Exists("FFIN_SUM_PROCE") Then
                    .Bookmarks("FFIN_SUM_PROCE").Range.Text = NullToStr(m_oProce.FechaFinSuministro)
                End If
            Else
                If .Bookmarks.Exists("FECSUM_PROCE") Then
                    .Bookmarks("FECSUM_PROCE").Select
                    .Application.Selection.cut
                End If
            End If
            
            If m_oProce.DefFormaPago = EnProceso Then
                If .Bookmarks.Exists("COD_PAGO_PROCE") Then
                    .Bookmarks("COD_PAGO_PROCE").Range.Text = NullToStr(m_oProce.PagCod)
                End If
                If .Bookmarks.Exists("DEN_PAGO_PROCE") Then
                    .Bookmarks("DEN_PAGO_PROCE").Range.Text = NullToStr(oPagos.Item(m_oProce.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                End If
            Else
                
                If .Bookmarks.Exists("PAGO_PROCE") Then
                    .Bookmarks("PAGO_PROCE").Select
                    .Application.Selection.cut
                End If
            End If
                
            
            ' Especificaciones de Proceso
            If m_oProce.DefEspecificaciones = True Then
                If .Bookmarks.Exists("OPC_ESP_PROCESO") Then
                    If m_oProce.esp <> "" And Not IsNull(m_oProce.esp) Then
                        .Bookmarks("ESPEC_PROCESO").Range.Text = m_oProce.esp
                        .Bookmarks("OPC_ESP_PROCESO").Delete
                    Else
                        .Bookmarks("OPC_ESP_PROCESO").Range.Delete
                    End If
                Else
                    If .Bookmarks.Exists("ESPEC_PROCESO") Then
                        If m_oProce.esp <> "" And Not IsNull(m_oProce.esp) Then
                            .Bookmarks("ESPEC_PROCESO").Range.Text = m_oProce.esp
                        Else
                            .Bookmarks("ESPEC_PROCESO").Range.Text = ""
                        End If
                    End If
                End If
            
                ' Ficheros adjuntos de Proceso
                If .Bookmarks.Exists("PROC_ADJ") Then
                    m_oProce.CargarTodasLasEspecificaciones
                    If m_oProce.especificaciones.Count > 0 Then
                        .Bookmarks("PROC_ADJ").Select
                        Set rangeword = docword.Bookmarks("PROC_ADJ").Range
                        rangeword.Copy
                        .Application.Selection.Delete
                        iNumFich = 1
                        For Each oEsp In m_oProce.especificaciones
                            .Application.Selection.Paste
                            .Bookmarks("NOM_ADJ_PROC").Range.Text = oEsp.nombre
                            .Bookmarks("FEC_ADJ_PROC").Range.Text = Format(oEsp.Fecha, "Short Date")
                            .Bookmarks("DEN_ADJ_PROC").Range.Text = NullToStr(oEsp.Comentario)
                            iNumFich = iNumFich + 1
                        Next
                        .Bookmarks("PROC_ADJ").Delete
                        .Bookmarks("PROCESO_ADJ").Delete
                    Else
                        .Bookmarks("PROCESO_ADJ").Range.Delete
                    End If
                Else
                    .Bookmarks("PROCESO_ADJ").Range.Delete
                End If
            Else
                .Bookmarks("OPC_ESP_PROCESO").Range.Delete
                .Bookmarks("PROCESO_ADJ").Range.Delete
                
            End If
            'Fin especificaciones proceso
            
            DoEvents
            If frmESPERA.ProgressBar2.Value < 90 Then
               frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
            End If
            If frmESPERA.ProgressBar1.Value < 10 Then
                frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
            End If
            
            'Tratamiento items
            If m_oProce.Estado = ParcialmenteCerrado Then
                bCerrados = True
            Else
                bCerrados = False
            End If
            m_oProce.CargarTodosLosItems OrdItemPorOrden, , , , , , True
            
            Set oIAdjudicaciones = m_oProce
            Set m_oProvesAsig = oIAdjudicaciones.DevolverProveedoresConAdjudicaciones
            If m_oProvesAsig.Count > 1 Then
                Set m_oAdjudicaciones = oIAdjudicaciones.DevolverAdjudicaciones
            End If
            
            If g_oGruposProce Is Nothing Then
                If g_oItemsProce Is Nothing Then
                    m_oProce.CargarTodosLosGrupos bSinpres:=True
                    Set oGruposProce = m_oProce.Grupos
                    Set m_oAdjs = oIAdjudicaciones.DevolverAdjudicacionesDeProveedor(m_oProve.Cod, bCerrados)
                Else
                    Set m_oAdjs = oFSGSRaiz.Generar_CAdjudicaciones
                    Set oGruposProce = oFSGSRaiz.Generar_CGrupos
                    For Each oItem In g_oItemsProce
                        Set oAdjsAux = oIAdjudicaciones.DevolverAdjudicacionesDeGrupoOItem(m_oProve.Cod, , oItem.Id, bCerrados)
                        For Each oAdj In oAdjsAux
                            m_oAdjs.Add oAdj.ProveCod, oAdj.NumOfe, oAdj.Id, oAdj.Precio, oAdj.Porcentaje, , oItem.Precio, , , oItem.grupoCod
                        Next
                        If oGruposProce.Item(oItem.grupoCod) Is Nothing Then
                            oGruposProce.Add m_oProce, oItem.grupoCod, ""
                            oGruposProce.Item(oItem.grupoCod).CargarTodosLosDatos
                        End If
                    Next
                End If
            Else
                Set m_oAdjs = oFSGSRaiz.Generar_CAdjudicaciones
                Set oGruposProce = g_oGruposProce 'se pasa a la variable local
                For Each oGrupo In oGruposProce
                    If oGrupo.Items Is Nothing Then
                        If bCerrados Then
                            oGrupo.CargarTodosLosItems OrdItemPorOrden, , , , , , , , , , 3
                        Else
                            oGrupo.CargarTodosLosItems OrdItemPorOrden
                        End If
                        oGrupo.CargarTodosLosDatos
                    End If
                    Set oAdjsAux = oIAdjudicaciones.DevolverAdjudicacionesDeGrupoOItem(m_oProve.Cod, oGrupo.Codigo, , bCerrados)
                    For Each oAdj In oAdjsAux
                        m_oAdjs.Add oAdj.ProveCod, oAdj.NumOfe, oAdj.Id, oAdj.Precio, oAdj.Porcentaje, , oAdj.PresUnitario, , , oGrupo.Codigo
                    Next
                    
                Next
            End If
        
            DoEvents
            If frmESPERA.ProgressBar2.Value < 90 Then
               frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
            End If
            If frmESPERA.ProgressBar1.Value < 10 Then
                frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
            End If
        
            dTotalGen = 0
            iNumEsp = 1
            If docword.Bookmarks.Exists("ITEM_TOD") Then
                If m_oProce.DefDestino <> EnProceso Then
                    Destino = True
                Else
                    Destino = False
                End If
                If m_oProce.DefFormaPago <> EnProceso Then
                    Pago = True
                Else
                    Pago = False
                End If
                If m_oProce.DefFechasSum <> EnProceso Then
                    Fecha = True
                Else
                    Fecha = False
                End If
                FSGSLibrary.DatosConfigurablesAWord blankword, "ITEM_DEST", "ITEM_PAGO", "ITEM_FECSUM", Destino, Pago, Fecha
                If .Bookmarks.Exists("GRUPO_TOT") Then
                    .Bookmarks("GRUPO_TOT").Select
                    .Application.Selection.cut
                    .Application.Selection.TypeBackspace
                    Set rangewordGR = blankword.Bookmarks("GRUPO_TOT").Range
                End If
                .Bookmarks("ITEM_TOD").Select
                .Application.Selection.cut
                Set rangeword = blankword.Bookmarks("ITEM_TOD").Range
                sGrupo = ""
                For Each oAdj In m_oAdjs
                    If sGrupo <> m_oProce.Items.Item(CStr(oAdj.Id)).grupoCod Then
                        If sGrupo <> "" Then
                            DoEvents
                            If frmESPERA.ProgressBar2.Value < 90 Then
                               frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
                            End If
                            If frmESPERA.ProgressBar1.Value < 10 Then
                                frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
                            Else
                                frmESPERA.ProgressBar1.Value = 1
                            End If
                        
                            If blankword.Bookmarks.Exists("GRUPO_TOT") Then
                                rangewordGR.Copy
                                .Application.Selection.Paste
                                If .Bookmarks.Exists("TOTAL_GR_SIN") Then
                                    .Bookmarks("TOTAL_GR_SIN").Range.Text = FormateoNumerico(dTotalGrupo, m_sFormatoNumber)
                                End If
                                dTotalGrupo = AplicarAtributosGrupo(dTotalGrupo, sGrupo)
                                If .Bookmarks.Exists("TOTAL_GR") Then
                                    If m_sComentario <> "" Then
                                        .Bookmarks("TOTAL_GR").Range.Select
                                        .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                                        .Application.Selection.TypeText Text:=m_sComentario
                                        .Application.activewindow.ActivePane.Close
                                        .Application.Selection.MoveRight Unit:=1, Count:=2
                                    End If
                                    .Bookmarks("TOTAL_GR").Range.Text = FormateoNumerico(dTotalGrupo, m_sFormatoNumber)
                                End If
                                .Bookmarks("GRUPO_TOT").Delete
                                
                            End If
                            dTotalGen = dTotalGen + dTotalGrupo
                        End If
                        sGrupo = m_oProce.Items.Item(CStr(oAdj.Id)).grupoCod
                        dTotalGrupo = 0
                        'Pongo la tabla entera con cabeceras
                        rangeword.Copy
                        .Application.Selection.Paste
                        .Bookmarks("ITEM").Select
                        .Application.Selection.cut
                        Set rangewordIT = blankword.Bookmarks("ITEM").Range
                    End If
                    
                    rangewordIT.Copy
                    .Application.Selection.Paste
                    

                    If .Bookmarks.Exists("NUM_ART") Then
                       .Bookmarks("NUM_ART").Range.Text = iNumEsp
                    End If
                    If .Bookmarks.Exists("COD_ART") Then
                        .Bookmarks("COD_ART").Range.Text = NullToStr(m_oProce.Items.Item(CStr(oAdj.Id)).ArticuloCod)
                    End If
                    If .Bookmarks.Exists("DEN_ART") Then
                        .Bookmarks("DEN_ART").Range.Text = NullToStr(m_oProce.Items.Item(CStr(oAdj.Id)).Descr)
                    End If
                    If .Bookmarks.Exists("COD_GRUPO") Then
                        .Bookmarks("COD_GRUPO").Range.Text = NullToStr(m_oProce.Items.Item(CStr(oAdj.Id)).grupoCod)
                    End If

                    If .Bookmarks.Exists("CANTIDAD") Then
                        .Bookmarks("CANTIDAD").Range.Text = FormateoNumerico((oAdj.Porcentaje / 100) * m_oProce.Items.Item(CStr(oAdj.Id)).Cantidad, m_sFormatoNumber)
                    End If
                    If .Bookmarks.Exists("PRECIO") Then
                        .Bookmarks("PRECIO").Range.Text = FormateoNumerico((oAdj.Precio), m_sFormatoNumber)
                    End If
                    dTotalItem = oAdj.Precio * (oAdj.Porcentaje / 100) * m_oProce.Items.Item(CStr(oAdj.Id)).Cantidad
                    'Aplico los atributos de total item
                    dTotalItem = AplicarAtributosTotalItem(dTotalItem, m_oProce.Items.Item(CStr(oAdj.Id)))
                    'Sumo al total de grupo el total item con atribs
                    dTotalGrupo = dTotalGrupo + dTotalItem
                    If .Bookmarks.Exists("TOTAL_ART") Then
                        If m_sComentario <> "" Then
                            .Bookmarks("TOTAL_ART").Range.Select
                            .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                            .Application.Selection.TypeText Text:=m_sComentario
                            .Application.activewindow.ActivePane.Close
                            .Application.Selection.MoveRight Unit:=1, Count:=2
                        End If
                        .Bookmarks("TOTAL_ART").Range.Text = FormateoNumerico(dTotalItem, m_sFormatoNumber)
                    End If

                    If .Bookmarks.Exists("COD_UNI") Then
                        .Bookmarks("COD_UNI").Range.Text = m_oProce.Items.Item(CStr(oAdj.Id)).UniCod
                    End If
                    If .Bookmarks.Exists("DEN_UNI") Then
                        .Bookmarks("DEN_UNI").Range.Text = oUnidades.Item(m_oProce.Items.Item(CStr(oAdj.Id)).UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                    End If
                    
                    If Destino = True Then
                        If .Bookmarks.Exists("COD_DEST") Then
                            .Bookmarks("COD_DEST").Range.Text = NullToStr(m_oProce.Items.Item(CStr(oAdj.Id)).DestCod)
                            If .Bookmarks.Exists("DESTINO") Then
                                If Not BuscarEnArray(ayDest, m_oProce.Items.Item(CStr(oAdj.Id)).DestCod) Then
                                    ayDest(INUMDEST) = m_oProce.Items.Item(CStr(oAdj.Id)).DestCod
                                    INUMDEST = INUMDEST + 1
                                    ReDim Preserve ayDest(UBound(ayDest) + 1)
                                End If
                            End If
                        End If
                    
                        If .Bookmarks.Exists("DEN_DEST") Then
                            .Bookmarks("DEN_DEST").Range.Text = NullToStr(oDestinos.Item(m_oProce.Items.Item(CStr(oAdj.Id)).DestCod))
                            If .Bookmarks.Exists("DESTINO") Then
                                If Not BuscarEnArray(ayDest, m_oProce.Items.Item(CStr(oAdj.Id)).DestCod) Then
                                    ayDest(INUMDEST) = m_oProce.Items.Item(CStr(oAdj.Id)).DestCod
                                    INUMDEST = INUMDEST + 1
                                    ReDim Preserve ayDest(UBound(ayDest) + 1)
                                End If
                            End If
                        End If
                    End If
                    
                    If Pago = True Then
                        If .Bookmarks.Exists("COD_PAGO") Then
                            .Bookmarks("COD_PAGO").Range.Text = NullToStr(m_oProce.Items.Item(CStr(oAdj.Id)).PagCod)
                            If .Bookmarks.Exists("PAGO") Then
                                If Not BuscarEnArray(ayPago, m_oProce.Items.Item(CStr(oAdj.Id)).PagCod) Then
                                    ayPago(iNumPago) = m_oProce.Items.Item(CStr(oAdj.Id)).PagCod
                                    iNumPago = iNumPago + 1
                                    ReDim Preserve ayPago(UBound(ayPago) + 1)
                                End If
                            End If
                        End If

                        If .Bookmarks.Exists("DEN_PAGO") Then
                            .Bookmarks("DEN_PAGO").Range.Text = NullToStr(oPagos.Item(m_oProce.Items.Item(CStr(oAdj.Id)).PagCod))
                            If .Bookmarks.Exists("PAGO") Then
                                If Not BuscarEnArray(ayPago, m_oProce.Items.Item(CStr(oAdj.Id)).PagCod) Then
                                    ayPago(iNumPago) = m_oProce.Items.Item(CStr(oAdj.Id)).PagCod
                                    iNumPago = iNumPago + 1
                                    ReDim Preserve ayPago(UBound(ayPago) + 1)
                                End If
                            End If
                        End If
                    End If
                    If Fecha = True Then
                        If .Bookmarks.Exists("FINI_SUMINISTRO") Then
                            .Bookmarks("FINI_SUMINISTRO").Range.Text = Format(NullToStr(m_oProce.Items.Item(CStr(oAdj.Id)).FechaInicioSuministro), "Short Date")
                        End If

                        If .Bookmarks.Exists("FFIN_SUMINISTRO") Then
                            .Bookmarks("FFIN_SUMINISTRO").Range.Text = Format(NullToStr(m_oProce.Items.Item(CStr(oAdj.Id)).FechaFinSuministro), "Short Date")
                        End If
                    End If
                    .Bookmarks("ITEM").Delete
                    iNumEsp = iNumEsp + 1
                Next
                If blankword.Bookmarks.Exists("GRUPO_TOT") Then
                    rangewordGR.Copy
                    .Application.Selection.Paste
                    If .Bookmarks.Exists("TOTAL_GR_SIN") Then
                        .Bookmarks("TOTAL_GR_SIN").Range.Text = FormateoNumerico(dTotalGrupo, m_sFormatoNumber)
                    End If
                    dTotalGrupo = AplicarAtributosGrupo(dTotalGrupo, sGrupo)
                    If .Bookmarks.Exists("TOTAL_GR") Then
                        If m_sComentario <> "" Then
                            .Bookmarks("TOTAL_GR").Range.Select
                            .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                            .Application.Selection.TypeText Text:=m_sComentario
                            .Application.activewindow.ActivePane.Close
                            .Application.Selection.MoveRight Unit:=1, Count:=2
                        End If
                        .Bookmarks("TOTAL_GR").Range.Text = FormateoNumerico(dTotalGrupo, m_sFormatoNumber)
                    End If
                    .Bookmarks("GRUPO_TOT").Delete
                End If
                dTotalGen = dTotalGen + dTotalGrupo
                
                If .Bookmarks.Exists("TOTAL_SIN") Then
                    .Bookmarks("TOTAL_SIN").Range.Text = FormateoNumerico(dTotalGen, m_sFormatoNumber)
                End If
                dTotalGen = AplicarAtributosProce(dTotalGen)
                If .Bookmarks.Exists("TOTAL") Then
                    If m_sComentario <> "" Then
                        .Bookmarks("TOTAL").Range.Select
                        .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                        .Application.Selection.TypeText Text:=m_sComentario
                        .Application.activewindow.ActivePane.Close
                    End If
                    .Bookmarks("TOTAL").Range.Text = FormateoNumerico(dTotalGen, m_sFormatoNumber)
                End If
             End If
            
            'Especificaciones a nivel de grupo
            
            If m_oProce.DefEspGrupos = True Then
            
                If .Bookmarks.Exists("GRUPO_ESP") Then
                    .Bookmarks("GRUPO_ESP").Select
                    Set rangeword = .Bookmarks("GRUPO_ESP").Range
                    rangeword.Copy
                    .Application.Selection.Delete
                    iNumEsp = 0
                    If m_oProce.DefEspGrupos Then
                        For Each oGrupo In oGruposProce
                            oGrupo.CargarTodasLasEspecificaciones
                            If Not oGrupo.esp = "" Then
                                .Application.Selection.Paste
                                .Bookmarks("COD_ESP_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                                .Bookmarks("DEN_ESP_GRUPO").Range.Text = NullToStr(oGrupo.esp)
                                .Bookmarks("GRUPO_ESP").Delete
                                iNumEsp = iNumEsp + 1
                            End If
                        Next
                    End If
                End If
                If .Bookmarks.Exists("GRUPO_ADJ") Then
                    .Bookmarks("GRUPO_ADJ").Select
                    Set rangeword = .Bookmarks("GRUPO_ADJ").Range
                    rangeword.Copy
                    .Application.Selection.Delete
                    iNumFich = 0

                    For Each oGrupo In oGruposProce
                        oGrupo.CargarTodasLasEspecificaciones
                        If oGrupo.especificaciones.Count > 0 Then
                            For Each oEsp In oGrupo.especificaciones
                                .Application.Selection.Paste
                                    .Bookmarks("COD_ADJ_GRUPO").Range.Text = oGrupo.Codigo
                                    .Bookmarks("NOM_ADJ_GRUPO").Range.Text = oEsp.nombre
                                    .Bookmarks("FEC_ADJ_GRUPO").Range.Text = Format(oEsp.Fecha, "Short Date")
                                    .Bookmarks("DEN_ADJ_GRUPO").Range.Text = NullToStr(oEsp.Comentario)
                                iNumFich = iNumFich + 1
                            Next
                        End If
                     Next
                End If
                                
            Else
                .Bookmarks("GRUPO_ESPEC").Range.Delete
                .Bookmarks("GRUPO_ADJ").Range.Delete
                .Bookmarks("GRUPO_ADJUNTOS").Range.Delete
            End If

                
                ' ESPECIFICACIONES A NIVEL DE ITEM
            If m_oProce.DefEspItems = True Then
                If .Bookmarks.Exists("ITEM_ESP") Then
                    .Bookmarks("ITEM_ESP").Select
                    Set rangeword = .Bookmarks("ITEM_ESP").Range
                    rangeword.Copy
                    .Application.Selection.Delete
                    iNumEsp = 1
                    iNumFich = 0
                    For Each oAdj In m_oAdjs
                        With m_oProce.Items.Item(CStr(oAdj.Id))
                            If .CargarEspGeneral <> "" Then
                                docword.Application.Selection.Paste
                                docword.Bookmarks("DEN_ESP_ITEM").Range.Text = NullToStr(.esp)
                                docword.Bookmarks("NUM_ESP_ITEM").Range.Text = iNumEsp
                                docword.Bookmarks("ITEM_ESP").Delete
                                iNumFich = iNumFich + 1
                            End If
                            iNumEsp = iNumEsp + 1
                        End With
                    Next
                End If
                    ' Ahora los ficheros adjuntos de item
                iNumEsp = 1
                If .Bookmarks.Exists("ITEM_ADJ") Then
                    .Bookmarks("ITEM_ADJ").Select
                    Set rangeword = .Bookmarks("ITEM_ADJ").Range
                    rangeword.Copy
                    .Application.Selection.Delete
                    iNumFich2 = 0
                    For Each oAdj In m_oAdjs
                        With m_oProce.Items.Item(CStr(oAdj.Id))
                       
                        .CargarTodasLasEspecificaciones
                        If .especificaciones.Count <> 0 Then
                            For Each oEsp In .especificaciones
                                    docword.Application.Selection.Paste
                                    docword.Bookmarks("NUM_ADJ_ITEM").Range.Text = iNumEsp
                                    docword.Bookmarks("NOM_ADJ_ITEM").Range.Text = oEsp.nombre
                                    docword.Bookmarks("FEC_ADJ_ITEM").Range.Text = Format(oEsp.Fecha, "Short Date")
                                    docword.Bookmarks("DEN_ADJ_ITEM").Range.Text = NullToStr(oEsp.Comentario)
                                    docword.Bookmarks("ITEM_ADJ").Delete
                            Next
                            iNumFich2 = iNumFich2 + 1
                        End If
                        iNumEsp = iNumEsp + 1
                        End With
                    Next
                End If
                
            Else
                  .Bookmarks("ITEM_ESPEC").Range.Delete
                  .Bookmarks("ITEM_ADJUNTOS").Range.Delete
            End If
                
                
       
           If m_oProce.DefDestino = EnProceso Then
                If .Bookmarks.Exists("DESTINO") Then
                    If Not BuscarEnArray(ayDest, oDestinos.Item(m_oProce.DestCod).Cod) Then
                           ayDest(INUMDEST) = oDestinos.Item(m_oProce.DestCod).Cod
                           INUMDEST = INUMDEST + 1
                           ReDim Preserve ayDest(UBound(ayDest) + 1)
                    End If
                End If
            End If
            If .Bookmarks.Exists("DESTINO") Then
                 .Bookmarks("DESTINO").Select
                 Set rangeword = .Bookmarks("DESTINO").Range
                 rangeword.Copy
                 .Application.Selection.Delete
                 For INUMDEST = 0 To UBound(ayDest) - 1
                     .Application.Selection.Paste
                     .Bookmarks("DEST_COD").Range.Text = ayDest(INUMDEST)
                     .Bookmarks("DEST_DIR").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).dir)
                     .Bookmarks("DEST_POB").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).POB)
                     .Bookmarks("DEST_CP").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).cP)
                     .Bookmarks("DEST_DEN").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                     .Bookmarks("DEST_PROV").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).Provi)
                     .Bookmarks("DEST_PAIS").Range.Text = NullToStr(oDestinos.Item(ayDest(INUMDEST)).Pais)
                 Next
             End If
            
            If m_oProce.DefFormaPago = EnProceso Then
                If .Bookmarks.Exists("PAGO") Then
                    If Not BuscarEnArray(ayPago, oPagos.Item(m_oProce.PagCod).Cod) Then
                        ayPago(iNumPago) = oPagos.Item(m_oProce.PagCod).Cod
                        iNumPago = iNumPago + 1
                        ReDim Preserve ayPago(UBound(ayPago) + 1)
                    End If
                End If
            End If
            If .Bookmarks.Exists("PAGO") Then
                    .Bookmarks("PAGO").Select
                    Set rangeword = .Bookmarks("PAGO").Range
                    rangeword.Copy
                    .Application.Selection.Delete
                   For iNumPago = 0 To UBound(ayPago) - 1
                        .Application.Selection.Paste
                        .Bookmarks("PAGO_COD").Range.Text = ayPago(iNumPago)
                        .Bookmarks("PAGO_DEN").Range.Text = NullToStr(oPagos.Item(ayPago(iNumPago)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                    Next
            End If
                    
    End With
                
            
    If Not docword Is Nothing Then
        Dim oContrato As CContrato
        Dim sRuta As String
        Set oContrato = oFSGSRaiz.Generar_CContrato
        
        sRuta = oContrato.DevolverRutaContrato
        Set oContrato = Nothing
        If sRuta <> "" Then
            If InStr(Len(sRuta), sRuta, "\") = 0 Then
                sRuta = sRuta & "\"
            
            End If
        End If
        
        sTemp = GenerateRandomPath
        sTemp = sRuta & sTemp

        If Not oFos.FolderExists(sTemp) Then
            oFos.CreateFolder (sTemp)
        End If
        
        docword.SaveAs filename:=sTemp & "\" & txtNom.Text & ".doc", fileformat:=0
        
        docword.Close False
        blankword.Close False

        m_sCarpeta = sTemp
        m_sArchivo = txtNom.Text & ".doc"
    End If
    
    

    GenerarArchivoContrato = teserror

    appword.Options.SavePropertiesPrompt = bSalvar
    
    Unload frmESPERA
    If bVisibleWord = False Then
        ' queda una tarea
        ' de word abierta que no se destruye con appword = nothing
        appword.Quit
    End If
    
    Set appword = Nothing
    Set docword = Nothing
    Set rangeword = Nothing
    Set rangewordGR = Nothing
    Set oEquipo = Nothing
    Set oiasignaciones = Nothing
    Set oAsignaciones = Nothing
    Set oIAdjudicaciones = Nothing
    Set oEsp = Nothing
    Set oItem = Nothing
    Set m_oAdjs = Nothing
    Set oAdj = Nothing
    Set oItem = Nothing
    Set oCon = Nothing
    Set oDestinos = Nothing
    Set oPagos = Nothing
    Set oUnidades = Nothing
    Set oAdjsAux = Nothing
    Set oGruposProce = Nothing
    Set oProves = Nothing
    Unload frmESPERA
    
    Exit Function
    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
       If err.Number = 462 Then ' the remote server machine... " han cerrado el word"
           Set appword = Nothing
           Set appword = CreateObject("Word.Application")
           appword.Options.SavePropertiesPrompt = False
           Set docword = appword.Documents.Add(g_sPlantilla)
            If m_iWordVer >= 9 Then
                If appword.Visible Then docword.activewindow.WindowState = 2
            Else
                appword.Windows(docword.Name).WindowState = 1
            End If
           teserror.NumError = TESOtroerror
        ElseIf err.Number = 5941 Then  'Marcador no encontrado

            Resume Next
        Else
          m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "GenerarArchivoContrato", err, Erl, , m_bActivado)
          Exit Function
        End If
   End If
        
End Function


Private Sub sdbcIdiomas_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcIdiomas.DataFieldList = "Column 0"
    sdbcIdiomas.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcIdiomas_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcContacto_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcContacto.DroppedDown Then
        sdbcContacto.Text = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcContacto_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Seleccion del contacto del proveedor
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0seg.</remarks>
Private Sub sdbcContacto_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcContacto.Value = "" Then
        sdbcContacto.Text = ""
        Exit Sub
    End If
    
    If sdbcContacto.Text = "" Then
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcContacto.Text = sdbcContacto.Columns(0).Text
    m_lIdContacto = sdbcContacto.Columns(1).Value
    m_bRespetarCombo = False
    
    DoEvents
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcContacto_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Carga los contactos del proveedor en la cambo
''' </summary>
''' <remarks>Tiempo m�ximo:0,3seg.</remarks>
Private Sub sdbcContacto_DropDown()
    Dim oCon As CContacto
        
    'Carga los contactos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcContacto.RemoveAll
    
    Set m_oProve = oFSGSRaiz.generar_CProveedor
    m_oProve.Cod = sdbcProveedores.Value
    m_oProve.CargarTodosLosContactos , , , , True
    m_lIdContacto = 0
    For Each oCon In m_oProve.Contactos
        sdbcContacto.AddItem oCon.Apellidos & ", " & oCon.nombre & Chr(m_lSeparador) & oCon.Id
    Next
    
    sdbcContacto.SelStart = 0
    sdbcContacto.SelLength = Len(sdbcContacto.Text)
    sdbcContacto.Refresh
    Screen.MousePointer = vbNormal
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcContacto_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcContacto_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcContacto.DataFieldList = "Column 0"
    sdbcContacto.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcContacto_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcContacto_PositionList(ByVal Text As String)
Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcContacto.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcContacto.Rows - 1
            bm = sdbcContacto.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcContacto.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcContacto.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcContacto_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Seleccion de proveedor
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0seg.</remarks>
Private Sub sdbcProveedores_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveedores.Value = "..." Or Trim(sdbcProveedores.Value) = "" Then
        sdbcProveedores.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcProveedores.Text = sdbcProveedores.Columns(0).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    If Not g_oProveedores Is Nothing Then
        Set m_oProve = g_oProveedores.Item(sdbcProveedores.Value)
        Set g_oProveSeleccionado = g_oProveedores.Item(sdbcProveedores.Value)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcProveedores_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveedores_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcContacto.Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcProveedores_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveedores_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveedores.DataFieldList = "Column 1"
    sdbcProveedores.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcProveedores_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoCod_Change()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcTipoDen.Text = ""
        m_bRespetarCombo = False
        
        If sdbcTipoCod.Text <> "" Then
            m_bCargarComboDesde = True
        Else
            m_bCargarComboDesde = False
        End If
        Set m_oTipoSeleccionado = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcTipoCod_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcTipoCod.Value = "..." Or Trim(sdbcTipoCod.Value) = "" Then
        sdbcTipoCod.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcTipoDen.Text = sdbcTipoCod.Columns(1).Text
    sdbcTipoCod.Text = sdbcTipoCod.Columns(0).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    Set m_oTipoSeleccionado = m_oTiposContrato.Item(sdbcTipoCod.Text)
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Carga de contratos en el combo
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0,2seg.</remarks>
Private Sub sdbcTipoCod_DropDown()
    
            
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    
    CargarContratos
    
    Dim oSolic As CSolicitud
    sdbcTipoCod.RemoveAll
    If Not m_oTiposContrato Is Nothing Then
        For Each oSolic In m_oTiposContrato
            sdbcTipoCod.AddItem oSolic.Codigo & Chr(m_lSeparador) & NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        Next
    End If
        
    Set oSolic = Nothing
    


    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcTipoCod.DataFieldList = "Column 0"
    sdbcTipoCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub sdbcTipoCod_PositionList(ByVal Text As String)
        
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcTipoCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoCod.Rows - 1
            bm = sdbcTipoCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipoCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcTipoCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoCod_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>
''' Comprueba si es correcto el contrato introducido
''' </summary>
''' <param name="Cancel">Explicaci�n par�metro 1</param>
''' <remarks>Llamada desde; Tiempo m�ximo:0,2</remarks>
Private Sub sdbcTipoCod_Validate(Cancel As Boolean)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Trim(sdbcTipoCod.Value) = "" Then Exit Sub
    
    If UCase(Trim(sdbcTipoCod.Value)) = UCase(Trim(sdbcTipoCod.Columns(0).Value)) Then
        m_bRespetarCombo = True
        sdbcTipoDen = sdbcTipoCod.Columns(1).Value
        m_bRespetarCombo = False
        Exit Sub
    End If
    
    Set m_oTipoSeleccionado = Nothing
    Screen.MousePointer = vbHourglass
    
'    m_oTiposContrato.DevolverSolicitudesDeTipoContratoUsuarioPeticionario oUsuarioSummit.Cod, sdbcTipoCod.Text, , True
'
    CargarContratos
    Screen.MousePointer = vbNormal
    
    If m_oTiposContrato.Item(1) Is Nothing Then
        oMensajes.NoValido 123 ' Tipo de contrato
        sdbcTipoCod.Text = ""
        Exit Sub
    Else
        If UCase(m_oTiposContrato.Item(1).Codigo) <> UCase(sdbcTipoCod.Value) Then
            oMensajes.NoValido 123 ' Tipo de contrato
            sdbcTipoCod.Text = ""
            Exit Sub
        Else
            m_bRespetarCombo = True
            sdbcTipoDen.Text = NullToStr(m_oTiposContrato.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            sdbcTipoCod.Columns(0).Value = sdbcTipoCod.Text
            sdbcTipoCod.Columns(1).Value = sdbcTipoDen.Text
            m_bRespetarCombo = False
            m_bCargarComboDesde = False
            Set m_oTipoSeleccionado = m_oTiposContrato.Item(1)
        End If
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub sdbcTipoDen_Change()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If Not m_bRespetarCombo Then
        m_bRespetarCombo = True
        sdbcTipoCod.Text = ""
        m_bRespetarCombo = False
        
        If sdbcTipoDen.Text <> "" Then
            m_bCargarComboDesde = True
        Else
            m_bCargarComboDesde = False
        End If
        Set m_oTipoSeleccionado = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcTipoDen_CloseUp()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcTipoDen.Value = "....." Or Trim(sdbcTipoDen.Value) = "" Then
        sdbcTipoDen.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcTipoCod.Text = sdbcTipoDen.Columns(1).Text
    sdbcTipoDen.Text = sdbcTipoDen.Columns(0).Text
    m_bRespetarCombo = False
    
    m_bCargarComboDesde = False
    
    Set m_oTipoSeleccionado = m_oTiposContrato.Item(sdbcTipoCod.Text)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga los contratos en la combo
''' </summary>
''' <remarks>Llamada desde; Tiempo m�ximo:0,2seg.</remarks>
Private Sub sdbcTipoDen_DropDown()
    Dim oSolic As CSolicitud

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcTipoDen.RemoveAll
    CargarContratos
    For Each oSolic In m_oTiposContrato
        sdbcTipoDen.AddItem NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & oSolic.Codigo
    Next
        
    Set oSolic = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoDen_InitColumnProps()
    sdbcTipoDen.DataFieldList = "Column 0"
    sdbcTipoDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcTipoDen_PositionList(ByVal Text As String)
    
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcTipoDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoDen.Rows - 1
            bm = sdbcTipoDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipoDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcTipoDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "sdbcTipoDen_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub CargarRecursos()
    Dim ador As ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONTR_WIZARD, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.caption = ador(0).Value '1
        m_sIdiCaption = ador(0).Value
        ador.MoveNext
        lblSel.caption = ador(0).Value
        ador.MoveNext
        lblNom.caption = ador(0).Value
        m_sIdiNombre = ador(0).Value 'Nombre del contrato
        ador.MoveNext
        lblTipo.caption = ador(0).Value
        ador.MoveNext
        opProce.caption = ador(0).Value
        ador.MoveNext
        opGrupo.caption = ador(0).Value
        ador.MoveNext
        opItem.caption = ador(0).Value
        ador.MoveNext
        cmdCancelar.caption = ador(0).Value
        ador.MoveNext 'Finalizar ya no se usa
        ador.MoveNext
        lblIdi.caption = ador(0).Value 'Idioma para el documento de contrato:
        ador.MoveNext
        m_sIdiIdioma = ador(0).Value 'Idioma
        ador.MoveNext
        m_sIdiPlantilla = ador(0).Value 'Plantilla
        ador.MoveNext
        sdbcTipoCod.Columns(0).caption = ador(0).Value
        sdbcTipoDen.Columns(1).caption = ador(0).Value
        ador.MoveNext
        sdbcTipoCod.Columns(1).caption = ador(0).Value
        sdbcTipoDen.Columns(0).caption = ador(0).Value
        ador.MoveNext
        lblContac.caption = ador(0).Value
        ador.MoveNext
        m_stxtImp = ador(0).Value
        ador.MoveNext
        lblProveedor.caption = ador(0).Value
        ador.MoveNext
        m_sLiteralGeneracionContrato = ador(0).Value 'Generaci�n de contrato
        ador.Close
    End If

    Set ador = Nothing

    cmdContinuar.caption = "           >>"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub CargarAtributos()
    Dim oatrib As CAtributo
    Dim oAtribGr As CAtribTotalGrupo
    Dim iAmbProce As Integer
    Dim iAmbGrupo As Integer
    Dim iAmbItem As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_iAplicarTotalProce = 0
    
    'Carga los atributos de proceso (�mbito=1), definiciones 'CProceso.Atributos
    m_oProce.CargarAtributos , , , False, AmbProceso
    
    'Carga los atributos de grupo (�mbito=2), definiciones 'CProceso.AtributosGrupo
    m_oProce.CargarAtributos , , , False, AmbGrupo
    
    'Carga los atributos de item (�mbito=3), definiciones  'CProceso.AtributosItem
    m_oProce.CargarAtributos , , , False, AmbItem
    
    'ahora cargo en la clase m_oAtribsFormulas los atributos que tienen f�rmulas a aplicar.Esta clase la uso para que
    'sea m�s f�cil aplicar las f�rmulas de los atributos,por el orden.
    Set m_oAtribsFormulas = Nothing
    Set m_oAtribsFormulas = oFSGSRaiz.Generar_CAtributos
    
    iAmbItem = 0
    'a�ado a la clase los atributos de �mbito item
    If Not m_oProce.AtributosItem Is Nothing Then
        For Each oatrib In m_oProce.AtributosItem
            If Not IsNull(oatrib.PrecioAplicarA) And oatrib.UsarPrec = 1 Then
                m_oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden
                iAmbItem = iAmbItem + 1
            End If
        Next
    End If
    
     'a�ado a la clase los atributos de �mbito grupo
     iAmbGrupo = 0
    If Not m_oProce.AtributosGrupo Is Nothing Then
        For Each oatrib In m_oProce.AtributosGrupo
            If Not IsNull(oatrib.PrecioAplicarA) Then
                If oatrib.PrecioAplicarA = 1 Then
                    oatrib.CargarUsarPrecTotalGrupo
                    For Each oAtribGr In oatrib.AtribTotalGrupo
                        If oAtribGr.UsarPrec = 1 Then
                            If m_oAtribsFormulas.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                m_oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden
                                m_oAtribsFormulas.Item(CStr(oatrib.idAtribProce)).CargarUsarPrecTotalGrupo
                                iAmbGrupo = iAmbGrupo + 1
                            End If
                        End If
                    Next
                ElseIf oatrib.UsarPrec = 1 Then
                    m_oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden
                    iAmbGrupo = iAmbGrupo + 1
                End If
            End If
        Next
    End If
    
     'a�ado a la clase los atributos de �mbito proceso
     iAmbProce = 0
    If Not m_oProce.ATRIBUTOS Is Nothing Then
        For Each oatrib In m_oProce.ATRIBUTOS
            If Not IsNull(oatrib.PrecioAplicarA) Then
                If oatrib.PrecioAplicarA = 1 Then
                    oatrib.CargarUsarPrecTotalGrupo
                    For Each oAtribGr In oatrib.AtribTotalGrupo
                        If oAtribGr.UsarPrec = 1 Then
                            If m_oAtribsFormulas.Item(CStr(oatrib.Id)) Is Nothing Then
                                m_oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden
                                m_oAtribsFormulas.Item(CStr(oatrib.Id)).CargarUsarPrecTotalGrupo
                                iAmbProce = iAmbProce + 1
                            End If
                        End If
                    Next
                ElseIf oatrib.UsarPrec = 1 Then
                    m_oAtribsFormulas.Add oatrib.Id, oatrib.Cod, oatrib.Den, oatrib.Tipo, oatrib.PreferenciaValorBajo, oatrib.GMN1, oatrib.GMN2, oatrib.GMN3, oatrib.GMN4, oatrib.Heredado, oatrib.Articulo, oatrib.indice, oatrib.valor, oatrib.FECACT, oatrib.IDPlantilla, oatrib.AnyoProce, oatrib.CodProce, oatrib.codgrupo, oatrib.idAtribProce, oatrib.TipoIntroduccion, oatrib.Minimo, oatrib.Maximo, oatrib.BajaLogica, oatrib.interno, oatrib.ambito, oatrib.Obligatorio, oatrib.PrecioFormula, oatrib.PrecioAplicarA, oatrib.PrecioDefecto, oatrib.TipoPonderacion, , oatrib.Formula, oatrib.Numero, oatrib.ValorPondSi, oatrib.ValorPondNo, True, oatrib.UsarPrec, oatrib.Orden
                    iAmbProce = iAmbProce + 1
                End If
            End If
        Next
    End If
    
    'Cargamos los atributos ofertados
    If m_oAtribsFormulas.Count > 0 Then
        If iAmbProce > 0 Then 'Cargo en COferta.AtribProcOfertados
            m_oOferta.CargarAtributosProcesoOfertados
        End If
        If iAmbGrupo > 0 Then 'Cargo en COferta.AtribGrOfertados
            m_oOferta.CargarAtributosGrupoOfertados
        End If
        If iAmbItem > 0 Then 'Cargo en COferta.AtribItemOfertados
            m_oOferta.CargarAtributosItemOfertados
        End If
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "CargarAtributos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function AplicarAtributosTotalItem(ByVal dImporte As Double, ByVal oItem As CItem) As Double
    Dim scod1 As String
    Dim bSiguiente As Boolean
    Dim dImporteProve As Double
    Dim lngMinimo As Long
    Dim oatrib As CAtributo
    Dim iOrden As Integer
    Dim dValorAtrib As Double
    Dim lAtribAnterior As Long
  

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sComentario = ""
    'Comprueba si est� asignado el item a un s�lo proveedor para aplicarle los atributos
    If Not ComprobarAplicarAtribsItem(oItem.Id) Then
        AplicarAtributosTotalItem = dImporte
        Exit Function
    End If
    
    Screen.MousePointer = vbHourglass
    dImporteProve = dImporte
    
    If Not m_oOferta Is Nothing Then
        lAtribAnterior = 0
        bSiguiente = False
        iOrden = 1
    
        While bSiguiente = False
            lngMinimo = 0
            For Each oatrib In m_oAtribsFormulas
                'si el atributo aplica la f�rmula al total del item
                If oatrib.codgrupo = oItem.grupoCod Or IsNull(oatrib.codgrupo) Then
                    If oatrib.PrecioAplicarA = 2 And oatrib.UsarPrec = 1 Then
                        If oatrib.Orden = iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                            lngMinimo = oatrib.idAtribProce
                            Exit For
                        Else
                            If oatrib.Orden > iOrden And lAtribAnterior <> oatrib.idAtribProce Then
                                If lngMinimo = 0 Then
                                    lngMinimo = oatrib.idAtribProce
                                ElseIf (oatrib.Orden < m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                    lngMinimo = oatrib.idAtribProce
                                End If
                            End If
                        End If
                    End If
                End If
            Next
    
            If (lngMinimo = 0) Then
                bSiguiente = True
            ElseIf (m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                bSiguiente = True
            Else
                dValorAtrib = 0
                Set oatrib = m_oAtribsFormulas.Item(CStr(lngMinimo))
                lAtribAnterior = oatrib.idAtribProce
                iOrden = oatrib.Orden
                Select Case oatrib.ambito
                Case 1
                    'Proceso
                    If Not m_oOferta.AtribProcOfertados Is Nothing Then
                        If Not m_oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                            dValorAtrib = NullToDbl0(m_oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                        End If
                    End If
    
                Case 2
                    'Grupo
                    If Not m_oOferta.AtribGrOfertados Is Nothing Then
                        scod1 = oItem.grupoCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(oItem.grupoCod))
                        scod1 = scod1 & CStr(oatrib.idAtribProce)
                        If Not m_oOferta.AtribGrOfertados.Item(scod1) Is Nothing Then
                            dValorAtrib = NullToDbl0(m_oOferta.AtribGrOfertados.Item(scod1).valorNum)
                        End If
                    End If
    
                Case 3  'Item
                    If Not m_oOferta.AtribItemOfertados Is Nothing Then
                        scod1 = CStr(oItem.Id) & "$" & CStr(oatrib.idAtribProce)
                        If Not m_oOferta.AtribItemOfertados.Item(scod1) Is Nothing Then
                            dValorAtrib = NullToDbl0(m_oOferta.AtribItemOfertados.Item(scod1).valorNum)
                        End If
                    End If
                End Select
                'Para no repetir todos los ifs
                If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                    Select Case oatrib.PrecioFormula
                    Case "+"
                        dImporteProve = dImporteProve + dValorAtrib
                        m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / m_oOferta.Cambio & """" & vbLf
                    Case "-"
                        dImporteProve = dImporteProve - dValorAtrib
                        m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / m_oOferta.Cambio & """" & vbLf
                    Case "/"
                        dImporteProve = dImporteProve / dValorAtrib
                        m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib & """" & vbLf
                    Case "*"
                        dImporteProve = dImporteProve * dValorAtrib
                        m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib & """" & vbLf
                    Case "+%"
                        dImporteProve = dImporteProve + (dImporteProve * (dValorAtrib / 100))
                        m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib & """" & vbLf
                    Case "-%"
                        dImporteProve = dImporteProve - (dImporteProve * (dValorAtrib / 100))
                        m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib & """" & vbLf
                    End Select
                End If
                                    
                Set oatrib = Nothing
                bSiguiente = False
                iOrden = iOrden + 1
    
            End If
        Wend
            
                
    End If

    Screen.MousePointer = vbNormal
    
    AplicarAtributosTotalItem = dImporteProve
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "AplicarAtributosTotalItem", err, Erl, , m_bActivado)
      Exit Function
   End If
    
End Function

Private Function AplicarAtributosGrupo(ByVal dImporte As Double, ByVal sGrupo As String) As Double
Dim dAdjudicado As Double
Dim oatrib As CAtributo
Dim scod1 As String
Dim lAtribAnterior As Long
Dim bSiguiente As Boolean
Dim dValorAtrib As Double
Dim iOrden As Integer
Dim lngMinimo As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sComentario = ""
    
    If Not ComprobarAplicarAtributosGrupos(sGrupo) Then
        AplicarAtributosGrupo = dImporte
        Exit Function
    End If
    
    Screen.MousePointer = vbHourglass

    dAdjudicado = dImporte
                
        '**** APLICA LOS ATRIBUTOS A APLICAR AL TOTAL DEL GRUPO ****
        'Ahora aplica las operaciones para los atributos de grupo.Aplica las f�rmulas al importe obtenido
    If Not m_oOferta Is Nothing Then
         lAtribAnterior = 0
         bSiguiente = False
         iOrden = 1
         While bSiguiente = False
             lngMinimo = 0
             For Each oatrib In m_oAtribsFormulas
                 'si el atributo aplica la f�rmula al total del grupo
                 If oatrib.PrecioAplicarA = 1 Then
                     If Not oatrib.AtribTotalGrupo.Item(sGrupo) Is Nothing Then
                         If oatrib.AtribTotalGrupo.Item(sGrupo).UsarPrec = 1 Then
                            If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                lngMinimo = oatrib.idAtribProce
                                Exit For
                            Else
                                If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                                    If lngMinimo = 0 Then
                                        lngMinimo = oatrib.idAtribProce
                                        'lAtribAnterior = oAtrib.idAtribProce
                                    ElseIf (oatrib.Orden < m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                        lngMinimo = oatrib.idAtribProce
                                        'lAtribAnterior = oAtrib.idAtribProce
                                    End If
                                End If
                            End If
                         End If
                     End If
                 End If
             Next
             
             If (lngMinimo = 0) Then
                 bSiguiente = True
             ElseIf (m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
                 bSiguiente = True
             Else
                 dValorAtrib = 0
                 Set oatrib = m_oAtribsFormulas.Item(CStr(lngMinimo))
                 lAtribAnterior = oatrib.idAtribProce
                 iOrden = oatrib.Orden
                 Select Case oatrib.ambito
                     Case 1
                         'Proceso
                         If Not m_oOferta.AtribProcOfertados Is Nothing Then
                             If Not m_oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                                 dValorAtrib = NullToDbl0(m_oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                             End If
                         End If
                                 
                     Case 2
                         'Grupo
                         If oatrib.codgrupo = sGrupo Or IsNull(oatrib.codgrupo) Then
                             If Not m_oOferta.AtribGrOfertados Is Nothing Then
                                 scod1 = sGrupo & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(sGrupo))
                                 scod1 = scod1 & CStr(oatrib.idAtribProce)
                                 If Not m_oOferta.AtribGrOfertados.Item(scod1) Is Nothing Then
                                     dValorAtrib = NullToDbl0(m_oOferta.AtribGrOfertados.Item(scod1).valorNum)
                                 End If
                             End If
                         End If
                 End Select
                 If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                     Select Case oatrib.PrecioFormula
                         Case "+"
                            dAdjudicado = dAdjudicado + dValorAtrib
                            m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / m_oOferta.Cambio & """" & vbLf

                         Case "-"
                            dAdjudicado = dAdjudicado - dValorAtrib
                            m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / m_oOferta.Cambio & """" & vbLf
                         Case "/"
                            dAdjudicado = dAdjudicado / dValorAtrib
                            m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib & """" & vbLf
                         Case "*"
                            dAdjudicado = dAdjudicado * dValorAtrib
                            m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib & """" & vbLf
                         Case "+%"
                            dAdjudicado = dAdjudicado + ((dValorAtrib / 100) * dAdjudicado)
                            m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib & """" & vbLf
                         Case "-%"
                            dAdjudicado = dAdjudicado - ((dValorAtrib / 100) * dAdjudicado)
                            m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib & """" & vbLf
                     End Select
                 End If
                 
                 Set oatrib = Nothing
                 bSiguiente = False
                 iOrden = iOrden + 1
                     
             End If
        Wend
        
    End If
    
    Screen.MousePointer = vbNormal
    
    AplicarAtributosGrupo = dAdjudicado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "AplicarAtributosGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


Private Function ComprobarAplicarAtribsItem(ByVal lItem As Long) As Boolean
''' TRue si el item  s�lo est� adjudicado a un proveedor, False en caso contrario
Dim sProvAnt As String
Dim bAdj As Boolean
Dim oProv As CProveedor
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bAdj = True
    sProvAnt = m_oProve.Cod
    If Not m_oAdjudicaciones Is Nothing Then
        For Each oProv In m_oProvesAsig
            sCod = CStr(lItem) & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            If Not m_oAdjudicaciones.Item(sCod) Is Nothing Then
                If m_oAdjudicaciones.Item(sCod).Porcentaje > 0 Then
                    If sProvAnt <> oProv.Cod Then
                        bAdj = False
                        Exit For
                    End If
                End If
            End If
        Next
    End If
    
    ComprobarAplicarAtribsItem = bAdj
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "ComprobarAplicarAtribsItem", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function ComprobarAplicarAtributosGrupos(Optional ByVal sGrupo As String) As Boolean
Dim sProvAnt As String
Dim bSoloUno As Boolean
Dim bComprobar As Boolean
Dim oProv As CProveedor
Dim sCod As String
Dim oItem As CItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
bSoloUno = True
sProvAnt = m_oProve.Cod
If m_iAplicarTotalProce = 0 Then m_iAplicarTotalProce = 1

'Buscamos el proveedor al que est� adjudicado el grupo, si hay m�s de uno no se aplica nada
    For Each oProv In m_oProvesAsig
        'comprobamos que el precio de la oferta no sea nulo
        For Each oItem In m_oProce.Items
            If sGrupo <> "" Then
                If oItem.grupoCod = sGrupo Then
                    bComprobar = True
                Else
                    bComprobar = False
                End If
            Else
                bComprobar = True
            End If
            If bComprobar Then
                sCod = CStr(oItem.Id) & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
                If Not m_oAdjudicaciones Is Nothing Then
                    If Not m_oAdjudicaciones.Item(sCod) Is Nothing Then
                        If m_oAdjudicaciones.Item(sCod).Porcentaje > 0 Then
                            If sProvAnt <> oProv.Cod Then
                                bSoloUno = False
                                m_iAplicarTotalProce = 2
                                Exit For
                            End If
                        End If
                    End If
                End If
                If bSoloUno Then
                    If m_oAdjs.Item(sCod) Is Nothing Then
                    'Si hay alg�n item del grupo que no se incluye en el contrato
                    'no se aplican los atributos de grupo
                        bSoloUno = False
                        m_iAplicarTotalProce = 2
                        Exit For
                    End If
                End If
            End If
        Next
        If bSoloUno = False Then Exit For
    Next

ComprobarAplicarAtributosGrupos = bSoloUno
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "ComprobarAplicarAtributosGrupos", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function AplicarAtributosProce(ByVal dImporte As Double) As Double
Dim oatrib As CAtributo
Dim bSiguiente As Boolean
Dim dValorAtrib As Double
Dim dAdjudicadoProc As Double
Dim iOrden As Integer
Dim lAtribAnterior As Long
Dim lngMinimo As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sComentario = ""
    If m_iAplicarTotalProce = 0 Then
        ComprobarAplicarAtributosGrupos
    End If
    If m_iAplicarTotalProce = 2 Then
        AplicarAtributosProce = dImporte
        Exit Function
    End If
    
    Screen.MousePointer = vbHourglass
    
    dAdjudicadoProc = dImporte

    lAtribAnterior = 0
    iOrden = 1
    bSiguiente = False
    While bSiguiente = False
        lngMinimo = 0
        For Each oatrib In m_oAtribsFormulas
            If Not IsNull(oatrib.PrecioFormula) And oatrib.PrecioAplicarA = 0 Then
                If oatrib.UsarPrec = 1 Then
                    If oatrib.Orden = iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                        lngMinimo = oatrib.idAtribProce
                        Exit For
                    Else
                        If oatrib.Orden > iOrden And oatrib.idAtribProce <> lAtribAnterior Then
                            If lngMinimo = 0 Then
                                lngMinimo = oatrib.idAtribProce
                            ElseIf (oatrib.Orden < m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden) Then
                                lngMinimo = oatrib.idAtribProce
                            ElseIf oatrib.idAtribProce = lngMinimo Then
                                bSiguiente = True
                                Exit For
                            End If
                        End If
                    End If
                End If
            End If
        Next
        If (lngMinimo = 0) Then
            bSiguiente = True
        ElseIf (m_oAtribsFormulas.Item(CStr(lngMinimo)).Orden = iOrden - 1) Then
            bSiguiente = True
        Else
            dValorAtrib = 0
            Set oatrib = m_oAtribsFormulas.Item(CStr(lngMinimo))
            lAtribAnterior = oatrib.idAtribProce
            iOrden = oatrib.Orden
        
            If Not m_oOferta Is Nothing Then
                If Not m_oOferta.AtribProcOfertados Is Nothing Then
                    If Not m_oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                        dValorAtrib = NullToDbl0(m_oOferta.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum)
                        If Not IsNull(dValorAtrib) And dValorAtrib <> 0 Then
                            Select Case oatrib.PrecioFormula
                                Case "+"
                                    dAdjudicadoProc = dAdjudicadoProc + dValorAtrib
                                    m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " + " & dValorAtrib / m_oOferta.Cambio & """" & vbLf
                                Case "-"
                                    dAdjudicadoProc = dAdjudicadoProc - dValorAtrib
                                    m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " - " & dValorAtrib / m_oOferta.Cambio & """" & vbLf
                                Case "/"
                                    dAdjudicadoProc = dAdjudicadoProc / dValorAtrib
                                    m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " / " & dValorAtrib & """" & vbLf
                                Case "*"
                                    dAdjudicadoProc = dAdjudicadoProc * dValorAtrib
                                    m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " * " & dValorAtrib & """" & vbLf
                                Case "+%"
                                    dAdjudicadoProc = dAdjudicadoProc + ((dValorAtrib / 100) * dAdjudicadoProc)
                                    m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " +% " & dValorAtrib & """" & vbLf
                                Case "-%"
                                    dAdjudicadoProc = dAdjudicadoProc - ((dValorAtrib / 100) * dAdjudicadoProc)
                                    m_sComentario = m_sComentario & """" & oatrib.Cod & ": " & m_stxtImp & " -% " & dValorAtrib & """" & vbLf
                            End Select
                        End If
                    End If
                End If
            End If
            
            Set oatrib = Nothing
            bSiguiente = False
            iOrden = iOrden + 1
        End If
    Wend

    Screen.MousePointer = vbNormal
    
    AplicarAtributosProce = dAdjudicadoProc
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "AplicarAtributosProce", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


''' <summary>
''' Carga los contratos del proceso
''' </summary>
''' <remarks>Llamada desde:form_load; Tiempo m�ximo:0,3</remarks>
''' <revisado por>MMV 05/10/2011</revisado por>
Private Sub CargarContratos()
    Dim iAnyo As Integer
    Dim iCod As Long
    Dim sGMN1Cod As String
    Dim sDesdeCod, sDesdeDen As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bCargarComboDesde Then
        sDesdeCod = sdbcTipoCod.Value
        sDesdeDen = sdbcTipoDen.Value
    Else
        sDesdeCod = ""
        sDesdeDen = ""
    End If
    
    If Not m_oProcesoSeleccionado Is Nothing Then
        iAnyo = m_oProcesoSeleccionado.Anyo
        iCod = m_oProcesoSeleccionado.Cod
        sGMN1Cod = m_oProcesoSeleccionado.GMN1Cod
    End If
    
    m_oTiposContrato.CargarSolicitudesDeTipoContratoUsuarioPeticionario oUsuarioSummit.Persona.Cod, sDesdeCod, sDesdeDen, , iAnyo, sGMN1Cod, iCod, True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "CargarContratos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Devuelve una cadena de 10 caracteres aleatorios
''' </summary>
''' <returns>Cadena aleatoria de 10 numeros</returns>
''' <remarks>Llamada desde:=WizardContrato; Tiempo m�ximo</remarks>
Public Function GenerateRandomPath() As String
    Dim s As String
    Dim i As Integer
    Dim lowerbound As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lowerbound = 65
    Dim upperbound As Integer
    upperbound = 90
    Randomize
    For i = 1 To 10
        s = s & Chr(Int((upperbound - lowerbound + 1) * Rnd() + lowerbound))
    Next
    GenerateRandomPath = s
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "GenerateRandomPath", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
''' <summary>
'''  Funcion que devuelve el ID de Empresa de la distribucion del proceso en caso de que sea unica
'''  En caso contrario nos devolvera el ID de empresa del Usuario
''' </summary>
''' <param name="iAnyo">A�o del proceso</param>
''' <param name="sGMN1Cod">Codigo GMN1 proceso</param>
''' <param name="iCod">Codigo proceso</param>
''' <returns>ID Empresa</returns>
''' <remarks>Llamada desde:=WizardContrato; Tiempo m�ximo</remarks>
Function ObtenerEmpresa(ByVal iAnyo As Integer, ByVal sGMN1Cod As String, ByVal iCod As Long) As Long
Dim lResultado As Long
Dim oProceso As CProceso
Dim sGMN1 As String
Dim sGMN2 As String
Dim sGMN3 As String


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
lResultado = 0

If iAnyo > 0 Then
    Set oProceso = oFSGSRaiz.Generar_CProceso
    oProceso.Anyo = iAnyo
    oProceso.GMN1Cod = sGMN1Cod
    oProceso.Cod = iCod
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
        sGMN1 = oUsuarioSummit.Persona.UON1
        sGMN2 = oUsuarioSummit.Persona.UON2
        sGMN3 = oUsuarioSummit.Persona.UON3
        
    End If
    lResultado = oProceso.DevolverEmpresa(False, sGMN1, sGMN2, sGMN3)
    
    Set oProceso = Nothing
End If

ObtenerEmpresa = lResultado

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmCONTRWizard", "ObtenerEmpresa", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
