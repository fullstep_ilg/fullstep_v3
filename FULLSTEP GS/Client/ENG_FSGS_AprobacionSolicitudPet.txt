_______________________________________________________________________________________
		FULLSTEP GS
		AVISO DE APROBACI�N DE SOLICITUD
	
 Esta notificaci�n es para indicarle que su solicitud de compra @ID "@DESCR_BREVE" con fecha 
 de @FECHA_ALTA ha sido recibida y aprobada por "@APROBADOR_NOM @APROBADOR_APE".
 Puede acceder a la secci�n de seguimiento del E-Procurement para seguir la evoluci�n de 
 su solicitud.

 Comentarios del comprador:

 	@COMENTARIO	


 Un saludo

 
PD: A continuaci�n le presentamos la informaci�n relativa a su solicitud:
_______________________________________________________________________________________

Datos de la solicitud:
 
 Identificador:		@ID
 Descripci�n breve:		@DESCR_BREVE 
 Fecha de alta: 		@FECHA_ALTA
 Fecha de necesidad:	@FECHA_NECESIDAD
 Importe aproximado:	@IMPORTE (@MON_COD - @MON_DEN)
 Tipo de solicitud:     	@TIPOCOD - @TIPODEN	

_______________________________________________________________________________________

Datos del comprador:
 
 C�digo:			@APROBADOR_COD
 Nombre:			@APROBADOR_NOM @APROBADOR_APE
 Tel�fono:		@APROBADOR_TFNO
 Email:			@APROBADOR_EMAIL
 Fax:			@APROBADOR_FAX
 Unidad organizativa:	@APROBADOR_UON
 Departamento:		@APROBADOR_DEP_COD - @APROBADOR_DEP_DEN
_______________________________________________________________________________________

Datos del peticionario:

 C�digo:			@PET_COD
 Nombre:			@PET_NOM @PET_APE
 Tel�fono:		@PET_TFNO
 Email:			@PET_EMAIL
 Fax:			@PET_FAX
 Unidad organizativa:	@PET_UON
 Departamento:		@PET_DEP_COD - @PET_DEP_DEN
 
 
