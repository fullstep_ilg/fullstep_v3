VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPRESPorMat 
   Caption         =   "Presupuestos por material (Consulta)"
   ClientHeight    =   5415
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7605
   Icon            =   "frmPRESPorMat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5415
   ScaleWidth      =   7605
   Begin VB.Frame frmSel 
      Height          =   1800
      Left            =   60
      TabIndex        =   14
      Top             =   0
      Width           =   7425
      Begin VB.CommandButton cmdSelMat 
         Height          =   315
         Left            =   6090
         Picture         =   "frmPRESPorMat.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   180
         Width           =   345
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   1815
         TabIndex        =   2
         Top             =   600
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
         Height          =   285
         Left            =   1815
         TabIndex        =   4
         Top             =   960
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
         Height          =   285
         Left            =   1815
         TabIndex        =   6
         Top             =   1320
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
         Height          =   285
         Left            =   2895
         TabIndex        =   5
         Top             =   960
         Width           =   3540
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5054
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6244
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
         Height          =   285
         Left            =   2895
         TabIndex        =   7
         Top             =   1320
         Width           =   3540
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6244
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
         Height          =   285
         Left            =   2895
         TabIndex        =   3
         Top             =   600
         Width           =   3540
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4498
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1164
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6244
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   1830
         TabIndex        =   0
         Top             =   240
         Width           =   960
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1693
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAnyo 
         Caption         =   "A�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   690
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Commodity:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   120
         TabIndex        =   17
         Top             =   660
         Width           =   1680
      End
      Begin VB.Label lblGMN2_4 
         Caption         =   "Familia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   120
         TabIndex        =   16
         Top             =   1020
         Width           =   1650
      End
      Begin VB.Label lblGMN3_4 
         Caption         =   "Subfamilia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Left            =   120
         TabIndex        =   15
         Top             =   1380
         Width           =   1665
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3195
      Left            =   60
      ScaleHeight     =   3195
      ScaleWidth      =   7455
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   1860
      Width           =   7455
      Begin SSDataWidgets_B.SSDBGrid sdbgPresupuestos 
         Height          =   3120
         Left            =   0
         TabIndex        =   8
         Top             =   0
         Width           =   7395
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   1
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         BalloonHelp     =   0   'False
         RowNavigation   =   1
         CellNavigation  =   1
         MaxSelectedRows =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   1905
         Columns(0).Caption=   "C�digo grupo"
         Columns(0).Name =   "COD"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   3
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777152
         Columns(1).Width=   4260
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   50
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777152
         Columns(2).Width=   3069
         Columns(2).Caption=   "Presupuesto"
         Columns(2).Name =   "PRES"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   2
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "Standard"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1588
         Columns(3).Caption=   "Objetivo %"
         Columns(3).Name =   "OBJ"
         Columns(3).Alignment=   2
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "0.0#\%"
         Columns(3).FieldLen=   256
         _ExtentX        =   13044
         _ExtentY        =   5503
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   7605
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   4995
      Width           =   7605
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   60
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdFiltrar 
         Caption         =   "&Filtrar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6480
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   60
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmPRESPorMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'variables para el resize
Private iAltura As Double
Private iAnchura As Double

' Variables de seguridad
Public bRMat As Boolean
Private bModif As Boolean

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4

'Anyo seleccionado
Public iAnyoSeleccionado As Integer

'Control de flujo
Private Accion As accionessummit
Private bModoEdicion As Boolean
Private bModError As Boolean
Private bValError As Boolean


''' Coleccion de Presupuestos
Public oPresupuestos As CPresPorMateriales

''' Presupuesto en edicion
Private oPresupuestoEnEdicion As CPresPorMaterial
Private oPresupuestoAAnyadir As CPresPorMaterial
Private oIBAseDatosEnEdicion As IBaseDatos

''' Listado de PresPorProyectos
Public sOrdenListado As String
''' Indice para la grid
Private p As Long
''' Control de combos
Private GMN1RespetarCombo As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean

'Multilenguaje

Private sIdiTitulos(1 To 4) As String
Private sIdiPresupuesto As String
Private sIdiObjetivo As String
Private sIdiOrdenando As String
Private sIdiConsulta As String
Private sIdiEdicion As String
Private sIdiMaterial As String
    
Private Sub cmdSelMat_Click()
    
    frmSELMAT.sOrigen = "frmPRESPorMat"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1

End Sub

Private Sub Form_Activate()
    sdbcGMN1_4Cod.SetFocus
End Sub

Private Sub Form_Load()

Me.Height = 5820
Me.Width = 7695
If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

CargarRecursos
Show

CargarAnyos

iAnyoSeleccionado = Year(Date)
    
Accion = accionessummit.ACCPresMatCon

' Incializar la coleccion de presupuestos
Set oPresupuestos = oFSGSRaiz.Generar_CPresPorMateriales


ConfigurarNombres

ConfigurarSeguridad

End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
bModoEdicion = False
Set oPresupuestos = Nothing
Set oPresupuestoEnEdicion = Nothing
Set oGruposMN1 = Nothing
Set oGruposMN2 = Nothing
Set oGruposMN3 = Nothing
Set oGruposMN4 = Nothing
Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing

End Sub

Private Sub sdbcAnyo_Click()
    
    If iAnyoSeleccionado <> Int(sdbcAnyo.Value) Then
        sdbcGMN1_4Cod.Text = ""
    End If
        
    iAnyoSeleccionado = Int(sdbcAnyo.Value)
    
End Sub

Private Sub sdbcAnyo_CloseUp()
    
    If iAnyoSeleccionado <> Int(sdbcAnyo.Value) Then
        sdbcGMN1_4Cod.Text = ""
    End If
    
    iAnyoSeleccionado = Int(sdbcAnyo.Value)
    
    
End Sub


Private Sub GMN1Seleccionado()
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
   
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    sdbcGMN2_4Cod = ""
    sdbcGMN2_4Den = ""
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod.Text
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod.Text
        
    sdbcGMN3_4Cod = ""
    sdbcGMN3_4Den = ""
   
End Sub
Private Sub GMN3Seleccionado()

    Set oGMN3Seleccionado = Nothing
    
    If sdbcGMN3_4Cod.Text = "" Or sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
   
    Set oPresupuestos = Nothing
    Set oPresupuestos = oFSGSRaiz.Generar_CPresPorMateriales
    
    
    CargarPresupuestos
    
    If oPresupuestos.Count > 0 Then
        cmdFiltrar.Enabled = True
        cmdRestaurar.Enabled = True
        cmdModoEdicion.Enabled = True
    End If
    
End Sub
Private Sub ConfigurarNombres()

lblGMN1_4.Caption = gParametrosGenerales.gsDEN_GMN1 & ":"
lblGMN2_4.Caption = gParametrosGenerales.gsDEN_GMN2 & ":"
lblGMN3_4.Caption = gParametrosGenerales.gsDEN_GMN3 & ":"
sdbgPresupuestos.Columns(0).Caption = gParametrosGenerales.gsDEN_GMN4

End Sub

Public Sub PonerMatSeleccionadoEnCombos()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
        
    If Not oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    GMN3Seleccionado
           
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    
End Sub
Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATModificar)) Is Nothing) Then
        bModif = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATRestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
Else
    
    bModif = True

End If

If Not bModif Then
    
    cmdModoEdicion.Visible = False

End If

End Sub



Private Sub sdbgPresupuestos_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    
    bValError = False
    Cancel = False
    
    If sdbgPresupuestos.Columns(2).Value <> "" Then
        If Not IsNumeric(sdbgPresupuestos.Columns(2).Value) Then
'            basMensajes.NoValido "Presupuesto"
            basMensajes.NoValido sIdiPresupuesto
            
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If sdbgPresupuestos.Columns(3).Value <> "" Then
        If Not IsNumeric(sdbgPresupuestos.Columns(3).Value) Then
'            basMensajes.NoValido "Objetivo"
            basMensajes.NoValido sIdiObjetivo
            Cancel = True
            GoTo Salir
        End If
    End If
    
Salir:
        
    bValError = Cancel
    sdbgPresupuestos.SetFocus
    
End Sub
Private Sub sdbgPresupuestos_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacer.Enabled = False Then
    
        cmdDeshacer.Enabled = True
        
    End If
    
    If Accion = ACCPresMatCon Then
    
        Set oPresupuestoEnEdicion = Nothing
        
        Select Case gParametrosGenerales.giNEM
            
            Case 1
            
            Case 2
            
            Case 3
            
            Case 4
            
                     Set oPresupuestoEnEdicion = oPresupuestos.Item(CStr(sdbgPresupuestos.Bookmark))
        
        End Select
        
        ' Si no existe el presupuesto habra que anyadirlo
            If IsNull(oPresupuestoEnEdicion.Anyo) Then
            
            Set oPresupuestoAAnyadir = oPresupuestoEnEdicion
            Set oPresupuestoEnEdicion = Nothing
            
            Accion = accionessummit.ACCPresMatAnya
        Else
            
          
            Set oIBAseDatosEnEdicion = oPresupuestoEnEdicion
            
            teserror = oIBAseDatosEnEdicion.IniciarEdicion
            
            If teserror.NumError = TESInfModificada Then
                
                TratarError teserror
                sdbgPresupuestos.DataChanged = False
                
                sdbgPresupuestos.Columns(2).Value = oPresupuestoEnEdicion.Presupuesto
                sdbgPresupuestos.Columns(3).Value = oPresupuestoEnEdicion.Objetivo
                
                    
                teserror.NumError = TESnoerror
                
            End If
            
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                sdbgPresupuestos.SetFocus
            Else
                Accion = ACCPresMatMod
            End If
            
        End If
        
            
    End If
    
End Sub
Private Sub sdbgPresupuestos_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    
    Dim sCodigo As String
    Dim sDenominacion As String
    Dim lCodigo As Integer
    Dim lDenominacion As Integer
    Dim sHeadCaption As String
    Dim oIMAsig As IMaterialAsignado
    
    If bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgPresupuestos.Columns(ColIndex).Caption
'    sdbgPresupuestos.Columns(ColIndex).Caption = "Ordenando...."
    sdbgPresupuestos.Columns(ColIndex).Caption = sIdiOrdenando
    
    ''' Volvemos a cargar las PresPorMateriales, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    
    Select Case gParametrosGenerales.giNEM
    
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4 'Cuatro niveles de materiales
            
                        
'                If Caption <> "Presupuestos por material (Consulta)" Then
                If Caption <> sIdiTitulos(3) Then
                
                    If InStr(1, Caption, sIdiTitulos(1), vbTextCompare) <> 0 Then
                    
                        ''' Datos filtrados por codigo
                        
                        If InStr(1, Caption, "*", vbTextCompare) <> 0 Then
                            
                            lCodigo = Len(Caption) - Len(sIdiTitulos(1))
                            sCodigo = Mid(Caption, Len(sIdiTitulos(1)) + 1, lCodigo - 1)
'                            sCodigo = Right(Caption, Len(Caption) - 44)
'                            sCodigo = Left(sCodigo, Len(sCodigo) - 2)
                        
                            Select Case ColIndex
                            Case 0 'Ordenar por codigo de material
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , , , , , , True
                                End If
                     
                            Case 1    'Ordenar por denominacion
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , True, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , True, , , , , True
                                End If
                                     
                             Case 2  'Ordenar por presupuesto
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , , True, , , , True
                                End If
                                     
                             Case 3  'Ordenar por objetivo
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , , , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , , , , , , , , , True, , , True
                                End If
                            
                            End Select
                        
                        Else
                        
                            lCodigo = Len(Caption) - Len(sIdiTitulos(1))
                            sCodigo = Mid(Caption, Len(sIdiTitulos(1)) + 1, lCodigo)
'                            sCodigo = Right(Caption, Len(Caption) - 44)
'                            sCodigo = Left(sCodigo, Len(sCodigo) - 1)
                        
                            Select Case ColIndex
                            Case 0 'Ordenar por codigo de material
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , , , , , , True
                                End If
                     
                            Case 1    'Ordenar por denominacion
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , True, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , True, , , , , True
                                End If
                                     
                             Case 2  'Ordenar por presupuesto
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , , True, , , , True
                                End If
                                     
                             Case 3  'Ordenar por objetivo
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , , , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , sCodigo, , True, , , , , , , , True, , , True
                                End If
                            
                            End Select
                        
                       End If
                       
                    Else
                        
                        ''' Datos filtrados por denominacion
                        
                        lDenominacion = Len(Caption) - Len(sIdiTitulos(2))
                        sDenominacion = Mid(Caption, Len(sIdiTitulos(2)) + 1, lDenominacion)
'                       sDenominacion = Right(Caption, Len(Caption) - 50)
                        
                        If InStr(1, sDenominacion, "*", vbTextCompare) <> 0 Then
                        
                            sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
'                            sDenominacion = Left(sDenominacion, Len(sDenominacion) - 2)
                            
                            Select Case ColIndex
                            Case 0 'Ordenar por codigo de material
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , , , , , , True
                                End If
                     
                            Case 1    'Ordenar por denominacion
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , True, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , True, , , , , True
                                End If
                                     
                             Case 2  'Ordenar por presupuesto
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , , True, , , , True
                                End If
                                     
                             Case 3  'Ordenar por objetivo
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , , , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, , , , , , , , , True, , , True
                                End If
                            
                            End Select
                                        
                        Else
                            
'                            sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                            
                            Select Case ColIndex
                            Case 0 'Ordenar por codigo de material
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , , , , , , True
                                End If
                     
                            Case 1    'Ordenar por denominacion
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , True, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , True, , , , , True
                                End If
                                     
                             Case 2  'Ordenar por presupuesto
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , , True, , , , True
                                End If
                                     
                             Case 3  'Ordenar por objetivo
                                If bRMat Then
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , , , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                                Else
                                    oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , sDenominacion, True, , , , , , , , True, , , True
                                End If
                            
                            End Select
                        End If
                            
                    End If
                            
                Else
                
                    ''' Datos no filtrados
                    
                    Select Case ColIndex
                    
                        Case 0 'Ordenar por codigo de material
                            If bRMat Then
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                            Else
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , , , , , True
                            End If
                
                        Case 1    'Ordenar por denominacion
                        
                            If bRMat Then
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , True, , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                            Else
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , True, , , , , True
                            End If
                            
                    Case 2  'Ordenar por presupuesto
                            
                            If bRMat Then
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                            Else
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , True, , , , True
                            End If
            
                    Case 3  'Ordenar por objetivo
                            
                            If bRMat Then
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , , True, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                            Else
                                oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , , True, , , True
                            End If
            
                    End Select
                
                End If
                    
                Select Case ColIndex
                Case 0
                    sOrdenListado = "COD"
                Case 1
                    sOrdenListado = "DEN"
                Case 2
                    sOrdenListado = "PRES"
                Case 2
                    sOrdenListado = "OBJ"
                End Select
            
    End Select
    
    
    sdbgPresupuestos.ReBind
    sdbgPresupuestos.Columns(ColIndex).Caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbgPresupuestos_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgPresupuestos.DataChanged = False Then
            
            sdbgPresupuestos.CancelUpdate
            sdbgPresupuestos.DataChanged = False
            
            If Not oPresupuestoEnEdicion Is Nothing Then
                oIBAseDatosEnEdicion.CancelarEdicion
                Set oIBAseDatosEnEdicion = Nothing
                Set oPresupuestoEnEdicion = Nothing
            End If
           
            cmdDeshacer.Enabled = False
            
            Accion = ACCPresMatCon
            
        Else
        
            If sdbgPresupuestos.IsAddRow Then
           
                cmdDeshacer.Enabled = False
                
                Accion = ACCPresMatCon
            
            End If
            
        End If
        
    End If
    
End Sub

Private Sub sdbgPresupuestos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
        If sdbgPresupuestos.DataChanged = True Then cmdDeshacer.Enabled = True
                
End Sub
Private Sub sdbgPresupuestos_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim i As Integer
    Dim j As Integer
    
    Dim oPresMat As CPresPorMaterial
    
    Dim iNumPresPorMateriales As Integer

    If oPresupuestos Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If
    
    Select Case gParametrosGenerales.giNEM
    
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4
                'Cuatro niveles en la estructura de materiales
                
                iNumPresPorMateriales = oPresupuestos.Count
                
                If IsNull(StartLocation) Then       'If the grid is empty then
                    If ReadPriorRows Then               'If moving backwards through grid then
                        p = iNumPresPorMateriales - 1                             'pointer = # of last grid row
                    Else                                        'else
                        p = 0                                       'pointer = # of first grid row
                    End If
                Else                                        'If the grid already has data in it then
                    p = StartLocation                       'pointer = location just before or after the row where data will be added
                
                    If ReadPriorRows Then               'If moving backwards through grid then
                            p = p - 1                               'move pointer back one row
                    Else                                        'else
                            p = p + 1                               'move pointer ahead one row
                    End If
                End If
                
                'The pointer (p) now points to the row of the grid where you will start adding data.
                
                
                For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
                    
                    If p < 0 Or p > iNumPresPorMateriales - 1 Then Exit For           'If the pointer is outside the grid then stop this
                    
                    Set oPresMat = oPresupuestos.Item(CStr(p))
                    
                    For j = 0 To 3
                  
                        Select Case j
                                
                                Case 0:
                                        RowBuf.Value(i, 0) = oPresMat.Cod
                                        
                                Case 1:
                                        RowBuf.Value(i, 1) = oPresMat.Den
                                
                                Case 2:
                                        
                                        RowBuf.Value(i, 2) = oPresMat.Presupuesto
                                        
                                Case 3:
                                        
                                        RowBuf.Value(i, 3) = oPresMat.Objetivo
                                        
                        End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
                    
                    Next j
                
                    RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
                
                    If ReadPriorRows Then                               'move the pointer forward or backward, depending
                        p = p - 1                                           'on which way it's supposed to move
                    Else
                        p = p + 1
                    End If
                        r = r + 1                                               'increment the number of rows read
                    Next i
                
                RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read
            
                Set oPresMat = Nothing
            
        
        End Select
End Sub
Private Sub sdbgPresupuestos_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Objetivo: y si el presupuesto es nuevo, anyadirlo al BD
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim v As Variant
    
    bModError = False
    
    teserror.NumError = TESnoerror
    
    If bModError = False Then
        cmdDeshacer.Enabled = False
    End If
    
 If Accion = ACCPresMatAnya Then
 
    If Not oPresupuestoAAnyadir Is Nothing Then
        
        oPresupuestoAAnyadir.Anyo = iAnyoSeleccionado
        
        If sdbgPresupuestos.Columns(2).Value = "" Then
            oPresupuestoAAnyadir.Presupuesto = Null
        Else
            oPresupuestoAAnyadir.Presupuesto = sdbgPresupuestos.Columns(2).Value
        End If
        
        If sdbgPresupuestos.Columns(3).Value = "" Then
            oPresupuestoAAnyadir.Objetivo = Null
        Else
           oPresupuestoAAnyadir.Objetivo = sdbgPresupuestos.Columns(3).Value
        End If
        
                
        Set oIBaseDatos = oPresupuestoAAnyadir
       
        
        teserror = oIBaseDatos.AnyadirABaseDatos
        
        If teserror.NumError <> TESnoerror Then
            
            v = sdbgPresupuestos.ActiveCell.Value
            TratarError teserror
            sdbgPresupuestos.SetFocus
            bModError = True
            RowBuf.RowCount = 0
            sdbgPresupuestos.ActiveCell.Value = v
            oPresupuestoAAnyadir.Anyo = Null
            Set oIBaseDatos = Nothing
        
        End If
            
        'Registro de acciones
            
        basSeguridad.RegistrarAccion accionessummit.ACCPresMatAnya, "Anyo:" & CStr(iAnyoSeleccionado) & "CodGMN1:" & sdbcGMN1_4Cod.Text & "CodGMN2:" & sdbcGMN2_4Cod.Text & "CodGMN3:" & sdbcGMN3_4Cod.Text & "CodGMN4:" & sdbgPresupuestos.Columns(0).Text
        
        Set oIBaseDatos = Nothing
        Set oPresupuestoAAnyadir = Nothing
        Accion = ACCPresMatCon
        Exit Sub
    
    End If
    
 End If
 
 If Accion = ACCPresMatMod Then
 
    ''' Modificamos en la base de datos
    
    If Not oPresupuestoEnEdicion Is Nothing Then
    
        If sdbgPresupuestos.Columns(2).Value <> "" Then
            oPresupuestoEnEdicion.Presupuesto = sdbgPresupuestos.Columns(2).Value
        Else
            oPresupuestoEnEdicion.Presupuesto = Null
        End If
        If sdbgPresupuestos.Columns(3).Value <> "" Then
            oPresupuestoEnEdicion.Objetivo = sdbgPresupuestos.Columns(3).Value
        Else
            oPresupuestoEnEdicion.Objetivo = Null
        End If
    
        teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
       
        If teserror.NumError <> TESnoerror Then
        
            v = sdbgPresupuestos.ActiveCell.Value
            TratarError teserror
            sdbgPresupuestos.SetFocus
            bModError = True
            RowBuf.RowCount = 0
            sdbgPresupuestos.ActiveCell.Value = v
            
        Else
            
            ''' Registro de acciones
            Select Case gParametrosGenerales.giNEM
                
                Case 1
                
                Case 2
                
                Case 3
                
                Case 4
                
            End Select
            
            'Registro de acciones
            basSeguridad.RegistrarAccion accionessummit.ACCPresMatMod, "Anyo:" & CStr(iAnyoSeleccionado) & "CodGMN1:" & sdbcGMN1_4Cod.Text & "CodGMN2:" & sdbcGMN2_4Cod.Text & "CodGMN3:" & sdbcGMN3_4Cod.Text & "CodGMN4:" & sdbgPresupuestos.Columns(0).Text
            
            Accion = ACCPresMatCon
                        
            Set oIBAseDatosEnEdicion = Nothing
            Set oPresupuestoEnEdicion = Nothing
            
        End If
    
    End If
End If

End Sub

Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 6475 Then   'de tama�o de la ventana
            Me.Width = 6675        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 3470 Then  'cuando no se maximiza ni
            Me.Height = 3570       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width

    
    If Height >= 2630 Then Picture1.Height = Height - 2630
    If Width >= 160 Then Picture1.Width = Width - 160
    If Picture1.Height >= 100 Then sdbgPresupuestos.Height = Picture1.Height - 100
    If Picture1.Width >= 100 Then sdbgPresupuestos.Width = Picture1.Width - 100
    
    sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 15 / 100
    sdbgPresupuestos.Columns(1).Width = sdbgPresupuestos.Width * 40 / 100
    sdbgPresupuestos.Columns(2).Width = sdbgPresupuestos.Width * 25 / 100
    sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 20 / 100 - 450
     
    cmdModoEdicion.Left = Picture1.Left + Picture1.Width - cmdModoEdicion.Width - 90
    
End Sub
Private Sub cmdFiltrar_Click()

    ''' * Objetivo: Activar el formulario de filtrar.
    
    frmPRESPorMatFiltrar.WindowState = vbNormal
    frmPRESPorMatFiltrar.Show 1

End Sub
Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la PresPorProyecto actual
    
    sdbgPresupuestos.CancelUpdate
    sdbgPresupuestos.DataChanged = False
    
    If Not oPresupuestoEnEdicion Is Nothing Then
        oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set oPresupuestoEnEdicion = Nothing
    End If
    
    cmdDeshacer.Enabled = False
    
    Accion = ACCPresMatCon
        
End Sub
Private Sub cmdListado_Click()

    ''' * Objetivo: Obtener un listado de presupuestos por material
    
    If Not IsEmpty(iAnyoSeleccionado) Then
        frmLstPRESPorMat.iAnyoSeleccionado = iAnyoSeleccionado
        frmLstPRESPorMat.sdbcAnyo = iAnyoSeleccionado
        'frmLstPRESPorMat.sdbcAnyo_Validate False

    End If
    If Not oGMN1Seleccionado Is Nothing Then
        Set frmLstPRESPorMat.oGMN1Seleccionado = oGMN1Seleccionado
        frmLstPRESPorMat.sdbcGMN1_4Cod = oGMN1Seleccionado.Cod
        frmLstPRESPorMat.sdbcGMN1_4Cod_Validate False
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        Set frmLstPRESPorMat.oGMN2Seleccionado = oGMN2Seleccionado
        frmLstPRESPorMat.sdbcGMN2_4Cod = oGMN2Seleccionado.Cod
        frmLstPRESPorMat.sdbcGMN2_4Cod_Validate False
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        Set frmLstPRESPorMat.oGMN3Seleccionado = oGMN3Seleccionado
        frmLstPRESPorMat.sdbcGMN3_4Cod = oGMN3Seleccionado.Cod
        frmLstPRESPorMat.sdbcGMN3_4Cod_Validate False
    End If
    If sOrdenListado = "COD" Then frmLstPRESPorMat.opOrdCod = True
    If sOrdenListado = "DEN" Then frmLstPRESPorMat.opOrdDen = True
    If sOrdenListado = "PRES" Then frmLstPRESPorMat.opOrdPres = True
    If sOrdenListado = "OBJ" Then frmLstPRESPorMat.opOrdObj = True
    frmLstPRESPorMat.Show vbModal

End Sub
Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
                
        frmSel.Enabled = False
        
        sdbgPresupuestos.AllowUpdate = True
        
'        frmPRESPorMat.Caption = "Presupuestos por material (Edicion)"
        frmPRESPorMat.Caption = sIdiTitulos(4)
        
'        cmdModoEdicion.Caption = "&Consulta"
        cmdModoEdicion.Caption = sIdiConsulta
        
        cmdFiltrar.Visible = False
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
        cmdDeshacer.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCPresMatCon
    
    Else
            
        
        
        If sdbgPresupuestos.DataChanged = True Then
        
            v = sdbgPresupuestos.ActiveCell.Value
            sdbgPresupuestos.SetFocus
            sdbgPresupuestos.ActiveCell.Value = v
            
            bValError = False
            bModError = False
            
            sdbgPresupuestos.Update
            
            If bValError Or bModError Then
                Exit Sub
            End If
            
        End If
        
        frmSel.Enabled = True
        sdbgPresupuestos.AllowUpdate = False
        
        cmdDeshacer.Visible = False
        cmdFiltrar.Visible = True
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
        
        
'        frmPRESPorMat.Caption = "Presupuestos por material (Consulta)"
        frmPRESPorMat.Caption = sIdiTitulos(3)
        
'        cmdModoEdicion.Caption = "&Edicion"
        cmdModoEdicion.Caption = sIdiEdicion
        
        cmdRestaurar_Click
        
        bModoEdicion = False
        
    End If
    
    sdbgPresupuestos.SetFocus
    
End Sub
Private Sub cmdRestaurar_Click()

Dim oIMAsig As IMaterialAsignado

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
'    Caption = "Presupuestos por material (Consulta)"
    Caption = sIdiTitulos(3)
    
    Set oPresupuestos = Nothing
    Set oPresupuestos = oFSGSRaiz.Generar_CPresPorMateriales
  
    
    CargarPresupuestos
    
    
End Sub


Private Sub sdbcGMN1_4Cod_Change()
    
    If Not GMN1RespetarCombo Then
    
        GMN1RespetarCombo = True
        
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        
        GMN1RespetarCombo = False
        
        GMN1CargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oPresupuestos = Nothing
        sdbgPresupuestos.Update
        sdbgPresupuestos.ReBind
            
        cmdFiltrar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdFiltrar.Enabled = False
        cmdModoEdicion.Enabled = False
        
    End If
    
End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    GMN1CargarComboDesde = False
        
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN1_4Cod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oGruposMN1 = Nothing
    Set oGruposMN1 = oFSGSRaiz.generar_CGruposMatNivel1
  
    If GMN1CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
         
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.generar_CGruposMatNivel1
          
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
          
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.generar_CGruposMatNivel1
         
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(9) & Codigos.Den(i)
    Next
    
    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()

    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Cod.Rows - 1
            bm = sdbcGMN1_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    'If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
    '    GMN1RespetarCombo = True
    '    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    '    GMN1RespetarCombo = False
    '    Exit Sub
    'End If
    
    ''' Solo continuamos si existe el grupo
        
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.Comprador
      
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , , False)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
        Else
            GMN1RespetarCombo = True
            sdbcGMN1_4Den.Text = oGruposMN1.Item(scod1).Den
            GMN1RespetarCombo = False
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
        
            GMN1CargarComboDesde = False
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
      
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
            
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            
        Else
            GMN1RespetarCombo = True
            sdbcGMN1_4Den.Text = oGMN1.Den
            
            sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
            sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
            GMN1RespetarCombo = False
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
           
            GMN1CargarComboDesde = False
        End If
    
    End If
    
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub sdbcGMN1_4Den_Change()
    
    If Not GMN1RespetarCombo Then
    
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        
        GMN1RespetarCombo = False
        
        GMN1CargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oPresupuestos = Nothing
        sdbgPresupuestos.Update
        sdbgPresupuestos.ReBind
            
        cmdFiltrar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdFiltrar.Enabled = False
        cmdModoEdicion.Enabled = False
        
    End If
    
End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    GMN1CargarComboDesde = False
        
End Sub

Private Sub sdbcGMN1_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN1_4Den.RemoveAll
    
    Set oGruposMN1 = Nothing
    
     Screen.MousePointer = vbHourglass
    
     If GMN1CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
            
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.generar_CGruposMatNivel1
       
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , True, False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.generar_CGruposMatNivel1
        
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(9) & Codigos.Cod(i)
    Next
    
    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()

    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4Den.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4Den.Rows - 1
            bm = sdbcGMN1_4Den.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4Den.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4Den.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)
Dim bExiste As Boolean
Dim oIBaseDatos As IBaseDatos
Dim oGMN1 As CGrupoMatNivel1
Dim oIMAsig As IMaterialAsignado
    
    If sdbcGMN1_4Den.Text = "" Then Exit Sub
    
    'If sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text Then
    '    GMN1RespetarCombo = True
    '    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    '    GMN1RespetarCombo = False
    '    Exit Sub
    'End If
    
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        'Hay restriccion de material
        Set oIMAsig = oUsuarioSummit.Comprador
     
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, , Trim(sdbcGMN1_4Den), True, False)
        
        If oGruposMN1 Is Nothing Then
            'No existe
            sdbcGMN1_4Den.Text = ""
'            basMensajes.NoValido "Material"
            basMensajes.NoValido sIdiMaterial
        Else
            If UCase(oGruposMN1.Item(1).Den) <> UCase(sdbcGMN1_4Den.Text) Then
                'No existe
                sdbcGMN1_4Den.Text = ""
                
            Else
                GMN1RespetarCombo = True
                sdbcGMN1_4Cod.Text = oGruposMN1.Item(1).Cod
                GMN1RespetarCombo = False
                Set oGMN1Seleccionado = Nothing
                Set oGMN1Seleccionado = oGruposMN1.Item(1)
           
                GMN1CargarComboDesde = False
            End If
        
        End If
    
    Else
    
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.generar_CGruposMatNivel1
       
        oGruposMN1.CargarTodosLosGruposMatDesde 1, , sdbcGMN1_4Den.Text, True, False
        
            If oGruposMN1.Count = 0 Then
                'No existe
                sdbcGMN1_4Den.Text = ""
'                basMensajes.NoValido "Material"
                basMensajes.NoValido sIdiMaterial
            Else
            
                'Si el primero que encuentra es distinto, no existe porque estan ordenados por den.
                If UCase(oGruposMN1.Item(1).Den) <> UCase(sdbcGMN1_4Den.Text) Then
                    'No existe
                    sdbcGMN1_4Den.Text = ""
                
                Else
                    GMN1RespetarCombo = True
                    sdbcGMN1_4Cod.Text = oGruposMN1.Item(1).Cod
                    
                    sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
                    sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
        
                    GMN1RespetarCombo = False
                    Set oGMN1Seleccionado = Nothing
                    Set oGMN1Seleccionado = oGruposMN1.Item(1)
               
                    GMN1CargarComboDesde = False
                End If
            End If
        
    End If
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        
        sdbcGMN2_4Den.Text = ""
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        
        GMN2RespetarCombo = False
        
        GMN2CargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        Set oPresupuestos = Nothing
        sdbgPresupuestos.Update
        sdbgPresupuestos.ReBind
            
        cmdFiltrar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdFiltrar.Enabled = False
        cmdModoEdicion.Enabled = False
        
    End If
    
End Sub

Private Sub sdbcGMN2_4Cod_CloseUp()
    
    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    GMN2CargarComboDesde = False
        
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN2_4Cod.RemoveAll
    
    Set oGruposMN2 = Nothing
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If GMN2CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
         
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
           
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , False, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(9) & Codigos.Den(i)
    Next
    
    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_InitColumnProps()

    sdbcGMN2_4Cod.DataFieldList = "Column 0"
    sdbcGMN2_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN2_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN2_4Cod.Rows - 1
            bm = sdbcGMN2_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN2_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN2_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN2 As CGrupoMatNivel2
    Dim oIMAsig As IMaterialAsignado
    
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    'If sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text Then
    '    GMN2RespetarCombo = True
    '    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    '    GMN2RespetarCombo = False
    '    Exit Sub
    'End If
    
    ''' Solo continuamos si existe el grupo
    
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.Comprador
     
        Set oGruposMN2 = Nothing
        Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        
        If oGruposMN2 Is Nothing Then
            'No existe
            sdbcGMN2_4Cod.Text = ""
'            basMensajes.NoValido "Material"
            basMensajes.NoValido sIdiMaterial
        Else
            If oGruposMN2.Item(1) Is Nothing Then
                'No existe
                sdbcGMN2_4Cod.Text = ""
                
            Else
                GMN2RespetarCombo = True
                sdbcGMN2_4Den.Text = oGruposMN2.Item(1).Den
                GMN2RespetarCombo = False
                Set oGMN2Seleccionado = oGruposMN2.Item(1)
                GMN2CargarComboDesde = False
            End If
        End If
    Else
        Set oGMN2 = Nothing
        Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2
       
        oGMN2.GMN1Cod = sdbcGMN1_4Cod
        oGMN2.Cod = sdbcGMN2_4Cod
        Set oIBaseDatos = oGMN2
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN2_4Cod.Text = ""
            
        Else
            GMN2RespetarCombo = True
            sdbcGMN2_4Den.Text = oGMN2.Den
            
            sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
            sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
            GMN2RespetarCombo = False
            Set oGMN2Seleccionado = oGMN2
            GMN2CargarComboDesde = False
        End If
    
    End If
    
    
    Set oGMN2 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN2 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal
  
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN3_4Den.Text = ""
        
        GMN2RespetarCombo = False
        
        GMN2CargarComboDesde = True
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        Set oPresupuestos = Nothing
        sdbgPresupuestos.Update
        sdbgPresupuestos.ReBind
            
        cmdFiltrar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdFiltrar.Enabled = False
        cmdModoEdicion.Enabled = False
        
    End If
    
End Sub

Private Sub sdbcGMN2_4Den_CloseUp()
    
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    GMN2CargarComboDesde = False
        
End Sub

Private Sub sdbcGMN2_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If GMN2CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
         
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , Trim(sdbcGMN2_4Den), True, False)
        Else
          
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN2_4Den), True, False
            Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
           
            Set oGruposMN2 = Nothing
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , True, False)
        Else
           
             oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        End If
    End If
    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(9) & Codigos.Cod(i)
    Next
    
    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()

    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN2_4Den.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN2_4Den.Rows - 1
            bm = sdbcGMN2_4Den.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN2_4Den.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN2_4Den.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)
Dim bExiste As Boolean
Dim oIBaseDatos As IBaseDatos
Dim oGMN2 As CGrupoMatNivel2
Dim oIMAsig As IMaterialAsignado
    
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    'If sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text Then
    '    GMN2RespetarCombo = True
    '    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    '    GMN2RespetarCombo = False
    '    Exit Sub
    'End If
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        'Hay restriccion de material
        Set oIMAsig = oUsuarioSummit.Comprador
       
        Set oGruposMN2 = Nothing
        Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , Trim(sdbcGMN2_4Den), True, False)
        
        If oGruposMN2 Is Nothing Then
            'No existe
            sdbcGMN2_4Den.Text = ""
            
        Else
            'Si el primero que encuentra es distinto, no existe porque estan ordenados por den.
            If UCase(oGruposMN2.Item(1).Den) <> UCase(sdbcGMN2_4Den.Text) Then
                'No existe
                sdbcGMN2_4Den.Text = ""
                
            Else
                GMN2RespetarCombo = True
                sdbcGMN2_4Cod.Text = oGruposMN2.Item(1).Cod
                GMN2RespetarCombo = False
                Set oGMN2Seleccionado = oGruposMN2.Item(1)
                GMN2CargarComboDesde = False
            End If
        
        End If
    
    Else
        
        oGMN1Seleccionado.CargarTodosLosGruposMatDesde 1, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN2_4Den), True, False
        
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
        If oGruposMN2 Is Nothing Then
            'No existe
            sdbcGMN2_4Den.Text = ""
            
        Else
            If oGruposMN2.Count = 0 Then
            'No existe
                sdbcGMN2_4Den.Text = ""
            
            
            Else
                'Si el primero que encuentra es distinto, no existe porque estan ordenados por den.
                If UCase(oGruposMN2.Item(1).Den) <> UCase(sdbcGMN2_4Den.Text) Then
                    'No existe
                    sdbcGMN2_4Den.Text = ""
                    
                Else
                    GMN2RespetarCombo = True
                    sdbcGMN2_4Cod.Text = oGruposMN2.Item(1).Cod
                    
                    sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
                    sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
        
                    GMN2RespetarCombo = False
                    Set oGMN2Seleccionado = oGruposMN2.Item(1)
               
                    GMN2CargarComboDesde = False
                End If
            End If
        End If
        
    End If
    
    Set oGMN2 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN2 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcGMN3_4Cod_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        GMN3RespetarCombo = False
        
        GMN3CargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oPresupuestos = Nothing
        
        sdbgPresupuestos.Update
        sdbgPresupuestos.ReBind
            
        cmdFiltrar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdFiltrar.Enabled = False
        cmdModoEdicion.Enabled = False
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_CloseUp()
    
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    GMN3CargarComboDesde = False
        
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    
    sdbcGMN3_4Cod.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oGruposMN3 = Nothing
    
    If GMN3CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
    
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , False, False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
          
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(9) & Codigos.Den(i)
    Next
    
    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Cod_InitColumnProps()

    sdbcGMN3_4Cod.DataFieldList = "Column 0"
    sdbcGMN3_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN3_4Cod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN3_4Cod.Rows - 1
            bm = sdbcGMN3_4Cod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN3_4Cod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN3_4Cod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN3 As CGrupoMatNivel3
    Dim oIMAsig As IMaterialAsignado
    
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    'If sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text Then
    '    GMN3RespetarCombo = True
    '    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    '    GMN3RespetarCombo = False
    '    Exit Sub
    'End If
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.Comprador
       
        Set oGruposMN3 = Nothing
        Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        
        If oGruposMN3 Is Nothing Then
            'No existe
            sdbcGMN3_4Cod.Text = ""
            
        Else
            If oGruposMN3.Item(1) Is Nothing Then
                'No existe
                sdbcGMN3_4Cod.Text = ""
            
            Else
                GMN3RespetarCombo = True
                sdbcGMN3_4Den.Text = oGruposMN3.Item(1).Den
                GMN3RespetarCombo = False
                Set oGMN3Seleccionado = oGruposMN3.Item(1)
                GMN3Seleccionado
                GMN3CargarComboDesde = False
            End If
        End If
    Else
        Set oGMN3 = Nothing
        Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3
     
        oGMN3.GMN1Cod = sdbcGMN1_4Cod
        oGMN3.GMN2Cod = sdbcGMN2_4Cod
        oGMN3.Cod = sdbcGMN3_4Cod
        Set oIBaseDatos = oGMN3
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN3_4Cod.Text = ""
            
        Else
            GMN3RespetarCombo = True
            sdbcGMN3_4Den.Text = oGMN3.Den
            
            sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
            sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
            GMN3RespetarCombo = False
            Set oGMN3Seleccionado = oGMN3
            GMN3Seleccionado
            GMN3CargarComboDesde = False
        End If
    
    End If
    
    'GMN3Seleccionado
    
    Set oGMN3 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN3 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal
 
End Sub
Private Sub sdbcGMN3_4den_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        GMN3RespetarCombo = False
        
        GMN3CargarComboDesde = True
        Set oGMN3Seleccionado = Nothing
        Set oPresupuestos = Nothing
        sdbgPresupuestos.Update
        sdbgPresupuestos.ReBind
            
        cmdFiltrar.Enabled = False
        cmdRestaurar.Enabled = False
        cmdFiltrar.Enabled = False
        cmdModoEdicion.Enabled = False
        
    End If
    
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
    
    If sdbcGMN3_4Den.Value = "....." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    GMN3CargarComboDesde = False
        
End Sub

Private Sub sdbcGMN3_4Den_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
         
    sdbcGMN3_4Den.RemoveAll
    
    Set oGruposMN3 = Nothing
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If GMN3CargarComboDesde Then
    
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
         
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , Trim(sdbcGMN3_4Den), True, False)
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN3_4Den), True, False
            Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    Else
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.Comprador
         
            Set oGruposMN3 = Nothing
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , True, False)
        Else
             
             oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
             Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        End If
    End If
    
    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(9) & Codigos.Cod(i)
    Next
    
    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()

    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN3_4Den.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN3_4Den.Rows - 1
            bm = sdbcGMN3_4Den.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN3_4Den.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN3_4Den.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)
Dim bExiste As Boolean
Dim oIBaseDatos As IBaseDatos
Dim oGMN3 As CGrupoMatNivel3
Dim oIMAsig As IMaterialAsignado
    
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    'If sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text Then
    '    GMN3RespetarCombo = True
    '    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    '    GMN3RespetarCombo = False
    '    Exit Sub
    'End If
    ''' Solo continuamos si existe el grupo
    
    Screen.MousePointer = vbHourglass
    If bRMat Then
        'Hay restriccion de material
        Set oIMAsig = oUsuarioSummit.Comprador
     
        Set oGruposMN3 = Nothing
        Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(1, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , Trim(sdbcGMN3_4Den), True, False)
        
        If oGruposMN3 Is Nothing Then
            'No existe
            sdbcGMN3_4Den.Text = ""
            
        Else
            'Si el primero que encuentra es distinto, no existe porque estan ordenados por den.
            If UCase(oGruposMN3.Item(1).Den) <> UCase(sdbcGMN3_4Den.Text) Then
                'No existe
                sdbcGMN3_4Den.Text = ""
            
            Else
                GMN3RespetarCombo = True
                sdbcGMN3_4Cod.Text = oGruposMN3.Item(1).Cod
                GMN3RespetarCombo = False
                Set oGMN3Seleccionado = oGruposMN3.Item(1)
                GMN3Seleccionado
                GMN3CargarComboDesde = False
                'CargarPresupuestos
            End If
        
        End If
    
    Else
        oGMN2Seleccionado.CargarTodosLosGruposMatDesde 1, Trim(sdbcGMN3_4Cod), , True, False
        
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
        If oGruposMN3 Is Nothing Then
            'No existe
            sdbcGMN3_4Den.Text = ""
        
        Else
            If oGruposMN3.Count = 0 Then
            'No existe
                sdbcGMN3_4Den.Text = ""
        
            
            Else
                'Si el primero que encuentra es distinto, no existe porque estan ordenados por den.
                If UCase(oGruposMN3.Item(1).Den) <> UCase(sdbcGMN3_4Den.Text) Then
                    'No existe
                    sdbcGMN3_4Den.Text = ""
        
                Else
                    GMN3RespetarCombo = True
                    sdbcGMN3_4Cod.Text = oGruposMN3.Item(1).Cod
                    
                    sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
                    sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
        
                    GMN3RespetarCombo = False
                    Set oGMN3Seleccionado = oGruposMN3.Item(1)
                    GMN3Seleccionado
                    GMN3CargarComboDesde = False
                    'CargarPresupuestos
                End If
            End If
        End If
        
    End If
    
    
    Set oGMN3 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN3 = Nothing
    Set oIMAsig = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub CargarPresupuestos()
    
    ' Cargamos los presupuestos
    
    Select Case gParametrosGenerales.giNEM
        
        Case 1
        
        Case 2
        
        Case 3
        
        Case 4
                    
                    Screen.MousePointer = vbHourglass
                    If bRMat Then
                             
                        oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
                    Else
                         
                        oPresupuestos.CargarTodosLosPresupuestos iAnyoSeleccionado, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod, , , , , , , , , , , , , , , True
                                     
                    End If
                    Screen.MousePointer = vbNormal
    
                
                    sdbgPresupuestos.ReBind
            
    End Select
    
    
End Sub

Public Sub ponerCaption(texto, indice, comodin)
    
Caption = sIdiTitulos(indice) & texto & IIf(comodin, "*", "")
  

End Sub

    
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESPORMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    sIdiTitulos(1) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(2) = Ador(0).Value & " "
    Ador.MoveNext
    sIdiTitulos(3) = Ador(0).Value
    Caption = sIdiTitulos(3)
    Ador.MoveNext
    sIdiTitulos(4) = Ador(0).Value
    Ador.MoveNext
    lblAnyo.Caption = Ador(0).Value
    Ador.MoveNext
    
    sdbcGMN1_4Cod.Columns(0).Caption = Ador(0).Value
    sdbcGMN2_4Cod.Columns(0).Caption = Ador(0).Value
    sdbcGMN3_4Cod.Columns(0).Caption = Ador(0).Value
    sdbcGMN1_4Den.Columns(1).Caption = Ador(0).Value
    sdbcGMN2_4Den.Columns(1).Caption = Ador(0).Value
    sdbcGMN3_4Den.Columns(1).Caption = Ador(0).Value
    Ador.MoveNext
    
    sdbcGMN1_4Cod.Columns(1).Caption = Ador(0).Value
    sdbcGMN2_4Cod.Columns(1).Caption = Ador(0).Value
    sdbcGMN3_4Cod.Columns(1).Caption = Ador(0).Value
    sdbcGMN1_4Den.Columns(0).Caption = Ador(0).Value
    sdbcGMN2_4Den.Columns(0).Caption = Ador(0).Value
    sdbcGMN3_4Den.Columns(0).Caption = Ador(0).Value
    sdbgPresupuestos.Columns(1).Caption = Ador(0).Value

    Ador.MoveNext
    
    sdbgPresupuestos.Columns(0).Caption = Ador(0).Value
   ' Ador.MoveNext
   ' sdbgPresupuestos.Columns(1).Caption = Ador(0).Value '9
    Ador.MoveNext
    sdbgPresupuestos.Columns(2).Caption = Ador(0).Value
    sIdiPresupuesto = Ador(0).Value
    Ador.MoveNext
    sdbgPresupuestos.Columns(3).Caption = Ador(0).Value
    sIdiObjetivo = Ador(0).Value
    Ador.MoveNext
    
    cmdDeshacer.Caption = Ador(0).Value
    Ador.MoveNext
    cmdFiltrar.Caption = Ador(0).Value
    Ador.MoveNext
    cmdListado.Caption = Ador(0).Value
    Ador.MoveNext
    cmdModoEdicion.Caption = Ador(0).Value '15
    Ador.MoveNext
    cmdRestaurar.Caption = Ador(0).Value
    'Ador.MoveNext
    'sIdiPresupuesto = Ador(0).Value
    'Ador.MoveNext
    'sIdiObjetivo = Ador(0).Value
    Ador.MoveNext
    sIdiOrdenando = Ador(0).Value
    Ador.MoveNext
    sIdiConsulta = Ador(0).Value
    Ador.MoveNext
    sIdiEdicion = Ador(0).Value
    Ador.MoveNext
    sIdiMaterial = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub






