VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPonderacion 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DPonderacion"
   ClientHeight    =   3915
   ClientLeft      =   2820
   ClientTop       =   1890
   ClientWidth     =   7275
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPonderacion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3915
   ScaleWidth      =   7275
   Begin VB.CommandButton cmdModif1 
      Caption         =   "D&Modificar"
      Height          =   315
      Left            =   90
      TabIndex        =   41
      Top             =   3540
      Width           =   1005
   End
   Begin VB.CheckBox chkDef1 
      BackColor       =   &H00808000&
      Caption         =   "DGuardar en la definici�n del atributo "
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   42
      Top             =   120
      Visible         =   0   'False
      Width           =   3585
   End
   Begin VB.CommandButton cmdCancel1 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   3750
      TabIndex        =   40
      Top             =   3540
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton cmdAcep1 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2635
      TabIndex        =   39
      Top             =   3540
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.PictureBox picNumerico 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   3015
      Left            =   60
      ScaleHeight     =   3015
      ScaleWidth      =   7275
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   7275
      Begin VB.OptionButton optNoPond 
         BackColor       =   &H00808000&
         Caption         =   "DSin ponderaci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   43
         Top             =   580
         Value           =   -1  'True
         Width           =   1665
      End
      Begin VB.Frame fraPond0 
         BackColor       =   &H00808000&
         Height          =   1035
         Left            =   2025
         TabIndex        =   33
         Top             =   810
         Visible         =   0   'False
         Width           =   4935
         Begin VB.Label lblPond1 
            BackColor       =   &H00808000&
            Caption         =   "DElija el sistema de ponderaci�n que desee"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   600
            TabIndex        =   35
            Top             =   660
            Width           =   3795
         End
         Begin VB.Label lblPond0 
            BackColor       =   &H00808000&
            Caption         =   "DAtributo sin ponderaci�n"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   600
            TabIndex        =   34
            Top             =   300
            Width           =   3795
         End
      End
      Begin VB.OptionButton optManual 
         BackColor       =   &H00808000&
         Caption         =   "DManual"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   6
         Top             =   2020
         Width           =   1155
      End
      Begin VB.OptionButton optFormula 
         BackColor       =   &H00808000&
         Caption         =   "DF�rmula"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   5
         Top             =   1660
         Width           =   1155
      End
      Begin VB.OptionButton optDiscreta 
         BackColor       =   &H00808000&
         Caption         =   "DEscala discreta"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   4
         Top             =   1300
         Width           =   1695
      End
      Begin VB.OptionButton optContinua 
         BackColor       =   &H00808000&
         Caption         =   "DEscala continua"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   3
         Top             =   940
         Width           =   1590
      End
      Begin VB.Frame fraManual 
         BackColor       =   &H00808000&
         Height          =   975
         Left            =   2040
         TabIndex        =   10
         Top             =   600
         Visible         =   0   'False
         Width           =   4935
         Begin VB.Label lblManualBoo 
            BackColor       =   &H00808000&
            Caption         =   "DLos valores de ponderaci�n deber�n introducirse manualmente por el usuario"
            ForeColor       =   &H00FFFFFF&
            Height          =   435
            Left            =   300
            TabIndex        =   11
            Top             =   360
            Width           =   4275
         End
      End
      Begin VB.Frame fraContinua 
         BackColor       =   &H00808000&
         Height          =   2715
         Left            =   1980
         TabIndex        =   1
         Top             =   90
         Width           =   5010
         Begin VB.CommandButton cmdDeshacerCon 
            Caption         =   "D&Deshacer"
            Height          =   255
            Left            =   2175
            TabIndex        =   29
            Top             =   2400
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.CommandButton cmdEliCon 
            Caption         =   "D&Eliminar"
            Height          =   255
            Left            =   1200
            TabIndex        =   28
            Top             =   2400
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.CommandButton cmdAnyaCon 
            Caption         =   "DA&�adir"
            Height          =   255
            Left            =   225
            TabIndex        =   27
            Top             =   2400
            Visible         =   0   'False
            Width           =   915
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgContinua 
            Height          =   2055
            Left            =   225
            TabIndex        =   2
            Top             =   240
            Width           =   4635
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            Row.Count       =   3
            Col.Count       =   4
            Row(0).Col(0)   =   "0"
            Row(0).Col(1)   =   "10"
            Row(0).Col(2)   =   "1"
            Row(1).Col(0)   =   "11"
            Row(1).Col(1)   =   "20"
            Row(1).Col(2)   =   "2"
            Row(2).Col(0)   =   "20"
            Row(2).Col(1)   =   "999999"
            Row(2).Col(2)   =   "10"
            AllowAddNew     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   2752
            Columns(0).Caption=   "Desde"
            Columns(0).Name =   "DESDE"
            Columns(0).AllowSizing=   0   'False
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2143
            Columns(1).Caption=   "Hasta"
            Columns(1).Name =   "HASTA"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   2196
            Columns(2).Caption=   "=> Puntos"
            Columns(2).Name =   "PUNTOS"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "INDI"
            Columns(3).Name =   "INDI"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            _ExtentX        =   8176
            _ExtentY        =   3625
            _StockProps     =   79
            ForeColor       =   0
            BackColor       =   -2147483633
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraDiscreta 
         BackColor       =   &H00808000&
         Height          =   2685
         Left            =   1980
         TabIndex        =   12
         Top             =   120
         Width           =   4995
         Begin VB.CommandButton cmdEliDis 
            Caption         =   "D&Eliminar"
            Height          =   255
            Left            =   1200
            TabIndex        =   32
            Top             =   2280
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.CommandButton cmdAnyaDis 
            Caption         =   "DA&�adir"
            Height          =   255
            Left            =   225
            TabIndex        =   31
            Top             =   2280
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.CommandButton cmdDeshacerDis 
            Caption         =   "D&Deshacer"
            Height          =   255
            Left            =   2175
            TabIndex        =   30
            Top             =   2280
            Visible         =   0   'False
            Width           =   915
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgDiscreta 
            Height          =   1875
            Left            =   225
            TabIndex        =   13
            Top             =   300
            Width           =   4605
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            Row.Count       =   3
            Col.Count       =   3
            Row(0).Col(0)   =   "1/1/2002"
            Row(0).Col(1)   =   "1"
            Row(1).Col(0)   =   "1/1/2003"
            Row(1).Col(1)   =   "2"
            Row(2).Col(0)   =   "1/1/2004"
            Row(2).Col(1)   =   "3"
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3942
            Columns(0).Caption=   "Valor"
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   3200
            Columns(1).Caption=   " => Puntos"
            Columns(1).Name =   "PUNTOS"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "INDI"
            Columns(2).Name =   "INDI"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   8123
            _ExtentY        =   3307
            _StockProps     =   79
            ForeColor       =   0
            BackColor       =   -2147483633
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFormula 
         BackColor       =   &H00808000&
         Height          =   1035
         Left            =   2025
         TabIndex        =   7
         Top             =   1080
         Visible         =   0   'False
         Width           =   4935
         Begin VB.TextBox txtNumero 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   1725
            TabIndex        =   8
            Top             =   420
            Width           =   1215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcSigno 
            Height          =   315
            Left            =   825
            TabIndex        =   26
            Top             =   420
            Width           =   795
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   8361
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1402
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblValor 
            BackColor       =   &H00808000&
            Caption         =   "DValor"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   300
            TabIndex        =   9
            Top             =   480
            Width           =   495
         End
      End
   End
   Begin VB.PictureBox picBoolean 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   2205
      Left            =   105
      ScaleHeight     =   2205
      ScaleWidth      =   7155
      TabIndex        =   14
      Top             =   510
      Visible         =   0   'False
      Width           =   7155
      Begin VB.OptionButton optNoPond2 
         BackColor       =   &H00808000&
         Caption         =   "Sin ponderaci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   180
         TabIndex        =   44
         Top             =   480
         Value           =   -1  'True
         Width           =   1515
      End
      Begin VB.OptionButton optManual2 
         BackColor       =   &H00808000&
         Caption         =   "DManual"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   180
         TabIndex        =   25
         Top             =   1200
         Width           =   1515
      End
      Begin VB.OptionButton optAutomatico 
         BackColor       =   &H00808000&
         Caption         =   "Autom�tico"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   180
         TabIndex        =   24
         Top             =   840
         Width           =   1515
      End
      Begin VB.Frame fraManualBoolean 
         BackColor       =   &H00808000&
         Height          =   975
         Left            =   1740
         TabIndex        =   22
         Top             =   660
         Visible         =   0   'False
         Width           =   4935
         Begin VB.Label lblTextoManual 
            BackColor       =   &H00808000&
            Caption         =   "DLos valores de ponderaci�n deber�n introducirse manualmente por el usuario"
            ForeColor       =   &H00FFFFFF&
            Height          =   435
            Left            =   300
            TabIndex        =   23
            Top             =   360
            Width           =   4275
         End
      End
      Begin VB.Frame fraAutomatico 
         BackColor       =   &H00808000&
         Height          =   1875
         Left            =   1740
         TabIndex        =   15
         Top             =   120
         Visible         =   0   'False
         Width           =   4935
         Begin VB.TextBox txtPuntuacion2 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   1860
            TabIndex        =   17
            Text            =   "0"
            Top             =   1020
            Width           =   855
         End
         Begin VB.TextBox txtPuntuacion1 
            Alignment       =   1  'Right Justify
            Height          =   315
            Left            =   1860
            TabIndex        =   16
            Text            =   "3"
            Top             =   600
            Width           =   855
         End
         Begin VB.Label lblPuntos2 
            BackColor       =   &H00808000&
            Caption         =   "DPuntos"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   2820
            TabIndex        =   21
            Top             =   1080
            Width           =   615
         End
         Begin VB.Label lblNo 
            BackColor       =   &H00808000&
            Caption         =   "DValor = No"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   600
            TabIndex        =   20
            Top             =   1080
            Width           =   945
         End
         Begin VB.Label lblPuntos1 
            BackColor       =   &H00808000&
            Caption         =   "DPuntos"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   2820
            TabIndex        =   19
            Top             =   660
            Width           =   615
         End
         Begin VB.Label lblSi 
            BackColor       =   &H00808000&
            Caption         =   "DValor = S�"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   600
            TabIndex        =   18
            Top             =   660
            Width           =   960
         End
      End
      Begin VB.Frame fraPond0b 
         BackColor       =   &H00808000&
         Height          =   1035
         Left            =   1725
         TabIndex        =   36
         Top             =   330
         Visible         =   0   'False
         Width           =   4935
         Begin VB.Label lblPond0b 
            BackColor       =   &H00808000&
            Caption         =   "DAtributo sin ponderaci�n"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   600
            TabIndex        =   38
            Top             =   300
            Width           =   3795
         End
         Begin VB.Label lblPond1b 
            BackColor       =   &H00808000&
            Caption         =   "DElija el sistema de ponderaci�n que desee"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   600
            TabIndex        =   37
            Top             =   660
            Width           =   3795
         End
      End
   End
End
Attribute VB_Name = "frmPonderacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable de control de flujo de proceso
Public Accion As accionessummit

Public g_iTipo As Integer
Public g_bEdicion As Boolean
Public g_bError As Boolean
Public g_sOrigen As String
Public g_sQuien As String
Public g_bModDefAtrib As Boolean
Public g_iIntro As Integer
Public g_oAtributo As CAtributo

Private m_oPondIntermedia As CValoresPond
Private m_picActual As vb.PictureBox
Private m_bErrorC As Boolean
Private m_bErrorD As Boolean
Private m_bBorrar As Boolean
Private m_bSinUpdate As Boolean
Private m_sMensaje(0 To 10) As String
Private m_iIndiCole As Integer
Private m_iPond As Integer
Private m_bRespeCombo As Boolean
Private m_bRespetarComboSigno As Boolean
Private m_oListaEnEdicion As CValorPond
Private m_oIBAseDatosEnEdicion As IBaseDatos
Private m_iEnDefinicion As Integer

Public g_bAtribNoModif As Boolean

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub chkDef1_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkDef1.Value = vbChecked Then
        m_iEnDefinicion = 1
    Else
        m_iEnDefinicion = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "chkDef1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub cmdAcep1_Click()
    Dim iIndice As Integer
    Dim teserror As TipoErrorSummit
    Dim iIDAtributo As Long
    Dim iCountDisc As Integer
    Dim inum As Integer
    Dim oAtributoOrig As CAtributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If picNumerico.Visible Then
        If optContinua.Value Then
            m_iPond = 1
            If sdbgContinua.DataChanged Then
                sdbgContinua.Update
                If m_bErrorC Then
                    Exit Sub
                End If
            End If
                        
            sdbgContinua.MoveFirst
            
            If sdbgContinua.Columns("DESDE").Text = "" Then
                oMensajes.FaltaPonderacion
                Exit Sub
            Else
                Set g_oAtributo.ListaPonderacion = Nothing
                Set g_oAtributo.ListaPonderacion = oFSGSRaiz.Generar_CValoresPond
                
                iIDAtributo = g_oAtributo.Id
                For iIndice = 1 To sdbgContinua.Rows
                    If sdbgContinua.Columns("DESDE").Text <> "" Then
                        If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha Then
                            g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, , , , , iIndice, , StrToNull(sdbgContinua.Columns("PUNTOS").Value), CDate(sdbgContinua.Columns("DESDE").Text), CDate(sdbgContinua.Columns("HASTA").Text), , iIndice
                        Else
                            If g_oAtributo.Tipo = TiposDeAtributos.TipoNumerico Then
                                g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, g_oAtributo.AnyoProce, g_oAtributo.GMN1, g_oAtributo.CodProce, g_oAtributo.codgrupo, iIndice, , StrToNull(sdbgContinua.Columns("PUNTOS").Value), StrToNull(sdbgContinua.Columns("DESDE").Text), StrToNull(sdbgContinua.Columns("HASTA").Text), , iIndice
                            End If
                        End If
                    End If
                    sdbgContinua.MoveNext
                Next iIndice
                teserror = g_oAtributo.GuardarPonderacion(m_iPond, 0, 0, Null, 0, m_iEnDefinicion)
        
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Unload frmESPERA
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    g_oAtributo.TipoPonderacion = m_iPond
                End If
            End If
        Else
            If optDiscreta.Value Then
                m_iPond = 2
                If sdbgDiscreta.DataChanged Then
                    sdbgDiscreta.Update
                    If m_bErrorD Then
                        Exit Sub
                    End If
                End If
                
                sdbgDiscreta.MoveFirst
                
                iCountDisc = 0
                For iIndice = 1 To sdbgDiscreta.Rows
                    If sdbgDiscreta.Columns("PUNTOS").Value <> "" Then
                        iCountDisc = iCountDisc + 1
                    End If
                    sdbgDiscreta.MoveNext
                Next iIndice
                If iCountDisc = 0 Then
                    oMensajes.FaltaPonderacion
                    Exit Sub
                End If
                Set g_oAtributo.ListaPonderacion = Nothing
                Set g_oAtributo.ListaPonderacion = oFSGSRaiz.Generar_CValoresPond
                
                sdbgDiscreta.MoveFirst
                iIDAtributo = g_oAtributo.Id
                For iIndice = 1 To sdbgDiscreta.Rows
                    If sdbgDiscreta.Columns("VALOR").Value <> "" Then
                        If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha Then
                            g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, , , , , iIndice, CDate(sdbgDiscreta.Columns("VALOR").Value), StrToNull(sdbgDiscreta.Columns("PUNTOS").Value), , , , iIndice
                        Else
                            If g_oAtributo.Tipo = TiposDeAtributos.TipoNumerico Then
                                g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, , , , , iIndice, StrToDbl0(sdbgDiscreta.Columns("VALOR").Value), StrToNull(sdbgDiscreta.Columns("PUNTOS").Value), , , , iIndice
                            Else
                                g_oAtributo.ListaPonderacion.Add g_oAtributo.Id, , , , , iIndice, StrToNull(sdbgDiscreta.Columns("VALOR").Value), StrToNull(sdbgDiscreta.Columns("PUNTOS").Value), , , , iIndice
                            End If
                        End If
                    End If
                    sdbgDiscreta.MoveNext
                Next iIndice
                teserror = g_oAtributo.GuardarPonderacion(m_iPond, 0, 0, Null, Null, m_iEnDefinicion)
            
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Unload frmESPERA
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    g_oAtributo.TipoPonderacion = m_iPond
                End If
            Else
                If optFormula.Value Then
                    If txtNumero.Text = "" Or sdbcSigno.Text = "" Then
                        oMensajes.FaltaPonderacion
                        Exit Sub
                    End If
                    'Si la anterior pond era discreta le quito a la lista los valores de ponderaci�n
                    For iIndice = 1 To sdbgDiscreta.Rows
                        sdbgDiscreta.Columns("PUNTOS").Value = ""
                        g_oAtributo.ListaPonderacion.Item(StrToDbl0(sdbgDiscreta.Columns("INDI").Value)).ValorPond = Null
                        sdbgDiscreta.MoveNext
                    Next
                
                    Set m_oPondIntermedia = Nothing
                    m_bSinUpdate = True
                   'Si la anterior pond era continua elimino los valores de la lista
                    sdbgContinua.MoveLast
                    inum = sdbgContinua.Rows
                    For iIndice = inum To 1 Step -1    'To sdbgContinua.Rows
                        If sdbgContinua.Columns("INDI").Value <> "" Then
                            g_oAtributo.ListaPonderacion.Remove (StrToDbl0(sdbgContinua.Columns("INDI").Value))
                        End If
                        sdbgContinua.MovePrevious
                    Next
                                     
                    If txtNumero.Text <> NullToStr(g_oAtributo.Numero) Or sdbcSigno.Text <> NullToStr(g_oAtributo.Formula) Then
                            m_iPond = 3
                            teserror = g_oAtributo.GuardarPonderacion(m_iPond, 0, 0, sdbcSigno.Text, NullToDbl0(txtNumero.Text), m_iEnDefinicion)
            
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Unload frmESPERA
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            Else
                                g_oAtributo.Numero = NullToDbl0(txtNumero.Text)
                                g_oAtributo.Formula = sdbcSigno.Text
                                g_oAtributo.TipoPonderacion = m_iPond
                            End If
                    End If
                Else
                    If optManual.Value Then
                        m_iPond = 4
                        teserror = g_oAtributo.GuardarPonderacion(m_iPond, 0, 0, Null, Null, m_iEnDefinicion)
        
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Unload frmESPERA
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            g_oAtributo.TipoPonderacion = m_iPond
                        End If
                    Else
                        'Sin ponderaci�n
                        If optNoPond.Value Then
                            m_iPond = 0
                            teserror = g_oAtributo.GuardarPonderacion(m_iPond, 0, 0, Null, Null, m_iEnDefinicion)
        
                            If teserror.NumError <> TESnoerror Then
                                basErrores.TratarError teserror
                                Unload frmESPERA
                                Screen.MousePointer = vbNormal
                                Exit Sub
                            Else
                                g_oAtributo.TipoPonderacion = m_iPond
                            End If
                        End If
                    End If
                End If
            End If
        End If
    Else
        If optAutomatico.Value Then
            If txtPuntuacion1.Text = "" Or txtPuntuacion2.Text = "" Then
                oMensajes.FaltaPonderacion
                Exit Sub
            Else
                If txtPuntuacion1.Text <> NullToStr(g_oAtributo.ValorPondSi) Or txtPuntuacion2.Text <> NullToStr(g_oAtributo.ValorPondNo) Then
                    m_iPond = 5
                    teserror = g_oAtributo.GuardarPonderacion(m_iPond, StrToDbl0(txtPuntuacion1.Text), StrToDbl0(txtPuntuacion2.Text), Null, Null, m_iEnDefinicion)
        
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Unload frmESPERA
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        g_oAtributo.ValorPondNo = StrToDbl0(txtPuntuacion2.Text)
                        g_oAtributo.ValorPondSi = StrToDbl0(txtPuntuacion1.Text)
                        g_oAtributo.TipoPonderacion = m_iPond
                    End If
                End If
            End If
        Else
            If optManual2.Value Then
                m_iPond = 4
                txtPuntuacion1.Text = ""
                txtPuntuacion2.Text = ""
                teserror = g_oAtributo.GuardarPonderacion(m_iPond, 0, 0, Null, Null, m_iEnDefinicion)
            
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Unload frmESPERA
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    g_oAtributo.TipoPonderacion = m_iPond
                End If
            Else
                If optNoPond2.Value Then  'Sin ponderaci�n
                    m_iPond = 0
                    txtPuntuacion1.Text = ""
                    txtPuntuacion2.Text = ""
                    teserror = g_oAtributo.GuardarPonderacion(m_iPond, 0, 0, Null, Null, m_iEnDefinicion)
                
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Unload frmESPERA
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        g_oAtributo.TipoPonderacion = m_iPond
                    End If
                End If
            End If
        End If
    End If
    Select Case g_sOrigen
        Case "MDIfrmATRIB"
            frmAtrib.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmAtrib.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmAtrib.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmAtrib.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmAtrib.g_oAtributoEnEdicion = g_oAtributo
            
        Case "PLANTILLAS"
            frmPlantillasProce.sdbgAtribPlant.Columns("TIPOPOND").Value = m_iPond
            If m_iPond = 0 Then
                frmPlantillasProce.sdbgAtribPlant.Columns("POND").CellStyleSet ""
            Else
                frmPlantillasProce.sdbgAtribPlant.Columns("POND").CellStyleSet "Ponderacion"
            End If
        Case "frmPROCE"
            frmPROCE.sdbgAtributos.Columns("TIPOPONDERACION").Value = m_iPond
            If m_iPond = 0 Then
                frmPROCE.sdbgAtributos.Columns("PONDERACION").CellStyleSet ""
            Else
                frmPROCE.sdbgAtributos.Columns("PONDERACION").CellStyleSet "Ponderacion"
            End If
            If frmPROCE.sdbcGrupoA.Visible And frmPROCE.sdbcGrupoA.Columns(0).Value <> "************" Then
                Set oAtributoOrig = frmPROCE.g_oGrupoSeleccionado.ATRIBUTOS.Item(CStr(g_oAtributo.idAtribProce))
            Else
                Set oAtributoOrig = frmPROCE.g_oProcesoSeleccionado.ATRIBUTOS.Item(CStr(g_oAtributo.idAtribProce))
            End If
            oAtributoOrig.FECACT = g_oAtributo.FECACT
            oAtributoOrig.TipoPonderacion = m_iPond
            oAtributoOrig.Numero = g_oAtributo.Numero
            oAtributoOrig.Formula = g_oAtributo.Formula
            oAtributoOrig.ValorPondNo = g_oAtributo.ValorPondNo
            oAtributoOrig.ValorPondSi = g_oAtributo.ValorPondSi
            Set oAtributoOrig.ListaPonderacion = g_oAtributo.ListaPonderacion
            Set frmPROCE.g_oAtributoEnEdicion = oAtributoOrig
                                
        Case "PLANTILLASfrmATRIB"
            frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmPlantillasProce.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmPlantillasProce.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
            
        Case "frmPROCEfrmATRIB"
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmPROCE.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
        
        Case "frmFormulariosfrmATRIB"
            frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmFormularios.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmFormularios.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
            
        Case "frmDesglosefrmATRIB"
            frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmFormularios.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmFormularios.g_ofrmDesglose.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
            
        Case "frmPARTipoSolicitfrmATRIB"
            frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmPARTipoSolicit.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmPARTipoSolicit.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
        
        Case "frmPARTipoSolicitDesglosefrmATRIB"
            frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
    
        Case "ITEM_ATRIBESPfrmATRIB"
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TipoPond").Value = m_iPond
            frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            If m_iPond = 0 Then
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet ""
            Else
                frmPROCE.g_ofrmATRIB.sdbgAtributosGrupo.Columns("POND").CellStyleSet "Ponderacion"
            End If
            Set frmPROCE.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
        
        Case Else
            frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("TIPOPOND").Value = m_iPond
            frmESTRMATAtrib.g_ofrmATRIB.sdbgAtributosGrupo.Columns("FECACT").Value = g_oAtributo.FECACT
            Set frmESTRMATAtrib.g_ofrmATRIB.g_oAtributoEnEdicion = g_oAtributo
    End Select
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "cmdAcep1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAnyaCon_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdEliCon.Enabled = False
    cmdAnyaCon.Enabled = False
    cmdDeshacerCon.Enabled = False

    sdbgContinua.Scroll 0, sdbgContinua.Rows - sdbgContinua.Row
    
    If sdbgContinua.VisibleRows > 0 Then
        
        If sdbgContinua.VisibleRows > sdbgContinua.Rows Then
            sdbgContinua.Row = sdbgContinua.Rows
        Else
            sdbgContinua.Row = sdbgContinua.Rows - (sdbgContinua.Rows - sdbgContinua.VisibleRows) - 1
        End If
        
    End If

    If Me.Visible Then sdbgContinua.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "cmdAnyaCon_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub cmdCancel1_Click()
Dim iCountDisc As Integer
Dim iIndice As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_iPond = 1 And g_oAtributo.TipoPonderacion = EscalaContinua Then
        sdbgContinua.MoveFirst
        
        If sdbgContinua.Columns("DESDE").Text = "" Then
            oMensajes.FaltaPonderacion
            Exit Sub
        End If
    End If
    If m_iPond = 2 Then
        sdbgDiscreta.MoveFirst
            
        iCountDisc = 0
        For iIndice = 1 To sdbgDiscreta.Rows
            If sdbgDiscreta.Columns("PUNTOS").Value <> "" Then
                iCountDisc = iCountDisc + 1
            End If
            sdbgDiscreta.MoveNext
        Next iIndice
        If iCountDisc = 0 Then
            sdbgContinua.MoveFirst
        
            If sdbgContinua.Columns("DESDE").Text = "" And g_oAtributo.TipoPonderacion = EscalaContinua Then
                oMensajes.FaltaPonderacion
                Exit Sub
            End If
        End If
    End If
    Set m_oPondIntermedia = Nothing
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "cmdCancel1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub cmdDeshacerCon_Click()
''' * Objetivo: Deshacer la edicion en el valor actual
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgContinua.CancelUpdate
    sdbgContinua.DataChanged = False
    
    
    cmdAnyaCon.Enabled = True
    cmdEliCon.Enabled = True
    cmdDeshacerCon.Enabled = False
    
    If sdbgContinua.Rows = 1 Then
        sdbgContinua.MoveFirst
        sdbgContinua.Refresh
        sdbgContinua.Bookmark = sdbgContinua.SelBookmarks(0)
        sdbgContinua.SelBookmarks.RemoveAll
    End If
    m_bErrorC = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "cmdDeshacerCon_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdEliCon_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgContinua.Rows > 0 Then

        Screen.MousePointer = vbHourglass

        sdbgContinua.SelBookmarks.Add sdbgContinua.Bookmark
        EliminarContinuaSeleccionada
        Screen.MousePointer = vbNormal

    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "cmdEliCon_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdModif1_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        picNumerico.Enabled = True
        picBoolean.Enabled = True
        sdbgContinua.AllowAddNew = True
        sdbgContinua.AllowUpdate = True
        sdbgDiscreta.AllowUpdate = True
        sdbgDiscreta.Enabled = True
        sdbgContinua.Enabled = True
        sdbcSigno.Enabled = True
        cmdAcep1.Visible = True
        cmdCancel1.Visible = True
        cmdModif1.Visible = False
        txtNumero.Enabled = True
        txtPuntuacion1.Enabled = True
        txtPuntuacion2.Enabled = True
        fraAutomatico.Enabled = True
        fraFormula.Enabled = True
        cmdAnyaCon.Visible = True
        cmdAnyaCon.Enabled = True
        cmdEliCon.Visible = True
        cmdEliCon.Enabled = True
        cmdDeshacerCon.Visible = True
        cmdDeshacerCon.Enabled = False
        If (g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS") And g_bModDefAtrib Then
            chkDef1.Visible = True
        End If
        g_bEdicion = True
        
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "cmdModif1_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub optAutomatico_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bEdicion Then
        If (g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS") And g_bModDefAtrib Then
            If Not g_bAtribNoModif Then
                chkDef1.Visible = True
            End If
        End If
    End If
    fraAutomatico.Visible = True
    fraManualBoolean.Visible = False
    fraPond0b.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optAutomatico_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub optContinua_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bEdicion Then
        If (g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS") And g_bModDefAtrib Then
            If Not g_bAtribNoModif Then
                chkDef1.Visible = True
            End If
        End If
    End If
    fraContinua.Visible = True
    fraDiscreta.Visible = False
    fraManual.Visible = False
    fraFormula.Visible = False
    fraPond0.Visible = False
    If g_bEdicion Then
        cmdAnyaCon.Visible = True
        cmdAnyaCon.Enabled = True
        cmdEliCon.Visible = True
        cmdEliCon.Enabled = True
        cmdDeshacerCon.Visible = True
        cmdDeshacerCon.Enabled = False
    End If
    Set m_oPondIntermedia = Nothing
    Set m_oPondIntermedia = oFSGSRaiz.Generar_CValoresPond
    m_iIndiCole = 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optContinua_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub optDiscreta_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bEdicion Then
        If (g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS") And g_bModDefAtrib Then
            If Not g_bAtribNoModif Then
                chkDef1.Visible = True
            End If
        End If
    Else
        chkDef1.Visible = False
    End If
    fraContinua.Visible = False
    fraDiscreta.Visible = True
    fraManual.Visible = False
    fraFormula.Visible = False
    fraPond0.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optDiscreta_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub optFormula_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bEdicion Then
        If (g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS") And g_bModDefAtrib Then
            If Not g_bAtribNoModif Then
                chkDef1.Visible = True
            End If
        End If
    End If
    fraContinua.Visible = False
    fraDiscreta.Visible = False
    fraManual.Visible = False
    fraFormula.Visible = True
    fraPond0.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optFormula_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub optManual_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bEdicion Then
        If (g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS") And g_bModDefAtrib Then
            If Not g_bAtribNoModif Then
                chkDef1.Visible = True
            End If
        End If
    End If
    fraContinua.Visible = False
    fraDiscreta.Visible = False
    fraManual.Visible = True
    fraFormula.Visible = False
    fraPond0.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optManual_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub optManual2_Click()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    fraManualBoolean.Visible = True
    fraAutomatico.Visible = False
    fraPond0b.Visible = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optManual2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub



Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PONDERACION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then

        frmPonderacion.caption = Ador(0).Value
        Ador.MoveNext
        optContinua.caption = Ador(0).Value
        Ador.MoveNext
        optDiscreta.caption = Ador(0).Value
        Ador.MoveNext
        optFormula.caption = Ador(0).Value
        Ador.MoveNext
        optManual.caption = Ador(0).Value
        optManual2.caption = Ador(0).Value
        Ador.MoveNext
        cmdModif1.caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("DESDE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("HASTA").caption = Ador(0).Value
        Ador.MoveNext
        sdbgContinua.Columns("PUNTOS").caption = Ador(0).Value
        sdbgDiscreta.Columns("PUNTOS").caption = Ador(0).Value
        Ador.MoveNext
        chkDef1.caption = Ador(0).Value
        Ador.MoveNext
        cmdAcep1.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancel1.caption = Ador(0).Value
        Ador.MoveNext
        sdbgDiscreta.Columns("VALOR").caption = Ador(0).Value
        lblValor.caption = Ador(0).Value
        Ador.MoveNext
        lblTextoManual.caption = Ador(0).Value
        lblManualBoo.caption = Ador(0).Value
        Ador.MoveNext
        optAutomatico.caption = Ador(0).Value
        Ador.MoveNext
        lblSi.caption = Ador(0).Value
        Ador.MoveNext
        lblNo.caption = Ador(0).Value
        Ador.MoveNext
        lblPuntos1.caption = Ador(0).Value
        lblPuntos2.caption = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(0) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(1) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(2) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(3) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(4) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(5) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(6) = Ador(0).Value
        Ador.MoveNext
        lblPond0.caption = Ador(0).Value
        lblPond0b.caption = Ador(0).Value
        Ador.MoveNext
        lblPond1.caption = Ador(0).Value
        lblPond1b.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyaDis.caption = Ador(0).Value
        cmdAnyaCon.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliDis.caption = Ador(0).Value
        cmdEliCon.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacerDis.caption = Ador(0).Value
        cmdDeshacerCon.caption = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(7) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(8) = Ador(0).Value
        
        Ador.MoveNext
        optNoPond.caption = Ador(0).Value
        optNoPond2.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub Form_Load()
Dim oLista As CValorPond

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    
    Me.Width = 7395
    If g_oAtributo.Tipo = TipoBoolean Then
       cmdModif1.Top = 2775
       cmdAcep1.Top = 2775
       cmdCancel1.Top = 2775
       Me.Height = 3585
    Else
       cmdModif1.Top = 3500
       cmdAcep1.Top = 3500
       cmdCancel1.Top = 3500
       Me.Height = 4305
    End If
    
    g_bError = False
    CargarRecursos
    m_bBorrar = False
    m_iEnDefinicion = 0
    If Not g_bEdicion Or g_bAtribNoModif Then
        picBoolean.Enabled = False
        picNumerico.Enabled = False
        sdbgContinua.AllowAddNew = False
        sdbgContinua.AllowUpdate = False
        sdbgDiscreta.AllowUpdate = False
        sdbgDiscreta.Enabled = False
        sdbgContinua.Enabled = False
        cmdDeshacerDis.Enabled = False
        cmdDeshacerCon.Enabled = False
        sdbcSigno.Enabled = False
        cmdAcep1.Visible = False
        cmdCancel1.Visible = False
        If g_bAtribNoModif Then
            cmdModif1.Visible = False
            chkDef1.Enabled = False
            cmdAnyaCon.Visible = False
            cmdAnyaDis.Visible = False
            cmdDeshacerCon.Visible = False
            cmdDeshacerDis.Visible = False
            cmdEliCon.Visible = False
            cmdEliDis.Visible = False
            lblPond0.Top = lblPond0.Top + 130
            lblPond1.Visible = False
        Else
            cmdModif1.Visible = True
        End If
        chkDef1.Visible = False
        txtNumero.Enabled = False
        txtPuntuacion1.Enabled = False
        txtPuntuacion2.Enabled = False
        
    Else
        cmdModif1.Visible = False
        sdbgContinua.AllowAddNew = True
        sdbgContinua.AllowUpdate = True
        sdbgDiscreta.AllowUpdate = True
        picBoolean.Enabled = True
        picNumerico.Enabled = True
        sdbgDiscreta.Enabled = True
        sdbgContinua.Enabled = True
        sdbcSigno.Enabled = True
        cmdAcep1.Visible = True
        cmdCancel1.Visible = True
        cmdDeshacerDis.Enabled = False
        cmdDeshacerCon.Enabled = False
        If (g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS") And g_bModDefAtrib Then
            chkDef1.Visible = True
        Else
            chkDef1.Visible = False
        End If
        txtNumero.Enabled = True
        txtPuntuacion1.Enabled = True
        txtPuntuacion2.Enabled = True
    End If
    
    sdbcSigno.RemoveAll
    sdbcSigno.AddItem "+"
    sdbcSigno.AddItem "-"
    sdbcSigno.AddItem "*"
    sdbcSigno.AddItem "/"
    sdbcSigno.AddItem "+%"
    sdbcSigno.AddItem "-%"
    
    optContinua.Value = False
    optDiscreta.Value = False
    optFormula.Value = False
    optManual.Value = False
    
    fraFormula.Visible = False
    fraDiscreta.Visible = False
    fraContinua.Visible = False
    fraManual.Visible = False
    fraFormula.Visible = False
    fraPond0.Visible = False
    fraPond0b.Visible = False
    
    sdbgDiscreta.RemoveAll
    sdbgContinua.RemoveAll
    m_bSinUpdate = False
    
    Select Case g_oAtributo.TipoPonderacion
        Case 0
            Set m_oPondIntermedia = Nothing
            Set m_oPondIntermedia = oFSGSRaiz.Generar_CValoresPond
            m_iIndiCole = 1
            fraPond0.Visible = True
            If g_oAtributo.BajaLogica Then
                lblPond1.Visible = False
            End If
            Select Case g_oAtributo.Tipo
                   Case 1
                        picNumerico.Visible = True
                        picBoolean.Visible = False
                        optManual.Enabled = True
                        optContinua.Enabled = False
                        optFormula.Enabled = False
                        If g_oAtributo.TipoIntroduccion = 0 Then
                           optDiscreta.Enabled = False
                        Else
                           optDiscreta.Enabled = True
                           m_iIndiCole = 1
                           For Each oLista In g_oAtributo.ListaPonderacion
                               If oLista.IdOrden <> 0 Then
                                   sdbgDiscreta.AddItem oLista.ValorLista & Chr(9) & oLista.ValorPond & Chr(9) & m_iIndiCole
                                   m_iIndiCole = m_iIndiCole + 1
                               End If
                           Next
                        End If
                        Set m_picActual = picNumerico
                   Case 2
                        optContinua.Enabled = True
                        optFormula.Enabled = True
                        optManual.Enabled = True
                        If g_oAtributo.TipoIntroduccion = 0 Then
                            optDiscreta.Enabled = False
                        Else
                            optDiscreta.Enabled = True
                            m_iIndiCole = 1
                            For Each oLista In g_oAtributo.ListaPonderacion
                                If oLista.IdOrden <> 0 Then
                                    sdbgDiscreta.AddItem oLista.ValorLista & Chr(9) & oLista.ValorPond & Chr(9) & m_iIndiCole
                                    m_iIndiCole = m_iIndiCole + 1
                                End If
                            Next
                        End If
                        picNumerico.Visible = True
                        picBoolean.Visible = False
                        Set m_picActual = picNumerico
                   Case 3
                        optFormula.Enabled = False
                        If g_oAtributo.TipoIntroduccion = IntroLibre Then
                            optDiscreta.Enabled = False
                        Else
                            optDiscreta.Enabled = True
                            m_iIndiCole = 1
                            For Each oLista In g_oAtributo.ListaPonderacion
                                If oLista.IdOrden <> 0 Then
                                    sdbgDiscreta.AddItem oLista.ValorLista & Chr(9) & oLista.ValorPond & Chr(9) & m_iIndiCole
                                    m_iIndiCole = m_iIndiCole + 1
                                End If
                            Next
                        End If
                        optContinua.Enabled = True
                        optManual.Enabled = True
                        sdbcSigno.Text = ""
                        txtNumero.Text = ""
                        optFormula.Enabled = False
                        picNumerico.Visible = True
                        picBoolean.Visible = False
                        
                        Set m_picActual = picNumerico
                   Case 4
                        fraPond0b.Visible = True
                        If g_oAtributo.BajaLogica Then
                            lblPond1b.Visible = False
                        End If
                        optAutomatico.Value = False
                        txtPuntuacion1.Text = ""
                        txtPuntuacion2.Text = ""
                        picNumerico.Visible = False
                        picBoolean.Visible = True
                        Set m_picActual = picBoolean
            End Select
        Case 1
                        
            If g_iTipo <> 2 And g_iTipo <> 3 Then
                'mensaje de error, no coincide ponderacion con tipo de dato de atributo
                oMensajes.ErrorPonderacionTipoDato
                g_bError = True
                GoTo ERROR_Frm
            End If
            optContinua.Enabled = True
            optContinua.Value = True
            Set m_oPondIntermedia = Nothing
            Set m_oPondIntermedia = oFSGSRaiz.Generar_CValoresPond
            m_iIndiCole = 1

            For Each oLista In g_oAtributo.ListaPonderacion
                If oLista.IdOrden = 0 Then
                    sdbgContinua.AddItem oLista.ValorDesde & Chr(9) & oLista.ValorHasta & Chr(9) & oLista.ValorPond & Chr(9) & m_iIndiCole
                    m_oPondIntermedia.Add oLista.IDAtributo, oLista.AnyoProce, oLista.GMN1Proce, , , , , oLista.ValorPond, oLista.ValorDesde, oLista.ValorHasta, , m_iIndiCole
                    m_iIndiCole = m_iIndiCole + 1
                Else
                    sdbgDiscreta.AddItem oLista.ValorLista & Chr(9) & oLista.ValorPond & Chr(9) & oLista.IdOrden
                End If
            Next

            If g_iTipo = 2 Then
                optFormula.Enabled = True
            Else
                optFormula.Enabled = False
            End If
            If g_oAtributo.TipoIntroduccion = IntroLibre Then
                optDiscreta.Enabled = False
            Else
                optDiscreta.Enabled = True
            End If
            optManual.Enabled = True
            
            If g_bEdicion Then
                cmdAnyaCon.Visible = True
                cmdAnyaCon.Enabled = True
                cmdEliCon.Visible = True
                cmdEliCon.Enabled = True
                cmdDeshacerCon.Visible = True
                cmdDeshacerCon.Enabled = False
            End If
            
            picNumerico.Visible = True
            picBoolean.Visible = False
            fraContinua.Visible = True
            Set m_picActual = picNumerico
            
                
        Case 2
            If g_oAtributo.TipoIntroduccion = IntroLibre Then
                'mensaje de error, la ponderacion no se correspondecon el tipo de introducci�n
                oMensajes.ErrorPonderacionTipoIntroduccion
                g_bError = True
                GoTo ERROR_Frm
            End If
            If g_iTipo = 4 Then
                'mensaje de error, no coincide ponderacion con tipo de dato de atributo
                oMensajes.ErrorPonderacionTipoDato
                g_bError = True
                GoTo ERROR_Frm
            End If
            
            m_iIndiCole = 1
            For Each oLista In g_oAtributo.ListaPonderacion
                If oLista.IdOrden <> 0 Then
                    sdbgDiscreta.AddItem oLista.ValorLista & Chr(9) & oLista.ValorPond & Chr(9) & m_iIndiCole
                    m_iIndiCole = m_iIndiCole + 1
                End If
            Next
            If g_iTipo = 2 Then
                optFormula.Enabled = True
            Else
                optFormula.Enabled = False
            End If
            If g_iTipo = 2 Or g_iTipo = 3 Then
                optContinua.Enabled = True
            Else
                optContinua.Enabled = False
            End If
            optDiscreta.Enabled = True
            optDiscreta.Value = True
            optManual.Enabled = True
            fraDiscreta.Visible = True
            picNumerico.Visible = True
            picBoolean.Visible = False
            Set m_picActual = picNumerico

        Case 3
            If g_iTipo <> 2 Then
                'mensaje de error, no coincide ponderacion con tipo de atributo
                oMensajes.ErrorPonderacionTipoDato
                g_bError = True
                GoTo ERROR_Frm
            End If
            m_bRespetarComboSigno = True
            sdbcSigno.Text = NullToStr(g_oAtributo.Formula)
            m_bRespetarComboSigno = False
            txtNumero.Text = NullToStr(g_oAtributo.Numero)
            optContinua.Enabled = True
            If g_oAtributo.TipoIntroduccion = IntroLibre Then
                optDiscreta.Enabled = False
            Else
                optDiscreta.Enabled = True
                m_iIndiCole = 1
                For Each oLista In g_oAtributo.ListaPonderacion
                    If oLista.IdOrden <> 0 Then
                        sdbgDiscreta.AddItem oLista.ValorLista & Chr(9) & oLista.ValorPond & Chr(9) & m_iIndiCole
                        m_iIndiCole = m_iIndiCole + 1
                    End If
                Next
            End If
            optFormula.Enabled = True
            optManual.Enabled = True
            optFormula.Value = True
            fraFormula.Visible = True
            fraFormula.Enabled = True
            txtNumero.Enabled = True
            sdbcSigno.Enabled = True
            picNumerico.Visible = True
            picBoolean.Visible = False
            Set m_picActual = picNumerico
            
        Case 4
            If g_iTipo <> 4 Then
                If g_iTipo = 2 Then
                    optFormula.Enabled = True
                Else
                    optFormula.Enabled = False
                End If
                If g_iTipo = 2 Or g_iTipo = 3 Then
                    optContinua.Enabled = True
                Else
                    optContinua.Enabled = False
                End If
                If (g_iTipo = 2 Or g_iTipo = 1 Or g_iTipo = 3) And g_oAtributo.TipoIntroduccion = Introselec Then
                    optDiscreta.Enabled = True
                    m_iIndiCole = 1
                    For Each oLista In g_oAtributo.ListaPonderacion
                        If oLista.IdOrden <> 0 Then
                            sdbgDiscreta.AddItem oLista.ValorLista & Chr(9) & oLista.ValorPond & Chr(9) & m_iIndiCole
                            m_iIndiCole = m_iIndiCole + 1
                        End If
                    Next
                Else
                    optDiscreta.Enabled = False
                End If
                
                optManual.Enabled = True
                optManual.Value = True
                fraManual.Visible = True
                picNumerico.Visible = True
                picBoolean.Visible = False
                Set m_picActual = picNumerico
            Else
                picNumerico.Visible = False
                picBoolean.Visible = True
                optAutomatico.Value = False
                txtPuntuacion1.Text = NullToStr(g_oAtributo.ValorPondSi)
                txtPuntuacion2.Text = NullToStr(g_oAtributo.ValorPondNo)
                optManual2.Value = True
                Set m_picActual = picBoolean
            End If
            
        Case 5
              picNumerico.Visible = False
              picBoolean.Visible = True
              fraAutomatico.Visible = True
              optAutomatico.Value = True
              optManual.Value = False
              txtPuntuacion1.Text = NullToStr(g_oAtributo.ValorPondSi)
              txtPuntuacion2.Text = NullToStr(g_oAtributo.ValorPondNo)
              txtPuntuacion1.Enabled = True
              txtPuntuacion2.Enabled = True
              fraAutomatico.Enabled = False
              Set m_picActual = picBoolean
    End Select
    
    m_iPond = g_oAtributo.TipoPonderacion
    sdbgDiscreta.Enabled = True
    sdbgContinua.Enabled = True
    m_picActual.Visible = True
ERROR_Frm:
    Exit Sub
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
 Dim iCountDisc As Integer
 Dim iIndice As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_bDescargarFrm = False Then
    If m_iPond = 1 And g_oAtributo.TipoPonderacion = EscalaContinua Then
        sdbgContinua.MoveFirst
        
        If sdbgContinua.Columns("DESDE").Text = "" Then
            oMensajes.FaltaPonderacion
            Cancel = True
            Exit Sub
        End If
    End If
    If m_iPond = 2 Then
        sdbgDiscreta.MoveFirst
            
        iCountDisc = 0
        For iIndice = 1 To sdbgDiscreta.Rows
            If sdbgDiscreta.Columns("PUNTOS").Value <> "" Then
                iCountDisc = iCountDisc + 1
            End If
            sdbgDiscreta.MoveNext
        Next iIndice
        If iCountDisc = 0 Then
            sdbgContinua.MoveFirst
        
            If sdbgContinua.Columns("DESDE").Text = "" And g_oAtributo.TipoPonderacion = EscalaContinua Then
                oMensajes.FaltaPonderacion
                Cancel = True
                Exit Sub
            End If
        End If
    End If
End If
m_bDescargarFrm = False
    If Not m_oIBAseDatosEnEdicion Is Nothing Then
        m_oIBAseDatosEnEdicion.CancelarEdicion
        Set m_oIBAseDatosEnEdicion = Nothing
    End If
    Set m_oPondIntermedia = Nothing
    g_bAtribNoModif = False
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
   
End Sub

Private Sub optNoPond_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    fraContinua.Visible = False
    fraDiscreta.Visible = False
    fraManual.Visible = False
    fraFormula.Visible = False
    fraPond0.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optNoPond_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub optNoPond2_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    fraManualBoolean.Visible = False
    fraAutomatico.Visible = False
    fraPond0b.Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "optNoPond2_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcSigno_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarComboSigno Then
        optFormula.Value = True
        m_bRespetarComboSigno = False
        sdbcSigno.Text = ""
        m_bRespetarComboSigno = True
           
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbcSigno_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcSigno_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcSigno.DroppedDown Then
        sdbcSigno = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbcSigno_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcSigno_CloseUp()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If sdbcSigno.Value = "..." Or Trim(sdbcSigno.Value) = "" Then
        sdbcSigno.Text = ""
        Exit Sub
    End If
    
    m_bRespetarComboSigno = True
    sdbcSigno.Text = sdbcSigno.Columns(0).Text
    m_bRespetarComboSigno = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbcSigno_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcSigno_DropDown()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcSigno.RemoveAll
    sdbcSigno.AddItem "+"
    sdbcSigno.AddItem "-"
    sdbcSigno.AddItem "*"
    sdbcSigno.AddItem "/"
    sdbcSigno.AddItem "+%"
    sdbcSigno.AddItem "-%"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbcSigno_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbcSigno_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcSigno.DataFieldList = "Column 0"
    sdbcSigno.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbcSigno_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcSigno_PositionList(ByVal Text As String)
 ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    sdbcSigno.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcSigno.Rows - 1
            bm = sdbcSigno.GetBookmark(i)
            If UCase(sdbcSigno.Text) = UCase(Mid(sdbcSigno.Columns(0).CellText(bm), 1, Len(sdbcSigno.Text))) Then
                sdbcSigno.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbcSigno_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcSigno_Validate(Cancel As Boolean)
Dim i As Integer
Dim bm As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcSigno.Text <> "" Then

           For i = 0 To sdbcSigno.Rows - 1
            bm = sdbcSigno.GetBookmark(i)
            If UCase(sdbcSigno.Text) = UCase(Mid(sdbcSigno.Columns(0).CellText(bm), 1, Len(sdbcSigno.Text))) Then
                sdbcSigno.Bookmark = bm
                Exit Sub
            End If
           Next i
           oMensajes.NoValida " " & lblValor.caption
           sdbcSigno.Text = ""
    End If
   
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbcSigno_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbgContinua_AfterUpdate(RtnDispErrMsg As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    RtnDispErrMsg = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgContinua_AfterUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgContinua_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 DispPromptMsg = 0
 If sdbgContinua.Columns("INDI").Value <> "" Then
    m_oPondIntermedia.Remove (sdbgContinua.Columns("INDI").Value)
 End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgContinua_BeforeDelete", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgContinua_BeforeUpdate(Cancel As Integer)
Dim vValorDesde As Variant
Dim vValorHasta As Variant
Dim oListaInter As CValorPond
Dim irespuesta As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bErrorC = False
m_bBorrar = False
   If Not m_bSinUpdate Then
    If sdbgContinua.Columns("DESDE").Text = "" Then
        If sdbgContinua.Columns("HASTA").Text = "" And sdbgContinua.Columns("PUNTOS").Text = "" Then
            
            If sdbgContinua.Columns("INDI").Value <> "" Then
               m_oPondIntermedia.Remove (sdbgContinua.Columns("INDI").Value)
               m_bBorrar = True
               Exit Sub
            Else
                sdbgContinua.CancelUpdate
                If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha Then
                    oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & " " & m_sMensaje(3)
                Else
                    oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & " " & m_sMensaje(2)
                End If
                If Me.Visible Then sdbgContinua.SetFocus
                Cancel = True
                m_bErrorC = True
            End If
        End If
    Else
        If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha Then
            If Not IsDate(sdbgContinua.Columns("DESDE").Text) Then
                oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & " " & m_sMensaje(3)
                If Me.Visible Then sdbgContinua.SetFocus
                Cancel = True
                m_bErrorC = True
            Else
                If Not IsDate(sdbgContinua.Columns("HASTA").Text) Then
                    oMensajes.NoValido sdbgContinua.Columns("HASTA").caption & " " & m_sMensaje(3)
                    If Me.Visible Then sdbgContinua.SetFocus
                    Cancel = True
                    m_bErrorC = True
                Else
                    If g_oAtributo.Minimo <> "" And IsDate(g_oAtributo.Minimo) Then
                        If g_oAtributo.Maximo <> "" And IsDate(g_oAtributo.Maximo) Then
                            If CDate(g_oAtributo.Minimo) > CDate(sdbgContinua.Columns("DESDE").Value) Or CDate(g_oAtributo.Maximo) < CDate(sdbgContinua.Columns("DESDE").Value) Then
                              oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            ElseIf CDate(g_oAtributo.Maximo) < CDate(sdbgContinua.Columns("HASTA").Value) Or CDate(g_oAtributo.Minimo) > CDate(sdbgContinua.Columns("HASTA").Value) Then
                                oMensajes.NoValido sdbgContinua.Columns("HASTA").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                                If Me.Visible Then sdbgContinua.SetFocus
                                Cancel = True
                                m_bErrorC = True
                                Exit Sub
                            End If
                        Else
                            If CDate(g_oAtributo.Minimo) > CDate(sdbgContinua.Columns("DESDE").Value) Then
                              oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            ElseIf CDate(g_oAtributo.Minimo) > CDate(sdbgContinua.Columns("HASTA").Value) Then
                                oMensajes.NoValido sdbgContinua.Columns("HASTA").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                                If Me.Visible Then sdbgContinua.SetFocus
                                Cancel = True
                                m_bErrorC = True
                                Exit Sub
                            End If
                        End If
                    Else
                        If g_oAtributo.Maximo <> "" And IsDate(g_oAtributo.Maximo) Then
                            If CDate(g_oAtributo.Maximo) < CDate(sdbgContinua.Columns("DESDE").Value) Then
                              oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            ElseIf CDate(g_oAtributo.Maximo) < CDate(sdbgContinua.Columns("HASTA").Value) Then
                              oMensajes.NoValido sdbgContinua.Columns("HASTA").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                              If Me.Visible Then sdbgContinua.SetFocus
                              Cancel = True
                              m_bErrorC = True
                              Exit Sub
                            End If
                        End If
                    End If
                    If Not IsNumeric(sdbgContinua.Columns("PUNTOS").Value) Then
                        oMensajes.NoValida sdbgContinua.Columns("PUNTOS").caption & " " & m_sMensaje(2)
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                    End If
                End If
            End If
        Else
            If g_oAtributo.Tipo = TipoNumerico Then
                If Not IsNumeric(sdbgContinua.Columns("DESDE").Text) Then
                    oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & " " & m_sMensaje(2)
                    If Me.Visible Then sdbgContinua.SetFocus
                    Cancel = True
                    m_bErrorC = True
                Else
                    If Not IsNumeric(sdbgContinua.Columns("HASTA").Text) Then
                        oMensajes.NoValido sdbgContinua.Columns("HASTA").caption & " " & m_sMensaje(2)
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                    Else
                        If g_oAtributo.Minimo <> "" Then
                            If g_oAtributo.Maximo <> "" Then
                                If (VarToDbl0(g_oAtributo.Minimo) > VarToDbl0(sdbgContinua.Columns("DESDE").Value) Or VarToDbl0(g_oAtributo.Maximo) < VarToDbl0(sdbgContinua.Columns("DESDE").Value)) Then
                                    oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            Else
                                If VarToDbl0(g_oAtributo.Minimo) > (VarToDbl0(sdbgContinua.Columns("DESDE").Value)) Then
                                    oMensajes.NoValido sdbgContinua.Columns("DESDE").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            End If
                        End If
                        If g_oAtributo.Maximo <> "" Then
                            If g_oAtributo.Minimo <> "" Then
                                If (VarToDbl0(g_oAtributo.Maximo) < (VarToDbl0(sdbgContinua.Columns("HASTA").Value)) Or VarToDbl0(g_oAtributo.Minimo) > VarToDbl0(sdbgContinua.Columns("HASTA").Value)) Then
                                    oMensajes.NoValido sdbgContinua.Columns("HASTA").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            Else
                                If VarToDbl0(g_oAtributo.Maximo) < VarToDbl0(sdbgContinua.Columns("HASTA").Value) Then
                                    oMensajes.NoValido sdbgContinua.Columns("HASTA").caption & vbLf & m_sMensaje(7) & ": (" & g_oAtributo.Minimo & " - " & g_oAtributo.Maximo & ")"
                                    If Me.Visible Then sdbgContinua.SetFocus
                                    Cancel = True
                                    m_bErrorC = True
                                    Exit Sub
                                End If
                            End If
                        End If
                    
                        If Not IsNumeric(sdbgContinua.Columns("PUNTOS").Value) Then
                            oMensajes.NoValida sdbgContinua.Columns("PUNTOS").caption & " " & m_sMensaje(2)
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                        End If
                    End If
                End If
            End If
        End If
        
        If Not m_bErrorC Then
            If g_oAtributo.Tipo = TiposDeAtributos.TipoFecha Then
                If IsDate(sdbgContinua.Columns("DESDE").Text) And IsDate(sdbgContinua.Columns("HASTA").Text) Then
                    If CDate(sdbgContinua.Columns("DESDE").Text) > CDate(sdbgContinua.Columns("HASTA").Text) Then
                        oMensajes.NoValida m_sMensaje(1) 'sdbgContinua.Columns("DESDE").Caption
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                    End If
                End If
            Else
                If VarToDbl0(sdbgContinua.Columns("DESDE").Text) > VarToDbl0(sdbgContinua.Columns("HASTA").Text) Then
                    oMensajes.NoValida m_sMensaje(1) 'sdbgContinua.Columns("DESDE").Caption
                    If Me.Visible Then sdbgContinua.SetFocus
                    Cancel = True
                    m_bErrorC = True
                End If
            End If
            If Not m_bErrorC Then
                If g_oAtributo.Tipo = TipoNumerico Then
                    vValorDesde = VarToDbl0(sdbgContinua.Columns("DESDE").Text)
                    vValorHasta = VarToDbl0(sdbgContinua.Columns("HASTA").Text)
                Else
                    vValorDesde = CDate(sdbgContinua.Columns("DESDE").Text)
                    vValorHasta = CDate(sdbgContinua.Columns("HASTA").Text)
                End If
                If sdbgContinua.Row + 1 <= m_oPondIntermedia.Count Then
                    m_oPondIntermedia.Item(sdbgContinua.Columns("INDI").Value).ValorDesde = vValorDesde
                    m_oPondIntermedia.Item(sdbgContinua.Columns("INDI").Value).ValorHasta = vValorHasta
                End If
                For Each oListaInter In m_oPondIntermedia
                If sdbgContinua.Row + 1 <> oListaInter.indice Then
                    If Abs(vValorDesde) >= Abs(oListaInter.ValorDesde) And Abs(vValorDesde) < Abs(oListaInter.ValorHasta) Then
                        'el intervalo definido ya existe
                        oMensajes.NoValido m_sMensaje(0)
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                        Exit Sub
                    End If
                    If Abs(vValorHasta) > Abs(oListaInter.ValorDesde) And Abs(vValorHasta) < Abs(oListaInter.ValorHasta) Then
                        'el intervalo definido ya existe
                        oMensajes.NoValida m_sMensaje(0)
                        If Me.Visible Then sdbgContinua.SetFocus
                        Cancel = True
                        m_bErrorC = True
                        Exit Sub
                    End If
                    If Abs(vValorDesde) < Abs(oListaInter.ValorDesde) And Abs(vValorHasta) > Abs(oListaInter.ValorHasta) Then
                        'el intervalo introducido contiene uno de los ya existe
                        irespuesta = oMensajes.PreguntaEliminarIntervalo(m_sMensaje(5) & ": " & vbCrLf & oListaInter.ValorDesde & " - " & oListaInter.ValorHasta)
                        If irespuesta = vbNo Then
                            MsgBox m_sMensaje(6), vbInformation, "FULLSTEP"
                            If Me.Visible Then sdbgContinua.SetFocus
                            Cancel = True
                            m_bErrorC = True
                            Exit Sub
                        Else
                            If sdbgContinua.Row + 1 > m_oPondIntermedia.Count Then
                                m_oPondIntermedia.Add g_oAtributo.Id, , , , , , , StrToNull(sdbgContinua.Columns("PUNTOS").Value), vValorDesde, vValorHasta, , m_iIndiCole
                                m_iIndiCole = m_iIndiCole + 1
                            End If
                            m_oPondIntermedia.Remove (oListaInter.indice)
                            m_bBorrar = True
                            Exit Sub
                        End If
                    End If
                End If
                Next
                If sdbgContinua.Row + 1 > m_oPondIntermedia.Count Then
                    sdbgContinua.Columns("INDI").Value = sdbgContinua.Rows
                    m_oPondIntermedia.Add g_oAtributo.Id, g_oAtributo.AnyoProce, g_oAtributo.GMN1, g_oAtributo.CodProce, g_oAtributo.codgrupo, , , StrToNull(sdbgContinua.Columns("PUNTOS").Value), vValorDesde, vValorHasta, , m_iIndiCole
                    m_iIndiCole = m_iIndiCole + 1
                End If
            End If
        End If
    End If
   End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgContinua_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgContinua_Change()
Dim iIndice As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If cmdDeshacerCon.Enabled = False Then
    
        cmdAnyaCon.Enabled = False
        cmdEliCon.Enabled = False
        cmdDeshacerCon.Enabled = True
    
    End If
    
    sdbgDiscreta.MoveFirst
    For iIndice = 1 To sdbgDiscreta.Rows
        sdbgDiscreta.Columns("PUNTOS").Value = ""
        g_oAtributo.ListaPonderacion.Item(sdbgDiscreta.Columns("INDI").Value).ValorLista = Null
        sdbgDiscreta.MoveNext
    Next

    sdbcSigno.Text = ""
    m_bRespeCombo = True
    txtNumero.Text = ""
    m_bRespeCombo = False
    g_oAtributo.Formula = Null
    g_oAtributo.Numero = Null
    
    optContinua.Value = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgContinua_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgContinua_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: no dejar eliminar desde el teclado

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyDelete Then
        
        Exit Sub
     
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgContinua_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgContinua_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgContinua.IsAddRow Then
        cmdEliCon.Enabled = False
        cmdAnyaCon.Enabled = False
        cmdDeshacerCon.Enabled = True
    Else
        cmdEliCon.Enabled = True
        cmdAnyaCon.Enabled = True
        cmdDeshacerCon.Enabled = False
    End If
'    cmdAnyaCon.Enabled = True
'    cmdDeshacerCon.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgContinua_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgContinua_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgContinua.IsAddRow Then
        cmdEliCon.Enabled = False
    Else
        cmdEliCon.Enabled = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgContinua_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgDiscreta_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
 DispPromptMsg = 0
 If sdbgDiscreta.Columns("INDI").Value <> "" Then
    m_oPondIntermedia.Remove (sdbgDiscreta.Columns("INDI").Value)
 End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgDiscreta_BeforeDelete", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgDiscreta_BeforeUpdate(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bErrorD = False
    If Not IsNumeric(sdbgDiscreta.Columns("PUNTOS").Text) And sdbgDiscreta.Columns("PUNTOS").Value <> "" Then
        oMensajes.NoValida sdbgDiscreta.Columns("PUNTOS").caption & " " & m_sMensaje(2)
        If Me.Visible Then sdbgDiscreta.SetFocus
        Cancel = True
        m_bErrorD = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgDiscreta_BeforeUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbgDiscreta_Change()
 Dim iIndice As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If cmdDeshacerDis.Enabled = False Then
    
        cmdAnyaDis.Enabled = False
        cmdEliDis.Enabled = False
        cmdDeshacerDis.Enabled = True
    
    End If
    
    Set m_oPondIntermedia = Nothing
    sdbgContinua.MoveFirst
    For iIndice = 1 To sdbgContinua.Rows
        g_oAtributo.ListaPonderacion.Remove (sdbgContinua.Columns("INDI").Value)
        sdbgContinua.MoveNext
    Next
    sdbgContinua.RemoveAll

    sdbcSigno.Text = ""
    m_bRespeCombo = True
    txtNumero.Text = ""
    m_bRespeCombo = False
    g_oAtributo.Formula = Null
    g_oAtributo.Numero = Null
    optDiscreta.Value = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgDiscreta_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub sdbgDiscreta_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: no dejar eliminar desde el teclado

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyDelete Then
        
        Exit Sub
     
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgDiscreta_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgDiscreta_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgDiscreta.IsAddRow Then
        cmdEliDis.Enabled = False
    Else
        cmdEliDis.Enabled = True
    End If
    cmdAnyaDis.Enabled = True
    cmdDeshacerDis.Enabled = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgDiscreta_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgDiscreta_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgDiscreta.IsAddRow Then
        cmdEliDis.Enabled = False
    Else
        cmdEliDis.Enabled = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "sdbgDiscreta_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtNumero_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespeCombo Then
    
        If Not IsNumeric(txtNumero.Text) And txtNumero.Text <> "" Then
            oMensajes.NoValido lblValor.caption & vbLf & m_sMensaje(2)
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "txtNumero_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtPuntuacion1_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not IsNumeric(txtPuntuacion1.Text) And txtPuntuacion1.Text <> "" Then
    oMensajes.NoValido lblSi.caption
    If Me.Visible Then txtPuntuacion1.SetFocus
    Exit Sub
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "txtPuntuacion1_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
            
End Sub

Private Sub txtPuntuacion2_Change()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not IsNumeric(txtPuntuacion2.Text) And txtPuntuacion2.Text <> "" Then
    oMensajes.NoValido lblNo.caption
    If Me.Visible Then txtPuntuacion2.SetFocus
    Exit Sub
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "txtPuntuacion2_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Public Sub EliminarContinuaSeleccionada()
Dim irespuesta As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oListaEnEdicion = m_oPondIntermedia.Item(CStr(sdbgContinua.Columns("INDI").Value))
    m_oListaEnEdicion.ValorDesde = StrToNull(sdbgContinua.Columns("DESDE").Value)
    m_oListaEnEdicion.ValorHasta = StrToNull(sdbgContinua.Columns("HASTA").Value)
    
    Set m_oIBAseDatosEnEdicion = m_oListaEnEdicion
    
    irespuesta = oMensajes.PreguntaEliminar("( " & sdbgContinua.Columns("DESDE").caption & ": " & sdbgContinua.Columns("DESDE").Value & " - " & sdbgContinua.Columns("HASTA").caption & ": " & sdbgContinua.Columns("HASTA").Value & ")")

    If irespuesta = vbYes Then
        
        sdbgContinua.SelBookmarks.Add sdbgContinua.Bookmark
        m_oPondIntermedia.Remove (CStr(sdbgContinua.Columns("INDI").Value))

        If sdbgContinua.AddItemRowIndex(sdbgContinua.Bookmark) > -1 Then
            sdbgContinua.RemoveItem (sdbgContinua.AddItemRowIndex(sdbgContinua.Bookmark))
        Else
            sdbgContinua.RemoveItem (0)
        End If
    End If
    
    If sdbgContinua.Rows > 0 Then
        If IsEmpty(sdbgContinua.RowBookmark(sdbgContinua.Row)) Then
            sdbgContinua.Bookmark = sdbgContinua.RowBookmark(sdbgContinua.Row - 1)
        Else
            sdbgContinua.Bookmark = sdbgContinua.RowBookmark(sdbgContinua.Row)
        End If
    End If
    
    Set m_oListaEnEdicion = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    
    sdbgContinua.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgContinua.SetFocus
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPonderacion", "EliminarContinuaSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
