VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSELPROVEAnya 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "A�adir proveedores"
   ClientHeight    =   2100
   ClientLeft      =   1965
   ClientTop       =   4710
   ClientWidth     =   6780
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELPROVEAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2100
   ScaleWidth      =   6780
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   6780
      TabIndex        =   13
      Top             =   1680
      Width           =   6780
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   3450
         TabIndex        =   15
         Top             =   45
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   2250
         TabIndex        =   14
         Top             =   45
         Width           =   1005
      End
   End
   Begin VB.CommandButton cmdBuscarPortal 
      Caption         =   "Buscar en el portal"
      Height          =   330
      Left            =   1050
      TabIndex        =   12
      Top             =   120
      Width           =   5250
   End
   Begin VB.CommandButton cmdBuscar 
      BeginProperty Font 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6375
      Picture         =   "frmSELPROVEAnya.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   520
      Width           =   315
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
      Height          =   285
      Left            =   1020
      TabIndex        =   3
      Top             =   870
      Width           =   1510
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1296
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5345
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2663
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 3"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcCompCod 
      Height          =   285
      Left            =   1020
      TabIndex        =   5
      Top             =   1240
      Width           =   1510
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1111
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5001
      Columns(1).Caption=   "Apellido y nombre"
      Columns(1).Name =   "Ape"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2663
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 3"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcCompDen 
      Height          =   285
      Left            =   2540
      TabIndex        =   6
      Top             =   1240
      Width           =   3795
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5424
      Columns(0).Caption=   "Apellido y nombre"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1296
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6694
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
      Height          =   285
      Left            =   2540
      TabIndex        =   1
      Top             =   510
      Width           =   3795
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6165
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2117
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6694
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
      Height          =   285
      Left            =   1020
      TabIndex        =   0
      Top             =   510
      Width           =   1510
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2540
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6482
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   2663
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
      Height          =   285
      Left            =   2540
      TabIndex        =   4
      Top             =   870
      Width           =   3795
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6165
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1640
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6694
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin VB.Label lblCom 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label3"
      Height          =   285
      Left            =   1020
      TabIndex        =   11
      Top             =   1240
      Visible         =   0   'False
      Width           =   5300
   End
   Begin VB.Label lblEqp 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   1020
      TabIndex        =   10
      Top             =   870
      Visible         =   0   'False
      Width           =   5300
   End
   Begin VB.Label Label3 
      BackColor       =   &H00808000&
      Caption         =   "Proveedor:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   60
      TabIndex        =   9
      Top             =   540
      Width           =   975
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Comprador:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   60
      TabIndex        =   8
      Top             =   1260
      Width           =   855
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Equipo:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   60
      TabIndex        =   7
      Top             =   890
      Width           =   600
   End
End
Attribute VB_Name = "frmSELPROVEAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private oProves As CProveedores
Private oEqps As CEquipos
Private oComps As CCompradores
Private sEqpCod As String
Private sComCod As String
Private sProveCod As String
Private oEqpSeleccionado As CEquipo
Private CargarComboDesde As Boolean
Private CargarComboDesdeComp As Boolean
Private CargarComboDesdeEqp As Boolean
Private RespetarCombo As Boolean
Private bRMatProve As Boolean
Private bREqpProve As Boolean
Public bAnya As Boolean
Private bVerAsigEqp As Boolean
Private bVerAsigComp As Boolean
Public Accion As AccionesSummit
Public oActsN1 As CActividadesNivel1
Public sOrigen As String
Public bNoDescargar As Boolean

Private sIdioma() As String

Public Sub CargarProveedorPortal(ByVal oProve As CProveedor)

    RespetarCombo = True
    sdbcProveCod = oProve.Cod
    sdbcProveCod_Validate True

    sProveCod = sdbcProveCod
    RespetarCombo = False

End Sub

''' <summary>
''' Evento que se produce al pulsar el bot�n de aceptar
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
''' <revision>JVS 16/03/2011</revision>
Private Sub cmdAceptar_Click()
Dim sCod As String
Dim teserror As TipoErrorSummit
Dim oAsigSeleccionadas As CAsignaciones
    
    ' 5/11/2001 Es obligatorio seleccionar PROVEEDOR, EQUIPO Y COMPRADOR
    ' Adem�s del comprador seleccionado se incluyen los potenciales
    If sProveCod = "" Then
        oMensajes.FaltanDatos sIdioma(1)
        Exit Sub
    End If
        
    If sEqpCod = "" Then
        oMensajes.FaltanDatos sIdioma(2)
        Exit Sub
    End If
    
    If sComCod = "" Then
        oMensajes.FaltanDatos sIdioma(3)
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    sCod = sProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProveCod))
    sCod = sCod & NullToStr(sEqpCod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(NullToStr(sEqpCod)))
    sCod = sCod & NullToStr(sComCod) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCOM - Len(NullToStr(sComCod)))
    
      
    Set oAsigSeleccionadas = oFSGSRaiz.Generar_CAsignaciones
    
    'Lo asigna por defecto:
    oAsigSeleccionadas.Add sProveCod, "", True, sEqpCod, sComCod, , , , , , , 1
    
    'Asigna el proveedor 1101f
    If bVerAsigComp Then
        frmSELPROVE.oIasig.AsignarOtroProve oAsigSeleccionadas, sEqpCod, sComCod ' 5/11/2001
    Else
        If bVerAsigEqp Then
            frmSELPROVE.oIasig.AsignarOtroProve oAsigSeleccionadas, sEqpCod ' 5/11/2001
        Else
            frmSELPROVE.oIasig.AsignarOtroProve oAsigSeleccionadas ' 5/11/2001
        End If
    End If

    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        frmSELPROVE.DesbloquearProceso 1
        Exit Sub
    End If
    
    frmSELPROVE.DesbloquearProceso 1
    
    If basPublic.gParametrosInstalacion.gbSeleccionPositiva = True Then
        basSeguridad.RegistrarAccion AccionesSummit.ACCSelProceAnya, "Anyo:" & CStr(frmSELPROVE.sdbcAnyo) & "GMN1:" & CStr(frmSELPROVE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmSELPROVE.sdbcProceCod) & "Prove:" & sdbcProveCod.Value
    Else
        basSeguridad.RegistrarAccion AccionesSummit.ACCSelProveMod, "Anyo:" & CStr(frmSELPROVE.sdbcAnyo) & "GMN1:" & CStr(frmSELPROVE.sdbcGMN1_4Cod) & "Proce:" & CStr(frmSELPROVE.sdbcProceCod) & "Prove:" & sdbcProveCod.Value & "A�ade potencial"
    End If
    
    frmSELPROVE.cmdRestaurar_Click
    frmSELPROVE.Accion = ACCSelProveCon
    Set oAsigSeleccionadas = Nothing

    Screen.MousePointer = vbNormal
    
    Unload Me

         
End Sub

Private Sub cmdBuscar_Click()
    bNoDescargar = True
    
    frmPROVEBuscar.sOrigen = "SelProveAnya"
    If bRMatProve Then
        frmPROVEBuscar.bRMat = True
    End If
    
    If bREqpProve Then
        frmPROVEBuscar.bREqp = True
    End If
    
    frmPROVEBuscar.Show 1
    
End Sub

Private Sub cmdBuscarPortal_Click()
Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim oGrupoMatNivel4 As CGrupoMatNivel4
Dim arActividades()  As Variant

    
    bNoDescargar = True
    
    
    ReDim arActividades(6)

    Screen.MousePointer = vbHourglass
    
    Set oGrupoMatNivel4 = oFSGSRaiz.Generar_CGrupoMatNivel4
   
'        oGrupoMatNivel4.GMN1Cod = frmSELPROVE.oProcesoSeleccionado.GMN1Cod
'        oGrupoMatNivel4.GMN1Den = frmSELPROVE.oProcesoSeleccionado.GMN1Den
'        oGrupoMatNivel4.GMN2Cod = frmSELPROVE.oProcesoSeleccionado.GMN2Cod
'        oGrupoMatNivel4.GMN2Den = frmSELPROVE.oProcesoSeleccionado.GMN2Den
'        oGrupoMatNivel4.GMN3Cod = frmSELPROVE.oProcesoSeleccionado.GMN3Cod
'        oGrupoMatNivel4.GMN3Den = frmSELPROVE.oProcesoSeleccionado.GMN3Den
'        oGrupoMatNivel4.Cod = frmSELPROVE.oProcesoSeleccionado.GMN4Cod
'        oGrupoMatNivel4.Den = frmSELPROVE.oProcesoSeleccionado.GMN4Den
        
    
    
    arActividades = oGrupoMatNivel4.DevolverActividadAsociada(gParametrosGenerales.gbSincronizacionMat, basPublic.gParametrosInstalacion.gIdiomaPortal)
    
    With frmAdmProvePortal
                
        If (IsEmpty(arActividades(0)) And IsEmpty(arActividades(1)) And IsEmpty(arActividades(2)) And IsEmpty(arActividades(3)) And IsEmpty(arActividades(4))) Then
        
        Else
                       
            .g_vACN1Seleccionada = arActividades(0)
            .g_vACN2Seleccionada = arActividades(1)
            .g_vACN3Seleccionada = arActividades(2)
            .g_vACN4Seleccionada = arActividades(3)
            .g_vACN5Seleccionada = arActividades(4)
            
            Set oActsN1 = oFSGSRaiz.generar_CActividadesNivel1
            oActsN1.GenerarEstructuraActividades False, basPublic.gParametrosInstalacion.gIdiomaPortal
            
            
            If Not IsNull(.g_vACN5Seleccionada) Then
                scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                scod3 = scod2 & CStr(.g_vACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN3Seleccionada)))
                scod4 = scod3 & CStr(.g_vACN4Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN4Seleccionada)))
                scod5 = scod4 & CStr(.g_vACN5Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN5Seleccionada)))
                Set oACN5 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4).ActividadesNivel5.Item(scod5)
                If Not oACN5 Is Nothing Then ' pueden borrar la actividad desde el portal y no encontrarse
                    .lblActividad = oACN5.Cod & " - " & oACN5.Den
                Else
                        .g_vACN1Seleccionada = Empty ' para que luego no nos aparezca ning�n enlace
                        .g_vACN2Seleccionada = Empty
                        .g_vACN3Seleccionada = Empty
                        .g_vACN4Seleccionada = Empty
                        .g_vACN5Seleccionada = Empty
                End If
            Else
                If .g_vACN4Seleccionada <> "" Then
                    scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                    scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                    scod3 = scod2 & CStr(.g_vACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN3Seleccionada)))
                    scod4 = scod3 & CStr(.g_vACN4Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN4Seleccionada)))
                    Set oACN4 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4)
                    If Not oACN4 Is Nothing Then ' pueden borrar la actividad desde el portal y no encontrarse
                        .lblActividad = oACN4.Cod & " - " & oACN4.Den
                    Else
                            .g_vACN1Seleccionada = Empty ' para que luego no nos aparezca ning�n enlace
                            .g_vACN2Seleccionada = Empty
                            .g_vACN3Seleccionada = Empty
                            .g_vACN4Seleccionada = Empty
                            .g_vACN5Seleccionada = Empty
    
                     End If
                                             
                Else
                    If .g_vACN3Seleccionada <> "" Then
                            scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                            scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                            scod3 = scod2 & CStr(.g_vACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN3Seleccionada)))
                            Set oACN3 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3)
                            If Not oACN3 Is Nothing Then ' pueden borrar la actividad desde el portal y no encontrarse
                                .lblActividad = oACN3.Cod & " - " & oACN3.Den
                            Else
                                .g_vACN1Seleccionada = Empty ' para que luego no nos aparezca ning�n enlace
                                .g_vACN2Seleccionada = Empty
                                .g_vACN3Seleccionada = Empty
                                .g_vACN4Seleccionada = Empty
                                .g_vACN5Seleccionada = Empty
                            End If
                        Else
                            If .g_vACN2Seleccionada <> "" Then
                                scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                                scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                                Set oACN2 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2)
                                If Not oACN2 Is Nothing Then ' pueden borrar la actividad desde el portal y no encontrarse
                                    .lblActividad = oACN2.Cod & " - " & oACN2.Den
                                Else
                                    .g_vACN1Seleccionada = Empty ' para que luego no nos aparezca ning�n enlace
                                    .g_vACN2Seleccionada = Empty
                                    .g_vACN3Seleccionada = Empty
                                    .g_vACN4Seleccionada = Empty
                                    .g_vACN5Seleccionada = Empty
                                End If
                            Else
                                If .g_vACN1Seleccionada <> "" Then
                                    scod1 = CStr(arActividades(0)) & Mid$("                         ", 1, 10 - Len(CStr(arActividades(0))))
                                    Set oACN1 = oActsN1.Item(scod1)
                                    If Not oACN1 Is Nothing Then ' pueden borrar la actividad desde el portal y no encontrarse
                                        .lblActividad = oACN1.Cod & " - " & oACN1.Den
                                    Else
                                    .g_vACN1Seleccionada = Empty ' para que luego no nos aparezca ning�n enlace
                                    .g_vACN2Seleccionada = Empty
                                    .g_vACN3Seleccionada = Empty
                                    .g_vACN4Seleccionada = Empty
                                    .g_vACN5Seleccionada = Empty
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        
        .g_sOrigen = "frmSELPROVEAnya"
        
        End With
   
    
    Screen.MousePointer = vbNormal
    
    MDI.MostrarFormulario frmAdmProvePortal
    
    
End Sub

Private Sub cmdCancelar_Click()
    
    Unload Me
    
End Sub

Private Sub Form_Activate()
    Unload frmPROVEBuscar
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPROVEANYA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 3)
        For i = 1 To 3
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        Label1.caption = sIdioma(3) & ":"
        label2.caption = sIdioma(2) & ":"
        Label3.caption = sIdioma(1) & ":"
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmSELPROVEAnya.caption = Ador(0).Value
        Ador.MoveNext
        sdbcCompCod.Columns(0).caption = Ador(0).Value '7 cod
        sdbcCompDen.Columns(1).caption = Ador(0).Value
        sdbcEqpCod.Columns(0).caption = Ador(0).Value
        sdbcEqpDen.Columns(1).caption = Ador(0).Value
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcCompCod.Columns(1).caption = Ador(0).Value '8 Apellidos y nombre
        sdbcCompDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcEqpCod.Columns(1).caption = Ador(0).Value '9 Denominaci�n
        sdbcEqpDen.Columns(0).caption = Ador(0).Value
        sdbcProveCod.Columns(1).caption = Ador(0).Value '9 Denominaci�n
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscarPortal.caption = Ador(0).Value
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Deactivate()
'    On Error GoTo Error
'
'    If bNoDescargar = False Then
'        Unload Me
'    Else
'        bNoDescargar = False
'    End If
'    Exit Sub
'
'Error:
'    If Err.Number = 365 Then
'        Resume Next
'    End If
End Sub

Private Sub Form_Load()
 
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me

    Accion = ACCSelProveCon
    
    sProveCod = ""
    sEqpCod = ""
    sComCod = ""
    
    If bAnya = True Then
        If basOptimizacion.gTipoDeUsuario = comprador Then
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVERestMatComp)) Is Nothing) Then
                bRMatProve = True
            End If
            
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorEQPRestEquipo)) Is Nothing) Then
                bREqpProve = True
            End If
        End If
    Else
        bRMatProve = True
        If gParametrosGenerales.gbOblProveEqp Then
            bREqpProve = True
        End If
        If ((basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestAsigEqp)) Is Nothing)) Then
            bVerAsigEqp = True
        End If
        
        If ((basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVERestAsigComp)) Is Nothing)) Then
            bVerAsigComp = True
        End If
    End If

    If frmSELPROVE.bVerAsigComp Then
        sdbcCompCod.Visible = False
        sdbcCompDen.Visible = False
        lblCom.Visible = True
        lblCom.caption = basOptimizacion.gCodCompradorUsuario & " - " & oUsuarioSummit.comprador.Apel
        sComCod = basOptimizacion.gCodCompradorUsuario
        sEqpCod = basOptimizacion.gCodEqpUsuario
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " - " & oUsuarioSummit.comprador.DenEqp
        sEqpCod = basOptimizacion.gCodEqpUsuario
    
        Set oProves = Nothing
        Set oProves = oFSGSRaiz.generar_CProveedores

    Else

        If frmSELPROVE.bVerAsigEqp Then
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            lblEqp.Visible = True
            lblEqp.caption = basOptimizacion.gCodEqpUsuario & " - " & oUsuarioSummit.comprador.DenEqp
            sEqpCod = basOptimizacion.gCodEqpUsuario
            Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
            oEqpSeleccionado.Cod = sEqpCod
 
            Set oProves = Nothing
            Set oProves = oFSGSRaiz.generar_CProveedores
            
         Else
 
            Set oProves = Nothing
            Set oProves = oFSGSRaiz.generar_CProveedores
        
            Set oEqps = Nothing
            Set oEqps = oFSGSRaiz.Generar_CEquipos
        
            Set oComps = Nothing
            Set oComps = oFSGSRaiz.generar_CCompradores
         
            sdbcEqpCod.Visible = True
            sdbcEqpDen.Visible = True
            sdbcCompCod.Visible = True
            sdbcCompDen.Visible = True
                
        End If
    End If
    
    If oUsuarioSummit.Tipo <> TIpoDeUsuario.Administrador Then
    
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPortalConsultar)) Is Nothing Then
            cmdBuscarPortal.Visible = False
        End If
        
    End If
    
    If basParametros.gParametrosGenerales.giINSTWEB <> ConPortal Then
        cmdBuscarPortal.Visible = False
    End If

    
End Sub

Private Sub Form_Resize()

    cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 195
    cmdCancelar.Left = Me.Width / 2 + 195
End Sub

Private Sub Form_Unload(Cancel As Integer)
    bNoDescargar = False
    
    Set oEqpSeleccionado = Nothing
    Set oProves = Nothing
    Set oEqps = Nothing
    Set oComps = Nothing
    
    frmSELPROVE.DesbloquearProceso 1

    frmSELPROVE.Accion = ACCSelProveCon

    Set oActsN1 = Nothing
        
End Sub



Private Sub sdbcCompCod_Click()
    If Not sdbcCompCod.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
        sComCod = ""
    End If
End Sub

Private Sub sdbcCompDen_Click()
    If Not sdbcCompDen.DroppedDown Then
        sdbcCompCod = ""
        sdbcCompDen = ""
        sComCod = ""
    End If
End Sub

Private Sub sdbcEqpCod_Click()
    If Not sdbcEqpCod.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
        sEqpCod = ""
        sComCod = ""
    End If
End Sub

Private Sub sdbcEqpDen_Click()
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
        sEqpCod = ""
        sComCod = ""
    End If
End Sub

Private Sub sdbcProveCod_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcProveDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesde = True
        
    End If
    
End Sub


Private Sub sdbcProveCod_Click()
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
        sProveCod = ""
    End If
End Sub

Private Sub sdbcProveCod_CloseUp()
        
   
    If sdbcProveCod.Value = "" Or sdbcProveCod.Value = "..." Then
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Value
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Value
    RespetarCombo = False
    
    
    
End Sub

Private Sub sdbcProveCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

Screen.MousePointer = vbHourglass

sdbcProveCod.RemoveAll

If oProves Is Nothing Then
    Set oProves = oFSGSRaiz.generar_CProveedores
    
End If
If bAnya Then
    'Restricci�n de equipo  en proveedores
    If bREqpProve And Not bRMatProve Then
        oProves.BuscarProveedoresConEqpDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, sdbcProveCod.Text
    End If
    
    'Restricci�n de equipo y material en proveedores
    If bREqpProve And bRMatProve Then
        oProves.BuscarProveedoresConEqpYMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
    End If
    
    'Restricci�n de material en proveedores
    If bRMatProve And Not bREqpProve Then
        oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
    End If
    
    'Sin restricciones
    If Not bRMatProve And Not bREqpProve Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text)
    End If
Else
    oProves.BuscarProveedoresConGMNdelProceso gParametrosInstalacion.giCargaMaximaCombos, _
                                              basOptimizacion.gCodEqpUsuario, _
                                              basOptimizacion.gCodCompradorUsuario, bRMatProve, bREqpProve, _
                                              CInt(frmSELPROVE.sdbcAnyo), frmSELPROVE.sdbcGMN1_4Cod, CLng(frmSELPROVE.sdbcProceCod), _
                                              sdbcProveCod.Text
End If

Codigos = oProves.DevolverLosCodigos

For i = 0 To UBound(Codigos.Cod) - 1
    sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
Next

If Not oProves.EOF Then
    sdbcProveCod.AddItem "..." & Chr(m_lSeparador) & "....."
End If

sdbcProveCod.SelStart = 0
sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
sdbcProveCod.Refresh

Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = vbKeyDelete Then
        sdbcProveCod.DroppedDown = False
        sdbcProveCod.Text = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.DroppedDown = False
        sdbcProveDen.Text = ""
        sdbcProveDen.RemoveAll
        sProveCod = ""
    End If
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
    
    If Trim(sdbcProveCod.Value) = "" Then
        sProveCod = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = sdbcProveCod.Columns(0).Value Then
        sProveCod = sdbcProveCod
        RespetarCombo = True
        sdbcProveDen = sdbcProveCod.Columns(1).Value
        RespetarCombo = True
        Exit Sub
    End If
    
    If sdbcProveCod.Value = sdbcProveDen.Columns(1).Value Then
        sProveCod = sdbcProveCod
        RespetarCombo = True
        sdbcProveDen = sdbcProveDen.Columns(0).Value
        RespetarCombo = True
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    If bAnya Then
        'Restricci�n de equipo  en proveedores
        If bREqpProve And Not bRMatProve Then
            oProves.BuscarProveedoresConEqpDelCompDesde 1, basOptimizacion.gCodEqpUsuario, sdbcProveCod.Text
        End If
        
        'Restricci�n de equipo y material en proveedores
        If bREqpProve And bRMatProve Then
            oProves.BuscarProveedoresConEqpYMatDelCompDesde 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
        End If
        
        'Restricci�n de material en proveedores
        If bRMatProve And Not bREqpProve Then
            oProves.BuscarProveedoresConMatDelCompDesde 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
        End If
        
        'Sin restricciones
        If Not bRMatProve And Not bREqpProve Then
            oProves.BuscarTodosLosProveedoresDesde 1, , Trim(sdbcProveCod.Text)
        End If
    Else
        oProves.BuscarProveedoresConGMNdelProceso gParametrosInstalacion.giCargaMaximaCombos, _
                                              basOptimizacion.gCodEqpUsuario, _
                                              basOptimizacion.gCodCompradorUsuario, bRMatProve, bREqpProve, _
                                              CInt(frmSELPROVE.sdbcAnyo), frmSELPROVE.sdbcGMN1_4Cod, CLng(frmSELPROVE.sdbcProceCod), _
                                              sdbcProveCod.Text
    End If
    Screen.MousePointer = vbNormal
    
    If oProves.Count = 0 Then
        oMensajes.NoValido sIdioma(1)
        sdbcProveCod = ""
        Exit Sub
    Else
        If UCase(oProves.Item(1).Cod) <> UCase(sdbcProveCod) Then
            oMensajes.NoValido sIdioma(1)
            sdbcProveCod = ""
            Exit Sub
        Else
            RespetarCombo = True
            sdbcProveCod = oProves.Item(1).Cod
            sdbcProveDen = oProves.Item(1).Den
            RespetarCombo = False
            sProveCod = sdbcProveCod
        End If
    End If
    
End Sub

Private Sub sdbcProveDen_Change()
    
     If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcProveCod.Text = ""
        RespetarCombo = False
        
        CargarComboDesde = True
        sdbcProveCod = ""
    
    End If
End Sub

Private Sub sdbcProveDen_Click()
    
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
    
End Sub

Private Sub sdbcProveDen_CloseUp()

    
    If sdbcProveDen.Value = "" Or sdbcProveDen.Value = "....." Then
         sProveCod = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarCombo = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Value
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Value
    sProveCod = sdbcProveCod
    RespetarCombo = False
    
    
End Sub

Private Sub sdbcProveDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

Screen.MousePointer = vbHourglass

sdbcProveDen.RemoveAll

If oProves Is Nothing Then
    Set oProves = oFSGSRaiz.generar_CProveedores
   
End If
If bAnya Then
    'Restricci�n de equipo  en proveedores
    If bREqpProve And Not bRMatProve Then
        oProves.BuscarProveedoresConEqpDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, , Trim(sdbcProveDen), , , , , , , , True
    End If
    
    'Restricci�n de equipo y material en proveedores
    If bREqpProve And bRMatProve Then
        oProves.BuscarProveedoresConEqpYMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen), True
    End If
    
    'Restricci�n de material en proveedores
    If bRMatProve And Not bREqpProve Then
        oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen), , , , , , , True
    End If
    
    'Sin restricciones
    If Not bRMatProve And Not bREqpProve Then
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , Trim(sdbcProveDen), , , , , , , , , , , True
    End If
Else
    oProves.BuscarProveedoresConGMNdelProceso gParametrosInstalacion.giCargaMaximaCombos, _
                                              basOptimizacion.gCodEqpUsuario, _
                                              basOptimizacion.gCodCompradorUsuario, bRMatProve, bREqpProve, _
                                              CInt(frmSELPROVE.sdbcAnyo), frmSELPROVE.sdbcGMN1_4Cod, CLng(frmSELPROVE.sdbcProceCod), _
                                              , sdbcProveDen.Text, True
End If


Codigos = oProves.DevolverLosCodigos

For i = 0 To UBound(Codigos.Cod) - 1
   
    sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
Next

If Not oProves.EOF Then
    sdbcProveDen.AddItem "..." & Chr(m_lSeparador) & "....."
End If

Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpCod_Change()
     
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sComCod = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        RespetarCombo = False
        
        CargarComboDesdeEqp = True
            
        
    End If
    
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpCod.Rows - 1
            bm = sdbcEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then
        sEqpCod = ""
        sComCod = ""
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el equipo
    
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sEqpCod = ""
        sComCod = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        oMensajes.NoValido sIdioma(2)
    Else
        RespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        sEqpCod = oEquipos.Item(1).Cod
        sComCod = ""
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing

End Sub

Private Sub sdbcEqpDen_Change()
      
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcEqpCod.Text = ""
        sEqpCod = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sComCod = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll

        RespetarCombo = False
        
        CargarComboDesdeEqp = True
        Set oEqpSeleccionado = Nothing
            
        
    End If
          
End Sub
Private Sub sdbcEqpDen_CloseUp()
    
    If sdbcEqpDen.Value = "..." Or sdbcEqpDen = "" Then
        sdbcEqpDen.Text = ""
        sEqpCod = ""
        sComCod = ""
        Exit Sub
    End If
    
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sComCod = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    RespetarCombo = False
    
    sEqpCod = sdbcEqpCod
        
    CargarComboDesdeEqp = False
    
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    sdbcEqpDen.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpDen.Rows - 1
            bm = sdbcEqpDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
        sdbcCompCod = ""
        sdbcCompDen = ""
        sComCod = ""
        sEqpCod = ""
        sdbcCompCod.RemoveAll
        sdbcCompDen.RemoveAll
        oMensajes.NoValido sIdioma(2)
    Else
        RespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        sEqpCod = sdbcEqpCod.Text
        sComCod = ""
        RespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        CargarComboDesdeEqp = False
    End If
    
    Set oEquipos = Nothing

End Sub


Private Sub sdbcEqpCod_CloseUp()
    
    If sdbcEqpCod.Value = "..." Or sdbcEqpCod.Value = "" Then
        sEqpCod = ""
        sComCod = ""
        Exit Sub
    End If
    
    
    sdbcCompCod = ""
    sdbcCompDen = ""
    sComCod = ""
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
        
    RespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    RespetarCombo = False
    
    sEqpCod = sdbcEqpCod
        
    CargarComboDesdeEqp = False

End Sub
Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    
    sdbcEqpCod.RemoveAll
    
    If CargarComboDesdeEqp Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeEqp And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpCod_InitColumnProps()

    sdbcEqpCod.DataFieldList = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcCompCod_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompDen.Text = ""
        RespetarCombo = False
        
        CargarComboDesdeComp = True
        sComCod = ""
        
    End If
    
End Sub


Private Sub sdbcCompCod_CloseUp()
    
    
    If sdbcCompCod.Value = "..." Or sdbcCompCod.Value = "" Then
        sComCod = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompCod.Columns(1).Text
    sdbcCompCod.Text = sdbcCompCod.Columns(0).Text
    RespetarCombo = False
    sComCod = sdbcCompCod
    CargarComboDesdeComp = False
    
    
End Sub

Private Sub sdbcCompCod_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    sdbcCompCod.RemoveAll
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
        
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcCompCod.Text), , , False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, False, False
    End If
    
    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
        
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcCompCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompCod.AddItem "..."
    End If

    sdbcCompCod.SelStart = 0
    sdbcCompCod.SelLength = Len(sdbcCompCod.Text)
    sdbcCompCod.Refresh

    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbcCompCod_InitColumnProps()
    sdbcCompCod.DataFieldList = "Column 0"
    sdbcCompCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcCompCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompCod.Rows - 1
            bm = sdbcCompCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcCompCod_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    If sdbcCompCod.Text = "" Then
        sComCod = ""
        Exit Sub
    End If
    ''' Solo continuamos si existe el equipo
    If oEqpSeleccionado Is Nothing Then
        sdbcCompCod.Text = ""
        sComCod = ""
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    oEqpSeleccionado.CargarTodosLosCompradores sdbcCompCod.Text, , , True, , , False
    Set oCompradores = oEqpSeleccionado.Compradores
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompCod.Text = ""
        sComCod = ""
        oMensajes.NoValido sIdioma(3)
    Else
        RespetarCombo = True
        sdbcCompCod.Text = oCompradores.Item(1).Cod
        sdbcCompDen.Text = oCompradores.Item(1).Apel & ", " & oCompradores.Item(1).nombre
        sComCod = oCompradores.Item(1).Cod
        RespetarCombo = False
        CargarComboDesdeComp = False
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing

End Sub

Private Sub sdbcCompDen_Change()
    
    If Not RespetarCombo Then
    
        RespetarCombo = True
        sdbcCompCod.Text = ""
        RespetarCombo = False
        CargarComboDesdeComp = True
        sComCod = ""
            
    End If
    
End Sub


Private Sub sdbcCompDen_CloseUp()
    Dim sCod As String
    
    If sdbcCompDen.Value = "..." Or sdbcCompDen.Value = "" Then
        sComCod = ""
        Exit Sub
    End If
    
    RespetarCombo = True
    sdbcCompDen.Text = sdbcCompDen.Columns(0).Text
    sdbcCompCod.Text = sdbcCompDen.Columns(1).Text
    RespetarCombo = False
    
    sCod = oEqpSeleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodeqp - Len(oEqpSeleccionado.Cod))
    sComCod = sdbcCompCod
    CargarComboDesdeComp = False
    
       
End Sub

Private Sub sdbcCompDen_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    sdbcCompDen.RemoveAll
    
    If oEqpSeleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If CargarComboDesdeComp Then
        oEqpSeleccionado.CargarTodosLosCompradoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(Apellidos(sdbcEqpDen.Text)), True, False, True
    Else
        oEqpSeleccionado.CargarTodosLosCompradores , , , False, False, True, False
    End If

    Set oComps = oEqpSeleccionado.Compradores
    Codigos = oComps.DevolverLosCodigos
    Set oEqpSeleccionado.Compradores = Nothing
    
    For i = 0 To UBound(Codigos.Cod) - 1
            sdbcCompDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If CargarComboDesdeComp And Not oComps.EOF Then
        sdbcCompDen.AddItem "..."
    End If

    sdbcCompDen.SelStart = 0
    sdbcCompDen.SelLength = Len(sdbcCompDen.Text)
    sdbcCompDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcCompDen_InitColumnProps()
    sdbcCompDen.DataFieldList = "Column 0"
    sdbcCompDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcCompDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcCompDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcCompDen.Rows - 1
            bm = sdbcCompDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcCompDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcCompDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbcCompDen_Validate(Cancel As Boolean)

    Dim oCompradores As CCompradores
    Dim bExiste As Boolean
    
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If sdbcCompDen.Text = "" Then
        sComCod = ""
        Exit Sub
    End If
    
    
    ''' Solo continuamos si existe el equipo
    If oEqpSeleccionado Is Nothing Then
        sdbcCompDen.Text = ""
        sComCod = ""
        Exit Sub
    End If

    
    Screen.MousePointer = vbHourglass
    oEqpSeleccionado.CargarTodosLosCompradores , nombre(sdbcCompDen.Text), Apellidos(sdbcCompDen.Text), True, False, False, False
    Set oCompradores = oEqpSeleccionado.Compradores
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oCompradores.Count = 0)
    
    If Not bExiste Then
        sdbcCompDen.Text = ""
        sComCod = ""
        oMensajes.NoValido sIdioma(3)
        
    Else
        RespetarCombo = True
        sdbcCompCod.Text = oCompradores.Item(1).Cod
        sdbcCompDen.Text = oCompradores.Item(1).Apel & ", " & oCompradores.Item(1).nombre
        sComCod = oCompradores.Item(1).Cod
        RespetarCombo = False
        CargarComboDesdeComp = False
    
    End If
    
    Set oEqpSeleccionado.Compradores = Nothing
    Set oCompradores = Nothing

End Sub

Public Sub CargarProveedorConBusqueda()
Dim oProve As CProveedor

    Set oProve = frmPROVEBuscar.oProveEncontrados.Item(1)
    RespetarCombo = True
    sdbcProveCod = oProve.Cod
    sdbcProveDen = oProve.Den
    sProveCod = sdbcProveCod
    RespetarCombo = False
    Set oProve = Nothing
    
End Sub


