VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmParContr 
   Caption         =   "Tipos de contratos"
   ClientHeight    =   3795
   ClientLeft      =   240
   ClientTop       =   1545
   ClientWidth     =   10650
   Icon            =   "frmParContr.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3795
   ScaleWidth      =   10650
   StartUpPosition =   1  'CenterOwner
   Begin SSDataWidgets_B.SSDBDropDown sdbddIdioma 
      Height          =   525
      Left            =   360
      TabIndex        =   6
      Top             =   720
      Width           =   705
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmParContr.frx":014A
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      Columns(0).Width=   1191
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1244
      _ExtentY        =   926
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgContratos 
      Height          =   3315
      Left            =   60
      TabIndex        =   5
      Top             =   30
      Width           =   10560
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   4
      stylesets.count =   2
      stylesets(0).Name=   "BOT"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmParContr.frx":0166
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmParContr.frx":04B8
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      BalloonHelp     =   0   'False
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2143
      Columns(0).Caption=   "Idioma"
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   3
      Columns(1).Width=   14684
      Columns(1).Caption=   "Plantilla"
      Columns(1).Name =   "PLAN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   714
      Columns(2).Name =   "BOT"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).Style=   4
      Columns(2).ButtonsAlways=   -1  'True
      Columns(2).StyleSet=   "BOT"
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   18627
      _ExtentY        =   5847
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   10650
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   3240
      Width           =   10650
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2370
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   9600
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   210
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdAnyadir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin MSComDlg.CommonDialog cdlArchivo 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Plantillas de Word (*.dot)|*.dot"
   End
End
Attribute VB_Name = "frmParContr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'--------------------------------------------------------------------------------
''' *** Formulario: frmPARContr Plantillas de contratos para solicitudes de tipo contrato (CM)
''' *** Creacion: 26/11/2002 (EPB)
''' *** Modificaci�n:  Lo ha cambiado el equipo de mmv desde la versi�n 31900.2 que incluy� contratos en FSNWEB

Option Explicit
Public g_oSolicitud As CSolicitud
Public g_oSolicitPlantilla As CSolicitPlantilla
Public g_oSolicitPlantillas As CSolicitPlantillas
Private m_oIdiomas As CIdiomas
''' Pago en edicion
Private m_oSolicitPlantillaEnEdicion As CSolicitPlantilla
Private m_oIBAseDatosEnEdicion As IBaseDatos

''' Control de errores

Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bValError As Boolean

Public g_udtAccion As accionessummit
Private m_bModoEdicion As Boolean
Private m_bRespetarChange As Boolean
'Multilenguaje
Private m_sIdiTitulos(1 To 5) As String
Private m_sIdiCodigo As String
Private m_sIdiConsulta As String
Private m_sIdiEdicion As String
Private m_sIdiDenominacion As String
Private m_sIdiOrdenando As String
Private m_sIdiPlantilla(1 To 3) As String


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim sTitulo As String

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PARCONTR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Ador.MoveNext
    Ador.MoveNext
    m_sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    m_sIdiDenominacion = Ador(0).Value
    Ador.MoveNext
    cmdAnyadir.caption = Ador(0).Value '5
    Ador.MoveNext
    Ador.MoveNext
    cmdDeshacer.caption = Ador(0).Value
    Ador.MoveNext
    cmdEliminar.caption = Ador(0).Value
    Ador.MoveNext
    Ador.MoveNext
    cmdModoEdicion.caption = Ador(0).Value '10
    Ador.MoveNext
    cmdRestaurar.caption = Ador(0).Value
    Ador.MoveNext
    Ador.MoveNext
    m_sIdiConsulta = Ador(0).Value
    Ador.MoveNext
    m_sIdiEdicion = Ador(0).Value
    Ador.MoveNext
    m_sIdiTitulos(4) = Ador(0).Value '15 'comentar
    Ador.MoveNext
    m_sIdiOrdenando = Ador(0).Value
    Ador.MoveNext
    m_sIdiTitulos(5) = Ador(0).Value 'comentar
    Ador.MoveNext
    sdbgContratos.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgContratos.Columns(1).caption = Ador(0).Value
    m_sIdiPlantilla(1) = Ador(0).Value
    Ador.MoveNext
    m_sIdiPlantilla(2) = Ador(0).Value '20
    Ador.MoveNext
    m_sIdiPlantilla(3) = Ador(0).Value

    Ador.MoveNext
    sTitulo = Ador(0).Value '22 Titulo
    Ador.MoveNext
    m_sIdiTitulos(1) = sTitulo & " " & Ador(0).Value '23 (Consulta)
    caption = m_sIdiTitulos(1)
    Ador.MoveNext
    m_sIdiTitulos(2) = sTitulo & " " & Ador(0).Value   '24 (Edici�n)
    
    Ador.Close
    
    End If

    Set Ador = Nothing

End Sub

Private Sub cmdAnyadir_Click()
    
    sdbgContratos.Scroll 0, sdbgContratos.Rows - sdbgContratos.Row
    
    If sdbgContratos.VisibleRows > 0 Then
        
        If sdbgContratos.VisibleRows >= sdbgContratos.Rows Then
            sdbgContratos.Row = sdbgContratos.Rows
        Else
            sdbgContratos.Row = sdbgContratos.Rows - (sdbgContratos.Rows - sdbgContratos.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgContratos.SetFocus

End Sub

Private Sub cmdModoEdicion_Click()

If Not m_bModoEdicion Then
    Me.caption = m_sIdiTitulos(2)
    m_bModoEdicion = True
    cmdModoEdicion.caption = m_sIdiConsulta
    cmdAnyadir.Visible = True
    cmdEliminar.Visible = True
    cmdDeshacer.Visible = True
    cmdRestaurar.Visible = False
    sdbddIdioma.Enabled = True
    sdbgContratos.Columns(0).Locked = False
    sdbgContratos.Columns(1).Locked = False
    sdbgContratos.Columns(2).Locked = False
    sdbgContratos.AllowAddNew = True
    Dim i As Integer
        For i = sdbgContratos.Row To sdbgContratos.Rows
            sdbgContratos.Columns(2).CellStyleSet "BOT", i
        Next i

Else
   If sdbgContratos.DataChanged = True Then
       If (actualizarYSalir() = True) Then
            Exit Sub
       End If
    End If
    m_bModoEdicion = False
    Me.caption = m_sIdiTitulos(1)
    cmdModoEdicion.caption = m_sIdiEdicion
    cmdAnyadir.Visible = False
    cmdEliminar.Visible = False
    cmdDeshacer.Visible = False
    cmdRestaurar.Visible = True
    sdbddIdioma.Enabled = False
    sdbgContratos.Columns(0).Locked = True
    sdbgContratos.Columns(1).Locked = True
    sdbgContratos.Columns(2).Locked = True
    sdbgContratos.AllowAddNew = False
    
End If
End Sub

Private Sub Form_Load()
Dim oIdioma As CIdioma

    Me.Width = 10770
    Me.Height = 4200

    m_bModoEdicion = False
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sdbgContratos.Columns("IDI").DropDownHwnd = sdbddIdioma.hWnd
    cdlArchivo.FLAGS = cdlOFNFileMustExist & cdlOFNHideReadOnly

    'Carga el combo de los idiomas
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas

    sdbddIdioma.RemoveAll
    For Each oIdioma In m_oIdiomas
        sdbddIdioma.AddItem oIdioma.Cod
    Next

    sdbddIdioma.Enabled = False

    g_udtAccion = ACCTipoContrCon
    
    Set g_oSolicitPlantillas = oFSGSRaiz.Generar_CSolicitPlantillas
    g_oSolicitPlantillas.CargarTodasLasPlantillas g_oSolicitud.Id
       
    
    'Nuevo: Cargar todas las plantillas para ese tipo de solicitud contrato
    CargarGrid
    
End Sub

Private Sub Form_Resize()

If Me.Height < 1000 Then Exit Sub
If Me.Width < 1000 Then Exit Sub
sdbgContratos.Height = Me.Height - 885
sdbgContratos.Width = Me.Width - 210
cmdModoEdicion.Left = sdbgContratos.Width + sdbgContratos.Left - cmdModoEdicion.Width
'sdbgContratos.Columns(0).Width = (sdbgContratos.Width - 240) * 0.15
'sdbgContratos.Columns(1).Width = (sdbgContratos.Width - 240) * 0.3
'sdbgContratos.Columns(2).Width = (sdbgContratos.Width - 240) * 0.1
'sdbgContratos.Columns(3).Width = (sdbgContratos.Width - 240) * 0.39
sdbddIdioma.Width = sdbgContratos.Columns(0).Width
sdbddIdioma.Columns(0).Width = sdbddIdioma.Width
End Sub

Private Sub sdbddIdioma_CloseUp()
    Dim i As Integer
    Dim arRray As Variant
    Dim bEncontrado As Boolean
    Dim bUpdate As Boolean
    
    m_bRespetarChange = True
    bEncontrado = False
    If Not sdbgContratos.IsAddRow Then
    
        If Me.Visible Then sdbgContratos.SetFocus
    Else
        sdbgContratos.Columns(2).CellStyleSet "BOT", sdbgContratos.Rows
    End If
    m_bRespetarChange = False
End Sub

Private Sub sdbddIdioma_DropDown()
m_bRespetarChange = True
If sdbgContratos.DataChanged = True Then 'And Not sdbgContratos.IsAddRow Then
    sdbgContratos.Update
End If
m_bRespetarChange = False
End Sub

Private Sub sdbddIdioma_InitColumnProps()
    sdbddIdioma.DataFieldList = "Column 0"
    sdbddIdioma.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddIdioma_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddIdioma.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddIdioma.Rows - 1
            bm = sdbddIdioma.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddIdioma.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgContratos.Columns(sdbgContratos.Col).Value = Mid(sdbddIdioma.Columns(0).CellText(bm), 1, Len(Text))
                sdbddIdioma.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddIdioma_ValidateList(Text As String, RtnPassed As Integer)
Dim oIdioma As CIdioma
Dim bEncontrado As Boolean

    If Text = "..." Then RtnPassed = False

    ''' Si es nulo, correcto
        
    If Text = "" Then

        RtnPassed = False

    Else
        bEncontrado = False
        ''' Comprobar la existencia en la lista
        For Each oIdioma In m_oIdiomas
             If UCase(sdbgContratos.Columns(0).Text) = UCase(oIdioma.Cod) Then
                sdbgContratos.Columns(0).Text = oIdioma.Cod
                bEncontrado = True
                Exit For
            End If
        Next
        
        If bEncontrado Then
            RtnPassed = True
        Else
            oMensajes.NoValido sdbgContratos.Columns(0).caption
            RtnPassed = False
            sdbgContratos.Columns(0).Value = ""
        End If
            
    End If

End Sub

Private Sub sdbgContratos_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    RtnDispErrMsg = 0
        
    If IsEmpty(sdbgContratos.RowBookmark(sdbgContratos.Row)) Then
        sdbgContratos.Bookmark = sdbgContratos.RowBookmark(sdbgContratos.Row - 1)
    Else
        sdbgContratos.Bookmark = sdbgContratos.RowBookmark(sdbgContratos.Row)
    End If
    g_udtAccion = ACCTipoContrCon

End Sub

Private Sub sdbgContratos_AfterInsert(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If g_udtAccion <> ACCTipoContrAnya Then Exit Sub
    
    If m_bAnyaError = False Then
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        g_udtAccion = ACCTipoContrCon
    End If
End Sub

Private Sub sdbgContratos_AfterUpdate(RtnDispErrMsg As Integer)
    
    RtnDispErrMsg = 0
    
    
End Sub

Private Sub sdbgContratos_BeforeInsert(Cancel As Integer)
    sdbgContratos.Columns(2).CellStyleSet "BOT", sdbgContratos.Row
End Sub

Private Sub sdbgContratos_BtnClick()


On Error GoTo Error

    If Not m_bModoEdicion Then Exit Sub
    If sdbgContratos.Col = 2 Then
        cdlArchivo.DialogTitle = m_sIdiPlantilla(3)
        cdlArchivo.filename = ""
        cdlArchivo.Filter = m_sIdiPlantilla(2) & " (*.dot)|*.dot"
        cdlArchivo.DefaultExt = "dot"
        cdlArchivo.Action = 1
        If cdlArchivo.filename = "" Then Exit Sub
        If sdbgContratos.Columns(1).Value <> cdlArchivo.filename Then
            sdbgContratos.Columns(1).Value = cdlArchivo.filename
            If g_udtAccion = ACCTipoContrCon Then
                sdbgContratos_Change
            End If
        End If
        Exit Sub
    End If
    
Error:
    If err.Number = cdlCancel Then
        Exit Sub
    Else
        Resume Next
    End If

End Sub

Private Sub sdbgContratos_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
    For i = 0 To Me.sdbgContratos.Rows
        sdbgContratos.Columns(2).CellStyleSet "BOT"
    Next i
End Sub

Private Sub cmdDeshacer_Click()
    
    sdbgContratos.Columns(2).CellStyleSet "", sdbgContratos.Rows
    
    sdbgContratos.CancelUpdate
    sdbgContratos.DataChanged = False
    
    Set m_oIBAseDatosEnEdicion = Nothing
    Set m_oSolicitPlantillaEnEdicion = Nothing
    
    cmdAnyadir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
        
    g_udtAccion = ACCTipoContrCon
End Sub
Private Sub cmdEliminar_Click()
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
    
    If sdbgContratos.Rows = 0 Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    Select Case sdbgContratos.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
            irespuesta = oMensajes.PreguntaEliminar(m_sIdiTitulos(4) & ": " & sdbgContratos.Columns(0).Value & " - " & sdbgContratos.Columns(1).Value)
        Case Is > 1
            irespuesta = oMensajes.PreguntaEliminar(m_sIdiTitulos(5))
    End Select
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    ReDim aIdentificadores(sdbgContratos.SelBookmarks.Count)
    ReDim aBookmarks(sdbgContratos.SelBookmarks.Count)
        
    i = 0
    While i < sdbgContratos.SelBookmarks.Count
        aIdentificadores(i + 1) = sdbgContratos.Columns(3).CellValue(sdbgContratos.SelBookmarks(i)) 'Codigo
        aBookmarks(i + 1) = sdbgContratos.SelBookmarks(i)
        'sdbgContratos.Bookmark = sdbgContratos.SelBookmarks(i)
        i = i + 1
    Wend
        
    udtTeserror = g_oSolicitPlantillas.EliminarDeBaseDatos(aIdentificadores)
    If udtTeserror.NumError <> TESnoerror Then
        TratarError udtTeserror
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        For i = 0 To sdbgContratos.SelBookmarks.Count - 1
            g_oSolicitPlantillas.Remove sdbgContratos.Columns(3).CellValue(sdbgContratos.SelBookmarks(i))
        Next
        sdbgContratos.DeleteSelected
        DoEvents
    End If

    sdbgContratos.SelBookmarks.RemoveAll
    
    If sdbgContratos.Rows = 0 Then 'si se ha quedado sin plantillas, quitamos los tres puntos
        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_PLANTILLAS").Value = 0
        frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("CONTRATOS").Value = ""
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error.
    If sdbgContratos.DataChanged = True Then
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
        If sdbgContratos.Row = 0 Then
            sdbgContratos.MoveNext
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgContratos.MovePrevious
            End If
        Else
            sdbgContratos.MovePrevious
            DoEvents
            If m_bValError Or m_bAnyaError Or m_bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgContratos.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

Private Sub cmdRestaurar_Click()


    Screen.MousePointer = vbHourglass
    
    If m_bModoEdicion Then Exit Sub
    
    Set g_oSolicitPlantillas = oFSGSRaiz.Generar_CSolicitPlantillas
    g_oSolicitPlantillas.CargarTodasLasPlantillas g_oSolicitud.Id
    
    CargarGrid
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Unload(Cancel As Integer)

    Dim v As Variant

    If sdbgContratos.DataChanged = True Then
        If (actualizarYSalir() = True) Then
            Cancel = True
            Exit Sub
        End If
    End If

    Set m_oIdiomas = Nothing
    Set m_oSolicitPlantillaEnEdicion = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    Me.Visible = False
    
End Sub

Private Sub sdbgContratos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
    
End Sub
Private Sub sdbgContratos_BeforeUpdate(Cancel As Integer)
Dim bEncontrado As Boolean
Dim i As Integer
Dim teserror As TipoErrorSummit
Dim arRray As Variant

    ''' * Objetivo: Validar los datos
    Cancel = False
    m_bAnyaError = False
    m_bModError = False
    m_bValError = False

    If g_udtAccion <> ACCTipoContrCon Then
    
        If Trim(sdbgContratos.Columns(0).Value) = "" Then
            oMensajes.NoValida sdbgContratos.Columns(0).caption
            Cancel = True
            GoTo Salir
        End If
        
        If Trim(sdbgContratos.Columns(1).Value) = "" Then
            oMensajes.NoValida m_sIdiPlantilla(1)
            Cancel = True
            GoTo Salir
        End If
        
        bEncontrado = False
        '1� vamos a comprobar que no existe ya el idioma
        For i = 1 To g_oSolicitPlantillas.Count - 1
            If i <> sdbgContratos.Row + 1 Then
                If g_oSolicitPlantillas.Item(i).idioma = sdbgContratos.Columns(0).Value Then
                    bEncontrado = True
                    Exit For
                End If
            End If
        Next
        If bEncontrado Then
            oMensajes.NoValida sdbgContratos.Columns(0).caption
            Cancel = True
            GoTo Salir
        End If
            
        If sdbgContratos.IsAddRow Then
        
            Set m_oSolicitPlantillaEnEdicion = oFSGSRaiz.Generar_CSolicitPlantilla
            m_oSolicitPlantillaEnEdicion.idioma = sdbgContratos.Columns(0).Value
            m_oSolicitPlantillaEnEdicion.Ruta = sdbgContratos.Columns(1).Value
            m_oSolicitPlantillaEnEdicion.Solicitud = g_oSolicitud.Id
            
            Set m_oIBAseDatosEnEdicion = m_oSolicitPlantillaEnEdicion
            teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
            If teserror.NumError <> TESnoerror Then
                m_bAnyaError = True
                Cancel = True
                basErrores.TratarError teserror
                If Me.Visible Then sdbgContratos.SetFocus
                Exit Sub
            End If
            
            sdbgContratos.Columns(3).Value = m_oSolicitPlantillaEnEdicion.Id
            g_oSolicitPlantillas.Add m_oSolicitPlantillaEnEdicion.Id, g_oSolicitud.Id, m_oSolicitPlantillaEnEdicion.idioma, m_oSolicitPlantillaEnEdicion.Ruta
            RegistrarAccion ACCTipoContrAnya, " Idioma: " & m_oSolicitPlantillaEnEdicion.idioma & " Ruta: " & m_oSolicitPlantillaEnEdicion.Ruta
            
            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("NUM_PLANTILLAS").Value = sdbgContratos.Rows
            frmSOLConfiguracion.ssSOLConfig.ActiveRow.Cells("CONTRATOS").Value = "..."
       Else
            If m_oSolicitPlantillaEnEdicion Is Nothing Then
                sdbgContratos_Change
            End If
            m_oSolicitPlantillaEnEdicion.Ruta = sdbgContratos.Columns(1).Value
            m_oSolicitPlantillaEnEdicion.idioma = sdbgContratos.Columns(0).Value
            Set m_oIBAseDatosEnEdicion = m_oSolicitPlantillaEnEdicion
            teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                m_bModError = True
                Cancel = True
                If Me.Visible Then sdbgContratos.SetFocus
                Exit Sub
            End If
            RegistrarAccion ACCTipoContrMod, " Idioma: " & m_oSolicitPlantillaEnEdicion.idioma & " Ruta: " & m_oSolicitPlantillaEnEdicion.Ruta
            g_udtAccion = ACCTipoContrCon
        End If
    End If
Salir:
        
    m_bValError = Cancel
    If Me.Visible Then sdbgContratos.SetFocus
      
End Sub
Private Sub sdbgContratos_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    Dim teserror As TipoErrorSummit
    
    If m_bRespetarChange Then Exit Sub
    If Not m_bModoEdicion Then Exit Sub
    
    If cmdDeshacer.Enabled = False Then
    
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    End If
    
    If g_udtAccion = ACCTipoContrCon Then
         If Not sdbgContratos.IsAddRow Then
        
            Set m_oSolicitPlantillaEnEdicion = g_oSolicitPlantillas.Item(sdbgContratos.Columns(3).Value)
            Set m_oIBAseDatosEnEdicion = m_oSolicitPlantillaEnEdicion
            
            teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                If Me.Visible Then sdbgContratos.SetFocus
            Else
                g_udtAccion = ACCTipoContrMod
            End If
         Else
            g_udtAccion = ACCTipoContrAnya
         End If
    End If
End Sub

Private Sub CargarGrid()
    Dim i As Integer

    sdbgContratos.RemoveAll
        
    i = 1
    Do While i <= g_oSolicitPlantillas.Count
        sdbgContratos.AddItem g_oSolicitPlantillas.Item(i).idioma & Chr(m_lSeparador) & g_oSolicitPlantillas.Item(i).Ruta & Chr(m_lSeparador) & Chr(m_lSeparador) & g_oSolicitPlantillas.Item(i).Id
        i = i + 1
    Loop

End Sub


Private Sub sdbgContratos_KeyDown(KeyCode As Integer, Shift As Integer)

''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado
    If KeyCode = vbKeyDelete Then
        If m_bModoEdicion And sdbgContratos.SelBookmarks.Count > 0 Then
            cmdEliminar_Click
        End If
    End If
End Sub



Private Sub sdbgContratos_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgContratos.DataChanged = False Then
            sdbgContratos.Columns(2).CellStyleSet "", sdbgContratos.Rows
            sdbgContratos.CancelUpdate
            sdbgContratos.DataChanged = False
                       
            If Not sdbgContratos.IsAddRow Then
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
            Else
                cmdAnyadir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
            End If
            
            g_udtAccion = ACCTipoContrCon
            
        Else
            sdbgContratos.Columns(2).CellStyleSet "", sdbgContratos.Rows
            If sdbgContratos.IsAddRow Then
           
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
           
                g_udtAccion = ACCTipoContrCon
            
            End If
            
        End If
        
    End If
    
End Sub

Private Sub sdbgcontratos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgContratos.IsAddRow Then
        'sdbgContratos.Columns(0).Locked = True
        If sdbgContratos.DataChanged = False Then
            cmdAnyadir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
        
    Else
        If sdbgContratos.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgContratos.Row) Then
                sdbgContratos.Col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdAnyadir.Enabled = False
        cmdEliminar.Enabled = False
    End If
End Sub

