VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCatalogoAdjun 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   6075
   ClientLeft      =   1290
   ClientTop       =   2775
   ClientWidth     =   9735
   Icon            =   "frmCatalogoAdjun.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6075
   ScaleWidth      =   9735
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   6045
      Index           =   2
      Left            =   30
      TabIndex        =   12
      Top             =   -30
      Width           =   4455
      Begin VB.PictureBox Picture3 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   2010
         ScaleHeight     =   315
         ScaleWidth      =   2355
         TabIndex        =   29
         Top             =   5620
         Width           =   2355
         Begin VB.CommandButton cmdAbrirEsp 
            Height          =   300
            Index           =   2
            Left            =   1905
            Picture         =   "frmCatalogoAdjun.frx":0CB2
            Style           =   1  'Graphical
            TabIndex        =   34
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEsp 
            Height          =   300
            Index           =   2
            Left            =   1425
            Picture         =   "frmCatalogoAdjun.frx":0D2E
            Style           =   1  'Graphical
            TabIndex        =   33
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarEsp 
            Height          =   300
            Index           =   2
            Left            =   480
            Picture         =   "frmCatalogoAdjun.frx":0DAF
            Style           =   1  'Graphical
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirEsp 
            Height          =   300
            Index           =   2
            Left            =   0
            Picture         =   "frmCatalogoAdjun.frx":0E35
            Style           =   1  'Graphical
            TabIndex        =   31
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdModificarEsp 
            Height          =   300
            Index           =   2
            Left            =   960
            Picture         =   "frmCatalogoAdjun.frx":0E96
            Style           =   1  'Graphical
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   5025
         Index           =   2
         Left            =   60
         TabIndex        =   13
         Top             =   450
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   8864
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin VB.Label lblArticuloFich 
         BackColor       =   &H00808000&
         Caption         =   "Archivos adjuntos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   240
         Index           =   2
         Left            =   120
         TabIndex        =   16
         Top             =   180
         Width           =   2970
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   3000
      Index           =   1
      Left            =   4620
      TabIndex        =   6
      Top             =   3000
      Width           =   5025
      Begin VB.PictureBox Picture2 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   2580
         ScaleHeight     =   315
         ScaleWidth      =   2355
         TabIndex        =   23
         Top             =   2580
         Width           =   2355
         Begin VB.CommandButton cmdModificarEsp 
            Height          =   300
            Index           =   1
            Left            =   960
            Picture         =   "frmCatalogoAdjun.frx":0FE0
            Style           =   1  'Graphical
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirEsp 
            Height          =   300
            Index           =   1
            Left            =   0
            Picture         =   "frmCatalogoAdjun.frx":112A
            Style           =   1  'Graphical
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarEsp 
            Height          =   300
            Index           =   1
            Left            =   480
            Picture         =   "frmCatalogoAdjun.frx":118B
            Style           =   1  'Graphical
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEsp 
            Height          =   300
            Index           =   1
            Left            =   1425
            Picture         =   "frmCatalogoAdjun.frx":1211
            Style           =   1  'Graphical
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAbrirEsp 
            Height          =   300
            Index           =   1
            Left            =   1905
            Picture         =   "frmCatalogoAdjun.frx":1292
            Style           =   1  'Graphical
            TabIndex        =   24
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.CommandButton cmdPasarTodosALinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":130E
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   1710
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarTodosDesdeLinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":1667
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   1245
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarUnFichALinea 
         Height          =   315
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":19C1
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   825
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarUnFichDesdeLinea 
         Height          =   300
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":1D1A
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   480
         Width           =   495
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   2000
         Index           =   1
         Left            =   660
         TabIndex        =   7
         Top             =   450
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   3519
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Path"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin VB.Label lblArticuloFich 
         BackColor       =   &H00808000&
         Caption         =   "Archivos adjuntos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   240
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   210
         Width           =   4800
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   3000
      Index           =   0
      Left            =   4620
      TabIndex        =   0
      Top             =   -30
      Width           =   5025
      Begin VB.PictureBox Picture1 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   2580
         ScaleHeight     =   315
         ScaleWidth      =   2355
         TabIndex        =   17
         Top             =   2580
         Width           =   2355
         Begin VB.CommandButton cmdAbrirEsp 
            Height          =   300
            Index           =   0
            Left            =   1935
            Picture         =   "frmCatalogoAdjun.frx":2074
            Style           =   1  'Graphical
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEsp 
            Height          =   300
            Index           =   0
            Left            =   1455
            Picture         =   "frmCatalogoAdjun.frx":20F0
            Style           =   1  'Graphical
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarEsp 
            Height          =   300
            Index           =   0
            Left            =   510
            Picture         =   "frmCatalogoAdjun.frx":2171
            Style           =   1  'Graphical
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirEsp 
            Height          =   300
            Index           =   0
            Left            =   30
            Picture         =   "frmCatalogoAdjun.frx":21F7
            Style           =   1  'Graphical
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdModificarEsp 
            Height          =   300
            Index           =   0
            Left            =   990
            Picture         =   "frmCatalogoAdjun.frx":2258
            Style           =   1  'Graphical
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.CommandButton cmdPasarUnFichDesdeLinea 
         Height          =   300
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":23A2
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   480
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarUnFichALinea 
         Height          =   315
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":26FC
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   825
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarTodosDesdeLinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":2A55
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   1245
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarTodosALinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAdjun.frx":2DAF
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1710
         Width           =   495
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   2000
         Index           =   0
         Left            =   660
         TabIndex        =   1
         Top             =   450
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   3519
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Path"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin VB.Label lblArticuloFich 
         BackColor       =   &H00808000&
         Caption         =   "Archivos adjuntos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   240
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   210
         Width           =   4860
      End
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogoAdjun.frx":3108
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCatalogoAdjun"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_oLinea         As CLineaCatalogo
Public g_oArticulo      As CArticulo
Public g_oArticuloProve As CArticulo
Public g_oProveedor     As CProveedor
Public g_oIBaseDatos    As IBaseDatos

Private m_oEsp          As CEspecificacion

Public g_bCancelarEsp   As Boolean
Public g_sComentario    As String
Public g_bRespetarCombo As Boolean
Public g_bSoloRuta      As Boolean

'Restricciones
Private m_bModifArt      As Boolean
Private m_bRMatArt       As Boolean
Private m_bModifArtProve As Boolean
Private m_bRMatArtProve  As Boolean
Private m_bModifLinea    As Boolean

Private m_iPasado        As Integer
Private sayFileNames()   As String

'Idiomas
Private m_sIdiArticulo      As String
Private m_sIdiProveedor     As String
Private m_sIdiDialogTitle   As String
Private m_sIdiAllFiles      As String
Private m_sIdiArchivo       As String
Private m_sIdiGuardarEsp    As String
Private m_sIdiTipoOrig      As String
Private m_sIdiNombre        As String
Private m_skb As String
Private m_sCaption As String

Private Sub LeerAdjunto(ByVal Index As Integer, ByVal sFileName As String)
Dim oAdjun As CAdjunto
Dim iTipo As Integer
Dim sID_Rel As String
Select Case Index
    Case 1
        iTipo = TipoAdjunto.ProveedorArticulo
        sID_Rel = g_oProveedor.Cod & "#" & g_oArticuloProve.Cod
    Case 2
        iTipo = TipoAdjunto.EspecificacionLineaCatalogo
        sID_Rel = g_oLinea.Id
End Select
Set oAdjun = oFSGSRaiz.generar_cadjunto
oAdjun.Tipo = iTipo
oAdjun.Id = m_oEsp.Id
oAdjun.DataSize = m_oEsp.DataSize
oAdjun.LeerAdjunto sFileName, sID_Rel
End Sub

Private Sub cmdAbrirEsp_Click(Index As Integer)
Dim Item As MSComctlLib.listItem
Dim udtTipoEsp As TipoEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit

On Error GoTo ERROR

Set Item = lstvwArticuloEsp(Index).selectedItem

If Item Is Nothing Then
    oMensajes.SeleccioneFichero
Else
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & Item.Text
    sFileTitle = Item.Text
    'Comprueba si existe el fichero y si existe se borra:
    Dim FOSFile As Scripting.FileSystemObject
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing

    ' Cargamos el contenido en la esp.
    Select Case Index
        Case 0
            Set m_oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp(0).selectedItem.Tag))
            Set m_oEsp.Articulo = g_oArticulo
            udtTipoEsp = TipoEspecificacion.EspArticulo
        Case 1
            Set m_oEsp = g_oArticuloProve.especificaciones.Item(CStr(lstvwArticuloEsp(1).selectedItem.Tag))
            Set m_oEsp.Articulo = g_oArticuloProve
            Set m_oEsp.Proveedor = g_oProveedor
            udtTipoEsp = TipoEspecificacion.EspArticuloProveedor
        Case 2
            Set m_oEsp = g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp(2).selectedItem.Tag))
            Set m_oEsp.LineaCatalogo = g_oLinea
            udtTipoEsp = TipoEspecificacion.EspLineaCatalogo
    End Select
    
    If IsNull(m_oEsp.Ruta) Then 'Archivo adjunto, se lee de la base de datos
        Screen.MousePointer = vbHourglass
        teserror = m_oEsp.ComenzarLecturaData(udtTipoEsp)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set m_oEsp = Nothing
            Exit Sub
        End If
        Select Case Index
            Case 0
                m_oEsp.LeerAdjunto m_oEsp.Id, TipoEspecificacion.EspArticulo, m_oEsp.DataSize, sFileName
                'Lanzamos la aplicacion
                ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
            Case 1, 2
                LeerAdjunto Index, sFileName
                ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        End Select
    Else 'Archivo vinculado
        ArchivoAbrir True
    End If
End If
Set m_oEsp = Nothing
Screen.MousePointer = vbNormal
Exit Sub
ERROR:
    If err.Number = 70 Then
        Resume Next
    End If
End Sub
Private Sub ArchivoAbrir(ByVal bSoloRuta As Boolean)
Dim DataFile As Integer
Dim oFos As FileSystemObject
    
On Error GoTo Cancelar:

If bSoloRuta Then
    Set oFos = New FileSystemObject
    If oFos.FolderExists(m_oEsp.Ruta) And oFos.FileExists(m_oEsp.Ruta & m_oEsp.nombre) Then
        ShellExecute MDI.hWnd, "Open", m_oEsp.Ruta & m_oEsp.nombre, 0&, m_oEsp.Ruta, 1
    Else
        oMensajes.ImposibleModificarEsp (m_oEsp.Ruta & m_oEsp.nombre)
    End If
End If
Set m_oEsp = Nothing
Screen.MousePointer = vbNormal
    Exit Sub
    
Cancelar:
    
    If DataFile <> 0 Then
        Close DataFile
        DataFile = 0
    End If
    Screen.MousePointer = vbNormal
    Set m_oEsp = Nothing
End Sub

Private Sub cmdAnyadirEsp_Click(Index As Integer)
Dim teserror As TipoErrorSummit
Dim sRuta As String
Dim sFileName As String
Dim sFileTitle As String
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oFos As Scripting.FileSystemObject
Dim oArticulo As CArticulo
Dim oProveedor As CProveedor
Dim sAdjunto As String
Dim ArrayAdjunto() As String
Dim oFile As File
Dim bites As Long
Dim iTipo As Integer
Dim sTipo As String
Dim oAdjun As CAdjunto
Dim sID_Rel As String
On Error GoTo Cancelar:

cmmdEsp.filename = ""
cmmdEsp.DialogTitle = m_sIdiDialogTitle
cmmdEsp.Filter = m_sIdiAllFiles & "|*.*"
cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
cmmdEsp.ShowOpen
sFileName = cmmdEsp.filename
sFileTitle = cmmdEsp.FileTitle
If sFileName = "" Then Exit Sub
arrFileNames = ExtraerFicheros(sFileName)
frmPROCEComFich.chkProcFich.Visible = True
If Index = 2 Then frmPROCEComFich.chkProcFich.Visible = False
If UBound(arrFileNames) = 1 Then
    frmPROCEComFich.lblFich = sFileTitle
Else
    frmPROCEComFich.lblFich = ""
    frmPROCEComFich.Label1.Visible = False
    frmPROCEComFich.lblFich.Visible = False
    frmPROCEComFich.txtCom.Top = frmPROCEComFich.chkProcFich.Top
    frmPROCEComFich.chkProcFich.Top = frmPROCEComFich.Label1.Top
    frmPROCEComFich.txtCom.Height = 2000
End If
frmPROCEComFich.sOrigen = "frmCatalogoAdjunArt"
frmPROCEComFich.Show 1

If Not g_bCancelarEsp Then
    If g_bSoloRuta Then
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            Screen.MousePointer = vbHourglass
            Set m_oEsp = oFSGSRaiz.generar_CEspecificacion
            Select Case Index
                Case 0
                    Set oArticulo = g_oArticulo
                    Set oProveedor = Nothing
                    sTipo = "ART"
                Case 1
                    Set oArticulo = g_oArticuloProve
                    Set oProveedor = g_oProveedor
                    sTipo = "PROVE"
            End Select
            Set m_oEsp.Articulo = oArticulo
            Set m_oEsp.Proveedor = oProveedor
            m_oEsp.nombre = sFileTitle
            m_oEsp.Comentario = g_sComentario
            m_oEsp.DataSize = FileLen(sFileName)
            m_oEsp.Fecha = Now()
            sRuta = ArchivoComprobarRuta(sFileName, sFileTitle)
            If sRuta = "" Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set m_oEsp = Nothing
                Exit Sub
            End If
            
            m_oEsp.Ruta = sRuta
            
            Set g_oIBaseDatos = m_oEsp
            teserror = g_oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set m_oEsp = Nothing
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
            Set g_oIBaseDatos = Nothing
            oArticulo.especificaciones.Add m_oEsp.nombre, m_oEsp.Fecha, m_oEsp.Id, , , m_oEsp.Comentario, , m_oEsp.Articulo, m_oEsp.Proveedor, , m_oEsp.Ruta, , m_oEsp.DataSize
            oArticulo.EspAdj = 1
            lstvwArticuloEsp(Index).ListItems.Add , sTipo & CStr(m_oEsp.Id), sFileTitle, , "ESP"
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ToolTipText = m_oEsp.Comentario
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(m_oEsp.DataSize / 1024) & " " & m_skb
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Path", NullToStr(m_oEsp.Ruta)
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Com", m_oEsp.Comentario
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Fec", m_oEsp.Fecha
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).Tag = m_oEsp.Id
        Next
    Else
        Set oFos = New Scripting.FileSystemObject
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            Set m_oEsp = oFSGSRaiz.generar_CEspecificacion
            Select Case Index
                Case 0
                    Set oArticulo = g_oArticulo
                    Set oProveedor = Nothing
                    iTipo = TipoEspecificacion.EspArticulo
                    sTipo = "ART"
                Case 1
                    Set oArticulo = g_oArticuloProve
                    Set oProveedor = g_oProveedor
                    iTipo = TipoAdjunto.ProveedorArticulo
                    sTipo = "PROVE"
                    sID_Rel = oProveedor.Cod & "#" & oArticulo.Cod
                    g_oProveedor.GenerarPROVE_ART4 oArticulo.Cod
                Case 2
                    Set oArticulo = g_oArticulo
                    Set oProveedor = Nothing
                    Set m_oEsp.LineaCatalogo = g_oLinea
                    iTipo = TipoAdjunto.EspecificacionLineaCatalogo
                    sTipo = "LINEA"
                    sID_Rel = g_oLinea.Id
            End Select
            Set m_oEsp.Articulo = oArticulo
            Set m_oEsp.Proveedor = oProveedor
            m_oEsp.nombre = sFileTitle
            m_oEsp.Comentario = g_sComentario
            m_oEsp.DataSize = FileLen(sFileName)
            m_oEsp.Fecha = Now()
            
            m_oEsp.Ruta = Null
            Screen.MousePointer = vbHourglass
            
            Select Case Index
                Case 0
                    teserror = m_oEsp.ComenzarEscrituraData(iTipo)
                    If teserror.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        basErrores.TratarError teserror
                        Set m_oEsp = Nothing
                        Exit Sub
                    End If
                    Set oFile = oFos.GetFile(sFileName)
                    bites = oFile.Size
                    oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                    sAdjunto = m_oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspArticulo)
                    m_oEsp.DataSize = bites
                    m_oEsp.Fecha = Date & " " & Time
                Case Else
                    Set oAdjun = oFSGSRaiz.generar_cadjunto
                    oAdjun.nombre = sFileTitle
                    oAdjun.Comentario = g_sComentario
                    oAdjun.Tipo = iTipo
                    sAdjunto = oAdjun.GrabarAdjunto(arrFileNames(0) & "\", "", sID_Rel)
                    'Creamos un array, cada "substring" se asignar� a un elemento del array
                    ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                    oAdjun.Id = ArrayAdjunto(0)
                    oAdjun.SacarFecAct sID_Rel
                    m_oEsp.DataSize = oAdjun.DataSize
                    m_oEsp.Fecha = oAdjun.FECACT
            End Select
            'Creamos un array, cada "substring" se asignar�
            'a un elemento del array
            ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
            m_oEsp.Id = ArrayAdjunto(0)
            
            
            If m_oEsp.LineaCatalogo Is Nothing Then
                oArticulo.especificaciones.Add m_oEsp.nombre, m_oEsp.Fecha, m_oEsp.Id, , , m_oEsp.Comentario, , m_oEsp.Articulo, m_oEsp.Proveedor, , m_oEsp.Ruta, , m_oEsp.DataSize
                oArticulo.EspAdj = 1
            Else
                g_oLinea.especificaciones.Add m_oEsp.nombre, m_oEsp.Fecha, m_oEsp.Id, , , m_oEsp.Comentario, , , , , , , m_oEsp.DataSize
                g_oLinea.EspAdj = 1
            End If
            lstvwArticuloEsp(Index).ListItems.Add , sTipo & CStr(m_oEsp.Id), sFileTitle, , "ESP"
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ToolTipText = m_oEsp.Comentario
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(m_oEsp.DataSize / 1024) & " " & m_skb
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Path", NullToStr(m_oEsp.Ruta)
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Com", m_oEsp.Comentario
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).ListSubItems.Add , "Fec", m_oEsp.Fecha
            lstvwArticuloEsp(Index).ListItems.Item(sTipo & CStr(m_oEsp.Id)).Tag = m_oEsp.Id
            Select Case Index
                Case 2
                    Set frmCatalogoAtribEspYAdj.g_oLinea = g_oLinea
                    frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Add , "LINEA" & CStr(m_oEsp.Id), sFileTitle, , "ESP"
                    frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(m_oEsp.Id)).ToolTipText = m_oEsp.Comentario
                    frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(m_oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(m_oEsp.DataSize / 1024) & " " & m_skb
                    frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(m_oEsp.Id)).ListSubItems.Add , "Com", m_oEsp.Comentario
                    frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(m_oEsp.Id)).ListSubItems.Add , "Fec", m_oEsp.Fecha
                    frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(m_oEsp.Id)).Tag = m_oEsp.Id
            End Select
            
        Next
    End If
    
    Select Case Index
        Case 0
            basSeguridad.RegistrarAccion AccionesSummit.ACCArtAdjAnya, "Cod:" & oArticulo.Cod & " Archivo:" & m_oEsp.nombre
        Case 1
            basSeguridad.RegistrarAccion AccionesSummit.ACCMatPorProveAdjAnya, "Prove:" & oProveedor.Cod & " Art:" & oArticulo.Cod & " Archivo:" & m_oEsp.nombre
        Case 2
            basSeguridad.RegistrarAccion AccionesSummit.ACCCatAdjudAdjunAnya, "Linea:" & g_oLinea.Id & " Archivo:" & m_oEsp.nombre
    End Select
    lstvwArticuloEsp(Index).Refresh
    Set m_oEsp = Nothing
    Screen.MousePointer = vbNormal
End If

Screen.MousePointer = vbNormal
Exit Sub
Cancelar:
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        MsgBox err.Description
        Set m_oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Function ArchivoComprobarRuta(ByVal sFileFull As String, ByVal sFile As String) As String
Dim sdrive As String
Dim sdrive2 As String
Dim sRuta As String
Dim oFos As FileSystemObject
Dim oDrive As Scripting.Drive

    

        Set oFos = New FileSystemObject

        sdrive = oFos.GetDriveName(sFileFull)
        sRuta = Left(sFileFull, Len(sFileFull) - Len(sFile))
        sRuta = Right(sRuta, Len(sRuta) - Len(sdrive))
        Set oDrive = oFos.GetDrive(sdrive)
        sdrive2 = oDrive.ShareName
        
        If sdrive2 = "" Then
            sRuta = sdrive & sRuta
        Else
            sRuta = sdrive2 & sRuta
        End If
        Set oFos = Nothing
        Set oDrive = Nothing
        ArchivoComprobarRuta = sRuta
End Function

Private Sub cmdEliminarEsp_Click(Index As Integer)
Dim Item As MSComctlLib.listItem
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
        
On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp(Index).selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        irespuesta = oMensajes.PreguntaEliminar(m_sIdiArchivo & ": " & lstvwArticuloEsp(Index).selectedItem.Text)
        If irespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        Select Case Index
        Case 0
            Set m_oEsp = g_oArticulo.especificaciones.Item(lstvwArticuloEsp(0).selectedItem.Index)
            Set m_oEsp.Articulo = g_oArticulo
            Set g_oIBaseDatos = m_oEsp
            teserror = g_oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                g_oArticulo.especificaciones.Remove (lstvwArticuloEsp(0).selectedItem.Index)
                If g_oArticulo.especificaciones.Count = 0 Then
                    'If txtArticuloEsp(0).Text = "" Then
                        g_oArticulo.EspAdj = 0
                    'End If
                End If
                basSeguridad.RegistrarAccion AccionesSummit.ACCArtAdjEli, "Art:" & CStr(g_oArticulo.Cod) & " Archivo:" & m_oEsp.nombre
                lstvwArticuloEsp(0).ListItems.Remove (CStr(lstvwArticuloEsp(0).selectedItem.key))
            End If
            
        Case 1
            Set m_oEsp = g_oArticuloProve.especificaciones.Item(lstvwArticuloEsp(1).selectedItem.Index)
            Set m_oEsp.Articulo = g_oArticuloProve
            Set m_oEsp.Proveedor = g_oProveedor
            Set g_oIBaseDatos = m_oEsp
            teserror = g_oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                g_oArticuloProve.especificaciones.Remove (lstvwArticuloEsp(1).selectedItem.Index)
                If g_oArticulo.especificaciones.Count = 0 Then
                    g_oArticuloProve.EspAdj = 0
                End If
                basSeguridad.RegistrarAccion AccionesSummit.ACCMatPorProveAdjEli, "Prove:" & g_oProveedor.Cod & " Art:" & g_oArticuloProve.Cod & " Archivo:" & m_oEsp.nombre
                lstvwArticuloEsp(1).ListItems.Remove (CStr(lstvwArticuloEsp(1).selectedItem.key))
            End If

        Case 2
            Set m_oEsp = g_oLinea.especificaciones.Item(lstvwArticuloEsp(2).selectedItem.Index)
            Set m_oEsp.LineaCatalogo = g_oLinea
            Set g_oIBaseDatos = m_oEsp
            teserror = g_oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
            Else
                g_oLinea.especificaciones.Remove (lstvwArticuloEsp(2).selectedItem.Index)
                If g_oLinea.especificaciones.Count = 0 Then
                    g_oLinea.EspAdj = 0
                End If
                basSeguridad.RegistrarAccion AccionesSummit.ACCCatAdjudAdjunEli, "Linea:" & CStr(g_oLinea.Id) & " Archivo:" & m_oEsp.nombre
                Dim itemKey As String
                itemKey = CStr(lstvwArticuloEsp(2).selectedItem.key)
                lstvwArticuloEsp(2).ListItems.Remove (itemKey)
                Set frmCatalogoAtribEspYAdj.g_oLinea = g_oLinea
                frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Remove (itemKey)
            End If
        End Select
    End If
    
Cancelar:
    
    Set m_oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal


End Sub

Private Sub cmdModificarEsp_Click(Index As Integer)
Dim teserror As TipoErrorSummit
 
    If lstvwArticuloEsp(Index).selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        
        Screen.MousePointer = vbHourglass
        Select Case Index
        Case 0
            Set g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp(0).selectedItem.Tag)).Articulo = g_oArticulo
            Set g_oIBaseDatos = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp(0).selectedItem.Tag))
            teserror = g_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
            
            basSeguridad.RegistrarAccion AccionesSummit.ACCArtadjMod, "Art:" & g_oArticulo.Cod & "Archivo:" & CStr(lstvwArticuloEsp(0).selectedItem.Text)
            
            Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
            frmPROCEEspMod.g_sOrigen = "frmCatalogoAdjunArt"
            Screen.MousePointer = vbNormal
            frmPROCEEspMod.Show 1
        Case 1
            Set g_oArticuloProve.especificaciones.Item(CStr(lstvwArticuloEsp(1).selectedItem.Tag)).Articulo = g_oArticuloProve
            Set g_oArticuloProve.especificaciones.Item(CStr(lstvwArticuloEsp(1).selectedItem.Tag)).Proveedor = g_oProveedor
            Set g_oIBaseDatos = g_oArticuloProve.especificaciones.Item(CStr(lstvwArticuloEsp(1).selectedItem.Tag))
            teserror = g_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
            
            basSeguridad.RegistrarAccion AccionesSummit.ACCMatPorProveAdjMod, "Prove:" & g_oProveedor.Cod & " Art:" & g_oArticulo.Cod & "Archivo:" & CStr(lstvwArticuloEsp(1).selectedItem.Text)
            
            Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
            frmPROCEEspMod.g_sOrigen = "frmCatalogoAdjunArtProve"
            Screen.MousePointer = vbNormal
            frmPROCEEspMod.Show 1
        
        Case 2
            Set g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp(2).selectedItem.Tag)).LineaCatalogo = g_oLinea
            Set g_oIBaseDatos = g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp(2).selectedItem.Tag))
            teserror = g_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set g_oIBaseDatos = Nothing
                Exit Sub
            End If
            
            basSeguridad.RegistrarAccion AccionesSummit.ACCCatAdjudAdjunMod, "Linea:" & g_oLinea.Id & "Archivo:" & CStr(lstvwArticuloEsp(2).selectedItem.Text)
            
            Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
            frmPROCEEspMod.g_sOrigen = "frmCatalogoAdjunLinea"
            Screen.MousePointer = vbNormal
            frmPROCEEspMod.Show 1
        
        End Select
    End If

End Sub

Private Sub cmdPasarTodosALinea_Click(Index As Integer)
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim teserror As TipoErrorSummit
Dim sLineaError As String
    
If lstvwArticuloEsp(Index).ListItems.Count = 0 Then
    Exit Sub
End If

Set Item = lstvwArticuloEsp(Index).ListItems.Item(1)
If Item Is Nothing Then
    Exit Sub
End If

Screen.MousePointer = vbHourglass
'se a�aden las especificaciones del art�culo a la linea
Set oAdjun = oFSGSRaiz.generar_cadjunto

Select Case Index
    Case 0
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.Articulo, TipoAdjunto.EspecificacionLineaCatalogo, , , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
    Case 1
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.ProveedorArticulo, TipoAdjunto.EspecificacionLineaCatalogo, , , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
End Select
    
If teserror.NumError <> TESnoerror Then
    If teserror.NumError = TESImposibleAccederAlArchivo And IsArray(teserror.Arg1) Then
            sLineaError = m_sIdiProveedor & ": " & g_oLinea.ProveCod & "; " & m_sIdiArticulo & " " & g_oLinea.ArtCod_Interno & " - " & g_oLinea.ArtDen
            oMensajes.ImposibleAccederAFichero teserror.Arg1, sLineaError
    Else
        basErrores.TratarError teserror
        oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1, "ArtLineaCatalogo"
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
End If

g_oLinea.CargarTodasLasEspecificaciones , , True
AnyadirEspsALista (2)
If Not IsNull(g_oLinea.esp) Or g_oLinea.especificaciones.Count > 0 Then
    g_oLinea.EspAdj = 1
End If
Set oAdjun = Nothing
Screen.MousePointer = vbNormal
End Sub

Private Sub cmdPasarTodosDesdeLinea_Click(Index As Integer)
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim teserror As TipoErrorSummit
    
If lstvwArticuloEsp(2).ListItems.Count = 0 Then
    Exit Sub
End If

Set Item = lstvwArticuloEsp(2).ListItems.Item(1)
If Item Is Nothing Then
    Exit Sub
End If

Screen.MousePointer = vbHourglass
'se a�aden las especificaciones del art�culo a la linea
Set oAdjun = oFSGSRaiz.generar_cadjunto
Select Case Index
    Case 0
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.EspecificacionLineaCatalogo, TipoAdjunto.Articulo, , , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1, "LineaCatArt"
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        g_oArticulo.CargarTodasLasEspecificaciones , , True
        AnyadirEspsALista (0)
        If Not IsNull(g_oArticulo.esp) Or g_oArticulo.especificaciones.Count > 0 Then
            g_oArticulo.EspAdj = 1
        End If
    Case 1
        g_oProveedor.GenerarPROVE_ART4 g_oArticulo.Cod
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.EspecificacionLineaCatalogo, TipoAdjunto.ProveedorArticulo, , , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1, "LineaCatArt"
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        g_oArticuloProve.CargarTodasLasEspecificaciones , , True, g_oProveedor.Cod
        AnyadirEspsALista (1)
        If Not IsNull(g_oArticuloProve.esp) Or g_oArticuloProve.especificaciones.Count > 0 Then
            g_oArticuloProve.EspAdj = 1
        End If
End Select

Set oAdjun = Nothing
Screen.MousePointer = vbNormal

End Sub

Private Sub cmdPasarUnFichALinea_Click(Index As Integer)
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim teserror As TipoErrorSummit
        
Select Case Index
Case 0 'se a�aden las especificaciones del art�culo a la linea
    Set Item = lstvwArticuloEsp(0).selectedItem
    
    If Not Item Is Nothing Then
        Screen.MousePointer = vbHourglass
        Set oAdjun = oFSGSRaiz.generar_cadjunto
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.Articulo, TipoAdjunto.EspecificacionLineaCatalogo, g_oArticulo.especificaciones.Item(Item.Index).Id, , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            If teserror.NumError = TESImposibleAccederAlArchivo Then
                oMensajes.ImposibleAccederAFichero
            Else
                basErrores.TratarError teserror
            End If
            Exit Sub
        End If
                        
        lstvwArticuloEsp(2).ListItems.Add , "LINEA" & CStr(oAdjun.Id), Item.Text, , "ESP"
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(1) = Item.SubItems(1)
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(2) = Item.SubItems(3)
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(3) = Now
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).Tag = oAdjun.Id
         
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Add , "LINEA" & CStr(oAdjun.Id), Item.Text, , "ESP"
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(1) = Item.SubItems(1)
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(2) = Item.SubItems(3)
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(3) = Now
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).Tag = oAdjun.Id
         
        g_oLinea.especificaciones.Add Item.Text, Now, oAdjun.Id, , , Item.SubItems(3)
        g_oLinea.EspAdj = 1
        Set frmCatalogoAtribEspYAdj.g_oLinea = g_oLinea
         
        Screen.MousePointer = vbNormal
    
    End If

Case 1 'se a�aden las especificaciones del proveedor-art�culo a la linea
    Set Item = lstvwArticuloEsp(1).selectedItem
    
    If Not Item Is Nothing Then
        Screen.MousePointer = vbHourglass
        Set oAdjun = oFSGSRaiz.generar_cadjunto
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.ProveedorArticulo, TipoAdjunto.EspecificacionLineaCatalogo, g_oArticuloProve.especificaciones.Item(Item.Index).Id, , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            If teserror.NumError = TESImposibleAccederAlArchivo Then
                oMensajes.ImposibleAccederAFichero
            Else
                basErrores.TratarError teserror
            End If
            Exit Sub
        End If
                         
        lstvwArticuloEsp(2).ListItems.Add , "LINEA" & CStr(oAdjun.Id), Item.Text, , "ESP"
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(1) = Item.SubItems(1)
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(2) = Item.SubItems(3)
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(3) = Now
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oAdjun.Id)).Tag = oAdjun.Id
         
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Add , "LINEA" & CStr(oAdjun.Id), Item.Text, , "ESP"
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(1) = Item.SubItems(1)
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(2) = Item.SubItems(3)
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).SubItems(3) = Now
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oAdjun.Id)).Tag = oAdjun.Id
         
        g_oLinea.especificaciones.Add Item.Text, Now, oAdjun.Id, , , Item.SubItems(3)
        g_oLinea.EspAdj = 1
        Set frmCatalogoAtribEspYAdj.g_oLinea = g_oLinea
         
        Screen.MousePointer = vbNormal
    
    End If


End Select
End Sub

Private Sub cmdPasarUnFichDesdeLinea_Click(Index As Integer)
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim teserror As TipoErrorSummit
        
Select Case Index
Case 0 'se a�aden las especificaciones de la linea al art�culo
    Set Item = lstvwArticuloEsp(2).selectedItem
    If Not Item Is Nothing Then
        Screen.MousePointer = vbHourglass
        Set oAdjun = oFSGSRaiz.generar_cadjunto
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.EspecificacionLineaCatalogo, TipoAdjunto.Articulo, g_oLinea.especificaciones.Item(Item.Index).Id, , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1, "LineaCatArt"
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
                        
        lstvwArticuloEsp(0).ListItems.Add , "ART" & CStr(oAdjun.Id), Item.Text, , "ESP"
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oAdjun.Id)).SubItems(1) = Item.SubItems(1)
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oAdjun.Id)).SubItems(3) = Item.SubItems(2)
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oAdjun.Id)).SubItems(4) = Now
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oAdjun.Id)).Tag = oAdjun.Id
         
        g_oArticulo.especificaciones.Add Item.Text, Now, oAdjun.Id, , , Item.SubItems(2)
        g_oArticulo.EspAdj = 1
         
        Screen.MousePointer = vbNormal
    
    End If

Case 1 'se a�aden las especificaciones del proveedor-art�culo a la linea
    Set Item = lstvwArticuloEsp(2).selectedItem
    
    If Not Item Is Nothing Then
        Screen.MousePointer = vbHourglass
        Set oAdjun = oFSGSRaiz.generar_cadjunto
        g_oProveedor.GenerarPROVE_ART4 g_oArticulo.Cod
        teserror = oAdjun.CopiarAdjuntosCatalogo(TipoAdjunto.EspecificacionLineaCatalogo, TipoAdjunto.ProveedorArticulo, g_oLinea.especificaciones.Item(Item.Index).Id, , g_oArticulo.Cod, g_oProveedor.Cod, g_oLinea.Id)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1, "LineaCatArt"
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
                         
        lstvwArticuloEsp(1).ListItems.Add , "PROV" & CStr(oAdjun.Id), Item.Text, , "ESP"
        lstvwArticuloEsp(1).ListItems.Item("PROV" & CStr(oAdjun.Id)).SubItems(1) = Item.SubItems(1)
        lstvwArticuloEsp(1).ListItems.Item("PROV" & CStr(oAdjun.Id)).SubItems(3) = Item.SubItems(2)
        lstvwArticuloEsp(1).ListItems.Item("PROV" & CStr(oAdjun.Id)).SubItems(4) = Now
        lstvwArticuloEsp(1).ListItems.Item("PROV" & CStr(oAdjun.Id)).Tag = oAdjun.Id
         
        g_oArticuloProve.especificaciones.Add Item.Text, Now, oAdjun.Id, , , Item.SubItems(2)
        g_oArticuloProve.EspAdj = 1
         
        Screen.MousePointer = vbNormal
    
    End If
End Select
End Sub

Private Sub cmdSalvarEsp_Click(Index As Integer)
Dim Item As MSComctlLib.listItem
Dim udtTipoEsp As TipoEspecificacion
Dim DataFile As Integer
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oFos As FileSystemObject
    
On Error GoTo Cancelar:

Set Item = lstvwArticuloEsp(Index).selectedItem

If Item Is Nothing Then
    oMensajes.SeleccioneFichero
Else
    cmmdEsp.DialogTitle = m_sIdiGuardarEsp
    cmmdEsp.Filter = m_sIdiTipoOrig & "|*.*"
    cmmdEsp.filename = Item.Text
    cmmdEsp.ShowSave
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdiNombre
        Exit Sub
    End If
    DataFile = 1
    
    ' Cargamos el contenido en la esp.
    Select Case Index
        Case 0
            Set m_oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp(0).selectedItem.Tag))
            Set m_oEsp.Articulo = g_oArticulo
            udtTipoEsp = TipoEspecificacion.EspArticulo
        Case 1
            Set m_oEsp = g_oArticuloProve.especificaciones.Item(CStr(lstvwArticuloEsp(1).selectedItem.Tag))
            Set m_oEsp.Articulo = g_oArticuloProve
            Set m_oEsp.Proveedor = g_oProveedor
            udtTipoEsp = TipoEspecificacion.EspArticuloProveedor
        
        Case 2
            Set m_oEsp = g_oLinea.especificaciones.Item(CStr(lstvwArticuloEsp(2).selectedItem.Tag))
            Set m_oEsp.LineaCatalogo = g_oLinea
            udtTipoEsp = TipoEspecificacion.EspLineaCatalogo
    End Select
    
    If IsNull(m_oEsp.Ruta) Then 'Archivo adjunto
        Select Case Index
            Case 0
                Screen.MousePointer = vbHourglass
                teserror = m_oEsp.ComenzarLecturaData(TipoEspecificacion.EspArticulo)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set m_oEsp = Nothing
                    Exit Sub
                End If
                m_oEsp.LeerAdjunto m_oEsp.Id, TipoEspecificacion.EspArticulo, m_oEsp.DataSize, sFileName
            Case 1, 2
                LeerAdjunto Index, sFileName
        End Select
    Else 'Archivo vinculado
        Set oFos = New FileSystemObject
        If oFos.FolderExists(m_oEsp.Ruta) And oFos.FileExists(m_oEsp.Ruta & m_oEsp.nombre) Then
            oFos.CopyFile m_oEsp.Ruta & m_oEsp.nombre, sFileName, False
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            oMensajes.ImposibleModificarEsp (m_oEsp.Ruta & m_oEsp.nombre)
        End If
        Set oFos = Nothing
    End If
    Set m_oEsp = Nothing
End If

Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        If DataFile <> 0 Then
            Close DataFile
        End If
        Set m_oEsp = Nothing
    End If
    Screen.MousePointer = vbNormal


End Sub

Private Sub Form_Load()
              
    If g_oLinea Is Nothing Then Unload Me
    
    If g_oLinea.ArtCod_Interno = "" Then
        Frame1(0).Visible = False
        Frame1(1).Visible = False
        Me.Width = 8475
    End If
    
    CargarEsp
    
    CargarRecursos
    
    If g_oLinea.ArtCod_Interno = "" Then
        Me.caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & _
             " / " & m_sIdiArticulo & ": " & g_oLinea.ArtDen
    Else
        Me.caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & _
            " / " & m_sIdiArticulo & ": " & g_oArticulo.Cod
    End If
    ConfigurarSeguridad
    m_iPasado = 0

    'Configurar parametros del commondialog
    cmmdEsp.FLAGS = cdlOFNHideReadOnly
    ReDim sayFileNames(0)
End Sub
Private Sub CargarEsp()
Dim oProves As CProveedores

    If g_oLinea Is Nothing Then Unload Me

    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.CargarTodosLosProveedoresDesde3 1, g_oLinea.ProveCod, , True
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sIdiProveedor
        Set oProves = Nothing
        Set g_oProveedor = Nothing
    End If
    Set g_oProveedor = oProves.Item(1)

    If g_oLinea.ArtCod_Interno <> "" Then
        'Carga las especificaciones de todos
        Set g_oArticulo = oFSGSRaiz.Generar_CArticulo
        g_oArticulo.GMN1Cod = g_oLinea.GMN1Cod
        g_oArticulo.GMN2Cod = g_oLinea.GMN2Cod
        g_oArticulo.GMN3Cod = g_oLinea.GMN3Cod
        g_oArticulo.GMN4Cod = g_oLinea.GMN4Cod
        g_oArticulo.Cod = g_oLinea.ArtCod_Interno
        g_oArticulo.Den = g_oLinea.ArtDen
        g_oArticulo.CodigoUnidad = g_oLinea.CodigoUnidadArticulo
        If Not IsNull(g_oLinea.Concepto) Then
            g_oArticulo.Concepto = g_oLinea.Concepto
        End If
        If Not IsNull(g_oLinea.Almacenable) Then
            g_oArticulo.Almacenable = g_oLinea.Almacenable
        End If
        If Not IsNull(g_oLinea.Recepcionable) Then
            g_oArticulo.Recepcionable = g_oLinea.Recepcionable
        End If
        
        
        g_oArticulo.CargarTodasLasEspecificaciones , , True
        Set g_oIBaseDatos = g_oArticulo
        g_oIBaseDatos.IniciarEdicion
        Set g_oIBaseDatos = Nothing
        
        g_bRespetarCombo = False
        AnyadirEspsALista (0)
            
        Set g_oArticuloProve = oFSGSRaiz.Generar_CArticulo
        g_oArticuloProve.GMN1Cod = g_oLinea.GMN1Cod
        g_oArticuloProve.GMN2Cod = g_oLinea.GMN2Cod
        g_oArticuloProve.GMN3Cod = g_oLinea.GMN3Cod
        g_oArticuloProve.GMN4Cod = g_oLinea.GMN4Cod
        g_oArticuloProve.Cod = g_oLinea.ArtCod_Interno
        g_oArticuloProve.Den = g_oLinea.ArtDen
        g_oArticuloProve.CodigoUnidad = g_oLinea.CodigoUnidadArticulo
        If Not IsNull(g_oLinea.Concepto) Then
            g_oArticuloProve.Concepto = g_oLinea.Concepto
        End If
        If Not IsNull(g_oLinea.Almacenable) Then
            g_oArticuloProve.Almacenable = g_oLinea.Almacenable
        End If
        If Not IsNull(g_oLinea.Recepcionable) Then
            g_oArticuloProve.Recepcionable = g_oLinea.Recepcionable
        End If
        g_oArticuloProve.CargarTodasLasEspecificaciones , , True, g_oLinea.ProveCod
        If g_oArticuloProve.especificaciones Is Nothing Then
            cmdPasarUnFichALinea(1).Enabled = False
            cmdPasarUnFichDesdeLinea(1).Enabled = False
            cmdPasarTodosALinea(1).Enabled = False
            cmdPasarTodosDesdeLinea(1).Enabled = False
            cmdAnyadirEsp(1).Enabled = False
            cmdEliminarEsp(1).Enabled = False
            cmdModificarEsp(1).Enabled = False
            cmdSalvarEsp(1).Enabled = False
            cmdAbrirEsp(1).Enabled = False
        Else
            Set g_oIBaseDatos = g_oArticuloProve
            g_oIBaseDatos.IniciarEdicion
            Set g_oIBaseDatos = Nothing
            cmdPasarUnFichALinea(1).Enabled = True
            cmdPasarUnFichDesdeLinea(1).Enabled = True
            cmdPasarTodosALinea(1).Enabled = True
            cmdPasarTodosDesdeLinea(1).Enabled = True
            cmdAnyadirEsp(1).Enabled = True
            cmdEliminarEsp(1).Enabled = True
            cmdModificarEsp(1).Enabled = True
            cmdSalvarEsp(1).Enabled = True
            cmdAbrirEsp(1).Enabled = True
            
            g_bRespetarCombo = True
            g_bRespetarCombo = False
            AnyadirEspsALista (1)
        End If
    End If
    
    g_oLinea.CargarTodasLasEspecificaciones , , True
    g_bRespetarCombo = True
    g_bRespetarCombo = False
    AnyadirEspsALista (2)


    If g_oProveedor Is Nothing Then
        oMensajes.NoValido m_sIdiProveedor
        Unload Me
    End If

End Sub

Private Sub Form_Resize()
Dim dblAux As Double

If Me.Height < 1500 Then Exit Sub
If Me.Width < 1500 Then Exit Sub

On Error Resume Next

If g_oLinea.ArtCod_Interno <> "" Then
    Frame1(2).Height = Me.Height - 500
    Frame1(2).Width = Frame1(0).Width - 570
    
    Frame1(0).Left = Frame1(2).Width + 165
    Frame1(0).Height = Me.Height / 2 - 250
    Frame1(0).Width = Me.Width / 2 + 98
    
    Frame1(1).Left = Frame1(2).Width + 165
    Frame1(1).Top = Frame1(0).Height - 30
    Frame1(1).Height = Frame1(0).Height
    Frame1(1).Width = Frame1(0).Width
Else
    Frame1(2).Height = Me.Height - 500
    Frame1(2).Width = Me.Width - 300
End If

lstvwArticuloEsp(0).Width = Frame1(0).Width - 850
lstvwArticuloEsp(1).Width = lstvwArticuloEsp(0).Width
lstvwArticuloEsp(2).Width = Frame1(2).Width - 180

Picture1.Left = lstvwArticuloEsp(0).Width - Picture1.Width + 760
Picture2.Left = lstvwArticuloEsp(1).Width - Picture2.Width + 760
Picture3.Left = lstvwArticuloEsp(2).Width - Picture3.Width + 60

Picture1.Top = Frame1(0).Height - Picture1.Height - 75
Picture2.Top = Picture1.Top

dblAux = Frame1(2).Height - Picture3.Height - lblArticuloFich(2).Height
lstvwArticuloEsp(2).Height = dblAux - 330
dblAux = Picture3.Height + lblArticuloFich(2).Height
Picture3.Top = Frame1(2).Height - Picture3.Height - 75

dblAux = Frame1(0).Height - Picture1.Height - lblArticuloFich(0).Height
lstvwArticuloEsp(0).Height = dblAux - 330
lblArticuloFich(1).Top = lblArticuloFich(0).Top
lstvwArticuloEsp(1).Top = lstvwArticuloEsp(0).Top
lstvwArticuloEsp(1).Height = lstvwArticuloEsp(0).Height
'Botones
cmdPasarUnFichDesdeLinea(0).Left = cmdPasarUnFichALinea(0).Left
cmdPasarTodosALinea(0).Left = cmdPasarUnFichALinea(0).Left
cmdPasarTodosDesdeLinea(0).Left = cmdPasarUnFichALinea(0).Left
lstvwArticuloEsp(0).Left = cmdPasarUnFichALinea(0).Width + cmdPasarUnFichALinea(0).Left + 105

cmdPasarUnFichALinea(1).Left = cmdPasarUnFichALinea(0).Left
cmdPasarUnFichDesdeLinea(1).Left = cmdPasarUnFichALinea(0).Left
cmdPasarTodosALinea(1).Left = cmdPasarUnFichALinea(0).Left
cmdPasarTodosDesdeLinea(1).Left = cmdPasarUnFichALinea(0).Left
lstvwArticuloEsp(1).Left = cmdPasarUnFichALinea(1).Width + cmdPasarUnFichALinea(1).Left + 105
cmdPasarUnFichALinea(0).Top = lstvwArticuloEsp(0).Top
cmdPasarUnFichDesdeLinea(0).Top = cmdPasarUnFichALinea(0).Top + cmdPasarUnFichALinea(0).Height + 45
cmdPasarTodosALinea(0).Top = cmdPasarUnFichDesdeLinea(0).Top + cmdPasarUnFichDesdeLinea(0).Height + 110
cmdPasarTodosDesdeLinea(0).Top = cmdPasarTodosALinea(0).Top + cmdPasarTodosALinea(0).Height + 45
cmdPasarUnFichALinea(1).Top = cmdPasarUnFichALinea(0).Top
cmdPasarUnFichDesdeLinea(1).Top = cmdPasarUnFichDesdeLinea(0).Top
cmdPasarTodosALinea(1).Top = cmdPasarTodosALinea(0).Top
cmdPasarTodosDesdeLinea(1).Top = cmdPasarTodosDesdeLinea(0).Top


End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer
    Dim FOSFile As Scripting.FileSystemObject
    Dim bBorrando As Boolean

    On Error GoTo ERROR:
    
    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    
    Exit Sub

ERROR:
    
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
    

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_ADJUN, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        lblArticuloFich(2).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiArticulo = Ador(0).Value
        Ador.MoveNext
        m_sIdiProveedor = Ador(0).Value
        Ador.MoveNext
        m_sIdiDialogTitle = Ador(0).Value
        Ador.MoveNext
        m_sIdiAllFiles = Ador(0).Value
        Ador.MoveNext
        m_sIdiArchivo = Ador(0).Value
        lstvwArticuloEsp.Item(0).ColumnHeaders.Item(1).Text = Ador(0).Value
        lstvwArticuloEsp.Item(1).ColumnHeaders.Item(1).Text = Ador(0).Value
        lstvwArticuloEsp.Item(2).ColumnHeaders.Item(1).Text = Ador(0).Value
        Ador.MoveNext
        m_sIdiGuardarEsp = Ador(0).Value
        Ador.MoveNext
        m_sIdiTipoOrig = Ador(0).Value
        Ador.MoveNext
        m_sIdiNombre = Ador(0).Value
        
        Ador.MoveNext
        lstvwArticuloEsp.Item(0).ColumnHeaders.Item(4).Text = Ador(0).Value   'Comentario
        lstvwArticuloEsp.Item(1).ColumnHeaders.Item(4).Text = Ador(0).Value
        lstvwArticuloEsp.Item(2).ColumnHeaders.Item(3).Text = Ador(0).Value
        
        Ador.MoveNext
        lstvwArticuloEsp.Item(0).ColumnHeaders.Item(3).Text = Ador(0).Value   'ruta de archivo
        lstvwArticuloEsp.Item(1).ColumnHeaders.Item(3).Text = Ador(0).Value
        
        Ador.MoveNext
        lstvwArticuloEsp.Item(0).ColumnHeaders.Item(5).Text = Ador(0).Value   'fecha
        lstvwArticuloEsp.Item(1).ColumnHeaders.Item(5).Text = Ador(0).Value
        lstvwArticuloEsp.Item(2).ColumnHeaders.Item(4).Text = Ador(0).Value
        
        Ador.MoveNext
        lstvwArticuloEsp.Item(0).ColumnHeaders.Item(2).Text = Ador(0).Value
        lstvwArticuloEsp.Item(1).ColumnHeaders.Item(2).Text = Ador(0).Value
        lstvwArticuloEsp.Item(2).ColumnHeaders.Item(2).Text = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        Ador.MoveNext
        m_sCaption = Ador(0).Value '18 Archivos adjuntos de la l�nea del cat�logo
        Ador.MoveNext
        lblArticuloFich(0).caption = Ador(0).Value '19 Archivos adjuntos del maestro del art�culo
        Ador.MoveNext
        lblArticuloFich(1).caption = Ador(0).Value '20 Archivos adjuntos del proveedor / art�culo
        Ador.Close
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Public Sub ConfigurarSeguridad()
    Dim oIMaterialAsignado As IMaterialAsignado
    Dim oArticulos As CArticulos
    Dim oArt As CArticulo
    
    m_bModifArt = True
    m_bModifArtProve = True
    m_bModifLinea = True
    
    'Articulo
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
        m_bModifArt = False
    Else
        If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestMatComp)) Is Nothing) Then
            'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material, sino no puede modificarlo
            If g_oLinea.ArtCod_Interno <> "" Then
                Set oIMaterialAsignado = oUsuarioSummit.comprador
                Set oArticulos = oIMaterialAsignado.DevolverArticulos(g_oArticulo.Cod, , True)
                m_bModifArt = False
                m_bRMatArt = True
                For Each oArt In oArticulos
                    If oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And _
                        oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod Then
                        m_bModifArt = True
                        m_bRMatArt = False
                        Exit For
                    End If
                Next
            Else
                m_bModifArt = False
                m_bRMatArt = True
            End If
        End If
    End If
    'Articulo/proveedor
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtGestion)) Is Nothing) Then
        m_bModifArtProve = False
    Else
        If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtRestMAtComp)) Is Nothing) Then
            If g_oLinea.ArtCod_Interno <> "" Then
                m_bModifArtProve = False
                m_bRMatArtProve = True
                'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material/prove
                Set oIMaterialAsignado = g_oProveedor
                Set oIMaterialAsignado.ARTICULOS = oFSGSRaiz.Generar_CArticulos
                oIMaterialAsignado.ARTICULOS.Add g_oArticulo.GMN1Cod, g_oArticulo.GMN2Cod, g_oArticulo.GMN3Cod, g_oArticulo.GMN4Cod, g_oArticulo.Cod, g_oArticulo.Den, , , 1
                Set oArticulos = oIMaterialAsignado.DevolverArticulos(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True, False, False)
                If oArticulos.Count > 0 Then m_bModifArtProve = True: m_bRMatArtProve = False
            End If
        End If
    End If
    'Linea
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAdjModificar)) Is Nothing) Then
        m_bModifLinea = False
    End If

    If Not m_bModifArt Then
        cmdAnyadirEsp(0).Visible = False
        cmdEliminarEsp(0).Visible = False
        cmdModificarEsp(0).Visible = False
        cmdPasarUnFichDesdeLinea(0).Visible = False
        cmdPasarTodosDesdeLinea(0).Visible = False
        If m_bRMatArt Then
            cmdSalvarEsp(0).Visible = False
            cmdAbrirEsp(0).Visible = False
        End If
    End If
    
    If Not m_bModifArtProve Then
        cmdAnyadirEsp(1).Visible = False
        cmdEliminarEsp(1).Visible = False
        cmdModificarEsp(1).Visible = False
        cmdPasarUnFichDesdeLinea(1).Visible = False
        cmdPasarTodosDesdeLinea(1).Visible = False
        If m_bRMatArtProve Then
            cmdSalvarEsp(1).Visible = False
            cmdAbrirEsp(1).Visible = False
        End If
    End If
    
    If Not m_bModifLinea Then
        cmdAnyadirEsp(2).Visible = False
        cmdEliminarEsp(2).Visible = False
        cmdModificarEsp(2).Visible = False
        cmdPasarUnFichALinea(0).Visible = False
        cmdPasarUnFichALinea(1).Visible = False
        cmdPasarTodosALinea(0).Visible = False
        cmdPasarTodosALinea(1).Visible = False
    End If

End Sub

Public Sub AnyadirEspsALista(Index As Integer)
Dim oEsp As CEspecificacion

    Screen.MousePointer = vbHourglass
Select Case Index
Case 0
    lstvwArticuloEsp(0).ListItems.clear
    
    For Each oEsp In g_oArticulo.especificaciones
        lstvwArticuloEsp(0).ListItems.Add , "ART" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oEsp.Id)).ListSubItems.Add , "Path", NullToStr(oEsp.Ruta)
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwArticuloEsp(0).ListItems.Item("ART" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    
Case 1
    lstvwArticuloEsp(1).ListItems.clear
    
    For Each oEsp In g_oArticuloProve.especificaciones
        lstvwArticuloEsp(1).ListItems.Add , "PROVE" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwArticuloEsp(1).ListItems.Item("PROVE" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwArticuloEsp(1).ListItems.Item("PROVE" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwArticuloEsp(1).ListItems.Item("PROVE" & CStr(oEsp.Id)).ListSubItems.Add , "Path", NullToStr(oEsp.Ruta)
        lstvwArticuloEsp(1).ListItems.Item("PROVE" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwArticuloEsp(1).ListItems.Item("PROVE" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwArticuloEsp(1).ListItems.Item("PROVE" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    'Si no puede ver el articulo por la restricci�n de mat no le dejamos abrir los archivos
    If m_bRMatArtProve Then
        cmdSalvarEsp(1).Enabled = False
        cmdAbrirEsp(1).Enabled = False
    End If

Case 2
    lstvwArticuloEsp(2).ListItems.clear
    frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.clear
    
    For Each oEsp In g_oLinea.especificaciones
        lstvwArticuloEsp(2).ListItems.Add , "LINEA" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwArticuloEsp(2).ListItems.Item("LINEA" & CStr(oEsp.Id)).Tag = oEsp.Id
    
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Add , "LINEA" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        frmCatalogoAtribEspYAdj.lstvwArticuloEsp.ListItems.Item("LINEA" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next

End Select
    Screen.MousePointer = vbNormal

End Sub

