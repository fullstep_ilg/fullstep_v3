VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmREUConvocatoria 
   BackColor       =   &H00808000&
   Caption         =   "Convocatoria"
   ClientHeight    =   7350
   ClientLeft      =   6660
   ClientTop       =   1260
   ClientWidth     =   8385
   Icon            =   "frmREUConvocatoria.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7350
   ScaleWidth      =   8385
   Begin VB.OptionButton optImpreso 
      BackColor       =   &H00808000&
      Caption         =   "Carta"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   240
      TabIndex        =   19
      Top             =   5760
      Width           =   1200
   End
   Begin VB.Frame frmCarta 
      BackColor       =   &H00808000&
      Height          =   735
      Left            =   240
      TabIndex        =   14
      Top             =   5760
      Width           =   7935
      Begin VB.TextBox txtDot 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   255
         TabIndex        =   16
         Top             =   240
         Width           =   6255
      End
      Begin VB.CommandButton cmdDot 
         Height          =   285
         Left            =   7500
         Picture         =   "frmREUConvocatoria.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   240
         Width           =   315
      End
      Begin VB.Label lblPlantilla2 
         BackColor       =   &H00808000&
         Caption         =   "Plantilla:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   210
         Left            =   405
         TabIndex        =   17
         Top             =   300
         Width           =   810
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4500
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   6780
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   3300
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   6780
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPer 
      Height          =   2025
      Left            =   60
      TabIndex        =   2
      Top             =   240
      Width           =   8235
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   16
      stylesets.count =   1
      stylesets(0).Name=   "BajaLog"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(0).Picture=   "frmREUConvocatoria.frx":0D71
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterPos     =   3
      SplitterVisible =   -1  'True
      Columns.Count   =   16
      Columns(0).Width=   1032
      Columns(0).Caption=   "S�/No"
      Columns(0).Name =   "ASIG"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   1535
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Caption=   "Apellidos"
      Columns(2).Name =   "APE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   2011
      Columns(3).Caption=   "Nombre"
      Columns(3).Name =   "NOM"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   2355
      Columns(4).Caption=   "Cargo"
      Columns(4).Name =   "CAR"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   1958
      Columns(5).Caption=   "Tfno."
      Columns(5).Name =   "TFNO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   2117
      Columns(6).Caption=   "Fax"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   2540
      Columns(7).Caption=   "Email"
      Columns(7).Name =   "EMAIL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   2461
      Columns(8).Caption=   "Departamento"
      Columns(8).Name =   "DEP"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   1138
      Columns(9).Caption=   "UON1"
      Columns(9).Name =   "UON1"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   1111
      Columns(10).Caption=   "UON2"
      Columns(10).Name=   "UON2"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   1164
      Columns(11).Caption=   "UON3"
      Columns(11).Name=   "UON3"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(11).Locked=   -1  'True
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "BAJALOG"
      Columns(12).Name=   "BAJALOG"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   11
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "IDI"
      Columns(13).Name=   "IDI"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "TIPOMAIL"
      Columns(14).Name=   "TIPOMAIL"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "DEPDEN"
      Columns(15).Name=   "DEPDEN"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      _ExtentX        =   14526
      _ExtentY        =   3572
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgComp 
      Height          =   1785
      Left            =   60
      TabIndex        =   3
      Top             =   2400
      Width           =   8235
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Col.Count       =   11
      stylesets.count =   1
      stylesets(0).Name=   "BajaLog"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   -1  'True
      EndProperty
      stylesets(0).Picture=   "frmREUConvocatoria.frx":0D8D
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterPos     =   4
      SplitterVisible =   -1  'True
      Columns.Count   =   11
      Columns(0).Width=   1032
      Columns(0).Caption=   "S�/No"
      Columns(0).Name =   "ASIG"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   873
      Columns(1).Caption=   "Eqp"
      Columns(1).Name =   "EQP"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1773
      Columns(2).Caption=   "C�digo"
      Columns(2).Name =   "COD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   2831
      Columns(3).Caption=   "Apellidos"
      Columns(3).Name =   "APE"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(4).Width=   1799
      Columns(4).Caption=   "Nombre"
      Columns(4).Name =   "NOM"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(5).Width=   2064
      Columns(5).Caption=   "Tfno."
      Columns(5).Name =   "TFNO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   1799
      Columns(6).Caption=   "Fax"
      Columns(6).Name =   "FAX"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(7).Width=   2831
      Columns(7).Caption=   "EMail"
      Columns(7).Name =   "EMAIL"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "BAJALOG"
      Columns(8).Name =   "BAJALOG"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "IDI"
      Columns(9).Name =   "IDI"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "TIPOEMAIL"
      Columns(10).Name=   "TIPOEMAIL"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      _ExtentX        =   14526
      _ExtentY        =   3149
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   1980
      Top             =   60
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.OptionButton optMail 
      BackColor       =   &H00808000&
      Caption         =   "Html"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   240
      TabIndex        =   18
      Top             =   4440
      Value           =   -1  'True
      Width           =   1200
   End
   Begin VB.Frame frmMail 
      BackColor       =   &H00808000&
      Height          =   1215
      Left            =   240
      TabIndex        =   6
      Top             =   4440
      Width           =   7935
      Begin VB.CommandButton cmdMailTxt 
         Height          =   285
         Left            =   7485
         Picture         =   "frmREUConvocatoria.frx":0DA9
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   780
         Width           =   315
      End
      Begin VB.TextBox txtMailText 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1185
         MaxLength       =   255
         TabIndex        =   9
         Top             =   780
         Width           =   6255
      End
      Begin VB.TextBox txtMailHtml 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   255
         TabIndex        =   8
         Top             =   360
         Width           =   6255
      End
      Begin VB.CommandButton cmdMailHtml 
         Height          =   285
         Left            =   7500
         Picture         =   "frmREUConvocatoria.frx":0E68
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcIdiomas 
         Height          =   285
         Left            =   1200
         TabIndex        =   11
         Top             =   0
         Width           =   1455
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   6773
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "ID"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "OFFSET"
         Columns(2).Name =   "OFFSET"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   2566
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblEspacio 
         BackColor       =   &H00808000&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   2640
         TabIndex        =   20
         Top             =   0
         Width           =   210
      End
      Begin VB.Label lblPlantillaTxt 
         BackColor       =   &H00808000&
         Caption         =   "Plantilla:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   510
         TabIndex        =   13
         Top             =   840
         Width           =   810
      End
      Begin VB.Label lblPlantillaHtml 
         BackColor       =   &H00808000&
         Caption         =   "Plantilla:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   525
         TabIndex        =   12
         Top             =   420
         Width           =   810
      End
   End
   Begin VB.Label lblComp 
      BackColor       =   &H00808000&
      Caption         =   "Compradores"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   60
      TabIndex        =   5
      Top             =   2220
      Width           =   1755
   End
   Begin VB.Label lblPersonas 
      BackColor       =   &H00808000&
      Caption         =   "Personas implicadas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   30
      Width           =   1755
   End
   Begin VB.Shape Shape1 
      BorderColor     =   &H00FFFFFF&
      Height          =   2355
      Left            =   60
      Top             =   4320
      Width           =   8235
   End
End
Attribute VB_Name = "frmREUConvocatoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private WordVersion As Integer
Public iNumTotal As Integer 'Variable que contendr� el numero de convocatorias que se obtienen
Private bSesionIniciada As Boolean
Private oFos As Scripting.FileSystemObject
Private sIdioma() As String
' parche hoja convocatoria
Dim oPers As CPersonas
Dim oPer As CPersona

Private oIdiomas As CIdiomas
Private splantilla(3) As String
Private sPlantillaTxt(3) As String
Private sPlantillaHtm(3) As String
''' <summary>
''' Manda los mails de convocatoria
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim i As Integer
    Dim j As Integer
    Dim bActivarWord As Boolean
    Dim docword As Object
    Dim iNumParcial As Integer
    Dim appword As Object
    Dim bSalvar As Boolean
    Dim errormail As TipoErrorSummit
    Dim sCuerpo As String
    Dim sIdiomaMail() As String
    Dim sMailSubject() As String
    Dim sSubject As String
    Dim sMailSubjectFrom() As String
    Dim sSubjectFrom As String
    Dim sFileNameH() As String
    Dim sFileNameT() As String
    Dim iResult As Integer
    Dim bHTML As Boolean
    Dim oIdioma As CIdioma
    Dim ADORsUsuario As Ador.Recordset
    Dim oEmailReuConvo As CEmailReuConvo
    
    On Error GoTo Error:
    
    Select Case Me.sdbcIdiomas.Value
        Case "SPA"
            sPlantillaHtm(0) = Me.txtMailHtml.Text
            sPlantillaTxt(0) = Me.txtMailText.Text
        Case "ENG"
            sPlantillaHtm(1) = Me.txtMailHtml.Text
            sPlantillaTxt(1) = Me.txtMailText.Text
        Case "GER"
            sPlantillaHtm(2) = Me.txtMailHtml.Text
            sPlantillaTxt(2) = Me.txtMailText.Text
        Case "FRA"
            sPlantillaHtm(3) = Me.txtMailHtml.Text
            sPlantillaTxt(3) = Me.txtMailText.Text
    End Select
    
    'Comprobar las plantillas
    If optMail Then
        'DevolverInformacionMailManda usa StrToSQLNULL que traduce los "" por null
        For i = 0 To UBound(sPlantillaHtm) - 1
            If sPlantillaHtm(i) <> "" Then
                If LCase(Right(sPlantillaHtm(i), 3)) <> "htm" Then
                    oMensajes.NoValido sIdioma(1)
                    If Me.Visible Then txtMailHtml.SetFocus
                    Exit Sub
                End If
                
                If Not oFos.FileExists(sPlantillaHtm(i)) Then
                    oMensajes.PlantillaNoEncontrada sPlantillaHtm(i)
                    If Me.Visible Then txtMailHtml.SetFocus
                    Exit Sub
                End If
            End If
        Next

        For i = 0 To UBound(sPlantillaTxt) - 1
            If sPlantillaTxt(i) <> "" Then
                If LCase(Right(sPlantillaTxt(i), 3)) <> "txt" Then
                    oMensajes.NoValido sIdioma(1)
                    If Me.Visible Then txtMailText.SetFocus
                    Exit Sub
                End If
                
                If Not oFos.FileExists(sPlantillaTxt(i)) Then
                    oMensajes.PlantillaNoEncontrada sPlantillaTxt(i)
                    If Me.Visible Then txtMailText.SetFocus
                    Exit Sub
                End If
            End If
        Next
    Else
        If Right(txtDOT, 3) <> "dot" Then
            oMensajes.NoValido sIdioma(1)
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        End If
        
        If Not oFos.FileExists(txtDOT) Then
            oMensajes.PlantillaNoEncontrada txtDOT
            If Me.Visible Then txtDOT.SetFocus
            Exit Sub
        End If
    End If
        
    Screen.MousePointer = vbHourglass
    
    frmESPERA.lblGeneral.caption = sIdioma(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sIdioma(3)
    
    DoEvents
    
    Set appword = CreateObject("Word.Application")
    bSalvar = appword.Options.SavePropertiesPrompt
    appword.Options.SavePropertiesPrompt = False
    
    ReDim sIdiomaMail(0)
    ReDim sMailSubject(0)
    ReDim sMailSubjectFrom(0)
    ReDim sFileNameH(0)
    ReDim sFileNameT(0)
    
    If Not Me.optImpreso Then
        Set ADORsUsuario = frmREU.oGestorReuniones.DevolverInformacionMailManda(oUsuarioSummit.Cod, sPlantillaHtm, sPlantillaTxt)

        If ADORsUsuario.EOF Then
            ' -1 Fallo Pargen_Ins y Pargen_Dot. Deber�a ser imposible
            Set ADORsUsuario = Nothing
                    
            errormail = ComponerMensaje(sdbgComp.Columns("EMAIL").Text, "Convocatoria DevolverInformacionMail.eof", "", _
                                entidadNotificacion:=entidadNotificacion.ProcesoCompra, _
                                tipoNotificacion:=TipoNotificacionEmail.ConvocatoriaReunion)
            If errormail.NumError <> TESnoerror Then TratarError errormail
            
            Unload frmESPERA
            Set appword = Nothing
            Screen.MousePointer = vbNormal
            
            Exit Sub
        Else
            While Not ADORsUsuario.EOF
                ReDim Preserve sIdiomaMail(UBound(sIdiomaMail) + 1)
                ReDim Preserve sMailSubject(UBound(sMailSubject) + 1)
                ReDim Preserve sMailSubjectFrom(UBound(sMailSubjectFrom) + 1)
                ReDim Preserve sFileNameH(UBound(sFileNameH) + 1)
                ReDim Preserve sFileNameT(UBound(sFileNameT) + 1)
                
                sIdiomaMail(UBound(sIdiomaMail)) = ADORsUsuario.Fields("IDIOMA")
                sMailSubject(UBound(sMailSubject)) = ADORsUsuario.Fields("SUBJECT")
                sMailSubjectFrom(UBound(sMailSubjectFrom)) = ADORsUsuario.Fields("SUBJECTFROM")
                sFileNameH(UBound(sFileNameH)) = ADORsUsuario.Fields("PLANTILLA_H")
                sFileNameT(UBound(sFileNameT)) = ADORsUsuario.Fields("PLANTILLA_T")
                    
                ADORsUsuario.MoveNext
            Wend
        End If
    End If
    
    i = 0
    sdbgComp.MoveFirst
    'Primero los compradores
    
    iNumParcial = 0
    
    While i < sdbgComp.Rows
        iNumParcial = iNumParcial + 1
        
        If Int(sdbgComp.Columns("ASIG").Value) = 1 Or Int(sdbgComp.Columns("ASIG").Value) = -1 Then
            If optMail Then
                sCuerpo = ""
                sSubject = "Convocatoria No existe idioma"
                iResult = -1
                sSubjectFrom = ""
                
                For j = 1 To UBound(sIdiomaMail)
                    If sdbgComp.Columns("IDI").Value = sIdiomaMail(j) Then
                        sSubject = sMailSubject(j)
                        sSubjectFrom = sMailSubjectFrom(j)
                                                
                        Set oEmailReuConvo = GenerarCEmailReuConvo(oMensajes, frmREU.oGestorReuniones)
                        If sdbgComp.Columns("TIPOEMAIL").Value = 1 Then
                            iResult = oEmailReuConvo.GenerarMensajeConvocatoriaComprador(sFileNameH(j), bHTML, sCuerpo, frmREU.sdbcFecReu.Columns(0).Value, frmREU.lblref, sdbgComp.Columns("NOM").Value, _
                                        sdbgComp.Columns("APE").Value, sdbgComp.Columns("COD").Value, sdbgComp.Columns("COD").Text, sdbgComp.Columns("TFNO").Value, sdbgComp.Columns("FAX").Value, _
                                        sdbgComp.Columns("EMAIL").Value, sdbgComp.Columns("EQP").Value, sdbgComp.Columns("EQP").Text, oPers, frmREU.oReuSeleccionada.Procesos, sIdioma)
                            bHTML = True
                        Else
                            iResult = oEmailReuConvo.GenerarMensajeConvocatoriaComprador(sFileNameT(j), bHTML, sCuerpo, frmREU.sdbcFecReu.Columns(0).Value, frmREU.lblref, sdbgComp.Columns("NOM").Value, _
                                        sdbgComp.Columns("APE").Value, sdbgComp.Columns("COD").Value, sdbgComp.Columns("COD").Text, sdbgComp.Columns("TFNO").Value, sdbgComp.Columns("FAX").Value, _
                                        sdbgComp.Columns("EMAIL").Value, sdbgComp.Columns("EQP").Value, sdbgComp.Columns("EQP").Text, oPers, frmREU.oReuSeleccionada.Procesos, sIdioma)
                            bHTML = False
                        End If
                        Set oEmailReuConvo = Nothing
                        
                        Exit For
                    End If
                Next
                
                ' -1 No esta en el bucle
                ' -2 PlantillaNoEncontrada
                '  0 Si TODOS SUS procesos est�n cerrados no se saca hoja/mail de convocatoria
                '  1 Todo bien
                
                If iResult = 0 Then
                ElseIf iResult = -1 Or iResult = -2 Then
                    errormail = ComponerMensaje(sdbgComp.Columns("EMAIL").Text, sSubject, sCuerpo, , , bHTML, , , sSubjectFrom, _
                                    entidadNotificacion:=entidadNotificacion.ProcesoCompra, _
                                    tipoNotificacion:=TipoNotificacionEmail.ConvocatoriaReunion, _
                                    sToName:=sdbgComp.Columns("NOM").Text & " " & sdbgComp.Columns("APE").Text)
                ElseIf iResult = 1 Then
                    If Not bSesionIniciada Or oIdsMail Is Nothing Then
                        frmESPERA.lblContacto = sIdioma(4) & ": " & sdbgComp.Columns(1).Value & " " & sdbgComp.Columns(2).Value
                        frmESPERA.lblContacto.Refresh
                        DoEvents
                        frmESPERA.ProgressBar2.Value = val((iNumParcial / iNumTotal) * 100)
                        frmESPERA.ProgressBar1.Value = 2
                        frmESPERA.lblDetalle = sIdioma(5)
                        frmESPERA.lblDetalle.Refresh
                        Set oIdsMail = IniciarSesionMail
                        bSesionIniciada = True
                    End If
                    
                    frmESPERA.ProgressBar1.Value = 3
                    frmESPERA.lblDetalle = sIdioma(6) & " ..."
                    frmESPERA.lblDetalle.Refresh
                                        
                    If Trim(sdbgComp.Columns("EMAIL").Text) = "" Then
                        oMensajes.AvisoPersonaSinEmail sdbgComp.Columns("NOM").Value & " " & sdbgComp.Columns("APE").Text
                        
                        errormail = ComponerMensaje(sdbgComp.Columns("EMAIL").Text, sSubject, sCuerpo, , , bHTML, , , sSubjectFrom, _
                                            entidadNotificacion:=entidadNotificacion.ProcesoCompra, _
                                            tipoNotificacion:=TipoNotificacionEmail.ConvocatoriaReunion, _
                                            sToName:=sdbgComp.Columns("NOM").Text & " " & sdbgComp.Columns("APE").Text)
                    Else
                        ' enviar mensaje
                        errormail = ComponerMensaje(sdbgComp.Columns("EMAIL").Text, sSubject, sCuerpo, , , bHTML, , , sSubjectFrom, _
                                            entidadNotificacion:=entidadNotificacion.ProcesoCompra, _
                                            tipoNotificacion:=TipoNotificacionEmail.ConvocatoriaReunion, _
                                            sToName:=sdbgComp.Columns("NOM").Text & " " & sdbgComp.Columns("APE").Text)
                    End If
                    
                    frmESPERA.ProgressBar1.Value = 8
                    frmESPERA.lblDetalle = sIdioma(7)
                    frmESPERA.lblDetalle.Refresh
                End If
                
                If errormail.NumError <> TESnoerror Then TratarError errormail
            Else
                bActivarWord = True
                Set docword = GenerarDocumentoImpresoComprador(appword)
                If Not docword Is Nothing Then
                    frmESPERA.lblContacto = sIdioma(4) & ": " & sdbgComp.Columns(1).Value & " " & sdbgComp.Columns(2).Value
                    frmESPERA.lblContacto.Refresh
                    DoEvents
                    frmESPERA.ProgressBar2.Value = val((iNumParcial / iNumTotal) * 100)
                    frmESPERA.ProgressBar1.Value = 2
                    frmESPERA.lblDetalle = sIdioma(6) & " ..."
                    frmESPERA.lblDetalle.Refresh
                End If
                                
            End If
            
        End If
        
        i = i + 1
        
        sdbgComp.MoveNext
    Wend
    
    'Ahora las personas
    i = 0
    sdbgPer.MoveFirst
    
    While i < sdbgPer.Rows
        iNumParcial = iNumParcial + 1
    
        If Int(sdbgPer.Columns("ASIG").Value) = 1 Or Int(sdbgPer.Columns("ASIG").Value) = -1 Then
            If optMail Then
                sCuerpo = ""
                sSubject = "Convocatoria No existe idioma"
                iResult = -1
                
                For j = 1 To UBound(sIdiomaMail)
                    If sdbgPer.Columns("IDI").Value = sIdiomaMail(j) Then
                        sSubject = sMailSubject(j)
                        sSubjectFrom = sMailSubjectFrom(j)
                                                
                        Set oEmailReuConvo = GenerarCEmailReuConvo(oMensajes, frmREU.oGestorReuniones)
                        If sdbgPer.Columns("TIPOMAIL").Value = 1 Then
                            bHTML = True
                            iResult = oEmailReuConvo.GenerarMensajeConvocatoriaPersona(sFileNameH(j), bHTML, sCuerpo, frmREU.sdbcFecReu.Columns(0).Value, frmREU.lblref, sdbgPer.Columns("NOM").Value, _
                                        sdbgPer.Columns("APE").Value, sdbgPer.Columns("COD").Value, sdbgPer.Columns("COD").Text, sdbgPer.Columns("TFNO").Value, sdbgPer.Columns("FAX").Value, _
                                        sdbgPer.Columns("EMAIL").Value, sdbgPer.Columns("CAR").Value, sdbgPer.Columns("DEPDEN").Value, sdbgPer.Columns("UON1").Value, sdbgPer.Columns("UON2").Value, _
                                        sdbgPer.Columns("UON3").Value, oPers, frmREU.oReuSeleccionada.Procesos, sIdioma)
                        Else
                            bHTML = False
                            iResult = oEmailReuConvo.GenerarMensajeConvocatoriaPersona(sFileNameT(j), bHTML, sCuerpo, frmREU.sdbcFecReu.Columns(0).Value, frmREU.lblref, sdbgPer.Columns("NOM").Value, _
                                        sdbgPer.Columns("APE").Value, sdbgPer.Columns("COD").Value, sdbgPer.Columns("COD").Text, sdbgPer.Columns("TFNO").Value, sdbgPer.Columns("FAX").Value, _
                                        sdbgPer.Columns("EMAIL").Value, sdbgPer.Columns("CAR").Value, sdbgPer.Columns("DEPDEN").Value, sdbgPer.Columns("UON1").Value, sdbgPer.Columns("UON2").Value, _
                                        sdbgPer.Columns("UON3").Value, oPers, frmREU.oReuSeleccionada.Procesos, sIdioma)
                        End If
                        Set oEmailReuConvo = Nothing
                        
                        Exit For
                    End If
                Next
                
                ' -1 No esta en el bucle
                ' -2 PlantillaNoEncontrada
                '  0 Si TODOS SUS procesos est�n cerrados no se saca hoja/mail de convocatoria
                '  1 Todo bien
                
                If iResult = 0 Then
                ElseIf iResult = -1 Or iResult = -2 Then
                    errormail = ComponerMensaje(sdbgComp.Columns("EMAIL").Text, sSubject, sCuerpo, , , bHTML, , , sSubjectFrom, _
                                            entidadNotificacion:=entidadNotificacion.ProcesoCompra, _
                                            tipoNotificacion:=TipoNotificacionEmail.ConvocatoriaReunion, _
                                            sToName:=sdbgComp.Columns("NOM").Text & " " & sdbgComp.Columns("APE").Text)
                ElseIf iResult = 1 Then
                    If Not bSesionIniciada Or oIdsMail Is Nothing Then
                        frmESPERA.lblContacto = sIdioma(8) & ": " & sdbgPer.Columns(1).Value & " " & sdbgPer.Columns(2).Value
                        frmESPERA.lblContacto.Refresh
                        DoEvents
                        frmESPERA.ProgressBar2.Value = val((iNumParcial / iNumTotal) * 100)
                        frmESPERA.ProgressBar1.Value = 2
                        frmESPERA.lblDetalle = sIdioma(5)
                        frmESPERA.lblDetalle.Refresh
                        Set oIdsMail = IniciarSesionMail
                        bSesionIniciada = True
                    End If
                        
                    If Trim(sdbgPer.Columns("EMAIL").Text) = "" Then
                        oMensajes.AvisoPersonaSinEmail sdbgPer.Columns("NOM").Value & " " & sdbgPer.Columns("APE").Text
                        
                        errormail = ComponerMensaje(sdbgPer.Columns("EMAIL").Text, sSubject, sCuerpo, , , bHTML, , , sSubjectFrom, _
                                            entidadNotificacion:=entidadNotificacion.ProcesoCompra, _
                                            tipoNotificacion:=TipoNotificacionEmail.ConvocatoriaReunion, _
                                            sToName:=sdbgComp.Columns("NOM").Text & " " & sdbgComp.Columns("APE").Text)
                    Else
                        errormail = ComponerMensaje(sdbgPer.Columns("EMAIL").Text, sSubject, sCuerpo, , , bHTML, , , sSubjectFrom, _
                                            entidadNotificacion:=entidadNotificacion.ProcesoCompra, _
                                            tipoNotificacion:=TipoNotificacionEmail.ConvocatoriaReunion, _
                                            sToName:=sdbgComp.Columns("NOM").Text & " " & sdbgComp.Columns("APE").Text)
                    End If
                    
                    frmESPERA.lblContacto = sIdioma(8) & ": " & sdbgPer.Columns(1).Value & " " & sdbgPer.Columns(2).Value
                    frmESPERA.lblContacto.Refresh
                    DoEvents
                    frmESPERA.ProgressBar2.Value = val((iNumParcial / iNumTotal) * 100)
                    frmESPERA.ProgressBar1.Value = 2
                    frmESPERA.lblDetalle = sIdioma(7)
                    frmESPERA.lblDetalle.Refresh
                End If
                
                If errormail.NumError <> TESnoerror Then TratarError errormail
            Else
                bActivarWord = True
                
                Set docword = GenerarDocumentoImpresoPersona(appword)
                If Not docword Is Nothing Then
                    frmESPERA.lblContacto = sIdioma(8) & ": " & sdbgPer.Columns(1).Value & " " & sdbgPer.Columns(2).Value
                    frmESPERA.lblContacto.Refresh
                    DoEvents
                    frmESPERA.ProgressBar2.Value = val((iNumParcial / iNumTotal) * 100)
                    frmESPERA.ProgressBar1.Value = 3
                    frmESPERA.lblDetalle = sIdioma(6) & " ..."
                    frmESPERA.lblDetalle.Refresh
                End If
            End If
        End If
        
        i = i + 1
        
        sdbgPer.MoveNext
    Wend
    
    If bSesionIniciada Then
        frmESPERA.ProgressBar1.Value = 5
        frmESPERA.lblDetalle = sIdioma(9)
        FinalizarSesionMail
    End If
    
    appword.Options.SavePropertiesPrompt = bSalvar
    If bActivarWord Then
        appword.Visible = True
    End If
    If appword.Visible = False Then
        appword.Quit
    End If
    Set appword = Nothing
    Set docword = Nothing
    
    Unload frmESPERA
    
    Screen.MousePointer = vbNormal
    
    Unload Me
Error:
    Resume Next
End Sub



Private Sub cmdCancelar_Click()
        
    Unload Me
    
    
End Sub

Private Sub cmdDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdioma(10) & " (*.dot)|*.dot"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDOT = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub

Private Function GenerarDocumentoImpresoComprador(appword As Object) As Object
Dim ADORs As Ador.Recordset
Dim docword As Object
Dim rangeword As Object
Dim primeravez As Boolean
Dim bEs As Boolean

On Error GoTo Error:

    'SI ES UN ASISTENTE FIJO SE RELLENA LOS DATOS Y SE SALE DE LA FUNCION
    primeravez = True
    For Each oPer In oPers
        If oPer.Cod = sdbgComp.Columns("COD").Text Then
            ' ES UN ASISTENTE FIJO A REUNIONES
            Set ADORs = frmREU.oGestorReuniones.DevolverProcesosReunionParaPersona(sdbgComp.Columns("COD").Text, CDate(frmREU.sdbcFecReu.Columns(0).Value))
            Dim oProce As CProceso
            For Each oProce In frmREU.oReuSeleccionada.Procesos
                If oProce.Estado < conadjudicaciones Then
                    If primeravez Then
                        primeravez = False
                        ' la primera vez cargamos los datos generales
'                        If optMail Then
'                            Set docword = appword.Documents.Add(txtMailDot.Text)
'                        Else
                        Set docword = appword.Documents.Add(txtDOT.Text)
'                        End If
                        ' DATOS GENERALES
                        DatoAWord docword, "FEC_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "Short Date")
                        DatoAWord docword, "HORA_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "short time") 'Modificaci�n para separar la fecha de la hora
                        DatoAWord docword, "REF_REUNION", frmREU.lblref
                        DatoAWord docword, "NOM_CONVOCADO", sdbgComp.Columns("NOM").Value
                        DatoAWord docword, "APE_CONVOCADO", sdbgComp.Columns("APE").Value
                        DatoAWord docword, "COD_CONVOCADO", sdbgComp.Columns("COD").Value
                        DatoAWord docword, "TFNO_CONVOCADO", sdbgComp.Columns("TFNO").Value
                        DatoAWord docword, "FAX_CONVOCADO", sdbgComp.Columns("FAX").Value
                        DatoAWord docword, "MAIL_CONVOCADO", sdbgComp.Columns("EMAIL").Value
                        docword.Bookmarks("CARGO_CONVOCADO").Range.Text = docword.Bookmarks("CARGO_CONVOCADO").Range.Text & oPer.Cargo 'sdbgPer.Columns("CAR").Value
                        docword.Bookmarks("DPTO_CONVOCADO").Range.Text = docword.Bookmarks("DPTO_CONVOCADO").Range.Text & oPer.CodDep 'sdbgPer.Columns("DEP").Value
                        docword.Bookmarks("UO1_CONVOCADO").Range.Text = docword.Bookmarks("UO1_CONVOCADO").Range.Text & oPer.UON1 'sdbgPer.Columns("UON1").Value
                        docword.Bookmarks("UO2_CONVOCADO").Range.Text = docword.Bookmarks("UO2_CONVOCADO").Range.Text & oPer.UON2 'sdbgPer.Columns("UON2").Value
                        docword.Bookmarks("UO3_CONVOCADO").Range.Text = docword.Bookmarks("UO3_CONVOCADO").Range.Text & oPer.UON3 'sdbgPer.Columns("UON3").Value
                        docword.Bookmarks("EQP_CONVOCADO").Range.Text = ""
                        
                        If docword.Bookmarks.Exists("PROCESO") = True Then
                            Set rangeword = docword.Bookmarks("PROCESO").Range
                            rangeword.Copy
                            docword.Bookmarks("PROCESO").Select
                            docword.Application.Selection.Delete
                        End If
                    End If
                    docword.Application.Selection.Paste
                    docword.Bookmarks("ANYO_PROCESO").Range.Text = oProce.Anyo
                    docword.Bookmarks("GMN1_PROCESO").Range.Text = oProce.GMN1Cod
                    docword.Bookmarks("COD_PROCESO").Range.Text = oProce.Cod
                    docword.Bookmarks("DEN_PROCESO").Range.Text = oProce.Den
                    If oProce.Reudec Then
                        docword.Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(11)
                    Else
                        docword.Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(12)
                    End If
                    If docword.Bookmarks.Exists("RESP_PROCESO") = True Then
                        ADORs.MoveFirst
                        bEs = False
                        Do While Not ADORs.EOF
                            If ADORs("ANYO").Value = oProce.Anyo And ADORs("GMN1").Value = oProce.GMN1Cod And ADORs("COD").Value = oProce.Cod Then
                                bEs = True
                                Exit Do
                            End If
                            ADORs.MoveNext
                        Loop
                        If bEs Then
                            If NullToStr(ADORs("COM").Value) = sdbgComp.Columns("COD").Value Then
                                docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(11)
                            Else
                                docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(12)
                            End If
                        Else
                            docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(12)
                        End If
                    End If
                    docword.Bookmarks("HORA_PROCESO").Range.Text = Format(oProce.HoraEnReunion, "Short Time")
                        
                End If
            Next
            ADORs.Close
            Set GenerarDocumentoImpresoComprador = docword
            Exit Function
        End If
    Next
    
    'SI NO ES UN ASISTENTE FIJO A REUNIONES
    Set ADORs = frmREU.oGestorReuniones.DevolverProcesosReunionParaComprador(sdbgComp.Columns("EQP").Text, sdbgComp.Columns("COD").Text, CDate(frmREU.sdbcFecReu.Columns(0).Value))
    If ADORs Is Nothing Or ADORs.RecordCount = 0 Then
        Set GenerarDocumentoImpresoComprador = Nothing
        Set ADORs = Nothing
        Exit Function
    End If
'    If optMail Then
'        Set docword = appword.Documents.Add(txtMailDot.Text)
'    Else
        Set docword = appword.Documents.Add(txtDOT.Text)
'    End If

        
        
        
        With docword
            If .Bookmarks.Exists("FEC_REUNION") = True Then
                DatoAWord docword, "FEC_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "Short Date")
            End If
            If .Bookmarks.Exists("HORA_REUNION") = True Then 'Modificaci�n para poner la hora a parte de la fecha
                DatoAWord docword, "HORA_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "short time")
            End If
            If .Bookmarks.Exists("REF_REUNION") = True Then
                DatoAWord docword, "REF_REUNION", frmREU.lblref
            End If
            If .Bookmarks.Exists("NOM_CONVOCADO") = True Then
                DatoAWord docword, "NOM_CONVOCADO", sdbgComp.Columns("NOM").Value
            End If
            If .Bookmarks.Exists("APE_CONVOCADO") = True Then
                DatoAWord docword, "APE_CONVOCADO", sdbgComp.Columns("APE").Value
            End If
            If .Bookmarks.Exists("COD_CONVOCADO") = True Then
                DatoAWord docword, "COD_CONVOCADO", sdbgComp.Columns("COD").Value
            End If
            If .Bookmarks.Exists("TFNO_CONVOCADO") = True Then
                DatoAWord docword, "TFNO_CONVOCADO", sdbgComp.Columns("TFNO").Value
            End If
            If .Bookmarks.Exists("FAX_CONVOCADO") = True Then
                DatoAWord docword, "FAX_CONVOCADO", sdbgComp.Columns("FAX").Value
            End If
            If .Bookmarks.Exists("MAIL_CONVOCADO") = True Then
                DatoAWord docword, "MAIL_CONVOCADO", sdbgComp.Columns("EMAIL").Value
            End If
            If .Bookmarks.Exists("EQP_CONVOCADO") = True Then
                .Bookmark("EQP_CONVOCADO").Range.Text = .Bookmarks("EQP_CONVOCADO").Range.Text & sdbgComp.Columns("EQP").Value
            End If
            ' Para estos bookmarks no hay informaci�n en el caso de que el convocado sea un comprador
            If .Bookmarks.Exists("CARGO_CONVOCADO") = True Then
                .Bookmarks("CARGO_CONVOCADO").Range.Text = ""
            End If
            If .Bookmarks.Exists("UO1_CONVOCADO") = True Then
                .Bookmarks("UO1_CONVOCADO").Range.Text = ""
            End If
            If .Bookmarks.Exists("UO2_CONVOCADO") = True Then
                .Bookmarks("UO2_CONVOCADO").Range.Text = ""
            End If
            If .Bookmarks.Exists("UO3_CONVOCADO") = True Then
                .Bookmarks("UO3_CONVOCADO").Range.Text = ""
            End If
            If .Bookmarks.Exists("DPTO_CONVOCADO") = True Then
                .Bookmarks("DPTO_CONVOCADO").Range.Text = ""
            End If
            
            'Generamos la tabla de procesos
            If .Bookmarks.Exists("PROCESO") = True Then
                Set rangeword = .Bookmarks("PROCESO").Range
                rangeword.Copy
                .Bookmarks("PROCESO").Select
                .Application.Selection.Delete
                         
                'Rellenamos la tabla para cada proceso
                While Not ADORs.EOF
                    .Application.Selection.Paste
                    If .Bookmarks.Exists("ANYO_PROCESO") = True Then
                        .Bookmarks("ANYO_PROCESO").Range.Text = ADORs("ANYO").Value
                    End If
                    If .Bookmarks.Exists("GMN1_PROCESO") = True Then
                        .Bookmarks("GMN1_PROCESO").Range.Text = ADORs("GMN1").Value
                    End If
                    If .Bookmarks.Exists("COD_PROCESO") = True Then
                        .Bookmarks("COD_PROCESO").Range.Text = ADORs("COD").Value
                    End If
                    If .Bookmarks.Exists("DEN_PROCESO") = True Then
                        .Bookmarks("DEN_PROCESO").Range.Text = ADORs("DEN").Value
                    End If
                    If .Bookmarks.Exists("TIPO_PROCESO") = True Then
                        If Int(ADORs("DEC").Value) = 1 Or Int(ADORs("DEC").Value) = -1 Then
                            .Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(11)
                        Else
                            .Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(12)
                        End If
                    End If
                    If .Bookmarks.Exists("RESP_PROCESO") = True Then
                        If ADORs("EQP").Value = sdbgComp.Columns("EQP").Value _
                        And ADORs("COM").Value = sdbgComp.Columns("COD").Value Then
                            .Bookmarks("RESP_PROCESO").Range.Text = sIdioma(11)
                        Else
                            .Bookmarks("RESP_PROCESO").Range.Text = sIdioma(12)
                        End If
                    End If
                    If .Bookmarks.Exists("HORA_PROCESO") = True Then
                        .Bookmarks("HORA_PROCESO").Range.Text = Format(ADORs("HORA").Value, "Short Time")
                    End If
                             
                               
                ADORs.MoveNext

                Wend
                     
            ADORs.Close
            Set ADORs = Nothing
            End If
        End With
        Set GenerarDocumentoImpresoComprador = docword

        Exit Function

Error:

        Resume Next
        
End Function

Private Function GenerarDocumentoImpresoPersona(appword As Object) As Object
Dim ADORs As Ador.Recordset
Dim docword As Object
Dim rangeword As Object
Dim primeravez As Boolean
Dim bEs As Boolean

On Error GoTo Error:

    'SI ES UN ASISTENTE FIJO SE RELLENA LOS DATOS Y SE SALE DE LA FUNCION
    primeravez = True
    For Each oPer In oPers

        If oPer.Cod = sdbgPer.Columns("COD").Text Then
            Set ADORs = frmREU.oGestorReuniones.DevolverProcesosReunionParaPersona(sdbgPer.Columns("COD").Text, CDate(frmREU.sdbcFecReu.Columns(0).Value))
            ' ES UN ASISTENTE FIJO A REUNIONES
            Dim oProce As CProceso
            For Each oProce In frmREU.oReuSeleccionada.Procesos
                If oProce.Estado < conadjudicaciones Then
                    If primeravez Then
                        primeravez = False
                        ' la primera vez cargamos los datos generales
'                        If optMail Then
'                            Set docword = appword.Documents.Add(txtMailDot.Text)
'                        Else
                            Set docword = appword.Documents.Add(txtDOT.Text)
'                        End If
                        ' DATOS GENERALES
                        DatoAWord docword, "FEC_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "Short Date")
                        DatoAWord docword, "HORA_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "short time") 'Modificaci�n para separar la fecha de la hora
                        DatoAWord docword, "REF_REUNION", frmREU.lblref
                        DatoAWord docword, "NOM_CONVOCADO", sdbgPer.Columns("NOM").Value
                        DatoAWord docword, "APE_CONVOCADO", sdbgPer.Columns("APE").Value
                        DatoAWord docword, "COD_CONVOCADO", sdbgPer.Columns("COD").Value
                        DatoAWord docword, "TFNO_CONVOCADO", sdbgPer.Columns("TFNO").Value
                        DatoAWord docword, "FAX_CONVOCADO", sdbgPer.Columns("FAX").Value
                        DatoAWord docword, "MAIL_CONVOCADO", sdbgPer.Columns("EMAIL").Value
                        docword.Bookmarks("CARGO_CONVOCADO").Range.Text = docword.Bookmarks("CARGO_CONVOCADO").Range.Text & sdbgPer.Columns("CAR").Value
                        docword.Bookmarks("DPTO_CONVOCADO").Range.Text = docword.Bookmarks("DPTO_CONVOCADO").Range.Text & sdbgPer.Columns("DEP").Value
                        docword.Bookmarks("UO1_CONVOCADO").Range.Text = docword.Bookmarks("UO1_CONVOCADO").Range.Text & sdbgPer.Columns("UON1").Value
                        docword.Bookmarks("UO2_CONVOCADO").Range.Text = docword.Bookmarks("UO2_CONVOCADO").Range.Text & sdbgPer.Columns("UON2").Value
                        docword.Bookmarks("UO3_CONVOCADO").Range.Text = docword.Bookmarks("UO3_CONVOCADO").Range.Text & sdbgPer.Columns("UON3").Value
                        docword.Bookmarks("EQP_CONVOCADO").Range.Text = ""
                        
                        If docword.Bookmarks.Exists("PROCESO") = True Then
                            Set rangeword = docword.Bookmarks("PROCESO").Range
                            rangeword.Copy
                            docword.Bookmarks("PROCESO").Select
                            docword.Application.Selection.Delete
                        End If
                    End If
                    docword.Application.Selection.Paste
                    docword.Bookmarks("ANYO_PROCESO").Range.Text = oProce.Anyo
                    docword.Bookmarks("GMN1_PROCESO").Range.Text = oProce.GMN1Cod
                    docword.Bookmarks("COD_PROCESO").Range.Text = oProce.Cod
                    docword.Bookmarks("DEN_PROCESO").Range.Text = oProce.Den
                    If oProce.Reudec Then
                        docword.Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(11)
                    Else
                        docword.Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(12)
                    End If
                    If docword.Bookmarks.Exists("RESP_PROCESO") = True Then
                        ADORs.MoveFirst
                        bEs = False
                        Do While Not ADORs.EOF
                            If ADORs("ANYO").Value = oProce.Anyo And ADORs("GMN1").Value = oProce.GMN1Cod And ADORs("COD").Value = oProce.Cod Then
                                bEs = True
                                Exit Do
                            End If
                            ADORs.MoveNext
                        Loop
                        If bEs Then
                            If NullToStr(ADORs("COM").Value) = sdbgPer.Columns("COD").Value Then
                                docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(11)
                            Else
                                docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(12)
                            End If
                        Else
                            docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(12)
                        End If
                    End If
                    docword.Bookmarks("HORA_PROCESO").Range.Text = Format(oProce.HoraEnReunion, "Short Time")

                End If
            Next
            ADORs.Close
            Set GenerarDocumentoImpresoPersona = docword
            Exit Function
        End If

    Next
    
'SI NO ES UN ASISTENTE FIJO A REUNIONES
Set ADORs = frmREU.oGestorReuniones.DevolverProcesosReunionParaPersona(sdbgPer.Columns("COD").Text, CDate(frmREU.sdbcFecReu.Columns(0).Value))
If ADORs Is Nothing Or ADORs.RecordCount = 0 Then
    Set GenerarDocumentoImpresoPersona = Nothing
    Set ADORs = Nothing
    Exit Function
Else
'    If optMail Then
'        Set docword = appword.Documents.Add(txtMailDot.Text)
'    Else
    Set docword = appword.Documents.Add(txtDOT.Text)
'    End If
    With docword

            ' DATOS GENERALES
        If .Bookmarks.Exists("FEC_REUNION") = True Then
            DatoAWord docword, "FEC_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "Short Date")
        End If
        If .Bookmarks.Exists("HORA_REUNION") = True Then ' Modificaci�n para separar la fecha de la hora
            DatoAWord docword, "HORA_REUNION", Format(frmREU.sdbcFecReu.Columns(0).Value, "short time")
        End If
        If .Bookmarks.Exists("REF_REUNION") = True Then
            DatoAWord docword, "REF_REUNION", frmREU.lblref
        End If
        If .Bookmarks.Exists("NOM_CONVOCADO") = True Then
            DatoAWord docword, "NOM_CONVOCADO", sdbgPer.Columns("NOM").Value
        End If
        If .Bookmarks.Exists("APE_CONVOCADO") = True Then
            DatoAWord docword, "APE_CONVOCADO", sdbgPer.Columns("APE").Value
        End If
        If .Bookmarks.Exists("COD_CONVOCADO") = True Then
            DatoAWord docword, "COD_CONVOCADO", sdbgPer.Columns("COD").Value
        End If
        If .Bookmarks.Exists("TFNO_CONVOCADO") = True Then
            DatoAWord docword, "TFNO_CONVOCADO", sdbgPer.Columns("TFNO").Value
        End If
        If .Bookmarks.Exists("FAX_CONVOCADO") = True Then
            DatoAWord docword, "FAX_CONVOCADO", sdbgPer.Columns("FAX").Value
        End If
        If .Bookmarks.Exists("MAIL_CONVOCADO") = True Then
            DatoAWord docword, "MAIL_CONVOCADO", sdbgPer.Columns("EMAIL").Value
        End If
        If .Bookmarks.Exists("CARGO_CONVOCADO") = True Then
            .Bookmarks("CARGO_CONVOCADO").Range.Text = .Bookmarks("CARGO_CONVOCADO").Range.Text & sdbgPer.Columns("CAR").Value
        End If
        If .Bookmarks.Exists("DPTO_CONVOCADO") = True Then
            .Bookmarks("DPTO_CONVOCADO").Range.Text = .Bookmarks("DPTO_CONVOCADO").Range.Text & sdbgPer.Columns("DEP").Value
        End If
        If .Bookmarks.Exists("UO1_CONVOCADO") = True Then
            .Bookmarks("UO1_CONVOCADO").Range.Text = .Bookmarks("UO1_CONVOCADO").Range.Text & sdbgPer.Columns("UON1").Value
        End If
        If .Bookmarks.Exists("UO2_CONVOCADO") = True Then
            .Bookmarks("UO2_CONVOCADO").Range.Text = .Bookmarks("UO2_CONVOCADO").Range.Text & sdbgPer.Columns("UON2").Value
        End If
        If .Bookmarks.Exists("UO3_CONVOCADO") = True Then
            .Bookmarks("UO3_CONVOCADO").Range.Text = .Bookmarks("UO3_CONVOCADO").Range.Text & sdbgPer.Columns("UON3").Value
        End If
        If .Bookmarks.Exists("EQP_CONVOCADO") = True Then
            .Bookmarks("EQP_CONVOCADO").Range.Text = ""
        End If
        If .Bookmarks.Exists("PROCESO") = True Then
            Set rangeword = .Bookmarks("PROCESO").Range
            rangeword.Copy
            .Bookmarks("PROCESO").Select
            .Application.Selection.Delete
            'Rellenamos la tabla para cada proceso
            While Not ADORs.EOF
                 .Application.Selection.Paste
                If .Bookmarks.Exists("ANYO_PROCESO") = True Then
                    .Bookmarks("ANYO_PROCESO").Range.Text = ADORs("ANYO").Value
                End If
                If .Bookmarks.Exists("GMN1_PROCESO") = True Then
                    .Bookmarks("GMN1_PROCESO").Range.Text = ADORs("GMN1").Value
                End If
                If .Bookmarks.Exists("COD_PROCESO") = True Then
                    .Bookmarks("COD_PROCESO").Range.Text = ADORs("COD").Value
                End If
                If .Bookmarks.Exists("DEN_PROCESO") = True Then
                    .Bookmarks("DEN_PROCESO").Range.Text = ADORs("DEN").Value
                End If
                If .Bookmarks.Exists("TIPO_PROCESO") = True Then
                    If Int(ADORs("DEC").Value) = 1 Or Int(ADORs("DEC").Value) = -1 Then
                        .Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(11)
                    Else
                        .Bookmarks("TIPO_PROCESO").Range.Text = sIdioma(12)
                    End If
                End If
                If docword.Bookmarks.Exists("RESP_PROCESO") = True Then
                    If NullToStr(ADORs("COM").Value) = sdbgPer.Columns("COD").Value Then
                        docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(11)
                    Else
                        docword.Bookmarks("RESP_PROCESO").Range.Text = sIdioma(12)
                    End If
                End If
                If .Bookmarks.Exists("HORA_PROCESO") = True Then
                    .Bookmarks("HORA_PROCESO").Range.Text = Format(ADORs("HORA").Value, "Short Time")
                End If
                ADORs.MoveNext
            Wend
            ADORs.Close
            Set ADORs = Nothing

        End If 'If .bookmarks.Exists("PROCESO") = True Then
    End With
End If
                


Set GenerarDocumentoImpresoPersona = docword

Exit Function

Error:

        Resume Next
        
End Function

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_REUCONVOCATORIA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ReDim sIdioma(1 To 12)
        For i = 1 To 12
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        lblPlantilla2.caption = sIdioma(1) & ":"
        sdbgComp.Columns(0).caption = sIdioma(11) & "/" & sIdioma(12)
        sdbgPer.Columns(0).caption = sIdioma(11) & "/" & sIdioma(12)
        cmdAceptar.caption = Ador(0).Value '13
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmREUConvocatoria.caption = Ador(0).Value '15
        Ador.MoveNext
        LblComp.caption = Ador(0).Value
        Ador.MoveNext
        lblPersonas.caption = Ador(0).Value
        Ador.MoveNext
        optImpreso.caption = Ador(0).Value
        Ador.MoveNext
        sdbgComp.Columns(7).caption = Ador(0).Value
        sdbgPer.Columns(7).caption = Ador(0).Value
        Ador.MoveNext
        sdbgComp.Columns(1).caption = Ador(0).Value '20
        Ador.MoveNext
        sdbgComp.Columns(2).caption = Ador(0).Value '21 C�digo
        sdbgPer.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgComp.Columns(3).caption = Ador(0).Value '22 Apellidos
        sdbgPer.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgComp.Columns(4).caption = Ador(0).Value '23 Nombre
        sdbgPer.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgComp.Columns(5).caption = Ador(0).Value '24 tfno
        sdbgPer.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbgComp.Columns(6).caption = Ador(0).Value '25 Fax
        sdbgPer.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        sdbgPer.Columns(4).caption = Ador(0).Value '26 Cargo
        Ador.MoveNext
        sdbgPer.Columns(8).caption = Ador(0).Value
        Ador.MoveNext
        Me.optMail.caption = sdbgComp.Columns(7).caption
        Me.lblPlantillaHtml.caption = Ador(0).Value & ":"
        Ador.MoveNext
        Me.lblPlantillaTxt.caption = Ador(0).Value & ":"
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


Private Sub cmdMailHtml_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdioma(10) & " (*.htm)|*.htm"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    Me.txtMailHtml = cmmdDot.filename
    txtMailHtml_Validate (False)
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub

Private Sub cmdMailTxt_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    cmmdDot.Filter = sIdioma(10) & " (*.txt)|*.txt"
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    Me.txtMailText = cmmdDot.filename
    txtMailText_Validate (False)
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub
End Sub


Private Sub Form_Load()
        
    Me.Height = 6975
    Me.Width = 7665
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sdbgPer.Columns(9).caption = gParametrosGenerales.gsABR_UON1
    sdbgPer.Columns(10).caption = gParametrosGenerales.gsABR_UON2
    sdbgPer.Columns(11).caption = gParametrosGenerales.gsABR_UON3
    sdbgPer.AllowColumnSwapping = ssRelocateNotAllowed
    sdbgPer.AllowColumnMoving = ssRelocateNotAllowed
    sdbgComp.AllowColumnSwapping = ssRelocateNotAllowed
    sdbgComp.AllowColumnMoving = ssRelocateNotAllowed
    Set oFos = New FileSystemObject
    bSesionIniciada = False
    'WordVersion = GetWordVersion
    
    If gParametrosGenerales.giMail = 0 Then
        Me.optMail.Enabled = False
        Me.txtMailHtml.Enabled = False
        Me.txtMailText.Enabled = False
        Me.cmdMailHtml.Enabled = False
        Me.cmdMailTxt.Enabled = False
    End If
    
    ' cargamos los asistentes fijos a reuniones, si es uno de ellos se
    Set oPers = oFSGSRaiz.Generar_CPersonas
    oPers.CargarTodosLosAsistentesFijosAReuniones , , , , , , , , TipoOrdenacionPersonas.OrdPorCod
    
    CargarComboIdiomaYAuxiliares
    
End Sub

Private Sub Form_Resize()
    Dim i As Integer
    
    If Me.Height < 6690 Then
        On Error Resume Next
        Me.Height = 6690
        On Error GoTo 0
    End If
    'sdbgPer.Height = Me.Height * 0.29
    sdbgPer.Height = (Me.Height - Me.Shape1.Height - Me.cmdAceptar.Height - 1300) / 2
    
    'sdbgComp.Height = sdbgPer.Height - 200
    sdbgComp.Height = sdbgPer.Height
    
    LblComp.Top = sdbgPer.Top + sdbgPer.Height + 50
    sdbgComp.Top = LblComp.Top + 200
    sdbgPer.Width = Me.Width * 7395 / 7665
    sdbgComp.Width = sdbgPer.Width
    cmdAceptar.Top = Me.Height - 900 '810
    cmdCancelar.Top = cmdAceptar.Top
    Shape1.Top = Me.Height - 3360 '2460
    
    Me.frmMail.Top = Me.Shape1.Top + 140
    Me.frmCarta.Top = Me.frmMail.Top + Me.frmMail.Height + 50
    
    Me.optMail.Top = Me.frmMail.Top - 30
    Me.optImpreso.Top = Me.frmCarta.Top
    
'''    Me.sdbcIdiomas.Top = Me.frmMail.Top + 50
       
'    Me.optMail.Top = Shape1.Top + 120
'    Me.lblPlantillaHtml.Top = optMail.Top
'    Me.txtMailHtml.Top = optMail.Top
'    Me.cmdMailHtml.Top = optMail.Top
'    Me.lblPlantillaTxt.Top = Me.optMail.Top + 450
'    Me.txtMailText.Top = Me.lblPlantillaTxt.Top
'    Me.sdbcIdiomas.Top = Me.lblPlantillaHtml.Top
'    Me.cmdMailTxt.Top = Me.txtMailText.Top
'    optImpreso.Top = Me.txtMailText.Top + 450
'    lblPlantilla2.Top = optImpreso.Top
'    txtDot.Top = optImpreso.Top
'    cmdDot.Top = optImpreso.Top
      
    Shape1.Width = sdbgPer.Width
    
    i = Shape1.Width - Me.optImpreso.Width - 75
    i = i - Me.sdbcIdiomas.Width '- 65
    i = i - Me.lblPlantillaHtml.Width '- 75
    i = i - Me.cmdMailHtml.Width '- 20
    If i <= 0 Then i = 50
    Me.txtDOT.Width = i + 1500
    
    Me.frmMail.Width = Me.Shape1.Width - 380
    Me.frmCarta.Width = Me.Shape1.Width - 380
    
    Me.txtMailHtml.Width = Me.txtDOT.Width
    Me.txtMailText.Width = Me.txtDOT.Width

    Me.cmdDOT.Left = Me.txtDOT.Left + Me.txtDOT.Width + 45
    Me.cmdMailHtml.Left = Me.cmdDOT.Left
    Me.cmdMailTxt.Left = Me.cmdDOT.Left

    i = Shape1.Width - Me.cmdAceptar.Width - Me.cmdCancelar.Width - 65
    i = i / 2
    
    Me.cmdAceptar.Left = 60 + i
    Me.cmdCancelar.Left = Me.cmdAceptar.Left + Me.cmdAceptar.Width + 65
    
End Sub


Private Sub Form_Unload(Cancel As Integer)

    Set oFos = Nothing
    Me.Visible = False
    
End Sub

''' <summary>
''' Tras cambiar de idioma te muestra cual es la plantilla actual para el idioma
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbcIdiomas_CloseUp()
    If Me.sdbcIdiomas.Columns(1).Value = splantilla(0) Then
        Me.txtMailHtml.Text = sPlantillaHtm(0)
        Me.txtMailText.Text = sPlantillaTxt(0)
    ElseIf Me.sdbcIdiomas.Columns(1).Value = splantilla(1) Then
        Me.txtMailHtml.Text = sPlantillaHtm(1)
        Me.txtMailText.Text = sPlantillaTxt(1)
    ElseIf Me.sdbcIdiomas.Columns(1).Value = splantilla(2) Then
        Me.txtMailHtml.Text = sPlantillaHtm(2)
        Me.txtMailText.Text = sPlantillaTxt(2)
    ElseIf Me.sdbcIdiomas.Columns(1).Value = splantilla(3) Then
        Me.txtMailHtml.Text = sPlantillaHtm(3)
        Me.txtMailText.Text = sPlantillaTxt(3)
    End If
End Sub

Private Sub sdbcIdiomas_InitColumnProps()
     sdbcIdiomas.DataFieldList = "Column 1"
     sdbcIdiomas.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbgComp_AfterUpdate(RtnDispErrMsg As Integer)
    If sdbgComp.Columns(0).Value = "1" Or sdbgComp.Columns(0).Value = "-1" Then
        iNumTotal = iNumTotal + 1
    Else
        iNumTotal = iNumTotal - 1
    End If
    
End Sub

Private Sub sdbgComp_Change()
    sdbgComp.Update
End Sub

Private Sub sdbgComp_RowLoaded(ByVal Bookmark As Variant)
    If sdbgComp.Columns("BAJALOG").Value Then
        Dim i As Integer
        For i = 1 To sdbgComp.Columns.Count - 1
            sdbgComp.Columns(i).CellStyleSet "BajaLog"
        Next
    End If
End Sub

Private Sub sdbgPer_AfterUpdate(RtnDispErrMsg As Integer)
    
    If sdbgPer.Columns(0).Value = "1" Or sdbgPer.Columns(0).Value = "-1" Then
        iNumTotal = iNumTotal + 1
    Else
        iNumTotal = iNumTotal - 1
    End If
    
End Sub

Private Sub sdbgPer_BeforeUpdate(Cancel As Integer)
    If sdbgPer.Columns(0).ColChanged And sdbgPer.Columns(0).Value And sdbgPer.Columns("BAJALOG").Value Then
        If oMensajes.PreguntaSiNo(999) <> vbYes Then
            sdbgPer.Columns(0).Value = 0
            Cancel = -1
        End If
    End If
End Sub

Private Sub sdbgPer_Change()
    
    sdbgPer.Update
    
End Sub

Private Sub sdbgPer_RowLoaded(ByVal Bookmark As Variant)
    If sdbgPer.Columns("BAJALOG").Value Then
        Dim i As Integer
        For i = 1 To sdbgPer.Columns.Count - 1
            sdbgPer.Columns(i).CellStyleSet "BajaLog"
        Next
    End If
End Sub

Private Sub CargarComboIdiomaYAuxiliares()
    Dim oIdioma As CIdioma

    Set oIdiomas = oGestorParametros.DevolverIdiomas
    For Each oIdioma In oIdiomas
        sdbcIdiomas.AddItem oIdioma.Den & Chr(m_lSeparador) & oIdioma.Cod & Chr(m_lSeparador) & oIdioma.OFFSET
    Next
        
    splantilla(0) = "SPA"
    splantilla(1) = "ENG"
    splantilla(2) = "GER"
    splantilla(3) = "FRA"
    
    sPlantillaTxt(0) = gParametrosInstalacion.gsConvocatoriaMailTextSPA
    sPlantillaHtm(0) = gParametrosInstalacion.gsConvocatoriaMailHtmlSPA
    If basPublic.gParametrosInstalacion.gIdioma = splantilla(0) Then
        Me.txtMailHtml.Text = sPlantillaHtm(0)
        Me.txtMailText.Text = sPlantillaTxt(0)
        
        Me.sdbcIdiomas.Value = splantilla(0)
    End If
    
    sPlantillaTxt(1) = gParametrosInstalacion.gsConvocatoriaMailTextENG
    sPlantillaHtm(1) = gParametrosInstalacion.gsConvocatoriaMailHtmlENG
    If basPublic.gParametrosInstalacion.gIdioma = splantilla(1) Then
        Me.txtMailHtml.Text = sPlantillaHtm(1)
        Me.txtMailText.Text = sPlantillaTxt(1)
        
        Me.sdbcIdiomas.Text = splantilla(1)
    End If
    
    sPlantillaTxt(2) = gParametrosInstalacion.gsConvocatoriaMailTextGER
    sPlantillaHtm(2) = gParametrosInstalacion.gsConvocatoriaMailHtmlGER
    If basPublic.gParametrosInstalacion.gIdioma = splantilla(2) Then
        Me.txtMailHtml.Text = sPlantillaHtm(2)
        Me.txtMailText.Text = sPlantillaTxt(2)
        
        Me.sdbcIdiomas.Text = splantilla(2)
    End If
    
    sPlantillaTxt(3) = gParametrosInstalacion.gsConvocatoriaMailTextFRA
    sPlantillaHtm(3) = gParametrosInstalacion.gsConvocatoriaMailHtmlFRA
    If basPublic.gParametrosInstalacion.gIdioma = splantilla(3) Then
        Me.txtMailHtml.Text = sPlantillaHtm(3)
        Me.txtMailText.Text = sPlantillaTxt(3)
        
        Me.sdbcIdiomas.Text = splantilla(3)
    End If

    
End Sub

Private Sub txtMailHtml_Validate(Cancel As Boolean)
    If Trim(Me.txtMailHtml.Text) = "" Then
'        oMensajes.NoValido sIdioma(1)
'        Cancel = True
'        Exit Sub
    ElseIf LCase(Right(Me.txtMailHtml.Text, 3)) <> "htm" Then
        oMensajes.NoValido sIdioma(1)
        Cancel = True
        Exit Sub
    End If
    
    Select Case Me.sdbcIdiomas.Value
    Case "SPA"
        sPlantillaHtm(0) = Me.txtMailHtml.Text
    Case "ENG"
        sPlantillaHtm(1) = Me.txtMailHtml.Text
    Case "GER"
        sPlantillaHtm(2) = Me.txtMailHtml.Text
    Case "FRA"
        sPlantillaHtm(3) = Me.txtMailHtml.Text
    End Select
End Sub

Private Sub txtMailText_Validate(Cancel As Boolean)
    If Trim(Me.txtMailText.Text) = "" Then
'        oMensajes.NoValido sIdioma(1)
'        Cancel = True
'        Exit Sub
    ElseIf LCase(Right(Me.txtMailText.Text, 3)) <> "txt" Then
        oMensajes.NoValido sIdioma(1)
        Cancel = True
        Exit Sub
    End If
    
    Select Case Me.sdbcIdiomas.Value
    Case "SPA"
        sPlantillaTxt(0) = Me.txtMailText.Text
    Case "ENG"
        sPlantillaTxt(1) = Me.txtMailText.Text
    Case "GER"
        sPlantillaTxt(2) = Me.txtMailText.Text
    Case "FRA"
        sPlantillaTxt(3) = Me.txtMailText.Text
    End Select
End Sub


