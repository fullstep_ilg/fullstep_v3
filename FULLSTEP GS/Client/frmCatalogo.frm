VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCatalogo 
   Caption         =   "Cat�logo de aprovisionamiento"
   ClientHeight    =   7560
   ClientLeft      =   0
   ClientTop       =   1530
   ClientWidth     =   13590
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCatalogo.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7560
   ScaleWidth      =   13590
   Begin TabDlg.SSTab tabCatalogo 
      Height          =   7440
      Left            =   0
      TabIndex        =   0
      Top             =   60
      Width           =   13545
      _ExtentX        =   23892
      _ExtentY        =   13123
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DCategorias"
      TabPicture(0)   =   "frmCatalogo.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picNavigateCat"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "tvwCategorias"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "chkVerBajas"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmmdAdjun"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "picCategoria"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "DAdjudicaciones"
      TabPicture(1)   =   "frmCatalogo.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbddMonedas"
      Tab(1).Control(1)=   "sdbddUnidades"
      Tab(1).Control(2)=   "sdbddDestinos"
      Tab(1).Control(3)=   "sdbddUnidadesPedido"
      Tab(1).Control(4)=   "picNavigateAdj"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "sdbgAdjudicaciones"
      Tab(1).Control(6)=   "lblLeyenda"
      Tab(1).Control(7)=   "lblCategoria"
      Tab(1).ControlCount=   8
      Begin SSDataWidgets_B.SSDBDropDown sdbddMonedas 
         Height          =   1575
         Left            =   -73920
         TabIndex        =   37
         Top             =   4080
         Width           =   4080
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         MaxDropDownItems=   10
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   2
         stylesets(0).Name=   "Actuales"
         stylesets(0).BackColor=   10944511
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogo.frx":0182
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogo.frx":019E
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   2090
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4260
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Cambio"
         Columns(2).Name =   "Cambio"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   7197
         _ExtentY        =   2778
         _StockProps     =   77
      End
      Begin VB.PictureBox picCategoria 
         BackColor       =   &H00808000&
         Height          =   6570
         Left            =   6795
         ScaleHeight     =   6510
         ScaleWidth      =   6450
         TabIndex        =   33
         Top             =   405
         Width           =   6515
         Begin TabDlg.SSTab tabDatos 
            Height          =   3780
            Left            =   60
            TabIndex        =   38
            Top             =   2640
            Width           =   6360
            _ExtentX        =   11218
            _ExtentY        =   6668
            _Version        =   393216
            Tabs            =   6
            Tab             =   1
            TabsPerRow      =   6
            TabHeight       =   706
            BackColor       =   8421376
            TabCaption(0)   =   "DProveedores"
            TabPicture(0)   =   "frmCatalogo.frx":01BA
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "picProveedores"
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "DCampos de pedido"
            TabPicture(1)   =   "frmCatalogo.frx":01D6
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "picCamposPedido"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            TabCaption(2)   =   "DCampos de recepci�n"
            TabPicture(2)   =   "frmCatalogo.frx":01F2
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "picCamposRecepcion"
            Tab(2).ControlCount=   1
            TabCaption(3)   =   "DCostes"
            TabPicture(3)   =   "frmCatalogo.frx":020E
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "picCostes"
            Tab(3).ControlCount=   1
            TabCaption(4)   =   "DDescuentos"
            TabPicture(4)   =   "frmCatalogo.frx":022A
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "picDescuentos"
            Tab(4).ControlCount=   1
            TabCaption(5)   =   "DPrecios"
            TabPicture(5)   =   "frmCatalogo.frx":0246
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "picModEmision"
            Tab(5).ControlCount=   1
            Begin VB.PictureBox picCamposRecepcion 
               BackColor       =   &H00808000&
               Height          =   3015
               Left            =   -74880
               ScaleHeight     =   2955
               ScaleMode       =   0  'User
               ScaleWidth      =   6037.175
               TabIndex        =   69
               Top             =   540
               Width           =   6150
               Begin SSDataWidgets_B.SSDBGrid sdbgCamposRecepcion 
                  Height          =   2250
                  Left            =   90
                  TabIndex        =   72
                  Top             =   90
                  Width           =   5895
                  _Version        =   196617
                  DataMode        =   2
                  Col.Count       =   13
                  stylesets.count =   3
                  stylesets(0).Name=   "Gris"
                  stylesets(0).BackColor=   12632256
                  stylesets(0).Picture=   "frmCatalogo.frx":0262
                  stylesets(1).Name=   "Normal"
                  stylesets(1).ForeColor=   0
                  stylesets(1).BackColor=   16777215
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCatalogo.frx":027E
                  stylesets(2).Name=   "Selected"
                  stylesets(2).ForeColor=   16777215
                  stylesets(2).BackColor=   8388608
                  stylesets(2).HasFont=   -1  'True
                  BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(2).Picture=   "frmCatalogo.frx":029A
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   -2147483630
                  ForeColorOdd    =   -2147483630
                  BackColorEven   =   -2147483643
                  BackColorOdd    =   -2147483643
                  RowHeight       =   423
                  Columns.Count   =   13
                  Columns(0).Width=   2090
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Locked=   -1  'True
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16776960
                  Columns(1).Width=   4445
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).Locked=   -1  'True
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16776960
                  Columns(2).Width=   900
                  Columns(2).Caption=   "Obl."
                  Columns(2).Name =   "OBL"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Style=   2
                  Columns(3).Width=   3519
                  Columns(3).Caption=   "Valor"
                  Columns(3).Name =   "VAL"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   2672
                  Columns(4).Caption=   "�mbito"
                  Columns(4).Name =   "AMB"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(5).Width=   900
                  Columns(5).Caption=   "Int."
                  Columns(5).Name =   "INT"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(5).Style=   2
                  Columns(6).Width=   3200
                  Columns(6).Visible=   0   'False
                  Columns(6).Caption=   "TIPO_INTRODUCCION"
                  Columns(6).Name =   "TIPO_INTRODUCCION"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(7).Width=   3200
                  Columns(7).Visible=   0   'False
                  Columns(7).Caption=   "TIPO_DATOS"
                  Columns(7).Name =   "TIPO_DATOS"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(8).Width=   3200
                  Columns(8).Visible=   0   'False
                  Columns(8).Caption=   "MIN"
                  Columns(8).Name =   "MIN"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  Columns(9).Width=   3200
                  Columns(9).Visible=   0   'False
                  Columns(9).Caption=   "MAX"
                  Columns(9).Name =   "MAX"
                  Columns(9).DataField=   "Column 9"
                  Columns(9).DataType=   8
                  Columns(9).FieldLen=   256
                  Columns(10).Width=   3200
                  Columns(10).Visible=   0   'False
                  Columns(10).Caption=   "ATRIBID"
                  Columns(10).Name=   "ATRIBID"
                  Columns(10).DataField=   "Column 10"
                  Columns(10).DataType=   8
                  Columns(10).FieldLen=   256
                  Columns(11).Width=   3200
                  Columns(11).Visible=   0   'False
                  Columns(11).Caption=   "ID"
                  Columns(11).Name=   "ID"
                  Columns(11).DataField=   "Column 11"
                  Columns(11).DataType=   8
                  Columns(11).FieldLen=   256
                  Columns(12).Width=   3200
                  Columns(12).Visible=   0   'False
                  Columns(12).Caption=   "LISTA_EXTERNA"
                  Columns(12).Name=   "LISTA_EXTERNA"
                  Columns(12).DataField=   "Column 12"
                  Columns(12).DataType=   11
                  Columns(12).FieldLen=   256
                  _ExtentX        =   10398
                  _ExtentY        =   3969
                  _StockProps     =   79
                  BackColor       =   -2147483643
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddValorR 
                  Height          =   360
                  Left            =   2670
                  TabIndex        =   74
                  Top             =   2490
                  Width           =   3015
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":02B6
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5318
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin VB.CommandButton cmdAnyaCampoR 
                  Caption         =   "A�ad&ir"
                  Height          =   315
                  Left            =   120
                  TabIndex        =   71
                  Top             =   2400
                  Width           =   945
               End
               Begin VB.CommandButton cmdEliCampoR 
                  Caption         =   "Eli&minar"
                  Enabled         =   0   'False
                  Height          =   315
                  Left            =   1200
                  TabIndex        =   70
                  Top             =   2400
                  Width           =   945
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddAmbitoR 
                  Height          =   1575
                  Left            =   0
                  TabIndex        =   73
                  Top             =   0
                  Width           =   4080
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  MaxDropDownItems=   10
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   2
                  stylesets(0).Name=   "Actuales"
                  stylesets(0).BackColor=   10944511
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":02D2
                  stylesets(1).Name=   "Normal"
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCatalogo.frx":02EE
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DEN"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   7197
                  _ExtentY        =   2778
                  _StockProps     =   77
                  DataFieldToDisplay=   "Column 1"
               End
            End
            Begin VB.PictureBox picCostes 
               BackColor       =   &H00808000&
               Height          =   3015
               Left            =   -74910
               ScaleHeight     =   2955
               ScaleMode       =   0  'User
               ScaleWidth      =   6037.175
               TabIndex        =   61
               Top             =   540
               Width           =   6150
               Begin VB.CommandButton cmdA�adirCoste 
                  Caption         =   "A�ad&ir"
                  Height          =   315
                  Left            =   120
                  TabIndex        =   63
                  Top             =   2400
                  Width           =   945
               End
               Begin VB.CommandButton cmdEliminarCoste 
                  Caption         =   "Eli&minar"
                  Height          =   315
                  Left            =   1200
                  TabIndex        =   62
                  Top             =   2400
                  Width           =   945
               End
               Begin SSDataWidgets_B.SSDBGrid sdbgCostesCategoria 
                  Height          =   2130
                  Left            =   120
                  TabIndex        =   64
                  Top             =   120
                  Width           =   5895
                  _Version        =   196617
                  DataMode        =   2
                  Col.Count       =   19
                  stylesets.count =   5
                  stylesets(0).Name=   "ConDescr"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":030A
                  stylesets(1).Name=   "Gris"
                  stylesets(1).BackColor=   -2147483633
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCatalogo.frx":098C
                  stylesets(2).Name=   "Normal"
                  stylesets(2).ForeColor=   0
                  stylesets(2).BackColor=   16777215
                  stylesets(2).HasFont=   -1  'True
                  BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(2).Picture=   "frmCatalogo.frx":09A8
                  stylesets(3).Name=   "HayUniPed"
                  stylesets(3).HasFont=   -1  'True
                  BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(3).Picture=   "frmCatalogo.frx":09C4
                  stylesets(3).AlignmentPicture=   0
                  stylesets(4).Name=   "Selected"
                  stylesets(4).ForeColor=   16777215
                  stylesets(4).BackColor=   8388608
                  stylesets(4).HasFont=   -1  'True
                  BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(4).Picture=   "frmCatalogo.frx":0A28
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   -2147483630
                  ForeColorOdd    =   -2147483630
                  BackColorEven   =   -2147483643
                  BackColorOdd    =   -2147483643
                  RowHeight       =   423
                  Columns.Count   =   19
                  Columns(0).Width=   1773
                  Columns(0).Caption=   "COD"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Locked=   -1  'True
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16776960
                  Columns(1).Width=   4551
                  Columns(1).Caption=   "NOMBRE"
                  Columns(1).Name =   "NOMBRE"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).Locked=   -1  'True
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16776960
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "DESCR"
                  Columns(2).Name =   "DESCR_BTN"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Style=   4
                  Columns(2).ButtonsAlways=   -1  'True
                  Columns(3).Width=   1984
                  Columns(3).Caption=   "OPERACION"
                  Columns(3).Name =   "OPERACION"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   2037
                  Columns(4).Caption=   "VALOR"
                  Columns(4).Name =   "VALOR"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(5).Width=   3916
                  Columns(5).Caption=   "TIPO"
                  Columns(5).Name =   "TIPO"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(6).Width=   2170
                  Columns(6).Caption=   "GRUPO"
                  Columns(6).Name =   "GRUPO"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(7).Width=   3200
                  Columns(7).Visible=   0   'False
                  Columns(7).Caption=   "DESCR"
                  Columns(7).Name =   "DESCR"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(8).Width=   3200
                  Columns(8).Visible=   0   'False
                  Columns(8).Caption=   "INTRO"
                  Columns(8).Name =   "INTRO"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  Columns(9).Width=   3200
                  Columns(9).Visible=   0   'False
                  Columns(9).Caption=   "MIN"
                  Columns(9).Name =   "MIN"
                  Columns(9).DataField=   "Column 9"
                  Columns(9).DataType=   8
                  Columns(9).FieldLen=   256
                  Columns(10).Width=   3200
                  Columns(10).Visible=   0   'False
                  Columns(10).Caption=   "MAX"
                  Columns(10).Name=   "MAX"
                  Columns(10).DataField=   "Column 10"
                  Columns(10).DataType=   8
                  Columns(10).FieldLen=   256
                  Columns(11).Width=   3200
                  Columns(11).Visible=   0   'False
                  Columns(11).Caption=   "ATRIB_ID"
                  Columns(11).Name=   "ATRIB_ID"
                  Columns(11).DataField=   "Column 11"
                  Columns(11).DataType=   8
                  Columns(11).FieldLen=   256
                  Columns(12).Width=   3200
                  Columns(12).Visible=   0   'False
                  Columns(12).Caption=   "ID"
                  Columns(12).Name=   "ID"
                  Columns(12).DataField=   "Column 12"
                  Columns(12).DataType=   8
                  Columns(12).FieldLen=   256
                  Columns(13).Width=   3200
                  Columns(13).Visible=   0   'False
                  Columns(13).Caption=   "TIPO_HIDDEN"
                  Columns(13).Name=   "TIPO_HIDDEN"
                  Columns(13).DataField=   "Column 13"
                  Columns(13).DataType=   8
                  Columns(13).FieldLen=   256
                  Columns(14).Width=   3200
                  Columns(14).Caption=   "AMBITO"
                  Columns(14).Name=   "AMBITO"
                  Columns(14).DataField=   "Column 14"
                  Columns(14).DataType=   8
                  Columns(14).FieldLen=   256
                  Columns(15).Width=   3200
                  Columns(15).Visible=   0   'False
                  Columns(15).Caption=   "AMBITO_HIDDEN"
                  Columns(15).Name=   "AMBITO_HIDDEN"
                  Columns(15).DataField=   "Column 15"
                  Columns(15).DataType=   8
                  Columns(15).FieldLen=   256
                  Columns(16).Width=   3200
                  Columns(16).Visible=   0   'False
                  Columns(16).Caption=   "OPERACION_HIDDEN"
                  Columns(16).Name=   "OPERACION_HIDDEN"
                  Columns(16).DataField=   "Column 16"
                  Columns(16).DataType=   8
                  Columns(16).FieldLen=   256
                  Columns(17).Width=   3200
                  Columns(17).Caption=   "IMPUESTOS_BTN"
                  Columns(17).Name=   "IMPUESTOS_BTN"
                  Columns(17).DataField=   "Column 17"
                  Columns(17).DataType=   8
                  Columns(17).FieldLen=   256
                  Columns(17).Style=   4
                  Columns(17).ButtonsAlways=   -1  'True
                  Columns(18).Width=   3200
                  Columns(18).Visible=   0   'False
                  Columns(18).Caption=   "IMPUESTOS"
                  Columns(18).Name=   "IMPUESTOS"
                  Columns(18).DataField=   "Column 18"
                  Columns(18).DataType=   11
                  Columns(18).FieldLen=   256
                  _ExtentX        =   10398
                  _ExtentY        =   3757
                  _StockProps     =   79
                  BackColor       =   -2147483643
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddOperacionCoste 
                  Height          =   360
                  Left            =   0
                  TabIndex        =   65
                  Top             =   0
                  Width           =   3015
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0A44
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5318
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddTipoCoste 
                  Height          =   360
                  Left            =   0
                  TabIndex        =   66
                  Top             =   360
                  Width           =   2775
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0A60
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "COD"
                  Columns(2).Name =   "COD"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   4895
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddValorCoste 
                  Height          =   360
                  Left            =   3120
                  TabIndex        =   67
                  Top             =   360
                  Width           =   3015
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0A7C
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5318
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddAmbitoCoste 
                  Height          =   360
                  Left            =   3120
                  TabIndex        =   68
                  Top             =   0
                  Width           =   2775
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0A98
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "COD"
                  Columns(2).Name =   "COD"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   4895
                  _ExtentY        =   635
                  _StockProps     =   77
               End
            End
            Begin VB.PictureBox picDescuentos 
               BackColor       =   &H00808000&
               Height          =   3015
               Left            =   -74880
               ScaleHeight     =   2955
               ScaleMode       =   0  'User
               ScaleWidth      =   6037.175
               TabIndex        =   53
               Top             =   540
               Width           =   6150
               Begin VB.CommandButton cmdEliminarDescuento 
                  Caption         =   "Eli&minar"
                  Height          =   315
                  Left            =   1200
                  TabIndex        =   55
                  Top             =   2400
                  Width           =   945
               End
               Begin VB.CommandButton cmdA�adirDescuento 
                  Caption         =   "A�ad&ir"
                  Height          =   315
                  Left            =   120
                  TabIndex        =   54
                  Top             =   2400
                  Width           =   945
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddValorDescuento 
                  Height          =   360
                  Left            =   3240
                  TabIndex        =   56
                  Top             =   0
                  Width           =   3015
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0AB4
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5318
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddOperacionDescuento 
                  Height          =   360
                  Left            =   0
                  TabIndex        =   57
                  Top             =   0
                  Width           =   3015
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0AD0
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5318
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddTipoDescuento 
                  Height          =   360
                  Left            =   0
                  TabIndex        =   58
                  Top             =   360
                  Width           =   2775
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0AEC
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   3
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "COD"
                  Columns(2).Name =   "COD"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  _ExtentX        =   4895
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddAmbitoDescuento 
                  Height          =   360
                  Left            =   3120
                  TabIndex        =   59
                  Top             =   360
                  Width           =   3015
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0B08
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5318
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBGrid sdbgDescuentosCategoria 
                  Height          =   2130
                  Left            =   120
                  TabIndex        =   60
                  Top             =   120
                  Width           =   5895
                  _Version        =   196617
                  DataMode        =   2
                  Col.Count       =   17
                  stylesets.count =   4
                  stylesets(0).Name=   "ConDescr"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":0B24
                  stylesets(1).Name=   "Gris"
                  stylesets(1).BackColor=   -2147483633
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCatalogo.frx":11A6
                  stylesets(2).Name=   "Normal"
                  stylesets(2).ForeColor=   0
                  stylesets(2).BackColor=   16777215
                  stylesets(2).HasFont=   -1  'True
                  BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(2).Picture=   "frmCatalogo.frx":11C2
                  stylesets(3).Name=   "Selected"
                  stylesets(3).ForeColor=   16777215
                  stylesets(3).BackColor=   8388608
                  stylesets(3).HasFont=   -1  'True
                  BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(3).Picture=   "frmCatalogo.frx":11DE
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   -2147483630
                  ForeColorOdd    =   -2147483630
                  BackColorEven   =   -2147483643
                  BackColorOdd    =   -2147483643
                  RowHeight       =   423
                  Columns.Count   =   17
                  Columns(0).Width=   1773
                  Columns(0).Caption=   "COD"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Locked=   -1  'True
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16776960
                  Columns(1).Width=   4551
                  Columns(1).Caption=   "NOMBRE"
                  Columns(1).Name =   "NOMBRE"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).Locked=   -1  'True
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16776960
                  Columns(2).Width=   3200
                  Columns(2).Visible=   0   'False
                  Columns(2).Caption=   "DESCR"
                  Columns(2).Name =   "DESCR_BTN"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Style=   4
                  Columns(2).ButtonsAlways=   -1  'True
                  Columns(3).Width=   1984
                  Columns(3).Caption=   "OPERACION"
                  Columns(3).Name =   "OPERACION"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   2037
                  Columns(4).Caption=   "VALOR"
                  Columns(4).Name =   "VALOR"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(5).Width=   3916
                  Columns(5).Caption=   "TIPO"
                  Columns(5).Name =   "TIPO"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(6).Width=   2170
                  Columns(6).Caption=   "GRUPO"
                  Columns(6).Name =   "GRUPO"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(7).Width=   3200
                  Columns(7).Visible=   0   'False
                  Columns(7).Caption=   "DESCR"
                  Columns(7).Name =   "DESCR"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(8).Width=   3200
                  Columns(8).Visible=   0   'False
                  Columns(8).Caption=   "INTRO"
                  Columns(8).Name =   "INTRO"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  Columns(9).Width=   3200
                  Columns(9).Visible=   0   'False
                  Columns(9).Caption=   "MIN"
                  Columns(9).Name =   "MIN"
                  Columns(9).DataField=   "Column 9"
                  Columns(9).DataType=   8
                  Columns(9).FieldLen=   256
                  Columns(10).Width=   3200
                  Columns(10).Visible=   0   'False
                  Columns(10).Caption=   "MAX"
                  Columns(10).Name=   "MAX"
                  Columns(10).DataField=   "Column 10"
                  Columns(10).DataType=   8
                  Columns(10).FieldLen=   256
                  Columns(11).Width=   3200
                  Columns(11).Visible=   0   'False
                  Columns(11).Caption=   "ATRIB_ID"
                  Columns(11).Name=   "ATRIB_ID"
                  Columns(11).DataField=   "Column 11"
                  Columns(11).DataType=   8
                  Columns(11).FieldLen=   256
                  Columns(12).Width=   3200
                  Columns(12).Visible=   0   'False
                  Columns(12).Caption=   "ID"
                  Columns(12).Name=   "ID"
                  Columns(12).DataField=   "Column 12"
                  Columns(12).DataType=   8
                  Columns(12).FieldLen=   256
                  Columns(13).Width=   3200
                  Columns(13).Visible=   0   'False
                  Columns(13).Caption=   "TIPO_HIDDEN"
                  Columns(13).Name=   "TIPO_HIDDEN"
                  Columns(13).DataField=   "Column 13"
                  Columns(13).DataType=   8
                  Columns(13).FieldLen=   256
                  Columns(14).Width=   3200
                  Columns(14).Caption=   "AMBITO"
                  Columns(14).Name=   "AMBITO"
                  Columns(14).DataField=   "Column 14"
                  Columns(14).DataType=   8
                  Columns(14).FieldLen=   256
                  Columns(15).Width=   3200
                  Columns(15).Visible=   0   'False
                  Columns(15).Caption=   "AMBITO_HIDDEN"
                  Columns(15).Name=   "AMBITO_HIDDEN"
                  Columns(15).DataField=   "Column 15"
                  Columns(15).DataType=   8
                  Columns(15).FieldLen=   256
                  Columns(16).Width=   3200
                  Columns(16).Visible=   0   'False
                  Columns(16).Caption=   "OPERACION_HIDDEN"
                  Columns(16).Name=   "OPERACION_HIDDEN"
                  Columns(16).DataField=   "Column 16"
                  Columns(16).DataType=   8
                  Columns(16).FieldLen=   256
                  _ExtentX        =   10398
                  _ExtentY        =   3757
                  _StockProps     =   79
                  BackColor       =   -2147483643
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
            End
            Begin VB.PictureBox picModEmision 
               BackColor       =   &H00808000&
               Height          =   3015
               Left            =   -74880
               ScaleHeight     =   2955
               ScaleMode       =   0  'User
               ScaleWidth      =   6203.62
               TabIndex        =   50
               Top             =   570
               Width           =   6150
               Begin VB.CheckBox chkModEmision 
                  BackColor       =   &H00808000&
                  Caption         =   "Permitir modificaciones de precios en la emisi�n"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   285
                  Index           =   0
                  Left            =   120
                  TabIndex        =   52
                  Top             =   120
                  Width           =   5317
               End
               Begin VB.CheckBox chkModEmision 
                  BackColor       =   &H00808000&
                  Caption         =   "Actualizar los precios de las lineas de cat�logo con los precios de emisi�n"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   375
                  Index           =   1
                  Left            =   375
                  TabIndex        =   51
                  Top             =   510
                  Width           =   5668
               End
            End
            Begin VB.PictureBox picCamposPedido 
               BackColor       =   &H00808000&
               Height          =   3015
               Left            =   120
               ScaleHeight     =   2955
               ScaleMode       =   0  'User
               ScaleWidth      =   6037.175
               TabIndex        =   44
               Top             =   540
               Width           =   6150
               Begin VB.CommandButton cmdAnyaCampo 
                  Caption         =   "A�ad&ir"
                  Height          =   315
                  Left            =   120
                  TabIndex        =   46
                  Top             =   2400
                  Width           =   945
               End
               Begin VB.CommandButton cmdEliCampo 
                  Caption         =   "Eli&minar"
                  Height          =   315
                  Left            =   1200
                  TabIndex        =   45
                  Top             =   2400
                  Width           =   945
               End
               Begin SSDataWidgets_B.SSDBGrid sdbgCamposPedido 
                  Height          =   2130
                  Left            =   120
                  TabIndex        =   47
                  Top             =   120
                  Width           =   5891
                  _Version        =   196617
                  DataMode        =   2
                  Col.Count       =   14
                  stylesets.count =   3
                  stylesets(0).Name=   "Gris"
                  stylesets(0).BackColor=   12632256
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":11FA
                  stylesets(1).Name=   "Normal"
                  stylesets(1).ForeColor=   0
                  stylesets(1).BackColor=   16777215
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCatalogo.frx":1216
                  stylesets(2).Name=   "Selected"
                  stylesets(2).ForeColor=   16777215
                  stylesets(2).BackColor=   8388608
                  stylesets(2).HasFont=   -1  'True
                  BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(2).Picture=   "frmCatalogo.frx":1232
                  AllowRowSizing  =   0   'False
                  AllowGroupSizing=   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   -2147483630
                  ForeColorOdd    =   -2147483630
                  BackColorEven   =   -2147483643
                  BackColorOdd    =   -2147483643
                  RowHeight       =   423
                  Columns.Count   =   14
                  Columns(0).Width=   2090
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(0).Locked=   -1  'True
                  Columns(0).HasBackColor=   -1  'True
                  Columns(0).BackColor=   16776960
                  Columns(1).Width=   4445
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  Columns(1).Locked=   -1  'True
                  Columns(1).HasBackColor=   -1  'True
                  Columns(1).BackColor=   16776960
                  Columns(2).Width=   900
                  Columns(2).Caption=   "Obl."
                  Columns(2).Name =   "OBL"
                  Columns(2).DataField=   "Column 2"
                  Columns(2).DataType=   8
                  Columns(2).FieldLen=   256
                  Columns(2).Style=   2
                  Columns(3).Width=   3519
                  Columns(3).Caption=   "Valor"
                  Columns(3).Name =   "VAL"
                  Columns(3).DataField=   "Column 3"
                  Columns(3).DataType=   8
                  Columns(3).FieldLen=   256
                  Columns(4).Width=   2672
                  Columns(4).Caption=   "�mbito"
                  Columns(4).Name =   "AMB"
                  Columns(4).DataField=   "Column 4"
                  Columns(4).DataType=   8
                  Columns(4).FieldLen=   256
                  Columns(5).Width=   900
                  Columns(5).Caption=   "Int."
                  Columns(5).Name =   "INT"
                  Columns(5).DataField=   "Column 5"
                  Columns(5).DataType=   8
                  Columns(5).FieldLen=   256
                  Columns(5).Style=   2
                  Columns(6).Width=   3200
                  Columns(6).Visible=   0   'False
                  Columns(6).Caption=   "TIPO_INTRODUCCION"
                  Columns(6).Name =   "TIPO_INTRODUCCION"
                  Columns(6).DataField=   "Column 6"
                  Columns(6).DataType=   8
                  Columns(6).FieldLen=   256
                  Columns(7).Width=   3200
                  Columns(7).Visible=   0   'False
                  Columns(7).Caption=   "TIPO_DATOS"
                  Columns(7).Name =   "TIPO_DATOS"
                  Columns(7).DataField=   "Column 7"
                  Columns(7).DataType=   8
                  Columns(7).FieldLen=   256
                  Columns(8).Width=   3200
                  Columns(8).Visible=   0   'False
                  Columns(8).Caption=   "MIN"
                  Columns(8).Name =   "MIN"
                  Columns(8).DataField=   "Column 8"
                  Columns(8).DataType=   8
                  Columns(8).FieldLen=   256
                  Columns(9).Width=   3200
                  Columns(9).Visible=   0   'False
                  Columns(9).Caption=   "MAX"
                  Columns(9).Name =   "MAX"
                  Columns(9).DataField=   "Column 9"
                  Columns(9).DataType=   8
                  Columns(9).FieldLen=   256
                  Columns(10).Width=   3200
                  Columns(10).Visible=   0   'False
                  Columns(10).Caption=   "ATRIBID"
                  Columns(10).Name=   "ATRIBID"
                  Columns(10).DataField=   "Column 10"
                  Columns(10).DataType=   8
                  Columns(10).FieldLen=   256
                  Columns(11).Width=   3200
                  Columns(11).Visible=   0   'False
                  Columns(11).Caption=   "ID"
                  Columns(11).Name=   "ID"
                  Columns(11).DataField=   "Column 11"
                  Columns(11).DataType=   8
                  Columns(11).FieldLen=   256
                  Columns(12).Width=   3200
                  Columns(12).Visible=   0   'False
                  Columns(12).Caption=   "LISTA_EXTERNA"
                  Columns(12).Name=   "LISTA_EXTERNA"
                  Columns(12).DataField=   "Column 12"
                  Columns(12).DataType=   11
                  Columns(12).FieldLen=   256
                  Columns(13).Width=   3200
                  Columns(13).Caption=   "DMostrar en Reccepci�n"
                  Columns(13).Name=   "VERENRECEP"
                  Columns(13).DataField=   "Column 13"
                  Columns(13).DataType=   8
                  Columns(13).FieldLen=   256
                  Columns(13).Style=   2
                  _ExtentX        =   10391
                  _ExtentY        =   3757
                  _StockProps     =   79
                  BackColor       =   -2147483643
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
                  Height          =   360
                  Left            =   2520
                  TabIndex        =   48
                  Top             =   2400
                  Width           =   3015
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   1
                  stylesets(0).Name=   "Normal"
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":124E
                  DividerStyle    =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "VALOR"
                  Columns(0).Name =   "VALOR"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DESC"
                  Columns(1).Name =   "DESC"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5318
                  _ExtentY        =   635
                  _StockProps     =   77
               End
               Begin SSDataWidgets_B.SSDBDropDown sdbddAmbito 
                  Height          =   1575
                  Left            =   0
                  TabIndex        =   49
                  Top             =   0
                  Width           =   4080
                  DataFieldList   =   "Column 0"
                  ListAutoValidate=   0   'False
                  ListAutoPosition=   0   'False
                  MaxDropDownItems=   10
                  _Version        =   196617
                  DataMode        =   2
                  ColumnHeaders   =   0   'False
                  stylesets.count =   2
                  stylesets(0).Name=   "Actuales"
                  stylesets(0).BackColor=   10944511
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":126A
                  stylesets(1).Name=   "Normal"
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCatalogo.frx":1286
                  StyleSet        =   "Normal"
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3200
                  Columns(1).Caption=   "DEN"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   7197
                  _ExtentY        =   2778
                  _StockProps     =   77
                  DataFieldToDisplay=   "Column 1"
               End
            End
            Begin VB.PictureBox picProveedores 
               BackColor       =   &H00808000&
               Height          =   3015
               Left            =   -74880
               ScaleHeight     =   2955
               ScaleMode       =   0  'User
               ScaleWidth      =   6037.175
               TabIndex        =   39
               Top             =   540
               Width           =   6150
               Begin VB.CommandButton cmdEliCat 
                  Caption         =   "Eli&minar"
                  Enabled         =   0   'False
                  Height          =   315
                  Index           =   1
                  Left            =   1200
                  TabIndex        =   43
                  Top             =   2400
                  Width           =   945
               End
               Begin VB.CommandButton cmdAnyaCat 
                  Caption         =   "A�ad&ir"
                  Enabled         =   0   'False
                  Height          =   315
                  Index           =   1
                  Left            =   120
                  TabIndex        =   42
                  Top             =   2400
                  Width           =   945
               End
               Begin SSDataWidgets_B.SSDBGrid sdbgProves 
                  Height          =   1890
                  Left            =   120
                  TabIndex        =   41
                  Top             =   360
                  Width           =   5891
                  _Version        =   196617
                  DataMode        =   2
                  Col.Count       =   2
                  stylesets.count =   2
                  stylesets(0).Name=   "Normal"
                  stylesets(0).ForeColor=   0
                  stylesets(0).BackColor=   16777215
                  stylesets(0).HasFont=   -1  'True
                  BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(0).Picture=   "frmCatalogo.frx":12A2
                  stylesets(1).Name=   "Selected"
                  stylesets(1).ForeColor=   16777215
                  stylesets(1).BackColor=   8388608
                  stylesets(1).HasFont=   -1  'True
                  BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "Tahoma"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  stylesets(1).Picture=   "frmCatalogo.frx":12BE
                  AllowDelete     =   -1  'True
                  MultiLine       =   0   'False
                  AllowRowSizing  =   0   'False
                  AllowGroupMoving=   0   'False
                  AllowColumnMoving=   0
                  AllowGroupSwapping=   0   'False
                  AllowColumnSwapping=   0
                  AllowGroupShrinking=   0   'False
                  AllowColumnShrinking=   0   'False
                  AllowDragDrop   =   0   'False
                  SelectTypeCol   =   0
                  SelectTypeRow   =   3
                  StyleSet        =   "Normal"
                  ForeColorEven   =   -2147483630
                  ForeColorOdd    =   -2147483630
                  BackColorEven   =   -2147483643
                  BackColorOdd    =   -2147483643
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   2090
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "COD"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   4630
                  Columns(1).Caption=   "Denominaci�n"
                  Columns(1).Name =   "DEN"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   10391
                  _ExtentY        =   3334
                  _StockProps     =   79
                  BackColor       =   -2147483643
                  BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin VB.Label Label1 
                  BackColor       =   &H00808000&
                  Caption         =   "Proveedores para pedido libre:"
                  ForeColor       =   &H00FFFFFF&
                  Height          =   240
                  Index           =   2
                  Left            =   120
                  TabIndex        =   40
                  Top             =   120
                  Width           =   2790
               End
            End
         End
         Begin VB.TextBox txtObsAdjun 
            Height          =   795
            Left            =   30
            Locked          =   -1  'True
            MaxLength       =   500
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   10
            Top             =   195
            Width           =   6350
         End
         Begin VB.PictureBox picBotonAdjun 
            BackColor       =   &H00808000&
            BorderStyle     =   0  'None
            Height          =   345
            Left            =   3980
            ScaleHeight     =   345
            ScaleWidth      =   2385
            TabIndex        =   34
            Top             =   2100
            Width           =   2385
            Begin VB.CommandButton cmdAbrirAdjun 
               Enabled         =   0   'False
               Height          =   300
               Left            =   1950
               Picture         =   "frmCatalogo.frx":12DA
               Style           =   1  'Graphical
               TabIndex        =   16
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdSalvarAdjun 
               Enabled         =   0   'False
               Height          =   300
               Left            =   1470
               Picture         =   "frmCatalogo.frx":1356
               Style           =   1  'Graphical
               TabIndex        =   15
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdEliminarAdjun 
               Enabled         =   0   'False
               Height          =   300
               Left            =   510
               Picture         =   "frmCatalogo.frx":13D7
               Style           =   1  'Graphical
               TabIndex        =   13
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdA�adirAdjun 
               Enabled         =   0   'False
               Height          =   300
               Left            =   30
               Picture         =   "frmCatalogo.frx":145A
               Style           =   1  'Graphical
               TabIndex        =   12
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
            Begin VB.CommandButton cmdModificarAdjun 
               Enabled         =   0   'False
               Height          =   300
               Left            =   990
               Picture         =   "frmCatalogo.frx":14CC
               Style           =   1  'Graphical
               TabIndex        =   14
               TabStop         =   0   'False
               Top             =   30
               UseMaskColor    =   -1  'True
               Width           =   420
            End
         End
         Begin MSComctlLib.ListView lstvwAdjun 
            Height          =   855
            Left            =   30
            TabIndex        =   11
            Top             =   1230
            Width           =   6365
            _ExtentX        =   11218
            _ExtentY        =   1508
            View            =   3
            Arrange         =   1
            LabelEdit       =   1
            LabelWrap       =   0   'False
            HideSelection   =   0   'False
            HotTracking     =   -1  'True
            _Version        =   393217
            Icons           =   "ImageList1"
            SmallIcons      =   "ImageList1"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   3
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Text            =   "Fichero"
               Object.Width           =   2293
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Alignment       =   1
               SubItemIndex    =   1
               Text            =   "Tamanyo"
               Object.Width           =   2540
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Text            =   "Comentario"
               Object.Width           =   3528
            EndProperty
         End
         Begin VB.Label Label1 
            BackColor       =   &H00808000&
            Caption         =   "Descripci�n:"
            ForeColor       =   &H80000005&
            Height          =   240
            Index           =   0
            Left            =   90
            TabIndex        =   36
            Top             =   0
            Width           =   1410
         End
         Begin VB.Label Label1 
            BackColor       =   &H00808000&
            Caption         =   "Archivos adjuntos:"
            ForeColor       =   &H80000005&
            Height          =   240
            Index           =   1
            Left            =   90
            TabIndex        =   35
            Top             =   1020
            Width           =   1410
         End
      End
      Begin MSComDlg.CommonDialog cmmdAdjun 
         Left            =   5400
         Top             =   180
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddUnidades 
         Height          =   1575
         Left            =   -74205
         TabIndex        =   31
         Top             =   3000
         Width           =   4080
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         MaxDropDownItems=   10
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   1
         stylesets(0).Name=   "Actuales"
         stylesets(0).BackColor=   10944511
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogo.frx":1616
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2090
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4260
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7197
         _ExtentY        =   2778
         _StockProps     =   77
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddDestinos 
         Height          =   1260
         Left            =   -74400
         TabIndex        =   30
         Top             =   2500
         Width           =   7350
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogo.frx":1632
         stylesets(1).Name=   "Espec"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogo.frx":164E
         BevelColorFace  =   12632256
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   7
         Columns(0).Width=   1958
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).HeadBackColor=   -2147483633
         Columns(0).BackColor=   16777215
         Columns(1).Width=   7038
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadBackColor=   -2147483633
         Columns(2).Width=   3200
         Columns(2).Caption=   "Direcci�n"
         Columns(2).Name =   "DIR"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadBackColor=   -2147483633
         Columns(3).Width=   2090
         Columns(3).Caption=   "Poblaci�n"
         Columns(3).Name =   "POB"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HeadBackColor=   -2147483633
         Columns(4).Width=   1138
         Columns(4).Caption=   "CP"
         Columns(4).Name =   "CP"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HasHeadBackColor=   -1  'True
         Columns(4).HeadBackColor=   -2147483633
         Columns(5).Width=   926
         Columns(5).Caption=   "Pa�s"
         Columns(5).Name =   "PAI"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).HasHeadBackColor=   -1  'True
         Columns(5).HeadBackColor=   -2147483633
         Columns(6).Width=   1402
         Columns(6).Caption=   "Provincia"
         Columns(6).Name =   "PROVI"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).HasHeadBackColor=   -1  'True
         Columns(6).HeadBackColor=   -2147483633
         _ExtentX        =   12965
         _ExtentY        =   2222
         _StockProps     =   77
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddUnidadesPedido 
         Height          =   1575
         Left            =   -74505
         TabIndex        =   29
         Top             =   1995
         Width           =   8280
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         MaxDropDownItems=   10
         _Version        =   196617
         DataMode        =   2
         stylesets.count =   1
         stylesets(0).Name=   "Actuales"
         stylesets(0).BackColor=   10944511
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogo.frx":16CB
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   185
         Columns.Count   =   6
         Columns(0).Width=   1085
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   3
         Columns(1).Width=   5424
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   50
         Columns(2).Width=   1720
         Columns(2).Caption=   "FC"
         Columns(2).Name =   "FC"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   5
         Columns(2).NumberFormat=   "Standard"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1852
         Columns(3).Caption=   "Cant.m�n."
         Columns(3).Name =   "CANTMIN"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   5
         Columns(3).NumberFormat=   "Standard"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1826
         Columns(4).Caption=   "dCant.Max"
         Columns(4).Name =   "CANTMAXPED"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   2381
         Columns(5).Caption=   "dCant.Max Total"
         Columns(5).Name =   "CANTMAXTOTAL"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   14605
         _ExtentY        =   2778
         _StockProps     =   77
      End
      Begin VB.PictureBox picNavigateAdj 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   -74880
         ScaleHeight     =   420
         ScaleWidth      =   13350
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   6945
         Width           =   13350
         Begin VB.CommandButton cmdExcel 
            Caption         =   "DImportar precios"
            Height          =   345
            Index           =   3
            Left            =   9270
            TabIndex        =   79
            Top             =   60
            Width           =   1455
         End
         Begin VB.CommandButton cmdExcel 
            Caption         =   "DExportar precios"
            Height          =   345
            Index           =   0
            Left            =   7740
            TabIndex        =   75
            Top             =   60
            Width           =   1455
         End
         Begin VB.CommandButton cmdAnyaArticulo 
            Caption         =   "A�adir a&rt�culo"
            Height          =   345
            Left            =   1845
            TabIndex        =   19
            Top             =   60
            Width           =   1440
         End
         Begin VB.CommandButton cmdEliAdj 
            Caption         =   "&Eliminar"
            Height          =   345
            Left            =   3330
            TabIndex        =   20
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdAnyaAdjudicacion 
            Caption         =   "A�adir a&djudicaci�n"
            Height          =   345
            Left            =   0
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   60
            Width           =   1800
         End
         Begin VB.CommandButton cmdModoEdicion 
            Caption         =   "&Edici�n"
            Height          =   345
            Left            =   12285
            TabIndex        =   25
            Top             =   60
            Width           =   1065
         End
         Begin VB.CommandButton cmdBuscarAdj 
            Caption         =   "&Buscar"
            Height          =   345
            Left            =   11220
            TabIndex        =   23
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdDeshacer 
            Caption         =   "&Deshacer"
            Height          =   345
            Left            =   11220
            TabIndex        =   24
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdCambiarCat 
            Caption         =   "&Cambiar de categor�a"
            Height          =   345
            Left            =   4380
            TabIndex        =   21
            Top             =   60
            Width           =   1785
         End
         Begin VB.CommandButton cmdModifPrecios 
            Caption         =   "&Modificar precios"
            Height          =   345
            Left            =   6210
            TabIndex        =   22
            Top             =   60
            Width           =   1455
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAdjudicaciones 
         Height          =   6135
         Left            =   -74850
         TabIndex        =   17
         Top             =   810
         Width           =   11280
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         Col.Count       =   47
         stylesets.count =   9
         stylesets(0).Name=   "Blanco"
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogo.frx":16E7
         stylesets(1).Name=   "Rojo"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8421631
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogo.frx":1703
         stylesets(2).Name=   "NoPublicado"
         stylesets(2).BackColor=   11513775
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmCatalogo.frx":171F
         stylesets(3).Name=   "HayEsp"
         stylesets(3).BackColor=   65280
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmCatalogo.frx":173B
         stylesets(4).Name=   "GrisOscuro"
         stylesets(4).BackColor=   11184810
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmCatalogo.frx":17B8
         stylesets(5).Name=   "Normal"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmCatalogo.frx":17D4
         stylesets(6).Name=   "HayUniPed"
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmCatalogo.frx":17F0
         stylesets(6).AlignmentPicture=   0
         stylesets(7).Name=   "GrisSuave"
         stylesets(7).BackColor=   14671839
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "frmCatalogo.frx":1854
         stylesets(8).Name=   "Azul"
         stylesets(8).BackColor=   16777152
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "frmCatalogo.frx":1870
         MultiLine       =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         BalloonHelp     =   0   'False
         CellNavigation  =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   47
         Columns(0).Width=   979
         Columns(0).Caption=   "Pub."
         Columns(0).Name =   "PUB"
         Columns(0).Alignment=   2
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   2037
         Columns(1).Caption=   "Despublicaci�n"
         Columns(1).Name =   "DESPUB"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   7
         Columns(1).FieldLen=   256
         Columns(2).Width=   3175
         Columns(2).Caption=   "Art�culo"
         Columns(2).Name =   "ART"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).Style=   1
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16777152
         Columns(3).Width=   3175
         Columns(3).Caption=   "ARTDEN"
         Columns(3).Name =   "ARTDEN"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2196
         Columns(4).Caption=   "Proceso"
         Columns(4).Name =   "PROCE"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   16777215
         Columns(5).Width=   1826
         Columns(5).Caption=   "Proveedor"
         Columns(5).Name =   "PROVE"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Style=   1
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   16777215
         Columns(6).Width=   1376
         Columns(6).Caption=   "Destino"
         Columns(6).Name =   "DEST"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   1111
         Columns(7).Caption=   "D.Usu"
         Columns(7).Name =   "DEST_USU"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Style=   2
         Columns(8).Width=   1667
         Columns(8).Caption=   "Cant. Adj."
         Columns(8).Name =   "CANTADJ"
         Columns(8).Alignment=   1
         Columns(8).CaptionAlignment=   2
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   5
         Columns(8).NumberFormat=   "Standard"
         Columns(8).FieldLen=   256
         Columns(8).Locked=   -1  'True
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   16777152
         Columns(9).Width=   1455
         Columns(9).Caption=   "U. base"
         Columns(9).Name =   "UB"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   16777215
         Columns(10).Width=   1931
         Columns(10).Caption=   "Precio base (EUR)"
         Columns(10).Name=   "PrecioUB"
         Columns(10).Alignment=   1
         Columns(10).CaptionAlignment=   2
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   5
         Columns(10).NumberFormat=   "#,##0.0#"
         Columns(10).FieldLen=   256
         Columns(10).Style=   1
         Columns(10).HasBackColor=   -1  'True
         Columns(10).BackColor=   11073522
         Columns(11).Width=   1561
         Columns(11).Caption=   "Mon"
         Columns(11).Name=   "MON"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "UP"
         Columns(12).Name=   "UP"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "FC"
         Columns(13).Name=   "FC"
         Columns(13).Alignment=   1
         Columns(13).CaptionAlignment=   2
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   5
         Columns(13).NumberFormat=   "Standard"
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "Otras"
         Columns(14).Name=   "OTRAS"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         Columns(14).Style=   4
         Columns(14).ButtonsAlways=   -1  'True
         Columns(15).Width=   3200
         Columns(15).Visible=   0   'False
         Columns(15).Caption=   "Cant.m�n.ped."
         Columns(15).Name=   "CANTMIN"
         Columns(15).Alignment=   1
         Columns(15).CaptionAlignment=   2
         Columns(15).DataField=   "Column 15"
         Columns(15).DataType=   5
         Columns(15).NumberFormat=   "Standard"
         Columns(15).FieldLen=   256
         Columns(16).Width=   3200
         Columns(16).Visible=   0   'False
         Columns(16).Caption=   "ID"
         Columns(16).Name=   "ID"
         Columns(16).DataField=   "Column 16"
         Columns(16).DataType=   8
         Columns(16).FieldLen=   256
         Columns(17).Width=   3200
         Columns(17).Visible=   0   'False
         Columns(17).Caption=   "Homologado"
         Columns(17).Name=   "HOM"
         Columns(17).DataField=   "Column 17"
         Columns(17).DataType=   8
         Columns(17).FieldLen=   256
         Columns(18).Width=   3200
         Columns(18).Visible=   0   'False
         Columns(18).Caption=   "ANYO"
         Columns(18).Name=   "ANYO"
         Columns(18).DataField=   "Column 18"
         Columns(18).DataType=   2
         Columns(18).FieldLen=   256
         Columns(19).Width=   3200
         Columns(19).Visible=   0   'False
         Columns(19).Caption=   "GMN1"
         Columns(19).Name=   "GMN1"
         Columns(19).DataField=   "Column 19"
         Columns(19).DataType=   8
         Columns(19).FieldLen=   256
         Columns(20).Width=   3200
         Columns(20).Visible=   0   'False
         Columns(20).Caption=   "PROCECOD"
         Columns(20).Name=   "PROCECOD"
         Columns(20).DataField=   "Column 20"
         Columns(20).DataType=   2
         Columns(20).FieldLen=   256
         Columns(21).Width=   3200
         Columns(21).Visible=   0   'False
         Columns(21).Caption=   "NUEVA"
         Columns(21).Name=   "NUEVA"
         Columns(21).DataField=   "Column 21"
         Columns(21).DataType=   8
         Columns(21).FieldLen=   256
         Columns(22).Width=   3200
         Columns(22).Visible=   0   'False
         Columns(22).Caption=   "ARTCOD"
         Columns(22).Name=   "ARTCOD"
         Columns(22).DataField=   "Column 22"
         Columns(22).DataType=   8
         Columns(22).FieldLen=   256
         Columns(23).Width=   3200
         Columns(23).Visible=   0   'False
         Columns(23).Caption=   "GMN2"
         Columns(23).Name=   "GMN2"
         Columns(23).DataField=   "Column 23"
         Columns(23).DataType=   8
         Columns(23).FieldLen=   256
         Columns(24).Width=   3200
         Columns(24).Visible=   0   'False
         Columns(24).Caption=   "GMN3"
         Columns(24).Name=   "GMN3"
         Columns(24).DataField=   "Column 24"
         Columns(24).DataType=   8
         Columns(24).FieldLen=   256
         Columns(25).Width=   3200
         Columns(25).Visible=   0   'False
         Columns(25).Caption=   "GMN4"
         Columns(25).Name=   "GMN4"
         Columns(25).DataField=   "Column 25"
         Columns(25).DataType=   8
         Columns(25).FieldLen=   256
         Columns(26).Width=   3200
         Columns(26).Visible=   0   'False
         Columns(26).Caption=   "ITEM"
         Columns(26).Name=   "ITEM"
         Columns(26).DataField=   "Column 26"
         Columns(26).DataType=   8
         Columns(26).FieldLen=   256
         Columns(27).Width=   3200
         Columns(27).Visible=   0   'False
         Columns(27).Caption=   "PRES"
         Columns(27).Name=   "PRES"
         Columns(27).DataField=   "Column 27"
         Columns(27).DataType=   8
         Columns(27).FieldLen=   256
         Columns(28).Width=   3200
         Columns(28).Visible=   0   'False
         Columns(28).Caption=   "HAYUNIPED"
         Columns(28).Name=   "HAYUNIPED"
         Columns(28).DataField=   "Column 28"
         Columns(28).DataType=   8
         Columns(28).FieldLen=   256
         Columns(29).Width=   1826
         Columns(29).Caption=   "PORCEN_DESVIO"
         Columns(29).Name=   "PORCEN_DESVIO"
         Columns(29).DataField=   "Column 29"
         Columns(29).DataType=   5
         Columns(29).NumberFormat=   "Standard"
         Columns(29).FieldLen=   256
         Columns(30).Width=   1482
         Columns(30).Caption=   "Solicitud"
         Columns(30).Name=   "SOLICIT"
         Columns(30).Alignment=   1
         Columns(30).CaptionAlignment=   0
         Columns(30).DataField=   "Column 30"
         Columns(30).DataType=   8
         Columns(30).FieldLen=   256
         Columns(30).Locked=   -1  'True
         Columns(30).Style=   1
         Columns(30).ButtonsAlways=   -1  'True
         Columns(31).Width=   1508
         Columns(31).Caption=   "Ficha Art."
         Columns(31).Name=   "ESP"
         Columns(31).DataField=   "Column 31"
         Columns(31).DataType=   8
         Columns(31).FieldLen=   256
         Columns(31).Style=   4
         Columns(31).ButtonsAlways=   -1  'True
         Columns(32).Width=   3200
         Columns(32).Visible=   0   'False
         Columns(32).Caption=   "ESPADJ"
         Columns(32).Name=   "ESPADJ"
         Columns(32).DataField=   "Column 32"
         Columns(32).DataType=   8
         Columns(32).FieldLen=   256
         Columns(33).Width=   3200
         Columns(33).Visible=   0   'False
         Columns(33).Caption=   "GENERICO"
         Columns(33).Name=   "GENERICO"
         Columns(33).DataField=   "Column 33"
         Columns(33).DataType=   8
         Columns(33).FieldLen=   256
         Columns(34).Width=   2778
         Columns(34).Caption=   "DCampos de pedido"
         Columns(34).Name=   "CAMPED_BTN"
         Columns(34).DataField=   "Column 34"
         Columns(34).DataType=   8
         Columns(34).FieldLen=   256
         Columns(34).Style=   4
         Columns(34).ButtonsAlways=   -1  'True
         Columns(35).Width=   3200
         Columns(35).Visible=   0   'False
         Columns(35).Caption=   "CAMPED"
         Columns(35).Name=   "CAMPED"
         Columns(35).DataField=   "Column 35"
         Columns(35).DataType=   8
         Columns(35).FieldLen=   256
         Columns(36).Width=   3200
         Columns(36).Caption=   "DCampos de recepci�n"
         Columns(36).Name=   "CAMRECEP_BTN"
         Columns(36).DataField=   "Column 36"
         Columns(36).DataType=   8
         Columns(36).FieldLen=   256
         Columns(36).Style=   4
         Columns(36).ButtonsAlways=   -1  'True
         Columns(37).Width=   3200
         Columns(37).Visible=   0   'False
         Columns(37).Caption=   "CAMRECEP"
         Columns(37).Name=   "CAMRECEP"
         Columns(37).DataField=   "Column 37"
         Columns(37).DataType=   8
         Columns(37).FieldLen=   256
         Columns(38).Width=   3200
         Columns(38).Caption=   "COSTES_DESCUENTOS_BTN"
         Columns(38).Name=   "COSTES_DESCUENTOS_BTN"
         Columns(38).DataField=   "Column 38"
         Columns(38).DataType=   8
         Columns(38).FieldLen=   256
         Columns(38).Style=   4
         Columns(38).ButtonsAlways=   -1  'True
         Columns(39).Width=   3200
         Columns(39).Visible=   0   'False
         Columns(39).Caption=   "COSTES_DESCUENTOS"
         Columns(39).Name=   "COSTES_DESCUENTOS"
         Columns(39).DataField=   "Column 39"
         Columns(39).DataType=   8
         Columns(39).FieldLen=   256
         Columns(40).Width=   3200
         Columns(40).Visible=   0   'False
         Columns(40).Caption=   "CANT_IMP_MAX_PED"
         Columns(40).Name=   "CANT_IMP_MAX_PED"
         Columns(40).DataField=   "Column 40"
         Columns(40).DataType=   5
         Columns(40).NumberFormat=   "Standard"
         Columns(40).FieldLen=   256
         Columns(41).Width=   3200
         Columns(41).Visible=   0   'False
         Columns(41).Caption=   "CANT_IMP_MAX_TOTAL"
         Columns(41).Name=   "CANT_IMP_MAX_TOTAL"
         Columns(41).DataField=   "Column 41"
         Columns(41).DataType=   5
         Columns(41).NumberFormat=   "Standard"
         Columns(41).FieldLen=   256
         Columns(42).Width=   3200
         Columns(42).Caption=   "DCond.Suministro"
         Columns(42).Name=   "COND_SUMINISTRO"
         Columns(42).DataField=   "Column 42"
         Columns(42).DataType=   8
         Columns(42).FieldLen=   256
         Columns(42).Style=   4
         Columns(42).ButtonsAlways=   -1  'True
         Columns(42).StyleSet=   "HayUniPed"
         Columns(43).Width=   3200
         Columns(43).Visible=   0   'False
         Columns(43).Caption=   "TIPORECEPCION"
         Columns(43).Name=   "TIPORECEPCION"
         Columns(43).DataField=   "Column 43"
         Columns(43).DataType=   8
         Columns(43).FieldLen=   256
         Columns(44).Width=   3200
         Columns(44).Visible=   0   'False
         Columns(44).Caption=   "COND_SUMINISTRO_HIDDEN"
         Columns(44).Name=   "COND_SUMINISTRO_HIDDEN"
         Columns(44).DataField=   "Column 44"
         Columns(44).DataType=   8
         Columns(44).FieldLen=   256
         Columns(45).Width=   2540
         Columns(45).Caption=   "IMPUESTOS_BTN"
         Columns(45).Name=   "IMPUESTOS_BTN"
         Columns(45).DataField=   "Column 45"
         Columns(45).DataType=   8
         Columns(45).FieldLen=   256
         Columns(45).Style=   4
         Columns(45).ButtonsAlways=   -1  'True
         Columns(46).Width=   3200
         Columns(46).Visible=   0   'False
         Columns(46).Caption=   "IMPUESTOS_HIDDEN"
         Columns(46).Name=   "IMPUESTOS_HIDDEN"
         Columns(46).DataField=   "Column 46"
         Columns(46).DataType=   11
         Columns(46).FieldLen=   256
         _ExtentX        =   19897
         _ExtentY        =   10821
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CheckBox chkVerBajas 
         Caption         =   "Ver bajas l�gicas"
         Height          =   255
         Left            =   3360
         TabIndex        =   1
         Top             =   30
         Width           =   3675
      End
      Begin MSComctlLib.TreeView tvwCategorias 
         Height          =   6570
         Left            =   150
         TabIndex        =   9
         Top             =   360
         Width           =   6585
         _ExtentX        =   11615
         _ExtentY        =   11589
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picNavigateCat 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   120
         ScaleHeight     =   420
         ScaleWidth      =   13335
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   6945
         Width           =   13335
         Begin VB.CommandButton cmdExcel 
            Caption         =   "Dcarga adrt�culos Excel"
            Height          =   345
            Index           =   5
            Left            =   12420
            TabIndex        =   80
            Top             =   60
            Width           =   1815
         End
         Begin VB.CommandButton cmdExcel 
            Caption         =   "Dplantilla carga adrt�culos"
            Height          =   345
            Index           =   4
            Left            =   10470
            TabIndex        =   78
            Top             =   60
            Width           =   1965
         End
         Begin VB.CommandButton cmdExcel 
            Caption         =   "DImportar precios"
            Height          =   345
            Index           =   1
            Left            =   8910
            TabIndex        =   77
            Top             =   60
            Width           =   1485
         End
         Begin VB.CommandButton cmdExcel 
            Caption         =   "DExportar precios"
            Height          =   345
            Index           =   2
            Left            =   7440
            TabIndex        =   76
            Top             =   60
            Width           =   1455
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            Height          =   345
            Left            =   6390
            TabIndex        =   8
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdResCat 
            Caption         =   "&Restaurar"
            Height          =   345
            Left            =   4290
            TabIndex        =   6
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdBuscarCat 
            Caption         =   "&Buscar"
            Height          =   345
            Left            =   5340
            TabIndex        =   7
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliCat 
            Caption         =   "&Eliminar"
            Height          =   345
            Index           =   0
            Left            =   2160
            TabIndex        =   4
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdModifCat 
            Caption         =   "&Modificar"
            Height          =   345
            Left            =   1110
            TabIndex        =   3
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdAnyaCat 
            Caption         =   "&A�adir"
            Height          =   345
            Index           =   0
            Left            =   60
            TabIndex        =   2
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdBajaLogica 
            Caption         =   "Baja L&�gica"
            Height          =   345
            Left            =   3225
            TabIndex        =   5
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.Label lblLeyenda 
         BackColor       =   &H000080FF&
         Caption         =   "L�nea de cat�logo incompleta"
         ForeColor       =   &H80000014&
         Height          =   200
         Left            =   -66500
         TabIndex        =   32
         Top             =   60
         Visible         =   0   'False
         Width           =   2900
      End
      Begin VB.Label lblCategoria 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   -74880
         TabIndex        =   27
         Top             =   420
         Width           =   11250
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":188C
            Key             =   "CAT"
            Object.Tag             =   "CAT"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":199E
            Key             =   "SEGURO"
            Object.Tag             =   "SEGURO"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":1DF0
            Key             =   "Raiz"
            Object.Tag             =   "RAIZ"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":28BA
            Key             =   "BAJASEG"
            Object.Tag             =   "BAJASEG"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":2D0C
            Key             =   "BAJALOG2"
            Object.Tag             =   "BAJALOG2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":30AF
            Key             =   "BAJALOG1"
            Object.Tag             =   "BAJALOG1"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":344C
            Key             =   "CAT_INT"
            Object.Tag             =   "CAT_INT"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCatalogo.frx":3A76
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCatalogo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Constantes
Private Const m_sSumar As String = "+"
Private Const m_sSumarPorcentaje As String = "+%"
Private Const m_sRestar As String = "-"
Private Const m_sRestarPorcentaje As String = "-%"

'Variables de seguridad
Private bModifCat As Boolean
Private bConsultBajasLog As Boolean
Private bModifBajasLog As Boolean
Private bModifAdj As Boolean
Private bModifAdjPrecios As Boolean
Private bPrecioExcel As Boolean
Private bArticulosExcel As Boolean
Private bModifAdjCat As Boolean
Private bCatBajaLog As Boolean
Public bConsultProce As Boolean

Private m_bRespetarCombo As Boolean
Private bRespetarCheck As Boolean
'seguridad catalago
Private bConsultSegurCat As Boolean
Private bModifSegurCat As Boolean
Private bModifAccFSEPSegurCat As Boolean
Private bRUOSegurCat As Boolean
Private bRPerfUOSegurCat As Boolean
Private bRDepSegurCat As Boolean
Private bRUsuAprov As Boolean
Private bRMat As Boolean
Private bRDest As Boolean
Private bRConProceAper As Boolean

Public oCategoriasNivel1 As CCategoriaSN1
Public g_oAdjun As CAdjunto
Public oLineasCatalogo As CLineasCatalogo
Public oLineasAAnyadir As CLineasCatalogo
Public oLineaCatalogoSeleccionada As CLineaCatalogo
Private oLineaCatalogoEnEdicion As CLineaCatalogo
Private oUnidades As CUnidades
Private oDestinos As CDestinos
Private oMonedas As CMonedas 'Modificado el 22-2-2006
                             'Necesario xa cargar el grid de monedas
Private oUniPed As CUnidadPedido
'Campos pesonalizados
Private m_oIdiomas As CIdiomas
Private m_bModoEdicion As Boolean
Private m_oCampos As CCampos
Private m_oCamposR As CCampos
Public g_oCampos As CCampos

Private m_sFijo As String
Private m_sOpcionalSimple As String
Private m_sOpcionalExcluyente As String
Private m_sObligatorioExcluyente As String
Private m_sOperacionNoValida As String
Private m_sTipoNoValido As String
Private m_sNoCosteSeleccionado As String
Private m_sEliminarCoste As String
Private m_sEliminarDescuento As String
Private m_sNoDescuentoSeleccionado As String
Private m_sIntroduceGrupoCoste As String
Private m_sIntroduceGrupoDescuento As String
Private m_oCosteEnEdicion As CAtributo
Private m_oDescuentoEnEdicion As CAtributo
Private m_bErrorUpdate As Boolean
Private m_iRowActual As Long
Private m_bA�adiendoCoste As Boolean 'Variable que nos indicara cuando estamos a�adiendo un coste
Private m_bA�adiendoDescuento As Boolean 'Variable que nos indicara cuando estamos a�adiendo un coste
Private m_iNivelCategoriaSeleccionada As Integer

'Costes
Private m_oCostes As CAtributos
Public g_oCostesA�adir As CAtributos
'Descuentos
Private m_oDescuentos As CAtributos
Public g_oDescuentosA�adir As CAtributos

' Variable de control de flujo
Public Accion As AccionesSummit

' Variables para interactuar con otros forms
Public oCategoria1Seleccionada As CCategoriaN1
Public oCategoria2Seleccionada As CCategoriaN2
Public oCategoria3Seleccionada As CCategoriaN3
Public oCategoria4Seleccionada As CCategoriaN4
Public oCategoria5Seleccionada As CCategoriaN5
Public oIBaseDatos As IBaseDatos
Private m_oProvesSeleccionados As CProveedores
Private m_oProves As CProveedores
Private iNivel As Integer
Public bCancelarEsp As Boolean
Public sComentario As String

'Variables para la grid
Private bAnyaError As Boolean
Private bModError As Boolean
Private bValError As Boolean
Public bModoEdicion As Boolean
Private oIBAseDatosEnEdicion As IBaseDatos
Private oIBAseDatosEnEdicionUnidades As IBaseDatos
Private DesactivarTabClick As Boolean

''' Variables de control
Private bAnyadir As Boolean

Public CAT1Seleccionada As Variant
Public CAT2Seleccionada As Variant
Public CAT3Seleccionada As Variant
Public CAT4Seleccionada As Variant
Public CAT5Seleccionada As Variant
Public SeguridadCat As Variant
Public bContinuarCambioCat As Boolean
Public CopiarACategoria As Long
Public CopiarANivel As Integer
Public SubirSeguridad As Integer

Private m_bCargandoAdj As Boolean
Public g_bRefrescandoUniPed As Boolean
Private m_bRespetar As Boolean

'Seguridad para las solicitudes de ofertas:
Private m_bRSolicEquipo As Boolean
Private m_bRSolicAsig As Boolean
Private m_bRSolicUO As Boolean
Private m_bRSolicPerfUO As Boolean


'Idiomas
Private sIdiCategorias As String
Private sIdiCategoria As String
Private sIdiAnyadirSubCat As String
Private sIdiAnyadirCat As String
Private sIdiModificar As String
Private sIdiTitulo(1) As String
Private sIdiModos(1) As String
Private sIdiOrden As String
Private sIdiLineas As String
Private sIdiCaption  As String
Private sIdiFechaDespub As String
Private sIdiDenominacion As String
Private sIdiProve As String
Private sIdiDest As String
Private sIdiUB As String
Private sIdiPrecioUB As String
Private sIdiUP As String
Private sIdiMon As String
Private sIdiFC As String
Private sPorcenDesvio As String
Private sIdiCantMin As String
Private sIdiSolicitud As String
Private sIdiSelecAdjunto As String
Private sIdiTodosArchivos As String
Private sIdiElArchivo As String
Private sIdiGuardar As String
Private sIdiTipoOrig As String
Private sLabelForm As String
Private sIdiError(1 To 6) As String
Private m_skb As String
Private m_aIdentificadores As Collection

'Guardaremos los nombres de los temporales aqui, para borrarlos al salir
Private sayFileNames() As String

Public g_ofrmDetalleSolic As frmSolicitudDetalle

' Variables para func. grid
Private m_bCargarComboDesdeDD As Boolean

'Variables para preseleccionar una categor�a cuando se carga el formulario y mostrar sus adjudicaciones
Public g_oNodx As MSComctlLib.node
Public g_bCargarAdjudicacionesNodoSeleccionado As Boolean
Public g_sOrigen As String
Public g_ofrmATRIBMod As frmAtribMod
Private sConfirmEliminarCampos As String
Private sCamposNoSeleccionados As String

Private sCabecera As String
Private sLinea As String
Private sIdiTrue As String
Private sIdiFalse As String

Private WithEvents m_oFrmAnyaAdj As frmCATALAnyaAdj
Attribute m_oFrmAnyaAdj.VB_VarHelpID = -1

Private Sub CargarDatosPedido(oGrid As SSDBGrid, bRecep As Boolean, oCat As Object)
Dim oCampos As CCampos
oGrid.RemoveAll
oGrid.Columns("VAL").DropDownHwnd = 0
With oCat
    Set oCampos = .CamposPersonalizados.CargarDatos(.Id, iNivel, bRecep)
    If Not oCampos Is Nothing Then
        If oCampos.Count > 0 Then
            Dim oCam As CCampo
            For Each oCam In oCampos
                If oCam.Tipo = TipoBoolean Then
                    If IsNull(oCam.valor) Then
                        oGrid.AddItem oCam.Cod & Chr(m_lSeparador) & oCam.Den & Chr(m_lSeparador) & oCam.Obligatorio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCam.ambito & Chr(m_lSeparador) & oCam.interno & Chr(m_lSeparador) & oCam.TipoIntroduccion & Chr(m_lSeparador) & oCam.Tipo & Chr(m_lSeparador) & oCam.Minimo & Chr(m_lSeparador) & oCam.Maximo & Chr(m_lSeparador) & oCam.AtribID & Chr(m_lSeparador) & oCam.Id & Chr(m_lSeparador) & oCam.Lista_Externa & Chr(m_lSeparador) & oCam.MostrarEnRecep
                    Else
                        If StrToDbl0(oCam.valor) = 0 Then
                            oGrid.AddItem oCam.Cod & Chr(m_lSeparador) & oCam.Den & Chr(m_lSeparador) & oCam.Obligatorio & Chr(m_lSeparador) & sIdiFalse & Chr(m_lSeparador) & oCam.ambito & Chr(m_lSeparador) & oCam.interno & Chr(m_lSeparador) & oCam.TipoIntroduccion & Chr(m_lSeparador) & oCam.Tipo & Chr(m_lSeparador) & oCam.Minimo & Chr(m_lSeparador) & oCam.Maximo & Chr(m_lSeparador) & oCam.AtribID & Chr(m_lSeparador) & oCam.Id & Chr(m_lSeparador) & oCam.Lista_Externa & Chr(m_lSeparador) & oCam.MostrarEnRecep
                        Else
                            oGrid.AddItem oCam.Cod & Chr(m_lSeparador) & oCam.Den & Chr(m_lSeparador) & oCam.Obligatorio & Chr(m_lSeparador) & sIdiTrue & Chr(m_lSeparador) & oCam.ambito & Chr(m_lSeparador) & oCam.interno & Chr(m_lSeparador) & oCam.TipoIntroduccion & Chr(m_lSeparador) & oCam.Tipo & Chr(m_lSeparador) & oCam.Minimo & Chr(m_lSeparador) & oCam.Maximo & Chr(m_lSeparador) & oCam.AtribID & Chr(m_lSeparador) & oCam.Id & Chr(m_lSeparador) & oCam.Lista_Externa & Chr(m_lSeparador) & oCam.MostrarEnRecep
                        End If
                    End If
                Else
                    oGrid.AddItem oCam.Cod & Chr(m_lSeparador) & oCam.Den & Chr(m_lSeparador) & oCam.Obligatorio & Chr(m_lSeparador) & NullToStr(oCam.valor) & Chr(m_lSeparador) & oCam.ambito & Chr(m_lSeparador) & oCam.interno & Chr(m_lSeparador) & oCam.TipoIntroduccion & Chr(m_lSeparador) & oCam.Tipo & Chr(m_lSeparador) & oCam.Minimo & Chr(m_lSeparador) & oCam.Maximo & Chr(m_lSeparador) & oCam.AtribID & Chr(m_lSeparador) & oCam.Id & Chr(m_lSeparador) & oCam.Lista_Externa & Chr(m_lSeparador) & oCam.MostrarEnRecep
                End If
            Next
            If bRecep Then
                cmdEliCampo.Enabled = True
            Else
                cmdEliCampoR.Enabled = True
            End If
        Else
            If bRecep Then
                cmdEliCampo.Enabled = False
            Else
                cmdEliCampoR.Enabled = False
            End If
        End If
        
    End If
    If bRecep Then
        Set m_oCamposR = oCampos
    Else
        Set m_oCampos = oCampos
    End If
End With
End Sub

Private Sub CargarGridConUnidadesPedido()

    ''' * Objetivo: Cargar combo con la coleccion de unidades de pedido
       
    sdbddUnidadesPedido.RemoveAll
    
    If oLineaCatalogoSeleccionada Is Nothing Then Exit Sub
    
    For Each oUniPed In oLineaCatalogoSeleccionada.UnidadesPedido
        If oUniPed.FactorConversion = 0 Then
            sdbddUnidadesPedido.AddItem oUniPed.UnidadPedido & Chr(m_lSeparador) & oUniPed.UPDen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & NullToStr(oUniPed.CantidadMinima) & Chr(m_lSeparador) & NullToStr(oUniPed.CantidadMaximaPed) & Chr(m_lSeparador) & NullToStr(oUniPed.CantidadMaximaTotal)
        Else
            sdbddUnidadesPedido.AddItem oUniPed.UnidadPedido & Chr(m_lSeparador) & oUniPed.UPDen & Chr(m_lSeparador) & NullToStr(oUniPed.FactorConversion) & Chr(m_lSeparador) & NullToStr(oUniPed.CantidadMinima) & Chr(m_lSeparador) & NullToStr(oUniPed.CantidadMaximaPed) & Chr(m_lSeparador) & NullToStr(oUniPed.CantidadMaximaTotal)
        End If
    Next
    
    If sdbddUnidadesPedido.Rows = 0 Then
        sdbddUnidadesPedido.AddItem ""
    End If
    
End Sub

Private Sub CargarGridConUnidades()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oUni As CUnidad
    
    sdbddUnidades.RemoveAll
    
    For Each oUni In oUnidades
        sdbddUnidades.AddItem oUni.Cod & Chr(m_lSeparador) & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next
    
    If sdbddUnidades.Rows = 0 Then
        sdbddUnidades.AddItem ""
    End If
    
End Sub


Private Sub ArrangeDatosCategoria()
Dim nodx As MSComctlLib.node
Dim bSinDatosPers As Boolean

Set nodx = tvwCategorias.selectedItem

bSinDatosPers = False
If nodx Is Nothing Then
    bSinDatosPers = True
Else
    If nodx.Children > 0 Then bSinDatosPers = True
End If

On Error Resume Next
txtObsAdjun.Width = picCategoria.Width - 165
lstvwAdjun.Width = txtObsAdjun.Width
txtObsAdjun.Height = picCategoria.Height / 3 - 900
lstvwAdjun.Height = txtObsAdjun.Height
Label1(1).Top = txtObsAdjun.Top + txtObsAdjun.Height + 45
lstvwAdjun.Top = Label1(1).Top + Label1(1).Height + 45
picBotonAdjun.Top = lstvwAdjun.Top + lstvwAdjun.Height + 15
picBotonAdjun.Left = lstvwAdjun.Width - picBotonAdjun.Width
lstvwAdjun.ColumnHeaders.Item(1).Width = lstvwAdjun.Width * 0.3
lstvwAdjun.ColumnHeaders.Item(2).Width = lstvwAdjun.Width * 0.18
lstvwAdjun.ColumnHeaders.Item(3).Width = lstvwAdjun.Width * 0.54 - 300
picNavigateCat.Top = tvwCategorias.Height + 545

tabDatos.Top = picBotonAdjun.Top + picBotonAdjun.Height + 50
tabDatos.Width = picCategoria.Width - 100
tabDatos.Height = picCategoria.Height - (picBotonAdjun.Top + picBotonAdjun.Height) - 200

picProveedores.Height = tabDatos.Height - 600
picProveedores.Width = tabDatos.Width - 340

picCamposPedido.Height = picProveedores.Height
picCamposPedido.Width = picProveedores.Width

picCamposRecepcion.Height = picProveedores.Height
picCamposRecepcion.Width = picProveedores.Width
picModEmision.Height = picProveedores.Height
picModEmision.Width = picProveedores.Width
    
sdbgProves.Height = picProveedores.Height - 900
sdbgProves.Width = picProveedores.Width - 500

sdbgProves.Columns(0).Width = (sdbgProves.Width - 570) * 0.3
sdbgProves.Columns(1).Width = (sdbgProves.Width - 570) * 0.7

cmdAnyaCat(1).Top = sdbgProves.Top + sdbgProves.Height + 45
cmdEliCat(1).Top = cmdAnyaCat(1).Top

sdbgCamposPedido.Height = picCamposPedido.Height - 900
sdbgCamposPedido.Width = picCamposPedido.Width - 500
sdbgCamposPedido.Columns("COD").Width = (sdbgCamposPedido.Width - 570) * 0.1  '13
sdbgCamposPedido.Columns("DEN").Width = (sdbgCamposPedido.Width - 570) * 0.28 ' 3
sdbgCamposPedido.Columns("OBL").Width = (sdbgCamposPedido.Width - 570) * 0.05 '7
sdbgCamposPedido.Columns("VAL").Width = (sdbgCamposPedido.Width - 570) * 0.22 '5
sdbgCamposPedido.Columns("AMB").Width = (sdbgCamposPedido.Width - 570) * 0.15 '7
sdbgCamposPedido.Columns("INT").Width = (sdbgCamposPedido.Width - 570) * 0.11 '2
sdbgCamposPedido.Columns("VERENRECEP").Width = (sdbgCamposPedido.Width - 570) * 0.25

sdbgCamposRecepcion.Height = picCamposRecepcion.Height - 900
sdbgCamposRecepcion.Width = picCamposRecepcion.Width - 500
sdbgCamposRecepcion.Columns("COD").Width = (sdbgCamposRecepcion.Width - 570) * 0.13
sdbgCamposRecepcion.Columns("DEN").Width = (sdbgCamposRecepcion.Width - 570) * 0.3
sdbgCamposRecepcion.Columns("OBL").Width = (sdbgCamposRecepcion.Width - 570) * 0.07
sdbgCamposRecepcion.Columns("VAL").Width = (sdbgCamposRecepcion.Width - 570) * 0.25
sdbgCamposRecepcion.Columns("AMB").Width = (sdbgCamposRecepcion.Width - 570) * 0.17
sdbgCamposRecepcion.Columns("INT").Width = (sdbgCamposRecepcion.Width - 570) * 0.12

cmdAnyaCampo.Top = sdbgCamposPedido.Top + sdbgCamposPedido.Height + 45
cmdEliCampo.Top = cmdAnyaCampo.Top
cmdAnyaCampoR.Top = sdbgCamposRecepcion.Top + sdbgCamposRecepcion.Height + 45
cmdEliCampoR.Top = cmdAnyaCampoR.Top
If bSinDatosPers Then
    picModEmision.Visible = False
    picCostes.Height = picProveedores.Height
    picCostes.Width = picProveedores.Width
    picDescuentos.Height = picProveedores.Height
    picDescuentos.Width = picProveedores.Width
    sdbgCostesCategoria.Height = picCostes.Height - 900
    sdbgCostesCategoria.Width = picCostes.Width - 500
    sdbgDescuentosCategoria.Height = picDescuentos.Height - 900
    sdbgDescuentosCategoria.Width = picDescuentos.Width - 500
    cmdA�adirCoste.Top = sdbgCostesCategoria.Top + sdbgCostesCategoria.Height + 45
    cmdEliminarCoste.Top = cmdA�adirCoste.Top
    cmdA�adirDescuento.Top = sdbgDescuentosCategoria.Top + sdbgDescuentosCategoria.Height + 45
    cmdEliminarDescuento.Top = cmdA�adirDescuento.Top
Else
    picModEmision.Visible = True
    sdbgProves.Width = txtObsAdjun.Width
    txtObsAdjun.Width = picCategoria.Width - 165
    lstvwAdjun.Width = txtObsAdjun.Width
    chkModEmision(0).Width = sdbgProves.Width - 200
    chkModEmision(1).Width = sdbgProves.Width - chkModEmision(1).Left - 200
    picBotonAdjun.Left = lstvwAdjun.Width - picBotonAdjun.Width
    lstvwAdjun.ColumnHeaders.Item(1).Width = lstvwAdjun.Width * 0.3
    lstvwAdjun.ColumnHeaders.Item(2).Width = lstvwAdjun.Width * 0.18
    lstvwAdjun.ColumnHeaders.Item(3).Width = lstvwAdjun.Width * 0.54 - 300
End If
End Sub


Public Sub Arrange()
Dim dblA As Double
    
    On Error Resume Next
    
    tabCatalogo.Width = Me.Width - 145
    tabCatalogo.Height = Me.Height - 495
    dblA = tabCatalogo.Width - 400
    tvwCategorias.Width = dblA * 0.55
    tvwCategorias.Height = tabCatalogo.Height - 1035
    picNavigateCat.Top = tvwCategorias.Height + tvwCategorias.Top + 30
    picNavigateCat.Width = Me.Width - 100
    picCategoria.Width = dblA * 0.45
    picCategoria.Left = tvwCategorias.Left + tvwCategorias.Width + 100
    picCategoria.Height = tabCatalogo.Height - 1035
    
    ArrangeDatosCategoria
    
    sdbgAdjudicaciones.Width = tabCatalogo.Width - 350
    sdbgAdjudicaciones.Height = tvwCategorias.Height - lblCategoria.Height - 150
    lblCategoria.Width = sdbgAdjudicaciones.Width
    picNavigateAdj.Top = picNavigateCat.Top
    picNavigateAdj.Width = sdbgAdjudicaciones.Width
    cmdModoEdicion.Left = sdbgAdjudicaciones.Width - cmdModoEdicion.Width
End Sub

Private Sub CargarLineasCatalogo(ByRef node As MSComctlLib.node, ByRef iOrden As Integer, ByRef bHom As Boolean, ByRef oLineaCatalogo As CLineaCatalogo)
Set oLineaCatalogo = oFSGSRaiz.Generar_CLineaCatalogo
    
Select Case Left(node.Tag, 4)

    Case "CAT1"
            Set oCategoria1Seleccionada = Nothing
            Set oCategoria1Seleccionada = oFSGSRaiz.Generar_CCategoriaN1
            oCategoria1Seleccionada.Id = DevolverId(node)
            oCategoria1Seleccionada.Seguridad = DevolverSeguridadCategoria(node)
            
            Set oLineasCatalogo = oCategoria1Seleccionada.CargarLineasDeCatalogo(bHom, False, iOrden, False, chkVerBajas.Value, FSEPConf, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True)
            If oLineasCatalogo.Count = 0 Then
                oLineaCatalogo.Cat1 = oCategoria1Seleccionada.Id
                oLineaCatalogo.Cat2 = Null
                oLineaCatalogo.Cat3 = Null
                oLineaCatalogo.Cat4 = Null
                oLineaCatalogo.Cat5 = Null
                If oCategoria1Seleccionada.Seguridad = 1 Then
                    oLineaCatalogo.SeguridadCat = oCategoria1Seleccionada.Id
                    oLineaCatalogo.SeguridadNivel = 1
                End If
            End If

    Case "CAT2"
            Set oCategoria2Seleccionada = Nothing
            Set oCategoria2Seleccionada = oFSGSRaiz.Generar_CCategoriaN2
            oCategoria2Seleccionada.Id = DevolverId(node)
            oCategoria2Seleccionada.Cat1 = DevolverId(node.Parent)
            oCategoria2Seleccionada.Seguridad = DevolverSeguridadCategoria(node)
            
            Set oLineasCatalogo = oCategoria2Seleccionada.CargarLineasDeCatalogo(bHom, False, iOrden, False, chkVerBajas.Value, FSEPConf, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True)
            If oLineasCatalogo.Count = 0 Then
                oLineaCatalogo.Cat1 = oCategoria2Seleccionada.Cat1
                oLineaCatalogo.Cat2 = oCategoria2Seleccionada.Id
                oLineaCatalogo.Cat3 = Null
                oLineaCatalogo.Cat4 = Null
                oLineaCatalogo.Cat5 = Null
                'Si la categoria de nivel 2 tiene la seguridad configurada, se le indica a la linea del catalogo
                If oCategoria2Seleccionada.Seguridad = 1 Then
                    oLineaCatalogo.SeguridadCat = oCategoria2Seleccionada.Id
                    oLineaCatalogo.SeguridadNivel = 2
                End If
                'Si la categoria de nivel 2 no tiene la seguridad configurada, se mira en la categoria padre de nivel 1
                If oCategoria2Seleccionada.Seguridad = 0 Then
                    oCategoria2Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent)
                    If oCategoria2Seleccionada.Seguridad = 1 Then
                        oLineaCatalogo.SeguridadCat = oCategoria2Seleccionada.Cat1
                        oLineaCatalogo.SeguridadNivel = 1
                    End If
                End If
                
            End If

    Case "CAT3"
            Set oCategoria3Seleccionada = Nothing
            Set oCategoria3Seleccionada = oFSGSRaiz.Generar_CCategoriaN3
            oCategoria3Seleccionada.Id = DevolverId(node)
            oCategoria3Seleccionada.Cat2 = DevolverId(node.Parent)
            oCategoria3Seleccionada.Cat1 = DevolverId(node.Parent.Parent)
            oCategoria3Seleccionada.Seguridad = DevolverSeguridadCategoria(node)
            
            Set oLineasCatalogo = oCategoria3Seleccionada.CargarLineasDeCatalogo(bHom, False, iOrden, False, chkVerBajas.Value, FSEPConf, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True)
            If oLineasCatalogo.Count = 0 Then
                oLineaCatalogo.Cat1 = oCategoria3Seleccionada.Cat1
                oLineaCatalogo.Cat2 = oCategoria3Seleccionada.Cat2
                oLineaCatalogo.Cat3 = oCategoria3Seleccionada.Id
                oLineaCatalogo.Cat4 = Null
                oLineaCatalogo.Cat5 = Null
                
                'Si la categoria de nivel 3 tiene la seguridad configurada, se le indica a la linea del catalogo
                If oCategoria3Seleccionada.Seguridad = 1 Then
                    oLineaCatalogo.SeguridadCat = oCategoria3Seleccionada.Id
                    oLineaCatalogo.SeguridadNivel = 3
                End If
                'Si la categoria de nivel 3 no tiene la seguridad configurada, se mira en la categoria padre de nivel 2
                If oCategoria3Seleccionada.Seguridad = 0 Then
                    oCategoria3Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent)
                    If oCategoria3Seleccionada.Seguridad = 1 Then
                        oLineaCatalogo.SeguridadCat = oCategoria3Seleccionada.Cat2
                        oLineaCatalogo.SeguridadNivel = 2
                    Else
                        If oCategoria3Seleccionada.Seguridad = 0 Then
                            'Si la categoria de nivel 2 no tiene la seguridad configurada, se mira en la categoria padre de nivel 1
                            oCategoria3Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent.Parent)
                            If oCategoria3Seleccionada.Seguridad = 1 Then
                                oLineaCatalogo.SeguridadCat = oCategoria3Seleccionada.Cat1
                                oLineaCatalogo.SeguridadNivel = 1
                            End If
                        End If
                    End If
                End If
                
                
            End If

    Case "CAT4"
            Set oCategoria4Seleccionada = Nothing
            Set oCategoria4Seleccionada = oFSGSRaiz.Generar_CCategoriaN4
            oCategoria4Seleccionada.Id = DevolverId(node)
            oCategoria4Seleccionada.Cat3 = DevolverId(node.Parent)
            oCategoria4Seleccionada.Cat2 = DevolverId(node.Parent.Parent)
            oCategoria4Seleccionada.Cat1 = DevolverId(node.Parent.Parent.Parent)
            oCategoria4Seleccionada.Seguridad = DevolverSeguridadCategoria(node)
            
            Set oLineasCatalogo = oCategoria4Seleccionada.CargarLineasDeCatalogo(bHom, False, iOrden, False, chkVerBajas.Value, FSEPConf, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True)
            If oLineasCatalogo.Count = 0 Then
                oLineaCatalogo.Cat1 = oCategoria4Seleccionada.Cat1
                oLineaCatalogo.Cat2 = oCategoria4Seleccionada.Cat2
                oLineaCatalogo.Cat3 = oCategoria4Seleccionada.Cat3
                oLineaCatalogo.Cat4 = oCategoria4Seleccionada.Id
                oLineaCatalogo.Cat5 = Null
                'Si la categoria de nivel 4 tiene la seguridad configurada, se le indica a la linea del catalogo
                If oCategoria4Seleccionada.Seguridad = 1 Then
                    oLineaCatalogo.SeguridadCat = oCategoria4Seleccionada.Id
                    oLineaCatalogo.SeguridadNivel = 4
                End If
                'Si la categoria de nivel 4 no tiene la seguridad configurada, se mira en la categoria padre de nivel 3
                If oCategoria4Seleccionada.Seguridad = 0 Then
                    oCategoria4Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent)
                    If oCategoria4Seleccionada.Seguridad = 1 Then
                        oLineaCatalogo.SeguridadCat = oCategoria4Seleccionada.Cat3
                        oLineaCatalogo.SeguridadNivel = 3
                    Else
                        If oCategoria4Seleccionada.Seguridad = 0 Then
                            'Si la categoria de nivel 3 no tiene la seguridad configurada, se mira en la categoria padre de nivel 2
                            oCategoria4Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent.Parent)
                            If oCategoria4Seleccionada.Seguridad = 1 Then
                                oLineaCatalogo.SeguridadCat = oCategoria4Seleccionada.Cat2
                                oLineaCatalogo.SeguridadNivel = 2
                            Else
                                'Si la categoria de nivel 2 no tiene la seguridad configurada, se mira en la categoria padre de nivel 1
                                oCategoria4Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent.Parent.Parent)
                                If oCategoria4Seleccionada.Seguridad = 1 Then
                                    oLineaCatalogo.SeguridadCat = oCategoria4Seleccionada.Cat1
                                    oLineaCatalogo.SeguridadNivel = 1
                                End If
                            End If
                        End If
                    End If
                End If
            End If
    
    Case "CAT5"
            Set oCategoria5Seleccionada = Nothing
            Set oCategoria5Seleccionada = oFSGSRaiz.Generar_CCategoriaN5
            oCategoria5Seleccionada.Id = DevolverId(node)
            oCategoria5Seleccionada.Cat4 = DevolverId(node.Parent)
            oCategoria5Seleccionada.Cat3 = DevolverId(node.Parent.Parent)
            oCategoria5Seleccionada.Cat2 = DevolverId(node.Parent.Parent.Parent)
            oCategoria5Seleccionada.Cat1 = DevolverId(node.Parent.Parent.Parent.Parent)
            oCategoria5Seleccionada.Seguridad = DevolverSeguridadCategoria(node)
            
            Set oLineasCatalogo = oCategoria5Seleccionada.CargarLineasDeCatalogo(bHom, False, iOrden, chkVerBajas.Value, FSEPConf, bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True)
            If oLineasCatalogo.Count = 0 Then
                oLineaCatalogo.Cat1 = oCategoria5Seleccionada.Cat1
                oLineaCatalogo.Cat2 = oCategoria5Seleccionada.Cat2
                oLineaCatalogo.Cat3 = oCategoria5Seleccionada.Cat3
                oLineaCatalogo.Cat4 = oCategoria5Seleccionada.Cat4
                oLineaCatalogo.Cat5 = oCategoria5Seleccionada.Id
                'Si la categoria de nivel 5 tiene la seguridad configurada, se le indica a la linea del catalogo
                If oCategoria5Seleccionada.Seguridad = 1 Then
                    oLineaCatalogo.SeguridadCat = oCategoria5Seleccionada.Id
                    oLineaCatalogo.SeguridadNivel = 5
                End If
                'Si la categoria de nivel 5 no tiene la seguridad configurada, se mira en la categoria padre de nivel 4
                If oCategoria5Seleccionada.Seguridad = 0 Then
                    oCategoria5Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent)
                    If oCategoria5Seleccionada.Seguridad = 1 Then
                        oLineaCatalogo.SeguridadCat = oCategoria5Seleccionada.Cat4
                        oLineaCatalogo.SeguridadNivel = 4
                    Else
                        If oCategoria5Seleccionada.Seguridad = 0 Then
                            'Si la categoria de nivel 4 no tiene la seguridad configurada, se mira en la categoria padre de nivel 3
                            oCategoria5Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent.Parent)
                            If oCategoria5Seleccionada.Seguridad = 1 Then
                                oLineaCatalogo.SeguridadCat = oCategoria5Seleccionada.Cat3
                                oLineaCatalogo.SeguridadNivel = 3
                            Else
                                'Si la categoria de nivel 3 no tiene la seguridad configurada, se mira en la categoria padre de nivel 2
                                oCategoria5Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent.Parent.Parent)
                                If oCategoria5Seleccionada.Seguridad = 1 Then
                                    oLineaCatalogo.SeguridadCat = oCategoria5Seleccionada.Cat2
                                    oLineaCatalogo.SeguridadNivel = 2
                                Else
                                    'Si la categoria de nivel 2 no tiene la seguridad configurada, se mira en la categoria padre de nivel 1
                                    oCategoria5Seleccionada.Seguridad = DevolverSeguridadCategoria(node.Parent.Parent.Parent.Parent)
                                    If oCategoria5Seleccionada.Seguridad = 1 Then
                                        oLineaCatalogo.SeguridadCat = oCategoria5Seleccionada.Cat1
                                        oLineaCatalogo.SeguridadNivel = 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
End Select
End Sub

''' <summary>
''' Carga los textos del m�dulo
''' </summary>
''' <remarks>Llamada desde Form_Load; Tiempo m�ximo</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        tabCatalogo.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        'tabCatalogo.TabCaption(1) = Ador(0).Value '3Adjudicaciones
        Ador.MoveNext
        'Jerarqu�a de categor�as
        sIdiCategorias = Ador(0).Value
        Ador.MoveNext
        chkVerBajas.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyaCat(0).caption = "&" & Ador(0).Value
        cmdAnyaCat(1).caption = Ador(0).Value
        cmdAnyaCampo.caption = Ador(0).Value '6 A�adir
        cmdA�adirCoste.caption = Ador(0).Value '6 A�adir
        cmdA�adirDescuento.caption = Ador(0).Value '6 A�adir
        cmdAnyaCampoR.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyaAdjudicacion.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyaArticulo.caption = Ador(0).Value
        Ador.MoveNext
        cmdModifCat.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliCat(0).caption = "&" & Ador(0).Value
        cmdEliCat(1).caption = Ador(0).Value
        cmdEliCampo.caption = Ador(0).Value '10 Eliminar
        Me.cmdEliminarCoste.caption = Ador(0).Value '10 Eliminar
        Me.cmdEliminarDescuento.caption = Ador(0).Value '10 Eliminar
        cmdEliCampoR.caption = Ador(0).Value '10 Eliminar
        Ador.MoveNext
        cmdBajaLogica.caption = Ador(0).Value
        Ador.MoveNext
        cmdResCat.caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscarCat.caption = Ador(0).Value
        Ador.MoveNext
        cmdCambiarCat.caption = Ador(0).Value
        Ador.MoveNext
        cmdModifPrecios.caption = Ador(0).Value
        Ador.MoveNext
        cmdModoEdicion.caption = Ador(0).Value
        sIdiModos(1) = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("PUB").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("DESPUB").caption = Ador(0).Value
        sIdiFechaDespub = Ador(0).Value
        If FSEPConf Then
            Ador.MoveNext
        Else
            Ador.MoveNext
            sdbgAdjudicaciones.Columns("PROCE").caption = Ador(0).Value
        End If
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("ART").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("PROVE").caption = Ador(0).Value
        sIdiProve = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("DEST").caption = Ador(0).Value
        sIdiDest = Ador(0).Value
        If FSEPConf Then
            Ador.MoveNext
        Else
            Ador.MoveNext
            sdbgAdjudicaciones.Columns("CANTADJ").caption = Ador(0).Value
        End If
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("UB").caption = Ador(0).Value
        sIdiUB = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("PrecioUB").caption = Ador(0).Value
        sdbgAdjudicaciones.Columns("PrecioUB").caption = sdbgAdjudicaciones.Columns("PrecioUB").caption & "(" & gParametrosGenerales.gsMONCEN & ")"
        sIdiPrecioUB = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("UP").caption = Ador(0).Value
        sIdiUP = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("FC").caption = Ador(0).Value
        sdbddUnidadesPedido.Columns("FC").caption = Ador(0).Value
        sIdiFC = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("OTRAS").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CANTMIN").caption = Ador(0).Value
        sdbddUnidadesPedido.Columns("CANTMIN").caption = Ador(0).Value
        sIdiCantMin = Ador(0).Value
        Ador.MoveNext
        sIdiAnyadirSubCat = Ador(0).Value
        Ador.MoveNext
        sIdiAnyadirCat = Ador(0).Value
        Ador.MoveNext
        sIdiCategoria = Ador(0).Value
        Ador.MoveNext
        sIdiModificar = Ador(0).Value
        'cmdModifCampos.caption = Ador(0).Value
        Ador.MoveNext
        sIdiModos(0) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo(1) = Ador(0).Value
        
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        sIdiOrden = Ador(0).Value
        Ador.MoveNext
        sIdiLineas = Ador(0).Value
        Ador.MoveNext
        sIdiCaption = Ador(0).Value
        Ador.MoveNext
        lblLeyenda.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscarAdj.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliAdj.caption = Ador(0).Value
        Ador.MoveNext
        sIdiSolicitud = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("SOLICIT").caption = Ador(0).Value
        Ador.MoveNext
        Label1(0).caption = Ador(0).Value
        Ador.MoveNext
        Label1(1).caption = Ador(0).Value
        Ador.MoveNext
        Label1(2).caption = Ador(0).Value
        Ador.MoveNext
        lstvwAdjun.ColumnHeaders(1).Text = Ador(0).Value
        sIdiElArchivo = Ador(0).Value
        Ador.MoveNext
        lstvwAdjun.ColumnHeaders(3).Text = Ador(0).Value
        Ador.MoveNext
        sdbgProves.Columns(0).caption = Ador(0).Value
        sdbddUnidadesPedido.Columns("COD").caption = Ador(0).Value
        sdbddUnidades.Columns("COD").caption = Ador(0).Value
        sdbddDestinos.Columns("COD").caption = Ador(0).Value
        sdbgCostesCategoria.Columns("COD").caption = Ador(0).Value
        sdbgDescuentosCategoria.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgProves.Columns(1).caption = Ador(0).Value
        sdbgAdjudicaciones.Columns("ARTDEN").caption = Ador(0).Value
        sIdiDenominacion = Ador(0).Value
        sdbddUnidadesPedido.Columns("DEN").caption = Ador(0).Value
        sdbddUnidades.Columns("DEN").caption = Ador(0).Value
        sdbddDestinos.Columns("DEN").caption = Ador(0).Value
        sdbgCostesCategoria.Columns("NOMBRE").caption = Ador(0).Value
        sdbgDescuentosCategoria.Columns("NOMBRE").caption = Ador(0).Value
        Ador.MoveNext
        sIdiGuardar = Ador(0).Value
        Ador.MoveNext
        sIdiSelecAdjunto = Ador(0).Value
        Ador.MoveNext
        sIdiTodosArchivos = Ador(0).Value
        Ador.MoveNext
        sIdiTipoOrig = Ador(0).Value
        Ador.MoveNext
        sLabelForm = Ador(0).Value
        Ador.MoveNext
        sIdiError(1) = Ador(0).Value
        Ador.MoveNext
        sIdiError(2) = Ador(0).Value
        Ador.MoveNext
        sIdiError(3) = Ador(0).Value
        Ador.MoveNext
        sIdiError(4) = Ador(0).Value
        Ador.MoveNext
        sIdiError(5) = Ador(0).Value
        Ador.MoveNext
        sIdiError(6) = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("DEST_USU").caption = Ador(0).Value
        Ador.MoveNext
        'Label1(3).caption = Ador(0).Value 'Campos Personalizados
        Ador.MoveNext
        'Label1(4).caption = Ador(0).Value 'Campo 1
        Ador.MoveNext
        'Label1(5).caption = Ador(0).Value 'Campo 2
        Ador.MoveNext
        sdbgCamposPedido.Columns("OBL").caption = Mid(Ador(0).Value, 0, 3) & "." '68 Obligatorio --> Obl.
        sdbgCamposRecepcion.Columns("OBL").caption = Mid(Ador(0).Value, 0, 3) & "." '68 Obligatorio --> Obl.
        Ador.MoveNext
        'cmdAceptarCampos.caption = Ador(0).Value
        Ador.MoveNext
        'cmdCancelarCampos.caption = Ador(0).Value
        Ador.MoveNext
        sdbgCamposPedido.Columns("INT").caption = Ador(0).Value '71 Interno
        sdbgCamposRecepcion.Columns("INT").caption = Ador(0).Value '71 Interno
        Ador.MoveNext
        lstvwAdjun.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        Ador.MoveNext
        sdbddDestinos.Columns("DIR").caption = Ador(0).Value
        Ador.MoveNext
        sdbddDestinos.Columns("POB").caption = Ador(0).Value
        Ador.MoveNext
        sdbddDestinos.Columns("CP").caption = Ador(0).Value
        Ador.MoveNext
        sdbddDestinos.Columns("PAI").caption = Ador(0).Value
        Ador.MoveNext
        sdbddDestinos.Columns("PROVI").caption = Ador(0).Value
        Ador.MoveNext
        chkModEmision(0).caption = Ador(0).Value
        Ador.MoveNext
        chkModEmision(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("MON").caption = Ador(0).Value
        sIdiMon = Ador(0).Value
        
        Ador.MoveNext
        tabDatos.TabCaption(0) = Ador(0).Value
        
        Ador.MoveNext
        Ador.MoveNext
        tabDatos.TabCaption(5) = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        tabDatos.TabCaption(1) = Ador(0).Value '88 Campos de pedido
        sConfirmEliminarCampos = Ador(0).Value '88 Campos de pedido
        Ador.MoveNext
        tabDatos.TabCaption(3) = Ador(0).Value '89 Costes
        Ador.MoveNext
        tabDatos.TabCaption(4) = Ador(0).Value '90 Descuentos
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("VAL").caption = Ador(0).Value '91 Valor
        sdbgCostesCategoria.Columns("VALOR").caption = Ador(0).Value '91 Valor
        sdbgDescuentosCategoria.Columns("VALOR").caption = Ador(0).Value '91 Valor
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("AMB").caption = Ador(0).Value '92 �mbito
        sdbgCostesCategoria.Columns("AMBITO").caption = Ador(0).Value '92 �mbito
        sdbgDescuentosCategoria.Columns("AMBITO").caption = Ador(0).Value '92 �mbito
        Ador.MoveNext
        'sConfirmEliminarCampos = Ador(0).Value '93 �Desea eliminar los campos de pedido seleccionados?
        Ador.MoveNext
        sCamposNoSeleccionados = Ador(0).Value '94 No ha seleccionado ning�n campo de pedido
        Ador.MoveNext
        sCabecera = Ador(0).Value '95 Cabecera
        Ador.MoveNext
        sLinea = Ador(0).Value '96 L�nea
        Ador.MoveNext
        tabCatalogo.TabCaption(1) = Ador(0).Value '97 L�neas del Cat�logo
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("ESP").caption = Ador(0).Value '98 Ficha art.
        Ador.MoveNext
        sIdiTrue = Ador(0).Value  '99 S�
        Ador.MoveNext
        sIdiFalse = Ador(0).Value '100 No
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("CAMPED_BTN").caption = Ador(0).Value '101 Campos de pedido
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS_BTN").caption = Ador(0).Value '102 Costes/Descuentos
        Ador.MoveNext
        sdbgCostesCategoria.Columns("OPERACION").caption = Ador(0).Value '103 Operacion
        sdbgDescuentosCategoria.Columns("OPERACION").caption = Ador(0).Value '103 Operacion
        Ador.MoveNext
        sdbgCostesCategoria.Columns("DESCR_BTN").caption = Ador(0).Value '104 Descr
        sdbgDescuentosCategoria.Columns("DESCR_BTN").caption = Ador(0).Value '104 Descr
        Ador.MoveNext
        sdbgCostesCategoria.Columns("GRUPO").caption = Ador(0).Value '105 Grupo Comp
        sdbgDescuentosCategoria.Columns("GRUPO").caption = Ador(0).Value '105 Grupo Comp
        Ador.MoveNext
        sdbgCostesCategoria.Columns("TIPO").caption = Ador(0).Value '106 Tipo
        sdbgDescuentosCategoria.Columns("TIPO").caption = Ador(0).Value '106 Tipo
        Ador.MoveNext
        m_sFijo = Ador(0).Value  '107 Fijo
        Ador.MoveNext
        m_sObligatorioExcluyente = Ador(0).Value '108 obligatorio excluyente
        Ador.MoveNext
        m_sOpcionalExcluyente = Ador(0).Value   '109 opcional excluyente
        Ador.MoveNext
        m_sOpcionalSimple = Ador(0).Value  '110 opcional simple
        Ador.MoveNext
        m_sNoCosteSeleccionado = Ador(0).Value '111
        Ador.MoveNext
        m_sNoDescuentoSeleccionado = Ador(0).Value '112
        Ador.MoveNext
        m_sOperacionNoValida = Ador(0).Value '113
        Ador.MoveNext
        m_sTipoNoValido = Ador(0).Value '114
        Ador.MoveNext
        m_sIntroduceGrupoCoste = Ador(0).Value '115
        Ador.MoveNext
        m_sIntroduceGrupoDescuento = Ador(0).Value '116
        Ador.MoveNext
        sPorcenDesvio = Ador(0).Value '117 Porcen desvio
        sdbgAdjudicaciones.Columns("PORCEN_DESVIO").caption = sPorcenDesvio
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("COND_SUMINISTRO").caption = Ador(0).Value '118 Cond.Sum
        Ador.MoveNext
        sdbgAdjudicaciones.Columns("IMPUESTOS_BTN").caption = Ador(0).Value '119 Impuestos
        Me.sdbgCostesCategoria.Columns("IMPUESTOS_BTN").caption = Ador(0).Value '119 Impuestos
        Ador.MoveNext
        m_sEliminarCoste = Ador(0).Value
        Ador.MoveNext
        m_sEliminarDescuento = Ador(0).Value
        Ador.MoveNext
        tabDatos.TabCaption(2) = Ador(0).Value
        sdbgAdjudicaciones.Columns("CAMRECEP_BTN").caption = Ador(0).Value '101 Campos de Recepci�n
        Ador.MoveNext
        cmdExcel(0).caption = Ador(0).Value
        cmdExcel(2).caption = Ador(0).Value
        Ador.MoveNext
        cmdExcel(1).caption = Ador(0).Value
        cmdExcel(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgCamposPedido.Columns("VERENRECEP").caption = Ador(0).Value 'Mostrar en Recepci�n
        Ador.MoveNext
        cmdExcel(4).caption = Ador(0).Value
        Ador.MoveNext
        cmdExcel(5).caption = Ador(0).Value
        Ador.Close
    End If

    Set Ador = Nothing
    
End Sub
Private Sub ConfigurarAdjudicacionesFSEPConf()
    
    If bModifAdj Then
        cmdAnyaAdjudicacion.Visible = False
        cmdAnyaArticulo.Left = cmdAnyaAdjudicacion.Left
        cmdEliAdj.Left = cmdAnyaArticulo.Left + cmdAnyaArticulo.Width + 45
        PosicionarBotones
'        If bModifAdjPrecios And bModifAdjCat Then
'            cmdCambiarCat.Left = cmdEliAdj.Left + cmdEliAdj.Width + 45
'            cmdModifPrecios.Left = cmdCambiarCat.Left + cmdCambiarCat.Width + 45
'            If bPrecioExcel Then
'                cmdExcel(0).Left = cmdModifPrecios.Left + cmdModifPrecios.Width + 45
'                cmdExcel(1).Left = cmdExcel(0).Left + cmdExcel(0).Width + 45
'                If bArticulosExcel Then
'                    cmdExcel(4).Left = cmdExcel(0).Left + cmdExcel(0).Width + 45
'                    cmdExcel(5).Left = cmdExcel(1).Left + cmdExcel(1).Width + 45
'                    cmdBuscarAdj.Left = cmdExcel(5).Left + cmdExcel(5).Width + 45
'                Else
'                    cmdBuscarAdj.Left = cmdExcel(1).Left + cmdExcel(1).Width + 45
'                End If
'            Else
'                If bArticulosExcel Then
'                    cmdExcel(4).Left = cmdModifPrecios.Left + cmdModifPrecios.Width + 45
'                    cmdExcel(5).Left = cmdExcel(4).Left + cmdExcel(4).Width + 45
'                    cmdBuscarAdj.Left = cmdExcel(5).Left + cmdExcel(5).Width + 45
'                Else
'                    cmdBuscarAdj.Left = cmdModifPrecios.Left + cmdModifPrecios.Width + 45
'                End If
'            End If
'            cmdDeshacer.Left = cmdBuscarAdj.Left
'        Else
'            If bModifAdjCat Then
'                cmdCambiarCat.Left = cmdEliAdj.Left + cmdEliAdj.Width + 45
'                cmdBuscarAdj.Left = cmdCambiarCat.Left + cmdCambiarCat.Width + 45
'                cmdDeshacer.Left = cmdBuscarAdj.Left
'            End If
'            If bModifAdjPrecios Then
'                cmdModifPrecios.Left = cmdEliAdj.Left + cmdEliAdj.Width + 45
'                If bPrecioExcel Then
'                    cmdExcel(0).Left = cmdModifPrecios.Left + cmdModifPrecios.Width + 45
'                    cmdExcel(1).Left = cmdExcel(0).Left + cmdExcel(0).Width + 45
'                    If bArticulosExcel Then
'                        cmdExcel(4).Left = cmdExcel(0).Left + cmdExcel(0).Width + 45
'                        cmdExcel(5).Left = cmdExcel(1).Left + cmdExcel(1).Width + 45
'                        cmdBuscarAdj.Left = cmdExcel(5).Left + cmdExcel(5).Width + 45
'                    Else
'                        cmdBuscarAdj.Left = cmdExcel(1).Left + cmdExcel(1).Width + 45
'                    End If
'                Else
'                    If bArticulosExcel Then
'                        cmdExcel(4).Left = cmdModifPrecios.Left + cmdModifPrecios.Width + 45
'                        cmdExcel(5).Left = cmdExcel(4).Left + cmdExcel(4).Width + 45
'                        cmdBuscarAdj.Left = cmdExcel(5).Left + cmdExcel(5).Width + 45
'                    Else
'                        cmdBuscarAdj.Left = cmdModifPrecios.Left + cmdModifPrecios.Width + 45
'                    End If
'                End If
'                cmdDeshacer.Left = cmdBuscarAdj.Left
'            End If
'            If Not bModifAdjPrecios And Not bModifAdjCat Then
'                cmdBuscarAdj.Left = cmdEliAdj.Left + cmdEliAdj.Width + 45
'                cmdDeshacer.Left = cmdBuscarAdj.Left
'            End If
'        End If
    End If
    
    sdbgAdjudicaciones.Columns("PROCE").Visible = False
    sdbgAdjudicaciones.Columns("CANTADJ").Visible = False

End Sub


Public Sub ConfigurarInterfazSeguridad(ByVal nodx As MSComctlLib.node)
Dim i As Integer
''' INTEGRACION PEDIDOS APROVISIONAMIENTO
''' Cuando est� activada la integraci�n de este tipo de pedidos en sentido entrada/salida
''' Se puede marcar una categoria del cat�logo como categoria por defecto para la integraci�n
''' Esta opci�n solo est� disponible para el administrador. Solo se puede marcar una categor�a
''' con seguridad. Si tiene la marca, no se puede ni eliminar, ni dar de baja l�gica, ni tampoco eliminar su seguridad
''' Cuando se marca un rama por defecto para la integraci�n, supone eliminar la que tuviera esta marca.

    If Not bModifCat And Not bModifBajasLog And Not bConsultSegurCat Then Exit Sub
    
    ''' INTEGRACION
    MDI.mnuPopUpCatCatalog(12).Visible = False     '-------------
    MDI.mnuPopUpCatCatalog(13).Visible = False     'Marcar categoria integracion
    If nodx Is Nothing Then Set nodx = tvwCategorias.Nodes.Item("Raiz")
    'Restauramos los valores iniciales
    For i = 1 To 6
        MDI.mnuPopUpCatCatalog(i).Visible = True
        MDI.mnuPopUpCatCatalog(i).Enabled = True
    Next
    
    Select Case Left(nodx.Tag, 4)

    Case "Raiz"
            
            MDI.mnuPopUpCatCatalog(1).Visible = False  'Modificar
            MDI.mnuPopUpCatCatalog(2).Visible = False  'Eliminar
            MDI.mnuPopUpCatCatalog(4).Visible = False  'Crear subcategor�a

            MDI.mnuPopUpCatCatalog(5).Visible = False  'Baja l�gica
            MDI.mnuPopUpCatCatalog(6).Visible = False  'Deshacer baja l�gica

            MDI.mnuPopUpCatCatalog(7).Visible = False  '-------------
            MDI.mnuPopUpCatCatalog(8).Visible = False  'Configurar seguridad
            MDI.mnuPopUpCatCatalog(9).Visible = False  'Copiar seguridad
            MDI.mnuPopUpCatCatalog(10).Visible = False 'Eliminar seguridad
            MDI.mnuPopUpCatCatalog(11).Visible = False 'Consultar seguridad
            MDI.mnuPopUpCatCatalog(14).Visible = True 'Configurar
            If bModifCat Then
                cmdAnyaCat(0).Enabled = True
            End If
            
            cmdModifCat.Enabled = False
            cmdEliCat(0).Enabled = False
            cmdBajaLogica.Enabled = False
            
    Case "CAT1", "CAT2", "CAT3", "CAT4"
                
            If Not bModifCat And Not bModifBajasLog Then
                If bConsultSegurCat And Not bModifSegurCat Then
                    If nodx.Image <> "SEGURO" And nodx.Image <> "BAJASEG" And nodx.Image <> "CAT_INT" Then
                        Exit Sub
                    End If
                End If
            End If
            
            If bConsultSegurCat Then
                MDI.mnuPopUpCatCatalog(7).Visible = True
                If Not bModifSegurCat Then
                    MDI.mnuPopUpCatCatalog(8).Visible = False 'Configurar seguridad
                    MDI.mnuPopUpCatCatalog(9).Visible = False 'Copiar seguridad
                    MDI.mnuPopUpCatCatalog(10).Visible = False 'Eliminar seguridad
                    If nodx.Image = "SEGURO" Or nodx.Image = "BAJASEG" Then 'Consultar seguridad
                        MDI.mnuPopUpCatCatalog(11).Visible = True 'Consultar seguridad
                    Else
                        MDI.mnuPopUpCatCatalog(11).Visible = False 'Consultar seguridad
                    End If
                Else 'Modificacion
                    If nodx.Image = "BAJALOG2" Or nodx.Image = "BAJALOG1" Or nodx.Image = "BAJASEG" Then
                        MDI.mnuPopUpCatCatalog(8).Visible = False 'Configurar seguridad
                    Else
                        MDI.mnuPopUpCatCatalog(8).Visible = True  'Configurar seguridad
                    End If
                    If nodx.Image = "SEGURO" Or nodx.Image = "CAT_INT" Then
                        MDI.mnuPopUpCatCatalog(9).Visible = True  'Copiar seguridad
                        If nodx.Image = "SEGURO" Then
                            MDI.mnuPopUpCatCatalog(10).Visible = True 'Eliminar seguridad
                        Else
                            MDI.mnuPopUpCatCatalog(10).Visible = False 'Eliminar seguridad
                        End If
                        If nodx.Image = "SEGURO" And basParametros.gParametrosIntegracion.gbImportarPedAprov And oUsuarioSummit.Tipo = Administrador Then
                        ''' Solo los hacemos visibles cuando se tiene permiso de modificar seguridad y hay integracion
                            MDI.mnuPopUpCatCatalog(12).Visible = True  '-------------
                            MDI.mnuPopUpCatCatalog(13).Visible = True 'Marcar categoria integracion
                        End If
                    Else
                        MDI.mnuPopUpCatCatalog(9).Visible = False  'Copiar seguridad
                        MDI.mnuPopUpCatCatalog(10).Visible = False 'Eliminar seguridad
                    End If
                    If nodx.Image = "BAJASEG" Then
                        MDI.mnuPopUpCatCatalog(11).Visible = True 'Consultar seguridad
                    Else
                        MDI.mnuPopUpCatCatalog(11).Visible = False 'Consultar seguridad
                    End If
                End If
            End If
            
            If bModifCat Then
                cmdModifCat.Enabled = True
                cmdAnyaCat(0).Enabled = True
                If nodx.Children > 0 Then
                    cmdEliCat(0).Enabled = False
                    MDI.mnuPopUpCatCatalog(2).Enabled = False 'Eliminar
                Else
                    cmdEliCat(0).Enabled = True
                End If
                If nodx.Image = "BAJALOG2" Or nodx.Image = "BAJALOG1" Or nodx.Image = "BAJASEG" Then
                    MDI.mnuPopUpCatCatalog(1).Visible = False 'Modificar
                    MDI.mnuPopUpCatCatalog(4).Visible = False 'Crear subcategor�a
                End If
            Else
                cmdModifCat.Enabled = False
                cmdEliCat(0).Enabled = False
                cmdAnyaCat(0).Enabled = False
                MDI.mnuPopUpCatCatalog(1).Visible = False 'Modificar
                MDI.mnuPopUpCatCatalog(2).Visible = False 'Eliminar
                MDI.mnuPopUpCatCatalog(4).Visible = False 'Crear subcategor�a
            End If
            
            MDI.mnuPopUpCatCatalog(3).Visible = False 'Crear categor�a
            MDI.mnuPopUpCatCatalog(14).Visible = False 'Configurar
            
            cmdBajaLogica.Enabled = True
            
            If bModifBajasLog Then
                If nodx.Image = "BAJALOG2" Or nodx.Image = "BAJASEG" Or nodx.Image = "BAJALOG1" Then
                    MDI.mnuPopUpCatCatalog(5).Visible = False 'Baja l�gica
                    MDI.mnuPopUpCatCatalog(6).Visible = True  'Deshacer baja l�gica
                    cmdBajaLogica.Enabled = False
                Else
                    MDI.mnuPopUpCatCatalog(5).Visible = True  'Baja l�gica
                    MDI.mnuPopUpCatCatalog(6).Visible = False 'Deshacer baja l�gica
                    cmdBajaLogica.Enabled = True
                End If
            Else
                MDI.mnuPopUpCatCatalog(5).Visible = False     'Baja l�gica
                MDI.mnuPopUpCatCatalog(6).Visible = False     'Deshacer baja l�gica
                cmdBajaLogica.Enabled = False
            End If
            
            
    Case "CAT5"
            
        If Not bModifCat And Not bModifBajasLog Then
            If bConsultSegurCat And Not bModifSegurCat Then
                If nodx.Image <> "SEGURO" And nodx.Image <> "BAJASEG" Then
                    Exit Sub
                End If
            End If
        End If

        cmdAnyaCat(0).Enabled = False
        MDI.mnuPopUpCatCatalog(3).Visible = False 'Crear categor�a
        MDI.mnuPopUpCatCatalog(14).Visible = False 'Configurar
        If bModifCat Then
            cmdModifCat.Enabled = True
            cmdEliCat(0).Enabled = True
            MDI.mnuPopUpCatCatalog(4).Visible = True  'Crear subcategor�a
            MDI.mnuPopUpCatCatalog(4).Enabled = False 'Crear subcategor�a
            If nodx.Image = "BAJALOG2" Or nodx.Image = "BAJALOG1" Or nodx.Image = "BAJASEG" Then
                MDI.mnuPopUpCatCatalog(1).Visible = False 'Modificar
                MDI.mnuPopUpCatCatalog(4).Visible = False 'Crear subcategor�a
            End If
        Else
            cmdModifCat.Enabled = False
            cmdEliCat(0).Enabled = False
            MDI.mnuPopUpCatCatalog(1).Visible = False  'Modificar
            MDI.mnuPopUpCatCatalog(2).Visible = False  'Eliminar
            MDI.mnuPopUpCatCatalog(4).Visible = False  'Crear subcategor�a
        End If
    
        cmdBajaLogica.Enabled = True
        
        If bModifBajasLog Then
            If nodx.Image = "BAJALOG2" Or nodx.Image = "BAJALOG1" Or nodx.Image = "BAJASEG" Then
                MDI.mnuPopUpCatCatalog(5).Visible = False 'Baja l�gica
                MDI.mnuPopUpCatCatalog(6).Visible = True  'Deshacer baja l�gica
                cmdBajaLogica.Enabled = False
            Else
                MDI.mnuPopUpCatCatalog(5).Visible = True  'Baja l�gica
                MDI.mnuPopUpCatCatalog(6).Visible = False 'Deshacer baja l�gica
                cmdBajaLogica.Enabled = True
            End If
        Else
            MDI.mnuPopUpCatCatalog(5).Visible = False  'Baja l�gica
            MDI.mnuPopUpCatCatalog(6).Visible = False  'Deshacer baja l�gica
            cmdBajaLogica.Enabled = False
        End If
        
        If bConsultSegurCat Then
            MDI.mnuPopUpCatCatalog(7).Visible = True  '-----------
            If Not bModifSegurCat Then
                MDI.mnuPopUpCatCatalog(8).Visible = False  'Configurar seguridad
                MDI.mnuPopUpCatCatalog(9).Visible = False  'Copiar seguridad
                MDI.mnuPopUpCatCatalog(10).Visible = False 'Eliminar seguridad
                If nodx.Image = "SEGURO" Or nodx.Image = "BAJASEG" Then 'Consultar seguridad
                    MDI.mnuPopUpCatCatalog(11).Visible = True
                Else
                    MDI.mnuPopUpCatCatalog(11).Visible = False
                End If
            Else 'Modificacion
                If nodx.Image = "BAJALOG2" Or nodx.Image = "BAJALOG1" Or nodx.Image = "BAJASEG" Then
                    MDI.mnuPopUpCatCatalog(8).Visible = False  'Configurar seguridad
                Else
                    MDI.mnuPopUpCatCatalog(8).Visible = True   'Configurar seguridad
                End If
                If nodx.Image = "SEGURO" Or nodx.Image = "CAT_INT" Then
                    MDI.mnuPopUpCatCatalog(9).Visible = True   'Copiar seguridad
                    If nodx.Image = "SEGURO" Then
                        MDI.mnuPopUpCatCatalog(10).Visible = True  'Eliminar seguridad
                    Else
                        MDI.mnuPopUpCatCatalog(10).Visible = False  'Eliminar seguridad
                    End If
                    ''' Solo los hacemos visibles cuando se tiene permiso de modificar seguridad y hay integracion
                    If nodx.Image = "SEGURO" And basParametros.gParametrosIntegracion.gbImportarPedAprov And oUsuarioSummit.Tipo = Administrador Then
                        MDI.mnuPopUpCatCatalog(12).Visible = True  '-------------
                        MDI.mnuPopUpCatCatalog(13).Visible = True 'Marcar categoria integracion
                    End If
                Else
                    MDI.mnuPopUpCatCatalog(9).Visible = False  'Copiar seguridad
                    MDI.mnuPopUpCatCatalog(10).Visible = False 'Eliminar seguridad
                End If
                If nodx.Image = "BAJASEG" Then 'Consultar seguridad
                    MDI.mnuPopUpCatCatalog(11).Visible = True
                Else
                    MDI.mnuPopUpCatCatalog(11).Visible = False  'Consultar seguridad
                End If

            End If
        End If
                
    End Select
    
    If ((Not MDI.mnuPopUpCatCatalog(1).Visible And Not MDI.mnuPopUpCatCatalog(2).Visible And Not MDI.mnuPopUpCatCatalog(3).Visible And Not MDI.mnuPopUpCatCatalog(4).Visible And Not MDI.mnuPopUpCatCatalog(5).Visible And Not MDI.mnuPopUpCatCatalog(6).Visible) Or (Not MDI.mnuPopUpCatCatalog(8).Visible And Not MDI.mnuPopUpCatCatalog(9).Visible And Not MDI.mnuPopUpCatCatalog(10).Visible And Not MDI.mnuPopUpCatCatalog(11).Visible)) Then
        MDI.mnuPopUpCatCatalog(7).Visible = False '------------
    End If

End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub ConfigurarSeguridad()
Dim i As Integer
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing) Then
        bRConProceAper = True
    End If
    
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGModificar)) Is Nothing Then
        bModifCat = True
    End If

    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGBajaLogCon)) Is Nothing Then
        bConsultBajasLog = True
    End If

    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGBajaLogMod)) Is Nothing Then
        bModifBajasLog = True
    End If

    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSegurConsultar)) Is Nothing Then
        bConsultSegurCat = True
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSegurModificar)) Is Nothing Then
            bModifSegurCat = True
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAccesoFSEP)) Is Nothing Then
                bModifAccFSEPSegurCat = True
            Else
                bModifAccFSEPSegurCat = False
            End If
        Else
            bModifSegurCat = False
        End If
        
        If basOptimizacion.gTipoDeUsuario = Persona Or basOptimizacion.gTipoDeUsuario = comprador Then
            basOptimizacion.gCodPersonaUsuario = oUsuarioSummit.Persona.Cod
            basOptimizacion.gCodDepUsuario = oUsuarioSummit.Persona.CodDep
            basOptimizacion.gUON1Usuario = oUsuarioSummit.Persona.UON1
            basOptimizacion.gUON2Usuario = oUsuarioSummit.Persona.UON2
            basOptimizacion.gUON3Usuario = oUsuarioSummit.Persona.UON3
        End If
        
        bRUOSegurCat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSegurRestUO)) Is Nothing)
        bRPerfUOSegurCat = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSegurRestPerfUO)) Is Nothing)
            
        If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSegurRestDep)) Is Nothing Then
            bRDepSegurCat = True
        End If
    End If

    If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAdjModificar)) Is Nothing) Then
        bModifAdj = True
    End If
    
    If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAdjPrecioModificar)) Is Nothing) Then
        bModifAdjPrecios = True
    End If
    If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGPrecioExcel)) Is Nothing) Then
        bPrecioExcel = True
    End If
    If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGArticulosExcel)) Is Nothing) Then
        bArticulosExcel = True
    End If
    If (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAdjCategoriaCambiar)) Is Nothing) Then
        bModifAdjCat = True
    End If

    If (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuAprov)) Is Nothing)) Then
        bRUsuAprov = True
    End If

    If basOptimizacion.gTipoDeUsuario = comprador And (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestMatComp)) Is Nothing)) Then
        bRMat = True
    End If

    If (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestDest)) Is Nothing)) Then
        bRDest = True
    End If
    
    'Restricciones para las solicitudes de compras
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicRestEquipo)) Is Nothing) Then
        If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            'S�lo le afectar� la restricci�n si el usuario es un comprador.
            m_bRSolicEquipo = True
        End If
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicRestAsig)) Is Nothing) Then
        m_bRSolicAsig = True
    End If
    
    m_bRSolicUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicRestUO)) Is Nothing)
    m_bRSolicPerfUO = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicRestPerfUO)) Is Nothing)
                    
    'Categor�as
    If Not bModifCat Then
        cmdAnyaCat(0).Visible = False
        cmdModifCat.Visible = False
        cmdEliCat(0).Visible = False
        cmdAnyaCat(1).Visible = False
        txtObsAdjun.Locked = True
        cmdEliCat(1).Visible = False
        cmdA�adirAdjun.Visible = False
        cmdEliminarAdjun.Visible = False
        cmdModificarAdjun.Visible = False
        picModEmision.Enabled = False
        'cmdModifCampos.Visible = False
        If Not bModifBajasLog Then
            cmdBajaLogica.Visible = False
            cmdResCat.Left = cmdAnyaCat(0).Left
            cmdBuscarCat.Left = cmdModifCat.Left
        Else
            cmdBajaLogica.Left = cmdAnyaCat(0).Left
            cmdResCat.Left = cmdModifCat.Left
            cmdBuscarCat.Left = cmdEliCat(0).Left
        End If
    Else
        picModEmision.Enabled = True
        If Not bModifBajasLog Then
            cmdBajaLogica.Visible = False
            cmdBuscarCat.Left = cmdResCat.Left
            cmdResCat.Left = cmdBajaLogica.Left
        End If
    End If
    
    If Not bConsultBajasLog Then
        chkVerBajas.Visible = False
    End If
    cmdListado.Left = cmdBuscarCat.Left + cmdBuscarCat.Width + 45
    
    'Adjudicaciones
    If FSEPConf Then
        cmdAnyaAdjudicacion.Visible = False
    End If
    
    If Not bModifAdj Then
        cmdAnyaAdjudicacion.Visible = False
        cmdAnyaArticulo.Visible = False
        cmdEliAdj.Visible = False
    End If
    If Not bModifAdjCat Then
        cmdCambiarCat.Visible = False
    End If
    If Not bModifAdjPrecios Then
        cmdModifPrecios.Visible = False
        If bModifAdj Then
            cmdAnyaArticulo.Visible = False
        Else
            cmdDeshacer.Visible = False
            cmdModoEdicion.Visible = False
        End If
    End If
    If Not bPrecioExcel Then
        For i = 0 To 3
            cmdExcel(i).Visible = False
        Next i
    End If
    If Not bArticulosExcel Then
        cmdExcel(4).Visible = False
        cmdExcel(5).Visible = False
    End If
    PosicionarBotones
    If FSEPConf Then
        sdbgAdjudicaciones.Columns("SOLICIT").Visible = False
    Else
        If Not basParametros.gParametrosGenerales.gbSolicitudesCompras Then
            sdbgAdjudicaciones.Columns("SOLICIT").Visible = False  'si no hay solicitudes de compras no se ve el campo solicitud
        Else
            'Si no es el usuario administrador
            If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
                'Si no tiene permisos de consulta de las solicitudes no podr� visualizarlas
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGSolicAsignar)) Is Nothing) Then
                    sdbgAdjudicaciones.Columns("SOLICIT").Visible = False
                Else
                    sdbgAdjudicaciones.Columns("SOLICIT").Visible = True
                End If
            Else
                sdbgAdjudicaciones.Columns("SOLICIT").Visible = True
            End If
        End If
    End If
End Sub


Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

    If node Is Nothing Then Exit Function

    'posici�n del primer -%$
    iPosicion = InStr(6, node.Tag, "-%$")
    DevolverCod = Mid(node.Tag, 6, iPosicion - 6)

End Function

Public Function DevolverDen(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion As Integer

    If node Is Nothing Then Exit Function

    'posici�n del gui�n
    iPosicion = InStr(1, node.Text, "-")
    DevolverDen = Mid(node.Text, iPosicion + 2, Len(node.Text) - iPosicion)

End Function

Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant
Dim iPosicion1 As Integer
Dim iPosicion2 As Integer

    If node Is Nothing Then Exit Function
    
    'posici�n del primer -%$,justo �ntes del ID
    iPosicion1 = InStr(6, node.Tag, "-%$")
    iPosicion1 = iPosicion1 + 3
    'posici�n del segundo -%$
    iPosicion2 = InStr(iPosicion1, node.Tag, "-%$")
    If iPosicion2 = 0 Then
        DevolverId = val(Right(node.Tag, Len(node.Tag) - (iPosicion1 - 1)))
    Else
        DevolverId = val(Mid(node.Tag, iPosicion1, iPosicion2 - iPosicion1))
    End If
    
End Function


Private Function EliminarCATDeEstructura()
Dim nod As MSComctlLib.node
Dim nodSiguiente As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Set nod = tvwCategorias.selectedItem

Select Case Left(nod.Tag, 4)
    
    Case "CAT1"
            
            scod1 = DevolverCod(nod)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oCategoriasNivel1.Remove (scod1)
    
    Case "CAT2"
            
            scod1 = DevolverCod(nod.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nod)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oCategoriasNivel1.Item(scod1).CategoriasN2.Remove (scod1 & scod2)
            
    Case "CAT3"
            
            scod1 = DevolverCod(nod.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nod.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = DevolverCod(nod)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Remove (scod1 & scod2 & scod3)
            
    Case "CAT4"
            
            scod1 = DevolverCod(nod.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nod.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = DevolverCod(nod.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            scod4 = DevolverCod(nod)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Remove (scod1 & scod2 & scod3 & scod4)
            
    Case "CAT5"
            
            scod1 = DevolverCod(nod.Parent.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nod.Parent.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = DevolverCod(nod.Parent.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            scod4 = DevolverCod(nod.Parent)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
            scod5 = DevolverCod(nod)
            scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Remove (scod1 & scod2 & scod3 & scod4 & scod5)

End Select

    
    ' Me posiciono en el siguiente
    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If

    
    'Lo elimino del arbol
    tvwCategorias.Nodes.Remove (tvwCategorias.selectedItem.Index)
    Set tvwCategorias.selectedItem = nodSiguiente
    tvwCategorias_NodeClick nodSiguiente

    nodSiguiente.Selected = True
    If Me.Visible Then tvwCategorias.SetFocus

End Function



Private Sub GenerarEstructuraCategorias(ByVal VerBajas As Boolean, Optional ByVal ExpandirSeg As Long = 0)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim oCATN1 As CCategoriaN1
Dim oCatN2 As CCategoriaN2
Dim oCatN3 As CCategoriaN3
Dim oCatN4 As CCategoriaN4
Dim oCatN5 As CCategoriaN5

Dim nodx As MSComctlLib.node

tvwCategorias.Nodes.clear
Set nodx = tvwCategorias.Nodes.Add(, , "Raiz", sIdiCategorias, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
    
    
    'Todas las categorias
    Set oCategoriasNivel1 = oFSGSRaiz.Generar_CCategoriasN1
    oCategoriasNivel1.GenerarEstructuraCategorias VerBajas, False
    
    For Each oCATN1 In oCategoriasNivel1
        scod1 = oCATN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(oCATN1.Cod))
        Set nodx = tvwCategorias.Nodes.Add("Raiz", tvwChild, "CAT1" & scod1, oCATN1.Cod & " - " & oCATN1.Den, "CAT")
        nodx.Tag = "CAT1-" & oCATN1.Cod & "-%$" & oCATN1.Id
        If VerBajas Then
            If oCATN1.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
        End If
        If oCATN1.Seguridad <> 0 Then
            If oCATN1.BajaLogica = 1 Then
                nodx.Image = "BAJASEG"
            Else
                If oCATN1.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                    nodx.Image = "CAT_INT"
                Else
                    nodx.Image = "SEGURO"
                End If
            End If
            nodx.Tag = nodx.Tag & "-%$SEG" & oCATN1.Seguridad
        Else
            If oCATN1.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
        End If
        'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
        If Not g_oNodx Is Nothing Then
            If nodx.Tag = g_oNodx.Tag Then
                nodx.Selected = True
                tvwCategorias_NodeClick nodx
            End If
        End If
            
        For Each oCatN2 In oCATN1.CategoriasN2
            scod2 = oCatN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(oCatN2.Cod))
            Set nodx = tvwCategorias.Nodes.Add("CAT1" & scod1, tvwChild, "CAT2" & scod1 & scod2, oCatN2.Cod & " - " & oCatN2.Den, "CAT")
            nodx.Tag = "CAT2-" & oCatN2.Cod & "-%$" & oCatN2.Id
            If oCatN2.Seguridad <> 0 Then
                If oCatN2.BajaLogica = 1 Then
                    nodx.Image = "BAJASEG"
                Else
                    If oCatN2.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                        nodx.Image = "CAT_INT"
                    Else
                        nodx.Image = "SEGURO"
                    End If
                End If
                nodx.Tag = nodx.Tag & "-%$SEG" & oCatN2.Seguridad
                If ExpandirSeg = oCatN2.Seguridad Then
                    nodx.Parent.Expanded = True
                End If
            Else
                If oCatN2.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
            End If
            If VerBajas Then
                If oCatN2.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
            End If
            'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
            If Not g_oNodx Is Nothing Then
                If nodx.Tag = g_oNodx.Tag Then
                    nodx.Selected = True
                    tvwCategorias_NodeClick nodx
                End If
            End If
            
            For Each oCatN3 In oCatN2.CategoriasN3
                scod3 = oCatN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(oCatN3.Cod))
                Set nodx = tvwCategorias.Nodes.Add("CAT2" & scod1 & scod2, tvwChild, "CAT3" & scod1 & scod2 & scod3, oCatN3.Cod & " - " & oCatN3.Den, "CAT")
                nodx.Tag = "CAT3-" & oCatN3.Cod & "-%$" & oCatN3.Id
                If oCatN3.Seguridad <> 0 Then
                    If oCatN3.BajaLogica = 1 Then
                        nodx.Image = "BAJASEG"
                    Else
                        If oCatN3.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                            nodx.Image = "CAT_INT"
                        Else
                            nodx.Image = "SEGURO"
                        End If
                    End If
                    nodx.Tag = nodx.Tag & "-%$SEG" & oCatN3.Seguridad
                    If ExpandirSeg = oCatN3.Seguridad Then
                        nodx.Parent.Parent.Expanded = True
                        nodx.Parent.Expanded = True
                    End If
                Else
                    If oCatN3.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
                End If
                If VerBajas Then
                    If oCatN3.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
                End If
                'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
                If Not g_oNodx Is Nothing Then
                    If nodx.Tag = g_oNodx.Tag Then
                        nodx.Selected = True
                        tvwCategorias_NodeClick nodx
                    End If
                End If
                
                For Each oCatN4 In oCatN3.CategoriasN4
                    scod4 = oCatN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(oCatN4.Cod))
                    Set nodx = tvwCategorias.Nodes.Add("CAT3" & scod1 & scod2 & scod3, tvwChild, "CAT4" & scod1 & scod2 & scod3 & scod4, oCatN4.Cod & " - " & oCatN4.Den, "CAT")
                    nodx.Tag = "CAT4-" & oCatN4.Cod & "-%$" & oCatN4.Id
                    If oCatN4.Seguridad <> 0 Then
                        If oCatN4.BajaLogica = 1 Then
                            nodx.Image = "BAJASEG"
                        Else
                            If oCatN4.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                                nodx.Image = "CAT_INT"
                            Else
                                nodx.Image = "SEGURO"
                            End If
                        End If
                        nodx.Tag = nodx.Tag & "-%$SEG" & oCatN4.Seguridad
                        If ExpandirSeg = oCatN4.Seguridad Then
                            nodx.Parent.Parent.Parent.Expanded = True
                            nodx.Parent.Parent.Expanded = True
                            nodx.Parent.Expanded = True
                        End If
                    Else
                        If oCatN4.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
                    End If
                    If VerBajas Then
                        If oCatN4.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
                    End If
                    'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
                    If Not g_oNodx Is Nothing Then
                        If nodx.Tag = g_oNodx.Tag Then
                            nodx.Selected = True
                            tvwCategorias_NodeClick nodx
                        End If
                    End If
                    
                    For Each oCatN5 In oCatN4.CategoriasN5
                        scod5 = oCatN5.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(oCatN5.Cod))
                        Set nodx = tvwCategorias.Nodes.Add("CAT4" & scod1 & scod2 & scod3 & scod4, tvwChild, "CAT5" & scod1 & scod2 & scod3 & scod4 & scod5, oCatN5.Cod & " - " & oCatN5.Den, "CAT")
                        nodx.Tag = "CAT5-" & oCatN5.Cod & "-%$" & oCatN5.Id
                        If oCatN5.Seguridad <> 0 Then
                            If oCatN5.BajaLogica = 1 Then
                                nodx.Image = "BAJASEG"
                            Else
                                If oCatN5.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                                    nodx.Image = "CAT_INT"
                                Else
                                    nodx.Image = "SEGURO"
                                End If
                            End If
                            nodx.Tag = nodx.Tag & "-%$SEG" & oCatN5.Seguridad
                            If ExpandirSeg = oCatN5.Seguridad Then
                                nodx.Parent.Parent.Parent.Parent.Expanded = True
                                nodx.Parent.Parent.Parent.Expanded = True
                                nodx.Parent.Parent.Expanded = True
                                nodx.Parent.Expanded = True
                            End If
                        Else
                            If oCatN5.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
                        End If
                        If VerBajas Then
                            If oCatN5.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
                        End If
                        'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
                        If Not g_oNodx Is Nothing Then
                            If nodx.Tag = g_oNodx.Tag Then
                                nodx.Selected = True
                                tvwCategorias_NodeClick nodx
                            End If
                        End If
                    Next
                Next
            Next
        Next
    Next
        
Exit Sub

Error:
    Set nodx = Nothing
    Resume Next

End Sub

Public Sub CargarGridAdjudicaciones(ByVal node As MSComctlLib.node, ByVal iOrden As Integer)
Dim bHom As Boolean
Dim iLinea As Long
Dim oLineaCatalogo As CLineaCatalogo
Dim sPub As String
Dim sDest As String
Dim sEsp As String
Dim str_linea As String
    Screen.MousePointer = vbHourglass

    sdbgAdjudicaciones.RemoveAll
    
    If gParametrosGenerales.gbOblPedidosHom And node.Children = 0 Then
        bHom = True
    Else
        bHom = False
    End If
    
    CargarLineasCatalogo node, iOrden, bHom, oLineaCatalogo
    
    iLinea = 1

    m_bCargandoAdj = True
    While oLineasCatalogo.Count >= iLinea
        Set oLineaCatalogo = oLineasCatalogo.Item(iLinea)
        If oLineaCatalogo.Publicado Then
            sPub = "1"
        Else
            sPub = "0"
        End If
        
        If oLineaCatalogo.DestinoUsuario = True Then
            sDest = "1"
        Else
            sDest = "0"
        End If
        
        If oLineaCatalogo.EspAdj = 1 Or (Not IsNull(oLineaCatalogo.esp) And oLineaCatalogo.EspAdj <> "") Or oLineaCatalogo.EspAtr = 1 Then
            sEsp = "1"
        Else
            sEsp = "0"
        End If
        
        If IsNull(oLineaCatalogo.ProceCod) Then
            str_linea = sPub & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.FechaDespublicacion & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ArtCod_Interno
            str_linea = str_linea & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ArtDen & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ProveCod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Destino & Chr(m_lSeparador)
            str_linea = str_linea & sDest & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.UnidadCompra & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.PrecioUC & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.mon & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.UnidadPedido & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.FactorConversion & Chr(m_lSeparador)
            str_linea = str_linea & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.CantidadMinima & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Id & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Homologado & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Anyo & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN1Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ProceCod & Chr(m_lSeparador)
            str_linea = str_linea & 0 & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ArtCod_Interno & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN2Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN3Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN4Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Item & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.PresupuestoUC & Chr(m_lSeparador)
            str_linea = str_linea & IIf(oLineaCatalogo.HayUnidadesPedido, 1, 0) & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.PorcenDesvio & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Solicitud & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador)
            str_linea = str_linea & sEsp
            str_linea = str_linea & Chr(m_lSeparador) & CStr(oLineaCatalogo.ArtGenerico) & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de campos de pedido
            str_linea = str_linea & CStr(oLineaCatalogo.HayCamposPedido) & Chr(m_lSeparador) 'columna del valor que indica si tiene campos de pedido
            
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de campos de recepcion
            str_linea = str_linea & CStr(oLineaCatalogo.HayCamposRecepcion) & Chr(m_lSeparador) 'columna del valor que indica si tiene campos de recepcion
            
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de costes y descuentos
            str_linea = str_linea & CStr(oLineaCatalogo.HayCostesDescuentos) & Chr(m_lSeparador)   'columna del valor que indica si tiene costes/descuentos
            str_linea = str_linea & oLineaCatalogo.CantidadImporteMaxPed & Chr(m_lSeparador)    'columna con cantidad/importe maximo por pedido
            str_linea = str_linea & oLineaCatalogo.CantidadImporteMaxTotal & Chr(m_lSeparador)      'columna con cantidad/importe maximo por adjudicacion
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para las condiciones de suministro
            str_linea = str_linea & oLineaCatalogo.tipoRecepcion & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton condiciones de suministro
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de impuestos
            str_linea = str_linea & oLineaCatalogo.TieneImpuestos
            sdbgAdjudicaciones.AddItem str_linea
            
        Else
            str_linea = sPub & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.FechaDespublicacion & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ArtCod_Interno
            str_linea = str_linea & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ArtDen & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Anyo & "/" & oLineaCatalogo.GMN1Proce & "/" & oLineaCatalogo.ProceCod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ProveCod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Destino & Chr(m_lSeparador)
            str_linea = str_linea & sDest & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.CantidadAdj & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.UnidadCompra & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.PrecioUC & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.mon & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.UnidadPedido & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.FactorConversion & Chr(m_lSeparador)
            str_linea = str_linea & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.CantidadMinima & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Id & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Homologado & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Anyo & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN1Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ProceCod & Chr(m_lSeparador)
            str_linea = str_linea & 0 & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.ArtCod_Interno & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN2Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN3Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.GMN4Cod & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Item & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.PresupuestoUC
            str_linea = str_linea & Chr(m_lSeparador)
            str_linea = str_linea & IIf(oLineaCatalogo.HayUnidadesPedido, 1, 0) & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.PorcenDesvio & Chr(m_lSeparador)
            str_linea = str_linea & oLineaCatalogo.Solicitud & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador) & sEsp
            str_linea = str_linea & Chr(m_lSeparador) & CStr(oLineaCatalogo.ArtGenerico) & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de campos de pedido
            str_linea = str_linea & CStr(oLineaCatalogo.HayCamposPedido) & Chr(m_lSeparador) 'columna del valor que indica si tiene campos de pedido
            
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de campos de recepcion
            str_linea = str_linea & CStr(oLineaCatalogo.HayCamposRecepcion) & Chr(m_lSeparador) 'columna del valor que indica si tiene campos recepcion
            
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de costes y descuentos
            str_linea = str_linea & CStr(oLineaCatalogo.HayCostesDescuentos) & Chr(m_lSeparador)   'columna del valor que indica si tiene costes/descuentos
            str_linea = str_linea & oLineaCatalogo.CantidadImporteMaxPed & Chr(m_lSeparador)    'columna con cantidad/importe maximo por pedido
            str_linea = str_linea & oLineaCatalogo.CantidadImporteMaxTotal & Chr(m_lSeparador)      'columna con cantidad/importe maximo por adjudicacion
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para las condiciones de suministro
            str_linea = str_linea & oLineaCatalogo.tipoRecepcion & Chr(m_lSeparador)
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton condiciones de suministro
            str_linea = str_linea & "" & Chr(m_lSeparador) 'Columna para el boton de impuestos
            str_linea = str_linea & oLineaCatalogo.TieneImpuestos
            sdbgAdjudicaciones.AddItem str_linea
        End If
        iLinea = iLinea + 1
    Wend
    m_bCargandoAdj = False
    
    Set oLineaCatalogoSeleccionada = oLineaCatalogo
    Screen.MousePointer = vbNormal

    lblLeyenda.Visible = False
    
    Accion = ACCCatAdjudCon
    DoEvents
    
End Sub


Private Sub GenerarEstructuraCategoriasAprovisionamiento(ByVal VerBajas As Boolean, Optional ByVal ExpandirSeg As Long = 0)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim oCATN1 As CCategoriaN1
Dim oCatN2 As CCategoriaN2
Dim oCatN3 As CCategoriaN3
Dim oCatN4 As CCategoriaN4
Dim oCatN5 As CCategoriaN5

Dim nodx As MSComctlLib.node

tvwCategorias.Nodes.clear
Set nodx = tvwCategorias.Nodes.Add(, , "Raiz", sIdiCategorias, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
    
    
    'Todas las categorias
    Set oCategoriasNivel1 = oFSGSRaiz.Generar_CCategoriasN1
    oCategoriasNivel1.GenerarEstructuraCategoriasDeUsuario oUsuarioSummit.Persona.Cod, VerBajas
    
    For Each oCATN1 In oCategoriasNivel1
        scod1 = oCATN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(oCATN1.Cod))
        Set nodx = tvwCategorias.Nodes.Add("Raiz", tvwChild, "CAT1" & scod1, oCATN1.Cod & " - " & oCATN1.Den, "CAT")
        nodx.Tag = "CAT1-" & oCATN1.Cod & "-%$" & oCATN1.Id
        If VerBajas Then
            If oCATN1.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
        End If
        If oCATN1.Seguridad <> 0 Then
            If oCATN1.BajaLogica = 1 Then
                nodx.Image = "BAJASEG"
            Else
                If oCATN1.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                    nodx.Image = "CAT_INT"
                Else
                    nodx.Image = "SEGURO"
                End If
            End If
            nodx.Tag = nodx.Tag & "-%$SEG" & oCATN1.Seguridad
            If ExpandirSeg = oCATN1.Seguridad Then nodx.Expanded = True
        Else
            If oCATN1.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
        End If
        'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
        If Not g_oNodx Is Nothing Then
            If nodx.Tag = g_oNodx.Tag Then
                nodx.Selected = True
                tvwCategorias_NodeClick nodx
            End If
        End If
            
        For Each oCatN2 In oCATN1.CategoriasN2
            scod2 = oCatN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(oCatN2.Cod))
            Set nodx = tvwCategorias.Nodes.Add("CAT1" & scod1, tvwChild, "CAT2" & scod1 & scod2, oCatN2.Cod & " - " & oCatN2.Den, "CAT")
            nodx.Tag = "CAT2-" & oCatN2.Cod & "-%$" & oCatN2.Id
            If oCatN2.Seguridad <> 0 Then
                If oCatN2.BajaLogica = 1 Then
                    nodx.Image = "BAJASEG"
                Else
                    If oCatN2.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                        nodx.Image = "CAT_INT"
                    Else
                        nodx.Image = "SEGURO"
                    End If
                End If
                nodx.Tag = nodx.Tag & "-%$SEG" & oCatN2.Seguridad
                If ExpandirSeg = oCatN2.Seguridad Then
                    nodx.Parent.Expanded = True
                End If
            Else
                If oCatN2.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
            End If
            If VerBajas Then
                If oCatN2.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
            End If
            'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
            If Not g_oNodx Is Nothing Then
                If nodx.Tag = g_oNodx.Tag Then
                    nodx.Selected = True
                    tvwCategorias_NodeClick nodx
                End If
            End If
            
            For Each oCatN3 In oCatN2.CategoriasN3
                scod3 = oCatN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(oCatN3.Cod))
                Set nodx = tvwCategorias.Nodes.Add("CAT2" & scod1 & scod2, tvwChild, "CAT3" & scod1 & scod2 & scod3, oCatN3.Cod & " - " & oCatN3.Den, "CAT")
                nodx.Tag = "CAT3-" & oCatN3.Cod & "-%$" & oCatN3.Id
                If oCatN3.Seguridad <> 0 Then
                    If oCatN3.BajaLogica = 1 Then
                        nodx.Image = "BAJASEG"
                    Else
                        If oCatN3.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                            nodx.Image = "CAT_INT"
                        Else
                            nodx.Image = "SEGURO"
                        End If
                    End If
                    nodx.Tag = nodx.Tag & "-%$SEG" & oCatN3.Seguridad
                    If ExpandirSeg = oCatN3.Seguridad Then nodx.Expanded = True
                Else
                    If oCatN3.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
                End If
                If VerBajas Then
                    If oCatN3.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
                End If
                'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
                If Not g_oNodx Is Nothing Then
                    If nodx.Tag = g_oNodx.Tag Then
                        nodx.Selected = True
                        tvwCategorias_NodeClick nodx
                    End If
                End If
                
                For Each oCatN4 In oCatN3.CategoriasN4
                    scod4 = oCatN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(oCatN4.Cod))
                    Set nodx = tvwCategorias.Nodes.Add("CAT3" & scod1 & scod2 & scod3, tvwChild, "CAT4" & scod1 & scod2 & scod3 & scod4, oCatN4.Cod & " - " & oCatN4.Den, "CAT")
                    nodx.Tag = "CAT4-" & oCatN4.Cod & "-%$" & oCatN4.Id
                    If oCatN4.Seguridad <> 0 Then
                        If oCatN4.BajaLogica = 1 Then
                            nodx.Image = "BAJASEG"
                        Else
                            If oCatN4.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                                nodx.Image = "CAT_INT"
                            Else
                                nodx.Image = "SEGURO"
                            End If
                        End If
                        nodx.Tag = nodx.Tag & "-%$SEG" & oCatN4.Seguridad
                        If ExpandirSeg = oCatN4.Seguridad Then nodx.Expanded = True
                    Else
                        If oCatN4.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
                    End If
                    If VerBajas Then
                        If oCatN4.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
                    End If
                    'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
                    If Not g_oNodx Is Nothing Then
                        If nodx.Tag = g_oNodx.Tag Then
                            nodx.Selected = True
                            tvwCategorias_NodeClick nodx
                        End If
                    End If
                    
                    For Each oCatN5 In oCatN4.CategoriasN5
                        scod5 = oCatN5.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(oCatN5.Cod))
                        Set nodx = tvwCategorias.Nodes.Add("CAT4" & scod1 & scod2 & scod3 & scod4, tvwChild, "CAT5" & scod1 & scod2 & scod3 & scod4 & scod5, oCatN5.Cod & " - " & oCatN5.Den, "CAT")
                        nodx.Tag = "CAT5-" & oCatN5.Cod & "-%$" & oCatN5.Id
                        If oCatN5.Seguridad <> 0 Then
                            If oCatN5.BajaLogica = 1 Then
                                nodx.Image = "BAJASEG"
                            Else
                                If oCatN5.Integracion And basParametros.gParametrosIntegracion.gbImportarPedAprov Then
                                    nodx.Image = "CAT_INT"
                                Else
                                    nodx.Image = "SEGURO"
                                End If
                            End If
                            nodx.Tag = nodx.Tag & "-%$SEG" & oCatN5.Seguridad
                            If ExpandirSeg = oCatN5.Seguridad Then nodx.Expanded = True
                        Else
                            If oCatN5.BajaLogica = 1 Then nodx.Image = "BAJALOG1"
                        End If
                        If VerBajas Then
                            If oCatN5.BajaLogica = 2 Then nodx.Image = "BAJALOG2"
                        End If
                        'Si nos han pasado un nodo desde otro formulario para preseleccionarlo al cargar los nodos, lo chequeamos
                        If Not g_oNodx Is Nothing Then
                            If nodx.Tag = g_oNodx.Tag Then
                                nodx.Selected = True
                                tvwCategorias_NodeClick nodx
                            End If
                        End If
                    Next
                Next
            Next
        Next
    Next
Exit Sub

Error:
    Set nodx = Nothing
    Resume Next

End Sub



Private Sub MostrarDetallePrecioAdj(ByVal IdLinea As Long)
Dim oLineas As CLineasCatalogo
Dim oMoneda As CMoneda
Dim dblMonedaEquivalencia As Double

    If IsEmpty(oLineaCatalogoSeleccionada.Oferta) Or IsNull(oLineaCatalogoSeleccionada.Oferta) Then

        Set oLineas = oFSGSRaiz.Generar_CLineasCatalogo
        oLineas.CargarDatosDePrecios IdLinea
        
        Set oLineaCatalogoSeleccionada = oLineas.Item(CStr(IdLinea))
        If oLineaCatalogoSeleccionada Is Nothing Then
            Exit Sub
        End If

    End If
    
    frmDetallePrecios.txtMonOfe.Text = oLineaCatalogoSeleccionada.CambioOferta & "(" & oLineaCatalogoSeleccionada.MonOferta & ")"
    frmDetallePrecios.txtMonProce.Text = oLineaCatalogoSeleccionada.CambioProce & "(" & oLineaCatalogoSeleccionada.MonProce & ")"
   
    Set oMoneda = oMonedas.Item(sdbgAdjudicaciones.Columns("MON").Value)
    'Modificado 22-3-2006
    'Recuperamos los datos de la moneda q muestra el catalogo
    If Not (oMoneda Is Nothing) Then
        'En oMoneda.Equiv tenemos  la equivalencia respecto a moneda central
        dblMonedaEquivalencia = oMoneda.Equiv
    End If
    Set oMoneda = Nothing
    
    If oLineaCatalogoSeleccionada.PrecioAdj = 0 Then
        frmDetallePrecios.txtOferta.Text = 0
        frmDetallePrecios.txtProce.Text = 0
        frmDetallePrecios.txtCentral.Text = 0
        frmDetallePrecios.txtAdj.Text = 0
        frmDetallePrecios.txtAhorroAdj.Text = 0
    Else
        frmDetallePrecios.txtOferta.Text = Format(oLineaCatalogoSeleccionada.PrecioAdj, "#,##0.0####################")
        frmDetallePrecios.txtProce.Text = Format((oLineaCatalogoSeleccionada.PrecioAdj / oLineaCatalogoSeleccionada.CambioOferta), "#,##0.0####################")
        #If VERSION < 30600 Then
            frmDetallePrecios.txtCentral.Text = Format((oLineaCatalogoSeleccionada.PrecioAdj / oLineaCatalogoSeleccionada.CambioOferta) / oLineaCatalogoSeleccionada.CambioProce, "#,##0.0####################")
            frmDetallePrecios.txtAdj.Text = Format(((oLineaCatalogoSeleccionada.PrecioAdj / oLineaCatalogoSeleccionada.CambioOferta) / oLineaCatalogoSeleccionada.CambioProce), "#,##0.0####################")
        #Else
            'Ahora lo tengo en moneda central cando llega aki
            'para pasarlo a moneda catalogo lo multiplico por 1/dblMonedaEquivalencia
            frmDetallePrecios.txtCentral.Text = Format((oLineaCatalogoSeleccionada.PrecioAdj / oLineaCatalogoSeleccionada.CambioOferta) / oLineaCatalogoSeleccionada.CambioProce * dblMonedaEquivalencia, "#,##0.0####################")
            'Lo dejo en la moneda del catalogo
            frmDetallePrecios.txtAdj.Text = Format(oLineaCatalogoSeleccionada.PrecioAdj / oLineaCatalogoSeleccionada.CambioOferta * dblMonedaEquivalencia, "#,##0.0####################")
        #End If
        
    End If
        
    frmDetallePrecios.lblCambioOfe = frmDetallePrecios.sIdiEquivale & " " & oLineaCatalogoSeleccionada.MonProce
    frmDetallePrecios.lblCambioProce = frmDetallePrecios.sIdiEquivale & " " & gParametrosGenerales.gsMONCEN
    
    frmDetallePrecios.lblOferta = frmDetallePrecios.sIdiOferta & "(" & oLineaCatalogoSeleccionada.MonOferta & ")"
    frmDetallePrecios.lblProce = frmDetallePrecios.sIdiProceso & "(" & oLineaCatalogoSeleccionada.MonProce & ")"
    #If VERSION >= 30600 Then
        frmDetallePrecios.lblCentral.caption = frmDetallePrecios.sIdiCentral & "(" & Me.sdbgAdjudicaciones.Columns("MON").Text & ")"
        frmDetallePrecios.lblCentral2.caption = frmDetallePrecios.sIdiCentral & "(" & Me.sdbgAdjudicaciones.Columns("MON").Text & ")"
        frmDetallePrecios.lblCambioProceCentral = "-->(*1/" & oLineaCatalogoSeleccionada.CambioProce & "..)-->"
        frmDetallePrecios.lblCambioProceCentral = "-->(* " & TruncarDato(dblMonedaEquivalencia) & "..)-->"
        
    #Else
        frmDetallePrecios.lblCentral = frmDetallePrecios.sIdiCentral & "(" & gParametrosGenerales.gsMONCEN & ")"
        frmDetallePrecios.lblCentral2 = frmDetallePrecios.sIdiCentral & "(" & gParametrosGenerales.gsMONCEN & ")"
        frmDetallePrecios.lblCambioProceCentral = "-->(*1/" & oLineaCatalogoSeleccionada.CambioProce & "..)-->"
    #End If
    
    
    frmDetallePrecios.lblCambioOfeProce = "-->(*1/" & oLineaCatalogoSeleccionada.CambioOferta & "..)-->"
    
    
    If oLineaCatalogoSeleccionada.PresupuestoUC = 0 Then
        frmDetallePrecios.txtPres.Text = 0
        frmDetallePrecios.txtAhorroCat.Text = 0
    Else
        #If VERSION < 30600 Then
            frmDetallePrecios.txtPres.Text = Format((oLineaCatalogoSeleccionada.PresupuestoUC / oLineaCatalogoSeleccionada.CambioProce), "#,##0.0####################")
            frmDetallePrecios.txtAhorroCat.Text = Format(CDbl(frmDetallePrecios.txtPres.Text) - CDbl(sdbgAdjudicaciones.Columns("PrecioUB").Value), "#,##0.0####################")
        #Else
            frmDetallePrecios.txtPres.Text = Format((oLineaCatalogoSeleccionada.PresupuestoUC / oLineaCatalogoSeleccionada.CambioProce * dblMonedaEquivalencia), "#,##0.0####################")
            frmDetallePrecios.txtAhorroCat.Text = Format(CDbl(frmDetallePrecios.txtPres.Text) - CDbl(sdbgAdjudicaciones.Columns("PrecioUB").Value), "#,##0.0####################")
        #End If
        If CDbl(frmDetallePrecios.txtAhorroCat.Text) < 0 Then
            frmDetallePrecios.txtAhorroCat.Backcolor = RGB(253, 100, 72)
        End If
    End If
        
    If oLineaCatalogoSeleccionada.PrecioAdj <> 0 Then
        frmDetallePrecios.txtAhorroAdj.Text = Format(CDbl(frmDetallePrecios.txtPres.Text) - CDbl(frmDetallePrecios.txtAdj.Text), "#,##0.0####################")
        If CDbl(frmDetallePrecios.txtAhorroAdj.Text) < 0 Then
            frmDetallePrecios.txtAhorroAdj.Backcolor = RGB(253, 100, 72)
        End If
    End If
    
    If sdbgAdjudicaciones.Columns("PrecioUB").Value = 0 Then
        frmDetallePrecios.txtPrecioCat.Text = 0
    Else
        frmDetallePrecios.txtPrecioCat.Text = Format(CDbl(sdbgAdjudicaciones.Columns("PrecioUB").Value), "#,##0.0####################")
    End If
    
    frmDetallePrecios.Show 1

    Set oLineas = Nothing


End Sub

Private Sub PosicionarBotones()
Dim iLeft As Integer

iLeft = cmdEliAdj.Left + cmdEliAdj.Width + 45
If cmdCambiarCat.Visible Then
    cmdCambiarCat.Left = iLeft
    iLeft = cmdCambiarCat.Left + cmdCambiarCat.Width + 45
End If
If cmdModifPrecios.Visible Then
    cmdModifPrecios.Left = iLeft
    iLeft = cmdModifPrecios.Left + cmdModifPrecios.Width + 45
End If
If cmdExcel(0).Visible Then
    cmdExcel(0).Left = iLeft
    iLeft = cmdExcel(0).Left + cmdExcel(0).Width + 45
End If
If cmdExcel(3).Visible Then
    cmdExcel(3).Left = iLeft
    iLeft = cmdExcel(3).Left + cmdExcel(3).Width + 45
End If
If cmdBuscarAdj.Visible Then
    cmdBuscarAdj.Left = iLeft
    iLeft = cmdBuscarAdj.Left + cmdBuscarAdj.Width + 45
End If
cmdDeshacer.Left = cmdBuscarAdj.Left

iLeft = cmdListado.Left + cmdListado.Width + 45
If cmdExcel(1).Visible Then
    cmdExcel(1).Left = iLeft
    iLeft = cmdExcel(1).Left + cmdExcel(1).Width + 45
End If
If cmdExcel(2).Visible Then
    cmdExcel(2).Left = iLeft
    iLeft = cmdExcel(2).Left + cmdExcel(2).Width + 45
End If

If cmdExcel(4).Visible Then
    cmdExcel(4).Left = iLeft
    iLeft = cmdExcel(4).Left + cmdExcel(4).Width + 45
End If
If cmdExcel(5).Visible Then
    cmdExcel(5).Left = iLeft
    iLeft = cmdExcel(5).Left + cmdExcel(5).Width + 45
End If
End Sub

Private Sub chkModEmision_Click(Index As Integer)
Dim oCat As Object
Dim teserror As TipoErrorSummit

If bRespetarCheck Then Exit Sub
bRespetarCheck = True
If Index = 0 Then
    If chkModEmision(0).Value = vbUnchecked Then chkModEmision(1).Value = vbUnchecked
Else
    If chkModEmision(1).Value = vbChecked Then chkModEmision(0).Value = vbChecked
End If

Set oCat = DevolverCategoria

oCat.ModificarPrecEmision = IIf(chkModEmision(0).Value = vbChecked, True, False)
oCat.ModificarPrecEmisionCat = IIf(chkModEmision(1).Value = vbChecked, True, False)
teserror = oCat.ModificarEmisionPedidos
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    chkModEmision(0).Value = IIf(oCat.ModificarPrecEmision = True, vbChecked, vbUnchecked)
    chkModEmision(1).Value = IIf(oCat.ModificarPrecEmisionCat = True, vbChecked, vbUnchecked)
End If
bRespetarCheck = False
Set oCat = Nothing
End Sub

Private Sub chkVerBajas_Click()
    If chkVerBajas.Value = vbChecked Then
        If bRUsuAprov Then
            'Si el usuario es de aprovisionamiento y tiene restricci�n
            GenerarEstructuraCategoriasAprovisionamiento True
        Else
            'Si no
            GenerarEstructuraCategorias True
        End If
    Else
        If bRUsuAprov Then
            'Si el usuario es de aprovisionamiento y tiene restricci�n
             GenerarEstructuraCategoriasAprovisionamiento False
        Else
            'Si no
            GenerarEstructuraCategorias False
        End If
    End If
End Sub

Private Sub cmdAnyaAdjudicacion_Click()
    Set m_oFrmAnyaAdj = New frmCATALAnyaAdj
        
    If sdbgAdjudicaciones.Rows = 0 Then
        m_oFrmAnyaAdj.sdbcAnyo.Value = Year(Date)
    Else
        If IsEmpty(sdbgAdjudicaciones.Columns("ANYO").Value) Then
            m_oFrmAnyaAdj.sdbcAnyo.Value = Year(Date)
        Else
            m_oFrmAnyaAdj.sdbcAnyo.Value = sdbgAdjudicaciones.Columns("ANYO").Value
        End If
    End If
    m_oFrmAnyaAdj.Show 1
    Screen.MousePointer = vbNormal
    
    Unload m_oFrmAnyaAdj
    Set m_oFrmAnyaAdj = Nothing
    
    sdbgAdjudicaciones.AllowAddNew = False

    bAnyadir = True
    sdbgAdjudicaciones.SelBookmarks.RemoveAll
    Set m_aIdentificadores = Nothing
End Sub

Private Sub cmdAnyaArticulo_Click()
Dim node As MSComctlLib.node

    Set oLineasAAnyadir = Nothing
    Set oLineasAAnyadir = oFSGSRaiz.Generar_CLineasCatalogo
    
    Set node = tvwCategorias.selectedItem
    
    Select Case Left(node.Tag, 4)
    
            Case "CAT1"
                        frmCATALAnyaArt.vCat1 = DevolverId(node)
                        frmCATALAnyaArt.vCat2 = Null
                        frmCATALAnyaArt.vCat3 = Null
                        frmCATALAnyaArt.vCat4 = Null
                        frmCATALAnyaArt.vCat5 = Null
            
            Case "CAT2"
                        frmCATALAnyaArt.vCat1 = DevolverId(node.Parent)
                        frmCATALAnyaArt.vCat2 = DevolverId(node)
                        frmCATALAnyaArt.vCat3 = Null
                        frmCATALAnyaArt.vCat4 = Null
                        frmCATALAnyaArt.vCat5 = Null
            
            Case "CAT3"
                        frmCATALAnyaArt.vCat1 = DevolverId(node.Parent.Parent)
                        frmCATALAnyaArt.vCat2 = DevolverId(node.Parent)
                        frmCATALAnyaArt.vCat3 = DevolverId(node)
                        frmCATALAnyaArt.vCat4 = Null
                        frmCATALAnyaArt.vCat5 = Null
            
            Case "CAT4"
                        frmCATALAnyaArt.vCat1 = DevolverId(node.Parent.Parent.Parent)
                        frmCATALAnyaArt.vCat2 = DevolverId(node.Parent.Parent)
                        frmCATALAnyaArt.vCat3 = DevolverId(node.Parent)
                        frmCATALAnyaArt.vCat4 = DevolverId(node)
                        frmCATALAnyaArt.vCat5 = Null
            
            Case "CAT5"
            
                        frmCATALAnyaArt.vCat1 = DevolverId(node.Parent.Parent.Parent.Parent)
                        frmCATALAnyaArt.vCat2 = DevolverId(node.Parent.Parent.Parent)
                        frmCATALAnyaArt.vCat3 = DevolverId(node.Parent.Parent)
                        frmCATALAnyaArt.vCat4 = DevolverId(node.Parent)
                        frmCATALAnyaArt.vCat5 = DevolverId(node)
    
    End Select
    
    
    frmCATALAnyaArt.bRMat = bRMat
    frmCATALAnyaArt.Show 1
    
    If oLineasAAnyadir.Count > 0 Then
        lblLeyenda.Visible = True
        bModoEdicion = True
        cmdModoEdicion.caption = sIdiModos(0) 'Consulta
        cmdBuscarAdj.Visible = False
        cmdDeshacer.Visible = True
        'deshabilitar los botones
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdBuscarAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        cmdModifPrecios.Enabled = False
        cmdEliAdj.Enabled = True
        cmdDeshacer.Enabled = True
    End If

    bAnyadir = True
    sdbgAdjudicaciones.SelBookmarks.RemoveAll
       Set m_aIdentificadores = Nothing
End Sub

Private Sub cmdAnyaCampoR_Click()
    Set g_oCampos = Nothing
    Set g_oCampos = oFSGSRaiz.Generar_CCampos
    
    sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
    'A�ade un campo de tipo atributo de GS
    If Not g_ofrmATRIBMod Is Nothing Then
        Unload g_ofrmATRIBMod
        Set g_ofrmATRIBMod = Nothing
    End If

    Set g_ofrmATRIBMod = New frmAtribMod
    g_ofrmATRIBMod.g_sOrigen = "frmCatalogo_Recep"

    g_ofrmATRIBMod.Show vbModal
    
    Dim oCat As Object
    Set oCat = DevolverCategoria
    'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
    Dim tsError As TipoErrorSummit
    Set m_oCamposR = oFSGSRaiz.Generar_CCampos
    m_oCamposR.CargarDatos oCat.Id, iNivel, True
    tsError = m_oCamposR.AnyadirCampos(oCat.Id, iNivel, g_oCampos, True)

    Set m_oCamposR = Nothing
    Set m_oCamposR = oFSGSRaiz.Generar_CCampos
    m_oCamposR.CargarDatos oCat.Id, iNivel, True
    'Ponemos el ID a cada l�nea del grid
    With sdbgCamposRecepcion
        If .Rows > 0 Then
            Dim oCam As CCampo
            For Each oCam In m_oCamposR
                Dim i As Integer
                Dim vbm As Variant
                For i = 0 To .Rows - 1
                    vbm = .AddItemBookmark(i)
                    .Bookmark = vbm
                    If (.Columns("ID").CellValue(vbm) = CStr(0) Or .Columns("ID").CellValue(vbm) = "") And .Columns("COD").CellValue(vbm) = oCam.Cod And .Columns("ATRIBID").CellValue(vbm) = CStr(oCam.AtribID) Then
                        .Columns("ID").Value = oCam.Id
                    End If
                Next i
            Next
            cmdEliCampoR.Enabled = True
        End If
    End With
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

Public Sub cmdAnyaCat_Click(Index As Integer)
Dim nodx As MSComctlLib.node
Dim oCat As Object
Dim oProve As CProveedor
Dim teserror As TipoErrorSummit

Set nodx = tvwCategorias.selectedItem

Screen.MousePointer = vbHourglass

If Not nodx Is Nothing Then
    Select Case Index
    Case 0
        Accion = ACCCatCategoriaAnya
        Screen.MousePointer = vbNormal

        Select Case Left(nodx.Tag, 4)
        
            Case "Raiz"
                    frmCatalogoDetalle.caption = sIdiAnyadirCat
                    frmCatalogoDetalle.iCategoria = 1
                    frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT1
            Case "CAT1"
                    frmCatalogoDetalle.caption = sIdiAnyadirSubCat
                    frmCatalogoDetalle.iCategoria = 2
                    frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT2
            Case "CAT2"
                    frmCatalogoDetalle.caption = sIdiAnyadirSubCat
                    frmCatalogoDetalle.iCategoria = 3
                    frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT3
            Case "CAT3"
                    frmCatalogoDetalle.caption = sIdiAnyadirSubCat
                    frmCatalogoDetalle.iCategoria = 4
                    frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT4
            Case "CAT4"
                    frmCatalogoDetalle.caption = sIdiAnyadirSubCat
                    frmCatalogoDetalle.iCategoria = 5
                    frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT5
        End Select

        frmCatalogoDetalle.Show 1
    
    Case 1
        Set oCat = DevolverCategoria
        Set m_oProvesSeleccionados = oCat.ProveedoresPedidoLibre
        Screen.MousePointer = vbNormal
        frmPROVEBuscar.sOrigen = "frmCatProvePedLibre"
        frmPROVEBuscar.Show 1
        If Not m_oProves Is Nothing Then
        If m_oProves.Count > 0 Then
            Screen.MousePointer = vbHourglass
            Select Case iNivel
            Case 1
                teserror = m_oProves.AnyadirProveedoresACategoria(oCat.Id)
            Case 2
                teserror = m_oProves.AnyadirProveedoresACategoria(oCat.Cat1, oCat.Id)
            Case 3
                teserror = m_oProves.AnyadirProveedoresACategoria(oCat.Cat1, oCat.Cat2, oCat.Id)
            Case 4
                teserror = m_oProves.AnyadirProveedoresACategoria(oCat.Cat1, oCat.Cat2, oCat.Cat3, oCat.Id)
            Case 5
                teserror = m_oProves.AnyadirProveedoresACategoria(oCat.Cat1, oCat.Cat2, oCat.Cat3, oCat.Cat4, oCat.Id)
            End Select
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                Set oCat = Nothing
                Exit Sub
            End If
            For Each oProve In m_oProves
                oCat.ProveedoresPedidoLibre.Add oProve.Cod, oProve.Den
                sdbgProves.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den
            Next
            cmdEliCat(1).Enabled = True
            Set oCat = Nothing
            Set m_oProves = Nothing
        End If
        End If
    End Select
    
End If
Screen.MousePointer = vbNormal
End Sub



Public Sub cmdBajaLogica_Click()
Dim nodx As MSComctlLib.node
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
    
    Set nodx = tvwCategorias.selectedItem
    
    If Not nodx Is Nothing Then
        If nodx.Children <> 0 Then
            irespuesta = oMensajes.DarBajaLogica(nodx.Text, True)
        Else
            irespuesta = oMensajes.DarBajaLogica(nodx.Text, False)
        End If
        
        If irespuesta = vbYes Then
            If nodx.Image = "CAT_INT" Then
                irespuesta = oMensajes.PreguntaEliminar("categoria por defecto integraci�n")
                If irespuesta = vbNo Then
                    Exit Sub
                End If
            End If
            Select Case Left(nodx.Tag, 4)
                    Case "CAT1"
                        Set oCategoria1Seleccionada = oFSGSRaiz.Generar_CCategoriaN1
                        oCategoria1Seleccionada.Id = DevolverId(nodx)
                        oCategoria1Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                        scod1 = DevolverCod(nodx)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        
                        Set oCategoria1Seleccionada = oCategoriasNivel1.Item(scod1)
                        teserror = oCategoria1Seleccionada.DarBajaLogica
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            If oCategoria1Seleccionada.BajaLogica = MarcadoParaBaja Then
                                oMensajes.CategoriaEnProcesoBajaLogica
                                If nodx.Image = "SEGURO" Then
                                    nodx.Image = "BAJASEG"
                                Else
                                    nodx.Image = "BAJALOG1"
                                End If
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cod:" & CStr(oCategoria1Seleccionada.Cod) & " BajaLogica=1"
                            Else
                                If nodx.Image = "SEGURO" Then
                                    QuitarSeguridad nodx
                                    oCategoriasNivel1.Item(scod1).Seguridad = 0
                                End If
                                nodx.Image = "BAJALOG2"
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cod:" & CStr(oCategoria1Seleccionada.Cod) & " BajaLogica=2"
                            End If
                            MarcarTodosLosHijos nodx, oCategoria1Seleccionada.BajaLogica
                            Set oCategoria1Seleccionada = Nothing
                        End If
                
                    Case "CAT2"
                        Set oCategoria2Seleccionada = oFSGSRaiz.Generar_CCategoriaN2
                        scod1 = DevolverCod(nodx.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        
                        Set oCategoria2Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
                        

                        teserror = oCategoria2Seleccionada.DarBajaLogica
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            If oCategoria2Seleccionada.BajaLogica = MarcadoParaBaja Then
                                oMensajes.CategoriaEnProcesoBajaLogica
                                If nodx.Image = "SEGURO" Then
                                    nodx.Image = "BAJASEG"
                                Else
                                    nodx.Image = "BAJALOG1"
                                End If
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria2Seleccionada.Cat1Cod) & "Cod:" & CStr(oCategoria2Seleccionada.Cod) & " BajaLogica=1"
                            Else
                                If nodx.Image = "SEGURO" Then
                                    QuitarSeguridad nodx
                                End If
                                nodx.Image = "BAJALOG2"
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria2Seleccionada.Cat1Cod) & "Cod:" & CStr(oCategoria2Seleccionada.Cod) & " BajaLogica=2"
                            End If
                            MarcarTodosLosHijos nodx, oCategoria2Seleccionada.BajaLogica
                            Set oCategoria2Seleccionada = Nothing
                        End If
                
                    Case "CAT3"
                        Set oCategoria3Seleccionada = oFSGSRaiz.Generar_CCategoriaN3
                        scod1 = DevolverCod(nodx.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        scod3 = DevolverCod(nodx)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                        
                        Set oCategoria3Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
                        

                        teserror = oCategoria3Seleccionada.DarBajaLogica
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            If oCategoria3Seleccionada.BajaLogica = MarcadoParaBaja Then
                                oMensajes.CategoriaEnProcesoBajaLogica
                                If nodx.Image = "SEGURO" Then
                                    nodx.Image = "BAJASEG"
                                Else
                                    nodx.Image = "BAJALOG1"
                                End If
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat2:" & CStr(oCategoria3Seleccionada.Cat2Cod) & "Cat1:" & CStr(oCategoria3Seleccionada.Cat1Cod) & "Cod:" & CStr(oCategoria3Seleccionada.Cod) & " BajaLogica=1"
                            Else
                                If nodx.Image = "SEGURO" Then
                                    QuitarSeguridad nodx
                                End If
                                nodx.Image = "BAJALOG2"
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat2:" & CStr(oCategoria3Seleccionada.Cat2Cod) & "Cat1:" & CStr(oCategoria3Seleccionada.Cat1Cod) & "Cod:" & CStr(oCategoria3Seleccionada.Cod) & " BajaLogica=2"
                            End If
                            MarcarTodosLosHijos nodx, oCategoria3Seleccionada.BajaLogica
                            Set oCategoria3Seleccionada = Nothing
                        End If
                    
                    Case "CAT4"
                        Set oCategoria4Seleccionada = oFSGSRaiz.Generar_CCategoriaN4

                        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        scod3 = DevolverCod(nodx.Parent)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                        scod4 = DevolverCod(nodx)
                        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                        
                        Set oCategoria4Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
                        

                        teserror = oCategoria4Seleccionada.DarBajaLogica
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            If oCategoria4Seleccionada.BajaLogica = MarcadoParaBaja Then
                                oMensajes.CategoriaEnProcesoBajaLogica
                                If nodx.Image = "SEGURO" Then
                                    nodx.Image = "BAJASEG"
                                Else
                                    nodx.Image = "BAJALOG1"
                                End If
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria4Seleccionada.Cat1Cod) & "Cat2" & CStr(oCategoria4Seleccionada.Cat2Cod) & "Cat3" & CStr(oCategoria4Seleccionada.CAT3cod) & "Cod:" & CStr(oCategoria4Seleccionada.Cod) & " BajaLogica=1"
                            Else
                                If nodx.Image = "SEGURO" Then
                                    QuitarSeguridad nodx
                                End If
                                nodx.Image = "BAJALOG2"
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria4Seleccionada.Cat1Cod) & "Cat2" & CStr(oCategoria4Seleccionada.Cat2Cod) & "Cat3" & CStr(oCategoria4Seleccionada.CAT3cod) & "Cod:" & CStr(oCategoria4Seleccionada.Cod) & " BajaLogica=2"
                            End If
                            
                            MarcarTodosLosHijos nodx, oCategoria4Seleccionada.BajaLogica
                            Set oCategoria4Seleccionada = Nothing
                        End If
                    
                    Case "CAT5"
                        Set oCategoria5Seleccionada = oFSGSRaiz.Generar_CCategoriaN5
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent.Parent.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        scod3 = DevolverCod(nodx.Parent.Parent)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                        scod4 = DevolverCod(nodx.Parent)
                        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                        scod5 = DevolverCod(nodx)
                        scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                        
                        Set oCategoria5Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5)
                        

                        teserror = oCategoria5Seleccionada.DarBajaLogica
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            If oCategoria5Seleccionada.BajaLogica = MarcadoParaBaja Then
                                oMensajes.CategoriaEnProcesoBajaLogica
                                If nodx.Image = "SEGURO" Then
                                    nodx.Image = "BAJASEG"
                                Else
                                    nodx.Image = "BAJALOG1"
                                End If
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria5Seleccionada.Cat1Cod) & "Cat2" & CStr(oCategoria5Seleccionada.Cat2Cod) & "Cat3" & CStr(oCategoria5Seleccionada.CAT3cod) & "Cat4" & CStr(oCategoria5Seleccionada.CAT4cod) & "Cod:" & CStr(oCategoria5Seleccionada.Cod) & " BajaLogica=1"
                            Else
                                If nodx.Image = "SEGURO" Then
                                    QuitarSeguridad nodx
                                End If
                                nodx.Image = "BAJALOG2"
                                RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria5Seleccionada.Cat1Cod) & "Cat2" & CStr(oCategoria5Seleccionada.Cat2Cod) & "Cat3" & CStr(oCategoria5Seleccionada.CAT3cod) & "Cat4" & CStr(oCategoria5Seleccionada.CAT4cod) & "Cod:" & CStr(oCategoria5Seleccionada.Cod) & " BajaLogica=2"
                            End If
                            MarcarTodosLosHijos nodx, oCategoria5Seleccionada.BajaLogica
                            Set oCategoria5Seleccionada = Nothing
                        End If
            End Select
            If nodx.Image = "BAJALOG2" Or nodx.Image = "BAJALOG1" Or nodx.Image = "BAJASEG" Then
                txtObsAdjun.Locked = True
                cmdA�adirAdjun.Enabled = False
                cmdEliminarAdjun.Enabled = False
                cmdModificarAdjun.Enabled = False
                cmdAnyaCat(1).Enabled = False
                cmdEliCat(1).Enabled = False
                cmdAnyaCat(0).Enabled = False
                cmdModifCat.Enabled = False
                cmdBajaLogica.Enabled = False
            End If
        End If
        
    End If

End Sub

Private Sub MarcarTodosLosHijos(ByVal nodx As MSComctlLib.node, ByVal iBajalog As Integer)

Dim nod1 As MSComctlLib.node
Dim nod2 As MSComctlLib.node
Dim nod3 As MSComctlLib.node
Dim nod4 As MSComctlLib.node
Dim oCat As Object
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
                    
Select Case Left(nodx.Tag, 4)

    Case "CAT1"
        Set nod1 = nodx.Child
        If iBajalog = tipobajalogica.BajaLogica Then
            While Not nod1 Is Nothing
                If nod1.Image = "SEGURO" Then
                    QuitarSeguridad nod1
                End If
                nod1.Image = "BAJALOG2"
                
                Set nod2 = nod1.Child
                While Not nod2 Is Nothing
                    If nod2.Image = "SEGURO" Then
                        QuitarSeguridad nod2
                    End If
                    nod2.Image = "BAJALOG2"
                    
                    Set nod3 = nod2.Child
                    While Not nod3 Is Nothing
                        If nod3.Image = "SEGURO" Then
                            QuitarSeguridad nod3
                        End If
                        nod3.Image = "BAJALOG2"
                        
                        Set nod4 = nod3.Child
                        While Not nod4 Is Nothing
                            If nod4.Image = "SEGURO" Then
                                QuitarSeguridad nod4
                            End If
                            nod4.Image = "BAJALOG2"
                            Set nod4 = nod4.Next
                        Wend
                        Set nod3 = nod3.Next
                    Wend
                    Set nod2 = nod2.Next
                Wend
                Set nod1 = nod1.Next
             Wend
             
        Else
            Set oCat = DevolverCategoria
            While Not nod1 Is Nothing
                If nod1.Image = "SEGURO" Then
                    QuitarSeguridad nod1
                End If
                scod1 = DevolverCod(nod1.Parent)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                scod2 = DevolverCod(nod1)
                scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                If oCat.CategoriasN2.Item(scod1 & scod2).BajaLogica = BajaLogica Then
                    nod1.Image = "BAJALOG2"
                Else
                    nod1.Image = "BAJALOG1"
                End If
                    
                Set nod2 = nod1.Child
                While Not nod2 Is Nothing
                    If nod2.Image = "SEGURO" Then
                        QuitarSeguridad nod2
                    End If
                    scod3 = DevolverCod(nod2)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    If oCat.CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).BajaLogica = BajaLogica Then
                        nod2.Image = "BAJALOG2"
                    Else
                        nod2.Image = "BAJALOG1"
                    End If
                
                    Set nod3 = nod2.Child
                    While Not nod3 Is Nothing
                        If nod3.Image = "SEGURO" Then
                            QuitarSeguridad nod3
                        End If
                        scod4 = DevolverCod(nod3)
                        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                        If oCat.CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).BajaLogica = BajaLogica Then
                            nod3.Image = "BAJALOG2"
                        Else
                            nod3.Image = "BAJALOG1"
                        End If
                    
                        Set nod4 = nod3.Child
                        While Not nod4 Is Nothing
                            If nod4.Image = "SEGURO" Then
                                QuitarSeguridad nod4
                            End If
                            scod5 = DevolverCod(nod4)
                            scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                            If oCat.CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5).BajaLogica = BajaLogica Then
                                nod4.Image = "BAJALOG2"
                            Else
                                nod4.Image = "BAJALOG1"
                            End If
                        
                            Set nod4 = nod4.Next
                        Wend
                        Set nod3 = nod3.Next
                    Wend
                    Set nod2 = nod2.Next
                Wend
                Set nod1 = nod1.Next
             Wend
             Set oCat = Nothing
        End If
        
    DoEvents
    
    Case "CAT2"
        Set nod2 = nodx.Child
        
        If iBajalog = tipobajalogica.BajaLogica Then
            While Not nod2 Is Nothing
                If nod2.Image = "SEGURO" Then
                    QuitarSeguridad nod2
                End If
                nod2.Image = "BAJALOG2"
                
                Set nod3 = nod2.Child
                While Not nod3 Is Nothing
                    If nod3.Image = "SEGURO" Then
                        QuitarSeguridad nod3
                    End If
                    nod3.Image = "BAJALOG2"
                
                    Set nod4 = nod3.Child
                    While Not nod4 Is Nothing
                        If nod4.Image = "SEGURO" Then
                            QuitarSeguridad nod4
                        End If
                        nod4.Image = "BAJALOG2"
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
        Else
            Set oCat = DevolverCategoria
            While Not nod2 Is Nothing
                If nod2.Image = "SEGURO" Then
                    QuitarSeguridad nod2
                End If
                scod1 = DevolverCod(nod2.Parent.Parent)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                scod2 = DevolverCod(nod2.Parent)
                scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                scod3 = DevolverCod(nod2)
                scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                If oCat.CategoriasN3.Item(scod1 & scod2 & scod3).BajaLogica = BajaLogica Then
                    nod2.Image = "BAJALOG2"
                Else
                    nod2.Image = "BAJALOG1"
                End If
                
                Set nod3 = nod2.Child
                While Not nod3 Is Nothing
                    If nod3.Image = "SEGURO" Then
                        QuitarSeguridad nod3
                    End If
                    scod4 = DevolverCod(nod3)
                    scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                    If oCat.CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).BajaLogica = BajaLogica Then
                        nod3.Image = "BAJALOG2"
                    Else
                        nod3.Image = "BAJALOG1"
                    End If
                    
                    Set nod4 = nod3.Child
                    While Not nod4 Is Nothing
                        If nod4.Image = "SEGURO" Then
                            QuitarSeguridad nod4
                        End If
                        scod5 = DevolverCod(nod4)
                        scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                        If oCat.CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5).BajaLogica = BajaLogica Then
                            nod4.Image = "BAJALOG2"
                        Else
                            nod4.Image = "BAJALOG1"
                        End If
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                Set nod2 = nod2.Next
            Wend
            Set oCat = Nothing
        End If
        
    Case "CAT3"
            Set nod3 = nodx.Child
            If iBajalog = tipobajalogica.BajaLogica Then
                While Not nod3 Is Nothing
                    If nod3.Image = "SEGURO" Then
                        QuitarSeguridad nod3
                    End If
                    nod3.Image = "BAJALOG2"
                
                    Set nod4 = nod3.Child
                    While Not nod4 Is Nothing
                        If nod4.Image = "SEGURO" Then
                            QuitarSeguridad nod4
                        End If
                        nod4.Image = "BAJALOG2"
                        Set nod4 = nod4.Next
                    Wend
                    Set nod3 = nod3.Next
                Wend
                
            Else
                Set oCat = DevolverCategoria
                While Not nod3 Is Nothing
                    If nod3.Image = "SEGURO" Then
                        QuitarSeguridad nod3
                    End If
                    scod1 = DevolverCod(nod3.Parent.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nod3.Parent.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nod3.Parent)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    scod4 = DevolverCod(nod3)
                    scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                    If oCat.CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).BajaLogica = BajaLogica Then
                        nod3.Image = "BAJALOG2"
                    Else
                        nod3.Image = "BAJALOG1"
                    End If
                    
                    Set nod4 = nod3.Child
                    While Not nod4 Is Nothing
                        If nod4.Image = "SEGURO" Then
                            QuitarSeguridad nod4
                        End If
                        scod5 = DevolverCod(nod4)
                        scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                        If oCat.CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5).BajaLogica = BajaLogica Then
                            nod4.Image = "BAJALOG2"
                        Else
                            nod4.Image = "BAJALOG1"
                        End If
                        Set nod4 = nod4.Next
                    Wend
                    
                    Set nod3 = nod3.Next
                Wend
                Set oCat = Nothing
            End If
            
            
    Case "CAT4"
            Set nod4 = nodx.Child
            If iBajalog = tipobajalogica.BajaLogica Then
                While Not nod4 Is Nothing
                    If nod4.Image = "SEGURO" Then
                        QuitarSeguridad nod4
                    End If
                    nod4.Image = "BAJALOG2"
                    Set nod4 = nod4.Next
                Wend
            Else
                Set oCat = DevolverCategoria
                While Not nod4 Is Nothing
                    If nod4.Image = "SEGURO" Then
                        QuitarSeguridad nod4
                    End If
                    scod1 = DevolverCod(nod4.Parent.Parent.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nod4.Parent.Parent.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nod4.Parent.Parent)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    scod4 = DevolverCod(nod4.Parent)
                    scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                    scod5 = DevolverCod(nod4)
                    scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                    If oCat.CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5).BajaLogica = BajaLogica Then
                        nod4.Image = "BAJALOG2"
                    Else
                        nod4.Image = "BAJALOG1"
                    End If
                    Set nod4 = nod4.Next
                Wend
                Set oCat = Nothing
            End If
                
End Select

End Sub

Private Sub QuitarMarcaTodosSusPadres(ByVal nodx As MSComctlLib.node)
    
    Set nodx = nodx.Parent
    While Not nodx Is Nothing
        If Not nodx.Tag = "Raiz" Then
            If Not nodx.Image = "SEGURO" Then
                nodx.Image = "CAT"
            End If
        End If
        Set nodx = nodx.Parent
    Wend
    DoEvents
    
End Sub

Private Sub QuitarSeguridad(ByVal nodx As MSComctlLib.node)
Dim sTag As String
Dim iPos As Integer

    sTag = nodx.Tag
    iPos = InStr(1, sTag, "-%$SEG")
    nodx.Tag = Left(sTag, iPos - 1)

End Sub


Private Sub cmdBuscarAdj_Click()
    frmCATLINBuscar.Show 1
End Sub

Private Sub cmdBuscarCat_Click()
    frmCatalogoBuscar.bRUsuAprov = bRUsuAprov
    frmCatalogoBuscar.Show 1
End Sub

Private Sub cmdCambiarCat_Click()
Dim oLineasSeleccionadas As CLineasCatalogo
Dim oLinea As CLineaCatalogo
Dim node As MSComctlLib.node
Dim i As Long
Dim teserror As TipoErrorSummit
Dim SeguridadCatn As Integer
Dim SeguridadNivel As Integer
    If sdbgAdjudicaciones.Rows = 0 Then Exit Sub
    
    If ((Not IsNull(sdbgAdjudicaciones.Bookmark)) And (Not IsEmpty(sdbgAdjudicaciones.Bookmark)) And (sdbgAdjudicaciones.SelBookmarks.Count = 0)) Then
        sdbgAdjudicaciones.SelBookmarks.Add sdbgAdjudicaciones.Bookmark
    End If
    
    If sdbgAdjudicaciones.SelBookmarks.Count = 0 Then Exit Sub
    
    frmSELCat.sOrigen = "frmCatalogo"
    frmSELCat.bRUsuAprov = bRUsuAprov
    frmSELCat.bVerBajas = False
    frmSELCat.Show 1
    
    If Not bContinuarCambioCat Then Exit Sub
    
    If IsEmpty(CAT1Seleccionada) Then
        'No hemos querido cambiar de categor�a las l�neas
        sdbgAdjudicaciones.SelBookmarks.RemoveAll
        Set m_aIdentificadores = Nothing
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set oLineasSeleccionadas = Nothing
    Set oLineasSeleccionadas = oFSGSRaiz.Generar_CLineasCatalogo
    Set oLinea = Nothing
    
    'Tiene seguridad, hay que saber que categoria y nivel
    Dim oSeguridad As CSeguridad
    Set oSeguridad = oFSGSRaiz.Generar_CSeguridad
    Set oLinea = oFSGSRaiz.Generar_CLineaCatalogo
    
    For i = 0 To sdbgAdjudicaciones.SelBookmarks.Count - 1
        oLinea.Id = sdbgAdjudicaciones.Columns("ID").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i))
    
        Set node = tvwCategorias.selectedItem
        Select Case Left(node.Tag, 4)
    
            Case "CAT1"
                    oLinea.Cat1 = oCategoria1Seleccionada.Id
                    oLinea.Cat2 = Null
                    oLinea.Cat3 = Null
                    oLinea.Cat4 = Null
                    oLinea.Cat5 = Null
                    If SeguridadCat = 1 Then
                        oSeguridad.Categoria = oCategoria1Seleccionada.Id
                        oSeguridad.NivelCat = 1
                        oSeguridad.CargarDatosSeguridad
                        If oSeguridad.SeguridadConfigurada Then
                            SeguridadCatn = oSeguridad.Categoria
                            SeguridadNivel = oSeguridad.NivelCat
                        End If
                    End If
            Case "CAT2"
                    oLinea.Cat1 = oCategoria2Seleccionada.Cat1
                    oLinea.Cat2 = oCategoria2Seleccionada.Id
                    oLinea.Cat3 = Null
                    oLinea.Cat4 = Null
                    oLinea.Cat5 = Null
                    If SeguridadCat = 1 Then
                        oSeguridad.Categoria = oCategoria2Seleccionada.Id
                        oSeguridad.NivelCat = 2
                        oSeguridad.CargarDatosSeguridad
                        If oSeguridad.SeguridadConfigurada Then
                            SeguridadCatn = oSeguridad.Categoria
                            SeguridadNivel = oSeguridad.NivelCat
                        Else
                            oSeguridad.Categoria = oCategoria2Seleccionada.Cat1
                            oSeguridad.NivelCat = 1
                            oSeguridad.CargarDatosSeguridad
                            If oSeguridad.SeguridadConfigurada Then
                                SeguridadCatn = oSeguridad.Categoria
                                SeguridadNivel = oSeguridad.NivelCat
                            End If
                        End If
                    End If
            Case "CAT3"
                    oLinea.Cat1 = oCategoria3Seleccionada.Cat1
                    oLinea.Cat2 = oCategoria3Seleccionada.Cat2
                    oLinea.Cat3 = oCategoria3Seleccionada.Id
                    oLinea.Cat4 = Null
                    oLinea.Cat5 = Null
                    If SeguridadCat = 1 Then
                        oSeguridad.Categoria = oCategoria3Seleccionada.Id
                        oSeguridad.NivelCat = 3
                        oSeguridad.CargarDatosSeguridad
                        If oSeguridad.SeguridadConfigurada Then
                            SeguridadCatn = oSeguridad.Categoria
                            SeguridadNivel = oSeguridad.NivelCat
                        Else
                            oSeguridad.Categoria = oCategoria3Seleccionada.Cat2
                            oSeguridad.NivelCat = 2
                            oSeguridad.CargarDatosSeguridad
                            If oSeguridad.SeguridadConfigurada Then
                                SeguridadCatn = oSeguridad.Categoria
                                SeguridadNivel = oSeguridad.NivelCat
                            Else
                                oSeguridad.Categoria = oCategoria3Seleccionada.Cat1
                                oSeguridad.NivelCat = 1
                                oSeguridad.CargarDatosSeguridad
                                If oSeguridad.SeguridadConfigurada Then
                                    SeguridadCatn = oSeguridad.Categoria
                                    SeguridadNivel = oSeguridad.NivelCat
                                End If
                            End If
                        End If
                    End If
            Case "CAT4"
                    oLinea.Cat1 = oCategoria4Seleccionada.Cat1
                    oLinea.Cat2 = oCategoria4Seleccionada.Cat2
                    oLinea.Cat3 = oCategoria4Seleccionada.Cat3
                    oLinea.Cat4 = oCategoria4Seleccionada.Id
                    oLinea.Cat5 = Null
                    If SeguridadCat = 1 Then
                        oSeguridad.Categoria = oCategoria4Seleccionada.Id
                        oSeguridad.NivelCat = 4
                        oSeguridad.CargarDatosSeguridad
                        If oSeguridad.SeguridadConfigurada Then
                            SeguridadCatn = oSeguridad.Categoria
                            SeguridadNivel = oSeguridad.NivelCat
                        Else
                            oSeguridad.Categoria = oCategoria4Seleccionada.Cat3
                            oSeguridad.NivelCat = 3
                            oSeguridad.CargarDatosSeguridad
                            If oSeguridad.SeguridadConfigurada Then
                                SeguridadCatn = oSeguridad.Categoria
                                SeguridadNivel = oSeguridad.NivelCat
                            Else
                                oSeguridad.Categoria = oCategoria4Seleccionada.Cat2
                                oSeguridad.NivelCat = 2
                                oSeguridad.CargarDatosSeguridad
                                If oSeguridad.SeguridadConfigurada Then
                                    SeguridadCatn = oSeguridad.Categoria
                                    SeguridadNivel = oSeguridad.NivelCat
                                Else
                                    oSeguridad.Categoria = oCategoria4Seleccionada.Cat1
                                    oSeguridad.NivelCat = 1
                                    oSeguridad.CargarDatosSeguridad
                                    If oSeguridad.SeguridadConfigurada Then
                                        SeguridadCatn = oSeguridad.Categoria
                                        SeguridadNivel = oSeguridad.NivelCat
                                    End If
                                End If
                            End If
                        End If
                    End If
            Case "CAT5"
                    oLinea.Cat1 = oCategoria5Seleccionada.Cat1
                    oLinea.Cat2 = oCategoria5Seleccionada.Cat2
                    oLinea.Cat3 = oCategoria5Seleccionada.Cat3
                    oLinea.Cat4 = oCategoria5Seleccionada.Cat4
                    oLinea.Cat5 = oCategoria5Seleccionada.Id
                    If SeguridadCat = 1 Then
                        oSeguridad.Categoria = oCategoria5Seleccionada.Id
                        oSeguridad.NivelCat = 5
                        oSeguridad.CargarDatosSeguridad
                        If oSeguridad.SeguridadConfigurada Then
                            SeguridadCatn = oSeguridad.Categoria
                            SeguridadNivel = oSeguridad.NivelCat
                        Else
                            oSeguridad.Categoria = oCategoria5Seleccionada.Cat4
                            oSeguridad.NivelCat = 4
                            oSeguridad.CargarDatosSeguridad
                            If oSeguridad.SeguridadConfigurada Then
                                SeguridadCatn = oSeguridad.Categoria
                                SeguridadNivel = oSeguridad.NivelCat
                            Else
                                oSeguridad.Categoria = oCategoria5Seleccionada.Cat3
                                oSeguridad.NivelCat = 3
                                oSeguridad.CargarDatosSeguridad
                                If oSeguridad.SeguridadConfigurada Then
                                    SeguridadCatn = oSeguridad.Categoria
                                    SeguridadNivel = oSeguridad.NivelCat
                                Else
                                    oSeguridad.Categoria = oCategoria5Seleccionada.Cat2
                                    oSeguridad.NivelCat = 2
                                    oSeguridad.CargarDatosSeguridad
                                    If oSeguridad.SeguridadConfigurada Then
                                        SeguridadCatn = oSeguridad.Categoria
                                        SeguridadNivel = oSeguridad.NivelCat
                                    Else
                                        oSeguridad.Categoria = oCategoria5Seleccionada.Cat1
                                        oSeguridad.NivelCat = 1
                                        oSeguridad.CargarDatosSeguridad
                                        If oSeguridad.SeguridadConfigurada Then
                                            SeguridadCatn = oSeguridad.Categoria
                                            SeguridadNivel = oSeguridad.NivelCat
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
        End Select

        oLineasSeleccionadas.Add oLinea.Id
        
    Next
    
    teserror = oLineasSeleccionadas.CambiarDeCategoria(CAT1Seleccionada, NullToStr(CAT2Seleccionada), NullToStr(CAT3Seleccionada), NullToStr(CAT4Seleccionada), NullToStr(CAT5Seleccionada), SeguridadCatn, SeguridadNivel)
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    Else
        For i = 0 To sdbgAdjudicaciones.SelBookmarks.Count - 1
            RegistrarAccion AccionesSummit.ACCCAtAdjudCatCamb, "Cat1:" & CStr(oLinea.Cat1) & ", Cat2:" & NullToStr(oLinea.Cat2) & ", Cat3:" & NullToStr(oLinea.Cat3) & ", Cat4:" & NullToStr(oLinea.Cat4) & ", Cat5:" & NullToStr(oLinea.Cat5)
        Next
        sdbgAdjudicaciones.DeleteSelected
    End If
                    
    sdbgAdjudicaciones.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
    Set m_aIdentificadores = Nothing
    Set oLinea = Nothing
    Set oLineasSeleccionadas = Nothing

End Sub

Private Sub cmdDeshacer_Click()

    sdbgAdjudicaciones.CancelUpdate
    sdbgAdjudicaciones.DataChanged = False
    
    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = True Then Exit Sub
    
    If Not oLineasAAnyadir Is Nothing Then
        If oLineasAAnyadir.Count > 0 Then Exit Sub
    End If
    
    If bModifAdj Then
        cmdAnyaAdjudicacion.Enabled = True
        cmdAnyaArticulo.Enabled = True
        cmdEliAdj.Enabled = True
        cmdDeshacer.Enabled = False
    End If
    
    If bModifAdjCat Then
        cmdCambiarCat.Enabled = True
    End If
    
    If bModifAdjPrecios Then
        cmdModifPrecios.Enabled = True
    End If
    
    If tvwCategorias.selectedItem.Children <> 0 Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        If bModifAdjPrecios Then
            cmdModifPrecios.Enabled = True
        End If
    End If

    m_bCargarComboDesdeDD = False

    bAnyadir = False

End Sub

Private Sub cmdEliAdj_Click()
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim i, j As Long
Dim oLinea As CLineaCatalogo
Dim bContinuar As Boolean
                
    If sdbgAdjudicaciones.Rows = 0 Then Exit Sub
        
    If ((Not IsNull(sdbgAdjudicaciones.Bookmark)) And (Not IsEmpty(sdbgAdjudicaciones.Bookmark)) And (sdbgAdjudicaciones.SelBookmarks.Count = 0)) Then
        sdbgAdjudicaciones.SelBookmarks.Add sdbgAdjudicaciones.Bookmark
    End If
    
    If sdbgAdjudicaciones.SelBookmarks.Count = 0 Then Exit Sub
    
    irespuesta = oMensajes.PreguntaEliminar(sIdiLineas)
    If irespuesta = vbNo Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oLinea = Nothing
    Set oLinea = oFSGSRaiz.Generar_CLineaCatalogo

    For i = 0 To sdbgAdjudicaciones.SelBookmarks.Count - 1
        If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i))) = True Then
            oLineasAAnyadir.Remove sdbgAdjudicaciones.Columns("ID").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i))
            If oLineasAAnyadir.Count = 0 Then
                lblLeyenda.Visible = False
            End If
        Else
            
            oLinea.Id = sdbgAdjudicaciones.Columns("ID").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i))
        
            If GridCheckToBoolean(sdbgAdjudicaciones.Columns("PUB").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i))) = True Then
                If oLinea.ExistenPedidosFavoritos Then
                    If oMensajes.PreguntarEliminarPedidosFavoritos(3, oLineasCatalogo.Item(CStr(oLinea.Id)).ArtDen & " (" & oLineasCatalogo.Item(CStr(oLinea.Id)).ProveCod & ")") = vbYes Then
                        bContinuar = True
                    Else
                        bContinuar = False
                    End If
                Else
                    bContinuar = True
                End If
            Else
                bContinuar = True
            End If
                
            If bContinuar Then
                Set oIBaseDatos = oLinea
                teserror = oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    'Se han borrado todas las anteriores hay que eliminarlas
                    For j = i To sdbgAdjudicaciones.SelBookmarks.Count - 1
                        sdbgAdjudicaciones.SelBookmarks.Remove i
                    Next
                    m_bRespetar = True
                    sdbgAdjudicaciones.DeleteSelected
                    m_bRespetar = False
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    oLineasCatalogo.Remove sdbgAdjudicaciones.Columns("ID").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i))
                    RegistrarAccion AccionesSummit.ACCCatAdjudEli, "Id:" & CStr(oLinea.Id)
                End If
            End If
        End If
    Next

    m_bRespetar = True
    sdbgAdjudicaciones.DeleteSelected
    m_bRespetar = False
    sdbgAdjudicaciones.SelBookmarks.RemoveAll

    If bCatBajaLog Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        cmdModifPrecios.Enabled = False
        cmdModoEdicion.Enabled = False
    Else
        cmdModoEdicion.Enabled = True
        If oLineasAAnyadir Is Nothing Then
            cmdAnyaAdjudicacion.Enabled = True
            cmdAnyaArticulo.Enabled = True
            cmdEliAdj.Enabled = True
            cmdDeshacer.Enabled = False
            If bModifAdjPrecios Then
                cmdModifPrecios.Enabled = True
            End If
            If bModifAdjCat Then
                cmdCambiarCat.Enabled = True
            End If
        Else
            If oLineasAAnyadir.Count = 0 Then
                cmdAnyaAdjudicacion.Enabled = True
                cmdAnyaArticulo.Enabled = True
                cmdEliAdj.Enabled = True
                cmdDeshacer.Enabled = False
                If bModifAdjPrecios Then
                    cmdModifPrecios.Enabled = True
                End If
                If bModifAdjCat Then
                    cmdCambiarCat.Enabled = True
                End If
            End If
        End If
    End If
        
    Set m_aIdentificadores = Nothing
    Screen.MousePointer = vbNormal
    Set oLinea = Nothing

End Sub

Private Sub cmdEliCampoR_Click()
Screen.MousePointer = vbHourglass
    'Controlar que hay al menos un atributo seleccionado:
    '   Mirar en proveedores
    'Confirmar que se desea eliminar los atributos
    '   Mirar para eso la funcion cmdAceptarCampos_Click, tiene ejemplos de c�mo pedir confirmaci�n
    sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
    
    Dim i As Long
    Dim teserror As TipoErrorSummit
    If sdbgCamposRecepcion.SelBookmarks.Count > 0 Then
        i = oMensajes.PreguntaEliminar(sConfirmEliminarCampos)
        If i = vbYes Then
            Dim oCamposAEliminar As CCampos
            Set oCamposAEliminar = oFSGSRaiz.Generar_CCampos
            For i = 0 To sdbgCamposRecepcion.SelBookmarks.Count - 1
                With sdbgCamposRecepcion
                    oCamposAEliminar.Add .Columns("ID").CellValue(.SelBookmarks(i)), .Columns("COD").CellValue(.SelBookmarks(i)), , , , , , , , , , .Columns("ATRIBID").CellValue(.SelBookmarks(i))
                End With
            Next
            Dim oCat As Object
            Set oCat = DevolverCategoria
            teserror = oCamposAEliminar.EliminarCampos(oCat.Id, iNivel)
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                Set oCat = Nothing
                Exit Sub
            End If
            sdbgCamposRecepcion.DeleteSelected
            If sdbgCamposRecepcion.Rows <= 0 Then
                cmdEliCampo.Enabled = False
            End If
        Else
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        Set m_oCamposR = Nothing
        Set m_oCamposR = oFSGSRaiz.Generar_CCampos
        m_oCamposR.CargarDatos oCat.Id, iNivel, True
        sdbgCamposRecepcion.MoveFirst
        Screen.MousePointer = Normal
    Else
        Screen.MousePointer = vbNormal
        'PONER MENSAJE DE QUE NO HAY SELECCIONADO NING�N CAMPO DE PEDIDO
        MsgBox sCamposNoSeleccionados, vbInformation, tabDatos.TabCaption(1)
    End If
End Sub

Public Sub cmdEliCat_Click(Index As Integer)
Dim nodx As MSComctlLib.node
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim bLineasCat As Boolean
Dim oCat As Object
Dim i As Long
Dim oProve As CProveedor

Set nodx = tvwCategorias.selectedItem

If Not nodx Is Nothing Then
    Select Case Index
    Case 0
        Select Case Left(nodx.Tag, 4)
            
        Case "CAT1"
                 
                Accion = ACCCATCATEGORIAEli
                
                Set oCategoria1Seleccionada = Nothing
                Set oCategoria1Seleccionada = oFSGSRaiz.Generar_CCategoriaN1
                oCategoria1Seleccionada.Id = DevolverId(nodx)
                oCategoria1Seleccionada.Cod = DevolverCod(nodx)
                oCategoria1Seleccionada.Den = DevolverDen(nodx)
                oCategoria1Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                irespuesta = oMensajes.PreguntaEliminar(sIdiCategoria & ": " & CStr(oCategoria1Seleccionada.Cod) & " (" & oCategoria1Seleccionada.Den & ")")
                If irespuesta = vbNo Then Exit Sub
                
                bLineasCat = oCategoria1Seleccionada.ExistenLineasCatalogo
                If bLineasCat Then
                    irespuesta = oMensajes.PreguntaExistenLineasCat
                    If irespuesta = vbNo Then Exit Sub
                End If
            
                Screen.MousePointer = vbHourglass
                Set oIBaseDatos = oCategoria1Seleccionada
                teserror = oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    RegistrarAccion AccionesSummit.ACCCATCATEGORIAEli, "CAT1:" & CStr(oCategoria1Seleccionada.Id)
                    EliminarCATDeEstructura
                End If
                
                Set oIBaseDatos = Nothing
                Set oCategoria1Seleccionada = Nothing
        
        Case "CAT2"
                    
                Accion = ACCCATCATEGORIAEli
                
                Set oCategoria2Seleccionada = Nothing
                Set oCategoria2Seleccionada = oFSGSRaiz.Generar_CCategoriaN2
                oCategoria2Seleccionada.Cod = DevolverCod(nodx)
                oCategoria2Seleccionada.Id = DevolverId(nodx)
                oCategoria2Seleccionada.Cat1 = DevolverId(nodx.Parent)
                oCategoria2Seleccionada.Den = DevolverDen(nodx)
                oCategoria2Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
    
                irespuesta = oMensajes.PreguntaEliminar(sIdiCategoria & ": " & CStr(oCategoria2Seleccionada.Cod) & " (" & oCategoria2Seleccionada.Den & ")")
                If irespuesta = vbNo Then Exit Sub
                
    
                bLineasCat = oCategoria2Seleccionada.ExistenLineasCatalogo
                If bLineasCat Then
                    irespuesta = oMensajes.PreguntaExistenLineasCat
                    If irespuesta = vbNo Then Exit Sub
                End If
                
                Screen.MousePointer = vbHourglass
                Set oIBaseDatos = oCategoria2Seleccionada
                teserror = oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    RegistrarAccion ACCCATCATEGORIAEli, "CAT1:" & CStr(oCategoria2Seleccionada.Cat1) & "CAT2:" & CStr(oCategoria2Seleccionada.Id)
                    EliminarCATDeEstructura
                End If
                
                Set oIBaseDatos = Nothing
                Set oCategoria2Seleccionada = Nothing
                        
            
            Case "CAT3"
            
                 Accion = ACCCATCATEGORIAEli
                
                Set oCategoria3Seleccionada = Nothing
                Set oCategoria3Seleccionada = oFSGSRaiz.Generar_CCategoriaN3
                oCategoria3Seleccionada.Cod = DevolverCod(nodx)
                oCategoria3Seleccionada.Id = DevolverId(nodx)
                oCategoria3Seleccionada.Cat1 = DevolverId(nodx.Parent.Parent)
                oCategoria3Seleccionada.Cat2 = DevolverId(nodx.Parent)
                oCategoria3Seleccionada.Den = DevolverDen(nodx)
                oCategoria3Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                irespuesta = oMensajes.PreguntaEliminar(sIdiCategoria & ": " & CStr(oCategoria3Seleccionada.Cod) & " (" & oCategoria3Seleccionada.Den & ")")
                If irespuesta = vbNo Then Exit Sub
                
             
                bLineasCat = oCategoria3Seleccionada.ExistenLineasCatalogo
                If bLineasCat Then
                    irespuesta = oMensajes.PreguntaExistenLineasCat
                    If irespuesta = vbNo Then Exit Sub
                End If
                
                Screen.MousePointer = vbHourglass
                Set oIBaseDatos = oCategoria3Seleccionada
                teserror = oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    Screen.MousePointer = vbNormal
                    Exit Sub
                Else
                    RegistrarAccion ACCCATCATEGORIAEli, "CAT1:" & CStr(oCategoria3Seleccionada.Cat1) & "CAT2:" & CStr(oCategoria3Seleccionada.Cat2) & "CAT3:" & CStr(oCategoria3Seleccionada.Id)
                    EliminarCATDeEstructura
                End If
                
                Set oIBaseDatos = Nothing
                Set oCategoria3Seleccionada = Nothing
            
            Case "CAT4"
                 
                Accion = ACCCATCATEGORIAEli
                
                Set oCategoria4Seleccionada = Nothing
                Set oCategoria4Seleccionada = oFSGSRaiz.Generar_CCategoriaN4
                oCategoria4Seleccionada.Cod = DevolverCod(nodx)
                oCategoria4Seleccionada.Id = DevolverId(nodx)
                oCategoria4Seleccionada.Cat1 = DevolverId(nodx.Parent.Parent.Parent)
                oCategoria4Seleccionada.Cat2 = DevolverId(nodx.Parent.Parent)
                oCategoria4Seleccionada.Cat3 = DevolverId(nodx.Parent)
                oCategoria4Seleccionada.Den = DevolverDen(nodx)
                oCategoria4Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                irespuesta = oMensajes.PreguntaEliminar(sIdiCategoria & ": " & CStr(oCategoria4Seleccionada.Cod) & " (" & oCategoria4Seleccionada.Den & ")")
                If irespuesta = vbNo Then Exit Sub
                
    
                bLineasCat = oCategoria4Seleccionada.ExistenLineasCatalogo
                If bLineasCat Then
                    irespuesta = oMensajes.PreguntaExistenLineasCat
                    If irespuesta = vbNo Then Exit Sub
                End If
                
                Screen.MousePointer = vbHourglass
                Set oIBaseDatos = oCategoria4Seleccionada
                teserror = oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                Else
                    RegistrarAccion ACCCATCATEGORIAEli, "CAT1:" & CStr(oCategoria4Seleccionada.Cat1) & "CAT2:" & CStr(oCategoria4Seleccionada.Cat2) & "CAT3:" & CStr(oCategoria4Seleccionada.Cat3) & "CAT4:" & CStr(oCategoria4Seleccionada.Id)
                    EliminarCATDeEstructura
                End If
                    
                Set oIBaseDatos = Nothing
                Set oCategoria4Seleccionada = Nothing
                        
            Case "CAT5"
                 
                Accion = ACCCATCATEGORIAEli
                
                Set oCategoria5Seleccionada = Nothing
                Set oCategoria5Seleccionada = oFSGSRaiz.Generar_CCategoriaN5
                oCategoria5Seleccionada.Cod = DevolverCod(nodx)
                oCategoria5Seleccionada.Id = DevolverId(nodx)
                oCategoria5Seleccionada.Cat1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
                oCategoria5Seleccionada.Cat2 = DevolverId(nodx.Parent.Parent.Parent)
                oCategoria5Seleccionada.Cat3 = DevolverId(nodx.Parent.Parent)
                oCategoria5Seleccionada.Cat4 = DevolverId(nodx.Parent)
                oCategoria5Seleccionada.Den = DevolverDen(nodx)
                oCategoria5Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                irespuesta = oMensajes.PreguntaEliminar(sIdiCategoria & ": " & CStr(oCategoria5Seleccionada.Cod) & " (" & oCategoria5Seleccionada.Den & ")")
                If irespuesta = vbNo Then Exit Sub
                
    
                bLineasCat = oCategoria5Seleccionada.ExistenLineasCatalogo
                If bLineasCat Then
                    irespuesta = oMensajes.PreguntaExistenLineasCat
                    If irespuesta = vbNo Then Exit Sub
                End If
                
                Screen.MousePointer = vbHourglass
                Set oIBaseDatos = oCategoria5Seleccionada
                teserror = oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                Else
                    RegistrarAccion ACCCATCATEGORIAEli, "CAT1:" & CStr(oCategoria5Seleccionada.Cat1) & "CAT2:" & CStr(oCategoria5Seleccionada.Cat2) & "CAT3:" & CStr(oCategoria5Seleccionada.Cat3) & "CAT4:" & CStr(oCategoria5Seleccionada.Cat4) & "CAT5:" & CStr(oCategoria5Seleccionada.Id)
                    EliminarCATDeEstructura
                End If
                Set oIBaseDatos = Nothing
                Set oCategoria5Seleccionada = Nothing
            
        End Select
    Case 1
        Set oCat = DevolverCategoria
        Set m_oProves = oFSGSRaiz.generar_CProveedores
        If sdbgProves.SelBookmarks.Count > 0 Then
            For i = 0 To sdbgProves.SelBookmarks.Count - 1
                m_oProves.Add sdbgProves.Columns(0).CellValue(sdbgProves.SelBookmarks(i)), ""
            Next
            Screen.MousePointer = vbHourglass
            Select Case iNivel
            Case 1
                teserror = m_oProves.EliminarProveedoresDeCategoria(oCat.Id)
            Case 2
                teserror = m_oProves.EliminarProveedoresDeCategoria(oCat.Cat1, oCat.Id)
            Case 3
                teserror = m_oProves.EliminarProveedoresDeCategoria(oCat.Cat1, oCat.Cat2, oCat.Id)
            Case 4
                teserror = m_oProves.EliminarProveedoresDeCategoria(oCat.Cat1, oCat.Cat2, oCat.Cat3, oCat.Id)
            Case 5
                teserror = m_oProves.EliminarProveedoresDeCategoria(oCat.Cat1, oCat.Cat2, oCat.Cat3, oCat.Cat4, oCat.Id)
            End Select
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                Set oCat = Nothing
                Exit Sub
            End If
            sdbgProves.DeleteSelected
            For Each oProve In m_oProves
                oCat.ProveedoresPedidoLibre.Remove oProve.Cod
            Next
            If sdbgProves.Rows <= 0 Then
                cmdEliCat(1).Enabled = False
            End If
        End If
        Set oCat = Nothing
        Set m_oProves = Nothing
        
    End Select
    Screen.MousePointer = vbNormal

End If
End Sub

Private Sub cmdExcel_Click(Index As Integer)
Dim appexcel  As Object
Dim docPetExcel As Object
Dim oLineaCatalogo As CLineaCatalogo
Dim oCExcelCatalogo As FSGSInformes.CExcelCatalogo
Dim sErrores As String
Dim i As Integer

Set oCExcelCatalogo = New FSGSInformes.CExcelCatalogo
Set oCExcelCatalogo.oFSGSRaiz = oFSGSRaiz

Screen.MousePointer = vbHourglass
Select Case Index
    Case 0, 2 'Exporta precios excel
        If appexcel Is Nothing Then
            Set appexcel = CreateObject("Excel.Application")
        End If
        If Index = 2 And Not tvwCategorias.selectedItem Is Nothing Then
            Set oLineasCatalogo = oFSGSRaiz.Generar_CLineasCatalogo
            CargarLineasCatalogo tvwCategorias.selectedItem, 5, gParametrosGenerales.gbOblPedidosHom And tvwCategorias.selectedItem.Children = 0, oLineaCatalogo
        End If
        If Not oLineasCatalogo Is Nothing Then
            If oLineasCatalogo.Count > 0 Then
                Set docPetExcel = oCExcelCatalogo.ExportarPreciosExcel(appexcel, oLineasCatalogo, App.Path)
                If Not docPetExcel Is Nothing Then
                    On Error Resume Next
                    cmmdAdjun.CancelError = True
                    cmmdAdjun.Filter = "Excel|*.xls"
                    cmmdAdjun.filename = ValidFilename(docPetExcel.Name)
                    cmmdAdjun.ShowSave
                    
                    If err.Number <> cdlCancel Then
                        ' The user canceled.
                       docPetExcel.SaveAs cmmdAdjun.filename
                    End If
                    
                    appexcel.DisplayAlerts = False
                    appexcel.Quit
                End If
            End If
        End If
        Set appexcel = Nothing
        
    Case 1, 3, 5  '1, 3=Importar precios excel ; 5 = Cargar Articulos Excel
        cmmdAdjun.filename = ""
        cmmdAdjun.CancelError = False
        cmmdAdjun.FLAGS = cdlOFNHideReadOnly
        cmmdAdjun.Filter = "Excel|*.xlsx|Excel 97-2003|*.xls" '"(*.xls)|*.xls|(*.xlsx)|*.xlsx"
        cmmdAdjun.FilterIndex = 0
        cmmdAdjun.ShowOpen
        If cmmdAdjun.filename <> "" Then
            If appexcel Is Nothing Then
                Set appexcel = CreateObject("Excel.Application")
            End If
            If Index = 5 Then
                appexcel.DisplayAlerts = False
                i = 0
                Set docPetExcel = oCExcelCatalogo.CargarArticulosExcel(appexcel, cmmdAdjun.filename, sErrores, cmdExcel(Index).caption, frmESPERA, i)
                If sErrores <> "" Then
                    Select Case sErrores
                        Case "1"
                            oMensajes.NoExisteArchivo cmmdAdjun.filename
                        Case "2"
                            oMensajes.ArchivoFormatoIncorrecto cmmdAdjun.filename
                        Case Else
                            oMensajes.CargaCatalogoFinalizadaErr i
                            appexcel.DisplayAlerts = True
                            appexcel.Visible = True
                            BringWindowToTop appexcel.hWnd
                    End Select
                Else
                    oMensajes.CargaCatalogoFinalizada i
                End If
            Else
                Set docPetExcel = oCExcelCatalogo.ImportarPreciosExcel(appexcel, cmmdAdjun.filename, sErrores)
            End If
            If Not docPetExcel Is Nothing Then
                appexcel.Visible = True
            End If
            
            Set appexcel = Nothing
            
            If Index = 1 Then
                CargarGridAdjudicaciones tvwCategorias.selectedItem, 5
            End If
            
            If sErrores <> "" Then
                If Index <> 5 Then
                    oMensajes.ExcelCatalogoErrores sErrores
                End If
            End If
        End If
    Case 4  'Excel Carga articulos (descargar plantilla)
        Set appexcel = CreateObject("Excel.Application")
        Set docPetExcel = oCExcelCatalogo.ExcelCargaArticulos(appexcel, App.Path, i)
        If i > 0 Then
            'No se enuentra el fichero
            oMensajes.NoExisteArchivo App.Path & "\CargaArticulosCatalogo.xlt"
            Screen.MousePointer = vbNormal
            Exit Sub
        Else
            cmmdAdjun.CancelError = True
            cmmdAdjun.Filter = "Excel|*.xlsx"
            cmmdAdjun.filename = ValidFilename("CargaArticulosCatalogo")
             
            cmmdAdjun.CancelError = False
            cmmdAdjun.ShowSave
            appexcel.DisplayAlerts = False
            If err.Number <> cdlCancel Then
                ' The user canceled.
               docPetExcel.SaveAs cmmdAdjun.filename
            End If
            'appexcel.DisplayAlerts = False
            appexcel.Quit
        End If
        Set appexcel = Nothing
End Select

Set oCExcelCatalogo.oFSGSRaiz = Nothing
Set oCExcelCatalogo = Nothing
Screen.MousePointer = vbNormal
End Sub

Private Sub cmdlistado_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

    Set nodx = tvwCategorias.selectedItem
    If Not nodx Is Nothing Then
        
        If nodx.Tag <> "Raiz" Then
            
            Select Case Left(nodx.Tag, 4)
                Case "CAT1"
                            scod1 = DevolverCod(nodx)
                            frmLstCatalogo.CATN1Seleccionada = DevolverId(nodx)
                            scod2 = ""
                            frmLstCatalogo.CATN2Seleccionada = 0
                            scod3 = ""
                            frmLstCatalogo.CATN3Seleccionada = 0
                            scod4 = ""
                            frmLstCatalogo.CATN4Seleccionada = 0
                            scod5 = ""
                            frmLstCatalogo.CATN5Seleccionada = 0
                            frmLstCatalogo.lblCategoria = nodx.Text
                            
                Case "CAT2"
                            scod1 = DevolverCod(nodx.Parent)
                            frmLstCatalogo.CATN1Seleccionada = DevolverId(nodx.Parent)
                            scod2 = DevolverCod(nodx)
                            frmLstCatalogo.CATN2Seleccionada = DevolverId(nodx)
                            scod3 = ""
                            frmLstCatalogo.CATN3Seleccionada = 0
                            scod4 = ""
                            frmLstCatalogo.CATN4Seleccionada = 0
                            scod5 = ""
                            frmLstCatalogo.CATN5Seleccionada = 0
                            frmLstCatalogo.lblCategoria = scod1 & " - " & nodx.Text
                
                Case "CAT3"
                            scod1 = DevolverCod(nodx.Parent.Parent)
                            frmLstCatalogo.CATN1Seleccionada = DevolverId(nodx.Parent.Parent)
                            scod2 = DevolverCod(nodx.Parent)
                            frmLstCatalogo.CATN2Seleccionada = DevolverId(nodx.Parent)
                            scod3 = DevolverCod(nodx)
                            frmLstCatalogo.CATN3Seleccionada = DevolverId(nodx)
                            scod4 = ""
                            frmLstCatalogo.CATN4Seleccionada = 0
                            scod5 = ""
                            frmLstCatalogo.CATN5Seleccionada = 0
                            frmLstCatalogo.lblCategoria = scod1 & " - " & scod2 & " - " & nodx.Text
                
                Case "CAT4"
                            scod1 = DevolverCod(nodx.Parent.Parent.Parent)
                            frmLstCatalogo.CATN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                            scod2 = DevolverCod(nodx.Parent.Parent)
                            frmLstCatalogo.CATN2Seleccionada = DevolverId(nodx.Parent.Parent)
                            scod3 = DevolverCod(nodx.Parent)
                            frmLstCatalogo.CATN3Seleccionada = DevolverId(nodx.Parent)
                            scod4 = DevolverCod(nodx)
                            frmLstCatalogo.CATN4Seleccionada = DevolverId(nodx)
                            scod5 = ""
                            frmLstCatalogo.CATN5Seleccionada = 0
                            frmLstCatalogo.lblCategoria = scod1 & " - " & scod2 & " - " & scod3 & " - " & nodx.Text
                
                Case "CAT5"
                            scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                            frmLstCatalogo.CATN1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                            scod2 = DevolverCod(nodx.Parent.Parent.Parent)
                            frmLstCatalogo.CATN2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                            scod3 = DevolverCod(nodx.Parent.Parent)
                            frmLstCatalogo.CATN3Seleccionada = DevolverId(nodx.Parent.Parent)
                            scod4 = DevolverCod(nodx.Parent)
                            frmLstCatalogo.CATN4Seleccionada = DevolverId(nodx.Parent)
                            scod5 = DevolverCod(nodx)
                            frmLstCatalogo.CATN5Seleccionada = DevolverId(nodx)
                            frmLstCatalogo.lblCategoria = scod1 & " - " & scod2 & " - " & scod3 & " - " & scod4 & " - " & nodx.Text
            
            End Select
        
        End If
        
    End If
    
    frmLstCatalogo.chkVerBajas.Value = chkVerBajas.Value
    'frmLstCatalogo.bRMat = bRMat
    frmLstCatalogo.Show 1

End Sub

Public Sub cmdModifCat_Click()
Dim nodx As MSComctlLib.node
Dim teserror As TipoErrorSummit
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Set nodx = tvwCategorias.selectedItem

If Not nodx Is Nothing Then
        
    Screen.MousePointer = vbHourglass
    Select Case Left(nodx.Tag, 4)
        
    Case "CAT1"
            
            Accion = ACCCatCategoriaMod
            Set oCategoria1Seleccionada = Nothing
            scod1 = DevolverCod(nodx)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            Set oCategoria1Seleccionada = oCategoriasNivel1.Item(scod1)
            
            Set oIBaseDatos = oCategoria1Seleccionada
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError = TESnoerror Then
                frmCatalogoDetalle.iCategoria = 1
                frmCatalogoDetalle.caption = sIdiModificar
                frmCatalogoDetalle.txtCod = oCategoria1Seleccionada.Cod
                frmCatalogoDetalle.txtDen = oCategoria1Seleccionada.Den
                frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT1
                Screen.MousePointer = vbNormal
                frmCatalogoDetalle.Show 1
            Else
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set frmCatalogoDetalle = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
    
    Case "CAT2"

            Accion = ACCCatCategoriaMod

            Set oCategoria2Seleccionada = Nothing
            scod1 = DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nodx)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            Set oCategoria2Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
            
            Set oIBaseDatos = oCategoria2Seleccionada
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError = TESnoerror Then
                frmCatalogoDetalle.iCategoria = 2
                frmCatalogoDetalle.caption = sIdiModificar
                frmCatalogoDetalle.txtCod = oCategoria2Seleccionada.Cod
                frmCatalogoDetalle.txtDen = oCategoria2Seleccionada.Den
                frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT2
                Screen.MousePointer = vbNormal
                frmCatalogoDetalle.Show 1
            Else
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oCategoria2Seleccionada = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

        Case "CAT3"

            Accion = ACCCatCategoriaMod

            Set oCategoria3Seleccionada = Nothing
            scod1 = DevolverCod(nodx.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nodx.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = DevolverCod(nodx)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            Set oCategoria3Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
            
            Set oIBaseDatos = oCategoria3Seleccionada
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError = TESnoerror Then
                frmCatalogoDetalle.iCategoria = 3
                frmCatalogoDetalle.caption = sIdiModificar
                frmCatalogoDetalle.txtCod = oCategoria3Seleccionada.Cod
                frmCatalogoDetalle.txtDen = oCategoria3Seleccionada.Den
                frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT3
                Screen.MousePointer = vbNormal
                frmCatalogoDetalle.Show 1
            Else
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oCategoria3Seleccionada = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If

        Case "CAT4"

            Accion = ACCCatCategoriaMod
            
            Set oCategoria4Seleccionada = Nothing
            scod1 = DevolverCod(nodx.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nodx.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = DevolverCod(nodx.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            scod4 = DevolverCod(nodx)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
            Set oCategoria4Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
            
            Set oIBaseDatos = oCategoria4Seleccionada
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError = TESnoerror Then
                frmCatalogoDetalle.iCategoria = 4
                frmCatalogoDetalle.caption = sIdiModificar
                frmCatalogoDetalle.txtCod = oCategoria4Seleccionada.Cod
                frmCatalogoDetalle.txtDen = oCategoria4Seleccionada.Den
                frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT4
                Screen.MousePointer = vbNormal
                frmCatalogoDetalle.Show 1
            Else
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oCategoria4Seleccionada = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        
        Case "CAT5"

            Accion = ACCCatCategoriaMod
            
            Set oCategoria5Seleccionada = Nothing
            scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
            scod2 = DevolverCod(nodx.Parent.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
            scod3 = DevolverCod(nodx.Parent.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
            scod4 = DevolverCod(nodx.Parent)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
            scod5 = DevolverCod(nodx)
            scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            Set oCategoria5Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5)
            Set oIBaseDatos = oCategoria5Seleccionada
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError = TESnoerror Then
                frmCatalogoDetalle.iCategoria = 5
                frmCatalogoDetalle.caption = sIdiModificar
                frmCatalogoDetalle.txtCod = oCategoria5Seleccionada.Cod
                frmCatalogoDetalle.txtDen = oCategoria5Seleccionada.Den
                frmCatalogoDetalle.txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodCAT5
                Screen.MousePointer = vbNormal
                frmCatalogoDetalle.Show 1
            Else
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oCategoria5Seleccionada = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
    
    End Select
    Screen.MousePointer = vbNormal

End If

End Sub

Private Sub cmdModifPrecios_Click()
    If sdbgAdjudicaciones.Rows = 0 Then Exit Sub
        
    If ((Not IsNull(sdbgAdjudicaciones.Bookmark)) And (Not IsEmpty(sdbgAdjudicaciones.Bookmark)) And (sdbgAdjudicaciones.SelBookmarks.Count = 0)) Then
        sdbgAdjudicaciones.SelBookmarks.Add sdbgAdjudicaciones.Bookmark
    End If
    
    If sdbgAdjudicaciones.SelBookmarks.Count = 0 Then Exit Sub
    
    frmCatalogoModificarPrecios.Show 1
    
    sdbgAdjudicaciones.SelBookmarks.RemoveAll
    Screen.MousePointer = vbNormal
    Set m_aIdentificadores = Nothing
End Sub

Private Sub cmdModoEdicion_Click()
Dim v As Variant
Dim i As Long
    
    ''' * Objetivo: Cambiar entre modo de edicion y modo de consulta
    
    If Not bModoEdicion Then
                        
        If Not bModifAdj Then
            For i = 0 To 28
                sdbgAdjudicaciones.Columns(i).Locked = True
            Next
            If bModifAdjPrecios Then
                sdbgAdjudicaciones.Columns("PrecioUB").Locked = False
            End If
        Else
            For i = 0 To 28
                'excepto CantAdj
                sdbgAdjudicaciones.Columns(i).Locked = False
            Next
            
            sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
            sdbgAdjudicaciones.Columns("SOLICIT").Locked = False
            sdbgAdjudicaciones.Columns("PROCE").Locked = True
            sdbgAdjudicaciones.Columns("ART").Locked = True
            If sdbgAdjudicaciones.Columns("PROCE").Value = "" Then
                If GridCheckToBoolean(sdbgAdjudicaciones.Columns("GENERICO").Value) Then
                    sdbgAdjudicaciones.Columns("ARTDEN").Locked = False
                Else
                    sdbgAdjudicaciones.Columns("ARTDEN").Locked = True
                End If
            Else
                sdbgAdjudicaciones.Columns("ARTDEN").Locked = True
            End If
            
            'Enganchar las dropdown
            sdbgAdjudicaciones.Columns("DEST").DropDownHwnd = sdbddDestinos.hWnd
            sdbgAdjudicaciones.Columns("UB").DropDownHwnd = sdbddUnidades.hWnd
            sdbgAdjudicaciones.Columns("UP").DropDownHwnd = sdbddUnidadesPedido.hWnd
            sdbgAdjudicaciones.Columns("MON").DropDownHwnd = sdbddMonedas.hWnd
            If Not bModifAdjPrecios Then
                sdbgAdjudicaciones.Columns("PrecioUB").Locked = True
            End If
        End If
        
        If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
            sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
        Else
            sdbgAdjudicaciones.Columns("SOLICIT").Locked = False
        End If
        
        
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
        
        cmdModoEdicion.caption = sIdiModos(0) 'Consulta
        
        cmdBuscarAdj.Visible = False
        cmdDeshacer.Visible = True
        
        bModoEdicion = True
        
        Accion = ACCCatAdjudCon
        
    Else
                
        If sdbgAdjudicaciones.DataChanged Or bValError Or bAnyaError Or bModError Then
        
            v = sdbgAdjudicaciones.ActiveCell.Value
            If Me.Visible Then sdbgAdjudicaciones.SetFocus
            sdbgAdjudicaciones.ActiveCell.Value = v
            
            bValError = False
            bAnyaError = False
            bModError = False
            
            sdbgAdjudicaciones.Update
            
            If bValError Or bAnyaError Or bModError Then
                Exit Sub
            End If
            
        End If
        
        If Not oLineasAAnyadir Is Nothing Then
            If oLineasAAnyadir.Count > 0 Then
                Exit Sub
            End If
        End If
        
        For i = 1 To 28
            sdbgAdjudicaciones.Columns(i).Locked = True
        Next
        sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
               
        If Not bModifAdj Then
            sdbgAdjudicaciones.Columns("PUB").Locked = True
        End If
        
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEdit
        'En modo consulta soltamos los dropdowm
        sdbgAdjudicaciones.Columns("DEST").DropDownHwnd = 0
        sdbgAdjudicaciones.Columns("UB").DropDownHwnd = 0
        sdbgAdjudicaciones.Columns("UP").DropDownHwnd = 0
        sdbgAdjudicaciones.Columns("MON").DropDownHwnd = 0
        cmdBuscarAdj.Visible = True
        cmdBuscarAdj.Enabled = True
        cmdDeshacer.Visible = False

        cmdModoEdicion.caption = sIdiModos(1) 'Edici�n
        
        bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgAdjudicaciones.SetFocus

End Sub

Private Sub MostrarDetalleProveedor(ByVal CodProve As String)
Dim oProves As CProveedores
Dim oProve As CProveedor
Dim oCon As CContacto
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    
   oProves.CargarTodosLosProveedoresDesde3 1, CodProve, , True
    
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado sIdiProve
        Set oProves = Nothing
        Exit Sub
    End If
        
    Set oProve = oProves.Item(1)
    'Cargamos los datos del proveedor
    oProves.CargarDatosProveedor CodProve
    
    Set frmProveDetalle.g_oProveSeleccionado = oProve
    frmProveDetalle.caption = CodProve & "  " & oProve.Den
    frmProveDetalle.lblCodPortProve = NullToStr(oProve.CodPortal)
    frmProveDetalle.g_bPremium = oProve.EsPremium
    frmProveDetalle.g_bActivo = oProve.EsPremiumActivo
    frmProveDetalle.lblDir = NullToStr(oProve.Direccion)
    frmProveDetalle.lblCp = NullToStr(oProve.cP)
    frmProveDetalle.lblPob = NullToStr(oProve.Poblacion)
    frmProveDetalle.lblPaiCod = NullToStr(oProve.CodPais)
    frmProveDetalle.lblPaiDen = NullToStr(oProve.DenPais)
    frmProveDetalle.lblMonCod = NullToStr(oProve.CodMon)
    frmProveDetalle.lblMonDen = NullToStr(oProve.DenMon)
    frmProveDetalle.lblProviCod = NullToStr(oProve.CodProvi)
    frmProveDetalle.lblProviDen = NullToStr(oProve.DenProvi)
    frmProveDetalle.lblcalif1 = gParametrosGenerales.gsDEN_CAL1 & ":"
    frmProveDetalle.lblCalif2 = gParametrosGenerales.gsDEN_CAL2 & ":"
    frmProveDetalle.lblcalif3 = gParametrosGenerales.gsDEN_CAL3 & ":"
    
    If Trim(oProve.Calif1) <> "" Then
        frmProveDetalle.lblCal1Den = oProve.Calif1
    End If
    
    If Trim(oProve.Calif2) <> "" Then
        frmProveDetalle.lblcal2den = oProve.Calif2
    End If
    
    If Trim(oProve.Calif3) <> "" Then
        frmProveDetalle.lblcal3den = oProve.Calif3
    End If
    
    frmProveDetalle.lblCal1Val = DblToStr(oProve.Val1)
    frmProveDetalle.lblCal2Val = DblToStr(oProve.Val2)
    frmProveDetalle.lblCal3Val = DblToStr(oProve.Val3)

    frmProveDetalle.lblURL = NullToStr(oProve.URLPROVE)

    oProve.CargarTodosLosContactos , , , , True, , , , , , , True
    
    For Each oCon In oProve.Contactos
        
        frmProveDetalle.sdbgContactos.AddItem oCon.Apellidos & Chr(m_lSeparador) & oCon.nombre & Chr(m_lSeparador) & oCon.Departamento & Chr(m_lSeparador) & oCon.Cargo _
        & Chr(m_lSeparador) & oCon.Tfno & Chr(m_lSeparador) & oCon.Fax & Chr(m_lSeparador) & oCon.tfnomovil & Chr(m_lSeparador) & oCon.mail & Chr(m_lSeparador) & oCon.Def & Chr(m_lSeparador) & oCon.Port & Chr(m_lSeparador)
    
    Next
        
    Set oProves = Nothing
    Set oProve = Nothing
    
    
    frmProveDetalle.Show 1
    
End Sub

Private Sub MostrarDetalleItem()
Dim arResultados() As Variant
Dim iPosImg As Integer
Dim sMON As String
Dim sCant As String
Dim sPrec As String
Dim bAtribs As Boolean

    'Al redireccionar a frmItemAtribDetalle saltar lo de oPedidos con un sorigen
    
    If oLineaCatalogoSeleccionada Is Nothing Then Exit Sub
    
    arResultados = oLineaCatalogoSeleccionada.DevolverDatosDeItem(sMON, sCant, sPrec)
    
    'Cogemos la posici�n de la im�gen dependiendo del tama�o del array
    Select Case UBound(arResultados)
        
        Case 0:
                Exit Sub
        Case 30:
                iPosImg = 30
        Case 47:
                iPosImg = 47
        Case 17:
                iPosImg = 17
    End Select
    
    'La pantalla de detalle tiene que salir si
    '�   la l�nea de Catalogo viene de un �tem de proceso de compra O
    '�   el art�culo de la l�nea tiene imagen O
    '�   el art�culo de la l�nea tiene atributos
    'Si no hay ninguna de esas tres cosas sale un mensaje de que el art�culo no tiene datos, el mensaje actual.
    
    If ((Not IsNull(oLineaCatalogoSeleccionada.Anyo)) And (Not IsNull(oLineaCatalogoSeleccionada.ProceCod)) And (Not IsNull(oLineaCatalogoSeleccionada.Item))) Then
        'S�: la l�nea de Catalogo viene de un �tem de proceso de compra O
    ElseIf Not (((arResultados(iPosImg) = 0) Or IsEmpty(arResultados(iPosImg)))) Then
        'S�: el art�culo de la l�nea tiene imagen O
    ElseIf Not IsEmpty(arResultados(0)) Then
        'S�: el art�culo de la l�nea tiene atributos
    Else
        oMensajes.NoExistenValoresAtributosDeArticuloProve oLineaCatalogoSeleccionada.ArtCod_Interno, oLineaCatalogoSeleccionada.ProveCod
        Exit Sub
    End If
    
    frmItemAtribDetalle.CodProve = sdbgAdjudicaciones.Columns("PROVE").Value
    If oLineaCatalogoSeleccionada.ArtCod_Interno <> "" Then
        If sdbgAdjudicaciones.Columns("ARTCOD").Value <> "" Then
            frmItemAtribDetalle.CodArt = sdbgAdjudicaciones.Columns("ARTCOD").Value
        Else
            frmItemAtribDetalle.CodArt = arResultados(0)
        End If
    End If
    
    frmItemAtribDetalle.g_sGmn1 = sdbgAdjudicaciones.Columns("GMN1").Value
    
    
    'Si la l�nea proviene de una adjudicaci�n
    If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
        If IsNull(arResultados(0)) Then
            frmItemAtribDetalle.sOrigen = "PEQUENYA"
            frmItemAtribDetalle.BorderStyle = 1
        Else
            If IsEmpty(arResultados(18)) And IsEmpty(arResultados(47)) Then
                frmItemAtribDetalle.sOrigen = "PEQUENYA"
                frmItemAtribDetalle.BorderStyle = 1
            Else
                frmItemAtribDetalle.sOrigen = "GRANDE"
            End If
        End If
        
        frmItemAtribDetalle.caption = sdbgAdjudicaciones.Columns("ART").Value
        frmItemAtribDetalle.lblCodDest = arResultados(5)
        frmItemAtribDetalle.lblDenDest = arResultados(11)
        frmItemAtribDetalle.lblCodUni = arResultados(6)
        frmItemAtribDetalle.lblDenUni = arResultados(12)
        frmItemAtribDetalle.lblCodPag = arResultados(7)
        frmItemAtribDetalle.lblDenPag = arResultados(13)
        frmItemAtribDetalle.lblFecIni = arResultados(8)
        frmItemAtribDetalle.lblFecFin = arResultados(9)
        frmItemAtribDetalle.lblEsp.Text = NullToStr(arResultados(10))
        frmItemAtribDetalle.lblProceso = arResultados(14) & "/" & arResultados(17) & "/" & arResultados(15) & " " & arResultados(16)
        frmItemAtribDetalle.lblCantAdj = sCant
        frmItemAtribDetalle.lblPrecioAdjud = sPrec
        frmItemAtribDetalle.lblMoneda = sMON
        
        bAtribs = False
        
        'El �tem no consta de un art�culo codificado
        If IsNull(arResultados(0)) Then
            frmItemAtribDetalle.Height = 2865
            frmItemAtribDetalle.Top = 2500
            frmItemAtribDetalle.Left = 2500
        Else
            If IsEmpty(arResultados(18)) And IsEmpty(arResultados(47)) Then
            'Es un art�culo codificado pero sus atributos no tiene valores
                frmItemAtribDetalle.Height = 2865
                frmItemAtribDetalle.Top = 2500
                frmItemAtribDetalle.Left = 2500
            Else
                If Not IsEmpty(arResultados(18)) Then
                    bAtribs = True
                    
                    frmItemAtribDetalle.Label1 = NullToStr(arResultados(18))
                    frmItemAtribDetalle.Label1.ToolTipText = NullToStr(arResultados(19))
                    frmItemAtribDetalle.Label11 = NullToStr(arResultados(20))
                    frmItemAtribDetalle.Label1.Visible = True
                    frmItemAtribDetalle.Label11.Visible = True
                
                    If Not IsEmpty(arResultados(21)) Then
                        frmItemAtribDetalle.label2 = NullToStr(arResultados(21))
                        frmItemAtribDetalle.label2.ToolTipText = NullToStr(arResultados(22))
                        frmItemAtribDetalle.Labe12 = NullToStr(arResultados(23))
                        frmItemAtribDetalle.label2.Visible = True
                        frmItemAtribDetalle.Labe12.Visible = True
                    
                        If Not IsEmpty(arResultados(24)) Then
                            frmItemAtribDetalle.Label3 = NullToStr(arResultados(24))
                            frmItemAtribDetalle.Label3.ToolTipText = NullToStr(arResultados(25))
                            frmItemAtribDetalle.Label13 = NullToStr(arResultados(26))
                            frmItemAtribDetalle.Label3.Visible = True
                            frmItemAtribDetalle.Label13.Visible = True
                        
                            If Not IsEmpty(arResultados(27)) Then
                                frmItemAtribDetalle.Label4 = NullToStr(arResultados(27))
                                frmItemAtribDetalle.Label4.ToolTipText = NullToStr(arResultados(28))
                                frmItemAtribDetalle.Label14 = NullToStr(arResultados(29))
                                frmItemAtribDetalle.Label4.Visible = True
                                frmItemAtribDetalle.Label14.Visible = True
                        
                                If Not IsEmpty(arResultados(30)) Then
                                    frmItemAtribDetalle.Label5 = NullToStr(arResultados(30))
                                    frmItemAtribDetalle.Label5.ToolTipText = NullToStr(arResultados(31))
                                    frmItemAtribDetalle.Label15 = NullToStr(arResultados(32))
                                    frmItemAtribDetalle.Label5.Visible = True
                                    frmItemAtribDetalle.Label15.Visible = True
                            
                                    If Not IsEmpty(arResultados(33)) Then
                                        frmItemAtribDetalle.Label6 = NullToStr(arResultados(33))
                                        frmItemAtribDetalle.Label6.ToolTipText = NullToStr(arResultados(34))
                                        frmItemAtribDetalle.Label16 = NullToStr(arResultados(35))
                                        frmItemAtribDetalle.Label6.Visible = True
                                        frmItemAtribDetalle.Label16.Visible = True
                                    
                                        If Not IsEmpty(arResultados(36)) Then
                                            frmItemAtribDetalle.Label7 = NullToStr(arResultados(36))
                                            frmItemAtribDetalle.Label7.ToolTipText = NullToStr(arResultados(37))
                                            frmItemAtribDetalle.Label17 = NullToStr(arResultados(38))
                                            frmItemAtribDetalle.Label7.Visible = True
                                            frmItemAtribDetalle.Label17.Visible = True
                                        
                                            If Not IsEmpty(arResultados(39)) Then
                                                frmItemAtribDetalle.Label8 = NullToStr(arResultados(39))
                                                frmItemAtribDetalle.Label8.ToolTipText = NullToStr(arResultados(40))
                                                frmItemAtribDetalle.Label18 = NullToStr(arResultados(41))
                                                frmItemAtribDetalle.Label8.Visible = True
                                                frmItemAtribDetalle.Label18.Visible = True
                                            
                                                If Not IsEmpty(arResultados(42)) Then
                                                    frmItemAtribDetalle.Label9 = NullToStr(arResultados(42))
                                                    frmItemAtribDetalle.Label9.ToolTipText = NullToStr(arResultados(43))
                                                    frmItemAtribDetalle.Label19 = NullToStr(arResultados(44))
                                                    frmItemAtribDetalle.Label9.Visible = True
                                                    frmItemAtribDetalle.Label19.Visible = True
                                                
                                                    If Not IsEmpty(arResultados(45)) Then
                                                        frmItemAtribDetalle.label10 = NullToStr(arResultados(45))
                                                        frmItemAtribDetalle.label10.ToolTipText = NullToStr(arResultados(46))
                                                        frmItemAtribDetalle.Label20 = NullToStr(arResultados(47))
                                                        frmItemAtribDetalle.label10.Visible = True
                                                        frmItemAtribDetalle.Label20.Visible = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                
                frmItemAtribDetalle.Top = 1500
                frmItemAtribDetalle.Left = 2500
            End If
        End If
    
        If IsNull(arResultados(iPosImg)) Or (arResultados(iPosImg) = 0) Or IsEmpty(arResultados(iPosImg)) Then
            frmItemAtribDetalle.bImagen = False
        Else
            frmItemAtribDetalle.bImagen = True
        End If
    
    Else
        'La l�nea de cat�logo no proviene de una adjudicaci�n o FSEPConf=True
        frmItemAtribDetalle.sOrigen = "GRANDE"
        frmItemAtribDetalle.caption = sdbgAdjudicaciones.Columns("ART").Value
        If frmItemAtribDetalle.g_bEliminado = True Then
            Unload frmItemAtribDetalle
            Exit Sub
        End If
        bAtribs = False
        If Not IsEmpty(arResultados(0)) Then
            bAtribs = True
            
            frmItemAtribDetalle.Label1 = NullToStr(arResultados(0))
            frmItemAtribDetalle.Label1.ToolTipText = NullToStr(arResultados(1))
            frmItemAtribDetalle.Label11 = NullToStr(arResultados(2))
            frmItemAtribDetalle.Label1.Visible = True
            frmItemAtribDetalle.Label11.Visible = True
        
            If Not IsEmpty(arResultados(3)) Then
                frmItemAtribDetalle.label2 = NullToStr(arResultados(3))
                frmItemAtribDetalle.label2.ToolTipText = NullToStr(arResultados(4))
                frmItemAtribDetalle.Labe12 = NullToStr(arResultados(5))
                frmItemAtribDetalle.label2.Visible = True
                frmItemAtribDetalle.Labe12.Visible = True
        
                If Not IsEmpty(arResultados(6)) Then
                    frmItemAtribDetalle.Label3 = NullToStr(arResultados(6))
                    frmItemAtribDetalle.Label3.ToolTipText = NullToStr(arResultados(7))
                    frmItemAtribDetalle.Label13 = NullToStr(arResultados(8))
                    frmItemAtribDetalle.Label3.Visible = True
                    frmItemAtribDetalle.Label13.Visible = True
                    
                    If Not IsEmpty(arResultados(9)) Then
                        frmItemAtribDetalle.Label4 = NullToStr(arResultados(9))
                        frmItemAtribDetalle.Label4.ToolTipText = NullToStr(arResultados(10))
                        frmItemAtribDetalle.Label14 = NullToStr(arResultados(11))
                        frmItemAtribDetalle.Label4.Visible = True
                        frmItemAtribDetalle.Label14.Visible = True
                        
                        If Not IsEmpty(arResultados(12)) Then
                            frmItemAtribDetalle.Label5 = NullToStr(arResultados(12))
                            frmItemAtribDetalle.Label5.ToolTipText = NullToStr(arResultados(13))
                            frmItemAtribDetalle.Label15 = NullToStr(arResultados(14))
                            frmItemAtribDetalle.Label5.Visible = True
                            frmItemAtribDetalle.Label15.Visible = True
                            
                            If Not IsEmpty(arResultados(15)) Then
                                frmItemAtribDetalle.Label6 = NullToStr(arResultados(15))
                                frmItemAtribDetalle.Label6.ToolTipText = NullToStr(arResultados(16))
                                frmItemAtribDetalle.Label16 = NullToStr(arResultados(17))
                                frmItemAtribDetalle.Label6.Visible = True
                                frmItemAtribDetalle.Label16.Visible = True
                                
                                If Not IsEmpty(arResultados(18)) Then
                                    frmItemAtribDetalle.Label7 = NullToStr(arResultados(18))
                                    frmItemAtribDetalle.Label7.ToolTipText = NullToStr(arResultados(19))
                                    frmItemAtribDetalle.Label17 = NullToStr(arResultados(20))
                                    frmItemAtribDetalle.Label7.Visible = True
                                    frmItemAtribDetalle.Label17.Visible = True
                                    
                                    If Not IsEmpty(arResultados(21)) Then
                                        frmItemAtribDetalle.Label8 = NullToStr(arResultados(21))
                                        frmItemAtribDetalle.Label8.ToolTipText = NullToStr(arResultados(22))
                                        frmItemAtribDetalle.Label18 = NullToStr(arResultados(23))
                                        frmItemAtribDetalle.Label8.Visible = True
                                        frmItemAtribDetalle.Label18.Visible = True
                                        
                                        If Not IsEmpty(arResultados(24)) Then
                                            frmItemAtribDetalle.Label9 = NullToStr(arResultados(24))
                                            frmItemAtribDetalle.Label9.ToolTipText = NullToStr(arResultados(25))
                                            frmItemAtribDetalle.Label19 = NullToStr(arResultados(26))
                                            frmItemAtribDetalle.Label9.Visible = True
                                            frmItemAtribDetalle.Label19.Visible = True
                                            
                                            If Not IsEmpty(arResultados(27)) Then
                                                frmItemAtribDetalle.label10 = NullToStr(arResultados(27))
                                                frmItemAtribDetalle.label10.ToolTipText = NullToStr(arResultados(28))
                                                frmItemAtribDetalle.Label20 = NullToStr(arResultados(29))
                                                frmItemAtribDetalle.label10.Visible = True
                                                frmItemAtribDetalle.Label20.Visible = True
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
        
        If IsNull(arResultados(iPosImg)) Or (arResultados(iPosImg) = 0) Or IsEmpty(arResultados(iPosImg)) Then
            frmItemAtribDetalle.bImagen = False
        Else
            frmItemAtribDetalle.bImagen = True
        End If
            
        frmItemAtribDetalle.Frame1.Visible = False
        frmItemAtribDetalle.Frame2.Top = frmItemAtribDetalle.Frame1.Top
        frmItemAtribDetalle.Frame3.Top = frmItemAtribDetalle.Frame1.Top
        frmItemAtribDetalle.Height = 4600
        frmItemAtribDetalle.Top = 2000
        frmItemAtribDetalle.Left = 2500
    End If
    
    frmItemAtribDetalle.bAtribs = bAtribs
    
    frmItemAtribDetalle.ssdbArticulosAgregados.Visible = False
    frmItemAtribDetalle.Show 1
    
End Sub


Public Sub MostrarDetalleProceso(ByVal Anyo As Integer, ByVal GMN1 As String, ByVal ProceCod As Long)
        
    bConsultProce = True
    frmPROCE.g_sOrigen = "frmCatalogo"
    frmPROCE.sdbcAnyo.Value = Anyo
    frmPROCE.sdbcGMN1_4Cod.Value = GMN1
    frmPROCE.GMN1Seleccionado
    frmPROCE.ProceSelector1.Seleccion = PSSeleccion.PSTodos
    frmPROCE.sdbcProceCod.Value = ProceCod
    frmPROCE.sdbcProceCod_Validate False
    If Not bConsultProce Then
        Unload frmPROCE
    End If
End Sub
Private Sub cmdResCat_Click()
    Screen.MousePointer = vbHourglass
    'Si el usuario es de aprovisionamiento y tiene restricci�n
txtObsAdjun.Text = ""
lstvwAdjun.ListItems.clear
sdbgProves.RemoveAll

Set m_oCampos = Nothing
Set m_oCamposR = Nothing

txtObsAdjun.Locked = True
cmdAnyaCat(1).Enabled = False
cmdEliCat(1).Enabled = False
cmdA�adirAdjun.Enabled = False
cmdEliminarAdjun.Enabled = False
cmdModificarAdjun.Enabled = False
    
    If bRUsuAprov Then
        GenerarEstructuraCategoriasAprovisionamiento chkVerBajas.Value
    Else
        GenerarEstructuraCategorias chkVerBajas.Value
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Activate()
    If g_bCargarAdjudicacionesNodoSeleccionado Then
        cargarAdjudicacionesNodoSeleccionado
        Set g_oNodx = Nothing
        g_bCargarAdjudicacionesNodoSeleccionado = False
        g_sOrigen = ""
        
    End If
    PosicionarBotones
End Sub

Private Sub Form_Load()
Dim sCaption As String
Dim i As Long

    Me.Width = 11700
    Me.Height = 7965
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Arrange
    
    CargarRecursos
    
    lblLeyenda.Backcolor = RGB(255, 128, 128)
    
    sCaption = Me.caption
    tabCatalogo.TabVisible(1) = False
    ConfigurarSeguridad
        
    ' "Cargando estructura de categorias"
    Me.caption = sIdiTitulo(1)

    DoEvents
    
    PonerFieldSeparator Me
    
    sdbgAdjudicaciones.Columns("PROVE").FieldLen = gLongitudesDeCodigos.giLongCodPROVE
    sdbgAdjudicaciones.Columns("DEST").FieldLen = gLongitudesDeCodigos.giLongCodDEST
    sdbgAdjudicaciones.Columns("UB").FieldLen = gLongitudesDeCodigos.giLongCodUNI
    sdbgAdjudicaciones.Columns("UP").FieldLen = gLongitudesDeCodigos.giLongCodUNI
     
    'Inicializar el array de ficheros
    ReDim sayFileNames(0)

    'Si el usuario es de aprovisionamiento y tiene restricci�n
    If bRUsuAprov Then
        GenerarEstructuraCategoriasAprovisionamiento False
    Else
    'Si no
        GenerarEstructuraCategorias False
    End If
    
    If FSEPConf Then
        ConfigurarAdjudicacionesFSEPConf
    End If

    sdbgAdjudicaciones.SplitterPos = 4
    
    Set oLineasCatalogo = oFSGSRaiz.Generar_CLineasCatalogo
    
    'Cargamos las unidades de pedido
    Set oLineaCatalogoSeleccionada = oFSGSRaiz.Generar_CLineaCatalogo
    Screen.MousePointer = vbHourglass
    oLineaCatalogoSeleccionada.CargarUnidadesDePedido True
    CargarGridConUnidadesPedido
        
    'Cargamos las unidades
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    Screen.MousePointer = vbHourglass
    oUnidades.CargarTodasLasUnidades , , , , False
    CargarGridConUnidades

    'Cargamos los destinos
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    oDestinos.CargarTodosLosDestinos , , , , , , , , , , True
 
    CargarGridConDestinos
    
    'Cargamos las monedas
    'modificado 22-2-2006
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    oMonedas.CargarTodasLasMonedas , , , , , , , , , False
    CargarGridConMonedas
    
    
    If bModifCat Then
        ConfigurarInterfazSeguridad tvwCategorias.Nodes.Item("Raiz")
    End If
    
    Me.caption = sCaption

    bModoEdicion = False
    
    picNavigateAdj.Enabled = False
    picNavigateCat.Enabled = True
    
    Set oLineaCatalogoEnEdicion = oFSGSRaiz.Generar_CLineaCatalogo
    
    Set m_oIdiomas = oGestorParametros.DevolverIdiomas
    
    i = 0
    
    m_bModoEdicion = False
    
    tabDatos.TabVisible(1) = False
    tabDatos.TabVisible(2) = False
    tabDatos.TabVisible(3) = False
    tabDatos.TabVisible(4) = False
    tabDatos.TabVisible(5) = False
    
    'Campos de pedido: Cargar combo �mbito
    sdbgCamposPedido.Columns("AMB").DropDownHwnd = sdbddAmbito.hWnd
    CargarGridAmbito sdbddAmbito
    
    sdbgCamposRecepcion.Columns("AMB").DropDownHwnd = sdbddAmbitoR.hWnd
    CargarGridAmbito sdbddAmbitoR
    
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub Form_Resize()
    Arrange
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
    sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim v As Variant
    Dim i As Long
    Dim FOSFile As Scripting.FileSystemObject
    Dim bBorrando As Boolean
    
    If sdbgAdjudicaciones.DataChanged = True And tabCatalogo.TabVisible(1) Then
        
        v = sdbgAdjudicaciones.ActiveCell.Value
        tabCatalogo.Tab = 1
        If Me.Visible Then sdbgAdjudicaciones.SetFocus
        sdbgAdjudicaciones.ActiveCell.Value = v
        
        sdbgAdjudicaciones.Update
            
        bValError = False
        bAnyaError = False
        bModError = False
        
        If bValError Or bAnyaError Or bModError Then
            Cancel = True
        End If
        
    End If
    
    m_bCargarComboDesdeDD = False
    
    Unload frmCATSeguridad
    Unload frmDetallePrecios
    
    If Not g_ofrmDetalleSolic Is Nothing Then
        Unload g_ofrmDetalleSolic
        Set g_ofrmDetalleSolic = Nothing
    End If
    
    Set oLineasCatalogo = Nothing
    Set oCategoria1Seleccionada = Nothing
    Set oCategoria2Seleccionada = Nothing
    Set oCategoria3Seleccionada = Nothing
    Set oCategoria4Seleccionada = Nothing
    Set oCategoria5Seleccionada = Nothing
    Set oIBaseDatos = Nothing
    Set oUnidades = Nothing
    Set oDestinos = Nothing
    Set oMonedas = Nothing
    Set oLineasAAnyadir = Nothing
    Set oLineaCatalogoSeleccionada = Nothing
    Set oLineaCatalogoEnEdicion = Nothing
    
    bModoEdicion = False
On Error GoTo Error
    Set FOSFile = New Scripting.FileSystemObject
    
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    Me.Visible = False
    Exit Sub

Error:
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
End Sub

Private Sub lstvwAdjun_DblClick()
    cmdAbrirAdjun_Click
End Sub

Private Sub m_oFrmAnyaAdj_OnAnyaAdjSeleccionadas(oProceso As cProceso, oLineasNuevas As FSGSServer.CLineasCatalogo)
    Dim oLineaNueva As CLineaCatalogo
    Dim teserror As TipoErrorSummit
    Dim sLineaError As String
    Dim str_linea As String 'A�adida para ver mejor la linea Catalogo q se a�ade al grid
    Dim oIBaseDatos As IBaseDatos
    
    Screen.MousePointer = vbHourglass
    
    With sdbgAdjudicaciones
        .AllowAddNew = True
        
        For Each oLineaNueva In oLineasNuevas
            If oLineaCatalogoSeleccionada Is Nothing Then
                Dim oSeguridad As CSeguridad
                Set oSeguridad = oFSGSRaiz.Generar_CSeguridad
                
                oLineaNueva.Cat1 = CAT1Seleccionada
                oLineaNueva.Cat2 = CAT2Seleccionada
                oLineaNueva.Cat3 = CAT3Seleccionada
                oLineaNueva.Cat4 = CAT4Seleccionada
                oLineaNueva.Cat5 = CAT5Seleccionada
                If SeguridadCat = 1 Then
                    If CAT5Seleccionada <> "" And Not IsEmpty(CAT5Seleccionada) And Not IsNull(CAT5Seleccionada) Then
                        oSeguridad.Categoria = CAT5Seleccionada
                        oSeguridad.NivelCat = 5
                        oSeguridad.CargarDatosSeguridad
                        If oSeguridad.SeguridadConfigurada Then
                            oLineaNueva.SeguridadCat = CAT5Seleccionada
                            oLineaNueva.SeguridadNivel = 5
                        Else
                            oSeguridad.Categoria = CAT4Seleccionada
                            oSeguridad.NivelCat = 4
                            oSeguridad.CargarDatosSeguridad
                            If oSeguridad.SeguridadConfigurada Then
                                oLineaNueva.SeguridadCat = CAT4Seleccionada
                                oLineaNueva.SeguridadNivel = 4
                            Else
                                oSeguridad.Categoria = CAT3Seleccionada
                                oSeguridad.NivelCat = 3
                                oSeguridad.CargarDatosSeguridad
                                If oSeguridad.SeguridadConfigurada Then
                                    oLineaNueva.SeguridadCat = CAT3Seleccionada
                                    oLineaNueva.SeguridadNivel = 3
                                Else
                                    oSeguridad.Categoria = CAT3Seleccionada
                                    oSeguridad.NivelCat = 2
                                    oSeguridad.CargarDatosSeguridad
                                    If oSeguridad.SeguridadConfigurada Then
                                        oLineaNueva.SeguridadCat = CAT2Seleccionada
                                        oLineaNueva.SeguridadNivel = 2
                                    Else
                                        oLineaNueva.SeguridadCat = CAT1Seleccionada
                                        oLineaNueva.SeguridadNivel = 1
                                    End If
                                End If
                            End If
                        End If
                    Else
                        If CAT4Seleccionada <> "" And Not IsEmpty(CAT4Seleccionada) And Not IsNull(CAT4Seleccionada) Then
                            oSeguridad.Categoria = CAT4Seleccionada
                            oSeguridad.NivelCat = 4
                            oSeguridad.CargarDatosSeguridad
                            If oSeguridad.SeguridadConfigurada Then
                                oLineaNueva.SeguridadCat = CAT4Seleccionada
                                oLineaNueva.SeguridadNivel = 4
                            Else
                                oSeguridad.Categoria = CAT3Seleccionada
                                oSeguridad.NivelCat = 3
                                oSeguridad.CargarDatosSeguridad
                                If oSeguridad.SeguridadConfigurada Then
                                    oLineaNueva.SeguridadCat = CAT3Seleccionada
                                    oLineaNueva.SeguridadNivel = 3
                                Else
                                    oSeguridad.Categoria = CAT2Seleccionada
                                    oSeguridad.NivelCat = 2
                                    oSeguridad.CargarDatosSeguridad
                                    If oSeguridad.SeguridadConfigurada Then
                                        oLineaNueva.SeguridadCat = CAT2Seleccionada
                                        oLineaNueva.SeguridadNivel = 2
                                    Else
                                        oLineaNueva.SeguridadCat = CAT1Seleccionada
                                        oLineaNueva.SeguridadNivel = 1
                                    End If
                                End If
                            End If
                        Else
                            If CAT3Seleccionada <> "" And Not IsEmpty(CAT3Seleccionada) And Not IsNull(CAT3Seleccionada) Then
                                oSeguridad.Categoria = CAT3Seleccionada
                                oSeguridad.NivelCat = 3
                                oSeguridad.CargarDatosSeguridad
                                If oSeguridad.SeguridadConfigurada Then
                                    oLineaNueva.SeguridadCat = CAT3Seleccionada
                                    oLineaNueva.SeguridadNivel = 3
                                Else
                                    oSeguridad.Categoria = CAT2Seleccionada
                                    oSeguridad.NivelCat = 2
                                    oSeguridad.CargarDatosSeguridad
                                    If oSeguridad.SeguridadConfigurada Then
                                        oLineaNueva.SeguridadCat = CAT2Seleccionada
                                        oLineaNueva.SeguridadNivel = 2
                                    Else
                                        oLineaNueva.SeguridadCat = CAT1Seleccionada
                                        oLineaNueva.SeguridadNivel = 1
                                    End If
                                End If
                            Else
                                If CAT2Seleccionada <> "" And Not IsEmpty(CAT2Seleccionada) And Not IsNull(CAT2Seleccionada) Then
                                    oSeguridad.Categoria = frmCatalogo.CAT2Seleccionada
                                    oSeguridad.NivelCat = 2
                                    oSeguridad.CargarDatosSeguridad
                                    If oSeguridad.SeguridadConfigurada Then
                                        oLineaNueva.SeguridadCat = CAT2Seleccionada
                                        oLineaNueva.SeguridadNivel = 2
                                    Else
                                        oLineaNueva.SeguridadCat = CAT1Seleccionada
                                        oLineaNueva.SeguridadNivel = 1
                                    End If
                                Else
                                    If CAT1Seleccionada <> "" And Not IsEmpty(CAT1Seleccionada) And Not IsNull(CAT1Seleccionada) Then
                                        oLineaNueva.SeguridadCat = CAT1Seleccionada
                                        oLineaNueva.SeguridadNivel = 1
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                oLineaNueva.Cat1 = oLineaCatalogoSeleccionada.Cat1
                oLineaNueva.Cat2 = oLineaCatalogoSeleccionada.Cat2
                oLineaNueva.Cat3 = oLineaCatalogoSeleccionada.Cat3
                oLineaNueva.Cat4 = oLineaCatalogoSeleccionada.Cat4
                oLineaNueva.Cat5 = oLineaCatalogoSeleccionada.Cat5
                oLineaNueva.SeguridadCat = oLineaCatalogoSeleccionada.SeguridadCat
                oLineaNueva.SeguridadNivel = oLineaCatalogoSeleccionada.SeguridadNivel
            End If
                                
            Set oIBaseDatos = oLineaNueva
            teserror = oIBaseDatos.AnyadirABaseDatos
            Set oIBaseDatos = Nothing
            If teserror.NumError <> TESnoerror And teserror.NumError <> TESImposibleAccederAlArchivo Then
                .AllowAddNew = False
                Set oLineaNueva = Nothing
                Screen.MousePointer = vbNormal
                TratarError teserror
                Exit Sub
            Else
                'si el Arg1 es un array son los archivos que no se han podido copiar
                If teserror.NumError = TESImposibleAccederAlArchivo And IsArray(teserror.Arg1) Then
                    sLineaError = sdbgAdjudicaciones.Columns("PROVE").caption & ": " & oLineaNueva.ProveCod & "; " & sdbgAdjudicaciones.Columns("ART").caption & " " & oLineaNueva.ArtCod_Interno & " - " & oLineaNueva.ArtDen
                    oMensajes.ImposibleAccederAFichero teserror.Arg1, sLineaError
                End If
                oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, AccionesSummit.ACCCatAdjudAnya, "A�o:" & oLineaNueva.Anyo & " Proce:" & oLineaNueva.ProceCod & " Gmn1:" & oLineaNueva.GMN1Cod & " Art�culo:" & oLineaNueva.ArtCod_Interno & " Proveedor:" & oLineaNueva.ProveCod & " Destino:" & oLineaNueva.Destino & " Cantidad:" & oLineaNueva.CantidadAdj & " Unidad:" & oLineaNueva.UnidadCompra & " PrecioUC" & oLineaNueva.PrecioUC
                
                'Formamos la cadena a a�adir en el grid
                str_linea = "0" & Chr(m_lSeparador)
                str_linea = str_linea & "" & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.ArtCod_Interno 'sdbgAdjudicaciones.Columns("ART").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i)) & chr(m_lSeparador)
                str_linea = str_linea & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.ArtDen & Chr(m_lSeparador) 'sdbgAdjudicaciones.Columns("DEN").CellValue(sdbgAdjudicaciones.SelBookmarks.Item(i)) & chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.Anyo & "/" & oLineaNueva.GMN1Cod & "/" & oLineaNueva.ProceCod & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.ProveCod & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.Destino & Chr(m_lSeparador)
                str_linea = str_linea & BooleanToSQLBinary(oLineaNueva.DestinoUsuario) & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.CantidadAdj & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.UnidadCompra & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.PrecioUC & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.MonOferta & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.UnidadPedido & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.FactorConversion & Chr(m_lSeparador)
                str_linea = str_linea & "" & Chr(m_lSeparador)
                str_linea = str_linea & "" & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.Id & Chr(m_lSeparador)
                str_linea = str_linea & "" & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.Anyo & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.GMN1Cod & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.ProceCod & Chr(m_lSeparador)
                str_linea = str_linea & "0" & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.ArtCod_Interno & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.GMN2Cod & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.GMN3Cod & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.GMN4Cod & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.Item & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.PresupuestoUC & Chr(m_lSeparador)
                str_linea = str_linea & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.PorcenDesvio & Chr(m_lSeparador)
                str_linea = str_linea & oLineaNueva.Solicitud & Chr(m_lSeparador)
                str_linea = str_linea & "" & Chr(m_lSeparador)
                str_linea = str_linea & CStr(oLineaNueva.EspAdj)
                str_linea = str_linea & Chr(m_lSeparador) & "0" 'Se trata como no generico, no se puede cambiar la den
                str_linea = str_linea & Chr(m_lSeparador) & ""
                str_linea = str_linea & Chr(m_lSeparador) & ""
                str_linea = str_linea & Chr(m_lSeparador) & ""
                str_linea = str_linea & Chr(m_lSeparador) & ""
                str_linea = str_linea & Chr(m_lSeparador) & ""
                str_linea = str_linea & Chr(m_lSeparador) & ""
                str_linea = str_linea & Chr(m_lSeparador) & ""
                str_linea = str_linea & Chr(m_lSeparador) & oLineaNueva.tipoRecepcion
                
                .AddItem str_linea
                oLineasCatalogo.AddLineaCatalogo oLineaNueva
            End If
        Next
        
        .MoveLast
    End With
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddDestinos_DropDown()
    Dim ADORs As Ador.Recordset

    ''' * Objetivo: Abrir el combo de destinos de la forma adecuada
    If Not bModifAdj Or Not bModoEdicion Or sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
        sdbddDestinos.DroppedDown = False
        Exit Sub
    End If
    
    sdbddDestinos.RemoveAll
    
    If m_bCargarComboDesdeDD Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = oDestinos.DevolverTodosLosDestinos(CStr(sdbgAdjudicaciones.ActiveCell.Value), , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
        Else
            Set ADORs = oDestinos.DevolverTodosLosDestinos(CStr(sdbgAdjudicaciones.ActiveCell.Value), , , bRDest)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
        Else
            Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , bRDest)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbddDestinos.RemoveAll
        Screen.MousePointer = vbNormal
        sdbddDestinos.AddItem ""
        sdbgAdjudicaciones.ActiveCell.SelStart = 0
        sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbddDestinos.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbddDestinos.Rows = 0 Then
        sdbddDestinos.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)

End Sub

Private Sub sdbddDestinos_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddDestinos.DataFieldList = "Column 0"
    sdbddDestinos.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbddDestinos_PositionList(ByVal Text As String)
PositionList sdbddDestinos, Text
End Sub


Private Sub sdbddDestinos_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)

    ''' Si es nulo, correcto

    If Text = "" Then
        RtnPassed = True

    Else

        ''' Comprobar la existencia en la lista
        If sdbddDestinos.Columns(0).Value = UCase(Text) Then
            RtnPassed = True
            Exit Sub
        End If

        If bRDest Then
            oDestinos.CargarTodosLosDestinosUON oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , UCase(Text), , True
       Else
            oDestinos.CargarTodosLosDestinos UCase(Text), , True, , , , , , , , True
       End If

        If Not oDestinos.Item(1) Is Nothing Then
            RtnPassed = True
        Else
            sdbgAdjudicaciones.Columns("DEST").Value = ""
        End If

    End If

End Sub

Private Sub sdbddMonedas_CloseUp()
Dim oMoneda As CMoneda
Dim dblprecioAdj As Double
    If Not bModifAdj Or Not bModoEdicion Then
        sdbddMonedas.DroppedDown = False
        Exit Sub
    End If
    'Modificado 30-3-2006
    'En el Drop de monedas tenemos el cod de moneda la denominacion y la equivalencia
    'en una columna oculta
    'en el drop tenemos la equivalencia respecto a moneda del proceso no
    'respecto a la moneda en q fue emitida la oferta
    'por lo q primero lo adaptamos a moneda del proceso y luego a la moneda
    'que se ha escogido
    If sdbddMonedas.Tag <> "" Then
        If sdbddMonedas.Tag = sdbddMonedas.Columns("COD").Value Then
            Exit Sub
        End If
    
        'Modificamos el precio
        dblprecioAdj = StrToDbl0(sdbgAdjudicaciones.Columns("PRECIOUB").Value)
        Set oMoneda = oMonedas.Item(sdbddMonedas.Tag)
        dblprecioAdj = dblprecioAdj * (1 / oMoneda.Equiv)
        'tenemos en dblprecio el precio en valor central
        'Ahora lo pasamos a la moneda escogida en la drop
        dblprecioAdj = dblprecioAdj * StrToDbl0(sdbddMonedas.Columns("CAMBIO").Value)
        sdbgAdjudicaciones.Columns("PRECIOUB").Value = dblprecioAdj
        Set oMoneda = Nothing

    End If
End Sub

Private Sub sdbddMonedas_DropDown()
    If Not bModifAdj Or Not bModoEdicion Then
        sdbddMonedas.DroppedDown = False
        sdbddMonedas.Tag = ""
        Exit Sub
    End If
    sdbddMonedas.Tag = sdbgAdjudicaciones.Columns("MON").Value
    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas , , , , , , , , , False
    CargarGridConMonedas
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)
    Screen.MousePointer = vbNormal
End Sub


Private Sub sdbddMonedas_InitColumnProps()
   ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddMonedas.DataFieldList = "Column 0"
    sdbddMonedas.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddMonedas_PositionList(ByVal Text As String)
PositionList sdbddMonedas, Text
End Sub

Private Sub sdbddMonedas_ValidateList(Text As String, RtnPassed As Integer)
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
       
    If Text = "" Then
        RtnPassed = True

    Else

        ''' Comprobar la existencia en la lista
        oMonedas.CargarTodasLasMonedas UCase(Text), , True, , , , , , , False
        If Not oMonedas.Item(1) Is Nothing Then
            'edu incidencia 6578. reasignar el codigo de moneda en bdd por si hay diferencias de case con lo tecleado.
            sdbgAdjudicaciones.Columns("MON").Value = oMonedas.Item(1).Cod
            RtnPassed = True
        Else
            sdbgAdjudicaciones.Columns("MON").Value = ""
        End If

    End If

End Sub

Private Sub sdbddUnidades_DropDown()
    
    If Not bModifAdj Or Not bModoEdicion Or sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
        sdbddUnidades.DroppedDown = False
        Exit Sub
    End If
    
    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    Screen.MousePointer = vbHourglass
    
    oUnidades.CargarTodasLasUnidades , , , , False
        
    CargarGridConUnidades
    
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)
     
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbddUnidades_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddUnidades.DataFieldList = "Column 0"
    sdbddUnidades.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddUnidades_PositionList(ByVal Text As String)
PositionList sdbddUnidades, Text
End Sub


Private Sub sdbddUnidades_ValidateList(Text As String, RtnPassed As Integer)
    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else

        ''' Comprobar la existencia en la lista

        oUnidades.CargarTodasLasUnidades UCase(Text), , True, , False
        
        If Not oUnidades.Item(1) Is Nothing Then
            RtnPassed = True
        Else
            sdbgAdjudicaciones.Columns("UB").Value = ""
        End If

    End If

End Sub

Private Sub sdbddUnidadesPedido_CloseUp()
    sdbgAdjudicaciones.Columns("FC").Value = sdbddUnidadesPedido.Columns("FC").Value
    sdbgAdjudicaciones.Columns("CANTMIN").Value = sdbddUnidadesPedido.Columns("CANTMIN").Value
End Sub

Private Sub sdbddUnidadesPedido_DropDown()
    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
        
    If Not bModifAdj Or Not bModoEdicion Then
        sdbddUnidadesPedido.DroppedDown = False
        Exit Sub
    End If
    
    oLineaCatalogoSeleccionada.CargarUnidadesDePedido True
        
    CargarGridConUnidadesPedido
    
    sdbgAdjudicaciones.ActiveCell.SelStart = 0
    sdbgAdjudicaciones.ActiveCell.SelLength = Len(sdbgAdjudicaciones.ActiveCell.Text)

End Sub

Private Sub sdbddUnidadesPedido_InitColumnProps()
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddUnidadesPedido.DataFieldList = "Column 0"
    sdbddUnidadesPedido.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbddUnidadesPedido_PositionList(ByVal Text As String)
PositionList sdbddUnidadesPedido, Text
End Sub

Private Sub sdbddUnidadesPedido_RowLoaded(ByVal Bookmark As Variant)
    If sdbddUnidadesPedido.Columns("CANTMIN").Value <> "" Then
        sdbddUnidadesPedido.Columns(0).CellStyleSet "Actuales"
        sdbddUnidadesPedido.Columns(1).CellStyleSet "Actuales"
        sdbddUnidadesPedido.Columns(2).CellStyleSet "Actuales"
        sdbddUnidadesPedido.Columns(3).CellStyleSet "Actuales"
    End If
End Sub

Private Sub sdbddUnidadesPedido_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True
        sdbgAdjudicaciones.Columns("FC").Value = ""
        sdbgAdjudicaciones.Columns("CANTMIN").Value = ""

    Else
    
        ''' Comprobar la existencia en la lista
        If sdbddUnidadesPedido.Columns(0).Value = UCase(Text) Then
            RtnPassed = True
            sdbgAdjudicaciones.Columns("FC").Value = sdbddUnidadesPedido.Columns(2).Value
            sdbgAdjudicaciones.Columns("CANTMIN").Value = sdbddUnidadesPedido.Columns(3).Value
            Exit Sub
        End If
        
        oLineaCatalogoSeleccionada.CargarUnidadesDePedido True
        For Each oUniPed In oLineaCatalogoSeleccionada.UnidadesPedido
            If UCase(sdbgAdjudicaciones.Columns("UP").Value) = oUniPed.UnidadPedido Then
                RtnPassed = True
                If oUniPed.FactorConversion = 0 Then
                    sdbgAdjudicaciones.Columns("FC").Value = ""
                Else
                    sdbgAdjudicaciones.Columns("FC").Value = oUniPed.FactorConversion
                End If
                sdbgAdjudicaciones.Columns("CANTMIN").Value = NullToStr(oUniPed.CantidadMinima)
                Exit Sub
            End If
       Next
        
        sdbgAdjudicaciones.Columns("UP").Value = ""
        sdbgAdjudicaciones.Columns("FC").Value = ""
        sdbgAdjudicaciones.Columns("CANTMIN").Value = ""
    
    End If

End Sub


Private Sub sdbddValorR_DropDown()
Dim oAtributo As CAtributo
Dim oLista As CValorPond
Dim iIndice As Integer
iIndice = 1

If sdbgCamposRecepcion.Rows = 0 Then Exit Sub

If sdbgCamposRecepcion.Columns("VAL").Locked Then
    sdbddValorR.DroppedDown = False
    Exit Sub
End If
sdbddValorR.RemoveAll
sdbddValorR.DroppedDown = True

Set oAtributo = oFSGSRaiz.Generar_CAtributo
oAtributo.Id = sdbgCamposRecepcion.Columns("ATRIBID").Value

If oAtributo.Id > 0 Then
    If sdbgCamposRecepcion.Columns("TIPO_INTRODUCCION").Value = "1" Then
        oAtributo.CargarDatosAtributo
        oAtributo.CargarListaDeValores AtributoParent.Categoria
        For Each oLista In oAtributo.ListaPonderacion
            sdbddValorR.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
            iIndice = iIndice + 1
        Next
    Else
        If sdbgCamposRecepcion.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
            sdbddValorR.AddItem "1" & Chr(m_lSeparador) & sIdiTrue
            sdbddValorR.AddItem "0" & Chr(m_lSeparador) & sIdiFalse
        End If
    End If
End If

sdbddValorR.Width = sdbgCamposRecepcion.Columns("VAL").Width
sdbddValorR.Columns("DESC").Width = sdbddValorR.Width
End Sub

Private Sub sdbddValorR_InitColumnProps()
sdbddValorR.DataFieldList = "Column 0"
sdbddValorR.DataFieldToDisplay = "Column 1"
 
End Sub

Private Sub sdbddValorR_PositionList(ByVal Text As String)
Dim i As Long
Dim bm As Variant
On Error Resume Next
sdbddValorR.MoveFirst
If Text <> "" Then
    For i = 0 To sdbddValorR.Rows - 1
        bm = sdbddValorR.GetBookmark(i)
        If UCase(Text) = UCase(Mid(sdbddValorR.Columns(1).CellText(bm), 1, Len(Text))) Then
            sdbgCamposRecepcion.Columns("VAL").Value = Mid(sdbddValorR.Columns(1).CellText(bm), 1, Len(Text))
            sdbddValorR.Bookmark = bm
            Exit For
        End If
    Next i
End If
End Sub

Private Sub sdbgAdjudicaciones_AfterDelete(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    If Me.Visible Then sdbgAdjudicaciones.SetFocus
    If IsEmpty(sdbgAdjudicaciones.RowBookmark(sdbgAdjudicaciones.Row)) Then
        sdbgAdjudicaciones.Bookmark = sdbgAdjudicaciones.RowBookmark(sdbgAdjudicaciones.Row - 1)
    Else
        sdbgAdjudicaciones.Bookmark = sdbgAdjudicaciones.RowBookmark(sdbgAdjudicaciones.Row)
    End If
    
End Sub

Private Sub sdbgAdjudicaciones_AfterInsert(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If m_bCargandoAdj Then Exit Sub
    
    If bAnyaError = False Then
        If bCatBajaLog Then
            cmdAnyaAdjudicacion.Enabled = False
            cmdAnyaArticulo.Enabled = False
            cmdEliAdj.Enabled = False
            cmdCambiarCat.Enabled = False
            cmdModifPrecios.Enabled = False
            cmdModoEdicion.Enabled = False
        Else
            cmdAnyaAdjudicacion.Enabled = True
            cmdAnyaArticulo.Enabled = True
            cmdEliAdj.Enabled = True
            cmdDeshacer.Enabled = False
            cmdModoEdicion.Enabled = True
            If bModifAdjPrecios Then
                cmdModifPrecios.Enabled = True
            End If
            If bModifAdjCat Then
                cmdCambiarCat.Enabled = True
            End If
        End If
    End If
    
    If tvwCategorias.selectedItem.Children <> 0 Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        If bModifAdjPrecios Then
            cmdModifPrecios.Enabled = True
        End If
    End If

    bAnyadir = False

End Sub

Private Sub sdbgAdjudicaciones_AfterUpdate(RtnDispErrMsg As Integer)
Dim nodx As MSComctlLib.node

    RtnDispErrMsg = False

    Set nodx = tvwCategorias.selectedItem
    
    'Si hay alguna l�nea roja en la grid no habilitamos los botones de A�adir
    If Not oLineasAAnyadir Is Nothing Then
        If oLineasAAnyadir.Count > 0 Then
            cmdAnyaAdjudicacion.Enabled = False
            cmdAnyaArticulo.Enabled = False
            cmdCambiarCat.Enabled = False
            cmdEliAdj.Enabled = True
            cmdDeshacer.Enabled = False
            cmdModoEdicion.Enabled = True
            cmdModifPrecios.Enabled = False
        Else
            lblLeyenda.Visible = False
            cmdAnyaAdjudicacion.Enabled = True
            cmdAnyaArticulo.Enabled = True
            cmdCambiarCat.Enabled = True
            cmdEliAdj.Enabled = True
            cmdDeshacer.Enabled = False
            cmdModoEdicion.Enabled = True
            cmdModifPrecios.Enabled = True
        End If
        
        If tvwCategorias.selectedItem.Children <> 0 Then
            cmdAnyaAdjudicacion.Enabled = False
            cmdAnyaArticulo.Enabled = False
            cmdCambiarCat.Enabled = False
            cmdDeshacer.Enabled = False
            cmdEliAdj.Enabled = False
        End If
    
    Else
        If bAnyaError = False And bModError = False Then
            cmdAnyaAdjudicacion.Enabled = True
            cmdAnyaArticulo.Enabled = True
            cmdEliAdj.Enabled = True
            cmdDeshacer.Enabled = False
            cmdModoEdicion.Enabled = True
            If bModifAdjPrecios Then
                cmdModifPrecios.Enabled = True
            End If
            If bModifAdjCat Then
                cmdCambiarCat.Enabled = True
            End If
        End If

        If tvwCategorias.selectedItem.Children <> 0 Then
            cmdAnyaAdjudicacion.Enabled = False
            cmdAnyaArticulo.Enabled = False
            cmdEliAdj.Enabled = False
            cmdCambiarCat.Enabled = False
            If bModifAdjPrecios Then
                cmdModifPrecios.Enabled = True
            End If
        End If

    End If

    If bCatBajaLog Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        cmdModifPrecios.Enabled = False
        cmdModoEdicion.Enabled = False
    End If
        
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbgAdjudicaciones_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim vEstado(1) As Variant
    Dim oInstancias As CInstancias
    Dim lIdPerfil As Long
    
    'Comprobamos que la solicitud introducida exista y est� en estado aprobada
    If sdbgAdjudicaciones.Columns(ColIndex).Name = "SOLICIT" Then
        If sdbgAdjudicaciones.Columns("SOLICIT").Value <> "" Then
        
            If Not IsNumeric(sdbgAdjudicaciones.Columns("SOLICIT").Value) Then
                oMensajes.NoValida sIdiSolicitud
                Cancel = True
                Exit Sub
            End If
        
            If sdbgAdjudicaciones.Columns("SOLICIT").Value <> OldValue Then
                If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
                vEstado(1) = EstadoSolicitud.Aprobada
                Set oInstancias = oFSGSRaiz.Generar_CInstancias
                If m_bRSolicUO Then
                    oInstancias.BuscarSolicitudes OrdSolicPorId, , CLng(Trim(sdbgAdjudicaciones.Columns("SOLICIT").Value)), , , vEstado, , , m_bRSolicAsig, m_bRSolicEquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , , basOptimizacion.gCodPersonaUsuario, , , , , , , , , , , , , , , , m_bRSolicPerfUO, lIdPerfil
                Else
                    oInstancias.BuscarSolicitudes OrdSolicPorId, , CLng(Trim(sdbgAdjudicaciones.Columns("SOLICIT").Value)), , , vEstado, , , m_bRSolicAsig, m_bRSolicEquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, , , , , , basOptimizacion.gCodPersonaUsuario, , , , , , , , , , , , , , , , m_bRSolicPerfUO, lIdPerfil
                End If
                If oInstancias Is Nothing Then
                    oMensajes.NoValida sIdiSolicitud
                    Cancel = True
                    Exit Sub
                Else
                    If oInstancias.Count = 0 Then
                        oMensajes.NoValida sIdiSolicitud
                        Cancel = True
                        Exit Sub
                    End If
                End If
                Set oInstancias = Nothing
            End If
        End If
    End If
End Sub

Private Sub sdbgAdjudicaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgAdjudicaciones_BeforeInsert(Cancel As Integer)
    If Not bModoEdicion Then Exit Sub
        
    bAnyadir = True
    bAnyaError = False

End Sub

Private Sub sdbgAdjudicaciones_BeforeUpdate(Cancel As Integer)
    
    '' * Objetivo: Validar los datos
    Dim oProve As CProveedor
    Dim oProveedores As CProveedores
    Dim teserror As TipoErrorSummit
    
    Dim oLineaNueva As CLineaCatalogo
    Dim nodx As MSComctlLib.node
    Dim oArticulos As CArticulos
    Dim i As Long
    Dim sLineaError As String
    Dim oLineas As CLineasCatalogo
    Dim lID As String
    Dim bSinContactos As Boolean
    Dim bSinFormaPago As Boolean
    
   
    'If sdbgAdjudicaciones.col = 0 Then Exit Sub

    If m_bRespetar Then Exit Sub
    
    If g_bRefrescandoUniPed Then Exit Sub
    
    bValError = False
    bAnyaError = False
    bModError = False
    Cancel = False

    Screen.MousePointer = vbHourglass
    
    'Denominaci�n
    If Trim(sdbgAdjudicaciones.Columns("ARTDEN").Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValida sIdiDenominacion
        Cancel = True
        bValError = True
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
        Exit Sub
    End If

    'Fecha Despub.
    If sdbgAdjudicaciones.Columns("DESPUB").Text <> "" Then
        If Not IsDate(sdbgAdjudicaciones.Columns("DESPUB").Text) Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiFechaDespub
            sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
            bValError = True
            Cancel = True
            Exit Sub
        Else
            If (CDate(sdbgAdjudicaciones.Columns("DESPUB").Text) < Date) Then
                Screen.MousePointer = vbNormal
                oMensajes.NoValido sIdiFechaDespub
                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                bValError = True
                Cancel = True
                Exit Sub
            End If
        End If
    End If

    'Prove.
    If Trim(sdbgAdjudicaciones.Columns("PROVE").Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProve
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
        bValError = True
        Cancel = True
        Exit Sub
    Else
        lID = sdbgAdjudicaciones.Columns("ID").Value
        If Not oLineasCatalogo.Item(lID) Is Nothing Then
            If UCase(sdbgAdjudicaciones.Columns("PROVE").Value) <> UCase(oLineasCatalogo.Item(lID).ProveCod) Then
                Set oProveedores = oFSGSRaiz.generar_CProveedores
                oProveedores.CargarTodosLosProveedoresDesde3 1, Trim(sdbgAdjudicaciones.Columns("PROVE").Value), , True, , False
                If oProveedores.Count = 0 Then
                    Screen.MousePointer = vbNormal
                    oMensajes.NoValido sIdiProve
                    sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                    bValError = True
                    Cancel = True
                    Exit Sub
                End If
                
'                If gParametrosGenerales.giINSTWEB = ConPortal Then
'                    oProveedores.CargarDatosProveedor sdbgAdjudicaciones.Columns("PROVE").Value
'                    If ((IsNull(oProveedores.Item(CStr(sdbgAdjudicaciones.Columns("PROVE").Value)).CodPortal) Or (oProveedores.Item(CStr(sdbgAdjudicaciones.Columns("PROVE").Value)).CodPortal = ""))) Then
'                        Screen.MousePointer = vbNormal
'                        oMensajes.ProveCatalogProvePortal
'                        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
'                        bValError = True
'                        Cancel = True
'                        Exit Sub
'                    End If
'                End If
                If gParametrosGenerales.giINSTWEB = conweb Then
                    oProveedores.CargarDatosProveedor sdbgAdjudicaciones.Columns("PROVE").Value, True
                    If oProveedores.Item(CStr(sdbgAdjudicaciones.Columns("PROVE").Value)).UsuarioWeb Is Nothing Then
                        Screen.MousePointer = vbNormal
                        oMensajes.ProveCatalogProveWeb
                        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                        bValError = True
                        Cancel = True
                        Exit Sub
                    End If
                End If
                If sdbgAdjudicaciones.Columns("PUB").Value Then
                    Set oProve = oProveedores.Item(CStr(sdbgAdjudicaciones.Columns("PROVE").Value))
                    
                    
                    Set oLineas = oFSGSRaiz.Generar_CLineasCatalogo
                    lID = sdbgAdjudicaciones.Columns("ID").Value
                    oLineas.Add sdbgAdjudicaciones.Columns("ID").Value, Prove:=sdbgAdjudicaciones.Columns("PROVE").Value, FecDesp:=oLineasCatalogo.Item(lID).FechaDespublicacion, FechaActual:=oLineasCatalogo.Item(lID).FECACT
                    If oLineas.Item(lID).ExistenPedidosFavoritos() Then
                        If oMensajes.PreguntarEliminarPedidosFavoritos(1) <> vbYes Then
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    
                    
                    If Not IsNull(oProve.FormaPagoCod) Then
                        oProve.CargarTodosLosContactos "", "", , , , , , , , , , , , , , , , True
                        If oProve.Contactos.Count = 0 Then
                            bSinContactos = True
                        End If
                    Else
                        bSinFormaPago = True
                        
                    End If
                    
                    
                    If bSinContactos Or bSinFormaPago Then
                        oMensajes.LineaDespublicada bSinContactos, bSinFormaPago
                        sdbgAdjudicaciones.Columns("PUB").Value = 0
                        teserror = oLineas.ActivarDesactivarPublicacion(False)
                        oLineasCatalogo.Item(lID).FECACT = oLineas.Item(lID).FECACT
                    
                    End If
                End If
            End If
        End If
    End If

    'Art. prove
    If gParametrosGenerales.gbOblPedidosHom Then
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sdbgAdjudicaciones.Columns("GMN1").Value, sdbgAdjudicaciones.Columns("GMN2").Value, sdbgAdjudicaciones.Columns("GMN3").Value, sdbgAdjudicaciones.Columns("GMN4").Value, sdbgAdjudicaciones.Columns("PROVE").Value, 1, True, sdbgAdjudicaciones.Columns("ARTCOD").Value, True
        For i = 1 To oArticulos.Count
            If oArticulos.Item(i).Cod = sdbgAdjudicaciones.Columns("ARTCOD").Value Then
                If oArticulos.Item(i).Homologado <> 1 Then
                    Screen.MousePointer = vbNormal
                    oMensajes.ArticuloNoHomologado
                    sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                    bValError = True
                    Cancel = True
                    Exit Sub
                End If
            End If
        Next
    End If

    'Dest.
    If Trim(sdbgAdjudicaciones.Columns("DEST").Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiDest
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
        bValError = True
        Cancel = True
        Exit Sub
    End If

    'UB
    If Trim(sdbgAdjudicaciones.Columns("UB").Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiUB
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
        bValError = True
        Cancel = True
        Exit Sub
    End If

    'Precio UB.
    If ((Not IsNumeric(sdbgAdjudicaciones.Columns("PrecioUB").Value)) Or (IsEmpty(sdbgAdjudicaciones.Columns("PrecioUB").Value))) Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValida sIdiPrecioUB
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
        bValError = True
        Cancel = True
        Exit Sub
    End If

    'UP.
    If Trim(sdbgAdjudicaciones.Columns("UP").Value) = "" Then
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiUP
        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
        bValError = True
        Cancel = True
        Exit Sub
    End If
    'MON.
    #If VERSION >= 30600 Then
        If Trim(sdbgAdjudicaciones.Columns("MON").Value) = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValido sIdiMon
            sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
            bValError = True
            Cancel = True
            Exit Sub
        End If
    #End If
    
    'Porcen Desvio
    If Trim(sdbgAdjudicaciones.Columns("PORCEN_DESVIO").Text) <> "" Then
        If Not IsNumeric(sdbgAdjudicaciones.Columns("PORCEN_DESVIO").Text) Then
            Screen.MousePointer = vbNormal
            oMensajes.NoValida sPorcenDesvio
            sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
            bValError = True
            Cancel = True
            Exit Sub
        Else
            'Se comprueba que sea un porcentaje valido
            If sdbgAdjudicaciones.Columns("PORCEN_DESVIO").Text > 100 Or (sdbgAdjudicaciones.Columns("PORCEN_DESVIO").Text < 0 And sdbgAdjudicaciones.Columns("PORCEN_DESVIO").Text <> -1) Then
                Screen.MousePointer = vbNormal
                oMensajes.NoValida sPorcenDesvio
                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                bValError = True
                Cancel = True
                Exit Sub
            End If
        End If
    End If

    
    If bAnyadir And GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = True Then   'Inserci�n

            'Es una l�nea nueva que proviene de un art�culo
            'Inserci�n de una l�nea nueva que no pertenece a una l�nea de adjudicaci�n
            Set oLineaNueva = oFSGSRaiz.Generar_CLineaCatalogo
            oLineaNueva.Publicado = GridCheckToBoolean(sdbgAdjudicaciones.Columns("PUB").Value)
            
            If sdbgAdjudicaciones.Columns("DESPUB").Value = "" Or IsEmpty(sdbgAdjudicaciones.Columns("DESPUB").Value) Then
                oLineaNueva.FechaDespublicacion = Null
            Else
                oLineaNueva.FechaDespublicacion = CDate(sdbgAdjudicaciones.Columns("DESPUB").Value)
            End If
            oLineaNueva.ProceCod = Null
            oLineaNueva.Item = Null
            oLineaNueva.Anyo = Null
            oLineaNueva.ArtCod_Interno = sdbgAdjudicaciones.Columns("ARTCOD").Value
            oLineaNueva.ArtDen = sdbgAdjudicaciones.Columns("ARTDEN").Value
            oLineaNueva.ProveCod = sdbgAdjudicaciones.Columns("PROVE").Value
            oLineaNueva.Destino = UCase(sdbgAdjudicaciones.Columns("DEST").Value)
            oLineaNueva.CantidadAdj = sdbgAdjudicaciones.Columns("CANTADJ").Value
            oLineaNueva.UnidadCompra = UCase(sdbgAdjudicaciones.Columns("UB").Value)
            oLineaNueva.PrecioUC = sdbgAdjudicaciones.Columns("PrecioUB").Value
            oLineaNueva.UnidadPedido = UCase(sdbgAdjudicaciones.Columns("UP").Value)
            oLineaNueva.FactorConversion = sdbgAdjudicaciones.Columns("FC").Value
            oLineaNueva.HayCamposPedido = sdbgAdjudicaciones.Columns("CAMPED").Value
            oLineaNueva.HayCamposRecepcion = sdbgAdjudicaciones.Columns("CAMRECEP").Value
            oLineaNueva.HayCostesDescuentos = sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS").Value
            oLineaNueva.PorcenDesvio = StrToDbl0(sdbgAdjudicaciones.Columns("PORCEN_DESVIO").Text)
            Set oLineaNueva.CondicionesSuministro = oLineaCatalogoSeleccionada.CondicionesSuministro
            oLineaNueva.tipoRecepcion = sdbgAdjudicaciones.Columns("TIPORECEPCION").Value
            'Linea a�adida el 24-2-2006
            'Para la columna de monedas
            
            oLineaNueva.mon = sdbgAdjudicaciones.Columns("MON").Value
            oLineaNueva.Solicitud = StrToDblOrNull(sdbgAdjudicaciones.Columns("SOLICIT").Value)
            oLineaNueva.DestinoUsuario = GridCheckToBoolean(sdbgAdjudicaciones.Columns("DEST_USU").Value)
            oLineaNueva.EspAdj = sdbgAdjudicaciones.Columns("ESPADJ").Value
            Set nodx = tvwCategorias.selectedItem
            Select Case Left(nodx.Tag, 4)
                    Case "CAT1"
                            oLineaNueva.Cat1 = oCategoria1Seleccionada.Id
                            oLineaNueva.Cat2 = Null
                            oLineaNueva.Cat3 = Null
                            oLineaNueva.Cat4 = Null
                            oLineaNueva.Cat5 = Null
                            If nodx.Image = "SEGURO" Or nodx.Image = "CAT_INT" Then
                                oLineaNueva.SeguridadNivel = 1
                                oLineaNueva.SeguridadCat = oCategoria1Seleccionada.Id
                            Else
                                oLineaNueva.SeguridadNivel = Null
                                oLineaNueva.SeguridadCat = Null
                            End If

                    Case "CAT2"
                            oLineaNueva.Cat1 = oCategoria2Seleccionada.Cat1
                            oLineaNueva.Cat2 = oCategoria2Seleccionada.Id
                            oLineaNueva.Cat3 = Null
                            oLineaNueva.Cat4 = Null
                            oLineaNueva.Cat5 = Null
                            If nodx.Image = "SEGURO" Or nodx.Image = "CAT_INT" Then
                                oLineaNueva.SeguridadNivel = 2
                                oLineaNueva.SeguridadCat = oCategoria2Seleccionada.Id
                            Else
                                If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "CAT_INT" Then
                                    oLineaNueva.SeguridadNivel = 1
                                    oLineaNueva.SeguridadCat = oCategoria2Seleccionada.Cat1
                                Else
                                    oLineaNueva.SeguridadNivel = Null
                                    oLineaNueva.SeguridadCat = Null
                                End If
                            End If

                    Case "CAT3"
                            oLineaNueva.Cat1 = oCategoria3Seleccionada.Cat1
                            oLineaNueva.Cat2 = oCategoria3Seleccionada.Cat2
                            oLineaNueva.Cat3 = oCategoria3Seleccionada.Id
                            oLineaNueva.Cat4 = Null
                            oLineaNueva.Cat5 = Null
                            If nodx.Image = "SEGURO" Or nodx.Image = "CAT_INT" Then
                                oLineaNueva.SeguridadNivel = 3
                                oLineaNueva.SeguridadCat = oCategoria3Seleccionada.Id
                            Else
                                If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "CAT_INT" Then
                                    oLineaNueva.SeguridadNivel = 2
                                    oLineaNueva.SeguridadCat = oCategoria3Seleccionada.Cat2
                                Else
                                    If nodx.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Image = "CAT_INT" Then
                                        oLineaNueva.SeguridadNivel = 1
                                        oLineaNueva.SeguridadCat = oCategoria3Seleccionada.Cat1
                                    Else
                                        oLineaNueva.SeguridadNivel = Null
                                        oLineaNueva.SeguridadCat = Null
                                    End If
                                End If
                            End If

                    Case "CAT4"
                            oLineaNueva.Cat1 = oCategoria4Seleccionada.Cat1
                            oLineaNueva.Cat2 = oCategoria4Seleccionada.Cat2
                            oLineaNueva.Cat3 = oCategoria4Seleccionada.Cat3
                            oLineaNueva.Cat4 = oCategoria4Seleccionada.Id
                            oLineaNueva.Cat5 = Null
                            If nodx.Image = "SEGURO" Or nodx.Image = "CAT_INT" Then
                                oLineaNueva.SeguridadNivel = 4
                                oLineaNueva.SeguridadCat = oCategoria4Seleccionada.Id
                            Else
                                If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "CAT_INT" Then
                                    oLineaNueva.SeguridadNivel = 3
                                    oLineaNueva.SeguridadCat = oCategoria4Seleccionada.Cat3
                                Else
                                    If nodx.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Image = "CAT_INT" Then
                                        oLineaNueva.SeguridadNivel = 2
                                        oLineaNueva.SeguridadCat = oCategoria4Seleccionada.Cat2
                                    Else
                                        If nodx.Parent.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Parent.Image = "CAT_INT" Then
                                            oLineaNueva.SeguridadNivel = 1
                                            oLineaNueva.SeguridadCat = oCategoria4Seleccionada.Cat1
                                        Else
                                            oLineaNueva.SeguridadNivel = Null
                                            oLineaNueva.SeguridadCat = Null
                                        End If
                                    End If
                                End If
                            End If

                    Case "CAT5"
                            oLineaNueva.Cat1 = oCategoria5Seleccionada.Cat1
                            oLineaNueva.Cat2 = oCategoria5Seleccionada.Cat2
                            oLineaNueva.Cat3 = oCategoria5Seleccionada.Cat3
                            oLineaNueva.Cat4 = oCategoria5Seleccionada.Cat4
                            oLineaNueva.Cat5 = oCategoria5Seleccionada.Id
                            If nodx.Image = "SEGURO" Or nodx.Image = "CAT_INT" Then
                                oLineaNueva.SeguridadNivel = 5
                                oLineaNueva.SeguridadCat = oCategoria5Seleccionada.Id
                            Else
                                If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "CAT_INT" Then
                                    oLineaNueva.SeguridadNivel = 4
                                    oLineaNueva.SeguridadCat = oCategoria5Seleccionada.Cat4
                                Else
                                    If nodx.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Image = "CAT_INT" Then
                                        oLineaNueva.SeguridadNivel = 3
                                        oLineaNueva.SeguridadCat = oCategoria5Seleccionada.Cat3
                                    Else
                                        If nodx.Parent.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Parent.Image = "CAT_INT" Then
                                            oLineaNueva.SeguridadNivel = 2
                                            oLineaNueva.SeguridadCat = oCategoria5Seleccionada.Cat2
                                        Else
                                            If nodx.Parent.Parent.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Parent.Parent.Image = "CAT_INT" Then
                                                oLineaNueva.SeguridadNivel = 1
                                                oLineaNueva.SeguridadCat = oCategoria5Seleccionada.Cat1
                                            Else
                                                oLineaNueva.SeguridadNivel = Null
                                                oLineaNueva.SeguridadCat = Null
                                            End If
                                        End If
                                    End If
                                End If
                            End If

            End Select
            
            Set oIBaseDatos = oLineaNueva
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError <> TESnoerror And teserror.NumError <> TESImposibleAccederAlArchivo Then
                bAnyaError = True
                Screen.MousePointer = vbNormal
                TratarError teserror
                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                Cancel = True
                Exit Sub
            Else
                'si el Arg1 es un array son los archivos que no se han podido copiar
                If teserror.NumError = TESImposibleAccederAlArchivo And IsArray(teserror.Arg1) Then
                    sLineaError = sdbgAdjudicaciones.Columns("PROVE").caption & ": " & sdbgAdjudicaciones.Columns("PROVE").Text & "; " & sdbgAdjudicaciones.Columns("ART").caption & " " & sdbgAdjudicaciones.Columns("ART").Text
                    oMensajes.ImposibleAccederAFichero teserror.Arg1, sLineaError
                End If
                Accion = ACCCatAdjudAnya
                basSeguridad.RegistrarAccion Accion, "Linea:" & sdbgAdjudicaciones.Columns("ID").Value
                oLineasCatalogo.Add oLineaNueva.Id, , , , , oLineaNueva.ProveCod, oLineaNueva.ArtCod_Interno, , oLineaNueva.ArtDen, , oLineaNueva.Destino, oLineaNueva.UnidadCompra, oLineaNueva.CantidadAdj, oLineaNueva.PrecioUC, oLineaNueva.UnidadPedido, oLineaNueva.FactorConversion, oLineaNueva.CantidadMinima, oLineaNueva.Publicado, oLineaNueva.FechaDespublicacion, oLineaNueva.Cat1, oLineaNueva.Cat2, oLineaNueva.Cat3, oLineaNueva.Cat4, oLineaNueva.Cat5, oLineaNueva.SeguridadCat, , , , , , , , , oLineaNueva.FECACT, , , oLineaNueva.Solicitud, oLineaNueva.DestinoUsuario, oLineaNueva.esp, oLineaNueva.EspAdj, , , , , , , , , , , , oLineaNueva.HayCamposPedido, oLineaNueva.HayCostesDescuentos, oLineaNueva.PorcenDesvio, , , oLineaNueva.SeguridadNivel
                sdbgAdjudicaciones.Columns("NUEVA").Value = "0"
                sdbgAdjudicaciones.Columns("ESPADJ").Value = oLineaNueva.EspAdj
                oLineasAAnyadir.Remove sdbgAdjudicaciones.Columns("ID").Value
                sdbgAdjudicaciones.Columns("ID").Value = CStr(oLineaNueva.Id)
                sdbgAdjudicaciones.Refresh
                If bModifAdj Then
                    cmdDeshacer.Enabled = True
                End If
            End If

            Screen.MousePointer = vbHourglass
            oLineaCatalogoSeleccionada.CargarUnidadesDePedido True
            For Each oUniPed In oLineaCatalogoSeleccionada.UnidadesPedido
                If UCase(sdbgAdjudicaciones.Columns("UP").Value) = oUniPed.UnidadPedido Then
                    If oUniPed.FactorConversion = 0 Then
                    'Si la unidad no tiene FC a�adirla a PROVE_ART_UP, si es una art�culo codificado.
                        If oUniPed.ArtCod_Interno <> "" Then
                            oUniPed.ArtCod_Interno = sdbgAdjudicaciones.Columns("ARTCOD").Value
                            oUniPed.GMN1Cod = sdbgAdjudicaciones.Columns("GMN1").Value
                            oUniPed.GMN2Cod = sdbgAdjudicaciones.Columns("GMN2").Value
                            oUniPed.GMN3Cod = sdbgAdjudicaciones.Columns("GMN3").Value
                            oUniPed.GMN4Cod = sdbgAdjudicaciones.Columns("GMN4").Value
                            oUniPed.ProveCod = sdbgAdjudicaciones.Columns("PROVE").Value
                            oUniPed.UnidadCompra = UCase(sdbgAdjudicaciones.Columns("UB").Value)
                            oUniPed.FactorConversion = CDbl(sdbgAdjudicaciones.Columns("FC").Value)

                            Set oIBAseDatosEnEdicionUnidades = oUniPed
                            teserror = oIBAseDatosEnEdicionUnidades.AnyadirABaseDatos
                            If teserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                TratarError teserror
                                bAnyaError = True
                                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                                Cancel = True
                                Exit Sub
                            Else
                                Accion = ACCCatUniPedAnya
                                basSeguridad.RegistrarAccion Accion, "Linea:" & sdbgAdjudicaciones.Columns("ID").Value & ",  UniPed:" & sdbgAdjudicaciones.Columns("UP").Value
                            End If
                        End If
                    Else
                    'Tiene ya FC, si ha cambiado el FC o la CANTMIN modificar el reg
                        If sdbgAdjudicaciones.Columns("FC").Value <> oUniPed.FactorConversion Then
                            If oUniPed.ArtCod_Interno = "" Then
                                oUniPed.IdLinea = sdbgAdjudicaciones.Columns("ID").Value
                                oUniPed.FactorConversion = sdbgAdjudicaciones.Columns("FC").Value
                            Else
                                oUniPed.ArtCod_Interno = sdbgAdjudicaciones.Columns("ARTCOD").Value
                                oUniPed.GMN1Cod = sdbgAdjudicaciones.Columns("GMN1").Value
                                oUniPed.GMN2Cod = sdbgAdjudicaciones.Columns("GMN2").Value
                                oUniPed.GMN3Cod = sdbgAdjudicaciones.Columns("GMN3").Value
                                oUniPed.GMN4Cod = sdbgAdjudicaciones.Columns("GMN4").Value
                                oUniPed.ProveCod = sdbgAdjudicaciones.Columns("PROVE").Value
                                oUniPed.UnidadCompra = UCase(sdbgAdjudicaciones.Columns("UB").Value)
                                oUniPed.FactorConversion = sdbgAdjudicaciones.Columns("FC").Value
                            End If
                            
                            Set oIBAseDatosEnEdicionUnidades = oUniPed
                            teserror = oIBAseDatosEnEdicionUnidades.FinalizarEdicionModificando
                            If teserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                TratarError teserror
                                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                                bModError = True
                                Cancel = True
                                Exit Sub
                            Else
                                Accion = ACCCatUniPedMod
                                basSeguridad.RegistrarAccion Accion, "Linea:" & sdbgAdjudicaciones.Columns("ID").Value & "Unidad Pedido:" & sdbgAdjudicaciones.Columns("UP").Value
                            End If
                        End If
                    End If
                    Exit For
                End If
            Next

    Else
    'Es una l�nea ya existente

            'Modificamos una l�nea existente
            Set oLineaCatalogoEnEdicion = oLineasCatalogo.Item(sdbgAdjudicaciones.Columns("ID").Value)
            oLineaCatalogoEnEdicion.Publicado = GridCheckToBoolean(sdbgAdjudicaciones.Columns("PUB").Value)
            If sdbgAdjudicaciones.Columns("DESPUB").Value = "" Or IsEmpty(sdbgAdjudicaciones.Columns("DESPUB").Value) Then
                oLineaCatalogoEnEdicion.FechaDespublicacion = Null
            Else
                oLineaCatalogoEnEdicion.FechaDespublicacion = CDate(sdbgAdjudicaciones.Columns("DESPUB").Value)
            End If
            oLineaCatalogoEnEdicion.ArtCod_Interno = sdbgAdjudicaciones.Columns("ARTCOD").Value
            oLineaCatalogoEnEdicion.ArtDen = sdbgAdjudicaciones.Columns("ARTDEN").Value
            oLineaCatalogoEnEdicion.GMN1Cod = sdbgAdjudicaciones.Columns("GMN1").Value
            oLineaCatalogoEnEdicion.GMN2Cod = sdbgAdjudicaciones.Columns("GMN2").Value
            oLineaCatalogoEnEdicion.GMN3Cod = sdbgAdjudicaciones.Columns("GMN3").Value
            oLineaCatalogoEnEdicion.GMN4Cod = sdbgAdjudicaciones.Columns("GMN4").Value
            oLineaCatalogoEnEdicion.ProveCod = sdbgAdjudicaciones.Columns("PROVE").Value
            oLineaCatalogoEnEdicion.Destino = UCase(sdbgAdjudicaciones.Columns("DEST").Value)
            oLineaCatalogoEnEdicion.UnidadCompra = UCase(sdbgAdjudicaciones.Columns("UB").Value)
            oLineaCatalogoEnEdicion.UnidadPedido = UCase(sdbgAdjudicaciones.Columns("UP").Value)
            oLineaCatalogoEnEdicion.FactorConversion = sdbgAdjudicaciones.Columns("FC").Value
            oLineaCatalogoEnEdicion.PorcenDesvio = StrToDbl0(sdbgAdjudicaciones.Columns("PORCEN_DESVIO").Text)
            Set oLineaCatalogoEnEdicion.CondicionesSuministro = oLineaCatalogoSeleccionada.CondicionesSuministro
            oLineaCatalogoEnEdicion.tipoRecepcion = sdbgAdjudicaciones.Columns("TIPORECEPCION").Value
            'Linea a�adida el 22-2-2006
            'Para la columna de monedas
            #If VERSION >= 30600 Then
                 oLineaCatalogoEnEdicion.mon = sdbgAdjudicaciones.Columns("MON").Value
            #End If
            oLineaCatalogoEnEdicion.Solicitud = StrToDblOrNull(sdbgAdjudicaciones.Columns("SOLICIT").Value)

            If IsEmpty(sdbgAdjudicaciones.Columns("CANTMIN").Value) Then
                oLineaCatalogoEnEdicion.CantidadMinima = Null
            Else
                oLineaCatalogoEnEdicion.CantidadMinima = CDbl(sdbgAdjudicaciones.Columns("CANTMIN").Value)
            End If
            oLineaCatalogoEnEdicion.PrecioUC = sdbgAdjudicaciones.Columns("PrecioUB").Value
            oLineaCatalogoEnEdicion.DestinoUsuario = GridCheckToBoolean(sdbgAdjudicaciones.Columns("DEST_USU").Value)
            
            Set oIBAseDatosEnEdicion = oLineaCatalogoEnEdicion

            teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                bModError = True
                Cancel = True
                Exit Sub
            Else
                Accion = ACCCatAdjudMod
                basSeguridad.RegistrarAccion Accion, "Linea:" & sdbgAdjudicaciones.Columns("ID").Value
                If bModifAdj Then
                    cmdDeshacer.Enabled = True
                End If
            End If

            Screen.MousePointer = vbHourglass
            oLineaCatalogoSeleccionada.CargarUnidadesDePedido True
            For Each oUniPed In oLineaCatalogoSeleccionada.UnidadesPedido
                If UCase(sdbgAdjudicaciones.Columns("UP").Value) = oUniPed.UnidadPedido Then
                    If oUniPed.FactorConversion = 0 Then
                    'Si la unidad no tiene FC a�adirla a PROVE_ART_UP, si es una art�culo codificado.
                        If oUniPed.ArtCod_Interno <> "" Then
                            oUniPed.ArtCod_Interno = sdbgAdjudicaciones.Columns("ARTCOD").Value
                            oUniPed.GMN1Cod = sdbgAdjudicaciones.Columns("GMN1").Value
                            oUniPed.GMN2Cod = sdbgAdjudicaciones.Columns("GMN2").Value
                            oUniPed.GMN3Cod = sdbgAdjudicaciones.Columns("GMN3").Value
                            oUniPed.GMN4Cod = sdbgAdjudicaciones.Columns("GMN4").Value
                            oUniPed.ProveCod = sdbgAdjudicaciones.Columns("PROVE").Value
                            oUniPed.UnidadCompra = UCase(sdbgAdjudicaciones.Columns("UB").Value)
                            oUniPed.FactorConversion = CDbl(sdbgAdjudicaciones.Columns("FC").Value)

                            Set oIBAseDatosEnEdicionUnidades = oUniPed
                            teserror = oIBAseDatosEnEdicionUnidades.AnyadirABaseDatos
                            If teserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                bAnyaError = True
                                TratarError teserror
                                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                                Cancel = True
                                Exit Sub
                            Else
                                Accion = ACCCatUniPedAnya
                                basSeguridad.RegistrarAccion Accion, "Linea:" & sdbgAdjudicaciones.Columns("ID").Value & ",  UniPed:" & sdbgAdjudicaciones.Columns("UP").Value
                            End If
                        End If
                    Else
                    'Tiene ya FC, si ha cambiado el FC o la CANTMIN modificar el reg
                        If sdbgAdjudicaciones.Columns("FC").Value <> oUniPed.FactorConversion Then
                            If oUniPed.ArtCod_Interno = "" Then
                                oUniPed.IdLinea = sdbgAdjudicaciones.Columns("ID").Value
                                oUniPed.FactorConversion = sdbgAdjudicaciones.Columns("FC").Value
                            Else
                                oUniPed.ArtCod_Interno = sdbgAdjudicaciones.Columns("ARTCOD").Value
                                oUniPed.GMN1Cod = sdbgAdjudicaciones.Columns("GMN1").Value
                                oUniPed.GMN2Cod = sdbgAdjudicaciones.Columns("GMN2").Value
                                oUniPed.GMN3Cod = sdbgAdjudicaciones.Columns("GMN3").Value
                                oUniPed.GMN4Cod = sdbgAdjudicaciones.Columns("GMN4").Value
                                oUniPed.ProveCod = sdbgAdjudicaciones.Columns("PROVE").Value
                                oUniPed.UnidadCompra = UCase(sdbgAdjudicaciones.Columns("UB").Value)
                                oUniPed.FactorConversion = sdbgAdjudicaciones.Columns("FC").Value
                            End If
                            Set oIBAseDatosEnEdicionUnidades = oUniPed

                            teserror = oIBAseDatosEnEdicionUnidades.FinalizarEdicionModificando
                            If teserror.NumError <> TESnoerror Then
                                Screen.MousePointer = vbNormal
                                TratarError teserror
                                sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                                bModError = True
                                Cancel = True
                                Exit Sub
                            Else
                                Accion = ACCCatUniPedMod
                                basSeguridad.RegistrarAccion Accion, "Linea:" & sdbgAdjudicaciones.Columns("ID").Value & "Unidad Pedido:" & sdbgAdjudicaciones.Columns("UP").Value
                            End If
                        End If
                    End If
                    Exit For
                End If
            Next

    End If

    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Evento de grid cuando se pulsa algun boton en el grid
''' </summary>
''' <remarks>Llamada desde: Evento de grid; Tiempo m�ximo: instantes</remarks>
Private Sub sdbgAdjudicaciones_BtnClick()
    Dim lIdSolicitud As Variant
    Dim iRow As Long
    Dim vBookmark As Variant
    Dim sCol As String
    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = True Then
        Set oLineaCatalogoSeleccionada = oLineasAAnyadir.Item(sdbgAdjudicaciones.Columns("ID").Value)
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdCambiarCat.Enabled = False
        cmdEliAdj.Enabled = True
        cmdDeshacer.Enabled = True
    Else
        Set oLineaCatalogoSeleccionada = oLineasCatalogo.Item(CStr(sdbgAdjudicaciones.Columns("ID").Value))
        If Not oLineasAAnyadir Is Nothing Then
            If oLineasAAnyadir.Count > 0 Then
                cmdAnyaAdjudicacion.Enabled = False
                cmdAnyaArticulo.Enabled = False
                cmdCambiarCat.Enabled = False
                cmdEliAdj.Enabled = True
                cmdDeshacer.Enabled = True
            End If
        Else
            If sdbgAdjudicaciones.DataChanged = False Then
                If bModifAdj Then
                    cmdAnyaAdjudicacion.Enabled = True
                    cmdAnyaArticulo.Enabled = True
                    cmdEliAdj.Enabled = True
                    cmdDeshacer.Enabled = False
                End If
                If bModifAdjPrecios Then
                    cmdModifPrecios.Enabled = True
                End If
                If bModifAdjCat Then
                    cmdCambiarCat.Enabled = True
                End If
            End If
        End If
    End If
    
    If tvwCategorias.selectedItem.Children <> 0 Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        If bModifAdjPrecios Then
            cmdModifPrecios.Enabled = True
        End If
    End If
    
    If bCatBajaLog Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        cmdModifPrecios.Enabled = False
        cmdModoEdicion.Enabled = False
    End If
    
    If sdbgAdjudicaciones.col = -1 Then Exit Sub
    
    
    Select Case sdbgAdjudicaciones.Columns(sdbgAdjudicaciones.col).Name

        Case "DESPUB"
                If bModoEdicion And bModifAdj Then
                    Set frmCalendar.ctrDestination = Nothing
                    Set frmCalendar.frmDestination = frmCatalogo
                
                    frmCalendar.addtotop = 900 + 360
                    frmCalendar.addtoleft = 180
    
                    If sdbgAdjudicaciones.Columns("DESPUB").Value <> "" Then
                        frmCalendar.Calendar.Value = sdbgAdjudicaciones.Columns("DESPUB").Value
                    Else
                        frmCalendar.Calendar.Value = Date
                    End If
    
                    frmCalendar.Show 1
                    
                    If Me.Visible Then sdbgAdjudicaciones.SetFocus
                End If
                
        Case "PROCE"
                    If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
                        MostrarDetalleProceso oLineaCatalogoSeleccionada.Anyo, oLineaCatalogoSeleccionada.GMN1Cod, oLineaCatalogoSeleccionada.ProceCod
                    End If
            
        Case "ART"
                MostrarDetalleItem
        
        Case "PROVE"
                If bModoEdicion Then
                    If sdbgAdjudicaciones.Columns(sdbgAdjudicaciones.col).Locked = True Then
                        MostrarDetalleProveedor sdbgAdjudicaciones.Columns("PROVE").Value
                    Else
                        frmPROVEBuscar.sOrigen = "frmCatalogo"
                        frmPROVEBuscar.Show 1
                    End If
                Else
                    MostrarDetalleProveedor sdbgAdjudicaciones.Columns("PROVE").Value
                End If

        Case "PrecioUB"
                
                'Si la l�nea de cat�logo no proviene de una adjudicaci�n no debe de ir al detalle
                If sdbgAdjudicaciones.Columns("PROCE").Value = "" Then Exit Sub
                MostrarDetallePrecioAdj oLineaCatalogoSeleccionada.Id
        
        Case "COND_SUMINISTRO"
                If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = True Then
                    bModError = False
                    bAnyaError = False
                    bValError = False
                    
                    If sdbgAdjudicaciones.DataChanged = True Then
                        sdbgAdjudicaciones.Update
                        DoEvents
                    End If
                    
                    If bModError Or bAnyaError Or bValError Then
                        Exit Sub
                    End If
                    
                    frmCatalogoCondSuministro.g_bEdicion = bModoEdicion
                    frmCatalogoCondSuministro.Show vbModal
                Else
                    frmCatalogoCondSuministro.g_bEdicion = bModoEdicion
                    oLineaCatalogoSeleccionada.Id = oLineasCatalogo.Item(CStr(sdbgAdjudicaciones.Columns("ID").Value)).Id
                    frmCatalogoCondSuministro.Show vbModal
                End If
        Case "IMPUESTOS_BTN"
            frmImpuestos.g_sOrigen = "frmCatalogo_ImpuestosLineas"
            If Me.sdbgAdjudicaciones.Columns("ID").Value <> "" Then
                frmImpuestos.g_lAtribId = sdbgAdjudicaciones.Columns("ID").Value
            Else
                frmImpuestos.g_lAtribId = 0
            End If
            If Not IsNull(oLineaCatalogoSeleccionada.Concepto) Then
                frmImpuestos.g_ConceptoLineaCatalogo = oLineaCatalogoSeleccionada.Concepto
            End If
            frmImpuestos.PermiteModif = bModoEdicion
            frmImpuestos.g_sCaptionFormulario = sdbgAdjudicaciones.Columns("PROVE").Text & " - " & sdbgAdjudicaciones.Columns("ART").Text
            frmImpuestos.Show vbModal
                
                
        Case "SOLICIT"
            lIdSolicitud = IIf(sdbgAdjudicaciones.Columns("SOLICIT").Value = "", Null, sdbgAdjudicaciones.Columns("SOLICIT").Value)
            
            If sdbgAdjudicaciones.Columns("PROCE").Value = "" Then
            Else
                If Not IsNull(lIdSolicitud) Then
                    DetalleSolicitud lIdSolicitud
                End If
            End If
        Case "ESP"
        
                iRow = sdbgAdjudicaciones.Row

                If bModoEdicion Then

                    bAnyaError = False
                    bModError = False
                    bValError = False

                    sdbgAdjudicaciones.Update
                    DoEvents

                    If bAnyaError Or bModError Or bValError Then
                        Exit Sub
                    End If
                End If

                vBookmark = sdbgAdjudicaciones.RowBookmark(iRow)

                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If

                sdbgAdjudicaciones.Bookmark = vBookmark

                Set oLineaCatalogoSeleccionada = oLineasCatalogo.Item(CStr(sdbgAdjudicaciones.Columns("ID").Value))
                If oLineaCatalogoSeleccionada Is Nothing Then Exit Sub

                Set frmCatalogoAtribEspYAdj.g_oLinea = oLineaCatalogoSeleccionada
                
                frmCatalogoAtribEspYAdj.Show 1

                m_bRespetar = True
                If frmCatalogoAtribEspYAdj.g_oLinea.EspAdj = 1 Then
                    sdbgAdjudicaciones.Columns("ESPADJ").Value = "1"
                    sdbgAdjudicaciones.Columns("ESP").CellStyleSet "styEspAdjSi"
                Else
                    sdbgAdjudicaciones.Columns("ESPADJ").Value = "0"
                    sdbgAdjudicaciones.Columns("ESP").CellStyleSet ""
                End If
                
                DoEvents
                sdbgAdjudicaciones.Update
                If Me.Visible Then sdbgAdjudicaciones.SetFocus
                m_bRespetar = False
    Case "CAMPED_BTN", "CAMRECEP_BTN"
            iRow = sdbgAdjudicaciones.Row
            
            bAnyaError = False
            bModError = False
            bValError = False

            sdbgAdjudicaciones.Update
            DoEvents

            If bAnyaError Or bModError Or bValError Then
                Exit Sub
            End If
            vBookmark = sdbgAdjudicaciones.RowBookmark(iRow)

            If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                vBookmark = 0
            End If
            sCol = sdbgAdjudicaciones.Columns(sdbgAdjudicaciones.col).Name
            sdbgAdjudicaciones.Bookmark = vBookmark
            If oLineaCatalogoSeleccionada Is Nothing Then
                Set oLineaCatalogoSeleccionada = oLineasCatalogo.Item(CStr(sdbgAdjudicaciones.Columns("ID").Value))
            End If
            If oLineaCatalogoSeleccionada Is Nothing Then Exit Sub
            
            Set frmCatalogoCamposPed.g_oLinea = oLineaCatalogoSeleccionada
            frmCatalogoCamposPed.g_bRecep = IIf(sCol = "CAMPED_BTN", False, True)
            frmCatalogoCamposPed.Show vbModal
            Dim sNombre As String
            If sCol = "CAMPED_BTN" Then
                sNombre = "CAMPED"
            Else
                sNombre = "CAMRECEP"
            End If
            If sdbgAdjudicaciones.Columns(sNombre).Value = "1" Then
                sdbgAdjudicaciones.Columns(sNombre & "_BTN").CellStyleSet "HayUniPed"
            Else
                sdbgAdjudicaciones.Columns(sNombre).Value = "0"
                sdbgAdjudicaciones.Columns(sNombre & "_BTN").CellStyleSet ""
            End If
            sdbgAdjudicaciones.Refresh
    Case "COSTES_DESCUENTOS_BTN" 'Costes y descuentos de la linea del catalogo
            'Si el art�culo tiene tipo de recepci�n por importe no se permite la introducci�n de costes/descuentos
            If sdbgAdjudicaciones.Columns("TIPORECEPCION").Value <> 1 Then  '1=recepci�n por importe
                iRow = sdbgAdjudicaciones.Row
                
                bAnyaError = False
                bModError = False
                bValError = False
    
                sdbgAdjudicaciones.Update
                DoEvents
    
                If bAnyaError Or bModError Or bValError Then
                    Exit Sub
                End If
                vBookmark = sdbgAdjudicaciones.RowBookmark(iRow)
    
                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If
    
                sdbgAdjudicaciones.Bookmark = vBookmark
                
                If oLineaCatalogoSeleccionada Is Nothing Then
                    Set oLineaCatalogoSeleccionada = oLineasCatalogo.Item(CStr(sdbgAdjudicaciones.Columns("ID").Value))
                End If
                If oLineaCatalogoSeleccionada Is Nothing Then Exit Sub
                
                Set frmCatalogoCostesDescuentos.g_oLinea = oLineaCatalogoSeleccionada
                frmCatalogoCostesDescuentos.Show vbModal
                If sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS").Value = "1" Then
                    sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS_BTN").CellStyleSet "HayUniPed"
                Else
                    sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS").Value = "0"
                    sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS_BTN").CellStyleSet ""
                End If
                sdbgAdjudicaciones.Refresh
            End If
    End Select

End Sub


Private Sub sdbgAdjudicaciones_Change()
Dim teserror As TipoErrorSummit
Dim oLineas As CLineasCatalogo
Dim oLinea As CLineaCatalogo
Dim bPublicar As Boolean
Dim i As Long
Dim iIndice As Integer
Dim aAux As Variant
Dim lID As String
Dim bk As Variant
Dim bContinuar As Boolean

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    If bModoEdicion Then
        If cmdDeshacer.Enabled = False Then
             cmdAnyaAdjudicacion.Enabled = False
             cmdAnyaArticulo.Enabled = False
             cmdEliAdj.Enabled = False
             cmdDeshacer.Enabled = True
             cmdCambiarCat.Enabled = False
             cmdModifPrecios.Enabled = False
        End If
    End If
    
    If tvwCategorias.selectedItem.Children <> 0 Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        If bModifAdjPrecios Then
            cmdModifPrecios.Enabled = True
        End If
    End If
    
    
    With sdbgAdjudicaciones
        If GridCheckToBoolean(.Columns("PUB").Value) = False Then
            bPublicar = False
        Else
            bPublicar = True
        End If
        
        ' Si modificamos algo en el dropdown destino activamos la carga desde
        If sdbgAdjudicaciones.Columns(.col).Name = "DEST" Then
             m_bCargarComboDesdeDD = True
        End If
        
        If .col = 0 And bModifAdj Then
            If m_aIdentificadores Is Nothing Then
                Set m_aIdentificadores = New Collection
                m_aIdentificadores.Add .Bookmark
            End If
            Set oLineas = oFSGSRaiz.Generar_CLineasCatalogo
            For Each bk In m_aIdentificadores
                lID = .Columns("ID").CellValue(bk)
                If Not bPublicar And GridCheckToBoolean(.Columns("NUEVA").CellValue(bk)) = False Then
                    bContinuar = False
                    If oLineasCatalogo.Item(lID).ExistenPedidosFavoritos Then
                        If oMensajes.PreguntarEliminarPedidosFavoritos(2, oLineasCatalogo.Item(lID).ArtDen & " (" & oLineasCatalogo.Item(lID).ProveCod & ")") = vbYes Then
                            bContinuar = True
                        End If
                    Else
                        bContinuar = True
                    End If
                Else
                    bContinuar = True
                End If
                
                If Not bContinuar And m_aIdentificadores.Count = 1 Then
                    sdbgAdjudicaciones.CancelUpdate
                    sdbgAdjudicaciones.DataChanged = False
                    .Refresh
                    .Update
                    Exit Sub
                End If
                                            
                
                If bContinuar Then
                    If GridCheckToBoolean(.Columns("NUEVA").CellValue(bk)) = False Then 'Pub
                        oLineas.Add Id:=lID, Prove:=oLineasCatalogo.Item(lID).ProveCod, FecDesp:=oLineasCatalogo.Item(lID).FechaDespublicacion, FechaActual:=oLineasCatalogo.Item(lID).FECACT
                        If IsDate(.Columns("DESPUB").CellValue(bk)) Then
                            If CDate(sdbgAdjudicaciones.Columns("DESPUB").CellValue(bk)) < Date Then
                                If m_aIdentificadores.Count = 1 Then
                                    oMensajes.NoValido sIdiFechaDespub
                                    If Not bModoEdicion Then
                                        sdbgAdjudicaciones.Columns("DESPUB").Locked = False
                                        sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                                    End If
                                    sdbgAdjudicaciones.col = 1
                                    Exit Sub
                                Else
                                    oLineas.Item(lID).FechaDespublicacion = Null
                                End If
                            End If
                        End If
                    End If
                End If
            Next
            Screen.MousePointer = vbHourglass
            If oLineas.Count > 0 Then
                .Refresh
                .Update
                For Each oLinea In oLineas
                    oLinea.FECACT = oLineasCatalogo.Item(CStr(oLinea.Id)).FECACT
                Next
                Screen.MousePointer = vbNormal
                If teserror.NumError = TESOtroerror Then
                    ImposiblePublicarLineasCatalogo teserror.Arg1
                End If

                teserror = oLineas.ActivarDesactivarPublicacion(bPublicar)
                If teserror.NumError <> TESnoerror Then
                    If teserror.NumError = TESOtroerror Then
                        aAux = teserror.Arg1
                        iIndice = 1
                        For i = 0 To UBound(aAux, 2)
                            teserror.Arg1(1, i) = m_aIdentificadores(aAux(1, i) - iIndice + 1)
                            m_aIdentificadores.Remove (aAux(1, i) - iIndice + 1)
                            iIndice = iIndice + 1
                        Next
                    Else
                        Screen.MousePointer = Normal
                        TratarError teserror
                        sdbgAdjudicaciones.CancelUpdate
                        sdbgAdjudicaciones.DataChanged = False
                        Exit Sub
                    End If
                End If
                If m_aIdentificadores.Count > 0 Then
                    m_bRespetar = True
                    For Each bk In m_aIdentificadores
                        .Bookmark = bk
                        lID = .Columns("ID").Value
                        If GridCheckToBoolean(.Columns("NUEVA").CellValue(bk)) = False Then
                            If Not oLineas.Item(lID) Is Nothing Then
                                If bPublicar Then
                                    .Columns("PUB").Value = -1
                                    oLineasCatalogo.Item(lID).Publicado = True
                                    .Columns("DESPUB").Value = NullToStr(oLineas.Item(lID).FechaDespublicacion)
                                    oLineasCatalogo.Item(lID).FechaDespublicacion = oLineas.Item(lID).FechaDespublicacion
                                Else
                                    .Columns("PUB").Value = 0
                                    oLineasCatalogo.Item(lID).Publicado = False
                                End If
                                oLineasCatalogo.Item(lID).FECACT = oLineas.Item(lID).FECACT
                            End If
                        Else
                          .Columns("PUB").Value = 0
                        End If
                    Next
                    m_bRespetar = False
                End If
            Else
                If m_aIdentificadores.Count > 0 Then
                    m_bRespetar = True
                    For Each bk In m_aIdentificadores
                        .Bookmark = bk
                        If GridCheckToBoolean(.Columns("NUEVA").CellValue(bk)) = True Then
                            .Columns("PUB").Value = 0
                        End If
                    Next
                    m_bRespetar = False
                End If
            End If
            
            Set m_aIdentificadores = Nothing
            
        ElseIf .col = 3 And bModifAdj Then
            If Len(sdbgAdjudicaciones.Columns("ARTDEN").Value) + 1 > basParametros.gLongitudesDeCodigos.giLongCodDENART Then
                sdbgAdjudicaciones.Columns("ARTDEN").Value = Left(sdbgAdjudicaciones.Columns("ARTDEN").Value, basParametros.gLongitudesDeCodigos.giLongCodDENART)
            End If

        ElseIf .col = 6 And bModifAdj Then
            If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
                sdbgAdjudicaciones.CancelUpdate
                sdbgAdjudicaciones.DataChanged = False
                Exit Sub
            End If
        End If

    End With
    Screen.MousePointer = vbNormal

End Sub




Private Sub sdbgAdjudicaciones_Click()
''******************************************************************
''*   Guardamos las lineas que se han seleccionado en una coleccion
''*     para tratar las publicaciones sobre ellos en conjunto
''*
''*******************************************************************
Dim bk As Variant

    If sdbgAdjudicaciones.col = -1 Then
        If sdbgAdjudicaciones.SelBookmarks.Count > 0 Then
            Set m_aIdentificadores = New Collection
            
            For Each bk In sdbgAdjudicaciones.SelBookmarks
                m_aIdentificadores.Add bk
            Next
        End If
    End If

End Sub

''' <summary>
''' Evento que ordena la grid en base a la columna pinchada
''' </summary>
''' <remarks>Llamada desde: Tiempo m�ximo: < 1seg </remarks>
Private Sub sdbgAdjudicaciones_HeadClick(ByVal ColIndex As Integer)
    
Dim iOrden As Integer
Dim node As MSComctlLib.node

    If sdbgAdjudicaciones.Rows = 0 Then Exit Sub
    
    If bModoEdicion Then Exit Sub

    Set node = tvwCategorias.selectedItem

    If node Is Nothing Then Exit Sub
    
    ''' Volvemos a cargar las adjudicaciones, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'click'.
    
    Select Case ColIndex

        Case 0: iOrden = TipoOrdenacionCatalogo.OrdPorPub
        
        Case 1: iOrden = TipoOrdenacionCatalogo.OrdPorDespub

        Case 2, 3: iOrden = TipoOrdenacionCatalogo.OrdPorItem
        
        Case 4: iOrden = TipoOrdenacionCatalogo.OrdPorProce
         
        Case 5: iOrden = TipoOrdenacionCatalogo.OrdPorProve
        
        Case 6: iOrden = TipoOrdenacionCatalogo.OrdPorDest
        
        Case 7: iOrden = TipoOrdenacionCatalogo.OrdPorDestUsu

        Case 8: iOrden = TipoOrdenacionCatalogo.OrdPorCantAdj
    
        Case 9: iOrden = TipoOrdenacionCatalogo.OrdPorUC
    
        Case 10: iOrden = TipoOrdenacionCatalogo.OrdPorPrecio
   
        Case 11: iOrden = TipoOrdenacionCatalogo.OrdPorMon
        
        Case 12: iOrden = TipoOrdenacionCatalogo.OrdPorUP
        
        Case 13: iOrden = TipoOrdenacionCatalogo.OrdPorFC
    
        Case 15: iOrden = TipoOrdenacionCatalogo.OrdPorCantMin
    
        Case 29: iOrden = TipoOrdenacionCatalogo.OrdPorSolicit
        
    End Select
    
    If iOrden <> 0 Then
        CargarGridAdjudicaciones node, iOrden
    End If
        
End Sub

Private Sub sdbgAdjudicaciones_InitColumnProps()
    sdbgAdjudicaciones.Columns("ARTDEN").FieldLen = gLongitudesDeCodigos.giLongCodDENART
End Sub

Private Sub sdbgAdjudicaciones_KeyPress(KeyAscii As Integer)
    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
    
    If bCatBajaLog Then Exit Sub
    
    If KeyAscii = vbKeyEscape Then

        If sdbgAdjudicaciones.DataChanged = False Then

            sdbgAdjudicaciones.CancelUpdate
            sdbgAdjudicaciones.DataChanged = False

            If Not oLineaCatalogoEnEdicion Is Nothing Then
                Set oLineaCatalogoEnEdicion = Nothing
            End If

            If bModifAdj Then
                cmdAnyaAdjudicacion.Enabled = True
                cmdAnyaArticulo.Enabled = True
                cmdEliAdj.Enabled = True
                cmdDeshacer.Enabled = False
            End If
            If bModifAdjPrecios Then
                cmdModifPrecios.Enabled = True
            End If
            If bModifAdjCat Then
                cmdCambiarCat.Enabled = True
            End If
            
            Accion = ACCCatAdjudCon

            If tvwCategorias.selectedItem.Children <> 0 Then
                cmdAnyaAdjudicacion.Enabled = False
                cmdAnyaArticulo.Enabled = False
                cmdEliAdj.Enabled = False
                cmdCambiarCat.Enabled = False
                If bModifAdjPrecios Then
                    cmdModifPrecios.Enabled = True
                End If
            End If
            
            m_bCargarComboDesdeDD = False
            
        Else

            If sdbgAdjudicaciones.IsAddRow Then
                If bModifAdj Then
                    cmdAnyaAdjudicacion.Enabled = True
                    cmdAnyaArticulo.Enabled = True
                    cmdEliAdj.Enabled = True
                    cmdDeshacer.Enabled = False
                End If
                If bModifAdjPrecios Then
                    cmdModifPrecios.Enabled = True
                End If
                If bModifAdjCat Then
                    cmdCambiarCat.Enabled = True
                End If
                
                Accion = ACCCatAdjudCon
            
                If tvwCategorias.selectedItem.Children <> 0 Then
                    cmdAnyaAdjudicacion.Enabled = False
                    cmdAnyaArticulo.Enabled = False
                    cmdEliAdj.Enabled = False
                    cmdCambiarCat.Enabled = False
                    If bModifAdjPrecios Then
                        cmdModifPrecios.Enabled = True
                    End If
                End If
            End If

        End If

    End If

End Sub

Private Sub sdbgAdjudicaciones_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)


    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = True Then
        Set oLineaCatalogoSeleccionada = oLineasAAnyadir.Item(sdbgAdjudicaciones.Columns("ID").Value)
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdCambiarCat.Enabled = False
        cmdEliAdj.Enabled = True
        cmdDeshacer.Enabled = True
        cmdModifPrecios.Enabled = False
    Else
        Set oLineaCatalogoSeleccionada = oLineasCatalogo.Item(CStr(sdbgAdjudicaciones.Columns("ID").Value))
        
        If Not oLineasAAnyadir Is Nothing Then
            If oLineasAAnyadir.Count > 0 Then
                cmdAnyaAdjudicacion.Enabled = False
                cmdAnyaArticulo.Enabled = False
                cmdCambiarCat.Enabled = False
                cmdEliAdj.Enabled = True
                cmdDeshacer.Enabled = True
                cmdModifPrecios.Enabled = False
            End If
        Else
            If sdbgAdjudicaciones.DataChanged = False Then
                If bModifAdj Then
                    cmdAnyaAdjudicacion.Enabled = True
                    cmdAnyaArticulo.Enabled = True
                    cmdEliAdj.Enabled = True
                    cmdDeshacer.Enabled = False
                End If
                If bModifAdjPrecios Then
                    cmdModifPrecios.Enabled = True
                End If
                If bModifAdjCat Then
                    cmdCambiarCat.Enabled = True
                End If
            End If
        End If
    End If

    If tvwCategorias.selectedItem.Children <> 0 Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        If bModifAdjPrecios Then
            cmdModifPrecios.Enabled = True
        End If
    End If
    
    If bCatBajaLog Then
        cmdAnyaAdjudicacion.Enabled = False
        cmdAnyaArticulo.Enabled = False
        cmdEliAdj.Enabled = False
        cmdCambiarCat.Enabled = False
        cmdModifPrecios.Enabled = False
        cmdModoEdicion.Enabled = False
    End If
    
    If sdbgAdjudicaciones.col = -1 Then Exit Sub
    Select Case sdbgAdjudicaciones.Columns(sdbgAdjudicaciones.col).Name 'sdbgAdjudicaciones.col
        
        Case "PUB" 'Pub.
                If Not bCatBajaLog Then
                    If bModifAdj Then
                         sdbgAdjudicaciones.Columns("PUB").Locked = False
                    End If
                End If
        
        Case "DESPUB" 'Fecha DesPub.
                If Not bCatBajaLog Then
                    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) And bModifAdj Then
                        If bModoEdicion Then
                            sdbgAdjudicaciones.Columns("DESPUB").Locked = False
                            sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEditButton
                        Else
                            sdbgAdjudicaciones.Columns("DESPUB").Locked = True
                            sdbgAdjudicaciones.Columns("DESPUB").Style = ssStyleEdit
                        End If
                    End If
                End If
                
        Case "ART" 'Art
                If Not bCatBajaLog Then
                    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = True Then
                        sdbgAdjudicaciones.Columns("ART").Style = ssStyleEdit
                    Else
                        sdbgAdjudicaciones.Columns("ART").Style = ssStyleEditButton
                    End If
                End If
                
        Case "PROCE" 'Proce
                If Not bCatBajaLog Then
                    If sdbgAdjudicaciones.Columns("PROCE").Value = "" Then
                        sdbgAdjudicaciones.Columns("PROCE").Style = ssStyleEdit
                    Else
                        sdbgAdjudicaciones.Columns("PROCE").Style = ssStyleEditButton
                    End If
                End If
                
        Case "ARTDEN"
            If bModoEdicion And sdbgAdjudicaciones.Columns("PROCE").Value = "" Then
                If GridCheckToBoolean(sdbgAdjudicaciones.Columns("GENERICO").Value) Then
                    sdbgAdjudicaciones.Columns("ARTDEN").Locked = False
                Else
                    sdbgAdjudicaciones.Columns("ARTDEN").Locked = True
                End If
            Else
                sdbgAdjudicaciones.Columns("ARTDEN").Locked = True
            End If
       
                
        Case "PROVE" 'Prove
                If Not bCatBajaLog Then
                    If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
                        sdbgAdjudicaciones.Columns("PROVE").Locked = True
                    Else
                        sdbgAdjudicaciones.Columns("PROVE").Locked = False
                    End If
                End If
                If Not bModoEdicion Then
                    sdbgAdjudicaciones.Columns("PROVE").Locked = True
                End If
                sdbgAdjudicaciones.Columns("PROVE").Style = ssStyleEditButton
                
        Case "DEST" 'Destino
                If Not bCatBajaLog Then
                    If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
                        sdbgAdjudicaciones.Columns("DEST").Locked = True
                    ElseIf bModifAdj Then
                        sdbgAdjudicaciones.Columns("DEST").Locked = False
                    End If
                End If
                
                m_bCargarComboDesdeDD = False
                
        Case "DEST_USU" 'DestUsu
                If Not bCatBajaLog Then
                    If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
                        sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
                    ElseIf bModoEdicion = True Then
                        sdbgAdjudicaciones.Columns("DEST_USU").Locked = False
                    End If

                    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) And bModifAdj Then
                        sdbgAdjudicaciones.Columns("DEST_USU").Locked = False
                    End If
                End If
                
        Case "UB" 'Unidad Base
                If Not bCatBajaLog Then
                    If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
                        sdbgAdjudicaciones.Columns("UB").Locked = True
                    End If
                    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) And bModifAdj Then
                        sdbgAdjudicaciones.Columns("UB").Locked = False
                    End If
                    
                    If bModoEdicion And sdbgAdjudicaciones.Columns("PROCE").Value = "" Then
                        sdbgAdjudicaciones.DroppedDown = True
                        sdbddUnidades.MoveFirst
                    End If
                End If
                
        Case "PrecioUB" 'Precio
                If Not bCatBajaLog Then
                    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) Then
                        sdbgAdjudicaciones.Columns("PrecioUB").Locked = False
                        sdbgAdjudicaciones.Columns("PrecioUB").Style = ssStyleEdit
                    Else
                        If Not bModoEdicion Then
                            sdbgAdjudicaciones.Columns("PrecioUB").Locked = True
                            sdbgAdjudicaciones.Columns("PrecioUB").Style = ssStyleEditButton
                        End If
                    End If
                    If Not bModifAdjPrecios Then
                        sdbgAdjudicaciones.Columns("PrecioUB").Locked = True
                    End If
                    If sdbgAdjudicaciones.Columns("PrecioUB").Locked Then
                        sdbgAdjudicaciones.Columns("PrecioUB").Style = ssStyleEditButton
                    End If
                End If
                
        Case "UP" 'Unidad Pedido
                If Not bCatBajaLog Then
                    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) And bModifAdj Then
                        sdbgAdjudicaciones.Columns("PrecioUB").Locked = False
                    End If
                End If
                
        Case "FC" 'FC
                If Not bCatBajaLog Then
                    If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) And bModifAdj Then
                        sdbgAdjudicaciones.Columns("FC").Locked = False
                    End If
                End If
                
        Case "OTRAS" 'Otras
        
        Case "CANTMIN" 'Cant. min.
    
                If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) And bModifAdj Then
                    sdbgAdjudicaciones.Columns("CANTMIN").Locked = False
                End If
                
        Case "SOLICIT"  'Solicitud
            sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
            If sdbgAdjudicaciones.Columns("PROCE").Value <> "" Then
                sdbgAdjudicaciones.Columns("SOLICIT").Style = ssStyleEditButton
            Else
                sdbgAdjudicaciones.Columns("SOLICIT").Style = ssStyleEdit
            End If
    End Select
End Sub


Public Sub CargarProveedorConBusqueda()
Dim oProves As CProveedores

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbgAdjudicaciones.Columns("PROVE").Value = oProves.Item(1).Cod
End Sub

Private Sub sdbgAdjudicaciones_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer

    If Not IsNull(Bookmark) And Not IsEmpty(Bookmark) Then
        If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = True Or (sdbgAdjudicaciones.Columns("PRECIOUB").Value = "") Then
        'La l�nea a�n no est� a�adida a la BD
            For i = sdbgAdjudicaciones.Columns("DESPUB").Position To sdbgAdjudicaciones.Columns("UB").Position
                sdbgAdjudicaciones.Columns(i).CellStyleSet ("Rojo")
            Next
            For i = sdbgAdjudicaciones.Columns("MON").Position To sdbgAdjudicaciones.Columns("OTRAS").Position
                sdbgAdjudicaciones.Columns(i).CellStyleSet ("Rojo")
            Next
            sdbgAdjudicaciones.Columns("SOLICIT").CellStyleSet ("Rojo")
            #If VERSION >= 30600 Then
                sdbgAdjudicaciones.Columns("MON").Locked = False
            #End If
            Exit Sub
        End If
        
        'Icono de Unidades de Pedido
        If sdbgAdjudicaciones.Columns("HAYUNIPED").Value = "1" Then
            sdbgAdjudicaciones.Columns("OTRAS").CellStyleSet "HayUniPed"
        Else
            sdbgAdjudicaciones.Columns("OTRAS").CellStyleSet "Normal"
        End If
        
        'Campos de pedido(Atributos)
        If sdbgAdjudicaciones.Columns("CAMPED").Value = "1" Then
            sdbgAdjudicaciones.Columns("CAMPED_BTN").CellStyleSet "HayUniPed" 'Se pone el mismo icono que Unidades de pedido
        Else
            sdbgAdjudicaciones.Columns("CAMPED_BTN").CellStyleSet "Normal"
        End If
        
        'Campos de Recepci�n (Atributos)
        If sdbgAdjudicaciones.Columns("CAMRECEP").Value = "1" Then
            sdbgAdjudicaciones.Columns("CAMRECEP_BTN").CellStyleSet "HayUniPed" 'Se pone el mismo icono que Unidades de pedido
        Else
            sdbgAdjudicaciones.Columns("CAMRECEP_BTN").CellStyleSet "Normal"
        End If
        
        'Impuestos
        If sdbgAdjudicaciones.Columns("IMPUESTOS_HIDDEN").Value = True Then
            sdbgAdjudicaciones.Columns("IMPUESTOS_BTN").CellStyleSet "HayUniPed" 'Se pone el mismo icono que Unidades de pedido
        Else
            sdbgAdjudicaciones.Columns("IMPUESTOS_BTN").CellStyleSet "Normal"
        End If
        
        'Costes/Descuentos(Atributos)
        If sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS").Value = "1" Then
            sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS_BTN").CellStyleSet "HayUniPed" 'Se pone el mismo icono que Unidades de pedido
        Else
            sdbgAdjudicaciones.Columns("COSTES_DESCUENTOS_BTN").CellStyleSet "Normal"
        End If
        
        Me.sdbgAdjudicaciones.Columns("COND_SUMINISTRO").CellStyleSet "HayUniPed"
        
        If GridCheckToBoolean(sdbgAdjudicaciones.Columns("NUEVA").Value) = False Then
            If sdbgAdjudicaciones.Columns("ESPADJ").Value = "0" Then
                sdbgAdjudicaciones.Columns("ESP").CellStyleSet "Normal"
            Else
                sdbgAdjudicaciones.Columns("ESP").CellStyleSet "HayEsp"
            End If
        End If
        
        If sdbgAdjudicaciones.Columns("PROCE").Value = "" Then
            If bModoEdicion Then
                If Not bCatBajaLog Then
                    sdbgAdjudicaciones.Columns("PUB").Locked = False
                    sdbgAdjudicaciones.Columns("DESPUB").Locked = False
                    sdbgAdjudicaciones.Columns("MON").Locked = False
                    sdbgAdjudicaciones.Columns("UP").Locked = False
                    sdbgAdjudicaciones.Columns("FC").Locked = False
                    sdbgAdjudicaciones.Columns("OTRAS").Locked = False
                    sdbgAdjudicaciones.Columns("CANTMIN").Locked = False
                    If bModifAdjPrecios Then
                        sdbgAdjudicaciones.Columns("PrecioUB").Locked = False
                    End If
                    sdbgAdjudicaciones.Columns("SOLICIT").Locked = False
                    sdbgAdjudicaciones.Columns("CANTADJ").Locked = False
                    'Enganchar las dropdown
                    sdbgAdjudicaciones.Columns("DEST").DropDownHwnd = sdbddDestinos.hWnd
                    sdbgAdjudicaciones.Columns("UB").DropDownHwnd = sdbddUnidades.hWnd
                    sdbgAdjudicaciones.Columns("UP").DropDownHwnd = sdbddUnidadesPedido.hWnd
                    sdbgAdjudicaciones.Columns("MON").DropDownHwnd = sdbddMonedas.hWnd
                    End If
            End If
            If GridCheckToBoolean(sdbgAdjudicaciones.Columns("PUB").Value) = False Then
                sdbgAdjudicaciones.Columns("PUB").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("DESPUB").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("MON").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("UP").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("FC").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("CANTMIN").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("PROCE").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("ART").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("ARTDEN").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("PrecioUB").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("PROVE").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("DEST").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("DEST_USU").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("CANTADJ").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("SOLICIT").CellStyleSet ("GrisOscuro")
                

            Else
                sdbgAdjudicaciones.Columns("PUB").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("DESPUB").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("MON").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("UP").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("FC").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("CANTMIN").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("PROCE").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("ART").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("ARTDEN").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("PROVE").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("DEST").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("DEST_USU").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("CANTADJ").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("SOLICIT").CellStyleSet ("Blanco")
            End If
        
        Else
        'La l�nea proviene de una adj.
            Me.sdbgAdjudicaciones.Columns("COND_SUMINISTRO").CellStyleSet "HayUniPed"
            If bModoEdicion Then
                sdbgAdjudicaciones.Columns("PUB").Locked = False
                sdbgAdjudicaciones.Columns("DESPUB").Locked = False
                sdbgAdjudicaciones.Columns("PROVE").Locked = False
                sdbgAdjudicaciones.Columns("DEST").Locked = False
                sdbgAdjudicaciones.Columns("DEST_USU").Locked = False
                sdbgAdjudicaciones.Columns("UB").Locked = False
                sdbgAdjudicaciones.Columns("MON").Locked = False
                sdbgAdjudicaciones.Columns("UP").Locked = False
                sdbgAdjudicaciones.Columns("FC").Locked = False
                sdbgAdjudicaciones.Columns("OTRAS").Locked = False
                sdbgAdjudicaciones.Columns("CANTMIN").Locked = False
                sdbgAdjudicaciones.Columns("PrecioUB").Locked = False
                sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
                 'Enganchar las dropdown
                sdbgAdjudicaciones.Columns("DEST").DropDownHwnd = sdbddDestinos.hWnd
                sdbgAdjudicaciones.Columns("UB").DropDownHwnd = sdbddUnidades.hWnd
                sdbgAdjudicaciones.Columns("UP").DropDownHwnd = sdbddUnidadesPedido.hWnd
        
                sdbgAdjudicaciones.Columns("MON").DropDownHwnd = sdbddMonedas.hWnd
        
            End If
            
            If GridCheckToBoolean(sdbgAdjudicaciones.Columns("PUB").Value) = False Then
                sdbgAdjudicaciones.Columns("PUB").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("DESPUB").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("GrisSuave")
        
                sdbgAdjudicaciones.Columns("MON").CellStyleSet ("GrisSuave")
        
                sdbgAdjudicaciones.Columns("UP").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("FC").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("CANTMIN").CellStyleSet ("GrisSuave")
                sdbgAdjudicaciones.Columns("PROCE").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("ART").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("ARTDEN").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("PrecioUB").CellStyleSet ("GrisOscuro")
                If FSEPConf Then
                    sdbgAdjudicaciones.Columns("PROVE").CellStyleSet ("GrisSuave")
                    sdbgAdjudicaciones.Columns("PROVE").Locked = True
                    sdbgAdjudicaciones.Columns("DEST").CellStyleSet ("GrisSuave")
                    sdbgAdjudicaciones.Columns("DEST").Locked = True
                    sdbgAdjudicaciones.Columns("DEST_USU").CellStyleSet ("GrisSuave")
                    sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
                Else
                    sdbgAdjudicaciones.Columns("PROVE").CellStyleSet ("GrisOscuro")
                    sdbgAdjudicaciones.Columns("PROVE").Locked = True
                    sdbgAdjudicaciones.Columns("DEST").CellStyleSet ("GrisOscuro")
                    sdbgAdjudicaciones.Columns("DEST").Locked = True
                    sdbgAdjudicaciones.Columns("DEST_USU").CellStyleSet ("GrisOscuro")
                    sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
                End If
                sdbgAdjudicaciones.Columns("CANTADJ").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("GrisOscuro")
                sdbgAdjudicaciones.Columns("UB").Locked = True
                sdbgAdjudicaciones.Columns("SOLICIT").CellStyleSet ("GrisOscuro")
            Else
                sdbgAdjudicaciones.Columns("PUB").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("DESPUB").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("MON").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("UP").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("FC").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("CANTMIN").CellStyleSet ("Blanco")
                sdbgAdjudicaciones.Columns("PROCE").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("ART").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("ARTDEN").CellStyleSet ("Azul")
                If FSEPConf Then
                    sdbgAdjudicaciones.Columns("PROVE").CellStyleSet ("Blanco")
                    sdbgAdjudicaciones.Columns("PROVE").Locked = True
                    sdbgAdjudicaciones.Columns("DEST").CellStyleSet ("Blanco")
                    sdbgAdjudicaciones.Columns("DEST").Locked = True
                    sdbgAdjudicaciones.Columns("DEST_USU").CellStyleSet ("Blanco")
                    sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
                Else
                    sdbgAdjudicaciones.Columns("PROVE").CellStyleSet ("Azul")
                    sdbgAdjudicaciones.Columns("DEST").CellStyleSet ("Azul")
                    sdbgAdjudicaciones.Columns("DEST").Locked = True
                    sdbgAdjudicaciones.Columns("DEST_USU").CellStyleSet ("Azul")
                    sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
                End If
                sdbgAdjudicaciones.Columns("CANTADJ").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
                sdbgAdjudicaciones.Columns("UB").CellStyleSet ("Azul")
                sdbgAdjudicaciones.Columns("UB").Locked = True
                sdbgAdjudicaciones.Columns("SOLICIT").CellStyleSet ("Azul")
            End If

        End If
    End If
    
End Sub

Private Sub sdbgCamposPedido_BeforeUpdate(Cancel As Integer)
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub sdbgCamposPedido_Change()
    If sdbgCamposPedido.col > -1 Then
        Dim NombreColumna As String
        NombreColumna = Me.sdbgCamposPedido.Columns(Me.sdbgCamposPedido.col).Name
        If NombreColumna = "INT" Or NombreColumna = "OBL" Or NombreColumna = "VERENRECEP" Then
            'Lanzamos el evento update en las columnas "check" sin esperar al evento update de la celda
            Me.sdbgCamposPedido.Update
        End If
    End If
End Sub

Private Sub sdbgCamposPedido_RowLoaded(ByVal Bookmark As Variant)
sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
If sdbgCamposPedido.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoArchivo Then
    sdbgCamposPedido.Columns("VAL").CellStyleSet "Gris"
End If
End Sub

Private Sub sdbgCamposRecepcion_AfterColUpdate(ByVal ColIndex As Integer)
bModError = False
''' Modificamos en la base de datos
''Comprobacion de atributos de oferta obligatorios
With sdbgCamposRecepcion
    Dim tsError As TipoErrorSummit
    Dim iObl As Integer
    Dim iInt As Integer
    Select Case .Columns("INT").Value
        Case "False"
            iInt = 0
        Case "-1"
            iInt = 1
    End Select
    Select Case .Columns("OBL").Value
        Case "False"
            iObl = 0
        Case "-1"
            iObl = 1
    End Select
    If .Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
        tsError = m_oCamposR.ActualizarCampo(.Columns("ID").Value, iNivel, iObl, IIf(.Columns("VAL").Value = sIdiTrue, 1, IIf(.Columns("VAL").Value = sIdiFalse, 0, "")), .Columns("AMB").Value, iInt, .Columns("TIPO_DATOS").Value, 0)
    Else
        tsError = m_oCamposR.ActualizarCampo(.Columns("ID").Value, iNivel, iObl, .Columns("VAL").Value, .Columns("AMB").Value, iInt, .Columns("TIPO_DATOS").Value, 0)
    End If
    Dim oCat As Object
    Set oCat = DevolverCategoria
    Set m_oCamposR = Nothing
    Set m_oCamposR = oFSGSRaiz.Generar_CCampos
    m_oCamposR.CargarDatos oCat.Id, iNivel
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
        If Me.Visible Then .SetFocus
        .DataChanged = False
    End If
End With
End Sub

Private Sub sdbgCamposRecepcion_AfterDelete(RtnDispErrMsg As Integer)
RtnDispErrMsg = 0
If IsEmpty(sdbgCamposRecepcion.RowBookmark(sdbgCamposRecepcion.Row)) Then
    sdbgCamposRecepcion.Bookmark = sdbgCamposRecepcion.RowBookmark(sdbgCamposRecepcion.Row - 1)
Else
    sdbgCamposRecepcion.Bookmark = sdbgCamposRecepcion.RowBookmark(sdbgCamposRecepcion.Row)
End If
End Sub


Private Sub sdbgCamposRecepcion_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)

Cancel = False
If sdbgCamposRecepcion.Columns("VAL").Text <> "" Then
    Select Case UCase(sdbgCamposRecepcion.Columns("TIPO_DATOS").Text)
        Case 2 'Numero
            If (Not IsNumeric(sdbgCamposRecepcion.Columns("VAL").Text)) Then
                oMensajes.AtributoValorNoValido ("TIPO2")
                Cancel = True
                GoTo Salir
            Else
                If sdbgCamposRecepcion.Columns("MIN").Text <> "" And sdbgCamposRecepcion.Columns("MAX").Text <> "" Then
                    If StrToDbl0(sdbgCamposRecepcion.Columns("MIN").Text) > StrToDbl0(sdbgCamposRecepcion.Columns("VAL").Text) Or StrToDbl0(sdbgCamposRecepcion.Columns("MAX").Text) < StrToDbl0(sdbgCamposRecepcion.Columns("VAL").Text) Then
                        oMensajes.ValorEntreMaximoYMinimo sdbgCamposRecepcion.Columns("MIN").Text, sdbgCamposRecepcion.Columns("MAX").Text
                        Cancel = True
                        GoTo Salir
                    End If
                End If
            End If
        Case 3 'Fecha
            If (Not IsDate(sdbgCamposRecepcion.Columns("VAL").Text) And sdbgCamposRecepcion.Columns("VAL").Text <> "") Then
                oMensajes.AtributoValorNoValido ("TIPO3")
                Cancel = True
                GoTo Salir
            Else
                If sdbgCamposRecepcion.Columns("MIN").Text <> "" And sdbgCamposRecepcion.Columns("MAX").Text <> "" Then
                    If CDate(sdbgCamposRecepcion.Columns("MIN").Text) > CDate(sdbgCamposRecepcion.Columns("VAL").Text) Or CDate(sdbgCamposRecepcion.Columns("MAX").Text) < CDate(sdbgCamposRecepcion.Columns("VAL").Text) Then
                        oMensajes.ValorEntreMaximoYMinimo sdbgCamposRecepcion.Columns("MIN").Text, sdbgCamposRecepcion.Columns("MAX").Text
                        Cancel = True
                        GoTo Salir
                    End If
                End If
            End If
    End Select
End If
    sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
Exit Sub
Salir:
    If Me.Visible Then sdbgCamposRecepcion.SetFocus
End Sub

Private Sub sdbgCamposRecepcion_BeforeUpdate(Cancel As Integer)
sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub sdbgCamposRecepcion_BtnClick()
If sdbgCamposRecepcion.col < 0 Then Exit Sub

    If sdbgCamposRecepcion.Columns(sdbgCamposRecepcion.col).Name = "VAL" Then

        frmATRIBDescr.g_bEdicion = True
        frmATRIBDescr.caption = sdbgCamposRecepcion.Columns("COD").Value & ": " & sdbgCamposRecepcion.Columns("DEN").Value
        frmATRIBDescr.txtDescr.Text = sdbgCamposRecepcion.Columns(sdbgCamposRecepcion.col).Value
        frmATRIBDescr.g_sOrigen = "frmCatalogo_Recep"
        frmATRIBDescr.Show 1

        If sdbgCamposRecepcion.DataChanged Then
            sdbgCamposRecepcion.Update
        End If
    End If
End Sub

Private Sub sdbgCamposRecepcion_Change()
    If sdbgCamposRecepcion.col > -1 Then
        Dim NombreColumna As String
        NombreColumna = Me.sdbgCamposRecepcion.Columns(Me.sdbgCamposRecepcion.col).Name
        If NombreColumna = "INT" Or NombreColumna = "OBL" Then
            'Lanzamos el evento update en las columnas "check" sin esperar al evento update de la celda
            Me.sdbgCamposRecepcion.Update
        End If
    End If
End Sub


Private Sub sdbgCamposRecepcion_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyEscape Then
    If sdbgCamposRecepcion.DataChanged = False Then
        sdbgCamposRecepcion.CancelUpdate
        sdbgCamposRecepcion.DataChanged = False
    End If
End If
End Sub

Private Sub sdbgCamposRecepcion_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
sdbgCamposRecepcion.Columns("VAL").Locked = False
If sdbgCamposRecepcion.Columns("TIPO_INTRODUCCION").Value = 0 Then 'Libre
    If sdbgCamposRecepcion.Columns("TIPO_DATOS").Value = TipoBoolean Then
        sdbddValorR.RemoveAll
        sdbddValorR.AddItem ""
        sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = sdbddValorR.hWnd
        sdbddValorR.Enabled = True
    Else
        sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
        sdbddValorR.Enabled = False
        If sdbgCamposRecepcion.Columns("TIPO_DATOS").Value = 1 Then
            sdbgCamposRecepcion.Columns("VAL").Style = ssStyleEditButton
        ElseIf sdbgCamposRecepcion.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoArchivo Then
            sdbgCamposRecepcion.Columns("VAL").Locked = True
        End If
    End If
Else 'Lista
    sdbddValorR.RemoveAll
    sdbddValorR.AddItem ""
    If Not sdbgCamposRecepcion.Columns("LISTA_EXTERNA").Value Then
        sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = sdbddValorR.hWnd
        sdbddValorR.Enabled = True
        sdbddValorR.DroppedDown = True
        sdbddValorR_DropDown
        sdbddValorR.DroppedDown = True
    Else
        sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
        sdbddValorR.Enabled = False
        sdbgCamposRecepcion.Columns("VAL").Locked = True
    End If
End If
End Sub

Private Sub sdbgCamposRecepcion_RowLoaded(ByVal Bookmark As Variant)
sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
If sdbgCamposRecepcion.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoArchivo Then
    sdbgCamposRecepcion.Columns("VAL").CellStyleSet "Gris"
End If
End Sub

Private Sub sdbgCamposRecepcion_Scroll(Cancel As Integer)
sdbgCamposRecepcion.Columns("VAL").DropDownHwnd = 0
End Sub

Private Sub sdbgProves_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Public Sub tabCatalogo_Click(PreviousTab As Integer)
Dim nodx As MSComctlLib.node
Dim sCategoria As String

    If m_bModoEdicion Then
        tabCatalogo.Tab = 0
        Exit Sub
    End If

    If PreviousTab = 1 Then

        If bModoEdicion Then
            DesactivarTabClick = True
            tabCatalogo.Tab = 1
            DesactivarTabClick = False
            Exit Sub
        End If

        sdbgAdjudicaciones.Visible = False
        If Me.Visible Then tvwCategorias.SetFocus
        picNavigateAdj.Enabled = False
        picNavigateCat.Enabled = True
        cmdResCat.Enabled = True
        cmdBuscarCat.Enabled = True
        cmdListado.Enabled = True
        
        ConfigurarInterfazSeguridad tvwCategorias.selectedItem
    
    Else

        Set nodx = tvwCategorias.selectedItem

        If nodx Is Nothing Then Exit Sub
        
        If nodx.Tag = "Raiz" Then
            DesactivarTabClick = True
            tabCatalogo.Tab = 0
            DesactivarTabClick = False
            Exit Sub
        End If
        
        picNavigateCat.Enabled = False
        picNavigateAdj.Enabled = True
        cmdBuscarAdj.Enabled = True
        
        Select Case Left(nodx.Tag, 4)
            Case "CAT1"
                        sCategoria = nodx.Text
                        CAT1Seleccionada = DevolverId(nodx)
                        CAT2Seleccionada = Null
                        CAT3Seleccionada = Null
                        CAT4Seleccionada = Null
                        CAT5Seleccionada = Null
                        SeguridadCat = DevolverSeguridadCategoria(nodx)
                        
            Case "CAT2"
                        sCategoria = nodx.Parent.Text & ", " & nodx.Text
                        CAT1Seleccionada = DevolverId(nodx.Parent)
                        CAT2Seleccionada = DevolverId(nodx)
                        CAT3Seleccionada = Null
                        CAT4Seleccionada = Null
                        CAT5Seleccionada = Null
                        SeguridadCat = DevolverSeguridadCategoria(nodx)
                        If SeguridadCat = 0 Then
                            SeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                        End If

            
            Case "CAT3"
                        sCategoria = nodx.Parent.Parent.Text & ", " & nodx.Parent.Text & ", " & nodx.Text
                        CAT1Seleccionada = DevolverId(nodx.Parent.Parent)
                        CAT2Seleccionada = DevolverId(nodx.Parent)
                        CAT3Seleccionada = DevolverId(nodx)
                        CAT4Seleccionada = Null
                        CAT5Seleccionada = Null
                        SeguridadCat = DevolverSeguridadCategoria(nodx)
                        If SeguridadCat = 0 Then
                            SeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                            If SeguridadCat = 0 Then
                                SeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent)
                            End If
                        End If
            
            Case "CAT4"
                        sCategoria = nodx.Parent.Parent.Parent.Text & ", " & nodx.Parent.Parent.Text & ", " & nodx.Parent.Text & ", " & nodx.Text
                        CAT1Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                        CAT2Seleccionada = DevolverId(nodx.Parent.Parent)
                        CAT3Seleccionada = DevolverId(nodx.Parent)
                        CAT4Seleccionada = DevolverId(nodx)
                        CAT5Seleccionada = Null
                        SeguridadCat = DevolverSeguridadCategoria(nodx)
                        If SeguridadCat = 0 Then
                            SeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                            If SeguridadCat = 0 Then
                                SeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent)
                                If SeguridadCat = 0 Then
                                    SeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent.Parent)
                                End If
                            End If
                        End If
            
            Case "CAT5"
                        sCategoria = nodx.Parent.Parent.Parent.Parent.Text & ", " & nodx.Parent.Parent.Parent.Text & ", " & nodx.Parent.Parent.Text & ", " & nodx.Parent.Text & ", " & nodx.Text
                        CAT1Seleccionada = DevolverId(nodx.Parent.Parent.Parent.Parent)
                        CAT2Seleccionada = DevolverId(nodx.Parent.Parent.Parent)
                        CAT3Seleccionada = DevolverId(nodx.Parent.Parent)
                        CAT4Seleccionada = DevolverId(nodx.Parent)
                        CAT5Seleccionada = DevolverId(nodx)
                        SeguridadCat = DevolverSeguridadCategoria(nodx)
                        If SeguridadCat = 0 Then
                            SeguridadCat = DevolverSeguridadCategoria(nodx.Parent)
                            If SeguridadCat = 0 Then
                                SeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent)
                                If SeguridadCat = 0 Then
                                    SeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent.Parent)
                                    If SeguridadCat = 0 Then
                                        SeguridadCat = DevolverSeguridadCategoria(nodx.Parent.Parent.Parent.Parent)
                                    End If
                                End If
                            End If
                        End If
        
        End Select
        lblCategoria = sIdiCategoria & ": " & sCategoria

        CargarGridAdjudicaciones nodx, 5
        sdbgAdjudicaciones.MoveFirst

        'S�lo aparece el bot�n de detalle si el usuario tiene permiso de consulta sobre Apertura de procesos
        If oUsuarioSummit.Tipo <> Administrador Then
            If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing Then
                sdbgAdjudicaciones.Columns("PROCE").Style = ssStyleEdit
            Else
                sdbgAdjudicaciones.Columns("PROCE").Style = ssStyleEditButton
            End If
        Else
            sdbgAdjudicaciones.Columns("PROCE").Style = ssStyleEditButton
        End If
        
        sdbgAdjudicaciones.Visible = True
        sdbgAdjudicaciones.ReBind

        'Si es baja l�gica o marcado para baja l�gica no se pueden modificar las l�neas de cat�logo
        If nodx.Image = "BAJALOG1" Or nodx.Image = "BAJALOG2" Or nodx.Image = "BAJASEG" Then
            bCatBajaLog = True
            cmdAnyaAdjudicacion.Enabled = False
            cmdAnyaArticulo.Enabled = False
            cmdEliAdj.Enabled = False
            cmdCambiarCat.Enabled = False
            cmdModifPrecios.Enabled = False
            cmdModoEdicion.Enabled = False
        Else
            cmdModoEdicion.Enabled = True
            bCatBajaLog = False
        End If
        
        If nodx.Children <> 0 Then
            cmdAnyaAdjudicacion.Enabled = False
            cmdAnyaArticulo.Enabled = False
            cmdCambiarCat.Enabled = False
        End If
        
        If bRConProceAper Then
            sdbgAdjudicaciones.Columns("PROCE").Style = ssStyleEditButton
        End If
        
        If bModoEdicion Then
            If bModifAdj Then
                'Cambiamos este for por la columnas una a una
                ' bloquear, ESO FACILITARA LAS POSIBLES MODIFICACIONES

                sdbgAdjudicaciones.Columns("PUB").Locked = True
                sdbgAdjudicaciones.Columns("DESPUB").Locked = True
                sdbgAdjudicaciones.Columns("ART").Locked = True
                sdbgAdjudicaciones.Columns("PROCE").Locked = True
                sdbgAdjudicaciones.Columns("PROVE").Locked = True
                sdbgAdjudicaciones.Columns("DEST").Locked = True
                sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
                sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
                sdbgAdjudicaciones.Columns("UB").Locked = True
                sdbgAdjudicaciones.Columns("PRECIOUB").Locked = True
                sdbgAdjudicaciones.Columns("MON").Locked = True
                sdbgAdjudicaciones.Columns("UP").Locked = True
                sdbgAdjudicaciones.Columns("FC").Locked = True
                sdbgAdjudicaciones.Columns("OTRAS").Locked = True
                
                sdbgAdjudicaciones.Columns("PROCE").Locked = True
                sdbgAdjudicaciones.Columns("ART").Locked = True
                sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
                sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
                If Not bModifAdjPrecios Then
                    sdbgAdjudicaciones.Columns("PrecioUB").Locked = True
                End If
            Else
                'Cambiamos este for por la columnas una a una
                ' bloquear, ESO FACILITARA LAS POSIBLES MODIFICACIONES
                
                sdbgAdjudicaciones.Columns("PUB").Locked = True
                sdbgAdjudicaciones.Columns("DESPUB").Locked = True
                sdbgAdjudicaciones.Columns("ART").Locked = True
                sdbgAdjudicaciones.Columns("PROCE").Locked = True
                sdbgAdjudicaciones.Columns("PROVE").Locked = True
                sdbgAdjudicaciones.Columns("DEST").Locked = True
                sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
                sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
                sdbgAdjudicaciones.Columns("UB").Locked = True
                sdbgAdjudicaciones.Columns("PRECIOUB").Locked = True
                sdbgAdjudicaciones.Columns("MON").Locked = True
                sdbgAdjudicaciones.Columns("UP").Locked = True
                sdbgAdjudicaciones.Columns("FC").Locked = True
                sdbgAdjudicaciones.Columns("OTRAS").Locked = True
                sdbgAdjudicaciones.Columns("CANTMIN").Locked = True
                 
                sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
                If bModifAdjPrecios Then
                    sdbgAdjudicaciones.Columns("PrecioUB").Locked = False
                End If
            End If
        
        Else
            'Cambiamos este for por la columnas una a una
                ' bloquear, ESO FACILITARA LAS POSIBLES MODIFICACIONES
                
            sdbgAdjudicaciones.Columns("PUB").Locked = True
            sdbgAdjudicaciones.Columns("DESPUB").Locked = True
            sdbgAdjudicaciones.Columns("ART").Locked = True
            sdbgAdjudicaciones.Columns("PROCE").Locked = True
            sdbgAdjudicaciones.Columns("PROVE").Locked = True
            sdbgAdjudicaciones.Columns("DEST").Locked = True
            sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
            sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
            sdbgAdjudicaciones.Columns("UB").Locked = True
            sdbgAdjudicaciones.Columns("PRECIOUB").Locked = True
            sdbgAdjudicaciones.Columns("MON").Locked = True
            sdbgAdjudicaciones.Columns("UP").Locked = True
            sdbgAdjudicaciones.Columns("FC").Locked = True
            sdbgAdjudicaciones.Columns("OTRAS").Locked = True
            sdbgAdjudicaciones.Columns("CANTMIN").Locked = True
            
            sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
            
            If Not bModifAdj Then
                sdbgAdjudicaciones.Columns("PUB").Locked = True
            End If
        End If

        If bCatBajaLog Then
            'Cambiamos este for por la columnas una a una
            ' bloquear, ESO FACILITARA LAS POSIBLES MODIFICACIONES

            sdbgAdjudicaciones.Columns("PUB").Locked = True
            sdbgAdjudicaciones.Columns("DESPUB").Locked = True
            sdbgAdjudicaciones.Columns("ART").Locked = True
            sdbgAdjudicaciones.Columns("PROCE").Locked = True
            sdbgAdjudicaciones.Columns("PROVE").Locked = True
            sdbgAdjudicaciones.Columns("DEST").Locked = True
            sdbgAdjudicaciones.Columns("DEST_USU").Locked = True
            sdbgAdjudicaciones.Columns("CANTADJ").Locked = True
            sdbgAdjudicaciones.Columns("UB").Locked = True
            sdbgAdjudicaciones.Columns("PRECIOUB").Locked = True
            sdbgAdjudicaciones.Columns("MON").Locked = True
            sdbgAdjudicaciones.Columns("UP").Locked = True
            sdbgAdjudicaciones.Columns("FC").Locked = True
            sdbgAdjudicaciones.Columns("OTRAS").Locked = True
          
            sdbgAdjudicaciones.Columns("SOLICIT").Locked = True
        End If
    
    End If

End Sub

Private Sub tabDatos_Click(PreviousTab As Integer)
If m_bModoEdicion Then
    tabDatos.Tab = 1
End If
End Sub


Private Sub tvwCategorias_Collapse(ByVal node As MSComctlLib.node)
  ConfigurarInterfazSeguridad node
End Sub

Private Sub tvwCategorias_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim nodx As MSComctlLib.node


    If Button = 2 Then
    
        Set nodx = tvwCategorias.selectedItem
        
        If Not nodx Is Nothing Then

            Select Case Left(nodx.Tag, 4)
            
                Case "Raiz"
                        
                        If bModifCat Then
                            PopupMenu MDI.mnuPopUpCatCatalogo
                        End If
                    
                Case "CAT1", "CAT2", "CAT3", "CAT4"

                        If Not bModifCat And Not bModifBajasLog Then
                            If bConsultSegurCat And Not bModifSegurCat Then
                                If nodx.Image <> "SEGURO" And nodx.Image <> "BAJASEG" And nodx.Image <> "CAT_INT" Then
                                    Exit Sub
                                End If
                            End If
                        End If
                            
                        If bModifCat Or bConsultSegurCat Then
                            PopupMenu MDI.mnuPopUpCatCatalogo
                        End If
                                                    
                Case "CAT5"
                            
                        If Not bModifCat And Not bModifBajasLog Then
                            If bConsultSegurCat And Not bModifSegurCat Then
                                If nodx.Image <> "SEGURO" And nodx.Image <> "BAJASEG" And nodx.Image <> "CAT_INT" Then
                                    Exit Sub
                                End If
                            End If
                        End If
                        
                        If bModifCat Or bConsultSegurCat Then
                            PopupMenu MDI.mnuPopUpCatCatalogo
                        End If
            End Select
        
        End If
    
    End If

End Sub

Public Sub tvwCategorias_NodeClick(ByVal node As MSComctlLib.node)
    ConfigurarInterfazSeguridad node
    
    If node.Tag <> "Raiz" Then
        tabCatalogo.TabVisible(1) = True
        
        'Muestro los adjuntos y proveedores
        If bModifCat Then txtObsAdjun.Locked = False
        cmdAnyaCat(1).Enabled = True
        cmdEliCat(1).Enabled = True
        MostrarDatosDeCategoria
        
        If node.Children <> 0 Then
            cmdAnyaAdjudicacion.Enabled = False
            cmdAnyaArticulo.Enabled = False
            cmdEliAdj.Enabled = False
            cmdCambiarCat.Enabled = False
            
            tabDatos.TabVisible(1) = False
            tabDatos.TabVisible(2) = False
            tabDatos.TabVisible(3) = False
            tabDatos.TabVisible(4) = False
            tabDatos.TabVisible(5) = False
        Else
            tabDatos.Tab = 0
            tabDatos.TabVisible(1) = True
            tabDatos.TabVisible(2) = True
            tabDatos.TabVisible(3) = True
            tabDatos.TabVisible(4) = True
            tabDatos.TabVisible(5) = True
            If bModifAdj Then
                cmdAnyaAdjudicacion.Enabled = True
                cmdAnyaArticulo.Enabled = True
                cmdEliAdj.Enabled = True
            End If
            If bModifAdjCat Then
                cmdCambiarCat.Enabled = True
            End If
        End If
        If node.Image = "BAJALOG2" Or node.Image = "BAJALOG1" Or node.Image = "BAJASEG" Then
            txtObsAdjun.Locked = True
            cmdA�adirAdjun.Enabled = False
            cmdEliminarAdjun.Enabled = False
            cmdModificarAdjun.Enabled = False
            cmdAnyaCat(1).Enabled = False
            cmdEliCat(1).Enabled = False
            cmdAnyaCat(0).Enabled = False
            cmdModifCat.Enabled = False
            cmdBajaLogica.Enabled = False
            
        Else
            If bModifCat Then txtObsAdjun.Locked = False
            cmdBajaLogica.Enabled = True
            cmdModifCat.Enabled = True
            If Left(node.Tag, 4) <> "CAT5" Then
                cmdAnyaCat(0).Enabled = True
            Else
                cmdAnyaCat(0).Enabled = False
            End If
        End If
    Else
        tabCatalogo.TabVisible(1) = False
        txtObsAdjun.Locked = True
        cmdAnyaCat(1).Enabled = False
        cmdEliCat(1).Enabled = False
        cmdAbrirAdjun.Enabled = False
        cmdA�adirAdjun.Enabled = False
        cmdSalvarAdjun.Enabled = False
        cmdEliminarAdjun.Enabled = False
        cmdModificarAdjun.Enabled = False
        txtObsAdjun.Text = ""
        lstvwAdjun.ListItems.clear
        sdbgProves.RemoveAll
    End If
    
End Sub
Private Sub HabilitarBotonesAdjun(ByVal cont As Integer)
    
    cmdA�adirAdjun.Enabled = True
    If cont = 0 Then
        cmdEliminarAdjun.Enabled = False
        cmdModificarAdjun.Enabled = False
        cmdAbrirAdjun.Enabled = False
        cmdSalvarAdjun.Enabled = False
    Else
        cmdEliminarAdjun.Enabled = True
        cmdModificarAdjun.Enabled = True
        cmdAbrirAdjun.Enabled = True
        cmdSalvarAdjun.Enabled = True
    End If

End Sub

''' <summary>
''' Cargara el grid sdbgCamposPedido
''' </summary>
''' <remarks>Llamada desde: tvwCategorias_NodeClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub MostrarDatosDeCategoria()
Dim nodx As MSComctlLib.node
Dim oAdjun As CAdjunto
Dim oProve As CProveedor
Dim oLstItem As listItem
Dim oCat As Object

txtObsAdjun.Text = ""
lstvwAdjun.ListItems.clear
sdbgProves.RemoveAll
bRespetarCheck = True
chkModEmision(0).Value = vbUnchecked
chkModEmision(1).Value = vbUnchecked
bRespetarCheck = False
Set m_oCampos = Nothing
Set m_oCamposR = Nothing

Set nodx = tvwCategorias.selectedItem
If nodx Is Nothing Then Exit Sub
Screen.MousePointer = vbHourglass

Set oCat = DevolverCategoria
If oCat Is Nothing Then Exit Sub

With oCat
    m_bRespetarCombo = True
    txtObsAdjun.Text = NullToStr(.Observaciones)
    m_bRespetarCombo = False
    
    If .Adjuntos Is Nothing Then
        .CargarAdjuntos
    End If
    HabilitarBotonesAdjun .Adjuntos.Count
    
    If .ProveedoresPedidoLibre Is Nothing Then
        .CargarProveedoresParaPedidoLibre
    End If
    If .ProveedoresPedidoLibre.Count = 0 Then cmdEliCat(1).Enabled = False
    
    For Each oAdjun In .Adjuntos
        Set oLstItem = lstvwAdjun.ListItems.Add(key:="ESP_" & CStr(oAdjun.Id), Text:=oAdjun.nombre, SmallIcon:="ESP")
        oLstItem.ToolTipText = NullToStr(oAdjun.Comentario)
        oLstItem.ListSubItems.Add key:="Tamanyo", Text:=TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
        oLstItem.ListSubItems.Add key:="Com", Text:=NullToStr(oAdjun.Comentario)
        oLstItem.Tag = oAdjun.Id
    Next
    
    For Each oProve In .ProveedoresPedidoLibre
        sdbgProves.AddItem oProve.Cod & Chr(m_lSeparador) & oProve.Den
    Next
    
    'Modif Emision pedidos
    bRespetarCheck = True
    chkModEmision(0).Value = IIf(.ModificarPrecEmision = True, vbChecked, vbUnchecked)
    chkModEmision(1).Value = IIf(.ModificarPrecEmisionCat = True, vbChecked, vbUnchecked)
    bRespetarCheck = False
        
    If nodx.Children = 0 Then
        If tabDatos.TabVisible(1) = False Then
            ArrangeDatosCategoria
        End If
        'Campos personalizados
        If .CamposPersonalizados Is Nothing Then
            Set .CamposPersonalizados = oFSGSRaiz.Generar_CCampos
        End If
        CargarDatosPedido sdbgCamposPedido, False, oCat
        CargarDatosPedido sdbgCamposRecepcion, True, oCat
    Else
        If tabDatos.TabVisible(1) = True Then
            ArrangeDatosCategoria
        End If
    End If
End With


Select Case Left(nodx.Tag, 4)
'Comprobamos el nivel de la categoria seleccionada
    
    Case "CAT1"
            Set oCategoria1Seleccionada = Nothing
            Set oCategoria1Seleccionada = oFSGSRaiz.Generar_CCategoriaN1
            oCategoria1Seleccionada.Id = DevolverId(nodx)
            'Cargamos los costes de la categoria seleccionada
            oCategoria1Seleccionada.CargarCostes
            Set m_oCostes = oCategoria1Seleccionada.Costes
            'Cargamos los descuentos de la categoria seleccionada
            oCategoria1Seleccionada.CargarDescuentos
            
            
            Set m_oDescuentos = oCategoria1Seleccionada.Descuentos
            m_iNivelCategoriaSeleccionada = 1
    Case "CAT2"
            Set oCategoria2Seleccionada = Nothing
            Set oCategoria2Seleccionada = oFSGSRaiz.Generar_CCategoriaN2
            oCategoria2Seleccionada.Id = DevolverId(nodx)
            'Cargamos los costes de la categoria seleccionada
            oCategoria2Seleccionada.CargarCostes
            Set m_oCostes = oCategoria2Seleccionada.Costes
            
            
            'Cargamos los descuentos de la categoria seleccionada
            oCategoria2Seleccionada.CargarDescuentos
            Set m_oDescuentos = oCategoria2Seleccionada.Descuentos
            
            m_iNivelCategoriaSeleccionada = 2
    Case "CAT3"
            Set oCategoria3Seleccionada = Nothing
            Set oCategoria3Seleccionada = oFSGSRaiz.Generar_CCategoriaN3
            oCategoria3Seleccionada.Id = DevolverId(nodx)
            'Cargamos los costes de la categoria seleccionada
            oCategoria3Seleccionada.CargarCostes
            Set m_oCostes = oCategoria3Seleccionada.Costes
            
            'Cargamos los descuentos de la categoria seleccionada
            oCategoria3Seleccionada.CargarDescuentos
            Set m_oDescuentos = oCategoria3Seleccionada.Descuentos
            
            m_iNivelCategoriaSeleccionada = 3
    Case "CAT4"
            Set oCategoria4Seleccionada = Nothing
            Set oCategoria4Seleccionada = oFSGSRaiz.Generar_CCategoriaN4
            oCategoria4Seleccionada.Id = DevolverId(nodx)
            'Cargamos los costes de la categoria seleccionada
            oCategoria4Seleccionada.CargarCostes
            Set m_oCostes = oCategoria4Seleccionada.Costes
            
            
            'Cargamos los descuentos de la categoria seleccionada
            oCategoria4Seleccionada.CargarDescuentos
            Set m_oDescuentos = oCategoria4Seleccionada.Descuentos
            
            m_iNivelCategoriaSeleccionada = 4
    Case "CAT5"
            Set oCategoria5Seleccionada = Nothing
            Set oCategoria5Seleccionada = oFSGSRaiz.Generar_CCategoriaN5
            oCategoria5Seleccionada.Id = DevolverId(nodx)
            'Cargamos los costes de la categoria seleccionada
            oCategoria5Seleccionada.CargarCostes
            Set m_oCostes = oCategoria5Seleccionada.Costes
            
            
            'Cargamos los descuentos de la categoria seleccionada
            oCategoria5Seleccionada.CargarDescuentos
            Set m_oDescuentos = oCategoria5Seleccionada.Descuentos
            
            m_iNivelCategoriaSeleccionada = 5
End Select

'Cargamos el grid con los costes de la categoria
CargarGridCostesCategoriaCatalogo
'Cargamos el grid con los descuentos de la categoria
CargarGridDescuentosCategoriaCatalogo
'Configuramos los combos de la grid de costes
ConfigurarCombosGridCostes
'Configuramos los combos de la grid de descuentos
ConfigurarCombosGridDescuentos

Set oLstItem = Nothing
Set oCat = Nothing
Set nodx = Nothing
Screen.MousePointer = vbNormal
    
End Sub

''' <summary>Carga en la grid los costes de la categoria de catalogo</summary>
Private Sub CargarGridCostesCategoriaCatalogo()
    Dim oatrib As CAtributo
    sdbgCostesCategoria.RemoveAll
    
    If Not m_oCostes Is Nothing Then
        If m_oCostes.Count > 0 Then
            cmdEliminarCoste.Enabled = True
        Else
            cmdEliminarCoste.Enabled = False
        End If
        For Each oatrib In m_oCostes
            sdbgCostesCategoria.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.OperacionCosteDescuento & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.TipoCosteDescuento & Chr(m_lSeparador) & oatrib.GrupoCosteDescuento & Chr(m_lSeparador) & oatrib.Descripcion & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.TipoCosteDescuento & Chr(m_lSeparador) & oatrib.ambito & Chr(m_lSeparador) & oatrib.ambito & Chr(m_lSeparador) & oatrib.OperacionCosteDescuento & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.TieneImpuestos
        Next
    End If
    
End Sub

''' <summary>Carga en la grid los costes de la categoria de catalogo</summary>
Private Sub CargarGridDescuentosCategoriaCatalogo()
    Dim oatrib As CAtributo
    sdbgDescuentosCategoria.RemoveAll
    
    If Not m_oDescuentos Is Nothing Then
        If m_oDescuentos.Count > 0 Then
            cmdEliminarDescuento.Enabled = True
        Else
            cmdEliminarDescuento.Enabled = False
        End If
        For Each oatrib In m_oDescuentos
            sdbgDescuentosCategoria.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.OperacionCosteDescuento & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.TipoCosteDescuento & Chr(m_lSeparador) & oatrib.GrupoCosteDescuento & Chr(m_lSeparador) & oatrib.Descripcion & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.TipoCosteDescuento & Chr(m_lSeparador) & oatrib.ambito & Chr(m_lSeparador) & oatrib.ambito & Chr(m_lSeparador) & oatrib.OperacionCosteDescuento
        Next
    End If
    
End Sub

Public Sub DeshacerBajaLogica()
Dim nodx As MSComctlLib.node
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit
Dim oCategoria1Padre As CCategoriaN1
Dim oCategoria2Padre As CCategoriaN2
Dim oCategoria3Padre As CCategoriaN3
Dim oCategoria4Padre As CCategoriaN4
Dim oLineasPadre As CLineasCatalogo
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim bHom As Boolean
Dim bLineasPadre As Boolean
Dim NivelCat As Integer
    
    Screen.MousePointer = vbHourglass
    
    Set nodx = tvwCategorias.selectedItem
    
    If gParametrosGenerales.gbOblPedidosHom And nodx.Children = 0 Then
        bHom = True
    Else
        bHom = False
    End If
    
    If Not nodx Is Nothing Then
        irespuesta = oMensajes.DeshacerBajaLogica(nodx.Text)
        If irespuesta = vbYes Then
            Select Case Left(nodx.Tag, 4)
                    Case "CAT1"
                        Set oCategoria1Seleccionada = oFSGSRaiz.Generar_CCategoriaN1
                        scod1 = DevolverCod(nodx)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        
                        Set oCategoria1Seleccionada = oCategoriasNivel1.Item(scod1)
                        teserror = oCategoria1Seleccionada.DeshacerBajaLogica
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            oCategoria1Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                            If oCategoria1Seleccionada.Seguridad = 0 Then
                                nodx.Image = "CAT"
                            Else
                                nodx.Image = "SEGURO"
                            End If
                            QuitarMarcaTodosSusPadres nodx
                            RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cod:" & CStr(oCategoria1Seleccionada.Cod) & " BajaLog=0"
                            Set oCategoria1Seleccionada = Nothing
                        End If
                
                    Case "CAT2"
                        'Si el padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                        If nodx.Parent.Image <> "BAJALOG2" Then
                            Set oCategoria1Padre = oFSGSRaiz.Generar_CCategoriaN1
                            oCategoria1Padre.Id = DevolverId(nodx.Parent)
                            Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                            Set oLineasPadre = oCategoria1Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                            If oLineasPadre.Count <> 0 Then
                                bLineasPadre = True
                                irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Text)
                                If irespuesta = vbNo Then Exit Sub
                            End If
                        End If
                        
                        Set oCategoria2Seleccionada = oFSGSRaiz.Generar_CCategoriaN2
                        scod1 = DevolverCod(nodx.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        
                        Set oCategoria2Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
                        teserror = oCategoria2Seleccionada.DeshacerBajaLogica(bLineasPadre)
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            oCategoria2Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                            If oCategoria2Seleccionada.Seguridad = 0 Then
                                nodx.Image = "CAT"
                            Else
                                nodx.Image = "SEGURO"
                            End If
                            QuitarMarcaTodosSusPadres nodx
                            RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria2Seleccionada.Cat1Cod) & "Cod:" & CStr(oCategoria2Seleccionada.Cod) & " BajaLog=0"
                            Set oCategoria2Seleccionada = Nothing
                        End If
                
                    Case "CAT3"
                        'Si el padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                        If nodx.Parent.Image <> "BAJALOG2" Then
                            Set oCategoria2Padre = oFSGSRaiz.Generar_CCategoriaN2
                            oCategoria2Padre.Id = DevolverId(nodx.Parent)
                            oCategoria2Padre.Cat1 = DevolverId(nodx.Parent.Parent)
                            Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                            Set oLineasPadre = oCategoria2Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                            If oLineasPadre.Count <> 0 Then
                                bLineasPadre = True
                                NivelCat = 2
                                irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Text)
                                If irespuesta = vbNo Then Exit Sub
                            End If
                        Else
                            'Si el padre del padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                            If nodx.Parent.Parent.Image <> "BAJALOG2" Then
                                Set oCategoria1Padre = oFSGSRaiz.Generar_CCategoriaN1
                                oCategoria1Padre.Id = DevolverId(nodx.Parent.Parent)
                                Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                                Set oLineasPadre = oCategoria1Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                                If oLineasPadre.Count <> 0 Then
                                    NivelCat = 1
                                    bLineasPadre = True
                                    irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Parent.Text)
                                    If irespuesta = vbNo Then Exit Sub
                                End If
                            End If
                        End If
                    
                        Set oCategoria3Seleccionada = oFSGSRaiz.Generar_CCategoriaN3
                        scod1 = DevolverCod(nodx.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        scod3 = DevolverCod(nodx)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                        
                        Set oCategoria3Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
                        teserror = oCategoria3Seleccionada.DeshacerBajaLogica(bLineasPadre, NivelCat)
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            oCategoria3Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                            If oCategoria3Seleccionada.Seguridad = 0 Then
                                nodx.Image = "CAT"
                            Else
                                nodx.Image = "SEGURO"
                            End If
                            QuitarMarcaTodosSusPadres nodx
                            RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat2:" & CStr(oCategoria3Seleccionada.Cat2Cod) & "Cat1:" & CStr(oCategoria3Seleccionada.Cat1Cod) & "Cod:" & CStr(oCategoria3Seleccionada.Cod) & " BajaLog=0"
                            Set oCategoria3Seleccionada = Nothing
                        End If

                    Case "CAT4"
                        'Si el padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                        If nodx.Parent.Image <> "BAJALOG2" Then
                            Set oCategoria3Padre = oFSGSRaiz.Generar_CCategoriaN3
                            oCategoria3Padre.Id = DevolverId(nodx.Parent)
                            oCategoria3Padre.Cat2 = DevolverId(nodx.Parent.Parent)
                            oCategoria3Padre.Cat1 = DevolverId(nodx.Parent.Parent.Parent)
                            Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                            Set oLineasPadre = oCategoria3Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                            If oLineasPadre.Count <> 0 Then
                                bLineasPadre = True
                                NivelCat = 3
                                irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Text)
                                If irespuesta = vbNo Then Exit Sub
                            End If
                        Else
                            'Si el padre del padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                            If nodx.Parent.Parent.Image <> "BAJALOG2" Then
                                Set oCategoria2Padre = oFSGSRaiz.Generar_CCategoriaN2
                                oCategoria2Padre.Id = DevolverId(nodx.Parent.Parent)
                                oCategoria2Padre.Cat1 = DevolverId(nodx.Parent.Parent.Parent)
                                Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                                Set oLineasPadre = oCategoria2Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                                If oLineasPadre.Count <> 0 Then
                                    NivelCat = 2
                                    bLineasPadre = True
                                    irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Parent.Text)
                                    If irespuesta = vbNo Then Exit Sub
                                End If
                            Else
                                'Si el padre del padre del padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                                If nodx.Parent.Parent.Parent.Image <> "BAJALOG2" Then
                                    Set oCategoria1Padre = oFSGSRaiz.Generar_CCategoriaN1
                                    oCategoria1Padre.Id = DevolverId(nodx.Parent.Parent.Parent)
                                    Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                                    Set oLineasPadre = oCategoria1Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                                    If oLineasPadre.Count <> 0 Then
                                        NivelCat = 1
                                        bLineasPadre = True
                                        irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Parent.Parent.Text)
                                        If irespuesta = vbNo Then Exit Sub
                                    End If
                                End If
                            End If
                        End If
                    
                        Set oCategoria4Seleccionada = oFSGSRaiz.Generar_CCategoriaN4

                        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        scod3 = DevolverCod(nodx.Parent)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                        scod4 = DevolverCod(nodx)
                        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                        
                        Set oCategoria4Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
                        teserror = oCategoria4Seleccionada.DeshacerBajaLogica(bLineasPadre, NivelCat)
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            oCategoria4Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                            If oCategoria4Seleccionada.Seguridad = 0 Then
                                nodx.Image = "CAT"
                            Else
                                nodx.Image = "SEGURO"
                            End If
                            QuitarMarcaTodosSusPadres nodx
                            RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria4Seleccionada.Cat1Cod) & "Cat2" & CStr(oCategoria4Seleccionada.Cat2Cod) & "Cat3" & CStr(oCategoria4Seleccionada.CAT3cod) & "Cod:" & CStr(oCategoria4Seleccionada.Cod) & " BajaLog=0"
                            Set oCategoria4Seleccionada = Nothing
                        End If

                    Case "CAT5"
                        'Si el padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                        If nodx.Parent.Image <> "BAJALOG2" Then
                            Set oCategoria4Padre = oFSGSRaiz.Generar_CCategoriaN4
                            oCategoria4Padre.Id = DevolverId(nodx.Parent)
                            oCategoria4Padre.Cat3 = DevolverId(nodx.Parent.Parent)
                            oCategoria4Padre.Cat2 = DevolverId(nodx.Parent.Parent.Parent)
                            oCategoria4Padre.Cat1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
                            Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                            Set oLineasPadre = oCategoria4Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                            If oLineasPadre.Count <> 0 Then
                                bLineasPadre = True
                                NivelCat = 4
                                irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Text)
                                If irespuesta = vbNo Then Exit Sub
                            End If
                        Else
                            'Si el padre del padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                            If nodx.Parent.Parent.Image <> "BAJALOG2" Then
                                Set oCategoria3Padre = oFSGSRaiz.Generar_CCategoriaN3
                                oCategoria3Padre.Id = DevolverId(nodx.Parent.Parent)
                                oCategoria3Padre.Cat2 = DevolverId(nodx.Parent.Parent.Parent)
                                oCategoria3Padre.Cat1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
                                Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                                Set oLineasPadre = oCategoria3Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                                If oLineasPadre.Count <> 0 Then
                                    NivelCat = 3
                                    bLineasPadre = True
                                    irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Parent.Text)
                                    If irespuesta = vbNo Then Exit Sub
                                End If
                            Else
                                'Si el padre del padre del padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                                If nodx.Parent.Parent.Parent.Image <> "BAJALOG2" Then
                                    Set oCategoria2Padre = oFSGSRaiz.Generar_CCategoriaN2
                                    oCategoria2Padre.Id = DevolverId(nodx.Parent.Parent.Parent)
                                    oCategoria2Padre.Cat1 = DevolverId(nodx.Parent.Parent.Parent.Parent)
                                    Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                                    Set oLineasPadre = oCategoria2Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                                    If oLineasPadre.Count <> 0 Then
                                        NivelCat = 2
                                        bLineasPadre = True
                                        irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Parent.Parent.Text)
                                        If irespuesta = vbNo Then Exit Sub
                                    End If
                                Else
                                    'Si el padre del padre del padre no es baja l�gica avisamos de que se eliminar�n sus l�neas de cat.
                                    If nodx.Parent.Parent.Parent.Parent.Image <> "BAJALOG2" Then
                                        Set oCategoria1Padre = oFSGSRaiz.Generar_CCategoriaN1
                                        oCategoria1Padre.Id = DevolverId(nodx.Parent.Parent.Parent.Parent)
                                        Set oLineasPadre = oFSGSRaiz.Generar_CLineasCatalogo
                                        Set oLineasPadre = oCategoria1Padre.CargarLineasDeCatalogo(bHom, False, 5, True, chkVerBajas.Value)
                                        If oLineasPadre.Count <> 0 Then
                                            NivelCat = 1
                                            bLineasPadre = True
                                            irespuesta = oMensajes.PreguntaEliminarLineasCatalogoPadre(nodx.Parent.Parent.Parent.Parent.Text)
                                            If irespuesta = vbNo Then Exit Sub
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        
                        Set oCategoria5Seleccionada = oFSGSRaiz.Generar_CCategoriaN5
                        
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent.Parent.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                        scod3 = DevolverCod(nodx.Parent.Parent)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                        scod4 = DevolverCod(nodx.Parent)
                        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                        scod5 = DevolverCod(nodx)
                        scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                        
                        Set oCategoria5Seleccionada = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5)
                        teserror = oCategoria5Seleccionada.DeshacerBajaLogica(bLineasPadre, NivelCat)
                        If teserror.NumError <> TESnoerror Then
                            TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        Else
                            oCategoria5Seleccionada.Seguridad = DevolverSeguridadCategoria(nodx)
                            If oCategoria5Seleccionada.Seguridad = 0 Then
                                nodx.Image = "CAT"
                            Else
                                nodx.Image = "SEGURO"
                            End If
                            QuitarMarcaTodosSusPadres nodx
                            RegistrarAccion AccionesSummit.ACCCatCategoriaBajaLog, "Cat1:" & CStr(oCategoria5Seleccionada.Cat1Cod) & "Cat2" & CStr(oCategoria5Seleccionada.Cat2Cod) & "Cat3" & CStr(oCategoria5Seleccionada.CAT3cod) & "Cat4" & CStr(oCategoria5Seleccionada.CAT4cod) & "Cod:" & CStr(oCategoria5Seleccionada.Cod) & " BajaLog=0"
                            Set oCategoria5Seleccionada = Nothing
                        End If
            End Select
            If nodx.Image <> "BAJALOG2" And nodx.Image <> "BAJALOG1" And nodx.Image <> "BAJASEG" Then
                If bModifCat Then txtObsAdjun.Locked = False
                cmdA�adirAdjun.Enabled = True
                cmdEliminarAdjun.Enabled = True
                cmdModificarAdjun.Enabled = True
                cmdAnyaCat(1).Enabled = True
                cmdEliCat(1).Enabled = True
                cmdAnyaCat(0).Enabled = True
                cmdModifCat.Enabled = True
                cmdBajaLogica.Enabled = True
            End If
        End If
        
    End If

    Screen.MousePointer = vbNormal

End Sub
Public Function DevolverSeguridadCategoria(ByVal node As MSComctlLib.node) As Variant
Dim sSegur As String
Dim iPos As Integer
Dim sTag As String
    
    sTag = node.Tag
    iPos = InStr(1, sTag, "-%$SEG")
    If iPos <> 0 Then
        sTag = Right(sTag, Len(sTag) - iPos)
        sSegur = Right(sTag, Len(sTag) - 5)
    End If
    
    If sSegur <> "" Then
        DevolverSeguridadCategoria = CLng(sSegur)
    Else
        DevolverSeguridadCategoria = 0
    End If
End Function

''' <summary>Muestra los datos de seguridad</summary>
''' <remarks>Llamada desde: </remarks>
''' <revision>LTG 30/01/2013</revision>

Public Sub MostrarSeguridadCatalogo()
    Dim nodx As MSComctlLib.node
    Dim sSegur As String
    Dim iPos As Integer
    Dim sTag As String
    Dim sRama As String
    Dim oSeguridad As CSeguridad

    Set nodx = tvwCategorias.selectedItem
    sTag = nodx.Tag
    iPos = InStr(1, sTag, "-%$SEG")
    If iPos <> 0 Then 'Ya tiene seguridad
        sTag = Right(sTag, Len(sTag) - iPos)
        sSegur = Right(sTag, Len(sTag) - 5)
        Accion = ACCCatSeguridadMod
        frmCATSeguridad.bConsulSegur = bConsultSegurCat
        If nodx.Image = "BAJASEG" Then
            frmCATSeguridad.bModifSegur = False
        Else
            frmCATSeguridad.bModifSegur = bModifSegurCat
        End If
        frmCATSeguridad.bModifAcceso = bModifAccFSEPSegurCat
        frmCATSeguridad.bRUO = bRUOSegurCat
        frmCATSeguridad.bRPerfUO = bRPerfUOSegurCat
        frmCATSeguridad.bRDep = bRDepSegurCat
        Set frmCATSeguridad.oSeguridad = oFSGSRaiz.Generar_CSeguridad
        frmCATSeguridad.oSeguridad.Id = CLng(sSegur)
        frmCATSeguridad.oSeguridad.Categoria = DevolverId(nodx)
        frmCATSeguridad.oSeguridad.NivelCat = CInt(Right(Left(nodx.Tag, 4), 1))
        frmCATSeguridad.NombreRama = DevolverNombreRama(nodx)
        frmCATSeguridad.caption = sIdiCaption & " " & DevolverNombreRama(nodx)
        MDI.MostrarFormulario frmCATSeguridad

    Else
        sRama = ComprobarSeguridadDeRama(nodx)
        If sRama <> "" Then
            oMensajes.ImposibleConfigurarSeguridad sRama
        Else
            Accion = ACCCatSeguridadAnya
            Set oSeguridad = oFSGSRaiz.Generar_CSeguridad
            oSeguridad.Id = 0
            oSeguridad.Categoria = DevolverId(nodx)
            oSeguridad.NivelCat = CInt(Right(Left(nodx.Tag, 4), 1))
            
            frmCATSeguridad.bConsulSegur = bConsultSegurCat
            frmCATSeguridad.bModifSegur = bModifSegurCat
            frmCATSeguridad.bModifAcceso = bModifAccFSEPSegurCat
            frmCATSeguridad.bRUO = bRUOSegurCat
            frmCATSeguridad.bRPerfUO = bRPerfUOSegurCat
            frmCATSeguridad.bRDep = bRDepSegurCat
            Set frmCATSeguridad.oSeguridad = oSeguridad
            frmCATSeguridad.caption = sIdiCaption & " " & DevolverNombreRama(nodx)
            frmCATSeguridad.NombreRama = DevolverNombreRama(nodx)
            MDI.MostrarFormulario frmCATSeguridad
    
            Set oSeguridad = Nothing

        End If
    End If
    
    Accion = ACCCatCategoriaCon
End Sub

Private Function ComprobarSeguridadDeRama(ByVal nodx As MSComctlLib.node) As String

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oCatNivel1 As CCategoriaN1
Dim oCatNivel2 As CCategoriaN2
Dim oCatNivel3 As CCategoriaN3
Dim oCatNivel4 As CCategoriaN4
Dim oCatNivel5 As CCategoriaN5

ComprobarSeguridadDeRama = ""
Select Case Left(nodx.Tag, 4)
    Case "CAT1"
        scod1 = DevolverCod(nodx)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        Set oCatNivel1 = oCategoriasNivel1.Item(scod1)
        For Each oCatNivel2 In oCatNivel1.CategoriasN2
            If oCatNivel2.Seguridad <> 0 Then
                ComprobarSeguridadDeRama = oCatNivel2.Cat1Cod & " - " & oCatNivel2.Cod & " - " & oCatNivel2.Den
                Set oCatNivel1 = Nothing
                Set oCatNivel2 = Nothing
                Exit Function
            End If
            For Each oCatNivel3 In oCatNivel2.CategoriasN3
                If oCatNivel3.Seguridad <> 0 Then
                    ComprobarSeguridadDeRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
                    Set oCatNivel1 = Nothing
                    Set oCatNivel2 = Nothing
                    Set oCatNivel3 = Nothing
                    Exit Function
                End If
                For Each oCatNivel4 In oCatNivel3.CategoriasN4
                    If oCatNivel4.Seguridad <> 0 Then
                        ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                        Set oCatNivel1 = Nothing
                        Set oCatNivel2 = Nothing
                        Set oCatNivel3 = Nothing
                        Set oCatNivel4 = Nothing
                        Exit Function
                    End If
                    For Each oCatNivel5 In oCatNivel4.CategoriasN5
                        If oCatNivel5.Seguridad <> 0 Then
                            ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                            Set oCatNivel1 = Nothing
                            Set oCatNivel2 = Nothing
                            Set oCatNivel3 = Nothing
                            Set oCatNivel4 = Nothing
                            Set oCatNivel5 = Nothing
                            Exit Function
                        End If
                    Next '5
                Next '4
            Next '3
        Next '2

        
    Case "CAT2"
        If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        End If
        scod1 = DevolverCod(nodx.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        Set oCatNivel2 = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
        
        For Each oCatNivel3 In oCatNivel2.CategoriasN3
            If oCatNivel3.Seguridad <> 0 Then
                ComprobarSeguridadDeRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
                Set oCatNivel2 = Nothing
                Set oCatNivel3 = Nothing
                Exit Function
            End If
            For Each oCatNivel4 In oCatNivel3.CategoriasN4
                If oCatNivel4.Seguridad <> 0 Then
                    ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                    Set oCatNivel2 = Nothing
                    Set oCatNivel3 = Nothing
                    Set oCatNivel4 = Nothing
                    Exit Function
                End If
                For Each oCatNivel5 In oCatNivel4.CategoriasN5
                    If oCatNivel5.Seguridad <> 0 Then
                        ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                        Set oCatNivel2 = Nothing
                        Set oCatNivel3 = Nothing
                        Set oCatNivel4 = Nothing
                        Set oCatNivel5 = Nothing
                        Exit Function
                    End If
                Next '5
            Next '4
        Next '3
    
    Case "CAT3"
        If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent)
            Exit Function
        End If

        
        scod1 = DevolverCod(nodx.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        Set oCatNivel3 = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
        
        For Each oCatNivel4 In oCatNivel3.CategoriasN4
            If oCatNivel4.Seguridad <> 0 Then
                ComprobarSeguridadDeRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
                Set oCatNivel3 = Nothing
                Set oCatNivel4 = Nothing
                Exit Function
            End If
            For Each oCatNivel5 In oCatNivel4.CategoriasN5
                If oCatNivel5.Seguridad <> 0 Then
                    ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                    Set oCatNivel3 = Nothing
                    Set oCatNivel4 = Nothing
                    Set oCatNivel5 = Nothing
                    Exit Function
                End If
            Next '5
        Next '4
    
    Case "CAT4"
        If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent) & " - " & (nodx.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent.Parent)
            Exit Function
        End If
      
        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        
        'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
        Set oCatNivel4 = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
        
        For Each oCatNivel5 In oCatNivel4.CategoriasN5
            If oCatNivel5.Seguridad <> 0 Then
                ComprobarSeguridadDeRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
                Set oCatNivel4 = Nothing
                Set oCatNivel5 = Nothing
                Exit Function
            End If
        Next '5
    
    
    Case "CAT5"
        If nodx.Parent.Image = "SEGURO" Or nodx.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverDen(nodx.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & (nodx.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent.Parent)
            Exit Function
        End If
        If nodx.Parent.Parent.Parent.Parent.Image = "SEGURO" Or nodx.Parent.Parent.Parent.Parent.Image = "BAJASEG" Then
            ComprobarSeguridadDeRama = DevolverCod(nodx.Parent.Parent.Parent.Parent) & " - " & DevolverDen(nodx.Parent.Parent.Parent.Parent)
            Exit Function
        End If
        
End Select

End Function
Public Sub EliminarSeguridad()
Dim oSeguridad As CSeguridad
Dim oIBaseDatos As IBaseDatos
Dim teserror As TipoErrorSummit
Dim nodx As MSComctlLib.node
Dim iPos As Integer
Dim sTag As String
Dim sSegur As String
Dim irespuesta As Integer

    irespuesta = oMensajes.PreguntaEliminar(oMensajes.CargarTexto(202, 99) & ": " & DevolverCod(tvwCategorias.selectedItem))
    If irespuesta = vbNo Then
        Exit Sub
    End If
    
    Set nodx = tvwCategorias.selectedItem
    sTag = nodx.Tag
    iPos = InStr(1, sTag, "-%$SEG")
    If iPos <> 0 Then 'Ya tiene seguridad
        sTag = Right(sTag, Len(sTag) - iPos)
        sSegur = Right(sTag, Len(sTag) - 5)
    Else
        'oMensajes.imposiblee
        Exit Sub
    End If
    Set oSeguridad = oFSGSRaiz.Generar_CSeguridad
    oSeguridad.Id = CLng(sSegur)
    oSeguridad.Categoria = DevolverId(nodx)
    oSeguridad.NivelCat = Right(Left(nodx.Tag, 4), 1)
    Set oIBaseDatos = oSeguridad
    teserror = oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
    Else
        ActualizarSeguridadEnCategoria 0, nodx
        nodx.Image = "CAT"
        nodx.Tag = Left(nodx.Tag, iPos - 1)
        RegistrarAccion AccionesSummit.ACCCatSeguridadMod, "Cat: " & CStr(oSeguridad.Categoria) & " Nivel: " & CStr(oSeguridad.NivelCat) & " Seguridad eliminada: " & CStr(oSeguridad.Id)
    End If
End Sub

Public Sub ActualizarSeguridadEnCategoria(ByVal Seg As Long, ByVal nodx As MSComctlLib.node)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Select Case Left(nodx.Tag, 4)
    Case "CAT1"
        scod1 = DevolverCod(nodx)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        oCategoriasNivel1.Item(scod1).Seguridad = Seg
        
    Case "CAT2"
        scod1 = DevolverCod(nodx.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).Seguridad = Seg
        
    Case "CAT3"
        scod1 = DevolverCod(nodx.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).Seguridad = Seg
            
    Case "CAT4"
        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        
        oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).Seguridad = Seg
            
    Case "CAT5"
        scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx.Parent)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        scod5 = DevolverCod(nodx)
        scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
        
        oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5).Seguridad = Seg
End Select


End Sub
Private Function DevolverNombreRama(ByVal nodx As MSComctlLib.node) As String

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim oCatNivel1 As CCategoriaN1
Dim oCatNivel2 As CCategoriaN2
Dim oCatNivel3 As CCategoriaN3
Dim oCatNivel4 As CCategoriaN4
Dim oCatNivel5 As CCategoriaN5

DevolverNombreRama = ""
Select Case Left(nodx.Tag, 4)
    Case "CAT1"
        scod1 = DevolverCod(nodx)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        Set oCatNivel1 = oCategoriasNivel1.Item(scod1)
        DevolverNombreRama = oCatNivel1.Cod & " - " & oCatNivel1.Den
        
    Case "CAT2"
        scod1 = DevolverCod(nodx.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        Set oCatNivel2 = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
        DevolverNombreRama = oCatNivel2.Cat1Cod & " - " & oCatNivel2.Cod & " - " & oCatNivel2.Den
        
        
        
    Case "CAT3"
        scod1 = DevolverCod(nodx.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        Set oCatNivel3 = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
        DevolverNombreRama = oCatNivel3.Cat1Cod & " - " & oCatNivel3.Cat2Cod & " - " & oCatNivel3.Cod & " - " & oCatNivel3.Den
    
    Case "CAT4"
        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        
        'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
        Set oCatNivel4 = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
        DevolverNombreRama = oCatNivel4.Cat1Cod & " - " & oCatNivel4.Cat2Cod & " - " & oCatNivel4.CAT3cod & " - " & oCatNivel4.Cod & " - " & oCatNivel4.Den
    
    Case "CAT5"
        scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
        scod2 = DevolverCod(nodx.Parent.Parent.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
        scod3 = DevolverCod(nodx.Parent.Parent)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
        scod4 = DevolverCod(nodx.Parent)
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
        scod5 = DevolverCod(nodx)
        scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
        
        Set oCatNivel5 = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5)
        DevolverNombreRama = oCatNivel5.Cat1Cod & " - " & oCatNivel5.Cat2Cod & " - " & oCatNivel5.CAT3cod & " - " & oCatNivel5.CAT4cod & " - " & oCatNivel5.Cod & " - " & oCatNivel5.Den
End Select

End Function

Public Sub CopiarSeguridad()
Dim nodx As MSComctlLib.node
Dim nodm As MSComctlLib.node
Dim iPos As Integer
Dim sSegur As String
Dim sTag As String
Dim oSeguridad As CSeguridad
Dim teserror As TipoErrorSummit
Dim NuevaSeg As Long
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

    Set nodx = tvwCategorias.selectedItem
    sTag = nodx.Tag
    iPos = InStr(1, sTag, "-%$SEG")
    If iPos <> 0 Then 'Ya tiene seguridad
        sTag = Right(sTag, Len(sTag) - iPos)
        sSegur = Right(sTag, Len(sTag) - 5)
    
        Set oSeguridad = oFSGSRaiz.Generar_CSeguridad
        oSeguridad.Categoria = DevolverId(nodx)
        oSeguridad.NivelCat = CInt(Right(Left(nodx.Tag, 4), 1))
        oSeguridad.Id = CLng(sSegur) ' 1 si tiene seguridad, 0 si no la tiene
    Else
        Exit Sub
    End If
    CopiarANivel = 0
    CopiarACategoria = 0
    frmSELCat.sOrigen = "CopiarSeg"
    frmSELCat.bRUsuAprov = bRUsuAprov
    frmSELCat.Copia = oSeguridad.Categoria
    frmSELCat.bVerBajas = False
    frmSELCat.CopiaNivel = oSeguridad.NivelCat
    frmSELCat.Show 1
    If CopiarACategoria = 0 And CopiarANivel = 0 Then Exit Sub
    Screen.MousePointer = vbHourglass
    Select Case SubirSeguridad
    Case 0
        teserror = oSeguridad.CopiarAOtraCategoria(CopiarACategoria, CopiarANivel)
        Screen.MousePointer = vbNormal
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            Exit Sub
        Else
            NuevaSeg = 1 'Se indica que ya tiene seguridad
            RegistrarAccion AccionesSummit.ACCCatSeguridadMod, "Copiar seguridad: " & CStr(oSeguridad.Id) & " a categoria " & CStr(oSeguridad.Categoria) & " de nivel " & CStr(oSeguridad.NivelCat)
        End If
    Case Else
        Screen.MousePointer = vbNormal
        oMensajes.ImposibleConfigurarSeguridad ""
        Exit Sub
    End Select
    
    If SubirSeguridad < 2 Then
        Select Case CopiarANivel
        Case 1
            scod1 = CAT1Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(CAT1Seleccionada))
            Set nodm = tvwCategorias.Nodes("CAT1" & scod1)
        Case 2
            scod1 = CAT1Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(CAT1Seleccionada))
            scod2 = CAT2Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(CAT2Seleccionada))
            Set nodm = tvwCategorias.Nodes("CAT2" & scod1 & scod2)
        Case 3
            scod1 = CAT1Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(CAT1Seleccionada))
            scod2 = CAT2Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(CAT2Seleccionada))
            scod3 = CAT3Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(CAT3Seleccionada))
            Set nodm = tvwCategorias.Nodes("CAT3" & scod1 & scod2 & scod3)
        Case 4
            scod1 = CAT1Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(CAT1Seleccionada))
            scod2 = CAT2Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(CAT2Seleccionada))
            scod3 = CAT3Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(CAT3Seleccionada))
            scod4 = CAT4Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(CAT4Seleccionada))
            Set nodm = tvwCategorias.Nodes("CAT4" & scod1 & scod2 & scod3 & scod4)
        Case 5
            scod1 = CAT1Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(CAT1Seleccionada))
            scod2 = CAT2Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(CAT2Seleccionada))
            scod3 = CAT3Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(CAT3Seleccionada))
            scod4 = CAT4Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(CAT4Seleccionada))
            scod5 = CAT5Seleccionada & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(CAT5Seleccionada))
            Set nodm = tvwCategorias.Nodes("CAT5" & scod1 & scod2 & scod3 & scod4 & scod5)
        End Select
        nodm.Image = "SEGURO"
        nodm.Tag = nodm.Tag & "-%$SEG" & NuevaSeg
        ActualizarSeguridadEnCategoria NuevaSeg, nodm
        If SubirSeguridad = 1 Then
            nodx.Image = "CAT"
            nodx.Tag = Left(nodx.Tag, iPos - 1)
            ActualizarSeguridadEnCategoria 0, nodx
        End If
    Else
        Screen.MousePointer = vbHourglass
        'Si el usuario es de aprovisionamiento y tiene restricci�n
        If bRUsuAprov Then
            GenerarEstructuraCategoriasAprovisionamiento chkVerBajas.Value, NuevaSeg
        Else
        'Si no
            GenerarEstructuraCategorias chkVerBajas.Value, NuevaSeg
        End If
 
    End If
    Screen.MousePointer = vbNormal
    Set oSeguridad = Nothing
    Set nodm = Nothing
    Set nodx = Nothing


End Sub

Private Sub CargarGridConDestinos()

        ''' * Objetivo: Cargar combo con la coleccion de Destinos
    
    Dim oDest As CDestino
    
    sdbddDestinos.RemoveAll
    
    For Each oDest In oDestinos
        sdbddDestinos.AddItem oDest.Cod & Chr(m_lSeparador) & oDest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oDest.dir & Chr(m_lSeparador) & oDest.POB & Chr(m_lSeparador) & oDest.cP & Chr(m_lSeparador) & oDest.Pais & Chr(m_lSeparador) & oDest.Provi
    Next
    
    If sdbddDestinos.Rows = 0 Then
        sdbddDestinos.AddItem ""
    End If

End Sub
Private Sub CargarGridConMonedas()
    'Cargar Combo con la coleccion de monedas
    'Fecha 22-2-2006
    Dim oMon As CMoneda
    
    Me.sdbddMonedas.RemoveAll
    For Each oMon In oMonedas
        sdbddMonedas.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    If sdbddMonedas.Rows = 0 Then
        sdbddMonedas.AddItem ""
    End If
End Sub

Public Sub ModificarSolicitud(ByVal lIdSolicitud As Variant)
    frmSolicitudBuscar.g_sOrigen = "frmCatalogo"
    frmSolicitudBuscar.Show vbModal
End Sub

Public Sub DetalleSolicitud(ByVal lIdSolicitud As Variant)
    'Muestra el detalle de la solicitud:
    Dim oInstancias As CInstancias
    Dim oInstancia As CInstancia
    
    Set oInstancias = oFSGSRaiz.Generar_CInstancias

    oInstancias.BuscarSolicitudes OrdSolicPorId, , lIdSolicitud
    
    Set oInstancia = oInstancias.Item(1)
    
    If Not g_ofrmDetalleSolic Is Nothing Then
        Unload g_ofrmDetalleSolic
        Set g_ofrmDetalleSolic = Nothing
    End If
    Set g_ofrmDetalleSolic = New frmSolicitudDetalle
    
    g_ofrmDetalleSolic.g_sOrigen = "frmCatalogo"
    Set g_ofrmDetalleSolic.g_oSolicitudSeleccionada = oInstancia

    Screen.MousePointer = vbHourglass
    MDI.MostrarFormulario g_ofrmDetalleSolic
    Screen.MousePointer = vbNormal
    
    Set oInstancia = Nothing
    Set oInstancias = Nothing

End Sub

Public Sub PonerSolicitudSeleccionada(ByVal oSolic As CInstancia)
    'Pone en la clase y en la pantalla la solicitud seleccionada
    
    sdbgAdjudicaciones.Columns("SOLICIT").Value = oSolic.Id

    Set oSolic = Nothing
End Sub


Private Sub cmdAbrirAdjun_Click()
Dim Item As MSComctlLib.listItem
Dim nodx As MSComctlLib.node
Dim sFileName As String

Set nodx = tvwCategorias.selectedItem
If nodx Is Nothing Then Exit Sub

Set Item = lstvwAdjun.selectedItem
If Item Is Nothing Then
    oMensajes.SeleccioneFichero
    Exit Sub
Else
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & Item.Text
End If

LeerFichero sFileName
        
sayFileNames(UBound(sayFileNames)) = sFileName
ReDim Preserve sayFileNames(UBound(sayFileNames) + 1)

'Lanzamos la aplicacion
ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1

End Sub
Private Sub LeerFichero(ByVal sFileName As String)
Dim oAdjun As CAdjunto
Dim oCat As Object
Dim sID_Rel As String
Set oAdjun = oFSGSRaiz.generar_cadjunto
Set oCat = DevolverCategoria
Set oAdjun = oCat.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag))
Select Case iNivel
    Case 1
        Set oAdjun.Cat1 = oCat
        oAdjun.Tipo = TipoAdjunto.Categoria1
        sID_Rel = oAdjun.Cat1.Id
    Case 2
        Set oAdjun.Cat2 = oCat
        oAdjun.Tipo = TipoAdjunto.Categoria2
        sID_Rel = oAdjun.Cat2.Id
    Case 3
        Set oAdjun.Cat3 = oCat
        oAdjun.Tipo = TipoAdjunto.Categoria3
        sID_Rel = oAdjun.Cat3.Id
    Case 4
        Set oAdjun.Cat4 = oCat
        oAdjun.Tipo = TipoAdjunto.Categoria4
        sID_Rel = oAdjun.Cat4.Id
    Case 5
        Set oAdjun.Cat5 = oCat
        oAdjun.Tipo = TipoAdjunto.Categoria5
        sID_Rel = oAdjun.Cat5.Id
End Select

If oAdjun.DataSize = 0 Then
    oMensajes.NoValido sIdiElArchivo & " " & oAdjun.nombre
    Set oAdjun = Nothing
    Exit Sub
End If

oAdjun.LeerAdjunto sFileName, sID_Rel
        
End Sub
Private Sub cmdA�adirAdjun_Click()
Dim DataFile As Integer
Dim oAdjun As CAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim oCat As Object
Dim nodx As MSComctlLib.node
Dim arrFileNames As Variant
Dim iFile As Integer
Dim sPathTemp As String
Dim sAdjunto As String
Dim ArrayAdjunto() As String
On Error GoTo Cancelar:

    Set nodx = tvwCategorias.selectedItem
    If nodx Is Nothing Then Exit Sub
    
    cmmdAdjun.DialogTitle = sIdiSelecAdjunto
    cmmdAdjun.Filter = "Archivos |*.*"
    cmmdAdjun.filename = ""
    cmmdAdjun.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    cmmdAdjun.ShowOpen

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle

    If sFileName = "" Then
        Exit Sub
    End If

    arrFileNames = ExtraerFicheros(sFileName)

    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = False
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2300
    End If
    frmPROCEComFich.sOrigen = "frmCatalogo"
    bCancelarEsp = False
    frmPROCEComFich.Show 1

    If Not bCancelarEsp Then
        sPathTemp = FSGSLibrary.DevolverPathFichTemp
        Set oAdjun = oFSGSRaiz.generar_cadjunto
        Set oCat = DevolverCategoria
        Select Case iNivel
            Case 1
                Set oAdjun.Cat1 = oCat
                oAdjun.Tipo = TipoAdjunto.Categoria1
            Case 2
                Set oAdjun.Cat2 = oCat
                oAdjun.Tipo = TipoAdjunto.Categoria2
            Case 3
                Set oAdjun.Cat3 = oCat
                oAdjun.Tipo = TipoAdjunto.Categoria3
            Case 4
                Set oAdjun.Cat4 = oCat
                oAdjun.Tipo = TipoAdjunto.Categoria4
            Case 5
                Set oAdjun.Cat5 = oCat
                oAdjun.Tipo = TipoAdjunto.Categoria5
        End Select
        For iFile = 1 To UBound(arrFileNames)
            sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
            sFileTitle = arrFileNames(iFile)
            oAdjun.nombre = sFileTitle
            oAdjun.Comentario = sComentario
            sAdjunto = oAdjun.GrabarAdjunto(arrFileNames(0) & "\", sPathTemp)
            'Creamos un array, cada "substring" se asignar� a un elemento del array
            ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
            oAdjun.Id = ArrayAdjunto(0)
            If oCat.Adjuntos Is Nothing Then
                Set oCat.Adjuntos = oFSGSRaiz.Generar_CAdjuntos
            End If
            oCat.Adjuntos.Add oAdjun.Id, oAdjun.nombre, oAdjun.DataSize, oAdjun.Comentario
            
            lstvwAdjun.ListItems.Add , "ESP_" & CStr(oAdjun.Id), sFileTitle, , "ESP"
            lstvwAdjun.ListItems.Item("ESP_" & CStr(oAdjun.Id)).ToolTipText = oAdjun.Comentario
            lstvwAdjun.ListItems.Item("ESP_" & CStr(oAdjun.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
            lstvwAdjun.ListItems.Item("ESP_" & CStr(oAdjun.Id)).ListSubItems.Add , "Com", oAdjun.Comentario
            lstvwAdjun.ListItems.Item("ESP_" & CStr(oAdjun.Id)).Tag = oAdjun.Id
            HabilitarBotonesAdjun 1
        Next
        lstvwAdjun.Refresh
        basSeguridad.RegistrarAccion AccionesSummit.ACCCatCategoriaAnyaAdjun, "Cat:" & oCat.Cod & "Nivel:" & CStr(iNivel) & "Adjun:" & CStr(oAdjun.Id) & "Archivo:" & sFileTitle

        
    End If
    Set oCat = Nothing
    Set oAdjun = Nothing
    Set nodx = Nothing
    Exit Sub

Cancelar:
    On Error Resume Next
    If err.Number <> 32755 Then
        Close DataFile
        Set oCat = Nothing
        Set oAdjun = Nothing
    End If
End Sub

Private Sub cmdEliminarAdjun_Click()
Dim Item As MSComctlLib.listItem
Dim oAdjun As CAdjunto
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim oCat As Object
Dim nodx As MSComctlLib.node

On Error GoTo Cancelar:

    Set nodx = tvwCategorias.selectedItem
    If nodx Is Nothing Then Exit Sub
    
    Set Item = lstvwAdjun.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
    
        irespuesta = oMensajes.PreguntaEliminar(sIdiElArchivo & ": " & lstvwAdjun.selectedItem.Text)
        If irespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        Set oCat = DevolverCategoria
        Set oAdjun = oCat.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag))
        Select Case iNivel
            Case 1
                    Set oAdjun.Cat1 = oCat
            Case 2
                    Set oAdjun.Cat2 = oCat
            Case 3
                    Set oAdjun.Cat3 = oCat
            Case 4
                    Set oAdjun.Cat4 = oCat
            Case 5
                    Set oAdjun.Cat5 = oCat
        End Select
        
        Set oIBaseDatos = oAdjun

        teserror = oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            oCat.Adjuntos.Remove (CStr(lstvwAdjun.selectedItem.Tag))
            HabilitarBotonesAdjun oCat.Adjuntos.Count
            basSeguridad.RegistrarAccion AccionesSummit.ACCCatCategoriaAdjunEli, "Cat:" & oCat.Cod & "Nivel:" & CStr(iNivel) & "Adjun:" & oAdjun.Id & "Archivo:" & lstvwAdjun.selectedItem.Text

            lstvwAdjun.ListItems.Remove (CStr(lstvwAdjun.selectedItem.key))
        End If
        Set oCat = Nothing
        Set oAdjun = Nothing
        Set oIBaseDatos = Nothing
    End If

    Screen.MousePointer = vbNormal

Cancelar:
    Set oCat = Nothing
    Set oAdjun = Nothing
    Set oIBaseDatos = Nothing
    Set nodx = Nothing

End Sub

Private Sub cmdModificarAdjun_Click()
    Dim teserror As TipoErrorSummit
    Dim oCat As Object
    Dim nodx As MSComctlLib.node

    Set nodx = tvwCategorias.selectedItem
    If nodx Is Nothing Then Exit Sub

    If lstvwAdjun.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else

        Accion = ACCCatCategoriaAdjunMod
        Set oCat = DevolverCategoria
        Set g_oAdjun = oCat.Adjuntos.Item(CStr(lstvwAdjun.selectedItem.Tag))
        Select Case iNivel
            Case 1
                    Set g_oAdjun.Cat1 = oCat
            Case 2
                    Set g_oAdjun.Cat2 = oCat
            Case 3
                    Set g_oAdjun.Cat3 = oCat
            Case 4
                    Set g_oAdjun.Cat4 = oCat
            Case 5
                    Set g_oAdjun.Cat5 = oCat
        End Select

        Set oIBaseDatos = g_oAdjun
        teserror = oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oIBaseDatos = Nothing
            Exit Sub
        End If
        
        frmPROCEEspMod.g_sOrigen = "frmCatalogo"
        Set frmPROCEEspMod.g_oIBaseDatos = oIBaseDatos
        frmPROCEEspMod.Show 1
        Accion = ACCCatCategoriaCon
    End If
    Set g_oAdjun = Nothing
    Set oIBaseDatos = Nothing
    Set nodx = Nothing
End Sub

Private Sub cmdSalvarAdjun_Click()
Dim Item As MSComctlLib.listItem
Dim sFileName As String
Dim sFileTitle As String
Dim nodx As MSComctlLib.node

On Error GoTo Cancelar:

Set nodx = tvwCategorias.selectedItem
If nodx Is Nothing Then Exit Sub

Set Item = lstvwAdjun.selectedItem

If Item Is Nothing Then
    oMensajes.SeleccioneFichero
    Exit Sub
End If
cmmdAdjun.DialogTitle = sIdiGuardar
cmmdAdjun.CancelError = True
cmmdAdjun.Filter = sIdiTipoOrig & "|*.*"
cmmdAdjun.filename = Item.Text
cmmdAdjun.ShowSave

sFileName = cmmdAdjun.filename
sFileTitle = cmmdAdjun.FileTitle
If sFileTitle = "" Then
    oMensajes.NoValido sIdiElArchivo
    Exit Sub
End If

LeerFichero sFileName

Cancelar:
    On Error Resume Next
    Set nodx = Nothing
End Sub


Private Sub txtObsAdjun_Change()
    If Not m_bRespetarCombo Then
        If Accion <> ACCCatCategoriaAdjunMod Then
            Accion = ACCCatCategoriaAdjunMod
        End If
    End If
End Sub

Private Sub txtObsAdjun_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit
Dim vObs As Variant
Dim oCat As Object
Dim nodx As MSComctlLib.node

    If Accion = ACCCatCategoriaAdjunMod Then
    
        Set nodx = tvwCategorias.selectedItem
        If nodx Is Nothing Then Exit Sub
        If Left(nodx.Tag, 4) = "Raiz" Then Exit Sub
        Screen.MousePointer = vbHourglass
        
        Set oCat = DevolverCategoria
        
        vObs = oCat.Observaciones
        
        If StrComp(NullToStr(vObs), txtObsAdjun.Text, vbTextCompare) <> 0 Then
                
            oCat.Observaciones = StrToNull(txtObsAdjun.Text)
            Set oIBaseDatos = oCat
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                Set oIBaseDatos = Nothing
                Set oCat = Nothing
                Accion = ACCCatCategoriaCon
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, "Cat:" & oCat.Cod & "Nivel:" & CStr(iNivel) & "ID:" & CStr(oCat.Id)

        End If
        Set oCat = Nothing
        Set nodx = Nothing
        Screen.MousePointer = vbNormal
        Accion = ACCCatCategoriaCon
    End If
End Sub

Public Sub CargarProvesConBusquedaParaPedidoLibre()
Dim i As Long
Dim vbook As Variant

If frmPROVEBuscar.sdbgProveedores.SelBookmarks.Count = 0 Then Exit Sub
Set m_oProves = oFSGSRaiz.generar_CProveedores
For i = 0 To frmPROVEBuscar.sdbgProveedores.SelBookmarks.Count - 1
    
    With frmPROVEBuscar.sdbgProveedores
        vbook = .SelBookmarks.Item(i)

        If m_oProvesSeleccionados.Item(CStr(.Columns(0).CellValue(vbook))) Is Nothing Then
            'Si no esta asignado desde antes, lo asignaremos
            m_oProves.Add .Columns(0).CellValue(vbook), .Columns(1).CellValue(vbook)
        End If
    End With
Next
    
Unload frmPROVEBuscar

End Sub

Public Function DevolverCategoria() As Object
Dim oCat As Object
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String
Dim nodx As MSComctlLib.node
    
        Set nodx = tvwCategorias.selectedItem
        If nodx Is Nothing Then
            Set DevolverCategoria = Nothing
            Exit Function
        End If
        
        Select Case Left(nodx.Tag, 4)
            Case "CAT1"
                    iNivel = 1
                    scod1 = DevolverCod(nodx)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    Set oCat = oCategoriasNivel1.Item(scod1)
            Case "CAT2"
                    iNivel = 2
                    scod1 = DevolverCod(nodx.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    Set oCat = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2)
            Case "CAT3"
                    iNivel = 3
                    scod1 = DevolverCod(nodx.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nodx)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    Set oCat = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3)
            Case "CAT4"
                    iNivel = 4
                    scod1 = DevolverCod(nodx.Parent.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx.Parent.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nodx.Parent)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    scod4 = DevolverCod(nodx)
                    scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                    Set oCat = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4)
            Case "CAT5"
                    iNivel = 5
                    scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent)
                    scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT1 - Len(scod1))
                    scod2 = DevolverCod(nodx.Parent.Parent.Parent)
                    scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT2 - Len(scod2))
                    scod3 = DevolverCod(nodx.Parent.Parent)
                    scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT3 - Len(scod3))
                    scod4 = DevolverCod(nodx.Parent)
                    scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT4 - Len(scod4))
                    scod5 = DevolverCod(nodx)
                    scod5 = scod5 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodCAT5 - Len(scod5))
                    Set oCat = oCategoriasNivel1.Item(scod1).CategoriasN2.Item(scod1 & scod2).CategoriasN3.Item(scod1 & scod2 & scod3).CategoriasN4.Item(scod1 & scod2 & scod3 & scod4).CategoriasN5.Item(scod1 & scod2 & scod3 & scod4 & scod5)
        End Select

        Set DevolverCategoria = oCat
        Set oCat = Nothing
        Set nodx = Nothing
End Function

Public Sub ImposiblePublicarLineasCatalogo(ByVal Datos As Variant)
'*********************************************************************
'*** Descripci�n: Muestra un mensaje de imposibilidad de publicar  ***
'***              En caso de no poder informa de los proveedores   ***
'***              que no pudieron ser publicados                   ***
'*** Par�metros:                                                   ***
'***              Datos ::> Array (3xn)                            ***
'***                        {ID de Linea o Prove, identificador,   ***
'***                          causa error, dato}                   ***
'*** Valor que devuelve:    ----------                             ***
'*********************************************************************
Dim i As Integer
Dim oProves As CProveedores
Dim oProves2 As CProveedores
Dim sMensaje As String
Dim sCod As String
        
    Set oProves = oFSGSRaiz.generar_CProveedores
    sMensaje = ""
    For i = 0 To UBound(Datos, 2) 'desde 0 hasta el m�ximo de la 2� dimensi�n del array
        If Left(Datos(1, i), 1) = "#" Then
            sCod = Right(Datos(1, i), Len(Datos(1, i)) - 1)
        Else
            sCod = sdbgAdjudicaciones.Columns("PROVE").CellText(Datos(1, i))
        End If
        Select Case Datos(2, i)
        Case TESDatoEliminado
            Set oProves2 = oFSGSRaiz.generar_CProveedores
            oProves2.Add sCod, ""
            oProves2.CargarDatosProveedor oProves2.Item(sCod).Cod
            sMensaje = sMensaje & " " & oProves2.Item(sCod).Cod & " - " & oProves2.Item(sCod).Den
            sMensaje = sMensaje & vbCrLf & vbTab & _
              sdbgAdjudicaciones.Columns("ART").CellValue(Datos(1, i)) _
              & vbCrLf & vbTab & " (" & sIdiError(1) & ")"
            sMensaje = sMensaje & vbCrLf
        Case TESInfActualModificada
            Set oProves2 = oFSGSRaiz.generar_CProveedores
            oProves2.Add sCod, ""
            oProves2.CargarDatosProveedor oProves2.Item(sCod).Cod
            sMensaje = sMensaje & " " & oProves2.Item(sCod).Cod & " - " & oProves2.Item(sCod).Den
            sMensaje = sMensaje & vbCrLf & vbTab & _
              sdbgAdjudicaciones.Columns("ART").CellValue(Datos(1, i)) _
              & vbCrLf & vbTab & "(" & sIdiError(2) & ")"
            sMensaje = sMensaje & vbCrLf
              
        Case TESImposiblePublicarLineaCatProveNoFormaPago
            If oProves.Item(sCod) Is Nothing Then
                oProves.Add sCod, ""
                oProves.CargarDatosProveedor oProves.Item(sCod).Cod
                sMensaje = sMensaje & " " & oProves.Item(sCod).Cod & " - " & oProves.Item(sCod).Den
                sMensaje = sMensaje & " (" & sIdiError(3) & ")"
                sMensaje = sMensaje & vbCrLf
            End If
        
        Case TESImposiblePublicarLineaCatProveNoContactoAprov
            If oProves.Item(sCod) Is Nothing Then
                oProves.Add sCod, ""
                oProves.CargarDatosProveedor oProves.Item(sCod).Cod
                sMensaje = sMensaje & " " & oProves.Item(sCod).Cod & " - " & oProves.Item(sCod).Den
        
                sMensaje = sMensaje & " (" & sIdiError(4) & ")"
                sMensaje = sMensaje & vbCrLf
            End If
        Case TESImposiblePublicarLineaCatProveNoProvAutorizado
            If oProves.Item(sCod) Is Nothing Then
                oProves.Add sCod, ""
                oProves.CargarDatosProveedor oProves.Item(sCod).Cod
                sMensaje = sMensaje & " " & oProves.Item(sCod).Cod & " - " & oProves.Item(sCod).Den
                sMensaje = sMensaje & " (" & sIdiError(5) & ")"
                sMensaje = sMensaje & vbCrLf
            End If
        Case Else
            Set oProves2 = oFSGSRaiz.generar_CProveedores
            oProves2.Add sCod, ""
            oProves2.CargarDatosProveedor oProves2.Item(sCod).Cod
            sMensaje = sMensaje & " " & oProves2.Item(sCod).Cod & " - " & oProves2.Item(sCod).Den
            sMensaje = sMensaje & vbCrLf & vbTab & sdbgAdjudicaciones.Columns("ART").CellValue(Datos(1, i))
            sMensaje = sMensaje & vbCrLf & vbTab & "(" & sIdiError(6) & ")"
            sMensaje = sMensaje & vbCrLf
        End Select
        
    Next i
    Screen.MousePointer = vbNormal
    MostrarFormCatalogoPublicar sLabelForm, sMensaje
End Sub

''' <summary>
''' Cargamos el panel de adjudicaciones del nodo seleccionado
''' Y mostramos el formulario para a�adir articulos, frmCATALAnyaAdj
''' </summary>
''' <param name="X">Coordenada horizontal</param>
''' <param name="Y">Coordenada vertical</param>
''' <remarks>Llamada desde: frmADJ; Tiempo m�ximo:0,1</remarks>
Private Sub cargarAdjudicacionesNodoSeleccionado()
    'Sabemos que venimos de otro formulario porque en este momento esto solo se hace as�,
    'de modo que cogemos los valores que queremos precargar en el frmCATALAnyaAdj del formulario de origen
    Set m_oFrmAnyaAdj = New frmCATALAnyaAdj
    Select Case g_sOrigen
        Case "frmRESREU"
            Set m_oFrmAnyaAdj.oProcesoSeleccionado = frmRESREU.m_oProcesoSeleccionado
        Case "frmADJ"
            Set m_oFrmAnyaAdj.oProcesoSeleccionado = frmADJ.m_oProcesoSeleccionado
    End Select
    tabCatalogo.Tab = 1
    
    m_oFrmAnyaAdj.g_bProcesoPrecargado = True
    m_oFrmAnyaAdj.Show 1
    
    Unload m_oFrmAnyaAdj
    Set m_oFrmAnyaAdj = Nothing
    
    sdbgAdjudicaciones.AllowAddNew = False
End Sub

''' <summary>
''' Abre el panel de atributos para a�adir atributos como campos de pedido
''' Guarda los atributos seleccionados como campos de pedido de la categor�a
''' </summary>
''' <remarks>Llamada desde: frmCatalogo; Tiempo m�ximo:0,1</remarks>
Private Sub cmdAnyaCampo_Click()
    Set g_oCampos = Nothing
    Set g_oCampos = oFSGSRaiz.Generar_CCampos
    
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
    'A�ade un campo de tipo atributo de GS
    If Not g_ofrmATRIBMod Is Nothing Then
        Unload g_ofrmATRIBMod
        Set g_ofrmATRIBMod = Nothing
    End If

    Set g_ofrmATRIBMod = New frmAtribMod
    g_ofrmATRIBMod.g_sOrigen = "frmCatalogo"

    g_ofrmATRIBMod.Show vbModal
    
    Dim oCat As Object
    Set oCat = DevolverCategoria
    'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
    Dim tsError As TipoErrorSummit
    Set m_oCampos = oFSGSRaiz.Generar_CCampos
    m_oCampos.CargarDatos oCat.Id, iNivel
    tsError = m_oCampos.AnyadirCampos(oCat.Id, iNivel, g_oCampos)

    Set m_oCampos = Nothing
    Set m_oCampos = oFSGSRaiz.Generar_CCampos
    m_oCampos.CargarDatos oCat.Id, iNivel
    'Ponemos el ID a cada l�nea del grid
    With sdbgCamposPedido
        If .Rows > 0 Then
            Dim oCam As CCampo
            For Each oCam In m_oCampos
                Dim i As Integer
                Dim vbm As Variant
                For i = 0 To .Rows - 1
                    vbm = .AddItemBookmark(i)
                    .Bookmark = vbm
                    If (.Columns("ID").CellValue(vbm) = CStr(0) Or .Columns("ID").CellValue(vbm) = "") And .Columns("COD").CellValue(vbm) = oCam.Cod And .Columns("ATRIBID").CellValue(vbm) = CStr(oCam.AtribID) Then
                        .Columns("ID").Value = oCam.Id
                    End If
                Next i
            Next
            cmdEliCampo.Enabled = True
        End If
    End With
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

'''Eliminar los campos de pedido (atributos) seleccionados
Private Sub cmdEliCampo_Click()
    Screen.MousePointer = vbHourglass
    'Controlar que hay al menos un atributo seleccionado:
    '   Mirar en proveedores
    'Confirmar que se desea eliminar los atributos
    '   Mirar para eso la funcion cmdAceptarCampos_Click, tiene ejemplos de c�mo pedir confirmaci�n
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
    
    Dim i As Long
    Dim teserror As TipoErrorSummit
    'INDICA sdbgCamposPedido.SelBookmarks.Count LAS FILAS SELECCIONADAS EN LA GRID?????
    If sdbgCamposPedido.SelBookmarks.Count > 0 Then
        i = oMensajes.PreguntaEliminar(sConfirmEliminarCampos)
        If i = vbYes Then
            Dim oCamposAEliminar As CCampos
            Set oCamposAEliminar = oFSGSRaiz.Generar_CCampos
            For i = 0 To sdbgCamposPedido.SelBookmarks.Count - 1
                With sdbgCamposPedido
                    oCamposAEliminar.Add .Columns("ID").CellValue(.SelBookmarks(i)), .Columns("COD").CellValue(.SelBookmarks(i)), , , , , , , , , , .Columns("ATRIBID").CellValue(.SelBookmarks(i))
                End With
            Next
            Dim oCat As Object
            Set oCat = DevolverCategoria
            teserror = oCamposAEliminar.EliminarCampos(oCat.Id, iNivel)
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                Set oCat = Nothing
                Exit Sub
            End If
            sdbgCamposPedido.DeleteSelected
            If sdbgCamposPedido.Rows <= 0 Then
                cmdEliCampo.Enabled = False
            End If
        Else
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
        Set m_oCampos = Nothing
        Set m_oCampos = oFSGSRaiz.Generar_CCampos
        m_oCampos.CargarDatos oCat.Id, iNivel
        sdbgCamposPedido.MoveFirst
        Screen.MousePointer = Normal
    Else
        Screen.MousePointer = vbNormal
        'PONER MENSAJE DE QUE NO HAY SELECCIONADO NING�N CAMPO DE PEDIDO
        MsgBox sCamposNoSeleccionados, vbInformation, tabDatos.TabCaption(1)
    End If
End Sub


'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE CAMPOS DE PEDIDO
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>
''' Evento que salta al hacer cambios en la grid
''' </summary>
''' <param name="Cancel">Cancelacion del cambio</param>
''' <returns></returns>
''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>

Private Sub sdbgCamposPedido_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)

Cancel = False
If sdbgCamposPedido.Columns("VAL").Text <> "" Then
    Select Case UCase(sdbgCamposPedido.Columns("TIPO_DATOS").Text)
        Case 2 'Numero
            If (Not IsNumeric(sdbgCamposPedido.Columns("VAL").Text)) Then
                oMensajes.AtributoValorNoValido ("TIPO2")
                Cancel = True
                GoTo Salir
            Else
                If sdbgCamposPedido.Columns("MIN").Text <> "" And sdbgCamposPedido.Columns("MAX").Text <> "" Then
                    If StrToDbl0(sdbgCamposPedido.Columns("MIN").Text) > StrToDbl0(sdbgCamposPedido.Columns("VAL").Text) Or StrToDbl0(sdbgCamposPedido.Columns("MAX").Text) < StrToDbl0(sdbgCamposPedido.Columns("VAL").Text) Then
                        oMensajes.ValorEntreMaximoYMinimo sdbgCamposPedido.Columns("MIN").Text, sdbgCamposPedido.Columns("MAX").Text
                        Cancel = True
                        GoTo Salir
                    End If
                End If
            End If
        Case 3 'Fecha
            If (Not IsDate(sdbgCamposPedido.Columns("VAL").Text) And sdbgCamposPedido.Columns("VAL").Text <> "") Then
                oMensajes.AtributoValorNoValido ("TIPO3")
                Cancel = True
                GoTo Salir
            Else
                If sdbgCamposPedido.Columns("MIN").Text <> "" And sdbgCamposPedido.Columns("MAX").Text <> "" Then
                    If CDate(sdbgCamposPedido.Columns("MIN").Text) > CDate(sdbgCamposPedido.Columns("VAL").Text) Or CDate(sdbgCamposPedido.Columns("MAX").Text) < CDate(sdbgCamposPedido.Columns("VAL").Text) Then
                     oMensajes.ValorEntreMaximoYMinimo sdbgCamposPedido.Columns("MIN").Text, sdbgCamposPedido.Columns("MAX").Text
                        Cancel = True
                        GoTo Salir
                    End If
                End If
            End If
    End Select
End If
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
Exit Sub
Salir:
    If Me.Visible Then sdbgCamposPedido.SetFocus
End Sub

Private Sub sdbgCamposPedido_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
   If IsEmpty(sdbgCamposPedido.RowBookmark(sdbgCamposPedido.Row)) Then
        sdbgCamposPedido.Bookmark = sdbgCamposPedido.RowBookmark(sdbgCamposPedido.Row - 1)
    Else
        sdbgCamposPedido.Bookmark = sdbgCamposPedido.RowBookmark(sdbgCamposPedido.Row)
    End If
    
End Sub

Private Sub sdbgCamposPedido_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

'''' <summary>
'''' Evento que salta al hacer click en el boton de un atruto de texto largo
'''' </summary>
'''' <param name="Cancel">Click en un atributo de tipo texto largo</param>
'''' <returns></returns>
'''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>
'
Private Sub sdbgCamposPedido_BtnClick()
    If sdbgCamposPedido.col < 0 Then Exit Sub

    If sdbgCamposPedido.Columns(sdbgCamposPedido.col).Name = "VAL" Then

        frmATRIBDescr.g_bEdicion = True
        frmATRIBDescr.caption = sdbgCamposPedido.Columns("COD").Value & ": " & sdbgCamposPedido.Columns("DEN").Value
        frmATRIBDescr.txtDescr.Text = sdbgCamposPedido.Columns(sdbgCamposPedido.col).Value
        frmATRIBDescr.g_sOrigen = "frmCatalogo"
        frmATRIBDescr.Show 1

        If sdbgCamposPedido.DataChanged Then
            sdbgCamposPedido.Update
        End If
    End If
End Sub

Private Sub sdbgCamposPedido_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyEscape Then
        If sdbgCamposPedido.DataChanged = False Then
            sdbgCamposPedido.CancelUpdate
            sdbgCamposPedido.DataChanged = False
        End If
    End If
End Sub

Private Sub sdbgCamposPedido_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    sdbgCamposPedido.Columns("VAL").Locked = False
    If sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = 0 Then 'Libre
        If sdbgCamposPedido.Columns("TIPO_DATOS").Value = TipoBoolean Then
            sdbddValor.RemoveAll
            sdbddValor.AddItem ""
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
        Else
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
            sdbddValor.Enabled = False
            If sdbgCamposPedido.Columns("TIPO_DATOS").Value = 1 Then
                sdbgCamposPedido.Columns("VAL").Style = ssStyleEditButton
            ElseIf sdbgCamposPedido.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoArchivo Then
                sdbgCamposPedido.Columns("VAL").Locked = True
            End If
        End If
    Else 'Lista
        sdbddValor.RemoveAll
        sdbddValor.AddItem ""
        If Not sdbgCamposPedido.Columns("LISTA_EXTERNA").Value Then
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = sdbddValor.hWnd
            sdbddValor.Enabled = True
            sdbddValor.DroppedDown = True
            sdbddValor_DropDown
            sdbddValor.DroppedDown = True
        Else
            sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
            sdbddValor.Enabled = False
            sdbgCamposPedido.Columns("VAL").Locked = True
        End If
    End If
End Sub

Private Sub sdbgCamposPedido_Scroll(Cancel As Integer)
    sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
End Sub

''' <summary>
''' Modificamos en la base de datos
''' Comprobacion de atributos de oferta obligatorios
''' </summary>
''' <param name="ColIndex">Columna modificada</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgCamposPedido_AfterColUpdate(ByVal ColIndex As Integer)
    bModError = False
    ''' Modificamos en la base de datos
    ''Comprobacion de atributos de oferta obligatorios
    With sdbgCamposPedido
        Dim tsError As TipoErrorSummit
        Dim iObl As Integer
        Dim iInt As Integer
        Dim iVerRecep As Integer
        Select Case .Columns("INT").Value
            Case "False"
                iInt = 0
            Case "-1"
                iInt = 1
        End Select
        Select Case .Columns("OBL").Value
            Case "False"
                iObl = 0
            Case "-1"
                iObl = 1
        End Select
        Select Case .Columns("VERENRECEP").Value
            Case "False"
                iVerRecep = 0
            Case "-1"
                iVerRecep = 1
        End Select
        If .Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
            tsError = m_oCampos.ActualizarCampo(.Columns("ID").Value, iNivel, iObl, IIf(.Columns("VAL").Value = sIdiTrue, 1, IIf(.Columns("VAL").Value = sIdiFalse, 0, "")), .Columns("AMB").Value, iInt, .Columns("TIPO_DATOS").Value, iVerRecep)
        Else
            tsError = m_oCampos.ActualizarCampo(.Columns("ID").Value, iNivel, iObl, .Columns("VAL").Value, .Columns("AMB").Value, iInt, .Columns("TIPO_DATOS").Value, iVerRecep)
        End If
        Dim oCat As Object
        Set oCat = DevolverCategoria
        Set m_oCampos = Nothing
        Set m_oCampos = oFSGSRaiz.Generar_CCampos
        m_oCampos.CargarDatos oCat.Id, iNivel
        
        'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
        If tsError.NumError <> TESnoerror Then
            TratarError tsError
            If Me.Visible Then .SetFocus
            .DataChanged = False
        End If
    End With
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant
    On Error Resume Next
    sdbddValor.MoveFirst
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgCamposPedido.Columns("VAL").Value = Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgCamposPedido.Rows = 0 Then Exit Sub
   
    If sdbgCamposPedido.Columns("VAL").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    End If
    sdbddValor.RemoveAll
    sdbddValor.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    oAtributo.Id = sdbgCamposPedido.Columns("ATRIBID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgCamposPedido.Columns("TIPO_INTRODUCCION").Value = "1" Then
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Categoria
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValor.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        Else
            If sdbgCamposPedido.Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem "1" & Chr(m_lSeparador) & sIdiTrue
                sdbddValor.AddItem "0" & Chr(m_lSeparador) & sIdiFalse
            End If
        End If
    End If
    
    sdbddValor.Width = sdbgCamposPedido.Columns("VAL").Width
    sdbddValor.Columns("DESC").Width = sdbddValor.Width
End Sub
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

'Carga el Combo con la lista de �mbito
Private Sub CargarGridAmbito(oAmbito As SSDBDropDown)
    oAmbito.RemoveAll
    oAmbito.AddItem ambitodelcampodepedido.cabecera & Chr(m_lSeparador) & sCabecera
    oAmbito.AddItem ambitodelcampodepedido.Linea & Chr(m_lSeparador) & sLinea
End Sub

'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           COSTES
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>Configura como Dropdowns las columnas de operacion,tipo y ambito que siempre seran combos</summary>
Private Sub ConfigurarCombosGridCostes()
    'Le asigno los dropdowns a las columnas operacion,tipo y ambito
    sdbddOperacionCoste.RemoveAll
    sdbddOperacionCoste.AddItem ""
    sdbddTipoCoste.RemoveAll
    sdbddTipoCoste.AddItem ""
    sdbddAmbitoCoste.RemoveAll
    sdbddAmbitoCoste.AddItem ""
    sdbgCostesCategoria.Columns("OPERACION").DropDownHwnd = sdbddOperacionCoste.hWnd
    sdbgCostesCategoria.Columns("TIPO").DropDownHwnd = sdbddTipoCoste.hWnd
    sdbgCostesCategoria.Columns("AMBITO").DropDownHwnd = Me.sdbddAmbitoCoste.hWnd
    sdbddTipoCoste.Enabled = True
    sdbddOperacionCoste.Enabled = True
    sdbddAmbitoCoste.Enabled = True
End Sub


''' <summary>Elimina el coste seleccionado de la grid y de BD</summary>
Private Sub cmdEliminarCoste_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim oCostes As CAtributos
    Dim oCoste As CAtributo
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    Dim oCat As Object
    Set oCat = DevolverCategoria
    
    sdbgCostesCategoria.Columns("VALOR").DropDownHwnd = 0
    With sdbgCostesCategoria
        If .SelBookmarks.Count > 0 Then
            i = oMensajes.PreguntaEliminar(m_sEliminarCoste)
            If i = vbYes Then
                inum = 0
                Set oCostes = oFSGSRaiz.Generar_CAtributos
                While inum < .SelBookmarks.Count
                    .Bookmark = .SelBookmarks(inum)
                    oCostes.Add .Columns("ID").Value, .Columns("COD").Text, .Columns("NOMBRE").Text, TipoNumerico
                inum = inum + 1
                Wend
                '3. ELIMINAR DE BDD
                
                For Each oCoste In oCostes
                    tsError = oCoste.EliminarCosteDescuentoCategoriaCatalogo(iNivel)
                    If tsError.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError tsError
                        Exit Sub
                    End If
                Next
            
                '2.ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                '4. RECARGAR LA VARIABLE DE LOS COSTES
                Set m_oCostes = Nothing
                Set m_oCostes = oFSGSRaiz.Generar_CAtributos
                oCat.CargarCostes
                Set m_oCostes = oCat.Costes
                If sdbgCostesCategoria.Rows <= 0 Then
                    cmdEliminarCoste.Enabled = False
                End If
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox m_sNoCosteSeleccionado, vbInformation
            Exit Sub
        End If
    End With
    sdbgCostesCategoria.MoveLast
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Muestra el maestro de atributos para a�adir un coste</summary>
Private Sub cmdA�adirCoste_Click()
    Dim oCoste As CAtributo
    Dim tsError As TipoErrorSummit
    
    Set g_oCostesA�adir = Nothing
    Set g_oCostesA�adir = oFSGSRaiz.Generar_CAtributos
    
    Me.sdbgCostesCategoria.Columns("VALOR").DropDownHwnd = 0
    'A�ade un campo de tipo atributo de GS
    If Not g_ofrmATRIBMod Is Nothing Then
        Unload g_ofrmATRIBMod
        Set g_ofrmATRIBMod = Nothing
    End If

    Set g_ofrmATRIBMod = New frmAtribMod
    g_ofrmATRIBMod.g_sOrigen = "frmCatalogo_Costes"

    g_ofrmATRIBMod.Show vbModal
    
    Dim oCat As Object
    Set oCat = DevolverCategoria
    'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
    If Not g_oCostesA�adir Is Nothing Then
        For Each oCoste In g_oCostesA�adir
            tsError = oCoste.AnyadirCosteDescuentoCategoriaCatalogo(oCat.Id, Coste, iNivel, cabecera)
            'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
            If tsError.NumError <> TESnoerror Then
                TratarError tsError
            End If
        Next
    Else
        m_bA�adiendoCoste = False 'No se ha a�adido ningun coste
    End If
    
    Set m_oCostes = Nothing
    Set m_oCostes = oFSGSRaiz.Generar_CAtributos
    oCat.CargarCostes
    Set m_oCostes = oCat.Costes
    
    'Ponemos el ID a cada l�nea del grid
    With sdbgCostesCategoria
        If .Rows > 0 Then
            Dim oAtr As CAtributo
            For Each oAtr In m_oCostes
                Dim i As Integer
                Dim vbm As Variant
                For i = 0 To .Rows - 1
                    vbm = .AddItemBookmark(i)
                    .Bookmark = vbm
                    If (.Columns("ID").CellValue(vbm) = CStr(0) Or .Columns("ID").CellValue(vbm) = "") And .Columns("COD").CellValue(vbm) = oAtr.Cod And .Columns("ATRIB_ID").CellValue(vbm) = CStr(oAtr.Atrib) Then
                        .Columns("ID").Value = oAtr.Id
                    End If
                Next i
            Next
            cmdEliminarCoste.Enabled = True
        End If
    End With
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DE DROPDOWNs de COSTES
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddTipoCoste_CloseUp()
    sdbgCostesCategoria.Columns("TIPO_HIDDEN").Value = sdbddTipoCoste.Columns("VALOR").Value
    
    'Si el tipo de coste es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
    'y si tendria valor se eliminaria, sino se cambiara el estilo a normal
    Select Case sdbddTipoCoste.Columns("VALOR").Value
        Case TipoCostesDescuentosLineaCatalogo.Fijos
            sdbgCostesCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesCategoria.Columns("GRUPO").Text = ""
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgCostesCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesCategoria.Columns("GRUPO").Text = ""
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes, TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
            sdbgCostesCategoria.Columns("GRUPO").CellStyleSet "Normal"
            'Situo el foco en la columna del grupo si es obligatorio que lo especifique
            sdbgCostesCategoria.col = 6
    End Select
End Sub
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddOperacionCoste_CloseUp()
    sdbgCostesCategoria.Columns("OPERACION_HIDDEN").Value = sdbddOperacionCoste.Columns("VALOR").Value
End Sub
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddAmbitoCoste_CloseUp()
    sdbgCostesCategoria.Columns("AMBITO_HIDDEN").Value = sdbddAmbitoCoste.Columns("VALOR").Value
End Sub

''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddTipoCoste_DropDown()
    
    If sdbgCostesCategoria.Rows = 0 Then Exit Sub
   
    If sdbgCostesCategoria.Columns("VALOR").Locked Then
        sdbddTipoCoste.DroppedDown = False
        Exit Sub
    End If
    sdbddTipoCoste.RemoveAll
    sdbddTipoCoste.DroppedDown = True
    
    'A�adimos los tipos de costes
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Fijos & Chr(m_lSeparador) & m_sFijo
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes & Chr(m_lSeparador) & m_sObligatorioExcluyente
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes & Chr(m_lSeparador) & m_sOpcionalExcluyente
    sdbddTipoCoste.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Simples & Chr(m_lSeparador) & m_sOpcionalSimple
            
    'Se le modifica el ancho del dropdown
    sdbddTipoCoste.Width = sdbgCostesCategoria.Columns("TIPO").Width
    sdbddTipoCoste.Columns("DESC").Width = sdbddTipoCoste.Width
End Sub

''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddAmbitoCoste_DropDown()
    
    If sdbgCostesCategoria.Rows = 0 Then Exit Sub
   
    sdbddAmbitoCoste.RemoveAll
    sdbddAmbitoCoste.DroppedDown = True
    
    'A�adimos los tipos de costes
    sdbddAmbitoCoste.AddItem ambitodelcampodepedido.cabecera & Chr(m_lSeparador) & sCabecera
    sdbddAmbitoCoste.AddItem ambitodelcampodepedido.Linea & Chr(m_lSeparador) & sLinea
            
    'Se le modifica el ancho del dropdown
    sdbddAmbitoCoste.Width = sdbgCostesCategoria.Columns("AMBITO").Width
    sdbddAmbitoCoste.Columns("DESC").Width = sdbddAmbitoCoste.Width
End Sub


''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddOperacionCoste_DropDown()
    If sdbgCostesCategoria.Rows = 0 Then Exit Sub
   
    If sdbgCostesCategoria.Columns("VALOR").Locked Then
        sdbddOperacionCoste.DroppedDown = False
        Exit Sub
    End If
    sdbddOperacionCoste.RemoveAll
    sdbddOperacionCoste.DroppedDown = True
    
    'A�adimos las distintas operaciones
    sdbddOperacionCoste.AddItem m_sSumar & Chr(m_lSeparador) & m_sSumar
    sdbddOperacionCoste.AddItem m_sSumarPorcentaje & Chr(m_lSeparador) & m_sSumarPorcentaje
    
    'Se le modifica el ancho del dropdown
    sdbddOperacionCoste.Width = sdbgCostesCategoria.Columns("OPERACION").Width
    sdbddOperacionCoste.Columns("DESC").Width = sdbddOperacionCoste.Width
End Sub
''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddValorCoste_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgCostesCategoria.Rows = 0 Then Exit Sub
   
    If sdbgCostesCategoria.Columns("VALOR").Locked Then
        sdbddValorCoste.DroppedDown = False
        Exit Sub
    End If
    sdbddValorCoste.RemoveAll
    sdbddValorCoste.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    'Recogemos el atributo
    oAtributo.Id = sdbgCostesCategoria.Columns("ATRIB_ID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgCostesCategoria.Columns("INTRO").Value = "1" Then
            'Cargamos la lista de valores del atributo
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValorCoste.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        End If
    End If
    'Se le modifica el ancho del dropdown
    sdbddValorCoste.Width = sdbgCostesCategoria.Columns("VALOR").Width
    sdbddValorCoste.Columns("DESC").Width = sdbddValorCoste.Width
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddTipoCoste_InitColumnProps()
    sdbddTipoCoste.DataFieldList = "Column 0"
    sdbddTipoCoste.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddAmbitoCoste_InitColumnProps()
    sdbddAmbitoCoste.DataFieldList = "Column 0"
    sdbddAmbitoCoste.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddOperacionCoste_InitColumnProps()
    sdbddOperacionCoste.DataFieldList = "Column 0"
    sdbddOperacionCoste.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddValorCoste_InitColumnProps()
    sdbddValorCoste.DataFieldList = "Column 0"
    sdbddValorCoste.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddValorCoste_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValorCoste.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValorCoste.Rows - 1
            bm = sdbddValorCoste.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValorCoste.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgCostesCategoria.Columns("VALOR").Value = Mid(sdbddValorCoste.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValorCoste.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddOperacionCoste_PositionList(ByVal Text As String)
PositionList sdbddOperacionCoste, Text
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddTipoCoste_PositionList(ByVal Text As String)
PositionList sdbddTipoCoste, Text
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddAmbitoCoste_PositionList(ByVal Text As String)
PositionList sdbddAmbitoCoste, Text
End Sub


'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE COSTES DE LINEA DE CATALOGO
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

''' <summary>evento del grid que se produce por cada linea que se carga</summary>
Private Sub sdbgCostesCategoria_RowLoaded(ByVal Bookmark As Variant)
    
    'Si el tipo de coste es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
    Select Case sdbgCostesCategoria.Columns("TIPO_HIDDEN").CellValue(Bookmark)
        Case TipoCostesDescuentosLineaCatalogo.Fijos
            sdbgCostesCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesCategoria.Columns("TIPO").Text = m_sFijo
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgCostesCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgCostesCategoria.Columns("TIPO").Text = m_sOpcionalSimple
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
            sdbgCostesCategoria.Columns("TIPO").Text = m_sOpcionalExcluyente
        Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
            sdbgCostesCategoria.Columns("TIPO").Text = m_sObligatorioExcluyente
    End Select
    
    Select Case sdbgCostesCategoria.Columns("IMPUESTOS").CellValue(Bookmark)
        Case True
            sdbgCostesCategoria.Columns("IMPUESTOS_BTN").CellStyleSet "HayUniPed"
        Case False
            sdbgCostesCategoria.Columns("IMPUESTOS_BTN").CellStyleSet "Normal"
    End Select
    
    Select Case sdbgCostesCategoria.Columns("OPERACION_HIDDEN").CellValue(Bookmark)
        Case m_sSumar
            sdbgCostesCategoria.Columns("OPERACION").Text = m_sSumar
        Case m_sSumarPorcentaje
            sdbgCostesCategoria.Columns("OPERACION").Text = m_sSumarPorcentaje
        Case Else
             sdbgCostesCategoria.Columns("OPERACION").Text = m_sSumar
    End Select
    
    Select Case sdbgCostesCategoria.Columns("AMBITO_HIDDEN").CellValue(Bookmark)
        Case ambitodelcampodepedido.cabecera
            sdbgCostesCategoria.Columns("AMBITO").Text = sCabecera
        Case ambitodelcampodepedido.Linea
            sdbgCostesCategoria.Columns("AMBITO").Text = sLinea
    End Select
    
End Sub
''' <summary>evento del grid que se produce cuando el usuario cambia de celda o fila</summary>
Private Sub sdbgCostesCategoria_BeforeRowColChange(Cancel As Integer)
    If sdbgCostesCategoria.col = 6 Then
        'Si el tipo de coste es Obligatorios Excluyente o Opcionales Excluyente, y no se ha metido nada en la columna grupo, no dejara salir de ella
        Select Case sdbgCostesCategoria.Columns("TIPO_HIDDEN").Value
            Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes, TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
                If sdbgCostesCategoria.Columns("GRUPO").Text = "" Then
                    Cancel = True
                End If
        End Select
    End If
End Sub

''' <summary>evento del grid que se produce cuando el usuario cambia de celda o de fila</summary>
Private Sub sdbgCostesCategoria_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    With sdbgCostesCategoria
        'Evitamos que se ejecute el evento durante la carga inicial del grid en el que el valor de LastCol es -1
        'Si pasaba y habia varias filas, alguna de las cuales fuera lista, hacia que al asignarle a la columna valor el dropdown
        ', si la siguiente fila no era lista, la celda apareciera vacia aun teniendo valor
        If LastCol > -1 Then
            If .col > -1 Then
                'Si el atributo es de tipo lista ponemos a la columna valor el dropdown
                If .Columns(.col).Name = "VALOR" Then
                    If .Columns("INTRO").Value = 1 Then
                        'Lista
                        sdbddValorCoste.RemoveAll
                        sdbddValorCoste.AddItem ""
                        .Columns("VALOR").DropDownHwnd = sdbddValorCoste.hWnd
                        sdbddValorCoste.Enabled = True
                        sdbddValorCoste.DroppedDown = True
                        sdbddValorCoste_DropDown
                        sdbddValorCoste.DroppedDown = True
                    Else
                        'Introduccion libre
                        .Columns("VALOR").DropDownHwnd = 0
                        sdbddValorCoste.Enabled = False
                    End If
                End If
            End If
            
            'Si el tipo de coste es fijo o opcional simple, la columna de grupo quedara deshabilitada
            Select Case .Columns("TIPO_HIDDEN").Value
                Case TipoCostesDescuentosLineaCatalogo.Fijos, TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
                    .Columns("GRUPO").Locked = True
                Case Else
                    'Si es otro tipo de coste desbloquearemos la columna
                    .Columns("GRUPO").Locked = False
            End Select
            
            m_iRowActual = .Row
            m_bA�adiendoCoste = False
        End If
    End With
End Sub

''' <summary>
''' Evento que se produce al pulsar el boton de una celda del grid
''' </summary>
Private Sub sdbgCostesCategoria_BtnClick()
    frmImpuestos.g_sOrigen = "frmCatalogo_ImpuestosCategorias"
    If sdbgCostesCategoria.Columns("ID").Value <> "" Then
        frmImpuestos.g_lAtribId = sdbgCostesCategoria.Columns("ID").Value
    Else
        frmImpuestos.g_lAtribId = 0
    End If
    frmImpuestos.PermiteModif = True
    frmImpuestos.g_iNivelCategoria = iNivel
    frmImpuestos.g_ConceptoLineaCatalogo = Null
    frmImpuestos.g_sCaptionFormulario = sdbgCostesCategoria.Columns("COD").Text & " - " & sdbgCostesCategoria.Columns("NOMBRE").Text
    frmImpuestos.Show vbModal
End Sub

''' <summary>
''' evento que salta al actualizar una columna, al cambiar de campo. una vez hechas las comprobaciones en el Before, grabaremos en BD
''' </summary>
Private Sub sdbgCostesCategoria_AfterColUpdate(ByVal ColIndex As Integer)
    
    Dim teserror As TipoErrorSummit
    Dim v As Variant
       
    'Si no ha habido cambios en la fila, el objeto m_oCosteEnEdicion sera Nothing y no se grabara nada
    If Not m_oCosteEnEdicion Is Nothing Then
       m_bErrorUpdate = False
        ''' Actualizamos el valor de las propiedades
        m_oCosteEnEdicion.valor = IIf(sdbgCostesCategoria.Columns("VALOR").Text = "", 0, sdbgCostesCategoria.Columns("VALOR").Text)
        m_oCosteEnEdicion.OperacionCosteDescuento = sdbgCostesCategoria.Columns("OPERACION_HIDDEN").Text
        m_oCosteEnEdicion.TipoCosteDescuento = sdbgCostesCategoria.Columns("TIPO_HIDDEN").Text
        m_oCosteEnEdicion.GrupoCosteDescuento = sdbgCostesCategoria.Columns("GRUPO").Text
        m_oCosteEnEdicion.Descripcion = sdbgCostesCategoria.Columns("DESCR").Text
        m_oCosteEnEdicion.ambito = sdbgCostesCategoria.Columns("AMBITO_HIDDEN").Text
        
        ''' Modificamos en la base de datos
        teserror = m_oCosteEnEdicion.ModificarCosteDescuentoCategoriaCatalogo(m_iNivelCategoriaSeleccionada)
        
        If teserror.NumError <> TESnoerror Then
            v = sdbgCostesCategoria.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgCostesCategoria.SetFocus
            m_bErrorUpdate = True
            sdbgCostesCategoria.ActiveCell.Value = v
            sdbgCostesCategoria.DataChanged = False
            sdbgCostesCategoria.CancelUpdate
        Else
            Set m_oCosteEnEdicion = Nothing
        End If
        
        Screen.MousePointer = vbNormal
    End If
End Sub
''' <summary>
''' evento que salta al actualizar una columna, al cambiar de campo. Reliza las comprobaciones de los valores
''' </summary>
Private Sub sdbgCostesCategoria_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    
    Cancel = False
    
    If ColIndex >= 0 Then
        If sdbgCostesCategoria.Columns(ColIndex).Name = "VALOR" Then
            If sdbgCostesCategoria.Columns(ColIndex).Value <> "" Then
                If (Not IsNumeric(sdbgCostesCategoria.Columns("VALOR").Text)) Then
                    m_bErrorUpdate = True
                    oMensajes.AtributoValorNoValido ("TIPO2")
                    Cancel = True
                    GoTo Salir
                Else
                    If sdbgCostesCategoria.Columns("MIN").Text <> "" And sdbgCostesCategoria.Columns("MAX").Text <> "" Then
                        If StrToDbl0(sdbgCostesCategoria.Columns("MIN").Text) > StrToDbl0(sdbgCostesCategoria.Columns("VALOR").Text) Or StrToDbl0(sdbgCostesCategoria.Columns("MAX").Text) < StrToDbl0(sdbgCostesCategoria.Columns("VALOR").Text) Then
                            m_bErrorUpdate = True
                            oMensajes.ValorEntreMaximoYMinimo sdbgCostesCategoria.Columns("MIN").Text, sdbgCostesCategoria.Columns("MAX").Text
                            Cancel = True
                            GoTo Salir
                        End If
                    End If
                End If
                
                'Comprobaremos si es una lista, que se haya elegido un valor de la lista
                If sdbgCostesCategoria.Columns("INTRO").Text = "1" Then
                    Dim oAtributo As CAtributo
                    Dim bValorEncontrado As Boolean
                    Dim oLista As CValorPond
                    Set oAtributo = oFSGSRaiz.Generar_CAtributo
                    'Recogemos el atributo
                    oAtributo.Id = sdbgCostesCategoria.Columns("ATRIB_ID").Value
                    
                    If oAtributo.Id > 0 Then
                        'Cargamos la lista de valores del atributo
                        oAtributo.CargarDatosAtributo
                        oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
                        For Each oLista In oAtributo.ListaPonderacion
                            If oLista.ValorLista = sdbgCostesCategoria.Columns("VALOR").Text Then
                                bValorEncontrado = True
                                Exit For
                            End If
                        Next
                        If bValorEncontrado = False Then
                            oMensajes.AtributoValorNoValido ("NO_LISTA")
                            Cancel = True
                            GoTo Salir
                        End If
                    End If
                End If
            Else
                'El coste no tiene valor
                m_bErrorUpdate = True
                oMensajes.AtributosOblSinRellenar
                Cancel = True
                GoTo Salir
            End If
        End If
        
        'Compruebo los campos operacion
        If sdbgCostesCategoria.Columns(ColIndex).Name = "OPERACION" And sdbgCostesCategoria.Columns(ColIndex).Text <> m_sSumar And sdbgCostesCategoria.Columns(ColIndex).Text <> m_sSumarPorcentaje Then
            m_bErrorUpdate = True
            MsgBox m_sOperacionNoValida, vbExclamation
            Cancel = True
            GoTo Salir
        End If
        'Compruebo el campo tipo de coste
        If sdbgCostesCategoria.Columns(ColIndex).Name = "TIPO" And sdbgCostesCategoria.Columns("TIPO_HIDDEN").Text = "" Then
            m_bErrorUpdate = True
            MsgBox m_sTipoNoValido, vbExclamation
            Cancel = True
            GoTo Salir
        Else
            'Si el coste es de estos 2 tipos, se debera introducir un grupo
            Select Case sdbgCostesCategoria.Columns("TIPO_HIDDEN").Value
                Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes, TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
                    'Pongo el cancel=true para que no se ejecute el AftercolUpdate y no grabe en bd
                    ', ya que hasta que no meta el grupo no se deberia actualizar
                    If sdbgCostesCategoria.Columns("GRUPO").Value = "" Then
                        If Not sdbgCostesCategoria.Columns(ColIndex).Name = "TIPO" Then
                            m_bErrorUpdate = True
                            MsgBox m_sIntroduceGrupoCoste, vbExclamation
                            GoTo Salir
                        End If
                    End If
            End Select
        End If
    End If
    
Salir:
End Sub
''' <summary>
''' evento que salta al eliminar una fila del grid
''' </summary>
Private Sub sdbgCostesCategoria_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If IsEmpty(sdbgCostesCategoria.RowBookmark(sdbgCostesCategoria.Row)) Then
        sdbgCostesCategoria.Bookmark = sdbgCostesCategoria.RowBookmark(sdbgCostesCategoria.Row - 1)
    Else
        sdbgCostesCategoria.Bookmark = sdbgCostesCategoria.RowBookmark(sdbgCostesCategoria.Row)
    End If
End Sub
''' <summary>
''' Evita que se muestre el mensaje propio del grid al eliminar la fila
''' </summary>
Private Sub sdbgCostesCategoria_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' evento que salta al realizar cualquier cambio en la grid
''' </summary>
Private Sub sdbgCostesCategoria_Change()
    
    
    Set m_oCosteEnEdicion = oFSGSRaiz.Generar_CAtributo
    'Recojo el atributo que se va a modificar
    Set m_oCosteEnEdicion = m_oCostes.Item(sdbgCostesCategoria.Columns("ID").Value)
End Sub



'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           DESCUENTOS
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>Configura como Dropdowns las columnas de operacion,tipo y ambito que siempre seran combos</summary>
Private Sub ConfigurarCombosGridDescuentos()
    'Le asigno los dropdowns a las columnas operacion,tipo y ambito
    sdbddOperacionDescuento.RemoveAll
    sdbddOperacionDescuento.AddItem ""
    sdbddTipoDescuento.RemoveAll
    sdbddTipoDescuento.AddItem ""
    sdbddAmbitoDescuento.RemoveAll
    sdbddAmbitoDescuento.AddItem ""
    sdbgDescuentosCategoria.Columns("OPERACION").DropDownHwnd = sdbddOperacionDescuento.hWnd
    sdbgDescuentosCategoria.Columns("TIPO").DropDownHwnd = sdbddTipoDescuento.hWnd
    sdbgDescuentosCategoria.Columns("AMBITO").DropDownHwnd = Me.sdbddAmbitoDescuento.hWnd
    sdbddTipoDescuento.Enabled = True
    sdbddOperacionDescuento.Enabled = True
    sdbddAmbitoDescuento.Enabled = True
End Sub


''' <summary>Elimina el descuento seleccionado de la grid y de BD</summary>
Private Sub cmdEliminardescuento_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim odescuentos As CAtributos
    Dim odescuento As CAtributo
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    Dim oCat As Object
    Set oCat = DevolverCategoria
    
    sdbgDescuentosCategoria.Columns("VALOR").DropDownHwnd = 0
    With sdbgDescuentosCategoria
        If .SelBookmarks.Count > 0 Then
            i = oMensajes.PreguntaEliminar(m_sEliminarDescuento)
            If i = vbYes Then
                inum = 0
                Set odescuentos = oFSGSRaiz.Generar_CAtributos
                While inum < .SelBookmarks.Count
                    .Bookmark = .SelBookmarks(inum)
                    odescuentos.Add .Columns("ID").Value, .Columns("COD").Text, .Columns("NOMBRE").Text, TipoNumerico
                inum = inum + 1
                Wend
                '3. ELIMINAR DE BDD
                
                For Each odescuento In odescuentos
                    tsError = odescuento.EliminarCosteDescuentoCategoriaCatalogo(iNivel)
                    If tsError.NumError <> TESnoerror Then
                        Screen.MousePointer = vbNormal
                        TratarError tsError
                        Exit Sub
                    End If
                Next
            
                '2.ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                '4. RECARGAR LA VARIABLE DE LOS descuentoS
                Set m_oDescuentos = Nothing
                Set m_oDescuentos = oFSGSRaiz.Generar_CAtributos
                oCat.CargarDescuentos
                Set m_oDescuentos = oCat.Descuentos
                If sdbgDescuentosCategoria.Rows <= 0 Then
                    cmdEliminarDescuento.Enabled = False
                End If
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox m_sNoDescuentoSeleccionado, vbInformation
            Exit Sub
        End If
    End With
    sdbgDescuentosCategoria.MoveLast
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Muestra el maestro de atributos para a�adir un descuento</summary>
Private Sub cmdA�adirdescuento_Click()
    Dim odescuento As CAtributo
    Dim tsError As TipoErrorSummit
    
    Set g_oDescuentosA�adir = Nothing
    Set g_oDescuentosA�adir = oFSGSRaiz.Generar_CAtributos
    
    Me.sdbgDescuentosCategoria.Columns("VALOR").DropDownHwnd = 0
    'A�ade un campo de tipo atributo de GS
    If Not g_ofrmATRIBMod Is Nothing Then
        Unload g_ofrmATRIBMod
        Set g_ofrmATRIBMod = Nothing
    End If

    Set g_ofrmATRIBMod = New frmAtribMod
    g_ofrmATRIBMod.g_sOrigen = "frmCatalogo_Descuentos"

    g_ofrmATRIBMod.Show vbModal
    
    Dim oCat As Object
    Set oCat = DevolverCategoria
    'GUARDAMOS LOS CAMPOS A�ADIDOS EN BDD
    If Not g_oDescuentosA�adir Is Nothing Then
        For Each odescuento In g_oDescuentosA�adir
            tsError = odescuento.AnyadirCosteDescuentoCategoriaCatalogo(oCat.Id, Descuento, iNivel, cabecera)
            'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
            If tsError.NumError <> TESnoerror Then
                TratarError tsError
            End If
        Next
    Else
        m_bA�adiendoDescuento = False 'No se ha a�adido ningun descuento
    End If
    
    Set m_oDescuentos = Nothing
    Set m_oDescuentos = oFSGSRaiz.Generar_CAtributos
    oCat.CargarDescuentos
    Set m_oDescuentos = oCat.Descuentos
    
    'Ponemos el ID a cada l�nea del grid
    With sdbgDescuentosCategoria
        If .Rows > 0 Then
            Dim oAtr As CAtributo
            For Each oAtr In m_oDescuentos
                Dim i As Integer
                Dim vbm As Variant
                For i = 0 To .Rows - 1
                    vbm = .AddItemBookmark(i)
                    .Bookmark = vbm
                    If (.Columns("ID").CellValue(vbm) = CStr(0) Or .Columns("ID").CellValue(vbm) = "") And .Columns("COD").CellValue(vbm) = oAtr.Cod And .Columns("ATRIB_ID").CellValue(vbm) = CStr(oAtr.Atrib) Then
                        .Columns("ID").Value = oAtr.Id
                    End If
                Next i
            Next
            cmdEliminarDescuento.Enabled = True
        End If
    End With
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DE DROPDOWNs de descuentoS
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddTipodescuento_CloseUp()
    sdbgDescuentosCategoria.Columns("TIPO_HIDDEN").Value = sdbddTipoDescuento.Columns("VALOR").Value
    
    'Si el tipo de descuento es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
    'y si tendria valor se eliminaria, sino se cambiara el estilo a normal
    Select Case sdbddTipoDescuento.Columns("VALOR").Value
        Case TipoCostesDescuentosLineaCatalogo.Fijos
            sdbgDescuentosCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgDescuentosCategoria.Columns("GRUPO").Text = ""
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgDescuentosCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgDescuentosCategoria.Columns("GRUPO").Text = ""
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes, TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
            sdbgDescuentosCategoria.Columns("GRUPO").CellStyleSet "Normal"
            'Situo el foco en la columna del grupo si es obligatorio que lo especifique
            sdbgDescuentosCategoria.col = 6
    End Select
End Sub
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddOperaciondescuento_CloseUp()
    sdbgDescuentosCategoria.Columns("OPERACION_HIDDEN").Value = sdbddOperacionDescuento.Columns("VALOR").Value
End Sub
''' <summary>evento que salta al elegir una opcion del dropdown</summary>
Private Sub sdbddAmbitodescuento_CloseUp()
    sdbgDescuentosCategoria.Columns("AMBITO_HIDDEN").Value = sdbddAmbitoDescuento.Columns("VALOR").Value
End Sub

''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddTipodescuento_DropDown()
    
    If sdbgDescuentosCategoria.Rows = 0 Then Exit Sub
   
    If sdbgDescuentosCategoria.Columns("VALOR").Locked Then
        sdbddTipoDescuento.DroppedDown = False
        Exit Sub
    End If
    sdbddTipoDescuento.RemoveAll
    sdbddTipoDescuento.DroppedDown = True
    
    'A�adimos los tipos de descuentos
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Fijos & Chr(m_lSeparador) & m_sFijo
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes & Chr(m_lSeparador) & m_sObligatorioExcluyente
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes & Chr(m_lSeparador) & m_sOpcionalExcluyente
    sdbddTipoDescuento.AddItem TipoCostesDescuentosLineaCatalogo.Opcionales_Simples & Chr(m_lSeparador) & m_sOpcionalSimple
            
    'Se le modifica el ancho del dropdown
    sdbddTipoDescuento.Width = sdbgDescuentosCategoria.Columns("TIPO").Width
    sdbddTipoDescuento.Columns("DESC").Width = sdbddTipoDescuento.Width
End Sub

''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddAmbitodescuento_DropDown()
    
    If sdbgDescuentosCategoria.Rows = 0 Then Exit Sub
   
    sdbddAmbitoDescuento.RemoveAll
    sdbddAmbitoDescuento.DroppedDown = True
    
    'A�adimos los tipos de descuentos
    sdbddAmbitoDescuento.AddItem ambitodelcampodepedido.cabecera & Chr(m_lSeparador) & sCabecera
    sdbddAmbitoDescuento.AddItem ambitodelcampodepedido.Linea & Chr(m_lSeparador) & sLinea
            
    'Se le modifica el ancho del dropdown
    sdbddAmbitoDescuento.Width = sdbgDescuentosCategoria.Columns("AMBITO").Width
    sdbddAmbitoDescuento.Columns("DESC").Width = sdbddAmbitoDescuento.Width
End Sub


''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddOperaciondescuento_DropDown()
    If sdbgDescuentosCategoria.Rows = 0 Then Exit Sub
   
    If sdbgDescuentosCategoria.Columns("VALOR").Locked Then
        sdbddOperacionDescuento.DroppedDown = False
        Exit Sub
    End If
    sdbddOperacionDescuento.RemoveAll
    sdbddOperacionDescuento.DroppedDown = True
    
    'A�adimos las distintas operaciones
    sdbddOperacionDescuento.AddItem m_sRestar & Chr(m_lSeparador) & m_sRestar
    sdbddOperacionDescuento.AddItem m_sRestarPorcentaje & Chr(m_lSeparador) & m_sRestarPorcentaje
    
    'Se le modifica el ancho del dropdown
    sdbddOperacionDescuento.Width = sdbgDescuentosCategoria.Columns("OPERACION").Width
    sdbddOperacionDescuento.Columns("DESC").Width = sdbddOperacionDescuento.Width
End Sub
''' <summary>evento que salta al desplegar el dropdown</summary>
Private Sub sdbddValordescuento_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgDescuentosCategoria.Rows = 0 Then Exit Sub
   
    If sdbgDescuentosCategoria.Columns("VALOR").Locked Then
        sdbddValorDescuento.DroppedDown = False
        Exit Sub
    End If
    sdbddValorDescuento.RemoveAll
    sdbddValorDescuento.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    'Recogemos el atributo
    oAtributo.Id = sdbgDescuentosCategoria.Columns("ATRIB_ID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgDescuentosCategoria.Columns("INTRO").Value = "1" Then
            'Cargamos la lista de valores del atributo
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValorDescuento.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        End If
    End If
    'Se le modifica el ancho del dropdown
    sdbddValorDescuento.Width = sdbgDescuentosCategoria.Columns("VALOR").Width
    sdbddValorDescuento.Columns("DESC").Width = sdbddValorDescuento.Width
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddTipodescuento_InitColumnProps()
    sdbddTipoDescuento.DataFieldList = "Column 0"
    sdbddTipoDescuento.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddAmbitodescuento_InitColumnProps()
    sdbddAmbitoDescuento.DataFieldList = "Column 0"
    sdbddAmbitoDescuento.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddOperaciondescuento_InitColumnProps()
    sdbddOperacionDescuento.DataFieldList = "Column 0"
    sdbddOperacionDescuento.DataFieldToDisplay = "Column 1"
End Sub

''' <summary>evento que inicializa el dropdown</summary>
Private Sub sdbddValordescuento_InitColumnProps()
    sdbddValorDescuento.DataFieldList = "Column 0"
    sdbddValorDescuento.DataFieldToDisplay = "Column 1"
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddValordescuento_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValorDescuento.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValorDescuento.Rows - 1
            bm = sdbddValorDescuento.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValorDescuento.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgDescuentosCategoria.Columns("VALOR").Value = Mid(sdbddValorDescuento.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValorDescuento.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddOperaciondescuento_PositionList(ByVal Text As String)
PositionList sdbddOperacionDescuento, Text
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddTipodescuento_PositionList(ByVal Text As String)
PositionList sdbddTipoDescuento, Text
End Sub
''' <summary>Se posicionea en el combo segun el elemento seleccionado</summary>
Private Sub sdbddAmbitodescuento_PositionList(ByVal Text As String)
PositionList sdbddAmbitoDescuento, Text
End Sub


'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE descuentoS DE LINEA DE CATALOGO
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

''' <summary>evento del grid que se produce por cada linea que se carga</summary>
Private Sub sdbgdescuentosCategoria_RowLoaded(ByVal Bookmark As Variant)
    
    'Si el tipo de descuento es fijo o opcional simple, la columna de grupo se le pondra estilo gris, quedara deshabilitada
    Select Case sdbgDescuentosCategoria.Columns("TIPO_HIDDEN").CellValue(Bookmark)
        Case TipoCostesDescuentosLineaCatalogo.Fijos
            sdbgDescuentosCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgDescuentosCategoria.Columns("TIPO").Text = m_sFijo
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
            sdbgDescuentosCategoria.Columns("GRUPO").CellStyleSet "Gris"
            sdbgDescuentosCategoria.Columns("TIPO").Text = m_sOpcionalSimple
        Case TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
            sdbgDescuentosCategoria.Columns("TIPO").Text = m_sOpcionalExcluyente
        Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes
            sdbgDescuentosCategoria.Columns("TIPO").Text = m_sObligatorioExcluyente
    End Select
    
    Select Case sdbgDescuentosCategoria.Columns("OPERACION_HIDDEN").CellValue(Bookmark)
        Case m_sRestar
            sdbgDescuentosCategoria.Columns("OPERACION").Text = m_sRestar
        Case m_sRestarPorcentaje
            sdbgDescuentosCategoria.Columns("OPERACION").Text = m_sRestarPorcentaje
        Case Else
             sdbgDescuentosCategoria.Columns("OPERACION").Text = m_sRestar
    End Select
    
    Select Case sdbgDescuentosCategoria.Columns("AMBITO_HIDDEN").CellValue(Bookmark)
        Case ambitodelcampodepedido.cabecera
            sdbgDescuentosCategoria.Columns("AMBITO").Text = sCabecera
        Case ambitodelcampodepedido.Linea
            sdbgDescuentosCategoria.Columns("AMBITO").Text = sLinea
    End Select
    
End Sub
''' <summary>evento del grid que se produce cuando el usuario cambia de celda o fila</summary>
Private Sub sdbgdescuentosCategoria_BeforeRowColChange(Cancel As Integer)
    If sdbgDescuentosCategoria.col = 6 Then
        'Si el tipo de descuento es Obligatorios Excluyente o Opcionales Excluyente, y no se ha metido nada en la columna grupo, no dejara salir de ella
        Select Case sdbgDescuentosCategoria.Columns("TIPO_HIDDEN").Value
            Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes, TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
                If sdbgDescuentosCategoria.Columns("GRUPO").Text = "" Then
                    Cancel = True
                End If
        End Select
    End If
End Sub

''' <summary>evento del grid que se produce cuando el usuario cambia de celda o de fila</summary>
Private Sub sdbgdescuentosCategoria_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    'Evitamos que se ejecute el evento durante la carga inicial del grid en el que el valor de LastCol es -1
    'Si pasaba y habia varias filas, alguna de las cuales fuera lista, hacia que al asignarle a la columna valor el dropdown
    ', si la siguiente fila no era lista, la celda apareciera vacia aun teniendo valor
    If LastCol > -1 Then
        'Si el atributo es de tipo lista ponemos a la columna valor el dropdown
        If sdbgDescuentosCategoria.Columns(sdbgDescuentosCategoria.col).Name = "VALOR" Then
            If sdbgDescuentosCategoria.Columns("INTRO").Value = 1 Then
                'Lista
                sdbddValorDescuento.RemoveAll
                sdbddValorDescuento.AddItem ""
                sdbgDescuentosCategoria.Columns("VALOR").DropDownHwnd = sdbddValorDescuento.hWnd
                sdbddValorDescuento.Enabled = True
                sdbddValorDescuento.DroppedDown = True
                sdbddValordescuento_DropDown
                sdbddValorDescuento.DroppedDown = True
            Else
                'Introduccion libre
                sdbgDescuentosCategoria.Columns("VALOR").DropDownHwnd = 0
                sdbddValorDescuento.Enabled = False
            End If
        End If
        
        'Si el tipo de descuento es fijo o opcional simple, la columna de grupo quedara deshabilitada
        Select Case sdbgDescuentosCategoria.Columns("TIPO_HIDDEN").Value
            Case TipoCostesDescuentosLineaCatalogo.Fijos, TipoCostesDescuentosLineaCatalogo.Opcionales_Simples
                sdbgDescuentosCategoria.Columns("GRUPO").Locked = True
            Case Else
                'Si es otro tipo de descuento desbloquearemos la columna
                sdbgDescuentosCategoria.Columns("GRUPO").Locked = False
        End Select
        m_iRowActual = sdbgDescuentosCategoria.Row
        m_bA�adiendoDescuento = False
    End If
    
End Sub
''' <summary>
''' evento que salta al actualizar una columna, al cambiar de campo. una vez hechas las comprobaciones en el Before, grabaremos en BD
''' </summary>
Private Sub sdbgdescuentosCategoria_AfterColUpdate(ByVal ColIndex As Integer)
    
    Dim teserror As TipoErrorSummit
    Dim v As Variant
       
    'Si no ha habido cambios en la fila, el objeto m_odescuentoEnEdicion sera Nothing y no se grabara nada
    If Not m_oDescuentoEnEdicion Is Nothing Then
       m_bErrorUpdate = False
        ''' Actualizamos el valor de las propiedades
        m_oDescuentoEnEdicion.valor = IIf(sdbgDescuentosCategoria.Columns("VALOR").Text = "", 0, sdbgDescuentosCategoria.Columns("VALOR").Text)
        m_oDescuentoEnEdicion.OperacionCosteDescuento = sdbgDescuentosCategoria.Columns("OPERACION_HIDDEN").Text
        m_oDescuentoEnEdicion.TipoCosteDescuento = sdbgDescuentosCategoria.Columns("TIPO_HIDDEN").Text
        m_oDescuentoEnEdicion.GrupoCosteDescuento = sdbgDescuentosCategoria.Columns("GRUPO").Text
        m_oDescuentoEnEdicion.Descripcion = sdbgDescuentosCategoria.Columns("DESCR").Text
        m_oDescuentoEnEdicion.ambito = sdbgDescuentosCategoria.Columns("AMBITO_HIDDEN").Text
        
        ''' Modificamos en la base de datos
        teserror = m_oDescuentoEnEdicion.ModificarCosteDescuentoCategoriaCatalogo(m_iNivelCategoriaSeleccionada)
        
        If teserror.NumError <> TESnoerror Then
            v = sdbgDescuentosCategoria.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgDescuentosCategoria.SetFocus
            m_bErrorUpdate = True
            sdbgDescuentosCategoria.ActiveCell.Value = v
            sdbgDescuentosCategoria.DataChanged = False
            sdbgDescuentosCategoria.CancelUpdate
        Else
            Set m_oDescuentoEnEdicion = Nothing
        End If
        
        Screen.MousePointer = vbNormal
    End If
End Sub
''' <summary>
''' evento que salta al actualizar una columna, al cambiar de campo. Reliza las comprobaciones de los valores
''' </summary>
Private Sub sdbgdescuentosCategoria_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    
    Cancel = False
    
    If ColIndex >= 0 Then
        If sdbgDescuentosCategoria.Columns(ColIndex).Name = "VALOR" Then
            If sdbgDescuentosCategoria.Columns(ColIndex).Value <> "" Then
                If (Not IsNumeric(sdbgDescuentosCategoria.Columns("VALOR").Text)) Then
                    m_bErrorUpdate = True
                    oMensajes.AtributoValorNoValido ("TIPO2")
                    Cancel = True
                    GoTo Salir
                Else
                    If sdbgDescuentosCategoria.Columns("MIN").Text <> "" And sdbgDescuentosCategoria.Columns("MAX").Text <> "" Then
                        If StrToDbl0(sdbgDescuentosCategoria.Columns("MIN").Text) > StrToDbl0(sdbgDescuentosCategoria.Columns("VALOR").Text) Or StrToDbl0(sdbgDescuentosCategoria.Columns("MAX").Text) < StrToDbl0(sdbgDescuentosCategoria.Columns("VALOR").Text) Then
                            m_bErrorUpdate = True
                            oMensajes.ValorEntreMaximoYMinimo sdbgDescuentosCategoria.Columns("MIN").Text, sdbgDescuentosCategoria.Columns("MAX").Text
                            Cancel = True
                            GoTo Salir
                        End If
                    End If
                End If
                
                'Comprobaremos si es una lista, que se haya elegido un valor de la lista
                If sdbgDescuentosCategoria.Columns("INTRO").Text = "1" Then
                    Dim oAtributo As CAtributo
                    Dim bValorEncontrado As Boolean
                    Dim oLista As CValorPond
                    Set oAtributo = oFSGSRaiz.Generar_CAtributo
                    'Recogemos el atributo
                    oAtributo.Id = sdbgDescuentosCategoria.Columns("ATRIB_ID").Value
                    
                    If oAtributo.Id > 0 Then
                        'Cargamos la lista de valores del atributo
                        oAtributo.CargarDatosAtributo
                        oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
                        For Each oLista In oAtributo.ListaPonderacion
                            If oLista.ValorLista = sdbgDescuentosCategoria.Columns("VALOR").Text Then
                                bValorEncontrado = True
                                Exit For
                            End If
                        Next
                        If bValorEncontrado = False Then
                            oMensajes.AtributoValorNoValido ("NO_LISTA")
                            Cancel = True
                            GoTo Salir
                        End If
                    End If
                End If
            Else
                'El descuento no tiene valor
                m_bErrorUpdate = True
                oMensajes.AtributosOblSinRellenar
                Cancel = True
                GoTo Salir
            End If
        End If
        
        'Compruebo los campos operacion
        If sdbgDescuentosCategoria.Columns(ColIndex).Name = "OPERACION" And sdbgDescuentosCategoria.Columns(ColIndex).Text <> m_sRestar And sdbgDescuentosCategoria.Columns(ColIndex).Text <> m_sRestarPorcentaje Then
            m_bErrorUpdate = True
            MsgBox m_sOperacionNoValida, vbExclamation
            Cancel = True
            GoTo Salir
        End If
        'Compruebo el campo tipo de descuento
        If sdbgDescuentosCategoria.Columns(ColIndex).Name = "TIPO" And sdbgDescuentosCategoria.Columns("TIPO_HIDDEN").Text = "" Then
            m_bErrorUpdate = True
            MsgBox m_sTipoNoValido, vbExclamation
            Cancel = True
            GoTo Salir
        Else
            'Si el descuento es de estos 2 tipos, se debera introducir un grupo
            Select Case sdbgDescuentosCategoria.Columns("TIPO_HIDDEN").Value
                Case TipoCostesDescuentosLineaCatalogo.Obligatorios_Excluyentes, TipoCostesDescuentosLineaCatalogo.Opcionales_Excluyentes
                    'Pongo el cancel=true para que no se ejecute el AftercolUpdate y no grabe en bd
                    ', ya que hasta que no meta el grupo no se deberia actualizar
                    If sdbgDescuentosCategoria.Columns("GRUPO").Value = "" Then
                        If Not sdbgDescuentosCategoria.Columns(ColIndex).Name = "TIPO" Then
                            m_bErrorUpdate = True
                            MsgBox m_sIntroduceGrupoDescuento, vbExclamation
                            GoTo Salir
                        End If
                    End If
            End Select
        End If
    End If
    
Salir:
End Sub
''' <summary>
''' evento que salta al eliminar una fila del grid
''' </summary>
Private Sub sdbgdescuentosCategoria_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If IsEmpty(sdbgDescuentosCategoria.RowBookmark(sdbgDescuentosCategoria.Row)) Then
        sdbgDescuentosCategoria.Bookmark = sdbgDescuentosCategoria.RowBookmark(sdbgDescuentosCategoria.Row - 1)
    Else
        sdbgDescuentosCategoria.Bookmark = sdbgDescuentosCategoria.RowBookmark(sdbgDescuentosCategoria.Row)
    End If
End Sub
''' <summary>
''' Evita que se muestre el mensaje propio del grid al eliminar la fila
''' </summary>
Private Sub sdbgdescuentosCategoria_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

''' <summary>
''' evento que salta al realizar cualquier cambio en la grid
''' </summary>
Private Sub sdbgdescuentosCategoria_Change()
       
    Set m_oDescuentoEnEdicion = oFSGSRaiz.Generar_CAtributo
    'Recojo el atributo que se va a modificar
    Set m_oDescuentoEnEdicion = m_oDescuentos.Item(sdbgDescuentosCategoria.Columns("ID").Value)
End Sub

''' <summary>
''' Muestra la pantalla con todos los flujos de aprobacion de todas las categorias, y por cada aprovisionador que se seleccione, las categorias que tiene configuradas
''' </summary>
Public Sub MostrarConfiguracion()
    frmCatalogoConfiguracion.Show vbModal
End Sub
