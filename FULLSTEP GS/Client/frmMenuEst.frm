VERSION 5.00
Object = "{7B3D9A5A-0D84-4BFE-AAE3-64CDE5DDA825}#7.0#0"; "XpNetMenu.ocx"
Begin VB.Form frmMenuEst 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin OfficeXpStyleMenu.XpNetMenu XpNetMenu1 
      Left            =   0
      Top             =   0
      _ExtentX        =   926
      _ExtentY        =   926
      BmpCount        =   5
      Bmp:1           =   "frmMenuEst.frx":0000
      Mask:1          =   13160660
      Key:1           =   "#mnuConfOrdenarAsc"
      Bmp:2           =   "frmMenuEst.frx":0502
      Mask:2          =   13160660
      Key:2           =   "#mnuConfOrdenarDesc"
      Bmp:3           =   "frmMenuEst.frx":0A04
      Mask:3          =   13160660
      Key:3           =   "#mnuConfigOrdenAsc"
      Bmp:4           =   "frmMenuEst.frx":0F06
      Mask:4          =   13160660
      Key:4           =   "#mnuConfigOrdenDesc"
      Bmp:5           =   "frmMenuEst.frx":1408
      Mask:5          =   12632256
      Key:5           =   "#mnuConfigCampos"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RegName         =   ""
      RegID           =   ""
   End
   Begin VB.Menu mnuPOPUPConfig 
      Caption         =   "POPUPConfig"
      Visible         =   0   'False
      Begin VB.Menu mnuConfigOrdenAsc 
         Caption         =   "Ordenar ascendente"
      End
      Begin VB.Menu mnuConfigOrdenDesc 
         Caption         =   "Ordenar descendente"
      End
      Begin VB.Menu mnuConfigCampos 
         Caption         =   "Configurar campos"
      End
   End
End
Attribute VB_Name = "frmMenuEst"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sNomCampo As String

Private Sub Form_Load()
    Me.Width = 1
    Me.Height = 1
    CargarRecursos
    Me.Visible = False
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_EST_MENU, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        mnuConfigOrdenAsc.Caption = Ador(0).Value
        Ador.MoveNext
        mnuConfigOrdenDesc.Caption = Ador(0).Value
        Ador.MoveNext
        mnuConfigCampos.Caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

    
Private Sub Form_Unload(Cancel As Integer)
    g_sNomCampo = ""
End Sub

Private Sub mnuConfigCampos_Click()
    frmESTConfig.Show vbNormal, MDI
End Sub

Private Sub mnuConfigOrdenAsc_Click()
    'Ordena ascendente
    frmEST.OrdenarGridDesdeMenu True, g_sNomCampo
End Sub

Private Sub mnuConfigOrdenDesc_Click()
    'Ordena descendente
    frmEST.OrdenarGridDesdeMenu False, g_sNomCampo
End Sub
