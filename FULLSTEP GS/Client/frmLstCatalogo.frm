VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstCatalogo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de cat�logo"
   ClientHeight    =   5760
   ClientLeft      =   750
   ClientTop       =   2520
   ClientWidth     =   9930
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstCatalogo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   9930
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      Height          =   345
      Left            =   8865
      TabIndex        =   1
      Top             =   5370
      Width           =   1020
   End
   Begin TabDlg.SSTab sstabCatalogo 
      Height          =   5280
      Left            =   15
      TabIndex        =   0
      Top             =   30
      Width           =   9870
      _ExtentX        =   17410
      _ExtentY        =   9313
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstCatalogo.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraLinCatalog"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraOpciones"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraCategoria"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Timer1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "fraOpciones2"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      Begin VB.Frame fraOpciones2 
         Caption         =   "Opciones de categor�a"
         Height          =   795
         Left            =   120
         TabIndex        =   32
         Top             =   4320
         Width           =   9600
         Begin VB.CheckBox chkPrecModif 
            Caption         =   "Incluir permiso de modificaci�n de precios"
            Height          =   195
            Left            =   6165
            TabIndex        =   37
            Top             =   330
            Width           =   3330
         End
         Begin VB.CheckBox chkProves 
            Caption         =   "Incluir proveedores para pedido libre"
            Height          =   195
            Left            =   150
            TabIndex        =   34
            Top             =   330
            Width           =   3195
         End
         Begin VB.CheckBox chkCampos 
            Caption         =   "Incluir campos personalizados"
            Height          =   195
            Left            =   3420
            TabIndex        =   33
            Top             =   330
            Width           =   2655
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   5000
         Top             =   0
      End
      Begin VB.Frame fraCategoria 
         Caption         =   "Datos de categor�a"
         Height          =   735
         Left            =   120
         TabIndex        =   8
         Top             =   435
         Width           =   9600
         Begin VB.CommandButton cmdBorraCat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8505
            Picture         =   "frmLstCatalogo.frx":0CCE
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   270
            Width           =   315
         End
         Begin VB.CommandButton cmdSelCat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8880
            Picture         =   "frmLstCatalogo.frx":0D73
            Style           =   1  'Graphical
            TabIndex        =   9
            Top             =   270
            Width           =   315
         End
         Begin VB.Label lblCategoria 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1245
            TabIndex        =   11
            Top             =   270
            Width           =   7245
         End
      End
      Begin VB.Frame fraOpciones 
         Caption         =   "Mostrar"
         Height          =   795
         Left            =   120
         TabIndex        =   7
         Top             =   3540
         Width           =   9600
         Begin VB.CheckBox chkEspec 
            Caption         =   "Especificaciones"
            Height          =   195
            Left            =   4470
            TabIndex        =   36
            Top             =   360
            Width           =   2220
         End
         Begin VB.CheckBox chkAdjun 
            Caption         =   "Nombres de archivos adjuntos"
            Height          =   195
            Left            =   6960
            TabIndex        =   35
            Top             =   360
            Width           =   2550
         End
         Begin VB.CheckBox chkVerBajas 
            Caption         =   "Bajas l�gicas"
            Height          =   195
            Left            =   2550
            TabIndex        =   31
            Top             =   360
            Width           =   1710
         End
         Begin VB.CheckBox chkLinCatalog 
            Caption         =   "L�neas de cat�logo"
            Height          =   195
            Left            =   255
            TabIndex        =   22
            Top             =   345
            Width           =   1860
         End
      End
      Begin VB.Frame fraLinCatalog 
         Caption         =   "Datos de l�neas de cat�logo"
         Height          =   2310
         Left            =   120
         TabIndex        =   2
         Top             =   1245
         Width           =   9600
         Begin VB.CommandButton cmdBuscarProce 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8550
            Picture         =   "frmLstCatalogo.frx":0DDF
            Style           =   1  'Graphical
            TabIndex        =   23
            Top             =   1785
            Width           =   315
         End
         Begin VB.CommandButton cmdBorrarMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8535
            Picture         =   "frmLstCatalogo.frx":0E6C
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   690
            Width           =   315
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8910
            Picture         =   "frmLstCatalogo.frx":0F11
            Style           =   1  'Graphical
            TabIndex        =   12
            Top             =   690
            Width           =   315
         End
         Begin VB.CommandButton cmdBuscarProve 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   8535
            Picture         =   "frmLstCatalogo.frx":0F7D
            Style           =   1  'Graphical
            TabIndex        =   3
            Top             =   330
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2775
            TabIndex        =   4
            Top             =   330
            Width           =   5730
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6879
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2990
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   10107
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1245
            TabIndex        =   5
            Top             =   330
            Width           =   1515
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2990
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6879
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
            Height          =   285
            Left            =   1245
            TabIndex        =   14
            Top             =   1050
            Width           =   1500
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2990
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6879
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2646
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
            Height          =   285
            Left            =   2775
            TabIndex        =   15
            Top             =   1050
            Width           =   5730
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6879
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2990
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   10107
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
            Height          =   285
            Left            =   1245
            TabIndex        =   16
            Top             =   1410
            Width           =   1515
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1931
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6879
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
            Height          =   285
            Left            =   2775
            TabIndex        =   17
            Top             =   1410
            Width           =   5730
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6879
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1931
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   10107
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
            Height          =   285
            Left            =   1245
            TabIndex        =   24
            Top             =   1785
            Width           =   900
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1587
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
            Height          =   285
            Left            =   2650
            TabIndex        =   25
            Top             =   1785
            Width           =   900
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1693
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   1587
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProceCod 
            Height          =   285
            Left            =   4350
            TabIndex        =   26
            Top             =   1785
            Width           =   1050
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1667
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6879
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1852
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProceDen 
            Height          =   285
            Left            =   5415
            TabIndex        =   27
            Top             =   1785
            Width           =   3105
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6879
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1667
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5477
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblAnyo 
            Caption         =   "A�o:"
            Height          =   195
            Left            =   120
            TabIndex        =   30
            Top             =   1785
            Width           =   690
         End
         Begin VB.Label lblGMN1_4Cod 
            Caption         =   "CM:"
            Height          =   195
            Left            =   2200
            TabIndex        =   29
            Top             =   1845
            Width           =   375
         End
         Begin VB.Label lblProce 
            Caption         =   "Proceso:"
            Height          =   195
            Left            =   3675
            TabIndex        =   28
            Top             =   1845
            Width           =   690
         End
         Begin VB.Label lblDest 
            Caption         =   "Destino:"
            Height          =   195
            Left            =   120
            TabIndex        =   21
            Top             =   1425
            Width           =   840
         End
         Begin VB.Label lblMat 
            Caption         =   "Material:"
            Height          =   195
            Left            =   120
            TabIndex        =   20
            Top             =   720
            Width           =   840
         End
         Begin VB.Label lblArt 
            Caption         =   "Art�culo"
            Height          =   195
            Left            =   120
            TabIndex        =   19
            Top             =   1065
            Width           =   840
         End
         Begin VB.Label txtEstMat 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Left            =   1245
            TabIndex        =   18
            Top             =   690
            Width           =   7245
         End
         Begin VB.Label lblProve 
            Caption         =   "Proveedor:"
            Height          =   195
            Left            =   120
            TabIndex        =   6
            Top             =   375
            Width           =   840
         End
      End
   End
End
Attribute VB_Name = "frmLstCatalogo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_ador As Ador.Recordset
'Restricciones
Private m_bRMat As Boolean
Private m_bRAsig As Boolean
Private m_bRCompResp As Boolean
Private m_bREqpAsig As Boolean
Private m_bRUsuAper As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuDep As Boolean
Private bRUsuAprov As Boolean
Public bConsultBajasLog As Boolean
Private bRDest As Boolean
Private m_bMostrarSolicitud As Boolean

'Variables de categor�as
Public CATN1Seleccionada As Long
Public CATN2Seleccionada As Long
Public CATN3Seleccionada As Long
Public CATN4Seleccionada As Long
Public CATN5Seleccionada As Long
Public bCatHijos As Boolean

'Variables para combos
Private ProveCargarComboDesde As Boolean
Private ProveRespetarCombo As Boolean
Private DestRespetarCombo As Boolean
Private DestCargarComboDesde As Boolean
Private ProceRespetarCombo As Boolean
Private ProceCargarComboDesde As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN1RespetarCombo As Boolean
Private ArtiCargarComboDesde As Boolean
Private ArtiRespetarCombo As Boolean

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4
Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String

'Variables para proveedores
Private oProveSeleccionado As CProveedor
Private oProves As CProveedores

Private oDestinos As CDestinos

Private oProcesos As CProcesos

Private oArticulos As CArticulos


'Idiomas
Private sProveCod As String
Private sIdiCodigo As String
Private sIdiProceso As String
Private sIdiAnyo As String

'RPT
Private sMensajeSeleccion As String
Private sSeleccion As String
Private stxtTitulo As String
Private FormulaRpt(1, 1 To 7) As String      'formulas textos RPT
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private sIdiGenerando As String
Private stxtSeleccion As String
Private stxtCat As String
Private stxtDen As String
Private stxtBajaLog As String
Private stxtCampo1 As String
Private stxtCampo2 As String
Private stxtEspec As String
Private stxtAdjun As String
Private stxtProves As String
Private stxtEsBaja As String
Private stxtEsNoBaja As String
Private stxtEsMarcaBaja As String
Private stxtPub As String
Private stxtFecDespub As String
Private stxtProce As String
Private stxtArt As String
Private stxtProve As String
Private stxtDest As String
Private stxtUB As String
Private stxtPrecioUB As String
Private stxtUP As String
Private stxtFC As String
Private stxtCantMin As String
Private stxtPubSi As String
Private stxtAnio As String
Private stxtMat As String
Private stxtIcluirLinCat As String
Private stxtVerBajas As String
Private stxtSolicit As String
Private stxtModPrec As String
Private stxtActPrec As String
Private stxtMon As String ' a�adido el 28-2-2006

Private oFos As FileSystemObject
Private RepPath As String
    
Private Sub Arrange()

On Error Resume Next

    sstabCatalogo.Height = Me.Height - 870
    sstabCatalogo.Width = Me.Width - 180

    fraCategoria.Width = sstabCatalogo.Width - 270
    fraLinCatalog.Width = fraCategoria.Width
    fraOpciones.Width = fraCategoria.Width
    fraOpciones2.Width = fraOpciones.Width
    cmdObtener.Left = sstabCatalogo.Left + sstabCatalogo.Width - cmdObtener.Width
    cmdObtener.Top = sstabCatalogo.Top + sstabCatalogo.Height + 50

End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTCATALOGO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value '1
        stxtTitulo = Ador(0).Value
        Ador.MoveNext
        sstabCatalogo.caption = Ador(0).Value
        stxtSeleccion = Ador(0).Value & ":"
        Ador.MoveNext
        fraCategoria.caption = Ador(0).Value
        Ador.MoveNext
        fraLinCatalog.caption = Ador(0).Value
        Ador.MoveNext
        lblProve.caption = Ador(0).Value & ":"
        stxtProve = Ador(0).Value
        sProveCod = Ador(0).Value
        Ador.MoveNext
        lblMat.caption = Ador(0).Value
        stxtMat = Ador(0).Value
        Ador.MoveNext
        lblArt.caption = Ador(0).Value & ":"
        stxtArt = Ador(0).Value
        Ador.MoveNext
        lblDest.caption = Ador(0).Value & ":"
        stxtDest = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value & ":"
        stxtAnio = Ador(0).Value & ":"
        sIdiAnyo = Ador(0).Value
        Ador.MoveNext
        lblProce.caption = Ador(0).Value & ":" '10
        sIdiCodigo = Ador(0).Value
        sIdiProceso = Ador(0).Value
        stxtProce = Ador(0).Value
        Ador.MoveNext
        chkLinCatalog.caption = Ador(0).Value
        stxtIcluirLinCat = Ador(0).Value
        Ador.MoveNext
        chkVerBajas.caption = Ador(0).Value
        stxtVerBajas = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        stxtCat = Ador(0).Value
        Ador.MoveNext
        'stxtCat1 = Ador(0).Value
        Ador.MoveNext
        'stxtCat2 = Ador(0).Value
        Ador.MoveNext
        'stxtCat3 = Ador(0).Value '20
        Ador.MoveNext
        'stxtCat4 = Ador(0).Value
        Ador.MoveNext
        'stxtCat5 = Ador(0).Value
        Ador.MoveNext
        'stxtLineas = Ador(0).Value
        Ador.MoveNext
        stxtPub = Ador(0).Value
        Ador.MoveNext
        stxtFecDespub = Ador(0).Value '25
        Ador.MoveNext
        stxtUB = Ador(0).Value
        Ador.MoveNext
        stxtPrecioUB = Ador(0).Value
        Ador.MoveNext
        stxtUP = Ador(0).Value
        Ador.MoveNext
        stxtFC = Ador(0).Value
        Ador.MoveNext
        stxtCantMin = Ador(0).Value '30
        Ador.MoveNext
        
        ' FORMULAS RPT
        For i = 1 To 2
            FormulaRpt(1, i) = Ador(0).Value '31 32
            Ador.MoveNext
        Next

        sMensajeSeleccion = Ador(0).Value
        Ador.MoveNext
        fraOpciones.caption = Ador(0).Value
        Ador.MoveNext
        stxtPubSi = Ador(0).Value '35
        Ador.MoveNext
        sdbcArtiCod.Columns(0).caption = Ador(0).Value
        sdbcArtiDen.Columns(1).caption = Ador(0).Value
        sdbcDestCod.Columns(0).caption = Ador(0).Value
        sdbcDestDen.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value
        sdbcProceCod.Columns(0).caption = Ador(0).Value
        sdbcProceDen.Columns(1).caption = Ador(0).Value
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        
        Ador.MoveNext
        sdbcArtiCod.Columns(1).caption = Ador(0).Value
        sdbcArtiDen.Columns(0).caption = Ador(0).Value
        sdbcDestCod.Columns(1).caption = Ador(0).Value
        sdbcDestDen.Columns(0).caption = Ador(0).Value
        sdbcProceCod.Columns(1).caption = Ador(0).Value
        sdbcProceDen.Columns(0).caption = Ador(0).Value
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        stxtDen = Ador(0).Value
        Ador.MoveNext
        stxtSolicit = Ador(0).Value
        
        Ador.MoveNext
        stxtBajaLog = Ador(0).Value
        Ador.MoveNext
        stxtCampo1 = Ador(0).Value '40
        Ador.MoveNext
        stxtCampo2 = Ador(0).Value
        Ador.MoveNext
        stxtEspec = Ador(0).Value & ":"
        chkEspec.caption = Ador(0).Value
        Ador.MoveNext
        stxtAdjun = Ador(0).Value
        Ador.MoveNext
        stxtProves = Ador(0).Value
        Ador.MoveNext
        stxtEsBaja = Ador(0).Value
        Ador.MoveNext
        stxtEsNoBaja = Ador(0).Value
        Ador.MoveNext
        stxtEsMarcaBaja = Ador(0).Value
        Ador.MoveNext
        chkAdjun.caption = Ador(0).Value
        Ador.MoveNext
        chkProves.caption = Ador(0).Value
        Ador.MoveNext
        chkCampos.caption = Ador(0).Value '50
        Ador.MoveNext
        fraOpciones2.caption = Ador(0).Value
        Ador.MoveNext
        chkPrecModif.caption = Ador(0).Value
        Ador.MoveNext
        stxtModPrec = Ador(0).Value
        Ador.MoveNext
        stxtActPrec = Ador(0).Value
        Ador.MoveNext
        #If VERSION >= 30600 Then
            stxtMon = Ador(0).Value '55
        #End If
        Ador.Close
    End If

    Set Ador = Nothing
    
End Sub


Private Sub chkLinCatalog_Click()
'    If chkLinCatalog.Value = vbUnchecked Then
'        fraOpciones2.Enabled = True
'    Else
'        fraOpciones2.Enabled = False
'        chkProves.Value = vbUnchecked
'        chkCampos.Value = vbUnchecked
'    End If
End Sub

Private Sub cmdBorraCat_Click()
    lblCategoria = ""
    CATN1Seleccionada = 0
    CATN2Seleccionada = 0
    CATN3Seleccionada = 0
    CATN4Seleccionada = 0
    CATN5Seleccionada = 0
End Sub

Private Sub cmdBorrarMat_Click()
    txtEstMat = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    sdbcGMN1_4Cod.Text = ""
    sdbcArtiCod.Text = ""
End Sub

Private Sub cmdBuscarProce_Click()
    frmPROCEBuscar.bRDest = False
    frmPROCEBuscar.m_bProveAsigComp = False
    frmPROCEBuscar.m_bProveAsigEqp = False
    frmPROCEBuscar.bRMat = m_bRMat
    frmPROCEBuscar.bRAsig = m_bRAsig
    frmPROCEBuscar.bRCompResponsable = m_bRCompResp
    frmPROCEBuscar.bREqpAsig = m_bREqpAsig
    frmPROCEBuscar.bRUsuAper = m_bRUsuAper
    frmPROCEBuscar.bRUsuUON = m_bRUsuUON
    frmPROCEBuscar.bRUsuDep = m_bRUsuDep
    frmPROCEBuscar.sOrigen = "frmLstCatalogo"
    frmPROCEBuscar.sdbcAnyo = sdbcAnyo
    frmPROCEBuscar.sdbcGMN1Proce_Cod = sdbcGMN1_4Cod
    frmPROCEBuscar.sdbcGMN1Proce_Cod_Validate False
    frmPROCEBuscar.Show 1
End Sub

Private Sub cmdBuscarProve_Click()
    frmPROVEBuscar.sOrigen = "frmLstCatalogo"
    frmPROVEBuscar.Show 1
End Sub

Public Sub CargarProveedorConBusqueda()
    
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    ProveRespetarCombo = True
    sdbcProveCod.Text = oProves.Item(1).Cod
    sdbcProveDen.Text = oProves.Item(1).Den
    ProveRespetarCombo = False
End Sub

Private Sub cmdObtener_Click()
    
    If ComprobarSeleccion Then
        Screen.MousePointer = vbHourglass
        ObtenerListadoCatalogo
        Screen.MousePointer = vbNormal
    Else
        oMensajes.FaltanDatos (sMensajeSeleccion)
        Exit Sub
    End If
End Sub

Private Sub ObtenerListadoCatalogo()
    
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
   
    sSeleccion = GenerarTextoSeleccion
       
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
        
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptCategorias.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
        
   '*********** FORMULA FIELDS REPORT
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & FormulaRpt(1, 1) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & FormulaRpt(1, 2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & sSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & stxtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & stxtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAT")).Text = """" & stxtCat & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDEN")).Text = """" & stxtDen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBAJALOG")).Text = """" & stxtBajaLog & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAMPO1")).Text = """" & stxtCampo1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCAMPO2")).Text = """" & stxtCampo2 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEspec")).Text = """" & stxtEspec & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAdjuntos")).Text = """" & stxtAdjun & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProves")).Text = """" & stxtProves & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPub")).Text = """" & stxtPub & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFechaDespub")).Text = """" & stxtFecDespub & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProce")).Text = """" & stxtProce & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtArticulo")).Text = """" & stxtArt & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProve")).Text = """" & stxtProve & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDestino")).Text = """" & stxtDest & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUB")).Text = """" & stxtUB & """"
    'Modificado el 28-2-2006
    #If VERSION >= 30600 Then
        stxtPrecioUB = stxtPrecioUB
    #Else
        stxtPrecioUB = stxtPrecioUB & "(" & gParametrosGenerales.gsMONCEN & ")"
    #End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPrecioUB")).Text = """" & stxtPrecioUB & """"
    'A�ADIDO EL 28-2-2006
    #If VERSION >= 30600 Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMon")).Text = """" & stxtMon & """"
    #End If
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUP")).Text = """" & stxtUP & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFC")).Text = """" & stxtFC & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCantMin")).Text = """" & stxtCantMin & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSolicit")).Text = """" & stxtSolicit & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBAJA")).Text = """" & stxtEsBaja & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNORMAL")).Text = """" & stxtEsNoBaja & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtMARCA")).Text = """" & stxtEsMarcaBaja & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPubSi")).Text = """" & stxtPubSi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtModPrec")).Text = """" & stxtModPrec & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtActPrec")).Text = """" & stxtActPrec & """"
    
    If chkLinCatalog.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VLINEAS")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VLINEAS")).Text = """" & "N" & """"
    End If
    
    'Indica al report si se va a mostrar o no las solicitudes
    If m_bMostrarSolicitud = True Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VSOLICIT")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VSOLICIT")).Text = """" & "N" & """"
    End If
    If FSEPConf Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VPROCE")).Text = """" & "N" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VPROCE")).Text = """" & "S" & """"
    End If
    
    If chkCampos.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VCAMPOS")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VCAMPOS")).Text = """" & "N" & """"
    End If
    
    If chkEspec.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VESPEC")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VESPEC")).Text = """" & "N" & """"
    End If
    
    If chkAdjun.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VADJUN")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VADJUN")).Text = """" & "N" & """"
    End If
    
    If chkProves.Value = vbChecked Then
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VPROVES")).Text = """" & "S" & """"
    Else
        oReport.FormulaFields(crs_FormulaIndex(oReport, "VPROVES")).Text = """" & "N" & """"
    End If

    If basOptimizacion.gTipoDeUsuario = comprador Then
        Set g_ador = oGestorInformes.ListadoCatalogo2(CATN1Seleccionada, CATN2Seleccionada, CATN3Seleccionada, CATN4Seleccionada, CATN5Seleccionada, sdbcProveCod.Text, sdbcGMN1_4Cod.Text, sGMN2Cod, sGMN3Cod, sGMN4Cod, sdbcArtiCod.Text, sdbcDestCod.Text, val(sdbcAnyo.Text), val(sdbcProceCod.Text), m_bRMat, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, chkLinCatalog.Value, chkVerBajas.Value, bRUsuAprov, gParametrosInstalacion.gIdioma, IIf(chkPrecModif.Value = vbChecked, True, False))
    Else
        Set g_ador = oGestorInformes.ListadoCatalogo2(CATN1Seleccionada, CATN2Seleccionada, CATN3Seleccionada, CATN4Seleccionada, CATN5Seleccionada, sdbcProveCod.Text, sdbcGMN1_4Cod.Text, sGMN2Cod, sGMN3Cod, sGMN4Cod, sdbcArtiCod.Text, sdbcDestCod.Text, val(sdbcAnyo.Text), val(sdbcProceCod.Text), , , basOptimizacion.gCodPersonaUsuario, chkLinCatalog.Value, chkVerBajas.Value, bRUsuAprov, gParametrosInstalacion.gIdioma, IIf(chkPrecModif.Value = vbChecked, True, False))
    End If
    
    If g_ador Is Nothing Then
        Screen.MousePointer = vbNormal
        oMensajes.NoHayDatos
        Set oReport = Nothing
        Exit Sub
    Else
        oReport.Database.SetDataSource g_ador
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If

    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Me.Hide
    
    Set pv = New Preview
    pv.g_sOrigen = "frmLstCatalogo"
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show

    Timer1.Enabled = True
    pv.caption = stxtTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False

    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing

End Sub

Private Function GenerarTextoSeleccion() As String

    Dim sUnion As String

    sSeleccion = ""
    sUnion = ""
    
    If lblCategoria <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtCat & " " & lblCategoria
    End If
    
    If sdbcProveCod.Value <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtProve & ": " & Trim(sdbcProveCod.Value)
    End If
    
    If sGMN4Cod <> "" Or sGMN3Cod <> "" Or sGMN2Cod <> "" Or sGMN1Cod <> "" Then
        If txtEstMat <> "" Then
            If sSeleccion <> "" Then sUnion = "; "
            sSeleccion = sSeleccion & sUnion & stxtMat & " " & txtEstMat
        Else
            If sSeleccion <> "" Then sUnion = "; "
            sSeleccion = sSeleccion & sUnion & stxtMat & " " & sGMN1Cod
        End If
    Else
        If sdbcGMN1_4Cod.Value <> "" Then
            If sSeleccion <> "" Then sUnion = "; "
            sSeleccion = sSeleccion & sUnion & stxtMat & " " & sdbcGMN1_4Cod.Value
        End If
    End If
    
    If sdbcArtiCod.Value <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtArt & ": " & sdbcArtiCod.Value
    End If
    
    If sdbcDestCod.Value <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtDest & ": " & sdbcDestCod.Value
    End If
    
    If val(sdbcAnyo.Value) <> 0 Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtAnio & " " & Format(val(sdbcAnyo.Value), "0000")
    End If

    If sdbcProceCod.Value <> "" Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtProce & ": " & sdbcProceCod.Value
    End If

    If chkLinCatalog.Value = vbChecked Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtIcluirLinCat
    End If
        
    If chkVerBajas.Value = vbChecked Then
        If sSeleccion <> "" Then sUnion = "; "
        sSeleccion = sSeleccion & sUnion & stxtVerBajas
    End If

    GenerarTextoSeleccion = sSeleccion

End Function
Private Function ComprobarSeleccion() As Boolean
    'como m�nimo ha de seleccionarse la categor�a o alg�n filtro para las
    'las l�neas de cat�logo
    If lblCategoria = "" Then
        If sdbcProveCod.Text = "" And txtEstMat = "" And sdbcArtiCod.Text = "" And sdbcDestCod.Text = "" And sdbcAnyo.Text = "" And sdbcGMN1_4Cod = "" And sdbcProceCod.Text = "" Then
            ComprobarSeleccion = False
            Exit Function
        End If
    End If
    ComprobarSeleccion = True
End Function

Private Sub cmdSelCat_Click()
'    If chkLinCatalog.Value = vbChecked Then
        frmSELCat.bRUsuAprov = bRUsuAprov
        frmSELCat.bVerBajas = chkVerBajas.Value
        frmSELCat.sOrigen = "frmLstCatalogo"
        frmSELCat.Show 1
'    End If
End Sub

Private Sub cmdSelMat_Click()
'    If sdbcProveCod.Text <> "" Then
'        Set frmSELMATProve.oProveedor = oProveSeleccionado
'        frmSELMATProve.sOrigen = "frmLstCatalogo"
'        frmSELMATProve.bRMat = m_bRMat
'        frmSELMATProve.Show 1
'    Else
        frmSELMAT.sOrigen = "frmLstCatalogo"
        frmSELMAT.bRComprador = m_bRMat
        frmSELMAT.bRCompResponsable = m_bRCompResp
        frmSELMAT.bRUsuAper = m_bRUsuAper
        frmSELMAT.m_PYME = oUsuarioSummit.pyme 'obtener codigo pyme del usuario en caso de no ser usuario pyme se devolvera 0
        frmSELMAT.Show 1
'    End If
End Sub

Public Sub PonerMatProveSeleccionado()
         
    If sGMN1Cod <> "" Then
        txtEstMat = sGMN1Cod
    End If
    
    If sGMN2Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    End If
    
    If sGMN3Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    End If
    
    If sGMN4Cod <> "" Then
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    End If

    sdbcGMN1_4Cod.Text = sGMN1Cod
End Sub

Public Sub PonerMatSeleccionado()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
        txtEstMat = sGMN1Cod
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sGMN1Cod
    GMN1RespetarCombo = False
End Sub

Private Sub Form_Load()

    Me.Top = 1500
    Me.Left = 1500
    Me.Width = 10050
    Me.Height = 6165
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    lblGMN1_4Cod.caption = gParametrosGenerales.gsabr_GMN1 & ":"
    
    chkLinCatalog.Value = vbChecked
    If Not bConsultBajasLog Then
        chkVerBajas.Visible = False
    End If
    
    If FSEPConf Then
        lblAnyo.Visible = False
        sdbcAnyo.Visible = False
        lblGMN1_4Cod.Visible = False
        sdbcGMN1_4Cod.Visible = False
        lblProce.Visible = False
        sdbcProceCod.Visible = False
        sdbcProceDen.Visible = False
        cmdBuscarProce.Visible = False
        
        fraLinCatalog.Height = fraLinCatalog.Height - 350
        fraOpciones.Top = fraLinCatalog.Top + fraLinCatalog.Height + 50
        fraOpciones2.Top = fraOpciones.Top + fraOpciones.Height + 50
        sstabCatalogo.Height = sstabCatalogo.Height - 350
        cmdObtener.Top = cmdObtener.Top - 350
        Me.Height = Me.Height - 350
    End If
    
    CargarAnyos

    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oProves = oFSGSRaiz.generar_CProveedores
    Set oProcesos = oFSGSRaiz.Generar_CProcesos

End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    bRUsuAprov = False
    bConsultBajasLog = True
    bRDest = False
        
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestMatComp)) Is Nothing) Then
            m_bRMat = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestAsignado)) Is Nothing) Then
            m_bRAsig = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestResponsable)) Is Nothing) Then
            m_bRCompResp = True
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestEqpAsignado)) Is Nothing) Then
            m_bREqpAsig = True
        End If
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuAper)) Is Nothing) Then
        m_bRUsuAper = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuUON)) Is Nothing) Then
        m_bRUsuUON = True
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuDep)) Is Nothing) Then
        m_bRUsuDep = True
    End If
    
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGBajaLogCon)) Is Nothing) Then
        bConsultBajasLog = False
    End If
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuAprov)) Is Nothing) Then
        bRUsuAprov = True
    End If
    If (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestDest)) Is Nothing)) Then
        bRDest = True
    End If
    
    'Comprueba si se visualizar�n o no las solicitudes de compras
    m_bMostrarSolicitud = True
    If FSEPConf Then
        m_bMostrarSolicitud = False
    Else
        If basParametros.gParametrosGenerales.gbSolicitudesCompras = False Then
            m_bMostrarSolicitud = False
        End If
    End If
End Sub

Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    'sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oGruposMN1 = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    Set oProveSeleccionado = Nothing
    Set oProves = Nothing
    Set oDestinos = Nothing
    Set oProcesos = Nothing
    Set oArticulos = Nothing

End Sub

Private Sub sdbcAnyo_Click()
    sdbcProceCod = ""
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceCod.RemoveAll
    sdbcProceDen.RemoveAll
End Sub

Private Sub sdbcArtiCod_Change()
    If Not ArtiRespetarCombo Then
    
        ArtiRespetarCombo = True
        sdbcArtiDen.Text = ""
        ArtiRespetarCombo = False
        ArtiCargarComboDesde = True
    
    End If

End Sub

Private Sub sdbcArtiCod_CloseUp()
'    If sdbcArtiCod.Value = "..." Then
'        sdbcArtiCod.Text = ""
'        Exit Sub
'    End If
    
    ArtiRespetarCombo = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    ArtiRespetarCombo = False
    
    ArtiCargarComboDesde = False

End Sub

Private Sub sdbcArtiCod_DropDown()
    
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcArtiCod.RemoveAll
    
    If oGMN4Seleccionado Is Nothing Then
        Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
        Screen.MousePointer = vbNormal
    End If
    Dim iPyme As Integer
    iPyme = oUsuarioSummit.pyme 'obtengo el codigo pyme del usuario si el usuario no es pyme nos pasa un 0
    
    oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , , True, True, , , , , , , , , , , , , , , , , , iPyme
    Set oArticulos = oGMN4Seleccionado.ARTICULOS
    
    Codigos = oArticulos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcArtiCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcArtiCod_InitColumnProps()
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
PositionList sdbcArtiCod, Text
End Sub

Private Sub sdbcArtiCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    If sdbcArtiCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcProveCod.Text), 1, True, sdbcArtiCod.Text, True
    Screen.MousePointer = vbNormal
        
    bExiste = Not (oArticulos.Count = 0)
            
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        ArtiRespetarCombo = True
        sdbcArtiDen.Text = oArticulos.Item(1).Den
        
        sdbcArtiCod.Columns(0).Value = sdbcArtiCod.Text
        sdbcArtiCod.Columns(1).Value = sdbcArtiDen.Text
        
        ArtiRespetarCombo = False
        ArtiCargarComboDesde = False
    End If
    
End Sub

Private Sub sdbcArtiDen_Change()
    If Not ArtiRespetarCombo Then
    
        ArtiRespetarCombo = True
        sdbcArtiCod.Text = ""
        ArtiRespetarCombo = False
        ArtiCargarComboDesde = True
    
    End If

End Sub

Private Sub sdbcArtiDen_CloseUp()
'    If sdbcArtiDen.Value = "..." Then
'        sdbcArtiDen.Text = ""
'        Exit Sub
'    End If
    
    ArtiRespetarCombo = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    ArtiRespetarCombo = False
    
    ArtiCargarComboDesde = False

End Sub


Private Sub sdbcArtiDen_DropDown()
    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    
    sdbcArtiDen.RemoveAll
    
'    If txtEstMat = "" Then
'        If ArtiCargarComboDesde Then
'            oArticulos.CargarTodosLosArticulos sdbcArtiCod.Text
'        Else
'            oArticulos.CargarTodosLosArticulos
'        End If
'    Else
'        If ArtiCargarComboDesde Then
'            oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcProveCod.Text), 1, True, sdbcArtiCod.Text, False
'        Else
'            oArticulos.CargarTodosLosArticulosPorNivelDeMaterialYProveedor sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, Trim(sdbcProveCod.Text), 1, True, sdbcArtiCod.Text, False
'        End If
'    End If

    If oGMN4Seleccionado Is Nothing Then
        Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
        Screen.MousePointer = vbNormal
    End If
    
    Dim iPyme As Integer
    iPyme = oUsuarioSummit.pyme 'obtengo el codigo pyme del usuario si el usuario no es pyme nos pasa un 0
    
    oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , , True, True, , , , , , , , , , , , , , , , , , iPyme
    Set oArticulos = oGMN4Seleccionado.ARTICULOS
    
    Codigos = oArticulos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcArtiDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
'    If ArtiCargarComboDesde And Not oArticulos.EOF Then
'        sdbcArtiDen.AddItem "..."
'    End If

    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcArtiDen_InitColumnProps()
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
PositionList sdbcArtiDen, Text
End Sub

Private Sub sdbcDestCod_Change()
    If Not DestRespetarCombo Then
    
        DestRespetarCombo = True
        sdbcDestDen.Text = ""
        DestRespetarCombo = False
        
        DestCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcDestCod_CloseUp()
    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    
    DestRespetarCombo = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    DestRespetarCombo = False
    
    DestCargarComboDesde = False

End Sub


Private Sub sdbcDestCod_DropDown()
Dim ADORs As Recordset

  '  If sGMN4Cod = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oDestinos = Nothing
    
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    
    sdbcDestCod.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Columns(0).Text), , , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
    Else
        Set ADORs = oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Columns(0).Text), , , bRDest)
    End If
    
    If ADORs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCod.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing


    sdbcDestCod.SelStart = 0
    sdbcDestCod.SelLength = Len(sdbcDestCod.Text)
    sdbcDestCod.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcDestCod_InitColumnProps()
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub


Private Sub sdbcDestCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    If sdbcDestCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Destino
    
    Screen.MousePointer = vbHourglass
    oDestinos.CargarTodosLosDestinos sdbcDestCod.Text, , True, , , , , , , False, True, False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oDestinos.Count = 0)
    
    If Not bExiste Then
        sdbcDestCod.Text = ""
    Else
        DestRespetarCombo = True
        sdbcDestDen.Text = oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcDestCod.Columns(0).Value = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Value = sdbcDestDen.Text
        DestRespetarCombo = False
        
        DestCargarComboDesde = False
    End If
    
End Sub


Private Sub sdbcDestDen_Change()
    If Not DestRespetarCombo Then
    
        DestRespetarCombo = True
        sdbcDestCod.Text = ""
        DestRespetarCombo = False
        DestCargarComboDesde = True
    
    End If
End Sub

Private Sub sdbcDestDen_CloseUp()
    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    
    DestRespetarCombo = True
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    DestRespetarCombo = False
    
    DestCargarComboDesde = False

End Sub


Private Sub sdbcDestDen_DropDown()
Dim ADORs As Recordset

   ' If sGMN4Cod = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Set oDestinos = Nothing
    
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    
    sdbcDestDen.RemoveAll
    
    If Not oUsuarioSummit.Persona Is Nothing Then
        Set ADORs = oDestinos.DevolverTodosLosDestinos(, CStr(sdbcDestDen.Columns(0).Text), , bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
    Else
        Set ADORs = oDestinos.DevolverTodosLosDestinos(, CStr(sdbcDestDen.Columns(0).Text), , bRDest)
    End If
    
    If ADORs Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDen.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    ADORs.Close
    Set ADORs = Nothing


    sdbcDestDen.SelStart = 0
    sdbcDestDen.SelLength = Len(sdbcDestDen.Text)
    sdbcDestDen.Refresh
    
    Screen.MousePointer = vbNormal
    
End Sub


Private Sub sdbcDestDen_InitColumnProps()
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcDestDen_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcDestCod.DroppedDown = False
        sdbcDestCod.Text = ""
        sdbcDestCod.RemoveAll
        sdbcDestDen.DroppedDown = False
        sdbcDestDen.Text = ""
        sdbcDestDen.RemoveAll
    End If
End Sub


Private Sub sdbcGMN1_4Cod_Change()
    If Not GMN1RespetarCombo Then
    
        GMN1CargarComboDesde = True
        Set oGMN1Seleccionado = Nothing
        Set oGMN2Seleccionado = Nothing
        Set oGMN3Seleccionado = Nothing
        Set oGMN4Seleccionado = Nothing
        sdbcProceCod.Text = ""
        sdbcProceCod.RemoveAll
        sdbcProceDen.Text = ""
        sdbcProceDen.RemoveAll
    End If

End Sub

Private Sub sdbcGMN1_4Cod_Click()
    If Not sdbcGMN1_4Cod.DroppedDown Then
        sdbcGMN1_4Cod = ""
    End If
End Sub


Private Sub sdbcGMN1_4Cod_CloseUp()
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1CargarComboDesde = False
    
    sdbcProceCod.Columns(0).Value = ""
    sdbcProceCod.Columns(1).Value = ""
    sdbcProceDen.Text = ""

End Sub


Private Sub sdbcGMN1_4Cod_DropDown()
Dim Codigos As TipoDatosCombo
Dim oIProveAsig As ICompProveAsignados
Dim oIMAsig As IMaterialAsignado
Dim i As Integer
       
    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    If GMN1CargarComboDesde Then
    
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
             Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        End If
    Else
        If m_bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , , , False)
        Else
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
             oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
        End If
    End If
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcGMN1_4Cod_InitColumnProps()
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub




Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    Dim oIBaseDatos As IBaseDatos
    Dim oGMN1 As CGrupoMatNivel1
    Dim oIMAsig As IMaterialAsignado
    Dim scod1 As String
    
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text Then
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If m_bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(1, Trim(sdbcGMN1_4Cod), , True, False)
        scod1 = sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sdbcGMN1_4Cod.Text))
        If oGruposMN1.Item(scod1) Is Nothing Then
            'No existe
            sdbcGMN1_4Cod.Text = ""
            
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGruposMN1.Item(scod1)
            GMN1CargarComboDesde = False
        End If
        
    Else
        Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
        oGMN1.Cod = sdbcGMN1_4Cod
        Set oIBaseDatos = oGMN1
        
        bExiste = oIBaseDatos.ComprobarExistenciaEnBaseDatos
        
        If Not bExiste Then
            sdbcGMN1_4Cod.Text = ""
            
        Else
            Set oGMN1Seleccionado = Nothing
            Set oGMN1Seleccionado = oGMN1
            GMN1CargarComboDesde = False
        End If
    
    End If
    
    
    Set oGMN1 = Nothing
    Set oIBaseDatos = Nothing
    Set oGruposMN1 = Nothing
    Set oIMAsig = Nothing
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProceCod_Change()
    If Not ProceRespetarCombo Then
    
        ProceRespetarCombo = True
        sdbcProceDen.Text = ""
        ProceRespetarCombo = False
        ProceCargarComboDesde = True
    
    End If

End Sub

Private Sub sdbcProceCod_Click()
    If Not sdbcProceCod.DroppedDown Then
        sdbcProceCod = ""
    End If
End Sub


Private Sub sdbcProceCod_CloseUp()
    If sdbcProceCod.Value = "..." Or sdbcProceCod = "" Then
        sdbcProceCod.Text = ""
        Exit Sub
    End If
    
    ProceRespetarCombo = True
    sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
    sdbcProceCod.Text = sdbcProceCod.Columns(0).Text
    ProceRespetarCombo = False
    
    ProceCargarComboDesde = False

End Sub


Private Sub sdbcProceCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
    sdbcProceCod.RemoveAll
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcAnyo.Value = "" Then
        sdbcProceCod.Value = ""
        oMensajes.FaltanDatos sIdiAnyo
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If ProceCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , val(sdbcProceCod), , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    End If
    
    Codigos = oProcesos.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i) & Chr(m_lSeparador) & ""
    Next
    
    If Not oProcesos.EOF Then
        sdbcProceCod.AddItem "..."
    End If

    sdbcProceCod.SelStart = 0
    sdbcProceCod.SelLength = Len(sdbcProceCod.Text)
    sdbcProceCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProceCod_InitColumnProps()
    sdbcProceCod.DataFieldList = "Column 0"
    sdbcProceCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProceCod_PositionList(ByVal Text As String)
PositionList sdbcProceCod, Text
End Sub



Private Sub sdbcProceCod_Validate(Cancel As Boolean)
    
    If sdbcProceCod.Text = "" Then Exit Sub
    
    If sdbcAnyo.Value = "" Then
        sdbcProceCod.Value = ""
        oMensajes.FaltanDatos sIdiAnyo
        Exit Sub
    End If
    
    If Not IsNumeric(sdbcProceCod) Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiCodigo
        Exit Sub
    End If
    
    If sdbcGMN1_4Cod.Text = "" Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If sdbcProceCod.Value > giMaxProceCod Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceCod.Columns(0).Text Then
        ProceRespetarCombo = True
        sdbcProceDen.Text = sdbcProceCod.Columns(1).Text
        ProceRespetarCombo = False
        Exit Sub
    End If
    
    If sdbcProceCod.Text = sdbcProceDen.Columns(1).Text Then
        ProceRespetarCombo = True
        sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
        ProceRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proceso
    oProcesos.CargarTodosLosProcesosDesde 1, ParcialmenteCerrado, ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorCod, sdbcAnyo, sdbcGMN1_4Cod, , , , val(sdbcProceCod), , True, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    
    If oProcesos.Count = 0 Then
        sdbcProceCod.Text = ""
        Screen.MousePointer = vbNormal
        oMensajes.NoValido sIdiProceso
    Else
        ProceRespetarCombo = True
        sdbcProceDen.Text = oProcesos.Item(1).Den
        
        sdbcProceCod.Columns(0).Text = sdbcProceCod.Text
        sdbcProceCod.Columns(1).Text = sdbcProceDen.Text
        
        ProceRespetarCombo = False
        ProceCargarComboDesde = False
    End If

End Sub


Private Sub sdbcProceDen_Change()
    If Not ProceRespetarCombo Then
    
        ProceRespetarCombo = True
        sdbcProceCod.Text = ""
        ProceRespetarCombo = False
        ProceCargarComboDesde = True
    
    End If

End Sub

Private Sub sdbcProceDen_Click()
    If Not sdbcProceDen.DroppedDown Then
        sdbcProceDen.Text = ""
        sdbcProceCod.Text = ""
    End If
End Sub


Private Sub sdbcProceDen_CloseUp()
    If sdbcProceDen.Value = "....." Or sdbcProceDen = "" Then
        sdbcProceDen.Text = ""
        Exit Sub
    End If
    
    ProceRespetarCombo = True
    sdbcProceCod.Text = sdbcProceDen.Columns(1).Text
    sdbcProceDen.Text = sdbcProceDen.Columns(0).Text
    ProceRespetarCombo = False
    ProceCargarComboDesde = False

End Sub


Private Sub sdbcProceDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
           
    sdbcProceDen.RemoveAll
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    If sdbcAnyo.Value = "" Then
        sdbcProceDen.Value = ""
        oMensajes.FaltanDatos sIdiAnyo
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If ProceCargarComboDesde Then
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , , , sdbcProceDen, False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    Else
        oProcesos.CargarTodosLosProcesosDesde gParametrosInstalacion.giCargaMaximaCombos, TipoEstadoProceso.ParcialmenteCerrado, TipoEstadoProceso.ConAdjudicacionesNotificadas, TipoOrdenacionProcesos.OrdPorDen, sdbcAnyo, sdbcGMN1_4Cod, , , , , , False, False, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, m_bRMat, m_bRAsig, m_bRCompResp, m_bREqpAsig, m_bRUsuAper, m_bRUsuUON, m_bRUsuDep, basOptimizacion.gvarCodUsuario, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario
    End If
        
    Codigos = oProcesos.DevolverLosCodigos
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProceDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i) & Chr(m_lSeparador) & ""
    Next
    
    If Not oProcesos.EOF Then
        sdbcProceDen.AddItem "....."
    End If

    sdbcProceDen.SelStart = 0
    sdbcProceDen.SelLength = Len(sdbcProceDen.Text)
    sdbcProceDen.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProceDen_InitColumnProps()
    sdbcProceDen.DataFieldList = "Column 0"
    sdbcProceDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProceDen_PositionList(ByVal Text As String)
PositionList sdbcProceDen, Text
End Sub


Private Sub sdbcProveCod_Change()
    
    If Not ProveRespetarCombo Then
    
        ProveRespetarCombo = True
        sdbcProveDen.Text = ""
        ProveRespetarCombo = False
        txtEstMat = ""
        sGMN1Cod = ""
        sGMN2Cod = ""
        sGMN3Cod = ""
        sGMN4Cod = ""
        
        ProveCargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcProveCod_Click()
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "" Or sdbcProveCod.Value = "..." Then
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    ProveRespetarCombo = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Value
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Value
    ProveRespetarCombo = False
    
    ProveedorSeleccionado

End Sub


Private Sub sdbcProveCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
    
    If oProves Is Nothing Then
        Set oProves = oFSGSRaiz.generar_CProveedores
    End If
    
    'Restricci�n de material en proveedores
    If m_bRMat Then
        oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
    Else
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text)
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..." & Chr(m_lSeparador) & "....."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProveCod_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        sdbcProveCod.DroppedDown = False
        sdbcProveCod.Text = ""
        sdbcProveCod.RemoveAll
        sdbcProveDen.DroppedDown = False
        sdbcProveDen.Text = ""
        sdbcProveDen.RemoveAll
    End If
End Sub


Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub


Private Sub sdbcProveCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean

    If Trim(sdbcProveCod.Text) = "" Then Exit Sub

    ''' Solo continuamos si existe el proveedor

    Screen.MousePointer = vbHourglass

    'Restricci�n de material en proveedores
    If m_bRMat Then
        oProves.BuscarProveedoresConMatDelCompDesde 1, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, sdbcProveCod.Text
    Else
        oProves.BuscarTodosLosProveedoresDesde 1, , Trim(sdbcProveCod.Text)
    End If

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))

    Screen.MousePointer = vbNormal

    If Not bExiste Then
        sdbcProveCod.Text = ""
'        oMensajes.NoValido sProveCod
        Screen.MousePointer = Normal
    Else
        ProveRespetarCombo = True
        sdbcProveDen = oProves.Item(1).Den
        ProveRespetarCombo = False
        sProveCod = sdbcProveCod
        ProveedorSeleccionado
    End If

End Sub


Private Sub sdbcProveDen_Change()
     
     If Not ProveRespetarCombo Then
        ProveRespetarCombo = True
        sdbcProveCod.Text = ""
        ProveRespetarCombo = False
        txtEstMat = ""
        sGMN1Cod = ""
        sGMN2Cod = ""
        sGMN3Cod = ""
        sGMN4Cod = ""
        
        ProveCargarComboDesde = True
        sdbcProveCod = ""
    End If

End Sub

Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub


Private Sub sdbcProveDen_CloseUp()
    
    If sdbcProveDen.Value = "" Or sdbcProveDen.Value = "....." Then
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    ProveRespetarCombo = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Value
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Value
    sProveCod = sdbcProveCod
    ProveRespetarCombo = False

    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)

    ProveedorSeleccionado

End Sub


Private Sub sdbcProveDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll
    
    If oProves Is Nothing Then
        Set oProves = oFSGSRaiz.generar_CProveedores
    End If
    
    'Restricci�n de material en proveedores
    If m_bRMat Then
        oProves.BuscarProveedoresConMatDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , Trim(sdbcProveDen), , , , , , , True
    Else
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , Trim(sdbcProveDen), , , , , , , , , , , True
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..." & Chr(m_lSeparador) & "....."
    End If
    
    Screen.MousePointer = vbNormal

End Sub


Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub


Private Sub ProveedorSeleccionado()
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
    If oProveSeleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    txtEstMat = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    
'    If gParametrosGenerales.giINSTWEB = ConPortal Then
'        oProves.CargarDatosProveedor sdbcProveCod.Text
'        If ((IsNull(oProves.Item(CStr(sdbcProveCod.Text)).CodPortal) Or (oProves.Item(CStr(sdbcProveCod.Text)).CodPortal = ""))) Then
'            oMensajes.ProveCatalogProvePortal
'            Screen.MousePointer = vbNormal
'            sdbcProveCod.Text = ""
'            Set oProveSeleccionado = Nothing
'            Exit Sub
'        End If
'    End If
                
    If gParametrosGenerales.giINSTWEB = conweb Then
        oProves.CargarDatosProveedor sdbcProveCod.Text, True
        If oProves.Item(CStr(sdbcProveCod.Text)).UsuarioWeb Is Nothing Then
            oMensajes.ProveCatalogProveWeb
            Screen.MousePointer = vbNormal
            sdbcProveCod.Text = ""
            Set oProveSeleccionado = Nothing
            Exit Sub
        End If
    End If
                
    Screen.MousePointer = vbNormal

End Sub

Public Sub CargarProcesoConBusqueda()

    Set oProcesos = frmPROCEBuscar.oProceEncontrados
    Set frmPROCEBuscar.oProceEncontrados = Nothing
    sdbcAnyo.Text = oProcesos.Item(1).Anyo
    sdbcGMN1_4Cod.Text = oProcesos.Item(1).GMN1Cod
'    If sGMN1Cod <> oProcesos.Item(1).GMN1Cod And txtEstMat.Caption <> "" Then
'        txtEstMat.Caption = oProcesos.Item(1).GMN1Cod
'        sGMN2Cod = ""
'        sGMN3Cod = ""
'        sGMN4Cod = ""
'    End If
    sGMN1Cod = oProcesos.Item(1).GMN1Cod
    sdbcGMN1_4Cod_Validate False
    sdbcProceCod.Text = oProcesos.Item(1).Cod
    sdbcProceCod_Validate False
                
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub
