VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmConfiguracionVista 
   Caption         =   "Configuraci�n de vista"
   ClientHeight    =   8040
   ClientLeft      =   90
   ClientTop       =   1875
   ClientWidth     =   11265
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConfiguracionVista.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   8040
   ScaleWidth      =   11265
   Begin VB.PictureBox picControlVistaGrupo 
      BackColor       =   &H00A56E3A&
      Height          =   6525
      Left            =   3240
      ScaleHeight     =   6465
      ScaleWidth      =   7785
      TabIndex        =   20
      Top             =   360
      Visible         =   0   'False
      Width           =   7845
      Begin VB.CheckBox chkOcultarItCerradosGr 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar items cerrados"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   195
         TabIndex        =   50
         Top             =   4785
         Width           =   4080
      End
      Begin VB.CheckBox chkOcultarNoAdjGr 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar ofertas no adjudicables"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   4395
         TabIndex        =   49
         Top             =   4785
         Width           =   2880
      End
      Begin VB.CheckBox chkExcluirItCerradosResul 
         BackColor       =   &H00A56E3A&
         Caption         =   "Keine geschlossenen Items in die Ergebnisse einschliessen"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   195
         TabIndex        =   48
         Top             =   5025
         Width           =   4065
      End
      Begin VB.CheckBox chkOcultarProvSinOfeGr 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar proveedores sin ofertas"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   4395
         TabIndex        =   47
         Top             =   5025
         Width           =   2880
      End
      Begin VB.TextBox txtDecPorcenGr 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4650
         TabIndex        =   46
         Top             =   5670
         Width           =   400
      End
      Begin VB.TextBox txtDecCantGr 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6315
         TabIndex        =   45
         Top             =   5670
         Width           =   400
      End
      Begin VB.TextBox txtDecResultGr 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1530
         TabIndex        =   44
         Top             =   5670
         Width           =   400
      End
      Begin VB.TextBox txtDecPrecGr 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3075
         TabIndex        =   43
         Top             =   5670
         Width           =   400
      End
      Begin VB.CommandButton cmdRenombrarVistaGr 
         Caption         =   "Renombrar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5760
         TabIndex        =   26
         Top             =   6120
         Width           =   1725
      End
      Begin VB.CommandButton cmdEliminarVistaGr 
         Caption         =   "Eliminar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3975
         TabIndex        =   25
         Top             =   6120
         Width           =   1725
      End
      Begin VB.CommandButton cmdGuardarVistaNuevaGr 
         Caption         =   "D&Guardar en  vista nueva"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2160
         TabIndex        =   24
         Top             =   6120
         Width           =   1725
      End
      Begin VB.CommandButton cmdGuardarVistaGr 
         Caption         =   "D&Guardar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   23
         Top             =   6120
         Width           =   1725
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOcultarCamposGr 
         Height          =   2700
         Left            =   120
         TabIndex        =   21
         Top             =   1680
         Width           =   7545
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   5
         stylesets.count =   3
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmConfiguracionVista.frx":014A
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmConfiguracionVista.frx":049C
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Seleccionada"
         stylesets(2).ForeColor=   16777215
         stylesets(2).BackColor=   8388608
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmConfiguracionVista.frx":04B8
         DividerType     =   0
         BevelColorFrame =   -2147483646
         RowSelectionStyle=   1
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         PictureButton   =   "frmConfiguracionVista.frx":04D4
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   185
         Columns.Count   =   5
         Columns(0).Width=   450
         Columns(0).Name =   "INC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadBackColor=   12615680
         Columns(1).Width=   5874
         Columns(1).Caption=   "Campo"
         Columns(1).Name =   "CAMPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadBackColor=   12615680
         Columns(1).HeadStyleSet=   "Normal"
         Columns(2).Width=   5900
         Columns(2).Caption=   "Pertenece"
         Columns(2).Name =   "Pertenece"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadBackColor=   12615680
         Columns(2).HeadStyleSet=   "Normal"
         Columns(3).Width=   661
         Columns(3).Caption=   "Detal."
         Columns(3).Name =   "POND"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   4
         Columns(3).ButtonsAlways=   -1  'True
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HeadForeColor=   16777215
         Columns(3).HeadBackColor=   12615680
         Columns(3).HeadStyleSet=   "Normal"
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "COD"
         Columns(4).Name =   "COD"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   13309
         _ExtentY        =   4762
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaDefectoGr 
         Height          =   255
         Left            =   2115
         TabIndex        =   56
         Top             =   585
         Width           =   5310
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   3
         Columns(0).Width=   9366
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   9366
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   16777215
         BackColor       =   12615680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaActGr 
         Height          =   255
         Left            =   2115
         TabIndex        =   57
         Top             =   240
         Width           =   5310
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   3
         Columns(0).Width=   9366
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   9366
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblPlantillaGR 
         BackColor       =   &H00A56E3A&
         BackStyle       =   0  'Transparent
         Caption         =   "DPlantilla"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   58
         Top             =   285
         Width           =   3795
      End
      Begin VB.Shape Shape1 
         BorderColor     =   &H00FFFFFF&
         Height          =   6465
         Left            =   0
         Top             =   0
         Width           =   7740
      End
      Begin VB.Label lblVistaDefectoGr 
         BackColor       =   &H00A56E3A&
         Caption         =   "DVista por defecto:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   59
         Top             =   630
         Width           =   1920
      End
      Begin VB.Line Line11 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7720
         Y1              =   4680
         Y2              =   4680
      End
      Begin VB.Line Line9 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7720
         Y1              =   5265
         Y2              =   5265
      End
      Begin VB.Label lblNumDecGr 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DN�mero de decimales:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   195
         TabIndex        =   55
         Top             =   5385
         Width           =   1485
      End
      Begin VB.Label lblNumDecResultGr 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DResultados:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   570
         TabIndex        =   54
         Top             =   5715
         Width           =   855
      End
      Begin VB.Label lblNumDecPorcenGr 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DPorcentajes:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   3690
         TabIndex        =   53
         Top             =   5715
         Width           =   900
      End
      Begin VB.Label lblNumDecPrecGr 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DPrecios:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   2115
         TabIndex        =   52
         Top             =   5715
         Width           =   630
      End
      Begin VB.Label lblNumDecCantGr 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DCantidades:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   5355
         TabIndex        =   51
         Top             =   5715
         Width           =   840
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000004&
         X1              =   0
         X2              =   7720
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000004&
         X1              =   0
         X2              =   7720
         Y1              =   6000
         Y2              =   6000
      End
      Begin VB.Label lblMostrarCamposGr 
         BackColor       =   &H00A56E3A&
         Caption         =   "DMostrar campos"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   1320
         Width           =   1635
      End
   End
   Begin VB.PictureBox picControlVistaProceso 
      BackColor       =   &H00A56E3A&
      Height          =   6555
      Left            =   3240
      ScaleHeight     =   6495
      ScaleWidth      =   7785
      TabIndex        =   14
      Top             =   400
      Visible         =   0   'False
      Width           =   7845
      Begin VB.CheckBox chkOcultarGrCerrados 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar grupos cerrados"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   75
         TabIndex        =   36
         Top             =   4740
         Width           =   4080
      End
      Begin VB.CheckBox chkExcluirGrCerradosResul 
         BackColor       =   &H00A56E3A&
         Caption         =   "Keine geschlossenen Gruppen in die Ergebnisse einschliessen"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   75
         TabIndex        =   35
         Top             =   4950
         Width           =   4110
      End
      Begin VB.CheckBox chkOcultarNoAdj 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar ofertas no adjudicables"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   4275
         TabIndex        =   34
         Top             =   4755
         Width           =   2865
      End
      Begin VB.CheckBox chkOcultarProvSinOfe 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar proveedores sin ofertas"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   4275
         TabIndex        =   33
         Top             =   4995
         Width           =   2895
      End
      Begin VB.TextBox txtDecResult 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3090
         TabIndex        =   32
         Top             =   5640
         Width           =   400
      End
      Begin VB.TextBox txtDecPorcen 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4755
         TabIndex        =   31
         Top             =   5640
         Width           =   400
      End
      Begin VB.CommandButton cmdGuardarVista 
         Caption         =   "D&Guardar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   270
         TabIndex        =   30
         Top             =   6120
         Width           =   1725
      End
      Begin VB.CommandButton cmdGuardarVistaNueva 
         Caption         =   "D&Guardar en  vista nueva"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2040
         TabIndex        =   29
         Top             =   6120
         Width           =   1725
      End
      Begin VB.CommandButton cmdEliminarVista 
         Caption         =   "Eliminar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3855
         TabIndex        =   28
         Top             =   6120
         Width           =   1725
      End
      Begin VB.CommandButton cmdRenombrarVista 
         Caption         =   "Renombrar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5670
         TabIndex        =   27
         Top             =   6120
         Width           =   1725
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOcultarCampos 
         Height          =   1875
         Left            =   120
         TabIndex        =   15
         Top             =   2460
         Width           =   7425
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   7
         stylesets.count =   3
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmConfiguracionVista.frx":0826
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmConfiguracionVista.frx":0B78
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Seleccionada"
         stylesets(2).ForeColor=   16777215
         stylesets(2).BackColor=   8388608
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmConfiguracionVista.frx":0B94
         DividerType     =   0
         BevelColorFrame =   -2147483646
         RowSelectionStyle=   1
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         PictureButton   =   "frmConfiguracionVista.frx":0BB0
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   185
         Columns.Count   =   7
         Columns(0).Width=   450
         Columns(0).Name =   "INC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadBackColor=   12615680
         Columns(1).Width=   5689
         Columns(1).Caption=   "Campo"
         Columns(1).Name =   "CAMPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadBackColor=   12615680
         Columns(1).HeadStyleSet=   "Normal"
         Columns(2).Width=   5689
         Columns(2).Caption=   "Pertenece"
         Columns(2).Name =   "Pertenece"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadBackColor=   12615680
         Columns(2).HeadStyleSet=   "Normal"
         Columns(3).Width=   820
         Columns(3).Caption=   "Detal."
         Columns(3).Name =   "POND"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   4
         Columns(3).ButtonsAlways=   -1  'True
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HeadForeColor=   16777215
         Columns(3).HeadBackColor=   12615680
         Columns(3).HeadStyleSet=   "Normal"
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "COD"
         Columns(4).Name =   "COD"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "AMBITO"
         Columns(5).Name =   "AMBITO"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "GRUPO"
         Columns(6).Name =   "GRUPO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   13097
         _ExtentY        =   3307
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "Command1"
         Height          =   195
         Left            =   7080
         TabIndex        =   42
         Top             =   6120
         Visible         =   0   'False
         Width           =   135
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaDefecto 
         Height          =   255
         Left            =   1920
         TabIndex        =   81
         Top             =   585
         Width           =   5535
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   3
         Columns(0).Width=   9763
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   9763
         _ExtentY        =   450
         _StockProps     =   93
         Text            =   "Vista 1"
         ForeColor       =   16777215
         BackColor       =   12615680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaActual 
         Height          =   255
         Left            =   1920
         TabIndex        =   82
         Top             =   240
         Width           =   5535
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   3
         Columns(0).Width=   9763
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   9763
         _ExtentY        =   450
         _StockProps     =   93
         Text            =   "Vista 1"
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgSobres 
         Height          =   540
         Left            =   105
         TabIndex        =   83
         Top             =   1425
         Width           =   7095
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         DividerType     =   0
         DividerStyle    =   0
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         ForeColorEven   =   -2147483630
         BackColorEven   =   16367808
         BackColorOdd    =   16367808
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   3
         Columns(0).Width=   450
         Columns(0).Caption=   "MOSTRAR"
         Columns(0).Name =   "MOSTRAR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   5794
         Columns(1).Caption=   "SOBRE"
         Columns(1).Name =   "SOBRE"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "ID"
         Columns(2).Name =   "ID"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   12515
         _ExtentY        =   952
         _StockProps     =   79
         BackColor       =   16367808
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblMostrarSobres 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DMostrar contenido sobres"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   105
         TabIndex        =   84
         Top             =   1200
         Width           =   3945
      End
      Begin VB.Line lineSobres 
         BorderColor     =   &H00FFFFFF&
         X1              =   30
         X2              =   7235
         Y1              =   2010
         Y2              =   2010
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7745
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label lblVista 
         BackColor       =   &H00A56E3A&
         Caption         =   "Vista inicial"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   40
         Top             =   285
         Width           =   3930
      End
      Begin VB.Label lblVistaDefecto 
         BackColor       =   &H00A56E3A&
         Caption         =   "DVista por defecto:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   41
         Top             =   630
         Width           =   1920
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7745
         Y1              =   4560
         Y2              =   4560
      End
      Begin VB.Line Line7 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7745
         Y1              =   5235
         Y2              =   5235
      End
      Begin VB.Label lblNumDec 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DN�mero decimales:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   75
         TabIndex        =   39
         Top             =   5355
         Width           =   2205
      End
      Begin VB.Label lblNumDecResult 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DResultados:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   2130
         TabIndex        =   38
         Top             =   5700
         Width           =   855
      End
      Begin VB.Label lblNumDecPorcen 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DPorcentaje:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   3795
         TabIndex        =   37
         Top             =   5700
         Width           =   825
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7745
         Y1              =   6000
         Y2              =   6000
      End
      Begin VB.Label lblMostrarCampos 
         BackColor       =   &H00A56E3A&
         Caption         =   "DMostrar campos"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   2190
         Width           =   1635
      End
      Begin VB.Shape Shape2 
         BorderColor     =   &H00FFFFFF&
         Height          =   6465
         Left            =   0
         Top             =   0
         Width           =   7740
      End
   End
   Begin VB.PictureBox picControlVistaAll 
      BackColor       =   &H00A56E3A&
      Height          =   6450
      Left            =   3195
      ScaleHeight     =   6390
      ScaleWidth      =   7785
      TabIndex        =   17
      Top             =   400
      Visible         =   0   'False
      Width           =   7845
      Begin VB.CheckBox chkOcultarItCerradosAll 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar items cerrados"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   75
         Top             =   4545
         Width           =   4035
      End
      Begin VB.CheckBox chkOcultarNoAdjAll 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar ofertas no adjudicables"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   4320
         TabIndex        =   74
         Top             =   4545
         Width           =   3405
      End
      Begin VB.CheckBox chkExcluirItCerradosResulAll 
         BackColor       =   &H00A56E3A&
         Caption         =   "DNo incluir items cerrados en los resultados"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   240
         TabIndex        =   73
         Top             =   4800
         Width           =   4095
      End
      Begin VB.CheckBox chkOcultarProvSinOfeAll 
         BackColor       =   &H00A56E3A&
         Caption         =   "DOcultar proveedores sin ofertas"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   4320
         TabIndex        =   72
         Top             =   4785
         Width           =   3360
      End
      Begin VB.TextBox txtDecPorcenAll 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5010
         TabIndex        =   71
         Top             =   5550
         Width           =   400
      End
      Begin VB.TextBox txtDecPrecAll 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3315
         TabIndex        =   70
         Top             =   5550
         Width           =   400
      End
      Begin VB.TextBox txtDecCantAll 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6675
         TabIndex        =   69
         Top             =   5550
         Width           =   400
      End
      Begin VB.TextBox txtDecResultAll 
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1650
         TabIndex        =   68
         Top             =   5550
         Width           =   400
      End
      Begin VB.CommandButton cmdGuardarVistaAll 
         Caption         =   "D&Guardar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   315
         TabIndex        =   67
         Top             =   5985
         Width           =   1725
      End
      Begin VB.CommandButton cmdGuardarVistaNuevaAll 
         Caption         =   "D&Guardar en  vista nueva"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2115
         TabIndex        =   66
         Top             =   5985
         Width           =   1725
      End
      Begin VB.CommandButton cmdEliminarVistaAll 
         Caption         =   "Eliminar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3915
         TabIndex        =   65
         Top             =   5985
         Width           =   1725
      End
      Begin VB.CommandButton cmdRenombrarVistaAll 
         Caption         =   "Renombrar vista"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   5715
         TabIndex        =   64
         Top             =   5985
         Width           =   1725
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgOcultarCamposAll 
         Height          =   2700
         Left            =   120
         TabIndex        =   18
         Top             =   1425
         Width           =   7425
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   5
         stylesets.count =   3
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16367808
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmConfiguracionVista.frx":0F02
         stylesets(0).AlignmentPicture=   0
         stylesets(1).Name=   "Normal"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmConfiguracionVista.frx":1254
         stylesets(1).AlignmentText=   0
         stylesets(2).Name=   "Seleccionada"
         stylesets(2).ForeColor=   16777215
         stylesets(2).BackColor=   8388608
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmConfiguracionVista.frx":1270
         DividerType     =   0
         BevelColorFrame =   -2147483646
         RowSelectionStyle=   1
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         PictureButton   =   "frmConfiguracionVista.frx":128C
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorEven   =   15777984
         BackColorOdd    =   15777984
         RowHeight       =   370
         ExtraHeight     =   185
         Columns.Count   =   5
         Columns(0).Width=   450
         Columns(0).Name =   "INC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(0).HasHeadBackColor=   -1  'True
         Columns(0).HeadBackColor=   12615680
         Columns(1).Width=   5741
         Columns(1).Caption=   "Campo"
         Columns(1).Name =   "CAMPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasHeadForeColor=   -1  'True
         Columns(1).HasHeadBackColor=   -1  'True
         Columns(1).HeadForeColor=   16777215
         Columns(1).HeadBackColor=   12615680
         Columns(1).HeadStyleSet=   "Normal"
         Columns(2).Width=   5741
         Columns(2).Caption=   "Pertenece"
         Columns(2).Name =   "Pertenece"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasHeadForeColor=   -1  'True
         Columns(2).HasHeadBackColor=   -1  'True
         Columns(2).HeadForeColor=   16777215
         Columns(2).HeadBackColor=   12615680
         Columns(2).HeadStyleSet=   "Normal"
         Columns(3).Width=   714
         Columns(3).Caption=   "Detal."
         Columns(3).Name =   "POND"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   4
         Columns(3).ButtonsAlways=   -1  'True
         Columns(3).HasHeadForeColor=   -1  'True
         Columns(3).HasHeadBackColor=   -1  'True
         Columns(3).HeadForeColor=   16777215
         Columns(3).HeadBackColor=   12615680
         Columns(3).HeadStyleSet=   "Normal"
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "COD"
         Columns(4).Name =   "COD"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   13097
         _ExtentY        =   4762
         _StockProps     =   79
         ForeColor       =   0
         BackColor       =   16367808
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaDefectoAll 
         Height          =   255
         Left            =   2430
         TabIndex        =   60
         Top             =   465
         Width           =   4590
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   3
         Columns(0).Width=   8096
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8096
         _ExtentY        =   450
         _StockProps     =   93
         ForeColor       =   16777215
         BackColor       =   12615680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVistaActAll 
         Height          =   255
         Left            =   2400
         TabIndex        =   61
         Top             =   120
         Width           =   4590
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         BevelColorFace  =   13160660
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         Columns.Count   =   3
         Columns(0).Width=   8096
         Columns(0).Caption=   "VISTA"
         Columns(0).Name =   "VISTA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   8096
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Line Line8 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7740
         Y1              =   5160
         Y2              =   5160
      End
      Begin VB.Line Line11All 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7745
         Y1              =   4320
         Y2              =   4320
      End
      Begin VB.Label lblNumDecAll 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DN�mero de decimales:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   120
         TabIndex        =   80
         Top             =   5265
         Width           =   1485
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7740
         Y1              =   5865
         Y2              =   5865
      End
      Begin VB.Label lblNumDecResultAll 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DResultados:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   690
         TabIndex        =   79
         Top             =   5595
         Width           =   855
      End
      Begin VB.Label lblNumDecPrecAll 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DPrecios:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   2355
         TabIndex        =   78
         Top             =   5595
         Width           =   630
      End
      Begin VB.Label lblNumDecPorcenAll 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DPorcentajes:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   4050
         TabIndex        =   77
         Top             =   5595
         Width           =   900
      End
      Begin VB.Label lblNumDecCantAll 
         AutoSize        =   -1  'True
         BackColor       =   &H00A56E3A&
         Caption         =   "DCantidades:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   165
         Left            =   5715
         TabIndex        =   76
         Top             =   5595
         Width           =   840
      End
      Begin VB.Label lblVistaDefectoAll 
         BackColor       =   &H00A56E3A&
         Caption         =   "DVista por defecto:"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   435
         TabIndex        =   63
         Top             =   510
         Width           =   1875
      End
      Begin VB.Line Line12All 
         BorderColor     =   &H00FFFFFF&
         X1              =   0
         X2              =   7745
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Label lblPlantillaAll 
         BackColor       =   &H00A56E3A&
         Caption         =   "DPlantilla"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   435
         TabIndex        =   62
         Top             =   165
         Width           =   3795
      End
      Begin VB.Label lblMostrarCamposAll 
         BackColor       =   &H00A56E3A&
         Caption         =   "DMostrar campos"
         BeginProperty Font 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   1080
         Width           =   1635
      End
      Begin VB.Shape Shape8All 
         BorderColor     =   &H00FFFFFF&
         Height          =   6345
         Left            =   0
         Top             =   0
         Width           =   7740
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotalesProve 
      Height          =   1000
      Left            =   120
      TabIndex        =   8
      Top             =   4130
      Width           =   11000
      ScrollBars      =   1
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Row.Count       =   1
      stylesets.count =   7
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":15DE
      stylesets(1).Name=   "Adjudicado"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":15FA
      stylesets(2).Name=   "Proveedor"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":1616
      stylesets(2).AlignmentText=   0
      stylesets(2).AlignmentPicture=   1
      stylesets(3).Name=   "Red"
      stylesets(3).BackColor=   4744445
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":1632
      stylesets(3).AlignmentText=   1
      stylesets(4).Name=   "Importe"
      stylesets(4).ForeColor=   0
      stylesets(4).BackColor=   16701135
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmConfiguracionVista.frx":164E
      stylesets(4).AlignmentText=   1
      stylesets(5).Name=   "Green"
      stylesets(5).BackColor=   10409634
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmConfiguracionVista.frx":166A
      stylesets(5).AlignmentText=   1
      stylesets(6).Name=   "GrayHead"
      stylesets(6).ForeColor=   0
      stylesets(6).BackColor=   -2147483633
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmConfiguracionVista.frx":1686
      UseGroups       =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Proveedor"
      StyleSet        =   "Proveedor"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      SplitterVisible =   -1  'True
      Groups(0).Width =   4763
      Groups(0).Columns(0).Width=   4763
      Groups(0).Columns(0).Name=   "Vacia"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      _ExtentX        =   19403
      _ExtentY        =   1764
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgtotales2 
      Height          =   1000
      Left            =   120
      TabIndex        =   7
      Top             =   4130
      Visible         =   0   'False
      Width           =   11000
      ScrollBars      =   1
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Row.Count       =   1
      stylesets.count =   7
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":16A2
      stylesets(1).Name=   "Adjudicado"
      stylesets(1).BackColor=   10079487
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":16BE
      stylesets(2).Name=   "Proveedor"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":16DA
      stylesets(2).AlignmentText=   0
      stylesets(2).AlignmentPicture=   1
      stylesets(3).Name=   "Red"
      stylesets(3).BackColor=   4744445
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":16F6
      stylesets(3).AlignmentText=   1
      stylesets(4).Name=   "Importe"
      stylesets(4).ForeColor=   0
      stylesets(4).BackColor=   16701135
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmConfiguracionVista.frx":1712
      stylesets(4).AlignmentText=   1
      stylesets(5).Name=   "Green"
      stylesets(5).BackColor=   10409634
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmConfiguracionVista.frx":172E
      stylesets(5).AlignmentText=   1
      stylesets(6).Name=   "GrayHead"
      stylesets(6).ForeColor=   0
      stylesets(6).BackColor=   -2147483633
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmConfiguracionVista.frx":174A
      UseGroups       =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Proveedor"
      StyleSet        =   "Proveedor"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      SplitterVisible =   -1  'True
      Groups(0).Width =   5133
      Groups(0).Columns(0).Width=   5133
      Groups(0).Columns(0).Name=   "Vacia"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      _ExtentX        =   19403
      _ExtentY        =   1764
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotalesAll 
      Height          =   1000
      Left            =   120
      TabIndex        =   3
      Top             =   4130
      Visible         =   0   'False
      Width           =   11000
      ScrollBars      =   1
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeadLines  =   2
      Col.Count       =   2
      stylesets.count =   4
      stylesets(0).Name=   "Gray"
      stylesets(0).BackColor=   12632256
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":1766
      stylesets(1).Name=   "Proveedor"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":1782
      stylesets(1).AlignmentText=   1
      stylesets(2).Name=   "Red"
      stylesets(2).BackColor=   8421631
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":179E
      stylesets(3).Name=   "Green"
      stylesets(3).BackColor=   10409634
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":17BA
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Gray"
      StyleSet        =   "Gray"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3651
      Columns(0).Caption=   "Total por proveedor"
      Columns(0).Name =   "TOT"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   -2147483633
      Columns(0).HeadStyleSet=   "Proveedor"
      Columns(1).Width=   1958
      Columns(1).Caption=   "Objetivo total"
      Columns(1).Name =   "OBJTOT"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "Standard"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   -2147483633
      Columns(1).HeadStyleSet=   "Proveedor"
      _ExtentX        =   19403
      _ExtentY        =   1764
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picConfigVistas 
      BackColor       =   &H00A56E3A&
      Height          =   420
      Left            =   10200
      ScaleHeight     =   360
      ScaleWidth      =   930
      TabIndex        =   11
      Top             =   0
      Width           =   990
      Begin VB.CommandButton cmdVista 
         Height          =   285
         Left            =   120
         Picture         =   "frmConfiguracionVista.frx":17D6
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Dise�o de vistas"
         Top             =   40
         Width           =   315
      End
      Begin VB.CommandButton cmdInvertir 
         Height          =   285
         Left            =   480
         Picture         =   "frmConfiguracionVista.frx":1B18
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Invertir grid"
         Top             =   40
         Width           =   315
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgProce 
      Height          =   1050
      Left            =   120
      TabIndex        =   10
      Top             =   855
      Visible         =   0   'False
      Width           =   10995
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      stylesets.count =   4
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":1E8A
      stylesets(1).Name=   "ProveedorConAdjuntos"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":1EA6
      stylesets(1).AlignmentText=   2
      stylesets(1).AlignmentPicture=   0
      stylesets(2).Name=   "Proveedor"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":1EC2
      stylesets(2).AlignmentPicture=   1
      stylesets(3).Name=   "GrayHead"
      stylesets(3).ForeColor=   0
      stylesets(3).BackColor=   -2147483633
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":1EDE
      stylesets(3).AlignmentPicture=   1
      UseGroups       =   -1  'True
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      SelectByCell    =   -1  'True
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      CaptionAlignment=   0
      Groups(0).Width =   4763
      Groups(0).Caption=   "Proceso"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadBackColor=   9876917
      Groups(0).Columns(0).Width=   4763
      Groups(0).Columns(0).Name=   "Nombre"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).ButtonsAlways=   -1  'True
      Groups(0).Columns(0).HasHeadForeColor=   -1  'True
      Groups(0).Columns(0).HasHeadBackColor=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).HeadForeColor=   16777215
      Groups(0).Columns(0).HeadBackColor=   9876917
      Groups(0).Columns(0).BackColor=   9876917
      _ExtentX        =   19394
      _ExtentY        =   1852
      _StockProps     =   79
      Caption         =   "caption"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgGruposProve 
      Height          =   3285
      Left            =   120
      TabIndex        =   9
      Top             =   840
      Width           =   10995
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   3
      stylesets.count =   10
      stylesets(0).Name=   "Yellow"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   13170165
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":1EFA
      stylesets(0).AlignmentText=   1
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":1F16
      stylesets(2).Name=   "ProveedorConAdjuntos"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":1F32
      stylesets(2).AlignmentText=   2
      stylesets(2).AlignmentPicture=   0
      stylesets(3).Name=   "Adjudicado"
      stylesets(3).BackColor=   10079487
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":1F4E
      stylesets(4).Name=   "Proveedor"
      stylesets(4).BackColor=   -2147483633
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmConfiguracionVista.frx":1F6A
      stylesets(4).AlignmentText=   2
      stylesets(4).AlignmentPicture=   1
      stylesets(5).Name=   "Red"
      stylesets(5).BackColor=   4744445
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmConfiguracionVista.frx":1F86
      stylesets(5).AlignmentText=   1
      stylesets(6).Name=   "Importe"
      stylesets(6).ForeColor=   0
      stylesets(6).BackColor=   16701135
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmConfiguracionVista.frx":1FA2
      stylesets(6).AlignmentText=   1
      stylesets(7).Name=   "Green"
      stylesets(7).BackColor=   10409634
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmConfiguracionVista.frx":1FBE
      stylesets(7).AlignmentText=   1
      stylesets(8).Name=   "ItemCerrado"
      stylesets(8).ForeColor=   16777215
      stylesets(8).BackColor=   12632256
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmConfiguracionVista.frx":1FDA
      stylesets(9).Name=   "GrayHead"
      stylesets(9).ForeColor=   0
      stylesets(9).BackColor=   -2147483633
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmConfiguracionVista.frx":1FF6
      stylesets(9).AlignmentText=   1
      UseGroups       =   -1  'True
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   132
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Groups(0).Width =   4763
      Groups(0).Caption=   "Grupos"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadBackColor=   9876917
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   1667
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "COD"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).HasHeadForeColor=   -1  'True
      Groups(0).Columns(0).HasHeadBackColor=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).HeadBackColor=   9876917
      Groups(0).Columns(0).BackColor=   9876917
      Groups(0).Columns(0).HeadStyleSet=   "Normal"
      Groups(0).Columns(1).Width=   3096
      Groups(0).Columns(1).Caption=   "Nombre"
      Groups(0).Columns(1).Name=   "NOMBRE"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).ButtonsAlways=   -1  'True
      Groups(0).Columns(1).HasHeadForeColor=   -1  'True
      Groups(0).Columns(1).HasHeadBackColor=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).HeadBackColor=   9876917
      Groups(0).Columns(1).BackColor=   9876917
      Groups(0).Columns(1).HeadStyleSet=   "Normal"
      Groups(0).Columns(2).Width=   2328
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "Cerrado"
      Groups(0).Columns(2).Name=   "CERRADO"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   19403
      _ExtentY        =   5786
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgProveGrupos 
      Height          =   3280
      Left            =   120
      TabIndex        =   6
      Top             =   855
      Visible         =   0   'False
      Width           =   11000
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Row.Count       =   3
      Col.Count       =   3
      Row(0).Col(0)   =   "PROVE1"
      Row(0).Col(1)   =   "Proveedor 1"
      Row(1).Col(0)   =   "PROVE2"
      Row(1).Col(1)   =   "Proveedor 2"
      Row(2).Col(0)   =   "PROVE3"
      Row(2).Col(1)   =   "Proveedor 3"
      stylesets.count =   9
      stylesets(0).Name=   "Yellow"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   13170165
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":2012
      stylesets(0).AlignmentText=   1
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":202E
      stylesets(2).Name=   "Adjudicado"
      stylesets(2).BackColor=   10079487
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":204A
      stylesets(3).Name=   "Proveedor"
      stylesets(3).BackColor=   -2147483633
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":2066
      stylesets(3).AlignmentText=   0
      stylesets(3).AlignmentPicture=   1
      stylesets(4).Name=   "Red"
      stylesets(4).BackColor=   4744445
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmConfiguracionVista.frx":2082
      stylesets(4).AlignmentText=   1
      stylesets(5).Name=   "Importe"
      stylesets(5).ForeColor=   0
      stylesets(5).BackColor=   16701135
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmConfiguracionVista.frx":209E
      stylesets(5).AlignmentText=   1
      stylesets(6).Name=   "Green"
      stylesets(6).BackColor=   10409634
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmConfiguracionVista.frx":20BA
      stylesets(6).AlignmentText=   1
      stylesets(7).Name=   "ItemCerrado"
      stylesets(7).ForeColor=   16777215
      stylesets(7).BackColor=   12632256
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmConfiguracionVista.frx":20D6
      stylesets(8).Name=   "GrayHead"
      stylesets(8).ForeColor=   0
      stylesets(8).BackColor=   -2147483633
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmConfiguracionVista.frx":20F2
      UseGroups       =   -1  'True
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   132
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Groups(0).Width =   4551
      Groups(0).Caption=   "Proveedores"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadBackColor=   9876917
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   2249
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "COD"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).HasHeadForeColor=   -1  'True
      Groups(0).Columns(0).HasHeadBackColor=   -1  'True
      Groups(0).Columns(0).HasForeColor=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).HeadBackColor=   9876917
      Groups(0).Columns(0).BackColor=   9876917
      Groups(0).Columns(0).HeadStyleSet=   "Normal"
      Groups(0).Columns(1).Width=   2302
      Groups(0).Columns(1).Caption=   "Denominaci�n"
      Groups(0).Columns(1).Name=   "DEN"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HasHeadForeColor=   -1  'True
      Groups(0).Columns(1).HasHeadBackColor=   -1  'True
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).HeadBackColor=   9876917
      Groups(0).Columns(1).BackColor=   9876917
      Groups(0).Columns(1).HeadStyleSet=   "Normal"
      Groups(0).Columns(2).Width=   2249
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "VACIO"
      Groups(0).Columns(2).Name=   "VACIO"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      UseDefaults     =   0   'False
      _ExtentX        =   19403
      _ExtentY        =   5786
      _StockProps     =   79
      ForeColor       =   0
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgTotales 
      Height          =   1000
      Left            =   120
      TabIndex        =   5
      Top             =   4130
      Visible         =   0   'False
      Width           =   11000
      ScrollBars      =   1
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeadLines  =   2
      Col.Count       =   2
      stylesets.count =   4
      stylesets(0).Name=   "Gray"
      stylesets(0).BackColor=   12632256
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":210E
      stylesets(1).Name=   "Red"
      stylesets(1).BackColor=   8421631
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":212A
      stylesets(2).Name=   "Proveedor"
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":2146
      stylesets(2).AlignmentText=   1
      stylesets(3).Name=   "Green"
      stylesets(3).BackColor=   10409634
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":2162
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      HeadStyleSet    =   "Gray"
      StyleSet        =   "Gray"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3651
      Columns(0).Caption=   "Total por proveedor"
      Columns(0).Name =   "TOT"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   -2147483633
      Columns(0).HeadStyleSet=   "Proveedor"
      Columns(1).Width=   1958
      Columns(1).Caption=   "Objetivo total"
      Columns(1).Name =   "OBJTOT"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).NumberFormat=   "Standard"
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   -2147483633
      Columns(1).HeadStyleSet=   "Proveedor"
      _ExtentX        =   19403
      _ExtentY        =   1764
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAdj 
      Height          =   3280
      Left            =   120
      TabIndex        =   4
      Top             =   855
      Visible         =   0   'False
      Width           =   11000
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      GroupHeadLines  =   2
      Col.Count       =   22
      stylesets.count =   15
      stylesets(0).Name=   "NoHomologado"
      stylesets(0).ForeColor=   192
      stylesets(0).BackColor=   13170165
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":217E
      stylesets(0).AlignmentText=   1
      stylesets(1).Name=   "ProvActual"
      stylesets(1).ForeColor=   0
      stylesets(1).BackColor=   9693431
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":219A
      stylesets(2).Name=   "Yellow"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   13170165
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":21B6
      stylesets(2).AlignmentText=   1
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":21D2
      stylesets(4).Name=   "YellowHead"
      stylesets(4).ForeColor=   0
      stylesets(4).BackColor=   13170165
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmConfiguracionVista.frx":21EE
      stylesets(5).Name=   "BlueHead"
      stylesets(5).ForeColor=   12582912
      stylesets(5).BackColor=   16777215
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmConfiguracionVista.frx":220A
      stylesets(6).Name=   "Adjudicado"
      stylesets(6).BackColor=   10079487
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmConfiguracionVista.frx":2226
      stylesets(7).Name=   "Blue"
      stylesets(7).ForeColor=   12582912
      stylesets(7).BackColor=   16777215
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmConfiguracionVista.frx":2297
      stylesets(7).AlignmentText=   1
      stylesets(8).Name=   "Proveedor"
      stylesets(8).BackColor=   -2147483633
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmConfiguracionVista.frx":22B3
      stylesets(8).AlignmentText=   0
      stylesets(8).AlignmentPicture=   1
      stylesets(9).Name=   "Red"
      stylesets(9).BackColor=   4744445
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmConfiguracionVista.frx":22CF
      stylesets(9).AlignmentText=   1
      stylesets(10).Name=   "ProveedorConAdjuntos"
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmConfiguracionVista.frx":22EB
      stylesets(10).AlignmentText=   2
      stylesets(10).AlignmentPicture=   0
      stylesets(11).Name=   "Green"
      stylesets(11).BackColor=   10409634
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmConfiguracionVista.frx":2307
      stylesets(11).AlignmentText=   1
      stylesets(12).Name=   "YellowLeft"
      stylesets(12).ForeColor=   0
      stylesets(12).BackColor=   13170165
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmConfiguracionVista.frx":2323
      stylesets(12).AlignmentText=   0
      stylesets(13).Name=   "ItemCerrado"
      stylesets(13).ForeColor=   16777215
      stylesets(13).BackColor=   12632256
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmConfiguracionVista.frx":233F
      stylesets(14).Name=   "GrayHead"
      stylesets(14).ForeColor=   0
      stylesets(14).BackColor=   -2147483633
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmConfiguracionVista.frx":235B
      UseGroups       =   -1  'True
      DividerStyle    =   0
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      Levels          =   2
      RowHeight       =   741
      ExtraHeight     =   265
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Groups.Count    =   2
      Groups(0).Width =   10319
      Groups(0).Caption=   "(Pulse aqu� para ocultar o visualizar la segunda linea)"
      Groups(0).HeadStyleSet=   "GrayHead"
      Groups(0).Columns.Count=   17
      Groups(0).Columns(0).Width=   3096
      Groups(0).Columns(0).Caption=   "Descripci�n"
      Groups(0).Columns(0).Name=   "DESCR"
      Groups(0).Columns(0).CaptionAlignment=   0
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).ButtonsAlways=   -1  'True
      Groups(0).Columns(0).HasHeadBackColor=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).HeadBackColor=   -2147483633
      Groups(0).Columns(0).BackColor=   16776960
      Groups(0).Columns(0).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(0).StyleSet=   "YellowLeft"
      Groups(0).Columns(1).Width=   1376
      Groups(0).Columns(1).Caption=   "A�o/Dest"
      Groups(0).Columns(1).Name=   "ANYO"
      Groups(0).Columns(1).Alignment=   1
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(1).StyleSet=   "Yellow"
      Groups(0).Columns(2).Width=   1799
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "ID"
      Groups(0).Columns(2).Name=   "ID"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(3).Width=   1720
      Groups(0).Columns(3).Caption=   "Proveedor"
      Groups(0).Columns(3).Name=   "PROV"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   8
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(3).Locked=   -1  'True
      Groups(0).Columns(3).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(3).StyleSet=   "YellowLeft"
      Groups(0).Columns(4).Width=   1905
      Groups(0).Columns(4).Caption=   "Pres.unitario"
      Groups(0).Columns(4).Name=   "PRECAPE"
      Groups(0).Columns(4).Alignment=   1
      Groups(0).Columns(4).CaptionAlignment=   2
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   8
      Groups(0).Columns(4).NumberFormat=   "Standard"
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(4).Locked=   -1  'True
      Groups(0).Columns(4).HasHeadForeColor=   -1  'True
      Groups(0).Columns(4).HasHeadBackColor=   -1  'True
      Groups(0).Columns(4).HasForeColor=   -1  'True
      Groups(0).Columns(4).HeadBackColor=   13170165
      Groups(0).Columns(4).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(4).StyleSet=   "Yellow"
      Groups(0).Columns(5).Width=   1032
      Groups(0).Columns(5).Visible=   0   'False
      Groups(0).Columns(5).Caption=   "Adj"
      Groups(0).Columns(5).Name=   "ADJ"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).DataType=   8
      Groups(0).Columns(5).FieldLen=   256
      Groups(0).Columns(5).Locked=   -1  'True
      Groups(0).Columns(5).Style=   2
      Groups(0).Columns(6).Width=   1958
      Groups(0).Columns(6).Visible=   0   'False
      Groups(0).Columns(6).Caption=   "Art�culo"
      Groups(0).Columns(6).Name=   "ART"
      Groups(0).Columns(6).Alignment=   1
      Groups(0).Columns(6).CaptionAlignment=   0
      Groups(0).Columns(6).DataField=   "Column 6"
      Groups(0).Columns(6).DataType=   8
      Groups(0).Columns(6).FieldLen=   256
      Groups(0).Columns(6).Locked=   -1  'True
      Groups(0).Columns(6).HasHeadForeColor=   -1  'True
      Groups(0).Columns(6).HasHeadBackColor=   -1  'True
      Groups(0).Columns(6).HasBackColor=   -1  'True
      Groups(0).Columns(6).HeadBackColor=   16776960
      Groups(0).Columns(6).BackColor=   16776960
      Groups(0).Columns(6).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(6).StyleSet=   "Yellow"
      Groups(0).Columns(7).Width=   2223
      Groups(0).Columns(7).Caption=   "Objetivo"
      Groups(0).Columns(7).Name=   "OBJ"
      Groups(0).Columns(7).Alignment=   1
      Groups(0).Columns(7).CaptionAlignment=   2
      Groups(0).Columns(7).DataField=   "Column 7"
      Groups(0).Columns(7).DataType=   8
      Groups(0).Columns(7).FieldLen=   256
      Groups(0).Columns(7).Locked=   -1  'True
      Groups(0).Columns(7).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(7).StyleSet=   "Yellow"
      Groups(0).Columns(8).Width=   291
      Groups(0).Columns(8).Caption=   "Ahorro imp."
      Groups(0).Columns(8).Name=   "AHORROIMP"
      Groups(0).Columns(8).Alignment=   1
      Groups(0).Columns(8).CaptionAlignment=   0
      Groups(0).Columns(8).DataField=   "Column 8"
      Groups(0).Columns(8).DataType=   8
      Groups(0).Columns(8).Level=   1
      Groups(0).Columns(8).NumberFormat=   "Standard"
      Groups(0).Columns(8).FieldLen=   256
      Groups(0).Columns(8).Locked=   -1  'True
      Groups(0).Columns(8).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(9).Width=   238
      Groups(0).Columns(9).Caption=   "Ahorro%"
      Groups(0).Columns(9).Name=   "AHORROPORCEN"
      Groups(0).Columns(9).Alignment=   2
      Groups(0).Columns(9).CaptionAlignment=   2
      Groups(0).Columns(9).DataField=   "Column 9"
      Groups(0).Columns(9).DataType=   8
      Groups(0).Columns(9).Level=   1
      Groups(0).Columns(9).NumberFormat=   "0.0#\%"
      Groups(0).Columns(9).FieldLen=   256
      Groups(0).Columns(9).Locked=   -1  'True
      Groups(0).Columns(9).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(10).Width=   344
      Groups(0).Columns(10).Caption=   "IMP"
      Groups(0).Columns(10).Name=   "IMP"
      Groups(0).Columns(10).Alignment=   1
      Groups(0).Columns(10).CaptionAlignment=   2
      Groups(0).Columns(10).DataField=   "Column 10"
      Groups(0).Columns(10).DataType=   8
      Groups(0).Columns(10).Level=   1
      Groups(0).Columns(10).NumberFormat=   "Standard"
      Groups(0).Columns(10).FieldLen=   256
      Groups(0).Columns(10).Locked=   -1  'True
      Groups(0).Columns(10).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(10).StyleSet=   "Yellow"
      Groups(0).Columns(11).Width=   397
      Groups(0).Columns(11).Caption=   "Cantidad"
      Groups(0).Columns(11).Name=   "CANT"
      Groups(0).Columns(11).Alignment=   1
      Groups(0).Columns(11).CaptionAlignment=   2
      Groups(0).Columns(11).DataField=   "Column 11"
      Groups(0).Columns(11).DataType=   8
      Groups(0).Columns(11).Level=   1
      Groups(0).Columns(11).NumberFormat=   "Standard"
      Groups(0).Columns(11).FieldLen=   256
      Groups(0).Columns(11).Locked=   -1  'True
      Groups(0).Columns(11).HasHeadForeColor=   -1  'True
      Groups(0).Columns(11).HasHeadBackColor=   -1  'True
      Groups(0).Columns(11).HasForeColor=   -1  'True
      Groups(0).Columns(11).HeadForeColor=   12582912
      Groups(0).Columns(11).HeadBackColor=   16777215
      Groups(0).Columns(11).ForeColor=   12582912
      Groups(0).Columns(11).HeadStyleSet=   "BlueHead"
      Groups(0).Columns(11).StyleSet=   "Blue"
      Groups(0).Columns(12).Width=   370
      Groups(0).Columns(12).Caption=   "Adj. %"
      Groups(0).Columns(12).Name=   "PORCENADJ"
      Groups(0).Columns(12).Alignment=   1
      Groups(0).Columns(12).CaptionAlignment=   2
      Groups(0).Columns(12).DataField=   "Column 12"
      Groups(0).Columns(12).DataType=   8
      Groups(0).Columns(12).Level=   1
      Groups(0).Columns(12).NumberFormat=   "0.0#\%"
      Groups(0).Columns(12).FieldLen=   256
      Groups(0).Columns(12).Locked=   -1  'True
      Groups(0).Columns(12).HeadStyleSet=   "BlueHead"
      Groups(0).Columns(12).StyleSet=   "Blue"
      Groups(0).Columns(13).Width=   291
      Groups(0).Columns(13).Caption=   "Solicitud"
      Groups(0).Columns(13).Name=   "VINCULADO_ITEM"
      Groups(0).Columns(13).Alignment=   1
      Groups(0).Columns(13).DataField=   "Column 13"
      Groups(0).Columns(13).DataType=   8
      Groups(0).Columns(13).Level=   1
      Groups(0).Columns(13).FieldLen=   256
      Groups(0).Columns(13).Locked=   -1  'True
      Groups(0).Columns(13).Style=   1
      Groups(0).Columns(13).ButtonsAlways=   -1  'True
      Groups(0).Columns(13).HasHeadBackColor=   -1  'True
      Groups(0).Columns(13).HasBackColor=   -1  'True
      Groups(0).Columns(13).HeadBackColor=   -2147483633
      Groups(0).Columns(13).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(13).StyleSet=   "Yellow"
      Groups(0).Columns(14).Width=   1164
      Groups(0).Columns(14).Caption=   "DFec. ini. suministro"
      Groups(0).Columns(14).Name=   "INI"
      Groups(0).Columns(14).CaptionAlignment=   2
      Groups(0).Columns(14).DataField=   "Column 14"
      Groups(0).Columns(14).DataType=   8
      Groups(0).Columns(14).Level=   1
      Groups(0).Columns(14).FieldLen=   256
      Groups(0).Columns(14).Locked=   -1  'True
      Groups(0).Columns(14).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(14).StyleSet=   "YellowLeft"
      Groups(0).Columns(15).Width=   2302
      Groups(0).Columns(15).Caption=   "DFec. fin suministro"
      Groups(0).Columns(15).Name=   "FIN"
      Groups(0).Columns(15).CaptionAlignment=   2
      Groups(0).Columns(15).DataField=   "Column 15"
      Groups(0).Columns(15).DataType=   8
      Groups(0).Columns(15).Level=   1
      Groups(0).Columns(15).FieldLen=   256
      Groups(0).Columns(15).Locked=   -1  'True
      Groups(0).Columns(15).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(15).StyleSet=   "YellowLeft"
      Groups(0).Columns(16).Width=   4921
      Groups(0).Columns(16).Caption=   "DEstr. material"
      Groups(0).Columns(16).Name=   "GMN"
      Groups(0).Columns(16).CaptionAlignment=   2
      Groups(0).Columns(16).DataField=   "Column 16"
      Groups(0).Columns(16).DataType=   8
      Groups(0).Columns(16).Level=   1
      Groups(0).Columns(16).FieldLen=   256
      Groups(0).Columns(16).Locked=   -1  'True
      Groups(0).Columns(16).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(16).StyleSet=   "YellowLeft"
      Groups(1).Width =   5292
      Groups(1).Visible=   0   'False
      Groups(1).Caption=   "(Pulse sobre la cabecera para ocultar las cantidades)"
      Groups(1).CaptionAlignment=   0
      Groups(1).AllowSizing=   0   'False
      Groups(1).Columns.Count=   5
      Groups(1).Columns(0).Width=   900
      Groups(1).Columns(0).Caption=   "Pago"
      Groups(1).Columns(0).Name=   "PAG"
      Groups(1).Columns(0).DataField=   "Column 17"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).Locked=   -1  'True
      Groups(1).Columns(1).Width=   953
      Groups(1).Columns(1).Caption=   "Uni"
      Groups(1).Columns(1).Name=   "UNI"
      Groups(1).Columns(1).DataField=   "Column 18"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Locked=   -1  'True
      Groups(1).Columns(2).Width=   1164
      Groups(1).Columns(2).Caption=   "CERRADO"
      Groups(1).Columns(2).Name=   "CERRADO"
      Groups(1).Columns(2).DataField=   "Column 19"
      Groups(1).Columns(2).DataType=   11
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(3).Width=   2275
      Groups(1).Columns(3).Caption=   "CODPROVEACTUAL"
      Groups(1).Columns(3).Name=   "CODPROVEACTUAL"
      Groups(1).Columns(3).DataField=   "Column 20"
      Groups(1).Columns(3).DataType=   8
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(4).Width=   26
      Groups(1).Columns(4).Visible=   0   'False
      Groups(1).Columns(4).Caption=   "Importe"
      Groups(1).Columns(4).Name=   "DEST"
      Groups(1).Columns(4).Alignment=   1
      Groups(1).Columns(4).CaptionAlignment=   2
      Groups(1).Columns(4).DataField=   "Column 21"
      Groups(1).Columns(4).DataType=   8
      Groups(1).Columns(4).NumberFormat=   "Standard"
      Groups(1).Columns(4).FieldLen=   256
      Groups(1).Columns(4).Locked=   -1  'True
      Groups(1).Columns(4).HasHeadBackColor=   -1  'True
      Groups(1).Columns(4).HasForeColor=   -1  'True
      Groups(1).Columns(4).HeadStyleSet=   "YellowHead"
      UseDefaults     =   0   'False
      _ExtentX        =   19403
      _ExtentY        =   5786
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAll 
      Height          =   3280
      Left            =   120
      TabIndex        =   2
      Top             =   855
      Visible         =   0   'False
      Width           =   10995
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      GroupHeadLines  =   2
      Col.Count       =   23
      stylesets.count =   15
      stylesets(0).Name=   "ProvActual"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   9693431
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmConfiguracionVista.frx":2377
      stylesets(1).Name=   "NoHomologado"
      stylesets(1).ForeColor=   192
      stylesets(1).BackColor=   13170165
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmConfiguracionVista.frx":2393
      stylesets(1).AlignmentText=   1
      stylesets(2).Name=   "Yellow"
      stylesets(2).ForeColor=   0
      stylesets(2).BackColor=   13170165
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmConfiguracionVista.frx":23AF
      stylesets(2).AlignmentText=   1
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmConfiguracionVista.frx":23CB
      stylesets(4).Name=   "YellowHead"
      stylesets(4).ForeColor=   0
      stylesets(4).BackColor=   13170165
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmConfiguracionVista.frx":23E7
      stylesets(5).Name=   "BlueHead"
      stylesets(5).ForeColor=   12582912
      stylesets(5).BackColor=   16777215
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmConfiguracionVista.frx":2403
      stylesets(6).Name=   "Red"
      stylesets(6).BackColor=   4744445
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmConfiguracionVista.frx":241F
      stylesets(6).AlignmentText=   1
      stylesets(7).Name=   "Proveedor"
      stylesets(7).BackColor=   -2147483633
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmConfiguracionVista.frx":2490
      stylesets(7).AlignmentText=   0
      stylesets(7).AlignmentPicture=   1
      stylesets(8).Name=   "Blue"
      stylesets(8).ForeColor=   12582912
      stylesets(8).BackColor=   16777215
      stylesets(8).HasFont=   -1  'True
      BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(8).Picture=   "frmConfiguracionVista.frx":24AC
      stylesets(8).AlignmentText=   1
      stylesets(9).Name=   "Adjudicado"
      stylesets(9).BackColor=   10079487
      stylesets(9).HasFont=   -1  'True
      BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(9).Picture=   "frmConfiguracionVista.frx":24C8
      stylesets(10).Name=   "ProveedorConAdjuntos"
      stylesets(10).HasFont=   -1  'True
      BeginProperty stylesets(10).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(10).Picture=   "frmConfiguracionVista.frx":24E4
      stylesets(10).AlignmentText=   2
      stylesets(10).AlignmentPicture=   0
      stylesets(11).Name=   "ItemCerrado"
      stylesets(11).ForeColor=   16777215
      stylesets(11).BackColor=   12632256
      stylesets(11).HasFont=   -1  'True
      BeginProperty stylesets(11).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(11).Picture=   "frmConfiguracionVista.frx":2500
      stylesets(12).Name=   "YellowLeft"
      stylesets(12).ForeColor=   0
      stylesets(12).BackColor=   13170165
      stylesets(12).HasFont=   -1  'True
      BeginProperty stylesets(12).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(12).Picture=   "frmConfiguracionVista.frx":251C
      stylesets(12).AlignmentText=   0
      stylesets(13).Name=   "Green"
      stylesets(13).BackColor=   10409634
      stylesets(13).HasFont=   -1  'True
      BeginProperty stylesets(13).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(13).Picture=   "frmConfiguracionVista.frx":2538
      stylesets(13).AlignmentText=   1
      stylesets(14).Name=   "GrayHead"
      stylesets(14).ForeColor=   0
      stylesets(14).BackColor=   -2147483633
      stylesets(14).HasFont=   -1  'True
      BeginProperty stylesets(14).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(14).Picture=   "frmConfiguracionVista.frx":2554
      UseGroups       =   -1  'True
      DividerStyle    =   0
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      UseExactRowCount=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      CellNavigation  =   1
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      Levels          =   2
      RowHeight       =   741
      ExtraHeight     =   265
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Groups.Count    =   2
      Groups(0).Width =   10319
      Groups(0).Caption=   "(Pulse aqu� para ocultar o visualizar la segunda linea)"
      Groups(0).HeadStyleSet=   "GrayHead"
      Groups(0).Columns.Count=   17
      Groups(0).Columns(0).Width=   3096
      Groups(0).Columns(0).Caption=   "Descripci�n"
      Groups(0).Columns(0).Name=   "DESCR"
      Groups(0).Columns(0).CaptionAlignment=   0
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Locked=   -1  'True
      Groups(0).Columns(0).ButtonsAlways=   -1  'True
      Groups(0).Columns(0).HasHeadBackColor=   -1  'True
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).HeadBackColor=   -2147483633
      Groups(0).Columns(0).BackColor=   16776960
      Groups(0).Columns(0).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(0).StyleSet=   "YellowLeft"
      Groups(0).Columns(1).Width=   1376
      Groups(0).Columns(1).Caption=   "A�o/Dest"
      Groups(0).Columns(1).Name=   "ANYO"
      Groups(0).Columns(1).Alignment=   1
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).Locked=   -1  'True
      Groups(0).Columns(1).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(1).StyleSet=   "Yellow"
      Groups(0).Columns(2).Width=   1799
      Groups(0).Columns(2).Visible=   0   'False
      Groups(0).Columns(2).Caption=   "ID"
      Groups(0).Columns(2).Name=   "ID"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(3).Width=   1720
      Groups(0).Columns(3).Caption=   "Proveedor"
      Groups(0).Columns(3).Name=   "PROV"
      Groups(0).Columns(3).DataField=   "Column 3"
      Groups(0).Columns(3).DataType=   8
      Groups(0).Columns(3).FieldLen=   256
      Groups(0).Columns(3).Locked=   -1  'True
      Groups(0).Columns(3).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(3).StyleSet=   "YellowLeft"
      Groups(0).Columns(4).Width=   1905
      Groups(0).Columns(4).Caption=   "Pres.unitario"
      Groups(0).Columns(4).Name=   "PRECAPE"
      Groups(0).Columns(4).Alignment=   1
      Groups(0).Columns(4).CaptionAlignment=   2
      Groups(0).Columns(4).DataField=   "Column 4"
      Groups(0).Columns(4).DataType=   8
      Groups(0).Columns(4).NumberFormat=   "Standard"
      Groups(0).Columns(4).FieldLen=   256
      Groups(0).Columns(4).Locked=   -1  'True
      Groups(0).Columns(4).HasHeadForeColor=   -1  'True
      Groups(0).Columns(4).HasHeadBackColor=   -1  'True
      Groups(0).Columns(4).HasForeColor=   -1  'True
      Groups(0).Columns(4).HeadBackColor=   13170165
      Groups(0).Columns(4).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(4).StyleSet=   "Yellow"
      Groups(0).Columns(5).Width=   1032
      Groups(0).Columns(5).Visible=   0   'False
      Groups(0).Columns(5).Caption=   "Adj"
      Groups(0).Columns(5).Name=   "ADJ"
      Groups(0).Columns(5).DataField=   "Column 5"
      Groups(0).Columns(5).DataType=   8
      Groups(0).Columns(5).FieldLen=   256
      Groups(0).Columns(5).Locked=   -1  'True
      Groups(0).Columns(5).Style=   2
      Groups(0).Columns(6).Width=   1958
      Groups(0).Columns(6).Visible=   0   'False
      Groups(0).Columns(6).Caption=   "Art�culo"
      Groups(0).Columns(6).Name=   "ART"
      Groups(0).Columns(6).Alignment=   1
      Groups(0).Columns(6).CaptionAlignment=   0
      Groups(0).Columns(6).DataField=   "Column 6"
      Groups(0).Columns(6).DataType=   8
      Groups(0).Columns(6).FieldLen=   256
      Groups(0).Columns(6).Locked=   -1  'True
      Groups(0).Columns(6).HasHeadForeColor=   -1  'True
      Groups(0).Columns(6).HasHeadBackColor=   -1  'True
      Groups(0).Columns(6).HasBackColor=   -1  'True
      Groups(0).Columns(6).HeadBackColor=   16776960
      Groups(0).Columns(6).BackColor=   16776960
      Groups(0).Columns(6).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(6).StyleSet=   "Yellow"
      Groups(0).Columns(7).Width=   2223
      Groups(0).Columns(7).Caption=   "Objetivo"
      Groups(0).Columns(7).Name=   "OBJ"
      Groups(0).Columns(7).Alignment=   1
      Groups(0).Columns(7).CaptionAlignment=   2
      Groups(0).Columns(7).DataField=   "Column 7"
      Groups(0).Columns(7).DataType=   8
      Groups(0).Columns(7).FieldLen=   256
      Groups(0).Columns(7).Locked=   -1  'True
      Groups(0).Columns(7).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(7).StyleSet=   "Yellow"
      Groups(0).Columns(8).Width=   291
      Groups(0).Columns(8).Caption=   "Ahorro imp."
      Groups(0).Columns(8).Name=   "AHORROIMP"
      Groups(0).Columns(8).Alignment=   1
      Groups(0).Columns(8).CaptionAlignment=   0
      Groups(0).Columns(8).DataField=   "Column 8"
      Groups(0).Columns(8).DataType=   8
      Groups(0).Columns(8).Level=   1
      Groups(0).Columns(8).NumberFormat=   "Standard"
      Groups(0).Columns(8).FieldLen=   256
      Groups(0).Columns(8).Locked=   -1  'True
      Groups(0).Columns(8).HasHeadBackColor=   -1  'True
      Groups(0).Columns(8).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(9).Width=   318
      Groups(0).Columns(9).Caption=   "Ahorro%"
      Groups(0).Columns(9).Name=   "AHORROPORCEN"
      Groups(0).Columns(9).Alignment=   2
      Groups(0).Columns(9).CaptionAlignment=   2
      Groups(0).Columns(9).DataField=   "Column 9"
      Groups(0).Columns(9).DataType=   8
      Groups(0).Columns(9).Level=   1
      Groups(0).Columns(9).NumberFormat=   "0.0#\%"
      Groups(0).Columns(9).FieldLen=   256
      Groups(0).Columns(9).Locked=   -1  'True
      Groups(0).Columns(9).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(10).Width=   318
      Groups(0).Columns(10).Caption=   "IMP"
      Groups(0).Columns(10).Name=   "IMP"
      Groups(0).Columns(10).Alignment=   1
      Groups(0).Columns(10).CaptionAlignment=   2
      Groups(0).Columns(10).DataField=   "Column 10"
      Groups(0).Columns(10).DataType=   8
      Groups(0).Columns(10).Level=   1
      Groups(0).Columns(10).NumberFormat=   "Standard"
      Groups(0).Columns(10).FieldLen=   256
      Groups(0).Columns(10).Locked=   -1  'True
      Groups(0).Columns(10).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(10).StyleSet=   "Yellow"
      Groups(0).Columns(11).Width=   370
      Groups(0).Columns(11).Caption=   "Cantidad"
      Groups(0).Columns(11).Name=   "CANT"
      Groups(0).Columns(11).Alignment=   1
      Groups(0).Columns(11).CaptionAlignment=   2
      Groups(0).Columns(11).DataField=   "Column 11"
      Groups(0).Columns(11).DataType=   8
      Groups(0).Columns(11).Level=   1
      Groups(0).Columns(11).NumberFormat=   "Standard"
      Groups(0).Columns(11).FieldLen=   256
      Groups(0).Columns(11).Locked=   -1  'True
      Groups(0).Columns(11).HasHeadForeColor=   -1  'True
      Groups(0).Columns(11).HasHeadBackColor=   -1  'True
      Groups(0).Columns(11).HasForeColor=   -1  'True
      Groups(0).Columns(11).HeadForeColor=   12582912
      Groups(0).Columns(11).HeadBackColor=   16777215
      Groups(0).Columns(11).ForeColor=   12582912
      Groups(0).Columns(11).HeadStyleSet=   "BlueHead"
      Groups(0).Columns(11).StyleSet=   "Blue"
      Groups(0).Columns(12).Width=   370
      Groups(0).Columns(12).Caption=   "Adj. %"
      Groups(0).Columns(12).Name=   "PORCENADJ"
      Groups(0).Columns(12).Alignment=   1
      Groups(0).Columns(12).CaptionAlignment=   2
      Groups(0).Columns(12).DataField=   "Column 12"
      Groups(0).Columns(12).DataType=   8
      Groups(0).Columns(12).Level=   1
      Groups(0).Columns(12).NumberFormat=   "0.0#\%"
      Groups(0).Columns(12).FieldLen=   256
      Groups(0).Columns(12).Locked=   -1  'True
      Groups(0).Columns(12).HeadStyleSet=   "BlueHead"
      Groups(0).Columns(12).StyleSet=   "Blue"
      Groups(0).Columns(13).Width=   265
      Groups(0).Columns(13).Caption=   "Solicitud"
      Groups(0).Columns(13).Name=   "VINCULADO_ITEM"
      Groups(0).Columns(13).Alignment=   1
      Groups(0).Columns(13).DataField=   "Column 13"
      Groups(0).Columns(13).DataType=   8
      Groups(0).Columns(13).Level=   1
      Groups(0).Columns(13).FieldLen=   256
      Groups(0).Columns(13).Locked=   -1  'True
      Groups(0).Columns(13).Style=   1
      Groups(0).Columns(13).ButtonsAlways=   -1  'True
      Groups(0).Columns(13).HasHeadBackColor=   -1  'True
      Groups(0).Columns(13).HasBackColor=   -1  'True
      Groups(0).Columns(13).HeadBackColor=   -2147483633
      Groups(0).Columns(13).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(13).StyleSet=   "Yellow"
      Groups(0).Columns(14).Width=   1085
      Groups(0).Columns(14).Caption=   "DFec. ini suministro"
      Groups(0).Columns(14).Name=   "INI"
      Groups(0).Columns(14).CaptionAlignment=   2
      Groups(0).Columns(14).DataField=   "Column 14"
      Groups(0).Columns(14).DataType=   8
      Groups(0).Columns(14).Level=   1
      Groups(0).Columns(14).FieldLen=   256
      Groups(0).Columns(14).Locked=   -1  'True
      Groups(0).Columns(14).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(14).StyleSet=   "YellowLeft"
      Groups(0).Columns(15).Width=   2381
      Groups(0).Columns(15).Caption=   "DFec. fin suministro"
      Groups(0).Columns(15).Name=   "FIN"
      Groups(0).Columns(15).CaptionAlignment=   2
      Groups(0).Columns(15).DataField=   "Column 15"
      Groups(0).Columns(15).DataType=   8
      Groups(0).Columns(15).Level=   1
      Groups(0).Columns(15).FieldLen=   256
      Groups(0).Columns(15).Locked=   -1  'True
      Groups(0).Columns(15).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(15).StyleSet=   "YellowLeft"
      Groups(0).Columns(16).Width=   4921
      Groups(0).Columns(16).Caption=   "Destr. material"
      Groups(0).Columns(16).Name=   "GMN"
      Groups(0).Columns(16).CaptionAlignment=   2
      Groups(0).Columns(16).DataField=   "Column 16"
      Groups(0).Columns(16).DataType=   8
      Groups(0).Columns(16).Level=   1
      Groups(0).Columns(16).FieldLen=   256
      Groups(0).Columns(16).Locked=   -1  'True
      Groups(0).Columns(16).HeadStyleSet=   "YellowHead"
      Groups(0).Columns(16).StyleSet=   "YellowLeft"
      Groups(1).Width =   5292
      Groups(1).Visible=   0   'False
      Groups(1).Caption=   "(Pulse sobre la cabecera para ocultar las cantidades)"
      Groups(1).CaptionAlignment=   0
      Groups(1).AllowSizing=   0   'False
      Groups(1).Columns.Count=   6
      Groups(1).Columns(0).Width=   1535
      Groups(1).Columns(0).Caption=   "GRUPO"
      Groups(1).Columns(0).Name=   "GRUPO"
      Groups(1).Columns(0).DataField=   "Column 17"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   767
      Groups(1).Columns(1).Caption=   "Pago"
      Groups(1).Columns(1).Name=   "PAG"
      Groups(1).Columns(1).DataField=   "Column 18"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).Locked=   -1  'True
      Groups(1).Columns(2).Width=   767
      Groups(1).Columns(2).Caption=   "Uni"
      Groups(1).Columns(2).Name=   "UNI"
      Groups(1).Columns(2).DataField=   "Column 19"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).Locked=   -1  'True
      Groups(1).Columns(3).Width=   767
      Groups(1).Columns(3).Caption=   "CERRADO"
      Groups(1).Columns(3).Name=   "CERRADO"
      Groups(1).Columns(3).DataField=   "Column 20"
      Groups(1).Columns(3).DataType=   11
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(4).Width=   1455
      Groups(1).Columns(4).Caption=   "CODPROVEACTUAL"
      Groups(1).Columns(4).Name=   "CODPROVEACTUAL"
      Groups(1).Columns(4).DataField=   "Column 21"
      Groups(1).Columns(4).DataType=   8
      Groups(1).Columns(4).FieldLen=   256
      Groups(1).Columns(5).Width=   26
      Groups(1).Columns(5).Visible=   0   'False
      Groups(1).Columns(5).Caption=   "Importe"
      Groups(1).Columns(5).Name=   "DEST"
      Groups(1).Columns(5).Alignment=   1
      Groups(1).Columns(5).CaptionAlignment=   2
      Groups(1).Columns(5).DataField=   "Column 22"
      Groups(1).Columns(5).DataType=   8
      Groups(1).Columns(5).NumberFormat=   "Standard"
      Groups(1).Columns(5).FieldLen=   256
      Groups(1).Columns(5).Locked=   -1  'True
      Groups(1).Columns(5).HasHeadBackColor=   -1  'True
      Groups(1).Columns(5).HasForeColor=   -1  'True
      Groups(1).Columns(5).HeadStyleSet=   "YellowHead"
      UseDefaults     =   0   'False
      _ExtentX        =   19394
      _ExtentY        =   5786
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TabStrip sstabComparativa 
      Height          =   4800
      Left            =   0
      TabIndex        =   1
      Top             =   480
      Width           =   11205
      _ExtentX        =   19764
      _ExtentY        =   8467
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "DGeneral"
            Key             =   "General"
            Object.Tag             =   "General"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblTitulo 
      Alignment       =   2  'Center
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10155
   End
End
Attribute VB_Name = "frmConfiguracionVista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oPlantillaSeleccionada As CPlantilla

Private m_iNumColsGR As Integer
Private m_iNumColsAll As Integer

'Variables para idiomas
Private m_sCap(1 To 11) As String
Private m_sVista4 As String
Private m_sVista1 As String
Private m_sVista2 As String
Private m_sVista3 As String
Private m_sVista  As String
Private m_sAdjudicado As String
Private m_sProceso As String
Private m_sAhorrado As String
Private m_sConsumido As String
Private m_sAhorroPorce As String
Private m_sImporteOferta As String
Private m_sAhorroOferta As String
Private m_sPorcenAhorroOferta As String
Private sCaptionTotales As String
Private m_sGeneral As String
Private m_sImpAdj As String
Private m_sCons As String
Private m_sAdj As String
Private m_sAhorr As String
Private m_sAhorrOf As String
Private m_sImp As String
Private m_sTodos As String
Private m_sALL As String
Private m_sProveedor As String
Private m_sLitGrupo As String
Private m_sAnyo As String
Private m_sProvActual As String
Private m_sAhorroImp As String
Private m_sAhorroPorcen As String
Private m_sPresUnitario As String
Private m_sImporte As String
Private m_sSobre As String
Private m_sGVistaNueva As String
Private m_sRenombrarVista As String
Private m_sFecIniSum As String
Private m_sFecFinSum As String
Private m_sEstrMat As String

'Clases para la configuraci�n de las vistas
Private m_oConfVistasProce As CPlantConfVistasProce
Private m_oConfVistasAll As CPlantConfVistasAll
Public m_oVistaSeleccionada As CPlantConfVistaProce
Public m_oVistaSeleccionadaGr As CPlantConfVistaGrupo
Public m_oVistaSeleccionadaAll As CPlantConfVistaAll

Private m_oVistasInicial As CConfVistasProce
Private m_oVistasInicialGR As CConfVistasGrupo
Private m_oVistasInicialALL As CConfVistasAll

Private m_bGridProcVisible As Boolean
Private m_oGrupoSeleccionado As CGrupo

'Para mostrar los atributos de grupo que est�n definidos en varios grupos
Private Type TipoAtrib
    Id As Long
    Cod As String
    Visible As Boolean
    ColVisible As Boolean
    Posicion As Variant
    Width As Variant
End Type

'Array y clases para contener las posiciones ordenadas de las pesta�as de ALL y Grupos
Private m_arrOrden(13) As Variant
Private m_arrOrdenColsAll() As Variant
Private m_arrOrdenColsGr() As Variant

'Variables para contener los levels de las columnas no visibles
Private m_iLevelCantMax As Integer
Private m_iLevelCodProve As Integer
Private m_iLevelNumOfe As Integer
Private m_iLevelHom As Integer
Private m_iLevelIndice As Integer
Private m_iLevelCantMaxAll As Integer
Private m_iLevelCodProveAll As Integer
Private m_iLevelNumOfeAll As Integer
Private m_iLevelHomAll As Integer
Private m_iLevelIndiceAll As Integer

Private m_bCargandoVista As Boolean

'Constantes
Private Const ANCHO_FILA_GEN = 255.1181
Private Const ANCHO_FILA_GR = 480.189

Public m_bModif As Boolean

Private oVistasCombo As CPlantConfVistasProce

Private m_sVistaIni As String
Private m_sVistaPlan As String
Public g_sNombreVista As String

Private m_sCaptionSolicVinculada As String
Private m_sCaptionVistaSolicVinculada As String

Private Sub CargarComboVistas()
    Dim bVistaIni As Boolean
    Dim oVista As CPlantConfVistaProce
    
       
    bVistaIni = True
    
    sdbcVistaActual.RemoveAll
    sdbcVistaActGr.RemoveAll
    sdbcVistaActAll.RemoveAll
    sdbcVistaDefecto.RemoveAll
    sdbcVistaDefectoGr.RemoveAll
    sdbcVistaDefectoAll.RemoveAll

    Set oVistasCombo = Nothing
    Set oVistasCombo = oFSGSRaiz.Generar_CPlantConfVistasProce
    oVistasCombo.CargarCombosVistas g_oPlantillaSeleccionada, oUsuarioSummit.Cod
    
    For Each oVista In oVistasCombo
        Select Case oVista.Vista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaActual.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaActGr.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaActAll.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaDefecto.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaDefectoGr.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaDefectoAll.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
            Case Else
                sdbcVistaActual.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaActGr.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaActAll.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaDefecto.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaDefectoGr.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
                sdbcVistaDefectoAll.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
        End Select
    Next
    
    Select Case g_oPlantillaSeleccionada.VistaDefectoComp
        Case TipoDeVistaDefecto.vistainicial
            sdbcVistaActual.Text = m_sVistaIni
            sdbcVistaActGr.Text = m_sVistaIni
            sdbcVistaActAll.Text = m_sVistaIni
            sdbcVistaDefecto.Text = m_sVistaIni
            sdbcVistaDefectoGr.Text = m_sVistaIni
            sdbcVistaDefectoAll.Text = m_sVistaIni
            
        Case Else
            sdbcVistaActual.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
            sdbcVistaActGr.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
            sdbcVistaActAll.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
            sdbcVistaDefecto.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
            sdbcVistaDefectoGr.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
            sdbcVistaDefectoAll.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
            
    End Select
    
    sdbcVistaActual.Columns("COD").Value = g_oPlantillaSeleccionada.VistaDefectoComp
    sdbcVistaActGr.Columns("COD").Value = g_oPlantillaSeleccionada.VistaDefectoComp
    sdbcVistaActAll.Columns("COD").Value = g_oPlantillaSeleccionada.VistaDefectoComp
    sdbcVistaDefecto.Columns("COD").Value = g_oPlantillaSeleccionada.VistaDefectoComp
    sdbcVistaDefectoGr.Columns("COD").Value = g_oPlantillaSeleccionada.VistaDefectoComp
    sdbcVistaDefectoAll.Columns("COD").Value = g_oPlantillaSeleccionada.VistaDefectoComp
    
    sdbcVistaActual.SelStart = 0
    sdbcVistaActual.SelLength = Len(sdbcVistaActual.Text)
    sdbcVistaActual.Refresh
    
    sdbcVistaActGr.SelStart = 0
    sdbcVistaActGr.SelLength = Len(sdbcVistaActGr.Text)
    sdbcVistaActGr.Refresh
    
    sdbcVistaActAll.SelStart = 0
    sdbcVistaActAll.SelLength = Len(sdbcVistaActAll.Text)
    sdbcVistaActAll.Refresh
    
    sdbcVistaDefecto.SelStart = 0
    sdbcVistaDefecto.SelLength = Len(sdbcVistaDefecto.Text)
    sdbcVistaDefecto.Refresh
    
    sdbcVistaDefectoGr.SelStart = 0
    sdbcVistaDefectoGr.SelLength = Len(sdbcVistaDefectoGr.Text)
    sdbcVistaDefectoGr.Refresh

    sdbcVistaDefectoAll.SelStart = 0
    sdbcVistaDefectoAll.SelLength = Len(sdbcVistaDefectoAll.Text)
    sdbcVistaDefectoAll.Refresh

End Sub

Private Sub GuardarVistaAllEnBd()
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oAtribVista  As CPlantConfVistaAllAtrib


'Guarda la configuraci�n de la vista actual
Set oIBaseDatos = m_oVistaSeleccionadaAll

If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
    'Guarda la vista con los valores por defecto en base de datos
    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
    End If
    Set oIBaseDatos = Nothing
    
    'Guarda la configuraci�n de los atributos
    If Not m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib Is Nothing Then
        For Each oAtribVista In m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib
             Set oIBaseDatos = oAtribVista
             If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                teserror = oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    oIBaseDatos.CancelarEdicion
                    Exit Sub
                End If
                Set oIBaseDatos = Nothing
             End If
        Next
    End If
    
    ActualizarFormPlantillas
    
Else  'Modifica la vista actual
    teserror = oIBaseDatos.FinalizarEdicionModificando

    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        oIBaseDatos.CancelarEdicion
        Exit Sub
    End If
    Set oIBaseDatos = Nothing
    
End If

End Sub

Public Sub GuardarVistaGeneralEnBd()
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oAtribVista As CPlantConfVistaProceAtrib
Dim oSobreVista As CPlantConfVistaProceSobre

'Guarda la configuraci�n de la vista actual
    Set oIBaseDatos = m_oVistaSeleccionada
    
    If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
        'Si no exist�a en base de datos la guarda la vista con los valores por defecto en base de datos
        teserror = oIBaseDatos.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
        Set oIBaseDatos = Nothing

        'Inserta la configuraci�n de los atributos
        If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
            For Each oAtribVista In m_oVistaSeleccionada.PlantConfVistasProceAtrib
                 Set oIBaseDatos = oAtribVista
                 If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    Set oIBaseDatos = Nothing
                 End If
            Next
        End If
        
        'Inserta la configuraci�n de los sobre
         If Not m_oVistaSeleccionada.PlantConfVistasProceSobre Is Nothing Then
            For Each oSobreVista In m_oVistaSeleccionada.PlantConfVistasProceSobre
                 Set oIBaseDatos = oSobreVista
                 If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    Set oIBaseDatos = Nothing
                 End If
            Next
        End If

        ActualizarFormPlantillas
        
    Else
        'Modifica
        teserror = oIBaseDatos.FinalizarEdicionModificando
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            oIBaseDatos.CancelarEdicion
            Exit Sub
        End If
        
        Set oIBaseDatos = Nothing
        
    End If
End Sub

Private Sub GuardarVistaGrEnBd()
Dim teserror As TipoErrorSummit
Dim oIBaseDatos As IBaseDatos
Dim oAtribVista As CPlantConfVistaGrupoAtrib
Dim oGrupo As CGrupo
Dim iVista As Integer
Dim oVistaGR As CPlantConfVistaGrupo


For Each oGrupo In g_oPlantillaSeleccionada.Grupos
    If Not IsEmpty(m_oVistaSeleccionadaGr.Vista) And Not IsNull(m_oVistaSeleccionadaGr.Vista) Then
        iVista = m_oVistaSeleccionadaGr.Vista
    Else
        iVista = 1
    End If

    If Not oGrupo.PlantConfVistas Is Nothing Then
        If Not oGrupo.PlantConfVistas.Item(CStr(iVista)) Is Nothing Then
                
                
                Set oVistaGR = oGrupo.PlantConfVistas.Item(CStr(iVista))
                'Almacena en BD
                Set oIBaseDatos = oVistaGR
                If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                    'Guarda la vista con los valores por defecto en base de datos
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    Set oIBaseDatos = Nothing

                    'Guarda las configuraciones de los atributos
                    If Not oVistaGR.PlantConfVistasGrAtrib Is Nothing Then
                        For Each oAtribVista In oVistaGR.PlantConfVistasGrAtrib
                             Set oIBaseDatos = oAtribVista
                             If oIBaseDatos.ComprobarExistenciaEnBaseDatos = False Then
                                teserror = oIBaseDatos.AnyadirABaseDatos
                                If teserror.NumError <> TESnoerror Then
                                    basErrores.TratarError teserror
                                    oIBaseDatos.CancelarEdicion
                                    Exit Sub
                                End If
                               Set oIBaseDatos = Nothing
                             End If
                        Next
                    End If
                        
                    ActualizarFormPlantillas
                    Else

                        'Modifica la configuraci�n de la vista actual
                        Set oIBaseDatos = oVistaGR
                        teserror = oIBaseDatos.FinalizarEdicionModificando

                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            oIBaseDatos.CancelarEdicion
                            Exit Sub
                        End If
                        Set oIBaseDatos = Nothing

                    End If
        End If
    End If
Next
   
End Sub

Private Sub chkExcluirGrCerradosResul_Click()
With m_oVistaSeleccionada
        If m_bCargandoVista = False Then
            .HayCambios = True
        End If
        
        If chkExcluirGrCerradosResul.Value = vbChecked Then
            .ExcluirGrCerradosResult = True
        Else
            .ExcluirGrCerradosResult = False
        End If
    End With
End Sub

Private Sub chkOcultarGrCerrados_Click()
    Dim i As Integer
    Dim vbm As Variant
       
    'Oculta los grupos cerrados
    If chkOcultarGrCerrados.Value = vbChecked Then
        m_oVistaSeleccionada.OcultarGrCerrados = True
        
        'Para la grid sdbgGruposProve
        i = sdbgGruposProve.Rows - 1
        While i >= 0
            vbm = sdbgGruposProve.AddItemBookmark(i)
            If sdbgGruposProve.Columns("CERRADO").CellValue(vbm) = 1 Then
                sdbgGruposProve.RemoveItem (sdbgGruposProve.AddItemRowIndex(vbm))
            End If
            i = i - 1
        Wend
        
        'Para la grid sdbgProveGrupos
        For i = 1 To sdbgProveGrupos.Groups.Count - 1
            If Not IsEmpty(sdbgProveGrupos.Groups(i).TagVariant) Then
                If sdbgProveGrupos.Groups(i).Columns("CERRADO" & sdbgProveGrupos.Groups(i).TagVariant).Value = 1 Then
                    sdbgProveGrupos.Groups(i).Visible = False
                    sdbgtotales2.Groups(i).Visible = False
                End If
            End If
        Next
    Else   'Hace visibles los grupos cerrados
        m_oVistaSeleccionada.OcultarGrCerrados = False
        
        'Para la grid sdbgProveGrupos
        For i = 1 To sdbgProveGrupos.Groups.Count - 1
            If Not IsEmpty(sdbgProveGrupos.Groups(i).TagVariant) Then
                If sdbgProveGrupos.Groups(i).Columns("CERRADO" & sdbgProveGrupos.Groups(i).TagVariant).Value = 1 Then
                    sdbgProveGrupos.Groups(i).Visible = True
                    sdbgtotales2.Groups(i).Visible = True
                End If
            End If
        Next
    End If
End Sub

Private Sub cmdEliminarVista_Click()
 Dim irespuesta As Integer
 Dim oIBaseDatos As IBaseDatos
 Dim teserror As TipoErrorSummit
 
 irespuesta = oMensajes.PreguntaEliminar(sdbcVistaActual.Text)
 
 If irespuesta = vbYes Then
    Set oIBaseDatos = m_oVistaSeleccionada
    teserror = oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        oIBaseDatos.CancelarEdicion
        Exit Sub
    End If
    
    If g_oPlantillaSeleccionada.NombreVistaDefecto = sdbcVistaActual.Text Then
        g_oPlantillaSeleccionada.VistaDefectoComp = 0
        g_oPlantillaSeleccionada.NombreVistaDefecto = m_sVistaIni
    End If
    
    sdbcVistaActual.Text = m_sVistaIni
    sdbcVistaActual.Columns("COD").Value = 0
    
    ConfiguracionVistaActual 0, False

    'Carga en las grids la configuraci�n de la vista actual
    CargarVistasProc
    
    'Redimensiona la grid seg�n la configuraci�n seleccionada
    RedimensionarGrid
    
    ' pongo el nombre en el caption del grid
     If m_bGridProcVisible Then
        sdbgProce.caption = m_sVistaIni
        sdbgGruposProve.caption = ""
     Else
        sdbgGruposProve.caption = m_sVistaIni
        sdbgProce.caption = ""
     End If
     sdbgAll.caption = m_sVistaIni
     sdbgProveGrupos.caption = m_sVistaIni
     sdbgAdj.caption = m_sVistaIni
    
 End If

End Sub

Private Sub cmdEliminarVistaAll_Click()
 Dim irespuesta As Integer
 Dim oIBaseDatos As IBaseDatos
 Dim teserror As TipoErrorSummit
 
 irespuesta = oMensajes.PreguntaEliminar(sdbcVistaActAll.Text)
 
 If irespuesta = vbYes Then
    Set oIBaseDatos = m_oVistaSeleccionada
    teserror = oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        oIBaseDatos.CancelarEdicion
        Exit Sub
    End If
    
    If g_oPlantillaSeleccionada.NombreVistaDefecto = sdbcVistaActAll.Text Then
        g_oPlantillaSeleccionada.VistaDefectoComp = 0
        g_oPlantillaSeleccionada.NombreVistaDefecto = m_sVistaIni
    End If
    
    sdbcVistaActAll.Text = m_sVistaIni
    sdbcVistaActAll.Columns("COD").Value = 0

    ConfiguracionVistaActualAll 0, False
    
    'Carga en las grids la configuraci�n de la vista actual
    CargarVistasAll
    
    'Redimensiona la grid seg�n la configuraci�n seleccionada
    RedimensionarGridAll
    
    ' pongo el nombre en el caption del grid
     If m_bGridProcVisible Then
        sdbgProce.caption = m_sVistaIni
        sdbgGruposProve.caption = ""
     Else
        sdbgGruposProve.caption = m_sVistaIni
        sdbgProce.caption = ""
     End If
     sdbgAll.caption = m_sVistaIni
     sdbgProveGrupos.caption = m_sVistaIni
     sdbgAdj.caption = m_sVistaIni
    
 End If
End Sub

Private Sub cmdEliminarVistaGR_Click()
 Dim irespuesta As Integer
 Dim oIBaseDatos As IBaseDatos
 Dim teserror As TipoErrorSummit
 
 irespuesta = oMensajes.PreguntaEliminar(sdbcVistaActGr.Text)
 
 If irespuesta = vbYes Then
    Set oIBaseDatos = m_oVistaSeleccionada
    teserror = oIBaseDatos.EliminarDeBaseDatos
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        oIBaseDatos.CancelarEdicion
        Exit Sub
    End If
    
    If g_oPlantillaSeleccionada.NombreVistaDefecto = sdbcVistaActGr.Text Then
        g_oPlantillaSeleccionada.VistaDefectoComp = 0
        g_oPlantillaSeleccionada.NombreVistaDefecto = m_sVistaIni
    End If
    
    sdbcVistaActGr.Text = m_sVistaIni
    sdbcVistaActGr.Columns("COD").Value = 0
    
    ConfiguracionVistaActualGr 0, False

   'Carga en las grids la configuraci�n de la vista actual
    CargarVistasGrupo
   
   'Redimensiona la grid seg�n la configuraci�n seleccionada
    RedimensionarGridGrupo
    
    ' pongo el nombre en el caption del grid
     If m_bGridProcVisible Then
        sdbgProce.caption = m_sVistaIni
        sdbgGruposProve.caption = ""
     Else
        sdbgGruposProve.caption = m_sVistaIni
        sdbgProce.caption = ""
     End If
     sdbgAll.caption = m_sVistaIni
     sdbgProveGrupos.caption = m_sVistaIni
     sdbgAdj.caption = m_sVistaIni
    
    
 End If

End Sub

Private Sub cmdGuardarVista_Click()
    If Not m_oVistaSeleccionada Is Nothing Then
        GuardarVistaGeneral
    End If
    If Not m_oVistaSeleccionadaAll Is Nothing Then
        GuardarVistaAll
    End If
    If Not m_oVistaSeleccionadaGr Is Nothing Then
        GuardarVistaGr
    End If
    
    GuardarVistas
End Sub

Private Sub cmdGuardarVistaAll_Click()
    If Not m_oVistaSeleccionada Is Nothing Then
        GuardarVistaGeneral
    End If
    If Not m_oVistaSeleccionadaAll Is Nothing Then
        GuardarVistaAll
    End If
    If Not m_oVistaSeleccionadaGr Is Nothing Then
        GuardarVistaGr
    End If
    
    GuardarVistas

End Sub
Private Sub cmdGuardarVistaGr_Click()
    If Not m_oVistaSeleccionada Is Nothing Then
        GuardarVistaGeneral
    End If
    If Not m_oVistaSeleccionadaAll Is Nothing Then
        GuardarVistaAll
    End If
    If Not m_oVistaSeleccionadaGr Is Nothing Then
        GuardarVistaGr
    End If
    GuardarVistas
End Sub

Private Sub cmdGuardarVistaNueva_Click()
Dim teserror As TipoErrorSummit
Dim iVistaVieja As Integer
'Ponemos en la colecci�n lo que se haya modificado en pantalla
GuardarVistaGeneral
frmADJGuardarVista.g_sOrigen = "frmConfiguracionVista"
frmADJGuardarVista.caption = m_sGVistaNueva
Set frmADJGuardarVista.m_ofrmConfiguracionVista = Me
frmADJGuardarVista.Show 1
iVistaVieja = m_oVistaSeleccionada.Vista
If g_sNombreVista <> "" Then
    teserror = m_oVistaSeleccionada.GuardarVistaNuevaBd(g_sNombreVista, basOptimizacion.gvarCodUsuario, m_oVistaSeleccionadaAll)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    Else
        EliminarVistaDeColeccion CStr(iVistaVieja)
        oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
    End If
    ConfiguracionVistaActual teserror.Arg1, False
    sdbcVistaActual.Text = g_sNombreVista
    sdbcVistaActual.Refresh
    ConfigurarBotonesVistas TipoDeVistaDefecto.VistaDeUsuario
    CargarVistasProc
    
    ' pongo el nombre en el caption del grid
     If m_bGridProcVisible Then
        sdbgProce.caption = m_sVista & ": " & g_sNombreVista
        sdbgGruposProve.caption = ""
     Else
        sdbgGruposProve.caption = m_sVista & ": " & g_sNombreVista
        sdbgProce.caption = ""
     End If
     sdbgAll.caption = m_sVista & ": " & g_sNombreVista
     sdbgProveGrupos.caption = m_sVista & ": " & g_sNombreVista
     sdbgAdj.caption = m_sVista & ": " & g_sNombreVista
End If
End Sub

Private Sub cmdGuardarVistaNuevaAll_Click()
Dim teserror As TipoErrorSummit
Dim iVistaVieja As Integer
    If Not m_oVistaSeleccionadaAll Is Nothing Then
        GuardarVistaAll
    End If
    frmADJGuardarVista.g_sOrigen = "frmConfiguracionVista"
    frmADJGuardarVista.caption = m_sGVistaNueva
    Set frmADJGuardarVista.m_ofrmConfiguracionVista = Me
    frmADJGuardarVista.Show 1
    iVistaVieja = m_oVistaSeleccionada.Vista
    If g_sNombreVista <> "" Then
      teserror = m_oVistaSeleccionada.GuardarVistaNuevaBd(g_sNombreVista, basOptimizacion.gvarCodUsuario, m_oVistaSeleccionadaAll)
      If teserror.NumError <> TESnoerror Then
          basErrores.TratarError teserror
          Exit Sub
      Else
          m_oConfVistasProce.Remove (CStr(iVistaVieja))
          If Not m_oVistaSeleccionadaAll Is Nothing Then
             If Not m_oConfVistasAll.Item(CStr(iVistaVieja)) Is Nothing Then
                m_oConfVistasAll.Remove (CStr(iVistaVieja))
             End If
          End If
             If Not m_oVistaSeleccionadaGr Is Nothing Then
                If Not m_oGrupoSeleccionado Is Nothing Then
                    If Not m_oGrupoSeleccionado.PlantConfVistas.Item(CStr(iVistaVieja)) Is Nothing Then
                        m_oGrupoSeleccionado.PlantConfVistas.Remove (CStr(iVistaVieja))
                    End If
                End If
            End If
          oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
      End If
      ConfiguracionVistaActualAll teserror.Arg1, False
      sdbcVistaActAll.Refresh
      sdbcVistaActAll.Text = g_sNombreVista
      ConfigurarBotonesVistas TipoDeVistaDefecto.VistaDeUsuario
      CargarVistasAll
      
      ' pongo el nombre en el caption del grid
       If m_bGridProcVisible Then
          sdbgProce.caption = m_sVista & ": " & g_sNombreVista
          sdbgGruposProve.caption = ""
       Else
          sdbgGruposProve.caption = m_sVista & ": " & g_sNombreVista
          sdbgProce.caption = ""
       End If
       sdbgAll.caption = m_sVista & ": " & g_sNombreVista
       sdbgProveGrupos.caption = m_sVista & ": " & g_sNombreVista
       sdbgAdj.caption = m_sVista & ": " & g_sNombreVista
    End If
End Sub

Private Sub cmdGuardarVistaNuevaGr_Click()
    Dim teserror As TipoErrorSummit
    Dim iVistaVieja As Integer
    If Not m_oVistaSeleccionadaGr Is Nothing Then
        GuardarVistaGr
    End If
    frmADJGuardarVista.g_sOrigen = "frmConfiguracionVista"
    frmADJGuardarVista.caption = m_sGVistaNueva
    Set frmADJGuardarVista.m_ofrmConfiguracionVista = Me
    frmADJGuardarVista.Show 1
    iVistaVieja = m_oVistaSeleccionada.Vista
    If g_sNombreVista <> "" Then
        teserror = m_oVistaSeleccionada.GuardarVistaNuevaBd(g_sNombreVista, basOptimizacion.gvarCodUsuario, m_oVistaSeleccionadaAll)
        If teserror.NumError <> TESnoerror Then
           basErrores.TratarError teserror
           Exit Sub
        Else
            m_oConfVistasProce.Remove (CStr(iVistaVieja))
            If Not m_oVistaSeleccionadaAll Is Nothing Then
                If Not m_oConfVistasAll.Item(CStr(iVistaVieja)) Is Nothing Then
                    m_oConfVistasAll.Remove (CStr(iVistaVieja))
                End If
            End If
            If Not m_oVistaSeleccionadaGr Is Nothing Then
                If Not m_oGrupoSeleccionado Is Nothing Then
                    If Not m_oGrupoSeleccionado.PlantConfVistas.Item(CStr(iVistaVieja)) Is Nothing Then
                        m_oGrupoSeleccionado.PlantConfVistas.Remove (CStr(iVistaVieja))
                    End If
                End If
            oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
        End If
        ConfiguracionVistaActualGr teserror.Arg1, False
        sdbcVistaActGr.Text = g_sNombreVista
        ConfigurarBotonesVistas TipoDeVistaDefecto.VistaDeUsuario
        CargarVistasGrupo
        
        ' pongo el nombre en el caption del grid
        If m_bGridProcVisible Then
            sdbgProce.caption = m_sVista & ": " & g_sNombreVista
            sdbgGruposProve.caption = ""
        Else
            sdbgGruposProve.caption = m_sVista & ": " & g_sNombreVista
            sdbgProce.caption = ""
        End If
        sdbgAll.caption = m_sVista & ": " & g_sNombreVista
        sdbgProveGrupos.caption = m_sVista & ": " & g_sNombreVista
        sdbgAdj.caption = m_sVista & ": " & g_sNombreVista
    End If
  End If
End Sub

Private Sub EliminarVistaDeColeccion(ByVal sIndice As String)
Dim oGrupo As CGrupo
 If Not m_oConfVistasProce.Item(sIndice) Is Nothing Then
     m_oConfVistasProce.Remove (sIndice)
 End If
 If Not m_oVistaSeleccionadaAll Is Nothing Then
     If Not m_oConfVistasAll.Item(sIndice) Is Nothing Then
         m_oConfVistasAll.Remove (sIndice)
     End If
 End If
For Each oGrupo In g_oPlantillaSeleccionada.Grupos
     If Not oGrupo.PlantConfVistas Is Nothing Then
         If Not oGrupo.PlantConfVistas.Item(sIndice) Is Nothing Then
             oGrupo.PlantConfVistas.Remove (sIndice)
         End If
     End If
 Next
End Sub

Private Sub cmdInvertir_Click()
    Dim i As Integer
    'Oculta los pictures
    picControlVistaProceso.Visible = False
    picControlVistaAll.Visible = False
    picControlVistaGrupo.Visible = False
    'Al invertir hace un scroll de las grids para dejarlas en la posici�n inicial:
    If sdbgGruposProve.GrpPosition(0) > 0 Then
        i = sdbgGruposProve.Groups(0).Position
        sdbgGruposProve.Scroll -i, 0
        sdbgGruposProve.Update
        sdbgTotalesProve.Scroll -i, 0
        sdbgTotalesProve.Update
        sdbgGruposProve.AllowColumnMoving = ssRelocateWithinGroup
    End If
    If sdbgProveGrupos.GrpPosition(0) > 0 Then
        i = sdbgProveGrupos.Groups(0).Position
        sdbgProveGrupos.Scroll -i, 0
        sdbgProveGrupos.Update
        sdbgtotales2.Scroll -i, 0
        sdbgtotales2.Update
        sdbgProveGrupos.AllowColumnMoving = ssRelocateWithinGroup
    End If
    If sdbgProce.GrpPosition(0) > 0 Then
        i = sdbgProce.Groups(0).Position
        sdbgProce.Scroll -i, 0
        sdbgProce.Update
        sdbgProce.AllowColumnMoving = ssRelocateWithinGroup
    End If
    For i = 0 To sdbgGruposProve.Groups.Count - 1
        sdbgGruposProve.Groups(i).Position = i
    Next i
    For i = 0 To sdbgProce.Groups.Count - 1
        sdbgProce.Groups(i).Position = i
    Next i
    For i = 0 To sdbgProveGrupos.Groups.Count - 1
        sdbgProveGrupos.Groups(i).Position = i
    Next i
    'Invierte las grids
    If sdbgGruposProve.Visible = True Then
        sdbgGruposProve.Visible = False
        sdbgTotalesProve.Visible = False
        sdbgProveGrupos.Visible = True
        sdbgtotales2.Visible = True
        sdbgProce.Visible = False
        m_oVistaSeleccionada.TipoVision = 1
        For i = 0 To sdbgProveGrupos.Groups.Count - 1
            sdbgtotales2.Groups(i).Width = sdbgProveGrupos.Groups(i).Width
        Next i
    Else
        sdbgGruposProve.Visible = True
        sdbgTotalesProve.Visible = True
        If m_bGridProcVisible = True Then
            sdbgProce.Visible = True
            sdbgGruposProve.caption = ""
        Else
            If m_oVistaSeleccionada.Vista = 0 Then
                sdbgGruposProve.caption = m_sVistaIni
            Else
                sdbgGruposProve.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
            End If
        End If
        sdbgProveGrupos.Visible = False
        sdbgtotales2.Visible = False
        m_oVistaSeleccionada.TipoVision = 0
        For i = 0 To sdbgGruposProve.Groups.Count - 1
            sdbgTotalesProve.Groups(i).Width = sdbgGruposProve.Groups(i).Width
        Next i
        sdbgProce.Groups(0).Width = sdbgGruposProve.Groups(0).Width
    End If
    'Hace que el bot�n pierda el foco
    If Me.Visible Then sstabComparativa.SetFocus
End Sub
Private Sub RenombrarVista(oActual As SSDBCombo, oDefecto As SSDBCombo)
Dim teserror As TipoErrorSummit
frmADJGuardarVista.m_sNombreVistaAnterior = m_oVistaSeleccionada.NombreVista
frmADJGuardarVista.g_sOrigen = "frmConfiguracionVista"
Set frmADJGuardarVista.m_ofrmConfiguracionVista = Me
frmADJGuardarVista.caption = m_sRenombrarVista
frmADJGuardarVista.Show 1
If g_sNombreVista <> "" Then
    teserror = m_oVistaSeleccionada.RenombrarVista(g_sNombreVista)
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    Else
        oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
    End If
    oActual.Refresh
    oActual.Text = g_sNombreVista
    If m_oVistaSeleccionada.Vista = g_oPlantillaSeleccionada.VistaDefectoComp Then
        oDefecto.Text = g_sNombreVista
        g_oPlantillaSeleccionada.NombreVistaDefecto = g_sNombreVista
    End If
    If m_bGridProcVisible Then
       sdbgProce.caption = m_sVista & ": " & g_sNombreVista
       sdbgGruposProve.caption = ""
    Else
       sdbgGruposProve.caption = m_sVista & ": " & g_sNombreVista
       sdbgProce.caption = ""
    End If
    sdbgAll.caption = m_sVista & ": " & g_sNombreVista
    sdbgProveGrupos.caption = m_sVista & ": " & g_sNombreVista
    sdbgAdj.caption = m_sVista & ": " & g_sNombreVista
End If
End Sub

Private Sub cmdRenombrarVista_Click()
RenombrarVista sdbcVistaActual, sdbcVistaDefecto
If sdbcVistaDefecto.Text <> "" Then
    sdbcVistaDefectoAll.Text = sdbcVistaDefecto.Text
End If
If sdbcVistaActual.Text <> "" Then
    sdbcVistaActAll.Text = sdbcVistaActual.Text
End If
End Sub
Private Sub cmdRenombrarVistaAll_Click()
RenombrarVista sdbcVistaActAll, sdbcVistaDefectoAll
End Sub

Private Sub cmdRenombrarVistaGr_Click()
RenombrarVista sdbcVistaActGr, sdbcVistaDefectoGr
If sdbcVistaActGr.Text <> "" Then
    sdbcVistaActAll.Text = sdbcVistaActGr.Text
End If
If sdbcVistaDefectoGr.Text <> "" Then
    sdbcVistaDefectoAll.Text = sdbcVistaDefectoGr.Text
End If
End Sub
Private Sub cmdVista_Click()
    'Muestra la pantalla para ocultar o visualizar los campos
    If sstabComparativa.selectedItem.Index > 1 Then
        If sstabComparativa.selectedItem.Tag = "ALL" Then
            picControlVistaAll.Visible = True
        Else
            picControlVistaGrupo.Visible = True
        End If
    Else
        picControlVistaProceso.Visible = True
    End If
    
     'Hace que el bot�n pierda el foco
    If Me.Visible Then sstabComparativa.SetFocus
End Sub

Private Sub Form_Load()
Me.Height = 5685
Me.Width = 11385
If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If
CargarRecursos
LockWindowUpdate Me.hWnd
PonerFieldSeparator Me
''''ESTO LO QUITAREMOS DESPU�S Y PONDREMOS EL DETALLE DE LOS ATRIBUTOS
sdbgOcultarCampos.Columns("Pertenece").Width = sdbgOcultarCampos.Columns("Pertenece").Width + sdbgOcultarCampos.Columns("POND").Width
sdbgOcultarCamposAll.Columns("Pertenece").Width = sdbgOcultarCamposAll.Columns("Pertenece").Width + sdbgOcultarCamposAll.Columns("POND").Width
sdbgOcultarCamposGr.Columns("Pertenece").Width = sdbgOcultarCamposGr.Columns("Pertenece").Width + sdbgOcultarCamposGr.Columns("POND").Width
sdbgOcultarCampos.Columns("POND").Visible = False
sdbgOcultarCamposAll.Columns("POND").Visible = False
sdbgOcultarCamposGr.Columns("POND").Visible = False
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
ConfigurarPicControlVistaProceso  'Dependiendo de si es de admin pub o no se ver�n los sobres
lblTitulo.caption = g_oPlantillaSeleccionada.Fecha & "-" & g_oPlantillaSeleccionada.nombre
'Carga todos los atributos en la colecci�n
g_oPlantillaSeleccionada.CargarTodosLosAtributosVistas
CargarTabGeneral
'N�mero de columnas de los grupos 0 y 1 para las grid de grupos y ALL.se usar� para calclar
'las posiciones de las columnas.Lo hago aqu� para calcularlo solo una vez
m_iNumColsGR = sdbgAdj.Groups(0).Columns.Count + sdbgAdj.Groups(1).Columns.Count - 1
m_iNumColsAll = sdbgAll.Groups(0).Columns.Count + sdbgAll.Groups(1).Columns.Count - 1
LockWindowUpdate 0&
End Sub

''' <summary>Carga los textos en el idioma del usuario</summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo:0</remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
On Error Resume Next
Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CONFIG_VISTA, basPublic.gParametrosInstalacion.gIdioma)
If Not Ador Is Nothing Then
    With Ador(0)
        sdbgAdj.Groups(0).Columns("OBJ").caption = .Value '1 Objetivo
        sdbgAll.Groups(0).Columns("OBJ").caption = .Value '1 Objetivo
        m_sCap(1) = .Value '1 Objetivo
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("PORCENADJ").caption = .Value '2 Adj. %
        sdbgAll.Groups(0).Columns("PORCENADJ").caption = .Value '2 Adj. %
        m_sCap(2) = .Value '2 Adj. %
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("CANT").caption = .Value '3 Cantidad
        sdbgAll.Groups(0).Columns("CANT").caption = .Value '3 Cantidad
        m_sCap(3) = .Value
        Ador.MoveNext
        m_sCap(4) = .Value '4 Precio
        Ador.MoveNext
        m_sCap(9) = .Value '5 Cant.M�xima
        Ador.MoveNext
        m_sCap(10) = .Value     '6 Ahorro
        Ador.MoveNext
        sdbgGruposProve.Groups(0).Columns(0).caption = .Value  '7 C�digo
        sdbgProveGrupos.Groups(0).Columns(0).caption = .Value
        
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("PROV").caption = .Value   '8 Proveedor
        sdbgAll.Groups(0).Columns("PROV").caption = .Value
        m_sProvActual = .Value
        m_sProveedor = .Value
        
        Ador.MoveNext
        sdbgProveGrupos.Columns("DEN").caption = .Value '9 Denominaci�n
        
        Ador.MoveNext
        sdbgAdj.Groups(0).caption = .Value '10 (Pulse aqu�...
        sdbgAll.Groups(0).caption = .Value '10 (Pulse aqu�...
        Ador.MoveNext
        sdbgAdj.Groups(1).caption = .Value '1 (Pulse sobre la cabecera...
        sdbgAll.Groups(1).caption = .Value '11 (Pulse sobre la cabecera...
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("DESCR").caption = .Value '12 Descripci�n
        sdbgAll.Groups(0).Columns("DESCR").caption = .Value '12 Descripci�n
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("ANYO").caption = .Value '13 Anyo/Dest
        sdbgAll.Groups(0).Columns("ANYO").caption = .Value '13 Anyo/Dest
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("PRECAPE").caption = .Value '14 Pres.Unitario
        sdbgAll.Groups(0).Columns("PRECAPE").caption = .Value '14 Pres.Unitario
        
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("AHORROIMP").caption = .Value '15 Ahorro imp.
        sdbgAll.Groups(0).Columns("AHORROIMP").caption = .Value '15 Ahorro imp.
        Ador.MoveNext
        sdbgAdj.Groups(0).Columns("AHORROPORCEN").caption = .Value '16 Ahorro%
        sdbgAll.Groups(0).Columns("AHORROPORCEN").caption = .Value '16 Ahorro%

        Ador.MoveNext
        sdbgTotales.Columns("TOT").caption = .Value     '17 Total por proveedor
        sdbgTotalesAll.Columns("TOT").caption = .Value
        
        Ador.MoveNext
        m_sProceso = Ador("TOT").Value    '18 Proceso
        sdbgProce.Groups("Proceso").caption = .Value
        Ador.MoveNext
        
        m_sAdjudicado = .Value   '19 Adjudicado
        Ador.MoveNext
        sdbgProveGrupos.Groups(0).caption = .Value  '20 Proveedores
        Ador.MoveNext
        sCaptionTotales = .Value  '21 Total obj.
        sdbgTotales.Columns(1).caption = .Value
        Ador.MoveNext
        m_sVista = .Value   '22
        Ador.MoveNext
        m_sVista4 = .Value   '23
        Ador.MoveNext
        m_sVista1 = .Value  '24
        Ador.MoveNext
        m_sVista2 = .Value  '25
        Ador.MoveNext
        m_sVista3 = .Value  '26
        
        Ador.MoveNext
        cmdGuardarVista.caption = .Value  '27 Guardar Vista
        cmdGuardarVistaGr.caption = .Value  '27 Guardar Vista
        cmdGuardarVistaAll.caption = .Value  '27 Guardar Vista
        Ador.MoveNext
        sstabComparativa.Tabs(1).caption = .Value     '28 general
        m_sGeneral = .Value
        Ador.MoveNext
        sdbgGruposProve.Groups(0).caption = .Value   '29 grupos
        Ador.MoveNext
        sdbgGruposProve.Groups(0).Columns("Nombre").caption = .Value  '30 Nombre
        Ador.MoveNext
        m_sConsumido = .Value  '31 consumido
        Ador.MoveNext
        m_sAhorrado = .Value  '32 ahorrado
        Ador.MoveNext
        m_sAhorroPorce = .Value   '33 %Ahorro adj.
        Ador.MoveNext
        m_sImporteOferta = .Value  '34 Importe oferta
        Ador.MoveNext
        m_sAhorroOferta = .Value   '35 Ahorro oferta
        Ador.MoveNext
        m_sPorcenAhorroOferta = .Value  '36 %Ahorro oferta
        
        Ador.MoveNext
        m_sImpAdj = .Value  '37 Importe adj.
        sdbgAdj.Groups(0).Columns("IMP").caption = .Value
        sdbgAll.Groups(0).Columns("IMP").caption = .Value
        m_sImporte = .Value
        
        Ador.MoveNext
        cmdVista.ToolTipText = .Value  '38 Dise�o de vistas
        Ador.MoveNext
        cmdInvertir.ToolTipText = .Value  '39 Invertir grid

        Ador.MoveNext
        m_sCons = .Value  '40 Cons.
        Ador.MoveNext
        m_sAdj = .Value  '41  Adj.
        Ador.MoveNext
        m_sAhorr = .Value  '42  Ahorr.
        Ador.MoveNext
        m_sImp = .Value  '43 Imp.
        Ador.MoveNext
        m_sAhorrOf = .Value  '44 Ahorr.ofe
        Ador.MoveNext
        m_sTodos = .Value  '45  Para todos
        
        Ador.MoveNext
        Me.caption = .Value '46 Configuraci�n de vista
        Ador.MoveNext
        cmdRestaurar.caption = .Value '47 Restaurar
        Ador.MoveNext
        lblVista.caption = .Value '48 vista inicial
        
        Ador.MoveNext  '49 Todos
        m_sALL = .Value
        
        Ador.MoveNext
        m_sCap(5) = .Value     '50 Importe adj.(proveedor)
        Ador.MoveNext
        m_sCap(6) = .Value     '51 Item
        Ador.MoveNext
        m_sAnyo = .Value  '52 Anyo/Dest
        Ador.MoveNext
        m_sPresUnitario = .Value  '53 Pres.Unitario
        Ador.MoveNext
        m_sAhorroImp = .Value  '54 Ahorro imp.
        Ador.MoveNext
        m_sAhorroPorcen = .Value '55 Ahorro%
        
        Ador.MoveNext
        sdbgOcultarCampos.Columns("Pertenece").caption = .Value  '56 Pertenece
        sdbgOcultarCamposAll.Columns("Pertenece").caption = .Value
        sdbgOcultarCamposGr.Columns("Pertenece").caption = .Value
        Ador.MoveNext
        m_sLitGrupo = .Value  '57 Grupo
        Ador.MoveNext
        sdbgOcultarCampos.Columns("CAMPO").caption = .Value  '58 Campo
        sdbgOcultarCamposAll.Columns("CAMPO").caption = .Value  '58 Campo
        sdbgOcultarCamposGr.Columns("CAMPO").caption = .Value  '58 Campo
        Ador.MoveNext
        lblMostrarSobres.caption = .Value  '59 Mostrar contenido sobres
        Ador.MoveNext
        m_sSobre = .Value 'Sobre  '60 Sobre
        
        Ador.MoveNext
        m_sCap(7) = .Value  '61 Cantidad (proveedor)
        Ador.MoveNext
        m_sCap(8) = .Value  '62 Adj.% (proveedor)

        Ador.MoveNext
        lblMostrarCampos.caption = .Value  '63 Mostrar campos
        lblMostrarCamposAll.caption = .Value
        lblMostrarCamposGr.caption = .Value
        
        Ador.MoveNext
        sdbgOcultarCampos.Columns("POND").caption = .Value  '64 Det.
        sdbgOcultarCamposAll.Columns("POND").caption = .Value
        sdbgOcultarCamposGr.Columns("POND").caption = .Value
        Ador.MoveNext ' 65 Vista actual
        lblVista.caption = .Value
        lblPlantillaAll.caption = .Value
        lblPlantillaGR.caption = .Value
        Ador.MoveNext ' 66 vista defecto
        lblVistaDefecto.caption = .Value
        lblVistaDefectoAll.caption = .Value
        lblVistaDefectoGr.caption = .Value
        Ador.MoveNext ' 67 vista inicial
        m_sVistaIni = .Value
        Ador.MoveNext ' 68 vista de plantilla
        m_sVistaPlan = .Value
        Ador.MoveNext ' 69 ocultar grupos cerrados
        chkOcultarGrCerrados.caption = .Value
        Ador.MoveNext ' 70 no incluir grupo cerrado en los resultados
        chkExcluirGrCerradosResul.caption = .Value
        Ador.MoveNext ' 71 ocultar ofertas no adjudicables
        chkOcultarNoAdj.caption = .Value
        chkOcultarNoAdjAll.caption = .Value
        chkOcultarNoAdjGr.caption = .Value
        Ador.MoveNext ' 72 ocultar proveedores sin oferta
        chkOcultarProvSinOfe.caption = .Value
        chkOcultarProvSinOfeAll.caption = .Value
        chkOcultarProvSinOfeGr.caption = .Value
        Ador.MoveNext ' 73 Numeros decimales
        lblNumDec.caption = .Value
        lblNumDecAll.caption = .Value
        lblNumDecGr.caption = .Value
        Ador.MoveNext ' 74 Resultados
        lblNumDecResult.caption = .Value
        lblNumDecResultAll.caption = .Value
        lblNumDecResultGr.caption = .Value
        Ador.MoveNext ' 75 porcentaje
        lblNumDecPorcen.caption = .Value
        lblNumDecPorcenAll.caption = .Value
        lblNumDecPorcenGr.caption = .Value
        Ador.MoveNext ' 76 Precios
        lblNumDecPrecAll.caption = .Value
        lblNumDecPrecGr.caption = .Value
        Ador.MoveNext ' 77 Cantidades
        lblNumDecCantAll.caption = .Value
        lblNumDecCantGr.caption = .Value
        Ador.MoveNext ' 78 ocultar items cerrados
        chkOcultarItCerradosAll.caption = .Value
        chkOcultarItCerradosGr.caption = .Value
        Ador.MoveNext ' 79 No incluit items cerrados en los resultados
        chkExcluirItCerradosResul.caption = .Value
        chkExcluirItCerradosResulAll.caption = .Value
        Ador.MoveNext ' 80 Guardar en vista nueva
        cmdGuardarVistaNueva.caption = .Value
        cmdGuardarVistaNuevaAll.caption = .Value
        cmdGuardarVistaNuevaGr.caption = .Value
        m_sGVistaNueva = .Value
        Ador.MoveNext ' 81 Renombrar vista
        cmdRenombrarVista.caption = .Value
        cmdRenombrarVistaAll.caption = .Value
        cmdRenombrarVistaGr.caption = .Value
        m_sRenombrarVista = .Value
        Ador.MoveNext ' 82
        cmdEliminarVista.caption = .Value
        cmdEliminarVistaAll.caption = .Value
        cmdEliminarVistaGr.caption = .Value
        Ador.MoveNext ' 83
        sdbgAdj.Groups(0).Columns("VINCULADO_ITEM").caption = .Value
        sdbgAll.Groups(0).Columns("VINCULADO_ITEM").caption = .Value
        m_sCaptionSolicVinculada = .Value
        Ador.MoveNext
        m_sCaptionVistaSolicVinculada = .Value
        Ador.MoveNext
        m_sFecIniSum = .Value
        sdbgAdj.Groups(0).Columns("INI").caption = m_sFecIniSum
        sdbgAll.Groups(0).Columns("INI").caption = m_sFecIniSum
        Ador.MoveNext
        m_sFecFinSum = .Value
        sdbgAdj.Groups(0).Columns("FIN").caption = m_sFecFinSum
        sdbgAll.Groups(0).Columns("FIN").caption = m_sFecFinSum
        Ador.MoveNext
        m_sEstrMat = .Value
        sdbgAdj.Groups(0).Columns("GMN").caption = m_sEstrMat
        sdbgAll.Groups(0).Columns("GMN").caption = m_sEstrMat
        Ador.Close
    End With
End If
Set Ador = Nothing
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picControlVistaProceso.Visible = False
    picControlVistaAll.Visible = False
    picControlVistaGrupo.Visible = False
End Sub

Private Sub Form_Resize()
    'Redimensiona el formulario
    If Me.Width < 600 Then Exit Sub
    If Me.Height < 5000 Then Exit Sub
    
    'redimensiona el width del tab
    sstabComparativa.Width = Me.Width - 185

    'Redimensiona el width de las grids
    sdbgAdj.Width = sstabComparativa.Width - 200
    sdbgAll.Width = sstabComparativa.Width - 200
    sdbgProveGrupos.Width = sstabComparativa.Width - 200
    sdbgGruposProve.Width = sstabComparativa.Width - 200
    sdbgProce.Width = sstabComparativa.Width - 200
    sdbgTotales.Width = sdbgAdj.Width
    sdbgtotales2.Width = sdbgProveGrupos.Width
    sdbgTotalesProve.Width = sdbgGruposProve.Width
    sdbgTotalesAll.Width = sdbgAll.Width
    
    'Redimensiona el height de las grids y del tab seg�n est�n visibles o no los datos del proceso
    'y los atributos del proceso
    sstabComparativa.Height = Me.Height - 885
    sdbgAdj.Height = sstabComparativa.Height - sdbgTotales.Height - 520
    sdbgAll.Height = sstabComparativa.Height - sdbgTotalesAll.Height - 520
    
    If sdbgProce.Visible = True Or (Me.Visible = False And m_bGridProcVisible = True) Then
        sdbgGruposProve.Height = sstabComparativa.Height - sdbgProce.Height - sdbgTotalesProve.Height - 520
    Else
        sdbgGruposProve.Height = sstabComparativa.Height - sdbgTotalesProve.Height - 520
    End If

    sdbgProveGrupos.Height = sstabComparativa.Height - sdbgtotales2.Height - 520
    
    sdbgTotales.Top = sdbgAdj.Height + sdbgAdj.Top - 10
    sdbgtotales2.Top = sdbgProveGrupos.Height + sdbgProveGrupos.Top - 10
    sdbgTotalesProve.Top = sdbgGruposProve.Height + sdbgGruposProve.Top - 10
    sdbgTotalesAll.Top = sdbgAll.Height + sdbgAll.Top - 10
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim oGrupo As CGrupo

    For Each oGrupo In g_oPlantillaSeleccionada.Grupos
        Set oGrupo.PlantConfVistas = Nothing
        oGrupo.VistaDefectoPlant = Null
    Next
    Set g_oPlantillaSeleccionada.Atributosvistas = Nothing
    Set g_oPlantillaSeleccionada = Nothing
    Set m_oConfVistasProce = Nothing
    Set m_oConfVistasAll = Nothing
    Set m_oVistaSeleccionada = Nothing
    Set m_oVistaSeleccionadaGr = Nothing
    Set m_oVistaSeleccionadaAll = Nothing
    Set m_oVistasInicial = Nothing
    Set m_oVistasInicialGR = Nothing
    Set m_oVistasInicialALL = Nothing
    Set m_oGrupoSeleccionado = Nothing
    Me.Visible = False
End Sub

Private Sub CargarTab()
Dim i As Integer
Dim oGrupo As CGrupo
'Relleno los tabs con los grupos de la plantilla
If Not g_oPlantillaSeleccionada.Grupos Is Nothing Then
    If g_oPlantillaSeleccionada.Grupos.Count > 0 Then
        If g_oPlantillaSeleccionada.AdminPub = True Then 'Es un proceso de admin.pub
            CargarSobresEnTab
        Else
            'Muestra los grupos en el tab
            i = 2
            For Each oGrupo In g_oPlantillaSeleccionada.Grupos
                sstabComparativa.Tabs.Add i, "A" & oGrupo.Codigo, oGrupo.Codigo & " - " & oGrupo.Den
                sstabComparativa.Tabs(i).Tag = oGrupo.Codigo
                i = i + 1
            Next
            
            'Ahora a�ade el tab para todos los items (ALL)
            If g_oPlantillaSeleccionada.Grupos.Count > 1 Then
                sstabComparativa.Tabs.Add i, "ALL", m_sALL
                sstabComparativa.Tabs(i).Tag = "ALL"
            End If
        End If
    End If
End If
End Sub

Private Sub sdbcVistaActAll_CloseUp()
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
If CInt(sdbcVistaActAll.Columns("COD").Value) = CInt(m_oVistaSeleccionada.Vista) Then Exit Sub
Screen.MousePointer = vbHourglass
ConfiguracionVistaActualAll sdbcVistaActAll.Columns("COD").Value, False
LockWindowUpdate Me.hWnd
'Carga en las grids la configuraci�n de la vista actual
CargarVistasAll
'Redimensiona la grid seg�n la configuraci�n seleccionada
RedimensionarGridAll
LockWindowUpdate 0&
ConfigurarBotonesVistas sdbcVistaActAll.Columns("COD").Value
Select Case m_oVistaSeleccionada.Vista
    Case TipoDeVistaDefecto.vistainicial
        ' pongo el nombre en el caption del grid
        sdbgProce.caption = m_sVistaIni
        sdbgAll.caption = m_sVistaIni
        sdbgGruposProve.caption = m_sVistaIni
        sdbgProveGrupos.caption = m_sVistaIni
        sdbgAdj.caption = m_sVistaIni
    Case Else
        ' pongo el nombre en el caption del grid
        sdbgProce.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgAll.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgGruposProve.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgProveGrupos.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgAdj.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
End Select
Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcVistaActAll_DropDown()
Dim oVista As CPlantConfVistaProce
sdbcVistaActAll.RemoveAll
Set oVistasCombo = Nothing
Set oVistasCombo = oFSGSRaiz.Generar_CPlantConfVistasProce
oVistasCombo.CargarCombosVistas g_oPlantillaSeleccionada, oUsuarioSummit.Cod
If oVistasCombo.Count <> 0 Then
    For Each oVista In oVistasCombo
        Select Case oVista.Vista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaActAll.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
            Case Else
                sdbcVistaActAll.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
        End Select
    Next
Else
    sdbcVistaActAll.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial
End If
sdbcVistaActAll.SelStart = 0
sdbcVistaActAll.SelLength = Len(sdbcVistaActAll.Text)
sdbcVistaActAll.Refresh
Set oVista = Nothing
End Sub

Private Sub sdbcVistaActAll_InitColumnProps()
sdbcVistaActAll.DataFieldList = "Column 0"
sdbcVistaActAll.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcVistaActAll_PositionList(ByVal Text As String)
PositionList sdbcVistaActAll, Text
End Sub

Private Sub sdbcVistaActGr_CloseUp()
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
If CInt(sdbcVistaActGr.Columns("COD").Value) = CInt(m_oVistaSeleccionada.Vista) Then Exit Sub
Screen.MousePointer = vbHourglass
ConfiguracionVistaActualGr sdbcVistaActGr.Columns("COD").Value, False
'Carga en las grids la configuraci�n de la vista actual
CargarVistasGrupo
LockWindowUpdate Me.hWnd
'Redimensiona la grid seg�n la configuraci�n seleccionada
RedimensionarGridGrupo
LockWindowUpdate 0&
ConfigurarBotonesVistas sdbcVistaActGr.Columns("COD").Value
Select Case m_oVistaSeleccionada.Vista
    Case TipoDeVistaDefecto.vistainicial
        ' pongo el nombre en el caption del grid
        sdbgProce.caption = m_sVistaIni
        sdbgAll.caption = m_sVistaIni
        sdbgGruposProve.caption = m_sVistaIni
        sdbgProveGrupos.caption = m_sVistaIni
        sdbgAdj.caption = m_sVistaIni
    Case Else
        ' pongo el nombre en el caption del grid
        sdbgProce.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgAll.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgGruposProve.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgProveGrupos.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgAdj.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
End Select
Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcVistaActGr_DropDown()
Dim oVista As CPlantConfVistaProce
sdbcVistaActGr.RemoveAll
Set oVistasCombo = Nothing
Set oVistasCombo = oFSGSRaiz.Generar_CPlantConfVistasProce
oVistasCombo.CargarCombosVistas g_oPlantillaSeleccionada, oUsuarioSummit.Cod
If oVistasCombo.Count <> 0 Then
    For Each oVista In oVistasCombo
        Select Case oVista.Vista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaActGr.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
            Case Else
                sdbcVistaActGr.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
        End Select
    Next
Else
    sdbcVistaActGr.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial
End If
sdbcVistaActGr.SelStart = 0
sdbcVistaActGr.SelLength = Len(sdbcVistaActGr.Text)
sdbcVistaActGr.Refresh
Set oVista = Nothing
End Sub

Private Sub sdbcVistaActGr_InitColumnProps()
    sdbcVistaActGr.DataFieldList = "Column 0"
    sdbcVistaActGr.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcVistaActGr_PositionList(ByVal Text As String)
PositionList sdbcVistaActGr, Text
End Sub

Private Sub sdbcVistaActual_CloseUp()
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
If CInt(sdbcVistaActual.Columns("COD").Value) = CInt(m_oVistaSeleccionada.Vista) Then Exit Sub
Screen.MousePointer = vbHourglass
ConfiguracionVistaActual sdbcVistaActual.Columns("COD").Value, False
LockWindowUpdate Me.hWnd
'Carga en las grids la configuraci�n de la vista actual
CargarVistasProc
'Redimensiona la grid seg�n la configuraci�n seleccionada
RedimensionarGrid
LockWindowUpdate 0&
ConfigurarBotonesVistas sdbcVistaActual.Columns("COD").Value
Select Case m_oVistaSeleccionada.Vista
    Case TipoDeVistaDefecto.vistainicial
        If m_bGridProcVisible Then
            sdbgProce.caption = m_sVistaIni
            sdbgGruposProve.caption = ""
        Else
            sdbgGruposProve.caption = m_sVistaIni
            sdbgProce.caption = ""
        End If
        sdbgAll.caption = m_sVistaIni
        sdbgProveGrupos.caption = m_sVistaIni
        sdbgAdj.caption = m_sVistaIni
    Case Else
        If m_bGridProcVisible Then
            sdbgProce.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
            sdbgGruposProve.caption = ""
        Else
            sdbgGruposProve.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
            sdbgProce.caption = ""
        End If
        sdbgAll.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgProveGrupos.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
        sdbgAdj.caption = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
End Select
Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcVistaActual_DropDown()
Dim oVista As CPlantConfVistaProce
sdbcVistaActual.RemoveAll
Set oVistasCombo = Nothing
Set oVistasCombo = oFSGSRaiz.Generar_CPlantConfVistasProce
oVistasCombo.CargarCombosVistas g_oPlantillaSeleccionada, oUsuarioSummit.Cod
If oVistasCombo.Count <> 0 Then
    For Each oVista In oVistasCombo
        Select Case oVista.Vista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaActual.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
            Case Else
                sdbcVistaActual.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
        End Select
    Next
Else
    sdbcVistaActual.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial
End If
sdbcVistaActual.SelStart = 0
sdbcVistaActual.SelLength = Len(sdbcVistaActual.Text)
sdbcVistaActual.Refresh
Set oVista = Nothing
End Sub

Private Sub sdbcVistaActual_InitColumnProps()
sdbcVistaActual.DataFieldList = "Column 0"
sdbcVistaActual.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcVistaActual_PositionList(ByVal Text As String)
PositionList sdbcVistaActual, Text
End Sub

Private Sub sdbcVistaDefecto_CloseUp()
Dim teserror As TipoErrorSummit
'Pone por defecto la vista que se ha seleccionado
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
If CInt(sdbcVistaDefecto.Columns("COD").Value) = CInt(g_oPlantillaSeleccionada.VistaDefectoComp) Then Exit Sub
teserror = g_oPlantillaSeleccionada.ModificarVistaDefectoPlantilla(sdbcVistaDefecto.Columns("COD").Value)
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Exit Sub
End If
g_oPlantillaSeleccionada.NombreVistaDefecto = sdbcVistaDefecto.Text
End Sub
Private Sub sdbcVistaDefecto_DropDown()
Dim oVista As CPlantConfVistaProce
sdbcVistaDefecto.RemoveAll
Set oVistasCombo = Nothing
Set oVistasCombo = oFSGSRaiz.Generar_CPlantConfVistasProce
oVistasCombo.CargarCombosVistas g_oPlantillaSeleccionada, oUsuarioSummit.Cod
If oVistasCombo.Count <> 0 Then
    For Each oVista In oVistasCombo
        Select Case oVista.Vista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaDefecto.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
            Case Else
                sdbcVistaDefecto.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
        End Select
    Next
Else
    sdbcVistaDefecto.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial
End If
sdbcVistaDefecto.SelStart = 0
sdbcVistaDefecto.SelLength = Len(sdbcVistaDefecto.Text)
sdbcVistaDefecto.Refresh
Set oVista = Nothing
End Sub

Private Sub sdbcVistaDefecto_InitColumnProps()
sdbcVistaDefecto.DataFieldList = "Column 0"
sdbcVistaDefecto.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcVistaDefecto_PositionList(ByVal Text As String)
PositionList sdbcVistaDefecto, Text
End Sub

Private Sub sdbcVistaDefectoAll_CloseUp()
Dim teserror As TipoErrorSummit
'Pone por defecto la vista que se ha seleccionado
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
If CInt(sdbcVistaDefectoAll.Columns("COD").Value) = CInt(g_oPlantillaSeleccionada.VistaDefectoComp) Then Exit Sub
teserror = g_oPlantillaSeleccionada.ModificarVistaDefectoPlantilla(sdbcVistaDefectoAll.Columns("COD").Value)
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Exit Sub
End If
g_oPlantillaSeleccionada.NombreVistaDefecto = sdbcVistaDefectoAll.Text
End Sub

Private Sub sdbcVistaDefectoAll_DropDown()
Dim oVista As CPlantConfVistaProce
sdbcVistaDefectoAll.RemoveAll
Set oVistasCombo = Nothing
Set oVistasCombo = oFSGSRaiz.Generar_CPlantConfVistasProce
oVistasCombo.CargarCombosVistas g_oPlantillaSeleccionada, oUsuarioSummit.Cod
If oVistasCombo.Count <> 0 Then
    For Each oVista In oVistasCombo
        Select Case oVista.Vista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaDefectoAll.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
            Case Else
                sdbcVistaDefectoAll.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
        End Select
    Next
Else
    sdbcVistaDefectoAll.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial
End If
sdbcVistaDefectoAll.SelStart = 0
sdbcVistaDefectoAll.SelLength = Len(sdbcVistaDefectoAll.Text)
sdbcVistaDefectoAll.Refresh
Set oVista = Nothing
End Sub

Private Sub sdbcVistaDefectoAll_InitColumnProps()
sdbcVistaDefectoAll.DataFieldList = "Column 0"
sdbcVistaDefectoAll.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcVistaDefectoAll_PositionList(ByVal Text As String)
PositionList sdbcVistaDefectoAll, Text
End Sub

Private Sub sdbcVistaDefectoGr_CloseUp()
Dim teserror As TipoErrorSummit
'Pone por defecto la vista que se ha seleccionado
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
If CInt(sdbcVistaDefectoGr.Columns("COD").Value) = CInt(g_oPlantillaSeleccionada.VistaDefectoComp) Then Exit Sub
teserror = g_oPlantillaSeleccionada.ModificarVistaDefectoPlantilla(sdbcVistaDefectoGr.Columns("COD").Value)
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Exit Sub
End If
g_oPlantillaSeleccionada.NombreVistaDefecto = sdbcVistaDefectoGr.Text
End Sub

Private Sub sdbcVistaDefectoGr_DropDown()
Dim oVista As CPlantConfVistaProce
sdbcVistaDefectoGr.RemoveAll
Set oVistasCombo = Nothing
Set oVistasCombo = oFSGSRaiz.Generar_CPlantConfVistasProce
oVistasCombo.CargarCombosVistas g_oPlantillaSeleccionada, oUsuarioSummit.Cod
If oVistasCombo.Count <> 0 Then
    For Each oVista In oVistasCombo
        Select Case oVista.Vista
            Case TipoDeVistaDefecto.vistainicial
                sdbcVistaDefectoGr.AddItem m_sVistaIni & Chr(m_lSeparador) & oVista.Vista
            Case Else
                sdbcVistaDefectoGr.AddItem oVista.NombreVista & Chr(m_lSeparador) & oVista.Vista
        End Select
    Next
Else
    sdbcVistaDefectoGr.AddItem m_sVistaIni & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial & Chr(m_lSeparador) & TipoDeVistaDefecto.vistainicial
End If
sdbcVistaDefectoGr.SelStart = 0
sdbcVistaDefectoGr.SelLength = Len(sdbcVistaDefectoGr.Text)
sdbcVistaDefectoGr.Refresh
Set oVista = Nothing
End Sub

Private Sub sdbcVistaDefectoGr_InitColumnProps()
sdbcVistaDefectoGr.DataFieldList = "Column 0"
sdbcVistaDefectoGr.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcVistaDefectoGr_PositionList(ByVal Text As String)
PositionList sdbcVistaDefectoGr, Text
End Sub

''' <summary>
''' Cambia de posici�n las columnas en la grid (las correspondientes a los proveedores)
''' </summary>
''' <param name="WhatChanged">que cambia</param>
''' <param name="NewIndex">nuevo indice</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAdj_AfterPosChanged(ByVal WhatChanged As Integer, ByVal NewIndex As Integer)
Dim iNumColums As Integer
Dim iPos As Integer
Dim iOrden As Integer
Dim i As Integer
Dim sColumna As String
Dim iNumColumnas As Integer
Dim sProv As String
Dim iNumColsGrupo As Integer
Dim iGrupoVisible As Integer
Dim oAtribVista As CPlantConfVistaGrupoAtrib
'****** Cambia de posici�n las columnas en la grid (las correspondientes a los proveedores) *****
With sdbgAdj
    'Si se ha hecho un scroll lo deja todo en la posici�n original.
    If .GrpPosition(0) > 0 Then
        i = .Groups(0).Position
        .Scroll -i, 0
        .Update
        sdbgTotales.Scroll -i, 0
        sdbgTotales.Update
    End If
    For i = 0 To .Groups.Count - 1
        .Groups(i).Position = i
    Next i
    For i = 0 To sdbgTotales.Columns.Count - 1
        sdbgTotales.Columns(i).Position = i
    Next i
    iNumColums = .Groups(2).Columns.Count
    'Si se mueve una columna de los 2 primeros grupos y est� oculta la 2� fila de la grid almacena en la colecci�n de la vista las nuevas
    'posiciones. Si no est� oculta no hace nada
    If .Columns(NewIndex).Group <= 1 Then
        If .LevelCount = 1 Then
            If .Columns("ANYO").Visible Then m_oVistaSeleccionadaGr.AnyoPos = .Columns("ANYO").Position - (.Groups(0).Position * iNumColums)
            If .Columns("PROV").Visible Then m_oVistaSeleccionadaGr.ProveedorPos = .Columns("PROV").Position - (.Groups(0).Position * iNumColums)
            If .Columns("PRECAPE").Visible Then m_oVistaSeleccionadaGr.PresUniPos = .Columns("PRECAPE").Position - (.Groups(0).Position * iNumColums)
            If .Columns("OBJ").Visible Then m_oVistaSeleccionadaGr.OBJPos = .Columns("OBJ").Position - (.Groups(0).Position * iNumColums)
            If .Columns("AHORROIMP").Visible Then m_oVistaSeleccionadaGr.AhorroPos = .Columns("AHORROIMP").Position - (.Groups(0).Position * iNumColums)
            If .Columns("AHORROPORCEN").Visible Then m_oVistaSeleccionadaGr.AhorroPorcenPos = .Columns("AHORROPORCEN").Position - (.Groups(0).Position * iNumColums)
            If .Columns("IMP").Visible Then m_oVistaSeleccionadaGr.ImportePos = .Columns("IMP").Position - (.Groups(0).Position * iNumColums)
            If .Columns("CANT").Visible Then m_oVistaSeleccionadaGr.CantidadPos = .Columns("CANT").Position - (.Groups(0).Position * iNumColums)
            If .Columns("PORCENADJ").Visible Then m_oVistaSeleccionadaGr.AdjPos = .Columns("PORCENADJ").Position - (.Groups(0).Position * iNumColums)
            If .Columns("VINCULADO_ITEM").Visible Then m_oVistaSeleccionadaGr.SolicVinculadaPos = .Columns("VINCULADO_ITEM").Position - (.Groups(0).Position * iNumColums)
            If .Columns("INI").Visible Then m_oVistaSeleccionadaGr.FecIniSumPos = .Columns("INI").Position - (.Groups(0).Position * iNumColums)
            If .Columns("FIN").Visible Then m_oVistaSeleccionadaGr.FecFinSumPos = .Columns("FIN").Position - (.Groups(0).Position * iNumColums)
            If .Columns("GMN").Visible Then m_oVistaSeleccionadaGr.EstrMatPos = .Columns("GMN").Position - (.Groups(0).Position * iNumColums)
        End If
        Exit Sub
    Else
        'Cuando se mueve una columna de un proveedor a otra posici�n se mueve en todos los grupos:
        'Obtiene el nombre de la columna que se va a mover
        sProv = .Groups(.Columns(NewIndex).Group).TagVariant
        sColumna = Mid(.Columns(NewIndex).Name, 1, InStr(.Columns(NewIndex).Name, sProv) - 1)
        'Redimensiona tambi�n la columna en el resto de grupos de la grid
        iNumColums = .Groups(2).Columns.Count
        iPos = .Columns(NewIndex).Position
        iNumColumnas = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
        iOrden = iPos - (iNumColumnas + (.Columns(NewIndex).Group - 2) * iNumColums)
        For i = 2 To .Groups.Count - 1
            If sProv <> .Groups(i).TagVariant Then
                .Columns(sColumna & .Groups(i).TagVariant).Position = iNumColumnas + iOrden + (iNumColums * (i - 2))
                .Columns(sColumna & .Groups(i).TagVariant).Level = .Columns(sColumna & sProv).Level
            End If
        Next i
        If .LevelCount = 1 Then
            'Obtiene cual es el 1� proveedor que est� visible
            For iGrupoVisible = 1 To .Groups.Count - 1
                If .Groups(iGrupoVisible).Visible = True Then
                    Exit For
                End If
            Next iGrupoVisible
            If iGrupoVisible = .Groups.Count Then Exit Sub
            iNumColsGrupo = .Groups(iGrupoVisible).Columns.Count
            If IsNull(m_oVistaSeleccionadaGr.ImpAdjPos) Then
                m_oVistaSeleccionadaGr.ImpAdjPos = 1
            ElseIf .Columns("IMPADJ" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaGr.ImpAdjPos = .Columns("IMPADJ" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            If IsNull(m_oVistaSeleccionadaGr.CantProvPos) Then
                m_oVistaSeleccionadaGr.CantProvPos = 8
            ElseIf .Columns("AdjCantidad" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaGr.CantProvPos = .Columns("AdjCantidad" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            If IsNull(m_oVistaSeleccionadaGr.AdjProvPos) Then
                m_oVistaSeleccionadaGr.AdjProvPos = 9
            ElseIf .Columns("AdjPorcen" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaGr.AdjProvPos = .Columns("AdjPorcen" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            If IsNull(m_oVistaSeleccionadaGr.PrecioProvPos) Then
                m_oVistaSeleccionadaGr.PrecioProvPos = 2
            ElseIf .Columns("Precio" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaGr.PrecioProvPos = .Columns("Precio" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            'almacena los pos de los atributos
            For Each oAtribVista In m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib
                If IsNull(oAtribVista.Posicion) Then
                    oAtribVista.Posicion = 1
                ElseIf .Columns(oAtribVista.Atributo & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                    oAtribVista.Posicion = .Columns(oAtribVista.Atributo & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
                End If
            Next
            OrdenarColsGrupoProves True
        End If
    End If
End With
End Sub

Private Sub sdbgAdj_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
'Si el scroll no est� en la posici�n 0 no permite mover las columnas
If sdbgAdj.GrpPosition(0) <> 0 Then
    Cancel = True
    Exit Sub
End If
If ColIndex = 0 Then Cancel = True
If NewPos = 0 Then Cancel = True
If m_oVistaSeleccionadaGr Is Nothing Then
    Cancel = True
    Exit Sub
End If
m_oVistaSeleccionadaGr.HayCambios = True
End Sub

Private Sub sdbgAdj_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
Dim i As Integer
Dim sProv As String
Dim sColum As String
If m_oVistaSeleccionadaGr Is Nothing Then Exit Sub
'Indica que ha habido cambios en la vista
m_oVistaSeleccionadaGr.HayCambios = True
'Obtenemos el proveedor cuya columna se est� cambiando de tama�o.Hay que tener en cuenta el scroll:
If sdbgAdj.Groups(sdbgAdj.Columns(ColIndex).Group).Position <> sdbgAdj.Columns(ColIndex).Group Then
    sProv = sdbgAdj.Groups(sdbgAdj.GrpPosition(sdbgAdj.Columns(ColIndex).Group)).TagVariant
Else
    sProv = sdbgAdj.Groups(sdbgAdj.Columns(ColIndex).Group).TagVariant
End If
If sdbgAdj.Columns(ColIndex).Group <> sdbgAdj.Groups(0).Position And sProv <> "" Then
    'Si se modifica una columna de uno de los grupos de proveedores
    'se hace el resize tambi�n de la columna en el resto de grupos de la grid
    sColum = Mid(sdbgAdj.Columns(ColIndex).Name, 1, Len(sdbgAdj.Columns(ColIndex).Name) - Len(sProv))
    For i = 2 To sdbgAdj.Groups.Count - 1
        If sdbgAdj.Groups(i).TagVariant <> sProv Then
            sdbgAdj.Columns(sColum & sdbgAdj.Groups(i).TagVariant).Width = sdbgAdj.ResizeWidth
        End If
    Next i
Else   'es una columna de los datos del art�culo
    'Si se modifica el tamanyo de la columna de objetivos se redimensiona la
    'de totales de objetivos para que las 2 tengan el mismo width
    If sdbgAdj.Columns(ColIndex).Name <> "OBJ" Then Exit Sub
    If sdbgTotales.Columns("OBJTOT").Visible = True Then
        If sdbgAdj.ResizeWidth > 1000 Then
            sdbgTotales.Columns("OBJTOT").Width = sdbgAdj.ResizeWidth
            sdbgTotales.Columns(0).Width = sdbgAdj.Groups(0).Width - sdbgAdj.ResizeWidth
        Else
            sdbgTotales.Columns("OBJTOT").Width = 1000
            sdbgTotales.Columns(0).Width = sdbgAdj.Groups(0).Width - 1000
        End If
    Else
        sdbgTotales.Columns(0).Width = sdbgAdj.Groups(0).Width
    End If
End If
End Sub

''' <summary>
''' Evento en la cabecera del grid de adjudicacion de un grupo q no sea ni General ni All
''' </summary>
''' <param name="GrpIndex">Indice del grupo</param>
''' <remarks>Llamada desde: Evento que salta al hacer click en la cabecera del grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgAdj_GrpHeadClick(ByVal GrpIndex As Integer)
Dim i As Integer
Dim intPos As Variant
Dim j As Integer
Dim sGrupo As String
Dim iOrden As Integer
Dim inumColumns As Integer
Dim oAtribOrden As CAtributo
Dim vValores(17) As Variant
With sdbgAdj
    If GrpIndex < 1 Then
        If .Rows = 0 Then Exit Sub
        If m_bCargandoVista = False Then m_oVistaSeleccionadaGr.HayCambios = True
        intPos = .Bookmark
        .Update
        .Col = 0
        If Me.Enabled And Me.Visible Then sdbgTotales.SetFocus
        .Redraw = True
        If .LevelCount = 3 Then
            'Oculta la 2� fila de la grid:
            Screen.MousePointer = vbHourglass
            LockWindowUpdate Me.hWnd
            If m_bCargandoVista = False Then GuardarConfiguracionGridGrupo
            m_oVistaSeleccionadaGr.OcultarFila2 = True
            'Todos los campos de la grid que est�n en la 2� fila se hacen invisibles
            For i = 0 To .Columns.Count - 1
                If .Columns(i).Level = 1 Then
                    .Columns(i).Visible = False
                End If
            Next i
            vValores(0) = .Groups(0).Columns("DESCR").Value
            vValores(1) = .Groups(0).Columns("ANYO").Value
            vValores(2) = .Groups(0).Columns("ID").Value
            vValores(3) = .Groups(0).Columns("PROV").Value
            vValores(4) = .Groups(0).Columns("PRECAPE").Value
            vValores(5) = .Groups(0).Columns("ADJ").Value
            vValores(6) = .Groups(0).Columns("ART").Value
            vValores(7) = .Groups(0).Columns("OBJ").Value
            vValores(8) = .Groups(0).Columns("AHORROIMP").Value
            vValores(9) = .Groups(0).Columns("AHORROPORCEN").Value
            vValores(10) = .Groups(0).Columns("IMP").Value
            vValores(11) = .Groups(0).Columns("CANT").Value
            vValores(12) = .Groups(0).Columns("PORCENADJ").Value
            vValores(13) = .Groups(0).Columns("VINCULADO_ITEM").Value
            vValores(14) = .Groups(0).Columns("INI").Value
            vValores(15) = .Groups(0).Columns("FIN").Value
            vValores(16) = .Groups(0).Columns("GMN").Value
            .LevelCount = 1
            .Groups(0).Columns("DESCR").Value = vValores(0)
            .Groups(0).Columns("ANYO").Value = vValores(1)
            .Groups(0).Columns("ID").Value = vValores(2)
            .Groups(0).Columns("PROV").Value = vValores(3)
            .Groups(0).Columns("PRECAPE").Value = vValores(4)
            .Groups(0).Columns("ADJ").Value = vValores(5)
            .Groups(0).Columns("ART").Value = vValores(6)
            .Groups(0).Columns("OBJ").Value = vValores(7)
            .Groups(0).Columns("AHORROIMP").Value = vValores(8)
            .Groups(0).Columns("AHORROPORCEN").Value = vValores(9)
            .Groups(0).Columns("IMP").Value = vValores(10)
            .Groups(0).Columns("CANT").Value = vValores(11)
            .Groups(0).Columns("PORCENADJ").Value = vValores(12)
            .Groups(0).Columns("VINCULADO_ITEM").Value = vValores(13)
            .Groups(0).Columns("INI").Value = vValores(14)
            .Groups(0).Columns("FIN").Value = vValores(15)
            .Groups(0).Columns("GMN").Value = vValores(16)
            'ahora vuelve a poner las posiciones y width a todos los campos,ya que cuando oculta la 2� fila intercala las columnas de estas filas
            'dentro de las de la primera.
            If m_bCargandoVista = False Then OrdenarColsGrupo0 (True)
            .Groups(0).Columns("DESCR").Position = 0
            For i = 0 To 12
                Select Case m_arrOrden(i)
                    Case "ANYO"
                        .Groups(0).Columns("ANYO").Position = m_oVistaSeleccionadaGr.AnyoPos
                    Case "PROV"
                        .Groups(0).Columns("PROV").Position = m_oVistaSeleccionadaGr.ProveedorPos
                    Case "PRECAPE"
                        .Groups(0).Columns("PRECAPE").Position = m_oVistaSeleccionadaGr.PresUniPos
                    Case "OBJ"
                        .Groups(0).Columns("OBJ").Position = m_oVistaSeleccionadaGr.OBJPos
                    Case "AHORROIMP"
                        .Groups(0).Columns("AHORROIMP").Position = m_oVistaSeleccionadaGr.AhorroPos
                    Case "AHORROPORCEN"
                        .Groups(0).Columns("AHORROPORCEN").Position = m_oVistaSeleccionadaGr.AhorroPorcenPos
                    Case "IMP"
                        .Groups(0).Columns("IMP").Position = m_oVistaSeleccionadaGr.ImportePos
                    Case "CANT"
                        .Groups(0).Columns("CANT").Position = m_oVistaSeleccionadaGr.CantidadPos
                    Case "PORCENADJ"
                        .Groups(0).Columns("PORCENADJ").Position = m_oVistaSeleccionadaGr.AdjPos
                    Case "VINCULADO_ITEM"
                        .Groups(0).Columns("VINCULADO_ITEM").Position = m_oVistaSeleccionadaGr.SolicVinculadaPos
                    Case "INI"
                        .Groups(0).Columns("INI").Position = m_oVistaSeleccionadaGr.FecIniSumPos
                    Case "FIN"
                        .Groups(0).Columns("FIN").Position = m_oVistaSeleccionadaGr.FecFinSumPos
                    Case "GMN"
                        .Groups(0).Columns("GMN").Position = m_oVistaSeleccionadaGr.EstrMatPos
                End Select
            Next i
            .Groups(0).Columns("DESCR").Width = m_oVistaSeleccionadaGr.DescrWidth
            For i = 0 To 12
                Select Case m_arrOrden(i)
                    Case "ANYO"
                        .Groups(0).Columns("ANYO").Width = m_oVistaSeleccionadaGr.AnyoWidth
                    Case "PROV"
                        .Groups(0).Columns("PROV").Width = m_oVistaSeleccionadaGr.ProveedorWidth
                    Case "PRECAPE"
                        .Groups(0).Columns("PRECAPE").Width = m_oVistaSeleccionadaGr.PresUniWidth
                    Case "OBJ"
                        .Groups(0).Columns("OBJ").Width = m_oVistaSeleccionadaGr.OBJWidth
                    Case "AHORROIMP"
                        .Groups(0).Columns("AHORROIMP").Width = m_oVistaSeleccionadaGr.AhorroWidth
                    Case "AHORROPORCEN"
                        .Groups(0).Columns("AHORROPORCEN").Width = m_oVistaSeleccionadaGr.AhorroPorcenWidth
                    Case "IMP"
                        .Groups(0).Columns("IMP").Width = m_oVistaSeleccionadaGr.ImporteWidth
                    Case "CANT"
                        .Groups(0).Columns("CANT").Width = m_oVistaSeleccionadaGr.CantidadWidth
                    Case "PORCENADJ"
                        .Groups(0).Columns("PORCENADJ").Width = m_oVistaSeleccionadaGr.AdjWidth
                    Case "VINCULADO_ITEM"
                        .Groups(0).Columns("VINCULADO_ITEM").Width = m_oVistaSeleccionadaGr.SolicVinculadaWidth
                    Case "INI"
                        .Groups(0).Columns("INI").Width = m_oVistaSeleccionadaGr.FecIniSumWidth
                    Case "FIN"
                        .Groups(0).Columns("FIN").Width = m_oVistaSeleccionadaGr.FecFinSumWidth
                    Case "GMN"
                        .Groups(0).Columns("GMN").Width = m_oVistaSeleccionadaGr.EstrMatWidth
                End Select
            Next i
            If m_bCargandoVista = False Then OrdenarColsGrupoProves (True)                 'Ordena las columnas de los grupos de proveedores
            'Pone las posiciones a los grupos de los proveedores
            For i = 2 To .Groups.Count - 1
                sGrupo = .Groups(i).TagVariant
                ''''Posici�n de los campos:
                iOrden = m_iNumColsGR + (.Groups(i).Columns.Count * (i - 2))
                inumColumns = 3
                If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
                    For Each oAtribOrden In g_oPlantillaSeleccionada.Atributosvistas
                        If oAtribOrden.ambito = AmbItem And (oAtribOrden.codgrupo = m_oGrupoSeleccionado.Codigo Or IsNull(oAtribOrden.codgrupo)) Then inumColumns = inumColumns + 1
                    Next
                End If
                For j = 0 To inumColumns
                    Select Case m_arrOrdenColsGr(j)
                        Case "IMPADJ"
                            .Columns("IMPADJ" & sGrupo).Position = m_oVistaSeleccionadaGr.ImpAdjPos + iOrden
                        Case "PRECIO"
                            .Columns("Precio" & sGrupo).Position = m_oVistaSeleccionadaGr.PrecioProvPos + iOrden
                        Case "ADJPROV"
                            .Columns("AdjPorcen" & sGrupo).Position = m_oVistaSeleccionadaGr.AdjProvPos + iOrden
                        Case "CANTPROV"
                            .Columns("AdjCantidad" & sGrupo).Position = m_oVistaSeleccionadaGr.CantProvPos + iOrden
                        Case Else
                            'Es un atributo
                            .Columns(m_arrOrdenColsGr(j) & sGrupo).Position = m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(m_arrOrdenColsGr(j))).Posicion + iOrden
                    End Select
                Next j
                'ahora pone los widths
                For j = 0 To inumColumns
                    Select Case m_arrOrdenColsGr(j)
                        Case "IMPADJ"
                            .Columns("IMPADJ" & sGrupo).Width = m_oVistaSeleccionadaGr.ImpAdjWidth
                        Case "PRECIO"
                            .Columns("Precio" & sGrupo).Width = m_oVistaSeleccionadaGr.PrecioProvWidth
                        Case "ADJPROV"
                            .Columns("AdjPorcen" & sGrupo).Width = m_oVistaSeleccionadaGr.AdjProvWidth
                        Case "CANTPROV"
                            .Columns("AdjCantidad" & sGrupo).Width = m_oVistaSeleccionadaGr.CantProvWidth
                        Case Else
                            'Es un atributo
                            .Columns(m_arrOrdenColsGr(j) & sGrupo).Width = m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(m_arrOrdenColsGr(j))).Width
                    End Select
                Next j
            Next i
        Else
            'Visualiza la 2� fila de la grid:
            Screen.MousePointer = vbHourglass
            LockWindowUpdate Me.hWnd
            GuardarTamanyosCampos True
            .LevelCount = 3
            m_oVistaSeleccionadaGr.OcultarFila2 = False
            vValores(0) = .Groups(0).Columns("DESCR").Value
            vValores(1) = .Groups(0).Columns("ANYO").Value
            vValores(2) = .Groups(0).Columns("ID").Value
            vValores(3) = .Groups(0).Columns("PROV").Value
            vValores(4) = .Groups(0).Columns("PRECAPE").Value
            vValores(5) = .Groups(0).Columns("ADJ").Value
            vValores(6) = .Groups(0).Columns("ART").Value
            vValores(7) = .Groups(0).Columns("OBJ").Value
            vValores(8) = .Groups(0).Columns("AHORROIMP").Value
            vValores(9) = .Groups(0).Columns("AHORROPORCEN").Value
            vValores(10) = .Groups(0).Columns("IMP").Value
            vValores(11) = .Groups(0).Columns("CANT").Value
            vValores(12) = .Groups(0).Columns("PORCENADJ").Value
            vValores(13) = .Groups(0).Columns("VINCULADO_ITEM").Value
            vValores(14) = .Groups(0).Columns("INI").Value
            vValores(15) = .Groups(0).Columns("FIN").Value
            vValores(16) = .Groups(0).Columns("GMN").Value
            OcultarFilasTotales
            .Groups(0).Columns("DESCR").Value = vValores(0)
            .Groups(0).Columns("ANYO").Value = vValores(1)
            .Groups(0).Columns("ID").Value = vValores(2)
            .Groups(0).Columns("PROV").Value = vValores(3)
            .Groups(0).Columns("PRECAPE").Value = vValores(4)
            .Groups(0).Columns("ADJ").Value = vValores(5)
            .Groups(0).Columns("ART").Value = vValores(6)
            .Groups(0).Columns("OBJ").Value = vValores(7)
            .Groups(0).Columns("AHORROIMP").Value = vValores(8)
            .Groups(0).Columns("AHORROPORCEN").Value = vValores(9)
            .Groups(0).Columns("IMP").Value = vValores(10)
            .Groups(0).Columns("CANT").Value = vValores(11)
            .Groups(0).Columns("PORCENADJ").Value = vValores(12)
            .Groups(0).Columns("VINCULADO_ITEM").Value = vValores(13)
            .Groups(0).Columns("INI").Value = vValores(14)
            .Groups(0).Columns("FIN").Value = vValores(15)
            .Groups(0).Columns("GMN").Value = vValores(16)
        End If
        'Se posiciona en la fila en la que estaba
        .Bookmark = intPos
        Screen.MousePointer = vbNormal
        LockWindowUpdate 0&
    End If
End With
End Sub

Private Sub sdbgAdj_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
Dim intNumLevel As Integer
Dim i As Integer
Dim intLevel As Integer

If m_oVistaSeleccionadaGr Is Nothing Then Exit Sub

m_oVistaSeleccionadaGr.HayCambios = True
If GrpIndex = 0 Then  'Si redimensiona el grupo de los datos del item
    If sdbgTotales.Columns("OBJTOT").Visible = True Then
        'si la columna de totales de objetivos est� visible calcula su tamanyo
        intNumLevel = 0
        intLevel = sdbgAdj.Columns("OBJ").Level
        For i = 0 To sdbgAdj.Groups(0).Columns.Count - 1
            If sdbgAdj.Groups(0).Columns(i).Level = intLevel And sdbgAdj.Groups(0).Columns(i).Visible = True Then intNumLevel = intNumLevel + 1
        Next i
        'intNumLevel contiene el n� de campos del mismo nivel en el que se encuentra el objetivo
        'y que est�n visibles
        If intNumLevel > 0 Then
            If (sdbgAdj.Columns("OBJ").Width + (sdbgAdj.ResizeWidth - sdbgAdj.Groups(0).Width) / intNumLevel) > 1000 Then
                sdbgTotales.Columns("OBJTOT").Width = sdbgAdj.Columns("OBJ").Width + (sdbgAdj.ResizeWidth - sdbgAdj.Groups(0).Width) / intNumLevel
                sdbgTotales.Columns(0).Width = sdbgAdj.ResizeWidth - sdbgTotales.Columns("OBJTOT").Width
            Else
                sdbgTotales.Columns("OBJTOT").Width = 1000
                sdbgTotales.Columns(0).Width = sdbgAdj.ResizeWidth - sdbgTotales.Columns("OBJTOT").Width
            End If
        Else
            sdbgTotales.Columns("OBJTOT").Width = 1000
            sdbgTotales.Columns(0).Width = sdbgAdj.ResizeWidth - sdbgTotales.Columns("OBJTOT").Width
        End If
        
    Else  'La columna de totales de objetivos no est� visible
        sdbgTotales.Columns(0).Width = sdbgAdj.ResizeWidth
    End If
Else  'Si redimensiona un proveedor
    sdbgTotales.Columns(GrpIndex).Width = sdbgAdj.ResizeWidth
    For i = 2 To sdbgAdj.Groups.Count - 1
        If i <> GrpIndex Then
            sdbgAdj.Groups(i).Width = sdbgAdj.ResizeWidth
            sdbgTotales.Columns(i).Width = sdbgAdj.ResizeWidth
        End If
    Next i
End If
End Sub

''' <summary>
''' Inicializa el grid (anchos,alineaciones, etc)
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAdj_InitColumnProps()
With sdbgAdj
    .DividerStyle = ssDividerStyleBlackline
    .DividerStyle = ssDividerStyleBlackline
    .Groups(0).Width = 5850.142
    .Columns("DESCR").Width = 1964.976
    .Columns("ANYO").Width = 810.1418
    If ADMIN_OBLIGATORIO = True Then
        .Columns("PROV").Width = 0
        .Columns("PRECAPE").Width = 2050
    Else
        .Columns("PROV").Width = 1025
        .Columns("PRECAPE").Width = 1025
    End If
    .Columns("OBJ").Width = 1025
    .Columns("AHORROIMP").Width = 1470.047
    .Columns("AHORROPORCEN").Width = 1305.071
    .Columns("IMP").Width = 1025
    .Columns("CANT").Width = 1025
    .Columns("PORCENADJ").Width = 1025
    .Columns("VINCULADO_ITEM").Width = 1025
    .Columns("INI").Width = 1025
    .Columns("FIN").Width = 1025
    .Columns("GMN").Width = 1025
    .SplitterVisible = False
    sdbgTotales.SplitterVisible = False
End With
End Sub

Private Sub sdbgAdj_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picControlVistaProceso.Visible = False
    picControlVistaAll.Visible = False
    picControlVistaGrupo.Visible = False
End Sub

Private Sub sdbgAdj_RowResize(Cancel As Integer)
    If m_oVistaSeleccionadaGr Is Nothing Then Exit Sub
    m_oVistaSeleccionadaGr.HayCambios = True
End Sub

Private Sub sdbgAdj_SplitterMove(Cancel As Integer)
    Cancel = True
End Sub

''' <summary>
''' Cambia de posici�n las columnas en la grid (las correspondientes a los proveedores)
''' </summary>
''' <param name="WhatChanged">que cambia</param>
''' <param name="NewIndex">nuevo indice</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAll_AfterPosChanged(ByVal WhatChanged As Integer, ByVal NewIndex As Integer)
Dim iNumColums As Integer
Dim iPos As Integer
Dim iOrden As Integer
Dim i As Integer
Dim sColumna As String
Dim iNumColumnas As Integer
Dim sProv As String
Dim iNumColsGrupo As Integer
Dim iGrupoVisible As Integer
Dim oAtribVista As CPlantConfVistaAllAtrib
With sdbgAll
    '****** Cambia de posici�n las columnas en la grid (las correspondientes a los proveedores) *****
    'Si se ha hecho un scroll lo deja todo en la posici�n original.
    If .GrpPosition(0) > 0 Then
        i = .Groups(0).Position
        .Scroll -i, 0
        .Update
        sdbgTotalesAll.Scroll -i, 0
        sdbgTotalesAll.Update
    End If
    For i = 0 To .Groups.Count - 1
        .Groups(i).Position = i
    Next i
    For i = 0 To sdbgTotalesAll.Columns.Count - 1
        sdbgTotalesAll.Columns(i).Position = i
    Next i
    iNumColums = .Groups(2).Columns.Count
    'Si se mueve una columna de los 2 primeros grupos no hace nada
    If .Columns(NewIndex).Group <= 1 Then
        If .LevelCount = 1 Then
            If .Columns("ANYO").Visible = True Then m_oVistaSeleccionadaAll.AnyoPos = .Columns("ANYO").Position - (.Groups(0).Position * iNumColums)
            If .Columns("PROV").Visible = True Then m_oVistaSeleccionadaAll.ProveedorPos = .Columns("PROV").Position - (.Groups(0).Position * iNumColums)
            If .Columns("PRECAPE").Visible = True Then m_oVistaSeleccionadaAll.PresUniPos = .Columns("PRECAPE").Position - (.Groups(0).Position * iNumColums)
            If .Columns("OBJ").Visible = True Then m_oVistaSeleccionadaAll.OBJPos = .Columns("OBJ").Position - (.Groups(0).Position * iNumColums)
            If .Columns("AHORROIMP").Visible = True Then m_oVistaSeleccionadaAll.AhorroPos = .Columns("AHORROIMP").Position - (.Groups(0).Position * iNumColums)
            If .Columns("AHORROPORCEN").Visible = True Then m_oVistaSeleccionadaAll.AhorroPorcenPos = .Columns("AHORROPORCEN").Position - (.Groups(0).Position * iNumColums)
            If .Columns("IMP").Visible = True Then m_oVistaSeleccionadaAll.ImportePos = .Columns("IMP").Position - (.Groups(0).Position * iNumColums)
            If .Columns("CANT").Visible = True Then m_oVistaSeleccionadaAll.CantidadPos = .Columns("CANT").Position - (.Groups(0).Position * iNumColums)
            If .Columns("PORCENADJ").Visible = True Then m_oVistaSeleccionadaAll.AdjPos = .Columns("PORCENADJ").Position - (.Groups(0).Position * iNumColums)
            If .Columns("VINCULADO_ITEM").Visible = True Then m_oVistaSeleccionadaAll.SolicVinculadaPos = .Columns("VINCULADO_ITEM").Position - (.Groups(0).Position * iNumColums)
            If .Columns("INI").Visible = True Then m_oVistaSeleccionadaAll.FecIniSumPos = .Columns("INI").Position - (.Groups(0).Position * iNumColums)
            If .Columns("FIN").Visible = True Then m_oVistaSeleccionadaAll.FecFinSumPos = .Columns("FIN").Position - (.Groups(0).Position * iNumColums)
            If .Columns("GMN").Visible = True Then m_oVistaSeleccionadaAll.EstrMatPos = .Columns("GMN").Position - (.Groups(0).Position * iNumColums)
        End If
        Exit Sub
    Else
        'Cuando se mueve una columna de un proveedor a otra posici�n se mueve en todos los grupos:
        'Obtiene el nombre de la columna que se va a mover
        sProv = .Groups(.Columns(NewIndex).Group).TagVariant
        sColumna = Mid(.Columns(NewIndex).Name, 1, InStr(.Columns(NewIndex).Name, sProv) - 1)
        'Redimensiona tambi�n la columna en el resto de grupos de la grid
        iNumColums = .Groups(2).Columns.Count
        iPos = .Columns(NewIndex).Position
        iNumColumnas = .Groups(0).Columns.Count + .Groups(1).Columns.Count - 1
        iOrden = iPos - (iNumColumnas + (.Columns(NewIndex).Group - 2) * iNumColums)
        For i = 2 To .Groups.Count - 1
            If sProv <> .Groups(i).TagVariant Then
                .Columns(sColumna & .Groups(i).TagVariant).Position = iNumColumnas + iOrden + (iNumColums * (i - 2))
                .Columns(sColumna & .Groups(i).TagVariant).Level = .Columns(sColumna & sProv).Level
            End If
        Next i
        If .LevelCount = 1 Then
            'Obtiene cual es el 1� proveedor que est� visible
            For iGrupoVisible = 1 To .Groups.Count - 1
                If .Groups(iGrupoVisible).Visible = True Then
                    Exit For
                End If
            Next iGrupoVisible
            If iGrupoVisible = .Groups.Count Then Exit Sub
            iNumColsGrupo = .Groups(iGrupoVisible).Columns.Count
            If IsNull(m_oVistaSeleccionadaAll.ImpAdjPos) Then
                m_oVistaSeleccionadaAll.ImpAdjPos = 1
            ElseIf .Columns("IMPADJ" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaAll.ImpAdjPos = .Columns("IMPADJ" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            If IsNull(m_oVistaSeleccionadaAll.CantProvPos) Then
                m_oVistaSeleccionadaAll.CantProvPos = 8
            ElseIf .Columns("AdjCantidad" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaAll.CantProvPos = .Columns("AdjCantidad" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            If IsNull(m_oVistaSeleccionadaAll.AdjProvPos) Then
                m_oVistaSeleccionadaAll.AdjProvPos = 9
            ElseIf .Columns("AdjPorcen" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaAll.AdjProvPos = .Columns("AdjPorcen" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            If IsNull(m_oVistaSeleccionadaAll.PrecioProvPos) Then
                m_oVistaSeleccionadaAll.PrecioProvPos = 2
            ElseIf .Columns("Precio" & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                m_oVistaSeleccionadaAll.PrecioProvPos = .Columns("Precio" & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            End If
            'almacena los pos de los atributos
            For Each oAtribVista In m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib
                If IsNull(oAtribVista.Posicion) Then
                    oAtribVista.Posicion = 1
                ElseIf .Columns(oAtribVista.Atributo & .Groups(iGrupoVisible).TagVariant).Visible = True Then
                    oAtribVista.Posicion = .Columns(oAtribVista.Atributo & .Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
                End If
            Next
            OrdenarColsGrupoProves False
        End If
    End If
End With
End Sub

Private Sub sdbgAll_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
    'Si el scroll no est� en la posici�n 0 no permite mover las columnas
    If sdbgAll.GrpPosition(0) <> 0 Then
        Cancel = True
        Exit Sub
    End If
    If ColIndex = 0 Then Cancel = True
    If NewPos = 0 Then Cancel = True
    If m_oVistaSeleccionadaAll Is Nothing Then
        Cancel = True
        Exit Sub
    End If
    m_oVistaSeleccionadaAll.HayCambios = True
End Sub

Private Sub sdbgAll_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
Dim sProv As String
Dim i As Integer
Dim sColum As String
If m_oVistaSeleccionadaAll Is Nothing Then Exit Sub
m_oVistaSeleccionadaAll.HayCambios = True
'Obtenemos el proveedor cuya columna se est� cambiando de tama�o.Hay que tener en cuenta el scroll:
If sdbgAll.Groups(sdbgAll.Columns(ColIndex).Group).Position <> sdbgAll.Columns(ColIndex).Group Then
    sProv = sdbgAll.Groups(sdbgAll.GrpPosition(sdbgAll.Columns(ColIndex).Group)).TagVariant
Else
    sProv = sdbgAll.Groups(sdbgAll.Columns(ColIndex).Group).TagVariant
End If
If sdbgAll.Columns(ColIndex).Group <> sdbgAll.Groups(0).Position And sdbgAll.Columns(ColIndex).Group <> sdbgAll.Groups(1).Position Then
    sColum = Mid(sdbgAll.Columns(ColIndex).Name, 1, Len(sdbgAll.Columns(ColIndex).Name) - Len(sProv))
    For i = 2 To sdbgAll.Groups.Count - 1
        If sdbgAll.Groups(i).TagVariant <> sProv Then
            sdbgAll.Columns(sColum & sdbgAll.Groups(i).TagVariant).Width = sdbgAll.ResizeWidth
        End If
    Next i
Else   'es una columna de los datos del art�culo
    'Si se modifica el tamanyo de la columna de objetivos se redimensiona la
    'de totales de objetivos para que las 2 tengan el mismo width
    If sdbgAll.Columns(ColIndex).Name <> "OBJ" Then Exit Sub
    If sdbgTotalesAll.Columns("OBJTOT").Visible = True Then
        If sdbgAll.ResizeWidth > 1000 Then
            sdbgTotalesAll.Columns("OBJTOT").Width = sdbgAll.ResizeWidth
            sdbgTotalesAll.Columns(0).Width = sdbgAll.Groups(0).Width - sdbgAll.ResizeWidth
        Else
            sdbgTotalesAll.Columns("OBJTOT").Width = 1000
            sdbgTotalesAll.Columns(0).Width = sdbgAll.Groups(0).Width - 1000
        End If
    Else
        sdbgTotalesAll.Columns(0).Width = sdbgAll.Groups(0).Width
    End If
End If
End Sub

''' <summary>
''' Evento en la cabecera del grid de adjudicacion del grupo All
''' </summary>
''' <param name="GrpIndex">Indice del grupo</param>
''' <remarks>Llamada desde: Evento que salta al hacer click en la cabecera del grid; Tiempo m�ximo: 0</remarks>
Private Sub sdbgAll_GrpHeadClick(ByVal GrpIndex As Integer)
Dim i As Integer
Dim intPos As Variant
Dim sGrupo As String
Dim iOrden As Integer
Dim inumColumns As Integer
Dim j As Integer
Dim vValores(17) As Variant
With sdbgAll
    If GrpIndex < 1 Then
        If .Rows = 0 Then Exit Sub
        If m_bCargandoVista = False Then m_oVistaSeleccionadaAll.HayCambios = True
        intPos = .Bookmark
        .Update
        .Col = 0
        If Me.Enabled And Me.Visible Then sdbgTotalesAll.SetFocus
        .Redraw = True
        If .LevelCount = 3 Then
            Screen.MousePointer = vbHourglass
            LockWindowUpdate Me.hWnd
            If m_bCargandoVista = False Then GuardarConfiguracionGridAll
            m_oVistaSeleccionadaAll.OcultarFila2 = True
            For i = 0 To .Columns.Count - 1
                If .Columns(i).Level = 1 Then .Columns(i).Visible = False
            Next i
            vValores(0) = .Groups(0).Columns("DESCR").Value
            vValores(1) = .Groups(0).Columns("ANYO").Value
            vValores(2) = .Groups(0).Columns("ID").Value
            vValores(3) = .Groups(0).Columns("PROV").Value
            vValores(4) = .Groups(0).Columns("PRECAPE").Value
            vValores(5) = .Groups(0).Columns("ADJ").Value
            vValores(6) = .Groups(0).Columns("ART").Value
            vValores(7) = .Groups(0).Columns("OBJ").Value
            vValores(8) = .Groups(0).Columns("AHORROIMP").Value
            vValores(9) = .Groups(0).Columns("AHORROPORCEN").Value
            vValores(10) = .Groups(0).Columns("IMP").Value
            vValores(11) = .Groups(0).Columns("CANT").Value
            vValores(12) = .Groups(0).Columns("PORCENADJ").Value
            vValores(13) = .Groups(0).Columns("VINCULADO_ITEM").Value
            vValores(14) = .Groups(0).Columns("INI").Value
            vValores(15) = .Groups(0).Columns("FIN").Value
            vValores(16) = .Groups(0).Columns("GMN").Value
            .LevelCount = 1
            .Groups(0).Columns("DESCR").Value = vValores(0)
            .Groups(0).Columns("ANYO").Value = vValores(1)
            .Groups(0).Columns("ID").Value = vValores(2)
            .Groups(0).Columns("PROV").Value = vValores(3)
            .Groups(0).Columns("PRECAPE").Value = vValores(4)
            .Groups(0).Columns("ADJ").Value = vValores(5)
            .Groups(0).Columns("ART").Value = vValores(6)
            .Groups(0).Columns("OBJ").Value = vValores(7)
            .Groups(0).Columns("AHORROIMP").Value = vValores(8)
            .Groups(0).Columns("AHORROPORCEN").Value = vValores(9)
            .Groups(0).Columns("IMP").Value = vValores(10)
            .Groups(0).Columns("CANT").Value = vValores(11)
            .Groups(0).Columns("PORCENADJ").Value = vValores(12)
            .Groups(0).Columns("VINCULADO_ITEM").Value = vValores(13)
            .Groups(0).Columns("INI").Value = vValores(14)
            .Groups(0).Columns("FIN").Value = vValores(15)
            .Groups(0).Columns("GMN").Value = vValores(16)
            If m_bCargandoVista = False Then OrdenarColsGrupo0 (False)
            .Groups(0).Columns("DESCR").Position = 0
            For i = 0 To 12
                Select Case m_arrOrden(i)
                    Case "ANYO"
                        .Groups(0).Columns("ANYO").Position = m_oVistaSeleccionadaAll.AnyoPos
                    Case "PROV"
                        .Groups(0).Columns("PROV").Position = m_oVistaSeleccionadaAll.ProveedorPos
                    Case "PRECAPE"
                        .Groups(0).Columns("PRECAPE").Position = m_oVistaSeleccionadaAll.PresUniPos
                    Case "OBJ"
                        .Groups(0).Columns("OBJ").Position = m_oVistaSeleccionadaAll.OBJPos
                    Case "AHORROIMP"
                        .Groups(0).Columns("AHORROIMP").Position = m_oVistaSeleccionadaAll.AhorroPos
                    Case "AHORROPORCEN"
                        .Groups(0).Columns("AHORROPORCEN").Position = m_oVistaSeleccionadaAll.AhorroPorcenPos
                    Case "IMP"
                        .Groups(0).Columns("IMP").Position = m_oVistaSeleccionadaAll.ImportePos
                    Case "CANT"
                        .Groups(0).Columns("CANT").Position = m_oVistaSeleccionadaAll.CantidadPos
                    Case "PORCENADJ"
                        .Groups(0).Columns("PORCENADJ").Position = m_oVistaSeleccionadaAll.AdjPos
                    Case "VINCULADO_ITEM"
                        .Groups(0).Columns("VINCULADO_ITEM").Position = m_oVistaSeleccionadaAll.SolicVinculadaPos
                    Case "INI"
                        .Groups(0).Columns("INI").Position = m_oVistaSeleccionadaAll.FecIniSumPos
                    Case "FIN"
                        .Groups(0).Columns("FIN").Position = m_oVistaSeleccionadaAll.FecFinSumPos
                    Case "GMN"
                        .Groups(0).Columns("GMN").Position = m_oVistaSeleccionadaAll.EstrMatPos
                End Select
            Next i
            .Groups(0).Columns("DESCR").Width = m_oVistaSeleccionadaAll.DescrWidth
            For i = 0 To 12
                Select Case m_arrOrden(i)
                    Case "ANYO"
                        .Groups(0).Columns("ANYO").Width = m_oVistaSeleccionadaAll.AnyoWidth
                    Case "PROV"
                        .Groups(0).Columns("PROV").Width = m_oVistaSeleccionadaAll.ProveedorWidth
                    Case "PRECAPE"
                        .Groups(0).Columns("PRECAPE").Width = m_oVistaSeleccionadaAll.PresUniWidth
                    Case "OBJ"
                        .Groups(0).Columns("OBJ").Width = m_oVistaSeleccionadaAll.OBJWidth
                    Case "AHORROIMP"
                        .Groups(0).Columns("AHORROIMP").Width = m_oVistaSeleccionadaAll.AhorroWidth
                    Case "AHORROPORCEN"
                        .Groups(0).Columns("AHORROPORCEN").Width = m_oVistaSeleccionadaAll.AhorroPorcenWidth
                    Case "IMP"
                        .Groups(0).Columns("IMP").Width = m_oVistaSeleccionadaAll.ImporteWidth
                    Case "CANT"
                        .Groups(0).Columns("CANT").Width = m_oVistaSeleccionadaAll.CantidadWidth
                    Case "PORCENADJ"
                        .Groups(0).Columns("PORCENADJ").Width = m_oVistaSeleccionadaAll.AdjWidth
                    Case "VINCULADO_ITEM"
                        .Groups(0).Columns("VINCULADO_ITEM").Width = m_oVistaSeleccionadaAll.SolicVinculadaWidth
                    Case "INI"
                        .Groups(0).Columns("INI").Width = m_oVistaSeleccionadaAll.FecIniSumWidth
                    Case "FIN"
                        .Groups(0).Columns("FIN").Width = m_oVistaSeleccionadaAll.FecFinSumWidth
                    Case "GMN"
                        .Groups(0).Columns("GMN").Width = m_oVistaSeleccionadaAll.EstrMatWidth
                End Select
            Next i
            If m_bCargandoVista = False Then OrdenarColsGrupoProves (False)                 'Ordena los atributos y campos de los proveedores
            'Pone las posiciones a los grupos de los proveedores
            For i = 2 To .Groups.Count - 1
                sGrupo = .Groups(i).TagVariant
                ''''Posici�n de los campos:
                iOrden = m_iNumColsAll + (.Groups(i).Columns.Count * (i - 2))
                inumColumns = 3
                If Not m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib Is Nothing Then inumColumns = inumColumns + m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Count
                For j = 0 To inumColumns
                    Select Case m_arrOrdenColsAll(j)
                        Case "IMPADJ"
                            .Columns("IMPADJ" & sGrupo).Position = m_oVistaSeleccionadaAll.ImpAdjPos + iOrden
                        Case "PRECIO"
                            .Columns("Precio" & sGrupo).Position = m_oVistaSeleccionadaAll.PrecioProvPos + iOrden
                        Case "ADJPROV"
                            .Columns("AdjPorcen" & sGrupo).Position = m_oVistaSeleccionadaAll.AdjProvPos + iOrden
                        Case "CANTPROV"
                            .Columns("AdjCantidad" & sGrupo).Position = m_oVistaSeleccionadaAll.CantProvPos + iOrden
                        Case Else
                            'Es un atributo
                            .Columns(m_arrOrdenColsAll(j) & sGrupo).Position = m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(m_arrOrdenColsAll(j))).Posicion + iOrden
                    End Select
                Next j
                'ahora pone los widths
                For j = 0 To inumColumns
                    Select Case m_arrOrdenColsAll(j)
                        Case "IMPADJ"
                            .Columns("IMPADJ" & sGrupo).Width = m_oVistaSeleccionadaAll.ImpAdjWidth
                        Case "PRECIO"
                            .Columns("Precio" & sGrupo).Width = m_oVistaSeleccionadaAll.PrecioProvWidth
                        Case "ADJPROV"
                            .Columns("AdjPorcen" & sGrupo).Width = m_oVistaSeleccionadaAll.AdjProvWidth
                        Case "CANTPROV"
                            .Columns("AdjCantidad" & sGrupo).Width = m_oVistaSeleccionadaAll.CantProvWidth
                        Case Else
                            'Es un atributo
                            .Columns(m_arrOrdenColsAll(j) & sGrupo).Width = m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(m_arrOrdenColsAll(j))).Width
                    End Select
                Next j
            Next i
        Else
            Screen.MousePointer = vbHourglass
            LockWindowUpdate Me.hWnd
            GuardarTamanyosCampos False
            .LevelCount = 3
            m_oVistaSeleccionadaAll.OcultarFila2 = False
            vValores(0) = .Groups(0).Columns("DESCR").Value
            vValores(1) = .Groups(0).Columns("ANYO").Value
            vValores(2) = .Groups(0).Columns("ID").Value
            vValores(3) = .Groups(0).Columns("PROV").Value
            vValores(4) = .Groups(0).Columns("PRECAPE").Value
            vValores(5) = .Groups(0).Columns("ADJ").Value
            vValores(6) = .Groups(0).Columns("ART").Value
            vValores(7) = .Groups(0).Columns("OBJ").Value
            vValores(8) = .Groups(0).Columns("AHORROIMP").Value
            vValores(9) = .Groups(0).Columns("AHORROPORCEN").Value
            vValores(10) = .Groups(0).Columns("IMP").Value
            vValores(11) = .Groups(0).Columns("CANT").Value
            vValores(12) = .Groups(0).Columns("PORCENADJ").Value
            vValores(13) = .Groups(0).Columns("VINCULADO_ITEM").Value
            vValores(14) = .Groups(0).Columns("INI").Value
            vValores(15) = .Groups(0).Columns("FIN").Value
            vValores(16) = .Groups(0).Columns("GMN").Value
            OcultarFilasTotalesAll
            .Groups(0).Columns("DESCR").Value = vValores(0)
            .Groups(0).Columns("ANYO").Value = vValores(1)
            .Groups(0).Columns("ID").Value = vValores(2)
            .Groups(0).Columns("PROV").Value = vValores(3)
            .Groups(0).Columns("PRECAPE").Value = vValores(4)
            .Groups(0).Columns("ADJ").Value = vValores(5)
            .Groups(0).Columns("ART").Value = vValores(6)
            .Groups(0).Columns("OBJ").Value = vValores(7)
            .Groups(0).Columns("AHORROIMP").Value = vValores(8)
            .Groups(0).Columns("AHORROPORCEN").Value = vValores(9)
            .Groups(0).Columns("IMP").Value = vValores(10)
            .Groups(0).Columns("CANT").Value = vValores(11)
            .Groups(0).Columns("PORCENADJ").Value = vValores(12)
            .Groups(0).Columns("VINCULADO_ITEM").Value = vValores(13)
            .Groups(0).Columns("INI").Value = vValores(14)
            .Groups(0).Columns("FIN").Value = vValores(15)
            .Groups(0).Columns("GMN").Value = vValores(16)
        End If
        'Se posiciona en la fila en la que estaba
        .Bookmark = intPos
        Screen.MousePointer = vbNormal
        LockWindowUpdate 0&
    End If
End With
End Sub

Private Sub sdbgAll_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
Dim intNumLevel As Integer
Dim i As Integer
Dim intLevel As Integer
If m_oVistaSeleccionadaAll Is Nothing Then Exit Sub
With sdbgTotalesAll
    m_oVistaSeleccionadaAll.HayCambios = True
    If GrpIndex = 0 Then  'Si redimensiona el grupo de los datos del item
        If .Columns("OBJTOT").Visible = True Then
            'si la columna de totales de objetivos est� visible calcula su tamanyo
            intNumLevel = 0
            intLevel = sdbgAll.Columns("OBJ").Level
            For i = 0 To sdbgAll.Groups(0).Columns.Count - 1
                If sdbgAll.Groups(0).Columns(i).Level = intLevel And sdbgAll.Groups(0).Columns(i).Visible = True Then intNumLevel = intNumLevel + 1
            Next i
            'intNumLevel contiene el n� de campos del mismo nivel en el que se encuentra el objetivo
            'y que est�n visibles
            If intNumLevel > 0 Then
                If (sdbgAll.Columns("OBJ").Width + (sdbgAll.ResizeWidth - sdbgAll.Groups(0).Width) / intNumLevel) > 1000 Then
                    .Columns("OBJTOT").Width = sdbgAll.Columns("OBJ").Width + (sdbgAll.ResizeWidth - sdbgAll.Groups(0).Width) / intNumLevel
                    .Columns(0).Width = sdbgAll.ResizeWidth - .Columns("OBJTOT").Width
                Else
                    .Columns("OBJTOT").Width = 1000
                    .Columns(0).Width = sdbgAll.ResizeWidth - .Columns("OBJTOT").Width
                End If
            Else
                .Columns("OBJTOT").Width = 1000
                .Columns(0).Width = sdbgAll.ResizeWidth - .Columns("OBJTOT").Width
            End If
        Else  'La columna de totales de objetivos no est� visible
            .Columns(0).Width = sdbgAll.ResizeWidth
        End If
    Else  'Si redimensiona un proveedor
        .Columns(GrpIndex).Width = sdbgAll.ResizeWidth
        For i = 2 To sdbgAll.Groups.Count - 1
            If i <> GrpIndex Then
                sdbgAll.Groups(i).Width = sdbgAll.ResizeWidth
                .Columns(i).Width = sdbgAll.ResizeWidth
            End If
        Next i
    End If
End With
End Sub

''' <summary>
''' Inicializa el grid (anchos,alineaciones, etc)
''' </summary>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAll_InitColumnProps()
With sdbgAll
    .DividerStyle = ssDividerStyleBlackline
    .DividerStyle = ssDividerStyleBlackline
    .Groups(0).Width = 5850.142
    .Columns("DESCR").Width = 1964.976
    .Columns("ANYO").Width = 810.1418
    If ADMIN_OBLIGATORIO = True Then
        .Columns("PROV").Width = 0
        .Columns("PRECAPE").Width = 2050
    Else
        .Columns("PROV").Width = 1025
        .Columns("PRECAPE").Width = 1025
    End If
    .Columns("OBJ").Width = 1025
    .Columns("AHORROIMP").Width = 1470.047
    .Columns("AHORROPORCEN").Width = 1305.071
    .Columns("IMP").Width = 1025
    .Columns("CANT").Width = 1025
    .Columns("PORCENADJ").Width = 1025
    .Columns("VINCULADO_ITEM").Width = 1025
    .Columns("INI").Width = 1025
    .Columns("FIN").Width = 1025
    .Columns("GMN").Width = 1025
    .SplitterVisible = False
    sdbgTotalesAll.SplitterVisible = False
End With
End Sub

Private Sub sdbgAll_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picControlVistaProceso.Visible = False
    picControlVistaAll.Visible = False
    picControlVistaGrupo.Visible = False
End Sub

Private Sub sdbgAll_RowResize(Cancel As Integer)
    If m_oVistaSeleccionadaAll Is Nothing Then Exit Sub
    m_oVistaSeleccionadaAll.HayCambios = True
End Sub

Private Sub sdbgAll_SplitterMove(Cancel As Integer)
    Cancel = True
End Sub


Private Sub sdbgGruposProve_AfterPosChanged(ByVal WhatChanged As Integer, ByVal NewIndex As Integer)
    Dim iNumColums As Integer
    Dim iPos As Integer
    Dim iOrden As Integer
    Dim i As Integer
    Dim sColumna As String
    Dim iNumColumnas As Integer
    Dim sProv As String
    'Cuando se mueve una columna de un grupo a otra posici�n se mueve en todos los grupos
    If sdbgGruposProve.Columns(NewIndex).Group <= 0 Then Exit Sub
    'Obtiene el nombre de la columna que se va a mover
    sProv = sdbgGruposProve.Groups(sdbgGruposProve.Columns(NewIndex).Group).TagVariant
    sColumna = Mid(sdbgGruposProve.Columns(NewIndex).Name, 1, InStr(sdbgGruposProve.Columns(NewIndex).Name, sProv) - 1)
    'Redimensiona tambi�n la columna en el resto de grupos de la grid
    iNumColums = sdbgGruposProve.Groups(1).Columns.Count
    iPos = sdbgGruposProve.Columns(NewIndex).Position
    iNumColumnas = sdbgGruposProve.Groups(0).Columns.Count - 1
    iOrden = iPos - (iNumColumnas + (sdbgGruposProve.Columns(NewIndex).Group - 1) * iNumColums)
    For i = 1 To sdbgGruposProve.Groups.Count - 1
        If sProv <> sdbgGruposProve.Groups(i).TagVariant Then sdbgGruposProve.Columns(sColumna & sdbgGruposProve.Groups(i).TagVariant).Position = iNumColumnas + iOrden + (iNumColums * (i - 1))
    Next i
    'Redimensiona tambi�n en sdbgProveGrupos porque ambas deben ser iguales
    For i = 2 To sdbgProveGrupos.Groups.Count - 1
        If Not IsEmpty(sdbgProveGrupos.Groups(i).TagVariant) Then sdbgProveGrupos.Columns(sColumna & sdbgProveGrupos.Groups(i).TagVariant).Position = iNumColumnas + sdbgProveGrupos.Groups(1).Columns.Count + iOrden + (iNumColums * (i - 2))
    Next i
    m_oVistaSeleccionada.HayCambios = True
End Sub

Private Sub sdbgGruposProve_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
    'Al redimensionar una columna tiene que redimensionar esa columna en todos los grupos
    If sdbgGruposProve.Columns(ColIndex).Group = 0 Then
        Cancel = True
        Exit Sub
    End If
    'Si el scroll no est� en la posici�n 0 no permite mover las columnas
    If sdbgGruposProve.GrpPosition(0) <> 0 Then
        Cancel = True
        Exit Sub
    End If
End Sub

Private Sub sdbgGruposProve_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
    Dim i As Integer
    Dim sColumna As String
    Dim sProv As String
    'Al redimensionar una columna tiene que redimensionar esa columna en todos los grupos
    If m_oVistaSeleccionada Is Nothing Then Exit Sub
    'Si la columna pertenece al grupo 0 se pasan tambi�n los cambios a proveGrupos
    If sdbgGruposProve.Columns(ColIndex).Group = sdbgGruposProve.Groups(0).Position Then
        sColumna = sdbgGruposProve.Columns(ColIndex).Name
        Select Case sColumna
            Case "COD"
                sdbgProveGrupos.Columns("COD").Width = sdbgGruposProve.ResizeWidth
            Case "NOMBRE"
                sdbgProveGrupos.Columns("DEN").Width = sdbgGruposProve.ResizeWidth
        End Select
        m_oVistaSeleccionada.HayCambios = True
        Exit Sub
    End If
    'Obtenemos el proveedor cuya columna se est� cambiando de tama�o.Hay que tener en cuenta el scroll:
    If sdbgGruposProve.Groups(sdbgGruposProve.Columns(ColIndex).Group).Position <> sdbgGruposProve.Columns(ColIndex).Group Then
        sProv = sdbgGruposProve.Groups(sdbgGruposProve.GrpPosition(sdbgGruposProve.Columns(ColIndex).Group)).TagVariant
    Else
        sProv = sdbgGruposProve.Groups(sdbgGruposProve.Columns(ColIndex).Group).TagVariant
    End If
    'Obtiene el nombre de la columna
    sColumna = Mid(sdbgGruposProve.Columns(ColIndex).Name, 1, InStr(sdbgGruposProve.Columns(ColIndex).Name, sProv) - 1)
    'Redimensiona tambi�n la columna en el resto de grupos de la grid
    For i = 1 To sdbgGruposProve.Groups.Count - 1
        If sdbgGruposProve.Groups(i).TagVariant <> sProv Then sdbgGruposProve.Columns(sColumna & sdbgGruposProve.Groups(i).TagVariant).Width = sdbgGruposProve.ResizeWidth
    Next i
    'Tiene que redimensionar tambi�n en la grid sdbgProveGrupos,porque ambas tienen que ser iguales:
    For i = 1 To sdbgProveGrupos.Groups.Count - 1
        If Not IsEmpty(sdbgProveGrupos.Groups(i).TagVariant) Then sdbgProveGrupos.Columns(sColumna & sdbgProveGrupos.Groups(i).TagVariant).Width = sdbgGruposProve.ResizeWidth
    Next i
    m_oVistaSeleccionada.HayCambios = True
End Sub

Private Sub sdbgGruposProve_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
    Dim i As Integer
    If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
    If GrpIndex = 0 Then  'Si es el 1� grupo
        sdbgTotalesProve.Groups(0).Width = sdbgGruposProve.ResizeWidth
        sdbgProce.Groups(0).Width = sdbgGruposProve.ResizeWidth
        sdbgProveGrupos.Groups(0).Width = sdbgGruposProve.ResizeWidth
        m_oVistaSeleccionada.HayCambios = True
        Exit Sub
    End If
    m_oVistaSeleccionada.HayCambios = True
    'Redimensiona las grid de totales
    sdbgTotalesProve.Groups(GrpIndex).Width = sdbgGruposProve.ResizeWidth
    'Redimensiona la grid de procesos
    If sdbgProce.Groups.Count > 1 Then sdbgProce.Groups(GrpIndex).Width = sdbgGruposProve.ResizeWidth
    'Redimensiona tambi�n el resto de grupos de la grid
    For i = 1 To sdbgGruposProve.Groups.Count - 1
        If i <> GrpIndex Then
            sdbgGruposProve.Groups(i).Width = sdbgGruposProve.ResizeWidth
            sdbgTotalesProve.Groups(i).Width = sdbgGruposProve.ResizeWidth
            If sdbgProce.Groups.Count > 1 Then sdbgProce.Groups(i).Width = sdbgGruposProve.ResizeWidth
        End If
    Next i
    'Redimensiona tambi�n la grid de sdbgProveGrupos porque ambas deben ser iguales
    For i = 1 To sdbgProveGrupos.Groups.Count - 1
        sdbgProveGrupos.Groups(i).Width = sdbgGruposProve.ResizeWidth
        sdbgtotales2.Groups(i).Width = sdbgGruposProve.ResizeWidth
    Next i
End Sub

Private Sub sdbgGruposProve_InitColumnProps()
    sdbgGruposProve.SplitterVisible = False
    sdbgTotalesProve.SplitterVisible = False
    sdbgProce.SplitterVisible = False
End Sub

Private Sub sdbgGruposProve_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picControlVistaProceso.Visible = False
    picControlVistaAll.Visible = False
    picControlVistaGrupo.Visible = False
End Sub

Private Sub sdbgGruposProve_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
    Dim oatrib As CAtributo
    Dim sGrupo As String
    For i = 1 To 3
        'comprueba si el atributo es del grupo actual para cada uno de las columnas de atributos:
        sGrupo = sdbgGruposProve.Columns("COD").Value
        If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
            For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
                If oatrib.ambito = AmbGrupo Then
                    If oatrib.codgrupo <> sGrupo And Not IsNull(oatrib.codgrupo) Then
                        sdbgGruposProve.Columns(oatrib.Cod & sdbgGruposProve.Groups(i).TagVariant).CellStyleSet "GrayHead", sdbgGruposProve.Row
                    Else
                        sdbgGruposProve.Columns(oatrib.Cod & sdbgGruposProve.Groups(i).TagVariant).CellStyleSet "Normal", sdbgGruposProve.Row
                    End If
                End If
            Next
        End If
    Next i
End Sub

Private Sub sdbgGruposProve_RowResize(Cancel As Integer)
If m_oVistaSeleccionada Is Nothing Then Exit Sub
m_oVistaSeleccionada.HayCambios = True
'El ancho de fila de la grid de sdbgProveGrupos tiene que ser el mismo
sdbgProveGrupos.RowHeight = sdbgGruposProve.ResizeHeight
End Sub

Private Sub sdbgGruposProve_SplitterMove(Cancel As Integer)
    Cancel = True
End Sub

Private Sub sdbgOcultarCampos_Change()
'Oculta el campo correspondiente
If sdbgOcultarCampos.Columns("INC").Value = "-1" Then
    OcultarCampo sdbgOcultarCampos.Columns("COD").Value, True
Else
    OcultarCampo sdbgOcultarCampos.Columns("COD").Value, False
End If
End Sub

Private Sub sdbgOcultarCamposAll_Change()
'Oculta el campo correspondiente
If sdbgOcultarCamposAll.Columns("INC").Value = "-1" Then
    OcultarCampoAll sdbgOcultarCamposAll.Columns("COD").Value, True
Else
    OcultarCampoAll sdbgOcultarCamposAll.Columns("COD").Value, False
End If
End Sub

Private Sub sdbgOcultarCamposGr_Change()
'Oculta el campo correspondiente
If sdbgOcultarCamposGr.Columns("INC").Value = "-1" Then
    OcultarCampoGr sdbgOcultarCamposGr.Columns("COD").Value, True
Else
    OcultarCampoGr sdbgOcultarCamposGr.Columns("COD").Value, False
End If
End Sub

Private Sub sdbgProce_AfterPosChanged(ByVal WhatChanged As Integer, ByVal NewIndex As Integer)
    Dim iNumColums As Integer
    Dim iPos As Integer
    Dim iOrden As Integer
    Dim i As Integer
    Dim sColumna As String
     'Cuando se mueve una columna de un grupo a otra posici�n se mueve en todos los grupos
    If sdbgProce.Columns(NewIndex).Group <= 0 Then Exit Sub
     'Obtiene el nombre de la columna
    sColumna = Mid(sdbgProce.Columns(NewIndex).Name, 1, InStr(sdbgProce.Columns(NewIndex).Name, sdbgProce.Groups(sdbgProce.Columns(NewIndex).Group).TagVariant) - 1)
    'Redimensiona tambi�n la columna en el resto de grupos de la grid
    iNumColums = sdbgProce.Groups(1).Columns.Count
    iPos = sdbgProce.Columns(NewIndex).Position
    iOrden = iPos - (((sdbgProce.Columns(NewIndex).Group - 1) * iNumColums))
    For i = 1 To sdbgProce.Groups.Count - 1
        If i <> sdbgProce.Columns(NewIndex).Group Then sdbgProce.Columns(sColumna & sdbgProce.Groups(i).TagVariant).Position = iOrden + (iNumColums * (i - 1))
    Next i
    'Se mueve tambi�n la columna en la grid sdbgprovegrupos
    sdbgProveGrupos.Groups(1).Columns(sColumna).Position = 2 + iOrden
    m_oVistaSeleccionada.HayCambios = True
End Sub

Private Sub sdbgProce_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
If sdbgProce.Columns(ColIndex).Group = 0 Then
    Cancel = True
    Exit Sub
End If
'Si el scroll no est� en la posici�n 0 no permite mover las columnas
If sdbgProce.GrpPosition(0) <> 0 Then
    Cancel = True
    Exit Sub
End If
End Sub

Private Sub sdbgProce_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
'Al redimensionar una columna la redimensiona el resto de grupos,
Dim i As Integer
Dim sColumna As String
Dim sProv As String
If m_oVistaSeleccionada Is Nothing Then Exit Sub
'Al redimensionar una columna tiene que redimensionar esa columna en todos los grupos
If sdbgProce.Columns(ColIndex).Group = sdbgProce.Groups(0).Position Then Exit Sub
'Obtenemos el proveedor cuya columna se est� cambiando de tama�o.Hay que tener en cuenta el scroll:
If sdbgProce.Groups(sdbgProce.Columns(ColIndex).Group).Position <> sdbgProce.Columns(ColIndex).Group Then
    sProv = sdbgProce.Groups(sdbgProce.GrpPosition(sdbgProce.Columns(ColIndex).Group)).TagVariant
Else
    sProv = sdbgProce.Groups(sdbgProce.Columns(ColIndex).Group).TagVariant
End If
'Obtiene el nombre de la columna
sColumna = Mid(sdbgProce.Columns(ColIndex).Name, 1, Len(sdbgProce.Columns(ColIndex).Name) - Len(sProv))
'Redimensiona tambi�n la columna en el resto de grupos de la grid
For i = 1 To sdbgProce.Groups.Count - 1
    If sdbgProce.Groups(i).TagVariant <> sProv Then sdbgProce.Columns(sColumna & sdbgProce.Groups(i).TagVariant).Width = sdbgProce.ResizeWidth
Next i
'Redimensiona tambi�n la columna correspondiente en la grid sdbgProveGrupos
sdbgProveGrupos.Groups(1).Columns(sColumna).Width = sdbgProce.ResizeWidth
m_oVistaSeleccionada.HayCambios = True
End Sub

Private Sub sdbgProce_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
    Dim i As Integer
    If m_oVistaSeleccionada Is Nothing Then Exit Sub
    If GrpIndex = 0 Then
        sdbgTotalesProve.Groups(0).Width = sdbgProce.ResizeWidth
        sdbgGruposProve.Groups(0).Width = sdbgProce.ResizeWidth
        
        sdbgtotales2.Groups(0).Width = sdbgProce.ResizeWidth
        sdbgProveGrupos.Groups(0).Width = sdbgProce.ResizeWidth
        Exit Sub
    End If
    'Redimensiona las grid de totales
    sdbgTotalesProve.Groups(GrpIndex).Width = sdbgProce.ResizeWidth
    'Redimensiona la grid de adjudicaciones
    sdbgGruposProve.Groups(GrpIndex).Width = sdbgProce.ResizeWidth
    'Redimensiona tambi�n el resto de grupos de la grid
    For i = 1 To sdbgGruposProve.Groups.Count - 1
        If i <> GrpIndex Then
            sdbgGruposProve.Groups(i).Width = sdbgProce.ResizeWidth
            sdbgTotalesProve.Groups(i).Width = sdbgProce.ResizeWidth
            sdbgProce.Groups(i).Width = sdbgProce.ResizeWidth
        End If
    Next i
    'Redimensiona tambi�n la grid de sdbgProveGrupos porque ambas deben ser iguales
    For i = 1 To sdbgProveGrupos.Groups.Count - 1
        sdbgProveGrupos.Groups(i).Width = sdbgProce.ResizeWidth
        sdbgtotales2.Groups(i).Width = sdbgProce.ResizeWidth
    Next i
    m_oVistaSeleccionada.HayCambios = True
End Sub

Private Sub sdbgProce_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    picControlVistaProceso.Visible = False
    picControlVistaAll.Visible = False
    picControlVistaGrupo.Visible = False
End Sub

Private Sub sdbgProveGrupos_AfterPosChanged(ByVal WhatChanged As Integer, ByVal NewIndex As Integer)
    Dim iNumColums As Integer
    Dim iPos As Integer
    Dim iOrden As Integer
    Dim i As Integer
    Dim sColumna As String
    Dim iNumColumnsGrupo1 As Integer
    'Cuando se mueve una columna de un grupo a otra posici�n se mueve en todos los grupos
    If sdbgProveGrupos.Columns(NewIndex).Group <= 0 Then Exit Sub
    If IsEmpty(sdbgProveGrupos.Groups(sdbgProveGrupos.Columns(NewIndex).Group).TagVariant) Then
        'Cuando se mueve una columna de un grupo a otra posici�n se mueve en todos los grupos de sdbgProce
        'Obtiene el nombre de la columna
        sColumna = sdbgProveGrupos.Columns(NewIndex).Name
        'Redimensiona tambi�n la columna en el resto de grupos de la grid
        iNumColums = sdbgProce.Groups(1).Columns.Count
        iPos = sdbgProveGrupos.Columns(NewIndex).Position - 2
        For i = 1 To sdbgProce.Groups.Count - 1
            sdbgProce.Columns(sColumna & sdbgProce.Groups(i).TagVariant).Position = iPos + (iNumColums * (i - 1))
        Next i
        Exit Sub
    End If
    'Es un campo normal o un atributo de grupo
    'Obtiene el nombre de la columna
    sColumna = Left(sdbgProveGrupos.Columns(NewIndex).Name, Len(sdbgProveGrupos.Columns(NewIndex).Name) - Len(sdbgProveGrupos.Groups(sdbgProveGrupos.Columns(NewIndex).Group).TagVariant))
    'Redimensiona tambi�n la columna en el resto de grupos de la grid
    iNumColums = sdbgProveGrupos.Groups(2).Columns.Count
    iPos = sdbgProveGrupos.Columns(NewIndex).Position
    iNumColumnsGrupo1 = sdbgProveGrupos.Groups(1).Columns.Count
    If sdbgProveGrupos.GrpPosition(0) > 0 Then
        iOrden = (iPos + (iNumColums * sdbgProveGrupos.GrpPosition(0))) - ((sdbgProveGrupos.Columns(NewIndex).Group * iNumColums) + 2 + iNumColumnsGrupo1)
    Else
        iOrden = (iPos + (iNumColums * sdbgProveGrupos.GrpPosition(0))) - (((sdbgProveGrupos.Columns(NewIndex).Group - 2) * iNumColums) + 2 + iNumColumnsGrupo1)
    End If
    For i = 2 To sdbgProveGrupos.Groups.Count - 1
        If i <> sdbgProveGrupos.Columns(NewIndex).Group Then sdbgProveGrupos.Columns(sColumna & sdbgProveGrupos.Groups(i).TagVariant).Position = 2 + iOrden + iNumColumnsGrupo1 + (iNumColums * (i - 2))
    Next i
    'Redimensiona tambi�n en sdbgGruposProve porque ambas deben ser iguales
    For i = 1 To sdbgGruposProve.Groups.Count - 1
        sdbgGruposProve.Columns(sColumna & sdbgGruposProve.Groups(i).TagVariant).Position = 2 + iOrden + (iNumColums * (i - 1))
    Next i
    m_oVistaSeleccionada.HayCambios = True
End Sub

Private Sub sdbgProveGrupos_ColMove(ByVal ColIndex As Integer, ByVal NewPos As Integer, Cancel As Integer)
    'Al redimensionar una columna tiene que redimensionar esa columna en todos los grupos
    If sdbgProveGrupos.Columns(ColIndex).Group = 0 Then
        Cancel = True
        Exit Sub
    End If
    'Si el scroll no est� en la posici�n 0 no permite mover las columnas
    If sdbgProveGrupos.GrpPosition(0) <> 0 Then
        Cancel = True
        Exit Sub
    End If
End Sub

''' <summary>
''' Al redimensionar una columna tiene que redimensionar esa columna en todos los grupos
''' </summary>
''' <param name="ColIndex">columna</param>
''' <param name="Cancel">Cancel el redimensionar</param>
''' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgProveGrupos_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
    Dim i As Integer
    Dim sColumna As String
    Dim sProv As String
With sdbgProveGrupos
    'Al redimensionar una columna tiene que redimensionar esa columna en todos los grupos
    If m_oVistaSeleccionada Is Nothing Then Exit Sub
    'Si la columna pertenece al grupo 0 se pasan tambi�n los cambios a sdbgGruposProve
    If .Columns(ColIndex).Group = .Groups(0).Position Then
        sColumna = .Columns(ColIndex).Name
        Select Case sColumna
            Case "COD"
                sdbgGruposProve.Columns("COD").Width = .ResizeWidth
            Case "DEN"
                sdbgGruposProve.Columns("NOMBRE").Width = .ResizeWidth
        End Select
        m_oVistaSeleccionada.HayCambios = True
        Exit Sub
    End If
    'Obtenemos el proveedor cuya columna se est� cambiando de tama�o.Hay que tener en cuenta el scroll:
    If .Groups(.Columns(ColIndex).Group).Position <> .Columns(ColIndex).Group Then
        sProv = .Groups(.GrpPosition(.Columns(ColIndex).Group)).TagVariant
    Else
        sProv = .Groups(.Columns(ColIndex).Group).TagVariant
    End If
    'Obtiene el nombre de la columna
    sColumna = Mid(.Columns(ColIndex).Name, 1, Len(.Columns(ColIndex).Name) - Len(sProv))
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        If Not g_oPlantillaSeleccionada.Atributosvistas.Item(sColumna) Is Nothing Then
            'Redimensiona tambi�n la grid sdbgProce
            For i = 1 To sdbgProce.Groups.Count - 1
                sdbgProce.Columns(sColumna & sdbgProce.Groups(i).TagVariant).Width = .ResizeWidth
            Next i
            Exit Sub
        End If
    End If
    If g_oPlantillaSeleccionada.Solicitud = EnProceso Then
        'Redimensiona tambi�n la grid sdbgProce. Solo VINCULADO_PRO. los atribs bucle anterior.
        If sColumna = "VINCULADO_PRO" Then
            For i = 1 To sdbgProce.Groups.Count - 1
                sdbgProce.Columns(sColumna & sdbgProce.Groups(i).TagVariant).Width = .ResizeWidth
            Next i
            Exit Sub
        End If
    End If
    'Redimensiona tambi�n la columna en el resto de grupos de la grid
    For i = 2 To .Groups.Count - 1
        If i <> .Columns(ColIndex).Group Then
            If .Groups(i).TagVariant <> sProv Then
                .Columns(sColumna & .Groups(i).TagVariant).Width = .ResizeWidth
            End If
        End If
    Next i
    'Tiene que redimensionar tambi�n en la grid sdbgGruposProve,porque ambas tienen que ser iguales:
    For i = 1 To sdbgGruposProve.Groups.Count - 1
        sdbgGruposProve.Columns(sColumna & sdbgGruposProve.Groups(i).TagVariant).Width = .ResizeWidth
    Next i
    m_oVistaSeleccionada.HayCambios = True
End With
End Sub

Private Sub sdbgProveGrupos_GrpResize(ByVal GrpIndex As Integer, Cancel As Integer)
Dim i As Integer
If m_oVistaSeleccionada Is Nothing Then Exit Sub
If GrpIndex = 0 Then  'Si es el 1� grupo
    sdbgtotales2.Groups(0).Width = sdbgProveGrupos.ResizeWidth
    sdbgProce.Groups(0).Width = sdbgProveGrupos.ResizeWidth
    sdbgGruposProve.Groups(0).Width = sdbgProveGrupos.ResizeWidth
    m_oVistaSeleccionada.HayCambios = True
    Exit Sub
End If
'Redimensiona la grid de totales
sdbgtotales2.Groups(GrpIndex).Width = sdbgProveGrupos.ResizeWidth
'Redimensiona tambi�n el resto de grupos de la grid
For i = 1 To sdbgProveGrupos.Groups.Count - 1
    If i <> GrpIndex Then
        sdbgProveGrupos.Groups(i).Width = sdbgProveGrupos.ResizeWidth
        sdbgtotales2.Groups(i).Width = sdbgProveGrupos.ResizeWidth
    End If
Next i
For i = 1 To sdbgGruposProve.Groups.Count - 1
    sdbgGruposProve.Groups(i).Width = sdbgProveGrupos.ResizeWidth
    sdbgTotalesProve.Groups(i).Width = sdbgProveGrupos.ResizeWidth
Next i
'Tiene que redimensionar tambi�n en la grid sdbgProce,porque ambas tienen que ser iguales:
For i = 1 To sdbgProce.Groups.Count - 1
    sdbgProce.Groups(i).Width = sdbgProveGrupos.ResizeWidth
Next i
m_oVistaSeleccionada.HayCambios = True
End Sub

Private Sub sdbgProveGrupos_InitColumnProps()
sdbgProveGrupos.SplitterVisible = False
sdbgtotales2.SplitterVisible = False
End Sub

Private Sub sdbgProveGrupos_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
picControlVistaProceso.Visible = False
picControlVistaAll.Visible = False
picControlVistaGrupo.Visible = False
End Sub

Private Sub sdbgProveGrupos_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
Dim sGrupo As String
Dim oatrib As CAtributo
For i = 1 To sdbgProveGrupos.Groups.Count - 1
    If Not IsEmpty(sdbgProveGrupos.Groups(i).TagVariant) Then
        'comprueba si el atributo es del grupo actual para cada uno de las columnas de atributos:
        sGrupo = sdbgProveGrupos.Groups(i).TagVariant
        If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
            For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
                If oatrib.ambito = AmbGrupo Then
                    If oatrib.codgrupo <> sGrupo And Not IsNull(oatrib.codgrupo) Then
                        sdbgProveGrupos.Columns(oatrib.Cod & sdbgProveGrupos.Groups(i).TagVariant).CellStyleSet "GrayHead", sdbgProveGrupos.Row
                    Else
                        sdbgProveGrupos.Columns(oatrib.Cod & sdbgProveGrupos.Groups(i).TagVariant).CellStyleSet "Normal", sdbgProveGrupos.Row
                    End If
                End If
            Next
        End If
    End If
Next i
End Sub

Private Sub sdbgProveGrupos_RowResize(Cancel As Integer)
If m_oVistaSeleccionada Is Nothing Then Exit Sub
m_oVistaSeleccionada.HayCambios = True
'El ancho de fila de la grid de sdbgGruposProve tiene que ser el mismo
sdbgGruposProve.RowHeight = sdbgProveGrupos.ResizeHeight
End Sub

Private Sub sdbgProveGrupos_SplitterMove(Cancel As Integer)
    Cancel = True
End Sub

Private Sub sdbgSobres_Change()
Dim lSobre As Long
'Oculta o visualiza los grupos correspondientes a cada sobre

lSobre = sdbgSobres.Columns("ID").Value
If sdbgSobres.Columns("MOSTRAR").Value = "-1" Then
    'Hace visible los grupos correspondientes al sobre
    m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(sdbgSobres.Columns("ID").Value)).Visible = True
    VisualizarSobres lSobre, True
Else
    'Oculta los grupos correspondientes al sobre
    m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(sdbgSobres.Columns("ID").Value)).Visible = False
    VisualizarSobres lSobre, False
End If
'Carga los grupos en el tab
CargarSobresEnTab
End Sub

Private Sub sstabComparativa_BeforeClick(Cancel As Integer)
If sstabComparativa.selectedItem.Tag = "General" Then
    If Not m_oVistaSeleccionada Is Nothing Then GuardarVistaGeneral
ElseIf sstabComparativa.selectedItem.Tag = "ALL" Then
    If Not m_oVistaSeleccionadaAll Is Nothing Then GuardarVistaAll
Else
    If Not m_oVistaSeleccionadaGr Is Nothing Then GuardarVistaGr
End If
End Sub

Private Sub sstabComparativa_Click()
Dim i As Integer
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
'Si seleccionamos el mismo tab que en el que nos encontramos:
If sstabComparativa.selectedItem.Tag = sstabComparativa.Tag Then Exit Sub
'bloqueo el tab hasta que acabe la carga
sstabComparativa.Enabled = False
Me.Enabled = False
Screen.MousePointer = vbHourglass
LockWindowUpdate Me.hWnd
'sino cambiamos de tab:
If sstabComparativa.selectedItem.Tag = "General" Then  'Tab general
    'Si se ha hecho un scroll se deja la grid en la posici�n original
    If sdbgGruposProve.GrpPosition(0) > 0 Then
        i = sdbgGruposProve.Groups(0).Position
        sdbgGruposProve.Scroll -i, 0
        sdbgGruposProve.Update
        sdbgTotalesProve.Scroll -i, 0
        sdbgTotalesProve.Update
    End If
    If sdbgProveGrupos.GrpPosition(0) > 0 Then
        i = sdbgProveGrupos.Groups(0).Position
        sdbgProveGrupos.Scroll -i, 0
        sdbgProveGrupos.Update
        sdbgtotales2.Scroll -i, 0
        sdbgtotales2.Update
    End If
    If sdbgProce.GrpPosition(0) > 0 Then
        i = sdbgProce.Groups(0).Position
        sdbgProce.Scroll -i, 0
        sdbgProce.Update
    End If
    For i = 0 To sdbgGruposProve.Groups.Count - 1
        sdbgGruposProve.Groups(i).Position = i
    Next i
    For i = 0 To sdbgProce.Groups.Count - 1
        sdbgProce.Groups(i).Position = i
    Next i
    For i = 0 To sdbgProveGrupos.Groups.Count - 1
        sdbgProveGrupos.Groups(i).Position = i
    Next i
    sdbgGruposProve.AllowColumnMoving = ssRelocateWithinGroup
    sdbgProveGrupos.AllowColumnMoving = ssRelocateWithinGroup
    sdbgProce.AllowColumnMoving = ssRelocateWithinGroup
    'Hace visibles las grids correspondientes
    If m_oVistaSeleccionada.TipoVision = 0 Then
        sdbgGruposProve.Visible = True
        sdbgTotalesProve.Visible = True
        sdbgProce.Groups(0).Width = sdbgGruposProve.Groups(0).Width
    Else
        sdbgProveGrupos.Visible = True
        sdbgtotales2.Visible = True
        sdbgProce.Groups(0).Width = sdbgProveGrupos.Groups(0).Width
    End If
    If m_bGridProcVisible = True Then
        If m_oVistaSeleccionada.TipoVision = 0 Then
            sdbgProce.Visible = True
        Else
            sdbgProce.Visible = False
        End If
        sdbgProveGrupos.Groups(1).Visible = True
        sdbgtotales2.Groups(1).Visible = True
    Else
        sdbgProce.Visible = False
        sdbgProveGrupos.Groups(1).Visible = False
        sdbgtotales2.Groups(1).Visible = False
    End If
    'Hace invisibles las grids del resto de tabs
    sdbgAdj.Visible = False
    sdbgTotales.Visible = False
    sdbgAll.Visible = False
    sdbgTotalesAll.Visible = False
    '''ProcesoSeleccionado
    cmdInvertir.Enabled = True
    If g_oPlantillaSeleccionada Is Nothing Then
        'bloqueo el tab hasta que acabe la carga
        sstabComparativa.Enabled = True
        Me.Enabled = True
        Screen.MousePointer = vbNormal
        LockWindowUpdate 0&
        Exit Sub
    End If
    GeneralSeleccionado
    Me.Enabled = True  'Para saber si hay que cargar o no la grid
ElseIf sstabComparativa.selectedItem.Tag = "ALL" Then
    'Si se ha hecho un scroll se deja la grid en la posici�n original
    If sdbgAll.GrpPosition(0) > 0 Then
        i = sdbgAll.Groups(0).Position
        sdbgAll.Scroll -i, 0
        sdbgAll.Update
        sdbgTotalesAll.Scroll -i, 0
        sdbgTotalesAll.Update
    End If
    For i = 0 To sdbgAll.Groups.Count - 1
        sdbgAll.Groups(i).Position = i
    Next i
    For i = 0 To sdbgTotalesAll.Columns.Count - 1
        sdbgTotalesAll.Columns(i).Position = i
    Next i
    sdbgAll.Visible = True
    sdbgTotalesAll.Visible = True
    sdbgAll.AllowColumnMoving = ssRelocateWithinGroup
    'Hace invisibles las grids del resto de tabs
    sdbgAdj.Visible = False
    sdbgTotales.Visible = False
    sdbgProveGrupos.Visible = False
    sdbgGruposProve.Visible = False
    sdbgtotales2.Visible = False
    sdbgTotalesProve.Visible = False
    sdbgProce.Visible = False
    Set m_oGrupoSeleccionado = Nothing
    'Inhabilita el bot�n de invertir
    cmdInvertir.Enabled = False
    AllSeleccionado
Else    'A nivel del grupo
    'Si se ha hecho un scroll se deja la grid en la posici�n original
    If sdbgAdj.GrpPosition(0) > 0 Then
        i = sdbgAdj.Groups(0).Position
        sdbgAdj.Scroll -i, 0
        sdbgAdj.Update
        sdbgTotales.Scroll -i, 0
        sdbgTotales.Update
    End If
    For i = 0 To sdbgAdj.Groups.Count - 1
        sdbgAdj.Groups(i).Position = i
    Next i
    For i = 0 To sdbgTotales.Columns.Count - 1
        sdbgTotales.Columns(i).Position = i
    Next i
    sdbgAdj.Visible = True
    sdbgTotales.Visible = True
    sdbgAdj.AllowColumnMoving = ssRelocateWithinGroup
    'Hace invisibles las grids del resto de tabs
    sdbgAll.Visible = False
    sdbgTotalesAll.Visible = False
    sdbgProveGrupos.Visible = False
    sdbgGruposProve.Visible = False
    sdbgtotales2.Visible = False
    sdbgTotalesProve.Visible = False
    sdbgProce.Visible = False
    'grupo seleccionado por el tab:
    If Not m_oGrupoSeleccionado Is Nothing Then
        If Not (m_oGrupoSeleccionado.Codigo = g_oPlantillaSeleccionada.Grupos.Item(sstabComparativa.selectedItem.Tag).Codigo) Then Set m_oGrupoSeleccionado = g_oPlantillaSeleccionada.Grupos.Item(sstabComparativa.selectedItem.Tag)
    Else
        Set m_oGrupoSeleccionado = g_oPlantillaSeleccionada.Grupos.Item(sstabComparativa.selectedItem.Tag)
    End If
    'Inhabilita el bot�n de invertir
    cmdInvertir.Enabled = False
    GrupoSeleccionado
End If
'una vez que est� cargado vuelve a habilitar el tab
sstabComparativa.Enabled = True
Me.Enabled = True
sstabComparativa.Tag = sstabComparativa.selectedItem.Tag
LockWindowUpdate 0&
Screen.MousePointer = vbNormal
End Sub

Private Sub GuardarVistas()
If Not m_oVistaSeleccionada Is Nothing Then GuardarVistaGeneralEnBd
If Not m_oVistaSeleccionadaAll Is Nothing Then GuardarVistaAllEnBd
If Not m_oVistaSeleccionadaGr Is Nothing Then GuardarVistaGrEnBd
oMensajes.MensajeOKOnly 821, TipoIconoMensaje.Information
End Sub

''' <summary>Guarda la configuraci�n de la vista para el grupo ALL</summary>
''' <remarks>Llamada desde: cmdGuardarVistaAll_Click, cmdGuardarVistaNuevaAll_Click, sstabComparativa_BeforeClick; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarVistaAll()
Dim oAtribVista  As CPlantConfVistaAllAtrib
Dim iNumColProv As Long
Dim iNumColsGrupo As Integer
Dim i As Integer
Dim iGrupoVisible As Integer
With m_oVistaSeleccionadaAll
    'N� de columnas de los grupos de proveedores.Es necesario para obtener la posici�n de las celdas
    'por si se ha hecho scroll.Hay que hacerlo de esta forma,sino no funciona:
    iNumColProv = sdbgAll.Groups(2).Columns.Count
    If sdbgAll.GrpPosition(0) > 0 Then
        i = sdbgAll.Groups(0).Position
        sdbgAll.Scroll -i, 0
        sdbgAll.Update
        sdbgTotalesAll.Scroll -i, 0
        sdbgTotalesAll.Update
    End If
    For i = 0 To sdbgAll.Groups.Count - 1
        sdbgAll.Groups(i).Position = i
    Next i
    For i = 0 To sdbgTotalesAll.Columns.Count - 1
        sdbgTotalesAll.Columns(i).Position = i
    Next i
    sdbgAll.AllowColumnMoving = ssRelocateWithinGroup
    'Obtiene cual es el 1� proveedor que est� visible
    For iGrupoVisible = 1 To sdbgAll.Groups.Count - 1
        If sdbgAll.Groups(iGrupoVisible).Visible = True Then Exit For
    Next iGrupoVisible
    If iGrupoVisible = sdbgAll.Groups.Count Then Exit Sub
    If sdbgAll.LevelCount = 1 Then
        .OcultarFila2 = True
    Else
        .OcultarFila2 = False
    End If
    .Grupo0Width = sdbgAll.Groups(0).Width
    .GruposWidth = sdbgAll.Groups(2).Width
    'almacena los widths de las columnas
    .DescrWidth = sdbgAll.Columns("DESCR").Width
    If .OcultarFila2 = True Then
        If sdbgAll.Columns("ANYO").Visible = True Then
            .AnyoWidth = sdbgAll.Columns("ANYO").Width
            .AnyoLevel = 0
            .AnyoPos = sdbgAll.Columns("ANYO").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("PROV").Visible = True Then
            .ProveedorWidth = sdbgAll.Columns("PROV").Width
            .ProveedorLevel = 0
            .ProveedorPos = sdbgAll.Columns("PROV").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("PRECAPE").Visible = True Then
            .PresUniWidth = sdbgAll.Columns("PRECAPE").Width
            .PresUniLevel = 0
            .PresUniPos = sdbgAll.Columns("PRECAPE").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("OBJ").Visible = True Then
            .OBJWidth = sdbgAll.Columns("OBJ").Width
            .OBJLevel = 0
            .OBJPos = sdbgAll.Columns("OBJ").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("AHORROIMP").Visible = True Then
            .AhorroWidth = sdbgAll.Columns("AHORROIMP").Width
            .AhorroLevel = 0
            .AhorroPos = sdbgAll.Columns("AHORROIMP").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("AHORROPORCEN").Visible = True Then
            .AhorroPorcenWidth = sdbgAll.Columns("AHORROPORCEN").Width
            .AhorroPorcenLevel = 0
            .AhorroPorcenPos = sdbgAll.Columns("AHORROPORCEN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("IMP").Visible = True Then
            .ImporteWidth = sdbgAll.Columns("IMP").Width
            .ImporteLevel = 0
            .ImportePos = sdbgAll.Columns("IMP").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("CANT").Visible = True Then
            .CantidadWidth = sdbgAll.Columns("CANT").Width
            .CantidadLevel = 0
            .CantidadPos = sdbgAll.Columns("CANT").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("PORCENADJ").Visible = True Then
            .AdjWidth = sdbgAll.Columns("PORCENADJ").Width
            .AdjLevel = 0
            .AdjPos = sdbgAll.Columns("PORCENADJ").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        End If
        If sdbgAll.Columns("VINCULADO_ITEM").Visible = True Then
            .SolicVinculadaWidth = sdbgAll.Columns("VINCULADO_ITEM").Width
            .SolicVinculadaLevel = 0
            .SolicVinculadaPos = sdbgAll.Columns("VINCULADO_ITEM").Position - (sdbgAll.Groups(0).Position * iNumColProv)
            .SolicVinculadaVisible = sdbgAll.Columns("VINCULADO_ITEM").Visible
        End If
        If sdbgAll.Columns("INI").Visible = True Then
            .FecIniSumWidth = sdbgAll.Columns("INI").Width
            .FecIniSumLevel = 0
            .FecIniSumPos = sdbgAll.Columns("INI").Position - (sdbgAll.Groups(0).Position * iNumColProv)
            .FecIniSumVisible = sdbgAll.Columns("INI").Visible
        End If
        If sdbgAll.Columns("FIN").Visible = True Then
            .FecFinSumWidth = sdbgAll.Columns("FIN").Width
            .FecFinSumLevel = 0
            .FecFinSumPos = sdbgAll.Columns("FIN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
            .FecFinSumVisible = sdbgAll.Columns("FIN").Visible
        End If
        If sdbgAll.Columns("GMN").Visible = True Then
            .EstrMatWidth = sdbgAll.Columns("GMN").Width
            .EstrMatLevel = 0
            .EstrMatPos = sdbgAll.Columns("GMN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
            .EstrMatVisible = sdbgAll.Columns("GMN").Visible
        End If
    Else '2� fila visible
        'almacena el width de las columnas
        .AnyoWidth = sdbgAll.Columns("ANYO").Width
        .ProveedorWidth = sdbgAll.Columns("PROV").Width
        .PresUniWidth = sdbgAll.Columns("PRECAPE").Width
        .OBJWidth = sdbgAll.Columns("OBJ").Width
        .AhorroWidth = sdbgAll.Columns("AHORROIMP").Width
        .AhorroPorcenWidth = sdbgAll.Columns("AHORROPORCEN").Width
        .ImporteWidth = sdbgAll.Columns("IMP").Width
        .CantidadWidth = sdbgAll.Columns("CANT").Width
        .AdjWidth = sdbgAll.Columns("PORCENADJ").Width
        .SolicVinculadaWidth = sdbgAll.Columns("VINCULADO_ITEM").Width
        .FecIniSumWidth = sdbgAll.Columns("INI").Width
        .FecFinSumWidth = sdbgAll.Columns("FIN").Width
        .EstrMatWidth = sdbgAll.Columns("GMN").Width
        'almacena los position de las columnas
        .AnyoPos = sdbgAll.Columns("ANYO").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .ProveedorPos = sdbgAll.Columns("PROV").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .PresUniPos = sdbgAll.Columns("PRECAPE").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .OBJPos = sdbgAll.Columns("OBJ").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .AhorroPos = sdbgAll.Columns("AHORROIMP").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .AhorroPorcenPos = sdbgAll.Columns("AHORROPORCEN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .ImportePos = sdbgAll.Columns("IMP").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .CantidadPos = sdbgAll.Columns("CANT").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .AdjPos = sdbgAll.Columns("PORCENADJ").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .SolicVinculadaPos = sdbgAll.Columns("VINCULADO_ITEM").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .FecIniSumPos = sdbgAll.Columns("INI").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .FecFinSumPos = sdbgAll.Columns("FIN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        .EstrMatPos = sdbgAll.Columns("GMN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
        'almacena los levels de las columnas
        .AnyoLevel = sdbgAll.Columns("ANYO").Level
        .ProveedorLevel = sdbgAll.Columns("PROV").Level
        .PresUniLevel = sdbgAll.Columns("PRECAPE").Level
        .OBJLevel = sdbgAll.Columns("OBJ").Level
        .AhorroLevel = sdbgAll.Columns("AHORROIMP").Level
        .AhorroPorcenLevel = sdbgAll.Columns("AHORROPORCEN").Level
        .ImporteLevel = sdbgAll.Columns("IMP").Level
        .CantidadLevel = sdbgAll.Columns("CANT").Level
        .AdjLevel = sdbgAll.Columns("PORCENADJ").Level
        .SolicVinculadaLevel = sdbgAll.Columns("VINCULADO_ITEM").Level
        .FecIniSumLevel = sdbgAll.Columns("INI").Level
        .FecFinSumLevel = sdbgAll.Columns("FIN").Level
        .EstrMatLevel = sdbgAll.Columns("GMN").Level
        .SolicVinculadaVisible = sdbgAll.Columns("VINCULADO_ITEM").Visible
    End If
    'Los campos correspondientes a cada prov.:importe adjudicado,Cant.adjudicada,% adjudicado y precio adjudicado
    If sdbgAll.Groups.Count > 2 Then
        iNumColsGrupo = sdbgAll.Groups(iGrupoVisible).Columns.Count
        If .OcultarFila2 = False Then
            .ImpAdjPos = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            .CantProvPos = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            .AdjProvPos = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            .PrecioProvPos = sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
            .ImpAdjLevel = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            .CantProvLevel = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            .AdjProvLevel = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            .PrecioProvLevel = sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            .ImpAdjWidth = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
            .CantProvWidth = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
            .AdjProvWidth = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
            .PrecioProvWidth = sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
        Else
            If IsNull(.ImpAdjPos) Then
                .ImpAdjPos = 1
            ElseIf sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .ImpAdjPos = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
                .ImpAdjWidth = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .ImpAdjLevel = 0
            End If
            If IsNull(.CantProvPos) Then
                .CantProvPos = 8
            ElseIf sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .CantProvPos = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
                .CantProvWidth = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .CantProvLevel = 0
            End If
            If IsNull(.AdjProvPos) Then
                .AdjProvPos = 9
            ElseIf sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .AdjProvPos = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
                .AdjProvWidth = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .AdjProvLevel = 0
            End If
            If IsNull(.PrecioProvPos) Then
                .PrecioProvPos = 2
            ElseIf sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .PrecioProvPos = sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
                .PrecioProvWidth = sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .PrecioProvLevel = 0
            End If
        End If
        'almacena los width de los atributos
        For Each oAtribVista In .PlantConfVistasAllAtrib
            If .OcultarFila2 = False Then
                oAtribVista.Posicion = sdbgAll.Columns(oAtribVista.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
                oAtribVista.Fila = sdbgAll.Columns(oAtribVista.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
                oAtribVista.Width = sdbgAll.Columns(oAtribVista.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
            Else
                If IsNull(oAtribVista.Posicion) Then
                    oAtribVista.Posicion = 1
                    oAtribVista.Fila = 0
                ElseIf sdbgAll.Columns(oAtribVista.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                    oAtribVista.Posicion = sdbgAll.Columns(oAtribVista.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsAll - (iNumColsGrupo * (iGrupoVisible - 2))
                    oAtribVista.Fila = 0
                    oAtribVista.Width = sdbgAll.Columns(oAtribVista.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                End If
            End If
        Next
    End If
    'Almacena el ancho de la fila
    .AnchoFila = sdbgAll.RowHeight
    .ExcluirItCerradosResult = chkExcluirItCerradosResulAll.Value
    .OcultarNoAdj = chkOcultarNoAdjAll.Value
    .OcultarItCerrados = chkOcultarItCerradosAll.Value
    .OcultarProvSinOfe = chkOcultarProvSinOfeAll.Value
    'Almacena el n�mero de decimales:
    .DecCant = txtDecCantAll.Text
    .DecPorcen = txtDecPorcenAll.Text
    .DecPrecios = txtDecPrecAll.Text
    .DecResult = txtDecResultAll.Text
End With
End Sub

''' <summary>Guarda la configuraci�n de la vista para el grupo actual</summary>
''' <remarks>Llamada desde: cmdGuardarVistaGr_Click, cmdGuardarVistaNuevaGr_Click, sstabComparativa_BeforeClick; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarVistaGr()
Dim oAtribVista As CPlantConfVistaGrupoAtrib
Dim iNumColProv As Long
Dim iNumColsGrupo As Integer
Dim i As Integer
Dim iGrupoVisible As Integer
With m_oVistaSeleccionadaGr
    'N� de columnas de los grupos de proveedores.Es necesario para obtener la posici�n de las celdas
    'por si se ha hecho scroll.Hay que hacerlo de esta forma,sino no funciona:
    iNumColProv = sdbgAdj.Groups(2).Columns.Count
    If sdbgAdj.GrpPosition(0) > 0 Then
        i = sdbgAdj.Groups(0).Position
        sdbgAdj.Scroll -i, 0
        sdbgAdj.Update
        sdbgTotales.Scroll -i, 0
        sdbgTotales.Update
    End If
    For i = 0 To sdbgAdj.Groups.Count - 1
        sdbgAdj.Groups(i).Position = i
    Next i
    For i = 0 To sdbgTotales.Columns.Count - 1
        sdbgTotales.Columns(i).Position = i
    Next i
    sdbgAdj.AllowColumnMoving = ssRelocateWithinGroup
    iGrupoVisible = 2
    If sdbgAdj.LevelCount = 1 Then
        .OcultarFila2 = True
    Else
        .OcultarFila2 = False
    End If
    .Grupo0Width = sdbgAdj.Groups(0).Width
    .GruposWidth = sdbgAdj.Groups(2).Width
    'almacena los widths de las columnas
    .DescrWidth = sdbgAdj.Columns("DESCR").Width
    If .OcultarFila2 = True Then  '2� fila oculta
        If sdbgAdj.Columns("ANYO").Visible = True Then
            .AnyoWidth = sdbgAdj.Columns("ANYO").Width
            .AnyoPos = sdbgAdj.Columns("ANYO").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .AnyoLevel = 0
        End If
        If sdbgAdj.Columns("PROV").Visible = True Then
            .ProveedorWidth = sdbgAdj.Columns("PROV").Width
            .ProveedorPos = sdbgAdj.Columns("PROV").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .ProveedorLevel = 0
        End If
        If sdbgAdj.Columns("PRECAPE").Visible = True Then
            .PresUniWidth = sdbgAdj.Columns("PRECAPE").Width
            .PresUniPos = sdbgAdj.Columns("PRECAPE").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .PresUniLevel = 0
        End If
        If sdbgAdj.Columns("OBJ").Visible = True Then
            .OBJWidth = sdbgAdj.Columns("OBJ").Width
            .OBJPos = sdbgAdj.Columns("OBJ").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .OBJLevel = 0
        End If
        If sdbgAdj.Columns("AHORROIMP").Visible = True Then
            .AhorroWidth = sdbgAdj.Columns("AHORROIMP").Width
            .AhorroPos = sdbgAdj.Columns("AHORROIMP").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .AhorroLevel = 0
        End If
        If sdbgAdj.Columns("AHORROPORCEN").Visible = True Then
            .AhorroPorcenWidth = sdbgAdj.Columns("AHORROPORCEN").Width
            .AhorroPorcenPos = sdbgAdj.Columns("AHORROPORCEN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .AhorroPorcenLevel = 0
        End If
        If sdbgAdj.Columns("IMP").Visible = True Then
            .ImporteWidth = sdbgAdj.Columns("IMP").Width
            .ImportePos = sdbgAdj.Columns("IMP").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .ImporteLevel = 0
        End If
        If sdbgAdj.Columns("CANT").Visible = True Then
            .CantidadWidth = sdbgAdj.Columns("CANT").Width
            .CantidadPos = sdbgAdj.Columns("CANT").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .CantidadLevel = 0
        End If
        If sdbgAdj.Columns("PORCENADJ").Visible = True Then
            .AdjWidth = sdbgAdj.Columns("PORCENADJ").Width
            .AdjPos = sdbgAdj.Columns("PORCENADJ").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .AdjLevel = 0
        End If
        If sdbgAdj.Columns("VINCULADO_ITEM").Visible = True Then
            .SolicVinculadaWidth = sdbgAdj.Columns("VINCULADO_ITEM").Width
            .SolicVinculadaPos = sdbgAdj.Columns("VINCULADO_ITEM").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .SolicVinculadaLevel = 0
            .SolicVinculadaVisible = sdbgAdj.Columns("VINCULADO_ITEM").Visible
        End If
        If sdbgAdj.Columns("INI").Visible Then
            .FecIniSumWidth = sdbgAdj.Columns("INI").Width
            .FecIniSumPos = sdbgAdj.Columns("INI").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .FecIniSumLevel = 0
            .FecIniSumVisible = sdbgAdj.Columns("INI").Visible
        End If
        If sdbgAdj.Columns("FIN").Visible Then
            .FecFinSumWidth = sdbgAdj.Columns("FIN").Width
            .FecFinSumPos = sdbgAdj.Columns("FIN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .FecFinSumLevel = 0
            .FecFinSumVisible = sdbgAdj.Columns("FIN").Visible
        End If
        If sdbgAdj.Columns("GMN").Visible Then
            .EstrMatWidth = sdbgAdj.Columns("GMN").Width
            .EstrMatPos = sdbgAdj.Columns("GMN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
            .EstrMatLevel = 0
            .EstrMatVisible = sdbgAdj.Columns("GMN").Visible
        End If
    Else '2� fila no oculta
        'almacena los widths de las columnas
        .AnyoWidth = sdbgAdj.Columns("ANYO").Width
        .ProveedorWidth = sdbgAdj.Columns("PROV").Width
        .PresUniWidth = sdbgAdj.Columns("PRECAPE").Width
        .OBJWidth = sdbgAdj.Columns("OBJ").Width
        .AhorroWidth = sdbgAdj.Columns("AHORROIMP").Width
        .AhorroPorcenWidth = sdbgAdj.Columns("AHORROPORCEN").Width
        .ImporteWidth = sdbgAdj.Columns("IMP").Width
        .CantidadWidth = sdbgAdj.Columns("CANT").Width
        .AdjWidth = sdbgAdj.Columns("PORCENADJ").Width
        .SolicVinculadaWidth = sdbgAdj.Columns("VINCULADO_ITEM").Width
        .FecIniSumWidth = sdbgAdj.Columns("INI").Width
        .FecFinSumWidth = sdbgAdj.Columns("FIN").Width
        .EstrMatWidth = sdbgAdj.Columns("GMN").Width
        'almacena los position de las columnas
        .AnyoPos = sdbgAdj.Columns("ANYO").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .ProveedorPos = sdbgAdj.Columns("PROV").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .PresUniPos = sdbgAdj.Columns("PRECAPE").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .OBJPos = sdbgAdj.Columns("OBJ").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .AhorroPos = sdbgAdj.Columns("AHORROIMP").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .AhorroPorcenPos = sdbgAdj.Columns("AHORROPORCEN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .ImportePos = sdbgAdj.Columns("IMP").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .CantidadPos = sdbgAdj.Columns("CANT").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .AdjPos = sdbgAdj.Columns("PORCENADJ").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .SolicVinculadaPos = sdbgAdj.Columns("VINCULADO_ITEM").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .FecIniSumPos = sdbgAdj.Columns("INI").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .FecFinSumPos = sdbgAdj.Columns("FIN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        .EstrMatPos = sdbgAdj.Columns("GMN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
        'almacena los levels de las columnas
        .AnyoLevel = sdbgAdj.Columns("ANYO").Level
        .ProveedorLevel = sdbgAdj.Columns("PROV").Level
        .PresUniLevel = sdbgAdj.Columns("PRECAPE").Level
        .OBJLevel = sdbgAdj.Columns("OBJ").Level
        .AhorroLevel = sdbgAdj.Columns("AHORROIMP").Level
        .AhorroPorcenLevel = sdbgAdj.Columns("AHORROPORCEN").Level
        .ImporteLevel = sdbgAdj.Columns("IMP").Level
        .CantidadLevel = sdbgAdj.Columns("CANT").Level
        .AdjLevel = sdbgAdj.Columns("PORCENADJ").Level
        .SolicVinculadaLevel = sdbgAdj.Columns("VINCULADO_ITEM").Level
        .FecIniSumLevel = sdbgAdj.Columns("INI").Level
        .FecFinSumLevel = sdbgAdj.Columns("FIN").Level
        .EstrMatLevel = sdbgAdj.Columns("GMN").Level
        .SolicVinculadaVisible = sdbgAdj.Columns("VINCULADO_ITEM").Visible
    End If
    'Los campos correspondientes a cada prov.:importe adjudicado,Cant.adjudicada,% adjudicado y precio adjudicado
    If sdbgAdj.Groups.Count > 2 Then
        iNumColsGrupo = sdbgAdj.Groups(iGrupoVisible).Columns.Count
        If .OcultarFila2 = False Then
            '2� fila visible
            .ImpAdjPos = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            .CantProvPos = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            .AdjProvPos = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            .PrecioProvPos = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
            .ImpAdjLevel = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            .CantProvLevel = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            .AdjProvLevel = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            .PrecioProvLevel = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            .ImpAdjWidth = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
            .CantProvWidth = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
            .AdjProvWidth = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
            .PrecioProvWidth = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
        Else
            '2� fila oculta
            If IsNull(.ImpAdjPos) Then
                .ImpAdjPos = 1
            ElseIf sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .ImpAdjPos = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
                .ImpAdjWidth = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .ImpAdjLevel = 0
            End If
            If IsNull(.CantProvPos) Then
                .CantProvPos = 8
            ElseIf sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .CantProvPos = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
                .CantProvWidth = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .CantProvLevel = 0
            End If
            If IsNull(.AdjProvPos) Then
                .AdjProvPos = 9
            ElseIf sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .AdjProvPos = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
                .AdjProvWidth = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .AdjProvLevel = 0
            End If
            If IsNull(.PrecioProvPos) Then
                .PrecioProvPos = 2
            ElseIf sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .PrecioProvPos = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
                .PrecioProvWidth = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .PrecioProvLevel = 0
            End If
        End If
        'almacena los width de los atributos
        For Each oAtribVista In .PlantConfVistasGrAtrib
            If .OcultarFila2 = False Then
                oAtribVista.Posicion = sdbgAdj.Columns(oAtribVista.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
                oAtribVista.Fila = sdbgAdj.Columns(oAtribVista.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
                oAtribVista.Width = sdbgAdj.Columns(oAtribVista.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
            Else
                If IsNull(oAtribVista.Posicion) Then
                    oAtribVista.Posicion = 1
                    oAtribVista.Fila = 0
                ElseIf sdbgAdj.Columns(oAtribVista.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                    oAtribVista.Posicion = sdbgAdj.Columns(oAtribVista.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Position - m_iNumColsGR - (iNumColsGrupo * (iGrupoVisible - 2))
                    oAtribVista.Width = sdbgAdj.Columns(oAtribVista.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                    oAtribVista.Fila = 0
                End If
            End If
        Next
    End If
    'Almacena el ancho de la fila
    .AnchoFila = sdbgAdj.RowHeight
    'Almacena el n�mero de decimales:
    .DecCant = txtDecCantGr.Text
    .DecPorcen = txtDecPorcenGr.Text
    .DecPrecios = txtDecPrecGr.Text
    .DecResult = txtDecResultGr.Text
    .ExcluirItCerradosResult = chkExcluirItCerradosResul.Value
    .OcultarNoAdj = chkOcultarNoAdjGr.Value
    .OcultarItCerrados = chkOcultarItCerradosGr.Value
    .OcultarProvSinOfe = chkOcultarProvSinOfeGr.Value
End With
End Sub

''' <summary>Guarda la configuraci�n de la vista para el grupo general</summary>
''' <remarks>Llamada desde: cmdGuardarVista_Click, cmdGuardarVistaNueva_Click, sstabComparativa_BeforeClick; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarVistaGeneral()
Dim oatrib As CPlantConfVistaProceAtrib
Dim i As Integer
Dim iScroll As Integer
With m_oVistaSeleccionada
    If sstabComparativa.selectedItem.Tag = "General" Then
        If sdbgGruposProve.Visible = True Then
            .TipoVision = 0
        Else
            .TipoVision = 1
        End If
    End If
    'Si se ha hecho un scroll se scrolla otra vez para dejarlo en la posici�n 0
    If sdbgGruposProve.GrpPosition(0) > 0 Then
        iScroll = sdbgGruposProve.Groups(0).Position
        sdbgGruposProve.Scroll -iScroll, 0
        sdbgGruposProve.Update
        sdbgTotalesProve.Scroll -iScroll, 0
        sdbgTotalesProve.Update
    End If
    If sdbgProce.GrpPosition(0) > 0 Then
        i = sdbgProce.Groups(0).Position
        sdbgProce.Scroll -i, 0
        sdbgProce.Update
    End If
    For i = 0 To sdbgGruposProve.Groups.Count - 1
        sdbgGruposProve.Groups(i).Position = i
    Next i
    For i = 0 To sdbgProce.Groups.Count - 1
        sdbgProce.Groups(i).Position = i
    Next i
    For i = 0 To sdbgTotalesProve.Groups.Count - 1
        sdbgTotalesProve.Groups(i).Position = i
    Next i
    .GrupoWidth = sdbgGruposProve.Groups(2).Width
    'almacena los widths de las columnas
    .AdjWidth = sdbgGruposProve.Columns("Adjudicado" & sdbgGruposProve.Groups(2).TagVariant).Width
    .ConsumidoWidth = sdbgGruposProve.Columns("Consumido" & sdbgGruposProve.Groups(2).TagVariant).Width
    .AhorroAdjWidth = sdbgGruposProve.Columns("Ahorrado" & sdbgGruposProve.Groups(2).TagVariant).Width
    .ImporteWidth = sdbgGruposProve.Columns("Importe" & sdbgGruposProve.Groups(2).TagVariant).Width
    .AhorroAdjPorcenWidth = sdbgGruposProve.Columns("Ahorro%" & sdbgGruposProve.Groups(2).TagVariant).Width
    .AhorroOfeWidth = sdbgGruposProve.Columns("AhorroOferta" & sdbgGruposProve.Groups(2).TagVariant).Width
    .AhorroOfePorcenWidth = sdbgGruposProve.Columns("AhorroOfe%" & sdbgGruposProve.Groups(2).TagVariant).Width
    'almacena los position de las columnas
    .AdjPos = sdbgGruposProve.Columns("Adjudicado" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
    .ConsumidoPos = sdbgGruposProve.Columns("Consumido" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
    .AhorroAdjPos = sdbgGruposProve.Columns("Ahorrado" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
    .ImportePos = sdbgGruposProve.Columns("Importe" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
    .AhorroAdjPorcenPos = sdbgGruposProve.Columns("Ahorro%" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
    .AhorroOfePos = sdbgGruposProve.Columns("AhorroOferta" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
    .AhorroOfePorcenPos = sdbgGruposProve.Columns("AhorroOfe%" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
    If g_oPlantillaSeleccionada.Solicitud = EnItem Or g_oPlantillaSeleccionada.Solicitud = NoDefinido Then
        .SolicVinculadaWidth = 400
        .SolicVinculadaPos = 11
        'La posicion es la ultima tras los atribs de proceso de grid PROCE  si Solicitud = EnProceso
        'La posicion es la ultima tras los atribs de proceso de grid PROVEGRUPOS    si Solicitud = EnGrupo
        .SolicVinculadaVisible = False
    ElseIf g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
        .SolicVinculadaWidth = sdbgGruposProve.Columns("VINCULADO_GRU" & sdbgGruposProve.Groups(1).TagVariant).Width
        .SolicVinculadaPos = sdbgGruposProve.Columns("VINCULADO_GRU" & sdbgGruposProve.Groups(1).TagVariant).Position - 2
        .SolicVinculadaVisible = sdbgGruposProve.Columns("VINCULADO_GRU" & sdbgGruposProve.Groups(1).TagVariant).Visible
    ElseIf g_oPlantillaSeleccionada.Solicitud = EnProceso Then
        .SolicVinculadaWidth = sdbgProce.Columns("VINCULADO_PRO" & sdbgProce.Groups(1).TagVariant).Width
        .SolicVinculadaPos = sdbgProce.Columns("VINCULADO_PRO" & sdbgProce.Groups(1).TagVariant).Position
        .SolicVinculadaVisible = sdbgProce.Columns("VINCULADO_PRO" & sdbgProce.Groups(1).TagVariant).Visible
    End If
    'almacena los width de los atributos
    For Each oatrib In .PlantConfVistasProceAtrib
        If oatrib.ambito = TipoAmbitoProceso.AmbProceso Then  'si es un atributo de nivel de proceso
            oatrib.Posicion = sdbgProce.Columns(oatrib.Atributo & sdbgProce.Groups(1).TagVariant).Position
            If oatrib.Visible = True Then oatrib.Width = sdbgProce.Columns(oatrib.Atributo & sdbgProce.Groups(2).TagVariant).Width
        Else 'si es un atributo de nivel de grupo
            oatrib.Posicion = sdbgGruposProve.Columns(oatrib.CodAtributo & sdbgGruposProve.Groups(1).TagVariant).Position - 2
            If oatrib.Visible = True Then oatrib.Width = sdbgGruposProve.Columns(oatrib.CodAtributo & sdbgGruposProve.Groups(2).TagVariant).Width
        End If
    Next
    'Almacena el n�mero de decimales:
    .DecPorcen = txtDecPorcen.Text
    .DecResult = txtDecResult.Text
    .ExcluirGrCerradosResult = chkExcluirGrCerradosResul.Value
    .OcultarGrCerrados = chkOcultarGrCerrados.Value
    .OcultarNoAdj = chkOcultarNoAdj.Value
    .OcultarProvSinOfe = chkOcultarProvSinOfe.Value
    'Almacena el ancho de la fila
    .AnchoFila = sdbgGruposProve.RowHeight
    'Almacena la configuraci�n del grupo 0
    .Grupo0Width = sdbgGruposProve.Groups(0).Width
    .codWidth = sdbgGruposProve.Columns("COD").Width
    .DenWidth = sdbgGruposProve.Columns("NOMBRE").Width
    'si se hab�a hecho un scroll se deja en la posici�n original
    If iScroll > 0 Then
        sdbgTotalesProve.Scroll iScroll, 0
        sdbgTotalesProve.Update
        sdbgGruposProve.Scroll iScroll, 0
        sdbgGruposProve.Update
    End If
    .HayCambios = False
End With
End Sub

Private Sub AllSeleccionado()
Dim sVista As String
'Nos posicionamos en la pesta�a de ALL
ConfiguracionVistaActualAll m_oVistaSeleccionada.Vista, False
Select Case g_oPlantillaSeleccionada.VistaDefectoComp
    Case TipoDeVistaDefecto.vistainicial
        sdbcVistaDefectoAll.Text = m_sVistaIni
    Case Else
        sdbcVistaDefectoAll.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
End Select
Select Case m_oVistaSeleccionada.Vista
    Case TipoDeVistaDefecto.vistainicial
        sdbcVistaActAll.Text = m_sVistaIni
        sVista = m_sVistaIni
    Case Else
        sdbcVistaActAll.Text = m_oVistaSeleccionada.NombreVista
        sVista = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
End Select
sdbcVistaDefectoAll.Columns("COD").Value = CStr(g_oPlantillaSeleccionada.VistaDefectoComp)
sdbcVistaActAll.Columns("COD").Value = CStr(m_oVistaSeleccionada.Vista)
sdbgAll.caption = sVista
'Carga la grid
CargarGridAll
RedimensionarGridAll
CargarVistasAll
MostrarSplitAll
End Sub

Private Sub ConfiguracionVistaActualAll(ByVal iVista As Variant, ByVal bDefecto As Boolean)
Dim oatrib As CPlantConfVistaAllAtrib
'Carga las configuraci�n de la vista que se pasa por par�metro para el usuario y
'la plantilla seleccionada
Set m_oVistaSeleccionadaAll = Nothing
If Not m_oVistaSeleccionadaGr Is Nothing Then If m_oVistaSeleccionadaGr.Vista <> iVista Then Set m_oVistaSeleccionadaGr = Nothing
'Si no se ha cargado la vista en la colecci�n todav�a se carga
If Not IsNull(iVista) And iVista <> 0 Then If m_oConfVistasAll.Item(CStr(iVista)) Is Nothing Then m_oConfVistasAll.CargarTodasLasVistas g_oPlantillaSeleccionada, iVista
'Si no hay datos en BD para esa vista,o no hab�a vista por defecto para la plantilla se a�ade
'a la colecci�n y se inserta en BD con los valores de la vista inicial para ese usuario:
If IsNull(iVista) Or iVista = 0 Then
    'En este caso la vista por defecto ser� la vista 1
    Set m_oVistasInicialALL = Nothing
    Set m_oVistasInicialALL = oFSGSRaiz.Generar_CConfVistasAll
    m_oVistasInicialALL.CargarVistaInicial oUsuarioSummit.Cod
    If m_oConfVistasAll.Item("0") Is Nothing Then
        If m_oVistasInicialALL.Item("00") Is Nothing Then
            CargarDatosDefectoEnVista "ALL", 0, 0, False
        Else
            CargarDatosDefectoEnVista "ALL", 0, 0, True
        End If
    End If
    Set m_oVistaSeleccionadaAll = m_oConfVistasAll.Item("0")
ElseIf m_oConfVistasAll.Item(CStr(iVista)) Is Nothing Then
    If m_oVistasInicialALL Is Nothing Then
        Set m_oVistasInicialALL = oFSGSRaiz.Generar_CConfVistasAll
        m_oVistasInicialALL.CargarVistaInicial oUsuarioSummit.Cod
    End If
    If m_oVistasInicialALL.Item("00") Is Nothing Then
        CargarDatosDefectoEnVista "ALL", iVista, BooleanToSQLBinary(False), False
    Else
        CargarDatosDefectoEnVista "ALL", iVista, BooleanToSQLBinary(False), True
    End If
    Set m_oVistaSeleccionadaAll = m_oConfVistasAll.Item(CStr(iVista))
Else
    Set m_oVistaSeleccionadaAll = m_oConfVistasAll.Item(CStr(iVista))
End If
'Carga las configuraciones de los atributos
If m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib Is Nothing Then
    m_oVistaSeleccionadaAll.CargarConfAtributos
    'Si se carga un atributo en la vistas que no existe en la colecci�n de atributos
    'lo elimina de la colecci�n
    For Each oatrib In m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib
        If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
            If g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)) Is Nothing Then
                m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Remove (CStr(oatrib.Atributo))
            ElseIf g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).ambito <> AmbItem Then
                m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Remove (CStr(oatrib.Atributo))
            End If
        End If
    Next
End If
m_oVistaSeleccionadaAll.HayCambios = False
If m_oVistaSeleccionada.Vista <> iVista Then
    ConfiguracionVistaActual iVista, False
    CargarVistasProc
End If
End Sub

''' <summary>
''' Establece las propiedades de una vista
''' </summary>
''' <param name="sAmbito">Grupo/All</param>
''' <param name="iVista">Id Vista</param>
''' <param name="iDefecto">Id Vista defecto</param>
''' <param name="bInicial">vista inical si/no</param>
''' <remarks>Llamada desde: ConfiguracionVistaActualGr  ConfiguracionVistaActualAll ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarDatosDefectoEnVista(ByVal sAmbito As String, ByVal iVista As Integer, ByVal iDefecto As Integer, ByVal bInicial As Boolean)
Dim ConfigVista As TipoConfigVista
Dim oVistaInicial
With ConfigVista
    If bInicial = True Then
        'Datos de la vista inicial
        Select Case sAmbito
            Case "GR"
                Set oVistaInicial = oFSGSRaiz.Generar_CConfVistaGrupo
                Set oVistaInicial = m_oVistasInicialGR.Item("00")
            Case "ALL"
                Set oVistaInicial = oFSGSRaiz.Generar_CConfVistaAll
                Set oVistaInicial = m_oVistasInicialALL.Item("00")
        End Select
        .iAnyoPos = oVistaInicial.AnyoPos
        .bAnyoVisible = oVistaInicial.AnyoVisible
        .dblAnyoWidth = oVistaInicial.AnyoWidth
        .iAnyoLevel = oVistaInicial.AnyoLevel
        .iImportePos = oVistaInicial.ImportePos
        .bImporteVisible = oVistaInicial.ImporteVisible
        .dblImporteWidth = oVistaInicial.ImporteWidth
        .iImporteLevel = oVistaInicial.ImporteLevel
        .iAdjPos = oVistaInicial.AdjPos
        .bAdjVisible = oVistaInicial.AdjVisible
        .dblAdjWidth = oVistaInicial.AdjWidth
        .iAdjLevel = oVistaInicial.AdjLevel
        .iProvPos = oVistaInicial.ProveedorPos
        .bProvVisible = oVistaInicial.ProveedorVisible
        .dblProvWidth = oVistaInicial.ProveedorWidth
        .iProvLevel = oVistaInicial.ProveedorLevel
        .iAhorroImpPos = oVistaInicial.AhorroPos
        .bAhorroImpVisible = oVistaInicial.AhorroVisible
        .dblAhorroImpWidth = oVistaInicial.AhorroWidth
        .iAhorroImpLevel = oVistaInicial.AhorroLevel
        .iAhorroPorcenPos = oVistaInicial.AhorroPorcenPos
        .bAhorroPorcenVisible = oVistaInicial.AhorroPorcenVisible
        .dblAhorroPorcenWidth = oVistaInicial.AhorroPorcenWidth
        .iAhorroPorcenLevel = oVistaInicial.AhorroPorcenLevel
        .iPresUniPos = oVistaInicial.PresUniPos
        .bPresUniVisible = oVistaInicial.PresUniVisible
        .dblPresUniWidth = oVistaInicial.PresUniWidth
        .iPresUniLevel = oVistaInicial.PresUniLevel
        .iObjPos = oVistaInicial.OBJPos
        .bObjVisible = oVistaInicial.OBJVisible
        .dblObjWidth = oVistaInicial.OBJWidth
        .iObjLevel = oVistaInicial.OBJLevel
        .iCantPos = oVistaInicial.CantidadPos
        .bCantVisible = oVistaInicial.CantidadVisible
        .dblCantWidth = oVistaInicial.CantidadWidth
        .iCantLevel = oVistaInicial.CantidadLevel
        .dblDescrWidth = oVistaInicial.DescrWidth
        .bOcultarFila2 = oVistaInicial.OcultarFila2
        .dblGrupo0Width = oVistaInicial.Grupo0Width
        .dblGruposWidth = oVistaInicial.GruposWidth
        .dblImpAdjWidth = oVistaInicial.ImpAdjWidth
        .bImpAdjVisible = oVistaInicial.ImpAdjVisible
        .iImpAdjLevel = oVistaInicial.ImpAdjLevel
        .iImpAdjPos = oVistaInicial.ImpAdjPos
        .iCantProvLevel = oVistaInicial.CantProvLevel
        .iCantProvPos = oVistaInicial.CantProvPos
        .bCantProvVisible = oVistaInicial.CantProvVisible
        .dblCantProvWidth = oVistaInicial.CantProvWidth
        .iAdjProvLevel = oVistaInicial.AdjProvLevel
        .iAdjProvPos = oVistaInicial.AdjProvPos
        .bAdjProvVisible = oVistaInicial.AdjProvVisible
        .dblAdjProvWidth = oVistaInicial.AdjProvWidth
        .iPrecioProvLevel = oVistaInicial.PrecioProvLevel
        .iPrecioProvPos = oVistaInicial.PrecioProvPos
        .bPrecioProvVisible = oVistaInicial.PrecioProvVisible
        .dblPrecioProvWidth = oVistaInicial.PrecioProvWidth
        .iSolicVinculadaLevel = oVistaInicial.SolicVinculadaLevel
        .iSolicVinculadaPos = oVistaInicial.SolicVinculadaPos
        .bSolicVinculadaVisible = oVistaInicial.SolicVinculadaVisible
        .dblSolicVinculadaWidth = oVistaInicial.SolicVinculadaWidth
        .iDecCant = oVistaInicial.DecCant
        .iDecPorcen = oVistaInicial.DecPorcen
        .iDecPrecios = oVistaInicial.DecPrecios
        .iDecResult = oVistaInicial.DecResult
        .bOcultarItCerrados = oVistaInicial.OcultarItCerrados
        .bOcultarNoAdj = oVistaInicial.OcultarNoAdj
        .bOcultarProvSinOfe = oVistaInicial.OcultarProvSinOfe
        .bExcluirItCerradosResult = oVistaInicial.ExcluirCerradosResult
        .iFecIniSumLevel = oVistaInicial.FecIniSumLevel
        .iFecIniSumPos = oVistaInicial.FecIniSumPos
        .bFecIniSumVisible = oVistaInicial.FecIniSumVisible
        .dblFecIniSumWidth = oVistaInicial.FecIniSumWidth
        .iFecFinSumLevel = oVistaInicial.FecFinSumLevel
        .iFecFinSumPos = oVistaInicial.FecFinSumPos
        .bFecFinSumVisible = oVistaInicial.FecFinSumVisible
        .dblFecFinSumWidth = oVistaInicial.FecFinSumWidth
        .iEstrMatLevel = oVistaInicial.EstrMatLevel
        .iEstrMatPos = oVistaInicial.EstrMatPos
        .bEstrMatVisible = oVistaInicial.EstrMatVisible
        .dblEstrMatWidth = oVistaInicial.EstrMatWidth
        .dblAnchoFila = oVistaInicial.AnchoFila
        Set oVistaInicial = Nothing
    Else
        'Datos por defecto
        .iAnyoPos = 1
        .bAnyoVisible = True
        .dblAnyoWidth = 810.1418
        .iAnyoLevel = 0
        .iImportePos = 10
        .bImporteVisible = True
        .dblImporteWidth = 1025
        .iImporteLevel = 1
        .iAdjPos = 12
        .bAdjVisible = True
        .dblAdjWidth = 1025
        .iAdjLevel = 1
        .iProvPos = 3
        .bProvVisible = True
        .dblProvWidth = 1025
        .iProvLevel = 0
        .iAhorroImpPos = 8
        .bAhorroImpVisible = True
        .dblAhorroImpWidth = 1470.047
        .iAhorroImpLevel = 1
        .iAhorroPorcenPos = 9
        .bAhorroPorcenVisible = True
        .dblAhorroPorcenWidth = 1305.071
        .iAhorroPorcenLevel = 1
        .iPresUniPos = 4
        .bPresUniVisible = True
        .dblPresUniWidth = 1025
        .iPresUniLevel = 0
        .iObjPos = 7
        .bObjVisible = True
        .dblObjWidth = 1025
        .iObjLevel = 0
        .iCantPos = 11
        .bCantVisible = True
        .dblCantWidth = 1025
        .iCantLevel = 1
        .dblDescrWidth = 1964.976
        .bOcultarFila2 = False
        .dblGrupo0Width = 5850.142
        .dblGruposWidth = 1600
        .dblImpAdjWidth = 500
        .bImpAdjVisible = False
        .iImpAdjLevel = 0
        .iImpAdjPos = 1
        .iCantProvLevel = 1
        .iCantProvPos = 8
        .bCantProvVisible = True
        .dblCantProvWidth = 900
        .iAdjProvLevel = 1
        .iAdjProvPos = 9
        .bAdjProvVisible = True
        .dblAdjProvWidth = 700
        .iSolicVinculadaLevel = 1
        .iSolicVinculadaPos = 13
        .bSolicVinculadaVisible = False
        .dblSolicVinculadaWidth = 400
        .iPrecioProvLevel = 0
        .iPrecioProvPos = 2
        .bPrecioProvVisible = True
        .dblPrecioProvWidth = 1600
        .iFecIniSumLevel = 1
        .iFecIniSumPos = 14
        .bFecIniSumVisible = True
        .dblFecIniSumWidth = 700
        .iFecFinSumLevel = 1
        .iFecFinSumPos = 15
        .bFecFinSumVisible = True
        .dblFecFinSumWidth = 700
        .iEstrMatLevel = 1
        .iEstrMatPos = 16
        .bEstrMatVisible = True
        .dblEstrMatWidth = 1964.976
        .dblAnchoFila = 480.189
    End If
    If iDefecto = 1 Then
        .bDefecto = True
    Else
        .bDefecto = False
    End If
    Select Case sAmbito
        Case "GR"
            If m_oGrupoSeleccionado.PlantConfVistas Is Nothing Then
                Set m_oGrupoSeleccionado.PlantConfVistas = oFSGSRaiz.Generar_CPlantConfVistasGrupo
            End If
            m_oGrupoSeleccionado.PlantConfVistas.Add g_oPlantillaSeleccionada, m_oGrupoSeleccionado, iVista, ConfigVista
        Case "ALL"
            m_oConfVistasAll.Add g_oPlantillaSeleccionada, iVista, ConfigVista
    End Select
End With
End Sub

''' <summary>
''' Recompone la grid seg�n la configuraci�n de la vista
''' </summary>
''' <remarks>Llamada desde: cmdEliminarVista_Click      sdbcVistaActual_CloseUp     sstabComparativa_Click      CargarAdjudicaciones; Tiempo m�ximo: 0,2</remarks>
Private Sub RedimensionarGrid()
Dim i As Integer
Dim sGrupo As String
Dim oatrib As CPlantConfVistaProceAtrib
Dim iOrden As Integer
Dim sProv As String
Dim iVisible As Integer
Dim vbm As Variant
Dim oAtribRepetido As CPlantConfVistaProceAtrib
Dim bAtribRepetido  As Boolean
Dim bColVisible As Boolean
Dim j As Integer
Dim iVisibleInvertida As Integer
Dim lSobre As Long
Dim sColumna As String
Dim k As Integer
Dim vAtribNormal() As TipoAtrib
Dim vAtribRep() As TipoAtrib
Dim lIndex As Integer
Dim MaxPosVincPro As Integer
m_bGridProcVisible = False
'Si se ha hecho un scroll se vuelve a hacerlo para dejar la grid en la posici�n
'original porque sino no funciona bien
If sdbgGruposProve.GrpPosition(0) > 0 Then
    i = sdbgGruposProve.Groups(0).Position
    sdbgGruposProve.Scroll -i, 0
    sdbgGruposProve.Update
    sdbgTotalesProve.Scroll -i, 0
    sdbgTotalesProve.Update
End If
If sdbgProveGrupos.GrpPosition(0) > 0 Then
    i = sdbgProveGrupos.Groups(0).Position
    sdbgProveGrupos.Scroll -i, 0
    sdbgProveGrupos.Update
    sdbgtotales2.Scroll -i, 0
    sdbgtotales2.Update
End If
If sdbgProce.GrpPosition(0) > 0 Then
    i = sdbgProce.Groups(0).Position
    sdbgProce.Scroll -i, 0
    sdbgProce.Update
End If
For i = 0 To sdbgGruposProve.Groups.Count - 1
    sdbgGruposProve.Groups(i).Position = i
Next i
For i = 0 To sdbgProce.Groups.Count - 1
    sdbgProce.Groups(i).Position = i
Next i
For i = 0 To sdbgProveGrupos.Groups.Count - 1
    sdbgProveGrupos.Groups(i).Position = i
Next i
sdbgGruposProve.AllowColumnMoving = ssRelocateWithinGroup
sdbgProveGrupos.AllowColumnMoving = ssRelocateWithinGroup
sdbgProce.AllowColumnMoving = ssRelocateWithinGroup
'Redimensiona el grupo 0 de la grid
If IsNull(m_oVistaSeleccionada.Grupo0Width) Or m_oVistaSeleccionada.Grupo0Width = 0 Then
    sdbgGruposProve.Groups(0).Width = 2700
    sdbgTotalesProve.Groups(0).Width = 2700
    sdbgProce.Groups(0).Width = 2700
Else
    sdbgGruposProve.Groups(0).Width = m_oVistaSeleccionada.Grupo0Width
    sdbgTotalesProve.Groups(0).Width = m_oVistaSeleccionada.Grupo0Width
    sdbgProce.Groups(0).Width = m_oVistaSeleccionada.Grupo0Width
End If
If IsNull(m_oVistaSeleccionada.codWidth) Or m_oVistaSeleccionada.codWidth = 0 Then
    sdbgGruposProve.Columns("COD").Width = 945
Else
    sdbgGruposProve.Columns("COD").Width = m_oVistaSeleccionada.codWidth
End If
If IsNull(m_oVistaSeleccionada.DenWidth) Or m_oVistaSeleccionada.DenWidth = 0 Then
    sdbgGruposProve.Columns("NOMBRE").Width = 1755
Else
    sdbgGruposProve.Columns("NOMBRE").Width = m_oVistaSeleccionada.DenWidth
End If
'Crea 2 arrays de atributos.Uno van a ser los atributos normales y otros los que est�n
'repetidos en varios grupos:
ReDim vAtribNormal(0)
ReDim vAtribRep(0)
If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
    For Each oatrib In m_oVistaSeleccionada.PlantConfVistasProceAtrib
         If oatrib.ambito = TipoAmbitoProceso.AmbGrupo Then  'si es un atributo de grupo
             bAtribRepetido = False
             bColVisible = oatrib.Visible
             'Comprueba si el atributo est� visible para el resto de los grupos:
             For Each oAtribRepetido In m_oVistaSeleccionada.PlantConfVistasProceAtrib
                 If oatrib.Atributo <> oAtribRepetido.Atributo Then
                     If oatrib.CodAtributo = oAtribRepetido.CodAtributo Then
                         bAtribRepetido = True
                         If oAtribRepetido.Visible = True Then
                             bColVisible = True
                             Exit For
                         End If
                     End If
                 End If
            Next
            If bAtribRepetido = True Then
                'Es un atributo repetido
                ReDim Preserve vAtribRep(UBound(vAtribRep) + 1)
                vAtribRep(UBound(vAtribRep)).Id = oatrib.Atributo
                vAtribRep(UBound(vAtribRep)).Cod = oatrib.CodAtributo
                vAtribRep(UBound(vAtribRep)).Visible = oatrib.Visible
                vAtribRep(UBound(vAtribRep)).ColVisible = bColVisible
                vAtribRep(UBound(vAtribRep)).Posicion = oatrib.Posicion
                vAtribRep(UBound(vAtribRep)).Width = oatrib.Width
            Else
                'Es un atributo normal
                ReDim Preserve vAtribNormal(UBound(vAtribNormal) + 1)
                vAtribNormal(UBound(vAtribNormal)).Id = oatrib.Atributo
                vAtribNormal(UBound(vAtribNormal)).Cod = oatrib.CodAtributo
                vAtribNormal(UBound(vAtribNormal)).Visible = oatrib.Visible
                vAtribNormal(UBound(vAtribNormal)).ColVisible = bColVisible
                vAtribNormal(UBound(vAtribNormal)).Posicion = oatrib.Posicion
                vAtribNormal(UBound(vAtribNormal)).Width = oatrib.Width
            End If
        End If
    Next
End If
'''''''''''''''''''''Redimensiona sdbgGruposProve''''''''''''''''''''
With sdbgGruposProve
    For i = 1 To 3
        sGrupo = .Groups(i).TagVariant
        .Groups(i).Visible = True
        sdbgTotalesProve.Groups(i).Visible = True
        If sdbgProce.Groups.Count > 2 Then
            sdbgProce.Groups(i).Width = m_oVistaSeleccionada.GrupoWidth
            sdbgProce.Groups(i).Visible = .Groups(i).Visible
        End If
        'tama�o del grupo
        .Groups(i).Width = m_oVistaSeleccionada.GrupoWidth
        sdbgTotalesProve.Groups(i).Width = m_oVistaSeleccionada.GrupoWidth
        'Redimensiona las columnas de los grupos
        iOrden = .Groups(0).Columns.Count + (.Groups(i).Columns.Count * (i - 1)) - 1
        If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
            .Columns("VINCULADO_GRU" & sGrupo).Visible = m_oVistaSeleccionada.SolicVinculadaVisible
            If (m_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial) Then .Columns("VINCULADO_GRU" & sGrupo).Position = m_oVistaSeleccionada.SolicVinculadaPos + iOrden
        End If
        'atributos visibles
        If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
            'Si hay atributos normales los trata
            If UBound(vAtribNormal) > 0 Then
                For k = 1 To UBound(vAtribNormal)
                    If vAtribNormal(k).Posicion <> 0 Then .Columns(vAtribNormal(k).Cod & sGrupo).Position = vAtribNormal(k).Posicion + iOrden
                    .Columns(vAtribNormal(k).Cod & sGrupo).Visible = vAtribNormal(k).Visible
                Next k
            End If
            If UBound(vAtribRep) > 0 Then
                For k = 1 To UBound(vAtribRep)
                    If vAtribRep(k).Cod <> vAtribRep(k - 1).Cod Then
                        If vAtribRep(k).Posicion <> 0 Then .Columns(vAtribRep(k).Cod & sGrupo).Position = vAtribRep(k).Posicion + iOrden
                        .Columns(vAtribRep(k).Cod & sGrupo).Visible = vAtribRep(k).ColVisible
                    End If
                Next k
            End If
        End If
        .Columns("Consumido" & sGrupo).Position = m_oVistaSeleccionada.ConsumidoPos + iOrden
        .Columns("Adjudicado" & sGrupo).Position = m_oVistaSeleccionada.AdjPos + iOrden
        .Columns("Ahorrado" & sGrupo).Position = m_oVistaSeleccionada.AhorroAdjPos + iOrden
        .Columns("Ahorro%" & sGrupo).Position = m_oVistaSeleccionada.AhorroAdjPorcenPos + iOrden
        .Columns("Importe" & sGrupo).Position = m_oVistaSeleccionada.ImportePos + iOrden
        .Columns("AhorroOferta" & sGrupo).Position = m_oVistaSeleccionada.AhorroOfePos + iOrden
        .Columns("AhorroOfe%" & sGrupo).Position = m_oVistaSeleccionada.AhorroOfePorcenPos + iOrden
        .Columns("Consumido" & sGrupo).Visible = m_oVistaSeleccionada.ConsumidoVisible
        .Columns("Adjudicado" & sGrupo).Visible = m_oVistaSeleccionada.AdjVisible
        .Columns("Ahorrado" & sGrupo).Visible = m_oVistaSeleccionada.AhorroAdjVisible
        .Columns("Ahorro%" & sGrupo).Visible = m_oVistaSeleccionada.AhorroAdjPorcenVisible
        .Columns("Importe" & sGrupo).Visible = m_oVistaSeleccionada.ImporteVisible
        .Columns("AhorroOferta" & sGrupo).Visible = m_oVistaSeleccionada.AhorroOfeVisible
        .Columns("AhorroOfe%" & sGrupo).Visible = m_oVistaSeleccionada.AhorroOfePorcenVisible
        'Width de las columnas: Pone los width en el mismo orden en que est�n los position,porque sino no
        'lo hace bien
        For j = 0 To .Groups(i).Columns.Count - 1
            sColumna = ""
            lIndex = .Groups(i).ColPosition(j)
            sColumna = Mid(.Groups(i).Columns(lIndex).Name, 1, InStr(.Groups(i).Columns(lIndex).Name, .Groups(i).TagVariant) - 1)
            If sColumna <> "" Then
                Select Case sColumna
                    Case "Consumido"
                        .Columns("Consumido" & sGrupo).Width = m_oVistaSeleccionada.ConsumidoWidth
                    Case "Adjudicado"
                        .Columns("Adjudicado" & sGrupo).Width = m_oVistaSeleccionada.AdjWidth
                    Case "Ahorrado"
                        .Columns("Ahorrado" & sGrupo).Width = m_oVistaSeleccionada.AhorroAdjWidth
                    Case "Ahorro%"
                        .Columns("Ahorro%" & sGrupo).Width = m_oVistaSeleccionada.AhorroAdjPorcenWidth
                    Case "Importe"
                        .Columns("Importe" & sGrupo).Width = m_oVistaSeleccionada.ImporteWidth
                    Case "AhorroOferta"
                        .Columns("AhorroOferta" & sGrupo).Width = m_oVistaSeleccionada.AhorroOfeWidth
                    Case "AhorroOfe%"
                        .Columns("AhorroOfe%" & sGrupo).Width = m_oVistaSeleccionada.AhorroOfePorcenWidth
                    Case "VINCULADO_GRU"
                        .Columns("VINCULADO_GRU" & sGrupo).Width = m_oVistaSeleccionada.SolicVinculadaWidth
                    Case Else  'Comprueba si es un atributo
                        If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
                            If sColumna <> "CodProve" And sColumna <> "NumOfe" And sColumna <> "VACIO" Then
                                'Es un atributo
                                If UBound(vAtribNormal) > 0 Then
                                    For k = 1 To UBound(vAtribNormal)
                                        If vAtribNormal(k).Cod = sColumna Then
                                            If vAtribNormal(k).Width = 0 Then
                                                .Groups(i).Columns(lIndex).Width = 500
                                            Else
                                                .Groups(i).Columns(lIndex).Width = vAtribNormal(k).Width
                                            End If
                                            Exit For
                                        End If
                                    Next k
                                End If
                                If UBound(vAtribRep) > 0 Then
                                    For k = 1 To UBound(vAtribRep)
                                        If vAtribRep(k).Cod <> vAtribRep(k - 1).Cod Then
                                            If vAtribRep(k).Cod = sColumna Then
                                                If vAtribRep(k).Width = 0 Then
                                                    .Columns(vAtribRep(k).Cod & sGrupo).Width = 500
                                                Else
                                                    .Columns(vAtribRep(k).Cod & sGrupo).Width = vAtribRep(k).Width
                                                End If
                                            End If
                                        End If
                                    Next k
                                End If
                            End If
                        End If
                End Select
            End If
        Next
        iVisible = 1
        iVisibleInvertida = 1
        sdbgTotalesProve.Columns("Importe" & sGrupo).Visible = m_oVistaSeleccionada.ImporteVisible
        iVisible = iVisible + BooleanToSQLBinary(m_oVistaSeleccionada.ImporteVisible)
        sdbgTotalesProve.Columns("AhorroOferta" & sGrupo).Visible = m_oVistaSeleccionada.AhorroOfeVisible
        iVisible = iVisible + BooleanToSQLBinary(m_oVistaSeleccionada.AhorroOfeVisible)
        sdbgTotalesProve.Columns("Consumido" & sGrupo).Visible = m_oVistaSeleccionada.ConsumidoVisible
        iVisible = iVisible + BooleanToSQLBinary(m_oVistaSeleccionada.ConsumidoVisible)
        iVisibleInvertida = iVisibleInvertida + BooleanToSQLBinary(m_oVistaSeleccionada.ConsumidoVisible)
        sdbgTotalesProve.Columns("Ahorrado" & sGrupo).Visible = m_oVistaSeleccionada.AhorroAdjVisible
        iVisible = iVisible + BooleanToSQLBinary(m_oVistaSeleccionada.AhorroAdjVisible)
        iVisibleInvertida = iVisibleInvertida + BooleanToSQLBinary(m_oVistaSeleccionada.AhorroAdjVisible)
        sdbgTotalesProve.Columns("Consumido" & sGrupo).Width = sdbgTotalesProve.Groups(i).Width / iVisible
        sdbgTotalesProve.Columns("Adjudicado" & sGrupo).Width = sdbgTotalesProve.Groups(i).Width / iVisible
        sdbgTotalesProve.Columns("Ahorrado" & sGrupo).Width = sdbgTotalesProve.Groups(i).Width / iVisible
        sdbgTotalesProve.Columns("Importe" & sGrupo).Width = sdbgTotalesProve.Groups(i).Width / iVisible
        sdbgTotalesProve.Columns("AhorroOferta" & sGrupo).Width = sdbgTotalesProve.Groups(i).Width / iVisible
    Next i
    'atributos visibles
    If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
        If UBound(vAtribRep) > 0 Then
            'Se recorre todas las filas y dentro de cada fila los grupos para poner el atributo en gris o no
            For j = 0 To .Rows - 1
                .Bookmark = j
                For k = 1 To UBound(vAtribRep)
                    If vAtribRep(k).Visible = True Then
                        'Visualiza el valor ofertado y quita el gris del campo.
                        For i = 1 To .Groups.Count - 1
                            sGrupo = .Groups(i).TagVariant
                            If .Columns("COD").Value = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(vAtribRep(k).Id)).codgrupo Then .Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(vAtribRep(k).Id)).Cod & .Groups(i).TagVariant).CellStyleSet "Normal", j
                        Next i
                    Else
                        'El atributo no est� visible,lo pone en gris
                        For i = 1 To .Groups.Count - 1
                            sGrupo = .Groups(i).TagVariant
                            If .Columns("COD").Value = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(vAtribRep(k).Id)).codgrupo Then .Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(vAtribRep(k).Id)).Cod & .Groups(i).TagVariant).CellStyleSet "GrayHead", j
                        Next i
                    End If
                Next k
            Next j
        End If
    End If
    'Si es un proceso de adminitraci�n p�blica oculta los grupos cuyo sobre est� cerrado o no visible
    If g_oPlantillaSeleccionada.AdminPub = True Then
        i = .Rows - 1
        While i >= 0
            vbm = .AddItemBookmark(i)
            lSobre = g_oPlantillaSeleccionada.Grupos.Item(CStr(.Columns("COD").CellValue(vbm))).Sobre
            If m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(lSobre)).Visible = False Then .RemoveItem (.AddItemRowIndex(vbm))
            i = i - 1
        Wend
    End If
End With
    
'''''''''''''''''''''Redimensiona sdbgProveGrupos''''''''''''''''''''
With sdbgProveGrupos
    'Redimensiona el grupo 0 de la grid
    If IsNull(m_oVistaSeleccionada.Grupo0Width) Or m_oVistaSeleccionada.Grupo0Width = 0 Then
        .Groups(0).Width = 2700
        sdbgtotales2.Groups(0).Width = 2700
    Else
        .Groups(0).Width = m_oVistaSeleccionada.Grupo0Width
        sdbgtotales2.Groups(0).Width = m_oVistaSeleccionada.Grupo0Width
    End If
    If IsNull(m_oVistaSeleccionada.codWidth) Or m_oVistaSeleccionada.codWidth = 0 Then
        .Columns("COD").Width = 945
    Else
        .Columns("COD").Width = m_oVistaSeleccionada.codWidth
    End If
    If IsNull(m_oVistaSeleccionada.DenWidth) Or m_oVistaSeleccionada.DenWidth = 0 Then
        .Columns("DEN").Width = 1755
    Else
        .Columns("DEN").Width = m_oVistaSeleccionada.DenWidth
    End If
    For i = 1 To .Groups.Count - 1
        sProv = .Groups(i).TagVariant
        .Groups(i).Width = m_oVistaSeleccionada.GrupoWidth  'tama�o del grupo
        sdbgtotales2.Groups(i).Width = m_oVistaSeleccionada.GrupoWidth
        If sProv <> "" Then
            'Redimensiona las columnas de los grupos
            iOrden = 2 + .Groups(1).Columns.Count + (.Groups(i).Columns.Count * (i - 2))
            If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
                .Columns("VINCULADO_GRU" & sProv).Visible = m_oVistaSeleccionada.SolicVinculadaVisible
                If (m_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial) Then .Columns("VINCULADO_GRU" & sProv).Position = m_oVistaSeleccionada.SolicVinculadaPos + iOrden
            End If
            'atributos visibles
            If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
                'si hay atributos normales los trata (El atributo s�lo est� definido en un grupo o para todos)
                If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
                    'Si hay atributos normales los trata
                    If UBound(vAtribNormal) > 0 Then
                        For k = 1 To UBound(vAtribNormal)
                            If vAtribNormal(k).Posicion <> 0 Then .Columns(vAtribNormal(k).Cod & sProv).Position = vAtribNormal(k).Posicion + iOrden
                            .Columns(vAtribNormal(k).Cod & sProv).Visible = vAtribNormal(k).Visible
                        Next k
                    End If
                    If UBound(vAtribRep) > 0 Then
                        For k = 1 To UBound(vAtribRep)
                            If vAtribRep(k).Cod <> vAtribRep(k - 1).Cod Then
                                If vAtribRep(k).Posicion <> 0 Then .Columns(vAtribRep(k).Cod & sProv).Position = vAtribRep(k).Posicion + iOrden
                                .Columns(vAtribRep(k).Cod & sProv).Visible = vAtribRep(k).ColVisible
                            End If
                        Next k
                    End If
                End If
            End If
            .Columns("Consumido" & sProv).Position = m_oVistaSeleccionada.ConsumidoPos + iOrden
            .Columns("Adjudicado" & sProv).Position = m_oVistaSeleccionada.AdjPos + iOrden
            .Columns("Ahorrado" & sProv).Position = m_oVistaSeleccionada.AhorroAdjPos + iOrden
            .Columns("Ahorro%" & sProv).Position = m_oVistaSeleccionada.AhorroAdjPorcenPos + iOrden
            .Columns("Importe" & sProv).Position = m_oVistaSeleccionada.ImportePos + iOrden
            .Columns("AhorroOferta" & sProv).Position = m_oVistaSeleccionada.AhorroOfePos + iOrden
            .Columns("AhorroOfe%" & sProv).Position = m_oVistaSeleccionada.AhorroOfePorcenPos + iOrden
            .Columns("Consumido" & sProv).Visible = m_oVistaSeleccionada.ConsumidoVisible
            .Columns("Adjudicado" & sProv).Visible = m_oVistaSeleccionada.AdjVisible
            .Columns("Ahorrado" & sProv).Visible = m_oVistaSeleccionada.AhorroAdjVisible
            .Columns("Ahorro%" & sProv).Visible = m_oVistaSeleccionada.AhorroAdjPorcenVisible
            .Columns("Importe" & sProv).Visible = m_oVistaSeleccionada.ImporteVisible
            .Columns("AhorroOferta" & sProv).Visible = m_oVistaSeleccionada.AhorroOfeVisible
            .Columns("AhorroOfe%" & sProv).Visible = m_oVistaSeleccionada.AhorroOfePorcenVisible
            'Width de las columnas: Pone los width en el mismo orden en que est�n los position,porque sino no
            'lo hace bien
            For j = 0 To .Groups(i).Columns.Count - 1
                sColumna = ""
                lIndex = .Groups(i).ColPosition(j)
                sColumna = Left(.Groups(i).Columns(lIndex).Name, Len(.Groups(i).Columns(lIndex).Name) - Len(sProv))
                If sColumna <> "" Then
                    Select Case sColumna
                        Case "Consumido"
                            .Columns("Consumido" & sProv).Width = m_oVistaSeleccionada.ConsumidoWidth
                        Case "Adjudicado"
                            .Columns("Adjudicado" & sProv).Width = m_oVistaSeleccionada.AdjWidth
                        Case "Ahorrado"
                            .Columns("Ahorrado" & sProv).Width = m_oVistaSeleccionada.AhorroAdjWidth
                        Case "Ahorro%"
                            .Columns("Ahorro%" & sProv).Width = m_oVistaSeleccionada.AhorroAdjPorcenWidth
                        Case "Importe"
                            .Columns("Importe" & sProv).Width = m_oVistaSeleccionada.ImporteWidth
                        Case "AhorroOferta"
                            .Columns("AhorroOferta" & sProv).Width = m_oVistaSeleccionada.AhorroOfeWidth
                        Case "AhorroOfe%"
                            .Columns("AhorroOfe%" & sProv).Width = m_oVistaSeleccionada.AhorroOfePorcenWidth
                        Case "VINCULADO_GRU"
                            .Columns("VINCULADO_GRU" & sProv).Width = m_oVistaSeleccionada.SolicVinculadaWidth
                        Case Else  'Comprueba si es un atributo
                            If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
                                If sColumna <> "CodGrupo" And sColumna <> "NumOfe" And sColumna <> "CERRADO" Then
                                    'Es un atributo
                                    If UBound(vAtribNormal) > 0 Then
                                        For k = 1 To UBound(vAtribNormal)
                                            If vAtribNormal(k).Cod = sColumna Then
                                                If vAtribNormal(k).Width = 0 Then
                                                    .Groups(i).Columns(lIndex).Width = 500
                                                Else
                                                    .Groups(i).Columns(lIndex).Width = vAtribNormal(k).Width
                                                End If
                                                Exit For
                                            End If
                                        Next k
                                    End If
                                    If UBound(vAtribRep) > 0 Then
                                        For k = 1 To UBound(vAtribRep)
                                            If vAtribRep(k).Cod <> vAtribRep(k - 1).Cod Then
                                                If vAtribRep(k).Cod = sColumna Then
                                                    If vAtribRep(k).Width = 0 Then
                                                        .Columns(vAtribRep(k).Cod & sProv).Width = 500
                                                    Else
                                                        .Columns(vAtribRep(k).Cod & sProv).Width = vAtribRep(k).Width
                                                    End If
                                                End If
                                            End If
                                        Next k
                                    End If
                                End If
                            End If
                    End Select
                End If
            Next
            sdbgtotales2.Columns("Consumido" & sProv).Visible = m_oVistaSeleccionada.ConsumidoVisible
            sdbgtotales2.Columns("Ahorrado" & sProv).Visible = m_oVistaSeleccionada.AhorroAdjVisible
            If iVisibleInvertida > 0 Then
                sdbgtotales2.Columns("Consumido" & sProv).Width = sdbgtotales2.Groups(i).Width / iVisibleInvertida
                sdbgtotales2.Columns("Adjudicado" & sProv).Width = sdbgtotales2.Groups(i).Width / iVisibleInvertida
                sdbgtotales2.Columns("Ahorrado" & sProv).Width = sdbgtotales2.Groups(i).Width / iVisibleInvertida
            End If
            'Si es un proceso de adminitraci�n p�blica oculta los grupos cuyo sobre est� no visible
            If g_oPlantillaSeleccionada.AdminPub = True Then
                lSobre = g_oPlantillaSeleccionada.Grupos.Item(CStr(.Groups(i).TagVariant)).Sobre
                If m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(lSobre)).Visible = False Then
                    .Groups(i).Visible = False
                    sdbgtotales2.Groups(i).Visible = False
                End If
            End If
        End If
    Next i
    'atributos visibles
    If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
        If UBound(vAtribRep) > 0 Then
            'Se recorre todas las filas y dentro de cada fila los grupos para poner el atributo en gris o no
            For j = 0 To .Rows - 1
                .Bookmark = j
                For k = 1 To UBound(vAtribRep)
                    If vAtribRep(k).Visible = True Then
                         'Visualiza el valor ofertado y quita el gris del campo.
                        For i = 1 To .Groups.Count - 1
                            If .Groups(i).TagVariant = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(vAtribRep(k).Id)).codgrupo Then
                                .Columns(vAtribRep(k).Cod & .Groups(i).TagVariant).CellStyleSet "Normal", j
                            End If
                        Next i
                    Else
                        'El atributo no est� visible,lo pone en gris
                        For i = 1 To .Groups.Count - 1
                            If .Groups(i).TagVariant = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(vAtribRep(k).Id)).codgrupo Then
                                .Columns(vAtribRep(k).Cod & .Groups(i).TagVariant).CellStyleSet "GrayHead", j
                            End If
                        Next i
                    End If
                Next k
            Next j
        End If
    End If
    'Configura el ancho de las filas de las grids de datos
    If m_oVistaSeleccionada.AnchoFila = 0 Or IsNull(m_oVistaSeleccionada.AnchoFila) Then
        sdbgGruposProve.RowHeight = ANCHO_FILA_GEN
        .RowHeight = ANCHO_FILA_GEN
    Else
        sdbgGruposProve.RowHeight = m_oVistaSeleccionada.AnchoFila
        .RowHeight = m_oVistaSeleccionada.AnchoFila
    End If
End With

'''''''''''''''''''''Redimensiona sdbgProce''''''''''''''''''''
With sdbgProce
    'Redimensiona la grid de proceso
    For i = 1 To .Groups.Count - 1
        'Primero hace todos los atributos de proceso visibles,porque sino no funciona bien
        For j = 0 To .Groups(i).Columns.Count - 1
            .Groups(i).Columns(j).Visible = True
        Next j
        .Groups(i).Width = sdbgGruposProve.Groups(i).Width
        sProv = .Groups(i).TagVariant
        iOrden = (.Groups(i).Columns.Count * (i - 1))
        If g_oPlantillaSeleccionada.Solicitud = EnProceso Then
            If (m_oVistaSeleccionada.Vista <> TipoDeVistaDefecto.vistainicial) Then
                m_bGridProcVisible = m_bGridProcVisible Or m_oVistaSeleccionada.SolicVinculadaVisible
                MaxPosVincPro = .Columns("VINCULADO_PRO" & sProv).Position
                .Columns("VINCULADO_PRO" & sProv).Position = m_oVistaSeleccionada.SolicVinculadaPos + iOrden
                If .Columns("VINCULADO_PRO" & sProv).Group > i Then
                    .Columns("VINCULADO_PRO" & sProv).Position = MaxPosVincPro
                    m_oVistaSeleccionada.SolicVinculadaPos = MaxPosVincPro - iOrden
                End If
                MaxPosVincPro = sdbgProveGrupos.Columns("VINCULADO_PRO").Position
                sdbgProveGrupos.Columns("VINCULADO_PRO").Position = m_oVistaSeleccionada.SolicVinculadaPos + 2
                If sdbgProveGrupos.Columns("VINCULADO_PRO").Group > 1 Then
                    'Grabado De vista Responsable/Plantilla/Otro usuario MAL.
                    sdbgProveGrupos.Columns("VINCULADO_PRO").Position = MaxPosVincPro
                    m_oVistaSeleccionada.SolicVinculadaPos = sdbgProveGrupos.Columns("VINCULADO_PRO").Position - 2
                End If
            End If
            .Columns("VINCULADO_PRO" & sProv).Visible = m_oVistaSeleccionada.SolicVinculadaVisible
            sdbgProveGrupos.Columns("VINCULADO_PRO").Visible = m_oVistaSeleccionada.SolicVinculadaVisible
        End If
        For Each oatrib In m_oVistaSeleccionada.PlantConfVistasProceAtrib
            If oatrib.ambito = TipoAmbitoProceso.AmbProceso Then
                If oatrib.Posicion <> 0 Then
                    .Columns(oatrib.Atributo & sProv).Position = oatrib.Posicion + iOrden
                    sdbgProveGrupos.Columns(CStr(oatrib.CodAtributo)).Position = oatrib.Posicion + 2
                End If
                If oatrib.Visible = True Then m_bGridProcVisible = True
                .Columns(oatrib.Atributo & sProv).Visible = oatrib.Visible
                sdbgProveGrupos.Columns(CStr(oatrib.CodAtributo)).Visible = oatrib.Visible
            End If
        Next
        'Ahora pone el width de las columnas en el mismo orden que los position
        For j = 0 To .Groups(i).Columns.Count - 1
            lIndex = .Groups(i).ColPosition(j)
            sColumna = Mid(.Groups(i).Columns(lIndex).Name, 1, InStr(1, .Groups(i).Columns(lIndex).Name, sProv) - 1)
            If sColumna <> "" Then
                If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib.Item(CStr(sColumna)) Is Nothing Then
                    .Groups(i).Columns(lIndex).Width = m_oVistaSeleccionada.PlantConfVistasProceAtrib.Item(CStr(sColumna)).Width
                    sdbgProveGrupos.Columns(CStr(sColumna)).Width = m_oVistaSeleccionada.PlantConfVistasProceAtrib.Item(CStr(sColumna)).Width
                End If
                If sColumna = "VINCULADO_PRO" Then
                    .Groups(i).Columns(lIndex).Width = m_oVistaSeleccionada.SolicVinculadaWidth
                    sdbgProveGrupos.Columns(CStr(sColumna)).Width = m_oVistaSeleccionada.SolicVinculadaWidth
                End If
            End If
        Next j
    Next i
    'Si est� visible alg�n atributo de proceso o la solic vinculada se muestra la grid de proceso
    If m_bGridProcVisible = True Then
        If m_oVistaSeleccionada.TipoVision = 0 Then
            If .Visible = False Then sdbgGruposProve.Height = sstabComparativa.Height - .Height - sdbgTotalesProve.Height - 520
            .Visible = True
            'Muestra visibles s�lo los grupos que est�n visibles
            For i = 1 To .Groups.Count - 1
                If sdbgGruposProve.Groups(i).Visible = True Then
                    .Groups(i).Visible = True
                Else
                    .Groups(i).Visible = False
                End If
            Next i
        Else
            .Visible = False
        End If
        sdbgGruposProve.Top = .Top + .Height
        sdbgGruposProve.GroupHeaders = False
        sdbgProveGrupos.Groups(1).Visible = True
        sdbgtotales2.Groups(1).Visible = True
        sdbgTotalesProve.Top = sdbgGruposProve.Height + sdbgGruposProve.Top + 1 - 60
    Else
        If .Visible = True Then sdbgGruposProve.Height = sstabComparativa.Height - sdbgTotalesProve.Height - 520
        .Visible = False
        sdbgGruposProve.GroupHeaders = True
        sdbgGruposProve.Top = 855
        sdbgProveGrupos.Groups(1).Visible = False
        sdbgtotales2.Groups(1).Visible = False
    End If
    If m_oVistaSeleccionada.TipoVision = 0 Then
        sdbgGruposProve.Visible = True
        sdbgTotalesProve.Visible = True
        sdbgProveGrupos.Visible = False
        sdbgtotales2.Visible = False
    Else
        sdbgGruposProve.Visible = False
        sdbgTotalesProve.Visible = False
        sdbgtotales2.Groups(0).Width = sdbgProveGrupos.Groups(0).Width
        sdbgProveGrupos.Visible = True
        sdbgtotales2.Visible = True
    End If
    sdbgGruposProve.MoveFirst
    sdbgProveGrupos.MoveFirst
End With
End Sub

''' <summary>
''' Muestra/Oculta y redimensiona las columnas y grupos de sdgAll, columnas fijas y las de los diferentes proveedores
''' Tambien redimensiona las columnas fijas de sdbgTotalesAll
''' </summary>
''' <remarks>Llamada desde: cmdEliminarVistaAll_Click    sdbcVistaActAll_CloseUp  CargarAdjudicacionesAll
''' ; Tiempo m�ximo:0</remarks>
Private Sub RedimensionarGridAll()
Dim i As Integer
Dim sGrupo As String
Dim oatrib As CPlantConfVistaAllAtrib
Dim iOrden As Integer
Dim sColumna As String
Dim j As Integer
Dim iIndex As Integer
Dim inumColumns As Integer
With sdbgAll
    'Si se ha hecho un scroll se vuelve a hacerlo para dejar el grid en la posici�n
    'original porque sino no funciona bien
    If .GrpPosition(0) > 0 Then
        DoEvents
        i = .Groups(0).Position
        .Scroll -i, 0
        .Update
        sdbgTotalesAll.Scroll -i, 0
        sdbgTotalesAll.Update
    End If
    For j = 0 To .Groups.Count - 1
        .Groups(j).Position = j
    Next j
    For j = 0 To sdbgTotalesAll.Columns.Count - 1
        sdbgTotalesAll.Columns(j).Position = j
    Next j
    .AllowColumnMoving = ssRelocateWithinGroup
    '''''''''''''''''''''Redimensiona ''''''''''''''''''''
    .LevelCount = 3
    'Redimensiona el grupo 0 (el de las descripciones):
    .Groups(0).Width = m_oVistaSeleccionadaAll.Grupo0Width
    'Level de los campos
    .Columns("DESCR").Level = 0
    .Columns("ANYO").Level = m_oVistaSeleccionadaAll.AnyoLevel
    .Columns("PROV").Level = m_oVistaSeleccionadaAll.ProveedorLevel
    .Columns("PRECAPE").Level = m_oVistaSeleccionadaAll.PresUniLevel
    .Columns("OBJ").Level = m_oVistaSeleccionadaAll.OBJLevel
    .Columns("AHORROIMP").Level = m_oVistaSeleccionadaAll.AhorroLevel
    .Columns("AHORROPORCEN").Level = m_oVistaSeleccionadaAll.AhorroPorcenLevel
    .Columns("IMP").Level = m_oVistaSeleccionadaAll.ImporteLevel
    .Columns("CANT").Level = m_oVistaSeleccionadaAll.CantidadLevel
    .Columns("PORCENADJ").Level = m_oVistaSeleccionadaAll.AdjLevel
    .Columns("VINCULADO_ITEM").Level = m_oVistaSeleccionadaAll.SolicVinculadaLevel
    .Columns("INI").Level = m_oVistaSeleccionadaAll.FecIniSumLevel
    .Columns("FIN").Level = m_oVistaSeleccionadaAll.FecFinSumLevel
    .Columns("GMN").Level = m_oVistaSeleccionadaAll.EstrMatLevel
    'Posici�n de los campos del grupo 0(les pone las posiciones en orden,porque sino a veces no funciona bien)
    OrdenarColsGrupo0 (False)
    .Columns("DESCR").Position = 0
    For i = 0 To 12
        Select Case m_arrOrden(i)
            Case "ANYO"
                .Groups(0).Columns("ANYO").Position = m_oVistaSeleccionadaAll.AnyoPos
            Case "PROV"
                .Groups(0).Columns("PROV").Position = m_oVistaSeleccionadaAll.ProveedorPos
            Case "PRECAPE"
                .Groups(0).Columns("PRECAPE").Position = m_oVistaSeleccionadaAll.PresUniPos
            Case "OBJ"
                .Groups(0).Columns("OBJ").Position = m_oVistaSeleccionadaAll.OBJPos
            Case "AHORROIMP"
                .Groups(0).Columns("AHORROIMP").Position = m_oVistaSeleccionadaAll.AhorroPos
            Case "AHORROPORCEN"
                .Groups(0).Columns("AHORROPORCEN").Position = m_oVistaSeleccionadaAll.AhorroPorcenPos
            Case "IMP"
                .Groups(0).Columns("IMP").Position = m_oVistaSeleccionadaAll.ImportePos
            Case "CANT"
                .Groups(0).Columns("CANT").Position = m_oVistaSeleccionadaAll.CantidadPos
            Case "PORCENADJ"
                .Groups(0).Columns("PORCENADJ").Position = m_oVistaSeleccionadaAll.AdjPos
            Case "VINCULADO_ITEM"
                .Groups(0).Columns("VINCULADO_ITEM").Position = m_oVistaSeleccionadaAll.SolicVinculadaPos
            Case "INI"
                .Groups(0).Columns("INI").Position = m_oVistaSeleccionadaAll.FecIniSumPos
            Case "FIN"
                .Groups(0).Columns("FIN").Position = m_oVistaSeleccionadaAll.FecFinSumPos
            Case "GMN"
                .Groups(0).Columns("GMN").Position = m_oVistaSeleccionadaAll.EstrMatPos
        End Select
    Next i
    'Visibilidad de los campos del grupo 0
    .Columns("DESCR").Visible = True
    .Columns("ANYO").Visible = m_oVistaSeleccionadaAll.AnyoVisible
    If g_oPlantillaSeleccionada.AdminPub = True Then
        .Columns("PROV").Visible = False
    Else
        .Columns("PROV").Visible = m_oVistaSeleccionadaAll.ProveedorVisible
    End If
    .Columns("PRECAPE").Visible = m_oVistaSeleccionadaAll.PresUniVisible
    .Columns("OBJ").Visible = m_oVistaSeleccionadaAll.OBJVisible
    .Columns("AHORROIMP").Visible = m_oVistaSeleccionadaAll.AhorroVisible
    .Columns("AHORROPORCEN").Visible = m_oVistaSeleccionadaAll.AhorroPorcenVisible
    .Columns("IMP").Visible = m_oVistaSeleccionadaAll.ImporteVisible
    .Columns("CANT").Visible = m_oVistaSeleccionadaAll.CantidadVisible
    .Columns("PORCENADJ").Visible = m_oVistaSeleccionadaAll.AdjVisible
    .Columns("VINCULADO_ITEM").Visible = m_oVistaSeleccionadaAll.SolicVinculadaVisible
    .Columns("INI").Visible = m_oVistaSeleccionadaAll.FecIniSumVisible
    .Columns("FIN").Visible = m_oVistaSeleccionadaAll.FecFinSumVisible
    .Columns("GMN").Visible = m_oVistaSeleccionadaAll.EstrMatVisible
    'Tama�o de los campos del grupo 0 (se pone los widths en el mismo orden en que est�n los position,porque
    'sino a veces no funciona bien)
    For j = 0 To .Groups(0).Columns.Count - 1
        iIndex = .Groups(0).ColPosition(j)
        Select Case .Groups(0).Columns(iIndex).Name
            Case "DESCR"
                .Columns("DESCR").Width = m_oVistaSeleccionadaAll.DescrWidth
            Case "ANYO"
                .Columns("ANYO").Width = m_oVistaSeleccionadaAll.AnyoWidth
            Case "PROV"
                .Columns("PROV").Width = m_oVistaSeleccionadaAll.ProveedorWidth
            Case "PRECAPE"
                .Columns("PRECAPE").Width = m_oVistaSeleccionadaAll.PresUniWidth
            Case "OBJ"
                .Columns("OBJ").Width = m_oVistaSeleccionadaAll.OBJWidth
            Case "AHORROIMP"
                .Columns("AHORROIMP").Width = m_oVistaSeleccionadaAll.AhorroWidth
            Case "AHORROPORCEN"
                .Columns("AHORROPORCEN").Width = m_oVistaSeleccionadaAll.AhorroPorcenWidth
            Case "IMP"
                .Columns("IMP").Width = m_oVistaSeleccionadaAll.ImporteWidth
            Case "CANT"
                .Columns("CANT").Width = m_oVistaSeleccionadaAll.CantidadWidth
            Case "PORCENADJ"
                .Columns("PORCENADJ").Width = m_oVistaSeleccionadaAll.AdjWidth
            Case "VINCULADO_ITEM"
                .Columns("VINCULADO_ITEM").Width = m_oVistaSeleccionadaAll.SolicVinculadaWidth
            Case "INI"
                .Columns("INI").Width = m_oVistaSeleccionadaAll.FecIniSumWidth
            Case "FIN"
                .Columns("FIN").Width = m_oVistaSeleccionadaAll.FecFinSumWidth
            Case "GMN"
                .Columns("GMN").Width = m_oVistaSeleccionadaAll.EstrMatWidth
        End Select
    Next j
    m_iLevelCantMaxAll = 0
    m_iLevelCodProveAll = 0
    m_iLevelNumOfeAll = 0
    m_iLevelHomAll = 0
    m_iLevelIndiceAll = 0
    'Ordena los atributos:
    OrdenarColsGrupoProves (False)
    'Redimensiona el resto de grupos (para cada proveedor)
    For i = 2 To 4
        sGrupo = .Groups(i).TagVariant
        If .Groups(i).Visible = False Then
            .Groups(i).Visible = True
            sdbgTotalesAll.Columns(i).Visible = True
        End If
        .Groups(i).Width = m_oVistaSeleccionadaAll.GruposWidth   'tama�o del grupo
        sdbgTotalesAll.Columns(i).Width = m_oVistaSeleccionadaAll.GruposWidth
        '''''Lo siguiente lo hacemos en este orden para que los campos de la grid se redimensionen correctamente
        ''''Level de los campos:
        .Columns("IMPADJ" & sGrupo).Level = m_oVistaSeleccionadaAll.ImpAdjLevel
        .Columns("Precio" & sGrupo).Level = m_oVistaSeleccionadaAll.PrecioProvLevel
        .Columns("AdjPorcen" & sGrupo).Level = m_oVistaSeleccionadaAll.AdjProvLevel
        .Columns("AdjCantidad" & sGrupo).Level = m_oVistaSeleccionadaAll.CantProvLevel
        'Atributos
        For Each oatrib In m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib
            .Columns(oatrib.Atributo & sGrupo).Level = oatrib.Fila
        Next
        ''''Posici�n de los campos:
        iOrden = m_iNumColsAll + (.Groups(i).Columns.Count * (i - 2))
        inumColumns = 3
        If Not m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib Is Nothing Then inumColumns = inumColumns + m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Count
        For j = 0 To inumColumns
            Select Case m_arrOrdenColsAll(j)
                Case "IMPADJ"
                    .Columns("IMPADJ" & sGrupo).Position = m_oVistaSeleccionadaAll.ImpAdjPos + iOrden
                Case "PRECIO"
                    .Columns("Precio" & sGrupo).Position = m_oVistaSeleccionadaAll.PrecioProvPos + iOrden
                Case "ADJPROV"
                    .Columns("AdjPorcen" & sGrupo).Position = m_oVistaSeleccionadaAll.AdjProvPos + iOrden
                Case "CANTPROV"
                    .Columns("AdjCantidad" & sGrupo).Position = m_oVistaSeleccionadaAll.CantProvPos + iOrden
                Case Else
                    'Es un atributo
                    .Columns(m_arrOrdenColsAll(j) & sGrupo).Position = m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(m_arrOrdenColsAll(j))).Posicion + iOrden
            End Select
        Next j
         ''''Level de los campos (lo volvemos a poner despu�s de la posici�n porque sino no funciona bien)
        For j = inumColumns To 0 Step -1
            Select Case m_arrOrdenColsAll(j)
                Case "IMPADJ"
                    .Columns("IMPADJ" & sGrupo).Level = m_oVistaSeleccionadaAll.ImpAdjLevel
                Case "PRECIO"
                    .Columns("Precio" & sGrupo).Level = m_oVistaSeleccionadaAll.PrecioProvLevel
                Case "ADJPROV"
                    .Columns("AdjPorcen" & sGrupo).Level = m_oVistaSeleccionadaAll.AdjProvLevel
                Case "CANTPROV"
                    .Columns("AdjCantidad" & sGrupo).Level = m_oVistaSeleccionadaAll.CantProvLevel
                Case Else
                    'Es un atributo
                    .Columns(m_arrOrdenColsAll(j) & sGrupo).Level = m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(m_arrOrdenColsAll(j))).Fila
            End Select
        Next j
        ''''Visibilidad de los campos:
        'El importe adjudicado,precio adjudicado,cant.adjudicada y %adjudicado
        .Columns("IMPADJ" & sGrupo).Visible = m_oVistaSeleccionadaAll.ImpAdjVisible
        .Columns("Precio" & sGrupo).Visible = m_oVistaSeleccionadaAll.PrecioProvVisible
        .Columns("AdjPorcen" & sGrupo).Visible = m_oVistaSeleccionadaAll.AdjProvVisible
        .Columns("AdjCantidad" & sGrupo).Visible = m_oVistaSeleccionadaAll.CantProvVisible
        'Atributos
        For Each oatrib In m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib
            If g_oPlantillaSeleccionada.AdminPub = True Then
                'Si el sobre al que pertenece el grupo del atributo no est� visible no se visualiza
                'el atributo:
                Dim sGru As String
                sGru = NullToStr(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo)
                If sGru <> "" Then
                    If m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(g_oPlantillaSeleccionada.Grupos.Item(sGru).Sobre)).Visible = False Then
                        .Columns(oatrib.Atributo & sGrupo).Visible = False
                    Else
                        .Columns(oatrib.Atributo & sGrupo).Visible = oatrib.Visible
                    End If
                Else
                    .Columns(oatrib.Atributo & sGrupo).Visible = oatrib.Visible
                End If
            Else
                .Columns(oatrib.Atributo & sGrupo).Visible = oatrib.Visible
            End If
        Next
        '''Tama�o de los campos:
        'El importe adjudicado,precio adjudicado,cant.adjudicada y %adjudicado
        For j = 0 To .Groups(i).Columns.Count - 1
            sColumna = ""
            iIndex = .Groups(i).ColPosition(j)
            sColumna = Mid(.Groups(i).Columns(iIndex).Name, 1, InStr(.Groups(i).Columns(iIndex).Name, .Groups(i).TagVariant) - 1)
            Select Case sColumna
                Case "IMPADJ"
                    If m_oVistaSeleccionadaAll.ImpAdjWidth = 0 Then
                        .Columns("IMPADJ" & sGrupo).Width = 500
                    Else
                        .Columns("IMPADJ" & sGrupo).Width = m_oVistaSeleccionadaAll.ImpAdjWidth
                    End If
                Case "Precio"
                    If m_oVistaSeleccionadaAll.PrecioProvWidth = 0 Then
                        .Columns("Precio" & sGrupo).Width = 1600
                    Else
                        .Columns("Precio" & sGrupo).Width = m_oVistaSeleccionadaAll.PrecioProvWidth
                    End If
                Case "AdjPorcen"
                    If m_oVistaSeleccionadaAll.AdjProvWidth = 0 Then
                        .Columns("AdjPorcen" & sGrupo).Width = 700
                    Else
                        .Columns("AdjPorcen" & sGrupo).Width = m_oVistaSeleccionadaAll.AdjProvWidth
                    End If
                Case "AdjCantidad"
                    If m_oVistaSeleccionadaAll.CantProvWidth = 0 Then
                        .Columns("AdjCantidad" & sGrupo).Width = 900
                    Else
                        .Columns("AdjCantidad" & sGrupo).Width = m_oVistaSeleccionadaAll.CantProvWidth
                    End If
                Case Else  'Comprueba si la columna es un atributo
                    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
                        If sColumna <> "CantMax" And sColumna <> "CodProve" And sColumna <> "NumOfe" And sColumna <> "Hom" And sColumna <> "Indice" Then
                            'atributos
                            If Not m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(sColumna)) Is Nothing Then
                                If m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(sColumna)).Width = 0 Then
                                    .Columns(sColumna & sGrupo).Width = 500
                                Else
                                    .Columns(sColumna & sGrupo).Width = m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(sColumna)).Width
                                End If
                            End If
                        End If
                    End If
            End Select
        Next j
    Next i
    m_iLevelCantMaxAll = .Groups(2).Columns("CantMax" & .Groups(2).TagVariant).Level
    m_iLevelCodProveAll = .Groups(2).Columns("CodProve" & .Groups(2).TagVariant).Level
    m_iLevelNumOfeAll = .Groups(2).Columns("NumOfe" & .Groups(2).TagVariant).Level
    m_iLevelHomAll = .Groups(2).Columns("Hom" & .Groups(2).TagVariant).Level
    m_iLevelIndiceAll = .Groups(2).Columns("Indice" & .Groups(2).TagVariant).Level
    If m_oVistaSeleccionadaAll.OcultarFila2 = True Then
        m_bCargandoVista = True
        sdbgAll_GrpHeadClick 0
        m_bCargandoVista = False
    End If
    'Pone el ancho de las filas
    If m_oVistaSeleccionadaAll.AnchoFila = 0 Or IsNull(m_oVistaSeleccionadaAll.AnchoFila) Then
        .RowHeight = ANCHO_FILA_GR
    Else
        .RowHeight = m_oVistaSeleccionadaAll.AnchoFila
    End If
    'Visualiza la columna de Objetivos totales s�lo cuando est� visible la de Objetivos
    If sdbgTotalesAll.Rows > 0 And .Groups.Count > 2 Then
        If .Groups(2).Visible = True Then
            sdbgTotalesAll.SplitterPos = 2
        End If
    End If
    If .Columns("OBJ").Width > 1000 Then
        sdbgTotalesAll.Columns("OBJTOT").Width = .Columns("OBJ").Width
    Else
        sdbgTotalesAll.Columns("OBJTOT").Width = 1000
    End If
    sdbgTotalesAll.Columns(0).Width = .Groups(0).Width - sdbgTotalesAll.Columns("OBJTOT").Width
    If .Columns("OBJ").Visible = False Then
        sdbgTotalesAll.Columns("OBJTOT").caption = ""
        sdbgTotalesAll.MoveFirst
    Else
        sdbgTotalesAll.Columns("OBJTOT").caption = sCaptionTotales
        sdbgTotalesAll.MoveFirst
    End If
    .MoveFirst
End With
End Sub

''' <summary>
''' Muestra/Oculta y redimensiona las columnas y grupos de sdgAdj, columnas fijas y las de los diferentes proveedores
''' Tambien redimensiona las columnas fijas de sdbgTotales
''' </summary>
''' <remarks>Llamada desde: cmdEliminarVistaGR_Click    sdbcVistaActGr_CloseUp  CargarAdjudicacionesGrupo
''' ; Tiempo m�ximo:0</remarks>
Private Sub RedimensionarGridGrupo()
Dim i As Integer
Dim sGrupo As String
Dim oatrib As CPlantConfVistaGrupoAtrib
Dim iOrden As Integer
Dim sColumna As String
Dim j As Integer
Dim iIndex As Integer
Dim inumColumns As Integer
Dim oAtribOrden As CAtributo
With sdbgAdj
    'Si se ha hecho un scroll se vuelve a hacerlo para dejar el grid en la posici�n
    'original porque sino no funciona bien
    If .GrpPosition(0) > 0 Then
        DoEvents
        i = .Groups(0).Position
        .Scroll -i, 0
        .Update
        sdbgTotales.Scroll -i, 0
        sdbgTotales.Update
    End If
    For j = 0 To .Groups.Count - 1
        .Groups(j).Position = j
    Next j
    For j = 0 To sdbgTotales.Columns.Count - 1
        sdbgTotales.Columns(j).Position = j
    Next j
    .AllowColumnMoving = ssRelocateWithinGroup
    '''''''''''''''''''''Redimensiona ''''''''''''''''''''
    .LevelCount = 3
    'Redimensiona el grupo 0 (el de las descripciones):
    .Groups(0).Width = m_oVistaSeleccionadaGr.Grupo0Width
    'Level de los campos del grupo 0
    .Columns("DESCR").Level = 0
    .Columns("ANYO").Level = m_oVistaSeleccionadaGr.AnyoLevel
    .Columns("PROV").Level = m_oVistaSeleccionadaGr.ProveedorLevel
    .Columns("PRECAPE").Level = m_oVistaSeleccionadaGr.PresUniLevel
    .Columns("OBJ").Level = m_oVistaSeleccionadaGr.OBJLevel
    .Columns("AHORROIMP").Level = m_oVistaSeleccionadaGr.AhorroLevel
    .Columns("AHORROPORCEN").Level = m_oVistaSeleccionadaGr.AhorroPorcenLevel
    .Columns("IMP").Level = m_oVistaSeleccionadaGr.ImporteLevel
    .Columns("CANT").Level = m_oVistaSeleccionadaGr.CantidadLevel
    .Columns("PORCENADJ").Level = m_oVistaSeleccionadaGr.AdjLevel
    .Columns("VINCULADO_ITEM").Level = m_oVistaSeleccionadaGr.SolicVinculadaLevel
    .Columns("INI").Level = m_oVistaSeleccionadaGr.FecIniSumLevel
    .Columns("FIN").Level = m_oVistaSeleccionadaGr.FecFinSumLevel
    .Columns("GMN").Level = m_oVistaSeleccionadaGr.EstrMatLevel
    'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
    OrdenarColsGrupo0 (True)
    'Posici�n de los campos del grupo 0
    .Columns("DESCR").Position = 0
    For i = 0 To 12
        Select Case m_arrOrden(i)
            Case "ANYO"
                .Columns("ANYO").Position = m_oVistaSeleccionadaGr.AnyoPos
            Case "PROV"
                .Columns("PROV").Position = m_oVistaSeleccionadaGr.ProveedorPos
            Case "PRECAPE"
                .Columns("PRECAPE").Position = m_oVistaSeleccionadaGr.PresUniPos
            Case "OBJ"
                .Columns("OBJ").Position = m_oVistaSeleccionadaGr.OBJPos
            Case "AHORROIMP"
                .Columns("AHORROIMP").Position = m_oVistaSeleccionadaGr.AhorroPos
            Case "AHORROPORCEN"
                .Columns("AHORROPORCEN").Position = m_oVistaSeleccionadaGr.AhorroPorcenPos
            Case "IMP"
                .Columns("IMP").Position = m_oVistaSeleccionadaGr.ImportePos
            Case "CANT"
                .Columns("CANT").Position = m_oVistaSeleccionadaGr.CantidadPos
            Case "PORCENADJ"
                .Columns("PORCENADJ").Position = m_oVistaSeleccionadaGr.AdjPos
            Case "VINCULADO_ITEM"
                .Columns("VINCULADO_ITEM").Position = m_oVistaSeleccionadaGr.SolicVinculadaPos
            Case "INI"
                .Columns("INI").Position = m_oVistaSeleccionadaGr.FecIniSumPos
            Case "FIN"
                .Columns("FIN").Position = m_oVistaSeleccionadaGr.FecFinSumPos
            Case "GMN"
                .Columns("GMN").Position = m_oVistaSeleccionadaGr.EstrMatPos
        End Select
    Next i
    'Visibilidad de los campos del grupo 0
    .Columns("DESCR").Visible = True
    .Columns("ANYO").Visible = m_oVistaSeleccionadaGr.AnyoVisible
    If g_oPlantillaSeleccionada.AdminPub = True Then
        .Columns("PROV").Visible = False
    Else
        .Columns("PROV").Visible = m_oVistaSeleccionadaGr.ProveedorVisible
    End If
    .Columns("PRECAPE").Visible = m_oVistaSeleccionadaGr.PresUniVisible
    .Columns("OBJ").Visible = m_oVistaSeleccionadaGr.OBJVisible
    .Columns("AHORROIMP").Visible = m_oVistaSeleccionadaGr.AhorroVisible
    .Columns("AHORROPORCEN").Visible = m_oVistaSeleccionadaGr.AhorroPorcenVisible
    .Columns("IMP").Visible = m_oVistaSeleccionadaGr.ImporteVisible
    .Columns("CANT").Visible = m_oVistaSeleccionadaGr.CantidadVisible
    .Columns("PORCENADJ").Visible = m_oVistaSeleccionadaGr.AdjVisible
    .Columns("VINCULADO_ITEM").Visible = m_oVistaSeleccionadaGr.SolicVinculadaVisible
    .Columns("INI").Visible = m_oVistaSeleccionadaGr.FecIniSumVisible
    .Columns("FIN").Visible = m_oVistaSeleccionadaGr.FecFinSumVisible
    .Columns("GMN").Visible = m_oVistaSeleccionadaGr.EstrMatVisible
    'Tama�o de los campos del grupo 0
    'Se pone el width en el mismo orden en el que est�n los positions,porque sino algunas veces no funciona
    'bien
    For j = 0 To .Groups(0).Columns.Count - 1
        iIndex = .Groups(0).ColPosition(j)
        Select Case .Groups(0).Columns(iIndex).Name
            Case "DESCR"
                .Columns("DESCR").Width = m_oVistaSeleccionadaGr.DescrWidth
            Case "ANYO"
                .Columns("ANYO").Width = m_oVistaSeleccionadaGr.AnyoWidth
            Case "PROV"
                .Columns("PROV").Width = m_oVistaSeleccionadaGr.ProveedorWidth
            Case "PRECAPE"
                .Columns("PRECAPE").Width = m_oVistaSeleccionadaGr.PresUniWidth
            Case "OBJ"
                .Columns("OBJ").Width = m_oVistaSeleccionadaGr.OBJWidth
            Case "AHORROIMP"
                .Columns("AHORROIMP").Width = m_oVistaSeleccionadaGr.AhorroWidth
            Case "AHORROPORCEN"
                .Columns("AHORROPORCEN").Width = m_oVistaSeleccionadaGr.AhorroPorcenWidth
            Case "IMP"
                .Columns("IMP").Width = m_oVistaSeleccionadaGr.ImporteWidth
            Case "CANT"
                .Columns("CANT").Width = m_oVistaSeleccionadaGr.CantidadWidth
            Case "PORCENADJ"
                .Columns("PORCENADJ").Width = m_oVistaSeleccionadaGr.AdjWidth
            Case "VINCULADO_ITEM"
                .Columns("VINCULADO_ITEM").Width = m_oVistaSeleccionadaGr.SolicVinculadaWidth
            Case "INI"
                .Columns("INI").Width = m_oVistaSeleccionadaGr.FecIniSumWidth
            Case "FIN"
                .Columns("FIN").Width = m_oVistaSeleccionadaGr.FecFinSumWidth
            Case "GMN"
                .Columns("GMN").Width = m_oVistaSeleccionadaGr.EstrMatWidth
        End Select
    Next j
    'Ordena las posiciones de los grupos de proveedores:
    OrdenarColsGrupoProves True
    m_iLevelCantMax = 0
    m_iLevelCodProve = 0
    m_iLevelNumOfe = 0
    m_iLevelHom = 0
    m_iLevelIndice = 0
    'Redimensiona el resto de grupos (para cada proveedor)
    For i = 2 To 4
        sGrupo = .Groups(i).TagVariant
        If .Groups(i).Visible = False Then
            .Groups(i).Visible = True
            sdbgTotales.Columns(i).Visible = True
        End If
        .Groups(i).Width = m_oVistaSeleccionadaGr.GruposWidth   'tama�o del grupo
        sdbgTotales.Columns(i).Width = m_oVistaSeleccionadaGr.GruposWidth
        ''''Level de los campos:
        .Columns("IMPADJ" & sGrupo).Level = m_oVistaSeleccionadaGr.ImpAdjLevel
        .Columns("Precio" & sGrupo).Level = m_oVistaSeleccionadaGr.PrecioProvLevel
        .Columns("AdjPorcen" & sGrupo).Level = m_oVistaSeleccionadaGr.AdjProvLevel
        .Columns("AdjCantidad" & sGrupo).Level = m_oVistaSeleccionadaGr.CantProvLevel
        'Atributos
        For Each oatrib In m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib
            .Columns(oatrib.Atributo & sGrupo).Level = oatrib.Fila
        Next
        ''''Posici�n de los campos:
        iOrden = m_iNumColsGR + (.Groups(i).Columns.Count * (i - 2))
        inumColumns = 3
        If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
            For Each oAtribOrden In g_oPlantillaSeleccionada.Atributosvistas
                If oAtribOrden.ambito = AmbItem And (oAtribOrden.codgrupo = m_oGrupoSeleccionado.Codigo Or IsNull(oAtribOrden.codgrupo)) Then
                    inumColumns = inumColumns + 1
                End If
            Next
        End If
        For j = 0 To inumColumns
            Select Case m_arrOrdenColsGr(j)
                Case "IMPADJ"
                    .Columns("IMPADJ" & sGrupo).Position = m_oVistaSeleccionadaGr.ImpAdjPos + iOrden
                Case "PRECIO"
                    .Columns("Precio" & sGrupo).Position = m_oVistaSeleccionadaGr.PrecioProvPos + iOrden
                Case "ADJPROV"
                    .Columns("AdjPorcen" & sGrupo).Position = m_oVistaSeleccionadaGr.AdjProvPos + iOrden
                Case "CANTPROV"
                    .Columns("AdjCantidad" & sGrupo).Position = m_oVistaSeleccionadaGr.CantProvPos + iOrden
                Case Else
                    'Es un atributo
                    .Columns(m_arrOrdenColsGr(j) & sGrupo).Position = m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(m_arrOrdenColsGr(j))).Posicion + iOrden
            End Select
        Next j
        ''''Level de los campos (lo volvemos a poner despu�s de la posici�n porque sino no funciona bien)
        For j = inumColumns To 0 Step -1
            Select Case m_arrOrdenColsGr(j)
                Case "IMPADJ"
                    .Columns("IMPADJ" & sGrupo).Level = m_oVistaSeleccionadaGr.ImpAdjLevel
                Case "PRECIO"
                    .Columns("Precio" & sGrupo).Level = m_oVistaSeleccionadaGr.PrecioProvLevel
                Case "ADJPROV"
                    .Columns("AdjPorcen" & sGrupo).Level = m_oVistaSeleccionadaGr.AdjProvLevel
                Case "CANTPROV"
                    .Columns("AdjCantidad" & sGrupo).Level = m_oVistaSeleccionadaGr.CantProvLevel
                Case Else
                    'Es un atributo
                    .Columns(m_arrOrdenColsGr(j) & sGrupo).Level = m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(m_arrOrdenColsGr(j))).Fila
            End Select
        Next j
        ''''Visibilidad de los campos:
        'El importe adjudicado,precio adjudicado,cant.adjudicada y %adjudicado
        .Columns("IMPADJ" & sGrupo).Visible = m_oVistaSeleccionadaGr.ImpAdjVisible
        .Columns("Precio" & sGrupo).Visible = m_oVistaSeleccionadaGr.PrecioProvVisible
        .Columns("AdjPorcen" & sGrupo).Visible = m_oVistaSeleccionadaGr.AdjProvVisible
        .Columns("AdjCantidad" & sGrupo).Visible = m_oVistaSeleccionadaGr.CantProvVisible
        'Atributos
        For Each oatrib In m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib
            .Columns(oatrib.Atributo & sGrupo).Visible = oatrib.Visible
        Next
        ''''Tama�o de los campos:
        'El importe adjudicado,precio adjudicado,cant.adjudicada y %adjudicado
        For j = 0 To .Groups(i).Columns.Count - 1
            sColumna = ""
            iIndex = .Groups(i).ColPosition(j)
            sColumna = Mid(.Groups(i).Columns(iIndex).Name, 1, InStr(.Groups(i).Columns(iIndex).Name, .Groups(i).TagVariant) - 1)
            Select Case sColumna
                Case "IMPADJ"
                    If m_oVistaSeleccionadaGr.ImpAdjWidth = 0 Then
                        .Columns("IMPADJ" & sGrupo).Width = 500
                    Else
                        .Columns("IMPADJ" & sGrupo).Width = m_oVistaSeleccionadaGr.ImpAdjWidth
                    End If
                Case "Precio"
                    If m_oVistaSeleccionadaGr.PrecioProvWidth = 0 Then
                        .Columns("Precio" & sGrupo).Width = 1600
                    Else
                        .Columns("Precio" & sGrupo).Width = m_oVistaSeleccionadaGr.PrecioProvWidth
                    End If
                Case "AdjPorcen"
                    If m_oVistaSeleccionadaGr.AdjProvWidth = 0 Then
                        .Columns("AdjPorcen" & sGrupo).Width = 700
                    Else
                        .Columns("AdjPorcen" & sGrupo).Width = m_oVistaSeleccionadaGr.AdjProvWidth
                    End If
                Case "AdjCantidad"
                    If m_oVistaSeleccionadaGr.CantProvWidth = 0 Then
                        .Columns("AdjCantidad" & sGrupo).Width = 900
                    Else
                        .Columns("AdjCantidad" & sGrupo).Width = m_oVistaSeleccionadaGr.CantProvWidth
                    End If
                Case Else  'Comprueba si la columna es un atributo
                    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
                        If sColumna <> "CantMax" And sColumna <> "CodProve" And sColumna <> "NumOfe" And sColumna <> "Hom" And sColumna <> "Indice" Then
                            If Not m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(sColumna)) Is Nothing Then
                                If m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(sColumna)).Width = 0 Then
                                    .Columns(sColumna & sGrupo).Width = 500
                                Else
                                    .Columns(sColumna & sGrupo).Width = m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(sColumna)).Width
                                End If
                            End If
                        End If
                    End If
            End Select
        Next j
    Next i
    m_iLevelCantMax = .Groups(2).Columns("CantMax" & .Groups(2).TagVariant).Level
    m_iLevelCodProve = .Groups(2).Columns("CodProve" & .Groups(2).TagVariant).Level
    m_iLevelNumOfe = .Groups(2).Columns("NumOfe" & .Groups(2).TagVariant).Level
    m_iLevelHom = .Groups(2).Columns("Hom" & .Groups(2).TagVariant).Level
    m_iLevelIndice = .Groups(2).Columns("Indice" & .Groups(2).TagVariant).Level
    If m_oVistaSeleccionadaGr.OcultarFila2 = True Then
        m_bCargandoVista = True
        sdbgAdj_GrpHeadClick 0
        m_bCargandoVista = False
    End If
    'Pone el ancho de las filas
    If m_oVistaSeleccionadaGr.AnchoFila = 0 Or IsNull(m_oVistaSeleccionadaGr.AnchoFila) Then
        .RowHeight = ANCHO_FILA_GR
    Else
        .RowHeight = m_oVistaSeleccionadaGr.AnchoFila
    End If
    'Visualiza la columna de Objetivos totales s�lo cuando est� visible la de Objetivos
    If sdbgTotales.Rows > 0 And .Groups.Count > 2 Then
        If .Groups(2).Visible = True Then sdbgTotales.SplitterPos = 2
    End If
        
    If .Columns("OBJ").Width > 1000 Then
        sdbgTotales.Columns("OBJTOT").Width = .Columns("OBJ").Width
    Else
        sdbgTotales.Columns("OBJTOT").Width = 1000
    End If
    sdbgTotales.Columns(0).Width = .Groups(0).Width - sdbgTotales.Columns("OBJTOT").Width
    If .Columns("OBJ").Visible = False Then
        sdbgTotales.Columns("OBJTOT").caption = ""
        sdbgTotales.MoveFirst
    Else
        sdbgTotales.Columns("OBJTOT").caption = sCaptionTotales
        sdbgTotales.MoveFirst
    End If
    .MoveFirst
End With
End Sub


''' <summary>
''' Guardar en la vista la configuracion del tab All
''' </summary>
''' <remarks>Llamada desde: _GrpHeadClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarConfiguracionGridAll()
Dim iNumColProv As Long
Dim oatrib As CPlantConfVistaAllAtrib
Dim i As Integer
Dim j As Integer
Dim iDifProv As Integer
With m_oVistaSeleccionadaAll
    'Comprueba si ha hecho un scroll.Si lo ha hecho lo deshace
    If sdbgAll.GrpPosition(0) > 0 Then
        i = sdbgAll.Groups(0).Position
        sdbgAll.Scroll -i, 0
        sdbgAll.Update
        sdbgTotalesAll.Scroll -i, 0
        sdbgTotalesAll.Update
    End If
    For j = 0 To sdbgAll.Groups.Count - 1
        sdbgAll.Groups(j).Position = j
    Next j
    For j = 0 To sdbgTotalesAll.Columns.Count - 1
        sdbgTotalesAll.Columns(j).Position = j
    Next j
    'N� de columnas de los grupos de proveedores.Es necesario para obtener la posici�n de las celdas
    'por si se ha hecho scroll.Hay que hacerlo de esta forma,sino no funciona:
    If sdbgAll.Groups.Count > 2 Then
        iNumColProv = sdbgAll.Groups(2).Columns.Count
    Else
        iNumColProv = 0
    End If
    'Guarda la configuraci�n de la grid antes de ocultar la 2� fila
    .AnyoLevel = sdbgAll.Columns("ANYO").Level
    .ProveedorLevel = sdbgAll.Columns("PROV").Level
    .PresUniLevel = sdbgAll.Columns("PRECAPE").Level
    .OBJLevel = sdbgAll.Columns("OBJ").Level
    .AhorroLevel = sdbgAll.Columns("AHORROIMP").Level
    .AhorroPorcenLevel = sdbgAll.Columns("AHORROPORCEN").Level
    .ImporteLevel = sdbgAll.Columns("IMP").Level
    .CantidadLevel = sdbgAll.Columns("CANT").Level
    .AdjLevel = sdbgAll.Columns("PORCENADJ").Level
    .SolicVinculadaLevel = sdbgAll.Columns("VINCULADO_ITEM").Level
    .FecIniSumLevel = sdbgAll.Columns("INI").Level
    .FecFinSumLevel = sdbgAll.Columns("FIN").Level
    .EstrMatLevel = sdbgAll.Columns("GMN").Level
    .AnyoVisible = sdbgAll.Columns("ANYO").Visible
    .ProveedorVisible = sdbgAll.Columns("PROV").Visible
    .PresUniVisible = sdbgAll.Columns("PRECAPE").Visible
    .OBJVisible = sdbgAll.Columns("OBJ").Visible
    .AhorroVisible = sdbgAll.Columns("AHORROIMP").Visible
    .AhorroPorcenVisible = sdbgAll.Columns("AHORROPORCEN").Visible
    .ImporteVisible = sdbgAll.Columns("IMP").Visible
    .CantidadVisible = sdbgAll.Columns("CANT").Visible
    .AdjVisible = sdbgAll.Columns("PORCENADJ").Visible
    .SolicVinculadaVisible = sdbgAll.Columns("VINCULADO_ITEM").Visible
    .FecIniSumVisible = sdbgAll.Columns("INI").Visible
    .FecFinSumVisible = sdbgAll.Columns("FIN").Visible
    .EstrMatVisible = sdbgAll.Columns("GMN").Visible
    .AnyoPos = sdbgAll.Columns("ANYO").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .ProveedorPos = sdbgAll.Columns("PROV").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .PresUniPos = sdbgAll.Columns("PRECAPE").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .OBJPos = sdbgAll.Columns("OBJ").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .AhorroPos = sdbgAll.Columns("AHORROIMP").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .AhorroPorcenPos = sdbgAll.Columns("AHORROPORCEN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .ImportePos = sdbgAll.Columns("IMP").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .CantidadPos = sdbgAll.Columns("CANT").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .AdjPos = sdbgAll.Columns("PORCENADJ").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .SolicVinculadaPos = sdbgAll.Columns("VINCULADO_ITEM").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .FecIniSumPos = sdbgAll.Columns("INI").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .FecFinSumPos = sdbgAll.Columns("FIN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .EstrMatPos = sdbgAll.Columns("GMN").Position - (sdbgAll.Groups(0).Position * iNumColProv)
    .DescrWidth = sdbgAll.Columns("DESCR").Width
    .AnyoWidth = sdbgAll.Columns("ANYO").Width
    .ProveedorWidth = sdbgAll.Columns("PROV").Width
    .PresUniWidth = sdbgAll.Columns("PRECAPE").Width
    .OBJWidth = sdbgAll.Columns("OBJ").Width
    .AhorroWidth = sdbgAll.Columns("AHORROIMP").Width
    .AhorroPorcenWidth = sdbgAll.Columns("AHORROPORCEN").Width
    .ImporteWidth = sdbgAll.Columns("IMP").Width
    .CantidadWidth = sdbgAll.Columns("CANT").Width
    .AdjWidth = sdbgAll.Columns("PORCENADJ").Width
    .SolicVinculadaWidth = sdbgAll.Columns("VINCULADO_ITEM").Width
    .FecIniSumWidth = sdbgAll.Columns("INI").Width
    .FecFinSumWidth = sdbgAll.Columns("FIN").Width
    .EstrMatWidth = sdbgAll.Columns("GMN").Width
    .Grupo0Width = sdbgAll.Groups(0).Width
    If sdbgAll.Groups.Count > 2 Then
        iDifProv = sdbgAll.Groups(2).Columns.Count - 9
        .GruposWidth = sdbgAll.Groups(2).Width
        .ImpAdjVisible = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(2).TagVariant).Visible
        .ImpAdjWidth = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(2).TagVariant).Width
        .ImpAdjLevel = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(2).TagVariant).Level
        .ImpAdjPos = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(2).TagVariant).Position - m_iNumColsAll
        .PrecioProvVisible = sdbgAll.Columns("Precio" & sdbgAll.Groups(2).TagVariant).Visible
        .PrecioProvWidth = sdbgAll.Columns("Precio" & sdbgAll.Groups(2).TagVariant).Width
        .PrecioProvLevel = sdbgAll.Columns("Precio" & sdbgAll.Groups(2).TagVariant).Level
        .PrecioProvPos = sdbgAll.Columns("Precio" & sdbgAll.Groups(2).TagVariant).Position - m_iNumColsAll
        .AdjProvVisible = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(2).TagVariant).Visible
        .AdjProvWidth = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(2).TagVariant).Width
        .AdjProvLevel = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(2).TagVariant).Level
        .AdjProvPos = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(2).TagVariant).Position - m_iNumColsAll
        .CantProvVisible = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(2).TagVariant).Visible
        .CantProvWidth = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(2).TagVariant).Width
        .CantProvLevel = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(2).TagVariant).Level
        .CantProvPos = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(2).TagVariant).Position - m_iNumColsAll
        For Each oatrib In .PlantConfVistasAllAtrib
            oatrib.Fila = sdbgAll.Columns(oatrib.Atributo & sdbgAll.Groups(2).TagVariant).Level
            oatrib.Posicion = sdbgAll.Columns(oatrib.Atributo & sdbgAll.Groups(2).TagVariant).Position - m_iNumColsAll
            oatrib.Visible = sdbgAll.Columns(oatrib.Atributo & sdbgAll.Groups(2).TagVariant).Visible
            oatrib.Width = sdbgAll.Columns(oatrib.Atributo & sdbgAll.Groups(2).TagVariant).Width
        Next
    End If
    m_iLevelCantMaxAll = sdbgAll.Columns("CantMax" & sdbgAll.Groups(2).TagVariant).Level
    m_iLevelCodProveAll = sdbgAll.Columns("CodProve" & sdbgAll.Groups(2).TagVariant).Level
    m_iLevelNumOfeAll = sdbgAll.Columns("NumOfe" & sdbgAll.Groups(2).TagVariant).Level
    m_iLevelHomAll = sdbgAll.Columns("Hom" & sdbgAll.Groups(2).TagVariant).Level
    m_iLevelIndiceAll = sdbgAll.Columns("Indice" & sdbgAll.Groups(2).TagVariant).Level
    If i > 0 Then
        'Si se hab�a hecho un scroll se deja como estaba antes
        sdbgTotalesAll.Scroll i, 0
        sdbgTotalesAll.Update
        sdbgAll.Scroll i, 0
        sdbgAll.Update
    End If
End With
End Sub

''' <summary>
''' Guardar en la vista la configuracion del Grupo
''' </summary>
''' <remarks>Llamada desde: sdbgAdj_GrpHeadClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarConfiguracionGridGrupo()
Dim oatrib As CPlantConfVistaGrupoAtrib
Dim iNumColProv As Long
Dim i As Integer
Dim j As Integer
Dim iDifProv As Integer
With m_oVistaSeleccionadaGr
    'Comprueba si ha hecho un scroll.Si lo ha hecho lo deshace
    If sdbgAdj.GrpPosition(0) > 0 Then
        i = sdbgAdj.Groups(0).Position
        sdbgAdj.Scroll -i, 0
        sdbgAdj.Update
        sdbgTotales.Scroll -i, 0
        sdbgTotales.Update
    End If
    For j = 0 To sdbgAdj.Groups.Count - 1
        sdbgAdj.Groups(j).Position = j
    Next j
    For j = 0 To sdbgTotales.Columns.Count - 1
        sdbgTotales.Columns(j).Position = j
    Next j
    'N� de columnas de los grupos de proveedores.Es necesario para obtener la posici�n de las celdas
    'por si se ha hecho scroll.Hay que hacerlo de esta forma,sino no funciona:
    If sdbgAdj.Groups.Count > 2 Then
        iNumColProv = sdbgAdj.Groups(2).Columns.Count
    Else
        iNumColProv = 0
    End If
    'Guarda la configuraci�n de la grid antes de ocultar la 2� fila
    .AnyoLevel = sdbgAdj.Columns("ANYO").Level
    .ProveedorLevel = sdbgAdj.Columns("PROV").Level
    .PresUniLevel = sdbgAdj.Columns("PRECAPE").Level
    .OBJLevel = sdbgAdj.Columns("OBJ").Level
    .AhorroLevel = sdbgAdj.Columns("AHORROIMP").Level
    .AhorroPorcenLevel = sdbgAdj.Columns("AHORROPORCEN").Level
    .ImporteLevel = sdbgAdj.Columns("IMP").Level
    .CantidadLevel = sdbgAdj.Columns("CANT").Level
    .AdjLevel = sdbgAdj.Columns("PORCENADJ").Level
    .SolicVinculadaLevel = sdbgAdj.Columns("VINCULADO_ITEM").Level
    .FecIniSumLevel = sdbgAdj.Columns("INI").Level
    .FecFinSumLevel = sdbgAdj.Columns("FIN").Level
    .EstrMatLevel = sdbgAdj.Columns("GMN").Level
    .AnyoVisible = sdbgAdj.Columns("ANYO").Visible
    .ProveedorVisible = sdbgAdj.Columns("PROV").Visible
    .PresUniVisible = sdbgAdj.Columns("PRECAPE").Visible
    .OBJVisible = sdbgAdj.Columns("OBJ").Visible
    .AhorroVisible = sdbgAdj.Columns("AHORROIMP").Visible
    .AhorroPorcenVisible = sdbgAdj.Columns("AHORROPORCEN").Visible
    .ImporteVisible = sdbgAdj.Columns("IMP").Visible
    .CantidadVisible = sdbgAdj.Columns("CANT").Visible
    .AdjVisible = sdbgAdj.Columns("PORCENADJ").Visible
    .SolicVinculadaVisible = sdbgAdj.Columns("VINCULADO_ITEM").Visible
    .FecIniSumVisible = sdbgAdj.Columns("INI").Visible
    .FecFinSumVisible = sdbgAdj.Columns("FIN").Visible
    .EstrMatVisible = sdbgAdj.Columns("GMN").Visible
    .AnyoPos = sdbgAdj.Columns("ANYO").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .ProveedorPos = sdbgAdj.Columns("PROV").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .PresUniPos = sdbgAdj.Columns("PRECAPE").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .OBJPos = sdbgAdj.Columns("OBJ").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .AhorroPos = sdbgAdj.Columns("AHORROIMP").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .AhorroPorcenPos = sdbgAdj.Columns("AHORROPORCEN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .ImportePos = sdbgAdj.Columns("IMP").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .CantidadPos = sdbgAdj.Columns("CANT").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .AdjPos = sdbgAdj.Columns("PORCENADJ").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .SolicVinculadaPos = sdbgAdj.Columns("VINCULADO_ITEM").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .FecIniSumPos = sdbgAdj.Columns("INI").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .FecFinSumPos = sdbgAdj.Columns("FIN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .EstrMatPos = sdbgAdj.Columns("GMN").Position - (sdbgAdj.Groups(0).Position * iNumColProv)
    .DescrWidth = sdbgAdj.Columns("DESCR").Width
    .AnyoWidth = sdbgAdj.Columns("ANYO").Width
    .ProveedorWidth = sdbgAdj.Columns("PROV").Width
    .PresUniWidth = sdbgAdj.Columns("PRECAPE").Width
    .OBJWidth = sdbgAdj.Columns("OBJ").Width
    .AhorroWidth = sdbgAdj.Columns("AHORROIMP").Width
    .AhorroPorcenWidth = sdbgAdj.Columns("AHORROPORCEN").Width
    .ImporteWidth = sdbgAdj.Columns("IMP").Width
    .CantidadWidth = sdbgAdj.Columns("CANT").Width
    .AdjWidth = sdbgAdj.Columns("PORCENADJ").Width
    .SolicVinculadaWidth = sdbgAdj.Columns("VINCULADO_ITEM").Width
    .FecIniSumWidth = sdbgAdj.Columns("INI").Width
    .FecFinSumWidth = sdbgAdj.Columns("FIN").Width
    .EstrMatWidth = sdbgAdj.Columns("GMN").Width
    .Grupo0Width = sdbgAdj.Groups(0).Width
    If sdbgAdj.Groups.Count > 2 Then
        iDifProv = sdbgAdj.Groups(2).Columns.Count - 9
        .GruposWidth = sdbgAdj.Groups(2).Width
        .ImpAdjVisible = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(2).TagVariant).Visible
        .ImpAdjWidth = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(2).TagVariant).Width
        .ImpAdjLevel = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(2).TagVariant).Level
        .ImpAdjPos = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(2).TagVariant).Position - m_iNumColsGR
        .PrecioProvVisible = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(2).TagVariant).Visible
        .PrecioProvWidth = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(2).TagVariant).Width
        .PrecioProvLevel = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(2).TagVariant).Level
        .PrecioProvPos = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(2).TagVariant).Position - m_iNumColsGR
        .AdjProvVisible = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(2).TagVariant).Visible
        .AdjProvWidth = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(2).TagVariant).Width
        .AdjProvLevel = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(2).TagVariant).Level
        .AdjProvPos = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(2).TagVariant).Position - m_iNumColsGR
        .CantProvVisible = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(2).TagVariant).Visible
        .CantProvWidth = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(2).TagVariant).Width
        .CantProvLevel = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(2).TagVariant).Level
        .CantProvPos = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(2).TagVariant).Position - m_iNumColsGR
        For Each oatrib In .PlantConfVistasGrAtrib
            oatrib.Fila = sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(2).TagVariant).Level
            oatrib.Posicion = sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(2).TagVariant).Position - m_iNumColsGR
            oatrib.Visible = sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(2).TagVariant).Visible
            oatrib.Width = sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(2).TagVariant).Width
        Next
    End If
    m_iLevelCantMax = sdbgAdj.Columns("CantMax" & sdbgAdj.Groups(2).TagVariant).Level
    m_iLevelCodProve = sdbgAdj.Columns("CodProve" & sdbgAdj.Groups(2).TagVariant).Level
    m_iLevelNumOfe = sdbgAdj.Columns("NumOfe" & sdbgAdj.Groups(2).TagVariant).Level
    m_iLevelHom = sdbgAdj.Columns("Hom" & sdbgAdj.Groups(2).TagVariant).Level
    m_iLevelIndice = sdbgAdj.Columns("Indice" & sdbgAdj.Groups(2).TagVariant).Level
    If i > 0 Then
        'Si se hab�a hecho un scroll se deja como estaba antes
        sdbgTotales.Scroll i, 0
        sdbgTotales.Update
        sdbgAdj.Scroll i, 0
        sdbgAdj.Update
    End If
End With
End Sub

''' <summary>
''' Ocultar Filas en grid Totales
''' </summary>
''' <remarks>Llamada desde: sdbgAdj_GrpHeadClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub OcultarFilasTotales()
Dim i As Integer
Dim lngPosicionScroll As Long
Dim iNumColProv As Integer
Dim oatrib As CPlantConfVistaGrupoAtrib
Dim iOrden As Integer
Dim iIndex As Integer
Dim j As Integer
Dim sColumna As String
Dim inumColumns As Integer
Dim lDifTamanyo As Long
Dim iVisibles As Integer
Dim oAtribOrden As CAtributo
Dim lSumFalta As Long
Dim lSumWidth As Long
With m_oVistaSeleccionadaGr
    iNumColProv = sdbgAdj.Groups(2).Columns.Count
    lngPosicionScroll = (sdbgAdj.Groups(0).Position * iNumColProv)
    'Dimensiona la grid:
    sdbgAdj.Columns("DESCR").Level = 0
    sdbgAdj.Columns("ANYO").Level = .AnyoLevel
    sdbgAdj.Columns("PROV").Level = .ProveedorLevel
    sdbgAdj.Columns("PRECAPE").Level = .PresUniLevel
    sdbgAdj.Columns("OBJ").Level = .OBJLevel
    sdbgAdj.Columns("PORCENADJ").Level = .AdjLevel
    sdbgAdj.Columns("VINCULADO_ITEM").Level = .SolicVinculadaLevel
    sdbgAdj.Columns("AHORROPORCEN").Level = .AhorroPorcenLevel
    sdbgAdj.Columns("AHORROIMP").Level = .AhorroLevel
    sdbgAdj.Columns("CANT").Level = .CantidadLevel
    sdbgAdj.Columns("IMP").Level = .ImporteLevel
    sdbgAdj.Columns("INI").Level = .FecIniSumLevel
    sdbgAdj.Columns("FIN").Level = .FecFinSumLevel
    sdbgAdj.Columns("GMN").Level = .EstrMatLevel
    sdbgAdj.Columns("DESCR").Position = 0 + lngPosicionScroll
    For i = 0 To 12
        Select Case m_arrOrden(i)
            Case "ANYO"
                sdbgAdj.Columns("ANYO").Position = .AnyoPos + lngPosicionScroll
            Case "PROV"
                sdbgAdj.Columns("PROV").Position = .ProveedorPos + lngPosicionScroll
            Case "PRECAPE"
                sdbgAdj.Columns("PRECAPE").Position = .PresUniPos + lngPosicionScroll
            Case "OBJ"
                sdbgAdj.Columns("OBJ").Position = .OBJPos + lngPosicionScroll
            Case "AHORROIMP"
                sdbgAdj.Columns("AHORROIMP").Position = .AhorroPos + lngPosicionScroll
            Case "AHORROPORCEN"
                sdbgAdj.Columns("AHORROPORCEN").Position = .AhorroPorcenPos + lngPosicionScroll
            Case "IMP"
                sdbgAdj.Columns("IMP").Position = .ImportePos + lngPosicionScroll
            Case "CANT"
                sdbgAdj.Columns("CANT").Position = .CantidadPos + lngPosicionScroll
            Case "PORCENADJ"
                sdbgAdj.Columns("PORCENADJ").Position = .AdjPos + lngPosicionScroll
            Case "VINCULADO_ITEM"
                sdbgAdj.Columns("VINCULADO_ITEM").Position = .SolicVinculadaPos + lngPosicionScroll
            Case "INI"
                sdbgAdj.Columns("INI").Position = .FecIniSumPos + lngPosicionScroll
            Case "FIN"
                sdbgAdj.Columns("FIN").Position = .FecFinSumPos + lngPosicionScroll
            Case "GMN"
                sdbgAdj.Columns("GMN").Position = .EstrMatPos + lngPosicionScroll
        End Select
    Next i
    'Volvemos a poner el level,porque sino no funciona bien
    sdbgAdj.Columns("DESCR").Level = 0
    sdbgAdj.Columns("ANYO").Level = .AnyoLevel
    sdbgAdj.Columns("PROV").Level = .ProveedorLevel
    sdbgAdj.Columns("PRECAPE").Level = .PresUniLevel
    sdbgAdj.Columns("OBJ").Level = .OBJLevel
    sdbgAdj.Columns("PORCENADJ").Level = .AdjLevel
    sdbgAdj.Columns("VINCULADO_ITEM").Level = .SolicVinculadaLevel
    sdbgAdj.Columns("AHORROPORCEN").Level = .AhorroPorcenLevel
    sdbgAdj.Columns("AHORROIMP").Level = .AhorroLevel
    sdbgAdj.Columns("CANT").Level = .CantidadLevel
    sdbgAdj.Columns("IMP").Level = .ImporteLevel
    sdbgAdj.Columns("INI").Level = .FecIniSumLevel
    sdbgAdj.Columns("FIN").Level = .FecFinSumLevel
    sdbgAdj.Columns("GMN").Level = .EstrMatLevel
    'columnas visibles
    sdbgAdj.Columns("ANYO").Visible = .AnyoVisible
    If g_oPlantillaSeleccionada.AdminPub = True Then
        sdbgAdj.Columns("PROV").Visible = False
    Else
        sdbgAdj.Columns("PROV").Visible = .ProveedorVisible
    End If
    sdbgAdj.Columns("PRECAPE").Visible = .PresUniVisible
    sdbgAdj.Columns("OBJ").Visible = .OBJVisible
    sdbgAdj.Columns("AHORROIMP").Visible = .AhorroVisible
    sdbgAdj.Columns("AHORROPORCEN").Visible = .AhorroPorcenVisible
    sdbgAdj.Columns("IMP").Visible = .ImporteVisible
    sdbgAdj.Columns("CANT").Visible = .CantidadVisible
    sdbgAdj.Columns("PORCENADJ").Visible = .AdjVisible
    sdbgAdj.Columns("VINCULADO_ITEM").Visible = .SolicVinculadaVisible
    sdbgAdj.Columns("INI").Visible = .FecIniSumVisible
    sdbgAdj.Columns("FIN").Visible = .FecFinSumVisible
    sdbgAdj.Columns("GMN").Visible = .EstrMatVisible
    'Calcula la diferencia de tama�o de las columnas (si se ha cambiado el tama�o del grupo u ocultado columnas que estaban visibles)
    lDifTamanyo = 0
    iVisibles = 0
    lSumFalta = 0
    lSumWidth = 0
    If .AnyoLevel = 1 And .AnyoVisible = True Then iVisibles = iVisibles + 1
    If .ProveedorLevel = 1 And .ProveedorVisible = True Then iVisibles = iVisibles + 1
    If .PresUniLevel = 1 And .PresUniVisible = True Then iVisibles = iVisibles + 1
    If .OBJLevel = 1 And .OBJVisible = True Then iVisibles = iVisibles + 1
    If .AhorroLevel = 1 And .AhorroVisible = True Then iVisibles = iVisibles + 1
    If .AhorroPorcenLevel = 1 And .AhorroPorcenVisible = True Then iVisibles = iVisibles + 1
    If .ImporteLevel = 1 And .ImporteVisible = True Then iVisibles = iVisibles + 1
    If .CantidadLevel = 1 And .CantidadVisible = True Then iVisibles = iVisibles + 1
    If .AdjLevel = 1 And .AdjVisible = True Then iVisibles = iVisibles + 1
    If .SolicVinculadaLevel = 1 And .SolicVinculadaVisible = True Then iVisibles = iVisibles + 1
    If .FecIniSumLevel = 1 And .FecIniSumVisible = True Then iVisibles = iVisibles + 1
    If .FecFinSumLevel = 1 And .FecFinSumVisible = True Then iVisibles = iVisibles + 1
    If .EstrMatLevel = 1 And .EstrMatVisible = True Then iVisibles = iVisibles + 1
    If sdbgAdj.Groups(0).Width <> .Grupo0Width And iVisibles > 0 Then lDifTamanyo = (sdbgAdj.Groups(0).Width - .Grupo0Width) / iVisibles
    If .AnyoLevel = 1 And .AnyoVisible = True Then lSumWidth = lSumWidth + .AnyoWidth
    If .ProveedorLevel = 1 And .ProveedorVisible = True Then lSumWidth = lSumWidth + .ProveedorWidth
    If .PresUniLevel = 1 And .PresUniVisible = True Then lSumWidth = lSumWidth + .PresUniWidth
    If .OBJLevel = 1 And .OBJVisible = True Then lSumWidth = lSumWidth + .OBJWidth
    If .AhorroLevel = 1 And .AhorroVisible = True Then lSumWidth = lSumWidth + .AhorroWidth
    If .AhorroPorcenLevel = 1 And .AhorroPorcenVisible = True Then lSumWidth = lSumWidth + .AhorroPorcenWidth
    If .ImporteLevel = 1 And .ImporteVisible = True Then lSumWidth = lSumWidth + .ImporteWidth
    If .CantidadLevel = 1 And .CantidadVisible = True Then lSumWidth = lSumWidth + .CantidadWidth
    If .AdjLevel = 1 And .AdjVisible = True Then lSumWidth = lSumWidth + .AdjWidth
    If .SolicVinculadaLevel = 1 And .SolicVinculadaVisible = True Then lSumWidth = lSumWidth + .SolicVinculadaWidth
    If .FecIniSumLevel = 1 And .FecIniSumVisible = True Then lSumWidth = lSumWidth + .FecIniSumWidth
    If .FecFinSumLevel = 1 And .FecFinSumVisible = True Then lSumWidth = lSumWidth + .FecFinSumWidth
    If .EstrMatLevel = 1 And .EstrMatVisible = True Then lSumWidth = lSumWidth + .EstrMatWidth
    If lSumWidth < .Grupo0Width And iVisibles > 0 Then lSumFalta = (.Grupo0Width - lSumWidth) / iVisibles
    'Width de las columnas.Se tiene que hacer en el mismo orden en el que est�n los position de las columnas
    For i = 0 To sdbgAdj.Groups(0).Columns.Count - 1
        iIndex = sdbgAdj.Groups(0).ColPosition(i)
        Select Case sdbgAdj.Groups(0).Columns(iIndex).Name
            Case "DESCR"
                sdbgAdj.Columns("DESCR").Width = .DescrWidth
            Case "ANYO"
                If .AnyoLevel = 1 And .AnyoVisible = True Then
                    sdbgAdj.Columns("ANYO").Width = .AnyoWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("ANYO").Width = .AnyoWidth
                End If
            Case "PROV"
                If .ProveedorLevel = 1 And .ProveedorVisible = True Then
                    sdbgAdj.Columns("PROV").Width = .ProveedorWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("PROV").Width = .ProveedorWidth
                End If
            Case "PRECAPE"
                If .PresUniLevel = 1 And .PresUniVisible = True Then
                    sdbgAdj.Columns("PRECAPE").Width = .PresUniWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("PRECAPE").Width = .PresUniWidth
                End If
            Case "OBJ"
                If .OBJLevel = 1 And .OBJVisible = True Then
                    sdbgAdj.Columns("OBJ").Width = .OBJWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("OBJ").Width = .OBJWidth
                End If
            Case "AHORROIMP"
                If .AhorroLevel = 1 And .AhorroVisible = True Then
                    sdbgAdj.Columns("AHORROIMP").Width = .AhorroWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("AHORROIMP").Width = .AhorroWidth
                End If
            Case "AHORROPORCEN"
                If .AhorroPorcenLevel = 1 And .AhorroPorcenVisible = True Then
                    sdbgAdj.Columns("AHORROPORCEN").Width = .AhorroPorcenWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("AHORROPORCEN").Width = .AhorroPorcenWidth
                End If
            Case "IMP"
                If .ImporteLevel = 1 And .ImporteVisible = True Then
                    sdbgAdj.Columns("IMP").Width = .ImporteWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("IMP").Width = .ImporteWidth
                End If
            Case "CANT"
                If .CantidadLevel = 1 And .CantidadVisible = True Then
                    sdbgAdj.Columns("CANT").Width = .CantidadWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("CANT").Width = .CantidadWidth
                End If
            Case "PORCENADJ"
                If .AdjLevel = 1 And .AdjVisible = True Then
                    sdbgAdj.Columns("PORCENADJ").Width = .AdjWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("PORCENADJ").Width = .AdjWidth
                End If
            Case "VINCULADO_ITEM"
                If .SolicVinculadaLevel = 1 And .SolicVinculadaVisible = True Then
                    sdbgAdj.Columns("VINCULADO_ITEM").Width = .SolicVinculadaWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("VINCULADO_ITEM").Width = .SolicVinculadaWidth
                End If
            Case "INI"
                If .FecIniSumLevel = 1 And .FecIniSumVisible = True Then
                    sdbgAdj.Columns("INI").Width = .FecIniSumWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("INI").Width = .FecIniSumWidth
                End If
            Case "FIN"
                If .FecFinSumLevel = 1 And .FecFinSumVisible = True Then
                    sdbgAdj.Columns("FIN").Width = .FecFinSumWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("FIN").Width = .FecFinSumWidth
                End If
            Case "GMN"
                If .EstrMatLevel = 1 And .EstrMatVisible = True Then
                    sdbgAdj.Columns("GMN").Width = .EstrMatWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAdj.Columns("GMN").Width = .EstrMatWidth
                End If
        End Select
    Next i
    'Calcula la dif. de tama�o de las columnas de los grupos, en el caso de que haya habido cambios en el tama�o del grupo
    'u ocultado columnas de la 2� fila que estaban visibles
    lDifTamanyo = 0
    iVisibles = 0
    lSumFalta = 0
    lSumWidth = 0
    If .ImpAdjLevel = 1 And .ImpAdjVisible = True Then iVisibles = iVisibles + 1
    If .PrecioProvLevel = 1 And .PrecioProvVisible = True Then iVisibles = iVisibles + 1
    If .AdjProvLevel = 1 And .AdjProvVisible = True Then iVisibles = iVisibles + 1
    If .CantProvLevel = 1 And .CantProvVisible = True Then iVisibles = iVisibles + 1
    If Not .PlantConfVistasGrAtrib Is Nothing Then
        For Each oatrib In .PlantConfVistasGrAtrib
            If oatrib.Fila = 1 And oatrib.Visible = True Then iVisibles = iVisibles + 1
        Next
    End If
    If sdbgAdj.Groups(2).Width <> .GruposWidth And iVisibles > 0 Then lDifTamanyo = (sdbgAdj.Groups(2).Width - .GruposWidth) / iVisibles
    If .ImpAdjLevel = 1 And .ImpAdjVisible = True Then lSumWidth = lSumWidth + .ImpAdjWidth
    If .PrecioProvLevel = 1 And .PrecioProvVisible = True Then lSumWidth = lSumWidth + .PrecioProvWidth
    If .AdjProvLevel = 1 And .AdjProvVisible = True Then lSumWidth = lSumWidth + .AdjProvWidth
    If .CantProvLevel = 1 And .CantProvVisible = True Then lSumWidth = lSumWidth + .CantProvWidth
    If Not .PlantConfVistasGrAtrib Is Nothing Then
        For Each oatrib In .PlantConfVistasGrAtrib
            If oatrib.Fila = 1 And oatrib.Visible = True Then lSumWidth = lSumWidth + oatrib.Width
        Next
    End If
    If lSumWidth < .GruposWidth And iVisibles > 0 Then lSumFalta = (.GruposWidth - lSumWidth) / iVisibles
    For i = 2 To 4
        'Level de los campos:
        sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(i).TagVariant).Level = .ImpAdjLevel
        sdbgAdj.Columns("Precio" & sdbgAdj.Groups(i).TagVariant).Level = .PrecioProvLevel
        sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(i).TagVariant).Level = .AdjProvLevel
        sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(i).TagVariant).Level = .CantProvLevel
        'Atributos
        For Each oatrib In .PlantConfVistasGrAtrib
            sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(i).TagVariant).Level = oatrib.Fila
        Next
        'Visibilidad de los campos:
        sdbgAdj.Columns("ImpAdj" & sdbgAdj.Groups(i).TagVariant).Visible = .ImpAdjVisible
        sdbgAdj.Columns("Precio" & sdbgAdj.Groups(i).TagVariant).Visible = .PrecioProvVisible
        sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(i).TagVariant).Visible = .CantProvVisible
        sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(i).TagVariant).Visible = .AdjProvVisible
        sdbgAdj.Columns("CantMax" & sdbgAdj.Groups(i).TagVariant).Level = m_iLevelCantMax
        sdbgAdj.Columns("CodProve" & sdbgAdj.Groups(i).TagVariant).Level = m_iLevelCodProve
        sdbgAdj.Columns("NumOfe" & sdbgAdj.Groups(i).TagVariant).Level = m_iLevelNumOfe
        sdbgAdj.Columns("Hom" & sdbgAdj.Groups(i).TagVariant).Level = m_iLevelHom
        sdbgAdj.Columns("Indice" & sdbgAdj.Groups(i).TagVariant).Level = m_iLevelIndice
        'Atributos
        For Each oatrib In .PlantConfVistasGrAtrib
            sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(i).TagVariant).Visible = oatrib.Visible
        Next
        'Posici�n de los campos:
        iOrden = m_iNumColsGR + (sdbgAdj.Groups(i).Columns.Count * (i - 2))
        inumColumns = 3
        If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
            For Each oAtribOrden In g_oPlantillaSeleccionada.Atributosvistas
                If oAtribOrden.ambito = AmbItem And (oAtribOrden.codgrupo = m_oGrupoSeleccionado.Codigo Or IsNull(oAtribOrden.codgrupo)) Then inumColumns = inumColumns + 1
            Next
        End If
        For j = 0 To inumColumns
            Select Case m_arrOrdenColsGr(j)
                Case "IMPADJ"
                    If sdbgAdj.Groups(0).Position = 0 Then sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(i).TagVariant).Position = .ImpAdjPos + iOrden
                Case "PRECIO"
                    If sdbgAdj.Groups(0).Position = 0 Then sdbgAdj.Columns("Precio" & sdbgAdj.Groups(i).TagVariant).Position = .PrecioProvPos + iOrden
                Case "ADJPROV"
                    If sdbgAdj.Groups(0).Position = 0 Then sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(i).TagVariant).Position = .AdjProvPos + iOrden
                Case "CANTPROV"
                    If sdbgAdj.Groups(0).Position = 0 Then sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(i).TagVariant).Position = .CantProvPos + iOrden
                Case Else
                    'Es un atributo
                    sdbgAdj.Columns(m_arrOrdenColsGr(j) & sdbgAdj.Groups(i).TagVariant).Position = .PlantConfVistasGrAtrib.Item(CStr(m_arrOrdenColsGr(j))).Posicion + iOrden
            End Select
        Next j
        ''''Level de los campos (lo volvemos a poner despu�s de la posici�n porque sino no funciona bien)
        For j = inumColumns To 0 Step -1
            Select Case m_arrOrdenColsGr(j)
                Case "IMPADJ"
                    sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(i).TagVariant).Level = .ImpAdjLevel
                Case "PRECIO"
                    sdbgAdj.Columns("Precio" & sdbgAdj.Groups(i).TagVariant).Level = .PrecioProvLevel
                Case "ADJPROV"
                    sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(i).TagVariant).Level = .AdjProvLevel
                Case "CANTPROV"
                    sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(i).TagVariant).Level = .CantProvLevel
                Case Else
                    'Es un atributo
                    sdbgAdj.Columns(m_arrOrdenColsGr(j) & sdbgAdj.Groups(i).TagVariant).Level = .PlantConfVistasGrAtrib.Item(CStr(m_arrOrdenColsGr(j))).Fila
            End Select
        Next j
        'Visibilidad de los campos:
        sdbgAdj.Columns("ImpAdj" & sdbgAdj.Groups(i).TagVariant).Visible = .ImpAdjVisible
        sdbgAdj.Columns("Precio" & sdbgAdj.Groups(i).TagVariant).Visible = .PrecioProvVisible
        sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(i).TagVariant).Visible = .CantProvVisible
        sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(i).TagVariant).Visible = .AdjProvVisible
        'Atributos
        If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
            For Each oatrib In .PlantConfVistasGrAtrib
                sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(i).TagVariant).Visible = oatrib.Visible
            Next
        End If
        ''''Tama�o de los campos:
        'El importe adjudicado,precio adjudicado,cant.adjudicada y %adjudicado y los atributos.En el mismo
        'orden que los position
        For j = 0 To sdbgAdj.Groups(i).Columns.Count - 1
            sColumna = ""
            iIndex = sdbgAdj.Groups(i).ColPosition(j)
            sColumna = Mid(sdbgAdj.Groups(i).Columns(iIndex).Name, 1, InStr(sdbgAdj.Groups(i).Columns(iIndex).Name, sdbgAdj.Groups(i).TagVariant) - 1)
            Select Case sColumna
                Case "IMPADJ"
                    If .ImpAdjLevel = 1 And .ImpAdjVisible = True Then
                        sdbgAdj.Columns("ImpAdj" & sdbgAdj.Groups(i).TagVariant).Width = .ImpAdjWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAdj.Columns("ImpAdj" & sdbgAdj.Groups(i).TagVariant).Width = .ImpAdjWidth
                    End If
                Case "Precio"
                    If .PrecioProvLevel = 1 And .PrecioProvVisible = True Then
                        sdbgAdj.Columns("Precio" & sdbgAdj.Groups(i).TagVariant).Width = .PrecioProvWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAdj.Columns("Precio" & sdbgAdj.Groups(i).TagVariant).Width = .PrecioProvWidth
                    End If
                Case "AdjPorcen"
                    If .AdjProvLevel = 1 And .AdjProvVisible = True Then
                        sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(i).TagVariant).Width = .AdjProvWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(i).TagVariant).Width = .AdjProvWidth
                    End If
                Case "AdjCantidad"
                    If .CantProvLevel = 1 And .CantProvVisible = True Then
                        sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(i).TagVariant).Width = .CantProvWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(i).TagVariant).Width = .CantProvWidth
                    End If
                Case Else  'Comprueba si la columna es un atributo
                    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
                        If sColumna <> "CantMax" And sColumna <> "CodProve" And sColumna <> "NumOfe" And sColumna <> "Hom" And sColumna <> "Indice" Then
                            If Not .PlantConfVistasGrAtrib.Item(CStr(sColumna)) Is Nothing Then
                                If .PlantConfVistasGrAtrib.Item(CStr(sColumna)).Fila = 1 And .PlantConfVistasGrAtrib.Item(CStr(sColumna)).Visible = True Then
                                    sdbgAdj.Columns(sColumna & sdbgAdj.Groups(i).TagVariant).Width = .PlantConfVistasGrAtrib.Item(CStr(sColumna)).Width + lDifTamanyo + lSumFalta
                                Else
                                    sdbgAdj.Columns(sColumna & sdbgAdj.Groups(i).TagVariant).Width = .PlantConfVistasGrAtrib.Item(CStr(sColumna)).Width
                                End If
                            End If
                        End If
                    End If
            End Select
        Next j
    Next i
    'Visualiza la columna de Objetivos totales s�lo cuando est� visible la de Objetivos
    If sdbgTotales.Rows > 0 And sdbgAdj.Groups.Count > 2 Then If sdbgAdj.Groups(2).Visible = True Then sdbgTotales.SplitterPos = 2
    If sdbgAdj.Columns("OBJ").Width > 1000 Then
        sdbgTotales.Columns("OBJTOT").Width = sdbgAdj.Columns("OBJ").Width
    Else
        sdbgTotales.Columns("OBJTOT").Width = 1000
    End If
    sdbgTotales.Columns(0).Width = sdbgAdj.Groups(0).Width - sdbgTotales.Columns("OBJTOT").Width
    If sdbgAdj.Columns("OBJ").Visible = False Then
        sdbgTotales.Columns("OBJTOT").caption = ""
        sdbgTotales.MoveFirst
    Else
        sdbgTotales.Columns("OBJTOT").caption = sCaptionTotales
        sdbgTotales.MoveFirst
    End If
    If sdbgAdj.Visible Then sdbgAdj.SetFocus
End With
End Sub

''' <summary>
''' Ocultar Filas en grid Totales
''' </summary>
''' <remarks>Llamada desde: sdbgAll_GrpHeadClick ; Tiempo m�ximo: 0,2</remarks>
Private Sub OcultarFilasTotalesAll()
Dim i As Integer
Dim lngPosicionScroll As Long
Dim iNumColProv As Integer
Dim oatrib As CPlantConfVistaAllAtrib
Dim iOrden As Integer
Dim iIndex As Integer
Dim j As Integer
Dim sColumna As String
Dim inumColumns As Integer
Dim lDifTamanyo As Long
Dim iVisibles As Integer
Dim lSumFalta As Long
Dim lSumWidth As Long
With m_oVistaSeleccionadaAll
    iNumColProv = sdbgAll.Groups(2).Columns.Count
    lngPosicionScroll = (sdbgAll.Groups(0).Position * iNumColProv)
    'Dimensiona la grid:
    sdbgAll.Columns("DESCR").Level = 0
    sdbgAll.Columns("ANYO").Level = .AnyoLevel
    sdbgAll.Columns("PROV").Level = .ProveedorLevel
    sdbgAll.Columns("PRECAPE").Level = .PresUniLevel
    sdbgAll.Columns("OBJ").Level = .OBJLevel
    sdbgAll.Columns("PORCENADJ").Level = .AdjLevel
    sdbgAll.Columns("VINCULADO_ITEM").Level = .SolicVinculadaLevel
    sdbgAll.Columns("AHORROPORCEN").Level = .AhorroPorcenLevel
    sdbgAll.Columns("AHORROIMP").Level = .AhorroLevel
    sdbgAll.Columns("CANT").Level = .CantidadLevel
    sdbgAll.Columns("IMP").Level = .ImporteLevel
    sdbgAll.Columns("INI").Level = .FecIniSumLevel
    sdbgAll.Columns("FIN").Level = .FecFinSumLevel
    sdbgAll.Columns("GMN").Level = .EstrMatLevel
    sdbgAll.Columns("DESCR").Position = 0 + lngPosicionScroll
    For i = 0 To 12
        Select Case m_arrOrden(i)
            Case "ANYO"
                sdbgAll.Columns("ANYO").Position = .AnyoPos + lngPosicionScroll
            Case "PROV"
                sdbgAll.Columns("PROV").Position = .ProveedorPos + lngPosicionScroll
            Case "PRECAPE"
                sdbgAll.Columns("PRECAPE").Position = .PresUniPos + lngPosicionScroll
            Case "OBJ"
                sdbgAll.Columns("OBJ").Position = .OBJPos + lngPosicionScroll
            Case "AHORROIMP"
                sdbgAll.Columns("AHORROIMP").Position = .AhorroPos + lngPosicionScroll
            Case "AHORROPORCEN"
                sdbgAll.Columns("AHORROPORCEN").Position = .AhorroPorcenPos + lngPosicionScroll
            Case "IMP"
                sdbgAll.Columns("IMP").Position = .ImportePos + lngPosicionScroll
            Case "CANT"
                sdbgAll.Columns("CANT").Position = .CantidadPos + lngPosicionScroll
            Case "PORCENADJ"
                sdbgAll.Columns("PORCENADJ").Position = .AdjPos + lngPosicionScroll
            Case "VINCULADO_ITEM"
                sdbgAll.Columns("VINCULADO_ITEM").Position = .SolicVinculadaPos + lngPosicionScroll
            Case "INI"
                sdbgAll.Groups(0).Columns("INI").Position = .FecIniSumPos + lngPosicionScroll
            Case "FIN"
                sdbgAll.Groups(0).Columns("FIN").Position = .FecFinSumPos + lngPosicionScroll
            Case "GMN"
                sdbgAll.Groups(0).Columns("GMN").Position = .EstrMatPos + lngPosicionScroll
        End Select
    Next i
    'Volvemos a poner el level,porque sino no funciona bien
    sdbgAll.Columns("DESCR").Level = 0
    sdbgAll.Columns("ANYO").Level = .AnyoLevel
    sdbgAll.Columns("PROV").Level = .ProveedorLevel
    sdbgAll.Columns("PRECAPE").Level = .PresUniLevel
    sdbgAll.Columns("OBJ").Level = .OBJLevel
    sdbgAll.Columns("PORCENADJ").Level = .AdjLevel
    sdbgAll.Columns("VINCULADO_ITEM").Level = .SolicVinculadaLevel
    sdbgAll.Columns("AHORROPORCEN").Level = .AhorroPorcenLevel
    sdbgAll.Columns("AHORROIMP").Level = .AhorroLevel
    sdbgAll.Columns("CANT").Level = .CantidadLevel
    sdbgAll.Columns("IMP").Level = .ImporteLevel
    sdbgAll.Columns("INI").Level = .FecIniSumLevel
    sdbgAll.Columns("FIN").Level = .FecFinSumLevel
    sdbgAll.Columns("GMN").Level = .EstrMatLevel
    'columnas visibles
    sdbgAll.Columns("ANYO").Visible = .AnyoVisible
    If g_oPlantillaSeleccionada.AdminPub = True Then
        sdbgAll.Columns("PROV").Visible = False
    Else
        sdbgAll.Columns("PROV").Visible = .ProveedorVisible
    End If
    sdbgAll.Columns("PRECAPE").Visible = .PresUniVisible
    sdbgAll.Columns("OBJ").Visible = .OBJVisible
    sdbgAll.Columns("AHORROIMP").Visible = .AhorroVisible
    sdbgAll.Columns("AHORROPORCEN").Visible = .AhorroPorcenVisible
    sdbgAll.Columns("IMP").Visible = .ImporteVisible
    sdbgAll.Columns("CANT").Visible = .CantidadVisible
    sdbgAll.Columns("PORCENADJ").Visible = .AdjVisible
    sdbgAll.Columns("VINCULADO_ITEM").Visible = .SolicVinculadaVisible
    sdbgAll.Columns("INI").Visible = .FecIniSumVisible
    sdbgAll.Columns("FIN").Visible = .FecFinSumVisible
    sdbgAll.Columns("GMN").Visible = .EstrMatVisible
    'Calcula la diferencia de tama�o de las columnas (si se ha cambiado el tama�o del grupo u ocultado columnas que estaban visibles)
    lDifTamanyo = 0
    iVisibles = 0
    lSumFalta = 0
    lSumWidth = 0
    If .AnyoLevel = 1 And .AnyoVisible = True Then iVisibles = iVisibles + 1
    If .ProveedorLevel = 1 And .ProveedorVisible = True Then iVisibles = iVisibles + 1
    If .PresUniLevel = 1 And .PresUniVisible = True Then iVisibles = iVisibles + 1
    If .OBJLevel = 1 And .OBJVisible = True Then iVisibles = iVisibles + 1
    If .AhorroLevel = 1 And .AhorroVisible = True Then iVisibles = iVisibles + 1
    If .AhorroPorcenLevel = 1 And .AhorroPorcenVisible = True Then iVisibles = iVisibles + 1
    If .ImporteLevel = 1 And .ImporteVisible = True Then iVisibles = iVisibles + 1
    If .CantidadLevel = 1 And .CantidadVisible = True Then iVisibles = iVisibles + 1
    If .AdjLevel = 1 And .AdjVisible = True Then iVisibles = iVisibles + 1
    If .SolicVinculadaLevel = 1 And .SolicVinculadaVisible = True Then iVisibles = iVisibles + 1
    If .FecIniSumLevel = 1 And .FecIniSumVisible = True Then iVisibles = iVisibles + 1
    If .FecFinSumLevel = 1 And .FecFinSumVisible = True Then iVisibles = iVisibles + 1
    If .EstrMatLevel = 1 And .EstrMatVisible = True Then iVisibles = iVisibles + 1
    If sdbgAll.Groups(0).Width <> .Grupo0Width And iVisibles > 0 Then lDifTamanyo = (sdbgAll.Groups(0).Width - .Grupo0Width) / iVisibles
    If .AnyoLevel = 1 And .AnyoVisible = True Then lSumWidth = lSumWidth + .AnyoWidth
    If .ProveedorLevel = 1 And .ProveedorVisible = True Then lSumWidth = lSumWidth + .ProveedorWidth
    If .PresUniLevel = 1 And .PresUniVisible = True Then lSumWidth = lSumWidth + .PresUniWidth
    If .OBJLevel = 1 And .OBJVisible = True Then lSumWidth = lSumWidth + .OBJWidth
    If .AhorroLevel = 1 And .AhorroVisible = True Then lSumWidth = lSumWidth + .AhorroWidth
    If .AhorroPorcenLevel = 1 And .AhorroPorcenVisible = True Then lSumWidth = lSumWidth + .AhorroPorcenWidth
    If .ImporteLevel = 1 And .ImporteVisible = True Then lSumWidth = lSumWidth + .ImporteWidth
    If .CantidadLevel = 1 And .CantidadVisible = True Then lSumWidth = lSumWidth + .CantidadWidth
    If .AdjLevel = 1 And .AdjVisible = True Then lSumWidth = lSumWidth + .AdjWidth
    If .SolicVinculadaLevel = 1 And .SolicVinculadaVisible = True Then lSumWidth = lSumWidth + .SolicVinculadaWidth
    If .FecIniSumLevel = 1 And .FecIniSumVisible = True Then lSumWidth = lSumWidth + .FecIniSumWidth
    If .FecFinSumLevel = 1 And .FecFinSumVisible = True Then lSumWidth = lSumWidth + .FecFinSumWidth
    If .EstrMatLevel = 1 And .EstrMatVisible = True Then lSumWidth = lSumWidth + .EstrMatWidth
    If lSumWidth < .Grupo0Width And iVisibles > 0 Then lSumFalta = (.Grupo0Width - lSumWidth) / iVisibles
    'Tama�o de los campos del grupo 0 (se pone los widths en el mismo orden en que est�n los position,porque
    'sino a veces no funciona bien)
    For j = 0 To sdbgAll.Groups(0).Columns.Count - 1
        iIndex = sdbgAll.Groups(0).ColPosition(j)
        Select Case sdbgAll.Groups(0).Columns(iIndex).Name
            Case "DESCR"
                sdbgAll.Columns("DESCR").Width = .DescrWidth
            Case "ANYO"
                If .AnyoLevel = 1 And .AnyoVisible = True Then
                    sdbgAll.Columns("ANYO").Width = .AnyoWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("ANYO").Width = .AnyoWidth
                End If
            Case "PROV"
                If .ProveedorLevel = 1 And .ProveedorVisible = True Then
                    sdbgAll.Columns("PROV").Width = .ProveedorWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("PROV").Width = .ProveedorWidth
                End If
            Case "PRECAPE"
                If .PresUniLevel = 1 And .PresUniVisible = True Then
                    sdbgAll.Columns("PRECAPE").Width = .PresUniWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("PRECAPE").Width = .PresUniWidth
                End If
            Case "OBJ"
                If .OBJLevel = 1 And .OBJVisible = True Then
                    sdbgAll.Columns("OBJ").Width = .OBJWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("OBJ").Width = .OBJWidth
                End If
            Case "AHORROIMP"
                If .AhorroLevel = 1 And .AhorroVisible = True Then
                    sdbgAll.Columns("AHORROIMP").Width = .AhorroWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("AHORROIMP").Width = .AhorroWidth
                End If
            Case "AHORROPORCEN"
                If .AhorroPorcenLevel = 1 And .AhorroPorcenVisible = True Then
                    sdbgAll.Columns("AHORROPORCEN").Width = .AhorroPorcenWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("AHORROPORCEN").Width = .AhorroPorcenWidth
                End If
            Case "IMP"
                If .ImporteLevel = 1 And .ImporteVisible = True Then
                    sdbgAll.Columns("IMP").Width = .ImporteWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("IMP").Width = .ImporteWidth
                End If
            Case "CANT"
                If .CantidadLevel = 1 And .CantidadVisible = True Then
                    sdbgAll.Columns("CANT").Width = .CantidadWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("CANT").Width = .CantidadWidth
                End If
            Case "PORCENADJ"
                If .AdjLevel = 1 And .AdjVisible = True Then
                    sdbgAll.Columns("PORCENADJ").Width = .AdjWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("PORCENADJ").Width = .AdjWidth
                End If
            Case "VINCULADO_ITEM"
                If .SolicVinculadaLevel = 1 And .SolicVinculadaVisible = True Then
                    sdbgAll.Columns("VINCULADO_ITEM").Width = .SolicVinculadaWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("VINCULADO_ITEM").Width = .SolicVinculadaWidth
                End If
            Case "INI"
                If .FecIniSumLevel = 1 And .FecIniSumVisible = True Then
                    sdbgAll.Columns("INI").Width = .FecIniSumWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("INI").Width = .FecIniSumWidth
                End If
            Case "FIN"
                If .FecFinSumLevel = 1 And .FecFinSumVisible = True Then
                    sdbgAll.Columns("FIN").Width = .FecFinSumWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("FIN").Width = .FecFinSumWidth
                End If
            Case "GMN"
                If .EstrMatLevel = 1 And .EstrMatVisible = True Then
                    sdbgAll.Columns("GMN").Width = .EstrMatWidth + lDifTamanyo + lSumFalta
                Else
                    sdbgAll.Columns("GMN").Width = .EstrMatWidth
                End If
        End Select
    Next j
    'Calcula la dif. de tama�o de las columnas de los grupos, en el caso de que haya habido cambios en el tama�o del grupo
    'u ocultado columnas de la 2� fila que estaban visibles
    lDifTamanyo = 0
    iVisibles = 0
    lSumFalta = 0
    lSumWidth = 0
    If .ImpAdjLevel = 1 And .ImpAdjVisible = True Then iVisibles = iVisibles + 1
    If .PrecioProvLevel = 1 And .PrecioProvVisible = True Then iVisibles = iVisibles + 1
    If .AdjProvLevel = 1 And .AdjProvVisible = True Then iVisibles = iVisibles + 1
    If .CantProvLevel = 1 And .CantProvVisible = True Then iVisibles = iVisibles + 1
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        For Each oatrib In .PlantConfVistasAllAtrib
            If oatrib.Fila = 1 And oatrib.Visible = True Then iVisibles = iVisibles + 1
        Next
    End If
    If sdbgAll.Groups(2).Width <> .GruposWidth And iVisibles > 0 Then lDifTamanyo = (sdbgAll.Groups(2).Width - .GruposWidth) / iVisibles
    If .ImpAdjLevel = 1 And .ImpAdjVisible = True Then lSumWidth = lSumWidth + .ImpAdjWidth
    If .PrecioProvLevel = 1 And .PrecioProvVisible = True Then lSumWidth = lSumWidth + .PrecioProvWidth
    If .AdjProvLevel = 1 And .AdjProvVisible = True Then lSumWidth = lSumWidth + .AdjProvWidth
    If .CantProvLevel = 1 And .CantProvVisible = True Then lSumWidth = lSumWidth + .CantProvWidth
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        For Each oatrib In .PlantConfVistasAllAtrib
            If oatrib.Fila = 1 And oatrib.Visible = True Then lSumWidth = lSumWidth + oatrib.Width
        Next
    End If
    If lSumWidth < .GruposWidth And iVisibles > 0 Then lSumFalta = (.GruposWidth - lSumWidth) / iVisibles
    'Ahora dimensiona los campos para cada proveedor
    For i = 2 To sdbgAll.Groups.Count - 1
        'Level de los campos:
        sdbgAll.Columns("ImpAdj" & sdbgAll.Groups(i).TagVariant).Level = .ImpAdjLevel
        sdbgAll.Columns("Precio" & sdbgAll.Groups(i).TagVariant).Level = .PrecioProvLevel
        sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(i).TagVariant).Level = .AdjProvLevel
        sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(i).TagVariant).Level = .CantProvLevel
        For Each oatrib In .PlantConfVistasAllAtrib
            sdbgAll.Columns(oatrib.Atributo & sdbgAll.Groups(i).TagVariant).Level = oatrib.Fila
        Next
        'Visibilidad de los campos
        sdbgAll.Columns("ImpAdj" & sdbgAll.Groups(i).TagVariant).Visible = .ImpAdjVisible
        sdbgAll.Columns("Precio" & sdbgAll.Groups(i).TagVariant).Visible = .PrecioProvVisible
        sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(i).TagVariant).Visible = .CantProvVisible
        sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(i).TagVariant).Visible = .AdjProvVisible
        sdbgAll.Columns("CantMax" & sdbgAll.Groups(i).TagVariant).Level = m_iLevelCantMaxAll
        sdbgAll.Columns("CodProve" & sdbgAll.Groups(i).TagVariant).Level = m_iLevelCodProveAll
        sdbgAll.Columns("NumOfe" & sdbgAll.Groups(i).TagVariant).Level = m_iLevelNumOfeAll
        sdbgAll.Columns("Hom" & sdbgAll.Groups(i).TagVariant).Level = m_iLevelHomAll
        sdbgAll.Columns("Indice" & sdbgAll.Groups(i).TagVariant).Level = m_iLevelIndiceAll
        'Atributos
        For Each oatrib In .PlantConfVistasAllAtrib
            sdbgAll.Columns(oatrib.Atributo & sdbgAll.Groups(i).TagVariant).Visible = oatrib.Visible
        Next
        'Posici�n de los campos:
        iOrden = m_iNumColsAll + (sdbgAll.Groups(i).Columns.Count * (i - 2))
        inumColumns = 3
        If Not .PlantConfVistasAllAtrib Is Nothing Then inumColumns = inumColumns + .PlantConfVistasAllAtrib.Count
        For j = 0 To inumColumns
            Select Case m_arrOrdenColsAll(j)
                Case "IMPADJ"
                    If sdbgAll.Groups(0).Position = 0 Then sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(i).TagVariant).Position = .ImpAdjPos + iOrden
                Case "PRECIO"
                    If sdbgAll.Groups(0).Position = 0 Then sdbgAll.Columns("Precio" & sdbgAll.Groups(i).TagVariant).Position = .PrecioProvPos + iOrden
                Case "ADJPROV"
                    If sdbgAll.Groups(0).Position = 0 Then sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(i).TagVariant).Position = .AdjProvPos + iOrden
                Case "CANTPROV"
                    If sdbgAll.Groups(0).Position = 0 Then sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(i).TagVariant).Position = .CantProvPos + iOrden
                Case Else
                    'Es un atributo
                    sdbgAll.Columns(m_arrOrdenColsAll(j) & sdbgAll.Groups(i).TagVariant).Position = .PlantConfVistasAllAtrib.Item(CStr(m_arrOrdenColsAll(j))).Posicion + iOrden
            End Select
        Next j
        ''''Level de los campos (lo volvemos a poner despu�s de la posici�n porque sino no funciona bien)
        For j = inumColumns To 0 Step -1
            Select Case m_arrOrdenColsAll(j)
                Case "IMPADJ"
                    sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(i).TagVariant).Level = .ImpAdjLevel
                Case "PRECIO"
                    sdbgAll.Columns("Precio" & sdbgAll.Groups(i).TagVariant).Level = .PrecioProvLevel
                Case "ADJPROV"
                    sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(i).TagVariant).Level = .AdjProvLevel
                Case "CANTPROV"
                    sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(i).TagVariant).Level = .CantProvLevel
                Case Else
                    'Es un atributo
                    sdbgAll.Columns(m_arrOrdenColsAll(j) & sdbgAll.Groups(i).TagVariant).Level = .PlantConfVistasAllAtrib.Item(CStr(m_arrOrdenColsAll(j))).Fila
            End Select
        Next j
        'Visibilidad de los campos
        sdbgAll.Columns("ImpAdj" & sdbgAll.Groups(i).TagVariant).Visible = .ImpAdjVisible
        sdbgAll.Columns("Precio" & sdbgAll.Groups(i).TagVariant).Visible = .PrecioProvVisible
        sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(i).TagVariant).Visible = .CantProvVisible
        sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(i).TagVariant).Visible = .AdjProvVisible
        'Atributos
        For Each oatrib In .PlantConfVistasAllAtrib
            sdbgAll.Columns(oatrib.Atributo & sdbgAll.Groups(i).TagVariant).Visible = oatrib.Visible
        Next
        '''Tama�o de los campos:
        'El importe adjudicado,precio adjudicado,cant.adjudicada y %adjudicado
        For j = 0 To sdbgAll.Groups(i).Columns.Count - 1
            sColumna = ""
            iIndex = sdbgAll.Groups(i).ColPosition(j)
            sColumna = Mid(sdbgAll.Groups(i).Columns(iIndex).Name, 1, InStr(sdbgAll.Groups(i).Columns(iIndex).Name, sdbgAll.Groups(i).TagVariant) - 1)
            Select Case sColumna
                Case "IMPADJ"
                    If .ImpAdjLevel = 1 And .ImpAdjVisible = True Then
                        sdbgAll.Columns("ImpAdj" & sdbgAll.Groups(i).TagVariant).Width = .ImpAdjWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAll.Columns("ImpAdj" & sdbgAll.Groups(i).TagVariant).Width = .ImpAdjWidth
                    End If
                Case "Precio"
                    If .PrecioProvLevel = 1 And .PrecioProvVisible = True Then
                        sdbgAll.Columns("Precio" & sdbgAll.Groups(i).TagVariant).Width = .PrecioProvWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAll.Columns("Precio" & sdbgAll.Groups(i).TagVariant).Width = .PrecioProvWidth
                    End If
                Case "AdjPorcen"
                    If .AdjProvLevel = 1 And .AdjProvVisible = True Then
                        sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(i).TagVariant).Width = .AdjProvWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(i).TagVariant).Width = .AdjProvWidth
                    End If
                Case "AdjCantidad"
                    If .CantProvLevel = 1 And .CantProvVisible = True Then
                        sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(i).TagVariant).Width = .CantProvWidth + lDifTamanyo + lSumFalta
                    Else
                        sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(i).TagVariant).Width = .CantProvWidth
                    End If
                Case Else  'Comprueba si la columna es un atributo
                    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
                        If sColumna <> "CantMax" And sColumna <> "CodProve" And sColumna <> "NumOfe" And sColumna <> "Hom" And sColumna <> "Indice" Then
                            'atributos
                            If Not .PlantConfVistasAllAtrib.Item(CStr(sColumna)) Is Nothing Then
                                If .PlantConfVistasAllAtrib.Item(CStr(sColumna)).Fila = 1 And .PlantConfVistasAllAtrib.Item(CStr(sColumna)).Visible = True Then
                                    sdbgAll.Columns(sColumna & sdbgAll.Groups(i).TagVariant).Width = .PlantConfVistasAllAtrib.Item(CStr(sColumna)).Width + lDifTamanyo + lSumFalta
                                Else
                                    sdbgAll.Columns(sColumna & sdbgAll.Groups(i).TagVariant).Width = .PlantConfVistasAllAtrib.Item(CStr(sColumna)).Width
                                End If
                            End If
                        End If
                    End If
            End Select
        Next j
    Next i
    'Visualiza la columna de Objetivos totales s�lo cuando est� visible la de Objetivos
    If sdbgTotalesAll.Rows > 0 And sdbgAll.Groups.Count > 2 Then If sdbgAll.Groups(2).Visible = True Then sdbgTotalesAll.SplitterPos = 2
    If sdbgAll.Columns("OBJ").Width > 1000 Then
        sdbgTotalesAll.Columns("OBJTOT").Width = sdbgAll.Columns("OBJ").Width
    Else
        sdbgTotalesAll.Columns("OBJTOT").Width = 1000
    End If
    sdbgTotalesAll.Columns(0).Width = sdbgAll.Groups(0).Width - sdbgTotalesAll.Columns("OBJTOT").Width
    If sdbgAll.Columns("OBJ").Visible = False Then
        sdbgTotalesAll.Columns("OBJTOT").caption = ""
        sdbgTotalesAll.MoveFirst
    Else
        sdbgTotalesAll.Columns("OBJTOT").caption = sCaptionTotales
        sdbgTotalesAll.MoveFirst
    End If
    If sdbgAll.Visible Then sdbgAll.SetFocus
End With
End Sub

''' <summary>
''' Muestra/Oculta las columnas fijas de sdgAll y redimensiona las columnas fijas de  sdbgTotalesAll
''' </summary>
''' <remarks>Llamada desde: CargarAdjudicacionesAll ; Tiempo m�ximo:0</remarks>
Private Sub PrepararGridAll()
    Dim i As Integer
With sdbgAll
    .RemoveAll
    i = .Groups.Count
    .Groups(0).Position = 0
    sdbgTotalesAll.Columns(0).Position = 0
    'Hace visibles todos los grupos de proveedores porque sino no funciona el scroll
    For i = 2 To .Groups.Count - 1
        .Groups(i).Visible = True
    Next i
    While i > 2
        .Groups.Remove (2)
        i = .Groups.Count
    Wend
    .Redraw = True
    .LevelCount = 3
    .Columns("PORCENADJ").Level = 1
    .Columns("VINCULADO_ITEM").Level = 1
    .Columns("AHORROPORCEN").Level = 1
    .Columns("AHORROIMP").Level = 1
    .Columns("CANT").Level = 1
    .Columns("IMP").Level = 1
    .Columns("INI").Level = 1
    .Columns("FIN").Level = 1
    .Columns("GMN").Level = 1
    .Columns("AHORROIMP").Position = 8
    .Columns("AHORROPORCEN").Position = 9
    .Columns("IMP").Position = 10
    .Columns("CANT").Position = 11
    .Columns("PORCENADJ").Position = 12
    .Columns("VINCULADO_ITEM").Position = 13
    .Columns("INI").Position = 14
    .Columns("FIN").Position = 15
    .Columns("GMN").Position = 16
    .Columns("DESCR").Visible = True
    .Columns("ANYO").Visible = m_oVistaSeleccionadaAll.AnyoVisible
    If g_oPlantillaSeleccionada.AdminPub = True Then
        .Columns("PROV").Visible = False
    Else
        .Columns("PROV").Visible = m_oVistaSeleccionadaAll.ProveedorVisible
    End If
    .Columns("PRECAPE").Visible = m_oVistaSeleccionadaAll.PresUniVisible
    .Columns("OBJ").Visible = m_oVistaSeleccionadaAll.OBJVisible
    .Columns("AHORROIMP").Visible = m_oVistaSeleccionadaAll.AhorroVisible
    .Columns("AHORROPORCEN").Visible = m_oVistaSeleccionadaAll.AhorroPorcenVisible
    .Columns("IMP").Visible = m_oVistaSeleccionadaAll.ImporteVisible
    .Columns("CANT").Visible = m_oVistaSeleccionadaAll.CantidadVisible
    .Columns("PORCENADJ").Visible = m_oVistaSeleccionadaAll.AdjVisible
    .Columns("VINCULADO_ITEM").Visible = m_oVistaSeleccionadaAll.SolicVinculadaVisible
    .Columns("INI").Visible = m_oVistaSeleccionadaAll.FecIniSumVisible
    .Columns("FIN").Visible = m_oVistaSeleccionadaAll.FecFinSumVisible
    .Columns("GMN").Visible = m_oVistaSeleccionadaAll.EstrMatVisible
    If .Columns("OBJ").Visible = False Then
        sdbgTotalesAll.Columns("OBJTOT").caption = ""
    Else
        If .Columns("OBJ").Width > 1000 Then
            sdbgTotalesAll.Columns("OBJTOT").Width = .Columns("OBJ").Width
        Else
            sdbgTotalesAll.Columns("OBJTOT").Width = 1000
        End If
        sdbgTotalesAll.Columns(0).Width = .Groups(0).Width - sdbgTotalesAll.Columns("OBJTOT").Width
        sdbgTotalesAll.Columns("OBJTOT").caption = sCaptionTotales
    End If
    DoEvents
End With
End Sub

''' <summary>
''' Muestra/Oculta las columnas fijas de sdgAdj y redimensiona las columnas fijas de  sdbgTotales
''' </summary>
''' <remarks>Llamada desde: CargarAdjudicacionesGrupo ; Tiempo m�ximo:0</remarks>
Public Sub PrepararGridGrupo()
    Dim i As Integer
With sdbgAdj
    .RemoveAll
    'Hace visibles todos los grupos de proveedores porque sino no funciona el scroll
    For i = 2 To .Groups.Count - 1
        .Groups(i).Visible = True
    Next i
    .Groups(0).Position = 0
    i = .Groups.Count
    While i > 2
        .Groups.Remove (2)
        i = .Groups.Count
    Wend
    .Redraw = True
    .LevelCount = 3
    .Columns("PORCENADJ").Level = 1
    .Columns("VINCULADO_ITEM").Level = 1
    .Columns("AHORROPORCEN").Level = 1
    .Columns("AHORROIMP").Level = 1
    .Columns("CANT").Level = 1
    .Columns("IMP").Level = 1
    .Columns("INI").Level = 1
    .Columns("FIN").Level = 1
    .Columns("GMN").Level = 1
    .Columns("AHORROIMP").Position = 8
    .Columns("AHORROPORCEN").Position = 9
    .Columns("IMP").Position = 10
    .Columns("CANT").Position = 11
    .Columns("PORCENADJ").Position = 12
    .Columns("VINCULADO_ITEM").Position = 13
    .Columns("INI").Position = 14
    .Columns("FIN").Position = 15
    .Columns("GMN").Position = 16
    .Columns("DESCR").Visible = True
    .Columns("ANYO").Visible = m_oVistaSeleccionadaGr.AnyoVisible
    If g_oPlantillaSeleccionada.AdminPub = True Then
        .Columns("PROV").Visible = False
    Else
        .Columns("PROV").Visible = m_oVistaSeleccionadaGr.ProveedorVisible
    End If
    .Columns("PRECAPE").Visible = m_oVistaSeleccionadaGr.PresUniVisible
    .Columns("OBJ").Visible = m_oVistaSeleccionadaGr.OBJVisible
    .Columns("AHORROIMP").Visible = m_oVistaSeleccionadaGr.AhorroVisible
    .Columns("AHORROPORCEN").Visible = m_oVistaSeleccionadaGr.AhorroPorcenVisible
    .Columns("IMP").Visible = m_oVistaSeleccionadaGr.ImporteVisible
    .Columns("CANT").Visible = m_oVistaSeleccionadaGr.CantidadVisible
    .Columns("PORCENADJ").Visible = m_oVistaSeleccionadaGr.AdjVisible
    .Columns("VINCULADO_ITEM").Visible = m_oVistaSeleccionadaGr.SolicVinculadaVisible
    .Columns("INI").Visible = m_oVistaSeleccionadaGr.FecIniSumVisible
    .Columns("FIN").Visible = m_oVistaSeleccionadaGr.FecFinSumVisible
    .Columns("GMN").Visible = m_oVistaSeleccionadaGr.EstrMatVisible
    If .Columns("OBJ").Visible = False Then
        sdbgTotales.Columns("OBJTOT").caption = ""
    Else
        If .Columns("OBJ").Width > 1000 Then
            sdbgTotales.Columns("OBJTOT").Width = .Columns("OBJ").Width
        Else
            sdbgTotales.Columns("OBJTOT").Width = 1000
        End If
        sdbgTotales.Columns(0).Width = .Groups(0).Width - sdbgTotales.Columns("OBJTOT").Width
        sdbgTotales.Columns("OBJTOT").caption = sCaptionTotales
    End If
    DoEvents
End With
End Sub

Private Sub CargarGridAll()
Dim i As Integer
Dim iGroupIndex As Integer
Dim ogroup As SSDataWidgets_B.Group
Dim oColumn As SSDataWidgets_B.Column
Dim oatrib As CAtributo
Dim j As Integer
'Comprobamos que hay un proceso y grupo seleccionado
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
'Limpiamos la linea de totales
sdbgTotalesAll.Columns(0).Position = 0
sdbgTotalesAll.Columns(1).Position = 1
sdbgTotalesAll.RemoveAll
i = 2
While i < sdbgTotalesAll.Columns.Count
    sdbgTotalesAll.Columns(i).Visible = True
    sdbgTotalesAll.Columns.Remove i
Wend
PrepararGridAll
'Cargamos los proveedores en los grupos de la grid.
iGroupIndex = 2
For j = 1 To 3
    'A�adimos el proveedor a la grid
    sdbgAll.Groups.Add iGroupIndex
    Set ogroup = sdbgAll.Groups(iGroupIndex)
    ogroup.caption = m_sProveedor & " " & j
    sdbgAll.Groups(iGroupIndex).Width = 1600
    sdbgAll.Groups(iGroupIndex).AllowSizing = True
    sdbgAll.Groups(iGroupIndex).CaptionAlignment = ssCaptionAlignmentCenter
    ogroup.Visible = True
    ogroup.HeadStyleSet = "Proveedor"
    'A�adimos el proveedor a la grid de totales
    sdbgTotalesAll.Columns.Add sdbgTotalesAll.Columns.Count
    Set oColumn = sdbgTotalesAll.Columns(sdbgTotalesAll.Columns.Count - 1)
    oColumn.Name = "PROVE" & j
    oColumn.caption = m_sProveedor & " " & j
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Visible = True
    oColumn.HeadStyleSet = "Proveedor"
    oColumn.Width = 160
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = "Precio" & "PROVE" & j
    oColumn.HeadStyleSet = "YellowHead"
    oColumn.caption = m_sCap(4)
    oColumn.StyleSet = "Yellow"
    oColumn.Level = 0
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Locked = True
    oColumn.Visible = m_oVistaSeleccionadaAll.PrecioProvVisible
    DoEvents
    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = "CantMax" & "PROVE" & j
    oColumn.caption = m_sCap(9)
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Level = 0
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 2
    Set oColumn = ogroup.Columns(2)
    oColumn.Name = "AdjPorcen" & "PROVE" & j
    oColumn.caption = m_sCap(2)
    oColumn.HeadStyleSet = "BlueHead"
    oColumn.StyleSet = "Blue"
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Level = 1
    oColumn.Width = 300
    oColumn.Locked = True
    oColumn.Visible = m_oVistaSeleccionadaAll.AdjProvVisible
    DoEvents
    ogroup.Columns.Add 3
    Set oColumn = ogroup.Columns(3)
    oColumn.Name = "AdjCantidad" & "PROVE" & j
    oColumn.caption = m_sCap(3)
    oColumn.StyleSet = "Blue"
    oColumn.HeadStyleSet = "BlueHead"
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Level = 1
    oColumn.Width = 1300
    oColumn.Locked = True
    oColumn.Visible = m_oVistaSeleccionadaAll.CantProvVisible
    DoEvents
    ogroup.Columns.Add 4
    Set oColumn = ogroup.Columns(4)
    oColumn.Name = "CodProve" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 5
    Set oColumn = ogroup.Columns(5)
    oColumn.Name = "NumOfe" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 6
    Set oColumn = ogroup.Columns(6)
    oColumn.Name = "Hom" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 7
    Set oColumn = ogroup.Columns(7)
    oColumn.Name = "Indice" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 8
    Set oColumn = ogroup.Columns(8)
    oColumn.Name = "IMPADJ" & "PROVE" & j
    oColumn.HeadStyleSet = "YellowHead"
    oColumn.caption = m_sImpAdj
    oColumn.StyleSet = "Yellow"
    oColumn.Level = 0
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Visible = m_oVistaSeleccionadaAll.ImpAdjVisible
    oColumn.Locked = True
    DoEvents
    'A�adimos los atributos a cada grupo
    i = 9
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
            If oatrib.ambito = AmbItem Then
                ogroup.Columns.Add i
                Set oColumn = ogroup.Columns(i)
                oColumn.Name = oatrib.idAtribProce & "PROVE" & j
                oColumn.caption = oatrib.Cod
                oColumn.HeadStyleSet = "YellowHead"
                oColumn.StyleSet = "Yellow"
                oColumn.Level = 0
                oColumn.Locked = True
                oColumn.Visible = m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib.Item(CStr(oatrib.idAtribProce)).Visible
                oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                'Si el atributo es de tipo boolean la celda ser� un checkbox
                If oatrib.Tipo = TipoBoolean Then
                    oColumn.Style = ssStyleCheckBox
                End If
                i = i + 1
                DoEvents
            End If
        Next
    End If
    ogroup.TagVariant = "PROVE" & j
    iGroupIndex = iGroupIndex + 1
Next j
'***************** CONTENIDO DE LA GRID *****************
'Ahora rellenamos la grid:
'Inicializamos a 0 la linea de totales
sdbgTotalesAll.AddItem m_sCap(10)
sdbgTotalesAll.Update
sdbgTotalesAll.AddItem m_sAdjudicado
sdbgTotalesAll.MoveNext
sdbgTotalesAll.Update
sdbgTotalesAll.MoveFirst
'Rellena la grid
sdbgAll.AddItem m_sCap(6) & " 1"
sdbgAll.AddItem m_sCap(6) & " 2"
sdbgAll.AddItem m_sCap(6) & " 3"
sdbgAll.AddItem m_sCap(6) & " 4"
'Si el objetivo est� visible lo deja visible en la grid de totales
If sdbgAll.Columns("OBJ").Visible = False Then
    sdbgTotalesAll.Columns("OBJTOT").caption = ""
Else
    sdbgTotalesAll.Columns("OBJTOT").caption = sCaptionTotales
End If
End Sub

Private Sub MostrarSplit()
If sdbgAdj.Groups.Count > 2 Then
    sdbgAdj.SplitterVisible = True
    sdbgTotales.SplitterVisible = True
    
    If sdbgAdj.VisibleGrps > 1 Then
        sdbgAdj.SplitterPos = 1
        sdbgTotales.SplitterPos = 2
    End If
Else
    sdbgAdj.SplitterVisible = False
    sdbgTotales.SplitterVisible = False
End If
End Sub

Private Sub MostrarSplitAll()
If sdbgAll.Groups.Count > 2 Then
    sdbgAll.SplitterVisible = True
    sdbgTotalesAll.SplitterVisible = True
        
    If sdbgAll.VisibleGrps > 1 Then
        sdbgAll.SplitterPos = 1
        sdbgTotalesAll.SplitterPos = 2
    End If
Else
    sdbgAll.SplitterVisible = False
    sdbgTotalesAll.SplitterVisible = False
End If
End Sub

''' <summary>
'''Carga la configuraci�n de la vista actual para el proceso seleccionado en las grids y campos de configuraci�n correspondientes
''' </summary>
''' <remarks>Llamada desde: cmdEliminarVista_Click  cmdGuardarVistaNueva_Click      sdbcVistaActual_CloseUp; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarVistasProc()
    Dim oatrib As CPlantConfVistaProceAtrib
    Dim sPertenece As String
    Dim oSobre As CPlantConfVistaProceSobre
    Dim sGrupo As String
    'Limpia las grids
    sdbgOcultarCampos.RemoveAll
    If m_oVistaSeleccionada Is Nothing Then Exit Sub
    'Primero carga los campos fijos:
    'Consumido
    sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.ConsumidoVisible) & Chr(m_lSeparador) & m_sConsumido & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Consumido"
    'Adjudicado
    sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.AdjVisible) & Chr(m_lSeparador) & m_sAdjudicado & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Adjudicado"
    'Ahorrado adjudicado
    sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.AhorroAdjVisible) & Chr(m_lSeparador) & m_sAhorrado & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Ahorrado"
    '%Ahorrado adjudicado
    sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.AhorroAdjPorcenVisible) & Chr(m_lSeparador) & m_sAhorroPorce & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Ahorro%"
    'Importe
    sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.ImporteVisible) & Chr(m_lSeparador) & m_sImporteOferta & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Importe"
    'Ahorro oferta
    sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.AhorroOfeVisible) & Chr(m_lSeparador) & m_sAhorroOferta & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AhorroOferta"
    '%ahorro oferta
    sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.AhorroOfePorcenVisible) & Chr(m_lSeparador) & m_sPorcenAhorroOferta & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AhorroOfe%"
    'Solic Vinculado
    If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
        sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.SolicVinculadaVisible) & Chr(m_lSeparador) & m_sCaptionVistaSolicVinculada & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "SolicVinculadaGrupo"
    ElseIf g_oPlantillaSeleccionada.Solicitud = EnProceso Then
        sdbgOcultarCampos.AddItem BooleanToSQLBinary(m_oVistaSeleccionada.SolicVinculadaVisible) & Chr(m_lSeparador) & m_sCaptionVistaSolicVinculada & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "SolicVinculadaProce"
    End If
    'Ahora carga los atributos en la grid:
    If Not m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
        For Each oatrib In m_oVistaSeleccionada.PlantConfVistasProceAtrib
            sPertenece = ""
            If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
                If Not g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)) Is Nothing Then
                    If g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).ambito = AmbProceso Then
                        sPertenece = m_sProceso
                    Else
                        If g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).ambito = AmbGrupo Then
                            sPertenece = m_sLitGrupo & ":"
                            If IsNull(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo) Then
                                sPertenece = sPertenece & m_sTodos
                            Else
                                sGrupo = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo
                                sPertenece = sPertenece & sGrupo
                            End If
                        End If
                    End If
                    sdbgOcultarCampos.AddItem BooleanToSQLBinary(oatrib.Visible) & Chr(m_lSeparador) & oatrib.CodAtributo & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.Atributo & Chr(m_lSeparador) & ""
                End If
            End If
        Next
    End If
    If g_oPlantillaSeleccionada.AdminPub = True Then
        sdbgSobres.RemoveAll
        'Si es un proceso de administraci�n p�blica carga la grid con los sobres a mostrar
        sdbgSobres.Enabled = True
        For Each oSobre In m_oVistaSeleccionada.PlantConfVistasProceSobre
            If oSobre.Visible = True Then
                'Est� visible
                sdbgSobres.AddItem "-1" & Chr(m_lSeparador) & m_sSobre & oSobre.Sobre & Chr(m_lSeparador) & oSobre.Sobre
            Else
                'invisible
                sdbgSobres.AddItem "0" & Chr(m_lSeparador) & m_sSobre & oSobre.Sobre & Chr(m_lSeparador) & oSobre.Sobre
            End If
        Next
    End If
     'Chequea o no los checks correspondientes:
    chkOcultarGrCerrados.Value = BooleanToSQLBinary(m_oVistaSeleccionada.OcultarGrCerrados)
    chkExcluirGrCerradosResul.Value = BooleanToSQLBinary(m_oVistaSeleccionada.ExcluirGrCerradosResult)
    chkOcultarNoAdj.Value = BooleanToSQLBinary(m_oVistaSeleccionada.OcultarNoAdj)
    chkOcultarProvSinOfe.Value = BooleanToSQLBinary(m_oVistaSeleccionada.OcultarProvSinOfe)
    'N�mero de decimales a mostrar:
    txtDecPorcen.Text = m_oVistaSeleccionada.DecPorcen
    txtDecResult.Text = m_oVistaSeleccionada.DecResult
    ConfigurarBotonesVistas m_oVistaSeleccionada.Vista
End Sub

''' <summary>Carga en la grid de configuraci�n de las vistas de los campos,los campos a mostrar y atributos del grupo ALL</summary>
''' <remarks>Llamada desde: GrupoEscaladoSeleccionado, cmdEliminarVistaALL_Click, cmdGuardarVistaNuevaALL_Click,
''' sdbcVistaActAll_CloseUp, CargarAdjudicacionesALL; Tiempo m�ximo:0</remarks>
Private Sub CargarVistasAll()
Dim oatrib As CPlantConfVistaAllAtrib
Dim sPertenece As String
Dim sGrupo As String
Dim bMostrarAtrib As Boolean
With sdbgOcultarCamposAll
    'Limpia las grids
    .RemoveAll
    If m_oVistaSeleccionadaAll Is Nothing Then
        Exit Sub
    End If
    'Primero carga los campos fijos:
    'A�o/Dest
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.AnyoVisible) & Chr(m_lSeparador) & m_sAnyo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "ANYO"
    'Proveedor actual
    If g_oPlantillaSeleccionada.AdminPub = False Then
        .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.ProveedorVisible) & Chr(m_lSeparador) & m_sProvActual & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PROV"
    Else
        m_oVistaSeleccionadaAll.ProveedorWidth = 0
        If m_oVistaSeleccionadaAll.PresUniWidth = 1025 Then m_oVistaSeleccionadaAll.PresUniWidth = 2050
    End If
    'Pres.Unitario
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.PresUniVisible) & Chr(m_lSeparador) & m_sPresUnitario & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PRECAPE"
    'Fechas de suministro
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.FecIniSumVisible) & Chr(m_lSeparador) & m_sFecIniSum & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "INI"
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.FecFinSumVisible) & Chr(m_lSeparador) & m_sFecFinSum & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "FIN"
    'Estr. material
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.EstrMatVisible) & Chr(m_lSeparador) & m_sEstrMat & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "GMN"
    '%Objetivo
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.OBJVisible) & Chr(m_lSeparador) & m_sCap(1) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "OBJ"
    'Ahorro Imp.
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.AhorroVisible) & Chr(m_lSeparador) & m_sAhorroImp & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AHORROIMP"
    'Ahorro %
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.AhorroPorcenVisible) & Chr(m_lSeparador) & m_sAhorroPorcen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AHORROPORCEN"
    'Importe
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.ImporteVisible) & Chr(m_lSeparador) & m_sImporte & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "IMP"
    'Cantidad
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.CantidadVisible) & Chr(m_lSeparador) & m_sCap(3) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "CANT"
    'Adj%
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.AdjVisible) & Chr(m_lSeparador) & m_sCap(2) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PORCENADJ"
    'El importe adjudicado:
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.ImpAdjVisible) & Chr(m_lSeparador) & m_sCap(5) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "IMPADJ"
    'El precio de adjudicaci�n (por proveedor)
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.PrecioProvVisible) & Chr(m_lSeparador) & m_sCap(4) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Precio"
    'El importe adjudicado (por proveedor)
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.AdjProvVisible) & Chr(m_lSeparador) & m_sCap(8) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AdjPorcen"
    'La cantidad adjudicada (por proveedor)
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.CantProvVisible) & Chr(m_lSeparador) & m_sCap(7) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AdjCantidad"
    'Vinculado_item
    If g_oPlantillaSeleccionada.Solicitud = EnItem Then .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaAll.SolicVinculadaVisible) & Chr(m_lSeparador) & m_sCaptionVistaSolicVinculada & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "VINCULADO_ITEM"
    'Ahora carga los atributos en la grid:
    If Not m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib Is Nothing Then
        For Each oatrib In m_oVistaSeleccionadaAll.PlantConfVistasAllAtrib
            sPertenece = ""
            If Not g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)) Is Nothing Then
                If g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).ambito = AmbItem Then
                    bMostrarAtrib = True
                    'Si el atributo es de �mbito item:
                    sPertenece = m_sCap(6) & ":"
                    
                    If IsNull(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo) Then
                        sPertenece = sPertenece & m_sTodos   'Pertenece a todos los grupos
                    Else
                        sGrupo = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo
                        sPertenece = sPertenece & sGrupo   'Pertenece a un grupo en concreto
                        If g_oPlantillaSeleccionada.AdminPub = True Then
                            'Si es un proc. de admin.pub. comprueba que el sobre al que pertenece el grupo
                            'del atributo no invisible.
                            If m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(g_oPlantillaSeleccionada.Grupos.Item(sGrupo).Sobre)).Visible = False Then
                                bMostrarAtrib = False
                            End If
                        End If
                    End If
                    If bMostrarAtrib Then .AddItem BooleanToSQLBinary(oatrib.Visible) & Chr(m_lSeparador) & oatrib.CodAtributo & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.Atributo
                End If
            End If
        Next
    End If
    'Chequea o no los checks correspondientes:
    chkOcultarItCerradosAll.Value = IIf(m_oVistaSeleccionadaAll.OcultarItCerrados = True, 1, 0)
    chkExcluirItCerradosResulAll.Value = IIf(m_oVistaSeleccionadaAll.ExcluirItCerradosResult = True, 1, 0)
    chkOcultarNoAdjAll.Value = IIf(m_oVistaSeleccionadaAll.OcultarNoAdj = True, 1, 0)
    chkOcultarProvSinOfeAll.Value = IIf(m_oVistaSeleccionadaAll.OcultarProvSinOfe = True, 1, 0)
    'N�mero de decimales a mostrar:
    txtDecCantAll.Text = m_oVistaSeleccionadaAll.DecCant
    txtDecPorcenAll.Text = m_oVistaSeleccionadaAll.DecPorcen
    txtDecPrecAll.Text = m_oVistaSeleccionadaAll.DecPrecios
    txtDecResultAll.Text = m_oVistaSeleccionadaAll.DecResult
End With
End Sub

Public Sub GeneralSeleccionado()
Dim sVista As String
'Cargamos la configuraci�n de la vista por defecto para el proceso seleccionado
ConfiguracionVistaActual m_oVistaSeleccionada.Vista, False
CargarVistasProc
RedimensionarGrid
'Carga las combos de vista por defecto y vista actual
Select Case g_oPlantillaSeleccionada.VistaDefectoComp
    Case TipoDeVistaDefecto.vistainicial
        sdbcVistaDefecto.Text = m_sVistaIni
    Case Else
        sdbcVistaDefecto.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
End Select
Select Case m_oVistaSeleccionada.Vista
    Case TipoDeVistaDefecto.vistainicial
        sdbcVistaActual.Text = m_sVistaIni
        sVista = m_sVistaIni
    Case Else
        sdbcVistaActual.Text = m_oVistaSeleccionada.NombreVista
        sVista = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
End Select
sdbcVistaDefecto.Columns("COD").Value = CStr(g_oPlantillaSeleccionada.VistaDefectoComp)
sdbcVistaActual.Columns("COD").Value = CStr(m_oVistaSeleccionada.Vista)
' pongo el nombre en el caption del grid
If m_bGridProcVisible Then
    sdbgProce.caption = sVista
    sdbgGruposProve.caption = ""
Else
    sdbgGruposProve.caption = sVista
    sdbgProce.caption = ""
End If
sdbgProveGrupos.caption = sVista
End Sub

''' <summary>Carga en la grid de configuraci�n de las vistas de los campos,los campos a mostrar y atributos del grupo</summary>
''' <remarks>Llamada desde: GrupoEscaladoSeleccionado, cmdEliminarVistaGR_Click, cmdGuardarVistaNuevaGr_Click,
''' sdbcVistaActGr_CloseUp, CargarAdjudicacionesGrupo; Tiempo m�ximo:0</remarks>
Private Sub CargarVistasGrupo()
Dim oatrib As CPlantConfVistaGrupoAtrib
Dim sPertenece As String
With sdbgOcultarCamposGr
    'Limpia la grid
    .RemoveAll
    If m_oVistaSeleccionadaGr Is Nothing Then Exit Sub
    'Primero carga los campos fijos:
    'A�o/Dest
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.AnyoVisible) & Chr(m_lSeparador) & m_sAnyo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "ANYO"
    'Proveedor actual
    If g_oPlantillaSeleccionada.AdminPub = False Then
        .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.ProveedorVisible) & Chr(m_lSeparador) & m_sProvActual & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PROV"
    Else
        m_oVistaSeleccionadaGr.ProveedorWidth = 0
        If m_oVistaSeleccionadaGr.PresUniWidth = 1025 Then m_oVistaSeleccionadaGr.PresUniWidth = 2050
    End If
    'Pres.Unitario
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.PresUniVisible) & Chr(m_lSeparador) & m_sPresUnitario & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PRECAPE"
    'Fechas de suministro
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.FecIniSumVisible) & Chr(m_lSeparador) & m_sFecIniSum & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "INI"
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.FecFinSumVisible) & Chr(m_lSeparador) & m_sFecFinSum & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "FIN"
    'Estr. material
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.EstrMatVisible) & Chr(m_lSeparador) & m_sEstrMat & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "GMN"
    '%Objetivo
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.OBJVisible) & Chr(m_lSeparador) & m_sCap(1) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "OBJ"
    'Ahorro Imp.
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.AhorroVisible) & Chr(m_lSeparador) & m_sAhorroImp & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AHORROIMP"
    'Ahorro %
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.AhorroPorcenVisible) & Chr(m_lSeparador) & m_sAhorroPorcen & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AHORROPORCEN"
    'Importe
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.ImporteVisible) & Chr(m_lSeparador) & m_sImporte & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "IMP"
    'Cantidad
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.CantidadVisible) & Chr(m_lSeparador) & m_sCap(3) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "CANT"
    'Adj%
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.AdjVisible) & Chr(m_lSeparador) & m_sCap(2) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "PORCENADJ"
    'El importe adjudicado:
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.ImpAdjVisible) & Chr(m_lSeparador) & m_sCap(5) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "IMPADJ"
    'El precio de adjudicaci�n (por proveedor)
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.PrecioProvVisible) & Chr(m_lSeparador) & m_sCap(4) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "Precio"
    'El importe adjudicado (por proveedor)
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.AdjProvVisible) & Chr(m_lSeparador) & m_sCap(8) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AdjPorcen"
    'La cantidad adjudicada (por proveedor)
    .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.CantProvVisible) & Chr(m_lSeparador) & m_sCap(7) & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "AdjCantidad"
    'Solic Vinculado
    If g_oPlantillaSeleccionada.Solicitud = EnItem Then .AddItem BooleanToSQLBinary(m_oVistaSeleccionadaGr.SolicVinculadaVisible) & Chr(m_lSeparador) & m_sCaptionVistaSolicVinculada & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "VINCULADO_ITEM"
    'Ahora carga los atributos:
    .Enabled = True
    If Not m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib Is Nothing Then
        For Each oatrib In m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib
            sPertenece = ""
            If Not g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)) Is Nothing Then
                If g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).ambito = AmbItem Then
                    If IsNull(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo) Or g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo = m_oGrupoSeleccionado.Codigo Then
                        sPertenece = m_sCap(6) & ":"
                        If IsNull(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo) Then
                            sPertenece = sPertenece & m_sTodos
                        ElseIf g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo = m_oGrupoSeleccionado.Codigo Then
                            sPertenece = sPertenece & g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo
                        End If
                        .AddItem BooleanToSQLBinary(oatrib.Visible) & Chr(m_lSeparador) & oatrib.CodAtributo & Chr(m_lSeparador) & sPertenece & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.Atributo
                    End If
                End If
            End If
        Next
    End If
    'Chequea o no los checks correspondientes:
    chkOcultarItCerradosGr.Value = IIf(m_oVistaSeleccionadaGr.OcultarItCerrados = True, 1, 0)
    chkExcluirItCerradosResul.Value = IIf(m_oVistaSeleccionadaGr.ExcluirItCerradosResult = True, 1, 0)
    chkOcultarNoAdjGr.Value = IIf(m_oVistaSeleccionadaGr.OcultarNoAdj = True, 1, 0)
    chkOcultarProvSinOfeGr.Value = IIf(m_oVistaSeleccionadaGr.OcultarProvSinOfe = True, 1, 0)
    'N�mero de decimales a mostrar:
    txtDecCantGr.Text = m_oVistaSeleccionadaGr.DecCant
    txtDecPorcenGr.Text = m_oVistaSeleccionadaGr.DecPorcen
    txtDecPrecGr.Text = m_oVistaSeleccionadaGr.DecPrecios
    txtDecResultGr.Text = m_oVistaSeleccionadaGr.DecResult
End With
End Sub

Private Sub sstabComparativa_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
picControlVistaProceso.Visible = False
picControlVistaAll.Visible = False
picControlVistaGrupo.Visible = False
End Sub

Private Sub ConfigurarPicControlVistaProceso()
'Configuar el picture de control de la vista de proceso,seg�n si el proceso es de administraci�n p�blica o no
If g_oPlantillaSeleccionada.AdminPub = True Then
    'Es de administraci�n p�blica
    lineSobres.Visible = True
    sdbgSobres.Visible = True
    lblMostrarSobres.Visible = True
    lblMostrarCampos.Top = 1150
    sdbgOcultarCampos.Top = 1365
    sdbgOcultarCampos.Height = 1695
Else
    'Si no es un proceso de administraci�n p�blica no aparecen los sobres
    lineSobres.Visible = False
    sdbgSobres.Visible = False
    lblMostrarSobres.Visible = False
    lblMostrarCampos.Top = 1150
    sdbgOcultarCampos.Top = 1400
    sdbgOcultarCampos.Height = 3000
End If
End Sub

Private Sub ConfigurarBotonesVistas(ByVal iTipoVista As Integer)
If iTipoVista <> 0 Then
    cmdGuardarVista.Visible = m_bModif
    cmdGuardarVistaNueva.Visible = m_bModif
    cmdEliminarVista.Visible = m_bModif
    cmdRenombrarVista.Visible = m_bModif
    cmdGuardarVistaGr.Visible = m_bModif
    cmdGuardarVistaNuevaGr.Visible = m_bModif
    cmdEliminarVistaGr.Visible = m_bModif
    cmdRenombrarVistaGr.Visible = m_bModif
    cmdGuardarVistaAll.Visible = m_bModif
    cmdGuardarVistaNuevaAll.Visible = m_bModif
    cmdEliminarVistaAll.Visible = m_bModif
    cmdRenombrarVistaAll.Visible = m_bModif
    cmdGuardarVista.Left = 150
    cmdGuardarVistaNueva.Left = cmdGuardarVista.Left + cmdGuardarVista.Width + 50
    cmdEliminarVista.Left = cmdGuardarVistaNueva.Left + cmdGuardarVistaNueva.Width + 50
    cmdRenombrarVista.Left = cmdEliminarVista.Left + cmdEliminarVista.Width + 50
    cmdGuardarVistaGr.Left = 150
    cmdGuardarVistaNuevaGr.Left = cmdGuardarVistaGr.Left + cmdGuardarVistaGr.Width + 50
    cmdEliminarVistaGr.Left = cmdGuardarVistaNuevaGr.Left + cmdGuardarVistaNuevaGr.Width + 50
    cmdRenombrarVistaGr.Left = cmdEliminarVistaGr.Left + cmdEliminarVistaGr.Width + 50
    cmdGuardarVistaAll.Left = 150
    cmdGuardarVistaNuevaAll.Left = cmdGuardarVistaAll.Left + cmdGuardarVistaAll.Width + 50
    cmdEliminarVistaAll.Left = cmdGuardarVistaNuevaAll.Left + cmdGuardarVistaNuevaAll.Width + 50
    cmdRenombrarVistaAll.Left = cmdEliminarVistaAll.Left + cmdEliminarVistaAll.Width + 50
Else
    cmdGuardarVista.Visible = False
    cmdGuardarVistaNueva.Visible = m_bModif
    cmdEliminarVista.Visible = False
    cmdRenombrarVista.Visible = False
    cmdGuardarVistaGr.Visible = False
    cmdGuardarVistaNuevaGr.Visible = m_bModif
    cmdEliminarVistaGr.Visible = False
    cmdRenombrarVistaGr.Visible = False
    cmdGuardarVistaAll.Visible = False
    cmdGuardarVistaNuevaAll.Visible = m_bModif
    cmdEliminarVistaAll.Visible = False
    cmdRenombrarVistaAll.Visible = False
    cmdGuardarVistaNueva.Left = 2800
    cmdGuardarVistaNuevaGr.Left = 2800
    cmdGuardarVistaNuevaAll.Left = 2800
End If
End Sub

Private Sub CargarTabGeneral()
'Obtiene las vistas por defecto para el tab de proceso y el de ALL
Set m_oConfVistasProce = Nothing
Set m_oConfVistasProce = oFSGSRaiz.Generar_CPlantConfVistasProce
ConfiguracionVistaActual g_oPlantillaSeleccionada.VistaDefectoComp, True
g_oPlantillaSeleccionada.NombreVistaDefecto = m_oVistaSeleccionada.NombreVista
CargarTab
CargarGridGeneral
RedimensionarGrid
CargarVistasProc
CargarComboVistas
If Not g_oPlantillaSeleccionada Is Nothing Then
    Select Case g_oPlantillaSeleccionada.VistaDefectoComp
        Case TipoDeVistaDefecto.vistainicial
            If m_bGridProcVisible Then
                sdbgProce.caption = m_sVistaIni
                sdbgGruposProve.caption = ""
            Else
                sdbgGruposProve.caption = m_sVistaIni
                sdbgProce.caption = ""
            End If
            sdbgAll.caption = m_sVistaIni
            sdbgProveGrupos.caption = m_sVistaIni
            sdbgAdj.caption = m_sVistaIni
        Case Else
            If m_bGridProcVisible Then
                sdbgProce.caption = m_sVista & ": " & g_oPlantillaSeleccionada.NombreVistaDefecto
                sdbgGruposProve.caption = ""
            Else
                sdbgGruposProve.caption = m_sVista & ": " & g_oPlantillaSeleccionada.NombreVistaDefecto
                sdbgProce.caption = ""
            End If
            sdbgAll.caption = m_sVista & ": " & g_oPlantillaSeleccionada.NombreVistaDefecto
            sdbgProveGrupos.caption = m_sVista & ": " & g_oPlantillaSeleccionada.NombreVistaDefecto
            sdbgAdj.caption = m_sVista & ": " & g_oPlantillaSeleccionada.NombreVistaDefecto
    End Select
End If
'El tab ALL
If g_oPlantillaSeleccionada.Grupos.Count > 1 Then
    Set m_oConfVistasAll = Nothing
    Set m_oConfVistasAll = oFSGSRaiz.Generar_CPlantConfVistasALL
End If
End Sub

''' <summary>
''' Carga las configuraci�n de la vista que se pasa por par�metro para el usuario y el proceso seleccionado.
''' </summary>
''' <param name="iVista">Id vista</param>
''' <param name="bDefecto">Si es vista por defecto o no</param>
''' <remarks>Llamada desde: GeneralSeleccionado     CargarTabGeneral    sdbcVistaActual_CloseUp     cmdGuardarVistaNueva_Click
''' cmdEliminarVista_Click  ConfiguracionVistaActualAll     ConfiguracionVistaActualGr; Tiempo m�ximo: 0,2</remarks>
Private Sub ConfiguracionVistaActual(ByVal iVista As Variant, ByVal bDefecto As Boolean)
Dim oatrib As CPlantConfVistaProceAtrib
Dim oVistaInicial As CConfVistaProce
Dim PosSegunVinculada As Integer
Set m_oVistaSeleccionada = Nothing
If Not m_oVistaSeleccionadaGr Is Nothing Then If m_oVistaSeleccionadaGr.Vista <> iVista Then Set m_oVistaSeleccionadaGr = Nothing
If Not m_oVistaSeleccionadaAll Is Nothing Then If m_oVistaSeleccionadaAll.Vista <> iVista Then Set m_oVistaSeleccionadaAll = Nothing
'Si no se ha cargado la vista en la colecci�n todav�a se carga
If Not IsNull(iVista) Then If m_oConfVistasProce.Item(CStr(iVista)) Is Nothing Then m_oConfVistasProce.CargarTodasLasVistas g_oPlantillaSeleccionada, iVista
If g_oPlantillaSeleccionada.Solicitud = EnItem Then
    PosSegunVinculada = 11
Else
    PosSegunVinculada = 13
End If
'Si no hay datos en BD para esa vista,o no hab�a vista por defecto para el proceso se a�ade
'a la colecci�n con los valores de la vista inicial para ese usuario
If IsNull(iVista) Then
    Set m_oVistasInicial = Nothing
    Set m_oVistasInicial = oFSGSRaiz.Generar_CConfVistasProce
    m_oVistasInicial.CargarVistaInicial oUsuarioSummit.Cod
    If m_oVistasInicial.Item("00") Is Nothing Then
        'Si no existe la vista inicial carga unos valores por defecto:
        m_oConfVistasProce.Add g_oPlantillaSeleccionada, 0, , 1, True, 850, 2, False, 850, 3, True, 850, 4, False, 850, 5, True, 850, 6, False, 850, 7, False, 850, 0, 2600, , , ANCHO_FILA_GEN, 2700, 945, 1755, , 0, 0, 0, 0, 2, 2, PosSegunVinculada, False, 400
    Else
        Set oVistaInicial = m_oVistasInicial.Item("00")
        'Carga en la vista por defecto los valores de la vista inicial:
        m_oConfVistasProce.Add g_oPlantillaSeleccionada, 0, , oVistaInicial.ConsumidoPos, oVistaInicial.ConsumidoVisible, oVistaInicial.ConsumidoWidth, oVistaInicial.ImportePos, oVistaInicial.ImporteVisible, oVistaInicial.ImporteWidth, oVistaInicial.AdjPos, oVistaInicial.AdjVisible, oVistaInicial.AdjWidth, oVistaInicial.AhorroAdjPorcenPos, oVistaInicial.AhorroAdjPorcenVisible, oVistaInicial.AhorroAdjPorcenWidth, oVistaInicial.AhorroAdjPos, oVistaInicial.AhorroAdjVisible, oVistaInicial.AhorroAdjWidth, oVistaInicial.AhorroOfePorcenPos, oVistaInicial.AhorroOfePorcenVisible, oVistaInicial.AhorroOfePorcenWidth, oVistaInicial.AhorroOfePos, oVistaInicial.AhorroOfeVisible, oVistaInicial.AhorroOfeWidth, oVistaInicial.TipoVision, oVistaInicial.GrupoWidth, , , oVistaInicial.AnchoFila, oVistaInicial.Grupo0Width, oVistaInicial.codWidth, oVistaInicial.DenWidth, , oVistaInicial.OcultarGrCerrados, oVistaInicial.ExcluirCerradosResult, oVistaInicial.OcultarNoAdj, oVistaInicial.OcultarProvSinOfe, _
        oVistaInicial.DecResult, oVistaInicial.DecPorcen, PosSegunVinculada, False, 400
        Set oVistaInicial = Nothing
    End If
    Set m_oVistaSeleccionada = m_oConfVistasProce.Item(CStr("0"))
ElseIf m_oConfVistasProce.Item(CStr(iVista)) Is Nothing Then
    If m_oVistasInicial Is Nothing Then
        Set m_oVistasInicial = oFSGSRaiz.Generar_CConfVistasProce
        m_oVistasInicial.CargarVistaInicial oUsuarioSummit.Cod
    End If
    If m_oVistasInicial.Item("00") Is Nothing Then
        m_oConfVistasProce.Add g_oPlantillaSeleccionada, iVista, , 1, True, 850, 2, False, 850, 3, True, 850, 4, False, 850, 5, True, 850, 6, False, 850, 7, False, 850, 0, 2600, , , ANCHO_FILA_GEN, 2700, 945, 1755, , 0, 0, 0, 0, 2, 2, PosSegunVinculada, False, 400
    Else
        Set oVistaInicial = m_oVistasInicial.Item("00")
        'Carga en la vista por defecto los valores de la vista inicial:
        m_oConfVistasProce.Add g_oPlantillaSeleccionada, iVista, , oVistaInicial.ConsumidoPos, oVistaInicial.ConsumidoVisible, oVistaInicial.ConsumidoWidth, oVistaInicial.ImportePos, oVistaInicial.ImporteVisible, oVistaInicial.ImporteWidth, oVistaInicial.AdjPos, oVistaInicial.AdjVisible, oVistaInicial.AdjWidth, oVistaInicial.AhorroAdjPorcenPos, oVistaInicial.AhorroAdjPorcenVisible, oVistaInicial.AhorroAdjPorcenWidth, oVistaInicial.AhorroAdjPos, oVistaInicial.AhorroAdjVisible, oVistaInicial.AhorroAdjWidth, oVistaInicial.AhorroOfePorcenPos, oVistaInicial.AhorroOfePorcenVisible, oVistaInicial.AhorroOfePorcenWidth, oVistaInicial.AhorroOfePos, oVistaInicial.AhorroOfeVisible, oVistaInicial.AhorroOfeWidth, oVistaInicial.TipoVision, oVistaInicial.GrupoWidth, , , oVistaInicial.AnchoFila, oVistaInicial.Grupo0Width, oVistaInicial.codWidth, oVistaInicial.DenWidth, , oVistaInicial.OcultarGrCerrados, oVistaInicial.ExcluirCerradosResult, oVistaInicial.OcultarNoAdj, oVistaInicial.OcultarProvSinOfe, _
        oVistaInicial.DecResult, oVistaInicial.DecPorcen, PosSegunVinculada, False, 400
        Set oVistaInicial = Nothing
    End If
    Set m_oVistaSeleccionada = m_oConfVistasProce.Item(CStr(iVista))
Else
    Set m_oVistaSeleccionada = m_oConfVistasProce.Item(CStr(iVista))
End If
'Carga las configuraciones de los atributos
If m_oVistaSeleccionada.PlantConfVistasProceAtrib Is Nothing Then
    m_oVistaSeleccionada.CargarConfAtributos
    'Si se carga un atributo en la vistas que no existe en la colecci�n de atributos
    'lo elimina de la colecci�n
    For Each oatrib In m_oVistaSeleccionada.PlantConfVistasProceAtrib
        If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
            If g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)) Is Nothing Then
                m_oVistaSeleccionada.PlantConfVistasProceAtrib.Remove (CStr(oatrib.Atributo))
            ElseIf g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).ambito = AmbItem Then
                m_oVistaSeleccionada.PlantConfVistasProceAtrib.Remove (CStr(oatrib.Atributo))
            End If
        End If
    Next
End If
'Carga las configuraciones de los sobres
If g_oPlantillaSeleccionada.AdminPub = True Then If m_oVistaSeleccionada.PlantConfVistasProceSobre Is Nothing Then m_oVistaSeleccionada.CargarConfSobres
End Sub

Private Sub ConfiguracionVistaActualGr(ByVal iVista As Variant, ByVal bDefecto As Boolean)
Dim oatrib As CPlantConfVistaGrupoAtrib
    'Carga las configuraci�n de la vista que se pasa por par�metro para el usuario y
    'el grupo seleccionado
    Set m_oVistaSeleccionadaGr = Nothing
    If Not m_oVistaSeleccionadaAll Is Nothing Then If m_oVistaSeleccionadaAll.Vista <> iVista Then Set m_oVistaSeleccionadaAll = Nothing
    If iVista <> 0 Then  'Si no se ha cargado la vista en la colecci�n todav�a se carga
        If m_oGrupoSeleccionado.PlantConfVistas Is Nothing Then Set m_oGrupoSeleccionado.PlantConfVistas = oFSGSRaiz.Generar_CPlantConfVistasGrupo
        If m_oGrupoSeleccionado.PlantConfVistas.Item(CStr(iVista)) Is Nothing Then m_oGrupoSeleccionado.CargarTodasLasConfVistasPlantilla g_oPlantillaSeleccionada, iVista
    End If
    'Si no hay datos en BD para esa vista,o no hab�a vista por defecto para la plantilla se a�ade
    'a la colecci�n y se inserta en BD con los valores de la vista inicial para ese usuario:
    If iVista = 0 Then
        'Carga la vista por defecto:
        If m_oGrupoSeleccionado.PlantConfVistas Is Nothing Then Set m_oGrupoSeleccionado.PlantConfVistas = oFSGSRaiz.Generar_CPlantConfVistasGrupo
        Set m_oVistasInicialGR = Nothing
        Set m_oVistasInicialGR = oFSGSRaiz.Generar_CConfVistasGrupo
        m_oVistasInicialGR.CargarVistaInicial oUsuarioSummit.Cod
        If m_oGrupoSeleccionado.PlantConfVistas.Item("0") Is Nothing Then
            If m_oVistasInicialGR.Item("00") Is Nothing Then
                CargarDatosDefectoEnVista "GR", 0, 0, False
            Else
                CargarDatosDefectoEnVista "GR", 0, 0, True
            End If
        End If
        Set m_oVistaSeleccionadaGr = m_oGrupoSeleccionado.PlantConfVistas.Item("0")
    ElseIf m_oGrupoSeleccionado.PlantConfVistas.Item(CStr(iVista)) Is Nothing Then
        If m_oVistasInicialGR Is Nothing Then
            Set m_oVistasInicialGR = oFSGSRaiz.Generar_CConfVistasGrupo
            m_oVistasInicialGR.CargarVistaInicial oUsuarioSummit.Cod
        End If
        If m_oVistasInicialGR.Item("00") Is Nothing Then
            CargarDatosDefectoEnVista "GR", iVista, BooleanToSQLBinary(False), False
        Else
            CargarDatosDefectoEnVista "GR", iVista, BooleanToSQLBinary(False), True
        End If
        Set m_oVistaSeleccionadaGr = m_oGrupoSeleccionado.PlantConfVistas.Item(CStr(iVista))
    Else
        Set m_oVistaSeleccionadaGr = m_oGrupoSeleccionado.PlantConfVistas.Item(CStr(iVista))
    End If
    'Carga las configuraciones de los atributos
    If m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib Is Nothing Then
        m_oVistaSeleccionadaGr.CargarConfAtributos
        'Si se carga un atributo en la vistas que no existe en la colecci�n de atributos
        'lo elimina de la colecci�n
        For Each oatrib In m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib
            If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
                If g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)) Is Nothing Then
                    m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Remove (CStr(oatrib.Atributo))
                ElseIf Not (g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).ambito = AmbItem And _
                        (g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo = m_oGrupoSeleccionado.Codigo Or _
                        IsNull(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(oatrib.Atributo)).codgrupo))) Then
                    m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Remove (CStr(oatrib.Atributo))
                End If
            End If
        Next
    End If
    m_oVistaSeleccionadaGr.HayCambios = False
    If m_oVistaSeleccionada.Vista <> iVista Then
        ConfiguracionVistaActual iVista, False
        CargarVistasProc
    End If
End Sub

''' <summary>
''' Oculta o hace visible un campo del grid
''' </summary>
''' <param name="Campo">Campo</param>
''' <param name="bVisible">Visible o no</param>
''' <remarks>Llamada desde: sdbgOcultarCampos_Change ; Tiempo m�ximo: 0,2</remarks>
Private Sub OcultarCampo(ByVal Campo As String, bVisible As Boolean)
Dim i As Integer
Dim bCamposVisibles As Boolean
Dim iVisible As Integer
Dim j As Integer
Dim oatrib As CAtributo
Dim bAtribRepetido As Boolean
Dim bColVisible As Boolean
Dim iVisibleInvertida As Integer
Dim bProporcional As Boolean
Dim iNumColVisibles As Integer
Dim sCol As String
Dim bNormal As Boolean
Dim bControlar As Boolean
Dim oGrupo As CGrupo
Dim bCasoVincPro As Boolean
Dim lIndex As Integer
With m_oVistaSeleccionada
    .HayCambios = True
    Select Case Campo
        Case "Consumido"
            .ConsumidoVisible = bVisible
        Case "Adjudicado"
            .AdjVisible = bVisible
        Case "Ahorrado"
            .AhorroAdjVisible = bVisible
        Case "Ahorro%"
            .AhorroAdjPorcenVisible = bVisible
        Case "Importe"
            .ImporteVisible = bVisible
        Case "AhorroOferta"
            .AhorroOfeVisible = bVisible
        Case "AhorroOfe%"
            .AhorroOfePorcenVisible = bVisible
        Case "SolicVinculadaProce", "SolicVinculadaGrupo"
            .SolicVinculadaVisible = bVisible
        Case Else  'Es un atributo
            .PlantConfVistasProceAtrib.Item(Campo).Visible = bVisible
    End Select
    'Calcula el n�mero de columnas que van a estar visibles en las grids de totales.Lo hacemos as�,porque sino
    'la grid muestra las columnas con diferentes tama�os
    iVisible = 1  'la de adjudicado siempre estar� visible
    iVisible = iVisible + BooleanToSQLBinary(.ImporteVisible)
    iVisible = iVisible + BooleanToSQLBinary(.AhorroOfeVisible)
    iVisible = iVisible + BooleanToSQLBinary(.ConsumidoVisible)
    iVisible = iVisible + BooleanToSQLBinary(.AhorroAdjVisible)
    iVisibleInvertida = 1
    iVisibleInvertida = iVisibleInvertida + BooleanToSQLBinary(.ConsumidoVisible)
    iVisibleInvertida = iVisibleInvertida + BooleanToSQLBinary(.AhorroAdjVisible)
    bControlar = False
    If Campo = "SolicVinculadaProce" Then
        bControlar = True
        For i = 1 To sdbgProce.Groups.Count - 1
            sdbgProce.Columns("VINCULADO_PRO" & sdbgProce.Groups(i).TagVariant).Visible = bVisible
        Next i
        sdbgProveGrupos.Columns("VINCULADO_PRO").Visible = bVisible
    End If
    'Si es un atributo a nivel de proceso: habr� que ocultar o no el campo y ocultar o mostrar la grid
    'de procesos
    If Not .PlantConfVistasProceAtrib.Item(Campo) Is Nothing Then
        If .PlantConfVistasProceAtrib.Item(Campo).ambito = TipoAmbitoProceso.AmbProceso Then
            bControlar = True
            For i = 1 To sdbgProce.Groups.Count - 1
                sdbgProce.Columns(Campo & sdbgProce.Groups(i).TagVariant).Visible = bVisible
            Next i
            sdbgProveGrupos.Columns(Campo).Visible = bVisible
        End If
    End If
    If bControlar Then
        'comprueba si se muestra o no la grid de procesos:
        bCamposVisibles = False
        For j = 1 To sdbgProce.Groups.Count - 1
            If sdbgProce.Groups(j).Visible = True Then Exit For
        Next j
        For i = 0 To sdbgProce.Groups(j).Columns.Count - 1
            If sdbgProce.Groups(j).Columns(i).Visible = True Then
                bCamposVisibles = True
                Exit For
            End If
        Next i
        If bCamposVisibles = False Then
            m_bGridProcVisible = False
            If sdbgProce.Visible = True Then
                sdbgProce.Visible = False
                sdbgGruposProve.caption = sdbgProce.caption
                sdbgProce.caption = ""
            End If
            sdbgProveGrupos.Groups(1).Visible = False
            sdbgtotales2.Groups(1).Visible = False
            sdbgGruposProve.GroupHeaders = True
            sdbgGruposProve.Top = 855
            sdbgGruposProve.Height = sstabComparativa.Height - sdbgTotalesProve.Height - 520
        Else
            m_bGridProcVisible = True
            If .TipoVision = 0 Then
                If sdbgProce.Visible = False Then
                    sdbgProce.Visible = True
                    sdbgProce.caption = sdbgGruposProve.caption
                    sdbgGruposProve.caption = ""
                End If
                'Muestra visibles s�lo los grupos que est�n visibles
                For i = 1 To sdbgProce.Groups.Count - 1
                    If sdbgGruposProve.Groups(i).Visible = True Then
                        sdbgProce.Groups(i).Visible = True
                        sdbgProce.Groups(i).Width = sdbgGruposProve.Groups(i).Width
                    Else
                        sdbgProce.Groups(i).Visible = False
                    End If
                Next i
            Else
                sdbgProce.Visible = False
            End If
            sdbgProce.Top = 855
            sdbgProveGrupos.Groups(1).Visible = True
            sdbgtotales2.Groups(1).Visible = True
            sdbgGruposProve.Top = sdbgProce.Top + sdbgProce.Height
            sdbgGruposProve.GroupHeaders = False
            sdbgProce.Groups(0).Width = sdbgGruposProve.Groups(0).Width
            'si est� visible la grid de atributos de proceso
            'Comprueba si los atributos ten�an tama�os definidos en BD,sino el tama�o se reparte proporcionalmente entre los que est�n visibles
            bProporcional = True
            iNumColVisibles = 0
            For i = 0 To sdbgProce.Groups(1).Columns.Count - 1
                sCol = Mid(sdbgProce.Groups(1).Columns(i).Name, 1, Len(sdbgProce.Groups(1).Columns(i).Name) - Len(sdbgProce.Groups(1).TagVariant))
                If Not .PlantConfVistasProceAtrib.Item(CStr(sCol)) Is Nothing Then If .PlantConfVistasProceAtrib.Item(CStr(sCol)).Width <> 0 Then bProporcional = False
                If sdbgProce.Groups(1).Columns(i).Visible = True Then iNumColVisibles = iNumColVisibles + 1
            Next i
            If bProporcional = True And iNumColVisibles <> 0 Then
                For i = 1 To sdbgProce.Groups.Count - 1
                    For j = 0 To sdbgProce.Groups(i).Columns.Count - 1
                        sdbgProce.Groups(i).Columns(j).Width = sdbgProce.Groups(i).Width / iNumColVisibles
                    Next j
                Next i
                bCasoVincPro = False
                For i = 0 To sdbgProveGrupos.Groups(1).Columns.Count - 1
                    sdbgProveGrupos.Groups(1).Columns(i).Width = sdbgProveGrupos.Groups(1).Width / iNumColVisibles
                    If sdbgProveGrupos.Groups(1).Columns(i).Name = "VINCULADO_PRO" Then bCasoVincPro = True
                Next i
                If bCasoVincPro And (cmdGuardarVista.Visible = False) Then
                    .GrupoWidth = sdbgProveGrupos.Groups(1).Width
                    For i = 0 To sdbgProveGrupos.Groups(1).Columns.Count - 1
                        lIndex = sdbgProveGrupos.Groups(1).ColPosition(i)
                        If sdbgProveGrupos.Groups(1).Columns(lIndex).Name = "VINCULADO_PRO" Then
                            If .SolicVinculadaVisible Then .SolicVinculadaWidth = sdbgProveGrupos.Columns("VINCULADO_PRO").Width
                            .SolicVinculadaPos = sdbgProveGrupos.Columns("VINCULADO_PRO").Position - 2
                        Else
                            If sdbgProveGrupos.Columns(sdbgProveGrupos.Groups(1).Columns(lIndex).Name).Visible Then .PlantConfVistasProceAtrib.Item(sdbgProveGrupos.Groups(1).Columns(lIndex).Name).Width = sdbgProveGrupos.Columns(sdbgProveGrupos.Groups(1).Columns(lIndex).Name).Width
                            .PlantConfVistasProceAtrib.Item(sdbgProveGrupos.Groups(1).Columns(lIndex).Name).Posicion = sdbgProveGrupos.Columns(sdbgProveGrupos.Groups(1).Columns(lIndex).Name).Position - 2
                        End If
                    Next
                End If
            End If
        End If
        Exit Sub
    End If
    If Campo = "SolicVinculadaGrupo" Then
        For i = 1 To sdbgGruposProve.Groups.Count - 1
            sdbgGruposProve.Columns("VINCULADO_GRU" & sdbgGruposProve.Groups(i).TagVariant).Visible = bVisible
        Next i
        For Each oGrupo In g_oPlantillaSeleccionada.Grupos
            sdbgProveGrupos.Columns("VINCULADO_GRU" & oGrupo.Codigo).Visible = bVisible
        Next
        Exit Sub
    End If
    bNormal = False
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        If Not g_oPlantillaSeleccionada.Atributosvistas.Item(Campo) Is Nothing Then
            If g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).ambito = AmbGrupo Then
                'Es un atributo de grupo
                'Comprueba si �ste atributo est� definido en m�s grupos. y si estar� visible para el resto de grupos
                bAtribRepetido = False
                bColVisible = .PlantConfVistasProceAtrib.Item(CStr(Campo)).Visible
                For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
                    If oatrib.ambito = AmbGrupo Then
                        If oatrib.idAtribProce <> Campo Then
                            If oatrib.Cod = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(Campo)).Cod Then
                                bAtribRepetido = True
                                If .PlantConfVistasProceAtrib.Item(CStr(oatrib.idAtribProce)).Visible = True Then
                                    bColVisible = True
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                Next
                'Comprueba si el atributo est� visible para el resto de los grupos:
                If bAtribRepetido = True Then
                    If bVisible = True Then
                        'Visualiza el campo y quita el gris del campo.
                        For j = 0 To sdbgGruposProve.Rows - 1
                            sdbgGruposProve.Bookmark = j
                            For i = 1 To sdbgGruposProve.Groups.Count - 1
                                sdbgGruposProve.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(Campo)).Cod & sdbgGruposProve.Groups(i).TagVariant).Visible = bColVisible
                                If sdbgGruposProve.Columns("COD").Value = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(Campo)).codgrupo Then sdbgGruposProve.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgGruposProve.Groups(i).TagVariant).CellStyleSet "Normal", j
                            Next i
                        Next j
                        For j = 0 To sdbgProveGrupos.Rows - 1
                            sdbgProveGrupos.Bookmark = j
                            For i = 2 To sdbgProveGrupos.Groups.Count - 1
                                sdbgProveGrupos.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgProveGrupos.Groups(i).TagVariant).Visible = bColVisible
                                If sdbgProveGrupos.Groups(i).TagVariant = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(Campo)).codgrupo Then sdbgProveGrupos.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgProveGrupos.Groups(i).TagVariant).CellStyleSet "Normal", j
                            Next i
                        Next j
                    Else
                        'El atributo no estar� visible,pone en gris el campo
                        For j = 0 To sdbgGruposProve.Rows - 1
                            sdbgGruposProve.Bookmark = j
                            For i = 1 To sdbgGruposProve.Groups.Count - 1
                                sdbgGruposProve.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgGruposProve.Groups(i).TagVariant).Visible = bColVisible
                                If sdbgGruposProve.Columns("COD").Value = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(Campo)).codgrupo Then sdbgGruposProve.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgGruposProve.Groups(i).TagVariant).CellStyleSet "GrayHead", j
                            Next i
                        Next j
                        For j = 0 To sdbgProveGrupos.Rows - 1
                            sdbgProveGrupos.Bookmark = j
                            For i = 2 To sdbgProveGrupos.Groups.Count - 1
                                sdbgProveGrupos.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgProveGrupos.Groups(i).TagVariant).Visible = bColVisible
                                If sdbgProveGrupos.Groups(i).TagVariant = g_oPlantillaSeleccionada.Atributosvistas.Item(CStr(Campo)).codgrupo Then sdbgProveGrupos.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgProveGrupos.Groups(i).TagVariant).CellStyleSet "GrayHead", j
                            Next i
                        Next j
                    End If
                Else
                    'Es un atributo normal,no est� repetido en varios grupos
                    For i = 1 To sdbgGruposProve.Groups.Count - 1
                        sdbgGruposProve.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgGruposProve.Groups(i).TagVariant).Visible = bVisible
                    Next i
                    For i = 2 To sdbgProveGrupos.Groups.Count - 1
                        sdbgProveGrupos.Columns(g_oPlantillaSeleccionada.Atributosvistas.Item(Campo).Cod & sdbgProveGrupos.Groups(i).TagVariant).Visible = bVisible
                    Next i
                End If
            Else
                bNormal = True
            End If
        Else
            bNormal = True
        End If
    Else
        bNormal = True
    End If
    If bNormal = True Then
        'Redimensiona sdbgGruposProve
        For i = 1 To sdbgGruposProve.Groups.Count - 1
            sdbgGruposProve.Columns(Campo & sdbgGruposProve.Groups(i).TagVariant).Visible = bVisible
            If Campo = "Importe" Or Campo = "AhorroOferta" Or Campo = "Consumido" Or Campo = "Ahorrado" Then sdbgTotalesProve.Columns(Campo & sdbgTotalesProve.Groups(i).TagVariant).Visible = bVisible
            'Pone el mismo tama�o a todas las columnas visibles de la grid de totales
            For j = 0 To sdbgTotalesProve.Groups(i).Columns.Count - 1
                sdbgTotalesProve.Groups(i).Columns(j).Width = sdbgTotalesProve.Groups(i).Width / iVisible
            Next j
        Next i
        'Redimensiona sdbgProveGrupos
        For i = 2 To sdbgProveGrupos.Groups.Count - 1
            sdbgProveGrupos.Columns(Campo & sdbgProveGrupos.Groups(i).TagVariant).Visible = bVisible
            If Campo = "Consumido" Or Campo = "Ahorrado" Then sdbgtotales2.Columns(Campo & sdbgtotales2.Groups(i).TagVariant).Visible = bVisible
            'Pone el mismo tama�o a todas las columnas visibles de la grid de totales
            For j = 0 To sdbgtotales2.Groups(i).Columns.Count - 1
                sdbgtotales2.Groups(i).Columns(j).Width = sdbgtotales2.Groups(i).Width / iVisibleInvertida
            Next j
        Next i
    End If
    sdbgGruposProve.MoveFirst
    sdbgProveGrupos.MoveFirst
End With
End Sub

''' <summary>Oculta o hace visible un campo del grid</summary>
''' <param name="Campo">Campo a ocultar o hacer visible</param>
''' <param name="bVisible">visibilidad del campo</param>
''' <remarks>Llamada desde: sdbgOcultarCamposAll_Change; Tiempo m�ximo:0</remarks>
Private Sub OcultarCampoAll(ByVal Campo As String, ByVal bVisible As Boolean)
Dim i As Integer
With m_oVistaSeleccionadaAll
    .HayCambios = True
    Select Case Campo
        Case "ANYO"
            .AnyoVisible = bVisible
            sdbgAll.Columns("ANYO").Visible = bVisible
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("ANYO").Position = .AnyoPos
        Case "PROV"
            .ProveedorVisible = bVisible
            sdbgAll.Columns("PROV").Visible = bVisible
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("PROV").Position = .ProveedorPos
        Case "PRECAPE"
            .PresUniVisible = bVisible
            sdbgAll.Columns("PRECAPE").Visible = bVisible
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("PRECAPE").Position = .PresUniPos
        Case "OBJ"
            .OBJVisible = bVisible
            sdbgAll.Columns("OBJ").Visible = bVisible
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("OBJ").Position = .OBJPos
        Case "AHORROIMP"
            .AhorroVisible = bVisible
            sdbgAll.Columns("AHORROIMP").Visible = bVisible
            If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns("AHORROIMP").Level = 1
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("AHORROIMP").Position = .AhorroPos
        Case "AHORROPORCEN"
            .AhorroPorcenVisible = bVisible
            sdbgAll.Columns("AHORROPORCEN").Visible = bVisible
            If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns("AHORROPORCEN").Level = 1
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("AHORROPORCEN").Position = .AhorroPorcenPos
        Case "IMP"
            .ImporteVisible = bVisible
            sdbgAll.Columns("IMP").Visible = bVisible
            If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns("IMP").Level = 1
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("IMP").Position = .ImportePos
        Case "CANT"
            .CantidadVisible = bVisible
            sdbgAll.Columns("CANT").Visible = bVisible
            If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns("CANT").Level = 1
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("CANT").Position = .CantidadPos
        Case "PORCENADJ"
            .AdjVisible = bVisible
            sdbgAll.Columns("PORCENADJ").Visible = bVisible
            If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns("PORCENADJ").Level = 1
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("PORCENADJ").Position = .AdjPos
        Case "VINCULADO_ITEM"
            .SolicVinculadaVisible = bVisible
            sdbgAll.Columns("VINCULADO_ITEM").Visible = bVisible
            If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns("VINCULADO_ITEM").Level = 1
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("VINCULADO_ITEM").Position = .SolicVinculadaPos
        Case "IMPADJ"  'El importe de la adjudicaci�n
            .ImpAdjVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAll.Groups.Count - 1
                sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Visible = bVisible
                If .ImpAdjWidth = 0 Then sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Width = 500
            Next i
        Case "Precio"
            .PrecioProvVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAll.Groups.Count - 1
                sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Visible = bVisible
                If .PrecioProvWidth = 0 Then sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Width = 500
            Next i
        Case "AdjPorcen"
            .AdjProvVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAll.Groups.Count - 1
                sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Visible = bVisible
                If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Level = 1
                If .AdjProvWidth = 0 Then sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Width = 500
            Next i
        Case "AdjCantidad"
            .CantProvVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAll.Groups.Count - 1
                sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Visible = bVisible
                If bVisible = True And sdbgAll.LevelCount = 3 Then sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Level = 1
                If .CantProvWidth = 0 Then sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Width = 500
            Next i
        Case "INI"
            .FecIniSumVisible = bVisible
            sdbgAll.Columns("INI").Visible = bVisible
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("INI").Position = .FecIniSumPos
        Case "FIN"
            .FecFinSumVisible = bVisible
            sdbgAll.Columns("FIN").Visible = bVisible
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("FIN").Position = .FecFinSumPos
        Case "GMN"
            .EstrMatVisible = bVisible
            sdbgAll.Columns("GMN").Visible = bVisible
            If sdbgAll.GrpPosition(0) = 0 Then sdbgAll.Columns("GMN").Position = .EstrMatPos
        Case Else  'Es un atributo
            .PlantConfVistasAllAtrib.Item(Campo).Visible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAll.Groups.Count - 1
                sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Visible = bVisible
                If .PlantConfVistasAllAtrib.Item(Campo).Width = 0 Then sdbgAll.Columns(Campo & sdbgAll.Groups(i).TagVariant).Width = 500
            Next i
    End Select
    'Columna de Total de objetivos
    If sdbgTotalesAll.Rows > 0 And sdbgAll.Groups.Count > 2 Then If sdbgAll.Groups(2).Visible = True Then sdbgTotalesAll.SplitterPos = 2
    If sdbgAll.Columns("OBJ").Visible = False Then
        sdbgTotalesAll.Columns("OBJTOT").caption = ""
        sdbgTotalesAll.MoveFirst
    Else
        If sdbgAll.Columns("OBJ").Width > 1000 Then
            sdbgTotalesAll.Columns("OBJTOT").Width = sdbgAll.Columns("OBJ").Width
            sdbgTotalesAll.Columns(0).Width = sdbgAll.Groups(0).Width - sdbgAll.Columns("OBJ").Width
        Else
            sdbgTotalesAll.Columns("OBJTOT").Width = 1000
            sdbgTotalesAll.Columns(0).Width = sdbgAll.Groups(0).Width - 1000
        End If
        sdbgTotalesAll.Columns("OBJTOT").caption = sCaptionTotales
        sdbgTotalesAll.MoveFirst
    End If
    If sdbgAll.Visible Then sdbgAll.SetFocus
    sdbgAll.MoveFirst
End With
End Sub

''' <summary>Oculta o hace visible un campo del grid</summary>
''' <param name="Campo">Campo a ocultar o hacer visible</param>
''' <param name="bVisible">visibilidad del campo</param>
''' <remarks>Llamada desde: sdbgOcultarCamposGr_Change; Tiempo m�ximo:0</remarks>
Private Sub OcultarCampoGr(ByVal Campo As String, ByVal bVisible As Boolean)
Dim i As Integer
With m_oVistaSeleccionadaGr
    .HayCambios = True
    Select Case Campo
        Case "ANYO"
            .AnyoVisible = bVisible
            sdbgAdj.Columns("ANYO").Visible = bVisible
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("ANYO").Position = .AnyoPos
        Case "PROV"
            .ProveedorVisible = bVisible
            sdbgAdj.Columns("PROV").Visible = bVisible
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("PROV").Position = .ProveedorPos
        Case "PRECAPE"
            .PresUniVisible = bVisible
            sdbgAdj.Columns("PRECAPE").Visible = bVisible
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("PRECAPE").Position = .PresUniPos
        Case "OBJ"
            .OBJVisible = bVisible
            sdbgAdj.Columns("OBJ").Visible = bVisible
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("OBJ").Position = .OBJPos
        Case "AHORROIMP"
            .AhorroVisible = bVisible
            sdbgAdj.Columns("AHORROIMP").Visible = bVisible
            If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns("AHORROIMP").Level = 1
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("AHORROIMP").Position = .AhorroPos
        Case "AHORROPORCEN"
            .AhorroPorcenVisible = bVisible
            sdbgAdj.Columns("AHORROPORCEN").Visible = bVisible
            If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns("AHORROPORCEN").Level = 1
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("AHORROPORCEN").Position = .AhorroPorcenPos
        Case "IMP"
            .ImporteVisible = bVisible
            sdbgAdj.Columns("IMP").Visible = bVisible
            If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns("IMP").Level = 1
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("IMP").Position = .ImportePos
        Case "CANT"
            .CantidadVisible = bVisible
            sdbgAdj.Columns("CANT").Visible = bVisible
            If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns("CANT").Level = 1
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("CANT").Position = .CantidadPos
        Case "PORCENADJ"
            .AdjVisible = bVisible
            sdbgAdj.Columns("PORCENADJ").Visible = bVisible
            If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns("PORCENADJ").Level = 1
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("PORCENADJ").Position = .AdjPos
        Case "VINCULADO_ITEM"
            .SolicVinculadaVisible = bVisible
            sdbgAdj.Columns("VINCULADO_ITEM").Visible = bVisible
            If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns("VINCULADO_ITEM").Level = 1
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("VINCULADO_ITEM").Position = .SolicVinculadaPos
        Case "IMPADJ"  'El importe de la adjudicaci�n
            .ImpAdjVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAdj.Groups.Count - 1
                sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Visible = bVisible
                If .ImpAdjWidth = 0 Then sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Width = 500
            Next i
        Case "Precio"
            .PrecioProvVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAdj.Groups.Count - 1
                sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Visible = bVisible
                If .PrecioProvWidth = 0 Then sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Width = 500
            Next i
        Case "AdjPorcen"
            .AdjProvVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAdj.Groups.Count - 1
                sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Visible = bVisible
                If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Level = 1
                If .AdjProvWidth = 0 Then sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Width = 500
            Next i
        Case "AdjCantidad"
            .CantProvVisible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAdj.Groups.Count - 1
                sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Visible = bVisible
                If bVisible = True And sdbgAdj.LevelCount = 3 Then sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Level = 1
                If .CantProvWidth = 0 Then sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Width = 500
            Next i
        Case "INI"
            .FecIniSumVisible = bVisible
            sdbgAdj.Columns("INI").Visible = bVisible
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("INI").Position = .FecIniSumPos
        Case "FIN"
            .FecFinSumVisible = bVisible
            sdbgAdj.Columns("FIN").Visible = bVisible
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("FIN").Position = .FecFinSumPos
        Case "GMN"
            .EstrMatVisible = bVisible
            sdbgAdj.Columns("GMN").Visible = bVisible
            If sdbgAdj.GrpPosition(0) = 0 Then sdbgAdj.Columns("GMN").Position = .EstrMatPos
        Case Else  'Es un atributo
            .PlantConfVistasGrAtrib.Item(Campo).Visible = bVisible
            'Lo oculta o hace visible en las grids
            For i = 2 To sdbgAdj.Groups.Count - 1
                sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Visible = bVisible
                If .PlantConfVistasGrAtrib.Item(Campo).Width = 0 Then sdbgAdj.Columns(Campo & sdbgAdj.Groups(i).TagVariant).Width = 500
            Next i
    End Select
    'Columna de Total de objetivos
    If sdbgTotales.Rows > 0 And sdbgAdj.Groups.Count > 2 Then
        If sdbgAdj.Groups(2).Visible = True Then sdbgTotales.SplitterPos = 2
    End If
    If sdbgAdj.Columns("OBJ").Visible = False Then
        sdbgTotales.Columns("OBJTOT").caption = ""
        sdbgTotales.MoveFirst
    Else
        If sdbgAdj.Columns("OBJ").Width > 1000 Then
            sdbgTotales.Columns("OBJTOT").Width = sdbgAdj.Columns("OBJ").Width
            sdbgTotales.Columns(0).Width = sdbgAdj.Groups(0).Width - sdbgAdj.Columns("OBJ").Width
        Else
            sdbgTotales.Columns("OBJTOT").Width = 1000
            sdbgTotales.Columns(0).Width = sdbgAdj.Groups(0).Width - 1000
        End If
        sdbgTotales.Columns("OBJTOT").caption = sCaptionTotales
        sdbgTotales.MoveFirst
    End If
    If sdbgAdj.Visible Then sdbgAdj.SetFocus
    sdbgAdj.MoveFirst
End With
End Sub

Private Sub GrupoSeleccionado()
Dim sVista As String
'Nos posicionamos en la pesta�a correspondiente a un grupo
ConfiguracionVistaActualGr m_oVistaSeleccionada.Vista, False
'Carga las combos de vista por defecto y vista actual
Select Case g_oPlantillaSeleccionada.VistaDefectoComp
    Case TipoDeVistaDefecto.vistainicial
        sdbcVistaDefectoGr.Text = m_sVistaIni
    Case Else
        sdbcVistaDefectoGr.Text = g_oPlantillaSeleccionada.NombreVistaDefecto
End Select
Select Case m_oVistaSeleccionada.Vista
    Case TipoDeVistaDefecto.vistainicial
        sdbcVistaActGr.Text = m_sVistaIni
        sVista = m_sVistaIni
    Case Else
        sdbcVistaActGr.Text = m_oVistaSeleccionada.NombreVista
        sVista = m_sVista & ": " & m_oVistaSeleccionada.NombreVista
End Select
sdbcVistaDefectoGr.Columns("COD").Value = CStr(g_oPlantillaSeleccionada.VistaDefectoComp)
sdbcVistaActGr.Columns("COD").Value = CStr(m_oVistaSeleccionada.Vista)
sdbgAdj.caption = sVista
CargarGridGrupo
RedimensionarGridGrupo
'Carga en los paneles la configuraci�n de la vista actual
CargarVistasGrupo
MostrarSplit
End Sub

''' <summary>Carga los datos en el grid de configuraci�n de vistas de adjudicaciones</summary>
''' <remarks>Llamada desde: GrupoSeleccionado</remarks>
Private Sub CargarGridGrupo()
Dim i As Integer
Dim iGroupIndex As Integer
Dim ogroup As SSDataWidgets_B.Group
Dim oColumn As SSDataWidgets_B.Column
Dim oatrib As CAtributo
Dim j As Integer
'Comprobamos que hay un proceso y grupo seleccionado
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
If m_oGrupoSeleccionado Is Nothing Then Exit Sub
'Prepara la grid poni�ndola en la configuraci�n normal para la carga
PrepararGridGrupo
'Limpiamos la linea de totales
sdbgTotales.Columns(0).Position = 0
sdbgTotales.Columns(1).Position = 1
sdbgTotales.RemoveAll
i = 2
While i < sdbgTotales.Columns.Count
    sdbgTotales.Columns(i).Visible = True
    sdbgTotales.Columns.Remove i
Wend

'Cargamos los proveedores en los grupos de la grid.
iGroupIndex = 2

For j = 1 To 3
    'A�adimos el proveedor a la grid
    sdbgAdj.Groups.Add iGroupIndex
    Set ogroup = sdbgAdj.Groups(iGroupIndex)
    ogroup.caption = m_sProveedor & " " & j
    sdbgAdj.Groups(iGroupIndex).Width = 1600
    sdbgAdj.Groups(iGroupIndex).AllowSizing = True
    sdbgAdj.Groups(iGroupIndex).CaptionAlignment = ssCaptionAlignmentCenter
    ogroup.HeadStyleSet = "Proveedor"
    'A�adimos el proveedor a la grid de totales
    sdbgTotales.Columns.Add sdbgTotales.Columns.Count
    Set oColumn = sdbgTotales.Columns(sdbgTotales.Columns.Count - 1)
    oColumn.Name = "PROVE" & j
    oColumn.caption = m_sProveedor & " " & j
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.HeadStyleSet = "Proveedor"
    oColumn.Width = 1600
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = "Precio" & "PROVE" & j
    oColumn.HeadStyleSet = "YellowHead"
    oColumn.caption = m_sCap(4)
    oColumn.StyleSet = "Yellow"
    oColumn.Level = 0
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Visible = m_oVistaSeleccionadaGr.PrecioProvVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = "CantMax" & "PROVE" & j
    oColumn.caption = m_sCap(9)
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Level = 0
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 2
    Set oColumn = ogroup.Columns(2)
    oColumn.Name = "AdjPorcen" & "PROVE" & j
    oColumn.caption = m_sCap(2)
    oColumn.HeadStyleSet = "BlueHead"
    oColumn.StyleSet = "Blue"
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Level = 1
    oColumn.Visible = m_oVistaSeleccionadaGr.AdjProvVisible
    oColumn.Locked = True
    oColumn.Width = 300
    DoEvents
    ogroup.Columns.Add 3
    Set oColumn = ogroup.Columns(3)
    oColumn.Name = "AdjCantidad" & "PROVE" & j
    oColumn.caption = m_sCap(3)
    oColumn.StyleSet = "Blue"
    oColumn.HeadStyleSet = "BlueHead"
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Level = 1
    oColumn.Visible = m_oVistaSeleccionadaGr.CantProvVisible
    oColumn.Locked = True
    oColumn.Width = 1300
    DoEvents
    ogroup.Columns.Add 4
    Set oColumn = ogroup.Columns(4)
    oColumn.Name = "CodProve" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 5
    Set oColumn = ogroup.Columns(5)
    oColumn.Name = "NumOfe" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 6
    Set oColumn = ogroup.Columns(6)
    oColumn.Name = "Hom" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 7
    Set oColumn = ogroup.Columns(7)
    oColumn.Name = "Indice" & "PROVE" & j
    oColumn.Level = 1
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 8
    Set oColumn = ogroup.Columns(8)
    oColumn.Name = "IMPADJ" & "PROVE" & j
    oColumn.HeadStyleSet = "YellowHead"
    oColumn.caption = m_sImpAdj
    oColumn.StyleSet = "Yellow"
    oColumn.Level = 0
    oColumn.Visible = m_oVistaSeleccionadaGr.ImpAdjVisible
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignCenter
    oColumn.Locked = True
    DoEvents
    'A�adimos los atributos a cada grupo
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        i = 9
        For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
            If oatrib.ambito = AmbItem And (oatrib.codgrupo = m_oGrupoSeleccionado.Codigo Or IsNull(oatrib.codgrupo)) Then
                ogroup.Columns.Add i
                Set oColumn = ogroup.Columns(i)
                oColumn.Name = oatrib.idAtribProce & "PROVE" & j
                oColumn.caption = oatrib.Cod
                oColumn.HeadStyleSet = "YellowHead"
                oColumn.StyleSet = "Yellow"
                oColumn.Visible = m_oVistaSeleccionadaGr.PlantConfVistasGrAtrib.Item(CStr(oatrib.idAtribProce)).Visible
                oColumn.Level = 0
                oColumn.Locked = True
                oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                If oatrib.Tipo = TipoBoolean Then oColumn.Style = ssStyleCheckBox
                i = i + 1
                DoEvents
            End If
        Next
    End If
    ogroup.TagVariant = "PROVE" & j
    iGroupIndex = iGroupIndex + 1
Next
'Inicializamos a 0 la linea de totales
sdbgTotales.AddItem m_sCap(10)
sdbgTotales.Update
sdbgTotales.AddItem m_sAdjudicado
sdbgTotales.MoveNext
sdbgTotales.Update
sdbgTotales.MoveFirst
'Rellena la grid
sdbgAdj.AddItem m_sCap(6) & " 1"
sdbgAdj.AddItem m_sCap(6) & " 2"
sdbgAdj.AddItem m_sCap(6) & " 3"
sdbgAdj.AddItem m_sCap(6) & " 4"
'Si el objetivo est� visible lo deja visible en la grid de totales
If sdbgAdj.Columns("OBJ").Visible = False Then
    sdbgTotales.Columns("OBJTOT").caption = ""
Else
    sdbgTotales.Columns("OBJTOT").caption = sCaptionTotales
End If
End Sub

Private Sub CargarGridGeneral()
Dim oGrupo As CGrupo
'Carga las grids de la pesta�a general:
CargarGruposProve
CargarProveGrupos
'Carga la grid de los atributos de proceso:
CargarGridAtributosProc
sdbgProveGrupos.AddItem m_sProveedor & " 1"
sdbgProveGrupos.AddItem m_sProveedor & " 2"
sdbgProveGrupos.AddItem m_sProveedor & " 3"
For Each oGrupo In g_oPlantillaSeleccionada.Grupos
    sdbgGruposProve.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
Next
sdbgTotalesProve.AddItem ""
sdbgtotales2.AddItem ""
End Sub

''' <summary>
''' Carga de la estructura del grid sdbgGruposProve
''' </summary>
''' <remarks>Llamada desde: CargarAdjudicaciones ; Tiempo m�ximo: 0,3</remarks>
Private Sub CargarGruposProve()
Dim i As Integer
Dim iGroupIndex As Integer
Dim ogroup As SSDataWidgets_B.Group
Dim oColumn As SSDataWidgets_B.Column
Dim oatrib As CAtributo
Dim bYaExiste As Boolean
Dim j As Integer
Dim iProv As Integer
'Limpiamos la grid de adjudicaciones
sdbgGruposProve.RemoveAll
i = sdbgGruposProve.Groups.Count
While i > 1
    sdbgGruposProve.Groups.Remove (1)
    i = sdbgGruposProve.Groups.Count
Wend
 'Limpiamos la grid de totales
sdbgTotalesProve.RemoveAll
i = sdbgTotalesProve.Groups.Count
While i > 1
    sdbgTotalesProve.Groups.Remove (1)
    i = sdbgTotalesProve.Groups.Count
Wend
iGroupIndex = 1
For iProv = 1 To 3
    'A�adimos el proveedor a la grid sdbgGruposProve
    sdbgGruposProve.Groups.Add iGroupIndex
    Set ogroup = sdbgGruposProve.Groups(iGroupIndex)
    ogroup.caption = m_sProveedor & " " & iProv
    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
    ogroup.Width = m_oVistaSeleccionada.GrupoWidth
    ogroup.AllowSizing = True
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = "Consumido" & "PROVE" & iProv
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.caption = m_sConsumido
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.ConsumidoVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = "Adjudicado" & "PROVE" & iProv
    oColumn.Alignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAdjudicado
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AdjVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 2
    Set oColumn = ogroup.Columns(2)
    oColumn.Name = "Ahorrado" & "PROVE" & iProv
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAhorrado
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.AhorroAdjVisible
    oColumn.Width = 300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 3
    Set oColumn = ogroup.Columns(3)
    oColumn.Name = "Ahorro%" & "PROVE" & iProv
    oColumn.caption = m_sAhorroPorce
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AhorroAdjPorcenVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 4
    Set oColumn = ogroup.Columns(4)
    oColumn.Name = "Importe" & "PROVE" & iProv
    oColumn.caption = m_sImporteOferta
    oColumn.StyleSet = "Importe"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.ImporteVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 5
    Set oColumn = ogroup.Columns(5)
    oColumn.Name = "AhorroOferta" & "PROVE" & iProv
    oColumn.caption = m_sAhorroOferta
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AhorroOfeVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 6
    Set oColumn = ogroup.Columns(6)
    oColumn.Name = "AhorroOfe%" & "PROVE" & iProv
    oColumn.caption = m_sPorcenAhorroOferta
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AhorroOfePorcenVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 7
    Set oColumn = ogroup.Columns(7)
    oColumn.Name = "CodProve" & "PROVE" & iProv
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 8
    Set oColumn = ogroup.Columns(8)
    oColumn.Name = "NumOfe" & "PROVE" & iProv
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 9
    Set oColumn = ogroup.Columns(9)
    oColumn.Name = "VACIO" & "PROVE" & iProv
    oColumn.Visible = False
    DoEvents
    ogroup.TagVariant = "PROVE" & iProv   'almacena en el tag del grupo el c�digo del proveedor
    ogroup.HeadStyleSet = "Proveedor"
    DoEvents
    'Si hay atributos de grupo los a�adimos a la grid
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        i = 10
        For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
            If oatrib.ambito = AmbGrupo Then
                'Si no existe la columna para ese c�digo de atributo la a�ade:
                bYaExiste = False
                If i > 10 Then
                    For j = 10 To ogroup.Columns.Count - 1
                        If Mid(ogroup.Columns(j).Name, 1, Len(oatrib.Cod)) = oatrib.Cod Then
                            bYaExiste = True
                            Exit For
                        End If
                    Next j
                End If
                If bYaExiste = False Then
                    ogroup.Columns.Add i
                    Set oColumn = ogroup.Columns(i)
                    oColumn.Name = oatrib.Cod & "PROVE" & iProv
                    oColumn.caption = oatrib.Cod
                    oColumn.HeadStyleSet = "GrayHead"
                    oColumn.Visible = m_oVistaSeleccionada.PlantConfVistasProceAtrib.Item(CStr(oatrib.idAtribProce)).Visible
                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                    If oatrib.Tipo = TipoBoolean Then
                        oColumn.Style = ssStyleCheckBox
                    End If
                    oColumn.Locked = True
                    i = i + 1
                    DoEvents
                End If
            End If
        Next
    End If
    If (g_oPlantillaSeleccionada.Solicitud = EnGrupo) Then
        ogroup.Columns.Add i
        Set oColumn = ogroup.Columns(i)
        oColumn.Name = "VINCULADO_GRU" & "PROVE" & iProv
        oColumn.caption = m_sCaptionSolicVinculada
        oColumn.HeadStyleSet = "GrayHead"
        oColumn.Visible = m_oVistaSeleccionada.SolicVinculadaVisible
        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
        oColumn.Locked = True
        oColumn.Width = 1025
        oColumn.Style = ssStyleEditButton
        oColumn.ButtonsAlways = True
        DoEvents
    End If
    'A�adimos el proveedor a la grid de totales
    sdbgTotalesProve.Groups.Add iGroupIndex
    Set ogroup = sdbgTotalesProve.Groups(iGroupIndex)
    ogroup.caption = m_sProveedor & " " & iProv
    ogroup.HeadStyleSet = "Proveedor"
    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
    ogroup.Width = m_oVistaSeleccionada.GrupoWidth
    ogroup.AllowSizing = True
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = "Consumido" & "PROVE" & iProv
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.caption = m_sCons
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.ConsumidoVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = "Adjudicado" & "PROVE" & iProv
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAdj
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = True
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 2
    Set oColumn = ogroup.Columns(2)
    oColumn.Name = "Ahorrado" & "PROVE" & iProv
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAhorr
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.AhorroAdjVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 3
    Set oColumn = ogroup.Columns(3)
    oColumn.Name = "Importe" & "PROVE" & iProv
    oColumn.caption = m_sImp
    oColumn.StyleSet = "Importe"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.ImporteVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 4
    Set oColumn = ogroup.Columns(4)
    oColumn.Name = "AhorroOferta" & "PROVE" & iProv
    oColumn.caption = m_sAhorrOf
    oColumn.StyleSet = "Importe"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AhorroOfeVisible
    oColumn.Locked = True
    DoEvents
    ogroup.TagVariant = "PROVE" & iProv 'almacena en el tag del grupo el c�digo del proveedor
    iGroupIndex = iGroupIndex + 1
Next
End Sub

''' <summary>
''' Carga de la estructura del grid sdbgProveGrupos
''' </summary>
''' <remarks>Llamada desde: CargarAdjudicaciones; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarProveGrupos()
Dim oGrupo As CGrupo
Dim i As Integer
Dim iGroupIndex As Integer
Dim ogroup As SSDataWidgets_B.Group
Dim oColumn As SSDataWidgets_B.Column
Dim oatrib As CAtributo
Dim bYaExiste As Boolean
Dim j As Integer
'Limpiamos la grid de adjudicaciones
sdbgProveGrupos.RemoveAll
i = sdbgProveGrupos.Groups.Count
While i > 1
    sdbgProveGrupos.Groups.Remove (1)
    i = sdbgProveGrupos.Groups.Count
Wend
'Limpiamos la grid de totales
sdbgtotales2.RemoveAll
i = sdbgtotales2.Groups.Count
While i > 1
    sdbgtotales2.Groups.Remove (1)
    i = sdbgtotales2.Groups.Count
Wend
iGroupIndex = 1
'Si hay atributos a nivel de proceso que cargue la grid de los atributos de proceso:
'A�adimos el grupo para los atributos de proceso a la grid sdbgProveGrupos
sdbgProveGrupos.Groups.Add iGroupIndex
Set ogroup = sdbgProveGrupos.Groups(iGroupIndex)
ogroup.caption = m_sProceso
ogroup.HeadStyleSet = "Proveedor"
ogroup.CaptionAlignment = ssCaptionAlignmentCenter
ogroup.Width = m_oVistaSeleccionada.GrupoWidth
ogroup.AllowSizing = True
i = 0
If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
    For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
        If oatrib.ambito = AmbProceso Then
            'Ahora a�ade los atributos para cada proveedor...
            ogroup.Columns.Add i
            Set oColumn = ogroup.Columns(i)
            oColumn.Name = oatrib.idAtribProce
            oColumn.caption = oatrib.Cod
            oColumn.HeadStyleSet = "GrayHead"
            oColumn.Visible = m_oVistaSeleccionada.PlantConfVistasProceAtrib.Item(CStr(oatrib.idAtribProce)).Visible
            oColumn.Locked = True
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            'Si el atributo es de tipo Si/No la celda ser� un checkbox
            If oatrib.Tipo = TipoBoolean Then
                oColumn.Style = ssStyleCheckBox
            End If
            i = i + 1
        End If
    Next
End If
If g_oPlantillaSeleccionada.Solicitud = EnProceso Then
    ogroup.Columns.Add i
    Set oColumn = ogroup.Columns(i)
    oColumn.Name = "VINCULADO_PRO"
    oColumn.caption = m_sCaptionSolicVinculada
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.SolicVinculadaVisible
    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
    oColumn.Locked = True
    oColumn.Width = 1025
    oColumn.Style = ssStyleEditButton
    oColumn.ButtonsAlways = True
    i = i + 1
    DoEvents
End If
sdbgtotales2.Groups.Add iGroupIndex
sdbgtotales2.Groups(iGroupIndex).caption = m_sProceso
sdbgtotales2.Groups(iGroupIndex).HeadStyleSet = "Proveedor"
sdbgtotales2.Groups(iGroupIndex).CaptionAlignment = ssCaptionAlignmentCenter
sdbgtotales2.Groups(iGroupIndex).AllowSizing = True
sdbgtotales2.Groups(iGroupIndex).Columns.Add 0
sdbgtotales2.Groups(iGroupIndex).Columns(0).caption = ""
sdbgtotales2.Groups(iGroupIndex).Columns(0).Locked = True
sdbgtotales2.Groups(iGroupIndex).Width = m_oVistaSeleccionada.GrupoWidth
iGroupIndex = iGroupIndex + 1
'Carga las columnas de la grid sdbgProveGrupos
For Each oGrupo In g_oPlantillaSeleccionada.Grupos
    'A�adimos el grupo a la grid sdbgProveGrupos
    sdbgProveGrupos.Groups.Add iGroupIndex
    Set ogroup = sdbgProveGrupos.Groups(iGroupIndex)
    ogroup.caption = oGrupo.Codigo & " " & oGrupo.Den
    ogroup.HeadStyleSet = "Proveedor"
    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
    ogroup.Width = m_oVistaSeleccionada.GrupoWidth
    ogroup.AllowSizing = True
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = "Consumido" & oGrupo.Codigo
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.caption = m_sConsumido
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.ConsumidoVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = "Adjudicado" & oGrupo.Codigo
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAdjudicado
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AdjVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 2
    Set oColumn = ogroup.Columns(2)
    oColumn.Name = "Ahorrado" & oGrupo.Codigo
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAhorrado
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.AhorroAdjVisible
    oColumn.Width = 300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 3
    Set oColumn = ogroup.Columns(3)
    oColumn.Name = "Ahorro%" & oGrupo.Codigo
    oColumn.caption = m_sAhorroPorce
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AhorroAdjPorcenVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 4
    Set oColumn = ogroup.Columns(4)
    oColumn.Name = "Importe" & oGrupo.Codigo
    oColumn.caption = m_sImporteOferta
    oColumn.StyleSet = "Importe"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.ImporteVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 5
    Set oColumn = ogroup.Columns(5)
    oColumn.Name = "AhorroOferta" & oGrupo.Codigo
    oColumn.caption = m_sAhorroOferta
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AhorroOfeVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 6
    Set oColumn = ogroup.Columns(6)
    oColumn.Name = "AhorroOfe%" & oGrupo.Codigo
    oColumn.caption = m_sPorcenAhorroOferta
    oColumn.StyleSet = "Yellow"
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = m_oVistaSeleccionada.AhorroOfePorcenVisible
    oColumn.Width = 1300
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 7
    Set oColumn = ogroup.Columns(7)
    oColumn.Name = "CodGrupo" & oGrupo.Codigo
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 8
    Set oColumn = ogroup.Columns(8)
    oColumn.Name = "NumOfe" & oGrupo.Codigo
    oColumn.Visible = False
    DoEvents
    ogroup.Columns.Add 9
    Set oColumn = ogroup.Columns(9)
    oColumn.Name = "CERRADO" & oGrupo.Codigo
    oColumn.Visible = False
    DoEvents
    ogroup.TagVariant = oGrupo.Codigo  'almacena en el tag del grupo el c�digo del grupo
    DoEvents
    'A�adimos los atributos
    If Not g_oPlantillaSeleccionada.Atributosvistas Is Nothing Then
        i = 10
        For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
            If oatrib.ambito = AmbGrupo Then
                'Si no existe la columna para ese c�digo de atributo la a�ade:
                bYaExiste = False
                If i > 10 Then
                    For j = 10 To ogroup.Columns.Count - 1
                        If Mid(ogroup.Columns(j).Name, 1, Len(oatrib.Cod)) = oatrib.Cod Then
                            bYaExiste = True
                            Exit For
                        End If
                    Next j
                End If
                If bYaExiste = False Then
                    ogroup.Columns.Add i
                    Set oColumn = ogroup.Columns(i)
                    oColumn.Name = oatrib.Cod & oGrupo.Codigo
                    oColumn.caption = oatrib.Cod
                    oColumn.HeadStyleSet = "GrayHead"
                    oColumn.Visible = m_oVistaSeleccionada.PlantConfVistasProceAtrib.Item(CStr(oatrib.idAtribProce)).Visible
                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                    'Si el atributo es de tipo Si/No la celda ser� una checkbox
                    If oatrib.Tipo = TipoBoolean Then
                        oColumn.Style = ssStyleCheckBox
                    End If
                    oColumn.Locked = True
                    i = i + 1
                    DoEvents
                End If
            End If
        Next
    End If
    If g_oPlantillaSeleccionada.Solicitud = EnGrupo Then
        ogroup.Columns.Add i
        Set oColumn = ogroup.Columns(i)
        oColumn.Name = "VINCULADO_GRU" & oGrupo.Codigo
        oColumn.caption = m_sCaptionSolicVinculada
        oColumn.HeadStyleSet = "GrayHead"
        oColumn.Visible = m_oVistaSeleccionada.SolicVinculadaVisible
        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
        oColumn.Locked = True
        oColumn.Width = 1025
        oColumn.Style = ssStyleEditButton
        oColumn.ButtonsAlways = True
        i = i + 1
        DoEvents
    End If
    'A�adimos el proveedor a la grid de totales
    sdbgtotales2.Groups.Add iGroupIndex
    Set ogroup = sdbgtotales2.Groups(iGroupIndex)
    ogroup.caption = oGrupo.Codigo & " " & oGrupo.Den
    ogroup.HeadStyleSet = "Proveedor"
    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
    ogroup.AllowSizing = True
    ogroup.Width = m_oVistaSeleccionada.GrupoWidth
    ogroup.Columns.Add 0
    Set oColumn = ogroup.Columns(0)
    oColumn.Name = "Consumido" & oGrupo.Codigo
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.caption = m_sCons
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.ConsumidoVisible
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 1
    Set oColumn = ogroup.Columns(1)
    oColumn.Name = "Adjudicado" & oGrupo.Codigo
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAdj
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.Visible = True
    oColumn.Locked = True
    DoEvents
    ogroup.Columns.Add 2
    Set oColumn = ogroup.Columns(2)
    oColumn.Name = "Ahorrado" & oGrupo.Codigo
    oColumn.Alignment = ssCaptionAlignmentRight
    oColumn.CaptionAlignment = ssColCapAlignRightJustify
    oColumn.caption = m_sAhorr
    oColumn.HeadStyleSet = "GrayHead"
    oColumn.Visible = m_oVistaSeleccionada.AhorroAdjVisible
    oColumn.Locked = True
    DoEvents
    ogroup.TagVariant = oGrupo.Codigo  'almacena en el tag del grupo el c�digo del grupo
    iGroupIndex = iGroupIndex + 1
Next
End Sub

''' <summary>
''' cargar la grid sdbgProce
''' </summary>
''' <remarks>Llamada desde: CargarAdjudicaciones ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridAtributosProc()
Dim iGroupIndex As Integer
Dim ogroup As SSDataWidgets_B.Group
Dim oColumn As SSDataWidgets_B.Column
Dim i As Integer
Dim oatrib As CAtributo
Dim j As Integer
Dim bHayAtribs As Boolean
bHayAtribs = False
For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
    If oatrib.ambito = AmbProceso Then
        bHayAtribs = True
        Exit For
    End If
Next
If g_oPlantillaSeleccionada Is Nothing Then Exit Sub
iGroupIndex = 1
'1� cargo las cabeceras de la grid:los proveedores y los atributos
 For j = 1 To 3
    'A�adimos el proveedor a la grid sdbgProce
    sdbgProce.Groups.Add iGroupIndex
    Set ogroup = sdbgProce.Groups(iGroupIndex)
    ogroup.caption = m_sProveedor & " " & j
    ogroup.CaptionAlignment = ssCaptionAlignmentCenter
    ogroup.Width = 1600
    ogroup.AllowSizing = True
    ogroup.TagVariant = "PROVE" & j
    'Le pone el mismo styleset que tiene sdbgGruposProve
    ogroup.HeadStyleSet = sdbgGruposProve.Groups.Item(iGroupIndex).HeadStyleSet
    i = 0
    For Each oatrib In g_oPlantillaSeleccionada.Atributosvistas
        If oatrib.ambito = AmbProceso Then
            'Ahora a�ade los atributos para cada proveedor...
            ogroup.Columns.Add i
            Set oColumn = ogroup.Columns(i)
            oColumn.Name = oatrib.idAtribProce & "PROVE" & j
            oColumn.caption = oatrib.Cod
            oColumn.HeadStyleSet = "GrayHead"
            oColumn.Visible = m_oVistaSeleccionada.PlantConfVistasProceAtrib.Item(CStr(oatrib.idAtribProce)).Visible
            oColumn.Locked = True
            oColumn.CaptionAlignment = ssColCapAlignLeftJustify
            'Si el atributo es de tipo Si/No la celda ser� un checkbox
            If oatrib.Tipo = TipoBoolean Then
                oColumn.Style = ssStyleCheckBox
            End If
            i = i + 1
        End If
    Next
    If g_oPlantillaSeleccionada.Solicitud = EnProceso Then
        ogroup.Columns.Add i
        Set oColumn = ogroup.Columns(i)
        oColumn.Name = "VINCULADO_PRO" & "PROVE" & j
        oColumn.caption = m_sCaptionSolicVinculada
        oColumn.HeadStyleSet = "GrayHead"
        oColumn.Visible = m_oVistaSeleccionada.SolicVinculadaVisible
        oColumn.Locked = True
        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
        oColumn.Width = 1025
        oColumn.Style = ssStyleEditButton
        oColumn.ButtonsAlways = True
        DoEvents
    End If
    iGroupIndex = iGroupIndex + 1
    ogroup.TagVariant = "PROVE" & j
Next
'Ahora rellenamos los valores de la grid:
sdbgProce.AddItem ""
'Ponemos el splitter visible
If sdbgProce.Groups.Count > 1 Then
    sdbgProce.SplitterVisible = True
    sdbgProce.SplitterPos = 1
End If
End Sub

Private Sub CargarSobresEnTab()
Dim i As Integer
Dim oGrupo As CGrupo
Dim bVisible As Boolean
'Relleno los tabs con los grupos de la plantilla
If g_oPlantillaSeleccionada.Grupos Is Nothing Then Exit Sub
If g_oPlantillaSeleccionada.Grupos.Count = 0 Then Exit Sub
'Primero borra todos los tabs menos el General y el de ALL
i = sstabComparativa.Tabs.Count
While i > 1
    sstabComparativa.Tabs.Remove i
    i = sstabComparativa.Tabs.Count
Wend
'Ahora cargo los sobres de la plantilla (visualiza todos los sobres que est�n
'visibles en la colecci�n de sobres)
i = 2
For Each oGrupo In g_oPlantillaSeleccionada.Grupos
    bVisible = True
    If Not m_oVistaSeleccionada.PlantConfVistasProceSobre Is Nothing Then
        If Not m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(oGrupo.Sobre)) Is Nothing Then
            If m_oVistaSeleccionada.PlantConfVistasProceSobre.Item(CStr(oGrupo.Sobre)).Visible = False Then
                bVisible = False
            End If
        Else
            bVisible = False
        End If
    End If
    If bVisible = True Then
        sstabComparativa.Tabs.Add i, "A" & oGrupo.Codigo, "(" & oGrupo.Sobre & ") " & oGrupo.Codigo & " - " & oGrupo.Den
        sstabComparativa.Tabs(i).Tag = oGrupo.Codigo
        i = i + 1
    End If
Next
'Ahora a�ade el tab para todos los items (ALL)
If sstabComparativa.Tabs.Count > 2 Then
    sstabComparativa.Tabs.Add i, "ALL", m_sALL
    sstabComparativa.Tabs(i).Tag = "ALL"
End If
End Sub

Private Sub VisualizarSobres(ByVal lSobre As Long, ByVal bVisualizar As Boolean)
'Oculta o visualiza los grupos correspondientes en las grids seg�n el sobre que se pasa por par�metro
Dim oGrupo As CGrupo
Dim i As Integer
Dim vbm As Variant
Dim sGrupo As String
If g_oPlantillaSeleccionada.Grupos Is Nothing Then Exit Sub
If g_oPlantillaSeleccionada.Grupos.Count = 0 Then Exit Sub
If bVisualizar = True Then
    'Para la grid sdbgGruposProve:
    For Each oGrupo In g_oPlantillaSeleccionada.Grupos
        If oGrupo.Sobre = lSobre Then sdbgGruposProve.AddItem oGrupo.Codigo & Chr(m_lSeparador) & oGrupo.Den
    Next
    'Para la grid sdbgProveGrupos
    For i = 1 To sdbgProveGrupos.Groups.Count - 1
        If Not IsEmpty(sdbgProveGrupos.Groups(i).TagVariant) Then
            sGrupo = sdbgProveGrupos.Groups(i).TagVariant
            If g_oPlantillaSeleccionada.Grupos.Item(CStr(sGrupo)).Sobre = lSobre Then
                sdbgProveGrupos.Groups(i).Visible = True
                sdbgtotales2.Groups(i).Visible = True
            End If
        End If
    Next
Else
    'Elimina los grupos de las grids sdbgGruposProve y sdbgProveGrupos
    'Para la grid sdbgGruposProve:
    i = sdbgGruposProve.Rows - 1
    While i >= 0
        vbm = sdbgGruposProve.AddItemBookmark(i)
        sGrupo = sdbgGruposProve.Columns("COD").CellValue(vbm)
        If g_oPlantillaSeleccionada.Grupos.Item(CStr(sGrupo)).Sobre = lSobre Then sdbgGruposProve.RemoveItem (sdbgGruposProve.AddItemRowIndex(vbm))
        i = i - 1
    Wend
    'Para la grid sdbgProveGrupos
    For i = 1 To sdbgProveGrupos.Groups.Count - 1
        If Not IsEmpty(sdbgProveGrupos.Groups(i).TagVariant) Then
            sGrupo = sdbgProveGrupos.Groups(i).TagVariant
            If g_oPlantillaSeleccionada.Grupos.Item(CStr(sGrupo)).Sobre = lSobre Then
                sdbgProveGrupos.Groups(i).Visible = False
                sdbgtotales2.Groups(i).Visible = False
            End If
        End If
    Next
End If
End Sub

Private Sub ActualizarFormPlantillas()
'Si se ha guardado alguna vista en BD actualiza la grid de plantillas para que la
'plantilla seleccionada muestre el icono de vistas
If frmPlantillasProce.g_oPlantillaSeleccionada.VistasConf = False Then
    frmPlantillasProce.g_oPlantillaSeleccionada.VistasConf = True
    frmPlantillasProce.lstvwplantillas.selectedItem.Icon = "PlantillaConVista"
    frmPlantillasProce.lstvwplantillas.selectedItem.SmallIcon = "PlantillaConVista"
    frmPlantillasProce.CargarGridPlantillas
End If
End Sub

''' <summary>
''' Ordena en un array las columnas del grupo 0 (campos item) del grd ADJ (grupo) y del grid ALL
''' </summary>
''' <param name="bGrupo">Pesta�a de grupo o no</param>
''' <remarks>Llamada desde: sdbgAdj_GrpHeadClick    sdbgAll_GrpHeadClick        RedimensionarGridGrupo      RedimensionarGridAll; Tiempo m�ximo: 0,2</remarks>
Private Sub OrdenarColsGrupo0(ByVal bGrupo As Boolean)
Dim iOrden As Integer
Dim iOrdenados As Integer
Dim iOrdenAnt As Integer
Dim sCol As String
Dim bEncontrado As Boolean
Dim i As Integer

'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
iOrdenAnt = 0
iOrdenados = 0
Erase m_arrOrden
If bGrupo = True Then
    With m_oVistaSeleccionadaGr
        'Pesta�a de grupo
        While iOrdenados < 13
            iOrden = 0
            sCol = ""
            If iOrdenAnt <= .AnyoPos Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "ANYO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = .AnyoPos
                    sCol = "ANYO"
                End If
            End If
            If (iOrdenAnt <= .ProveedorPos) Then
                If .ProveedorPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "PROV" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .ProveedorPos
                        sCol = "PROV"
                    End If
                End If
            End If
            If (iOrdenAnt <= .PresUniPos) Then
                If .PresUniPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "PRECAPE" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .PresUniPos
                        sCol = "PRECAPE"
                    End If
                End If
            End If
            If (iOrdenAnt <= .OBJPos) Then
                If .OBJPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "OBJ" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .OBJPos
                        sCol = "OBJ"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AhorroPos) Then
                If .AhorroPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "AHORROIMP" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AhorroPos
                        sCol = "AHORROIMP"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AhorroPorcenPos) Then
                If .AhorroPorcenPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "AHORROPORCEN" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AhorroPorcenPos
                        sCol = "AHORROPORCEN"
                    End If
                End If
            End If
            If (iOrdenAnt <= .ImportePos) Then
                If .ImportePos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "IMP" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .ImportePos
                        sCol = "IMP"
                    End If
                End If
            End If
            If (iOrdenAnt <= .CantidadPos) Then
               If .CantidadPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "CANT" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .CantidadPos
                        sCol = "CANT"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AdjPos) Then
                If .AdjPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "PORCENADJ" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AdjPos
                        sCol = "PORCENADJ"
                    End If
                End If
            End If
            If (iOrdenAnt <= .SolicVinculadaPos) Then
                If .SolicVinculadaPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "VINCULADO_ITEM" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .SolicVinculadaPos
                        sCol = "VINCULADO_ITEM"
                    End If
                End If
            End If
            If (iOrdenAnt <= .FecIniSumPos) Then
               If .FecIniSumPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "INI" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .FecIniSumPos
                        sCol = "INI"
                    End If
                End If
            End If
            If (iOrdenAnt <= .FecFinSumPos) Then
               If .FecFinSumPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "FIN" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .FecFinSumPos
                        sCol = "FIN"
                    End If
                End If
            End If
            If (iOrdenAnt <= .EstrMatPos) Then
               If .EstrMatPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "GMN" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .EstrMatPos
                        sCol = "GMN"
                    End If
                End If
            End If
            If sCol <> "" Then
                m_arrOrden(iOrdenados) = sCol
                iOrdenados = iOrdenados + 1
                iOrdenAnt = iOrden
            Else
                iOrdenAnt = iOrdenAnt + 1
            End If
        Wend
    End With
Else
    With m_oVistaSeleccionadaAll
        'Pesta�a de ALL
        While iOrdenados < 13
            iOrden = 0
            sCol = ""
            If iOrdenAnt <= .AnyoPos Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrden(i) = "ANYO" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = .AnyoPos
                    sCol = "ANYO"
                End If
            End If
            If (iOrdenAnt <= .ProveedorPos) Then
                If (.ProveedorPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "PROV" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .ProveedorPos
                        sCol = "PROV"
                    End If
                End If
            End If
            If (iOrdenAnt <= .PresUniPos) Then
                If (.PresUniPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "PRECAPE" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .PresUniPos
                        sCol = "PRECAPE"
                    End If
                End If
            End If
            If (iOrdenAnt <= .OBJPos) Then
                If (.OBJPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "OBJ" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .OBJPos
                        sCol = "OBJ"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AhorroPos) Then
                If (.AhorroPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "AHORROIMP" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AhorroPos
                        sCol = "AHORROIMP"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AhorroPorcenPos) Then
                If (.AhorroPorcenPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "AHORROPORCEN" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AhorroPorcenPos
                        sCol = "AHORROPORCEN"
                    End If
                End If
            End If
            If (iOrdenAnt <= .ImportePos) Then
                If (.ImportePos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "IMP" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .ImportePos
                        sCol = "IMP"
                    End If
                End If
            End If
            If (iOrdenAnt <= .CantidadPos) Then
               If (.CantidadPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "CANT" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .CantidadPos
                        sCol = "CANT"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AdjPos) Then
                If (.AdjPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "PORCENADJ" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AdjPos
                        sCol = "PORCENADJ"
                    End If
                End If
            End If
            If (iOrdenAnt <= .SolicVinculadaPos) Then
                If .SolicVinculadaPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "VINCULADO_ITEM" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .SolicVinculadaPos
                        sCol = "VINCULADO_ITEM"
                    End If
                End If
            End If
            If iOrdenAnt <= .FecIniSumPos Then
                If (.FecIniSumPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "INI" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .FecIniSumPos
                        sCol = "INI"
                    End If
                End If
            End If
            If iOrdenAnt <= .FecFinSumPos Then
                If (.FecFinSumPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "FIN" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .FecFinSumPos
                        sCol = "FIN"
                    End If
                End If
            End If
            If iOrdenAnt <= .EstrMatPos Then
                If (.EstrMatPos < iOrden) Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrden(i) = "GMN" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .EstrMatPos
                        sCol = "GMN"
                    End If
                End If
            End If
            If sCol <> "" Then
                m_arrOrden(iOrdenados) = sCol
                iOrdenados = iOrdenados + 1
                iOrdenAnt = iOrden
            Else
                iOrdenAnt = iOrdenAnt + 1
            End If
        Wend
    End With
End If
End Sub

Private Sub OrdenarColsGrupoProves(ByVal bGrupo As Boolean)
Dim iOrden As Integer
Dim iOrdenados As Integer
Dim iOrdenAnt As Integer
Dim sCol As String
Dim bEncontrado As Boolean
Dim i As Integer
Dim oAtribGr As CPlantConfVistaGrupoAtrib
Dim oAtribAll As CPlantConfVistaAllAtrib
'Posici�n de los campos del grupo 0 (les pone las posiciones en orden,porque sino a veces no funciona bien)
iOrdenAnt = 0
iOrdenados = 0
If bGrupo = True Then
    With m_oVistaSeleccionadaGr
        'Pesta�a de grupo
        If IsNull(.ImpAdjPos) Or .ImpAdjPos = 0 Then .ImpAdjPos = 1
        If IsNull(.PrecioProvPos) Or .PrecioProvPos = 0 Then .PrecioProvPos = 3
        If IsNull(.AdjProvPos) Or .AdjProvPos = 0 Then .AdjProvPos = 9
        If IsNull(.CantProvPos) Or .CantProvPos = 0 Then .CantProvPos = 8
        For Each oAtribGr In .PlantConfVistasGrAtrib
            If IsNull(oAtribGr.Posicion) Or oAtribGr.Posicion = 0 Then oAtribGr.Posicion = 1
        Next
        If Not .PlantConfVistasGrAtrib Is Nothing Then
            ReDim m_arrOrdenColsGr(4 + .PlantConfVistasGrAtrib.Count)
        Else
            ReDim m_arrOrdenColsGr(4)
        End If
        While iOrdenados < UBound(m_arrOrdenColsGr)
            iOrden = 0
            sCol = ""
            If iOrdenAnt <= .ImpAdjPos Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrdenColsGr(i) = "IMPADJ" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = .ImpAdjPos
                    sCol = "IMPADJ"
                End If
            End If
            If (iOrdenAnt <= .PrecioProvPos) Then
                If .PrecioProvPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrdenColsGr(i) = "PRECIO" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .PrecioProvPos
                        sCol = "PRECIO"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AdjProvPos) Then
                If .AdjProvPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrdenColsGr(i) = "ADJPROV" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AdjProvPos
                        sCol = "ADJPROV"
                    End If
                End If
            End If
            If (iOrdenAnt <= .CantProvPos) Then
                If .CantProvPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrdenColsGr(i) = "CANTPROV" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .CantProvPos
                        sCol = "CANTPROV"
                    End If
                End If
            End If
            If Not .PlantConfVistasGrAtrib Is Nothing Then
                For Each oAtribGr In .PlantConfVistasGrAtrib
                    If oAtribGr.Posicion < iOrden Or (iOrden = 0 And sCol = "") Then
                        bEncontrado = False
                        For i = 0 To iOrdenados
                            If m_arrOrdenColsGr(i) = oAtribGr.Atributo Then
                                bEncontrado = True
                                Exit For
                            End If
                        Next i
                        If bEncontrado = False Then
                            iOrden = oAtribGr.Posicion
                            sCol = oAtribGr.Atributo
                        End If
                    End If
                Next
            End If
            If sCol <> "" Then
                m_arrOrdenColsGr(iOrdenados) = sCol
                iOrdenados = iOrdenados + 1
                iOrdenAnt = iOrden
            Else
                iOrdenAnt = iOrdenAnt + 1
            End If
        Wend
    End With
Else
    With m_oVistaSeleccionadaAll
        'Pesta�a de ALL
        If IsNull(.ImpAdjPos) Or .ImpAdjPos = 0 Then .ImpAdjPos = 1
        If IsNull(.PrecioProvPos) Or .PrecioProvPos = 0 Then .PrecioProvPos = 3
        If IsNull(.AdjProvPos) Or .AdjProvPos = 0 Then .AdjProvPos = 9
        If IsNull(.CantProvPos) Or .CantProvPos = 0 Then .CantProvPos = 8
        For Each oAtribAll In .PlantConfVistasAllAtrib
            If IsNull(oAtribAll.Posicion) Or oAtribAll.Posicion = 0 Then oAtribAll.Posicion = 1
        Next
        If Not .PlantConfVistasAllAtrib Is Nothing Then
            ReDim m_arrOrdenColsAll(4 + .PlantConfVistasAllAtrib.Count)
        Else
            ReDim m_arrOrdenColsAll(4)
        End If
        While iOrdenados < UBound(m_arrOrdenColsAll)
            iOrden = 0
            sCol = ""
            If iOrdenAnt <= .ImpAdjPos Then
                bEncontrado = False
                For i = 0 To iOrdenados
                    If m_arrOrdenColsAll(i) = "IMPADJ" Then
                        bEncontrado = True
                        Exit For
                    End If
                Next i
                If bEncontrado = False Then
                    iOrden = .ImpAdjPos
                    sCol = "IMPADJ"
                End If
            End If
            If (iOrdenAnt <= .PrecioProvPos) Then
                If .PrecioProvPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrdenColsAll(i) = "PRECIO" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .PrecioProvPos
                        sCol = "PRECIO"
                    End If
                End If
            End If
            If (iOrdenAnt <= .AdjProvPos) Then
                If .AdjProvPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrdenColsAll(i) = "ADJPROV" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .AdjProvPos
                        sCol = "ADJPROV"
                    End If
                End If
            End If
            If (iOrdenAnt <= .CantProvPos) Then
                If .CantProvPos < iOrden Or (iOrden = 0 And sCol = "") Then
                    bEncontrado = False
                    For i = 0 To iOrdenados
                        If m_arrOrdenColsAll(i) = "CANTPROV" Then
                            bEncontrado = True
                            Exit For
                        End If
                    Next i
                    If bEncontrado = False Then
                        iOrden = .CantProvPos
                        sCol = "CANTPROV"
                    End If
                End If
            End If
            If Not .PlantConfVistasAllAtrib Is Nothing Then
                For Each oAtribAll In .PlantConfVistasAllAtrib
                    If oAtribAll.Posicion < iOrden Or (iOrden = 0 And sCol = "") Then
                        bEncontrado = False
                        For i = 0 To iOrdenados
                            If m_arrOrdenColsAll(i) = oAtribAll.Atributo Then
                                bEncontrado = True
                                Exit For
                            End If
                        Next i
                        If bEncontrado = False Then
                            iOrden = oAtribAll.Posicion
                            sCol = oAtribAll.Atributo
                        End If
                    End If
                Next
            End If
            If sCol <> "" Then
                m_arrOrdenColsAll(iOrdenados) = sCol
                iOrdenados = iOrdenados + 1
                iOrdenAnt = iOrden
            Else
                iOrdenAnt = iOrdenAnt + 1
            End If
        Wend
    End With
End If
End Sub

''' <summary>
''' Ponemos en la colecci�n lo que se haya modificado en pantalla
''' </summary>
''' <param name="bGrupo">Pesta�a de grupo o no</param>
''' <remarks>Llamada desde: sdbgAdj_GrpHeadClick    sdbgAll_GrpHeadClick; Tiempo m�ximo: 0,2</remarks>
Private Sub GuardarTamanyosCampos(ByVal bGrupo As Boolean)
Dim iGrupoVisible As Integer
Dim oatrib As CPlantConfVistaGrupoAtrib
Dim oAtribAll As CPlantConfVistaAllAtrib
'al ocultar la 2� fila de la grid comprueba si los tama�os de los grupos son distintos, y si es as� almacena las nuevas posiciones de los campos
If bGrupo = True Then
    With m_oVistaSeleccionadaGr
        If sdbgAdj.Columns("DESCR").Visible = True Then .DescrWidth = sdbgAdj.Columns("DESCR").Width
        If sdbgAdj.Columns("ANYO").Visible = True Then
            .AnyoWidth = sdbgAdj.Columns("ANYO").Width
            .AnyoLevel = sdbgAdj.Columns("ANYO").Level
        End If
        If sdbgAdj.Columns("PROV").Visible = True Then
            .ProveedorWidth = sdbgAdj.Columns("PROV").Width
            .ProveedorLevel = sdbgAdj.Columns("PROV").Level
        End If
        If sdbgAdj.Columns("PRECAPE").Visible = True Then
            .PresUniWidth = sdbgAdj.Columns("PRECAPE").Width
            .PresUniLevel = sdbgAdj.Columns("PRECAPE").Level
        End If
        If sdbgAdj.Columns("OBJ").Visible = True Then
            .OBJWidth = sdbgAdj.Columns("OBJ").Width
            .OBJLevel = sdbgAdj.Columns("OBJ").Level
        End If
        If sdbgAdj.Columns("AHORROIMP").Visible = True Then
            .AhorroWidth = sdbgAdj.Columns("AHORROIMP").Width
            .AhorroLevel = sdbgAdj.Columns("AHORROIMP").Level
        End If
        If sdbgAdj.Columns("AHORROPORCEN").Visible = True Then
            .AhorroPorcenWidth = sdbgAdj.Columns("AHORROPORCEN").Width
            .AhorroPorcenLevel = sdbgAdj.Columns("AHORROPORCEN").Level
        End If
        If sdbgAdj.Columns("IMP").Visible = True Then
            .ImporteWidth = sdbgAdj.Columns("IMP").Width
            .ImporteLevel = sdbgAdj.Columns("IMP").Level
        End If
        If sdbgAdj.Columns("CANT").Visible = True Then
            .CantidadWidth = sdbgAdj.Columns("CANT").Width
            .CantidadLevel = sdbgAdj.Columns("CANT").Level
        End If
        If sdbgAdj.Columns("PORCENADJ").Visible = True Then
            .AdjWidth = sdbgAdj.Columns("PORCENADJ").Width
            .AdjLevel = sdbgAdj.Columns("PORCENADJ").Level
        End If
        If sdbgAdj.Columns("VINCULADO_ITEM").Visible = True Then
            .SolicVinculadaWidth = sdbgAdj.Columns("VINCULADO_ITEM").Width
            .SolicVinculadaLevel = sdbgAdj.Columns("VINCULADO_ITEM").Level
        End If
        If sdbgAdj.Columns("INI").Visible = True Then
            .FecIniSumWidth = sdbgAdj.Columns("INI").Width
            .FecIniSumLevel = sdbgAdj.Columns("INI").Level
        End If
        If sdbgAdj.Columns("FIN").Visible = True Then
            .FecFinSumWidth = sdbgAdj.Columns("FIN").Width
            .FecFinSumLevel = sdbgAdj.Columns("FIN").Level
        End If
        If sdbgAdj.Columns("GMN").Visible = True Then
            .EstrMatWidth = sdbgAdj.Columns("GMN").Width
            .EstrMatLevel = sdbgAdj.Columns("GMN").Level
        End If
        If sdbgAdj.Groups.Count > 2 Then
            'Obtiene cual es el 1� proveedor que est� visible
            For iGrupoVisible = 1 To sdbgAdj.Groups.Count - 1
                If sdbgAdj.Groups(iGrupoVisible).Visible = True Then
                    Exit For
                End If
            Next iGrupoVisible
            If iGrupoVisible = sdbgAdj.Groups.Count Then Exit Sub
            If sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .ImpAdjWidth = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .ImpAdjLevel = sdbgAdj.Columns("IMPADJ" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            End If
            If sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .PrecioProvWidth = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .PrecioProvLevel = sdbgAdj.Columns("Precio" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            End If
            If sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .AdjProvWidth = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .AdjProvLevel = sdbgAdj.Columns("AdjPorcen" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            End If
            If sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .CantProvWidth = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                .CantProvLevel = sdbgAdj.Columns("AdjCantidad" & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
            End If
            For Each oatrib In .PlantConfVistasGrAtrib
                If sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Visible = True Then
                    oatrib.Width = sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Width
                    oatrib.Fila = sdbgAdj.Columns(oatrib.Atributo & sdbgAdj.Groups(iGrupoVisible).TagVariant).Level
                End If
            Next
        End If
    End With
Else
    With m_oVistaSeleccionadaAll
        If sdbgAll.Columns("DESCR").Visible = True Then .DescrWidth = sdbgAll.Columns("DESCR").Width
        If sdbgAll.Columns("ANYO").Visible = True Then
            .AnyoWidth = sdbgAll.Columns("ANYO").Width
            .AnyoLevel = sdbgAll.Columns("ANYO").Level
        End If
        If sdbgAll.Columns("PROV").Visible = True Then
            .ProveedorWidth = sdbgAll.Columns("PROV").Width
            .ProveedorLevel = sdbgAll.Columns("PROV").Level
        End If
        If sdbgAll.Columns("PRECAPE").Visible = True Then
            .PresUniWidth = sdbgAll.Columns("PRECAPE").Width
            .PresUniLevel = sdbgAll.Columns("PRECAPE").Level
        End If
        If sdbgAll.Columns("OBJ").Visible = True Then
            .OBJWidth = sdbgAll.Columns("OBJ").Width
            .OBJLevel = sdbgAll.Columns("OBJ").Level
        End If
        If sdbgAll.Columns("AHORROIMP").Visible = True Then
            .AhorroWidth = sdbgAll.Columns("AHORROIMP").Width
            .AhorroLevel = sdbgAll.Columns("AHORROIMP").Level
        End If
        If sdbgAll.Columns("AHORROPORCEN").Visible = True Then
            .AhorroPorcenWidth = sdbgAll.Columns("AHORROPORCEN").Width
            .AhorroPorcenLevel = sdbgAll.Columns("AHORROPORCEN").Level
        End If
        If sdbgAll.Columns("IMP").Visible = True Then
            .ImporteWidth = sdbgAll.Columns("IMP").Width
            .ImporteLevel = sdbgAll.Columns("IMP").Level
        End If
        If sdbgAll.Columns("CANT").Visible = True Then
            .CantidadWidth = sdbgAll.Columns("CANT").Width
            .CantidadLevel = sdbgAll.Columns("CANT").Level
        End If
        If sdbgAll.Columns("PORCENADJ").Visible = True Then
            .AdjWidth = sdbgAll.Columns("PORCENADJ").Width
            .AdjLevel = sdbgAll.Columns("PORCENADJ").Level
        End If
        If sdbgAll.Columns("VINCULADO_ITEM").Visible = True Then
            .SolicVinculadaWidth = sdbgAll.Columns("VINCULADO_ITEM").Width
            .SolicVinculadaLevel = sdbgAll.Columns("VINCULADO_ITEM").Level
        End If
        If sdbgAll.Columns("INI").Visible = True Then
            .FecIniSumWidth = sdbgAll.Columns("INI").Width
            .FecIniSumLevel = sdbgAll.Columns("INI").Level
        End If
        If sdbgAll.Columns("FIN").Visible = True Then
            .FecFinSumWidth = sdbgAll.Columns("FIN").Width
            .FecFinSumLevel = sdbgAll.Columns("FIN").Level
        End If
        If sdbgAll.Columns("GMN").Visible = True Then
            .EstrMatWidth = sdbgAll.Columns("GMN").Width
            .EstrMatLevel = sdbgAll.Columns("GMN").Level
        End If
        If sdbgAll.Groups.Count > 2 Then
            'Obtiene cual es el 1� proveedor que est� visible
            For iGrupoVisible = 1 To sdbgAll.Groups.Count - 1
                If sdbgAll.Groups(iGrupoVisible).Visible = True Then
                    Exit For
                End If
            Next iGrupoVisible
            If iGrupoVisible = sdbgAll.Groups.Count Then Exit Sub
            If sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .ImpAdjWidth = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .ImpAdjLevel = sdbgAll.Columns("IMPADJ" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            End If
            If sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .PrecioProvWidth = sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .PrecioProvLevel = sdbgAll.Columns("Precio" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            End If
            If sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .AdjProvWidth = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .AdjProvLevel = sdbgAll.Columns("AdjPorcen" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            End If
            If sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                .CantProvWidth = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                .CantProvLevel = sdbgAll.Columns("AdjCantidad" & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
            End If
            For Each oAtribAll In .PlantConfVistasAllAtrib
                If sdbgAll.Columns(oAtribAll.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Visible = True Then
                    oAtribAll.Width = sdbgAll.Columns(oAtribAll.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Width
                    oAtribAll.Fila = sdbgAll.Columns(oAtribAll.Atributo & sdbgAll.Groups(iGrupoVisible).TagVariant).Level
                End If
            Next
        End If
    End With
End If
End Sub

Private Sub txtDecResult_LostFocus()
    If Not IsNumeric(txtDecResult.Text) Then
        txtDecResult.Text = m_oVistaSeleccionada.DecResult
        Exit Sub
    End If
    'Si el n�mero de decimales es el mismo que ten�a antes no hace nada
    If Trim(txtDecResult.Text) = m_oVistaSeleccionada.DecResult Then
        Exit Sub
    End If
    m_oVistaSeleccionada.DecResult = txtDecResult.Text
End Sub
