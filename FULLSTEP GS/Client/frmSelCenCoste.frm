VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSelCenCoste 
   BackColor       =   &H00808000&
   Caption         =   "frmSelCContables"
   ClientHeight    =   7200
   ClientLeft      =   1860
   ClientTop       =   2760
   ClientWidth     =   8700
   Icon            =   "frmSelCenCoste.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7200
   ScaleMode       =   0  'User
   ScaleWidth      =   8924.355
   Begin VB.CommandButton cmdBuscar 
      Height          =   315
      Left            =   8310
      Picture         =   "frmSelCenCoste.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   150
      Width           =   336
   End
   Begin MSComctlLib.TreeView tvwestrCC 
      Height          =   6195
      Left            =   60
      TabIndex        =   2
      Top             =   510
      Width           =   8565
      _ExtentX        =   15108
      _ExtentY        =   10927
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      Appearance      =   1
   End
   Begin VB.TextBox txtCentroCoste 
      Height          =   285
      Left            =   1500
      TabIndex        =   0
      Top             =   150
      Width           =   6735
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "cmdCancelar"
      Height          =   315
      Left            =   4470
      TabIndex        =   4
      Top             =   6810
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "cmdAceptar"
      Height          =   315
      Left            =   3390
      TabIndex        =   3
      Top             =   6810
      Width           =   1005
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   420
      Top             =   1260
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCenCoste.frx":019B
            Key             =   "CC1"
            Object.Tag             =   "CC1"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCenCoste.frx":024B
            Key             =   "RAIZ"
            Object.Tag             =   "RAIZ"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCenCoste.frx":02E9
            Key             =   "CC2"
            Object.Tag             =   "CC2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCenCoste.frx":0399
            Key             =   "CC3"
            Object.Tag             =   "CC3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCenCoste.frx":042B
            Key             =   "CC4"
            Object.Tag             =   "CC4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSelCenCoste.frx":04BE
            Key             =   "CCC"
            Object.Tag             =   "CCC"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblLitFiltro 
      BackStyle       =   0  'Transparent
      Caption         =   "DFiltro por centro de coste : "
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   180
      Width           =   2085
   End
End
Attribute VB_Name = "frmSelCenCoste"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_sOrigen As String
Public g_oCenCos As CCentroCoste
Public g_oOrigen As Form
Public g_sPres0 As String
Public g_lIdEmpresa As Long
Public g_bCentrosSM As Boolean
Public g_bSaltarComprobacionArbol As Boolean

Private m_lNodo As Long
Private m_sCentros As String
Private m_oCenCostes As CCentrosCoste
Private m_bDobleClic As Boolean

Public m_bDescargarFrm As Boolean

Dim m_bActivado As Boolean

Private m_bUnload As Boolean



Private m_sMsgError As String
''' <summary>
''' Se elije un centro de coste seleccionado en el �rbol de centros de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde: tvwestrCC_DblClick; tvwestrCC_KeyPress; txtCentroCoste_KeyPress; Form_DblClick</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdAceptar_Click()

Dim nodx As MSComctlLib.node
Dim irespuesta As Integer


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwestrCC.selectedItem
    
    Set g_oCenCos = Nothing

    If Not nodx Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        If g_bCentrosSM Then
            Set g_oCenCos = m_oCenCostes.Item(DevolverCod(nodx))
        Else
            Set g_oCenCos = oFSGSRaiz.Generar_CCentroCoste
            Select Case Left(nodx.Tag, 4)
            
            Case "Raiz"
                If Me.Visible Then tvwestrCC.SetFocus
                m_bDobleClic = False
                Screen.MousePointer = vbNormal
                Exit Sub
            Case "CCx-"
                If Not m_bDobleClic Then
                    oMensajes.MensajeOKOnly (1063)
                End If
                m_bDobleClic = False
                Screen.MousePointer = vbNormal
                Exit Sub
                
            Case "CCe-"
                If Not m_bDobleClic Then
                    oMensajes.MensajeOKOnly (1078)
                End If
                m_bDobleClic = False
                Screen.MousePointer = vbNormal
                Exit Sub
            
            Case "CC1-"
                g_oCenCos.UON1 = DevolverCod(nodx)
                g_oCenCos.UON2 = ""
                g_oCenCos.UON3 = ""
                g_oCenCos.UON4 = ""

            Case "CC2-"
                g_oCenCos.UON1 = DevolverCod(nodx.Parent)
                g_oCenCos.UON2 = DevolverCod(nodx)
                g_oCenCos.UON3 = ""
                g_oCenCos.UON4 = ""
                
            Case "CC3-"
                g_oCenCos.UON1 = DevolverCod(nodx.Parent.Parent)
                g_oCenCos.UON2 = DevolverCod(nodx.Parent)
                g_oCenCos.UON3 = DevolverCod(nodx)
                g_oCenCos.UON4 = ""
                
            Case "CC4-"
                g_oCenCos.UON1 = DevolverCod(nodx.Parent.Parent.Parent)
                g_oCenCos.UON2 = DevolverCod(nodx.Parent.Parent)
                g_oCenCos.UON3 = DevolverCod(nodx.Parent)
                g_oCenCos.UON4 = DevolverCod(nodx)
                    
            End Select
            g_oCenCos.CargarCodigoSM 'CAgo el c�digo SM que necesita para la validaci�n de activo y no lo tiene
            If Not g_bSaltarComprobacionArbol Then
                If Not m_oCenCostes.EsDelArbol(g_sPres0, g_oCenCos.UON1, g_oCenCos.UON2, g_oCenCos.UON3, g_oCenCos.UON4) Then
                     oMensajes.MensajeOKOnly (1100)
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
            
            g_oCenCos.Cod = DevolverCod(nodx)
            g_oCenCos.Den = DevolverTexto(nodx)
            If g_oCenCos.UON2 <> "" Then
                g_oCenCos.CodConcat = g_oCenCos.UON1
                If g_oCenCos.UON3 <> "" Then
                    g_oCenCos.CodConcat = g_oCenCos.CodConcat & "-" & g_oCenCos.UON2
                    If g_oCenCos.UON4 <> "" Then
                        g_oCenCos.CodConcat = g_oCenCos.CodConcat & "-" & g_oCenCos.UON3
                    End If
                End If
            End If
        End If
                    

        If g_sOrigen = "Pedidos" Then
            If g_oOrigen.m_oArbolCabecera.ImpModo = TipoModoImputacionSM.Nivellinea Then
                If Not g_oOrigen.g_oOrdenesTemporales Is Nothing Then
                    If g_oOrigen.g_oOrdenesTemporales.Count > 0 Then 'Se comprueba que haya alguna linea seleccionada para pedido.
                        If g_oOrigen.txtCenCoste.Text <> "" Then
                            If g_oOrigen.m_oCenCoste.UON1 <> g_oCenCos.UON1 Or g_oOrigen.m_oCenCoste.UON2 <> g_oCenCos.UON2 Or g_oOrigen.m_oCenCoste.UON3 <> g_oCenCos.UON3 Or g_oOrigen.m_oCenCoste.UON4 <> g_oCenCos.UON4 Then
                                irespuesta = oMensajes.CambiarCentroCosteLineas(g_oOrigen.m_oArbolCabecera.NomNivelImputacion)
                            End If
                        Else
                            irespuesta = oMensajes.CambiarCentroCosteLineas(g_oOrigen.m_oArbolCabecera.NomNivelImputacion)
                        End If
        
                        If irespuesta = vbYes Then
                            Set g_oOrigen.m_oCenCoste = g_oCenCos
                            g_oOrigen.bSelectDeCen = True
                            If g_oCenCos.CodConcat = "" Then
                                g_oOrigen.txtCenCoste.Text = nodx.Text
                            Else
                                g_oOrigen.txtCenCoste.Text = nodx.Text & " (" & g_oCenCos.CodConcat & ")"
                            End If
                        End If
                    Else
                        Set g_oOrigen.m_oCenCoste = g_oCenCos
                        g_oOrigen.bSelectDeCen = True
                        If g_oCenCos.CodConcat = "" Then
                            g_oOrigen.txtCenCoste.Text = nodx.Text
                        Else
                            g_oOrigen.txtCenCoste.Text = nodx.Text & " (" & g_oCenCos.CodConcat & ")"
                        End If
                    End If
                    g_oOrigen.m_bDatosSMCargados = True
                End If
            Else
                Set g_oOrigen.m_oCenCoste = g_oCenCos
                g_oOrigen.bSelectDeCen = True
                If g_oCenCos.CodConcat = "" Then
                    g_oOrigen.txtCenCoste.Text = nodx.Text
                Else
                    g_oOrigen.txtCenCoste.Text = nodx.Text & " (" & g_oCenCos.CodConcat & ")"
                End If
            End If
        ElseIf g_sOrigen = "MisPedidos" Then
            Set g_oOrigen.m_oCenCosteSel = g_oCenCos
            g_oOrigen.bSelectDeCen = True
            If g_oCenCos.CodConcat = "" Then
                g_oOrigen.g_sSM = nodx.Text
            Else
                g_oOrigen.g_sSM = nodx.Text & " (" & g_oCenCos.CodConcat & ")"
            End If
            
        ElseIf g_sOrigen = "SelActivo" Then
            Set frmSelActivo.g_oCCSeleccionado = g_oCenCos
            frmSelActivo.g_sCodCentroSM = g_oCenCos.COD_SM
        
        ElseIf g_sOrigen = "SelContable" Then
            Set frmSelCContables.g_oCenCos = g_oCenCos
        
        ElseIf g_sOrigen = "frmSeguimiento" Then
            Set frmSeguimiento.g_oCenCos = g_oCenCos
        
        ElseIf g_sOrigen = "frmLstPedidos" Then
            Set frmLstPedidos.g_oCenCos = g_oCenCos
            
        ElseIf g_sOrigen = "frmSolicitudes" Then
            Set frmSolicitudes.g_oCenCos = g_oCenCos
            
        ElseIf g_sOrigen = "frmSolicitudBuscar" Then
            Set frmSolicitudBuscar.g_oCenCos = g_oCenCos
            
        ElseIf g_sOrigen = "frmLstSolicitud" Then
            Set frmLstSolicitud.g_oCenCos = g_oCenCos
            
        ElseIf g_sOrigen = "frmFacturaBuscar" Then
            Set frmFacturaBuscar.g_oCenCos = g_oCenCos
            
        ElseIf g_sOrigen = "frmFormularios" Then
            frmFormularios.MostrarCentroCosteSeleccionado g_oCenCos
            
        ElseIf g_sOrigen = "frmDesgloseValores" Then
            frmDesgloseValores.MostrarCentroCosteSeleccionado g_oCenCos
            
        End If

    End If
    
    m_bDobleClic = False

    Screen.MousePointer = vbNormal

   Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Evento de cancelar.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Funci�n que carga los recursos de la ventana, etiquetas...
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset



'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SEL_CENCOSTE, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        Me.caption = Ador(0).Value

        Ador.MoveNext

        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblLitFiltro.caption = Ador(0).Value
        Ador.MoveNext
        m_sCentros = Ador(0).Value
        Ador.MoveNext
        cmdBuscar.ToolTipText = Ador(0).Value

        Ador.Close

    End If

    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub


''' <summary>
''' Evento que salta cuando se hace doble click sobre el formulario.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_DblClick()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdAceptar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "Form_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Carga del formulario.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    m_bDescargarFrm = False
    m_bActivado = False
    m_bUnload = False
    CargarRecursos
    
    Set m_oCenCostes = oFSGSRaiz.Generar_CCentrosCoste
    Set g_oCenCos = oFSGSRaiz.Generar_CCentroCoste
    
    m_bDobleClic = False

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    DoEvents

If g_bCentrosSM Then
    GenerarEstructuraSM
Else
    GenerarEstructuraOrg
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Genera la estructura de los centros de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde: frmSelCenCoste.Form_Load.</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub GenerarEstructuraOrg()

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Unidades organizativas
Dim oUnidadesOrg As CUnidadesOrgNivel1

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
Dim oUON4 As CUnidadOrgNivel4

' Otras
Dim nodx As node


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oUnidadesOrg = oFSGSRaiz.Generar_CUnidadesOrgNivel1
     
    'Cargamos la estructura de centros de coste.
    If g_lIdEmpresa = 0 Then
        oUnidadesOrg.CargarEstructuraUONs_SM basOptimizacion.gvarCodUsuario
    Else
        oUnidadesOrg.CargarEstructuraUONs_SM basOptimizacion.gvarCodUsuario, g_lIdEmpresa
    End If
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    tvwestrCC.Nodes.clear
    tvwestrCC.ImageList = ImageList1
    Set nodx = tvwestrCC.Nodes.Add(, , "Raiz", m_sCentros, "RAIZ")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    If oUnidadesOrg Is Nothing Then
        Exit Sub
    End If
            
    For Each oUON1 In oUnidadesOrg
    
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        If Not oUON1.CC Then
            Set nodx = tvwestrCC.Nodes.Add("Raiz", tvwChild, "CC1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "CC1")
            nodx.Tag = "CCx-" & CStr(oUON1.Cod)
        Else
            Set nodx = tvwestrCC.Nodes.Add("Raiz", tvwChild, "CC1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "CCC")
            nodx.Tag = "CC1-" & CStr(oUON1.Cod)
        End If
        
        For Each oUON2 In oUON1.UnidadesOrgNivel2
                                
            scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
            scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
            If Not oUON2.CC Then
                Set nodx = tvwestrCC.Nodes.Add("CC1" & scod1, tvwChild, "CC2" & scod1 & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "CC2")
                nodx.Tag = "CCx-" & CStr(oUON2.Cod)
            Else
                Set nodx = tvwestrCC.Nodes.Add("CC1" & scod1, tvwChild, "CC2" & scod1 & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "CCC")
                nodx.Tag = "CC2-" & CStr(oUON2.Cod)
            End If
            
            For Each oUON3 In oUON2.UnidadesOrgNivel3
            
                scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
                scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
                scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
                If Not oUON3.CC Then
                    Set nodx = tvwestrCC.Nodes.Add("CC2" & scod1 & scod2, tvwChild, "CC3" & scod1 & scod2 & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "CC3")
                    nodx.Tag = "CCx-" & CStr(oUON3.Cod)
                Else
                    Set nodx = tvwestrCC.Nodes.Add("CC2" & scod1 & scod2, tvwChild, "CC3" & scod1 & scod2 & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "CCC")
                    nodx.Tag = "CC3-" & CStr(oUON3.Cod)
                End If
                For Each oUON4 In oUON3.UnidadesOrgNivel4
                
                    scod1 = oUON4.CodUnidadOrgNivel1 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON4.CodUnidadOrgNivel1))
                    scod2 = scod1 & oUON4.CodUnidadOrgNivel2 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON4.CodUnidadOrgNivel2))
                    scod3 = scod2 & oUON4.CodUnidadOrgNivel3 & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON4.CodUnidadOrgNivel3))
                    scod4 = scod3 & oUON4.Cod & Mid$(Space(25), 1, basParametros.gLongitudesDeCodigos.giLongCodUON4 - Len(oUON4.Cod))
                    If Not oUON4.CC Then
                        Set nodx = tvwestrCC.Nodes.Add("CC3" & scod1 & scod2 & scod3, tvwChild, "CC4" & scod1 & scod2 & scod3 & scod4, CStr(oUON4.Cod) & " - " & oUON4.Den, "CC4")
                        nodx.Tag = "CCx-" & CStr(oUON4.Cod)
                    Else
                        Set nodx = tvwestrCC.Nodes.Add("CC3" & scod1 & scod2 & scod3, tvwChild, "CC4" & scod1 & scod2 & scod3 & scod4, CStr(oUON4.Cod) & " - " & oUON4.Den, "CCC")
                        nodx.Tag = "CC4-" & CStr(oUON4.Cod)
                    End If
                Next
            Next
        Next
    Next
    
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oUON4 = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "GenerarEstructuraOrg", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Genera la estructura de los centros de coste SM.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde: frmSelCenCoste.Form_Load.</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub GenerarEstructuraSM()
Dim nodx As node
Dim oCenCoste As CCentroCoste


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oCenCostes = oFSGSRaiz.Generar_CCentrosCoste
    m_oCenCostes.CargarCentrosDeCoste , , , , , , basOptimizacion.gvarCodUsuario, False, g_lIdEmpresa, True
     
   '************************************************************
    'Generamos la estructura arborea
    
    tvwestrCC.Nodes.clear
    tvwestrCC.ImageList = ImageList1
    Set nodx = tvwestrCC.Nodes.Add(, , "Raiz", m_sCentros, "RAIZ")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    
    If m_oCenCostes Is Nothing Then
        Exit Sub
    End If
        
    For Each oCenCoste In m_oCenCostes
        Set nodx = tvwestrCC.Nodes.Add("Raiz", tvwChild, "CC1" & oCenCoste.COD_SM, oCenCoste.CodConcat, "CCC")
        nodx.Tag = "CC1-" & CStr(oCenCoste.COD_SM)
    Next
    
    
    Set nodx = Nothing
    
    Set oCenCoste = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "GenerarEstructuraSM", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Devuelve el c�digo del nodo seleccionado.
''' </summary>
''' <param name="Node">Nodo</param>
''' <returns>Devuelve el c�digo del nodo.</returns>
''' <remarks>Llamada desde: frmSelCenCoste.cmdAceptar_Click.</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Function DevolverCod(ByVal node As MSComctlLib.node) As Variant


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Function
DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


''' <summary>
''' Devuelve el texto del nodo seleccionado.
''' </summary>
''' <param name="Node">Nodo</param>
''' <returns>Devuelve el texto del nodo.</returns>
''' <remarks>Llamada desde: frmSelCenCoste.cmdAceptar_Click.</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Function DevolverTexto(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Function
DevolverTexto = Right(node.Text, Len(node.Text) - (Len(DevolverCod(node)) + 3))
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "DevolverTexto", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function


''' <summary>
''' Resize del formulario.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 1200 Then Exit Sub
    If Me.Width < 500 Then Exit Sub

    tvwestrCC.Width = Me.Width - 65

    tvwestrCC.Height = Me.Height - 1500
    cmdAceptar.Top = Me.Height - 895
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (tvwestrCC.Width / 2) - (cmdAceptar.Width / 2) - 300
    cmdCancelar.Left = (tvwestrCC.Width / 2) + 300
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub


''' <summary>
''' Descarga del formulario y las variables.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_bSaltarComprobacionArbol = False
    g_sOrigen = ""
    Set g_oCenCos = Nothing
    Set g_oOrigen = Nothing
    g_sPres0 = ""
    g_lIdEmpresa = 0
    g_bCentrosSM = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub


''' <summary>
''' Evento que salta cuando se hace doble click en alg�n nodo del �rbol de centros de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub tvwestrCC_DblClick()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDobleClic = True
    cmdAceptar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "tvwestrCC_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Evento que salta cuando estando situado en el �rbol de centros de coste se presiona alguna tecla.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub tvwestrCC_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        If Not tvwestrCC.selectedItem Is Nothing Then
            cmdAceptar_Click
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "tvwestrCC_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


''' <summary>
''' Change de la caja de texto del centro de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub txtCentroCoste_Change()
Dim nodx As MSComctlLib.node


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtCentroCoste.Text = "" Then Exit Sub
    For Each nodx In tvwestrCC.Nodes
        If InStr(1, nodx.Text, txtCentroCoste.Text, vbTextCompare) > 0 Then
            nodx.Selected = True
            Set tvwestrCC.selectedItem = nodx
            m_lNodo = nodx.Index
            Exit For
        End If
    Next

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "txtCentroCoste_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Bot�n te va moviendo a trav�s del �rbol de centros de coste por los diferentes centros de cuyo nombre forme parte el texto introducido por el usuario en la caja de texto del centro de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub cmdBuscar_Click()
Dim nodx As MSComctlLib.node
Dim i As Integer
Dim bParar As Boolean


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If m_lNodo >= tvwestrCC.Nodes.Count Then m_lNodo = 1
bParar = False
For i = m_lNodo + 1 To tvwestrCC.Nodes.Count
        Set nodx = tvwestrCC.Nodes.Item(i)
        If InStr(1, nodx.Text, txtCentroCoste.Text, vbTextCompare) > 0 Then
            nodx.Selected = True
            Set tvwestrCC.selectedItem = nodx
            m_lNodo = nodx.Index
            Exit For
        End If
        If i = tvwestrCC.Nodes.Count And Not bParar Then
            i = 2
            m_lNodo = 1
            bParar = True
        End If
Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "cmdBuscar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


''' <summary>
''' Change de la caja de texto en la que se introduce el c�digo del centro de coste.
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks></remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub txtCentroCoste_KeyPress(KeyAscii As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        If Not tvwestrCC.selectedItem Is Nothing Then
            cmdAceptar_Click
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmSelCenCoste", "txtCentroCoste_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
