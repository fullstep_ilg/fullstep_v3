VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstCATSeguridad 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de la seguridad del cat�logo"
   ClientHeight    =   2730
   ClientLeft      =   240
   ClientTop       =   1935
   ClientWidth     =   7590
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstCATSeguridad.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2730
   ScaleWidth      =   7590
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Interval        =   2000
      Left            =   4110
      Top             =   2370
   End
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   6540
      TabIndex        =   0
      Top             =   2325
      Width           =   1020
   End
   Begin TabDlg.SSTab sstab1 
      Height          =   2235
      Left            =   0
      TabIndex        =   1
      Top             =   15
      Width           =   7545
      _ExtentX        =   13309
      _ExtentY        =   3942
      _Version        =   393216
      Style           =   1
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstCATSeguridad.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame7"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame6"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      Begin VB.Frame Frame6 
         Caption         =   "Opciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   795
         Left            =   135
         TabIndex        =   6
         Top             =   1245
         Width           =   7260
         Begin VB.CheckBox chkVerBajas 
            Caption         =   "Mostrar bajas l�gicas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   210
            TabIndex        =   7
            Top             =   345
            Width           =   4170
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Datos de categor�a"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   750
         Left            =   120
         TabIndex        =   2
         Top             =   435
         Width           =   7260
         Begin VB.CommandButton cmdCatego 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6810
            Picture         =   "frmLstCATSeguridad.frx":0CCE
            Style           =   1  'Graphical
            TabIndex        =   5
            Top             =   285
            Width           =   315
         End
         Begin VB.CommandButton cmdBorra1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6435
            Picture         =   "frmLstCATSeguridad.frx":0D3A
            Style           =   1  'Graphical
            TabIndex        =   4
            Top             =   285
            Width           =   315
         End
         Begin VB.Label lblCategoria 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   195
            TabIndex        =   3
            Top             =   285
            Width           =   6195
         End
      End
   End
End
Attribute VB_Name = "frmLstCATSeguridad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oFos As FileSystemObject
Private RepPath As String
Private sSeleccion As String
Private sText() As String
Private sCategoria As String
Private sGenerando As String
Private sVisualizando As String
Private sSeleccionando As String
Private stxtSel  As String
Private stxtPag As String
Private stxtDe As String
Private stxtnivel  As String
Private stxtAprob1  As String
Private stxtAprovi  As String
Private stxtImporte  As String
Private stxtSi  As String
Private stxtNo  As String
Private stxtNotif1  As String
Private stxtNotif2  As String
        
Public ACN1Seleccionada As Long
Public ACN2Seleccionada As Long
Public ACN3Seleccionada As Long
Public ACN4Seleccionada As Long
Public ACN5Seleccionada As Long
Public DenCatSeleccionada As String
Public lSeguridad As Long
Public lCategoria As Long
Public lNivelCat As Long
Public sOrigen As String
Public g_ador As ador.Recordset

Private bRUsuAprov As Boolean
Private bBajaLog As Boolean


Private Sub cmdBorra1_Click()
    lblCategoria = ""
    ACN1Seleccionada = 0
    ACN2Seleccionada = 0
    ACN3Seleccionada = 0
    ACN4Seleccionada = 0
    ACN5Seleccionada = 0
    lSeguridad = 0
    
End Sub

Private Sub cmdCatego_Click()
    frmSELCat.sOrigen = "frmLstCATSeguridad"
    frmSELCat.bRUsuAprov = bRUsuAprov
    frmSELCat.bVerBajas = chkVerBajas
    frmSELCat.Show 1

End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    Dim i As Integer
    
    bRUsuAprov = False
    bBajaLog = True
        
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGBajaLogCon)) Is Nothing Then
        bBajaLog = False
    End If
    If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGRestUsuAprov)) Is Nothing Then
        bRUsuAprov = True
    End If
End Sub


Private Sub cmdObtener_Click()
Dim iAnio As Integer
Dim oReport As CRAXDRT.Report
Dim SubListado As CRAXDRT.Report
Dim pv As Preview
Dim sTit As String
Dim sMostrar As String
Dim oSeguridad As CSeguridad
Dim oTable  As CRAXDRT.DatabaseTable


    If lSeguridad = 0 Or lblCategoria.caption = "" Then
        oMensajes.DebeElegirUnaCategoria
        Exit Sub
    End If
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptCATSeguridad.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    Screen.MousePointer = vbHourglass
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
        
    sSeleccion = sCategoria & ": " & lblCategoria.caption

    sTit = Me.caption
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & Trim(sSeleccion) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & stxtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & stxtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & sTit & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & stxtSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNivel")).Text = """" & stxtnivel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAprobAdj")).Text = """" & stxtAprob1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAprovi")).Text = """" & stxtAprovi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtIMPORTE")).Text = """" & stxtImporte & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "stxtSI")).Text = """" & stxtSi & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "stxtNO")).Text = """" & stxtNo & """"

    Set oSeguridad = oFSGSRaiz.Generar_CSeguridad
    oSeguridad.Id = lSeguridad
    oSeguridad.Categoria = lCategoria
    oSeguridad.NivelCat = lNivelCat
    oSeguridad.CargarDatosAprobadorTipo1
    oReport.FormulaFields(crs_FormulaIndex(oReport, "APROB_TIPO1")).Text = """" & oSeguridad.AprobadorLimAdj.Cod & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "APROB_NOM")).Text = """" & oSeguridad.AprobadorLimAdj.nombre & " " & oSeguridad.AprobadorLimAdj.Apellidos & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "ID_SEG")).Text = """" & oSeguridad.Id & """"
    
    EstablecerCategoriaYNivelSeleccionado oSeguridad
    
    Set g_ador = oSeguridad.DevolverAprovisionadoresADO
    Set oSeguridad = Nothing
    
    If g_ador Is Nothing Then
        Screen.MousePointer = vbNormal
        oMensajes.NoHayDatos
        Set oReport = Nothing
        Exit Sub
    Else
        oReport.Database.SetDataSource g_ador
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    
    Set pv = New Preview
    pv.g_sOrigen = "frmLstCATSeguridad"
    pv.Hide
    pv.caption = sTit
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = sGenerando & " " & sTit
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sSeleccionando
    frmESPERA.lblDetalle = sVisualizando
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Load()
Timer1.Enabled = False
Me.Height = 3135
Me.Width = 7710

Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

If sOrigen = "" Then
    ACN1Seleccionada = 0
    ACN2Seleccionada = 0
    ACN3Seleccionada = 0
    ACN4Seleccionada = 0
    ACN5Seleccionada = 0
    lSeguridad = 0
    
End If

CargarRecursos

ConfigurarSeguridad

If Not bBajaLog Then
    chkVerBajas.Visible = False
    chkVerBajas.Value = vbUnchecked
End If

End Sub

Private Sub CargarRecursos()
Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTCATSEGURIDAD, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        sCategoria = ador(0).Value
        ador.MoveNext
        sGenerando = ador(0).Value
        ador.MoveNext
        sSeleccionando = ador(0).Value
        ador.MoveNext
        sVisualizando = ador(0).Value
        ador.MoveNext
        caption = ador(0).Value
        ador.MoveNext
        sstab1.TabCaption(0) = ador(0).Value
        ador.MoveNext
        Frame7.caption = ador(0).Value
        ador.MoveNext
        Frame6.caption = ador(0).Value
        ador.MoveNext
        'chkDetallePer.caption = Ador(0).Value
        ador.MoveNext
        chkVerBajas.caption = ador(0).Value
        ador.MoveNext
        cmdObtener.caption = ador(0).Value
        ador.MoveNext
        stxtSel = ador(0).Value
        ador.MoveNext
        stxtPag = ador(0).Value
        ador.MoveNext
        stxtDe = ador(0).Value
        ador.MoveNext
        stxtnivel = ador(0).Value
        ador.MoveNext
        stxtAprob1 = ador(0).Value
        ador.MoveNext
        stxtAprovi = ador(0).Value
        ador.MoveNext
        stxtImporte = ador(0).Value
        ador.MoveNext
        ador.MoveNext
        stxtSi = ador(0).Value
        ador.MoveNext
        stxtNo = ador(0).Value
        ador.MoveNext
        stxtNotif1 = ador(0).Value
        ador.MoveNext
        stxtNotif2 = ador(0).Value
        
        ador.Close
        
    End If
    Set ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
sOrigen = ""
End Sub

Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub
''' <summary>Para la categoria que se haya seleccionado, se le asigna al objeto seguridad el id de la categoria y el nivel de la misma</summary>
Private Sub EstablecerCategoriaYNivelSeleccionado(ByRef oSeguridad As CSeguridad)
    If ACN5Seleccionada > 0 Then
        oSeguridad.Categoria = ACN5Seleccionada
        oSeguridad.NivelCat = 5
    ElseIf ACN4Seleccionada > 0 Then
        oSeguridad.Categoria = ACN4Seleccionada
        oSeguridad.NivelCat = 4
    ElseIf ACN3Seleccionada > 0 Then
        oSeguridad.Categoria = ACN3Seleccionada
        oSeguridad.NivelCat = 3
    ElseIf ACN2Seleccionada > 0 Then
        oSeguridad.Categoria = ACN2Seleccionada
        oSeguridad.NivelCat = 2
    Else
        oSeguridad.Categoria = ACN1Seleccionada
        oSeguridad.NivelCat = 1
    End If
    
End Sub
