VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "richtx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{446276EE-F180-4092-8999-4B4D33FA389F}#347.6#0"; "rmpHTML.ocx"
Begin VB.Form frmMensaje 
   Caption         =   "Mensaje de e-mail desde FullStep GS"
   ClientHeight    =   7830
   ClientLeft      =   480
   ClientTop       =   1500
   ClientWidth     =   8400
   Icon            =   "frmMensaje.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   7830
   ScaleWidth      =   8400
   Begin rmpHTML.HTMLed HTMLed 
      Height          =   3945
      Left            =   0
      TabIndex        =   17
      Top             =   3660
      Width           =   8325
      _ExtentX        =   14684
      _ExtentY        =   6959
      BorderStyle     =   0
   End
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   885
      Left            =   0
      Picture         =   "frmMensaje.frx":09CA
      ScaleHeight     =   885
      ScaleWidth      =   8400
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   0
      Width           =   8400
      Begin VB.CommandButton cmdEnviar 
         Appearance      =   0  'Flat
         Caption         =   "Enviar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   90
         Picture         =   "frmMensaje.frx":393E8
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   90
         Width           =   915
      End
      Begin VB.CommandButton cmdCancelar 
         Appearance      =   0  'Flat
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   705
         Left            =   1230
         Picture         =   "frmMensaje.frx":396F2
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   90
         Width           =   915
      End
   End
   Begin VB.Frame Frame1 
      Height          =   2685
      Left            =   0
      TabIndex        =   10
      Top             =   840
      Width           =   8355
      Begin VB.CheckBox chkAcuseRecibo 
         Caption         =   "DPedir acuse de recibo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1020
         TabIndex        =   8
         Top             =   2400
         Width           =   6255
      End
      Begin VB.TextBox txtBcc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1020
         TabIndex        =   4
         Top             =   930
         Width           =   7245
      End
      Begin VB.TextBox txtCc 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1020
         TabIndex        =   3
         Top             =   570
         Width           =   7245
      End
      Begin VB.CommandButton cmdAdjuntar 
         Caption         =   "Adjuntar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   720
         Left            =   7320
         Picture         =   "frmMensaje.frx":3983C
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   1635
         UseMaskColor    =   -1  'True
         Width           =   945
      End
      Begin VB.TextBox txtDestino 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1020
         TabIndex        =   2
         Top             =   210
         Width           =   7245
      End
      Begin VB.TextBox txtAsunto 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1020
         TabIndex        =   5
         Top             =   1290
         Width           =   7245
      End
      Begin MSComctlLib.ListView lstvwAdjuntos 
         Height          =   675
         Left            =   1020
         TabIndex        =   6
         Top             =   1650
         Width           =   6225
         _ExtentX        =   10980
         _ExtentY        =   1191
         View            =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "FicheroAdjunto"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label lblBcc 
         Caption         =   "Cco:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   16
         Top             =   990
         Width           =   915
      End
      Begin VB.Label lblCc 
         Caption         =   "Cc:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   630
         Width           =   915
      End
      Begin VB.Label lblDestino 
         Caption         =   "Para:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   270
         Width           =   915
      End
      Begin VB.Label lblAsunto 
         Caption         =   "Asunto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1320
         Width           =   705
      End
      Begin VB.Label lblAdjuntos 
         Caption         =   "Archivos adjuntos:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   120
         TabIndex        =   11
         Top             =   1650
         Width           =   795
      End
   End
   Begin MSComDlg.CommonDialog cmmdAdjuntar 
      Left            =   7260
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7920
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMensaje.frx":398AE
            Key             =   "ADJ"
            Object.Tag             =   "ADJ"
         EndProperty
      EndProperty
   End
   Begin RichTextLib.RichTextBox txtMensaje 
      Height          =   3975
      Left            =   0
      TabIndex        =   9
      Top             =   3660
      Width           =   8355
      _ExtentX        =   14737
      _ExtentY        =   7011
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   2
      TextRTF         =   $"frmMensaje.frx":39A08
   End
End
Attribute VB_Name = "frmMensaje"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas
Public g_bHTML As Boolean
Public g_sCuerpo As String
Public g_sProve As String
Public g_sNombreApellidos As String
Public g_sSubjectFrom As String

'Variables privadas
Private sTemp As String
Private iNumElmtos As Integer
Private oFos As Scripting.FileSystemObject
Private m_sCuerpoName As String
Private m_bCargando As Boolean

'Variables para Multilenguaje
Private sIdiSelAdjuntos As String
Private sIdiTodos As String
Private sIdiDirNoValida As String
Private m_skb As String
Private m_sTitSeguimAcep As String

Private m_oErrorMail As TipoErrorSummit

Private m_bCancelarMail As Boolean
Private m_sCssHTML As String
Public g_sOrigen As String

'parametros para el registro de notificaciones
Public lIdInstancia As Long
Public entidadNotificacion As Integer
Public tipoNotificacion As Integer
Public Anyo As Integer
Public GMN1 As String
Public Proce As Long
Public sToName As String
Public sRemitenteEmpresa As String

Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean

Private m_sMsgError As String

Private Function fmsSacarCCS() As String
Dim sLinea As String
Dim sCss As String
Dim iPos As Integer
Dim s As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
s = sTemp & m_sCuerpoName
Open s For Input As 1

Do While Not EOF(1) And InStr(sLinea, ".css") = 0
    Line Input #1, sLinea
Loop

m_sCssHTML = sLinea

If Not EOF(1) Then
    'Con Estilos
    iPos = InStr(sLinea, "http://")
    If iPos = 0 Then
        iPos = InStr(sLinea, "https://")
    End If
    sCss = Mid(sLinea, iPos, Len(sLinea) - iPos - 1)
End If
Close 1

fmsSacarCCS = sCss
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "fmsSacarCCS", err, Erl, , m_bActivado, m_sMsgError)
        Exit Function
    End If

End Function


Public Function errormail() As TipoErrorSummit
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Function
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    errormail = m_oErrorMail
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "errormail", err, Erl, , m_bActivado, m_sMsgError)
        Exit Function
    End If
End Function

Private Sub cmdAdjuntar_Click()
Dim sFileName As String
Dim sFileTitle As String
Dim oFile As File
Dim sSize As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error GoTo ErrAdjuntar
    
    cmmdAdjuntar.CancelError = True
    cmmdAdjuntar.DialogTitle = sIdiSelAdjuntos
    cmmdAdjuntar.FLAGS = cdlOFNHideReadOnly
    cmmdAdjuntar.Filter = sIdiTodos & "|*.*"
    cmmdAdjuntar.ShowOpen
    
    sFileName = cmmdAdjuntar.filename
    sFileTitle = cmmdAdjuntar.FileTitle
    
    If sFileName = "" Then Exit Sub
    
    iNumElmtos = iNumElmtos + 1
    sSize = " (" & TamanyoAdjuntos(FileLen(sFileName) / 1024) & " " & m_skb & ")"
    
    lstvwAdjuntos.ListItems.Add , "NEW" & CStr(iNumElmtos), sFileTitle & sSize, , "ADJ"
    lstvwAdjuntos.ListItems.Item("NEW" & iNumElmtos).Tag = sFileTitle
    
    Set oFile = oFos.GetFile(sFileName)
    oFile.Copy sTemp & sFileTitle, True
    
Exit Sub
    
ErrAdjuntar:
    
    Exit Sub
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "cmdAdjuntar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        GoTo ErrAdjuntar
        Exit Sub
    End If
End Sub

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bCancelarMail = True
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' <summary>
''' Envia el correo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdEnviar_Click()
    Dim Item As MSComctlLib.listItem
    Dim arAdjuntos() As String
    Dim i As Integer
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    If txtDestino = "" Then
        oMensajes.errormail sIdiDirNoValida
        Exit Sub
    Else
        If Not ComprobarEmail(txtDestino.Text) Then
            oMensajes.MensajeOKOnly 1132
            Exit Sub
        End If
    End If
    cmdEnviar.Enabled = False
    Screen.MousePointer = vbHourglass
    
    ReDim arAdjuntos(0)
    i = 0
    For Each Item In lstvwAdjuntos.ListItems
        arAdjuntos(i) = Item.Tag
        ReDim Preserve arAdjuntos(i + 1)
        i = i + 1
    Next
    
    m_bCancelarMail = False
    
    Dim strSessionId As String
    strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    If i > 0 Then
        m_oErrorMail = EnviarMensaje(txtDestino.Text, txtAsunto.Text, IIf(g_bHTML = True, HTMLed.allHTML, txtMensaje.Text), arAdjuntos, Trim(txtCc.Text), Trim(txtBcc.Text), SQLBinaryToBoolean(chkAcuseRecibo.Value), g_bHTML, g_sOrigen, g_sProve, g_sNombreApellidos, g_sSubjectFrom, lIdInstancia, entidadNotificacion, tipoNotificacion, Anyo, GMN1, Proce, sToName, strSessionId, sRemitenteEmpresa)
    Else
        m_oErrorMail = EnviarMensaje(txtDestino.Text, txtAsunto.Text, IIf(g_bHTML = True, HTMLed.allHTML, txtMensaje.Text), , Trim(txtCc.Text), Trim(txtBcc.Text), SQLBinaryToBoolean(chkAcuseRecibo.Value), g_bHTML, g_sOrigen, g_sProve, g_sNombreApellidos, g_sSubjectFrom, lIdInstancia, entidadNotificacion, tipoNotificacion, Anyo, GMN1, Proce, sToName, strSessionId, sRemitenteEmpresa)
    End If
   
    cmdEnviar.Enabled = True
    Screen.MousePointer = vbNormal
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "cmdEnviar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbNormal
    CargarRecursos
    m_bActivado = False
    m_bDescargarFrm = False
    
    If txtCc.Text = "" Then
        txtCc.Text = gParametrosInstalacion.gsRecipientCC
    End If
    
    txtBcc.Text = gParametrosInstalacion.gsRecipientBCC
    
    If basPublic.gParametrosInstalacion.gbAcuseRecibo Then
        chkAcuseRecibo.Value = vbChecked
    Else
        chkAcuseRecibo.Value = vbUnchecked
    End If
    
    sTemp = FSGSLibrary.DevolverPathFichTemp
    Set oFos = New Scripting.FileSystemObject
    
    m_oErrorMail.NumError = TESnoerror
    
    m_bCancelarMail = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Resize()
    'Redimensiona el formulario:
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 1300 Then Exit Sub
    If Me.Height < 4000 Then Exit Sub
    
    txtMensaje.Height = Me.Height - txtMensaje.Top - 435
    txtMensaje.Width = Me.Width - 120
    
    HTMLed.Height = Me.Height - txtMensaje.Top - 435
    HTMLed.Width = Me.Width - 120
    
    Frame1.Width = Me.Width - 120
    txtAsunto.Width = Me.Width - 1230
    txtBcc.Width = Me.Width - 1230
    txtCc.Width = Me.Width - 1230
    txtDestino.Width = Me.Width - 1230
    lstvwAdjuntos.Width = Me.Width - 2250
    
    cmdAdjuntar.Left = Me.Width - 1155
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

''' <summary>
''' Marca a la pagina q llamo si se pulsa enviar mail o no.
''' Descarga variables.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
Dim Item As MSComctlLib.listItem
Dim bErroresEnElBorrado As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    Screen.MousePointer = vbHourglass
    
    If m_bCancelarMail = True Then
        Select Case g_sOrigen
            Case "frmOfePetAnya"
                frmOFEPetAnya.g_bCancelarMail = True
            Case "FrmOBJAnya"
                FrmOBJAnya.g_bCancelarMail = True
            Case "frmADJAnya"
                frmADJAnya.g_bCancelarMail = True
            Case "frmNotificaPedido"
                frmNotificaPedido.g_bCancelarMail = True
            Case "frmOFEAvisoAnya"
                frmOFEAvisoAnya.g_bCancelarMail = True
            Case "frmSeguimComunic"
                frmSeguimComunic.g_bCancelarMail = True
            Case "frmPedidosEmision1"
                frmPedidosEmision1.g_bCancelarMail = True
            Case "frmPROCE"
                frmPROCE.g_bCancelarMail = True
        End Select
        m_bCancelarMail = False
    End If
    
    g_sOrigen = ""
    g_sCuerpo = ""
    g_sProve = ""
    g_sNombreApellidos = ""
    
    On Error Resume Next 'Al eliminar los ficheros temp si le da algun error de permiso denegado que siga
    For Each Item In lstvwAdjuntos.ListItems
        If Left(Item.key, 3) = "NEW" Then
          If oFos.FileExists(sTemp & Item.Tag) Then
              oFos.DeleteFile sTemp & Item.Tag, True
          End If
        End If
    Next
    
    If g_bHTML = True Then   'Si es html elimina el fichero .htm generado
        oFos.DeleteFile sTemp & m_sCuerpoName, True
        g_bHTML = False
    End If
    
    Set oFos = Nothing
    Screen.MousePointer = vbDefault

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
    End If
    
End Sub




Private Sub lstvwAdjuntos_DblClick()
Dim Item As MSComctlLib.listItem
Dim sFileName As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set Item = lstvwAdjuntos.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        sFileName = sTemp & Item.Tag
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "lstvwAdjuntos_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
    
End Sub

Private Sub lstvwAdjuntos_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = 46 Then
    ' suprimir
        If lstvwAdjuntos.selectedItem Is Nothing Then
            oMensajes.SeleccioneFichero
        Else
            lstvwAdjuntos.ListItems.Remove (CStr(lstvwAdjuntos.selectedItem.key))
            lstvwAdjuntos.Refresh
        End If
    End If
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_MENSAJE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        cmdEnviar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblDestino.caption = Ador(0).Value
        Ador.MoveNext
        lblAsunto.caption = Ador(0).Value
        Ador.MoveNext
        lblAdjuntos.caption = Ador(0).Value
        Ador.MoveNext
        cmdAdjuntar.caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiSelAdjuntos = Ador(0).Value
        Ador.MoveNext
        sIdiTodos = Ador(0).Value
        Ador.MoveNext
        sIdiDirNoValida = Ador(0).Value
        Ador.MoveNext
        lblBcc.caption = Ador(0).Value
        Ador.MoveNext
        chkAcuseRecibo.caption = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        Ador.MoveNext
        m_sTitSeguimAcep = Ador(0).Value
        
        Ador.Close
    
    End If

    If g_sOrigen = "frmSeguimAceptacion" Then
        Me.caption = m_sTitSeguimAcep
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------

End Sub

Public Sub CargarCuerpoMensaje()
    Dim DataFile As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bHTML = True Then
        m_bCargando = True
        
        HTMLed.Visible = True
        txtMensaje.Visible = False
        
        m_sCuerpoName = "Cuerpo" & CStr(Year(Now())) & CStr(Month(Now())) & CStr(Day(Now())) & CStr(Hour(Now())) & CStr(Minute(Now())) & CStr(Second(Now())) & ".htm"
        
        DataFile = 1
        Open sTemp & m_sCuerpoName For Binary Access Write As DataFile
        Put DataFile, , g_sCuerpo
        
        DoEvents
        
        Close DataFile
        DataFile = 0
        HTMLed.CSShttp = True
        HTMLed.CSSText = fmsSacarCCS()
        HTMLed.setLicenceKey "1127-9948-2699"
        HTMLed.Open_document sTemp & m_sCuerpoName
        
        'Quitamos los botones que no nos interesan
        HTMLed.HiddenButtons = "Spell Check;Image;RemoteImage"
        m_bCargando = False
        
    Else
        'Es de tipo texto
        HTMLed.Visible = False
        txtMensaje.Visible = True
        
        txtMensaje.Text = g_sCuerpo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmMensaje", "CargarCuerpoMensaje", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub








