VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmBloqueosCondiciones 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "frmBloqueosCondiciones"
   ClientHeight    =   3810
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13575
   Icon            =   "frmBloqueosCondiciones.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3810
   ScaleWidth      =   13575
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAyuda 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   312
      Left            =   12360
      Picture         =   "frmBloqueosCondiciones.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3420
      Width           =   435
   End
   Begin VB.CommandButton cmdEliminarCondicion 
      Height          =   315
      Left            =   12990
      Picture         =   "frmBloqueosCondiciones.frx":0EE3
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   450
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirCondicion 
      Height          =   312
      Left            =   12990
      Picture         =   "frmBloqueosCondiciones.frx":0F75
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   90
      Width           =   432
   End
   Begin VB.TextBox txtFormula 
      Height          =   285
      Left            =   960
      TabIndex        =   1
      Top             =   3420
      Width           =   11295
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCondiciones 
      Height          =   3255
      Left            =   90
      TabIndex        =   6
      Top             =   60
      Width           =   12735
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   17
      stylesets.count =   6
      stylesets(0).Name=   "No"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmBloqueosCondiciones.frx":0FF7
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "S�"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmBloqueosCondiciones.frx":1013
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Gris"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmBloqueosCondiciones.frx":102F
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmBloqueosCondiciones.frx":104B
      stylesets(4).Name=   "ActiveRow"
      stylesets(4).ForeColor=   16777215
      stylesets(4).BackColor=   8388608
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmBloqueosCondiciones.frx":1067
      stylesets(5).Name=   "Amarillo"
      stylesets(5).BackColor=   12648447
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmBloqueosCondiciones.frx":1083
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowAddNew     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   159
      Columns.Count   =   17
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   1826
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   5
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "TIPO_CAMPO"
      Columns(2).Name =   "TIPO_CAMPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   2
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID_GRUPO"
      Columns(3).Name =   "ID_GRUPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   3
      Columns(3).FieldLen=   256
      Columns(4).Width=   2064
      Columns(4).Caption=   "DGrupo"
      Columns(4).Name =   "GRUPO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   3
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "ID_CAMPO"
      Columns(5).Name =   "ID_CAMPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   3
      Columns(5).FieldLen=   256
      Columns(6).Width=   3519
      Columns(6).Caption=   "DCampo"
      Columns(6).Name =   "CAMPO"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   3
      Columns(7).Width=   1640
      Columns(7).Caption=   "DOperador"
      Columns(7).Name =   "OPERADOR"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   3
      Columns(8).Width=   1561
      Columns(8).Caption=   "DDirecto"
      Columns(8).Name =   "DIRECTO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   2064
      Columns(9).Caption=   "DValor"
      Columns(9).Name =   "VALOR"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "TIPO_VALOR"
      Columns(10).Name=   "TIPO_VALOR"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   2
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "ID_GRUPO_VALOR"
      Columns(11).Name=   "ID_GRUPO_VALOR"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   3
      Columns(11).FieldLen=   256
      Columns(12).Width=   2064
      Columns(12).Caption=   "DGrupo"
      Columns(12).Name=   "GRUPO_VALOR"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(12).Style=   3
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "ID_CAMPO_VALOR"
      Columns(13).Name=   "ID_CAMPO_VALOR"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   3
      Columns(13).FieldLen=   256
      Columns(14).Width=   3519
      Columns(14).Caption=   "CAMPO_VALOR"
      Columns(14).Name=   "CAMPO_VALOR"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "FECACT"
      Columns(15).Name=   "FECACT"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   7
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Caption=   "dMoneda"
      Columns(16).Name=   "MONEDA"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(16).Locked=   -1  'True
      _ExtentX        =   22463
      _ExtentY        =   5741
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddCampo 
      Height          =   555
      Left            =   9420
      TabIndex        =   7
      Top             =   1080
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   6165
      Columns(2).Caption=   "CAMPO"
      Columns(2).Name =   "CAMPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValorBooleano 
      Height          =   555
      Left            =   12210
      TabIndex        =   8
      Top             =   1320
      Width           =   1215
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "VALOR"
      Columns(0).Name =   "VALOR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   2143
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOperador 
      Height          =   555
      Left            =   3480
      TabIndex        =   9
      Top             =   3240
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   6165
      Columns(0).Caption=   "OPERADOR"
      Columns(0).Name =   "OPERADOR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddCampoValor 
      Height          =   555
      Left            =   9210
      TabIndex        =   10
      Top             =   1680
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   6165
      Columns(2).Caption=   "CAMPO_VALOR"
      Columns(2).Name =   "CAMPO_VALOR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddGrupoValor 
      Height          =   555
      Left            =   9330
      TabIndex        =   11
      Top             =   2280
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6165
      Columns(1).Caption=   "GRUPO"
      Columns(1).Name =   "GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6174
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddGrupo 
      Height          =   555
      Left            =   -120
      TabIndex        =   0
      Top             =   2640
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6165
      Columns(1).Caption=   "GRUPO"
      Columns(1).Name =   "GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddMoneda 
      Height          =   555
      Left            =   9060
      TabIndex        =   12
      Top             =   2790
      Width           =   3495
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1773
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3519
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.Label lblCondicion 
      BackColor       =   &H00808000&
      Caption         =   "Condici�n"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   60
      TabIndex        =   5
      Top             =   3420
      Width           =   1035
   End
End
Attribute VB_Name = "frmBloqueosCondiciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_lIdFormulario As Long
Public m_bModifFlujo As Boolean
Public m_lIdFlujo As Long
Public m_sFormula As String

Public m_lIdCond As Long
Private m_oCondiciones As CCondsBloqueoConds
Private m_arCamposGenericos(2 To 9) As String
'Variable Privadas
Private m_oCondicionEnEdicion As CCondBloqueoCond
Private m_oCondicionAnyadir As CCondBloqueoCond
Private m_oIBAseDatosEnEdicion As IBaseDatos
Private m_oIBaseDatos As IBaseDatos

Private m_sGrupo As String
Private m_sCampo As String
Private m_sFechaActual As String
Private m_sOperador As String
Private m_sDirecto As String
Private m_sValor As String
Private m_sCampoGenerico As String

Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bError As Boolean

Private m_sMensajesCondicion(1 To 6) As String  'literales para basmensajes
Private m_sErrorFormula(1 To 12) As String
Private m_sValorBooleano(1) As String ' No y Si

Private m_oMonedas As CMonedas
Private m_oGrupos As CFormGrupos

Private lIdPrecondicion As Long
Private Sub sdbddMoneda_DropDown()
    If Not estaBloqueadaMoneda Then
        sdbddMoneda.Enabled = True
        cargarComboMonedas
    Else
        sdbddMoneda.Enabled = False
    End If
End Sub

Private Sub sdbddMoneda_InitColumnProps()
    sdbddMoneda.DataFieldList = "Column 0"
    sdbddMoneda.DataFieldToDisplay = "Column 0"
End Sub


Private Sub cmdAnyadirCondicion_Click()

    sdbgCondiciones.Scroll 0, sdbgCondiciones.Rows - sdbgCondiciones.Row
    
    If sdbgCondiciones.VisibleRows > 0 Then
        
        If sdbgCondiciones.VisibleRows >= sdbgCondiciones.Rows Then
            If sdbgCondiciones.VisibleRows = sdbgCondiciones.Rows Then
                sdbgCondiciones.Row = sdbgCondiciones.Rows - 1
            Else
                sdbgCondiciones.Row = sdbgCondiciones.Rows
    
            End If
        Else
            sdbgCondiciones.Row = sdbgCondiciones.Rows - (sdbgCondiciones.Rows - sdbgCondiciones.VisibleRows) - 1
        End If
        
    End If
    
    sdbgCondiciones.Columns("COD").Value = "X" & m_oCondiciones.ObtenerCOD(m_lIdCond)
    
    sdbgCondiciones.Columns("GRUPO").CellStyleSet "Normal", sdbgCondiciones.Row
    sdbgCondiciones.Columns("GRUPO").Locked = False
    sdbgCondiciones.Columns("GRUPO").DropDownHwnd = sdbddGrupo.hWnd
    sdbgCondiciones.Columns("VALOR").Locked = True
    sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
    sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hWnd
    sdbgCondiciones.Columns("GRUPO_VALOR").Locked = False
    sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
    sdbgCondiciones.Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hWnd
    sdbgCondiciones.Columns("CAMPO_VALOR").Locked = False
    sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row

    sdbgCondiciones.col = 4
    If Me.Visible Then sdbgCondiciones.SetFocus
    sdbgCondiciones.DoClick
    
End Sub

Private Sub cmdAyuda_Click()
    'si hay cambios los guarda
    If sdbgCondiciones.DataChanged = True Then
        sdbgCondiciones.Update
        If m_bError = True Then Exit Sub
    End If
    
    MostrarFormSOLAyudaCalculos oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdEliminarCondicion_Click()
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit

    If sdbgCondiciones.IsAddRow Then
        If sdbgCondiciones.DataChanged Then
            sdbgCondiciones.CancelUpdate
        End If
    Else
        If sdbgCondiciones.Rows = 0 Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        If sdbgCondiciones.SelBookmarks.Count = 1 Then
            irespuesta = oMensajes.PreguntaEliminar(sdbgCondiciones.Columns("COD").Value)
            
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            If sdbgCondiciones.IsAddRow Then
                If sdbgCondiciones.DataChanged Then
                    sdbgCondiciones.CancelUpdate
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If

            Set m_oCondicionEnEdicion = m_oCondiciones.Item(CStr(sdbgCondiciones.Columns("ID").Value))
            
            Set m_oIBAseDatosEnEdicion = m_oCondicionEnEdicion
            teserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos()
            
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                
                If Me.Visible Then sdbgCondiciones.SetFocus
                Exit Sub
            Else
                m_oCondiciones.Remove (CStr(sdbgCondiciones.Columns("ID").Value))
                If sdbgCondiciones.AddItemRowIndex(sdbgCondiciones.Bookmark) > -1 Then
                    sdbgCondiciones.RemoveItem (sdbgCondiciones.AddItemRowIndex(sdbgCondiciones.Bookmark))
                Else
                    sdbgCondiciones.RemoveItem (0)
                End If
                If IsEmpty(sdbgCondiciones.GetBookmark(0)) Then
                    sdbgCondiciones.Bookmark = sdbgCondiciones.GetBookmark(-1)
                Else
                    sdbgCondiciones.Bookmark = sdbgCondiciones.GetBookmark(0)
                End If
                
                basSeguridad.RegistrarAccion accionessummit.ACCCondicionBloqueoElim, "Condicion Particular:" & m_lIdCond & ",ID:" & m_oCondicionEnEdicion.Id
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If
            
            Set m_oIBAseDatosEnEdicion = Nothing
            Set m_oCondicionEnEdicion = Nothing
            
        End If
            
        sdbgCondiciones.SelBookmarks.RemoveAll
    
        Screen.MousePointer = vbNormal
    
    End If
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_BLOQUEOSCONDICIONES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value
        Ador.MoveNext
        
        m_sGrupo = Ador(0).Value
        m_sMensajesCondicion(1) = Ador(0).Value
        Ador.MoveNext
        m_sCampo = Ador(0).Value
        m_sMensajesCondicion(2) = Ador(0).Value
        Ador.MoveNext
        m_sOperador = Ador(0).Value
        m_sMensajesCondicion(3) = Ador(0).Value
        Ador.MoveNext
        m_sDirecto = Ador(0).Value '5
        m_sMensajesCondicion(4) = Ador(0).Value
        Ador.MoveNext
        m_sValor = Ador(0).Value
        m_sMensajesCondicion(5) = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirCondicion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarCondicion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        lblCondicion.caption = Ador(0).Value
        Ador.MoveNext
        cmdAyuda.ToolTipText = Ador(0).Value '10
                                        
        Ador.MoveNext
        m_arCamposGenericos(2) = Ador(0).Value
        Ador.MoveNext
        m_arCamposGenericos(3) = Ador(0).Value
        Ador.MoveNext
        m_arCamposGenericos(4) = Ador(0).Value
        Ador.MoveNext
        m_arCamposGenericos(5) = Ador(0).Value
        Ador.MoveNext
        m_arCamposGenericos(6) = Ador(0).Value ' 15
        Ador.MoveNext
        m_arCamposGenericos(7) = Ador(0).Value
        Ador.MoveNext
        m_arCamposGenericos(8) = Ador(0).Value
        Ador.MoveNext
        m_arCamposGenericos(9) = Ador(0).Value
        Ador.MoveNext
        
        m_sMensajesCondicion(6) = Ador(0).Value
        Ador.MoveNext
        m_sValorBooleano(0) = Ador(0).Value
        Ador.MoveNext
        m_sValorBooleano(1) = Ador(0).Value
        
       For i = 1 To 11
            Ador.MoveNext
            m_sErrorFormula(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sCampoGenerico = Ador(0).Value
        
        Ador.MoveNext
        m_sFechaActual = Ador(0).Value
        
        Ador.MoveNext
        sdbgCondiciones.Columns("MONEDA").caption = Ador(0).Value
        Ador.MoveNext
        m_sErrorFormula(12) = Ador(0).Value
                                                                               
        Ador.Close
    End If
End Sub

Private Sub ConfigurarSeguridad()
        
    If m_bModifFlujo Then
        sdbddGrupo.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgCondiciones.Columns("GRUPO").DropDownHwnd = sdbddGrupo.hWnd
        sdbddCampo.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgCondiciones.Columns("CAMPO").DropDownHwnd = sdbddCampo.hWnd
        sdbddOperador.AddItem ""
        sdbgCondiciones.Columns("OPERADOR").DropDownHwnd = sdbddOperador.hWnd
        sdbddValorBooleano.AddItem ""
        sdbddGrupoValor.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hWnd
        sdbddCampoValor.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgCondiciones.Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hWnd
        sdbgCondiciones.Columns("MONEDA").DropDownHwnd = sdbddMoneda.hWnd
   
    Else
        sdbgCondiciones.AllowAddNew = False
        sdbgCondiciones.AllowUpdate = False
        sdbgCondiciones.AllowDelete = False

        cmdAnyadirCondicion.Enabled = False
        cmdEliminarCondicion.Enabled = False
        txtFormula.Enabled = False
    End If
End Sub


Private Sub Form_Initialize()
    Set m_oGrupos = oFSGSRaiz.Generar_CFormGrupos
End Sub

Private Sub Form_Load()

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    ConfigurarSeguridad
    CargarRecursos

    PonerFieldSeparator Me

    CargarCondiciones

    Set m_oMonedas = oFSGSRaiz.Generar_CMonedas
    m_oMonedas.CargarTodasLasMonedas , , , , , False, True
    cargarComboMonedas

    txtFormula.Text = m_sFormula

End Sub


Private Sub CargarCondiciones()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim oCampoValor As CFormItem
    Dim oCondicion As CCondBloqueoCond
    
    Dim lIdGrupo As Long
    Dim sGrupo As String
    Dim sCampo As String
    Dim sOperador As String
    Dim bDirecto As Boolean
    Dim sValor As String
    Dim lIdGrupoValor As Long
    Dim sGrupoValor As String
    Dim sCampoValor As String
    Dim i As Integer
    
    sdbgCondiciones.RemoveAll
    
    Set m_oCondiciones = oFSGSRaiz.Generar_CCondsBloqueoConds
        m_oCondiciones.CargarCondiciones (m_lIdCond)
        
    For Each oCondicion In m_oCondiciones
        If oCondicion.tipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario Then
            Set oCampo = oFSGSRaiz.Generar_CFormCampo
            oCampo.Id = oCondicion.Campo
            oCampo.CargarDatosFormCampo
            For i = 1 To oCampo.Denominaciones.Count
                If oCampo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                    sCampo = oCampo.Denominaciones.Item(i).Den
                    Exit For
                End If
            Next
                      
            Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
            oGrupo.Id = oCampo.Grupo.Id
            lIdGrupo = oCampo.Grupo.Id
            oGrupo.CargarDatosFormGrupo
            
            Set oCondicion.CampoForm = oCampo
            
            For i = 1 To oGrupo.Denominaciones.Count
                If oGrupo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                    sGrupo = oGrupo.Denominaciones.Item(i).Den
                    Exit For
                End If
            Next
            
        Else
            sGrupo = m_sCampoGenerico
            lIdGrupo = 0
            sCampo = m_arCamposGenericos(oCondicion.tipoCampo)
        End If
        sOperador = oCondicion.Operador
        If oCondicion.TipoValor = TipoValorCondicionEnlace.CampoDeFormulario Then
            bDirecto = False
            sValor = ""
            Set oCampoValor = oFSGSRaiz.Generar_CFormCampo
            oCampoValor.Id = oCondicion.CampoValor
            oCampoValor.CargarDatosFormCampo
            For i = 1 To oCampoValor.Denominaciones.Count
                If oCampoValor.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                    sCampoValor = oCampoValor.Denominaciones.Item(i).Den
                    Exit For
                End If
            Next
                      
            Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
            oGrupo.Id = oCampoValor.Grupo.Id
            lIdGrupoValor = oCampoValor.Grupo.Id
            oGrupo.CargarDatosFormGrupo
            For i = 1 To oGrupo.Denominaciones.Count
                If oGrupo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                    sGrupoValor = oGrupo.Denominaciones.Item(i).Den
                    Exit For
                End If
            Next
        ElseIf oCondicion.TipoValor = TipoValorCondicionEnlace.DepPetcionario Or oCondicion.TipoValor = TipoValorCondicionEnlace.ImporteAbierto Or _
            oCondicion.TipoValor = TipoValorCondicionEnlace.ImporteAdjudicado Or oCondicion.TipoValor = TipoValorCondicionEnlace.ImporteEmitido Or _
            oCondicion.TipoValor = TipoValorCondicionEnlace.ImporteSolicitud Or oCondicion.TipoValor = TipoValorCondicionEnlace.NumProcesAbiertos Or _
            oCondicion.TipoValor = TipoValorCondicionEnlace.Peticionario Or oCondicion.TipoValor = TipoValorCondicionEnlace.UONPeticionario Then
            bDirecto = False
            sGrupoValor = m_sCampoGenerico
            lIdGrupoValor = 0
            sCampoValor = m_arCamposGenericos(oCondicion.TipoValor)

        Else
            bDirecto = True
            If Not oCampo Is Nothing Then
                If oCampo.Tipo = TipoBoolean Then
                    sValor = m_sValorBooleano(Abs(CInt(oCondicion.valor)))
                Else
                    sValor = oCondicion.valor
                End If
            Else
                sValor = oCondicion.valor
            End If
            sGrupoValor = ""
            lIdGrupoValor = 0
            sCampoValor = ""
        End If
        
        sdbgCondiciones.AddItem oCondicion.Id & Chr(m_lSeparador) & oCondicion.Cod & Chr(m_lSeparador) & oCondicion.tipoCampo & Chr(m_lSeparador) & lIdGrupo & Chr(m_lSeparador) & sGrupo & Chr(m_lSeparador) & oCondicion.Campo & Chr(m_lSeparador) & sCampo & Chr(m_lSeparador) & oCondicion.Operador & Chr(m_lSeparador) & bDirecto & Chr(m_lSeparador) & sValor & Chr(m_lSeparador) & oCondicion.TipoValor & Chr(m_lSeparador) & lIdGrupoValor & Chr(m_lSeparador) & sGrupoValor & Chr(m_lSeparador) & oCondicion.CampoValor & Chr(m_lSeparador) & sCampoValor & Chr(m_lSeparador) & oCondicion.FECACT & Chr(m_lSeparador) & oCondicion.Moneda
    Next
End Sub

Private Sub sdbddCampo_CloseUp()
    If sdbgCondiciones.Columns("CAMPO").Value = "" Then Exit Sub

    sdbgCondiciones.Columns("TIPO_CAMPO").Value = sdbddCampo.Columns(0).Value
    sdbgCondiciones.Columns("ID_CAMPO").Value = sdbddCampo.Columns(1).Value
    sdbgCondiciones.Columns("CAMPO").Value = sdbddCampo.Columns(2).Value
    
    If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.UONPeticionario Then
        sdbgCondiciones.Columns("DIRECTO").Value = True
        sdbgCondiciones.Columns("DIRECTO").Locked = True
        sdbgCondiciones.Columns("DIRECTO").CellStyleSet "Gris", sdbgCondiciones.Row
        sdbgCondiciones.Columns("VALOR").Value = ""
        sdbgCondiciones.Columns("VALOR").Locked = False
        sdbgCondiciones.Columns("VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
        sdbgCondiciones.Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.ValorEstatico
        sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = 0
        sdbgCondiciones.Columns("GRUPO_VALOR").Value = ""
        sdbgCondiciones.Columns("GRUPO_VALOR").Locked = True
        sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
        sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = 0
        sdbgCondiciones.Columns("CAMPO_VALOR").Value = ""
        sdbgCondiciones.Columns("CAMPO_VALOR").Locked = True
        sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
    Else
        sdbgCondiciones.Columns("DIRECTO").Value = False
        sdbgCondiciones.Columns("DIRECTO").Locked = False
        sdbgCondiciones.Columns("DIRECTO").CellStyleSet "Normal", sdbgCondiciones.Row
        sdbgCondiciones.Columns("TIPO_VALOR").Value = ""
        sdbgCondiciones.Columns("VALOR").Value = ""
        sdbgCondiciones.Columns("VALOR").Locked = True
        sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
        sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hWnd
        sdbgCondiciones.Columns("GRUPO_VALOR").Locked = False
        sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
        sdbgCondiciones.Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hWnd
        sdbgCondiciones.Columns("CAMPO_VALOR").Locked = False
        sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
    End If
    
    CondicionSeleccionada.Campo = sdbgCondiciones.Columns("ID_CAMPO").Value
    CondicionSeleccionada.cargarCampo
   
    If Not estaBloqueadaMoneda Then
        bloquearMoneda False
        sdbgCondiciones.Columns("MONEDA").DropDownHwnd = sdbddMoneda.hWnd
        If sdbgCondiciones.Columns("MONEDA").Value = "" Then
            sdbgCondiciones.Columns("MONEDA").Value = gParametrosGenerales.gsMONCEN
            CondicionSeleccionada.Moneda = gParametrosGenerales.gsMONCEN
        End If
    Else
        bloquearMoneda True
        sdbgCondiciones.Columns("MONEDA").DropDownHwnd = 0
        sdbgCondiciones.Columns("MONEDA").Value = ""
        CondicionSeleccionada.Moneda = ""
    End If

End Sub

Private Sub sdbddCampo_DropDown()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim sDenCampo As String
    Dim iTipoCampo As TipoCampoCondicionEnlace
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddCampo.RemoveAll
            
    If sdbgCondiciones.Columns("ID_GRUPO").Value <> "" Then
        If CLng(sdbgCondiciones.Columns("ID_GRUPO").Value) > 0 Then
            iTipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario
        End If
    End If
    If iTipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario Then
        Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
        oGrupo.Id = sdbgCondiciones.Columns("ID_GRUPO").Value
        Set oGrupo.Formulario = oFSGSRaiz.Generar_CFormulario 'Lo pongo para enga�ar al objeto y que cargue los datos de FORM_CAMPO
        oGrupo.CargarTodosLosCampos
        For Each oCampo In oGrupo.Campos
            If oCampo.Tipo <> TipoDesglose And oCampo.CampoPadre Is Nothing Then
                For i = 1 To oCampo.Denominaciones.Count
                    If oCampo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                        sDenCampo = oCampo.Denominaciones.Item(i).Den
                        Exit For
                    End If
                Next
                sdbddCampo.AddItem TipoCampoCondicionEnlace.CampoDeFormulario & Chr(m_lSeparador) & oCampo.Id & Chr(m_lSeparador) & sDenCampo
             End If
        Next
        If sdbddCampo.Rows = 0 Then
            sdbddCampo.AddItem 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
        End If
    Else
        For i = LBound(m_arCamposGenericos) To UBound(m_arCamposGenericos)
            sdbddCampo.AddItem i & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(i)
        Next
    End If
    
    
    
    Set oGrupo = Nothing
    Set oCampo = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddCampo_InitColumnProps()
    sdbddCampo.DataFieldList = "Column 0, Column 1"
    sdbddCampo.DataFieldToDisplay = "Column 2"
End Sub

Private Sub sdbddCampo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddCampo.MoveFirst

    If sdbgCondiciones.Columns("CAMPO").Value <> "" Then
        For i = 0 To sdbddCampo.Rows - 1
            bm = sdbddCampo.GetBookmark(i)
            If UCase(sdbgCondiciones.Columns("CAMPO").Value) = UCase(Mid(sdbddCampo.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO").Value))) Then
                sdbgCondiciones.Columns("CAMPO").Value = Mid(sdbddCampo.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO").Value))
                sdbddCampo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddCampoValor_CloseUp()
    If sdbgCondiciones.Columns("CAMPO_VALOR").Value = "" Then Exit Sub

    sdbgCondiciones.Columns("TIPO_VALOR").Value = sdbddCampoValor.Columns(0).Value
    sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = sdbddCampoValor.Columns(1).Value
    sdbgCondiciones.Columns("CAMPO_VALOR").Value = sdbddCampoValor.Columns(2).Value
    
End Sub

Private Sub sdbddCampoValor_DropDown()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim sDenCampo As String
    Dim iTipoCampo As TipoCampoCondicionEnlace
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddCampoValor.RemoveAll
    
    If sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value <> "" Then
        If CLng(sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value) > 0 Then
            iTipoCampo = TipoValorCondicionEnlace.CampoDeFormulario
        End If
    End If
    If iTipoCampo = TipoCampoCondicionEnlace.CampoDeFormulario Then
        Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
        oGrupo.Id = sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value
        Set oGrupo.Formulario = oFSGSRaiz.Generar_CFormulario 'Lo pongo para enga�ar al objeto y que cargue los datos de FORM_CAMPO
        oGrupo.CargarTodosLosCampos
        For Each oCampo In oGrupo.Campos
            If oCampo.Tipo <> TipoDesglose And oCampo.CampoPadre Is Nothing Then
                For i = 1 To oCampo.Denominaciones.Count
                    If oCampo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                        sDenCampo = oCampo.Denominaciones.Item(i).Den
                        Exit For
                    End If
                Next
                sdbddCampoValor.AddItem TipoValorCondicionEnlace.CampoDeFormulario & Chr(m_lSeparador) & oCampo.Id & Chr(m_lSeparador) & sDenCampo
            End If
        Next
    Else
        For i = LBound(m_arCamposGenericos) To UBound(m_arCamposGenericos)
            sdbddCampoValor.AddItem i & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & m_arCamposGenericos(i)
        Next
    End If
    If sdbddCampoValor.Rows = 0 Then
        sdbddCampoValor.AddItem 0 & Chr(m_lSeparador) & 0 & Chr(m_lSeparador) & ""
    End If
    
    Set oGrupo = Nothing
    Set oCampo = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddCampoValor_InitColumnProps()
    sdbddCampoValor.DataFieldList = "Column 2"
    
End Sub

Private Sub sdbddCampoValor_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddCampoValor.MoveFirst

    If sdbgCondiciones.Columns("CAMPO_VALOR").Value <> "" Then
        For i = 0 To sdbddCampoValor.Rows - 1
            bm = sdbddCampoValor.GetBookmark(i)
            If UCase(sdbgCondiciones.Columns("CAMPO_VALOR").Value) = UCase(Mid(sdbddCampoValor.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO_VALOR").Value))) Then
                sdbgCondiciones.Columns("CAMPO_VALOR").Value = Mid(sdbddCampoValor.Columns(2).CellText(bm), 1, Len(sdbgCondiciones.Columns("CAMPO_VALOR").Value))
                sdbddCampoValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddGrupo_CloseUp()
    If sdbgCondiciones.Columns("GRUPO").Value = "" Then Exit Sub
    
    sdbgCondiciones.Columns("ID_GRUPO").Value = sdbddGrupo.Columns(0).Value
    sdbgCondiciones.Columns("GRUPO").Value = sdbddGrupo.Columns(1).Value
    
    sdbgCondiciones.Columns("DIRECTO").Value = False
    sdbgCondiciones.Columns("DIRECTO").Locked = False
    sdbgCondiciones.Columns("DIRECTO").CellStyleSet "Normal", sdbgCondiciones.Row
    sdbgCondiciones.Columns("TIPO_VALOR").Value = ""
    sdbgCondiciones.Columns("VALOR").Value = ""
    sdbgCondiciones.Columns("VALOR").Locked = True
    sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
    sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hWnd
    sdbgCondiciones.Columns("GRUPO_VALOR").Locked = False
    sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
    sdbgCondiciones.Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hWnd
    sdbgCondiciones.Columns("CAMPO_VALOR").Locked = False
    sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
End Sub

Private Sub sdbddGrupo_DropDown()
    Dim oFormulario As CFormulario
    Dim oGrupo As CFormGrupo
    Dim sDenGrupo As String
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddGrupo.RemoveAll
    
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = m_lIdFormulario
    oFormulario.CargarTodosLosGrupos

    sdbddGrupo.AddItem 0 & Chr(m_lSeparador) & m_sCampoGenerico
    For Each oGrupo In oFormulario.Grupos
        For i = 1 To oGrupo.Denominaciones.Count
            If oGrupo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                sDenGrupo = oGrupo.Denominaciones.Item(i).Den
                Exit For
            End If
        Next
        sdbddGrupo.AddItem oGrupo.Id & Chr(m_lSeparador) & sDenGrupo
    Next
    
    Set oFormulario = Nothing
    Set oGrupo = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddGrupo_InitColumnProps()
    sdbddGrupo.DataFieldList = "Column 0"
    sdbddGrupo.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddGrupo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddGrupo.MoveFirst

    If sdbgCondiciones.Columns("GRUPO").Value <> "" Then
        For i = 0 To sdbddGrupo.Rows - 1
            bm = sdbddGrupo.GetBookmark(i)
            If UCase(sdbgCondiciones.Columns("GRUPO").Value) = UCase(Mid(sdbddGrupo.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO").Value))) Then
                sdbgCondiciones.Columns("GRUPO").Value = Mid(sdbddGrupo.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO").Value))
                sdbddGrupo.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub

Private Sub sdbddGrupoValor_CloseUp()
    If sdbgCondiciones.Columns("GRUPO_VALOR").Value = "" Then Exit Sub

    sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = sdbddGrupoValor.Columns(0).Value
    sdbgCondiciones.Columns("GRUPO_VALOR").Value = sdbddGrupoValor.Columns(1).Value
    
End Sub

Private Sub sdbddGrupoValor_DropDown()
    Dim oFormulario As CFormulario
    Dim oGrupo As CFormGrupo
    Dim sDenGrupo As String
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddGrupoValor.RemoveAll
    
    sdbddGrupoValor.AddItem 0 & Chr(m_lSeparador) & m_sCampoGenerico
    
    Set oFormulario = oFSGSRaiz.Generar_CFormulario
    oFormulario.Id = m_lIdFormulario
    oFormulario.CargarTodosLosGrupos

    For Each oGrupo In oFormulario.Grupos
        For i = 1 To oGrupo.Denominaciones.Count
            If oGrupo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                sDenGrupo = oGrupo.Denominaciones.Item(i).Den
                Exit For
            End If
        Next
        sdbddGrupoValor.AddItem oGrupo.Id & Chr(m_lSeparador) & sDenGrupo
    Next
    
    Set oFormulario = Nothing
    Set oGrupo = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddGrupoValor_InitColumnProps()
    sdbddGrupoValor.DataFieldList = "Column 0"
    sdbddGrupoValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddGrupoValor_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddGrupoValor.MoveFirst

    If sdbgCondiciones.Columns("GRUPO_VALOR").Value <> "" Then
        For i = 0 To sdbddGrupoValor.Rows - 1
            bm = sdbddGrupoValor.GetBookmark(i)
            If UCase(sdbgCondiciones.Columns("GRUPO_VALOR").Value) = UCase(Mid(sdbddGrupoValor.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO_VALOR").Value))) Then
                sdbgCondiciones.Columns("GRUPO_VALOR").Value = Mid(sdbddGrupoValor.Columns(1).CellText(bm), 1, Len(sdbgCondiciones.Columns("GRUPO_VALOR").Value))
                sdbddGrupoValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddOperador_CloseUp()
    If sdbgCondiciones.Columns("OPERADOR").Value = "" Then Exit Sub

    sdbgCondiciones.Columns("OPERADOR").Value = sdbddOperador.Columns(0).Value
    
End Sub

Private Sub sdbddOperador_DropDown()
    Dim oCampo As CFormItem
    Dim iTipoCampo As TiposDeAtributos
    
    Screen.MousePointer = vbHourglass
    sdbddOperador.RemoveAll
    
    If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
        If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) > 0 Then
            Select Case CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value)
                Case TipoCampoCondicionEnlace.CampoDeFormulario
                    If sdbgCondiciones.Columns("ID_CAMPO").Value <> "" Then
                        If CLng(sdbgCondiciones.Columns("ID_CAMPO").Value) > 0 Then
                            Set oCampo = oFSGSRaiz.Generar_CFormCampo
                            oCampo.Id = sdbgCondiciones.Columns("ID_CAMPO").Value
                            oCampo.CargarDatosFormCampo
                            iTipoCampo = oCampo.Tipo
                        End If
                    End If
                Case TipoCampoCondicionEnlace.Peticionario, TipoCampoCondicionEnlace.DepPetcionario, TipoCampoCondicionEnlace.UONPeticionario
                    iTipoCampo = TiposDeAtributos.TipoString
                Case TipoCampoCondicionEnlace.NumProcesAbiertos, TipoCampoCondicionEnlace.ImporteAdjudicado, TipoCampoCondicionEnlace.ImporteSolicitud, TipoCampoCondicionEnlace.ImporteEmitido, TipoCampoCondicionEnlace.ImporteAbierto
                    iTipoCampo = TiposDeAtributos.TipoNumerico
            End Select
            
            Select Case iTipoCampo
                Case TiposDeAtributos.TipoBoolean
                    sdbddOperador.AddItem "="
                Case TiposDeAtributos.TipoNumerico, TiposDeAtributos.TipoFecha
                    sdbddOperador.AddItem ">"
                    sdbddOperador.AddItem "<"
                    sdbddOperador.AddItem ">="
                    sdbddOperador.AddItem "<="
                    sdbddOperador.AddItem "="
                    sdbddOperador.AddItem "<>"
                Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoMedio
                    sdbddOperador.AddItem "="
                    sdbddOperador.AddItem "Like"
                    sdbddOperador.AddItem "<>"
            End Select
        End If
    End If
    
    If sdbddOperador.Rows = 0 Then
        sdbddOperador.AddItem ""
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddOperador_InitColumnProps()
    sdbddOperador.DataFieldList = "Column 0"
    sdbddOperador.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddOperador_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddOperador.MoveFirst

    If sdbgCondiciones.Columns("OPERADOR").Value <> "" Then
        For i = 0 To sdbddOperador.Rows - 1
            bm = sdbddOperador.GetBookmark(i)
            If UCase(sdbgCondiciones.Columns("OPERADOR").Value) = UCase(Mid(sdbddOperador.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("OPERADOR").Value))) Then
                sdbgCondiciones.Columns("OPERADOR").Value = Mid(sdbddOperador.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("OPERADOR").Value))
                sdbddOperador.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValorBooleano_CloseUp()
    If sdbgCondiciones.Columns("VALOR").Value = "" Then Exit Sub

    sdbgCondiciones.Columns("VALOR").Value = sdbddValorBooleano.Columns(0).Value
    
End Sub

Private Sub sdbddValorBooleano_DropDown()
    Screen.MousePointer = vbHourglass
    sdbddValorBooleano.RemoveAll
    
    sdbddValorBooleano.AddItem m_sValorBooleano(Abs(CInt(True)))
    sdbddValorBooleano.AddItem m_sValorBooleano(Abs(CInt(False)))
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddValorBooleano_InitColumnProps()
    sdbddValorBooleano.DataFieldList = "Column 0"
    sdbddValorBooleano.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValorBooleano_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddValorBooleano.MoveFirst

    If sdbgCondiciones.Columns("VALOR").Value <> "" Then
        For i = 0 To sdbddValorBooleano.Rows - 1
            bm = sdbddValorBooleano.GetBookmark(i)
            If UCase(sdbgCondiciones.Columns("VALUE").Value) = UCase(Mid(sdbddValorBooleano.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("VALOR").Value))) Then
                sdbgCondiciones.Columns("VALOR").Value = Mid(sdbddValorBooleano.Columns(0).CellText(bm), 1, Len(sdbgCondiciones.Columns("VALOR").Value))
                sdbddValorBooleano.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgCondiciones_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgCondiciones.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgCondiciones.GetBookmark(0)) Then
        sdbgCondiciones.Bookmark = sdbgCondiciones.GetBookmark(-1)
    Else
        sdbgCondiciones.Bookmark = sdbgCondiciones.GetBookmark(0)
    End If
    
    If Me.Visible Then sdbgCondiciones.SetFocus
End Sub

Private Sub sdbgCondiciones_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0

    If IsEmpty(sdbgCondiciones.GetBookmark(0)) Then
        sdbgCondiciones.Bookmark = sdbgCondiciones.GetBookmark(-1)
    Else
        sdbgCondiciones.Bookmark = sdbgCondiciones.GetBookmark(0)
    End If
End Sub

Private Sub sdbgCondiciones_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
      
    If Not m_oCondicionEnEdicion Is Nothing Then
        sdbgCondiciones.Columns("FECACT").Value = m_oCondicionEnEdicion.FECACT
    End If
    
    Set m_oCondicionEnEdicion = Nothing
End Sub

Private Sub sdbgCondiciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub


Private Sub sdbgCondiciones_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim sValor As String
    Dim sUON1 As String
    Dim sUON2 As String
    Dim sUON3 As String
    
    sdbgCondiciones.Columns("VALOR").Mask = ""
    sdbgCondiciones.Columns("VALOR").PromptInclude = False
    'Comprobar datos Obligatorios
    If sdbgCondiciones.Columns("COD").Value = "" Then
        m_bError = True
        oMensajes.NoValido "ID"
        GoTo Salir
    ElseIf IsNumeric(sdbgCondiciones.Columns("COD").Value) Then
        m_bError = True
        oMensajes.NoValido "ID"
        GoTo Salir
    End If
    If sdbgCondiciones.Columns("TIPO_CAMPO").Value = "" Or sdbgCondiciones.Columns("TIPO_CAMPO").Value = "0" Then
        m_bError = True
        oMensajes.NoValido m_sMensajesCondicion(2)
        GoTo Salir
    End If
    If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.CampoDeFormulario And _
            (sdbgCondiciones.Columns("ID_CAMPO").Value = "" Or sdbgCondiciones.Columns("ID_CAMPO").Value = "0") Then
        m_bError = True
        oMensajes.NoValido m_sMensajesCondicion(2)
        GoTo Salir
    End If
    If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.UONPeticionario And _
            sdbgCondiciones.Columns("VALOR").Value <> "" Then
        sUON1 = Mid(sdbgCondiciones.Columns("VALOR").Value, 1, basParametros.gLongitudesDeCodigos.giLongCodUON1)
        sUON2 = Mid(sdbgCondiciones.Columns("VALOR").Value, basParametros.gLongitudesDeCodigos.giLongCodUON1 + 2, basParametros.gLongitudesDeCodigos.giLongCodUON2)
        sUON3 = Mid(sdbgCondiciones.Columns("VALOR").Value, basParametros.gLongitudesDeCodigos.giLongCodUON1 + basParametros.gLongitudesDeCodigos.giLongCodUON2 + 3, basParametros.gLongitudesDeCodigos.giLongCodUON3)
        If (Trim(sUON1) = "" And Trim(sUON2) = "" And Trim(sUON3) = "") Or (Trim(sUON3) <> "" And Trim(sUON2) = "") Or (Trim(sUON2) <> "" And Trim(sUON1) = "") Then
            m_bError = True
            oMensajes.NoValido m_sMensajesCondicion(5)
            GoTo Salir
        End If
        
    End If
    If sdbgCondiciones.Columns("OPERADOR").Value = "" Then
        m_bError = True
        oMensajes.NoValido m_sMensajesCondicion(3)
        GoTo Salir
    End If
    'Establecer TIPO_VALOR por si no se ha tocado:
    If sdbgCondiciones.Columns("TIPO_VALOR").Value = "" Or sdbgCondiciones.Columns("TIPO_VALOR").Value = "0" Then
        If sdbgCondiciones.Columns("DIRECTO").Value = True Then
            sdbgCondiciones.Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.ValorEstatico
        Else
            sdbgCondiciones.Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.CampoDeFormulario
        End If
    End If
    If CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.CampoDeFormulario And _
            (sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = "" Or sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = "0") Then
        m_bError = True
        oMensajes.NoValido m_sMensajesCondicion(2)
        GoTo Salir
    End If
    If CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ValorEstatico And _
            sdbgCondiciones.Columns("VALOR").Value = "" Then
        m_bError = True
        oMensajes.NoValido m_sMensajesCondicion(5)
        GoTo Salir
    End If
    
    'Comprobar que los tipos de datos coinciden
    Dim iTipoCampo As TiposDeAtributos
    Dim iTipoCampoValor As TiposDeAtributos
    Dim oCampo As CFormItem
    Dim oCampoValor As CFormItem
    Dim vValorConvertido As Variant
    Select Case CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value)
        Case TipoCampoCondicionEnlace.CampoDeFormulario
            If sdbgCondiciones.Columns("ID_CAMPO").Value <> "" Then
                If CLng(sdbgCondiciones.Columns("ID_CAMPO").Value) > 0 Then
                    Set oCampo = oFSGSRaiz.Generar_CFormCampo
                    oCampo.Id = sdbgCondiciones.Columns("ID_CAMPO").Value
                    oCampo.CargarDatosFormCampo
                    iTipoCampo = oCampo.Tipo
                End If
            End If
        Case TipoCampoCondicionEnlace.Peticionario, TipoCampoCondicionEnlace.DepPetcionario, TipoCampoCondicionEnlace.UONPeticionario
            iTipoCampo = TiposDeAtributos.TipoString
        Case TipoCampoCondicionEnlace.NumProcesAbiertos, TipoCampoCondicionEnlace.ImporteAdjudicado, TipoCampoCondicionEnlace.ImporteSolicitud, TipoCampoCondicionEnlace.ImporteEmitido, TipoCampoCondicionEnlace.ImporteAbierto
            iTipoCampo = TiposDeAtributos.TipoNumerico
    End Select
    If Not sdbgCondiciones.Columns("DIRECTO").Value Then
        Select Case CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value)
            Case TipoValorCondicionEnlace.CampoDeFormulario
                If sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value <> "" Then
                    If CLng(sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value) > 0 Then
                        Set oCampoValor = oFSGSRaiz.Generar_CFormCampo
                        oCampoValor.Id = sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value
                        oCampoValor.CargarDatosFormCampo
                        iTipoCampoValor = oCampoValor.Tipo
                    End If
                End If
            Case TipoCampoCondicionEnlace.Peticionario, TipoCampoCondicionEnlace.DepPetcionario, TipoCampoCondicionEnlace.UONPeticionario
                iTipoCampoValor = TiposDeAtributos.TipoString
            Case TipoCampoCondicionEnlace.NumProcesAbiertos, TipoCampoCondicionEnlace.ImporteAdjudicado, TipoCampoCondicionEnlace.ImporteSolicitud, TipoCampoCondicionEnlace.ImporteEmitido, TipoCampoCondicionEnlace.ImporteAbierto
                iTipoCampoValor = TiposDeAtributos.TipoNumerico
        End Select
    End If
    
    sValor = sdbgCondiciones.Columns("VALOR").Value
    
    On Error GoTo Error
    Select Case iTipoCampo
        Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
            If iTipoCampoValor > 0 Then
                If iTipoCampoValor <> TipoString And iTipoCampoValor <> TipoTextoCorto And iTipoCampoValor <> TipoTextoMedio And iTipoCampoValor <> TipoTextoLargo Then
                    err.Raise 13 'Type mismatch
                End If
            Else
               vValorConvertido = CStr(sdbgCondiciones.Columns("VALOR").Value)
            End If
        Case TiposDeAtributos.TipoNumerico
            If iTipoCampoValor > 0 Then
                If Not iTipoCampoValor = TipoNumerico Then
                    err.Raise 13 'Type mismatch
                End If
            Else
               vValorConvertido = CDbl(sdbgCondiciones.Columns("VALOR").Value)
            End If
        Case TiposDeAtributos.TipoFecha
            If iTipoCampoValor > 0 Then
                If Not iTipoCampoValor = TipoFecha Then
                    err.Raise 13 'Type mismatch
                End If
            Else
               vValorConvertido = CDate(sdbgCondiciones.Columns("VALOR").Value)
            End If
        Case TiposDeAtributos.TipoBoolean
            If iTipoCampoValor > 0 Then
                If Not iTipoCampoValor = TipoBoolean Then
                    err.Raise 13 'Type mismatch
                End If
            Else
                If sdbgCondiciones.Columns("VALOR").Value <> m_sValorBooleano(0) And sdbgCondiciones.Columns("VALOR").Value <> m_sValorBooleano(1) Then
                    err.Raise 13 'Type mismatch
                Else
                    If sdbgCondiciones.Columns("VALOR").Value = m_sValorBooleano(0) Then
                        sValor = "0"
                    ElseIf sdbgCondiciones.Columns("VALOR").Value = m_sValorBooleano(1) Then
                        sValor = "1"
                    End If
                End If
            End If
    End Select
    
    'Guardar Datos
     If sdbgCondiciones.IsAddRow Then
        Set m_oCondicionAnyadir = oFSGSRaiz.Generar_CCondBloqueoCond
        m_oCondicionAnyadir.Cod = Trim(sdbgCondiciones.Columns("COD").Value)
        m_oCondicionAnyadir.IDCOD = m_lIdCond
        m_oCondicionAnyadir.tipoCampo = CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value)
        m_oCondicionAnyadir.Campo = CLng(sdbgCondiciones.Columns("ID_CAMPO").Value)
        m_oCondicionAnyadir.Operador = sdbgCondiciones.Columns("OPERADOR").Value
        m_oCondicionAnyadir.TipoValor = CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value)
        m_oCondicionAnyadir.CampoValor = CLng(sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value)
        m_oCondicionAnyadir.valor = sValor
        If sdbgCondiciones.Columns("MONEDA").Value = "" Then
            If m_oCondicionAnyadir.incluyeCampoImporte Or m_arCamposGenericos(6) = sdbddCampo.Columns(2).Value Or m_arCamposGenericos(7) = sdbddCampo.Columns(2).Value Then
                sdbgCondiciones.Columns("MONEDA").Value = gParametrosGenerales.gsMONCEN
            End If
        End If
        m_oCondicionAnyadir.Moneda = sdbgCondiciones.Columns("MONEDA").Value
       
        Set m_oIBAseDatosEnEdicion = m_oCondicionAnyadir
        teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bAnyaError = True
            If Me.Visible Then sdbgCondiciones.SetFocus
            sdbgCondiciones.CancelUpdate
            GoTo Salir
        Else
            basSeguridad.RegistrarAccion accionessummit.ACCCondicionBloqueoAnya, "Condicion Particular:" & m_lIdCond & ",ID:" & m_oCondicionAnyadir.Id
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If

        sdbgCondiciones.Columns("ID").Value = m_oCondicionAnyadir.Id
        sdbgCondiciones.Columns("FECACT").Value = m_oCondicionAnyadir.FECACT
        m_oCondiciones.AddCondicion m_oCondicionAnyadir
        m_bAnyaError = False

    Else
        m_oCondicionEnEdicion.IDCOD = m_lIdCond
        m_oCondicionEnEdicion.Cod = Trim(sdbgCondiciones.Columns("COD").Value)
        m_oCondicionEnEdicion.tipoCampo = CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value)
        m_oCondicionEnEdicion.Campo = CLng(sdbgCondiciones.Columns("ID_CAMPO").Value)
        m_oCondicionEnEdicion.Operador = sdbgCondiciones.Columns("OPERADOR").Value
        m_oCondicionEnEdicion.TipoValor = CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value)
        m_oCondicionEnEdicion.CampoValor = CLng(sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value)
        m_oCondicionEnEdicion.valor = sValor
        If sdbgCondiciones.Columns("MONEDA").Value = "" Then
            If m_oCondicionEnEdicion.incluyeCampoImporte Or m_arCamposGenericos(6) = sdbddCampo.Columns(2).Value Or m_arCamposGenericos(7) = sdbddCampo.Columns(2).Value Then
                sdbgCondiciones.Columns("MONEDA").Value = gParametrosGenerales.gsMONCEN
            End If
        End If
        m_oCondicionEnEdicion.Moneda = sdbgCondiciones.Columns("MONEDA").Value
        
        Set m_oIBAseDatosEnEdicion = m_oCondicionEnEdicion
        teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bModError = True
            If Me.Visible Then sdbgCondiciones.SetFocus
            sdbgCondiciones.DataChanged = False
            GoTo Salir
        Else
            ''' Registro de acciones
            sdbgCondiciones.Columns("FECACT").Value = m_oCondicionEnEdicion.FECACT
            basSeguridad.RegistrarAccion accionessummit.ACCCondicionBloqueoModif, "Condicion Particular:" & m_lIdCond & ",ID:" & m_oCondicionEnEdicion.Id
            m_bModError = False
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If

        Set m_oCondicionEnEdicion = Nothing

    End If

    Set m_oCondicionAnyadir = Nothing
    Exit Sub
    
Salir:
    If Me.Visible Then sdbgCondiciones.SetFocus
    Cancel = True
    Exit Sub
    
Error:
    If err.Number = 13 Then 'Type mismatch
        Cancel = True
        oMensajes.NoValido m_sMensajesCondicion(6)
        If Me.Visible Then sdbgCondiciones.SetFocus
        m_bError = True
        Exit Sub
    End If
End Sub

Private Sub sdbgCondiciones_Change()
    Dim teserror As TipoErrorSummit
    
    If sdbgCondiciones.Columns(sdbgCondiciones.col).Name = "MONEDA" Then
        Exit Sub
    End If
    
    If Not sdbgCondiciones.IsAddRow Then
        Set m_oCondicionEnEdicion = Nothing
        Set m_oCondicionEnEdicion = m_oCondiciones.Item(CStr(sdbgCondiciones.Columns("ID").Value))
        
        Set m_oIBAseDatosEnEdicion = m_oCondicionEnEdicion
            
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            sdbgCondiciones.DataChanged = False
            
            CargarCondiciones
            
            teserror.NumError = TESnoerror
                
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgCondiciones.SetFocus
        End If
               
    End If
    
    If sdbgCondiciones.col >= 0 Then
        Select Case sdbgCondiciones.Columns(sdbgCondiciones.col).Name
            Case "DIRECTO"
                If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
                    If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.UONPeticionario Then
                        sdbgCondiciones.Columns("DIRECTO").Value = True
                    End If
                End If
                If sdbgCondiciones.Columns("DIRECTO").Value = True Then
                    sdbgCondiciones.Columns("VALOR").Locked = False
                    sdbgCondiciones.Columns("VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                    If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
                        If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.UONPeticionario Then
                            sdbgCondiciones.Columns("DIRECTO").Locked = True
                            sdbgCondiciones.Columns("DIRECTO").CellStyleSet "Gris", sdbgCondiciones.Row
                        End If
                    End If
                    sdbgCondiciones.Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.ValorEstatico
                    sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = 0
                    sdbgCondiciones.Columns("GRUPO_VALOR").Value = ""
                    sdbgCondiciones.Columns("GRUPO_VALOR").Locked = True
                    sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                    sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = 0
                    sdbgCondiciones.Columns("CAMPO_VALOR").Value = ""
                    sdbgCondiciones.Columns("CAMPO_VALOR").Locked = True
                    sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                Else
                    sdbgCondiciones.Columns("VALOR").Value = ""
                    sdbgCondiciones.Columns("VALOR").Locked = True
                    sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                    sdbgCondiciones.Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.CampoDeFormulario
                    sdbgCondiciones.Columns("GRUPO_VALOR").Locked = False
                    sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hWnd
                    sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                    sdbgCondiciones.Columns("CAMPO_VALOR").Locked = False
                    sdbgCondiciones.Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hWnd
                    sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                End If
            Case "GRUPO"
                sdbgCondiciones.Columns("ID_CAMPO").Value = 0
                sdbgCondiciones.Columns("CAMPO").Value = ""
                sdbgCondiciones.Columns("OPERADOR").Value = ""
            Case "CAMPO"
                If sdbgCondiciones.Columns("CAMPO").Value = "" Then
                    sdbgCondiciones.Columns("TIPO_CAMPO").Value = 0
                    sdbgCondiciones.Columns("GRUPO").Locked = False
                    sdbgCondiciones.Columns("GRUPO").DropDownHwnd = sdbddGrupo.hWnd
                    sdbgCondiciones.Columns("GRUPO").CellStyleSet "Normal", sdbgCondiciones.Row
                End If
            Case "GRUPO_VALOR"
                sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = 0
                sdbgCondiciones.Columns("CAMPO_VALOR").Value = ""
        End Select
    End If
End Sub



Private Sub sdbgCondiciones_InitColumnProps()
'''
    sdbgCondiciones.Columns("GRUPO").caption = m_sGrupo
    sdbgCondiciones.Columns("CAMPO").caption = m_sCampo
    sdbgCondiciones.Columns("OPERADOR").caption = m_sOperador
    sdbgCondiciones.Columns("DIRECTO").caption = m_sDirecto
    sdbgCondiciones.Columns("VALOR").caption = m_sValor
    sdbgCondiciones.Columns("GRUPO_VALOR").caption = m_sGrupo
    sdbgCondiciones.Columns("CAMPO_VALOR").caption = m_sCampo
End Sub

Private Sub sdbgCondiciones_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete And sdbgCondiciones.col = -1 Then
            cmdEliminarCondicion_Click
        Exit Sub
    End If
End Sub

Private Sub sdbgCondiciones_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgCondiciones.DataChanged Then
                sdbgCondiciones.Update
                If m_bError Then
                    Exit Sub
                End If
            End If
        Case vbKeyBack
            If sdbgCondiciones.col >= 0 Then
                Select Case sdbgCondiciones.Columns(sdbgCondiciones.col).Name
                    Case "GRUPO"
                        sdbgCondiciones.Columns("TIPO_CAMPO").Value = 0
                        sdbgCondiciones.Columns("ID_GRUPO").Value = 0
                        sdbgCondiciones.Columns("GRUPO").Value = " "
                        sdbgCondiciones.Columns("ID_CAMPO").Value = 0
                        sdbgCondiciones.Columns("CAMPO").Value = ""
                        sdbgCondiciones.Columns("OPERADOR").Value = ""
                        sdbgCondiciones.col = sdbgCondiciones.Columns("COD").Position
                    Case "CAMPO"
                        sdbgCondiciones.Columns("ID_CAMPO").Value = 0
                        sdbgCondiciones.Columns("CAMPO").Value = ""
                        sdbgCondiciones.Columns("TIPO_CAMPO").Value = 0
                        sdbgCondiciones.Columns("GRUPO").Locked = False
                        sdbgCondiciones.Columns("GRUPO").DropDownHwnd = sdbddGrupo.hWnd
                        sdbgCondiciones.Columns("GRUPO").CellStyleSet "Normal", sdbgCondiciones.Row
                        sdbgCondiciones.col = sdbgCondiciones.Columns("GRUPO").Position
                    Case "OPERADOR"
                        sdbgCondiciones.Columns("OPERADOR").Value = ""
                    Case "GRUPO_VALOR"
                        sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = 0
                        sdbgCondiciones.Columns("GRUPO_VALOR").Value = ""
                        sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = 0
                        sdbgCondiciones.Columns("CAMPO_VALOR").Value = ""
                        sdbgCondiciones.col = sdbgCondiciones.Columns("OPERADOR").Position
                    Case "CAMPO_VALOR"
                        sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = 0
                        sdbgCondiciones.Columns("CAMPO_VALOR").Value = ""
                        sdbgCondiciones.col = sdbgCondiciones.Columns("GRUPO_VALOR").Position
                End Select
            End If
    End Select
End Sub

Private Sub sdbgCondiciones_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bMalaColumna As Boolean
    Dim iIndexMensaje As Integer
    Dim sColumnaARellenar As String
    
    Set m_oCondicionEnEdicion = m_oCondiciones.Item(CStr(sdbgCondiciones.Columns("ID").Value))
    If m_oCondicionEnEdicion Is Nothing Then
        Set m_oCondicionEnEdicion = oFSGSRaiz.Generar_CCondBloqueoCond
    End If
    
    If sdbgCondiciones.IsAddRow Then
        cmdAnyadirCondicion.Enabled = False
    Else
        cmdAnyadirCondicion.Enabled = True
    End If

    If sdbgCondiciones.col < 0 Then
        Exit Sub
    End If
    
    If sdbgCondiciones.IsAddRow And Not IsNull(LastRow) Then 'Cambiamos de fila para a�adir
        If sdbgCondiciones.IsAddRow Then
            sdbgCondiciones.Columns("COD").Value = "X" & m_oCondiciones.ObtenerCOD(m_lIdCond)
        End If
        sdbgCondiciones.Columns("GRUPO").CellStyleSet "Normal", sdbgCondiciones.Row
        sdbgCondiciones.Columns("GRUPO").Locked = False
        sdbgCondiciones.Columns("GRUPO").DropDownHwnd = sdbddGrupo.hWnd
        sdbgCondiciones.Columns("VALOR").Locked = True
        sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
        sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hWnd
        sdbgCondiciones.Columns("GRUPO_VALOR").Locked = False
        sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
        sdbgCondiciones.Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hWnd
        sdbgCondiciones.Columns("CAMPO_VALOR").Locked = False
        sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
    End If
    
    If Not sdbgCondiciones.IsAddRow Or IsNull(LastRow) Then 'Si no estamos a�adiendo o Si estamos a�adiendo pero cambiamos de columna
        Select Case sdbgCondiciones.Columns(sdbgCondiciones.col).Name
            Case "COD"
                If sdbgCondiciones.IsAddRow Then
                    sdbgCondiciones.Columns("COD").Value = "X" & m_oCondiciones.ObtenerCOD(m_lIdCond)
                End If
            Case "GRUPO"
                If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
                     sdbgCondiciones.Columns("GRUPO").CellStyleSet "Normal", sdbgCondiciones.Row
                     sdbgCondiciones.Columns("GRUPO").Locked = False
                     sdbgCondiciones.Columns("GRUPO").DropDownHwnd = sdbddGrupo.hWnd
               End If
                
            Case "DIRECTO"
                sdbgCondiciones.Columns("DIRECTO").Locked = False
                sdbgCondiciones.Columns("DIRECTO").CellStyleSet "Normal", sdbgCondiciones.Row
                If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
                    If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.UONPeticionario Then
                        
                        sdbgCondiciones.Columns("DIRECTO").Value = True
                        sdbgCondiciones.Columns("DIRECTO").Locked = True
                        sdbgCondiciones.Columns("DIRECTO").CellStyleSet "Gris", sdbgCondiciones.Row
                        
                        sdbgCondiciones.Columns("VALOR").Locked = False
                        sdbgCondiciones.Columns("VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                        
                        sdbgCondiciones.Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.ValorEstatico
                        
                        sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = 0
                        
                        sdbgCondiciones.Columns("GRUPO_VALOR").Value = ""
                        sdbgCondiciones.Columns("GRUPO_VALOR").Locked = True
                        sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                        
                        sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = 0
                        
                        sdbgCondiciones.Columns("CAMPO_VALOR").Value = ""
                        sdbgCondiciones.Columns("CAMPO_VALOR").Locked = True
                        sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                        
                    End If
                End If
                
            Case "VALOR"
                Dim sValor_aux As String
                sdbgCondiciones.Columns("VALOR").Mask = ""
                sdbgCondiciones.Columns("VALOR").PromptInclude = False
                sValor_aux = sdbgCondiciones.Columns("VALOR").Value
                sdbgCondiciones.Columns("VALOR").Value = ""
                sdbgCondiciones.Columns("VALOR").Value = sValor_aux
                
                If sdbgCondiciones.Columns("TIPO_VALOR").Value <> "" Then
                    If CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) <= TipoValorCondicionEnlace.CampoDeFormulario Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.DepPetcionario Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteAbierto Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteAdjudicado Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteEmitido Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteSolicitud Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.NumProcesAbiertos Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.Peticionario Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.UONPeticionario Then
                        
                        sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                        sdbgCondiciones.Columns("VALOR").Locked = True
                    Else
                        sdbgCondiciones.Columns("VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                        sdbgCondiciones.Columns("VALOR").Locked = False
                        
                        If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
                            If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.CampoDeFormulario Then
                                If sdbgCondiciones.Columns("ID_CAMPO").Value <> "" Then
                                    Dim oCampo As CFormItem
                                    Set oCampo = oFSGSRaiz.Generar_CFormCampo
                                    oCampo.Id = CLng(sdbgCondiciones.Columns("ID_CAMPO").Value)
                                    oCampo.CargarDatosFormCampo
                                    Set m_oCondicionEnEdicion.CampoForm = oCampo
                                   
                                    If oCampo.Tipo = TipoBoolean Then
                                        sdbgCondiciones.Columns("VALOR").Style = ssStyleComboBox
                                        sdbgCondiciones.Columns("VALOR").DropDownHwnd = sdbddValorBooleano.hWnd
                                    Else
                                        sdbgCondiciones.Columns("VALOR").Style = ssStyleEdit
                                    End If
                                End If

                            ElseIf CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.UONPeticionario Then
                                Dim i As Integer
                                For i = 1 To basParametros.gLongitudesDeCodigos.giLongCodUON1
                                    sdbgCondiciones.Columns("VALOR").Mask = sdbgCondiciones.Columns("VALOR").Mask & "&"
                                Next
                                sdbgCondiciones.Columns("VALOR").Mask = sdbgCondiciones.Columns("VALOR").Mask & "-"
                                For i = 1 To basParametros.gLongitudesDeCodigos.giLongCodUON2
                                    sdbgCondiciones.Columns("VALOR").Mask = sdbgCondiciones.Columns("VALOR").Mask & "&"
                                Next
                                sdbgCondiciones.Columns("VALOR").Mask = sdbgCondiciones.Columns("VALOR").Mask & "-"
                                For i = 1 To basParametros.gLongitudesDeCodigos.giLongCodUON3
                                    sdbgCondiciones.Columns("VALOR").Mask = sdbgCondiciones.Columns("VALOR").Mask & "&"
                                Next
                                sdbgCondiciones.Columns("VALOR").PromptInclude = True
                                sdbgCondiciones.Columns("VALOR").Value = ""
                                sdbgCondiciones.Columns("VALOR").Value = sValor_aux
                            End If
                        End If
                    End If
                End If
                
            Case "GRUPO_VALOR", "CAMPO_VALOR"
                If sdbgCondiciones.Columns("TIPO_VALOR").Value <> "" Then
                    If CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) <= TipoValorCondicionEnlace.CampoDeFormulario Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.DepPetcionario Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteAbierto Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteAdjudicado Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteEmitido Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ImporteSolicitud Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.NumProcesAbiertos Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.Peticionario Or _
                        CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.UONPeticionario Then
                        
                        sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                        sdbgCondiciones.Columns("GRUPO_VALOR").Locked = False
                        sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = sdbddGrupoValor.hWnd
                        sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Normal", sdbgCondiciones.Row
                        sdbgCondiciones.Columns("CAMPO_VALOR").Locked = False
                        sdbgCondiciones.Columns("CAMPO_VALOR").DropDownHwnd = sdbddCampoValor.hWnd
                    
                    Else
                        sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                        sdbgCondiciones.Columns("GRUPO_VALOR").Locked = True
                        sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Gris", sdbgCondiciones.Row
                        sdbgCondiciones.Columns("CAMPO_VALOR").Locked = True
                    End If
                End If
            Case "MONEDA"
                    If Not m_oCondiciones.Item(CStr(sdbgCondiciones.Columns("ID").Value)) Is Nothing Then
                        If Not estaBloqueadaMoneda Then
                            sdbgCondiciones.Columns("MONEDA").DropDownHwnd = sdbddMoneda.hWnd
                        Else
                            sdbgCondiciones.Columns("MONEDA").DropDownHwnd = 0
                        End If
                    End If
        End Select
    End If

    bMalaColumna = False
    Select Case sdbgCondiciones.Columns(sdbgCondiciones.col).Name
        Case "CAMPO"
            If Not sdbgCondiciones.Columns("CAMPO").Locked Then
                bMalaColumna = True
                iIndexMensaje = 1
                sColumnaARellenar = "GRUPO"
                If sdbgCondiciones.Columns("ID_GRUPO").Value <> "" Then
                    bMalaColumna = False
                End If
            End If
            
        Case "OPERADOR"
            bMalaColumna = True
            iIndexMensaje = 2
            sColumnaARellenar = "CAMPO"
            If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
                If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) > 0 Then
                    bMalaColumna = False
                End If
            End If
            
        Case "CAMPO_VALOR"
            If Not sdbgCondiciones.Columns("CAMPO_VALOR").Locked Then
                bMalaColumna = True
                iIndexMensaje = 1
                sColumnaARellenar = "GRUPO_VALOR"
                If sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value <> "" Then
                    bMalaColumna = False
                End If
            End If
    End Select
    
    If bMalaColumna Then
        oMensajes.NoValido m_sMensajesCondicion(iIndexMensaje)
        sdbgCondiciones.col = sdbgCondiciones.Columns(sColumnaARellenar).Position
    End If
End Sub

Private Sub sdbgCondiciones_RowLoaded(ByVal Bookmark As Variant)
Dim lIdGrupo As Long
Dim i As Integer
Dim sGrupo As String
Dim lIdGrupoValor As Long
Dim sGrupoValor As String
    If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
        If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) <> TipoCampoCondicionEnlace.CampoDeFormulario Then
            sdbgCondiciones.Columns("ID_GRUPO").Value = 0
            sdbgCondiciones.Columns("GRUPO").Value = m_sCampoGenerico
            sdbgCondiciones.Columns("GRUPO").DropDownHwnd = 0
        Else
            Dim oGrupo As CFormGrupo
            Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
            oGrupo.Id = sdbgCondiciones.Columns("ID_GRUPO").Value
            lIdGrupo = sdbgCondiciones.Columns("ID_GRUPO").Value
            oGrupo.CargarDatosFormGrupo
            For i = 1 To oGrupo.Denominaciones.Count
                If oGrupo.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                    sGrupo = oGrupo.Denominaciones.Item(i).Den
                    Exit For
                End If
            Next
            sdbgCondiciones.Columns("GRUPO").Value = sGrupo
        End If
    End If
    If sdbgCondiciones.Columns("TIPO_CAMPO").Value <> "" Then
        If CInt(sdbgCondiciones.Columns("TIPO_CAMPO").Value) = TipoCampoCondicionEnlace.UONPeticionario Then
            sdbgCondiciones.Columns("DIRECTO").Value = True
            sdbgCondiciones.Columns("TIPO_VALOR").Value = TipoValorCondicionEnlace.ValorEstatico
            sdbgCondiciones.Columns("DIRECTO").Locked = True
            sdbgCondiciones.Columns("DIRECTO").CellStyleSet "Gris"
        End If
    End If
    If sdbgCondiciones.Columns("TIPO_VALOR").Value <> "" Then
        If CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoValorCondicionEnlace.ValorEstatico Then
            sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = 0
            sdbgCondiciones.Columns("GRUPO_VALOR").Value = ""
            sdbgCondiciones.Columns("GRUPO_VALOR").Locked = True
            sdbgCondiciones.Columns("GRUPO_VALOR").CellStyleSet "Gris" ', sdbgCondiciones.Row
            sdbgCondiciones.Columns("ID_CAMPO_VALOR").Value = 0
            sdbgCondiciones.Columns("CAMPO_VALOR").Value = ""
            sdbgCondiciones.Columns("CAMPO_VALOR").Locked = True
            sdbgCondiciones.Columns("CAMPO_VALOR").CellStyleSet "Gris" ', sdbgCondiciones.Row
        Else
            If CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoCampoCondicionEnlace.Peticionario Or _
                CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoCampoCondicionEnlace.DepPetcionario Or _
                CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoCampoCondicionEnlace.ImporteAbierto Or _
                CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoCampoCondicionEnlace.ImporteAdjudicado Or _
                CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoCampoCondicionEnlace.ImporteEmitido Or _
                CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoCampoCondicionEnlace.ImporteSolicitud Or _
                CInt(sdbgCondiciones.Columns("TIPO_VALOR").Value) = TipoCampoCondicionEnlace.NumProcesAbiertos Then
                sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value = 0
                sdbgCondiciones.Columns("GRUPO_VALOR").Value = m_sCampoGenerico
                sdbgCondiciones.Columns("VALOR").Value = ""
                sdbgCondiciones.Columns("VALOR").Locked = True
                sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris"
                sdbgCondiciones.Columns("GRUPO_VALOR").DropDownHwnd = 0
            Else
                sdbgCondiciones.Columns("VALOR").Value = ""
                sdbgCondiciones.Columns("VALOR").Locked = True
                sdbgCondiciones.Columns("VALOR").CellStyleSet "Gris" ', sdbgCondiciones.Row
                Dim oGrupoValor As CFormGrupo
                Set oGrupoValor = oFSGSRaiz.Generar_CFormGrupo
                oGrupoValor.Id = sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value
                lIdGrupoValor = sdbgCondiciones.Columns("ID_GRUPO_VALOR").Value
                oGrupoValor.CargarDatosFormGrupo
                For i = 1 To oGrupoValor.Denominaciones.Count
                    If oGrupoValor.Denominaciones.Item(i).Cod = basParametros.gParametrosGenerales.gIdioma Then
                        sGrupoValor = oGrupoValor.Denominaciones.Item(i).Den
                        Exit For
                    End If
                Next
                sdbgCondiciones.Columns("GRUPO_VALOR").Value = sGrupoValor
            End If
        End If
    End If
    
    If Not IsEmpty(sdbgCondiciones.Columns("ID").Value) Then
        bloquearMoneda estaBloqueadaMoneda
    End If

End Sub


Private Sub txtFormula_Change()
    m_sFormula = Trim(txtFormula.Text)
End Sub

Private Sub txtFormula_GotFocus()
    'si hay cambios los guarda
    If sdbgCondiciones.DataChanged = True Then
        sdbgCondiciones.Update
    End If
End Sub

Private Function ValidarFormula() As Boolean
    Dim sVariables() As String
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim oCondicion As CCondBloqueoCond
    Dim i As Integer
    Dim sCaracter As String
    
    Set iEq = New USPExpression
    
    If Not m_oCondiciones Is Nothing Then
        If m_oCondiciones.Count > 0 Then
            ReDim sVariables(m_oCondiciones.Count)
            i = 0
            Set oCondicion = Nothing
            For Each oCondicion In m_oCondiciones
                sVariables(i) = oCondicion.Cod
                i = i + 1
            Next
        Else
            ReDim sVariables(0)
        End If
    Else
        ReDim sVariables(0)
    End If
    
    If UBound(sVariables) > 0 Or m_sFormula <> "" Or lIdPrecondicion > 0 Then
        lIndex = iEq.Parse(m_sFormula, sVariables, lErrCode)
        
        If lErrCode <> USPEX_NO_ERROR Then
            ' Parsing error handler
            Select Case lErrCode
                Case USPEX_DIVISION_BY_ZERO
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(1))
                Case USPEX_EMPTY_EXPRESSION
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(2))
                Case USPEX_MISSING_OPERATOR
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(3))
                Case USPEX_SYNTAX_ERROR
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(4))
                Case USPEX_UNKNOWN_FUNCTION
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(5))
                Case USPEX_UNKNOWN_OPERATOR
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(6))
                Case USPEX_WRONG_PARAMS_NUMBER
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(7))
                Case USPEX_UNKNOWN_IDENTIFIER
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(8))
                Case USPEX_UNKNOWN_VAR
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(9))
                Case USPEX_VARIABLES_NOTUNIQUE
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(10))
                Case USPEX_UNBALANCED_PAREN
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(12))
                Case Else
                    sCaracter = Mid(m_sFormula, lIndex)
                    oMensajes.FormulaIncorrecta (m_sErrorFormula(11) & vbCrLf & sCaracter)
            End Select
            
            If Me.Visible Then txtFormula.SetFocus
            
            Set iEq = Nothing
            ValidarFormula = False
            Exit Function
        End If
    End If
    Set iEq = Nothing
    ValidarFormula = True
End Function

Private Sub Form_Unload(Cancel As Integer)
    If sdbgCondiciones.DataChanged Then
        m_bError = False
        sdbgCondiciones.Update
        If m_bError Then
            Cancel = True
            Exit Sub
        End If
    End If
    Cancel = CInt(Not ValidarFormula)

    If Not CBool(Cancel) Then
        If m_oCondiciones.Count > 0 Then
            frmBloqueoFlujos.m_bErrorConds = False
        Else
            frmBloqueoFlujos.m_bErrorConds = True
        End If
        frmBloqueoFlujos.m_bBtnClk = False
        Set m_oCondicionEnEdicion = Nothing
        Set m_oCondicionAnyadir = Nothing
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oIBaseDatos = Nothing
        Set m_oCondiciones = Nothing
        lIdPrecondicion = 0
    End If
    
    Set m_oGrupos = Nothing
End Sub

Private Sub cargarComboMonedas()
    Dim sDen As String
    Dim oMoneda As CMoneda
    For Each oMoneda In m_oMonedas
        sDen = ""
        sDen = sDen & Chr(m_lSeparador) & oMoneda.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        Me.sdbddMoneda.AddItem oMoneda.Cod & sDen
    Next
End Sub

Private Sub bloquearMoneda(ByVal Value As Boolean)
    If Value Then
        sdbgCondiciones.Columns("MONEDA").CellStyleSet "Gris", sdbgCondiciones.Row
    Else
        sdbgCondiciones.Columns("MONEDA").CellStyleSet "Normal", sdbgCondiciones.Row
    End If
End Sub

Private Function CondicionSeleccionada() As CCondBloqueoCond
    If sdbgCondiciones.IsAddRow Then
        If m_oCondicionAnyadir Is Nothing Then
            Set m_oCondicionAnyadir = oFSGSRaiz.Generar_CCondBloqueoCond
        End If
        Set CondicionSeleccionada = m_oCondicionAnyadir
    Else
        Set CondicionSeleccionada = m_oCondiciones.Item(CStr(sdbgCondiciones.Columns("ID").Value))
        If CondicionSeleccionada Is Nothing Then
            If Not m_oCondicionEnEdicion Is Nothing Then
                Set CondicionSeleccionada = m_oCondicionEnEdicion
            Else
                Set CondicionSeleccionada = m_oCondiciones.Item(CStr(sdbgCondiciones.Columns("ID").Value))
            End If
        End If
    End If
End Function

Public Function estaBloqueadaMoneda() As Boolean
    estaBloqueadaMoneda = Not (CondicionSeleccionada.incluyeCampoImporte Or m_arCamposGenericos(6) = sdbgCondiciones.Columns("CAMPO").Value Or m_arCamposGenericos(7) = sdbgCondiciones.Columns("CAMPO").Value _
                                    Or m_arCamposGenericos(8) = sdbgCondiciones.Columns("CAMPO").Value Or m_arCamposGenericos(9) = sdbgCondiciones.Columns("CAMPO").Value)
End Function
