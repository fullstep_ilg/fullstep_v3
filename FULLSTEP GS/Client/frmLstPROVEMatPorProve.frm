VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPROVEMatPorProve 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de material por proveedor (opciones)"
   ClientHeight    =   2595
   ClientLeft      =   660
   ClientTop       =   1080
   ClientWidth     =   6390
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPROVEMatPorProve.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2595
   ScaleWidth      =   6390
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   6390
      TabIndex        =   12
      Top             =   2220
      Width           =   6390
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener"
         Height          =   375
         Left            =   5040
         TabIndex        =   11
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2175
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   6375
      _ExtentX        =   11245
      _ExtentY        =   3836
      _Version        =   393216
      Style           =   1
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPROVEMatPorProve.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstPROVEMatPorProve.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Opciones"
      TabPicture(2)   =   "frmLstPROVEMatPorProve.frx":0CEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraTipoPer"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame4 
         Height          =   1455
         Left            =   -74760
         TabIndex        =   19
         Top             =   480
         Width           =   5895
         Begin VB.OptionButton optOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   600
            TabIndex        =   4
            Top             =   450
            Value           =   -1  'True
            Width           =   2910
         End
         Begin VB.OptionButton optOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   600
            TabIndex        =   5
            Top             =   900
            Width           =   2955
         End
      End
      Begin VB.Frame fraTipoPer 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1740
         Left            =   -74880
         TabIndex        =   16
         Top             =   360
         Width           =   6135
         Begin VB.CheckBox chkDatosBasicos 
            Caption         =   "Incluir datos b�sicos"
            Height          =   195
            Left            =   225
            TabIndex        =   6
            Top             =   375
            Width           =   2685
         End
         Begin VB.CheckBox chkCalifica 
            Caption         =   "Incluir datos homologaci�n"
            Height          =   195
            Left            =   2895
            TabIndex        =   7
            Top             =   375
            Width           =   3090
         End
         Begin VB.CheckBox chkObs 
            Caption         =   "Incluir observaciones"
            Height          =   195
            Left            =   2895
            TabIndex        =   9
            Top             =   750
            Width           =   3090
         End
         Begin VB.CheckBox chkContactos 
            Caption         =   "Incluir contactos"
            Height          =   195
            Left            =   225
            TabIndex        =   8
            Top             =   750
            Width           =   2685
         End
         Begin VB.CheckBox chkArticulosHomo 
            Caption         =   "Incluir art�culos homologados"
            Height          =   285
            Left            =   225
            TabIndex        =   10
            Top             =   1275
            Width           =   4650
         End
         Begin VB.Frame Frame3 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   950
            Left            =   120
            TabIndex        =   18
            Top             =   120
            Width           =   5895
         End
         Begin VB.Frame Frame1 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Left            =   120
            TabIndex        =   17
            Top             =   1080
            Width           =   5895
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1635
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   6135
         Begin VB.OptionButton optProve 
            Caption         =   "Proveedor"
            Height          =   195
            Left            =   60
            TabIndex        =   1
            Top             =   1125
            Value           =   -1  'True
            Width           =   1320
         End
         Begin VB.CommandButton cmdBuscar 
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5760
            Picture         =   "frmLstPROVEMatPorProve.frx":0D06
            Style           =   1  'Graphical
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   1080
            Width           =   315
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Listado completo"
            Height          =   210
            Left            =   60
            TabIndex        =   0
            Top             =   450
            Width           =   3495
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2745
            TabIndex        =   3
            Top             =   1080
            Width           =   2985
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5265
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   1410
            TabIndex        =   2
            Top             =   1080
            Width           =   1335
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2355
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4920
         Top             =   120
      End
   End
End
Attribute VB_Name = "frmLstPROVEMatPorProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Variables de seguridad
Private bREqp As Boolean ' Restriccion de equipo
'Private bRMatAsig As Boolean
Public RespetarComboProve As Boolean
Private oProveSeleccionado As CProveedor
Private oProves As CProveedores

'Multilenguaje
Private sIdiMatTodos As String
Private sIdiMatProve As String
Private sIdiGenerando As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private sIdiTitulo As String
Private srIdiTitulo As String
Private srIdiSeleccion As String
Private srIdiProveedor As String
Private srIdiNIF As String
Private srIdiDireccion As String
Private srIdiMoneda As String
Private srIdiHomologacion As String
Private srIdiObservaciones As String
Private srIdiPag As String
Private srIdiDe As String
Private srIdiContactos As String
Private srIdiEstMat As String
Private srIdiArticulo As String



Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> Administrador Then
        
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVERestEquipo)) Is Nothing) And basParametros.gParametrosGenerales.gbOblProveEqp Then
        bREqp = True
    End If
        
End If

End Sub
Public Sub CargarProveedorConBusqueda()

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
   
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False
    'ProveedorSeleccionado
    
End Sub
Private Sub cmdBuscar_Click()

    frmPROVEBuscar.sOrigen = "LstMatPorProve"
    
    If bREqp Then
        frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
    End If
    
    frmPROVEBuscar.Show 1
        
End Sub

Private Sub cmdObtener_Click()
   ''' * Objetivo: Obtener un listado de Material por Proveedor
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    Dim SelectionTextRpt(1 To 2, 1 To 17) As String
    ' ARRAY SelectionTextRpt FORMULA FIELDS para SUBREPORT
    Dim SelectionTextSubRpt(1 To 2, 1 To 10) As String
    Dim oReport As Object
    Dim oCRMatPorProve As CRMatPorProve
    Dim pv As Preview
    Dim sCodComp As String
    Dim sCodEqp As String
    Dim i As Integer
    Dim RepPath As String
    Dim SubListado As CRAXDRT.Report
    Dim oFos As FileSystemObject
    Dim bOcultarHomologacion As Boolean
    Dim bMostrarArticulos As Boolean
    Dim oIMAsig As IMaterialAsignado
    Dim oGruposMN1 As CGruposMatNivel1
            
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    Set oCRMatPorProve = GenerarCRMatPorProve
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptMATporProve.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    

    '*********** FORMULA FIELDS REPORT
    ' Selecci�n
    SelectionTextRpt(2, 1) = "SEL"
    If optTodos = True Then
        SelectionTextRpt(1, 1) = sIdiMatTodos
    Else
        If optProve = True Then
            SelectionTextRpt(1, 1) = sIdiMatProve & " " & sdbcProveDen.Text
        End If
    End If
              
        
        'DATOS OPCIONALES
        SelectionTextRpt(2, 2) = "OcultarDatos"
        If chkDatosBasicos.Value = vbUnchecked Then
            SelectionTextRpt(1, 2) = "S"
        Else
            SelectionTextRpt(1, 2) = "N"
        End If
        SelectionTextRpt(2, 3) = "OcultarContactos"
        If chkContactos.Value = vbUnchecked Then
            SelectionTextRpt(1, 3) = "S"
        Else
            SelectionTextRpt(1, 3) = "N"
        End If
        SelectionTextRpt(2, 4) = "OcultarHomologacion"
        
        If chkCalifica.Value = vbUnchecked Then
            SelectionTextRpt(1, 4) = "S"
            bOcultarHomologacion = False
        Else
            SelectionTextRpt(1, 4) = "N"
            bOcultarHomologacion = True
        End If
        SelectionTextRpt(2, 5) = "OcultarObs"
        If chkObs.Value = vbUnchecked Then
            SelectionTextRpt(1, 5) = "S"
        Else
            SelectionTextRpt(1, 5) = "N"
        End If
        SelectionTextRpt(2, 6) = "MostrarArticulosHomo"
        If chkArticulosHomo.Value = vbChecked Then
            SelectionTextRpt(1, 6) = "S"
            bMostrarArticulos = True
        Else
            SelectionTextRpt(1, 6) = "N"
            bMostrarArticulos = False
        End If
        
        SelectionTextRpt(2, 7) = "txtTITULO"
        SelectionTextRpt(1, 7) = srIdiTitulo
        SelectionTextRpt(2, 8) = "txtSELECCION"
        SelectionTextRpt(1, 8) = srIdiSeleccion
        SelectionTextRpt(2, 9) = "txtPROVEEDOR"
        SelectionTextRpt(1, 9) = srIdiProveedor & ":"
        SelectionTextRpt(2, 10) = "txtNIF"
        SelectionTextRpt(1, 10) = srIdiNIF & ":"
        SelectionTextRpt(2, 11) = "txtDIRECCION"
        SelectionTextRpt(1, 11) = srIdiDireccion & ":"
        SelectionTextRpt(2, 12) = "txtMONEDA"
        SelectionTextRpt(1, 12) = srIdiMoneda & ":"
        SelectionTextRpt(2, 13) = "txtHOMOLOGACION"
        SelectionTextRpt(1, 13) = srIdiHomologacion & ":"
        SelectionTextRpt(2, 14) = "txtOBSERVACIONES"
        SelectionTextRpt(1, 14) = srIdiObservaciones & ":"
        SelectionTextRpt(2, 15) = "txtPAG"
        SelectionTextRpt(1, 15) = srIdiPag
        SelectionTextRpt(2, 16) = "txtDE"
        SelectionTextRpt(1, 16) = srIdiDe
        
        If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGruposMN1 = Nothing
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)

            SelectionTextRpt(2, 17) = "GMN1"
            SelectionTextRpt(1, 17) = oGruposMN1.Item(1).Cod
        Else
            SelectionTextRpt(2, 17) = "GMN1"
            SelectionTextRpt(1, 17) = ""
        End If
        
        ' FORMULA FIELDS SUBREPORTS
        SelectionTextSubRpt(2, 1) = "GMN1NOM"
        SelectionTextSubRpt(1, 1) = gParametrosGenerales.gsDEN_GMN1 & ":"
        SelectionTextSubRpt(2, 2) = "GMN2NOM"
        SelectionTextSubRpt(1, 2) = gParametrosGenerales.gsDEN_GMN2 & ":"
        SelectionTextSubRpt(2, 3) = "GMN3NOM"
        SelectionTextSubRpt(1, 3) = gParametrosGenerales.gsDEN_GMN3 & ":"
        SelectionTextSubRpt(2, 4) = "GMN4NOM"
        SelectionTextSubRpt(1, 4) = gParametrosGenerales.gsDEN_GMN4 & ":"
        SelectionTextSubRpt(2, 5) = "CAL1NOM"
        SelectionTextSubRpt(1, 5) = gParametrosGenerales.gsDEN_CAL1 & ":"
        SelectionTextSubRpt(2, 6) = "CAL2NOM"
        SelectionTextSubRpt(1, 6) = gParametrosGenerales.gsDEN_CAL2 & ":"
        SelectionTextSubRpt(2, 7) = "CAL3NOM"
        SelectionTextSubRpt(1, 7) = gParametrosGenerales.gsDEN_CAL3 & ":"
        SelectionTextSubRpt(2, 8) = "txtCONTACTOS"
        SelectionTextSubRpt(1, 8) = srIdiContactos & ":"
        SelectionTextSubRpt(2, 9) = "txtEstMat"
        SelectionTextSubRpt(1, 9) = srIdiEstMat
        SelectionTextSubRpt(2, 10) = "txtArti"
        SelectionTextSubRpt(1, 10) = srIdiArticulo & ":"
        
    
        ' FORMULA FIELDS REPORT
        For i = 1 To UBound(SelectionTextRpt, 2)
            oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
        Next i
        oReport.FormulaFields(crs_FormulaIndex(oReport, "IDI")).Text = """" & gParametrosInstalacion.gIdioma & """"
        
        
    'SUBREPORT material
    Set SubListado = oReport.OpenSubreport("rptMAT")
    
       
    For i = 1 To 4
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, i))).Text = """" & SelectionTextSubRpt(1, i) & """"
    Next i
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 9))).Text = """" & SelectionTextSubRpt(1, 9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 10))).Text = """" & SelectionTextSubRpt(1, 10) & """"


    Set SubListado = oReport.OpenSubreport("rptCON")
    
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 8))).Text = """" & SelectionTextSubRpt(1, 8) & """"
  
    ' SUBREPORTS CALIFICACIONES
    If SelectionTextRpt(1, 4) = "N" Then
        Set SubListado = oReport.OpenSubreport("rptCAL1")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 5))).Text = """" & SelectionTextSubRpt(1, 5) & """"
        Set SubListado = oReport.OpenSubreport("rptCAL2")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 6))).Text = """" & SelectionTextSubRpt(1, 6) & """"
        Set SubListado = oReport.OpenSubreport("rptCAL3")
        SubListado.FormulaFields(crs_FormulaIndex(SubListado, SelectionTextSubRpt(2, 7))).Text = """" & SelectionTextSubRpt(1, 7) & """"
    End If


    Screen.MousePointer = vbHourglass
    
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        oCRMatPorProve.ListadoMatPorProve oGestorInformes, oReport, optTodos, bREqp, basOptimizacion.gCodEqpUsuario, optProve, sdbcProveCod.Text, optOrdCod, bOcultarHomologacion, bMostrarArticulos, oGruposMN1.Item(1).Cod
    Else
        oCRMatPorProve.ListadoMatPorProve oGestorInformes, oReport, optTodos, bREqp, basOptimizacion.gCodEqpUsuario, optProve, sdbcProveCod.Text, optOrdCod, bOcultarHomologacion, bMostrarArticulos
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sIdiGenerando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    pv.caption = sIdiTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oIMAsig = Nothing
    Set oGruposMN1 = Nothing
End Sub



Private Sub Form_Load()

Me.Width = 6510
Me.Height = 3000
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

CargarRecursos

PonerFieldSeparator Me

ConfigurarSeguridad
Set oProves = oFSGSRaiz.generar_CProveedores

If Me.optTodos.Value = True Then
    sdbcProveCod.Enabled = False
    sdbcProveDen.Enabled = False
    cmdBuscar.Enabled = False
End If

End Sub



Private Sub Form_Unload(Cancel As Integer)
    RespetarComboProve = False
    Set oProveSeleccionado = Nothing
    Set oProves = Nothing

End Sub

Private Sub optProve_Click()
sdbcProveDen.Enabled = True
    sdbcProveCod.Enabled = True
    cmdBuscar.Enabled = True
End Sub

Private Sub optTodos_Click()
    sdbcProveDen.Enabled = False
    sdbcProveCod.Enabled = False
    cmdBuscar.Enabled = False
End Sub

Private Sub sdbcProveCod_Change()
    
    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveDen.Text = ""
        RespetarComboProve = False
               
    End If
    
End Sub

Private Sub sdbcProveCod_Click()
     
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveCod.Rows - 1
            bm = sdbcProveCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor

    Screen.MousePointer = vbHourglass
    If bREqp Then
        oProves.BuscarProveedoresConEqpDelCompDesde 1, basOptimizacion.gCodEqpUsuario, sdbcProveCod, , True, , , , , , , , , , , False
    Else
        oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False
    End If

    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        
        RespetarComboProve = False
        'ProveedorSeleccionado
    End If
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_Change()
    
    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        RespetarComboProve = False
        
    End If
    
End Sub

Private Sub sdbcProveDen_Click()
    
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcProveDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcProveDen.Rows - 1
            bm = sdbcProveDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProveDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProveDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcProveDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor

    Screen.MousePointer = vbHourglass
    If bREqp Then
        oProves.BuscarProveedoresConEqpDelCompDesde 1, basOptimizacion.gCodEqpUsuario, , sdbcProveDen, True, , , , , , , , , , , False
    Else
        oProves.CargarTodosLosProveedoresDesde3 1, , Trim(sdbcProveDen.Text), True, , False
    End If

    bExiste = Not (oProves.Count = 0)
    
    If Not bExiste Then
        sdbcProveDen.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        
        sdbcProveDen.Columns(0).Value = sdbcProveDen.Text
        sdbcProveDen.Columns(1).Value = sdbcProveCod.Text
        
        RespetarComboProve = False
        'ProveedorSeleccionado
    End If
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcProveDen_CloseUp()

    
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    'ProveedorSeleccionado
    
End Sub

Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oIMAsig As IMaterialAsignado
    Dim oGruposMN1 As CGruposMatNivel1

    sdbcProveDen.RemoveAll

    Screen.MousePointer = vbHourglass
    
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)

        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , Trim(oGruposMN1.Item(1).Cod)
    Else
        If bREqp Then
            oProves.BuscarProveedoresConEqpDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, , sdbcProveDen, , , , , , , , True, , , False
        Else
            oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveDen, , True, False
        End If
    End If
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next
    
    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
    
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    'ProveedorSeleccionado
    
End Sub
Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
    Dim oIMAsig As IMaterialAsignado
    Dim oGruposMN1 As CGruposMatNivel1
    
    sdbcProveCod.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set oProves = Nothing
    Set oProves = oFSGSRaiz.generar_CProveedores
    
    If gParametrosGenerales.gbPymes And oUsuarioSummit.Tipo <> Administrador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        
        oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , , , , , , Trim(oGruposMN1.Item(1).Cod) ', , ,, , , , OrdPorDen, OrdPorCalif1, OrdPorCalif2, OrdPorcalif3, False
    Else
        If bREqp Then
            oProves.BuscarProveedoresConEqpDelCompDesde gParametrosInstalacion.giCargaMaximaCombos, basOptimizacion.gCodEqpUsuario, sdbcProveCod, sdbcProveDen, , , , , , , , , , , , False
        Else
            oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveCod.Text), , , , False
        End If
    End If
    
    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If
    Set oIMAsig = Nothing
    Set oGruposMN1 = Nothing
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub Timer1_Timer()
  If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPROVE_MATPORPROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(2) = Ador(0).Value
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        optProve.caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        optOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        optOrdDen.caption = Ador(0).Value
        Ador.MoveNext
        chkDatosBasicos.caption = Ador(0).Value
        Ador.MoveNext
        chkCalifica.caption = Ador(0).Value
        Ador.MoveNext
        chkContactos.caption = Ador(0).Value
        Ador.MoveNext
        chkObs.caption = Ador(0).Value
        Ador.MoveNext
        
        chkArticulosHomo.caption = Ador(0).Value
        Ador.MoveNext
        
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        sIdiMatTodos = Ador(0).Value
        Ador.MoveNext
        sIdiMatProve = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando = Ador(0).Value
        Ador.MoveNext
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext
        sIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiTitulo = Ador(0).Value
        Ador.MoveNext
        srIdiSeleccion = Ador(0).Value
        Ador.MoveNext
        srIdiProveedor = Ador(0).Value
        Ador.MoveNext
        srIdiNIF = Ador(0).Value
        Ador.MoveNext
        srIdiDireccion = Ador(0).Value
        Ador.MoveNext
        srIdiMoneda = Ador(0).Value
        Ador.MoveNext
        srIdiHomologacion = Ador(0).Value
        Ador.MoveNext
        srIdiObservaciones = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.MoveNext
        srIdiContactos = Ador(0).Value
        Ador.MoveNext
        srIdiEstMat = Ador(0).Value
        Ador.MoveNext
        srIdiArticulo = Ador(0).Value
        
        Ador.Close
    
    End If


   Set Ador = Nothing



End Sub



