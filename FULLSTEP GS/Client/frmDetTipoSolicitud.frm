VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmDetTipoSolicitud 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DDetalle de tipo de solicitud"
   ClientHeight    =   5535
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7995
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDetTipoSolicitud.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5535
   ScaleWidth      =   7995
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   5500
      Left            =   120
      ScaleHeight     =   5505
      ScaleWidth      =   7905
      TabIndex        =   4
      Top             =   120
      Width           =   7900
      Begin VB.TextBox txtCod 
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   0
         Top             =   0
         Width           =   2500
      End
      Begin VB.TextBox txtDen 
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   405
         Width           =   4500
      End
      Begin VB.TextBox txtGestor 
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   810
         Width           =   4500
      End
      Begin VB.TextBox txtComent 
         Height          =   1700
         Left            =   0
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   3
         Top             =   1220
         Width           =   7800
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgAdjuntos 
         Height          =   2400
         Left            =   0
         TabIndex        =   8
         Top             =   2980
         Width           =   7800
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   8
         stylesets.count =   2
         stylesets(0).Name=   "Abrir"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmDetTipoSolicitud.frx":0CB2
         stylesets(1).Name=   "Guardar"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmDetTipoSolicitud.frx":0D3E
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         SelectByCell    =   -1  'True
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2566
         Columns(1).Caption=   "ARCHIVO"
         Columns(1).Name =   "ARCHIVO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1296
         Columns(2).Caption=   "SIZE"
         Columns(2).Name =   "SIZE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   2170
         Columns(3).Caption=   "FECHA"
         Columns(3).Name =   "FECHA"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   2302
         Columns(4).Caption=   "USU"
         Columns(4).Name =   "USU"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   1773
         Columns(5).Caption=   "DComentario"
         Columns(5).Name =   "COMENT"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Style=   4
         Columns(5).ButtonsAlways=   -1  'True
         Columns(6).Width=   1323
         Columns(6).Caption=   "SALVAR"
         Columns(6).Name =   "SALVAR"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Style=   4
         Columns(6).ButtonsAlways=   -1  'True
         Columns(6).StyleSet=   "Guardar"
         Columns(7).Width=   1323
         Columns(7).Caption=   "ABRIR"
         Columns(7).Name =   "ABRIR"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Style=   4
         Columns(7).ButtonsAlways=   -1  'True
         Columns(7).StyleSet=   "Abrir"
         _ExtentX        =   13758
         _ExtentY        =   4233
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblCodigo 
         BackColor       =   &H00808000&
         Caption         =   "DC�digo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   7
         Top             =   30
         Width           =   1095
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "DDenominaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   6
         Top             =   435
         Width           =   1095
      End
      Begin VB.Label lblGestor 
         BackColor       =   &H00808000&
         Caption         =   "DGestor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   5
         Top             =   840
         Width           =   1095
      End
   End
   Begin MSComDlg.CommonDialog cmmdAdjun 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmDetTipoSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_oSolicitud As CSolicitud

Private m_sIdiKB As String
Private m_sIdiGuardar As String
Private m_sIdiTipoOrig As String
Private m_sIdiElArchivo As String

Private m_aSayFileNames() As String

'Control de Errores
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
     Unload Me
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not m_bActivado Then
        m_bActivado = True
   End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bDescargarFrm = False
m_bActivado = False
    
    Me.Width = 8155
    Me.Height = 6015
    
    CargarRecursos
    
    'Inicializar el array de ficheros
    ReDim m_aSayFileNames(0)
    
    CargarDatosSolicitud
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DET_TIPO_SOLICIT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value & " " & NullToStr(g_oSolicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)  '1 Detalle de tipo de solicitud:
        Ador.MoveNext
        lblCodigo.caption = Ador(0).Value ' 2 C�digo:
        Ador.MoveNext
        lblDen.caption = Ador(0).Value ' 3 Denominaci�n:
        Ador.MoveNext
        lblGestor.caption = Ador(0).Value ' 4 Gestor:
        Ador.MoveNext
        sdbgAdjuntos.Columns("ARCHIVO").caption = Ador(0).Value '5 Archivo
        Ador.MoveNext
        sdbgAdjuntos.Columns("SIZE").caption = Ador(0).Value '6 Tama�o
        Ador.MoveNext
        sdbgAdjuntos.Columns("FECHA").caption = Ador(0).Value '7 Fecha
        Ador.MoveNext
        sdbgAdjuntos.Columns("USU").caption = Ador(0).Value '8 Usuario
        Ador.MoveNext
        sdbgAdjuntos.Columns("COMENT").caption = Ador(0).Value '9 Comentario
        Ador.MoveNext
        sdbgAdjuntos.Columns("SALVAR").caption = Ador(0).Value '10 Salvar
        Ador.MoveNext
        sdbgAdjuntos.Columns("ABRIR").caption = Ador(0).Value '11 Abrir
        Ador.MoveNext
        m_sIdiKB = Ador(0).Value  '12 KB
        Ador.MoveNext
        m_sIdiGuardar = Ador(0).Value '13 Guardar archivo adjunto
        Ador.MoveNext
        m_sIdiTipoOrig = Ador(0).Value  '14 Tipo original
        Ador.MoveNext
        m_sIdiElArchivo = Ador(0).Value  '15 Archivo

        Ador.Close
        
    End If
    
    Set Ador = Nothing


    
End Sub

Private Sub CargarDatosSolicitud()
Dim oAdjunto As CCampoAdjunto
Dim sCadena As String

    'Carga los datos de la solicitud:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_oSolicitud.CargarDatosSolicitud
    
    txtCod.Text = g_oSolicitud.Codigo
    txtDen.Text = NullToStr(g_oSolicitud.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
    If Not g_oSolicitud.Gestor Is Nothing Then
        If Not g_oSolicitud.Gestor Is Nothing Then
            txtGestor.Text = NullToStr(g_oSolicitud.Gestor.nombre) & " " & NullToStr(g_oSolicitud.Gestor.Apellidos)
        End If
    End If
    If Not g_oSolicitud.Descripciones Is Nothing Then
        txtComent.Text = NullToStr(g_oSolicitud.Descripciones.Item(gParametrosInstalacion.gIdioma).Den)
    End If
    
    If Not g_oSolicitud.Gestor Is Nothing Then
        txtGestor.Text = NullToStr(g_oSolicitud.Gestor.nombre) & " " & NullToStr(g_oSolicitud.Gestor.Apellidos)
    End If
    
    'Carga los adjuntos
    g_oSolicitud.CargarAdjuntos True
    
    For Each oAdjunto In g_oSolicitud.Adjuntos
        sCadena = ""
        sCadena = oAdjunto.Id & Chr(9) & oAdjunto.nombre & Chr(9) & TamanyoAdjuntos(oAdjunto.DataSize / 1024) & " " & m_sIdiKB
        sCadena = sCadena & Chr(9) & Format(oAdjunto.Fecha, "General Date")
        
        If Not oAdjunto.Persona Is Nothing Then
            sCadena = sCadena & Chr(9) & NullToStr(oAdjunto.Persona.nombre) & " " & NullToStr(oAdjunto.Persona.Apellidos)
        End If
        
        sdbgAdjuntos.AddItem sCadena
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "CargarDatosSolicitud", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim FOSFile As Scripting.FileSystemObject
Dim i As Integer

    'Borramos los archivos temporales que hayamos creado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
       m_bDescargarFrm = False
       oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(m_aSayFileNames)
        If FOSFile.FileExists(m_aSayFileNames(i)) Then
            FOSFile.DeleteFile m_aSayFileNames(i), True
        End If
        i = i + 1
    Wend
    Set FOSFile = Nothing
    
    Set g_oSolicitud = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub sdbgAdjuntos_BtnClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAdjuntos.col < 0 Then Exit Sub
    
    If sdbgAdjuntos.Rows = 0 Then
        oMensajes.SeleccioneFichero
    End If
            
    Select Case sdbgAdjuntos.Columns(sdbgAdjuntos.col).Name
        Case "SALVAR"
            GuardarAdjunto
            
        Case "ABRIR"
            AbrirAdjunto
            
        Case "COMENT"
            VerComentario
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "sdbgAdjuntos_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub GuardarAdjunto()
Dim DataFile As Integer
Dim oAdjun As CCampoAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim TESError As TipoErrorSummit
Dim bytChunk() As Byte
Dim lSize As Long
Dim lInit As Long

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If g_oSolicitud Is Nothing Then Exit Sub

    cmmdAdjun.DialogTitle = m_sIdiGuardar
    cmmdAdjun.CancelError = True
    cmmdAdjun.Filter = m_sIdiTipoOrig & "|*.*"
    cmmdAdjun.filename = sdbgAdjuntos.Columns("ARCHIVO").Value
    cmmdAdjun.ShowSave

    sFileName = cmmdAdjun.filename
    sFileTitle = cmmdAdjun.FileTitle
    If sFileTitle = "" Then
        oMensajes.NoValido m_sIdiElArchivo
        Exit Sub
    End If
    DataFile = 1

    ' Cargamos el contenido en la esp.
    Set oAdjun = g_oSolicitud.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
    Set oAdjun.Solicitud = g_oSolicitud
    
    'Abrimos el fichero para escritura binaria
    Open sFileName For Binary Access Write As DataFile
    
    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If

    TESError = oAdjun.ComenzarLecturaData
    If TESError.NumError <> TESnoerror Then
        basErrores.TratarError TESError
        Set oAdjun = Nothing
        Exit Sub
    End If

    Dim arrData() As Byte
    arrData = oAdjun.LeerAdjunto(oAdjun.Id, 0)
    
    WriteByteArray sFileName, arrData
    Close DataFile
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
    On Error Resume Next

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Close DataFile
        Set oAdjun = Nothing
        Exit Sub
    End If
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "GuardarAdjunto", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub AbrirAdjunto()
Dim DataFile As Integer
Dim oAdjun As CCampoAdjunto
Dim sFileName As String
Dim sFileTitle As String
Dim TESError As TipoErrorSummit
Dim bytChunk() As Byte
Dim lSize As Long
Dim lInit As Long
Dim FOSFile As Scripting.FileSystemObject
Dim oFos As Scripting.FileSystemObject
Dim oFile As Scripting.File

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If g_oSolicitud Is Nothing Then Exit Sub
    
    sFileName = FSGSLibrary.DevolverPathFichTemp
    sFileName = sFileName & sdbgAdjuntos.Columns("ARCHIVO").Value
    sFileTitle = sdbgAdjuntos.Columns("ARCHIVO").Value
    
    'Comprueba si existe el fichero y si existe se borra:
    Set FOSFile = New Scripting.FileSystemObject
    If FOSFile.FileExists(sFileName) Then
        FOSFile.DeleteFile sFileName, True
    End If
    Set FOSFile = Nothing

    Set oAdjun = g_oSolicitud.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value))
    Set oAdjun.Solicitud = g_oSolicitud

    If oAdjun.DataSize = 0 Then
        oMensajes.NoValido m_sIdiElArchivo & " " & oAdjun.nombre
        Set oAdjun = Nothing
        Exit Sub
    End If

    TESError = oAdjun.ComenzarLecturaData
    If TESError.NumError <> TESnoerror Then
        TratarError TESError
        Set oAdjun = Nothing
        Exit Sub
    End If
    Dim arrData() As Byte
    arrData = oAdjun.LeerAdjunto(oAdjun.Id, 0)
    
    WriteByteArray sFileName, arrData
    
    'Lanzamos la aplicacion
    ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
       
    Set oAdjun = Nothing
    Set oFos = Nothing
    Set oFile = Nothing
    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
    If err.Number = 70 Then
        Resume Next
    End If
    
        
    If DataFile <> 0 Then
        Close DataFile
        DataFile = 0
    End If

    Set oAdjun = Nothing
    Set oFos = Nothing

   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "AbrirAdjunto", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub VerComentario()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    If g_oSolicitud Is Nothing Then Exit Sub
    
    MostrarFormFormCampoDescr oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sdbgAdjuntos.Columns("COMENT").caption, False, -1, NullToStr(g_oSolicitud.Adjuntos.Item(CStr(sdbgAdjuntos.Columns("ID").Value)).Comentario)
    
    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "VerComentario", err, Erl, , m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub sdbgAdjuntos_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAdjuntos.Columns("SALVAR").CellStyleSet "Guardar"
    sdbgAdjuntos.Columns("ABRIR").CellStyleSet "Abrir"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmDetTipoSolicitud", "sdbgAdjuntos_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
