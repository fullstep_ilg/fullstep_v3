VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{57A1F96E-5A81-4063-8193-6E7BB254EDBD}#1.0#0"; "DXAnimatedGIF.ocx"
Begin VB.Form frmSubastaMonitor 
   Caption         =   "Form1"
   ClientHeight    =   8760
   ClientLeft      =   2955
   ClientTop       =   930
   ClientWidth     =   15015
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSubastaMonitor.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8760
   ScaleWidth      =   15015
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Left            =   12600
      Top             =   480
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5115
      Left            =   3480
      Picture         =   "frmSubastaMonitor.frx":0CB2
      ScaleHeight     =   5115
      ScaleWidth      =   7365
      TabIndex        =   1
      Top             =   1380
      Width           =   7365
      Begin DXAnimatedGIF.DXGif DXGif1 
         Height          =   300
         Left            =   2070
         TabIndex        =   2
         Top             =   2280
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   529
         BackColor       =   12632256
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "La p�gina se est� cargando"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   960
         TabIndex        =   6
         Top             =   840
         Width           =   5655
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Espere, por favor."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   960
         TabIndex        =   5
         Top             =   1200
         Width           =   5655
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "www.fullstep.com - � 1997-2010 FULLSTEP NETWORKS"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   4500
         Width           =   4965
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Todos los derechos reservados"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   180
         TabIndex        =   3
         Top             =   4740
         Width           =   3615
      End
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   4215
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12255
      ExtentX         =   21616
      ExtentY         =   7435
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmSubastaMonitor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_iAnyo As Integer
Public g_sGmn1 As String
Public g_iCod As Long
Public g_sTitulo As String
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Public Idioma As String
Public GestorIdiomas As CGestorIdiomas
Public Raiz As CRaiz
Public Mensajes As CMensajes
Public UsuCod As String
Public IdSesion As String

Private m_ParamGen As ParametrosGenerales
Private m_sMsgError As String

Public Property Get ParamGen() As ParametrosGenerales
    ParamGen = m_ParamGen
End Property
Public Property Let ParamGen(ByRef vParametrosGenerales As ParametrosGenerales)
    m_ParamGen = vParametrosGenerales
End Property

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = GestorIdiomas.DevolverTextosDelModulo(FRM_TRANSICION, Idioma)
    
    If Not Ador Is Nothing Then
        Label1.Caption = Ador(0).Value
        Ador.MoveNext
        Label2.Caption = Ador(0).Value
        Ador.MoveNext
        Label3.Caption = Replace(Ador(0).Value, "@#@", Year(Now))
        Ador.MoveNext
        Label4.Caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
End Sub

Private Sub Form_Activate()
    If Not Raiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If

    If Not m_bActivado Then m_bActivado = True
    
    Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Formulario", "frmSubastaMonitor", "Form_Activate", Err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    If Not Raiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    m_bDescargarFrm = False
    Dim sCadena As String
    
    DXGif1.FileName = App.Path & "\BarraCargando.gif"
    
    WebBrowser1.Visible = True
    
    Picture1.Refresh
    
    CargarRecursos
    
    Timer1.Interval = 2000

    sCadena = ParamGen.gcolRutas("GSCUIProcesos")
    sCadena = sCadena & "?usu=" & UsuCod & "&session=" & IdSesion & "&pantalla=SubastaMonitor"
    sCadena = sCadena & "&Anyo=" & g_iAnyo & "&Cod=" & g_iCod & "&GMN1=" & g_sGmn1
    
    WebBrowser1.Navigate2 sCadena
    DoEvents
    Timer1.Enabled = True
    Me.Caption = g_sTitulo

    Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Formulario", "frmSubastaMonitor", "Form_Load", Err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


Private Sub Form_Resize()
    On Error Resume Next
    
    WebBrowser1.Left = 0
    WebBrowser1.Top = 0
    WebBrowser1.Width = Me.Width
    WebBrowser1.Height = Me.Height
    Picture1.Top = WebBrowser1.Height / 2 - Picture1.Height / 2
    Picture1.Left = WebBrowser1.Width / 2 - Picture1.Width / 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not Raiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        m_bDescargarFrm = False
        Mensajes.MensajeOKOnly m_sMsgError, Critical
    End If

    WebBrowser1.Stop

    Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Formulario", "frmSubastaMonitor", "Form_Unload", Err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Timer1_Timer()
    If Not Raiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    Picture1.Refresh
    If WebBrowser1.ReadyState = READYSTATE_COMPLETE Then
        Picture1.Visible = False
        Timer1.Enabled = False
    End If

    Exit Sub
ERROR:
    If Err.Number <> 0 Then
        m_bDescargarFrm = Raiz.TratarError("Formulario", "frmSubastaMonitor", "Timer1_Timer", Err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub


