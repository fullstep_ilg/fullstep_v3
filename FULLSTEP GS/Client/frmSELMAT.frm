VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELMAT 
   BackColor       =   &H00808000&
   Caption         =   "Selecci�n de material"
   ClientHeight    =   5835
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5235
   Icon            =   "frmSELMAT.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5835
   ScaleWidth      =   5235
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   2
      Top             =   5475
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2625
      TabIndex        =   1
      Top             =   5475
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   1035
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   7646
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":1106
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":1218
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":132A
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":177C
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":182C
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":18EE
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":199E
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":1A60
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":1B10
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":1BD2
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELMAT.frx":1C9A
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Ha introducido un c�digo de articulo inexistente."
      ForeColor       =   &H80000014&
      Height          =   210
      Left            =   90
      TabIndex        =   5
      Top             =   90
      Width           =   4950
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Se dispone a dar alta del art�culo en la estructura de materiales."
      ForeColor       =   &H80000014&
      Height          =   225
      Left            =   90
      TabIndex        =   4
      Top             =   300
      Width           =   4995
   End
   Begin VB.Label Label3 
      BackColor       =   &H00808000&
      Caption         =   "Seleccione el material en el que desea ubicar el art�culo:"
      ForeColor       =   &H80000014&
      Height          =   300
      Left            =   90
      TabIndex        =   3
      Top             =   660
      Width           =   4965
   End
End
Attribute VB_Name = "frmSELMAT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public oActsN1 As CActividadesNivel1
Public oGMN1Seleccionado As CGrupoMatNivel1
Public oGMN2Seleccionado As CGrupoMatNivel2
Public oGMN3Seleccionado As CGrupoMatNivel3
Public oGMN4Seleccionado As CGrupoMatNivel4

Public sGMN1 As String
Public sGMN2 As String
Public sGMN3 As String
Public sGMN4 As String

Public oGMNSeleccionado As Object

Public g_bAceptar As Boolean
Public g_bParaPedido As Boolean

Public oProcesoSeleccionado As CProceso
Public bPermProcMultiMaterial As Boolean

Private sIdioma() As String
' Variables de restricciones
Public bRComprador As Boolean

' Variables para interactuar con otros forms
Public sOrigen As String

Public bRCompResponsable As Boolean
Public bRUsuAper As Boolean

' Variable de control de flujo
Public Accion As AccionesSummit

Public m_PYME As Integer

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private m_inivelSeleccion As Integer
Private m_sSeleccioneMatNivel As String

Private Sub cmdAceptar_Click()
    Dim oACN1 As CActividadNivel1
    Dim oACN2 As CActividadNivel2
    Dim oACN3 As CActividadNivel3
    Dim oACN4 As CActividadNivel4
    Dim oACN5 As CActividadNivel5
    Dim node As MSComctlLib.node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim scod5 As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set node = tvwEstrMat.selectedItem
    
    If node Is Nothing Then Exit Sub
    
    If node.Tag = "Raiz" Then
        Screen.MousePointer = vbHourglass
        
        If sOrigen = "frmAdmProvePortalresult" Then
            With frmAdmProvePortal
                .g_vACN1Seleccionada = Null
                .g_vACN2Seleccionada = Null
                .g_vACN3Seleccionada = Null
                .g_vACN4Seleccionada = Null
                .g_vACN5Seleccionada = Null
                .lblActividad = ""
            End With
        
            g_bAceptar = True
            Unload Me
        End If
    
        Screen.MousePointer = vbNormal

        Exit Sub
    End If
    
    If sOrigen = "frmAdmProvePortalresult" Then
        
        Screen.MousePointer = vbHourglass

        Select Case Left(node.Tag, 4)
        
            Case "ACN1"
                
                With frmAdmProvePortal
                
                    .g_vACN1Seleccionada = DevolverId(node)
                    .g_vACN2Seleccionada = Null
                    .g_vACN3Seleccionada = Null
                    .g_vACN4Seleccionada = Null
                    .g_vACN5Seleccionada = Null
                    .lblActividad = ""
                    scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                    
                    Set oACN1 = oActsN1.Item(scod1)
                    
                    .lblActividad = oACN1.Cod & " - " & oACN1.Den
                
                End With
                
            Case "ACN2"
                
                With frmAdmProvePortal
                
                    .g_vACN1Seleccionada = DevolverId(node.Parent)
                    .g_vACN2Seleccionada = DevolverId(node)
                    .g_vACN3Seleccionada = Null
                    .g_vACN4Seleccionada = Null
                    .g_vACN5Seleccionada = Null
                
                    scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                    scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                    
                    Set oACN2 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2)
                    
                    .lblActividad = oACN2.Cod & " - " & oACN2.Den
                
                End With
                
                
            Case "ACN3"
                
                With frmAdmProvePortal
                
                    .g_vACN1Seleccionada = DevolverId(node.Parent.Parent)
                    .g_vACN2Seleccionada = DevolverId(node.Parent)
                    .g_vACN3Seleccionada = DevolverId(node)
                    .g_vACN4Seleccionada = Null
                    .g_vACN5Seleccionada = Null
                
                    scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                    scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                    scod3 = scod2 & CStr(.g_vACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN3Seleccionada)))
                    
                    Set oACN3 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3)
                    
                    .lblActividad = oACN3.Cod & " - " & oACN3.Den
                
                End With
                
            Case "ACN4"
            
                With frmAdmProvePortal
                
                    .g_vACN1Seleccionada = DevolverId(node.Parent.Parent.Parent)
                    .g_vACN2Seleccionada = DevolverId(node.Parent.Parent)
                    .g_vACN3Seleccionada = DevolverId(node.Parent)
                    .g_vACN4Seleccionada = DevolverId(node)
                    .g_vACN5Seleccionada = Null
                    scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                    scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                    scod3 = scod2 & CStr(.g_vACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN3Seleccionada)))
                    scod4 = scod3 & CStr(.g_vACN4Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN4Seleccionada)))
                    
                    Set oACN4 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4)
                    
                    .lblActividad = oACN4.Cod & " - " & oACN4.Den
                End With
                
            Case "ACN5"
                
                With frmAdmProvePortal
                
                    .g_vACN1Seleccionada = DevolverId(node.Parent.Parent.Parent.Parent)
                    .g_vACN2Seleccionada = DevolverId(node.Parent.Parent.Parent)
                    .g_vACN3Seleccionada = DevolverId(node.Parent.Parent)
                    .g_vACN4Seleccionada = DevolverId(node.Parent)
                    .g_vACN5Seleccionada = DevolverId(node)
                
                    scod1 = CStr(.g_vACN1Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN1Seleccionada)))
                    scod2 = scod1 & CStr(.g_vACN2Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN2Seleccionada)))
                    scod3 = scod2 & CStr(.g_vACN3Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN3Seleccionada)))
                    scod4 = scod3 & CStr(.g_vACN4Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN4Seleccionada)))
                    scod5 = scod4 & CStr(.g_vACN5Seleccionada) & Mid$("                         ", 1, 10 - Len(CStr(.g_vACN5Seleccionada)))
                    
                    Set oACN5 = oActsN1.Item(scod1).ActividadesNivel2.Item(scod2).ActividadesNivel3.Item(scod3).ActividadesNivel4.Item(scod4).ActividadesNivel5.Item(scod5)
                    
                    frmAdmProvePortal.lblActividad = oACN5.Cod & " - " & oACN5.Den
                
                End With
                
                
        End Select
        
        Screen.MousePointer = vbHourglass
        
        g_bAceptar = True
        Unload Me
    Else
    
        Screen.MousePointer = vbHourglass
        Select Case Left(node.Tag, 4)
                    
                Case "GMN1"
                            If sOrigen = "frmProce" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmSOLAbrirProc" Then
                                Screen.MousePointer = vbNormal
                                oMensajes.SeleccioneMatNivel4
                                Exit Sub
                            End If
                            
                            Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                            oGMN1Seleccionado.Cod = DevolverCod(node)
                            oGMN1Seleccionado.Den = Mid$(node.Text, Len(DevolverCod(node)) + 4, Len(node.Text) - Len(DevolverCod(node)))
                            Set oGMNSeleccionado = oGMN1Seleccionado
                                
                Case "GMN2"
                            If sOrigen = "frmProce" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmSOLAbrirProc" Then
                                Screen.MousePointer = vbNormal
                                oMensajes.SeleccioneMatNivel4
                                Exit Sub
                            End If
                                
                            Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                            oGMN1Seleccionado.Cod = DevolverCod(node.Parent)
                            oGMN1Seleccionado.Den = Mid$(node.Parent.Text, Len(CStr(DevolverCod(node.Parent))) + 4, Len(node.Parent.Text) - Len(CStr(DevolverCod(node.Parent))))
                            
                            Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                            oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent)
                            oGMN2Seleccionado.Cod = DevolverCod(node)
                            oGMN2Seleccionado.Den = Mid$(node.Text, Len(CStr(DevolverCod(node))) + 4, Len(node.Text) - Len(CStr(DevolverCod(node))))
                            oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent)
                            Set oGMNSeleccionado = oGMN2Seleccionado
                            
                            
                Case "GMN3"
                            If sOrigen = "frmProce" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmSOLAbrirProc" Then
                                Screen.MousePointer = vbNormal
                                oMensajes.SeleccioneMatNivel4
                                Exit Sub
                            End If
                            
                            Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                            oGMN1Seleccionado.Cod = DevolverCod(node.Parent.Parent)
                            oGMN1Seleccionado.Den = Mid$(node.Parent.Parent.Text, Len(CStr(DevolverCod(node.Parent.Parent))) + 4, Len(node.Parent.Parent.Text) - Len(CStr(DevolverCod(node.Parent.Parent))))
                            
                            Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                            oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent)
                            oGMN2Seleccionado.Cod = DevolverCod(node.Parent)
                            oGMN2Seleccionado.Den = Mid$(node.Parent.Text, Len(CStr(DevolverCod(node.Parent))) + 4, Len(node.Parent.Text) - Len(CStr(DevolverCod(node.Parent))))
                            
                            Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
                            oGMN3Seleccionado.Cod = DevolverCod(node)
                            oGMN3Seleccionado.Den = Mid$(node.Text, Len(CStr(DevolverCod(node))) + 4, Len(node.Text) - Len(CStr(DevolverCod(node))))
                            oGMN3Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent)
                            oGMN3Seleccionado.GMN2Cod = DevolverCod(node.Parent)
                            Set oGMNSeleccionado = oGMN3Seleccionado
                            
                            
                Case "GMN4"
                                
                            Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                            oGMN1Seleccionado.Cod = DevolverCod(node.Parent.Parent.Parent)
                            oGMN1Seleccionado.Den = Mid$(node.Parent.Parent.Parent.Text, Len(CStr(DevolverCod(node.Parent.Parent.Parent))) + 4, Len(node.Parent.Parent.Parent.Text) - Len(CStr(DevolverCod(node.Parent.Parent.Parent))))
                            
                            Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                            oGMN2Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent.Parent)
                            oGMN2Seleccionado.Cod = DevolverCod(node.Parent.Parent)
                            oGMN2Seleccionado.Den = Mid$(node.Parent.Parent.Text, Len(CStr(DevolverCod(node.Parent.Parent))) + 4, Len(node.Parent.Parent.Text) - Len(CStr(DevolverCod(node.Parent.Parent))))
                            
                            Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
                            oGMN3Seleccionado.Cod = DevolverCod(node.Parent)
                            oGMN3Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent.Parent)
                            oGMN3Seleccionado.GMN2Cod = DevolverCod(node.Parent.Parent)
                            oGMN3Seleccionado.Den = Mid$(node.Parent.Text, Len(CStr(DevolverCod(node.Parent))) + 4, Len(node.Parent.Text) - Len(CStr(DevolverCod(node.Parent))))
                                
                            Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
                            
                            oGMN4Seleccionado.Cod = DevolverCod(node)
                            oGMN4Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent.Parent)
                            oGMN4Seleccionado.GMN2Cod = DevolverCod(node.Parent.Parent)
                            oGMN4Seleccionado.GMN3Cod = DevolverCod(node.Parent)
                            oGMN4Seleccionado.Den = Mid$(node.Text, Len(CStr(DevolverCod(node))) + 4, Len(node.Text) - Len(CStr(DevolverCod(node))))
                            
                            If sOrigen = "frmProce" Then
                                Set frmPROCE.g_oProcesoSeleccionado.MaterialProce = Nothing
                                Set frmPROCE.g_oProcesoSeleccionado.MaterialProce = oFSGSRaiz.Generar_CGruposMatNivel4
                            End If
                            Set oGMNSeleccionado = oGMN4Seleccionado
                            
                                                    
        End Select
    
        Screen.MousePointer = vbNormal
        
    End If
    
    If Me.nivelSeleccion <> 0 Then
        If oGMNSeleccionado.Nivel < Me.nivelSeleccion Then
            oMensajes.mensajeGenericoOkOnly Replace(m_sSeleccioneMatNivel, "###", nivelSeleccion), Exclamation
            Exit Sub
        End If
    End If
    
    Select Case sOrigen
        Case "MDIfrmATRIB"
            frmAtrib.ctlAtributos.PonerMatSeleccionadoEnCombos
            
        Case "GMN4frmATRIB", "ART4frmATRIB", "POSIfrmATRIB"
            frmESTRMATAtrib.g_ofrmATRIB.ctlAtributos.PonerMatSeleccionadoEnCombos
            
        Case "frmPROCEfrmATRIB"
            frmPROCE.g_ofrmATRIB.ctlAtributos.PonerMatSeleccionadoEnCombos
            
        Case "PLANTILLASfrmATRIB"
            frmPlantillasProce.g_ofrmATRIB.ctlAtributos.PonerMatSeleccionadoEnCombos
            
        Case "frmESTRCOMPComPorMat"
            
            frmESTRCOMPComPorMat.PonerMatSeleccionadoEnCombos
            
        Case "frmPROVE"
            
            frmPROVE.PonerMatSeleccionadoEnCombos
        
        Case "frmPROVEEqpPorProve"
            
            frmPROVEEqpPorProve.PonerMatSeleccionadoEnCombos
        
        Case "frmPROVEProvePorEqp"
            
            frmPROVEProvePorEqp.PonerMatSeleccionadoEnCombos
        
        Case "frmPROVEProvePorMat"
            
            frmPROVEProvePorMat.PonerMatSeleccionadoEnCombos
        
        Case "frmProce"
            
            frmPROCE.PonerMatSeleccionadoEnCombos
            
         
        Case "frmProveBuscar"
            
            frmPROVEBuscar.PonerMatSeleccionadoEnCombos
        
        Case "frmPROCEBuscar"
            
            frmPROCEBuscar.PonerMatSeleccionadoEnCombos
            
        Case "frmPROCEAltaArticulos"
                    
            Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
                
            oGMN4Seleccionado.Cod = DevolverCod(node)
            oGMN4Seleccionado.GMN1Cod = DevolverCod(node.Parent.Parent.Parent)
            oGMN4Seleccionado.GMN2Cod = DevolverCod(node.Parent.Parent)
            oGMN4Seleccionado.GMN3Cod = DevolverCod(node.Parent)
            oGMN4Seleccionado.Den = Mid$(node.Text, Len(CStr(DevolverCod(node))) + 4, Len(node.Text) - Len(CStr(DevolverCod(node))))
            
            Set frmPROCE.g_oGMN4Seleccionado = oGMN4Seleccionado
            
        Case "frmPROCEBuscarArticulo"
        
            frmPROCEBuscarArticulo.PonerMatSeleccionado
              
        Case "frmInfAhorroNegMatReu"
            
            frmInfAhorroNegMatReu.PonerMatSeleccionadoEnCombos
        
        Case "frmInfAhorroNegMatDesde"
            
            frmInfAhorroNegMatDesde.PonerMatSeleccionadoEnCombos
        
        Case "frmInfAhorroApliMat"
            
            frmInfAhorroApliMat.PonerMatSeleccionadoEnCombos
            
        Case "frmLstMAT"
            
            frmLstMAT.PonerMatSeleccionado ("frmSELMAT")
            
        Case "frmLstART"
            
            frmLstART.PonerMatSeleccionado ("frmSELMAT")
            
        Case "frmLstESTRCOMPComPorMat"
            
            frmLstESTRCOMPComPorMat.PonerMatSeleccionado
            
        Case "frmLstPROVEProvPorMat"
            
            frmLstPROVEProvPorMat.PonerMatSeleccionado
            
        Case "frmLstPROVEEqpPorProve"
            
            frmLstPROVEEqpPorProve.PonerMatSeleccionadoEnCombos
        
        Case "frmLstPROVEProvePorEqp"
            
            frmLstPROVEProvePorEqp.PonerMatSeleccionadoEnCombos
        
        Case "frmLstPROVE"
            
            frmLstPROVE.PonerMatSeleccionadoEnCombos

        Case "frmLstPRESPorMat"
            
            frmLstPRESPorMat.PonerMatSeleccionado
            
        Case "frmLstPROCEA2B1"
            
            frmListados.ofrmLstProce1.PonerMatSeleccionado
            
        Case "frmLstPROCEA2B2"
            
            frmListados.ofrmlstProce2.PonerMatSeleccionado
            
        Case "frmLstPROCEA2B3"
            
            frmListados.ofrmLstProce3.PonerMatSeleccionado
            
        Case "frmLstPROCEA2B4C1"
            
            frmLstOFEBuzon.PonerMatSeleccionado
        
        Case "frmLstPROCEA2B4C2"
        
            frmListados.ofrmLstProce4.PonerMatSeleccionado
        
        Case "frmLstPROCEA2B5"
            
            frmListados.ofrmlstProce5.PonerMatSeleccionado
        Case "frmLstPROCEA6B3C1"
            
            frmListados.ofrmLstProce6.PonerMatSeleccionado
            
        Case "frmLstPROCEfrmPROCE"
            
            frmPROCE.g_ofrmLstPROCE.PonerMatSeleccionado
            
        Case "frmLstPROCEfrmSELPROVE"
            
            frmSELPROVE.ofrmLstPROCE.PonerMatSeleccionado
            
        Case "frmLstPROCEfrmOFEPet"
            
            frmOFEPet.ofrmLstPROCE.PonerMatSeleccionado
            
        Case "frmLstPROCEfrmOFERec"
            
            frmOFERec.ofrmLstPROCE.PonerMatSeleccionado
            
        Case "frmLstPROCEfrmOFEHistWeb"
            
            frmOFEHistWeb.ofrmLstPROCE.PonerMatSeleccionado
            
        Case "frmLstINFAhorroNegA4B1C1D1"
            
            frmListados.ofrmLstNegGenReu.PonerMatSeleccionado
            
        Case "frmLstINFAhorroNegA4B1C1D2"
            
            frmListados.ofrmLstNegGenDesde.PonerMatSeleccionado
            
        Case "frmLstINFAhorroNegA4B1C2D1"
            
            frmListados.ofrmLstNegMatReu.PonerMatSeleccionado
            
        Case "frmLstINFAhorroNegA4B1C2D2"
            
            frmListados.ofrmLstNegMatDesde.PonerMatSeleccionado
            
        Case "frmLstINFAhorroNegA4B1C3D1"
            
            frmListados.ofrmLstNegEqpResReu.PonerMatSeleccionado
            
        Case "frmLstINFAhorroNegA4B1C3D2"
            
            frmListados.ofrmLstNegEqpResDesde.PonerMatSeleccionado
        Case "frmLstINFAhorroNegA4B1C4D1"
            
            frmListados.ofrmLstNegEqpReu.PonerMatSeleccionado
            
        Case "frmLstINFAhorroNegA4B1C4D2"
            
            frmListados.ofrmLstNegEqpDesde.PonerMatSeleccionado
        
        Case "frmLstREU"
            
            frmLstREU.PonerMatSeleccionado
            
        Case "frmLstINFAhorroAplA4B2C2"
            
            frmListados.ofrmLstApliMat.PonerMatSeleccionado
            
        Case "frmLstINFAhorroAplfrmInfAhorroApliMat"
            
            frmInfAhorroApliMat.ofrmLstApliMat.PonerMatSeleccionado
        
        Case "frmLstOFEBuzon"
            
            frmLstOFEBuzon.PonerMatSeleccionado
        
        Case "frmSeguimiento"
            
            frmSeguimiento.PonerMatSeleccionado
            
        Case "frmCATALAnyaArt"
    
            frmCATALAnyaArt.PonerMatSeleccionado
    
        Case "frmCATLINBuscar"
    
            frmCATLINBuscar.PonerMatSeleccionado
            
        Case "frmLstCatalogo"
        
            frmLstCatalogo.PonerMatSeleccionado
            
        Case "frmLstPedidos"
            
            frmLstPedidos.PonerMatSeleccionado
        
        Case "frmLstINFAhorroNeg"
            
            frmInfAhorroNegMatReu.ofrmLstAhorroNeg.PonerMatSeleccionado "frmLstINFAhorroNeg"
            
        Case "frmLstINFAhorroNegDesde"
            
            frmInfAhorroNegMatDesde.ofrmLstAhorroNeg.PonerMatSeleccionado "frmLstINFAhorroNegDesde"
    
        Case "frmLstAtributos", "POSIfrmLstAtributos", "ART4frmLstAtributos", "MDIfrmLstAtributos", "frmPROCEfrmLstAtributos", "PLANTILLASfrmLstAtributos"
            frmLstAtributos.PonerMatSeleccionadoEnCombos
            
        Case "frmFormulariosfrmATRIB"
            frmFormularios.g_ofrmATRIB.ctlAtributos.PonerMatSeleccionadoEnCombos
            
        Case "frmDesgloseValores"
            frmDesgloseValores.PonerMatSeleccionado False
            
        Case "frmDesgloseValoresGrid"
            frmDesgloseValores.PonerMatSeleccionado True
            
        Case "frmSolicitudDesglose"
            frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.PonerMatSeleccionado

        Case "frmSolicitudDesgloseP"
            frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.PonerMatSeleccionado

        Case "frmSolicitudBuscar"
            frmSolicitudBuscar.PonerMatSeleccionado
        
        Case "frmFormularios"
            frmFormularios.PonerMatSeleccionado
            
        Case "frmDesglosefrmATRIB"
            frmFormularios.g_ofrmDesglose.g_ofrmATRIB.ctlAtributos.PonerMatSeleccionadoEnCombos
            
        Case "frmSolicitudDetalle"
            frmSolicitudes.g_ofrmDetalleSolic.PonerMatSeleccionado
            
        Case "frmPARTipoSolicitfrmATRIB"
            frmPARTipoSolicit.g_ofrmATRIB.ctlAtributos.PonerMatSeleccionadoEnCombos
            
        Case "frmPARTipoSolicitDesglosefrmATRIB"
            frmPARTipoSolicit.g_ofrmDesglose.g_ofrmATRIB.ctlAtributos.PonerMatSeleccionadoEnCombos
     
        Case "frmSOLAbrirProc"
            frmSOLAbrirProc.PonerMatSeleccionado
            
        Case "frmFlujosRoles"
            Me.Hide 'Le descargo en el origen
            Exit Sub
        Case "frmProce"
            frmPROCE.PonerMatSeleccionadoEnCombos
            
        Case "frmESTRMAT"
            Dim bSeleccionIncorrecta As Boolean
            
            Select Case gParametrosGenerales.giNEM
            
                Case 1
                    If oGMN1Seleccionado Is Nothing Then
                        bSeleccionIncorrecta = True
                    End If
                
                Case 2
                    If oGMN2Seleccionado Is Nothing Then
                        bSeleccionIncorrecta = True
                    End If
                
                Case 3
                    If oGMN3Seleccionado Is Nothing Then
                        bSeleccionIncorrecta = True
                    End If
                
                Case 4
                    If oGMN4Seleccionado Is Nothing Then
                        bSeleccionIncorrecta = True
                    End If
                    
            End Select
            
            If bSeleccionIncorrecta Then
                oMensajes.SeleccioneMatNivel4
            Else
                frmESTRMAT.Reubicar
            End If
        
        Case "frmESTRMATBuscar"
            frmESTRMATBuscar.PonerMatSeleccionado
            
        Case "frmPROCEAltaArticulosNoCodif"
            frmPROCE.g_oGMN4Seleccionado.Cod = oGMN4Seleccionado.Cod
            frmPROCE.g_oGMN4Seleccionado.GMN3Cod = oGMN4Seleccionado.GMN3Cod
            frmPROCE.g_oGMN4Seleccionado.GMN2Cod = oGMN4Seleccionado.GMN2Cod
            frmPROCE.g_oGMN4Seleccionado.GMN1Cod = oGMN4Seleccionado.GMN1Cod
             
        Case "frmPROCEModifArtNoCodif"
            frmPROCE.ActualizarMaterialArticuloNoCodif oGMN4Seleccionado.GMN1Cod, oGMN4Seleccionado.GMN2Cod, oGMN4Seleccionado.GMN3Cod, oGMN4Seleccionado.Cod
            
        Case "frmSolicitudes"
            frmSolicitudes.PonerMatSeleccionado
            
        Case "frmLstSolicitud"
            frmLstSolicitud.PonerMatSeleccionado
            
        Case "frmFormAnyaCampos"
            frmFormAnyaCampos.PonerMatSeleccionado
            
        Case "frmDetalleCampos"
            frmDetalleCampos.PonerMatSeleccionado
            
    End Select
    
    g_bAceptar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        ReDim sIdioma(1 To 17)
        For i = 1 To 17
            sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        Me.caption = sIdioma(3)
        cmdAceptar.caption = sIdioma(8)
        cmdCancelar.caption = sIdioma(9)
        m_sSeleccioneMatNivel = sIdioma(17)
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    Dim sCaption As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    m_bDescargarFrm = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    g_bAceptar = False
    CargarRecursos
    
    Me.Height = 5385
    Me.Width = 5355
    
    Label1.Visible = False
    Label2.Visible = False
    Label3.Visible = False
    cmdAceptar.Top = 315
    cmdCancelar.Top = 315
    cmdCancelar.Visible = True
    cmdAceptar.Visible = True
    tvwEstrMat.Top = 120
    
    If sOrigen = "frmAdmProvePortalresult" Then
        If gParametrosGenerales.gbSincronizacionMat = True Then
            Me.caption = sIdioma(3)
        Else
            Me.caption = sIdioma(1)
        End If
    Else
        If sOrigen = "frmAdmProvePortal2" Then
            If gParametrosGenerales.gbSincronizacionMat = True Then
                Me.caption = sIdioma(8)
            Else
                Me.caption = sIdioma(2)
            End If
            cmdAceptar.Visible = False
            cmdCancelar.Visible = False
            Exit Sub
        Else
            If sOrigen = "frmPROCEAltaArticulos" And bPermProcMultiMaterial Then
                Me.caption = sIdioma(14)
                Me.Height = 6345
                Label1.Visible = True
                Label2.Visible = True
                Label3.Visible = True
                tvwEstrMat.Top = Label3.Top + Label3.Height + 75
                tvwEstrMat.Height = Me.Height - 2000
                cmdAceptar.Top = tvwEstrMat.Top + tvwEstrMat.Height + 75
                cmdCancelar.Top = cmdAceptar.Top
               ' Me.Height = 6345
                Label1.caption = sIdioma(11)
                Label2.caption = sIdioma(12)
                Label3.caption = sIdioma(13)
                
            Else
                If sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmPROCEModifArtNoCodif" Then
                    Label1.caption = sIdioma(16)
                    Label1.Visible = True
                    Label2.Visible = False
                    Label3.Visible = False
                    tvwEstrMat.Top = Label1.Top + Label1.Height + 75
                    cmdCancelar.Visible = False
                    cmdAceptar.Top = tvwEstrMat.Top + tvwEstrMat.Height + 75
                    cmdAceptar.Left = (Me.Width / 2) - (cmdAceptar.Width / 2)
                    Me.Height = 6000
                Else
                    If sOrigen = "frmPROCEMostrarMateriales" Then
                        Me.caption = sIdioma(15)
                        cmdAceptar.Visible = False
                        cmdCancelar.Left = (Me.Width / 2) - (cmdCancelar.Width / 2)
                    Else
                        Me.caption = sIdioma(3)
                    End If
                End If
            End If
        End If
    End If
    
    sCaption = Me.caption
    
    Me.caption = sIdioma(4)
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    DoEvents
    
    tvwEstrMat.Scroll = False
    
    If sOrigen = "frmAdmProvePortalresult" Then
        GenerarEstructuraActividades
    ElseIf sOrigen = "frmProce" Then
        GenerarEstructuraMatProceAper
    ElseIf sOrigen = "frmPROCEMostrarMateriales" Then
        MostrarMateriales
    Else
        GenerarEstructuraMat False
    End If
    Screen.MousePointer = vbNormal
    
    tvwEstrMat.Scroll = True
    
    Me.caption = sCaption
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

''' <summary>Genera la estructura de materiales</summary>
''' <param name="bOrdenadoPorDen">Ordenado por denominaci�n<param>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 23/08/2011</revision>

Private Sub GenerarEstructuraMat(ByVal bOrdenadoPorDen As Boolean)
    Dim oGrupsMN1 As CGruposMatNivel1
    Dim oGrupsMN4 As CGruposMatNivel4
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim oIMaterialComprador As IMaterialAsignado
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim nodx As MSComctlLib.node
    Dim sImage As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    tvwEstrMat.Nodes.clear
    
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
    nodx.Tag = "Raiz"
    nodx.Expanded = True
    nodx.Selected = True

    If bRComprador Then
        Set oIMaterialComprador = oUsuarioSummit.comprador
    
        With oIMaterialComprador
            If sOrigen = "frmPROCEAltaArticulos" And Not frmPROCE.m_oPlantillaSeleccionada Is Nothing And Not g_bParaPedido Then
                Set oGrupsMN4 = oIMaterialComprador.DevolverGruposMN4Asignados(, , , bOrdenadoPorDen, False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario, frmPROCE.m_oPlantillaSeleccionada.Id)
            Else
                Set oGrupsMN4 = oIMaterialComprador.DevolverGruposMN4Asignados(, , , bOrdenadoPorDen, False, , bRUsuAper, bRCompResponsable, basOptimizacion.gCodPersonaUsuario)
            End If
                
            If Not oGrupsMN4 Is Nothing Then
                If sOrigen = "frmSOLAbrirProc" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmPROCEModifArtNoCodif" Then
                    sImage = "GMN4"
                Else
                    sImage = "GMN4A"
                End If
                
                For Each oGMN4 In oGrupsMN4
                    scod1 = oGMN4.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN4.GMN1Cod))
                    scod2 = oGMN4.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN4.GMN2Cod))
                    scod3 = oGMN4.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN4.GMN3Cod))
                    scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                    
                    If Not ExisteNodo("GMN1" & scod1) Then
                        Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN4.GMN1Cod & " - " & oGMN4.GMN1Den, "GMN1")
                        nodx.Tag = "GMN1" & oGMN4.GMN1Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                    End If
                    If Not ExisteNodo("GMN2" & scod1 & scod2) Then
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN4.GMN2Cod & " - " & oGMN4.GMN2Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN4.GMN2Cod ' Con la X0X indicamos que el GMN1 no esta asignado directamente al comprador.
                    End If
                    If Not ExisteNodo("GMN3" & scod1 & scod2 & scod3) Then
                        Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN4.GMN3Cod & " - " & oGMN4.GMN3Den, "GMN3")
                        nodx.Tag = "GMN3" & oGMN4.GMN3Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    End If
                    If sOrigen <> "frmPRESPorMat" Then
                        Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, sImage)
                        nodx.Tag = "GMN4" & oGMN4.Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                        If sOrigen = "frmSOLAbrirProc" Or sOrigen = "frmPROCEAltaArticulos" Or sOrigen = "frmPROCEBuscarArticulo" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Then
                            nodx.Parent.Parent.Parent.Expanded = False
                            nodx.Parent.Parent.Expanded = False
                            nodx.Parent.Expanded = False
                        Else
                            If Not oGMN4Seleccionado Is Nothing Then
                                If oGMN4.GMN1Cod = oGMN4Seleccionado.GMN1Cod And oGMN4.GMN2Cod = oGMN4Seleccionado.GMN2Cod And oGMN4.GMN3Cod = oGMN4Seleccionado.GMN3Cod And oGMN4.Cod = oGMN4Seleccionado.Cod Then
                                    nodx.Parent.Parent.Parent.Expanded = True
                                    nodx.Parent.Parent.Expanded = True
                                    nodx.Parent.Expanded = True
                                    nodx.Selected = True
                                End If
                            End If
                        End If
                        
                        If sOrigen = "frmProce" Then
                            'Material seleccionado en la apertura
                            If frmPROCE.lblMat.caption <> "" Then
                                If oGMN4.Cod = frmPROCE.g_oGMN4Seleccionado.Cod And oGMN4.GMN3Cod = frmPROCE.g_oGMN4Seleccionado.GMN3Cod And oGMN4.GMN2Cod = frmPROCE.g_oGMN4Seleccionado.GMN2Cod And oGMN4.GMN1Cod = frmPROCE.g_oGMN4Seleccionado.GMN1Cod Then
                                    nodx.Selected = True
                                End If
                            End If
                        End If
                    End If
                Next
                 'Si solo tiene un material  lo mostramos expandido
                If tvwEstrMat.Nodes.Count = 5 Then
                    tvwEstrMat.Nodes(1).Expanded = True
                    tvwEstrMat.Nodes(1).Child.Expanded = True
                    tvwEstrMat.Nodes(1).Child.Child.Expanded = True
                    tvwEstrMat.Nodes(1).Child.Child.Child.Expanded = True
                    Set tvwEstrMat.selectedItem = tvwEstrMat.Nodes(1).Child.Child.Child.Child
                End If
            End If
        End With
    Else 'No hay restriccion de comprador
        Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
                
        If sOrigen = "frmPROCEAltaArticulos" And Not frmPROCE.m_oPlantillaSeleccionada Is Nothing Then
            oGrupsMN1.GenerarEstructuraMateriales bOrdenadoPorDen, frmPROCE.m_oPlantillaSeleccionada.Id, m_PYME
            If oGrupsMN1.Count = 0 Then
                oGrupsMN1.GenerarEstructuraMateriales bOrdenadoPorDen, , m_PYME
            End If
        Else
            oGrupsMN1.GenerarEstructuraMateriales bOrdenadoPorDen, , m_PYME, , , Me.nivelSeleccion, Me.sGMN1, Me.sGMN2, Me.sGMN3, Me.sGMN4
        End If
        
        Select Case gParametrosGenerales.giNEM
            Case 1 'Solo un nivel de material
                For Each oGMN1 In oGrupsMN1
                    scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                Next
                    
            Case 2 'Dos niveles de materiales
                Select Case sOrigen
                    Case "frmPRESPorMat"
                        For Each oGMN1 In oGrupsMN1
                            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                            nodx.Tag = "GMN1" & oGMN1.Cod
                        Next
                                
                    Case Else
                        For Each oGMN1 In oGrupsMN1
                            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                            nodx.Tag = "GMN1" & oGMN1.Cod
                            
                            For Each oGMN2 In oGMN1.GruposMatNivel2
                                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                nodx.Tag = "GMN2" & oGMN2.Cod
                            Next
                        Next
                End Select
            Case 3 'Hay tres niveles de materiales
                Select Case sOrigen
                    Case "frmPRESPorMat"
                        For Each oGMN1 In oGrupsMN1
                            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                            nodx.Tag = "GMN1" & oGMN1.Cod
                            
                            For Each oGMN2 In oGMN1.GruposMatNivel2
                                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                nodx.Tag = "GMN2" & oGMN2.Cod
                            Next
                        Next
                    
                    Case Else
                        For Each oGMN1 In oGrupsMN1
                            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                            nodx.Tag = "GMN1" & oGMN1.Cod
                            
                            For Each oGMN2 In oGMN1.GruposMatNivel2
                                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                nodx.Tag = "GMN2" & oGMN2.Cod
                                    
                                For Each oGMN3 In oGMN2.GruposMatNivel3
                                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                    Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                    nodx.Tag = "GMN3" & oGMN3.Cod
                                Next
                            Next
                        Next
                    
                End Select
            
            Case 4 'Cuatro niveles de materiales
                Select Case sOrigen
                    Case "frmPRESPorMat"
                        For Each oGMN1 In oGrupsMN1
                            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                            nodx.Tag = "GMN1" & oGMN1.Cod
                            
                            For Each oGMN2 In oGMN1.GruposMatNivel2
                                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                nodx.Tag = "GMN2" & oGMN2.Cod
                                    
                                For Each oGMN3 In oGMN2.GruposMatNivel3
                                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                    Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                    nodx.Tag = "GMN3" & oGMN3.Cod
                                Next
                            Next
                        Next
                    
                    Case "frmPROCEBuscarArticulo"
                        Dim j As Integer
                        If Not bPermProcMultiMaterial And Not g_bParaPedido Then
                            For Each oGMN1 In oGrupsMN1
                                For j = 1 To oProcesoSeleccionado.MaterialProce.Count
                                    If oGMN1.Cod = oProcesoSeleccionado.MaterialProce.Item(j).GMN1Cod Then
                                        scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                                        Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                                        nodx.Tag = "GMN1" & oGMN1.Cod
                                        
                                        For Each oGMN2 In oGMN1.GruposMatNivel2
                                            If oGMN2.Cod = oProcesoSeleccionado.MaterialProce.Item(j).GMN2Cod Then
                                                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                                                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                                nodx.Tag = "GMN2" & oGMN2.Cod
                                                
                                                For Each oGMN3 In oGMN2.GruposMatNivel3
                                                    If oGMN3.Cod = oProcesoSeleccionado.MaterialProce.Item(j).GMN3Cod Then
                                                        scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                                        Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                                        nodx.Tag = "GMN3" & oGMN3.Cod
                                                        
                                                        For Each oGMN4 In oGMN3.GruposMatNivel4
                                                            If oGMN4.Cod = oProcesoSeleccionado.MaterialProce.Item(j).Cod Then
                                                                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                                                Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                                                nodx.Tag = "GMN4" & oGMN4.Cod
                                                                If Not oGMN4Seleccionado Is Nothing Then
                                                                    If oGMN1.Cod = oGMN4Seleccionado.GMN1Cod And oGMN2.Cod = oGMN4Seleccionado.GMN2Cod And oGMN3.Cod = oGMN4Seleccionado.GMN3Cod And oGMN4.Cod = oGMN4Seleccionado.Cod Then
                                                                        nodx.Parent.Parent.Parent.Expanded = True
                                                                        nodx.Parent.Parent.Expanded = True
                                                                        nodx.Parent.Expanded = True
                                                                        nodx.Expanded = True
                                                                        nodx.Selected = True
                                                                    End If
                                                                End If
                                                            End If
                                                        Next
                                                    End If
                                                Next
                                            End If
                                        Next
                                    End If
                                Next
                            Next
                        Else
                            For Each oGMN1 In oGrupsMN1
                                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                                nodx.Tag = "GMN1" & oGMN1.Cod
                                
                                For Each oGMN2 In oGMN1.GruposMatNivel2
                                    scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                                    Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                    nodx.Tag = "GMN2" & oGMN2.Cod
                                    
                                    For Each oGMN3 In oGMN2.GruposMatNivel3
                                        scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                        nodx.Tag = "GMN3" & oGMN3.Cod
                                        
                                        For Each oGMN4 In oGMN3.GruposMatNivel4
                                            scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                            Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                            nodx.Tag = "GMN4" & oGMN4.Cod
                                            If Not oGMN4Seleccionado Is Nothing Then
                                                If oGMN1.Cod = oGMN4Seleccionado.GMN1Cod And oGMN2.Cod = oGMN4Seleccionado.GMN2Cod And oGMN3.Cod = oGMN4Seleccionado.GMN3Cod And oGMN4.Cod = oGMN4Seleccionado.Cod Then
                                                    nodx.Parent.Parent.Parent.Expanded = True
                                                    nodx.Parent.Parent.Expanded = True
                                                    nodx.Parent.Expanded = True
                                                    nodx.Expanded = True
                                                    nodx.Selected = True
                                                End If
                                            End If
                                        Next
                                    Next
                                Next
                            Next
                        
                        End If
                        
                    Case Else
                        For Each oGMN1 In oGrupsMN1
                            scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                            nodx.Tag = "GMN1" & oGMN1.Cod
                            
                            For Each oGMN2 In oGMN1.GruposMatNivel2
                                scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                                Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                nodx.Tag = "GMN2" & oGMN2.Cod
                                
                                For Each oGMN3 In oGMN2.GruposMatNivel3
                                    scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                    Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                    nodx.Tag = "GMN3" & oGMN3.Cod
                                    
                                    For Each oGMN4 In oGMN3.GruposMatNivel4
                                        scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                        nodx.Tag = "GMN4" & oGMN4.Cod
                                        If Not oGMN4Seleccionado Is Nothing Then
                                            If oGMN1.Cod = oGMN4Seleccionado.GMN1Cod And oGMN2.Cod = oGMN4Seleccionado.GMN2Cod And oGMN3.Cod = oGMN4Seleccionado.GMN3Cod And oGMN4.Cod = oGMN4Seleccionado.Cod Then
                                                nodx.Parent.Parent.Parent.Expanded = True
                                                nodx.Parent.Parent.Expanded = True
                                                nodx.Parent.Expanded = True
                                                nodx.Expanded = True
                                                nodx.Selected = True
                                            End If
                                        End If
                                    Next
                                Next
                            Next
                        Next
                
                End Select
        End Select
    End If

    Set oGrupsMN1 = Nothing
    Set nodx = Nothing
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "GenerarEstructuraMat", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba si existe un nodo en el �rbol. La colecci�n de nodos del �rbol genera un error si se intenta acceder a un nodo que no existe</summary>
''' <param name="sKey">Key del nodo<param>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 23/08/2011</revision>

Private Function ExisteNodo(ByVal sKey As String) As Boolean
    Dim nodx As MSComctlLib.node
    
    ''********* ������ NO MODIFICAR EL CONTROL DE ERRORES !!!!!! ***************
    On Error GoTo NoSeEncuentra:

    Set nodx = tvwEstrMat.Nodes.Item(sKey)
    ExisteNodo = True
    
    Exit Function
NoSeEncuentra:
    ExisteNodo = False
    Set nodx = Nothing
End Function

Private Sub Form_Resize()
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Height < 1200 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    If sOrigen = "frmPROCEAltaArticulosNoCodif" Or sOrigen = "frmPROCEModifArtNoCodif" Then
'        Me.Height = 6000
        tvwEstrMat.Height = Me.Height - cmdAceptar.Height - 1030
    Else
        If sOrigen <> "frmPROCEAltaArticulos" Then
            tvwEstrMat.Height = Me.Height - 1030
        Else
            tvwEstrMat.Height = Me.Height - 2000
        End If
    End If
   ' tvwEstrMat.Height = Me.Height - 1030
    'tvwEstrMat.Height = Me.Height - cmdAceptar.Height - 1030
    tvwEstrMat.Width = Me.Width - 360
    
    cmdAceptar.Top = Me.Height - 825
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (tvwEstrMat.Width / 2) - (cmdAceptar.Width / 2) - 300
    If sOrigen = "frmPROCEMostrarMateriales" Then
        cmdCancelar.Left = (Me.Width / 2) - (cmdCancelar.Width / 2)
    Else
        cmdCancelar.Left = (tvwEstrMat.Width / 2) + 300
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "Form_Resize", err, Erl)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGMN4Seleccionado = Nothing
If m_bDescargarFrm = False Then
    If sOrigen = "frmPROCEModifArtNoCodif" Or sOrigen = "frmPROCEAltaArticulosNoCodif" Then
        cmdAceptar_Click
        If oGMN4Seleccionado Is Nothing Then
            Cancel = True
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
End If
m_bDescargarFrm = False
sGMN1 = ""
sGMN2 = ""
sGMN3 = ""
sGMN4 = ""
Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing

Set oActsN1 = Nothing
m_inivelSeleccion = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Function

DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Public Function DevolverId(ByVal node As MSComctlLib.node) As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If node Is Nothing Then Exit Function

DevolverId = val(Right(node.Tag, Len(node.Tag) - 4))
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "DevolverId", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Sub GenerarEstructuraActividades()

Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
tvwEstrMat.Nodes.clear

If gParametrosGenerales.gbSincronizacionMat = True Then
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
Else
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(7), "Raiz2")
End If
nodx.Tag = "Raiz"
nodx.Expanded = True

    Set oActsN1 = oFSGSRaiz.generar_CActividadesNivel1
    frmEsperaPortal.Show
    DoEvents
    oActsN1.GenerarEstructuraActividades False, basPublic.gParametrosInstalacion.gIdiomaPortal
    Unload frmEsperaPortal
    For Each oACN1 In oActsN1
        
        scod1 = oACN1.Cod & Mid$("                         ", 1, 10 - Len(oACN1.Cod))
            
        Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACT")
        nodx.Tag = "ACN1" & oACN1.Id
        
        For Each oACN2 In oACN1.ActividadesNivel2
            scod2 = oACN2.Cod & Mid$("                         ", 1, 10 - Len(oACN2.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACT")
            nodx.Tag = "ACN2" & oACN2.Id
            For Each oACN3 In oACN2.ActividadesNivel3
                scod3 = oACN3.Cod & Mid$("                         ", 1, 10 - Len(oACN3.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACT")
                nodx.Tag = "ACN3" & oACN3.Id
                For Each oACN4 In oACN3.ActividadesNivel4
                    scod4 = oACN4.Cod & Mid$("                         ", 1, 10 - Len(oACN4.Cod))
                    Set nodx = tvwEstrMat.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACT")
                    nodx.Tag = "ACN4" & oACN4.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    For Each oACN5 In oACN4.ActividadesNivel5
                        scod5 = oACN5.Cod & Mid$("                         ", 1, 10 - Len(oACN5.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACT")
                        nodx.Tag = "ACN5" & oACN5.Id ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                    Next
                Next
            Next
    
        Next
    Next
       
        
Exit Sub

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "GenerarEstructuraActividades", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub GenerarEstructuraActividadesProveedor(ByVal CiaProve As Long)

'********************************************************************
'***** Descripcion : Metodo que crea el arbol de la estructura de ***
'***** actividades de cualquier nivel.                            ***
'***** Parametros: CiaProve, el codigo del proveedor en el portal ***
'********************************************************************

Dim oActsN1 As CActividadesNivel1
Dim oActsN2 As CActividadesNivel2
Dim oActsN3 As CActividadesNivel3
Dim oActsN4 As CActividadesNivel4
Dim oActsN5 As CActividadesNivel5

Dim oACN1 As CActividadNivel1
Dim oACN2 As CActividadNivel2
Dim oACN3 As CActividadNivel3
Dim oACN4 As CActividadNivel4
Dim oACN5 As CActividadNivel5

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim scod5 As String

Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
tvwEstrMat.Nodes.clear

If gParametrosGenerales.gbSincronizacionMat = True Then
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
Else
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(7), "Raiz2")
End If

nodx.Expanded = True

If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm

'***********Cargamos Las Actividades de Nivel1******************

Set oActsN1 = Nothing
Set oActsN1 = oFSGSRaiz.generar_CActividadesNivel1
oActsN1.CargarActividadesN1Proveedor CiaProve, gParametrosInstalacion.gIdiomaPortal

If Not oActsN1 Is Nothing Then

    For Each oACN1 In oActsN1
        scod1 = CStr(oACN1.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN1.Id)))
        If Not ExisteNodo("ACN1" & scod1) Then
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN1.Cod & " - " & oACN1.Den, "ACTASIG")
        End If
    Next
     
End If

Set oActsN1 = Nothing
Set oACN1 = Nothing

'***********Cargamos Las Actividades de Nivel2******************

Set oActsN2 = Nothing
Set oActsN2 = oFSGSRaiz.Generar_CActividadesNivel2
oActsN2.CargarActividadesN2Proveedor CiaProve, gParametrosInstalacion.gIdiomaPortal

If Not oActsN2 Is Nothing Then

    For Each oACN2 In oActsN2
    
        scod1 = CStr(oACN2.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.ACN1)))
        scod2 = CStr(oACN2.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN2.Id)))
        
        If Not ExisteNodo("ACN1" & scod1) Then
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN2.CodACT1 & " - " & oACN2.DenACT1, "ACT")
        End If
        If Not ExisteNodo("ACN2" & scod1 & scod2) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN2.Cod & " - " & oACN2.Den, "ACTASIG")
        End If
       
        If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
            nodx.Parent.Expanded = True
        End If
                
    Next
    
End If

Set oActsN2 = Nothing
Set oACN2 = Nothing


'***********Cargamos Las Actividades de Nivel3******************

Set oActsN3 = Nothing
Set oActsN3 = oFSGSRaiz.Generar_CActividadesNivel3
oActsN3.CargarActividadesN3Proveedor CiaProve, gParametrosInstalacion.gIdiomaPortal
        

If Not oActsN3 Is Nothing Then

    For Each oACN3 In oActsN3
    
        scod1 = CStr(oACN3.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.ACN1)))
        scod2 = CStr(oACN3.acn2) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.acn2)))
        scod3 = CStr(oACN3.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN3.Id)))
        
        If Not ExisteNodo("ACN1" & scod1) Then
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN3.CodACT1 & " - " & oACN3.DenACT1, "ACT")
        End If
        If Not ExisteNodo("ACN2" & scod1 & scod2) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN3.CodACT2 & " - " & oACN3.DenACT2, "ACT")
        End If
        If Not ExisteNodo("ACN3" & scod1 & scod2 & scod3) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN3.Cod & " - " & oACN3.Den, "ACTASIG")
        End If

        If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
        End If
  
    Next

End If

Set oActsN3 = Nothing
Set oACN3 = Nothing


'***********Cargamos Las Actividades de Nivel4******************

Set oActsN4 = Nothing
Set oActsN4 = oFSGSRaiz.Generar_CActividadesNivel4
oActsN4.CargarActividadesN4Proveedor CiaProve, gParametrosInstalacion.gIdiomaPortal

If Not oActsN4 Is Nothing Then

    For Each oACN4 In oActsN4
    
        scod1 = CStr(oACN4.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.ACN1)))
        scod2 = CStr(oACN4.acn2) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.acn2)))
        scod3 = CStr(oACN4.acn3) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.acn3)))
        scod4 = CStr(oACN4.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN4.Id)))
        
        If Not ExisteNodo("ACN1" & scod1) Then
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN4.CodACT1 & " - " & oACN4.DenACT1, "ACT")
        End If
        If Not ExisteNodo("ACN2" & scod1 & scod2) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN4.CodACT2 & " - " & oACN4.DenACT2, "ACT")
        End If
        If Not ExisteNodo("ACN3" & scod1 & scod2 & scod3) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN4.CodACT3 & " - " & oACN4.DenACT3, "ACT")
        End If
        If Not ExisteNodo("ACN4" & scod1 & scod2 & scod3 & scod4) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN4.Cod & " - " & oACN4.Den, "ACTASIG")
        End If

        If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
            nodx.Parent.Parent.Parent.Expanded = True
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
        End If
        
    Next

End If

Set oActsN4 = Nothing
Set oACN4 = Nothing
        

'***********Cargamos Las Actividades de Nivel5******************

Set oActsN5 = Nothing
Set oActsN5 = oFSGSRaiz.Generar_CActividadesNivel5
oActsN5.CargarActividadesN5Proveedor CiaProve, gParametrosInstalacion.gIdiomaPortal


If Not oActsN5 Is Nothing Then

    For Each oACN5 In oActsN5
    
        scod1 = CStr(oACN5.ACN1) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.ACN1)))
        scod2 = CStr(oACN5.acn2) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.acn2)))
        scod3 = CStr(oACN5.acn3) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.acn3)))
        scod4 = CStr(oACN5.acn4) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.acn4)))
        scod5 = CStr(oACN5.Id) & Mid$("                         ", 1, 10 - Len(CStr(oACN5.Id)))
        
        If Not ExisteNodo("ACN1" & scod1) Then
            Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "ACN1" & scod1, oACN5.CodACT1 & " - " & oACN5.DenACT1, "ACT")
        End If
        If Not ExisteNodo("ACN2" & scod1 & scod2) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN1" & scod1, tvwChild, "ACN2" & scod1 & scod2, oACN5.CodACT2 & " - " & oACN5.DenACT2, "ACT")
        End If
        If Not ExisteNodo("ACN3" & scod1 & scod2 & scod3) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN2" & scod1 & scod2, tvwChild, "ACN3" & scod1 & scod2 & scod3, oACN5.CodACT3 & " - " & oACN5.DenACT3, "ACT")
        End If
        If Not ExisteNodo("ACN4" & scod1 & scod2 & scod3 & scod4) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN3" & scod1 & scod2 & scod3, tvwChild, "ACN4" & scod1 & scod2 & scod3 & scod4, oACN5.CodACT4 & " - " & oACN5.DenACT4, "ACT")
        End If
        If Not ExisteNodo("ACN5" & scod1 & scod2 & scod3 & scod4 & scod5) Then
            Set nodx = tvwEstrMat.Nodes.Add("ACN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "ACN5" & scod1 & scod2 & scod3 & scod4 & scod5, oACN5.Cod & " - " & oACN5.Den, "ACTASIG")
        End If
        
        If nodx.Parent.Image = "ACTASIG" Or nodx.Image = "ACTASIG" Then
            nodx.Parent.Parent.Parent.Parent.Expanded = True
            nodx.Parent.Parent.Parent.Expanded = True
            nodx.Parent.Parent.Expanded = True
            nodx.Parent.Expanded = True
        End If
        
    Next

End If

Set oActsN5 = Nothing
Set oACN5 = Nothing
  
tvwEstrMat.Nodes("Raiz").Selected = True
Set nodx = Nothing
Exit Sub

ERROR_Frm:
    Set nodx = Nothing
    Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "GenerarEstructuraActividadesProveedor", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
    
End Sub


Private Sub GenerarEstructuraMatProceAper()

Dim oGrupsMN1 As CGruposMatNivel1

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4
Dim oIMaterialComprador As IMaterialAsignado

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Dim nodx As MSComctlLib.node

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
tvwEstrMat.Nodes.clear

Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
nodx.Selected = True

    
    If bRComprador Then
        
        Set oIMaterialComprador = oUsuarioSummit.comprador
        Set oGrupsMN1 = oIMaterialComprador.DevolverEstructuraMat
    Else
        Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        oGrupsMN1.GenerarEstructuraMateriales False
    End If
    
        
    Select Case gParametrosGenerales.giNEM
        
        Case 1 'Solo un nivel de material
                                
                    scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                    Set oGMN1 = oGrupsMN1.Item(scod1)
                    If oGMN1 Is Nothing Then
                        oMensajes.DatoEliminado sIdioma(6)
                        Set oGrupsMN1 = Nothing
                        Exit Sub
                    End If
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                                    
        Case 2 'Dos niveles de materiales
            
                scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                Set oGMN1 = oGrupsMN1.Item(scod1)
                
                If oGMN1 Is Nothing Then
                    oMensajes.DatoEliminado sIdioma(6)
                    Set oGrupsMN1 = Nothing
                    Exit Sub
                Else
                
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                        
                    Next
                End If
        
        Case 3 'Hay tres niveles de materiales
                        
                scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                Set oGMN1 = oGrupsMN1.Item(scod1)
                
                If oGMN1 Is Nothing Then
                    oMensajes.DatoEliminado sIdioma(6)
                    Set oGrupsMN1 = Nothing
                    Exit Sub
                Else
                
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                            nodx.Tag = "GMN3" & oGMN3.Cod
                        Next
                    Next
                End If
        
        Case 4 'Cuatro niveles de materiales
                
                scod1 = frmPROCE.sdbcGMN1_4Cod.Text & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmPROCE.sdbcGMN1_4Cod.Text))
                Set oGMN1 = oGrupsMN1.Item(scod1)
                
                If oGMN1 Is Nothing Then
                    oMensajes.DatoEliminado sIdioma(6)
                    Set oGrupsMN1 = Nothing
                    Exit Sub
                Else
                
                    Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                    nodx.Tag = "GMN1" & oGMN1.Cod
                    
                    For Each oGMN2 In oGMN1.GruposMatNivel2
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                        nodx.Tag = "GMN2" & oGMN2.Cod
                        
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                            Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                            nodx.Tag = "GMN3" & oGMN3.Cod
                            
                            For Each oGMN4 In oGMN3.GruposMatNivel4
                                scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                'Si tiene restricci�n de comprador.
                                If bRComprador Then
                                    Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4A")
                                Else
                                    Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                End If
                                nodx.Tag = "GMN4" & oGMN4.Cod ' Con la X0X indicamos que la GMN2 no esta asignada directamente al comprador.
                                If Not frmPROCE.g_oGMN4Seleccionado Is Nothing Then
                                    'Material seleccionado en la apertura
                                    If frmPROCE.lblMat.caption <> "" Then
                                        If oGMN4.Cod = frmPROCE.g_oGMN4Seleccionado.Cod And oGMN4.GMN3Cod = frmPROCE.g_oGMN4Seleccionado.GMN3Cod And oGMN4.GMN2Cod = frmPROCE.g_oGMN4Seleccionado.GMN2Cod And oGMN4.GMN1Cod = frmPROCE.g_oGMN4Seleccionado.GMN1Cod Then
                                            nodx.Parent.Parent.Parent.Expanded = True
                                            nodx.Parent.Parent.Expanded = True
                                            nodx.Parent.Expanded = True
                                            nodx.Selected = True
                                        End If
                                    End If
                                End If
                            Next
                        Next
                    Next
                     'Si solo tiene un material  lo mostramos expandido
                    If tvwEstrMat.Nodes.Count = 5 Then
                        tvwEstrMat.Nodes(1).Expanded = True
                        tvwEstrMat.Nodes(1).Child.Expanded = True
                        tvwEstrMat.Nodes(1).Child.Child.Expanded = True
                        tvwEstrMat.Nodes(1).Child.Child.Child.Expanded = True
                        Set tvwEstrMat.selectedItem = tvwEstrMat.Nodes(1).Child.Child.Child.Child
                    End If
              End If
                        
    End Select
    
    Screen.MousePointer = vbNormal
    
    Set oGrupsMN1 = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "GenerarEstructuraMatProceAper", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
Private Sub MostrarMateriales()
    Dim nodx As MSComctlLib.node
    Dim oGrupsMN1 As CGruposMatNivel1
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    'Recorrer el arbol y marcar cada rama de material seleccionada en el proceso y dejarla expandida
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm
    
    oProcesoSeleccionado.MaterialProce.DevolverEstructuraDeMateriales
    
    Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sIdioma(5), "Raiz")
    nodx.Selected = True
    
    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
    oGrupsMN1.GenerarEstructuraMateriales False
      Dim j As Integer
    For Each oGMN1 In oGrupsMN1
        For j = 1 To oProcesoSeleccionado.MaterialProce.Count
            If oProcesoSeleccionado.MaterialProce.Item(j).GMN1Cod = oGMN1.Cod Then
                scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
                For Each oGMN2 In oGMN1.GruposMatNivel2
                    If oProcesoSeleccionado.MaterialProce.Item(j).GMN2Cod = oGMN2.Cod Then
                        scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
                        For Each oGMN3 In oGMN2.GruposMatNivel3
                            If oProcesoSeleccionado.MaterialProce.Item(j).GMN3Cod = oGMN3.Cod Then
                                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                                For Each oGMN4 In oGMN3.GruposMatNivel4
                                    If oProcesoSeleccionado.MaterialProce.Item(j).Cod = oGMN4.Cod Then
                                        scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                                        
                                        Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
                                        nodx.Tag = "GMN1" & oGMN1.Cod
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
                                        nodx.Tag = "GMN2" & oGMN2.Cod
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                                        nodx.Tag = "GMN3" & oGMN3.Cod
                                        Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                                        nodx.Tag = "GMN4" & oGMN4.Cod
                                        nodx.Image = "GMN4A"
                                        nodx.Expanded = True
                                        nodx.Parent.Expanded = True
                                        nodx.Parent.Parent.Expanded = True
                                        nodx.Parent.Parent.Parent.Expanded = True
                                        nodx.Parent.Parent.Parent.Parent.Expanded = True
                                        
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
    Next
        
ERROR_Frm:
    Set nodx = Nothing
    Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "MostrarMateriales", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If

End Sub

Public Function GMNSeleccionado() As Object
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set GMNSeleccionado = oGMNSeleccionado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "GMNSeleccionado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Function isNodoSeleccionable(nodo As node) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    isNodoSeleccionable = (nodo.Image = "GMN4A")
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSELMAT", "isNodoSeleccionable", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function



Public Property Get nivelSeleccion() As Integer

    nivelSeleccion = m_inivelSeleccion

End Property

Public Property Let nivelSeleccion(ByVal iNivelSeleccion As Integer)

    m_inivelSeleccion = iNivelSeleccion

End Property
