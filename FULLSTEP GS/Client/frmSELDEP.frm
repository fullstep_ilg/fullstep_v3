VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELDEP 
   Caption         =   "DSelecci�n de departamento"
   ClientHeight    =   6660
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8205
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELDEP.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6660
   ScaleWidth      =   8205
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkTraslados 
      Caption         =   "Check1"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   5040
      Width           =   255
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cerrar"
      Height          =   315
      Left            =   4065
      TabIndex        =   2
      Top             =   6240
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   315
      Left            =   2880
      TabIndex        =   1
      Top             =   6240
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   4770
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   8070
      _ExtentX        =   14235
      _ExtentY        =   8414
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":0CB2
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":0D62
            Key             =   "PerBaja"
            Object.Tag             =   "PerBaja"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":1086
            Key             =   "PerCestaBaja"
            Object.Tag             =   "PerCestaBaja"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":13AA
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":145A
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":151B
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":18AE
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":195E
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":19FD
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":1A68
            Key             =   "PerCesta"
            Object.Tag             =   "PerCesta"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELDEP.frx":1AF3
            Key             =   "SELLO"
            Object.Tag             =   "SELLO"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblTraslados 
      Caption         =   "Trasladar los procesos abiertos por la persona a la nueva unidad organizativa."
      Height          =   495
      Left            =   480
      TabIndex        =   4
      Top             =   5040
      Width           =   4935
   End
End
Attribute VB_Name = "frmSELDEP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String
Public sCodDep As String
Public sOrigen As String
Public m_bModif As Boolean

Private bRUO As Boolean
Private bRDep As Boolean

''' <summary>
''' Recoge el departamento seleccionado
''' </summary>
''' <remarks>Llamada desde: Al hacer click sobre el bot�n; Tiempo m�ximo: 0,1 sg.</remarks>
Private Sub cmdAceptar_Click()
    Dim nodx As node
    
    Set nodx = tvwestrorg.selectedItem
    If Not nodx Is Nothing Then
        If nodx.Image = "Departamento" Then
            sCodDep = Mid(nodx.Tag, 5)
            If Not nodx.Parent Is Nothing Then
                Select Case Mid(nodx.Parent.Tag, 1, 4)
                    Case "UON1"
                        sUON1 = Mid(nodx.Parent.Tag, 5)
                        sUON2 = ""
                        sUON3 = ""
                    Case "UON2"
                        sUON1 = Mid(nodx.Parent.Parent.Tag, 5)
                        sUON2 = Mid(nodx.Parent.Tag, 5)
                        sUON3 = ""
                    Case "UON3"
                        sUON1 = Mid(nodx.Parent.Parent.Parent.Tag, 5)
                        sUON2 = Mid(nodx.Parent.Parent.Tag, 5)
                        sUON3 = Mid(nodx.Parent.Tag, 5)
                    Case Else
                        sUON1 = ""
                        sUON2 = ""
                        sUON3 = ""
                End Select
            End If
            Select Case sOrigen
                Case "frmFlujosRoles"
                    Me.Hide 'Lo descargo en el Origen
                    Exit Sub
                Case "frmESTRORG"
                    frmESTRORG.m_Reubicar = True
                    Me.Hide 'Lo descargo en el Origen
                    Exit Sub
            End Select
            Unload Me
        End If
    End If
End Sub

Private Sub cmdCancelar_Click()
    sCodDep = ""
    Unload Me
End Sub

Private Sub Form_Load()
        
    Screen.MousePointer = vbHourglass
        
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
        
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
        
    If sOrigen <> "frmESTRORG" Then
        chkTraslados.Visible = False
        lblTraslados.Visible = False
    End If
    
    CargarRecursos
    ConfigurarSeguridad
    GenerarEstructuraOrg False
    If sOrigen = "frmFlujosRoles" Then
        PreseleccionarDepartamento
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELDEP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        Me.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblTraslados.caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing

End Sub

Private Sub ConfigurarSeguridad()
    If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestUO)) Is Nothing) Then
            bRUO = True
        End If
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGRestDep)) Is Nothing) Then
            bRDep = True
        End If
    End If
    If Not m_bModif Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
    End If
End Sub

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados
Dim oDepAsoc As CDepAsociado

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Otras
Dim nodx As node
'Dim oDepartamento As CDepartamento
'Dim sCodDepartamento As String
'Dim varCodPersona As Variant
'Dim bOrdCod As Boolean
'Dim bOrdDen As Boolean
Dim sKeyImage As String

    
    sOrdenListadoDen = bOrdenadoPorDen

    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    If (oUsuarioSummit.Tipo = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (bRUO Or bRDep) Then
        
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False
        End Select
        
        
    Else
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
        End Select
        
        
    End If
    
        
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        If Not IsNull(oUON1.IdEmpresa) Then
            sKeyImage = "SELLO"
        Else
            sKeyImage = "UON1"
        End If
        
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, sKeyImage)
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        If Not IsNull(oUON2.IdEmpresa) Then
            sKeyImage = "SELLO"
        Else
            sKeyImage = "UON2"
        End If
        
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, sKeyImage)
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        If Not IsNull(oUON3.IdEmpresa) Then
            sKeyImage = "SELLO"
        Else
            sKeyImage = "UON3"
        End If
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, sKeyImage)
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And (bRUO Or bRDep) Then
    
        For Each oDepAsoc In oDepsAsocN0
            
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
        
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
        Next
    
    Else
    
        'Departamentos
            
            For Each oDepAsoc In oDepsAsocN0
            
                scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
                nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            
            Next
                
        For Each oDepAsoc In oDepsAsocN1
            
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
End Sub

''' <summary>
''' Selecciona en el buscador el departamanto previamente seleccionado
''' </summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1 sg.</remarks>
Private Sub PreseleccionarDepartamento()
    Dim nodx As node
    Dim bSeleccionar As Boolean
    
    If sCodDep <> "" Then
        For Each nodx In tvwestrorg.Nodes
            If nodx.Image = "Departamento" And Mid(nodx.Tag, 5) = sCodDep Then
                bSeleccionar = True
                If sUON3 <> "" And sUON2 <> "" And sUON1 <> "" Then
                    If Not nodx.Parent.Parent Is Nothing Then
                        If Not nodx.Parent.Parent.Parent Is Nothing Then
                            If Mid(nodx.Parent.Tag, 5) <> sUON3 Or Mid(nodx.Parent.Parent.Tag, 5) <> sUON2 Or Mid(nodx.Parent.Parent.Parent.Tag, 5) <> sUON1 Then
                                bSeleccionar = False
                            End If
                        Else
                            bSeleccionar = False
                        End If
                    Else
                        bSeleccionar = False
                    End If
                ElseIf sUON2 <> "" And sUON1 <> "" Then
                    If Not nodx.Parent.Parent Is Nothing Then
                        If Mid(nodx.Parent.Tag, 5) <> sUON2 Or Mid(nodx.Parent.Parent.Tag, 5) <> sUON1 Then
                            bSeleccionar = False
                        End If
                    Else
                        bSeleccionar = False
                    End If
                ElseIf sUON1 <> "" Then
                    If Mid(nodx.Parent.Tag, 5) <> sUON1 Then
                        bSeleccionar = False
                    End If
                End If
                If bSeleccionar Then
                    nodx.Selected = True
                    Exit For
                End If
            End If
        Next
    End If
End Sub

Private Sub Form_Resize()

    If sOrigen <> "frmESTRORG" Then
    
        If Me.Height - 960 > 0 Then
            If m_bModif Then
                tvwestrorg.Height = Me.Height - 960
            Else
                tvwestrorg.Height = Me.Height - 645
            End If
        End If
        If Me.Width - 250 > 0 Then
            tvwestrorg.Width = Me.Width - 250
        End If
        
        If Me.Height - 810 > 0 Then
            cmdAceptar.Top = Me.Height - 810
            cmdCancelar.Top = Me.Height - 810
        End If
        cmdAceptar.Left = Me.Width * 0.3459
        cmdCancelar.Left = cmdAceptar.Left + 1185
    Else
    
        If Me.Height - 1300 > 0 Then
            If m_bModif Then
                tvwestrorg.Height = Me.Height - 1300
            Else
                tvwestrorg.Height = Me.Height - 645
            End If
        End If
        If Me.Width - 250 > 0 Then
            tvwestrorg.Width = Me.Width - 250
        End If
        
        If Me.Height - 810 > 0 Then
            chkTraslados.Top = Me.Height - 1150
            lblTraslados.Top = Me.Height - 1150
            
            cmdAceptar.Top = Me.Height - 810
            cmdCancelar.Top = Me.Height - 810
        End If
        cmdAceptar.Left = Me.Width * 0.3459
        cmdCancelar.Left = cmdAceptar.Left + 1185
    
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    sCodDep = ""
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    sOrigen = ""
End Sub

Private Sub tvwestrorg_Click()
    If tvwestrorg.selectedItem.Image = "Departamento" Then
        cmdAceptar.Enabled = True
    Else
        cmdAceptar.Enabled = False
    End If
End Sub
