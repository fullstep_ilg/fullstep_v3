VERSION 5.00
Begin VB.Form frmUSUEXT 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Añadir usuario"
   ClientHeight    =   2655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4950
   Icon            =   "frmUSUEXT.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2655
   ScaleWidth      =   4950
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkBloq 
      BackColor       =   &H00808000&
      Caption         =   "Cuenta bloqueada"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   1500
      TabIndex        =   4
      Top             =   1860
      Width           =   3195
   End
   Begin VB.Timer timUsuExt 
      Interval        =   60000
      Left            =   75
      Top             =   1800
   End
   Begin VB.PictureBox picEdit 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   4950
      TabIndex        =   10
      Top             =   2235
      Width           =   4950
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1350
         TabIndex        =   5
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2505
         TabIndex        =   6
         Top             =   60
         Width           =   1005
      End
   End
   Begin VB.PictureBox picDatos 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   2070
      Left            =   1500
      ScaleHeight     =   2070
      ScaleWidth      =   3495
      TabIndex        =   11
      Top             =   120
      Width           =   3495
      Begin VB.TextBox txtPwdConf 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   0
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   900
         Width           =   3150
      End
      Begin VB.TextBox txtPwd 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   0
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   465
         Width           =   3150
      End
      Begin VB.TextBox txtCod 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         MaxLength       =   20
         TabIndex        =   0
         Top             =   60
         Width           =   1230
      End
      Begin VB.TextBox txtNombre 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         MaxLength       =   100
         TabIndex        =   3
         Top             =   1320
         Width           =   3165
      End
   End
   Begin VB.Label lblPwdConf 
      BackStyle       =   0  'Transparent
      Caption         =   "Confirmación:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   225
      TabIndex        =   12
      Top             =   1080
      Width           =   1260
   End
   Begin VB.Label lblNombre 
      BackStyle       =   0  'Transparent
      Caption         =   "Nombre:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   225
      TabIndex        =   9
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Label lblCod 
      BackStyle       =   0  'Transparent
      Caption         =   "Código:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   255
      Left            =   210
      TabIndex        =   8
      Top             =   210
      Width           =   1200
   End
   Begin VB.Label lblPwd 
      BackStyle       =   0  'Transparent
      Caption         =   "Contraseña:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   225
      Left            =   195
      TabIndex        =   7
      Top             =   660
      Width           =   1260
   End
End
Attribute VB_Name = "frmUSUEXT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_iBloqueo As Integer
Private m_sCodigo As String
Private m_sContraseña As String
Private m_sContrafirma As String
Private m_sNombre As String


Private Sub cmdAceptar_Click()
Dim oUsuario As CUsuario
Dim oUsuarioExterno As CUsuarioExterno
Dim oIBaseDatos As IBaseDatos
Dim bAnyadido As Boolean
Dim bModificado As Boolean
Dim teserror As TipoErrorSummit
Dim dfechahoracrypt As Date
Dim sPassUsu As String
Dim sAux As String
Dim i As Integer

Select Case frmUSUARIOS.Accion

Case ACCUsuAnya
    
    '************ Validacion de datos *********
    
    If InStr(1, txtCod.Text, " ") <> 0 Then
        basMensajes.NoValido m_sCodigo
        txtCod.SetFocus
        Exit Sub
    End If
    
    If Trim(txtCod) = "" Then
        basMensajes.NoValido m_sCodigo
        txtCod.SetFocus
        Exit Sub
    End If
    
    If Trim(txtPwd) = "" Then
        basMensajes.NoValida m_sContraseña
        txtPwd.SetFocus
        Exit Sub
    End If
    
    If Trim(txtPwdConf) = "" Then
        basMensajes.NoValida m_sContrafirma
        txtPwdConf.SetFocus
        Exit Sub
    End If
    
    If Trim(txtNombre) = "" Then
        basMensajes.NoValido m_sNombre
        txtNombre.SetFocus
        Exit Sub
    End If
    
    If txtPwd <> txtPwdConf Then
        basMensajes.NoValida m_sContrafirma
        txtPwdConf.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    '************************************************
    Set oUsuarioExterno = oFSGSRaiz.generar_CUsuarioExterno
    
    Set oUsuario = oFSGSRaiz.generar_cusuario
    oUsuario.Cod = txtCod.Text
    ''' Principio de encriptación de la contraseña
    dfechahoracrypt = Now
    sAux = txtCod.Text & Format(dfechahoracrypt, "DD\/MM\/YYYY HH\:NN\:SS")
    For i = 1 To Len(sAux)
        If InStr(1, basUtilidades.sAuxMatrix, Mid(sAux, i, 1)) = 0 Then
            basMensajes.CodUsuarioNoValido (Mid(sAux, i, 1))
            Screen.MousePointer = vbNormal
            txtCod.SetFocus
            Exit Sub
        End If
    Next
    sPassUsu = basUtilidades.Encriptar(txtCod.Text, txtPwd.Text, True, UsuarioExterno, dfechahoracrypt)

    'Datos
    oUsuario.pwd = sPassUsu
    oUsuario.fecpwd = dfechahoracrypt
    'Fin datos
    ''' Fin de encriptación de contraseña
    
    If chkBloq.Value = vbChecked Then
        oUsuario.Bloq = gParametrosGenerales.giLOGPREBLOQ
        g_iBloqueo = oUsuario.Bloq
    Else
        oUsuario.Bloq = 0
        g_iBloqueo = 0
    End If
    
    oUsuario.Tipo = TIpoDeUsuario.UsuarioExterno

     oUsuarioExterno.nombre = txtNombre.Text
    Set oUsuario.UsuarioExterno = oUsuarioExterno

    ' Utilizo el interface de BD para guardar el usuario
    Set oIBaseDatos = oUsuario
    
    teserror = oIBaseDatos.AnyadirABaseDatos
    
    If teserror.NumError = TESnoerror Then
    
    '******* Registrar accion ***************
        If gParametrosGenerales.gbActivLog Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCUsuAnya, "Cod:" & Trim(txtCod.Text)
        End If
    '*****************************************
        Set frmUSUARIOS.g_oUsuario = oUsuario
    Else
        Screen.MousePointer = vbNormal
        TratarError teserror
        Exit Sub
    End If

Case ACCUsuMod
    
     '************ Validacion de datos *********
    
    If Trim(txtPwd) = "" Then
        basMensajes.NoValida m_sContraseña
        txtPwd.SetFocus
        Exit Sub
    End If
    
    If Trim(txtPwdConf) = "" Then
        basMensajes.NoValida m_sContrafirma
        txtPwdConf.SetFocus
        Exit Sub
    End If
    
    If Trim(txtNombre) = "" Then
        basMensajes.NoValido m_sNombre
        txtNombre.SetFocus
        Exit Sub
    End If
    
    If txtPwd <> txtPwdConf Then
        basMensajes.NoValida m_sContrafirma
        txtPwdConf.SetFocus
        Exit Sub
    End If
    
    '************************************************
    Screen.MousePointer = vbHourglass
    
    With frmUSUARIOS
        ''' Principio de encriptación de la contraseña
        dfechahoracrypt = Now
        sAux = txtCod.Text & Format(dfechahoracrypt, "DD\/MM\/YYYY HH\:NN\:SS")
        For i = 1 To Len(sAux)
            If InStr(1, basUtilidades.sAuxMatrix, Mid(sAux, i, 1)) = 0 Then
                basMensajes.CodUsuarioNoValido (Mid(sAux, i, 1))
                Screen.MousePointer = vbNormal
                txtCod.SetFocus
                Exit Sub
            End If
        Next
        sPassUsu = basUtilidades.Encriptar(txtCod.Text, txtPwd.Text, True, UsuarioExterno, dfechahoracrypt)
    
        'Datos
        .g_oUsuario.pwd = sPassUsu
        .g_oUsuario.fecpwd = dfechahoracrypt
        'Fin datos
        ''' Fin de encriptación de contraseña
    
        If chkBloq.Value = vbChecked Then
            .g_oUsuario.Bloq = gParametrosGenerales.giLOGPREBLOQ
        Else
            .g_oUsuario.Bloq = 0
        End If
        
        g_iBloqueo = 0
    
        .g_oUsuario.UsuarioExterno.nombre = txtNombre.Text
        
        teserror = .g_oIBaseDatos.FinalizarEdicionModificando
        
        If teserror.NumError = TESnoerror Then
            '******* Registrar accion ***************
            If gParametrosGenerales.gbActivLog Then
                oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCUsuMod, "Cod:" & txtCod
            End If
            '*****************************************
        Else
            Screen.MousePointer = vbNormal
            TratarError teserror
            Exit Sub
        End If
    
    End With
    
    
End Select

Screen.MousePointer = vbNormal

Unload Me
End Sub

Private Sub cmdCancelar_Click()
Unload Me
End Sub


Private Sub Form_Load()

Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2

CargarRecursos

txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodUSU

Select Case frmUSUARIOS.Accion

Case ACCusudet
    
    picEdit.Visible = False

Case ACCUsuMod

    txtCod.Enabled = False
    
End Select

If gParametrosGenerales.giLOGPREBLOQ = 0 Then
    chkBloq.Enabled = False
End If

End Sub

Private Sub Form_Unload(Cancel As Integer)

frmUSUARIOS.sstabUsu.TabEnabled(0) = True
End Sub


Private Sub timUsuExt_Timer()

Static ThisTime As Integer

If ThisTime = giMinutosDeRetardoEnVentanasDeEdicion - 1 Then
    ThisTime = 1
    Unload Me
Else
    ThisTime = ThisTime + 1
End If

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_USUEXT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        lblCod.Caption = Ador(0).Value
        m_sCodigo = Ador(0).Value
        Ador.MoveNext
        lblPwd.Caption = Ador(0).Value
        m_sContraseña = Ador(0).Value
        Ador.MoveNext
        lblNombre.Caption = Ador(0).Value
        m_sNombre = Ador(0).Value
        Ador.MoveNext
        chkBloq.Caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.Caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.Caption = Ador(0).Value
        Ador.MoveNext
        m_sContrafirma = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
    
        
End Sub


