VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmRESREUHojaAdj 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Hoja de adjudicaciones"
   ClientHeight    =   1605
   ClientLeft      =   3525
   ClientTop       =   2460
   ClientWidth     =   7140
   Icon            =   "frmRESREUHojaAdj.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1605
   ScaleWidth      =   7140
   Begin VB.CommandButton cmdDetProceDot 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6593
      Picture         =   "frmRESREUHojaAdj.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      UseMaskColor    =   -1  'True
      Width           =   315
   End
   Begin VB.TextBox txtDetProceDot 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   285
      Left            =   1080
      MaxLength       =   255
      TabIndex        =   2
      Top             =   480
      Width           =   5430
   End
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3540
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1200
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2400
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1200
      Width           =   1005
   End
   Begin MSComDlg.CommonDialog cmmdDot 
      Left            =   0
      Top             =   1500
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label lblPlantilla 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00808000&
      Caption         =   "Plantilla:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   210
      TabIndex        =   4
      Top             =   525
      Width           =   810
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BorderColor     =   &H00FFFFFF&
      Height          =   975
      Left            =   60
      Top             =   120
      Width           =   7035
   End
End
Attribute VB_Name = "frmRESREUHojaAdj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables para la hoja de adjudicaciones
Private m_sProceFormula As String

Private oFos As Scripting.FileSystemObject
Public oDestinos As CDestinos
Public oPagos As CPagos
Public oUnidades As CUnidades
Public sOrigen As String
Public g_oOrigen As Form
Private m_iWordVer As Integer

Private m_iNumItemsEnHoja As Integer

'Multilenguaje
Private sIdiGrupos As String
Private sIdiResultados As String
Private sidiconsumido As String
Private sidiImporteAdj As String
Private sIdiAhorroPorcen As String
Private sIdiGeneral As String
Private sIdiGrupo As String
Private sIdiCodigo As String
Private sIdiNombre As String
Private sIdiMensaje As String
Private sIdiItem As String


'Plantilla
Private sIdiPlantilla As String
'Generando hoja de adjudicaciones...
Private sIdiGenHojaAdj As String
'Abriendo Microsoft Word...
Private sIdiAbrWord As String
'Abriendo Plantilla
Private sIdiAgrPlant As String
'Proceso:
Private sIdiProceso As String
'Generando datos de procesos...
Private sIdiGenDatosProce As String
'Proveedor:
Private sIdiProveedor As String
'Generando datos del proveedor...
Private sIdiGenDatProve As String
 
'Pasando firmas.....
Private sIdiPasFirmas As String
'reuni�n
Private sIdiReunion As String
'Plantillas de Excel
Private sIdiXLT As String
'Plantillas de Word
Private sIdiDOT As String
'Generando hoja comparativa...
Private sIdiGenComp As String
'Abriendo Microsoft Excel...
Private sIdiAbrExcel As String
'Introduciendo funciones de c�lculo de ahorros
Private sIdiIntrFunc As String
'Transfiriendo datos de ahorros generales y de proceso
Private sIdiTransDatosGen As String
'Moneda
Private sIdiMoneda2 As String
'Adjudicaci�n directa
Private sIdiAdjDir As String
'Adjudicaci�n en reuni�n
Private sIdiAdjReu As String
'Transfiriendo datos de presupuestos
Private sIdiTransDatPres As String
'Pres. Global
Private sIdiPresGlobal As String
'Pres. abierto
Private sIdiPresAbierto As String
'Pres. consumido
Private sIdiPresConsumido As String
'Adjudicado
Private sIdiAdjudicado As String
'Ahorrado
Private sIdiAhorrado As String
'% Ahorrado
Private sIdiPorcentAhor As String
'Transfiriendo datos de proveedores
Private sIdiTransDatProve As String
'Generando comparativas de grupo"
Private sIdiGenerarGrupos As String
'Descripci�n
Private sIdiDescripcion As String
'Presupuesto
Private sIdiPresupuesto As String
'Objetivo
Private sIdiObj As String
'Ahorro Importe
Private sIdiAhorImp As String
'Cantidad
Private sIdiCantidad As String
'Adj%
Private sIdiPorcentAdj As String
'Direcci�n:
Private sIdiDirec As String
'C.P.:
Private sIdiCP As String
'Poblaci�n:
Private sIdiPobl As String
'Provincia:
Private sIdiProvi As String
'Pa�s:
Private sIdiPais As String
'Moneda:
Private sIdiMoneda As String
'Precio
Private sIdiPrecio As String
'Transfiriendo datos de adjudicaciones
Private sIdiTransDatAdj As String
'Destino:
Private sIdiDestino As String
'Pago:
Private sIdiPago As String
'Unidad:
Private sIdiUnidad As String
'Inicio:
Private sIdiInicio As String
'Fin:
Private sIdiFin As String
'Calculando totales
Private sIdiCalcTotales As String
'Total por proveedor
Private sIdiTotProve As String
'Ahorro
Private sIdiAhorro As String
'Importe
Private sIdiImporte As String
'Proveedor
Private sIdiProv As String
'Objetivos totales
Private sIdiTotObj As String

Private m_sFormatoNumber As String
Private m_sFormatoNumberPorcen  As String

Private m_sFormatoNumberItem As String
Private m_sFormatoNumberPorcenItem As String
Private m_sFormatoNumberPrecioItem As String
Private m_sFormatoNumberCantItem As String

Private m_sFormatoNumberGr As String
Private m_sFormatoNumberPorcenGr As String
Private m_sFormatoNumberPrecioGr As String
Private m_sFormatoNumberCantGr As String

'form:control de errores
Public m_bDescargarFrm As Boolean
Dim m_bActivado As Boolean
Private m_sMsgError As String

'Atributos item tarea 2707
Private ListaAtribBmk() As String
Private m_sSi As String
Private m_sNo As String

Private Sub cmdAceptar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm
    Dim oGrupo As CGrupo
    Dim sCodGrupo As String
    
    If sOrigen <> "" And sOrigen <> "ComparativaItem" Then
        'N�mero de decimales para los datos de la pesta�a general:
        m_sFormatoNumber = FormateoNumericoComp(g_oOrigen.m_oVistaSeleccionada.DecResult)
        m_sFormatoNumberPorcen = FormateoNumericoComp(g_oOrigen.m_oVistaSeleccionada.DecPorcen, True)

        'N�mero de decimales para los datos de la comparativa a nivel de grupo:
        If (Not g_oOrigen.m_oVistaSeleccionadaGr Is Nothing) Then
            m_sFormatoNumberGr = FormateoNumericoComp(g_oOrigen.m_oVistaSeleccionadaGr.DecResult)
            m_sFormatoNumberPorcenGr = FormateoNumericoComp(g_oOrigen.m_oVistaSeleccionadaGr.DecPorcen, True)
            m_sFormatoNumberPrecioGr = FormateoNumericoComp(g_oOrigen.m_oVistaSeleccionadaGr.DecPrecios)
            m_sFormatoNumberCantGr = FormateoNumericoComp(g_oOrigen.m_oVistaSeleccionadaGr.DecCant)
        Else
            'Carga la vista inicial
            If Not g_oOrigen.m_oProcesoSeleccionado.Grupos Is Nothing Then
                Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(1)
                'Grupo.CargarVistaInicial otra vez clave vista+tipovista+usuario.
                'Pero esta vez el usuario es INI.
                sCodGrupo = "INI" & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUSU - Len("INI"))
                sCodGrupo = "00" & sCodGrupo
                If Not oGrupo.ConfVistas Is Nothing Then
                    If oGrupo.ConfVistas.Item(sCodGrupo) Is Nothing Then
                        oGrupo.CargarVistaInicial oUsuarioSummit.Cod
                    End If
                Else
                    oGrupo.CargarVistaInicial oUsuarioSummit.Cod
                End If
                If Not oGrupo.ConfVistas Is Nothing Then
                    If Not oGrupo.ConfVistas.Item(sCodGrupo) Is Nothing Then
                        m_sFormatoNumberGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecResult)
                        m_sFormatoNumberPorcenGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecPorcen, True)
                        m_sFormatoNumberPrecioGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecPrecios)
                        m_sFormatoNumberCantGr = FormateoNumericoComp(oGrupo.ConfVistas.Item(sCodGrupo).DecCant)
                    Else
                        m_sFormatoNumberGr = FormateoNumericoComp(2)
                        m_sFormatoNumberPorcenGr = FormateoNumericoComp(2, True)
                        m_sFormatoNumberPrecioGr = FormateoNumericoComp(2)
                        m_sFormatoNumberCantGr = FormateoNumericoComp(2)
                    End If
                Else
                    m_sFormatoNumberGr = FormateoNumericoComp(2)
                    m_sFormatoNumberPorcenGr = FormateoNumericoComp(2, True)
                    m_sFormatoNumberPrecioGr = FormateoNumericoComp(2)
                    m_sFormatoNumberCantGr = FormateoNumericoComp(2)
                End If
                Set oGrupo = Nothing
            End If
        End If
    End If
    
  Select Case sOrigen

    Case "ComparativaItem"
        'N�mero de decimales para los datos de la comparativa de item:
        m_sFormatoNumberItem = FormateoNumericoComp(g_oOrigen.g_oVistaSeleccionada.DecResult)
        m_sFormatoNumberPorcenItem = FormateoNumericoComp(g_oOrigen.g_oVistaSeleccionada.DecPorcen, True)
        m_sFormatoNumberPrecioItem = FormateoNumericoComp(g_oOrigen.g_oVistaSeleccionada.DecPrecios)
        m_sFormatoNumberCantItem = FormateoNumericoComp(g_oOrigen.g_oVistaSeleccionada.DecCant)
            
        ObtenerHojaComparativaItem
    
    Case "frmOFECOMP2"
        ObtenerHojaComparativa
    
    Case "frmOFECOMP"
        ObtenerHojaAdjudicacion
        
    Case "frmRESREU"
        ObtenerHojaAdjudicacionReu
    
  End Select
  
  Unload Me
  Exit Sub
    
ERROR_Frm:
    If err.Number = 5834 Or err.Number = 5941 Then
        Resume Next
    End If
    'Si el error es que no encuentra un formato o bookmark continuamos
    'Resume Next
        MsgBox err.Description, vbCritical, "Fullstep"

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo ERROR_Frm
      Exit Sub
   End If

End Sub


Private Sub cmdCancelar_Click()
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
     
End Sub

Private Sub cmdDetProceDot_Click()
On Error GoTo ErrPlantilla
    
    cmmdDot.CancelError = True
    cmmdDot.FLAGS = cdlOFNHideReadOnly
    
    Select Case sOrigen
    Case "ComparativaItem"
        cmmdDot.Filter = sIdiXLT & " (*.xlt)|*.xlt"
    Case "frmOFECOMP2"
        cmmdDot.Filter = sIdiXLT & " (*.xlt)|*.xlt"
    Case "frmOFECOMP"
        cmmdDot.Filter = sIdiDOT & " (*.dot)|*.dot"
    Case "frmRESREU"
        cmmdDot.Filter = sIdiDOT & " (*.dot)|*.dot"
    End Select
    
    cmmdDot.FilterIndex = 0
    cmmdDot.ShowOpen

    txtDetProceDot = cmmdDot.filename
    
    Exit Sub
    
ErrPlantilla:
    
    Exit Sub

End Sub



Private Sub Form_Activate()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bActivado Then
        m_bActivado = True
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    CargarRecursos
    
    Set oFos = New Scripting.FileSystemObject
    Set oDestinos = oFSGSRaiz.Generar_CDestinos
    Set oUnidades = oFSGSRaiz.Generar_CUnidades
    Set oPagos = oFSGSRaiz.Generar_CPagos
    oDestinos.CargarTodosLosDestinos , , , , , , , , , , True, True
    oUnidades.CargarTodasLasUnidades
    oPagos.CargarTodosLosPagos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oFos = Nothing
    Set oDestinos = Nothing
    Set oUnidades = Nothing
    Set oPagos = Nothing
    Set g_oOrigen = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Function ActivarCalculosVersion9() As String
Dim str As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    str = "Private Sub Worksheet_Change(ByVal Target As Range)" & Chr(13)
    str = str & vbLf & "    Application.CalculateFull" & Chr(13)
    str = str & vbLf & "End Sub"
    
    ActivarCalculosVersion9 = str
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ActivarCalculosVersion9", err, Erl, , m_bActivado)
      Exit Function
   End If
   
End Function

Private Function ActivarCalculosVersion8() As String
Dim str As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    str = "Private Sub Worksheet_Change(ByVal Target As Range)" & Chr(13)
    str = str & vbLf & "    SendKeys ""%^{F9}"", True"
    str = str & vbLf & "End Sub"
    ActivarCalculosVersion8 = str
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ActivarCalculosVersion8", err, Erl, , m_bActivado)
      Exit Function
   End If
   
End Function


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    On Error Resume Next
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_RESREU_HOJAADJ, basPublic.gParametrosInstalacion.gIdioma)
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblPlantilla.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sIdiPlantilla = Ador(0).Value '5
        Ador.MoveNext
        sIdiGenHojaAdj = Ador(0).Value
        Ador.MoveNext
        sIdiAbrWord = Ador(0).Value
        Ador.MoveNext
        sIdiAgrPlant = Ador(0).Value
        Ador.MoveNext
        sIdiProceso = Ador(0).Value
        Ador.MoveNext
        sIdiGenDatosProce = Ador(0).Value '10
        Ador.MoveNext
        sIdiProveedor = Ador(0).Value
        Ador.MoveNext
        sIdiGenDatProve = Ador(0).Value
        Ador.MoveNext
        sIdiPasFirmas = Ador(0).Value
        Ador.MoveNext
        sIdiReunion = Ador(0).Value
        Ador.MoveNext
        sIdiXLT = Ador(0).Value '15
        Ador.MoveNext
        sIdiDOT = Ador(0).Value
        Ador.MoveNext
        sIdiGenComp = Ador(0).Value
        Ador.MoveNext
        sIdiAbrExcel = Ador(0).Value
        Ador.MoveNext
        sIdiIntrFunc = Ador(0).Value
        Ador.MoveNext
        sIdiTransDatosGen = Ador(0).Value '20
        Ador.MoveNext
        sIdiProceso = Ador(0).Value
        Ador.MoveNext
        sIdiMoneda2 = Ador(0).Value
        Ador.MoveNext
        sIdiAdjDir = Ador(0).Value
        Ador.MoveNext
        sIdiAdjReu = Ador(0).Value
        Ador.MoveNext
        sIdiTransDatPres = Ador(0).Value
        Ador.MoveNext
        sIdiPresGlobal = Ador(0).Value
        Ador.MoveNext
        sIdiPresAbierto = Ador(0).Value
        Ador.MoveNext
        sIdiPresConsumido = Ador(0).Value
        Ador.MoveNext
        sIdiAdjudicado = Ador(0).Value
        Ador.MoveNext
        sIdiAhorrado = Ador(0).Value
        Ador.MoveNext
        sIdiPorcentAhor = Ador(0).Value
        Ador.MoveNext
        sIdiTransDatProve = Ador(0).Value
        Ador.MoveNext
        sIdiDescripcion = Ador(0).Value
        Ador.MoveNext
        sIdiPresupuesto = Ador(0).Value
        Ador.MoveNext
        sIdiObj = Ador(0).Value
        Ador.MoveNext
        sIdiAhorImp = Ador(0).Value
        Ador.MoveNext
        sIdiCantidad = Ador(0).Value
        Ador.MoveNext
        sIdiPorcentAdj = Ador(0).Value
        Ador.MoveNext
        sIdiDirec = Ador(0).Value
        Ador.MoveNext
        sIdiCP = Ador(0).Value
        Ador.MoveNext
        sIdiPobl = Ador(0).Value
        Ador.MoveNext
        sIdiProvi = Ador(0).Value
        Ador.MoveNext
        sIdiPais = Ador(0).Value
        Ador.MoveNext
        sIdiMoneda = Ador(0).Value
        Ador.MoveNext
        sIdiPrecio = Ador(0).Value
        Ador.MoveNext
        sIdiTransDatAdj = Ador(0).Value
        Ador.MoveNext
        sIdiDestino = Ador(0).Value
        Ador.MoveNext
        sIdiPago = Ador(0).Value
        Ador.MoveNext
        sIdiUnidad = Ador(0).Value
        Ador.MoveNext
        sIdiInicio = Ador(0).Value
        Ador.MoveNext
        sIdiFin = Ador(0).Value
        Ador.MoveNext
        sIdiCalcTotales = Ador(0).Value
        Ador.MoveNext
        sIdiTotProve = Ador(0).Value
        Ador.MoveNext
        sIdiAhorro = Ador(0).Value
        Ador.MoveNext
        sIdiImporte = Ador(0).Value
        Ador.MoveNext
        sIdiProv = Ador(0).Value
        Ador.MoveNext
        sIdiTotObj = Ador(0).Value
        Ador.MoveNext
        sIdiResultados = Ador(0).Value 'Resultados
        Ador.MoveNext
        sidiconsumido = Ador(0).Value 'Consumido
        Ador.MoveNext
        sidiImporteAdj = Ador(0).Value 'ImporteAdj
        Ador.MoveNext
        sIdiAhorroPorcen = Ador(0).Value 'Ahorro Porcen
        Ador.MoveNext
        sIdiGeneral = Ador(0).Value 'General
        Ador.MoveNext
        sIdiGrupo = Ador(0).Value 'Grupo
        Ador.MoveNext
        sIdiGrupos = Ador(0).Value
        Ador.MoveNext
        sIdiCodigo = Ador(0).Value
        Ador.MoveNext
        sIdiNombre = Ador(0).Value
        Ador.MoveNext
        sIdiMensaje = Ador(0).Value
        Ador.MoveNext
        sIdiItem = Ador(0).Value
        Ador.MoveNext
        sIdiGenerarGrupos = Ador(0).Value
        Ador.MoveNext
        m_sSi = Ador(0).Value
        Ador.MoveNext
        m_sNo = Ador(0).Value
        
        Ador.Close
        Set Ador = Nothing
    
    End If

End Sub
Private Sub ComparativaGeneral(xlSheet As Object, iIni As Integer)
Dim sCelda As String
Dim sCeldaPresAbierto As String
Dim sCeldaPresConsumido As String
Dim sCeldaAdjudicado As String
Dim sCeldaAhorrado As String
Dim sCeldaAhorradoPorcen As String
Dim sLetra As String
Dim i As Integer
Dim orange As Object
Dim sComentario As String
Dim sCelda2 As String
Dim oGrupo As CGrupo
Dim iStart As Integer
Dim sCod As String
Dim oProve As CProveedor
Dim bMostrarGrupo As Boolean
Dim bMostrarProve As Boolean
Dim sLetraMax As String
Dim bCargar As Boolean
Dim scodProve As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With g_oOrigen.m_oProcesoSeleccionado
        sCelda = "A" & iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiProceso & " " & CStr(.Anyo) & "/" & .GMN1Cod & "/" & CStr(.Cod) & " " & .Den

        sCelda = "D" & iIni + 1
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiMoneda2

        sCelda = "E" & iIni + 1
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = .MonCod
        sCelda = "F" & iIni + 1
        
        If .PermitirAdjDirecta Then
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiAdjDir

            sCelda = "G" & iIni + 1
            xlSheet.Range(sCelda).NumberFormat = "dd/mm/yyyy"
            xlSheet.Range(sCelda) = .FechaPresentacion
        Else
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiAdjReu

            sCelda = "G" & iIni + 1
            xlSheet.Range(sCelda).NumberFormat = "dd/mm/yyyy"
            If .FechaProximaReunion = "" Then
                xlSheet.Range(sCelda) = .FechaPresentacion
            Else
                xlSheet.Range(sCelda) = .FechaProximaReunion
            End If
        End If
        
    End With
        
    Set orange = xlSheet.Range("A" & iIni & ":" & "G" & iIni + 1)
    orange.Font.Bold = True
    orange.borderaround , 2
    orange.Interior.Color = RGB(192, 192, 192)
    
    'Pasamos los datos de presupuestos
    
    sCelda = "A" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiResultados
    
    xlSheet.Range(sCelda).ColumnWidth = 12
    xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
    xlSheet.Range(sCelda).borderaround , 2
    
    sCelda = "A" & 4 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiProceso
    
    xlSheet.Range(sCelda).ColumnWidth = 12
    xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "B" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiPresAbierto
    xlSheet.Range(sCelda).ColumnWidth = 12
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
   
    sCeldaPresAbierto = "B" & 4 + iIni
    xlSheet.Range(sCeldaPresAbierto).NumberFormat = m_sFormatoNumber
    xlSheet.Range(sCeldaPresAbierto).borderaround , 2
    xlSheet.Range(sCeldaPresAbierto).Interior.Color = RGB(245, 245, 200)
    
    sCelda = "C" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiPresConsumido
    xlSheet.Range(sCelda).ColumnWidth = 12
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    
    sCeldaPresConsumido = "C" & 4 + iIni
    xlSheet.Range(sCeldaPresConsumido).NumberFormat = m_sFormatoNumber
    xlSheet.Range(sCeldaPresConsumido).borderaround , 2
    xlSheet.Range(sCeldaPresConsumido).Interior.Color = RGB(245, 245, 200)

    
    sCelda = "D" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiAdjudicado
    xlSheet.Range(sCelda).ColumnWidth = 12
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
        
    sCeldaAdjudicado = "D" & 4 + iIni
    xlSheet.Range(sCeldaAdjudicado).NumberFormat = m_sFormatoNumber
    xlSheet.Range(sCeldaAdjudicado).borderaround , 2
    xlSheet.Range(sCeldaAdjudicado).Interior.Color = RGB(245, 245, 200)
    
    sCelda = "E" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiAhorrado
    xlSheet.Range(sCelda).ColumnWidth = 12
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    
    sCeldaAhorrado = "E" & 4 + iIni
    xlSheet.Range(sCeldaAhorrado).NumberFormat = m_sFormatoNumber
    xlSheet.Range(sCeldaAhorrado).borderaround , 2
    
    sCelda = "F" & 3 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiPorcentAhor
    xlSheet.Range(sCelda).ColumnWidth = 12
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    
    sCeldaAhorradoPorcen = "F" & 4 + iIni
    
    xlSheet.Range(sCeldaAhorradoPorcen).NumberFormat = m_sFormatoNumberPorcen
    xlSheet.Range(sCeldaAhorradoPorcen).borderaround , 2
    
    xlSheet.Range(sCeldaPresAbierto) = "= PresupuestoAbiertoProceso()"
    xlSheet.Range(sCeldaPresConsumido) = "= PresupuestoConsumidoProceso()"
    'Lo dejamos para el final, para cuando ya est� todo cargado
    'xlSheet.Range(sCeldaAdjudicado) = "= AdjudicadoProceso()"
    xlSheet.Range(sCeldaAhorrado) = "= (C" & 4 + iIni & "-D" & 4 + iIni & ")"
    xlSheet.Range(sCeldaAhorradoPorcen) = "= ((E" & 4 + iIni & "/" & "C" & 4 + iIni & ")*100)"
    'Formatos
    With xlSheet.Range(sCeldaAhorrado).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
    End With
    With xlSheet.Range(sCeldaAhorrado).formatconditions.Add(1, 6, "0")
        .Interior.Color = RGB(253, 100, 72)
    End With
    xlSheet.Range(sCeldaAhorrado).NumberFormat = m_sFormatoNumber
    With xlSheet.Range(sCeldaAhorradoPorcen).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
    End With
        With xlSheet.Range(sCeldaAhorradoPorcen).formatconditions.Add(1, 6, "0")
        .Interior.Color = RGB(253, 100, 72)
    End With
    xlSheet.Range(sCeldaAhorradoPorcen).NumberFormat = m_sFormatoNumberPorcen
    xlSheet.Range(sCeldaPresAbierto).NumberFormat = m_sFormatoNumber
    xlSheet.Range(sCeldaPresConsumido).NumberFormat = m_sFormatoNumber
    xlSheet.Range(sCeldaAdjudicado).NumberFormat = m_sFormatoNumber
    
    '2�) Mostrar cabeceras con proveedores
    sCelda = "A" & 6 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiGrupos
    xlSheet.Range(sCelda & ":B" & 6 + iIni).Merge
    xlSheet.Range(sCelda & ":B" & 6 + iIni).Interior.Color = RGB(128, 128, 0)
    Set orange = xlSheet.Range(sCelda & ":B" & 8 + iIni)
    orange.Font.Bold = True
    orange.borderaround , 3
    
    sCelda = "A" & 7 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiCodigo

    xlSheet.Range(sCelda).Interior.Color = RGB(128, 128, 0)
    xlSheet.Range(sCelda).borderaround , 3
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "B" & 7 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiNombre

    xlSheet.Range(sCelda).Interior.Color = RGB(128, 128, 0)
    xlSheet.Range(sCelda).borderaround , 3
    xlSheet.Range(sCelda).Font.Bold = True
    
    sLetra = "B"
            
    For Each oProve In g_oOrigen.m_oProvesAsig
    
        bMostrarProve = True
        If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
            If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
            End If
        Else
            bMostrarProve = ProvVisible(oProve.Cod)
        End If
        If bMostrarProve Then
            i = i + 3
            'Prove
            sCelda = DevolverLetra(sLetra, i - 2) & 6 + iIni
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = oProve.Cod & " - " & oProve.Den
            sCelda2 = DevolverLetra(sLetra, i) & 6 + iIni
            xlSheet.Range(sCelda & ":" & sCelda2).Merge
            Set orange = xlSheet.Range(sCelda & ":" & sCelda2)
            orange.Font.Size = 6
            orange.Interior.Color = RGB(192, 192, 192)
            orange.Font.Bold = True
            orange.borderaround , 3
            Set orange = xlSheet.Range(sCelda & ":" & sCelda2)
            orange.Font.Size = 6
            orange.Font.Bold = True
            orange.borderaround , 3
            orange.WrapText = False
            orange.borderaround , 3
            
            g_oOrigen.m_oProvesAsig.CargarDatosProveedor oProve.Cod
            With oProve
                sComentario = .Den & vbLf
                sComentario = sComentario & sIdiDirec & " " & .Direccion
                sComentario = sComentario & vbLf & sIdiCP & " " & .cP
                sComentario = sComentario & vbLf & sIdiPobl & " " & .Poblacion
                sComentario = sComentario & vbLf & sIdiProvi & " " & .CodProvi & " " & .DenProvi
                sComentario = sComentario & vbLf & sIdiPais & " " & .CodPais
                sComentario = sComentario & vbLf & sIdiMoneda & " " & .CodMon
    
            End With
            
            xlSheet.Range(sCelda).AddComment sComentario
            
            'Consumido
            sCelda = DevolverLetra(sLetra, i - 2) & 7 + iIni
            xlSheet.Range(sCelda).ColumnWidth = 12
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sidiconsumido
    
            Set orange = xlSheet.Range(sCelda)
            orange.Font.Bold = True
            orange.Interior.Color = RGB(192, 192, 192)
            orange.borderaround , 2
            
            'Adjudicado
            sCelda = DevolverLetra(sLetra, i - 1) & 7 + iIni
            xlSheet.Range(sCelda).ColumnWidth = 12
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiAdjudicado
    
            Set orange = xlSheet.Range(sCelda)
            orange.Font.Bold = True
            orange.Interior.Color = RGB(192, 192, 192)
            orange.borderaround , 2
            
            'ahorrado
            sCelda = DevolverLetra(sLetra, i) & 7 + iIni
            xlSheet.Range(sCelda).ColumnWidth = 12
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiAhorrado
            
            Set orange = xlSheet.Range(sCelda)
            orange.Font.Bold = True
            orange.borderaround , 2
            orange.Interior.Color = RGB(192, 192, 192)
        
            sLetraMax = DevolverLetra(sLetra, i)
        End If
        
    Next
    
    iStart = 8 + iIni
    
    'PONEMOS LOS GRUPOS
    For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
      bMostrarGrupo = True
      If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGrupo = False
        End If
      End If
      If bMostrarGrupo = True Then
        sLetra = "A"
        sCelda = sLetra & iStart
        
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = oGrupo.Codigo
        xlSheet.Range(sCelda).Interior.Color = RGB(128, 128, 0)
        xlSheet.Range(sCelda).borderaround , 2
            
        sLetra = DevolverLetra(sLetra, 1)
        sCelda = sLetra & iStart
            
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = oGrupo.Den
        xlSheet.Range(sCelda).Interior.Color = RGB(128, 128, 0)
        xlSheet.Range(sCelda).borderaround , 2
        xlSheet.Range(sCelda).WrapText = True
        
        sLetra = "C"
        Dim sLetraProve As String
        sLetraProve = "F"
        
        m_iNumItemsEnHoja = DevolverItemsConfirmados(oGrupo)
        
        For Each oProve In g_oOrigen.m_oProvesAsig
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                    bCargar = False
                End If
            End If
        
            bMostrarProve = True
            If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                    bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                End If
            Else
                bMostrarProve = ProvVisible(oProve.Cod)
            End If
            If bMostrarProve Then
        
                sCod = CStr(oGrupo.Codigo) & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                sCelda = sLetra & iStart
                
                'Consumido
                If bCargar Then
                    xlSheet.Range(sCelda) = "= PresupuestoConsumidoProveedorGrupo(""" & sLetraProve & """,""" & oGrupo.Codigo & """," & m_iNumItemsEnHoja & ")"
                    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
                Else
                    xlSheet.Range(sCelda).clearcontents
                    xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
                End If
                xlSheet.Range(sCelda).borderaround , 2
                xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumber
                'adj
                sLetra = DevolverLetra(sLetra, 1)
                sCelda = sLetra & iStart
                If bCargar Then
                    xlSheet.Range(sCelda) = "= AdjudicadoProveedorGrupoConAtrib(""" & sLetraProve & """,""" & oGrupo.Codigo & """," & m_iNumItemsEnHoja & ")"
                    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
                Else
                    xlSheet.Range(sCelda).clearcontents
                    xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
                End If
                xlSheet.Range(sCelda).borderaround , 2
                xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumber
                'ahorrado
                sLetra = DevolverLetra(sLetra, 1)
                sCelda = sLetra & iStart
                If bCargar Then
                    xlSheet.Range(sCelda) = "= (" & DevolverLetra(sLetra, -2) & iStart & "-" & DevolverLetra(sLetra, -1) & iStart & ")"
                    xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumber
                Else
                    xlSheet.Range(sCelda).clearcontents
                    xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
                End If
                xlSheet.Range(sCelda).borderaround , 2
                
                'Formatos
                With xlSheet.Range(sCelda).formatconditions.Add(1, 5, "0")
                        .Interior.Color = RGB(162, 250, 158)
                End With
                With xlSheet.Range(sCelda).formatconditions.Add(1, 6, "0")
                    .Interior.Color = RGB(253, 100, 72)
                End With
                
                sLetra = DevolverLetra(sLetra, 1)
                If bCargar Then
                    sLetraProve = DevolverLetra(sLetraProve, 1)
                End If
            End If
            
        Next
         iStart = iStart + 1
      End If
    Next
            
    '4�) Mostrar totales y cabeceras de proveedores
            
    iStart = iStart + 1
    i = 0
    sLetra = "B"
            
    For Each oProve In g_oOrigen.m_oProvesAsig
        bMostrarProve = True
        If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
            If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
            End If
        Else
            bMostrarProve = ProvVisible(oProve.Cod)
        End If
        
        If bMostrarProve Then
            i = i + 3
            sCelda = DevolverLetra(sLetra, i - 2) & iStart
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = oProve.Cod & " - " & oProve.Den
            sCelda2 = DevolverLetra(sLetra, i) & iStart
            xlSheet.Range(sCelda & ":" & sCelda2).Merge
            Set orange = xlSheet.Range(sCelda & ":" & sCelda2)
            orange.Font.Size = 6
            orange.Interior.Color = RGB(192, 192, 192)
            orange.Font.Bold = True
            orange.borderaround , 3
            
            Set orange = xlSheet.Range(sCelda & ":" & sCelda2)
            orange.Font.Size = 6
            orange.Font.Bold = True
            orange.borderaround , 3
            
            'Consumido
            sCelda = DevolverLetra(sLetra, i - 2) & 1 + iStart
            xlSheet.Range(sCelda).ColumnWidth = 12
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sidiconsumido
    
            Set orange = xlSheet.Range(sCelda)
            orange.Font.Bold = True
            orange.Interior.Color = RGB(192, 192, 192)
            orange.borderaround , 2
            
            sCelda2 = DevolverLetra(sLetra, i - 2) & 2 + iStart
            xlSheet.Range(sCelda2).Value = "= PresupuestoConsumidoProveedor(""" & DevolverLetra(sLetra, i - 2) & """)"
            xlSheet.Range(sCelda2).NumberFormat = m_sFormatoNumber
            Set orange = xlSheet.Range(sCelda2)
            orange.Font.Bold = True
            orange.Interior.Color = RGB(192, 192, 192)
            orange.borderaround , 2
            
            'Adjudicado
            sCelda = DevolverLetra(sLetra, i - 1) & 1 + iStart
            xlSheet.Range(sCelda).ColumnWidth = 12
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiAdjudicado
    
            Set orange = xlSheet.Range(sCelda)
            orange.Font.Bold = True
            orange.Interior.Color = RGB(192, 192, 192)
            orange.borderaround , 2
            
            sCelda2 = DevolverLetra(sLetra, i - 1) & 2 + iStart
            xlSheet.Range(sCelda2).Value = "= AdjudicadoProveedor(""" & DevolverLetra(sLetra, i - 1) & """)"
            xlSheet.Range(sCelda2).NumberFormat = m_sFormatoNumber
            Set orange = xlSheet.Range(sCelda2)
            orange.Font.Bold = True
            orange.Interior.Color = RGB(192, 192, 192)
            orange.borderaround , 2
            
            'ahorrado
            sCelda = DevolverLetra(sLetra, i) & 1 + iStart
            xlSheet.Range(sCelda).ColumnWidth = 12
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiAhorrado
            Set orange = xlSheet.Range(sCelda)
            orange.Font.Bold = True
            orange.Interior.Color = RGB(192, 192, 192)
            orange.borderaround , 2
            
            sCelda2 = DevolverLetra(sLetra, i) & 2 + iStart
            xlSheet.Range(sCelda2).Value = "= (" & DevolverLetra(sLetra, i - 2) & 2 + iStart & "-" & DevolverLetra(sLetra, i - 1) & 2 + iStart & ")"
            xlSheet.Range(sCelda2).NumberFormat = m_sFormatoNumber
            Set orange = xlSheet.Range(sCelda2)
            orange.Font.Bold = True
            orange.borderaround , 2
            'Formatos
            With orange.formatconditions.Add(1, 5, "0")
                    .Interior.Color = RGB(162, 250, 158)
            End With
            With orange.formatconditions.Add(1, 6, "0")
                .Interior.Color = RGB(253, 100, 72)
            End With
        
        End If
        
    Next
     
    sLetra = "A"
    sCelda = sLetra & iStart + 10
    
    '@@dpd  Incidencia 19317
    'Para office 2007 es obligatorio seleccionar la hoja para establecer el �rea de impresi�n
    xlSheet.Select
    
    xlSheet.PageSetup.PrintArea = "A1:" & sLetraMax & iStart + 10

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ComparativaGeneral", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>
''' Obtener Hoja Comparativa
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub ObtenerHojaComparativa()
Dim xlApp As Object
Dim xlBook As Object
Dim xlSheet As Object
Dim iIni As Integer
Dim sVersion As String
Dim iVersion As Integer
Dim oProject As Object
Dim oModule As Object
Dim i As Integer
Dim oAsig As COferta
Dim bMostrarGrupo As Boolean
Dim bMostrarProve As Boolean
Dim j As Integer
Dim oAtribs As CAtributos

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error GoTo ERROR_Frm

    'Comprobar las plantillas
    If UCase(Right(txtDetProceDot, 3)) <> "XLT" Then
        oMensajes.NoValida sIdiPlantilla
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
    
    If Not oFos.FileExists(txtDetProceDot) Then
        oMensajes.PlantillaNoEncontrada txtDetProceDot
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    Me.Hide

    frmESPERA.lblGeneral.caption = sIdiGenComp
    frmESPERA.lblGeneral.Refresh
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.ProgressBar1.Max = 5
    frmESPERA.ProgressBar2.Max = 100
    frmESPERA.Show
    DoEvents
    
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblDetalle = sIdiAbrExcel
    frmESPERA.lblDetalle.Refresh
    
    'Crear aplicaci�n excell
    Set xlApp = CreateObject("Excel.Application")
    sVersion = xlApp.VERSION
    
    iIni = basPublic.gParametrosInstalacion.giLongCabComp
    
    If iIni <= 0 Then
        iIni = 1
    End If
    
    Set xlBook = xlApp.Workbooks.Add(txtDetProceDot)
    
    frmESPERA.ProgressBar1.Value = 2
    frmESPERA.lblDetalle = sIdiIntrFunc
    frmESPERA.lblDetalle.Refresh
    
    'Primero introducimos las funciones de c�lculo de ahorros necesarias
    '********************************************************************
    Set oProject = xlApp.VBE.VBProjects.Item(1)
    Set oModule = oProject.VBComponents.Add(1)
    'A�adimos el option explicit y las variables de m�dulo
    oModule.codemodule.addfromstring CabeceraModulo
    'El split
    oModule.codemodule.addfromstring Split97
    'Las de proceso
    oModule.codemodule.addfromstring PresupuestoAbiertoProceso
    oModule.codemodule.addfromstring PresupuestoConsumidoProceso
    oModule.codemodule.addfromstring AdjudicadoProceso
    oModule.codemodule.addfromstring AplicarAtributosProceso
    oModule.codemodule.addfromstring CalcularImporteAdjudicadoProceso
    oModule.codemodule.addfromstring TodoElProcesoAQueProveedor
    oModule.codemodule.addfromstring AhorroTotalSegunObjetivo
    oModule.codemodule.addfromstring ImporteTotalSegunObjetivo
    frmESPERA.ProgressBar2.Value = 20
    
    'Las de grupo
    oModule.codemodule.addfromstring PresupuestoAbiertoGrupo
    oModule.codemodule.addfromstring PresupuestoConsumidoGrupo
    oModule.codemodule.addfromstring AdjudicadoGrupo
    oModule.codemodule.addfromstring CalcularImporteAdjudicadoGrupo
    oModule.codemodule.addfromstring TodoElGrupoAQueProveedor
    oModule.codemodule.addfromstring AplicarAtributosGrupo
    
    frmESPERA.ProgressBar2.Value = 40
    
    'Las de item
    oModule.codemodule.addfromstring PresupuestoAbiertoItem
    oModule.codemodule.addfromstring PresupuestoConsumidoItem
    oModule.codemodule.addfromstring AdjudicadoItem
    oModule.codemodule.addfromstring CalcularImporteAdjudicadoItem
    oModule.codemodule.addfromstring TodoElItemAQueProveedor
    oModule.codemodule.addfromstring AplicarAtributosItem
    oModule.codemodule.addfromstring ObtenerValorAtrib
    oModule.codemodule.addfromstring AplicarValorAtrib
    oModule.codemodule.addfromstring IncrementarColumna
    
    
    
    frmESPERA.ProgressBar2.Value = 80
    'Las de totales de proveedor
    oModule.codemodule.addfromstring AhorroProveedor
    oModule.codemodule.addfromstring ImporteProveedor
    oModule.codemodule.addfromstring PresupuestoConsumidoProveedor
    oModule.codemodule.addfromstring AdjudicadoProveedor
    oModule.codemodule.addfromstring PresupuestoConsumidoProveedorGrupo
    oModule.codemodule.addfromstring AdjudicadoProveedorGrupo
    oModule.codemodule.addfromstring AdjudicadoProveedorGrupoConAtrib
    
    oModule.codemodule.addfromstring ObtenerProveedorItem
    
    frmESPERA.ProgressBar2.Value = 100
    
    'Guardar todos los valores de los atributos de item/proveedor
    'Una hoja para cada grupo_atributo
    Dim oGrupo As CGrupo
    Dim oatrib As CAtributo
    Dim oProve As CProveedor
    Dim sLetra As String
    Dim oItem As CItem
    Dim sCelda As String
       
    'Vamos a guardar los valores de los atributos en hojas de la excel
    'Atributos de ambito proceso en una
    Set xlSheet = xlBook.Sheets.Add
    xlSheet.Name = "@P@R@O@C@E@"
    xlSheet.Visible = False
    i = 2
    Set oAtribs = g_oOrigen.m_oProcesoSeleccionado.ATRIBUTOS
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
            If oatrib.PrecioAplicarA < 3 Then
                    For Each oProve In g_oOrigen.m_oProvesAsig
                        bMostrarProve = True
                        If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                            If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                                bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                            End If
                        Else
                            bMostrarProve = ProvVisible(oProve.Cod)
                        End If
                        If bMostrarProve Then
                            If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas Is Nothing Then
                                Set oAsig = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod)
                                If Not oAsig Is Nothing Then
                                    sLetra = BuscarLetraProve(xlSheet, oProve.Cod, "B")
                                    sCelda = sLetra & i
                                    xlSheet.Range("A" & i).NumberFormat = "@"
                                    xlSheet.Range("A" & i).Value = TraducirPrecioFormula(oatrib.PrecioFormula)
                                    xlSheet.Range(sCelda).NumberFormat = "@"
                                    xlSheet.Range(sCelda).Value = oatrib.Cod
                                    If oatrib.PrecioAplicarA = 1 Then
                                         For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
                                            If EstaAplicado(oatrib.idAtribProce, oGrupo.Codigo) Then
                                                xlSheet.Range(sLetra & (i + 1)).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod)
                                            End If
                                         Next
                                    Else
                                        If EstaAplicado(oatrib.idAtribProce) Then
                                           xlSheet.Range(sLetra & (i + 1)).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod)
                                        End If
                                    End If
                               End If
                            End If
                        End If
                    Next
                    i = i + 2
            End If
        Next
    End If
    Set oAtribs = Nothing
    'Atributos de ambito grupo en otra
    Set xlSheet = xlBook.Sheets.Add
    xlSheet.Name = "@G@R@U@"
    xlSheet.Visible = False
    i = 2
    Set oAtribs = g_oOrigen.m_oProcesoSeleccionado.AtributosGrupo
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
            If oatrib.PrecioAplicarA = 2 Or oatrib.PrecioAplicarA = 1 Then
                    'Es de proceso luego est� en todos los grupos y todos los proves
                    For Each oProve In g_oOrigen.m_oProvesAsig
                        
                        bMostrarProve = True
                        If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                            If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                                bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                            End If
                        Else
                            bMostrarProve = ProvVisible(oProve.Cod)
                        End If
                        If bMostrarProve Then
                            j = 1
                            For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
                            
                                'Pongo todos los grupos por que har� referencia al numero de grupos
                                    xlSheet.Range("A" & i + j).NumberFormat = "@"
                                    xlSheet.Range("A" & i + j).Value = TraducirPrecioFormula(oatrib.PrecioFormula)
                                    xlSheet.Range("B" & i + j).NumberFormat = "@"
                                    xlSheet.Range("B" & i + j).Value = oGrupo.Codigo
                                    sLetra = BuscarLetraProve(xlSheet, oProve.Cod, "C")
                                    sCelda = sLetra & i
                                                        
                                    If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas Is Nothing Then
                                        Set oAsig = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod)
                                        If Not oAsig Is Nothing Then
                                            xlSheet.Range(sCelda).NumberFormat = "@"
                                            xlSheet.Range(sCelda).Value = oatrib.Cod
                                            xlSheet.Range(sLetra & i + j).NumberFormat = "@"
                                            If EstaAplicado(oatrib.idAtribProce, oGrupo.Codigo) Then
                                                xlSheet.Range(sLetra & i + j).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod, oGrupo.Codigo)
                                            End If
                                        End If
                                    End If
                                    j = j + 1
                            Next
                        End If
                    Next
                    i = i + g_oOrigen.m_oProcesoSeleccionado.Grupos.Count + 1
                End If
        Next
    End If
    Set oAtribs = Nothing
    'Guardar todos los valores de los atributos de ambito item
    'Una hoja para cada grupo_atributo
    Set oAtribs = g_oOrigen.m_oProcesoSeleccionado.AtributosItem
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
             If oatrib.PrecioAplicarA = 2 Then
                 'Comprobar que la hoja no exista********************
                 If IsNull(oatrib.codgrupo) Then
                     'Es de proceso luego est� en todos los grupos
                     For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
                       bMostrarGrupo = True
                       If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
                         If Not (g_oOrigen.m_oVistaSeleccionada.ConfVistasProceSobre.Item(CStr(oGrupo.Sobre)).Visible And g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 1) Then
                             bMostrarGrupo = False
                         End If
                       End If
                       If bMostrarGrupo = True Then
                         Set xlSheet = xlBook.Sheets.Add
                         xlSheet.Name = "@" & oGrupo.Codigo & "@" & oatrib.Cod
                         xlSheet.Visible = False
                         xlSheet.Range("A1").Value = TraducirPrecioFormula(oatrib.PrecioFormula)
                         For Each oProve In g_oOrigen.m_oProvesAsig
                            bMostrarProve = True
                            If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                                If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                                    bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                                End If
                            Else
                                bMostrarProve = ProvVisible(oProve.Cod)
                            End If
                            If bMostrarProve Then
                                If Not oGrupo.UltimasOfertas Is Nothing Then
                                  Set oAsig = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                  If Not oAsig Is Nothing Then
                                      sLetra = BuscarLetraProve(xlSheet, oProve.Cod, "B")
                                      For Each oItem In oGrupo.Items
                                            sCelda = sLetra & oItem.Id + 1
                                            xlSheet.Range(sCelda).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod, , oItem.Id)
                                      Next
                                  End If
                                End If
                            End If
                         Next
                       End If
                     Next
                 Else
                     
                     If Not ExisteLaHoja(xlBook, "@" & oatrib.codgrupo & "@" & oatrib.Cod) Then
                         Set xlSheet = xlBook.Sheets.Add
                         xlSheet.Name = "@" & oatrib.codgrupo & "@" & oatrib.Cod
                         xlSheet.Visible = False
                     Else
                         Set xlSheet = xlBook.Sheets.Item("@" & oatrib.codgrupo & "@" & oatrib.Cod)
                     End If
                     
                     Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(oatrib.codgrupo)
                     If Not oGrupo Is Nothing Then
                        bMostrarGrupo = True
                        If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
                          If Not (g_oOrigen.m_oVistaSeleccionada.ConfVistasProceSobre.Item(CStr(oGrupo.Sobre)).Visible And g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 1) Then
                              bMostrarGrupo = False
                          End If
                        End If
                        If bMostrarGrupo = True Then
                            xlSheet.Range("A1").Value = TraducirPrecioFormula(oatrib.PrecioFormula)
    
                            For Each oProve In g_oOrigen.m_oProvesAsig
                               bMostrarProve = True
                               If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                                    If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                                        bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                                    End If
                               Else
                                    bMostrarProve = ProvVisible(oProve.Cod)
                               End If
                               If bMostrarProve Then
                                    Set oAsig = Nothing
                                    DoEvents
                                    If Not oGrupo.UltimasOfertas Is Nothing Then
                                         Set oAsig = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                         
                                         If Not oAsig Is Nothing Then
                                             sLetra = BuscarLetraProve(xlSheet, oProve.Cod, "B")
                                             For Each oItem In oGrupo.Items
                                                    sCelda = sLetra & oItem.Id + 1
                                                    xlSheet.Range(sCelda).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod, , oItem.Id)
                                             Next
                                        End If
                                    End If
                               End If
                            Next
    
                        End If
                     End If
                 End If
                 
             End If
         Next
    End If
    Set oAtribs = Nothing
    Dim iNumSheets As Integer
    iNumSheets = xlBook.Sheets.Count
    
    'Nombramos las hojas con los c�digos de grupos
    '******************************************************************
    For i = 1 To g_oOrigen.m_oProcesoSeleccionado.Grupos.Count
        bMostrarGrupo = True
        If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
            If Not (g_oOrigen.m_oVistaSeleccionada.ConfVistasProceSobre.Item(CStr(g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(i).Sobre)).Visible And g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(i).Sobre)).Estado = 1) Then
                bMostrarGrupo = False
            End If
        End If
        If bMostrarGrupo = True Then
            xlApp.Sheets(iNumSheets).Select
            xlApp.Sheets(iNumSheets).Copy After:=xlApp.Sheets(iNumSheets)
        End If
    Next
    
    xlBook.Sheets.Item(iNumSheets).Name = sIdiGeneral
    j = 1
    For i = 1 To g_oOrigen.m_oProcesoSeleccionado.Grupos.Count
        bMostrarGrupo = True
        If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
            If Not (g_oOrigen.m_oVistaSeleccionada.ConfVistasProceSobre.Item(CStr(g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(i).Sobre)).Visible And g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(i).Sobre)).Estado = 1) Then
                bMostrarGrupo = False
            End If
        End If
        If bMostrarGrupo = True Then
            xlBook.Sheets.Item(j + iNumSheets).Name = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(i).Codigo
            j = j + 1
        End If
    Next
    
   'Rellenamos cada hoja de grupo
    frmESPERA.lblContacto.caption = ""
    frmESPERA.ProgressBar2.Value = 0
    frmESPERA.ProgressBar1.Value = 3
    
    frmESPERA.lblDetalle = sIdiGenerarGrupos
    frmESPERA.lblDetalle.Refresh
    
    For i = iNumSheets + 1 To (j - 1) + iNumSheets
        ComparativaDeGrupo xlBook.Sheets.Item(i), iIni
    Next
   
   frmESPERA.ProgressBar1.Value = 4
   frmESPERA.lblDetalle = sIdiTransDatosGen
   frmESPERA.Refresh
   frmESPERA.lblDetalle.Refresh
   DoEvents
   
   Set xlSheet = xlBook.Sheets(sIdiGeneral)
   
   'Rellenamos la comparativa general de resumen
   ComparativaGeneral xlSheet, iIni
   If IsNumeric(Left(sVersion, InStr(sVersion, ".") - 1)) Then
        iVersion = Left(sVersion, InStr(sVersion, ".") - 1)
   Else
        If Left(sVersion, 1) = "8" Then iVersion = 8
        If Left(sVersion, 3) = "9.0" Then iVersion = 9
        If Left(sVersion, 4) = "10.0" Then iVersion = 10
        If Left(sVersion, 4) = "11.0" Then iVersion = 11
   End If
   
   Dim iCalculation As Long
   If iVersion >= 10 Then  'Ponemos el calculo en manual pq sino falla
        iCalculation = xlApp.calculation
        xlApp.calculation = -4135
   End If
   xlApp.Sheets(sIdiGeneral).Select
   xlApp.Sheets(sIdiGeneral).Range("D" & basPublic.gParametrosInstalacion.giLongCabComp + 4) = "= AdjudicadoProceso()"
   xlApp.Sheets(sIdiGeneral).Range("D" & basPublic.gParametrosInstalacion.giLongCabComp + 4).NumberFormat = m_sFormatoNumber
   
   Dim lNumitems As Long
   
   For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
        bMostrarGrupo = True
        If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
            If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                bMostrarGrupo = False
            End If
        End If
        If bMostrarGrupo = True Then
            lNumitems = lNumitems + DevolverItemsConfirmados(oGrupo)
        End If
   Next
   
   If lNumitems > 100 Then
        If oMensajes.PreguntaActivarCalculosExcel = vbYes Then
            
            If iVersion < 9 Then
                 For i = 1 To xlApp.VBE.ActiveVBProject.VBComponents.Count
                    If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Type = 100 Then
                     If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Name <> "ThisWorkbook" Then
                        xlApp.VBE.ActiveVBProject.VBComponents.Item(i).codemodule.addfromstring ActivarCalculosVersion8
                     End If
                    End If
                 Next
            Else
                 For i = 1 To xlApp.VBE.ActiveVBProject.VBComponents.Count
                    If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Type = 100 Then
                     If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Name <> "ThisWorkbook" Then
                     
                         xlApp.VBE.ActiveVBProject.VBComponents.Item(i).codemodule.addfromstring ActivarCalculosVersion9
                     End If
                    End If
                 Next
                
            End If
        End If
    Else
        If iVersion < 9 Then
            For i = 1 To xlApp.VBE.ActiveVBProject.VBComponents.Count
               If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Type = 100 Then
                If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Name <> "ThisWorkbook" Then
                    xlApp.VBE.ActiveVBProject.VBComponents.Item(i).codemodule.addfromstring ActivarCalculosVersion8
                End If
               End If
            Next
        Else
            For i = 1 To xlApp.VBE.ActiveVBProject.VBComponents.Count
               If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Type = 100 Then
                   If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Name <> "ThisWorkbook" Then
                       xlApp.VBE.ActiveVBProject.VBComponents.Item(i).codemodule.addfromstring ActivarCalculosVersion9
                   End If
               End If
            Next
        End If
            
    End If
    If iVersion >= 9 Then
        xlApp.calculatefull
    End If
   If iVersion >= 10 Then  'Ponemos el calculo en manual pq sino falla
        xlApp.calculation = iCalculation
   End If
   
    frmESPERA.ProgressBar1.Value = 5
            
    xlApp.Visible = True
    BringWindowToTop xlApp.hWnd
    
    Screen.MousePointer = vbNormal
    Unload frmESPERA
    DoEvents
    Exit Sub
    
ERROR_Frm:

    If err.Number = 1004 Then
        Screen.MousePointer = vbNormal
        If (MsgBox(sIdiMensaje, vbExclamation, "FULLSTEP") = vbYes) Then
            Unload frmESPERA
            xlApp.Quit
            Set xlApp = Nothing
            Exit Sub
        End If
    Else
        Screen.MousePointer = vbNormal
        MsgBox err.Description
        If err.Number = 7 Then
            xlApp.Visible = True
            Unload frmESPERA
            Exit Sub
        End If
    End If
    
    Unload frmESPERA
    
    xlApp.Quit
    Set xlApp = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ObtenerHojaComparativa", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub

Private Function PresupuestoAbiertoProceso() As String
Dim sCodigo As String
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoAbiertoProceso() As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"

'Recorremos cada grupo
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        sCodigo = sCodigo & vbLf & "dImporte = dImporte + PresupuestoAbiertoGrupo(""" & oGrupo.Codigo & """)"
    End If
Next

sCodigo = sCodigo & vbLf & "PresupuestoAbiertoProceso= dImporte"
sCodigo = sCodigo & vbLf & "End Function"

PresupuestoAbiertoProceso = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoAbiertoProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function PresupuestoAbiertoGrupo() As String
Dim sCodigo As String
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoAbiertoGrupo(sCodGrupo As String) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim iNumItems As Integer"
sCodigo = sCodigo & vbLf & "Dim I As Integer"

sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 11 "
sCodigo = sCodigo & vbLf & "Select Case sCodGrupo"
        
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        sCodigo = sCodigo & vbLf & "    Case """ & oGrupo.Codigo & """"
        sCodigo = sCodigo & vbLf & "        iNumItems =" & DevolverItemsConfirmados(oGrupo)
    End If
Next
sCodigo = sCodigo & vbLf & "End Select"
sCodigo = sCodigo & vbLf & "For I = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "    dImporte = dImporte + PresupuestoAbiertoItem(sCodGrupo, iFila)"
sCodigo = sCodigo & vbLf & "    iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "Next I"
sCodigo = sCodigo & vbLf & "PresupuestoAbiertoGrupo = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
PresupuestoAbiertoGrupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoAbiertoGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Private Function PresupuestoAbiertoItem() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoAbiertoItem(sCodGrupo As String, iFila As Integer) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim sCeldaCant As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaPres As String"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim sLetra As String"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "'IFila contendr� la fila donde est� la cantidad del item"
sCodigo = sCodigo & vbLf & "Set xlsheet = Excel.Sheets.Item(sCodGrupo)"
sCodigo = sCodigo & vbLf & "sLetra = ""C"""
sCodigo = sCodigo & vbLf & "    sCeldaPres = sLetra & iFila - 1"
sCodigo = sCodigo & vbLf & "    sCeldaCant = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    "
sCodigo = sCodigo & vbLf & "    If xlsheet.Range(sCeldaPres).Value <> """" Then"
sCodigo = sCodigo & vbLf & "        dImporte = xlsheet.Range(sCeldaPres).Value"
sCodigo = sCodigo & vbLf & "    Else"
sCodigo = sCodigo & vbLf & "        dImporte = 0"
sCodigo = sCodigo & vbLf & "    End If"
    
sCodigo = sCodigo & vbLf & "    If xlsheet.Range(sCeldaCant).Value <> """" Then"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte * xlsheet.Range(sCeldaCant).Value"
sCodigo = sCodigo & vbLf & "    Else"
sCodigo = sCodigo & vbLf & "        dImporte = 0"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "    "
sCodigo = sCodigo & vbLf & "    PresupuestoAbiertoItem = dImporte"
sCodigo = sCodigo & vbLf & "End Function"

PresupuestoAbiertoItem = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoAbiertoItem", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Private Function PresupuestoConsumidoProceso() As String
Dim sCodigo As String
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoConsumidoProceso()"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "    'Recorremos cada grupo"
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        sCodigo = sCodigo & vbLf & "dImporte = dImporte + PresupuestoConsumidoGrupo(""" & oGrupo.Codigo & """)"
    End If
Next
sCodigo = sCodigo & vbLf & "PresupuestoConsumidoProceso = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
PresupuestoConsumidoProceso = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoConsumidoProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function PresupuestoConsumidoGrupo() As String
Dim sCodigo As String
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoConsumidoGrupo(sCodGrupo As String) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim iNumItems As Integer"
sCodigo = sCodigo & vbLf & "Dim I As Integer"

sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 11"
   
sCodigo = sCodigo & vbLf & "    Select Case sCodGrupo"
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        sCodigo = sCodigo & vbLf & "        Case """ & oGrupo.Codigo & """"
        sCodigo = sCodigo & vbLf & "            iNumItems = " & DevolverItemsConfirmados(oGrupo)
    End If
Next

sCodigo = sCodigo & vbLf & "    End Select"
    
sCodigo = sCodigo & vbLf & "    For I = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + PresupuestoConsumidoItem(sCodGrupo, iFila)"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "    Next I"

sCodigo = sCodigo & vbLf & "PresupuestoConsumidoGrupo = dImporte"

sCodigo = sCodigo & vbLf & "End Function"
PresupuestoConsumidoGrupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoConsumidoGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function PresupuestoConsumidoItem() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoConsumidoItem(sCodGrupo, iFila) As Double"
sCodigo = sCodigo & vbLf & " Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim sCeldaCant As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaPrecio As String"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim dPrecio As Double"
sCodigo = sCodigo & vbLf & "Dim dCantidad As Double"
sCodigo = sCodigo & vbLf & "Dim sLetra As String"
sCodigo = sCodigo & vbLf & "Dim iFilaProve As Integer"
sCodigo = sCodigo & vbLf & "Dim sCeldaProve As String"


sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(sCodGrupo)"
    
sCodigo = sCodigo & vbLf & "    sLetra = ""F"""
    
sCodigo = sCodigo & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 7"

sCodigo = sCodigo & vbLf & "    sCeldaProve = ""F"" & iFilaProve"
sCodigo = sCodigo & vbLf & "    sCeldaPrecio = ""C"" & iFila - 1"
    
sCodigo = sCodigo & vbLf & "    While xlsheet.Range(sCeldaProve).Value <> """""
        
sCodigo = sCodigo & vbLf & "        sCeldaCant = sLetra & iFila 'Cantidad adjudicada"
        
sCodigo = sCodigo & vbLf & "        If xlsheet.Range(sCeldaCant).Value <> """" Then"
sCodigo = sCodigo & vbLf & "            dCantidad = xlsheet.Range(sCeldaCant).Value"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            dCantidad = 0"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        If xlsheet.Range(sCeldaPrecio).Value <> """" Then"
sCodigo = sCodigo & vbLf & "            dPrecio = xlsheet.Range(sCeldaPrecio).Value"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            dPrecio = 0"
sCodigo = sCodigo & vbLf & "        End If"
        
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + dPrecio * dCantidad"
        
sCodigo = sCodigo & vbLf & "        sLetra = IncrementarColumna(sLetra, 1)"
sCodigo = sCodigo & vbLf & "        sCeldaProve = sLetra & iFilaProve"

sCodigo = sCodigo & vbLf & "    Wend"
    
sCodigo = sCodigo & vbLf & "    PresupuestoConsumidoItem = dImporte"
    
sCodigo = sCodigo & vbLf & "End Function"
PresupuestoConsumidoItem = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoConsumidoItem", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

Private Function AdjudicadoProceso() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AdjudicadoProceso() As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"

sCodigo = sCodigo & vbLf & "Set m_oGruposProve = New CGruposProve"
sCodigo = sCodigo & vbLf & "ReDim m_arrProve(0) As String"

sCodigo = sCodigo & vbLf & "dImporte = CalcularImporteAdjudicadoProceso"

sCodigo = sCodigo & vbLf & "AdjudicadoProceso = dImporte"

sCodigo = sCodigo & vbLf & "End Function"

AdjudicadoProceso = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AdjudicadoProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function AdjudicadoGrupo() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AdjudicadoGrupo(sCodGrupo As String) As Double"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim sLetra As String"
sCodigo = sCodigo & vbLf & "Dim iFilaProve As Integer"
sCodigo = sCodigo & vbLf & "Dim sCeldaProve As String"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim sCodProve As String"
sCodigo = sCodigo & vbLf & "Dim iFila As String"
sCodigo = sCodigo & vbLf & "Dim j As Integer"
sCodigo = sCodigo & vbLf & "Dim k As Integer"
sCodigo = sCodigo & vbLf & "iFila = 5 + " & basPublic.gParametrosInstalacion.giLongCabComp
sCodigo = sCodigo & vbLf & "CalcularImporteAdjudicadoGrupo(sCodGrupo)"
sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(sCodgrupo)"
sCodigo = sCodigo & vbLf & "    sLetra = ""F"""
sCodigo = sCodigo & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 7"
sCodigo = sCodigo & vbLf & "    sCeldaProve = ""F"" & iFilaProve"
sCodigo = sCodigo & vbLf & "    If xlsheet.Range(sCeldaProve).Value <> """" Then"
sCodigo = sCodigo & vbLf & "        iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 11"
sCodigo = sCodigo & vbLf & "        For k = 1 To m_oGruposProve.Count"
sCodigo = sCodigo & vbLf & "            If m_oGruposProve.Item(k).codgrupo = sCodgrupo Then"
sCodigo = sCodigo & vbLf & "                dImporte = dImporte + m_oGruposProve.Item(k).AdjudicadoNeto"
sCodigo = sCodigo & vbLf & "            End If"
sCodigo = sCodigo & vbLf & "        Next"
sCodigo = sCodigo & vbLf & "        sLetra = IncrementarColumna(sLetra, 1)"
sCodigo = sCodigo & vbLf & "        sCeldaProve = sLetra & iFilaProve"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "    AdjudicadoGrupo = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
AdjudicadoGrupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AdjudicadoGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function Comp_Item_AdjudicadoItemProve() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Function AdjudicadoItemProve(iFila As Integer) As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "Dim dPrecio As Double"
sCod = sCod & vbLf & "Dim dCantidad As Double"
sCod = sCod & vbLf & "Dim dValorAnterior As Double"
sCod = sCod & vbLf & "Dim sCodProve As String"
sCod = sCod & vbLf & "Dim i As Integer"
sCod = sCod & vbLf & "    'Guardamos el precio anterior item prove"
sCod = sCod & vbLf & "    i = iFila - (" & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14)"
sCod = sCod & vbLf & "    g_var_ValorAnteriorItemProveConAtrib(i, 0) = CDbl(Sheets.Item(4).Range(""F"" & iFila).Text)"
sCod = sCod & vbLf & "    If IsEmpty(g_var_ValorAnteriorItemProveConAtrib(i, 1)) Then g_var_ValorAnteriorItemProveConAtrib(i, 1) = CDbl(Sheets.Item(4).Range(""BC"" & iFila).Value)"
sCod = sCod & vbLf & "    If IsEmpty(g_var_ValorAnteriorItemProveConAtrib(i, 2)) Then g_var_ValorAnteriorItemProveConAtrib(i, 2) = CDbl(Sheets.Item(4).Range(""BD"" & iFila).Value)"
sCod = sCod & vbLf & "    If Sheets.Item(4).Range(""C"" & iFila).Value <> """" Then"
sCod = sCod & vbLf & "        dPrecio = Sheets.Item(4).Range(""C"" & iFila).Value"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        dPrecio = 0"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "    If Sheets.Item(4).Range(""D"" & iFila).Value <> """" Then"
sCod = sCod & vbLf & "        dCantidad = Sheets.Item(4).Range(""D"" & iFila).Value"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        dCantidad = 0"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "    dImporte = dPrecio * dCantidad"
sCod = sCod & vbLf & "    sCodProve = ObtenerProveedorItem(iFila)"
sCod = sCod & vbLf & "    dImporte = AplicarAtributosItem_Proveedor(dImporte, sCodProve, iFila)"
sCod = sCod & vbLf & "    AdjudicadoItemProve = dImporte"
sCod = sCod & vbLf & "End Function"
Comp_Item_AdjudicadoItemProve = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_AdjudicadoItemProve", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function Comp_Item_ConsumidoItemProve() As String
Dim sCod As String
Dim iIni As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
iIni = basPublic.gParametrosInstalacion.giLongCabCompItem
If iIni <= 0 Then iIni = 1
    
sCod = "Function ConsumidoItemProve(iFila As Integer) As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "Dim dPrecio As Double"
sCod = sCod & vbLf & "Dim dCantidad As Double"
sCod = sCod & vbLf & "Dim sCodProve As String"
sCod = sCod & vbLf & "If Sheets.Item(4).Range(""C""& " & iIni & " + 6).Value <> """" Then"
sCod = sCod & vbLf & "        dPrecio = Sheets.Item(4).Range(""C"" & " & iIni & " + 6).Value / Sheets.Item(4).Range(""B"" & " & iIni & " + 6).Value"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        dPrecio = 0"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "    If Sheets.Item(4).Range(""D"" & iFila).Value <> """" Then"
sCod = sCod & vbLf & "        dCantidad = Sheets.Item(4).Range(""D"" & iFila).Value"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        dCantidad = 0"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "    dImporte = dPrecio * dCantidad"
sCod = sCod & vbLf & "    ConsumidoItemProve = dImporte"
sCod = sCod & vbLf & "End Function"
Comp_Item_ConsumidoItemProve = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_ConsumidoItemProve", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function AdjudicadoItem() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AdjudicadoItem(sCodGrupo,iFila, Optional sLetra= """") As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "    dImporte = CalcularImporteAdjudicadoItem(sCodGrupo, iFila, sLetra)"
sCodigo = sCodigo & vbLf & "    AdjudicadoItem = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
AdjudicadoItem = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AdjudicadoItem", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function CalcularImporteAdjudicadoProceso() As String
Dim sCodigo As String
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function CalcularImporteAdjudicadoProceso()"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteConAtrib As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteConAtribAux As Double"
sCodigo = sCodigo & vbLf & "Dim j As Integer"
sCodigo = sCodigo & vbLf & "Dim k As Integer"
sCodigo = sCodigo & vbLf & "ReDim m_arrProve(0) As String"

For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        sCodigo = sCodigo & vbLf & "dImporte = dImporte + AdjudicadoGrupo(""" & oGrupo.Codigo & """)"
    End If
Next

sCodigo = sCodigo & vbLf & "For j = 0 To UBound(m_arrProve)"
sCodigo = sCodigo & vbLf & "    dImporteConAtribAux = 0"
sCodigo = sCodigo & vbLf & "    For k = 1 To m_oGruposProve.Count"
sCodigo = sCodigo & vbLf & "        If m_oGruposProve.Item(k).Prove = m_arrProve(j) Then"
sCodigo = sCodigo & vbLf & "            dImporteConAtribAux = dImporteConAtribAux + m_oGruposProve.Item(k).AdjudicadoNeto"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "    Next"
sCodigo = sCodigo & vbLf & "    dImporteConAtribAux = AplicarAtributosProceso_Proveedor(dImporteConAtribAux, m_arrProve(j))"
sCodigo = sCodigo & vbLf & "    dImporteConAtrib = dImporteConAtrib + dImporteConAtribAux"
sCodigo = sCodigo & vbLf & "Next"
sCodigo = sCodigo & vbLf & "    CalcularImporteAdjudicadoProceso = dImporteConAtrib"
sCodigo = sCodigo & vbLf & "End Function"

CalcularImporteAdjudicadoProceso = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "CalcularImporteAdjudicadoProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function CalcularImporteAdjudicadoGrupo() As String
Dim sCodigo As String
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function CalcularImporteAdjudicadoGrupo(sCodGrupo As String)"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim sLetra As String"
sCodigo = sCodigo & vbLf & "Dim iFilaProve As Integer"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteAux As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteBruto As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim iNumItems As Integer"
sCodigo = sCodigo & vbLf & "Dim i As Integer"
sCodigo = sCodigo & vbLf & "Dim sCodProve As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaProve As String"
sCodigo = sCodigo & vbLf & "Dim bAplicar As Boolean"
sCodigo = sCodigo & vbLf & "Dim bProveEsta As Boolean"
sCodigo = sCodigo & vbLf & "    Select Case sCodGrupo"
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        sCodigo = sCodigo & vbLf & "        Case """ & oGrupo.Codigo & """"
        sCodigo = sCodigo & vbLf & "        iNumItems = " & DevolverItemsConfirmados(oGrupo)
    End If
Next
sCodigo = sCodigo & vbLf & "    End Select"
sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(sCodgrupo)"
sCodigo = sCodigo & vbLf & "    sLetra = ""F"""
sCodigo = sCodigo & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 7"
sCodigo = sCodigo & vbLf & "    sCeldaProve = ""F"" & iFilaProve"
sCodigo = sCodigo & vbLf & "    While xlsheet.Range(sCeldaProve).Value <> """""
sCodigo = sCodigo & vbLf & "        iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 11"
sCodigo = sCodigo & vbLf & "        dImporte = 0"
sCodigo = sCodigo & vbLf & "        dImporteAux = 0"
sCodigo = sCodigo & vbLf & "        bAplicar = False"
sCodigo = sCodigo & vbLf & "        For i = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "            If xlsheet.Range(sLetra & iFila).Value <> """" Then"
sCodigo = sCodigo & vbLf & "                dImporteAux = dImporteAux + AdjudicadoItem(sCodgrupo, iFila, sLetra)"
sCodigo = sCodigo & vbLf & "                bAplicar = True"
sCodigo = sCodigo & vbLf & "            End If"
sCodigo = sCodigo & vbLf & "            iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "        Next i"
sCodigo = sCodigo & vbLf & "        sCodProve = ObtenerProveedorItem(sCodgrupo, iFila, sLetra)"
sCodigo = sCodigo & vbLf & "        dImporteBruto = dImporteAux"
sCodigo = sCodigo & vbLf & "        If bAplicar Then"
sCodigo = sCodigo & vbLf & "            dImporte = dImporte + AplicarAtributosGrupo_Proveedor(sCodgrupo, dImporteAux, sCodProve, iFila)"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        bProveEsta = False"
sCodigo = sCodigo & vbLf & "        For i = 0 To UBound(m_arrProve)"
sCodigo = sCodigo & vbLf & "            If m_arrProve(i) = sCodProve Then"
sCodigo = sCodigo & vbLf & "                bProveEsta = True"
sCodigo = sCodigo & vbLf & "                Exit For"
sCodigo = sCodigo & vbLf & "            End If"
sCodigo = sCodigo & vbLf & "        Next"
sCodigo = sCodigo & vbLf & "        If Not bProveEsta And bAplicar Then"
sCodigo = sCodigo & vbLf & "            m_arrProve(UBound(m_arrProve)) = sCodProve"
sCodigo = sCodigo & vbLf & "            ReDim Preserve m_arrProve(UBound(m_arrProve) + 1) As String"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        If bAplicar Then"
sCodigo = sCodigo & vbLf & "            'Guardamos en la coleccion grupoprove el adjudicado bruto y neto."
sCodigo = sCodigo & vbLf & "            If m_oGruposProve.Item(CStr(sCodgrupo & ""###"" & sCodProve)) Is Nothing Then"
sCodigo = sCodigo & vbLf & "                m_oGruposProve.Add sCodgrupo, sCodProve, , dImporteBruto, dImporte"
sCodigo = sCodigo & vbLf & "            Else"
sCodigo = sCodigo & vbLf & "                m_oGruposProve.Item(CStr(sCodgrupo & ""###"" & sCodProve)).AdjudicadoBruto = dImporteBruto"
sCodigo = sCodigo & vbLf & "                m_oGruposProve.Item(CStr(sCodgrupo & ""###"" & sCodProve)).AdjudicadoNeto = dImporte"
sCodigo = sCodigo & vbLf & "            End If"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        sLetra = IncrementarColumna(sLetra, 1)"
sCodigo = sCodigo & vbLf & "        sCeldaProve = sLetra & iFilaProve"
sCodigo = sCodigo & vbLf & "    Wend"
sCodigo = sCodigo & vbLf & "End Function"

CalcularImporteAdjudicadoGrupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "CalcularImporteAdjudicadoGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function
Private Function CalcularImporteAdjudicadoItem() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function CalcularImporteAdjudicadoItem(sCodGrupo, iFila, Optional sLetraParam = """") As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteAux As Double"
sCodigo = sCodigo & vbLf & "Dim sCeldaCant As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaPrecio As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaProve As String"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim dPrecio As Double"
sCodigo = sCodigo & vbLf & "Dim dCantidad As Double"
sCodigo = sCodigo & vbLf & "Dim sLetra As String"
sCodigo = sCodigo & vbLf & "Dim iFilaProve As Integer"
sCodigo = sCodigo & vbLf & "Dim sCodProve As String"
sCodigo = sCodigo & vbLf & "Dim bSalir As Boolean"
sCodigo = sCodigo & vbLf & "    bSalir = False"
sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(sCodGrupo)"
sCodigo = sCodigo & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 7"
sCodigo = sCodigo & vbLf & "    sLetra = sLetraParam"
sCodigo = sCodigo & vbLf & "    If sLetraParam = """" Then"
sCodigo = sCodigo & vbLf & "        sLetra = ""F"""
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "    sCeldaProve = sLetra & iFilaProve"
sCodigo = sCodigo & vbLf & "    While xlsheet.Range(sCeldaProve).Value <> """" And Not bSalir"
sCodigo = sCodigo & vbLf & "        dImporteAux = 0"
sCodigo = sCodigo & vbLf & "        sCodProve = ObtenerProveedorItem(sCodgrupo, iFila, sLetra)"
sCodigo = sCodigo & vbLf & "        sCeldaCant = sLetra & iFila 'Cantidad adjudicada"
sCodigo = sCodigo & vbLf & "        sCeldaPrecio = sLetra & iFila - 1"
sCodigo = sCodigo & vbLf & "        If xlsheet.Range(sCeldaCant).Value <> """" Then"
sCodigo = sCodigo & vbLf & "            dCantidad = xlsheet.Range(sCeldaCant).Value"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            dCantidad = 0"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        If xlsheet.Range(sCeldaPrecio).Value <> """" Then"
sCodigo = sCodigo & vbLf & "            dPrecio = xlsheet.Range(sCeldaPrecio).Value"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            dPrecio = 0"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        dImporteAux = dPrecio * dCantidad"
sCodigo = sCodigo & vbLf & "        If dCantidad <> 0 Then"
sCodigo = sCodigo & vbLf & "            dImporteAux = AplicarAtributosItem_Proveedor(sCodgrupo, Sheets.Item(sCodgrupo).Range(""DA"" & iFila).Value, dImporteAux, sCodProve, iFila)"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        If sLetraParam = """" Then"
sCodigo = sCodigo & vbLf & "            dImporte = dImporte + dImporteAux"
sCodigo = sCodigo & vbLf & "            sLetra = IncrementarColumna(sLetra, 1)"
sCodigo = sCodigo & vbLf & "            sCeldaProve = sLetra & iFilaProve"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            dImporte = dImporteAux"
sCodigo = sCodigo & vbLf & "            bSalir = True"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "    Wend"
sCodigo = sCodigo & vbLf & "    CalcularImporteAdjudicadoItem = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
CalcularImporteAdjudicadoItem = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "CalcularImporteAdjudicadoItem", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


Private Function AplicarAtributosItem(Optional ByVal xlApp As Object) As String
Dim sCodigo As String
Dim i As Integer
Dim arAtribs As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arAtribs = g_oOrigen.FormulaDeAtributosDeItemProveedor
    
    sCodigo = "Function AplicarAtributosItem_Proveedor(sCodGrupo,idItem, dImporte,sCodProve,iFila) As Double"
    sCodigo = sCodigo & vbLf & "Dim dValorAtrib As Variant"
    sCodigo = sCodigo & vbLf & "Dim sTipoOperacion As String"
    sCodigo = sCodigo & vbLf & "Dim sComent As String"
    sCodigo = sCodigo & vbLf & "Dim sComentario As String"
    sCodigo = sCodigo & vbLf & ""
    
    If IsArray(arAtribs) Then
        For i = 0 To UBound(arAtribs, 2)
            sCodigo = sCodigo & vbLf & "'" & arAtribs(1, i)
            sCodigo = sCodigo & vbLf & "dValorAtrib = ObtenerValorAtrib(""" & arAtribs(1, i) & """, sCodProve, sTipoOperacion, sComentario, " & arAtribs(3, i) & ", sCodGrupo, IdItem)"
            sCodigo = sCodigo & vbLf & "dImporte = AplicarValorAtrib( dValorAtrib, dImporte, sTipoOperacion)"
            sCodigo = sCodigo & vbLf & "If sComentario <> """" then"
            sCodigo = sCodigo & vbLf & "    sComent = sComent & """ & arAtribs(1, i) & ": Importe "" & sTipoOperacion & "" "" & CStr(CDbl(dValorAtrib)) & vbLf"
            sCodigo = sCodigo & vbLf & "End If "
        Next
    End If
    sCodigo = sCodigo & vbLf & "If sComent <> """" then"
    sCodigo = sCodigo & vbLf & "    If Sheets.Item(sCodGrupo).Range(""E"" & iFila - 1).Comment Is Nothing Then"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(sCodGrupo).Range(""E"" & iFila - 1).AddComment sComent"
    sCodigo = sCodigo & vbLf & "    Else"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(sCodGrupo).Range(""E"" & iFila - 1).Comment.Text sComent"
    sCodigo = sCodigo & vbLf & "    End If"
    sCodigo = sCodigo & vbLf & "End If"
    sCodigo = sCodigo & vbLf & "AplicarAtributosItem_Proveedor= dImporte"
    sCodigo = sCodigo & vbLf & "End Function"
    AplicarAtributosItem = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AplicarAtributosItem", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function AplicarAtributosGrupo() As String
Dim sCodigo As String
Dim i As Integer
Dim arAtribs As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arAtribs = g_oOrigen.FormulaDeAtributosDeGrupoProveedor
    
    sCodigo = "Function AplicarAtributosGrupo_Proveedor(sCodGrupo, dImporte,sCodProve,iFila) As Double"
    sCodigo = sCodigo & vbLf & "Dim dValorAtrib As Variant"
    sCodigo = sCodigo & vbLf & "Dim sTipoOperacion As String"
    sCodigo = sCodigo & vbLf & "Dim sComent As String"
    sCodigo = sCodigo & vbLf & "Dim sComentario As String"
    sCodigo = sCodigo & vbLf & ""
    If IsArray(arAtribs) Then
        For i = 0 To UBound(arAtribs, 2)
            sCodigo = sCodigo & vbLf & "'" & arAtribs(1, i)
            sCodigo = sCodigo & vbLf & "dValorAtrib = ObtenerValorAtrib(""" & arAtribs(1, i) & """, sCodProve, sTipoOperacion, sComentario," & arAtribs(3, i) & ", sCodGrupo)"
            sCodigo = sCodigo & vbLf & "dImporte = AplicarValorAtrib(dValorAtrib, dImporte, sTipoOperacion)"
            sCodigo = sCodigo & vbLf & "If sComentario <> """" then"
            sCodigo = sCodigo & vbLf & "    sComent = sComent & """ & arAtribs(1, i) & ": Importe "" & sTipoOperacion & "" "" & CStr(CDbl(dValorAtrib)) & vbLf"
            sCodigo = sCodigo & vbLf & "End If "
        Next
    End If
    sCodigo = sCodigo & vbLf & "If sComent <> """" then"
    sCodigo = sCodigo & vbLf & "    If Sheets.Item(sCodGrupo).Range(""D"" & iFila ).Comment Is Nothing Then"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(sCodGrupo).Range(""D"" & iFila ).AddComment sComent"
    sCodigo = sCodigo & vbLf & "    Else"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(sCodGrupo).Range(""D"" & iFila ).Comment.Text sComent"
    sCodigo = sCodigo & vbLf & "    End If"
    sCodigo = sCodigo & vbLf & "End If"
    sCodigo = sCodigo & vbLf & "AplicarAtributosGrupo_Proveedor= dImporte"
    sCodigo = sCodigo & vbLf & "End Function"
    
    AplicarAtributosGrupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AplicarAtributosGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function AplicarAtributosProceso() As String
Dim sCodigo As String
Dim i As Integer
Dim arAtribs As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arAtribs = g_oOrigen.FormulaDeAtributosDeProcesoProveedor
    
    sCodigo = "Function AplicarAtributosProceso_Proveedor(dImporte,sCodProve) As Double"
    sCodigo = sCodigo & vbLf & "Dim dValorAtrib As Variant"
    sCodigo = sCodigo & vbLf & "Dim sTipoOperacion As String"
    sCodigo = sCodigo & vbLf & "Dim sComent As String"
    sCodigo = sCodigo & vbLf & "Dim sComentario As String"
    sCodigo = sCodigo & vbLf & ""
    If IsArray(arAtribs) Then
        For i = 0 To UBound(arAtribs, 2)
            sCodigo = sCodigo & vbLf & "'" & arAtribs(1, i)
            sCodigo = sCodigo & vbLf & "dValorAtrib = ObtenerValorAtrib(""" & arAtribs(1, i) & """, sCodProve, sTipoOperacion, sComentario," & arAtribs(3, i) & ")"
            sCodigo = sCodigo & vbLf & "dImporte = AplicarValorAtrib(dValorAtrib, dImporte, sTipoOperacion)"
            sCodigo = sCodigo & vbLf & "If sComentario <> """" then"
            sCodigo = sCodigo & vbLf & "    sComent = sComent & """ & arAtribs(1, i) & ": Importe "" & sTipoOperacion & "" "" & CStr(CDbl(dValorAtrib)) & vbLf"
            sCodigo = sCodigo & vbLf & "End If "
        Next
    End If
    sCodigo = sCodigo & vbLf & "If sComent <> """" then"
    sCodigo = sCodigo & vbLf & "            If Sheets.Item(""" & sIdiGeneral & """).Range(""D"" & " & basPublic.gParametrosInstalacion.giLongCabComp & " + 4).Comment Is Nothing Then"
    sCodigo = sCodigo & vbLf & "                Sheets.Item(""" & sIdiGeneral & """).Range(""D"" & " & basPublic.gParametrosInstalacion.giLongCabComp & " + 4).AddComment sComent"
    sCodigo = sCodigo & vbLf & "            Else"
    sCodigo = sCodigo & vbLf & "                Sheets.Item(""" & sIdiGeneral & """).Range(""D"" & " & basPublic.gParametrosInstalacion.giLongCabComp & " + 4).Comment.Text sComent"
    sCodigo = sCodigo & vbLf & "            End If"
    sCodigo = sCodigo & vbLf & "End If"
    sCodigo = sCodigo & vbLf & "AplicarAtributosProceso_Proveedor= dImporte"
    sCodigo = sCodigo & vbLf & "End Function"
    
    AplicarAtributosProceso = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AplicarAtributosProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


Private Function TodoElGrupoAQueProveedor() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function TodoElGrupoAQueProveedor(sCodGrupo As String) As String"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim sProve As String"
sCodigo = sCodigo & vbLf & "Dim bCondicion As Boolean"
sCodigo = sCodigo & vbLf & "Dim sPrimerProve As String"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim iFilaAux As Integer"
sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(sCodGrupo)"
sCodigo = sCodigo & vbLf & "    iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 11"
sCodigo = sCodigo & vbLf & "    iFilaAux = iFila - 1"
sCodigo = sCodigo & vbLf & "    bCondicion = True"
sCodigo = sCodigo & vbLf & "    While xlsheet.Cells(iFilaAux, 3).Value <> """" And bCondicion"
sCodigo = sCodigo & vbLf & "        sProve = TodoElItemAQueProveedor(sCodGrupo, iFila)"
sCodigo = sCodigo & vbLf & "       If sProve = ""-*@@@@-*"" Then"
sCodigo = sCodigo & vbLf & "            bCondicion = False"
sCodigo = sCodigo & vbLf & "       End If"
sCodigo = sCodigo & vbLf & "        If sPrimerProve = """" Then"
sCodigo = sCodigo & vbLf & "            sPrimerProve = sProve"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        If sPrimerProve <> """" And sPrimerProve <> sProve and sProve <> """" Then"
sCodigo = sCodigo & vbLf & "            bCondicion = False"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        iFilaAux = iFilaAux + 3"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "    Wend"
sCodigo = sCodigo & vbLf & "    If Not bCondicion Then"
sCodigo = sCodigo & vbLf & "        TodoElGrupoAQueProveedor = ""-*@@@@-*"""
sCodigo = sCodigo & vbLf & "    Else"
sCodigo = sCodigo & vbLf & "        If sPrimerProve <> """" Then"
sCodigo = sCodigo & vbLf & "            TodoElGrupoAQueProveedor = sPrimerProve"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            TodoElGrupoAQueProveedor = """""
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "End Function"

TodoElGrupoAQueProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "TodoElGrupoAQueProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function TodoElItemAQueProveedor() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
  
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function TodoElItemAQueProveedor(sCodGrupo, iFila) As String"
sCodigo = sCodigo & vbLf & "Dim sCodProve As String"
sCodigo = sCodigo & vbLf & "Dim bCondicion As Boolean"
sCodigo = sCodigo & vbLf & "Dim sCeldaProve As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaPrimerProve As String"
sCodigo = sCodigo & vbLf & "Dim sLetra As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaCant As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaPrecio As String"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim iFilaProve As Integer"
sCodigo = sCodigo & vbLf & "Dim vAux As Variant"
sCodigo = sCodigo & vbLf & "Dim sAux As String"
sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(sCodGrupo)"
sCodigo = sCodigo & vbLf & "    sLetra = ""F"""
sCodigo = sCodigo & vbLf & "    sCeldaPrimerProve = """""
sCodigo = sCodigo & vbLf & "    iFilaProve =" & basPublic.gParametrosInstalacion.giLongCabComp & "+ 7"
sCodigo = sCodigo & vbLf & "    sCeldaProve = ""F"" & iFilaProve"
sCodigo = sCodigo & vbLf & "    bCondicion = True"
sCodigo = sCodigo & vbLf & "    While xlsheet.Range(sCeldaProve).Value <> """" And bCondicion"
sCodigo = sCodigo & vbLf & "        sCeldaCant = sLetra & iFila 'Cantidad adjudicada"
sCodigo = sCodigo & vbLf & "        sCeldaPrecio = sLetra & iFila - 1"
sCodigo = sCodigo & vbLf & "        If xlsheet.Range(sCeldaCant).Value <> """" Then"
sCodigo = sCodigo & vbLf & "            If sCeldaPrimerProve <> """" And sCeldaPrimerProve <> sCeldaProve and sCeldaProve <> """" Then"
sCodigo = sCodigo & vbLf & "                bCondicion = False"
sCodigo = sCodigo & vbLf & "            End If"
sCodigo = sCodigo & vbLf & "            sCeldaPrimerProve = sCeldaProve"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        sLetra = IncrementarColumna(sLetra, 1)"
sCodigo = sCodigo & vbLf & "        sCeldaProve = sLetra & iFilaProve"
sCodigo = sCodigo & vbLf & "    Wend"
sCodigo = sCodigo & vbLf & "    If Not bCondicion Then"
sCodigo = sCodigo & vbLf & "        TodoElItemAQueProveedor = ""-*@@@@-*"""
sCodigo = sCodigo & vbLf & "    Else"
sCodigo = sCodigo & vbLf & "        If sCeldaPrimerProve <> """" Then"
sCodigo = sCodigo & vbLf & "            vAux = Split97(xlsheet.Range(sCeldaPrimerProve).Value,"" - "")"
sCodigo = sCodigo & vbLf & "            sAux = vAux(0)"
sCodigo = sCodigo & vbLf & "            TodoElItemAQueProveedor = sAux"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            TodoElItemAQueProveedor = """""
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "End Function"

TodoElItemAQueProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "TodoElItemAQueProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function ComparativaDeGrupo(xlSheet As Object, iIni As Integer)
Dim sCeldaMax As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmESPERA.lblDetalle.caption = "Grupo:" & xlSheet.Name
    
    '1�) Mostrar datos de adjudicaciones
    frmESPERA.ProgressBar2.Value = 0
    frmESPERA.lblContacto.caption = sIdiTransDatAdj
    sCeldaMax = MostrarDatosDeAdjudicaciones(xlSheet, iIni)
    
    '2�) Mostrar datos de ahorros generales de grupo y de proceso
    frmESPERA.ProgressBar2.Value = 25
    frmESPERA.lblContacto.caption = sIdiTransDatosGen
    MostrarCabeceraDeGrupo xlSheet, iIni
    
    frmESPERA.ProgressBar2.Value = 50
    frmESPERA.lblContacto.caption = sIdiTransDatProve
    
    '3�) Mostrar cabeceras con proveedores
    MostrarCabeceraDeProveedoresDeGrupo xlSheet, iIni
    
    frmESPERA.ProgressBar2.Value = 75
    frmESPERA.lblContacto.caption = sIdiCalcTotales
    
    '4) MostrarTotalesGrupo
    MostrarTotalesGrupo xlSheet, iIni
    frmESPERA.ProgressBar2.Value = 100
    frmESPERA.lblContacto.caption = ""
    
    xlSheet.PageSetup.PrintArea = "A1:" & sCeldaMax
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ComparativaDeGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function MostrarCabeceraDeGrupo(xlSheet As Object, iIni As Integer)
Dim sCelda As String
Dim sCeldaPresAbierto As String
Dim sCeldaPresConsumido As String
Dim sCeldaAdjudicado As String
Dim sCeldaAhorrado As String
Dim sCeldaAhorradoPorcen As String
Dim orange As Object
Dim sCeldaPresAbiertoGrupo As String
Dim sCeldaPresConsumidoGrupo As String
Dim sCeldaAdjudicadoGrupo As String
Dim sCeldaAhorradoGrupo As String
Dim sCeldaAhorradoPorcenGrupo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With g_oOrigen.m_oProcesoSeleccionado
        
            sCelda = "A" & iIni
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiProceso & " " & CStr(.Anyo) & "/" & .GMN1Cod & "/" & CStr(.Cod) & " " & .Den
    
            sCelda = "D" & iIni + 1
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = sIdiMoneda2
            sCelda = "E" & iIni + 1
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda) = .MonCod
            sCelda = "F" & iIni + 1
            
            If .PermitirAdjDirecta Then
                xlSheet.Range(sCelda).NumberFormat = "@"
                xlSheet.Range(sCelda) = sIdiAdjDir
                sCelda = "G" & iIni + 1
                xlSheet.Range(sCelda).NumberFormat = "dd/mm/yyyy"
                xlSheet.Range(sCelda) = .FechaPresentacion
            Else
                xlSheet.Range(sCelda).NumberFormat = "@"
                xlSheet.Range(sCelda) = sIdiAdjReu
    
                sCelda = "G" & iIni + 1
                xlSheet.Range(sCelda).NumberFormat = "dd/mm/yyyy"
                If .FechaProximaReunion = "" Then
                    xlSheet.Range(sCelda) = .FechaPresentacion
                Else
                    xlSheet.Range(sCelda) = .FechaProximaReunion
                End If
            End If
            
        End With
            
        Set orange = xlSheet.Range("A" & iIni & ":" & "G" & iIni + 1)
        orange.Font.Bold = True
        orange.borderaround , 2
        orange.Interior.Color = RGB(192, 192, 192)
         
        'Pasamos los datos de presupuestos
    
        sCelda = "A" & 3 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiResultados
        
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
        xlSheet.Range(sCelda).borderaround , 2
        
        sCelda = "A" & 4 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiProceso
        
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
        xlSheet.Range(sCelda).borderaround , 2
        xlSheet.Range(sCelda).Font.Bold = True
        
        sCelda = "A" & 5 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiGrupo
        
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
        xlSheet.Range(sCelda).borderaround , 2
        xlSheet.Range(sCelda).Font.Bold = True
   
        
        sCelda = "B" & 3 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiPresAbierto
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        xlSheet.Range(sCelda).borderaround , 2
       
        sCeldaPresAbierto = "B" & 4 + iIni
        xlSheet.Range(sCeldaPresAbierto).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaPresAbierto).borderaround , 2
        xlSheet.Range(sCeldaPresAbierto).Interior.Color = RGB(245, 245, 200)
        
        sCeldaPresAbiertoGrupo = "B" & 5 + iIni
        xlSheet.Range(sCeldaPresAbiertoGrupo).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaPresAbiertoGrupo).borderaround , 2
        xlSheet.Range(sCeldaPresAbiertoGrupo).Interior.Color = RGB(245, 245, 200)
        
        sCelda = "C" & 3 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiPresConsumido
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        xlSheet.Range(sCelda).borderaround , 2
        
        sCeldaPresConsumido = "C" & 4 + iIni
        xlSheet.Range(sCeldaPresConsumido).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaPresConsumido).borderaround , 2
        xlSheet.Range(sCeldaPresConsumido).Interior.Color = RGB(245, 245, 200)
        
        sCeldaPresConsumidoGrupo = "C" & 5 + iIni
        xlSheet.Range(sCeldaPresConsumidoGrupo).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaPresConsumidoGrupo).borderaround , 2
        xlSheet.Range(sCeldaPresConsumidoGrupo).Interior.Color = RGB(245, 245, 200)
        
        sCelda = "D" & 3 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiAdjudicado
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        xlSheet.Range(sCelda).borderaround , 2
            
        sCeldaAdjudicado = "D" & 4 + iIni
        xlSheet.Range(sCeldaAdjudicado).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaAdjudicado).borderaround , 2
        xlSheet.Range(sCeldaAdjudicado).Interior.Color = RGB(245, 245, 200)
        
        sCeldaAdjudicadoGrupo = "D" & 5 + iIni
        xlSheet.Range(sCeldaAdjudicadoGrupo).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaAdjudicadoGrupo).borderaround , 2
        xlSheet.Range(sCeldaAdjudicadoGrupo).Interior.Color = RGB(245, 245, 200)
        
        sCelda = "E" & 3 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiAhorrado
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        xlSheet.Range(sCelda).borderaround , 2
        
        sCeldaAhorrado = "E" & 4 + iIni
        xlSheet.Range(sCeldaAhorrado).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaAhorrado).borderaround , 2
        
        sCeldaAhorradoGrupo = "E" & 5 + iIni
        xlSheet.Range(sCeldaAhorradoGrupo).NumberFormat = m_sFormatoNumberGr
        xlSheet.Range(sCeldaAhorradoGrupo).borderaround , 2
        
        sCelda = "F" & 3 + iIni
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = sIdiPorcentAhor
        xlSheet.Range(sCelda).ColumnWidth = 12
        xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        xlSheet.Range(sCelda).borderaround , 2
        
        sCeldaAhorradoPorcen = "F" & 4 + iIni
        xlSheet.Range(sCeldaAhorradoPorcen).NumberFormat = m_sFormatoNumberPorcenGr
        xlSheet.Range(sCeldaAhorradoPorcen).borderaround , 2
       
        sCeldaAhorradoPorcenGrupo = "F" & 5 + iIni
        xlSheet.Range(sCeldaAhorradoPorcenGrupo).NumberFormat = m_sFormatoNumberPorcenGr
        xlSheet.Range(sCeldaAhorradoPorcenGrupo).borderaround , 2
    
        'Datos generales del proceso, ahorro,abierto,consumido etc..
        'Los datos de proceso
        xlSheet.Range("B" & 4 + iIni) = "= " & sIdiGeneral & "!B" & basPublic.gParametrosInstalacion.giLongCabComp + 4
        xlSheet.Range("C" & 4 + iIni) = "= " & sIdiGeneral & "!C" & basPublic.gParametrosInstalacion.giLongCabComp + 4
        xlSheet.Range("D" & 4 + iIni) = "= " & sIdiGeneral & "!D" & basPublic.gParametrosInstalacion.giLongCabComp + 4
        xlSheet.Range("E" & 4 + iIni) = "= (C" & 4 + iIni & "-D" & 4 + iIni & ")"
        xlSheet.Range("F" & 4 + iIni) = "= ((E" & 4 + iIni & "/" & "C" & 4 + iIni & ")*100)"
        xlSheet.Range("B" & 5 + iIni) = "= PresupuestoAbiertoGrupo(""" & xlSheet.Name & """)"
        xlSheet.Range("C" & 5 + iIni) = "= PresupuestoConsumidoGrupo(""" & xlSheet.Name & """)"
        xlSheet.Range("D" & 5 + iIni) = "= AdjudicadoGrupo(""" & xlSheet.Name & """)"
        xlSheet.Range("E" & 5 + iIni) = "= (C" & 5 + iIni & "-D" & 5 + iIni & ")"
        xlSheet.Range("F" & 5 + iIni) = "= ((E" & 5 + iIni & "/" & "C" & 5 + iIni & ")*100)"
    
    'Ponemos los formatos de color condicionales
    With xlSheet.Range("E" & 4 + iIni).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
    End With
    With xlSheet.Range("E" & 4 + iIni).formatconditions.Add(1, 6, "0")
        .Interior.Color = RGB(253, 100, 72)
    End With
    With xlSheet.Range("F" & 4 + iIni).formatconditions.Add(1, 5, "0")
        .Interior.Color = RGB(162, 250, 158)
    End With
    With xlSheet.Range("F" & 4 + iIni).formatconditions.Add(1, 6, "0")
        .Interior.Color = RGB(253, 100, 72)
    End With
    With xlSheet.Range("E" & 5 + iIni).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
    End With
    With xlSheet.Range("E" & 5 + iIni).formatconditions.Add(1, 6, "0")
        .Interior.Color = RGB(253, 100, 72)
    End With
    With xlSheet.Range("F" & 5 + iIni).formatconditions.Add(1, 5, "0")
        .Interior.Color = RGB(162, 250, 158)
    End With
    With xlSheet.Range("F" & 5 + iIni).formatconditions.Add(1, 6, "0")
        .Interior.Color = RGB(253, 100, 72)
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "MostrarCabeceraDeGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function MostrarCabeceraDeProveedoresDeGrupo(xlSheet As Object, iIni As Integer)
Dim sCelda As String
Dim oGrupo As CGrupo
Dim i As Integer
Dim oProve As CProveedor
Dim sLetra As String
Dim orange As Object
Dim sComentario As String
Dim bMostrarProve As Boolean
Dim bCargar As Boolean
Dim scodProve As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(xlSheet.Name)
    
    sCelda = "A" & 8 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = "           " & sIdiDescripcion
    xlSheet.Range(sCelda & ":" & "B" & 8 + iIni).Merge
    
    xlSheet.Range(sCelda & ":" & "B" & 8 + iIni).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda & ":" & "B" & 8 + iIni).borderaround , 2
    xlSheet.Range(sCelda & ":" & "B" & 8 + iIni).Font.Bold = True
    
    sCelda = "C" & 8 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiPresupuesto

    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "D" & 8 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiObj
        
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "E" & 8 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sidiImporteAdj
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
        
    xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "A" & 9 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiAhorImp
    xlSheet.Range(sCelda & ":" & "B" & 8 + iIni).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "B" & 9 + iIni
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiAhorroPorcen
    xlSheet.Range(sCelda & ":" & "B" & 8 + iIni).Interior.Color = RGB(245, 245, 200)
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "C" & 9 + iIni
    xlSheet.Range(sCelda).borderaround , 2
    '"Cantidad"
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiCantidad
    xlSheet.Range(sCelda).Font.Color = vbBlue
    xlSheet.Range(sCelda).Font.Bold = True
    
    sCelda = "D" & 9 + iIni
    xlSheet.Range(sCelda).Font.Color = vbBlue
'   "Adj%"
    xlSheet.Range(sCelda).NumberFormat = "@"
    xlSheet.Range(sCelda) = sIdiPorcentAdj
    xlSheet.Range(sCelda).borderaround , 2
    xlSheet.Range(sCelda).Font.Bold = True
    
    sLetra = "F"
    
    i = 2
    For Each oProve In g_oOrigen.m_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
            bMostrarProve = True
            If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                    bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                End If
            Else
                bMostrarProve = ProvVisible(oProve.Cod)
            End If
            If bMostrarProve Then
                'Prove
                sCelda = DevolverLetra(sLetra, i - 2) & 7 + iIni
                xlSheet.Range(sCelda).NumberFormat = "@"
                xlSheet.Range(sCelda) = oProve.Cod & " - " & oProve.Den
                Set orange = xlSheet.Range(sCelda)
                orange.Font.Size = 6
                orange.Font.Bold = True
                orange.WrapText = False
                orange.borderaround , 2
                orange.Interior.Color = RGB(192, 192, 192)
                    
                With oProve
                    sComentario = .Den & vbLf
                    sComentario = sComentario & sIdiDirec & " " & .Direccion
                    sComentario = sComentario & vbLf & sIdiCP & " " & .cP
                    sComentario = sComentario & vbLf & sIdiPobl & " " & .Poblacion
                    sComentario = sComentario & vbLf & sIdiProvi & " " & .CodProvi & " " & .DenProvi
                    sComentario = sComentario & vbLf & sIdiPais & " " & .CodPais
                    sComentario = sComentario & vbLf & sIdiMoneda & " " & .CodMon
                End With
                xlSheet.Range(sCelda).AddComment sComentario
                'Precio
                sCelda = DevolverLetra(sLetra, i - 2) & 8 + iIni
                xlSheet.Range(sCelda).NumberFormat = "@"
                xlSheet.Range(sCelda) = sIdiPrecio
                Set orange = xlSheet.Range(sCelda)
                orange.Font.Bold = True
                orange.Interior.Color = RGB(245, 245, 200)
                orange.borderaround , 2
                'Cantidad
                sCelda = DevolverLetra(sLetra, i - 2) & 9 + iIni
                xlSheet.Range(sCelda).NumberFormat = "@"
                xlSheet.Range(sCelda) = sIdiCantidad
                Set orange = xlSheet.Range(sCelda)
                orange.Font.Bold = True
                orange.Font.Color = vbBlue
                orange.borderaround , 2
                i = i + 1
            End If
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "MostrarCabeceraDeProveedoresDeGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Mostrar datos de adjudicaciones
''' </summary>
''' <param name="xlSheet">Sheet</param>
''' <param name="iIni">col inicial</param>
''' <returns>datos de adjudicaciones</returns>
''' <remarks>Llamada desde: ComparativaDeGrupo ; Tiempo m�ximo: 0,2</remarks>
Private Function MostrarDatosDeAdjudicaciones(xlSheet As Object, iIni As Integer) As String
Dim oItem As CItem
Dim oGrupo As CGrupo
Dim iStart As Integer
Dim sLetra As String
Dim sCelda As String
Dim orange As Object
Dim sComentario As String
Dim iFila As Integer
Dim sCeldaPresu As String
Dim sFormulaPorcen As String
Dim sFormulaAhorro As String
Dim oProve As CProveedor
Dim sCeldaPrecio As String
Dim sCeldaCant As String
Dim sCod As String
Dim dporcentaje As Double
Dim dCantidadAdj  As Double
Dim sLetraItem As String
Dim sCeldaCantItem As String
Dim sCelda2 As String
Dim bMostrarProve As Boolean
Dim iNumProvesEnHoja As Integer
Dim sDatos As String
Dim bCargar As Boolean
Dim scodProve As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm

Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(xlSheet.Name)
iNumProvesEnHoja = 0
iStart = iIni + 10

xlSheet.Columns("A:A").ColumnWidth = xlSheet.Columns("A:A").ColumnWidth * 2

For Each oItem In oGrupo.Items
    If oItem.Confirmado Then
        sLetra = "A"
        
        sCelda = sLetra & iStart
        'Descripci�n
        xlSheet.Range(sCelda).NumberFormat = "@"
        xlSheet.Range(sCelda) = oItem.ArticuloCod & " " & oItem.Descr & "  (" & Right(Year(oItem.FechaInicioSuministro), 2) & "/" & oItem.DestCod & ")"
        'Guardamos el identificador
        xlSheet.Range("DA" & iStart + 1).Value = oItem.Id
        sCelda = sLetra & iStart & ":" & "B" & iStart
        Set orange = xlSheet.Range(sCelda)
        If oItem.Cerrado Then
            orange.Interior.Color = RGB(150, 150, 150)
            orange.Font.Color = vbWhite
        Else
            orange.Interior.Color = RGB(245, 245, 200)
        End If
        orange.borderaround , 2
        
        'A�adimos los comentarios del item
        If Not oDestinos.Item(CStr(oItem.DestCod)) Is Nothing Then
            sDatos = ""
            If Not IsNull(oDestinos.Item(CStr(oItem.DestCod)).dir) Then
                sDatos = " " & oDestinos.Item(CStr(oItem.DestCod)).dir
            End If
            If Not IsNull(oDestinos.Item(CStr(oItem.DestCod)).POB) Then
                sDatos = sDatos & " " & oDestinos.Item(CStr(oItem.DestCod)).POB
            End If
            If Not IsNull(oDestinos.Item(CStr(oItem.DestCod)).cP) Then
                sDatos = sDatos & " " & oDestinos.Item(CStr(oItem.DestCod)).cP
            End If
            If Not IsNull(oDestinos.Item(CStr(oItem.DestCod)).Provi) Then
                sDatos = sDatos & " " & oDestinos.Item(CStr(oItem.DestCod)).Provi
            End If
            If Not IsNull(oDestinos.Item(CStr(oItem.DestCod)).Pais) Then
                sDatos = sDatos & " " & oDestinos.Item(CStr(oItem.DestCod)).Pais
            End If
            sComentario = sIdiDestino & " " & oItem.DestCod & " - " & oDestinos.Item(CStr(oItem.DestCod)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & sDatos
        Else
            sComentario = sIdiDestino & " " & oItem.DestCod
        End If
        
        If Not oPagos.Item(CStr(oItem.PagCod)) Is Nothing Then
            sComentario = sComentario & vbLf & sIdiPago & " " & oItem.PagCod & " - " & oPagos.Item(CStr(oItem.PagCod)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        Else
            sComentario = sComentario & vbLf & sIdiPago & " " & oItem.PagCod
        End If
        
        If Not oUnidades.Item(CStr(oItem.UniCod)) Is Nothing Then
            sComentario = sComentario & vbLf & sIdiUnidad & " " & oItem.UniCod & " - " & oUnidades.Item(CStr(oItem.UniCod)).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        Else
            sComentario = sComentario & vbLf & sIdiUnidad & " " & oItem.UniCod
        End If
        
        sComentario = sComentario & vbLf & sIdiInicio & " " & oItem.FechaInicioSuministro
        sComentario = sComentario & vbLf & sIdiFin & " " & oItem.FechaFinSuministro
        sComentario = sComentario & vbLf & sIdiProv & ": " & g_oOrigen.sdbgAdj.Columns("PROV").CellValue(CStr(iFila))
        xlSheet.Range(sLetra & iStart).AddComment sComentario
        xlSheet.Range(sLetra & iStart).comment.Shape.Width = 200
        sLetra = DevolverLetra(sLetra, 2)
        sCelda = sLetra & iStart
        
        'El presupuesto seria en la C
        'Presupuesto
        If IsNull(oItem.Precio) Then
            xlSheet.Range(sCelda) = ""
        Else
            xlSheet.Range(sCelda) = CDbl(oItem.Precio)
        End If
        
        If oItem.Cerrado Then
            xlSheet.Range(sCelda).Interior.Color = RGB(150, 150, 150)
            xlSheet.Range(sCelda).Font.Color = vbWhite
        Else
            xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        End If
        xlSheet.Range(sCelda).borderaround , 2
        xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberPrecioGr
        
        sCeldaPresu = sCelda
        
        'Objetivo en la D
        sLetra = DevolverLetra(sLetra, 1)
        sCelda = sLetra & iStart
        If IsNull(oItem.Objetivo) Then
            xlSheet.Range(sCelda) = ""
        Else
            xlSheet.Range(sCelda) = CDbl(oItem.Objetivo)
        End If
        If oItem.Cerrado Then
            xlSheet.Range(sCelda).Interior.Color = RGB(150, 150, 150)
            xlSheet.Range(sCelda).Font.Color = vbWhite
        Else
            xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        End If
        
        xlSheet.Range(sCelda).borderaround , 2
        xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberGr
        sFormulaPorcen = "= (100* ( 0"
        'El importe adj en la E
        sLetra = DevolverLetra(sLetra, 1)
        sCelda = sLetra & iStart
        xlSheet.Range(sCelda) = "= AdjudicadoItem(""" & oGrupo.Codigo & """," & iStart + 1 & ")"
        If oItem.Cerrado Then
            xlSheet.Range(sCelda).Interior.Color = RGB(150, 150, 150)
            xlSheet.Range(sCelda).Font.Color = vbWhite
        Else
            xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
        End If
        
        xlSheet.Range(sCelda).borderaround , 2
        xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberGr
           
        sFormulaAhorro = "(0"
        iNumProvesEnHoja = 0
        For Each oProve In g_oOrigen.m_oProvesAsig
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
              bMostrarProve = True
              If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                  If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                      bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                  End If
              Else
                  bMostrarProve = ProvVisible(oProve.Cod)
              End If
              If bMostrarProve Then
                  iNumProvesEnHoja = iNumProvesEnHoja + 1
                  'Recorremos los proveedores
                  sFormulaPorcen = sFormulaPorcen & " + "
                  '************Precio **************************************************
                  sLetra = DevolverLetra(sLetra, 1)
                  sCelda = sLetra & iStart
                  sCeldaPrecio = sCelda
                  If Not oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)) Is Nothing Then
                      If IsNull(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                          xlSheet.Range(sCelda) = ""
                      Else
                          If oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Cambio = 0 Then
                              xlSheet.Range(sCelda) = 0
                          Else
                              xlSheet.Range(sCelda) = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(CStr(oItem.Id)).PrecioOferta / oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Cambio
                          End If
                      End If
                  Else
                      xlSheet.Range(sCelda) = ""
                  End If
                  xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberPrecioGr
                  If oItem.Cerrado Then
                      xlSheet.Range(sCelda).Interior.Color = RGB(150, 150, 150)
                      xlSheet.Range(sCelda).Font.Color = vbWhite
                  Else
                      xlSheet.Range(sCelda).Interior.Color = RGB(245, 245, 200)
                  End If
                  
                  xlSheet.Range(sCelda).borderaround , 2
                  
              '************Cantidad***************************************************
                  sCelda = sLetra & iStart + 1
                  sCeldaCant = sCelda
                  sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                  sCod = CStr(oItem.Id) & sCod
                  'sacar la cantidad adjudicada
                  If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                      dporcentaje = oGrupo.Adjudicaciones.Item(sCod).Porcentaje
                      If dporcentaje <> 0 Then
                          dCantidadAdj = (dporcentaje / 100) * oItem.Cantidad
                      Else
                          dCantidadAdj = 0
                      End If
                  Else
                      dCantidadAdj = 0
                  End If
                  If dCantidadAdj = 0 Then
                      xlSheet.Range(sCelda) = ""
                  Else
                      xlSheet.Range(sCelda) = dCantidadAdj
                      sFormulaAhorro = sFormulaAhorro & " + (C" & iStart & "*" & sCelda & ")"
                  End If
                  xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberCantGr
                  xlSheet.Range(sCelda).Font.Color = vbBlue
                  xlSheet.Range(sCelda).borderaround , 2
                  If oItem.Cerrado Then
                      xlSheet.Range(sCelda).Interior.Color = RGB(150, 150, 150)
                      xlSheet.Range(sCelda).Font.Color = vbWhite
                  End If
              
                  '******************************************************************************
                   sFormulaPorcen = sFormulaPorcen & sCelda
            End If
          End If
        Next
         
         sFormulaAhorro = sFormulaAhorro & ")"
         sLetra = "A"
         'Ahorro importe
         sCelda = sLetra & iStart + 1
         xlSheet.Range(sCelda) = "= PresupuestoConsumidoItem(""" & oGrupo.Codigo & """," & iStart + 1 & ") - AdjudicadoItem(""" & oGrupo.Codigo & """," & iStart + 1 & ")"
         xlSheet.Range(sCelda).borderaround , 2
         xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberGr
        
         With xlSheet.Range(sCelda).formatconditions.Add(1, 5, "0")
             .Interior.Color = RGB(162, 250, 158)
         End With
         With xlSheet.Range(sCelda).formatconditions.Add(1, 6, "0")
             .Interior.Color = RGB(253, 100, 72)
         End With
         
         'Ahorro %
         sLetra = DevolverLetra(sLetra, 1)
         sCelda = sLetra & iStart + 1
         sLetraItem = DevolverLetra(sLetra, 1)
         sCeldaCantItem = sLetraItem & iStart + 1
         
         xlSheet.Range(sCelda) = "=IF(PresupuestoConsumidoItem(""" & oGrupo.Codigo & """," & iStart + 1 & ")<>0,(A" & iStart + 1 & "/PresupuestoConsumidoItem(""" & oGrupo.Codigo & """," & iStart + 1 & ")*100),0)"
         xlSheet.Range(sCelda).borderaround , 2
         xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberPorcenGr
         sCeldaCant = sCelda
         With xlSheet.Range(sCelda).formatconditions.Add(1, 5, "0")
             .Interior.Color = RGB(162, 250, 158)
         End With
         With xlSheet.Range(sCelda).formatconditions.Add(1, 6, "0")
             .Interior.Color = RGB(253, 100, 72)
         End With
         
         'Cantidad necesaria
         sLetra = DevolverLetra(sLetra, 1)
         sCelda = sLetra & iStart + 1
         
         If oItem.Cantidad = 0 Then
             xlSheet.Range(sCelda) = ""
         Else
             xlSheet.Range(sCelda) = CDbl(oItem.Cantidad)
         End If
         xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberCantGr
         xlSheet.Range(sCelda).Font.Color = vbBlue
         xlSheet.Range(sCelda).borderaround , 2
         sFormulaPorcen = sFormulaPorcen & ")/" & sCelda & ")"
                     
         'Porcen adj.
         sLetra = DevolverLetra(sLetra, 1)
         sCelda = sLetra & iStart + 1
         sCeldaCant = sCelda
         xlSheet.Range(sCelda) = sFormulaPorcen
         xlSheet.Range(sCelda).Font.Color = vbBlue
         xlSheet.Range(sCelda).borderaround , 2
         xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberPorcenGr

         iStart = iStart + 3
         sCelda = "A" & iStart - 1
         sCelda2 = DevolverLetra("A", iNumProvesEnHoja + 4) & iStart - 1
         
         xlSheet.Range(sCelda & ":" & sCelda2).Interior.Color = RGB(192, 192, 192)
         xlSheet.Range(sCelda & ":" & sCelda2).borderaround , 2
    End If
    Next
    
    If iNumProvesEnHoja < 3 Then
        MostrarDatosDeAdjudicaciones = "H" & iStart + 3
    Else
        MostrarDatosDeAdjudicaciones = DevolverLetra("A", iNumProvesEnHoja + 4) & iStart + 3
    End If
    
    Exit Function
    
ERROR_Frm:
        
        Resume Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "MostrarDatosDeAdjudicaciones", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Function
   End If
End Function

Private Function MostrarTotalesGrupo(xlSheet As Object, iIni As Integer)
Dim sCelda As String
Dim orange As Object
Dim sLetra As String
Dim oGrupo As CGrupo
Dim iCol As Integer
Dim oProve As CProveedor
Dim sCelda2 As String
Dim bMostrarProve As Boolean
Dim bCargar As Boolean
Dim scodProve As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(xlSheet.Name)
m_iNumItemsEnHoja = DevolverItemsConfirmados(oGrupo)

sLetra = "A"
sCelda = sLetra & iIni + 10 + m_iNumItemsEnHoja * 3
xlSheet.Range(sCelda).NumberFormat = "@"
xlSheet.Range(sCelda).Value = sIdiTotProve

Set orange = xlSheet.Range("A" & iIni + 10 + m_iNumItemsEnHoja * 3 & ":" & "E" & 10 + m_iNumItemsEnHoja * 3 + iIni)
orange.Interior.Color = RGB(192, 192, 192)
orange.Font.Bold = True
orange.borderaround , 2

sCelda = sLetra & iIni + 11 + m_iNumItemsEnHoja * 3
'   "Ahorro"
xlSheet.Range(sCelda).NumberFormat = "@"
xlSheet.Range(sCelda).Value = sIdiAhorro

Set orange = xlSheet.Range("A" & iIni + 11 + m_iNumItemsEnHoja * 3 & ":" & "E" & 11 + m_iNumItemsEnHoja * 3 + iIni)
orange.Interior.Color = RGB(192, 192, 192)
orange.Font.Bold = True
orange.borderaround , 2

sCelda = sLetra & iIni + 12 + m_iNumItemsEnHoja * 3
'   "Importe"
xlSheet.Range(sCelda).NumberFormat = "@"
xlSheet.Range(sCelda).Value = sIdiAdjudicado

xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)

Set orange = xlSheet.Range("A" & iIni + 12 + m_iNumItemsEnHoja * 3 & ":" & "E" & 12 + m_iNumItemsEnHoja * 3 + iIni)
orange.Interior.Color = RGB(192, 192, 192)
orange.Font.Bold = True
orange.borderaround , 2

sCelda = sLetra & iIni + 13 + m_iNumItemsEnHoja * 3
xlSheet.Range(sCelda).NumberFormat = "@"
xlSheet.Range(sCelda).Value = sidiconsumido
xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
Set orange = xlSheet.Range("A" & iIni + 13 + m_iNumItemsEnHoja * 3 & ":" & "E" & 13 + m_iNumItemsEnHoja * 3 + iIni)
orange.Interior.Color = RGB(192, 192, 192)
orange.Font.Bold = True
orange.borderaround , 2

'a�ade la columna de Total objetivos
sLetra = "D"
sCelda = sLetra & iIni + 10 + m_iNumItemsEnHoja * 3
xlSheet.Range(sCelda).NumberFormat = "@"
xlSheet.Range(sCelda).Value = sIdiTotObj
Set orange = xlSheet.Range("D" & iIni + 10 + m_iNumItemsEnHoja * 3 & ":" & "D" & 10 + m_iNumItemsEnHoja * 3 + iIni)
orange.Interior.Color = RGB(192, 192, 192)
orange.Font.Bold = True
orange.borderaround , 2

sCelda = sLetra & iIni + 11 + m_iNumItemsEnHoja * 3
xlSheet.Range(sCelda).Value = "=AhorroTotalSegunObjetivo(""" & sLetra & """,""" & xlSheet.Name & """," & m_iNumItemsEnHoja & ")"
xlSheet.Range(sCelda).borderaround , 2
xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberGr

Set orange = xlSheet.Range("E" & iIni + 12 + m_iNumItemsEnHoja * 3 & ":" & "E" & 12 + m_iNumItemsEnHoja * 3 + iIni)
With orange.formatconditions.Add(1, 5, "0")
    .Interior.Color = RGB(162, 250, 158)
End With
With orange.formatconditions.Add(1, 6, "0")
    .Interior.Color = RGB(253, 100, 72)
End With
orange.Font.Bold = False
orange.borderaround , 2

sCelda = sLetra & iIni + 12 + m_iNumItemsEnHoja * 3
xlSheet.Range(sCelda).Value = "=ImporteTotalSegunObjetivo(""" & sLetra & """,""" & xlSheet.Name & """," & m_iNumItemsEnHoja & ")"
xlSheet.Range(sCelda).borderaround , 2
xlSheet.Range(sCelda).NumberFormat = m_sFormatoNumberGr
       
Set orange = xlSheet.Range("E" & iIni + 13 + m_iNumItemsEnHoja * 3 & ":" & "E" & 13 + m_iNumItemsEnHoja * 3 + iIni)
With orange.formatconditions.Add(1, 5, "0")
    .Interior.Color = RGB(162, 250, 158)
End With
With orange.formatconditions.Add(1, 6, "0")
    .Interior.Color = RGB(253, 100, 72)
End With
orange.Font.Bold = False
orange.borderaround , 2

sLetra = "F"
iCol = 6

    For Each oProve In g_oOrigen.m_oProvesAsig
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            scodProve = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
            If g_oOrigen.m_oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
    
            bMostrarProve = True
            If g_oOrigen.m_oVistaSeleccionada.Vista <> 0 Then
                If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(oProve.Cod).Visible <> TipoProvVisible.Visible Then
                    bMostrarProve = ComprobarAdjudicacionesDeProve(oProve.Cod)
                End If
            Else
                bMostrarProve = ProvVisible(oProve.Cod)
            End If
            If bMostrarProve Then
                   
                sCelda = sLetra & iIni + 10 + m_iNumItemsEnHoja * 3
                xlSheet.Range(sCelda).NumberFormat = "@"
                xlSheet.Range(sCelda).Value = oProve.Cod & " - " & oProve.Den
                xlSheet.Range(sCelda).Font.Size = 6
                xlSheet.Range(sCelda).Font.Bold = True
                xlSheet.Range(sCelda).Interior.Color = RGB(192, 192, 192)
                xlSheet.Range(sCelda).borderaround , 2
                
                sCelda2 = sLetra & iIni + 11 + m_iNumItemsEnHoja * 3
                    
                'Ahorro proveedor
                xlSheet.Range(sCelda2).NumberFormat = m_sFormatoNumberGr
                xlSheet.Range(sCelda2).Value = "= (" & sLetra & iIni + 13 + m_iNumItemsEnHoja * 3 & "-" & sLetra & iIni + 12 + m_iNumItemsEnHoja * 3 & ")"
                
                xlSheet.Range(sCelda2).borderaround , 2
                With xlSheet.Range(sCelda2).formatconditions.Add(1, 5, "0")
                    .Interior.Color = RGB(162, 250, 158)
                End With
                With xlSheet.Range(sCelda2).formatconditions.Add(1, 6, "0")
                    .Interior.Color = RGB(253, 100, 72)
                End With
                            
                sCelda2 = sLetra & iIni + 12 + m_iNumItemsEnHoja * 3
                xlSheet.Range(sCelda2).NumberFormat = m_sFormatoNumberGr
                xlSheet.Range(sCelda2).Value = "= AdjudicadoProveedorGrupo(""" & sLetra & """,""" & oGrupo.Codigo & """," & m_iNumItemsEnHoja & ")"
                xlSheet.Range(sCelda2).Interior.Color = RGB(192, 192, 192)
                xlSheet.Range(sCelda2).borderaround , 2
                
                sCelda2 = sLetra & iIni + 13 + m_iNumItemsEnHoja * 3
                xlSheet.Range(sCelda2).NumberFormat = m_sFormatoNumberGr
                xlSheet.Range(sCelda2).Value = "= PresupuestoConsumidoProveedorGrupo(""" & sLetra & """,""" & oGrupo.Codigo & """," & m_iNumItemsEnHoja & ")"
                xlSheet.Range(sCelda2).Interior.Color = RGB(192, 192, 192)
                xlSheet.Range(sCelda2).borderaround , 2
                
                iCol = iCol + 1
                
                sLetra = DevolverLetra(sLetra, 1)
            End If
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "MostrarTotalesGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function AhorroProveedor() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AhorroProveedor(Columna As Integer,sCodGrupo as string) As Double"
sCodigo = sCodigo & vbLf & "Dim fila As Integer"
sCodigo = sCodigo & vbLf & "Dim sumatmp As Double"
sCodigo = sCodigo & vbLf & "    fila = " & gParametrosInstalacion.giLongCabComp + 11
sCodigo = sCodigo & vbLf & "sumatmp = 0"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "While ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila, 3) <> """
sCodigo = sCodigo & vbLf & "    If ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila, Columna) <> """" Then"
sCodigo = sCodigo & vbLf & "        sumatmp = sumatmp +(ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila, 3) - ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila, Columna)) * ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila + 1, Columna)"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "    fila = fila + 3"
sCodigo = sCodigo & vbLf & "Wend"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "AhorroProveedor= sumatmp"
sCodigo = sCodigo & vbLf & "End Function"
AhorroProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AhorroProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function ImporteProveedor() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function ImporteProveedor(Columna As Integer,sCodGrupo as string) As Double"
sCodigo = sCodigo & vbLf & "Dim fila As Integer"
sCodigo = sCodigo & vbLf & "Dim sumatmp As Double"
sCodigo = sCodigo & vbLf & "    fila = " & gParametrosInstalacion.giLongCabComp + 11
sCodigo = sCodigo & vbLf & "sumatmp = 0"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "While ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila, 3) <> """
sCodigo = sCodigo & vbLf & "    If ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila, Columna) <> """" Then"
sCodigo = sCodigo & vbLf & "        sumatmp = sumatmp + ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila, Columna) * ThisWorkbook.Sheets.item(sCodGrupo).Cells(fila + 1, Columna)"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "    fila = fila + 3"
sCodigo = sCodigo & vbLf & "Wend"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "ImporteProveedor = sumatmp"
sCodigo = sCodigo & vbLf & "End Function"

ImporteProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ImporteProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Function PresupuestoConsumidoProveedor() As String
Dim sCodigo As String
Dim iGrupos As Integer
Dim bMostrarGr As Boolean
Dim oGrupo As CGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoConsumidoProveedor(sLetra As String) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim iNumGrupos As Integer"
sCodigo = sCodigo & vbLf & "Dim sCelda As String"
sCodigo = sCodigo & vbLf & "Dim I As Integer"

iGrupos = 0
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        iGrupos = iGrupos + 1
    End If
Next

sCodigo = sCodigo & vbLf & "dImporte = 0"
sCodigo = sCodigo & vbLf & "iNumGrupos = " & iGrupos
sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 8"
sCodigo = sCodigo & vbLf & "sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    For I = 0 To iNumGrupos - 1"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + Sheets.Item(""" & sIdiGeneral & """).Range(sCelda).Value"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 1"
sCodigo = sCodigo & vbLf & "        sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    Next I"
sCodigo = sCodigo & vbLf & "PresupuestoConsumidoProveedor = dImporte"
sCodigo = sCodigo & vbLf & "End Function"

PresupuestoConsumidoProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoConsumidoProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Function AdjudicadoProveedor() As String
Dim sCodigo As String
Dim iGrupos As Integer
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AdjudicadoProveedor(sLetra As String) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim iNumGrupos As Integer"
sCodigo = sCodigo & vbLf & "Dim sCelda As String"
sCodigo = sCodigo & vbLf & "Dim I As Integer"

iGrupos = 0
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        iGrupos = iGrupos + 1
    End If
Next

sCodigo = sCodigo & vbLf & "dImporte = 0"
sCodigo = sCodigo & vbLf & "iNumGrupos = " & iGrupos
sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 8"
sCodigo = sCodigo & vbLf & "sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    For I = 0 To iNumGrupos - 1"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + Sheets.Item(""" & sIdiGeneral & """).Range(sCelda).Value"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 1"
sCodigo = sCodigo & vbLf & "        sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    Next I"
sCodigo = sCodigo & vbLf & "AdjudicadoProveedor = dImporte"
sCodigo = sCodigo & vbLf & "End Function"

AdjudicadoProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AdjudicadoProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' A�ade al m�dulo: AdjudicadoProveedorGrupo.
''' </summary>
''' <param name></param>
''' <returns>El c�digo a copiar en el m�dulo de la hoja excel</returns>
''' <remarks>Llamada desde:ObtenerHojaComparativa; Tiempo m�ximo:0,1</remarks>

Function AdjudicadoProveedorGrupo() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AdjudicadoProveedorGrupo(sLetra As String, sCodGrupo As String,iNumItems as integer) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteAux As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim sCelda As String"
sCodigo = sCodigo & vbLf & "Dim sCodProve as String"
sCodigo = sCodigo & vbLf & "Dim I As Integer"
sCodigo = sCodigo & vbLf & "dImporte = 0"
sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 11"
sCodigo = sCodigo & vbLf & "sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "sCodProve = ObtenerProveedorItem(sCodgrupo, iFila, sLetra)"
sCodigo = sCodigo & vbLf & "    For i = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "        dImporteAux = Sheets.Item(sCodgrupo).Range(sCelda).Value * Sheets.Item(sCodgrupo).Range(sLetra & iFila - 1).Value"
sCodigo = sCodigo & vbLf & "        'Si hay cantidad adjudicada se le aplican los atributos."
sCodigo = sCodigo & vbLf & "        If Sheets.Item(sCodgrupo).Range(sCelda).Value <> """" Then"
sCodigo = sCodigo & vbLf & "            dImporteAux = AplicarAtributosItem_Proveedor(sCodgrupo, Sheets.Item(sCodgrupo).Range(""DA"" & iFila).Value, dImporteAux, sCodProve, iFila)"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + dImporteAux"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "        sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    Next i"
sCodigo = sCodigo & vbLf & "AdjudicadoProveedorGrupo = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
AdjudicadoProveedorGrupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AdjudicadoProveedorGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Function AdjudicadoProveedorGrupoConAtrib() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AdjudicadoProveedorGrupoConAtrib(sLetra As String, sCodGrupo As String,iNumItems as integer) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteAux As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim sCelda As String"
sCodigo = sCodigo & vbLf & "Dim sCodProve as String"
sCodigo = sCodigo & vbLf & "Dim i As Integer"
sCodigo = sCodigo & vbLf & "Dim bAplicar As Boolean"
sCodigo = sCodigo & vbLf & "dImporte = 0"
sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 11"
sCodigo = sCodigo & vbLf & "sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "bAplicar = False "
sCodigo = sCodigo & vbLf & "sCodProve = ObtenerProveedorItem(sCodGrupo, iFila, sLetra) "
sCodigo = sCodigo & vbLf & "For i = 0 To iNumItems - 1 "
sCodigo = sCodigo & vbLf & "    dImporteAux = Sheets.Item(sCodgrupo).Range(sCelda).Value * Sheets.Item(sCodgrupo).Range(sLetra & iFila - 1).Value "
sCodigo = sCodigo & vbLf & "    'Si hay cantidad adjudicada se le aplican los atributos. "
sCodigo = sCodigo & vbLf & "    If Sheets.Item(sCodgrupo).Range(sCelda).Value <> """" Then "
sCodigo = sCodigo & vbLf & "        dImporteAux = AplicarAtributosItem_Proveedor(sCodgrupo, Sheets.Item(sCodgrupo).Range(""DA"" & iFila).Value, dImporteAux, sCodProve, iFila) "
sCodigo = sCodigo & vbLf & "        bAplicar = True "
sCodigo = sCodigo & vbLf & "    End If "
sCodigo = sCodigo & vbLf & "    dImporte = dImporte + dImporteAux "
sCodigo = sCodigo & vbLf & "    iFila = iFila + 3 "
sCodigo = sCodigo & vbLf & "    sCelda = sLetra & iFila "
sCodigo = sCodigo & vbLf & "Next i "
sCodigo = sCodigo & vbLf & "If bAplicar Then "
sCodigo = sCodigo & vbLf & "    dImporte = AplicarAtributosGrupo_Proveedor(sCodgrupo, dImporte, sCodProve, iFila) "
sCodigo = sCodigo & vbLf & "End If "
sCodigo = sCodigo & vbLf & "AdjudicadoProveedorGrupoConAtrib = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
AdjudicadoProveedorGrupoConAtrib = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AdjudicadoProveedorGrupoConAtrib", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Function PresupuestoConsumidoProveedorGrupo() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function PresupuestoConsumidoProveedorGrupo(sLetra As String, sCodGrupo As String,iNumItems as integer) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim sCelda As String"
sCodigo = sCodigo & vbLf & "Dim I As Integer"
sCodigo = sCodigo & vbLf & "dImporte = 0"
sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 11"
sCodigo = sCodigo & vbLf & "sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    For I = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + Sheets.Item(sCodGrupo).Range(sCelda).Value * Sheets.Item(sCodGrupo).Range(""C"" & iFila - 1).Value"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "        sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    Next I"
sCodigo = sCodigo & vbLf & "PresupuestoConsumidoProveedorGrupo = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
PresupuestoConsumidoProveedorGrupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "PresupuestoConsumidoProveedorGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function Split97() As String
Dim sCodigo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Private Function Split97(ByVal sValor As Variant, sSeparador As String) As Variant"
sCodigo = sCodigo & vbLf & "Dim sTmp As String"
sCodigo = sCodigo & vbLf & "Dim arr() As String"
sCodigo = sCodigo & vbLf & "Dim iPos As Integer"
sCodigo = sCodigo & vbLf & "If IsNull(sValor) Then"
sCodigo = sCodigo & vbLf & "    Split97 = Null"
sCodigo = sCodigo & vbLf & "    Exit Function"
sCodigo = sCodigo & vbLf & "End If"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "On Error Resume Next"
sCodigo = sCodigo & vbLf & "sTmp = CStr(sValor)"
sCodigo = sCodigo & vbLf & "If Err <> 0 Then"
sCodigo = sCodigo & vbLf & "    Split97 = Null"
sCodigo = sCodigo & vbLf & "    Exit Function"
sCodigo = sCodigo & vbLf & "End If"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "ReDim arr(0) As String"
sCodigo = sCodigo & vbLf & "sTmp = sValor"
sCodigo = sCodigo & vbLf & "While (InStr(sTmp, sSeparador) > 0)"
sCodigo = sCodigo & vbLf & "    iPos = InStr(sTmp, sSeparador)"
sCodigo = sCodigo & vbLf & "    arr(UBound(arr)) = Left(sTmp, iPos - 1)"
sCodigo = sCodigo & vbLf & "    sTmp = Right(sTmp, Len(sTmp) - iPos)"
sCodigo = sCodigo & vbLf & "    ReDim Preserve arr(UBound(arr) + 1) As String"
sCodigo = sCodigo & vbLf & "Wend"
sCodigo = sCodigo & vbLf & "arr(UBound(arr)) = sTmp"
sCodigo = sCodigo & vbLf & "Split97 = arr"
sCodigo = sCodigo & vbLf & ""
sCodigo = sCodigo & vbLf & "End Function"

Split97 = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Split97", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function TodoElProcesoAQueProveedor() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function TodoElProcesoAQueProveedor() As String"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim sProve As String"
sCodigo = sCodigo & vbLf & "Dim bCondicion As Boolean"
sCodigo = sCodigo & vbLf & "Dim sPrimerProve As String"
sCodigo = sCodigo & vbLf & "Dim vAux As variant"
sCodigo = sCodigo & vbLf & "Dim sAux As String"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(""" & sIdiGeneral & """)"
sCodigo = sCodigo & vbLf & "    iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 8"
sCodigo = sCodigo & vbLf & "    bCondicion = True"
sCodigo = sCodigo & vbLf & "    sProve = """
sCodigo = sCodigo & vbLf & "    While xlsheet.Cells(iFila, 1).Value <> """" And bCondicion"
sCodigo = sCodigo & vbLf & "        vAux = Split97(xlsheet.Cells(iFila, 1),"" - "")"
sCodigo = sCodigo & vbLf & "        sAux = vAux(0)"
sCodigo = sCodigo & vbLf & "        sProve = TodoElGrupoAQueProveedor(sAux)"
sCodigo = sCodigo & vbLf & "        if sProve = ""-*@@@@-*"" then"
sCodigo = sCodigo & vbLf & "             bCondicion = False"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        If sPrimerProve = """" Then"
sCodigo = sCodigo & vbLf & "            sPrimerProve = sProve"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        If sPrimerProve <> """" And sPrimerProve <> sProve and sProve <> """" Then"
sCodigo = sCodigo & vbLf & "            bCondicion = False"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 1"
sCodigo = sCodigo & vbLf & "    Wend"
sCodigo = sCodigo & vbLf & "    If Not bCondicion Then"
sCodigo = sCodigo & vbLf & "        TodoElProcesoAQueProveedor = ""-*@@@@-*"""
sCodigo = sCodigo & vbLf & "    Else"
sCodigo = sCodigo & vbLf & "        If sPrimerProve <> """" Then"
sCodigo = sCodigo & vbLf & "            TodoElProcesoAQueProveedor = sPrimerProve"
sCodigo = sCodigo & vbLf & "        Else"
sCodigo = sCodigo & vbLf & "            TodoElProcesoAQueProveedor = """""
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "End Function"

TodoElProcesoAQueProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "TodoElProcesoAQueProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function ImporteTotalSegunObjetivo() As String
Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = sCodigo & "Function ImporteTotalSegunObjetivo(sLetra As String, sCodGrupo As String, iNumItems As Integer) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim sCelda As String"
sCodigo = sCodigo & vbLf & "Dim I As Integer"
sCodigo = sCodigo & vbLf & "dImporte = 0"
sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 10"
sCodigo = sCodigo & vbLf & "sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    For I = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + Sheets.Item(sCodGrupo).Range(sCelda).Value * Sheets.Item(sCodGrupo).Range(""C"" & iFila + 1).Value"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "        sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    Next I"
sCodigo = sCodigo & vbLf & "ImporteTotalSegunObjetivo = dImporte"
sCodigo = sCodigo & vbLf & "End function"
ImporteTotalSegunObjetivo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ImporteTotalSegunObjetivo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function AhorroTotalSegunObjetivo() As String

Dim sCodigo As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function AhorroTotalSegunObjetivo(sLetra As String, sCodGrupo As String, iNumItems As Integer) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim iFila As Integer"
sCodigo = sCodigo & vbLf & "Dim sCelda As String"
sCodigo = sCodigo & vbLf & "Dim I As Integer"
sCodigo = sCodigo & vbLf & "dImporte = 0"
sCodigo = sCodigo & vbLf & "iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 10"
sCodigo = sCodigo & vbLf & "sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    For I = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "        dImporte = dImporte + (Sheets.Item(sCodGrupo).Range(""C"" & iFila).Value - Sheets.Item(sCodGrupo).Range(sCelda).Value) * Sheets.Item(sCodGrupo).Range(""C"" & iFila + 1).Value"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "        sCelda = sLetra & iFila"
sCodigo = sCodigo & vbLf & "    Next I"
sCodigo = sCodigo & vbLf & "AhorroTotalSegunObjetivo = dImporte"
sCodigo = sCodigo & vbLf & "End Function"
AhorroTotalSegunObjetivo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "AhorroTotalSegunObjetivo", err, Erl, , m_bActivado)
      Exit Function
   End If

End Function

''' <summary>
''' Obtener Hoja Comparativa Item
''' </summary>
''' <remarks>Llamada desde: cmdAceptar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub ObtenerHojaComparativaItem()
Dim xlApp As Object
Dim xlBook As Object
Dim xlSheet As Object
Dim iIni As Integer
Dim sVersion As String
Dim iVersion As Integer
Dim oProject As Object
Dim oModule As Object
Dim i As Integer
Dim sCelda As String
Dim sCeldaPresAbierto As String
Dim sCeldaPresConsumido As String
Dim sCeldaAdjudicado As String
Dim sCeldaAhorrado As String
Dim sCeldaAhorradoPorcen As String
Dim sCeldaPresAbiertoGrupo As String
Dim sCeldaPresConsumidoGrupo As String
Dim sCeldaAdjudicadoGrupo As String
Dim sCeldaAhorradoGrupo As String
Dim sCeldaAhorradoPorcenGrupo As String
Dim sCeldaPresAbiertoItem As String
Dim sCeldaPresConsumidoItem As String
Dim sCeldaAdjudicadoItem As String
Dim sCeldaAhorradoItem As String
Dim sCeldaAhorradoPorcenItem As String
Dim orange As Object
Dim iFila As Integer
Dim sComentario As String
Dim scodProve As String
Dim oProve As CProveedor
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim iFilaItem As Integer
Dim iFilaAux As Integer
Dim sCodProveGrupo As String
Dim sCodProveItem As String
Dim sCod As String
Dim vbm As Variant
Dim bMostrarProve As Boolean
Dim iFilaMax  As Integer
Dim bCargar As Boolean
Dim scodProve2 As String
Dim oAtribs As CAtributos
Dim sCol As String
Dim dImporteGP As Double 'Importe adjudicado por proveedor y grupo.

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
On Error GoTo ERROR_Frm

    'El form debe ser frmADJItem
    If g_oOrigen.Name <> "frmADJItem" Then
        oMensajes.NoValido 47
        Unload Me
    End If
    'Comprobar las plantillas
    If UCase(Right(txtDetProceDot, 3)) <> "XLT" Then
        oMensajes.NoValida sIdiPlantilla
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
    
    If Not oFos.FileExists(txtDetProceDot) Then
        oMensajes.PlantillaNoEncontrada txtDetProceDot
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    Me.Hide
    
    'Crear aplicaci�n excell
    Set xlApp = CreateObject("Excel.Application")
    sVersion = xlApp.VERSION
    
    iIni = basPublic.gParametrosInstalacion.giLongCabCompItem
    If iIni <= 0 Then
        iIni = 1
    End If
    
    Set xlBook = xlApp.Workbooks.Add(txtDetProceDot)
    
    'Primero introducimos las funciones de c�lculo de ahorros necesarias
    '********************************************************************
    Set oProject = xlApp.VBE.VBProjects.Item(1)
    Set oModule = oProject.VBComponents.Add(1)
    
    'El split
    oModule.codemodule.addfromstring Split97
    oModule.codemodule.addfromstring Comp_Item_Variables
    oModule.codemodule.addfromstring Comp_Item_PresupuestoConsumidoItem
    oModule.codemodule.addfromstring Comp_Item_AdjudicadoItem
    oModule.codemodule.addfromstring Comp_Item_AplicarAtributosItemProveedor
    oModule.codemodule.addfromstring Comp_Item_TodoElItemAQueProveedor
    oModule.codemodule.addfromstring Comp_Item_CalcularImporteAdjudicadoItem
    oModule.codemodule.addfromstring Comp_Item_AdjudicadoItemProve
    oModule.codemodule.addfromstring Comp_Item_ConsumidoItemProve
    oModule.codemodule.addfromstring Comp_Item_AplicarAtributos_Grupo
    oModule.codemodule.addfromstring Comp_Item_AdjudicadoGrupo
    oModule.codemodule.addfromstring Comp_Item_TodoElGrupoAQueProveedor
    oModule.codemodule.addfromstring Comp_Item_AdjudicadoProceso
    oModule.codemodule.addfromstring Comp_Item_TodoElProcesoAQueProveedor
    oModule.codemodule.addfromstring Comp_Item_AplicarAtributos_Proceso
    oModule.codemodule.addfromstring Comp_Item_PresupuestoConsumidoGrupo
    oModule.codemodule.addfromstring Comp_Item_PresupuestoConsumidoProceso
    oModule.codemodule.addfromstring Comp_Item_ObtenerValorAtrib
    oModule.codemodule.addfromstring IncrementarColumna
    oModule.codemodule.addfromstring AplicarValorAtrib
    oModule.codemodule.addfromstring Comp_Item_ObtenerProveedorItem
    
    Dim oatrib As CAtributo
    Dim sLetra As String
    Dim oAsig As COferta

    'Vamos a guardar los valores de los atributos en hojas de la excel
    'Atributos de ambito proceso en una
    Set xlSheet = xlBook.Sheets.Add
    xlSheet.Name = "@P@R@O@C@E@"
    xlSheet.Visible = False
    i = 2
    Set oAtribs = g_oOrigen.m_oProcesoSeleccionado.ATRIBUTOS
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
            If oatrib.PrecioAplicarA < 3 Then
                    For Each oProve In g_oOrigen.m_oProvesAsig
                        bCargar = True
                        If gParametrosGenerales.gbProveGrupos Then
                            scodProve2 = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve2).Grupos.Item(g_oOrigen.g_oItemSeleccionado.grupoCod) Is Nothing Then
                                bCargar = False
                            End If
                        End If
                        If bCargar Then
                            bMostrarProve = False
                            If g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible Then
                                bMostrarProve = True 'ComprobarAdjudicacionesDeProve(oProve.Cod)
                            End If
                            If bMostrarProve Then
                                If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas Is Nothing Then
                                    Set oAsig = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod)
                                    If Not oAsig Is Nothing Then
                                        sLetra = BuscarLetraProve(xlSheet, oProve.Cod, "B")
                                        sCelda = sLetra & i
                                        xlSheet.Range("A" & i).NumberFormat = "@"
                                        xlSheet.Range("A" & i).Value = TraducirPrecioFormula(oatrib.PrecioFormula)
                                        xlSheet.Range(sCelda).NumberFormat = "@"
                                        xlSheet.Range(sCelda).Value = oatrib.Cod
                                        If oatrib.PrecioAplicarA = 1 Then
                                             For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
                                                If EstaAplicado(oatrib.idAtribProce, oGrupo.Codigo) Then
                                                    xlSheet.Range(sLetra & (i + 1)).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod)
                                                End If
                                             Next
                                        Else
                                            If EstaAplicado(oatrib.idAtribProce) Then
                                               xlSheet.Range(sLetra & (i + 1)).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod)
                                            End If
                                        End If
                                   End If
                                End If
                            End If
                        End If
                    Next
                    i = i + 2
            End If
        Next
    End If
    Set oAtribs = Nothing
    'Atributos de ambito grupo en otra
    Set xlSheet = xlBook.Sheets.Add
    xlSheet.Name = "@G@R@U@"
    xlSheet.Visible = False
    i = 2
    Set oGrupo = g_oOrigen.g_ogrupo
    Set oAtribs = g_oOrigen.m_oProcesoSeleccionado.AtributosGrupo
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
            If oatrib.PrecioAplicarA = 2 Or oatrib.PrecioAplicarA = 1 Then
                    'Es de proceso luego est� en todos los grupos y todos los proves
                    For Each oProve In g_oOrigen.m_oProvesAsig
                        bCargar = True
                        If gParametrosGenerales.gbProveGrupos Then
                            scodProve2 = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                            If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve2).Grupos.Item(g_oOrigen.g_oItemSeleccionado.grupoCod) Is Nothing Then
                                bCargar = False
                            End If
                        End If
                        If bCargar Then
                            bMostrarProve = False
                            If g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible Then
                                bMostrarProve = True
                            End If
                            If bMostrarProve Then
                                'Pongo todos los grupos por que har� referencia al numero de grupos
                                    xlSheet.Range("A" & i + 1).NumberFormat = "@"
                                    xlSheet.Range("A" & i + 1).Value = TraducirPrecioFormula(oatrib.PrecioFormula)
                                    xlSheet.Range("B" & i + 1).NumberFormat = "@"
                                    xlSheet.Range("B" & i + 1).Value = oGrupo.Codigo
                                    sLetra = BuscarLetraProve(xlSheet, oProve.Cod, "C")
                                    sCelda = sLetra & i
                                                        
                                    If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas Is Nothing Then
                                        Set oAsig = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod)
                                        If Not oAsig Is Nothing Then
                                            xlSheet.Range(sCelda).NumberFormat = "@"
                                            xlSheet.Range(sCelda).Value = oatrib.Cod
                                            xlSheet.Range(sLetra & i + 1).NumberFormat = "@"
                                            If EstaAplicado(oatrib.idAtribProce, oGrupo.Codigo) Then
                                                xlSheet.Range(sLetra & i + 1).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod, oGrupo.Codigo)
                                            End If
                                        End If
                                    End If
                            End If
                        End If
                    Next
                    i = i + 2
                End If
        Next
    End If
    Set oAtribs = Nothing
        
    'Guardar todos los valores de los atributos de ambito item
    'Una hoja
    Dim bMostrarGrupo As Boolean
    Set xlSheet = xlBook.Sheets.Add
    xlSheet.Name = "@I@T@E@M@"
    xlSheet.Visible = False
    i = 2
    Set oItem = g_oOrigen.g_oItemSeleccionado
    Set oAtribs = g_oOrigen.m_oProcesoSeleccionado.AtributosItem
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
             If oatrib.PrecioAplicarA = 2 Then
                 bMostrarGrupo = False
                 If IsNull(oatrib.codgrupo) Then
                    bMostrarGrupo = True
                 ElseIf oGrupo.Codigo = oatrib.codgrupo Then
                    bMostrarGrupo = True
                 End If
                 If bMostrarGrupo Then
                         xlSheet.Range("A" & i).NumberFormat = "@"
                         xlSheet.Range("A" & i).Value = oatrib.Cod
                         xlSheet.Range("B" & i).NumberFormat = "@"
                         xlSheet.Range("B" & i).Value = TraducirPrecioFormula(oatrib.PrecioFormula)
                         For Each oProve In g_oOrigen.m_oProvesAsig
                            bCargar = True
                            If gParametrosGenerales.gbProveGrupos Then
                                scodProve2 = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve2).Grupos.Item(g_oOrigen.g_oItemSeleccionado.grupoCod) Is Nothing Then
                                    bCargar = False
                                End If
                            End If
                            If bCargar Then
                                bMostrarProve = False
                                If g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible Then
                                    bMostrarProve = True
                                End If
                                If bMostrarProve Then
                                    If Not oGrupo.UltimasOfertas Is Nothing Then
                                      Set oAsig = oGrupo.UltimasOfertas.Item(Trim(oProve.Cod))
                                      If Not oAsig Is Nothing Then
                                          sLetra = BuscarLetraProve(xlSheet, oProve.Cod, "C")
                                          sCelda = sLetra & i
                                          xlSheet.Range(sCelda).Value = TraducirPrecio(oatrib, oAsig, oProve.Cod, , oItem.Id)
                                      End If
                                    End If
                                End If
                            End If
                         Next
                         i = i + 1
                 End If
             End If
         Next
    End If
    Set oAtribs = Nothing
    Set oItem = Nothing
    Set oGrupo = Nothing
        
    Dim iNumSheets As Integer
    iNumSheets = xlBook.Sheets.Count

    Set xlSheet = xlBook.Sheets.Item(iNumSheets)
        
    '----------------
    
    'Guardamos los grupos del proceso en "BA".
    iFila = 2
    For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
        If oGrupo.Codigo = g_oOrigen.g_ogrupo.Codigo Then
            xlSheet.Range("BA" & 1).NumberFormat = "@"
            xlSheet.Range("BA" & 1).Value = oGrupo.Codigo
        Else
            xlSheet.Range("BA" & iFila).NumberFormat = "@"
            xlSheet.Range("BA" & iFila).Value = oGrupo.Codigo
            iFila = iFila + 1
        End If
    Next

    '----------------

    'Datos de cabecera de proceso
    sCelda = "A" & iIni
    iFila = iIni
    With xlSheet
        'Proceso
        .Range("A" & iFila).NumberFormat = "@"
        .Range("A" & iFila) = sIdiProceso
        .Range("B" & iFila).NumberFormat = "@"
        .Range("B" & iFila) = g_oOrigen.m_oProcesoSeleccionado.Anyo & "/" & g_oOrigen.m_oProcesoSeleccionado.GMN1Cod & "/" & g_oOrigen.m_oProcesoSeleccionado.Cod & " " & g_oOrigen.m_oProcesoSeleccionado.Den
        'Grupo
        .Range("A" & iFila + 1).NumberFormat = "@"
        .Range("A" & iFila + 1) = sIdiGrupo
        .Range("B" & iFila + 1).NumberFormat = "@"
        .Range("B" & iFila + 1) = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(g_oOrigen.g_ogrupo.Codigo).Codigo & " - " & g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(g_oOrigen.g_ogrupo.Codigo).Den
        'Item
        .Range("A" & iFila + 2).NumberFormat = "@"
        .Range("A" & iFila + 2) = sIdiItem
        If g_oOrigen.g_oItemSeleccionado.ArticuloCod <> "" Then
            .Range("B" & iFila + 2).NumberFormat = "@"
            .Range("B" & iFila + 2) = g_oOrigen.g_oItemSeleccionado.ArticuloCod & " - " & g_oOrigen.g_oItemSeleccionado.Descr
        Else
            .Range("B" & iFila + 2).NumberFormat = "@"
            .Range("B" & iFila + 2) = g_oOrigen.g_oItemSeleccionado.Descr
        End If
            
        'Moneda
        .Range("D" & iFila + 3).NumberFormat = "@"
        .Range("D" & iFila + 3) = sIdiMoneda2
        .Range("E" & iFila + 3).NumberFormat = "@"
        .Range("E" & iFila + 3) = g_oOrigen.m_oProcesoSeleccionado.MonCod
        
        sCelda = "F" & iFila + 3
        If g_oOrigen.m_oProcesoSeleccionado.PermitirAdjDirecta Then
                .Range(sCelda).NumberFormat = "@"
                .Range(sCelda) = sIdiAdjDir
                 sCelda = "G" & iFila + 3
                .Range(sCelda).NumberFormat = "dd/mm/yyyy"
                .Range(sCelda) = g_oOrigen.m_oProcesoSeleccionado.FechaPresentacion
        Else
                .Range(sCelda).NumberFormat = "@"
                .Range(sCelda) = sIdiAdjReu
    
                sCelda = "G" & iFila + 3
                .Range(sCelda).NumberFormat = "dd/mm/yyyy"
                If g_oOrigen.m_oProcesoSeleccionado.FechaProximaReunion = "" Then
                    .Range(sCelda) = g_oOrigen.m_oProcesoSeleccionado.FechaPresentacion
                Else
                    .Range(sCelda) = g_oOrigen.m_oProcesoSeleccionado.FechaProximaReunion
                End If
        End If
                    
        Set orange = xlSheet.Range("A" & iFila & ":" & "G" & iFila + 3)
        orange.Font.Bold = True
        orange.borderaround , 2
        orange.Interior.Color = RGB(192, 192, 192)
                         
        'Pasamos los datos del item
        .Range("A" & iIni + 5).NumberFormat = "@"
        .Range("A" & iIni + 5) = sIdiUnidad
        .Range("A" & iIni + 5).Interior.Color = RGB(192, 192, 192)
        .Range("A" & iIni + 5).borderaround , 2
        
        .Range("B" & iIni + 5).NumberFormat = "@"
        .Range("B" & iIni + 5) = sIdiCantidad
        .Range("B" & iIni + 5).Interior.Color = RGB(192, 192, 192)
        .Range("B" & iIni + 5).borderaround , 2
        
        .Range("C" & iIni + 5).NumberFormat = "@"
        .Range("C" & iIni + 5) = sIdiPresupuesto
        .Range("C" & iIni + 5).Interior.Color = RGB(192, 192, 192)
        .Range("C" & iIni + 5).borderaround , 2
        
        .Range("D" & iIni + 5).NumberFormat = "@"
        .Range("D" & iIni + 5) = sIdiDestino
        .Range("D" & iIni + 5).Interior.Color = RGB(192, 192, 192)
        .Range("D" & iIni + 5).borderaround , 2
        
        .Range("E" & iIni + 5).NumberFormat = "@"
        .Range("E" & iIni + 5) = sIdiInicio
        .Range("E" & iIni + 5).Interior.Color = RGB(192, 192, 192)
        .Range("E" & iIni + 5).borderaround , 2
        
        .Range("F" & iIni + 5).NumberFormat = "@"
        .Range("F" & iIni + 5) = sIdiFin
        .Range("F" & iIni + 5).Interior.Color = RGB(192, 192, 192)
        .Range("F" & iIni + 5).borderaround , 2
        
        .Range("A" & iIni + 6).NumberFormat = "@"
        .Range("A" & iIni + 6) = g_oOrigen.g_oItemSeleccionado.UniCod
        'Cargamos las unidades
        Set oUnidades = oFSGSRaiz.Generar_CUnidades
        oUnidades.CargarTodasLasUnidades g_oOrigen.g_oItemSeleccionado.UniCod, , True
        If oUnidades.Count > 0 Then
            sComentario = oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        Else
            sComentario = ""
        End If
        .Range("A" & iIni + 6).AddComment sComentario
        
        Set orange = .Range("A" & iIni + 6)
        orange.borderaround , 2
        
        .Range("B" & iIni + 6) = g_oOrigen.g_oItemSeleccionado.Cantidad
        Set orange = .Range("B" & iIni + 6)
        orange.borderaround , 2
        orange.NumberFormat = m_sFormatoNumberCantItem
        
        .Range("C" & iIni + 6) = g_oOrigen.g_oItemSeleccionado.Presupuesto
        Set orange = .Range("C" & iIni + 6)
        orange.borderaround , 2
        orange.NumberFormat = m_sFormatoNumberPrecioItem
        
        .Range("D" & iIni + 6).NumberFormat = "@"
        .Range("D" & iIni + 6) = g_oOrigen.g_oItemSeleccionado.DestCod
        Set oDestinos = oFSGSRaiz.Generar_CDestinos
        oDestinos.CargarTodosLosDestinos g_oOrigen.g_oItemSeleccionado.DestCod, , True, , , , , , , , True
        If oDestinos.Count > 0 Then
            sComentario = oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sComentario = sComentario & vbLf & oDestinos.Item(1).dir
            sComentario = sComentario & vbLf & oDestinos.Item(1).POB
            sComentario = sComentario & vbLf & oDestinos.Item(1).cP
            sComentario = sComentario & vbLf & oDestinos.Item(1).Provi
            sComentario = sComentario & vbLf & oDestinos.Item(1).Pais
        Else
            sComentario = ""
        End If
        .Range("D" & iIni + 6).AddComment sComentario
        .Range("D" & iIni + 6).borderaround , 2
        
        .Range("E" & iIni + 6) = g_oOrigen.g_oItemSeleccionado.FechaInicioSuministro
        Set orange = .Range("E" & iIni + 6)
        orange.borderaround , 2
        orange.NumberFormat = "dd/mm/yyyy"
        
        .Range("F" & iIni + 6) = g_oOrigen.g_oItemSeleccionado.FechaFinSuministro
        Set orange = .Range("F" & iIni + 6)
        orange.borderaround , 2
        orange.NumberFormat = "dd/mm/yyyy"
        
        'Datos de resultados del proceso,grupo, item
        
        sCelda = "A" & 8 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiResultados
        .Range(sCelda & ":B" & iIni + 8).Merge
        .Range(sCelda & ":B" & iIni + 8).Interior.Color = RGB(192, 192, 192)
        .Range(sCelda & ":B" & iIni + 8).borderaround , 2
        
        sCelda = "A" & 9 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiProceso
        .Range(sCelda).Font.Bold = True
        .Range(sCelda & ":B" & iIni + 9).Merge
        .Range(sCelda & ":B" & iIni + 9).Interior.Color = RGB(192, 192, 192)
        .Range(sCelda & ":B" & iIni + 9).borderaround , 2
        
        sCelda = "A" & 10 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiGrupo
        .Range(sCelda).Font.Bold = True
        .Range(sCelda & ":B" & iIni + 10).Merge
        .Range(sCelda & ":B" & iIni + 10).Interior.Color = RGB(192, 192, 192)
        .Range(sCelda & ":B" & iIni + 10).borderaround , 2
        
        sCelda = "A" & 11 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiItem
        .Range(sCelda).Font.Bold = True
        .Range(sCelda & ":B" & iIni + 11).Merge
        .Range(sCelda & ":B" & iIni + 11).Interior.Color = RGB(192, 192, 192)
        .Range(sCelda & ":B" & iIni + 11).borderaround , 2
        
        sCelda = "C" & 8 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiPresAbierto
        .Range(sCelda).Interior.Color = RGB(245, 245, 200)
        .Range(sCelda).borderaround , 2
        
        'Presupuesto Abierto
        sCeldaPresAbierto = "C" & 9 + iIni
        .Range(sCeldaPresAbierto) = g_oOrigen.m_oProcesoSeleccionado.PresAbierto
        .Range(sCeldaPresAbierto).Interior.Color = RGB(245, 245, 200)
        .Range(sCeldaPresAbierto).borderaround , 2
        .Range(sCeldaPresAbierto).NumberFormat = m_sFormatoNumberItem
        
         sCelda = "D" & 8 + iIni
         .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiPresConsumido
        .Range(sCelda).ColumnWidth = 12
        .Range(sCelda).Interior.Color = RGB(245, 245, 200)
        .Range(sCelda).borderaround , 2
         
         sCeldaPresConsumido = "D" & 9 + iIni
        .Range(sCeldaPresConsumido).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaPresConsumido).borderaround , 2
        .Range(sCeldaPresConsumido).Interior.Color = RGB(245, 245, 200)
        
        sCelda = "E" & 8 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiAdjudicado
        .Range(sCelda).ColumnWidth = 12
        .Range(sCelda).Interior.Color = RGB(245, 245, 200)
        .Range(sCelda).borderaround , 2
            
        sCeldaAdjudicado = "E" & 9 + iIni
        .Range(sCeldaAdjudicado).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaAdjudicado).borderaround , 2
        .Range(sCeldaAdjudicado).Interior.Color = RGB(245, 245, 200)
        
        sCelda = "F" & 8 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiAhorrado
        .Range(sCelda).ColumnWidth = 12
        .Range(sCelda).Interior.Color = RGB(245, 245, 200)
        .Range(sCelda).borderaround , 2
        
        sCeldaAhorrado = "F" & 9 + iIni
        .Range(sCeldaAhorrado).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaAhorrado).borderaround , 2
        
        'Grupo
        sCeldaPresAbiertoGrupo = "C" & 10 + iIni
        .Range(sCeldaPresAbiertoGrupo).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaPresAbiertoGrupo).borderaround , 2
        .Range(sCeldaPresAbiertoGrupo).Interior.Color = RGB(245, 245, 200)
        
        sCeldaPresConsumidoGrupo = "D" & 10 + iIni
        .Range(sCeldaPresConsumidoGrupo).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaPresConsumidoGrupo).borderaround , 2
        .Range(sCeldaPresConsumidoGrupo).Interior.Color = RGB(245, 245, 200)
        
        sCeldaAdjudicadoGrupo = "E" & 10 + iIni
        .Range(sCeldaAdjudicadoGrupo).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaAdjudicadoGrupo).borderaround , 2
        .Range(sCeldaAdjudicadoGrupo).Interior.Color = RGB(245, 245, 200)
        
        sCeldaAhorradoGrupo = "F" & 10 + iIni
        .Range(sCeldaAhorradoGrupo).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaAhorradoGrupo).borderaround , 2
        
        sCeldaPresAbiertoItem = "C" & 11 + iIni
        .Range(sCeldaPresAbiertoItem).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaPresAbiertoItem).borderaround , 2
        .Range(sCeldaPresAbiertoItem).Interior.Color = RGB(245, 245, 200)
        
        sCeldaPresConsumidoItem = "D" & 11 + iIni
        .Range(sCeldaPresConsumidoItem).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaPresConsumidoItem).borderaround , 2
        .Range(sCeldaPresConsumidoItem).Interior.Color = RGB(245, 245, 200)
        
        sCeldaAdjudicadoItem = "E" & 11 + iIni
        .Range(sCeldaAdjudicadoItem).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaAdjudicadoItem).borderaround , 2
        .Range(sCeldaAdjudicadoItem).Interior.Color = RGB(245, 245, 200)
        
        sCeldaAhorradoItem = "F" & 11 + iIni
        .Range(sCeldaAhorradoItem).NumberFormat = m_sFormatoNumberItem
        .Range(sCeldaAhorradoItem).borderaround , 2
         
        sCelda = "G" & 8 + iIni
        .Range(sCelda).NumberFormat = "@"
        .Range(sCelda) = sIdiPorcentAhor
        .Range(sCelda).ColumnWidth = 12
        .Range(sCelda).Interior.Color = RGB(245, 245, 200)
        .Range(sCelda).borderaround , 2
        
        sCeldaAhorradoPorcen = "G" & 9 + iIni
        .Range(sCeldaAhorradoPorcen).NumberFormat = m_sFormatoNumberPorcenItem
        .Range(sCeldaAhorradoPorcen).borderaround , 2
       
        sCeldaAhorradoPorcenGrupo = "G" & 10 + iIni
        .Range(sCeldaAhorradoPorcenGrupo).NumberFormat = m_sFormatoNumberPorcenItem
        .Range(sCeldaAhorradoPorcenGrupo).borderaround , 2
    
        sCeldaAhorradoPorcenItem = "G" & 11 + iIni
        .Range(sCeldaAhorradoPorcenItem).NumberFormat = m_sFormatoNumberPorcenItem
        .Range(sCeldaAhorradoPorcenItem).borderaround , 2
    
        'Datos generales del proceso, ahorro,abierto,consumido etc..
        'Los datos de proceso
        .Range(sCeldaPresAbierto) = CDbl(g_oOrigen.sdbgResultados.Columns("Abierto").CellValue(g_oOrigen.sdbgResultados.RowBookmark(0)))
        .Range(sCeldaPresConsumido) = "= PresupuestoConsumidoProceso()"
        .Range(sCeldaAdjudicado) = "= AdjudicadoProceso()"
        .Range(sCeldaAhorrado) = "= (" & sCeldaPresConsumido & "-" & sCeldaAdjudicado & ")"
        .Range(sCeldaAhorradoPorcen) = "= ((" & sCeldaPresConsumido & "-" & sCeldaAdjudicado & ")/" & sCeldaPresConsumido & ")*100"
        'Los de grupo
        .Range(sCeldaPresAbiertoGrupo) = CDbl(g_oOrigen.sdbgResultados.Columns("Abierto").CellValue(g_oOrigen.sdbgResultados.RowBookmark(1)))
        .Range(sCeldaPresConsumidoGrupo) = "= PresupuestoConsumidoGrupo()"
        .Range(sCeldaAdjudicadoGrupo) = "= AdjudicadoGrupo()"
        .Range(sCeldaAhorradoGrupo) = "= (" & sCeldaPresConsumidoGrupo & "-" & sCeldaAdjudicadoGrupo & ")"
        .Range(sCeldaAhorradoPorcenGrupo) = "= ((" & sCeldaPresConsumidoGrupo & "-" & sCeldaAdjudicadoGrupo & ")/" & sCeldaPresConsumidoGrupo & ")*100"
        
        'Los de item
        .Range(sCeldaPresAbiertoItem) = g_oOrigen.g_oItemSeleccionado.Presupuesto
        .Range(sCeldaPresConsumidoItem) = "= PresupuestoConsumidoItem()"
        .Range(sCeldaAdjudicadoItem) = "= AdjudicadoItem()"
        .Range(sCeldaAhorradoItem) = "= (" & sCeldaPresConsumidoItem & "-" & sCeldaAdjudicadoItem & ")"
        .Range(sCeldaAhorradoPorcenItem) = "= ((" & sCeldaPresConsumidoItem & "-" & sCeldaAdjudicadoItem & ")/" & sCeldaPresConsumidoItem & ")*100"
        
        'Ponemos los formatos de color condicionales
        With .Range(sCeldaAhorrado).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
        End With
        With .Range(sCeldaAhorrado).formatconditions.Add(1, 6, "0")
            .Interior.Color = RGB(253, 100, 72)
        End With
        With .Range(sCeldaAhorradoPorcen).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
        End With
        With .Range(sCeldaAhorradoPorcen).formatconditions.Add(1, 6, "0")
            .Interior.Color = RGB(253, 100, 72)
        End With
        
        With .Range(sCeldaAhorradoGrupo).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
        End With
        With .Range(sCeldaAhorradoGrupo).formatconditions.Add(1, 6, "0")
            .Interior.Color = RGB(253, 100, 72)
        End With
        With .Range(sCeldaAhorradoPorcenGrupo).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
        End With
        With .Range(sCeldaAhorradoPorcenGrupo).formatconditions.Add(1, 6, "0")
            .Interior.Color = RGB(253, 100, 72)
        End With
        
        With .Range(sCeldaAhorradoItem).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
        End With
        With .Range(sCeldaAhorradoItem).formatconditions.Add(1, 6, "0")
            .Interior.Color = RGB(253, 100, 72)
        End With
        With .Range(sCeldaAhorradoPorcenItem).formatconditions.Add(1, 5, "0")
            .Interior.Color = RGB(162, 250, 158)
        End With
        With .Range(sCeldaAhorradoPorcenItem).formatconditions.Add(1, 6, "0")
            .Interior.Color = RGB(253, 100, 72)
        End With
        
        'Pasamos los datos de proveedores
        .Range("A" & iIni + 13).NumberFormat = "@"
        .Range("A" & iIni + 13) = sIdiProveedor
        .Range("A" & iIni + 13).Interior.Color = RGB(192, 192, 192)
        .Range("A" & iIni + 13 & ":B" & iIni + 13).Merge
        .Range("B" & iIni + 13).Interior.Color = RGB(192, 192, 192)
        .Range("A" & iIni + 13 & ":B" & iIni + 13).borderaround , 2
        .Range("A" & iIni + 13 & ":B" & iIni + 13).Font.Bold = True
        
        .Range("C" & iIni + 13).NumberFormat = "@"
        .Range("C" & iIni + 13) = sIdiPrecio
        .Range("C" & iIni + 13).Interior.Color = RGB(192, 192, 192)
        .Range("C" & iIni + 13).borderaround , 2
        
        .Range("D" & iIni + 13).NumberFormat = "@"
        .Range("D" & iIni + 13) = sIdiCantidad
        .Range("D" & iIni + 13).Interior.Color = RGB(192, 192, 192)
        .Range("D" & iIni + 13).borderaround , 2
        
        .Range("E" & iIni + 13).NumberFormat = "@"
        .Range("E" & iIni + 13) = sidiconsumido
        .Range("E" & iIni + 13).Interior.Color = RGB(192, 192, 192)
        .Range("E" & iIni + 13).borderaround , 2
        
        .Range("F" & iIni + 13).NumberFormat = "@"
        .Range("F" & iIni + 13) = sIdiAdjudicado
        .Range("F" & iIni + 13).Interior.Color = RGB(192, 192, 192)
        .Range("F" & iIni + 13).borderaround , 2
        
        .Range("G" & iIni + 13).NumberFormat = "@"
        .Range("G" & iIni + 13) = sIdiAhorrado
        .Range("G" & iIni + 13).Interior.Color = RGB(192, 192, 192)
        .Range("G" & iIni + 13).borderaround , 2
        
        
        iFila = iIni + 13
        iFilaMax = iFila + g_oOrigen.sdbgAdj.Rows
        For i = 0 To g_oOrigen.sdbgAdj.Rows - 1
            vbm = g_oOrigen.sdbgAdj.AddItemBookmark(i)
            scodProve = g_oOrigen.sdbgAdj.Columns("PROVECOD").CellValue(vbm)
            bCargar = True
            If gParametrosGenerales.gbProveGrupos Then
                scodProve2 = scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
                If g_oOrigen.g_oOrigen.m_oAsigs.Item(scodProve2).Grupos.Item(g_oOrigen.g_oItemSeleccionado.grupoCod) Is Nothing Then
                    bCargar = False
                End If
            End If
            If bCargar Then
                .Range("A" & iFila + i + 1).NumberFormat = "@"
                .Range("A" & iFila + i + 1) = scodProve
                .Range("BB" & iFila + i + 1).NumberFormat = "@"
                .Range("BB" & iFila + i + 1) = scodProve
                sCol = "BE"
                'Para cada proveedor a�adimos a continuacion el total de grupo.
                For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
                    If oGrupo.Codigo = g_oOrigen.g_ogrupo.Codigo Then
                        sCod = oGrupo.Codigo & scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
                        'A�adimos el total de grupo para ese proveedor SIN atributos de grupo aplicados.
                        .Range("BC" & iFila + i + 1).NumberFormat = m_sFormatoNumberPrecioItem
                        If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
                            dImporteGP = g_oOrigen.m_oAdjs.Item(sCod).AdjudicadoSin / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(scodProve).Cambio
                            .Range("BC" & iFila + i + 1) = dImporteGP 'Importe grupo/prove sin atributos de grupo
                        End If
                        'A�adimos el total de grupo para ese proveedor CON atributos de grupo aplicados.
                        .Range("BD" & iFila + i + 1).NumberFormat = m_sFormatoNumberPrecioItem
                        If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
                            dImporteGP = g_oOrigen.m_oAdjs.Item(sCod).Adjudicado / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(scodProve).Cambio
                            .Range("BD" & iFila + i + 1) = dImporteGP 'Importe grupo/prove con atributos de grupo
                        End If
                    Else
                        sCod = oGrupo.Codigo & scodProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(scodProve))
                        'A�adimos el total de grupo para ese proveedor SIN atributos de grupo aplicados.
                        .Range(sCol & iFila + i + 1).NumberFormat = m_sFormatoNumberPrecioItem
                        If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
                            dImporteGP = g_oOrigen.m_oAdjs.Item(sCod).AdjudicadoSin / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(scodProve).Cambio
                            .Range(sCol & iFila + i + 1) = dImporteGP 'Importe grupo/prove sin atributos de grupo
                        End If
                        sCol = SiguienteColumna(sCol, 1)
                        'A�adimos el total de grupo para ese proveedor CON atributos de grupo aplicados.
                        .Range(sCol & iFila + i + 1).NumberFormat = m_sFormatoNumberPrecioItem
                        If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
                            dImporteGP = g_oOrigen.m_oAdjs.Item(sCod).Adjudicado / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(scodProve).Cambio
                            .Range(sCol & iFila + i + 1) = dImporteGP 'Importe grupo/prove con atributos de grupo
                        End If
                        sCol = SiguienteColumna(sCol, 1)
                    End If
                Next
                sComentario = ""
                g_oOrigen.m_oProvesAsig.CargarDatosProveedor scodProve
                Set oProve = g_oOrigen.m_oProvesAsig.Item(scodProve)
                If Not oProve Is Nothing Then
                
                With oProve
                    xlSheet.Range("A" & iFila + i + 1).NumberFormat = "@"
                    xlSheet.Range("A" & iFila + i + 1) = scodProve & " - " & .Den
                    sComentario = .Den & vbLf
                    sComentario = sComentario & sIdiDirec & " " & .Direccion
                    sComentario = sComentario & vbLf & sIdiCP & " " & .cP
                    sComentario = sComentario & vbLf & sIdiPobl & " " & .Poblacion
                    sComentario = sComentario & vbLf & sIdiProvi & " " & .CodProvi & " " & .DenProvi
                    sComentario = sComentario & vbLf & sIdiPais & " " & .CodPais
                    sComentario = sComentario & vbLf & sIdiMoneda & " " & .CodMon
        
                End With
                
                End If
                
                .Range("A" & iFila + i + 1 & ":B" & iFila + i + 1).Merge
                .Range("A" & iFila + i + 1 & ":B" & iFila + i + 1).Interior.Color = RGB(200, 200, 200)
                .Range("A" & iFila + i + 1 & ":B" & iFila + i + 1).borderaround , 2
                .Range("A" & iFila + i + 1).AddComment sComentario
                'Precio unitario
                If g_oOrigen.sdbgAdj.Columns("PRECIO").CellValue(vbm) = "" Then
                    .Range("C" & iFila + i + 1) = ""
                Else
                    .Range("C" & iFila + i + 1) = CDbl(g_oOrigen.sdbgAdj.Columns("PRECIO").CellValue(vbm))
                End If
                
                .Range("C" & iFila + i + 1).Interior.Color = RGB(245, 245, 200)
                .Range("C" & iFila + i + 1).borderaround , 2
                .Range("C" & iFila + i + 1).NumberFormat = m_sFormatoNumberPrecioItem
                
                'Cant Adjudicada
                If g_oOrigen.sdbgAdj.Columns("CANTADJ").CellValue(vbm) = "" Then
                    .Range("D" & iFila + i + 1) = 0
                Else
                    .Range("D" & iFila + i + 1) = CDbl(g_oOrigen.sdbgAdj.Columns("CANTADJ").CellValue(vbm))
                End If
                .Range("D" & iFila + i + 1).Font.Color = RGB(0, 0, 255)
                .Range("D" & iFila + i + 1).borderaround , 2
                .Range("D" & iFila + i + 1).NumberFormat = m_sFormatoNumberCantItem
                
                'Consumido
                .Range("E" & iFila + i + 1) = "= ConsumidoItemProve(" & iFila + i + 1 & ")"
                .Range("E" & iFila + i + 1).Interior.Color = RGB(245, 245, 200)
                .Range("E" & iFila + i + 1).borderaround , 2
                .Range("E" & iFila + i + 1).NumberFormat = m_sFormatoNumberItem
                
                'Adjudicado
                .Range("F" & iFila + i + 1) = "= AdjudicadoItemProve(" & iFila + i + 1 & ")"
                .Range("F" & iFila + i + 1).Interior.Color = RGB(245, 245, 200)
                .Range("F" & iFila + i + 1).borderaround , 2
                .Range("F" & iFila + i + 1).NumberFormat = m_sFormatoNumberItem
                
                'Ahorrado
                .Range("G" & iFila + i + 1) = "= (E" & iFila + i + 1 & "-F" & iFila + i + 1 & ")"
                With .Range("G" & iFila + i + 1).formatconditions.Add(1, 5, "0")
                    .Interior.Color = RGB(162, 250, 158)
                End With
                With .Range("G" & iFila + i + 1).formatconditions.Add(1, 6, "0")
                    .Interior.Color = RGB(253, 100, 72)
                End With
                .Range("G" & iFila + i + 1).borderaround , 2
                .Range("G" & iFila + i + 1).NumberFormat = m_sFormatoNumberItem
            End If
        Next
    End With
    
    'Ahora necesitamos pasar la estructura necesaria para poder saber cuando hay que aplicar atributos de grupo o proceso
    'Todos los grupos con el proveedor al que se ha adjudicado
    iFila = 2
    For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
        
        sCodProveGrupo = ""
        
        If oGrupo.Codigo = g_oOrigen.g_ogrupo.Codigo Then
            xlSheet.Range("DD" & 1).NumberFormat = "@"
            xlSheet.Range("DD" & 1).Value = oGrupo.Codigo
        Else
            xlSheet.Range("DD" & iFila).NumberFormat = "@"
            xlSheet.Range("DD" & iFila).Value = oGrupo.Codigo
        End If
        
        iFilaItem = 2
        Dim bAdj As Boolean
        For Each oItem In oGrupo.Items
            bAdj = False
            'Reservo la primera para el item en cuesti�n
           sCodProveItem = ""
            For Each oProve In g_oOrigen.m_oProvesAsig
                If g_oOrigen.g_oVistaSeleccionada.ConfVistasItemProv.Item(oProve.Cod).Visible = TipoProvVisible.Visible Then
               
                    sCod = oItem.Id & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
                        If oGrupo.Adjudicaciones.Item(sCod).Porcentaje <> 0 Then
                            bAdj = True
                            If sCodProveItem <> "" Then
                                If sCodProveItem <> oProve.Cod Then
                                    'Varios proveedores
                                    sCodProveItem = "-*@@@@-*"
                                End If
                            Else
                                sCodProveItem = oProve.Cod
                            End If
                        End If
                    End If
                End If
            Next
            
            If bAdj Then
                If oGrupo.Codigo = g_oOrigen.g_ogrupo.Codigo Then
                    'Estamos en el grupo por lo que tenemos que poner tambi�n los items y sus proveedores
                    If oItem.Id = g_oOrigen.g_oItemSeleccionado.Id Then
                        xlSheet.Range("DF" & 1).Value = oItem.Id
                        xlSheet.Range("DG" & 1).NumberFormat = "@"
                        xlSheet.Range("DG" & 1).Value = sCodProveItem
                    Else
                        xlSheet.Range("DF" & iFilaItem).Value = oItem.Id
                        xlSheet.Range("DG" & iFilaItem).NumberFormat = "@"
                        xlSheet.Range("DG" & iFilaItem).Value = sCodProveItem
                        iFilaItem = iFilaItem + 1
                    End If
                End If

                If oGrupo.Codigo = g_oOrigen.g_ogrupo.Codigo Then
                    iFilaAux = 1
                Else
                    iFilaAux = iFila
                End If
            
                If sCodProveItem = "-*@@@@-*" Then
                    sCodProveGrupo = "-*@@@@-*"
                    xlSheet.Range("DE" & iFilaAux).NumberFormat = "@"
                    xlSheet.Range("DE" & iFilaAux).Value = "-*@@@@-*"
                    Exit For
                Else
                    If sCodProveGrupo = "" Then
                        sCodProveGrupo = sCodProveItem
                    Else
                        If sCodProveGrupo <> sCodProveItem Then
                            sCodProveGrupo = "-*@@@@-*"
                            xlSheet.Range("DE" & iFilaAux).NumberFormat = "@"
                            xlSheet.Range("DE" & iFilaAux).Value = "-*@@@@-*"
                            Exit For
                        End If
                    End If
                    xlSheet.Range("DE" & iFilaAux).NumberFormat = "@"
                    xlSheet.Range("DE" & iFilaAux).Value = sCodProveGrupo
                End If
            End If
        Next
        
        If oGrupo.Codigo <> g_oOrigen.g_ogrupo.Codigo Then
            iFila = iFila + 1
        End If
        
    Next
    
    xlSheet.PageSetup.PrintArea = "A1:" & "H" & iFilaMax + 4
    
   If IsNumeric(Left(sVersion, InStr(sVersion, ".") - 1)) Then
        iVersion = Left(sVersion, InStr(sVersion, ".") - 1)
   Else
        If Left(sVersion, 1) = "8" Then iVersion = 8
        If Left(sVersion, 3) = "9.0" Then iVersion = 9
        If Left(sVersion, 4) = "10.0" Then iVersion = 10
        If Left(sVersion, 4) = "11.0" Then iVersion = 11
   End If
    If iVersion < 9 Then
        For i = 1 To xlApp.VBE.ActiveVBProject.VBComponents.Count
           If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Type = 100 Then
            If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Name <> "ThisWorkbook" Then
                xlApp.VBE.ActiveVBProject.VBComponents.Item(i).codemodule.addfromstring ActivarCalculosVersion8
                Exit For
            End If
           End If
        Next
    Else
        For i = 1 To xlApp.VBE.ActiveVBProject.VBComponents.Count
           If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Type = 100 Then
            If xlApp.VBE.ActiveVBProject.VBComponents.Item(i).Name <> "ThisWorkbook" Then
                xlApp.VBE.ActiveVBProject.VBComponents.Item(i).codemodule.addfromstring ActivarCalculosVersion9
                Exit For
            End If
           End If
        Next
        xlApp.calculatefull
    End If
   
    xlApp.Visible = True
    BringWindowToTop xlApp.hWnd
    
    Screen.MousePointer = vbNormal
    DoEvents
    Exit Sub
    
ERROR_Frm:
    
    If err.Number = 1004 Then
        
        If (MsgBox(sIdiMensaje, vbExclamation, "FULLSTEP") = vbYes) Then
            xlApp.Quit
            Set xlApp = Nothing
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    Else
        MsgBox err.Description
        If err.Number = 7 Then
            xlApp.Visible = True
            Exit Sub
        End If
    End If
    
    xlApp.Quit
    Set xlApp = Nothing
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ObtenerHojaComparativaItem", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub

Private Function Comp_Item_PresupuestoConsumidoItem() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Function PresupuestoConsumidoItem() As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "Dim sCeldaCant As String"
sCod = sCod & vbLf & "Dim sCeldaProve As String"
sCod = sCod & vbLf & "Dim xlsheet As Worksheet"
sCod = sCod & vbLf & "Dim dPrecio As Double"
sCod = sCod & vbLf & "Dim dCantidad As Double"
sCod = sCod & vbLf & "Dim iFilaProve As Integer"
sCod = sCod & vbLf & "Dim dPresUni As Double"
sCod = sCod & vbLf & "    Set xlsheet = Excel.Sheets.Item(4)"
sCod = sCod & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "    sCeldaProve = ""A"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "    dPresUni = xlsheet.Range(""C"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 6).Value / xlsheet.Range(""B"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 6).Value"
sCod = sCod & vbLf & "    While xlsheet.Range(sCeldaProve).Value <> """""
sCod = sCod & vbLf & "        sCeldaCant = ""D"" & iFilaProve 'Cantidad adjudicada"
sCod = sCod & vbLf & "        If xlsheet.Range(sCeldaCant).Value <> """" Then"
sCod = sCod & vbLf & "            dCantidad = xlsheet.Range(sCeldaCant).Value"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            dCantidad = 0"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        dImporte = dImporte + dPresUni * dCantidad"
sCod = sCod & vbLf & "        iFilaProve = iFilaProve + 1"
sCod = sCod & vbLf & "        sCeldaProve = ""A"" & iFilaProve"
sCod = sCod & vbLf & "    Wend"
sCod = sCod & vbLf & "    PresupuestoConsumidoItem = dImporte"
sCod = sCod & vbLf & "End Function"
Comp_Item_PresupuestoConsumidoItem = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_PresupuestoConsumidoItem", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_AdjudicadoItem() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Function AdjudicadoItem() As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "    dImporte = CalcularImporteAdjudicadoItem()"
sCod = sCod & vbLf & "    AdjudicadoItem = dImporte"
sCod = sCod & vbLf & "End Function"
Comp_Item_AdjudicadoItem = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_AdjudicadoItem", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_CalcularImporteAdjudicadoItem() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Function CalcularImporteAdjudicadoItem(Optional ByVal iFilaUnProve As Integer = 0) As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "Dim dImporteAux As Double"
sCod = sCod & vbLf & "Dim sCeldaCant As String"
sCod = sCod & vbLf & "Dim xlsheet As Worksheet"
sCod = sCod & vbLf & "Dim dPrecio As Double"
sCod = sCod & vbLf & "Dim dCantidad As Double"
sCod = sCod & vbLf & "Dim iFilaProve As Integer"
sCod = sCod & vbLf & "Dim sCeldaProve As String"
sCod = sCod & vbLf & "Dim sCeldaPrecio As String"
sCod = sCod & vbLf & "Dim sCodProve As String"
sCod = sCod & vbLf & "Dim bTodosLosProve As Boolean"
sCod = sCod & vbLf & "    Set xlsheet = Excel.Sheets.Item(4)"
sCod = sCod & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "    sCeldaProve = ""A"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "    bTodosLosProve = True"
sCod = sCod & vbLf & "  While xlsheet.Range(sCeldaProve).Value <> """" And bTodosLosProve"
sCod = sCod & vbLf & "    If iFilaUnProve <> 0 Then"
sCod = sCod & vbLf & "        If xlsheet.Range(sCeldaProve).Value = xlsheet.Range(""A"" & iFilaUnProve).Value Then"
sCod = sCod & vbLf & "            bTodosLosProve = False"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            iFilaProve = iFilaProve + 1"
sCod = sCod & vbLf & "            sCeldaProve = ""A"" & iFilaProve"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "    if iFilaUnProve = 0 Or bTodosLosProve = False then"
sCod = sCod & vbLf & "        dImporteAux = 0"
sCod = sCod & vbLf & "        sCodProve = ObtenerProveedorItem(iFilaProve)"
sCod = sCod & vbLf & "        sCeldaCant = ""D"" & iFilaProve 'Cantidad adjudicada"
sCod = sCod & vbLf & "        If xlsheet.Range(sCeldaCant).Value <> """" Then"
sCod = sCod & vbLf & "            dCantidad = xlsheet.Range(sCeldaCant).Value"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            dCantidad = 0"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        sCeldaPrecio = ""C"" & iFilaProve"
sCod = sCod & vbLf & "        If xlsheet.Range(sCeldaPrecio).Value <> """" Then"
sCod = sCod & vbLf & "            dPrecio = xlsheet.Range(sCeldaPrecio).Value"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            dPrecio = 0"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        dImporteAux = dPrecio * dCantidad"
sCod = sCod & vbLf & "        If dCantidad <> 0 Then"
sCod = sCod & vbLf & "            dImporteAux = AplicarAtributosItem_Proveedor(dImporteAux, sCodProve)"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        dImporte = dImporte + dImporteAux"
sCod = sCod & vbLf & "        iFilaProve = iFilaProve + 1"
sCod = sCod & vbLf & "        sCeldaProve = ""A"" & iFilaProve"
sCod = sCod & vbLf & "   End if"
sCod = sCod & vbLf & "  wend"
sCod = sCod & vbLf & "    CalcularImporteAdjudicadoItem = dImporte"
sCod = sCod & vbLf & "End Function"
Comp_Item_CalcularImporteAdjudicadoItem = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_CalcularImporteAdjudicadoItem", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function Comp_Item_TodoElItemAQueProveedor() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Private Function TodoElItemAQueProveedor() As String"
sCod = sCod & vbLf & "Dim sCodProve As String"
sCod = sCod & vbLf & "Dim bCondicion As Boolean"
sCod = sCod & vbLf & "Dim sCeldaProve As String"
sCod = sCod & vbLf & "Dim sCeldaCant As String"
sCod = sCod & vbLf & "Dim sCeldaPrimerProve As String"
sCod = sCod & vbLf & "Dim xlsheet As Worksheet"
sCod = sCod & vbLf & "Dim iFilaProve As Integer"
sCod = sCod & vbLf & "Dim vAux As Variant"
sCod = sCod & vbLf & "Dim sAux As String"
sCod = sCod & vbLf & "    Set xlsheet = Excel.Sheets.Item(4)"
sCod = sCod & vbLf & "    sCeldaPrimerProve = """
sCod = sCod & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "    sCeldaProve = ""BB"" & iFilaProve"
sCod = sCod & vbLf & "    bCondicion = True"
sCod = sCod & vbLf & "    While xlsheet.Range(sCeldaProve).Value <> """" And bCondicion"
sCod = sCod & vbLf & "        sCeldaCant = ""D"" & iFilaProve 'Cantidad adjudicada"
sCod = sCod & vbLf & "        If xlsheet.Range(sCeldaCant).Value <> """" And xlsheet.Range(sCeldaCant).Value <> ""0"" Then"
sCod = sCod & vbLf & "            If sCeldaPrimerProve <> """" And sCeldaPrimerProve <> sCeldaProve And sCeldaProve <> """" Then"
sCod = sCod & vbLf & "                bCondicion = False"
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "            sCeldaPrimerProve = sCeldaProve"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        iFilaProve = iFilaProve + 1"
sCod = sCod & vbLf & "        sCeldaProve = ""BB"" & iFilaProve"
sCod = sCod & vbLf & "    Wend"
sCod = sCod & vbLf & "    If Not bCondicion Then"
sCod = sCod & vbLf & "        g_sCodProveItem = ""-*@@@@-*"""
sCod = sCod & vbLf & "        TodoElItemAQueProveedor = ""-*@@@@-*"""
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        If sCeldaPrimerProve <> """" Then"
sCod = sCod & vbLf & "            vAux = Split97(xlsheet.Range(sCeldaPrimerProve).Value,"" - "")"
sCod = sCod & vbLf & "            sAux = vAux(0)"
sCod = sCod & vbLf & "            g_sCodProveItem = sAux"
sCod = sCod & vbLf & "            TodoElItemAQueProveedor = sAux"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            g_sCodProveItem = """""
sCod = sCod & vbLf & "            TodoElItemAQueProveedor = """""
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "End Function"
Comp_Item_TodoElItemAQueProveedor = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_TodoElItemAQueProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_AplicarAtributosItemProveedor() As String
Dim sCodigo As String
Dim i As Integer
Dim arAtribs As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arAtribs = g_oOrigen.g_oOrigen.FormulaDeAtributosDeItemProveedor
    
    sCodigo = "Function AplicarAtributosItem_Proveedor(byval dImporte as double,byval sCodProve as string, optional byval iFila as integer) As Double"
    sCodigo = sCodigo & vbLf & "Dim dValorAtrib As Variant"
    sCodigo = sCodigo & vbLf & "Dim sTipoOperacion As String"
    sCodigo = sCodigo & vbLf & "Dim sComent As String"
    sCodigo = sCodigo & vbLf & "Dim sComentario As String"
    sCodigo = sCodigo & vbLf & ""
    If IsArray(arAtribs) Then
        For i = 0 To UBound(arAtribs, 2)
            sCodigo = sCodigo & vbLf & "'" & arAtribs(1, i)
            sCodigo = sCodigo & vbLf & "dValorAtrib = ObtenerValorAtrib(""" & arAtribs(1, i) & """, sCodProve, sTipoOperacion, sComentario, " & arAtribs(3, i) & ")"
            sCodigo = sCodigo & vbLf & "dImporte = AplicarValorAtrib(dValorAtrib, dImporte, sTipoOperacion)"
            sCodigo = sCodigo & vbLf & "If sComentario <> """" then"
            sCodigo = sCodigo & vbLf & "    sComent = sComent & """ & arAtribs(1, i) & ": Importe "" & sTipoOperacion & "" "" & CStr(CDbl(dValorAtrib)) & vbLf"
            sCodigo = sCodigo & vbLf & "End If "
        
        Next
    End If
    sCodigo = sCodigo & vbLf & "If sComent <> """" then"
    sCodigo = sCodigo & vbLf & "    If Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 11).Comment Is Nothing Then"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 11).AddComment sComent"
    sCodigo = sCodigo & vbLf & "    Else"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 11).Comment.Text sComent"
    sCodigo = sCodigo & vbLf & "    End If"
    sCodigo = sCodigo & vbLf & "    If iFila <> 0 Then"
    sCodigo = sCodigo & vbLf & "       If Sheets.Item(4).Range(""F"" & iFila).Comment Is Nothing Then"
    sCodigo = sCodigo & vbLf & "           Sheets.Item(4).Range(""F"" & iFila).AddComment  sComent"
    sCodigo = sCodigo & vbLf & "       Else"
    sCodigo = sCodigo & vbLf & "           Sheets.Item(4).Range(""F"" & iFila).Comment.Text sComent"
    sCodigo = sCodigo & vbLf & "       End If"
    sCodigo = sCodigo & vbLf & "    End If"
    sCodigo = sCodigo & vbLf & "End If"
    sCodigo = sCodigo & vbLf & "AplicarAtributosItem_Proveedor= dImporte"
    sCodigo = sCodigo & vbLf & "End Function"

Comp_Item_AplicarAtributosItemProveedor = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_AplicarAtributosItemProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_AdjudicadoGrupo() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Private Function AdjudicadoGrupo(Optional iFilaUnProve As Integer = 0) As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "Dim dValorAnteriorItemConAtrib As Double"
sCod = sCod & vbLf & "Dim dValorAnteriorGrupoSinAtrib As Double"
sCod = sCod & vbLf & "Dim sCodProve As string"
sCod = sCod & vbLf & "Dim dValorAnteriorGrupoConAtrib As Double"
sCod = sCod & vbLf & "Dim xlsheet As Worksheet"
sCod = sCod & vbLf & "Dim iFilaProve As Integer"
sCod = sCod & vbLf & "Dim sLetraProve As String"
sCod = sCod & vbLf & "Dim sCeldaProve As String"
sCod = sCod & vbLf & "Dim iFila As Integer"
sCod = sCod & vbLf & "Dim bTodosLosProve As Boolean"
sCod = sCod & vbLf & "Dim dValorAnteriorProcSinAtrib As Double"
sCod = sCod & vbLf & "Dim dValorRestoGruposConAtrib As Double"
sCod = sCod & vbLf & "Dim dValorRestoGrupoProvesConAtrib As Double"
sCod = sCod & vbLf & "Dim dValorAnteriorGrupoProveSinAtrib As Double"
sCod = sCod & vbLf & "Dim dValorAnteriorGrupoProveConAtrib As Double"
sCod = sCod & vbLf & "Dim i As Integer"
sCod = sCod & vbLf & "If IsEmpty(g_var_ValorAnteriorItemConAtrib) Then"
sCod = sCod & vbLf & "    dValorAnteriorItemConAtrib = VALORItemConAtrib"
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    dValorAnteriorItemConAtrib = g_var_ValorAnteriorItemConAtrib"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "If IsEmpty(g_var_ValorAnteriorGrupoSinAtrib) Then"
sCod = sCod & vbLf & "    dValorAnteriorGrupoSinAtrib = VALORGrupoSinAtrib"
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    dValorAnteriorGrupoSinAtrib = g_var_ValorAnteriorGrupoSinAtrib"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "If IsEmpty(g_var_ValorAnteriorGrupoConAtrib) Then"
sCod = sCod & vbLf & "    dValorAnteriorGrupoConAtrib = VALORGrupoConAtrib"
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    dValorAnteriorGrupoConAtrib = g_var_ValorAnteriorGrupoConAtrib"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "dValorAnteriorProcSinAtrib = VALORProcesoSinAtrib"
sCod = sCod & vbLf & "Set xlsheet = Excel.Sheets.Item(4)"
sCod = sCod & vbLf & "If iFilaUnProve <> 0 Then"
sCod = sCod & vbLf & "    iFilaProve = iFilaUnProve"
sCod = sCod & vbLf & "    i = iFilaUnProve - (" & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14)"
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "    i = 0"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "sLetraProve = ""BB"""
sCod = sCod & vbLf & "sCeldaProve = sLetraProve & iFilaProve"
sCod = sCod & vbLf & "bTodosLosProve = True"
sCod = sCod & vbLf & "dValorRestoGruposConAtrib = dValorAnteriorProcSinAtrib - dValorAnteriorGrupoConAtrib"
sCod = sCod & vbLf & "While xlsheet.Range(sCeldaProve).Value <> """" And bTodosLosProve"
sCod = sCod & vbLf & "    If xlsheet.Range(IncrementarColumna(sLetraProve, 2) & iFilaProve).Value <> """" Then"
sCod = sCod & vbLf & "        If IsEmpty(g_var_ValorAnteriorItemProveConAtrib(i, 2)) Then"
sCod = sCod & vbLf & "            dValorRestoGrupoProvesConAtrib = dValorAnteriorGrupoConAtrib - xlsheet.Range(IncrementarColumna(sLetraProve, 2) & iFilaProve).Value"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            dValorRestoGrupoProvesConAtrib = dValorAnteriorGrupoConAtrib - g_var_ValorAnteriorItemProveConAtrib(i, 2)"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        If IsEmpty(g_var_ValorAnteriorItemProveConAtrib(i, 1)) Then"
sCod = sCod & vbLf & "            dValorAnteriorGrupoProveSinAtrib = xlsheet.Range(IncrementarColumna(sLetraProve, 1) & iFilaProve).Value - g_var_ValorAnteriorItemProveConAtrib(i, 0) + CalcularImporteAdjudicadoItem(iFilaProve)"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            dValorAnteriorGrupoProveSinAtrib = g_var_ValorAnteriorItemProveConAtrib(i, 1) - g_var_ValorAnteriorItemProveConAtrib(i, 0) + CalcularImporteAdjudicadoItem(iFilaProve)"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        g_var_ValorAnteriorItemProveConAtrib(i, 1) = dValorAnteriorGrupoProveSinAtrib"
sCod = sCod & vbLf & "        sCodProve = ObtenerProveedorItem(iFilaProve)"
sCod = sCod & vbLf & "        dValorAnteriorGrupoProveConAtrib = AplicarAtributosGrupo(dValorAnteriorGrupoProveSinAtrib, sCodProve)"
sCod = sCod & vbLf & "        g_var_ValorAnteriorItemProveConAtrib(i, 2) = dValorAnteriorGrupoProveConAtrib"
sCod = sCod & vbLf & "        dImporte = dImporte + dValorAnteriorGrupoProveConAtrib"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "    iFilaProve = iFilaProve + 1"
sCod = sCod & vbLf & "    i = i + 1"
sCod = sCod & vbLf & "    sCeldaProve = sLetraProve & iFilaProve"
sCod = sCod & vbLf & "    If iFilaUnProve <> 0 Then bTodosLosProve = False"
sCod = sCod & vbLf & "Wend"
sCod = sCod & vbLf & "If iFilaUnProve = 0 Then"
sCod = sCod & vbLf & "   g_var_ValorAnteriorGrupoConAtrib = dImporte"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "AdjudicadoGrupo = dImporte"
sCod = sCod & vbLf & "End Function"

Comp_Item_AdjudicadoGrupo = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_AdjudicadoGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function CalcularAdjudicadoGrupoSinAtribDeGrupo() As Double
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim dImporte As Double
Dim iFilaItem As Integer
Dim oProve As CProveedor
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGrupo = g_oOrigen.g_ogrupo

For Each oItem In oGrupo.Items
    'Reservo la primera para el item en cuesti�n
    iFilaItem = 2
    For Each oProve In g_oOrigen.m_oProvesAsig
        sCod = oItem.Id & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
            dImporte = dImporte + oGrupo.Adjudicaciones.Item(sCod).ImporteAdjTot / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).Cambio
        End If
    Next
Next
CalcularAdjudicadoGrupoSinAtribDeGrupo = dImporte
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "CalcularAdjudicadoGrupoSinAtribDeGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function CalcularAdjudicadoItemConAtrib() As Double
Dim oGrupo As CGrupo
Dim oItem As CItem
Dim dImporte As Double
Dim iFilaItem As Integer
Dim oProve As CProveedor
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGrupo = g_oOrigen.g_ogrupo
Set oItem = g_oOrigen.g_oItemSeleccionado
'Reservo la primera para el item en cuesti�n
iFilaItem = 2

dImporte = 0

For Each oProve In g_oOrigen.m_oProvesAsig
    sCod = oItem.Id & oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
    If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
        If oGrupo.Adjudicaciones.Item(sCod).ImporteAdjTot <> 0 Then
            dImporte = dImporte + oGrupo.Adjudicaciones.Item(sCod).ImporteAdjTot / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProve.Cod).Cambio
        End If
    End If
Next

CalcularAdjudicadoItemConAtrib = dImporte
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "CalcularAdjudicadoItemConAtrib", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function Comp_Item_AplicarAtributos_Grupo() As String
Dim sCodigo As String
Dim i As Integer
Dim arAtribs As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arAtribs = g_oOrigen.g_oOrigen.FormulaDeAtributosDeGrupoProveedor
    
    sCodigo = "Private Function AplicarAtributosGrupo(ByVal dImporte As Double, ByVal sCodProve As String) As Double"
    sCodigo = sCodigo & vbLf & "Dim dValorAtrib As Variant"
    sCodigo = sCodigo & vbLf & "Dim sTipoOperacion As String"
    sCodigo = sCodigo & vbLf & "Dim sComent As String"
    sCodigo = sCodigo & vbLf & "Dim sComentario As String"
    sCodigo = sCodigo & vbLf & ""
    If IsArray(arAtribs) Then
        For i = 0 To UBound(arAtribs, 2)
            sCodigo = sCodigo & vbLf & "'" & arAtribs(1, i)
            sCodigo = sCodigo & vbLf & "dValorAtrib = ObtenerValorAtrib(""" & arAtribs(1, i) & """, sCodProve, sTipoOperacion, sComentario," & arAtribs(3, i) & ")"
            sCodigo = sCodigo & vbLf & "dImporte = AplicarValorAtrib(dValorAtrib, dImporte, sTipoOperacion)"
            sCodigo = sCodigo & vbLf & "If sComentario <> """" then"
            sCodigo = sCodigo & vbLf & "    sComent = sComent & """ & arAtribs(1, i) & ": Importe "" & sTipoOperacion & "" "" & CStr(CDbl(dValorAtrib)) & vbLf"
            sCodigo = sCodigo & vbLf & "End If "
        Next
    End If
    sCodigo = sCodigo & vbLf & "If sComent <> """" then"
    sCodigo = sCodigo & vbLf & "    If Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 10).Comment Is Nothing Then"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 10).AddComment sComent"
    sCodigo = sCodigo & vbLf & "    Else"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 10).Comment.Text sComent"
    sCodigo = sCodigo & vbLf & "    End If"
    sCodigo = sCodigo & vbLf & "End If"
    sCodigo = sCodigo & vbLf & "AplicarAtributosGrupo= dImporte"
    sCodigo = sCodigo & vbLf & "End Function"
    
Comp_Item_AplicarAtributos_Grupo = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_AplicarAtributos_Grupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_TodoElGrupoAQueProveedor() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Private Function TodoElGrupoAQueProveedor() As String"
sCod = sCod & vbLf & "Dim sCodProve As String"
sCod = sCod & vbLf & "Dim iFila As Integer"
sCod = sCod & vbLf & "Dim bCondicion As Boolean"
sCod = sCod & vbLf & "    bCondicion = True"
sCod = sCod & vbLf & "    iFila = 2"
sCod = sCod & vbLf & "    While Sheets.Item(4).Range(""DF"" & iFila).Value <> """" And bCondicion"
sCod = sCod & vbLf & "        If Sheets.Item(4).Range(""DG"" & iFila).Value <> """" And Sheets.Item(4).Range(""DG"" & iFila).Value <> ""-*@@@@-*"" Then"
sCod = sCod & vbLf & "            If sCodProve <> """" Then"
sCod = sCod & vbLf & "                If sCodProve <> Sheets.Item(4).Range(""DG"" & iFila).Value Then"
sCod = sCod & vbLf & "                    sCodProve = ""-*@@@@-*"""
sCod = sCod & vbLf & "                    bCondicion = False"
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                sCodProve = Sheets.Item(4).Range(""DG"" & iFila).Value"
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        iFila = iFila + 1"
sCod = sCod & vbLf & "    Wend"
sCod = sCod & vbLf & "    'Ahora con el primer item"
sCod = sCod & vbLf & "        If sCodProve <> """" Then"
sCod = sCod & vbLf & "            If sCodProve <> g_sCodProveItem Then"
sCod = sCod & vbLf & "                sCodProve = ""-*@@@@-*"""
sCod = sCod & vbLf & "                bCondicion = False"
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            sCodProve = g_sCodProveItem"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "g_varCodProveGrupo = sCodProve"
sCod = sCod & vbLf & "TodoElGrupoAQueProveedor = sCodProve"
sCod = sCod & vbLf & "End Function"

Comp_Item_TodoElGrupoAQueProveedor = sCod

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_TodoElGrupoAQueProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_Variables() As String
Dim sCod As String
Dim dTemp As Double
Dim INumProve As Integer
Dim oProve As CProveedor

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Option Explicit"
sCod = sCod & vbLf & "Public g_sCodProveItem As String"
sCod = sCod & vbLf & "Public g_varCodProveGrupo As variant"
sCod = sCod & vbLf & "Public g_varCodProveProceso As variant"
sCod = sCod & vbLf & "Public g_var_ValorAnteriorItemConAtrib As Variant"
sCod = sCod & vbLf & "Public g_var_ValorAnteriorGrupoSinAtrib As Variant"
sCod = sCod & vbLf & "Public g_var_ValorAnteriorGrupoConAtrib As Variant"
sCod = sCod & vbLf & "Public g_var_ValorAnteriorProcesoSinAtrib As Variant"
sCod = sCod & vbLf & "Public g_var_PRESAnteriorGrupo As Variant"
sCod = sCod & vbLf & "Public g_var_PRESAnteriorItem As Variant"
sCod = sCod & vbLf & "Public g_var_PRESAnteriorProceso As Variant"
INumProve = 0
For Each oProve In g_oOrigen.m_oProvesAsig
    INumProve = INumProve + 1
Next
sCod = sCod & vbLf & "Public g_var_ValorAnteriorItemProveConAtrib(0 To " & INumProve - 1 & ", 0 To 2) As Variant"

dTemp = 0
dTemp = CalcularAdjudicadoGrupoSinAtribDeGrupo

sCod = sCod & vbLf & "Public Const VALORGrupoSinAtrib = " & FSGSLibrary.ConvertirParaUsarEnMacro(dTemp)
dTemp = 0
dTemp = CalcularAdjudicadoItemConAtrib
sCod = sCod & vbLf & "Public Const VALORItemConAtrib = " & FSGSLibrary.ConvertirParaUsarEnMacro(dTemp)
sCod = sCod & vbLf & "Public Const VALORProcesoSinAtrib = " & FSGSLibrary.ConvertirParaUsarEnMacro(CalcularAdjudicadoProcesoSinAtribDeProceso)
sCod = sCod & vbLf & "Public Const VALORProcesoConAtrib = " & FSGSLibrary.ConvertirParaUsarEnMacro(g_oOrigen.sdbgResultados.Columns("Adjudicado").CellValue(g_oOrigen.sdbgResultados.RowBookmark(0)))
sCod = sCod & vbLf & "Public Const VALORGrupoConAtrib = " & FSGSLibrary.ConvertirParaUsarEnMacro(g_oOrigen.sdbgResultados.Columns("Adjudicado").CellValue(g_oOrigen.sdbgResultados.RowBookmark(1)))
sCod = sCod & vbLf & "Public Const PRESAnteriorGrupo = " & FSGSLibrary.ConvertirParaUsarEnMacro(g_oOrigen.sdbgResultados.Columns("Consumido").CellValue(g_oOrigen.sdbgResultados.RowBookmark(1)))
sCod = sCod & vbLf & "Public Const PRESAnteriorProceso = " & FSGSLibrary.ConvertirParaUsarEnMacro(g_oOrigen.sdbgResultados.Columns("Consumido").CellValue(g_oOrigen.sdbgResultados.RowBookmark(0)))
sCod = sCod & vbLf & "Public Const PRESAnteriorItem = " & FSGSLibrary.ConvertirParaUsarEnMacro(g_oOrigen.sdbgResultados.Columns("Consumido").CellValue(g_oOrigen.sdbgResultados.RowBookmark(2)))

Comp_Item_Variables = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_Variables", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_AdjudicadoProceso() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Private Function AdjudicadoProceso() As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "Dim dValorAnteriorGrupoConAtrib As Double"
sCod = sCod & vbLf & "Dim dValorAnteriorProcesoSinAtrib As Double"
sCod = sCod & vbLf & "Dim dValorAnteriorProcesoConAtrib As Double"
sCod = sCod & vbLf & "Dim sCodProve As String"
sCod = sCod & vbLf & "Dim xlsheet As Worksheet"
sCod = sCod & vbLf & "Dim iFilaProve As Integer"
sCod = sCod & vbLf & "Dim sCeldaProve As String"
sCod = sCod & vbLf & "Dim sLetraGrupo As String"
sCod = sCod & vbLf & "Dim dImporteAux As Double"
sCod = sCod & vbLf & "Set xlsheet = Excel.Sheets.Item(4)"
sCod = sCod & vbLf & "dImporte = 0"
sCod = sCod & vbLf & "iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "sCeldaProve = ""A"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 14"
sCod = sCod & vbLf & "While xlsheet.Range(sCeldaProve).Value <> """
sCod = sCod & vbLf & "    sCodProve = ObtenerProveedorItem(iFilaProve)"
sCod = sCod & vbLf & "    dImporteAux = 0"
sCod = sCod & vbLf & "    dImporteAux = AdjudicadoGrupo(iFilaProve)"
sCod = sCod & vbLf & "    sLetraGrupo = ""BF"""
sCod = sCod & vbLf & "    While xlsheet.Range(sLetraGrupo & iFilaProve).Value <> """
sCod = sCod & vbLf & "        dImporteAux = dImporteAux + xlsheet.Range(sLetraGrupo & iFilaProve).Value"
sCod = sCod & vbLf & "        sLetraGrupo = IncrementarColumna(sLetraGrupo, 2)"
sCod = sCod & vbLf & "    Wend"
sCod = sCod & vbLf & "    dImporteAux = AplicarAtributosProceso(dImporteAux, sCodProve)"
sCod = sCod & vbLf & "    dImporte = dImporte + dImporteAux"
sCod = sCod & vbLf & "    iFilaProve = iFilaProve + 1"
sCod = sCod & vbLf & "    sCeldaProve = ""A"" & iFilaProve"
sCod = sCod & vbLf & "Wend"
sCod = sCod & vbLf & "AdjudicadoProceso = dImporte"
sCod = sCod & vbLf & "End Function"

Comp_Item_AdjudicadoProceso = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_AdjudicadoProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function Comp_Item_PresupuestoConsumidoProceso() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Private Function PresupuestoConsumidoProceso() As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "If IsEmpty(g_var_PresAnteriorGrupo) Then"
sCod = sCod & vbLf & "    g_var_PresAnteriorGrupo = PRESAnteriorGrupo"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "If IsEmpty(g_var_PresAnteriorProceso) Then"
sCod = sCod & vbLf & "    g_var_PresAnteriorProceso = PRESAnteriorProceso"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "dImporte = PresupuestoConsumidoGrupo()"
sCod = sCod & vbLf & "PresupuestoConsumidoProceso = g_var_PresAnteriorProceso - PRESAnteriorGrupo + dImporte"
sCod = sCod & vbLf & "End Function"

Comp_Item_PresupuestoConsumidoProceso = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_PresupuestoConsumidoProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_AplicarAtributos_Proceso() As String
Dim sCodigo As String
Dim i As Integer
Dim arAtribs As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arAtribs = g_oOrigen.g_oOrigen.FormulaDeAtributosDeProcesoProveedor
    
    sCodigo = "Private Function AplicarAtributosProceso(ByVal dImporte As Double, ByVal sCodProve As String) As Double"
    sCodigo = sCodigo & vbLf & "Dim dValorAtrib As Variant"
    sCodigo = sCodigo & vbLf & "Dim sTipoOperacion As String"
    sCodigo = sCodigo & vbLf & "Dim sComent As String"
    sCodigo = sCodigo & vbLf & "Dim sComentario As String"
    sCodigo = sCodigo & vbLf & ""
    If IsArray(arAtribs) Then
        For i = 0 To UBound(arAtribs, 2)
            sCodigo = sCodigo & vbLf & "'" & arAtribs(1, i)
            sCodigo = sCodigo & vbLf & "dValorAtrib = ObtenerValorAtrib(""" & arAtribs(1, i) & """, sCodProve, sTipoOperacion, sComentario," & arAtribs(3, i) & ")"
            sCodigo = sCodigo & vbLf & "dImporte = AplicarValorAtrib(dValorAtrib, dImporte, sTipoOperacion)"
            sCodigo = sCodigo & vbLf & "If sComentario <> """" then"
            sCodigo = sCodigo & vbLf & "    sComent = sComent & """ & arAtribs(1, i) & ": Importe "" & sTipoOperacion & "" "" & CStr(CDbl(dValorAtrib)) & vbLf"
            sCodigo = sCodigo & vbLf & "End If "
        Next
    End If
    sCodigo = sCodigo & vbLf & "If sComent <> """" then"
    sCodigo = sCodigo & vbLf & "    If Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 9).Comment Is Nothing Then"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 9).AddComment sComent"
    sCodigo = sCodigo & vbLf & "    Else"
    sCodigo = sCodigo & vbLf & "        Sheets.Item(4).Range(""E"" & " & basPublic.gParametrosInstalacion.giLongCabCompItem & " + 9).Comment.Text sComent"
    sCodigo = sCodigo & vbLf & "    End If"
    sCodigo = sCodigo & vbLf & "End If"
    sCodigo = sCodigo & vbLf & "AplicarAtributosProceso= dImporte"
    sCodigo = sCodigo & vbLf & "End Function"

Comp_Item_AplicarAtributos_Proceso = sCodigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_AplicarAtributos_Proceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_TodoElProcesoAQueProveedor() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Private Function TodoElProcesoAQueProveedor() As String"
sCod = sCod & vbLf & "Dim sCodProve As String"
sCod = sCod & vbLf & "Dim sCodProveGrupo As String"
sCod = sCod & vbLf & "Dim iFila As Integer"
sCod = sCod & vbLf & "Dim bCondicion As Boolean"
sCod = sCod & vbLf & "bCondicion = True"
sCod = sCod & vbLf & "iFila = 2"
sCod = sCod & vbLf & "sCodProveGrupo = TodoElGrupoAQueProveedor()"
sCod = sCod & vbLf & "If sCodProveGrupo = ""-*@@@@-*"" Then"
sCod = sCod & vbLf & "    TodoElProcesoAQueProveedor = ""-*@@@@-*"""
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    'Buscar en el resto de grupos"
sCod = sCod & vbLf & "    bCondicion = True"
sCod = sCod & vbLf & "    sCodProve = """""
sCod = sCod & vbLf & "    While Sheets.Item(4).Range(""DD"" & iFila).Value <> """" And bCondicion"
sCod = sCod & vbLf & "        If Sheets.Item(4).Range(""DE"" & iFila).Value <> """" And Sheets.Item(4).Range(""DE"" & iFila).Value <> ""-*@@@@-*"" Then"
sCod = sCod & vbLf & "            If sCodProve <> """" Then"
sCod = sCod & vbLf & "                If sCodProve <> Sheets.Item(4).Range(""DE"" & iFila).Value Then"
sCod = sCod & vbLf & "                sCodProve = ""-*@@@@-*"""
sCod = sCod & vbLf & "                bCondicion = False"
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                sCodProve = Sheets.Item(4).Range(""DE"" & iFila).Value"
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "        iFila = iFila + 1"
sCod = sCod & vbLf & "    Wend"
sCod = sCod & vbLf & "    If sCodProveGrupo <> """" Then"
sCod = sCod & vbLf & "        If sCodProveGrupo = sCodProve Then"
sCod = sCod & vbLf & "            g_varCodProveProceso = sCodProve"
sCod = sCod & vbLf & "            TodoElProcesoAQueProveedor = sCodProve"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "            g_varCodProveProceso = ""-*@@@@*-"""
sCod = sCod & vbLf & "            TodoElProcesoAQueProveedor = ""-*@@@@*-"""
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        g_varCodProveProceso = sCodProve"
sCod = sCod & vbLf & "        TodoElProcesoAQueProveedor = sCodProve"
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "End Function"

Comp_Item_TodoElProcesoAQueProveedor = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_TodoElProcesoAQueProveedor", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function CalcularAdjudicadoProcesoSinAtribDeProceso() As Double
Dim oProv As CProveedor
Dim dImporte As Double
Dim oOferta As COferta
Dim sCod As String
Dim oGrupo As CGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
dImporte = 0

For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    For Each oProv In g_oOrigen.m_oProvesAsig
        If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod) Is Nothing Then
            Set oOferta = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod)
            sCod = oGrupo.Codigo & oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
            If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
                dImporte = dImporte + g_oOrigen.m_oAdjs.Item(sCod).Adjudicado / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(oProv.Cod).Cambio
            End If
        End If
    Next
Next

CalcularAdjudicadoProcesoSinAtribDeProceso = dImporte
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "CalcularAdjudicadoProcesoSinAtribDeProceso", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function Comp_Item_PresupuestoConsumidoGrupo() As String
Dim sCod As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCod = "Private Function PresupuestoConsumidoGrupo() As Double"
sCod = sCod & vbLf & "Dim dImporte As Double"
sCod = sCod & vbLf & "Dim dPresAnteriorItem As Double"
sCod = sCod & vbLf & "Dim dPresAnteriorGrupo As Double"
sCod = sCod & vbLf & "If IsEmpty(g_var_PresAnteriorItem) Then"
sCod = sCod & vbLf & "    g_var_PresAnteriorItem = PRESAnteriorItem"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "If IsEmpty(g_var_PresAnteriorGrupo) Then"
sCod = sCod & vbLf & "    g_var_PresAnteriorGrupo = PRESAnteriorGrupo"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "dImporte = PresupuestoConsumidoItem()"
sCod = sCod & vbLf & "PresupuestoConsumidoGrupo = g_var_PresAnteriorGrupo - PRESAnteriorItem + dImporte"
sCod = sCod & vbLf & "End Function"

Comp_Item_PresupuestoConsumidoGrupo = sCod
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "Comp_Item_PresupuestoConsumidoGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>Sacar en formato word la hoja de adjudicaciones. Items cerrados deben de salir. Llamada desde frmAdj.</summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo:2 (el word es muy lento)</remarks>
''' <revision>LTG 21/11/2011</revision>
Private Sub ObtenerHojaAdjudicacion()
    Dim dPresGrupo As Double
    Dim dAhorradoGrupo As Double
    Dim dAdjGrupo As Double
    Dim sComentario As String
    Dim appword As Object
    Dim docword As Object
    Dim blankword As Object
    Dim rangeword As Object
    Dim rangeword5 As Object
    Dim rangeword6 As Object
    Dim oProce As cProceso
    Dim oProve As CProveedor
    Dim oProves As CProveedores
    Dim oAdjs As CAdjudicaciones
    Dim oIasig As IAsignaciones
    Dim oIAdjudicaciones As IAdjudicaciones
    Dim dVolProceAdj As Double
    Dim dVolProveAdj As Double
    Dim dVolProcePres As Double
    Dim dVolProvePres As Double
    Dim oFirmas As CFirmas
    Dim oFirma As CFirma
    Dim INumProve As Integer
    Dim iNumProveTotal As Integer
    Dim sFirma As String
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim Destino As Boolean
    Dim Pago As Boolean
    Dim Fecha As Boolean
    Dim sCod As String
    Dim cont As Integer
    Dim bMostrarGr As Boolean
    Dim bError As Boolean
    Dim bVisibleWord As Boolean
    Dim sVersion As String
    Dim bSaleItem As Boolean
    Dim oEsc As CEscalado
    Dim dblImporteAux As Double
    Dim oAdj As CAdjudicacion
    'Atributos item tarea 2707
    Dim bm As Object
    Dim CountAtrib As Integer
    Dim i As Integer
    Dim bEsta As Boolean
    Dim strAtrib As String
    Dim MarcadoresAtribItem As String
    Dim MostrarAtribItem As String
    Dim IdsAtribItem As String
    Dim ListaAtribId As Long
    Dim oAtribCod As CAtributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error GoTo ERROR_Frm

    'Comprobar las plantillas
    If Right(txtDetProceDot, 3) <> "dot" Then
        oMensajes.NoValida sIdiPlantilla
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
    
    If Not oFos.FileExists(txtDetProceDot) Then
        oMensajes.PlantillaNoEncontrada txtDetProceDot
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
    Me.Hide

    frmESPERA.lblGeneral.caption = sIdiGenHojaAdj
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.lblDetalle = sIdiAbrWord
    frmESPERA.Show
    
    'Crear aplicaci�n word
    Set appword = CreateObject("Word.Application")
    bVisibleWord = appword.Visible
    appword.WindowState = 2
    sVersion = appword.VERSION
    If InStr(1, sVersion, ".") > 1 Then
        sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
        m_iWordVer = CInt(sVersion)
    Else
        m_iWordVer = 9
    End If

    Set oProce = g_oOrigen.m_oProcesoSeleccionado
    
    frmESPERA.ProgressBar1.Value = 2
    frmESPERA.lblDetalle = sIdiAgrPlant & " " & txtDetProceDot.Text

    'Crear documento general
    Set docword = appword.Documents.Add(txtDetProceDot.Text)
    'CREAR NUEVO DOCUMENTO A PARTIR DE LA PLANTILLA
    Set blankword = appword.Documents.Add(txtDetProceDot.Text)
    appword.Visible = True
    If m_iWordVer >= 9 Then
        If appword.Visible Then docword.activewindow.WindowState = 2
        If appword.Visible Then blankword.activewindow.WindowState = 2
    Else
        appword.Windows(docword.Name).WindowState = 1
        appword.Windows(blankword.Name).WindowState = 1
    End If

    frmESPERA.lblContacto = sIdiProceso & " " & CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod) & " " & oProce.Den
    With blankword
        'guardar los bookmarks de la plantilla
        CountAtrib = -1
        For Each bm In .Bookmarks
            strAtrib = Left(bm.Name, 6)
            If strAtrib = "ATRIB_" Then
                CountAtrib = CountAtrib + 1
                ReDim Preserve ListaAtribBmk(CountAtrib)
                ListaAtribBmk(CountAtrib) = Right(bm.Name, Len(bm.Name) - 6)
            End If
        Next bm
    
        If .Bookmarks.Exists("FECHA") Then
        DatoAWord blankword, "FECHA", Format(Date, "Short Date")
        End If
        If .Bookmarks.Exists("COD_PROCESO") Then
        DatoAWord blankword, "COD_PROCESO", CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
        End If
        If .Bookmarks.Exists("DEN_PROCESO") Then
        DatoAWord blankword, "DEN_PROCESO", oProce.Den
        End If
        If .Bookmarks.Exists("MAT1_PROCESO") Then
        DatoAWord blankword, "MAT1_PROCESO", oProce.GMN1Cod
        End If
        If .Bookmarks.Exists("MONEDA_PROCESO") Then
        DatoAWord blankword, "MONEDA_PROCESO", oProce.MonCod
        End If
        If .Bookmarks.Exists("CAMBIO_PROCESO") Then
        DatoAWord blankword, "CAMBIO_PROCESO", FormateoNumerico(oProce.Cambio, m_sFormatoNumber)
        End If
        If Not oProce.responsable Is Nothing Then
            If oProce.responsable.Cod <> "" Then
                Set oIasig = oProce
                oIasig.DevolverResponsable
                Set oIasig = Nothing
                If .Bookmarks.Exists("COD_RESPONSABLE") Then
                    .Bookmarks("COD_RESPONSABLE").Range.Text = oProce.responsable.Cod
                End If
                If .Bookmarks.Exists("NOM_RESPONSABLE") Then
                    .Bookmarks("NOM_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.nombre)
                End If
                If .Bookmarks.Exists("APE_RESPONSABLE") Then
                    .Bookmarks("APE_RESPONSABLE").Range.Text = oProce.responsable.Apel
                End If
                If .Bookmarks.Exists("DEQP_RESPONSABLE") Then
                    .Bookmarks("DEQP_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.DenEqp)
                End If
                If .Bookmarks.Exists("EQP_RESPONSABLE") Then
                    .Bookmarks("EQP_RESPONSABLE").Range.Text = oProce.responsable.codEqp
                End If
                If .Bookmarks.Exists("TLFNO_RESPONSABLE") Then
                    .Bookmarks("TLFNO_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.Tfno)
                End If
                If .Bookmarks.Exists("MAIL_RESPONSABLE") Then
                    .Bookmarks("MAIL_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.mail)
                End If
                If .Bookmarks.Exists("FAX_RESPONSABLE") Then
                    .Bookmarks("FAX_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.Fax)
                End If
                .Bookmarks("OPC_RESPONSABLE").Delete
            Else
                If docword.Bookmarks.Exists("OPC_RESPONSABLE") Then
                    .Bookmarks("OPC_RESPONSABLE").Range.Delete
                Else
                    .Bookmarks("COD_RESPONSABLE").Range.Text = ""
                    .Bookmarks("NOM_RESPONSABLE").Range.Text = ""
                    .Bookmarks("APE_RESPONSABLE").Range.Text = ""
                    .Bookmarks("DEQP_RESPONSABLE").Range.Text = ""
                    .Bookmarks("EQP_RESPONSABLE").Range.Text = ""
                    .Bookmarks("TLFNO_RESPONSABLE").Range.Text = ""
                    .Bookmarks("MAIL_RESPONSABLE").Range.Text = ""
                    .Bookmarks("FAX_RESPONSABLE").Range.Text = ""
                End If
            End If
        End If
        If oProce.DefDestino = EnProceso Then
            If .Bookmarks.Exists("COD_DEST_PROCE") Then
                DatoAWord blankword, "COD_DEST_PROCE", NullToStr(oProce.DestCod)
            End If
            If .Bookmarks.Exists("DEN_DEST_PROCE") Then
                DatoAWord blankword, "DEN_DEST_PROCE", NullToStr(oDestinos.Item(oProce.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            .Bookmarks("DEST_PROCE").Delete
        Else
            If .Bookmarks.Exists("DEST_PROCE") Then
                .Bookmarks("DEST_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        If oProce.DefFechasSum = EnProceso Then
            If .Bookmarks.Exists("FINI_SUM_PROCE") Then
                DatoAWord blankword, "FINI_SUM_PROCE", NullToStr(oProce.FechaInicioSuministro)
            End If
            If .Bookmarks.Exists("FFIN_SUM_PROCE") Then
                DatoAWord blankword, "FFIN_SUM_PROCE", NullToStr(oProce.FechaFinSuministro)
            End If
            .Bookmarks("FECSUM_PROCE").Delete
        Else
            If .Bookmarks.Exists("FECSUM_PROCE") Then
                .Bookmarks("FECSUM_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        If oProce.DefFormaPago = EnProceso Then
            If .Bookmarks.Exists("COD_PAGO_PROCE") Then
                DatoAWord blankword, "COD_PAGO_PROCE", NullToStr(oProce.PagCod)
            End If
            If .Bookmarks.Exists("DEN_PAGO_PROCE") Then
                DatoAWord blankword, "DEN_PAGO_PROCE", NullToStr(oPagos.Item(oProce.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            .Bookmarks("PAGO_PROCE").Delete
        Else
            If .Bookmarks.Exists("PAGO_PROCE") Then
                .Bookmarks("PAGO_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        If Not IsNull(oProce.ComentADJ) Then
            If .Bookmarks.Exists("ADJ_COMENT") Then
                DatoAWord blankword, "ADJ_COMENT", NullToStr(oProce.ComentADJ)
                .Bookmarks("COMENT_ADJ_PROCE").Delete
            End If
        Else
            If .Bookmarks.Exists("COMENT_ADJ_PROCE") Then
                .Bookmarks("COMENT_ADJ_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        dVolProceAdj = 0
        dVolProcePres = 0
        
        Set oIAdjudicaciones = oProce
        
        If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
            Set oProves = oProce.DevolverHojaAdjudicacionesParcCerrado
        Else
            Set oProves = oIAdjudicaciones.DevolverProveedoresConAdjudicaciones
        End If
        
        'Para cada proveedor
        INumProve = 1
        iNumProveTotal = oProves.Count
        If docword.Bookmarks.Exists("PROVEEDOR") Then
            .Bookmarks("PROVEEDOR").Select
            .Application.Selection.Delete
            Set rangeword = docword.Bookmarks("PROVEEDOR").Range
        End If
        For Each oProve In oProves
            frmESPERA.ProgressBar1.Value = 2
            frmESPERA.ProgressBar2.Value = (INumProve / iNumProveTotal) * 100
            frmESPERA.lblContacto = sIdiProveedor & oProve.Cod & " " & oProve.Den
            frmESPERA.lblDetalle = sIdiGenDatProve
            
            dVolProveAdj = 0
            dVolProvePres = 0
            rangeword.Copy
            .Application.Selection.Paste
            If .Bookmarks.Exists("COD_PROVEEDOR") Then
                .Bookmarks("COD_PROVEEDOR").Range.Text = oProve.Cod
            End If
            If .Bookmarks.Exists("DEN_PROVEEDOR") Then
                .Bookmarks("DEN_PROVEEDOR").Range.Text = NullToStr(oProve.Den)
            End If
            
            'Obtener el detalle de adjudicaci�n para cada proveedor
            'Puede que sea el de una reuni�n o las vigentes
            If docword.Bookmarks.Exists("GRUPO") Then
                If .Bookmarks.Exists("GRUPO") Then
                    .Bookmarks("GRUPO").Select
                    .Application.Selection.Delete
                End If
                Set rangeword5 = docword.Bookmarks("GRUPO").Range
                
                '****************************************
                If oProce.Grupos.Count = 0 Then
                    If .Bookmarks.Exists("GRUPO") Then
                        .Bookmarks("GRUPO").Select
                        .Application.Selection.Delete
                    End If
                Else
                    For Each oGrupo In oProce.Grupos
                      bMostrarGr = True
                      If oProce.AdminPublica = True Then
                        If oProce.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
                            bMostrarGr = False
                        End If
                      End If
                      If bMostrarGr = True Then
                        rangeword5.Copy
                        .Application.Selection.Paste
                        If .Bookmarks.Exists("COD_GRUPO") Then
                            .Bookmarks("COD_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                        End If
                        If .Bookmarks.Exists("DEN_GRUPO") Then
                            .Bookmarks("DEN_GRUPO").Range.Text = NullToStr(oGrupo.Den)
                        End If
                        If oGrupo.DefDestino Then
                            If .Bookmarks.Exists("COD_DEST_GR") Then
                                .Bookmarks("COD_DEST_GR").Range.Text = NullToStr(oGrupo.DestCod)
                            End If
                            If .Bookmarks.Exists("DEN_DEST_GR") Then
                                .Bookmarks("DEN_DEST_GR").Range.Text = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                            End If
                            .Bookmarks("GR_DEST").Delete
                        Else
                            If .Bookmarks.Exists("GR_DEST") Then
                                .Bookmarks("GR_DEST").Select
                                .Application.Selection.cut
                            End If
                        End If
                        
                        If oGrupo.DefFechasSum Then
                            If .Bookmarks.Exists("FINI_SUM_GR") Then
                                .Bookmarks("FINI_SUM_GR").Range.Text = NullToStr(oGrupo.FechaInicioSuministro)
                            End If
                            If .Bookmarks.Exists("FFIN_SUM_GR") Then
                                .Bookmarks("FFIN_SUM_GR").Range.Text = NullToStr(oGrupo.FechaFinSuministro)
                            End If
                            .Bookmarks("GR_FECSUM").Delete
                        Else
                            If .Bookmarks.Exists("GR_FECSUM") Then
                                .Bookmarks("GR_FECSUM").Select
                                .Application.Selection.cut
                            End If
                        End If
                        
                        If oGrupo.DefFormaPago Then
                            If .Bookmarks.Exists("COD_PAGO_GR") Then
                                .Bookmarks("COD_PAGO_GR").Range.Text = NullToStr(oGrupo.PagCod)
                            End If
                            If .Bookmarks.Exists("DEN_PAGO_GR") Then
                                .Bookmarks("DEN_PAGO_GR").Range.Text = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                            End If
                            .Bookmarks("GR_PAGO").Delete
                        Else
                            If .Bookmarks.Exists("GR_PAGO") Then
                                .Bookmarks("GR_PAGO").Select
                                .Application.Selection.cut
                            End If
                        End If
                        
                        '@@ pendiente
                        If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                            DevolverImporteParcialCerrado oProve.Cod, oGrupo.Items, oGrupo.Adjudicaciones, dAdjGrupo, dAhorradoGrupo, dPresGrupo
                        Else
                            dAdjGrupo = DevolverAdjudicadoProveedorGrupo(oProve.Cod, oGrupo.Codigo)
                        End If
                        
                        If .Bookmarks.Exists("VOLADJ_GR") Then
                            sComentario = ""
                            
                            sComentario = g_oOrigen.RPT_FormulaDeAtributosDeGrupoProveedor(oGrupo.Codigo, oProve.Cod)
                            If sComentario <> "" Then
                                .Bookmarks("VOLADJ_GR").Range.Select
                                .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                                .Application.Selection.TypeText Text:=sComentario
                                .Application.activewindow.ActivePane.Close
                            End If
                            
                            .Bookmarks("VOLADJ_GR").Range.Text = FormateoNumerico(dAdjGrupo, m_sFormatoNumberGr)
                            
                        End If
                        
                        If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                        Else
                            dAhorradoGrupo = DevolverAhorradoProveedorGrupo(oProve.Cod, oGrupo.Codigo)
                        End If
                        
                        If .Bookmarks.Exists("VOLAHO_GR") Then
                            .Bookmarks("VOLAHO_GR").Range.Text = FormateoNumerico(dAhorradoGrupo, m_sFormatoNumberGr)
                        End If
                        
                        If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                        Else
                            dPresGrupo = DevolverPresupuestadoProveedorGrupo(oProve.Cod, oGrupo.Codigo)
                        End If
                        
                        If .Bookmarks.Exists("VOLPRES_GR") Then
                            .Bookmarks("VOLPRES_GR").Range.Text = FormateoNumerico(dPresGrupo, m_sFormatoNumberGr)
                        End If
                        
                        If .Bookmarks.Exists("PORCEN_GR") Then
                           If dPresGrupo > 0 Then
                              .Bookmarks("PORCEN_GR").Range.Text = FormateoNumerico((dAhorradoGrupo / dPresGrupo) * 100, m_sFormatoNumberPorcenGr)
                            Else
                               .Bookmarks("PORCEN_GR").Range.Text = "-100"
                            End If
                        End If
                        
                        'Comprobamos los datos configurables del item para la funcion
                        'que compone las tablas
                        
                        If (oProce.DefDestino = EnItem) Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                            Destino = True
                        Else
                            Destino = False
                        End If
                        
                        If oProce.DefFormaPago = EnItem Or (oProce.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
                            Pago = True
                        Else
                            Pago = False
                        End If
                        If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                            Fecha = True
                        Else
                            Fecha = False
                        End If
                        If .Bookmarks.Exists("ESCALADO") Then
                            If oGrupo.UsarEscalados Then
                                .Bookmarks("ESCALADO").Delete
                            Else
                                .Bookmarks("ESCALADO").Select
                                .Application.Selection.cut
                                .Bookmarks("ESCALADO").Delete
                            End If
                        End If
                        
                        If CountAtrib <> -1 Then
                            If UBound(ListaAtribBmk) >= 0 Then
                                oGrupo.CargarAtributos vAnyo:=oProce.Anyo, vGMN1Cod:=oProce.GMN1Cod, vProce:=oProce.Cod, ambito:=AmbItem
                            End If
                            MarcadoresAtribItem = ""
                            MostrarAtribItem = ""
                            For i = LBound(ListaAtribBmk) To UBound(ListaAtribBmk)
                                bEsta = False
                                If MarcadoresAtribItem = "" Then
                                    MarcadoresAtribItem = ListaAtribBmk(i)
                                Else
                                    MarcadoresAtribItem = MarcadoresAtribItem & "##" & ListaAtribBmk(i)
                                End If
                                ListaAtribId = -1
                                For Each oAtribCod In oGrupo.AtributosItem
                                    If oAtribCod.ambito = AmbItem Then
                                        If Replace(ListaAtribBmk(i), "���", " ") = oAtribCod.Cod Then
                                            bEsta = True
                                            If .Bookmarks.Exists("ATRIB_" & ListaAtribBmk(i)) Then .Bookmarks("ATRIB_" & ListaAtribBmk(i)).Range.Text = oAtribCod.Den
                                            ListaAtribId = oAtribCod.idAtribProce
                                            Exit For
                                        End If
                                    End If
                                Next
                                If MostrarAtribItem = "" Then
                                    MostrarAtribItem = IIf(bEsta, 1, 0)
                                    IdsAtribItem = CStr(ListaAtribId)
                                Else
                                    MostrarAtribItem = MostrarAtribItem & "##" & IIf(bEsta, 1, 0)
                                    IdsAtribItem = MostrarAtribItem & "##" & CStr(ListaAtribId)
                                End If
                            Next
                        End If
                        
                        FSGSLibrary.DatosConfigurablesAWord blankword, "IT_DEST", "IT_PAGO", "IT_FECSUM", Destino, Pago, Fecha, , , MarcadoresAtribItem, MostrarAtribItem
                        
                        If blankword.Bookmarks.Exists("ITEM") Then
                                                        
                            Set rangeword6 = blankword.Bookmarks("ITEM").Range
                            .Bookmarks("ITEM").Select
                            .Application.Selection.cut
                            If oGrupo.Items.Count = 0 Then
                            Else
                                cont = 0
                                Set oAdjs = oGrupo.Adjudicaciones
                                
                                For Each oItem In oGrupo.Items
                                    If gParametrosGenerales.gbHojaAdjsAbierto Then
                                        bSaleItem = oItem.Confirmado
                                    Else
                                        If oProce.Estado = ParcialmenteCerrado Then
                                            bSaleItem = oItem.Confirmado And oItem.Cerrado
                                        Else
                                            bSaleItem = oItem.Confirmado
                                        End If
                                    End If
                                    
                                    If bSaleItem Then
                                        If oGrupo.UsarEscalados Then
                                            For Each oEsc In oGrupo.Escalados
                                                sCod = KeyEscalado(oProve.Cod, oItem.Id, oEsc.Id)
                                                
                                                TransferirDatosAdjudicacion blankword, rangeword6, oAdjs, sCod, oGrupo, oItem, oProve, cont, oEsc, MarcadoresAtribItem, MostrarAtribItem, IdsAtribItem
                                            Next
                                        Else
                                            sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                            sCod = CStr(oItem.Id) & sCod
                                            
                                            TransferirDatosAdjudicacion blankword, rangeword6, oAdjs, sCod, oGrupo, oItem, oProve, cont, , MarcadoresAtribItem, MostrarAtribItem, IdsAtribItem
                                        End If
                                    End If
                                Next
                                If .Bookmarks.Exists("ITEM") Then
                                    .Bookmarks("ITEM").Delete
                                End If
                            End If
                        End If
                        
                        If cont = 0 Then
                            If .Bookmarks.Exists("GRUPO") Then
                                .Bookmarks("GRUPO").Select
                                .Application.Selection.cut
                            End If
                        Else
                            If .Bookmarks.Exists("GRUPO") Then
                                .Bookmarks("GRUPO").Delete
                            End If
                        End If
                        
                        dVolProvePres = dVolProvePres + dPresGrupo
                        dVolProveAdj = dVolProveAdj + dAdjGrupo
                      End If
                    Next
                  
                    If .Bookmarks.Exists("GRUPO") Then
                        .Bookmarks("GRUPO").Delete
                    End If
                End If
            End If
            '****************************************
            
            frmESPERA.ProgressBar1.Value = 4
            
            'Pasamos los datos restantes del proveedor
            If .Bookmarks.Exists("VOLADJ_PROVEEDOR") Then
                .Bookmarks("VOLADJ_PROVEEDOR").Range.Text = FormateoNumerico(dVolProveAdj, m_sFormatoNumber)
            End If
            frmESPERA.ProgressBar1.Value = 5
            If .Bookmarks.Exists("VOLAHO_PROVEEDOR") Then
                .Bookmarks("VOLAHO_PROVEEDOR").Range.Text = FormateoNumerico(dVolProvePres - dVolProveAdj, m_sFormatoNumber)
            End If
                
            frmESPERA.ProgressBar1.Value = 6
            If .Bookmarks.Exists("VOLPRES_PROVEEDOR") Then
                .Bookmarks("VOLPRES_PROVEEDOR").Range.Text = FormateoNumerico(dVolProvePres, m_sFormatoNumber)
            End If
            frmESPERA.ProgressBar1.Value = 7
            If .Bookmarks.Exists("PORCEN_PROVEEDOR") Then
                If dVolProvePres = 0 Then
                    If dVolProveAdj <> 0 Then
                        .Bookmarks("PORCEN_PROVEEDOR").Range.Text = "-100"
                    Else
                        .Bookmarks("PORCEN_PROVEEDOR").Range.Text = "0"
                    End If
                Else
                    .Bookmarks("PORCEN_PROVEEDOR").Range.Text = FormateoNumerico(((dVolProvePres - dVolProveAdj) / dVolProvePres) * 100, m_sFormatoNumberPorcen)
                End If
            End If
            frmESPERA.ProgressBar1.Value = 8
            
            dVolProcePres = dVolProcePres + dVolProvePres
            dVolProceAdj = dVolProceAdj + AplicarAtributos(oProce, g_oOrigen.m_oAtribsFormulas, dVolProveAdj, TotalOferta, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE)
            INumProve = INumProve + 1
        Next
        If .Bookmarks.Exists("PROVEEDOR") Then
            .Bookmarks("PROVEEDOR").Delete
        End If
        frmESPERA.ProgressBar1.Value = 10
        
        'Pasamos los datos restantes del proceso
        If .Bookmarks.Exists("VOLPRES_PROCESO") Then
            .Bookmarks("VOLPRES_PROCESO").Range.Text = FormateoNumerico(dVolProcePres, m_sFormatoNumber)
        End If
        
        If .Bookmarks.Exists("VOLADJ_PROCESO") Then
                m_sProceFormula = ""
                For Each oProve In g_oOrigen.m_oProvesAsig
                    If m_sProceFormula = "" Then
                        If g_oOrigen.ComprobarAplicarAtribsProceso(oProve.Cod) = True Then
                            m_sProceFormula = g_oOrigen.RPT_FormulaDeAtributosProcesoProveedor(oProve.Cod)
                        End If
                    Else
                        Exit For
                    End If
                Next
             If m_sProceFormula <> "" Then
                .Bookmarks("VOLADJ_PROCESO").Range.Select
                .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                .Application.Selection.TypeText Text:=m_sProceFormula
                .Application.activewindow.ActivePane.Close
            End If
            
            .Bookmarks("VOLADJ_PROCESO").Range.Text = FormateoNumerico(dVolProceAdj, m_sFormatoNumber)
            
        End If
        If .Bookmarks.Exists("VOLAHO_PROCESO") Then
            .Bookmarks("VOLAHO_PROCESO").Range.Text = FormateoNumerico(dVolProcePres - dVolProceAdj, m_sFormatoNumber)
        End If
        If .Bookmarks.Exists("PORCEN_PROCESO") Then
            If dVolProcePres = 0 Then
                .Bookmarks("PORCEN_PROCESO").Range.Text = 0
            Else
                .Bookmarks("PORCEN_PROCESO").Range.Text = FormateoNumerico(((dVolProcePres - dVolProceAdj) / dVolProcePres) * 100, m_sFormatoNumberPorcen)
            End If
        End If
        
        frmESPERA.ProgressBar1.Value = 6
        frmESPERA.lblContacto = sIdiPasFirmas
        frmESPERA.lblDetalle = ""
        
        'Ahora tengo que poner las firmas
        Set oFirmas = oFSGSRaiz.generar_CFirmas
        oFirmas.cargartodaslasfirmas
        
        If oFirmas.Count > 0 Then
            For Each oFirma In oFirmas
                sFirma = "FIRMA" & oFirma.Cod
                If .Bookmarks.Exists(DblQuote(sFirma)) Then
                    .Bookmarks(DblQuote(sFirma)).Range.Text = oFirma.Den
                End If
                sFirma = ""
            Next
        End If
    End With
    
    ' cerrar documento word
    docword.Close (False)
    appword.Selection.HomeKey Unit:=6
    appword.WindowState = 1
    appword.Visible = True
    BringWindowToTop FindWindow(0&, appword.activewindow.caption & " - Microsoft Word")

    basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompObtenerHojaAdj, "Anyo:" & CStr(g_oOrigen.m_oProcesoSeleccionado.Anyo) & "Gmn1:" & g_oOrigen.m_oProcesoSeleccionado.GMN1Cod & "Proce:" & CStr(g_oOrigen.m_oProcesoSeleccionado.Cod)
    Screen.MousePointer = vbNormal
    
    Unload frmESPERA
    
Finalizar:
    On Error Resume Next
    If bError Then
        docword.Close (False)
        blankword.Close (False)
        If bVisibleWord = False Then
            appword.Quit
        End If
    End If
    Set appword = Nothing
    Set docword = Nothing
    Set blankword = Nothing
    Set rangeword = Nothing
    Set rangeword5 = Nothing
    Set rangeword6 = Nothing
    Set oProce = Nothing
    Set oProve = Nothing
    Set oProves = Nothing
    Set oAdjs = Nothing
    Set oFirmas = Nothing
    Set oFirma = Nothing
    Set oGrupo = Nothing
    Set oItem = Nothing
    Set oAdj = Nothing
    Exit Sub

ERROR_Frm:
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    
    Unload frmESPERA
    
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ObtenerHojaAdjudicacion", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
    
End Sub

''' <summary>Pasa los datos de una adjudicaci�n al doc. word</summary>
''' <param name="blankword">Documento word</param>
''' <param name="rangeword6">Rango en el doc. word</param>
''' <param name="oAdjs">Adjudicaciones</param>
''' <param name="sCod">C�digo de la adjudicaci�n</param>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="oItem">Objeto item</param>
''' <param name="oProve">Objeto proveedor</param>
''' <param name="cont">n� de adjudicaciones que se muestran en el doc.</param>
''' <param name="MarcadoresAtribItem">Marcadores de atributos de item en el documento, separados por ##</param>
''' <param name="MostrarAtribItem">Indica si se muestran los atributos de item, separados por ##</param>
''' <param name="IdsAtribItem">Ids de atributos de item en el documento, separados por ##</param>
''' <remarks>Llamada desde: ObtenerHojaAdjudicacion; Tiempo m�ximo:2 (el word es muy lento)</remarks>

Private Sub TransferirDatosAdjudicacion(ByRef blankword As Object, ByRef rangeword6 As Object, ByVal oAdjs As CAdjudicaciones, ByVal sCod As String, _
        ByVal oGrupo As CGrupo, ByVal oItem As CItem, ByVal oProve As CProveedor, ByRef cont As Integer, Optional ByVal oEsc As CEscalado, _
        Optional ByVal MarcadoresAtribItem As String, Optional ByVal MostrarAtribItem As String, Optional ByVal IdsAtribItem As String)
    Dim oAdj As CAdjudicacion
    Dim bAdj As Boolean
    Dim sComentario As String
    Dim vCantidad As Variant
    Dim ListaAtrib() As String
    Dim ListaVisibleAtrib() As String
    Dim ListaAtribId() As String
    Dim iAtr As Integer
    
    On Error GoTo ERROR_Frm
    
    bAdj = False
    
    err.clear
    Set oAdj = oAdjs.Item(sCod)
    If err = 0 Then
        If Not oAdj Is Nothing Then
            If NullToDbl0(oAdj.Porcentaje) <> 0 Then
                bAdj = True
            End If
        End If
    End If
    
    ListaAtrib = Split(MarcadoresAtribItem, "##")
    ListaVisibleAtrib = Split(MostrarAtribItem, "##")
    ListaAtribId = Split(IdsAtribItem, "##")
        
    If bAdj Then
        'la paso al documento
        rangeword6.Copy
       
        With blankword
            .Application.Selection.Paste
                                                                                                               
            If .Bookmarks.Exists("COD_ART") Then .Bookmarks("COD_ART").Range.Text = NullToStr(oItem.ArticuloCod)
            If .Bookmarks.Exists("DEN_ART") Then .Bookmarks("DEN_ART").Range.Text = NullToStr(oItem.Descr)
            If .Bookmarks.Exists("COD_DEST_IT") Then .Bookmarks("COD_DEST_IT").Range.Text = oItem.DestCod
            If .Bookmarks.Exists("DEN_DEST_IT") Then .Bookmarks("DEN_DEST_IT").Range.Text = oDestinos.Item(oItem.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            
            If blankword.Bookmarks.Exists("COD_UNI") Then
                If oGrupo.UsarEscalados Then
                    blankword.Bookmarks("COD_UNI").Range.Text = oGrupo.UnidadEscalado
                Else
                    blankword.Bookmarks("COD_UNI").Range.Text = oItem.UniCod
                End If
            End If
            If .Bookmarks.Exists("CANTIDAD") Then
                If oGrupo.UsarEscalados Then
                    vCantidad = oAdj.Adjudicado
                Else
                    vCantidad = oItem.Cantidad
                End If
                If Not IsNull(vCantidad) And Not IsEmpty(vCantidad) Then
                    .Bookmarks("CANTIDAD").Range.Text = FormateoNumerico((oAdj.Porcentaje / 100) * vCantidad, m_sFormatoNumberCantGr)
                Else
                    .Bookmarks("CANTIDAD").Range.Text = ""
                End If
            End If
            If oGrupo.UsarEscalados Then
                Select Case oGrupo.TipoEscalados
                    Case TModoEscalado.ModoDirecto
                        blankword.Bookmarks("ESC_RANGO").Range.Text = oEsc.Inicial
                    Case TModoEscalado.ModoRangos
                        blankword.Bookmarks("ESC_RANGO").Range.Text = oEsc.Inicial & " - " & oEsc.final
                End Select
            End If
            If blankword.Bookmarks.Exists("FINI_SUMINISTRO") Then blankword.Bookmarks("FINI_SUMINISTRO").Range.Text = oItem.FechaInicioSuministro
            If blankword.Bookmarks.Exists("FFIN_SUMINISTRO") Then blankword.Bookmarks("FFIN_SUMINISTRO").Range.Text = oItem.FechaFinSuministro
            If blankword.Bookmarks.Exists("COD_PAGO_IT") Then blankword.Bookmarks("COD_PAGO_IT").Range.Text = oItem.PagCod
            If blankword.Bookmarks.Exists("DEN_PAGO_IT") Then blankword.Bookmarks("DEN_PAGO_IT").Range.Text = oPagos.Item(oItem.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            
            If blankword.Bookmarks.Exists("PRESUPUESTO") Then
                If oGrupo.UsarEscalados Then
                    If Not IsNull(oItem.Escalados.Item(CStr(oEsc.Id)).Presupuesto) And Not IsEmpty(oItem.Escalados.Item(CStr(oEsc.Id)).Presupuesto) Then
                        blankword.Bookmarks("PRESUPUESTO").Range.Text = FormateoNumerico(oItem.Escalados.Item(CStr(oEsc.Id)).Presupuesto, m_sFormatoNumberPrecioGr)
                    Else
                        blankword.Bookmarks("PRESUPUESTO").Range.Text = ""
                    End If
                Else
                    If Not IsNull(oItem.Precio) And Not IsEmpty(oItem.Precio) Then
                        blankword.Bookmarks("PRESUPUESTO").Range.Text = FormateoNumerico(oItem.Precio, m_sFormatoNumberPrecioGr)
                    Else
                        blankword.Bookmarks("PRESUPUESTO").Range.Text = ""
                    End If
                End If
            End If
            
            If .Bookmarks.Exists("PRECIO") Then
                If oGrupo.UsarEscalados Then
                    blankword.Bookmarks("PRECIO").Range.Text = FormateoNumerico(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(oEsc.Id)).PrecioOferta / oAdj.Cambio, m_sFormatoNumberPrecioGr)
                Else
                    blankword.Bookmarks("PRECIO").Range.Text = FormateoNumerico(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).Lineas.Item(CStr(oItem.Id)).PrecioOferta / oAdj.Cambio, m_sFormatoNumberPrecioGr)
                End If
            End If
            
            
            If .Bookmarks.Exists("PRECIO_TOTAL") Then
                
                sComentario = ""
                sComentario = g_oOrigen.RPT_FormulaDeAtributosDeItemProveedor(oGrupo.Codigo, oProve.Cod, oItem.Id)
                
                If sComentario <> "" Then
                    Set rangeword6 = .Application.Selection.Range
                    
                     .Bookmarks("PRECIO_TOTAL").Range.Select
                     .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                     
                     .Application.Selection.TypeText Text:=sComentario
                    .Application.activewindow.ActivePane.Close
                    blankword.Bookmarks("PRECIO_TOTAL").Range.Text = FormateoNumerico((oAdj.ImporteAdjTot / oAdj.Cambio), m_sFormatoNumberPrecioGr)
                   
                    .Application.Selection.MoveRight Unit:=1, Count:=2
                Else
                    blankword.Bookmarks("PRECIO_TOTAL").Range.Text = FormateoNumerico((oAdj.ImporteAdjTot / oAdj.Cambio), m_sFormatoNumberPrecioGr)
                 End If
            End If
            
            If .Bookmarks.Exists("OFERTA") Then .Bookmarks("OFERTA").Range.Text = oAdj.NumOfe
                        
            If UBound(ListaAtrib) >= 0 Then
                For iAtr = 0 To UBound(ListaAtrib)
                    If ListaVisibleAtrib(iAtr) = "1" Then
                        If .Bookmarks.Exists("VALOR_" & ListaAtrib(iAtr)) Then
                            Select Case oGrupo.AtributosItem.Item(CStr(ListaAtribId(iAtr))).Tipo
                            Case TiposDeAtributos.TipoNumerico
                                blankword.Bookmarks("VALOR_" & ListaAtrib(iAtr)).Range.Text = FormateoNumerico(NullToStr(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oAdj.Id) & "$" & ListaAtribId(iAtr)).valorNum), m_sFormatoNumberPrecioGr)
                            Case TiposDeAtributos.TipoBoolean
                                If NullToStr(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oAdj.Id) & "$" & ListaAtribId(iAtr)).valorBool) Then
                                    blankword.Bookmarks("VALOR_" & ListaAtrib(iAtr)).Range.Text = m_sSi
                                Else
                                    blankword.Bookmarks("VALOR_" & ListaAtrib(iAtr)).Range.Text = m_sNo
                                End If
                            Case TiposDeAtributos.TipoFecha
                                blankword.Bookmarks("VALOR_" & ListaAtrib(iAtr)).Range.Text = Format(NullToStr(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oAdj.Id) & "$" & ListaAtribId(iAtr)).valorFec), "Short Date")
                            Case Else
                                blankword.Bookmarks("VALOR_" & ListaAtrib(iAtr)).Range.Text = NullToStr(oGrupo.UltimasOfertas.Item(Trim(oProve.Cod)).AtribItemOfertados.Item(CStr(oAdj.Id) & "$" & ListaAtribId(iAtr)).valorText)
                            End Select
                        End If
                    End If
                Next
            End If
        End With
        
        cont = cont + 1
    End If
    
Salir:
    Set oAdj = Nothing
    Exit Sub

ERROR_Frm:
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    Else
        err.Raise err.Number, , err.Description
    End If
    Resume Salir
End Sub

''' <summary>Sacar en formato word la hoja de adjudicaciones. Items cerrados deben de salir. Llamada desde frmResReu.</summary>
''' <remarks>Llamada desde: cmdAceptar_Click; Tiempo m�ximo:2 (el word es muy lento)</remarks>
''' <revision>LTG 28/11/2011</revision>
Private Sub ObtenerHojaAdjudicacionReu()
    Dim dPresGrupo As Double
    Dim dAhorradoGrupo As Double
    Dim dAdjGrupo As Double
    Dim sComentario As String
    Dim appword As Object
    Dim docword As Object
    Dim blankword As Object
    Dim rangeword As Object
    Dim rangeword5 As Object
    Dim rangeword6 As Object
    Dim oProce As cProceso
    Dim oProve As CProveedor
    Dim oProves As CProveedores
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oIasig As IAsignaciones
    Dim oIAdjudicaciones As IAdjudicaciones
    Dim dVolProceAdj As Double
    Dim dVolProveAdj As Double
    Dim dVolProcePres As Double
    Dim dVolProvePres As Double
    Dim oFirmas As CFirmas
    Dim oFirma As CFirma
    Dim INumProve As Integer
    Dim iNumProveTotal As Integer
    Dim sFirma As String
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim Destino As Boolean
    Dim Pago As Boolean
    Dim Fecha As Boolean
    Dim sCod As String
    Dim cont As Integer
    Dim bError As Boolean
    Dim bVisibleWord As Boolean
    Dim sVersion As String
    Dim bSaleItem As Boolean
    Dim oEsc As CEscalado
    Dim dblImporteAux As Double
    'Atributos item tarea 2707
    Dim bm As Object
    Dim CountAtrib As Integer
    Dim i As Integer
    Dim bEsta As Boolean
    Dim strAtrib As String
    Dim MarcadoresAtribItem As String
    Dim MostrarAtribItem As String
    Dim IdsAtribItem As String
    Dim ListaAtribId As Long
    Dim oAtribCod As CAtributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bError = False
    
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm
    
    'Comprobar las plantillas
    If Right(txtDetProceDot, 3) <> "dot" Then
        oMensajes.NoValida sIdiPlantilla
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
    
    If Not oFos.FileExists(txtDetProceDot) Then
        oMensajes.PlantillaNoEncontrada txtDetProceDot
        If Me.Visible Then txtDetProceDot.SetFocus
        Exit Sub
    End If
        
    If g_oOrigen.m_oReuSeleccionada Is Nothing Then
        oMensajes.NoValida sIdiReunion
        Exit Sub
    End If
        
    Screen.MousePointer = vbHourglass
    
    Me.Hide

    frmESPERA.lblGeneral.caption = sIdiGenHojaAdj
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Show
    DoEvents
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.lblDetalle = sIdiAbrWord

    'Crear aplicaci�n word
    Set appword = CreateObject("Word.Application")
    bVisibleWord = appword.Visible
    appword.WindowState = 2
    sVersion = appword.VERSION
    If InStr(1, sVersion, ".") > 1 Then
        sVersion = Left(sVersion, InStr(1, sVersion, ".") - 1)
        m_iWordVer = CInt(sVersion)
    Else
        m_iWordVer = 9
    End If
    
    Set oProce = g_oOrigen.m_oProcesoSeleccionado
      
    frmESPERA.ProgressBar1.Value = 2
    
    frmESPERA.lblDetalle = sIdiAgrPlant & " " & txtDetProceDot.Text
      
    'Crear documento general
    Set docword = appword.Documents.Add(txtDetProceDot.Text)
    'Crear documento en blanco con las mismas caracteristicas que la plantilla
    Set blankword = appword.Documents.Add(txtDetProceDot.Text)
    appword.Visible = True
    
    If m_iWordVer >= 9 Then
        If appword.Visible Then docword.activewindow.WindowState = 2
        If appword.Visible Then blankword.activewindow.WindowState = 2
    Else
        appword.Windows(blankword.Name).WindowState = 1
        appword.Windows(docword.Name).WindowState = 1
    End If
    
    frmESPERA.lblContacto = sIdiProceso & " " & CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod) & " " & oProce.Den
    frmESPERA.lblDetalle = sIdiGenDatosProce

    With blankword
        'guardar los bookmarks de la plantilla
        CountAtrib = -1
        For Each bm In .Bookmarks
            strAtrib = Left(bm.Name, 6)
            If strAtrib = "ATRIB_" Then
                CountAtrib = CountAtrib + 1
                ReDim Preserve ListaAtribBmk(CountAtrib)
                ListaAtribBmk(CountAtrib) = Right(bm.Name, Len(bm.Name) - 6)
            End If
        Next bm
    
        If .Bookmarks.Exists("FEC_REUNION") Then
            DatoAWord blankword, "FEC_REUNION", Format(g_oOrigen.sdbcFecReu.Columns(0).Value, "Short Date") & " " & Format(g_oOrigen.sdbcFecReu.Columns(0).Value, "Short time")
        End If
        
        If .Bookmarks.Exists("REF_REUNION") Then
            DatoAWord blankword, "REF_REUNION", g_oOrigen.lblref
        End If
        
        If .Bookmarks.Exists("COD_PROCESO") Then
            DatoAWord blankword, "COD_PROCESO", CStr(oProce.Anyo) & "/" & oProce.GMN1Cod & "/" & CStr(oProce.Cod)
        End If
        If .Bookmarks.Exists("DEN_PROCESO") Then
        DatoAWord blankword, "DEN_PROCESO", oProce.Den
        End If
        If .Bookmarks.Exists("MAT1_PROCESO") Then
        DatoAWord blankword, "MAT1_PROCESO", oProce.GMN1Cod
        End If
        If .Bookmarks.Exists("MONEDA_PROCESO") Then
        DatoAWord blankword, "MONEDA_PROCESO", oProce.MonCod
        End If
        If .Bookmarks.Exists("CAMBIO_PROCESO") Then
        DatoAWord blankword, "CAMBIO_PROCESO", FormateoNumerico(oProce.Cambio, m_sFormatoNumber)
        End If
        If Not oProce.responsable Is Nothing Then
            If oProce.responsable.Cod <> "" Then
                Set oIasig = oProce
                oIasig.DevolverResponsable
                Set oIasig = Nothing
                If .Bookmarks.Exists("COD_RESPONSABLE") Then
                    .Bookmarks("COD_RESPONSABLE").Range.Text = oProce.responsable.Cod
                End If
                If .Bookmarks.Exists("NOM_RESPONSABLE") Then
                    .Bookmarks("NOM_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.nombre)
                End If
                If .Bookmarks.Exists("APE_RESPONSABLE") Then
                    .Bookmarks("APE_RESPONSABLE").Range.Text = oProce.responsable.Apel
                End If
                If .Bookmarks.Exists("DEQP_RESPONSABLE") Then
                    .Bookmarks("DEQP_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.DenEqp)
                End If
                If .Bookmarks.Exists("EQP_RESPONSABLE") Then
                    .Bookmarks("EQP_RESPONSABLE").Range.Text = oProce.responsable.codEqp
                End If
                If .Bookmarks.Exists("TLFNO_RESPONSABLE") Then
                    .Bookmarks("TLFNO_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.Tfno)
                End If
                If .Bookmarks.Exists("MAIL_RESPONSABLE") Then
                    .Bookmarks("MAIL_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.mail)
                End If
                If .Bookmarks.Exists("FAX_RESPONSABLE") Then
                    .Bookmarks("FAX_RESPONSABLE").Range.Text = NullToStr(oProce.responsable.Fax)
                End If
                .Bookmarks("OPC_RESPONSABLE").Delete
            Else
                If docword.Bookmarks.Exists("OPC_RESPONSABLE") Then
                    .Bookmarks("OPC_RESPONSABLE").Range.Delete
                Else
                    .Bookmarks("COD_RESPONSABLE").Range.Text = ""
                    .Bookmarks("NOM_RESPONSABLE").Range.Text = ""
                    .Bookmarks("APE_RESPONSABLE").Range.Text = ""
                    .Bookmarks("DEQP_RESPONSABLE").Range.Text = ""
                    .Bookmarks("EQP_RESPONSABLE").Range.Text = ""
                    .Bookmarks("TLFNO_RESPONSABLE").Range.Text = ""
                    .Bookmarks("MAIL_RESPONSABLE").Range.Text = ""
                    .Bookmarks("FAX_RESPONSABLE").Range.Text = ""
                End If
            End If
        End If
        If oProce.DefDestino = EnProceso Then
            If .Bookmarks.Exists("COD_DEST_PROCE") Then
                DatoAWord blankword, "COD_DEST_PROCE", NullToStr(oProce.DestCod)
            End If
            If .Bookmarks.Exists("DEN_DEST_PROCE") Then
                DatoAWord blankword, "DEN_DEST_PROCE", NullToStr(oDestinos.Item(oProce.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            .Bookmarks("DEST_PROCE").Delete
        Else
            If .Bookmarks.Exists("DEST_PROCE") Then
                .Bookmarks("DEST_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        If oProce.DefFechasSum = EnProceso Then
            If .Bookmarks.Exists("FINI_SUM_PROCE") Then
                DatoAWord blankword, "FINI_SUM_PROCE", NullToStr(oProce.FechaInicioSuministro)
            End If
            If .Bookmarks.Exists("FFIN_SUM_PROCE") Then
                DatoAWord blankword, "FFIN_SUM_PROCE", NullToStr(oProce.FechaFinSuministro)
            End If
            .Bookmarks("FECSUM_PROCE").Delete
        Else
            If .Bookmarks.Exists("FECSUM_PROCE") Then
                .Bookmarks("FECSUM_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        If oProce.DefFormaPago = EnProceso Then
            If .Bookmarks.Exists("COD_PAGO_PROCE") Then
                DatoAWord blankword, "COD_PAGO_PROCE", NullToStr(oProce.PagCod)
            End If
            If .Bookmarks.Exists("DEN_PAGO_PROCE") Then
                DatoAWord blankword, "DEN_PAGO_PROCE", NullToStr(oPagos.Item(oProce.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            End If
            .Bookmarks("PAGO_PROCE").Delete
        Else
            
            If .Bookmarks.Exists("PAGO_PROCE") Then
                .Bookmarks("PAGO_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        If Not IsNull(oProce.ComentADJ) Then
            If .Bookmarks.Exists("ADJ_COMENT") Then
                DatoAWord blankword, "ADJ_COMENT", NullToStr(oProce.ComentADJ)
                .Bookmarks("COMENT_ADJ_PROCE").Delete
            End If
        Else
            If .Bookmarks.Exists("COMENT_ADJ_PROCE") Then
                .Bookmarks("COMENT_ADJ_PROCE").Select
                .Application.Selection.cut
            End If
        End If
        
        dVolProceAdj = 0
        dVolProcePres = 0
        
        Set oIAdjudicaciones = oProce
        
        If g_oOrigen.m_vFechaVigente > CDate(g_oOrigen.sdbcFecReu.Columns(0).Value) And Not IsNull(g_oOrigen.m_vFechaVigente) Then
            'La reuni�n no es la vigente
            If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                Set oProves = oProce.DevolverHojaAdjudicacionesParcCerrado(CDate(g_oOrigen.sdbcFecReu.Columns(0).Value))
            Else
                Set oProves = oIAdjudicaciones.DevolverProveedoresConAdjudicaciones(, , , CDate(g_oOrigen.sdbcFecReu.Columns(0).Value))
            End If
        Else
            If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                Set oProves = oProce.DevolverHojaAdjudicacionesParcCerrado
            Else
                Set oProves = oIAdjudicaciones.DevolverProveedoresConAdjudicaciones
            End If
        End If
        Set oIAdjudicaciones = Nothing
        'Para cada proveedor
        
        INumProve = 1
        iNumProveTotal = oProves.Count
            
        If docword.Bookmarks.Exists("PROVEEDOR") Then
            .Bookmarks("PROVEEDOR").Select
            .Application.Selection.Delete
            Set rangeword = docword.Bookmarks("PROVEEDOR").Range
        End If
        For Each oProve In oProves
        
            frmESPERA.ProgressBar1.Value = 2
            frmESPERA.ProgressBar2.Value = (INumProve / iNumProveTotal) * 100
            frmESPERA.lblContacto = sIdiProveedor & oProve.Cod & " " & oProve.Den
            frmESPERA.lblDetalle = sIdiGenDatProve
            
            dVolProveAdj = 0
            dVolProvePres = 0
            rangeword.Copy
            .Application.Selection.Paste
            If .Bookmarks.Exists("COD_PROVEEDOR") Then
                .Bookmarks("COD_PROVEEDOR").Range.Text = oProve.Cod
            End If
            If .Bookmarks.Exists("DEN_PROVEEDOR") Then
                .Bookmarks("DEN_PROVEEDOR").Range.Text = NullToStr(oProve.Den)
            End If
            'Obtener el detalle de adjudicaci�n para cada proveedor
            'Puede que sea el de una reuni�n o las vigentes
            
            If .Bookmarks.Exists("GRUPO") Then
                If .Bookmarks.Exists("GRUPO") Then
                    .Bookmarks("GRUPO").Select
                    .Application.Selection.Delete
                End If
                Set rangeword5 = docword.Bookmarks("GRUPO").Range
                
                '****************************************
                If oProce.Grupos.Count = 0 Then
                    If .Bookmarks.Exists("GRUPO") Then
                        .Bookmarks("GRUPO").Select
                        .Application.Selection.Delete
                    End If
                Else
                    For Each oGrupo In oProce.Grupos
                      If ExistenAdjs(oGrupo, oProve.Cod) Then
                        rangeword5.Copy
                        .Application.Selection.Paste
                        If .Bookmarks.Exists("COD_GRUPO") Then .Bookmarks("COD_GRUPO").Range.Text = NullToStr(oGrupo.Codigo)
                        If .Bookmarks.Exists("DEN_GRUPO") Then .Bookmarks("DEN_GRUPO").Range.Text = NullToStr(oGrupo.Den)
                        If oGrupo.DefDestino Then
                            If .Bookmarks.Exists("COD_DEST_GR") Then .Bookmarks("COD_DEST_GR").Range.Text = NullToStr(oGrupo.DestCod)
                            If .Bookmarks.Exists("DEN_DEST_GR") Then .Bookmarks("DEN_DEST_GR").Range.Text = NullToStr(oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                            .Bookmarks("GR_DEST").Delete
                        Else
                            If .Bookmarks.Exists("GR_DEST") Then
                                .Bookmarks("GR_DEST").Select
                                .Application.Selection.cut
                            End If
                        End If
                        
                        If oGrupo.DefFechasSum Then
                            If .Bookmarks.Exists("FINI_SUM_GR") Then .Bookmarks("FINI_SUM_GR").Range.Text = NullToStr(oGrupo.FechaInicioSuministro)
                            If .Bookmarks.Exists("FFIN_SUM_GR") Then .Bookmarks("FFIN_SUM_GR").Range.Text = NullToStr(oGrupo.FechaFinSuministro)
                            .Bookmarks("GR_FECSUM").Delete
                        Else
                            If .Bookmarks.Exists("GR_FECSUM") Then
                                .Bookmarks("GR_FECSUM").Select
                                .Application.Selection.cut
                            End If
                        End If
                        
                        If oGrupo.DefFormaPago Then
                            If .Bookmarks.Exists("COD_PAGO_GR") Then .Bookmarks("COD_PAGO_GR").Range.Text = NullToStr(oGrupo.PagCod)
                            If .Bookmarks.Exists("DEN_PAGO_GR") Then .Bookmarks("DEN_PAGO_GR").Range.Text = NullToStr(oPagos.Item(oGrupo.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                            .Bookmarks("GR_PAGO").Delete
                        Else
                            If .Bookmarks.Exists("GR_PAGO") Then
                                .Bookmarks("GR_PAGO").Select
                                .Application.Selection.cut
                            End If
                        End If
                        
                        '@@ pendiente
                        If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                            DevolverImporteParcialCerrado oProve.Cod, oGrupo.Items, oGrupo.Adjudicaciones, dAdjGrupo, dAhorradoGrupo, dPresGrupo
                        Else
                            dAdjGrupo = DevolverAdjudicadoProveedorGrupo(oProve.Cod, oGrupo.Codigo)
                        End If
                        
                        If .Bookmarks.Exists("VOLADJ_GR") Then
                            sComentario = ""
                            
                            sComentario = g_oOrigen.RPT_FormulaDeAtributosDeGrupoProveedor(oGrupo.Codigo, oProve.Cod)
                            
                            If sComentario <> "" Then
                                .Bookmarks("VOLADJ_GR").Range.Select
                                .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                                .Application.Selection.TypeText Text:=sComentario
                                .Application.activewindow.ActivePane.Close
                            End If
                            
                            .Bookmarks("VOLADJ_GR").Range.Text = FormateoNumerico(dAdjGrupo, m_sFormatoNumberGr)
                        End If
                        
                        If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                        Else
                            dAhorradoGrupo = DevolverAhorradoProveedorGrupo(oProve.Cod, oGrupo.Codigo)
                        End If
                        
                        If .Bookmarks.Exists("VOLAHO_GR") Then
                            .Bookmarks("VOLAHO_GR").Range.Text = FormateoNumerico(dAhorradoGrupo, m_sFormatoNumberGr)
                        End If
                        
                        If Not gParametrosGenerales.gbHojaAdjsAbierto And oProce.Estado = ParcialmenteCerrado Then
                        Else
                            dPresGrupo = DevolverPresupuestadoProveedorGrupo(oProve.Cod, oGrupo.Codigo)
                        End If
                        If .Bookmarks.Exists("VOLPRES_GR") Then
                            .Bookmarks("VOLPRES_GR").Range.Text = FormateoNumerico(dPresGrupo, m_sFormatoNumberGr)
                        End If
                        
                        If .Bookmarks.Exists("PORCEN_GR") Then
                           If dPresGrupo > 0 Then
                              .Bookmarks("PORCEN_GR").Range.Text = FormateoNumerico((dAhorradoGrupo / dPresGrupo) * 100, m_sFormatoNumberPorcenGr)
                            Else
                               .Bookmarks("PORCEN_GR").Range.Text = "-100"
                            End If
                        End If
                        
                        'Comprobamos los datos configurables del item para la funcion
                        'que compone las tablas
                        
                        If (oProce.DefDestino = EnItem) Or (oProce.DefDestino = EnGrupo And Not oGrupo.DefDestino) Then
                            Destino = True
                        Else
                            Destino = False
                        End If
                        
                        If oProce.DefFormaPago = EnItem Or (oProce.DefFormaPago = EnGrupo And Not oGrupo.DefFormaPago) Then
                            Pago = True
                        Else
                            Pago = False
                        End If
                        If oProce.DefFechasSum = EnItem Or (oProce.DefFechasSum = EnGrupo And Not oGrupo.DefFechasSum) Then
                            Fecha = True
                        Else
                            Fecha = False
                        End If
                        DoEvents
                        
                        If .Bookmarks.Exists("ESCALADO") Then
                            If oGrupo.UsarEscalados Then
                                .Bookmarks("ESCALADO").Delete
                            Else
                                .Bookmarks("ESCALADO").Select
                                .Application.Selection.cut
                                .Bookmarks("ESCALADO").Delete
                            End If
                        End If
                        
                        If CountAtrib <> -1 Then
                            If UBound(ListaAtribBmk) >= 0 Then
                                oGrupo.CargarAtributos vAnyo:=oProce.Anyo, vGMN1Cod:=oProce.GMN1Cod, vProce:=oProce.Cod, ambito:=AmbItem
                            End If
                            MarcadoresAtribItem = ""
                            MostrarAtribItem = ""
                            IdsAtribItem = ""
                            For i = LBound(ListaAtribBmk) To UBound(ListaAtribBmk)
                                bEsta = False
                                If MarcadoresAtribItem = "" Then
                                    MarcadoresAtribItem = ListaAtribBmk(i)
                                Else
                                    MarcadoresAtribItem = MarcadoresAtribItem & "##" & ListaAtribBmk(i)
                                End If
                                ListaAtribId = -1
                                For Each oAtribCod In oGrupo.AtributosItem
                                    If oAtribCod.ambito = AmbItem Then
                                        If Replace(ListaAtribBmk(i), "���", " ") = oAtribCod.Cod Then
                                            bEsta = True
                                            If .Bookmarks.Exists("ATRIB_" & ListaAtribBmk(i)) Then .Bookmarks("ATRIB_" & ListaAtribBmk(i)).Range.Text = oAtribCod.Den
                                            ListaAtribId = oAtribCod.idAtribProce
                                            Exit For
                                        End If
                                    End If
                                Next
                                If MostrarAtribItem = "" Then
                                    MostrarAtribItem = IIf(bEsta, 1, 0)
                                    IdsAtribItem = CStr(ListaAtribId)
                                Else
                                    MostrarAtribItem = MostrarAtribItem & "##" & IIf(bEsta, 1, 0)
                                    IdsAtribItem = MostrarAtribItem & "##" & CStr(ListaAtribId)
                                End If
                            Next
                        End If
                                               
                        FSGSLibrary.DatosConfigurablesAWord blankword, "IT_DEST", "IT_PAGO", "IT_FECSUM", Destino, Pago, Fecha, , , MarcadoresAtribItem, MostrarAtribItem
                        If blankword.Bookmarks.Exists("ITEM") Then
                            DoEvents
                            Set rangeword6 = blankword.Bookmarks("ITEM").Range
                            .Bookmarks("ITEM").Select
                            .Application.Selection.cut
                            If oGrupo.Items.Count = 0 Then
                            
                            Else
                                cont = 0
                                Set oAdjs = oGrupo.Adjudicaciones
                                
                                For Each oItem In oGrupo.Items
                                    If gParametrosGenerales.gbHojaAdjsAbierto Then
                                        bSaleItem = oItem.Confirmado
                                    Else
                                        If oProce.Estado = ParcialmenteCerrado Then
                                            bSaleItem = oItem.Confirmado And oItem.Cerrado
                                        Else
                                            bSaleItem = oItem.Confirmado
                                        End If
                                    End If
                                    
                                    If bSaleItem Then
                                        If oGrupo.UsarEscalados Then
                                            For Each oEsc In oGrupo.Escalados
                                                sCod = KeyEscalado(oProve.Cod, oItem.Id, oEsc.Id)
                                                TransferirDatosAdjudicacion blankword, rangeword6, oAdjs, sCod, oGrupo, oItem, oProve, cont, oEsc, MarcadoresAtribItem, MostrarAtribItem, IdsAtribItem
                                            Next
                                        Else
                                            sCod = oProve.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProve.Cod))
                                            sCod = CStr(oItem.Id) & sCod
                                            
                                            TransferirDatosAdjudicacion blankword, rangeword6, oAdjs, sCod, oGrupo, oItem, oProve, cont, , MarcadoresAtribItem, MostrarAtribItem, IdsAtribItem
                                        End If
                                    End If
                                    DoEvents
                                Next
                                If .Bookmarks.Exists("ITEM") Then
                                    .Bookmarks("ITEM").Delete
                                End If
                            End If
                        End If
                        
                        If cont = 0 Then
                            If .Bookmarks.Exists("GRUPO") Then
                                .Bookmarks("GRUPO").Select
                                .Application.Selection.cut
                            End If
                        Else
                            If .Bookmarks.Exists("GRUPO") Then
                                .Bookmarks("GRUPO").Delete
                            End If
                        End If
                        
                        dVolProvePres = dVolProvePres + dPresGrupo
                        dVolProveAdj = dVolProveAdj + dAdjGrupo
                      End If
                    Next
                    If .Bookmarks.Exists("GRUPO") Then
                        .Bookmarks("GRUPO").Delete
                    End If
                End If
            End If
            '****************************************
            
            frmESPERA.ProgressBar1.Value = 4
            If .Bookmarks.Exists("VOLADJ_PROVEEDOR") Then
                .Bookmarks("VOLADJ_PROVEEDOR").Range.Text = FormateoNumerico(dVolProveAdj, m_sFormatoNumber)
            End If
            frmESPERA.ProgressBar1.Value = 5
            If .Bookmarks.Exists("VOLAHO_PROVEEDOR") Then
                .Bookmarks("VOLAHO_PROVEEDOR").Range.Text = FormateoNumerico(dVolProvePres - dVolProveAdj, m_sFormatoNumber)
            End If
            
            frmESPERA.ProgressBar1.Value = 6
            If .Bookmarks.Exists("VOLPRES_PROVEEDOR") Then
                .Bookmarks("VOLPRES_PROVEEDOR").Range.Text = FormateoNumerico(dVolProvePres, m_sFormatoNumber)
            End If
            frmESPERA.ProgressBar1.Value = 7
            If .Bookmarks.Exists("PORCEN_PROVEEDOR") Then
                If dVolProvePres = 0 Then
                    If dVolProveAdj <> 0 Then
                        .Bookmarks("PORCEN_PROVEEDOR").Range.Text = "-100"
                    Else
                        .Bookmarks("PORCEN_PROVEEDOR").Range.Text = "0"
                    End If
                Else
                    .Bookmarks("PORCEN_PROVEEDOR").Range.Text = FormateoNumerico(((dVolProvePres - dVolProveAdj) / dVolProvePres) * 100, m_sFormatoNumberPorcen)
                End If
            End If
            frmESPERA.ProgressBar1.Value = 8
            
            dVolProcePres = dVolProcePres + dVolProvePres
            dVolProceAdj = dVolProceAdj + AplicarAtributos(oProce, g_oOrigen.m_oAtribsFormulas, dVolProveAdj, TotalOferta, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE)
            INumProve = INumProve + 1
        Next
        If .Bookmarks.Exists("PROVEEDOR") Then
            .Bookmarks("PROVEEDOR").Delete
        End If
        frmESPERA.ProgressBar1.Value = 10
        
        'Pasamos los datos restantes del proceso
        If .Bookmarks.Exists("VOLPRES_PROCESO") Then
            .Bookmarks("VOLPRES_PROCESO").Range.Text = FormateoNumerico(dVolProcePres, m_sFormatoNumber)
        End If
        
        If .Bookmarks.Exists("VOLADJ_PROCESO") Then
                m_sProceFormula = ""
                For Each oProve In g_oOrigen.m_oProvesAsig
                    If m_sProceFormula = "" Then
                        If g_oOrigen.ComprobarAplicarAtribsProceso(oProve.Cod) = True Then
                            m_sProceFormula = g_oOrigen.RPT_FormulaDeAtributosProcesoProveedor(oProve.Cod)
                        End If
                    Else
                        Exit For
                    End If
                Next
             If m_sProceFormula <> "" Then
                .Bookmarks("VOLADJ_PROCESO").Range.Select
                .Application.Selection.Comments.Add Range:=.Application.Selection.Range
                .Application.Selection.TypeText Text:=m_sProceFormula
                .Application.activewindow.ActivePane.Close
            End If
            .Bookmarks("VOLADJ_PROCESO").Range.Text = FormateoNumerico(dVolProceAdj, m_sFormatoNumber)
            
        End If
        If .Bookmarks.Exists("VOLAHO_PROCESO") Then
            .Bookmarks("VOLAHO_PROCESO").Range.Text = FormateoNumerico(dVolProcePres - dVolProceAdj, m_sFormatoNumber)
        End If
        If .Bookmarks.Exists("PORCEN_PROCESO") Then
            If dVolProcePres = 0 Then
                .Bookmarks("PORCEN_PROCESO").Range.Text = 0
            Else
                .Bookmarks("PORCEN_PROCESO").Range.Text = FormateoNumerico(((dVolProcePres - dVolProceAdj) / dVolProcePres) * 100, m_sFormatoNumberPorcen)
            End If
        End If
        
        frmESPERA.ProgressBar1.Value = 6
        frmESPERA.lblContacto = sIdiPasFirmas
        frmESPERA.lblDetalle = ""
        
        'Ahora tengo que poner las firmas
        Set oFirmas = oFSGSRaiz.generar_CFirmas
        oFirmas.cargartodaslasfirmas
        
        If oFirmas.Count > 0 Then
            For Each oFirma In oFirmas
                sFirma = "FIRMA" & oFirma.Cod
                If .Bookmarks.Exists(DblQuote(sFirma)) Then
                    .Bookmarks(DblQuote(sFirma)).Range.Text = oFirma.Den
                End If
                sFirma = ""
            Next
        End If
    End With
    
    ' cerrar documento word
    docword.Close (False)
    appword.Selection.HomeKey Unit:=6
    appword.WindowState = 1
    appword.Visible = True
    BringWindowToTop FindWindow(0&, appword.activewindow.caption & " - Microsoft Word")
    
    basSeguridad.RegistrarAccion AccionesSummit.ACCOfeCompObtenerHojaAdj, "Anyo:" & CStr(g_oOrigen.m_oProcesoSeleccionado.Anyo) & "Gmn1:" & g_oOrigen.m_oProcesoSeleccionado.GMN1Cod & "Proce:" & CStr(g_oOrigen.m_oProcesoSeleccionado.Cod)
    Screen.MousePointer = vbNormal
    
    Unload frmESPERA

Finalizar:
    On Error Resume Next
    If bError Then
        docword.Close (False)
        blankword.Close (False)
        If bVisibleWord = False Then
            appword.Quit
        End If
    End If
    
    Set appword = Nothing
    Set docword = Nothing
    Set blankword = Nothing
    Set rangeword = Nothing
    Set rangeword5 = Nothing
    Set rangeword6 = Nothing
    Set oProce = Nothing
    Set oProve = Nothing
    Set oProves = Nothing
    Set oAdjs = Nothing
    Set oAdj = Nothing
    Set oFirmas = Nothing
    Set oFirma = Nothing
    Set oGrupo = Nothing
    Set oItem = Nothing
    Exit Sub

ERROR_Frm:
    If err.Number = 5834 Or err.Number = 5941 Or err.Number = 9 Or err.Number = 91 Or err.Number = 4605 Then
        Resume Next
    End If
    
    Screen.MousePointer = vbNormal
    Resume Next
    Unload frmESPERA
    
    MsgBox err.Description, vbCritical, "Fullstep"
    bError = True
    Resume Finalizar
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ObtenerHojaAdjudicacionReu", err, Erl, , m_bActivado)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub

Private Function DevolverLetra(ByVal sCol As String, Optional ByVal iIncr As Integer) As String
Dim sDecena As String
Dim sUnidad As String
Dim idesvA As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Len(sCol) = 1 Then
    idesvA = iIncr + (Asc(sCol) - 65)
Else
    sDecena = Left(sCol, 1)
    sUnidad = Right(sCol, 1)
    idesvA = (Asc(sDecena) - 64) * 26 + Asc(sUnidad) - 65 + iIncr
End If
If idesvA \ 26 > 0 Then
    sDecena = Chr(65 + idesvA \ 26 - 1)
Else
    sDecena = ""
End If

sUnidad = Chr(65 + idesvA Mod 26)

DevolverLetra = sDecena & sUnidad
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "DevolverLetra", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function AplicarValorAtrib() As String
Dim sCod As String

sCod = "Function AplicarValorAtrib(ByVal dValor As Variant, ByVal dImporte As Double, ByRef sTipoOperacion As String)"
sCod = sCod & vbLf & "Dim sOperacion As String"
sCod = sCod & vbLf & ""
sCod = sCod & vbLf & "If dValor <> """" Then"
sCod = sCod & vbLf & "        Select Case sTipoOperacion"
sCod = sCod & vbLf & "        Case ""@suma@"""
sCod = sCod & vbLf & "            dImporte = dImporte + dValor"
sCod = sCod & vbLf & "            sOperacion = ""+"""
sCod = sCod & vbLf & "        Case ""@resta@"""
sCod = sCod & vbLf & "            dImporte = dImporte - dValor"
sCod = sCod & vbLf & "            sOperacion = ""-"""
sCod = sCod & vbLf & "        Case ""@mult@"""
sCod = sCod & vbLf & "            dImporte = dImporte * dValor"
sCod = sCod & vbLf & "            sOperacion = ""*"""
sCod = sCod & vbLf & "        Case ""@div@"""
sCod = sCod & vbLf & "            dImporte = dImporte / dValor"
sCod = sCod & vbLf & "            sOperacion = ""/"""
sCod = sCod & vbLf & "        Case ""@pormas@"""
sCod = sCod & vbLf & "            dImporte = dImporte + (dImporte * dValor / 100)"
sCod = sCod & vbLf & "            sOperacion = ""+%"""
sCod = sCod & vbLf & "        Case ""@pormenos@"""
sCod = sCod & vbLf & "            dImporte = dImporte - (dImporte * dValor / 100)"
sCod = sCod & vbLf & "            sOperacion = ""-%"""
sCod = sCod & vbLf & "        Case """""
sCod = sCod & vbLf & "        End Select"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "sTipoOperacion=sOperacion"
sCod = sCod & vbLf & "AplicarValorAtrib = dImporte"
sCod = sCod & vbLf & "End Function"

AplicarValorAtrib = sCod
End Function
Private Function ObtenerValorAtrib() As String
Dim sCod As String

    '@@PENDIENTE PARA M�S DE LA COLUMNA Z
    
sCod = "Function ObtenerValorAtrib(ByVal sCodAtrib As String, ByVal sCodProve As String, ByRef sTipoOperacion As String, ByRef sComentario As String, ByVal iAmbito As Integer,optional  ByVal sCodGrupo As String, optional ByVal idItem As Integer) As Variant"
sCod = sCod & vbLf & "Dim sCelda As String"
sCod = sCod & vbLf & "Dim I As Integer"
sCod = sCod & vbLf & "Dim j As Integer"
sCod = sCod & vbLf & "Dim iNumGrupos As Integer"
sCod = sCod & vbLf & "Dim bEncontrado As Boolean"
sCod = sCod & vbLf & "Dim xlsheet As Object"
sCod = sCod & vbLf & "Dim vValor As Variant"
sCod = sCod & vbLf & "Dim sLetra As String"
sCod = sCod & vbLf & ""
sCod = sCod & vbLf & "iNumGrupos = " & g_oOrigen.m_oProcesoSeleccionado.Grupos.Count
sCod = sCod & vbLf & "Select Case iAmbito"
sCod = sCod & vbLf & "Case 1"
sCod = sCod & vbLf & "    Set xlsheet = Worksheets(""@P@R@O@C@E@"")"
sCod = sCod & vbLf & "    sCelda = ""B1"""
sCod = sCod & vbLf & "    sLetra = ""B"""
sCod = sCod & vbLf & "    I = 1"
sCod = sCod & vbLf & "    With xlsheet"
sCod = sCod & vbLf & "        While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "            If .Range(sCelda).Value = sCodProve Then"
sCod = sCod & vbLf & "                bEncontrado = True"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                sLetra = IncrementarColumna(sLetra, 1)"
sCod = sCod & vbLf & "                sCelda = sLetra & ""1"""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Wend"
sCod = sCod & vbLf & "        If bEncontrado Then"
sCod = sCod & vbLf & "            bEncontrado = False"
sCod = sCod & vbLf & "          sCelda = sLetra & ""2"""
sCod = sCod & vbLf & "          I = 2"
sCod = sCod & vbLf & "            While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "                If .Range(sCelda).Value = sCodAtrib Then"
sCod = sCod & vbLf & "                    bEncontrado = True"
sCod = sCod & vbLf & "                Else"
sCod = sCod & vbLf & "                    I = I + 2"
sCod = sCod & vbLf & "                    sCelda = sLetra & I"
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Wend"
sCod = sCod & vbLf & "            If bEncontrado Then"
sCod = sCod & vbLf & "                sCelda = sLetra & CStr(I + 1)"
sCod = sCod & vbLf & "                vValor = .Range(sCelda).Value"
sCod = sCod & vbLf & "                sTipoOperacion = .Range(""A"" & I)"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                vValor = """""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "          vValor = """""
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    End With"
sCod = sCod & vbLf & "Case 2 'Ambito grupo"
sCod = sCod & vbLf & "    Set xlsheet = Worksheets(""@G@R@U@"")"
sCod = sCod & vbLf & "    sCelda = ""C1"""
sCod = sCod & vbLf & "    sLetra = ""C"""
sCod = sCod & vbLf & "    I = 1"
sCod = sCod & vbLf & "    With xlsheet"
sCod = sCod & vbLf & "        While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "            If .Range(sCelda).Value = sCodProve Then"
sCod = sCod & vbLf & "                bEncontrado = True"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                sLetra = IncrementarColumna(sLetra, 1)"
sCod = sCod & vbLf & "                sCelda = sLetra & ""1"""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Wend"
sCod = sCod & vbLf & "        If bEncontrado Then"
sCod = sCod & vbLf & "            bEncontrado = False"
sCod = sCod & vbLf & "            sCelda = sLetra & ""2"""
sCod = sCod & vbLf & "            I = 2"
sCod = sCod & vbLf & "            While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "                If .Range(sCelda).Value = sCodAtrib Then"
sCod = sCod & vbLf & "                    bEncontrado = True"
sCod = sCod & vbLf & "                Else"
sCod = sCod & vbLf & "                    I = I + iNumGrupos + 1"
sCod = sCod & vbLf & "                    sCelda = sLetra & I"
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Wend"
sCod = sCod & vbLf & "            If bEncontrado Then"
sCod = sCod & vbLf & "                For j = 1 To iNumGrupos"
sCod = sCod & vbLf & "                    If .Range(""B"" & I + j).Value = sCodGrupo Then"
sCod = sCod & vbLf & "                        sCelda = sLetra & CStr(I + j)"
sCod = sCod & vbLf & "                        vValor = .Range(sCelda).Value"
sCod = sCod & vbLf & "                        sTipoOperacion = .Range(""A"" & I + j)"
sCod = sCod & vbLf & "                    End If"
sCod = sCod & vbLf & "                Next"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                vValor = """""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "          vValor = """""
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    End With"
sCod = sCod & vbLf & "Case 3"
sCod = sCod & vbLf & "    For I = 1 To Worksheets.Count"
sCod = sCod & vbLf & "        If Worksheets(I).Name = ""@"" & sCodGrupo & ""@"" & sCodAtrib Then"
sCod = sCod & vbLf & "            Set xlsheet = Worksheets(""@"" & sCodGrupo & ""@"" & sCodAtrib)"
sCod = sCod & vbLf & "            Exit For"
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    Next"
sCod = sCod & vbLf & "    If Not xlsheet Is Nothing Then"
sCod = sCod & vbLf & "        With xlsheet"
sCod = sCod & vbLf & "            sCelda = ""B1"""
sCod = sCod & vbLf & "            sLetra = ""B"""
sCod = sCod & vbLf & "            I = 1"
sCod = sCod & vbLf & "            sTipoOperacion = .Range(""A1"").Value "
sCod = sCod & vbLf & "            While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "                If .Range(sCelda).Value = sCodProve Then"
sCod = sCod & vbLf & "                    bEncontrado = True"
sCod = sCod & vbLf & "                Else"
sCod = sCod & vbLf & "                    sLetra = IncrementarColumna(sLetra, 1)"
sCod = sCod & vbLf & "                    sCelda = sLetra & ""1"""
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Wend"
sCod = sCod & vbLf & "            If bEncontrado Then"
sCod = sCod & vbLf & "              vValor = .Range(Left(sCelda, 1) & idItem + 1).Value"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "              vValor = """""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        End With"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        vValor = """""
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "End Select"
sCod = sCod & vbLf & "If vValor <> """" Then"
sCod = sCod & vbLf & "    ObtenerValorAtrib = vValor"
sCod = sCod & vbLf & "    sComentario = ""X"""
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    Select Case sTipoOperacion"
sCod = sCod & vbLf & "    Case ""@suma@"", ""@resta@"""
sCod = sCod & vbLf & "        ObtenerValorAtrib = 0"
sCod = sCod & vbLf & "    Case ""@mult@"", ""@div@"""
sCod = sCod & vbLf & "        ObtenerValorAtrib = 1"
sCod = sCod & vbLf & "    Case ""@pormas@"", ""@pormenos@"""
sCod = sCod & vbLf & "        ObtenerValorAtrib = 0"
sCod = sCod & vbLf & "    Case Else"
sCod = sCod & vbLf & "        ObtenerValorAtrib = """""
sCod = sCod & vbLf & "    End Select"
sCod = sCod & vbLf & "    sComentario = """""
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "End Function"

ObtenerValorAtrib = sCod
End Function
Private Function BuscarLetraProve(ByVal xlSheet As Object, ByVal scodProve As String, ByVal sLetraInicial As String) As String
Dim sCelda As String
Dim i As Integer
Dim bEncontrado As Boolean
Dim sLetra As String

'Busca el proveedor por las celdas de la primera fila a partir de la columna sLetraInicial
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With xlSheet
        sCelda = sLetraInicial & "1"
        sLetra = sLetraInicial
        i = 1
        While xlSheet.Range(sCelda).Value <> "" And Not bEncontrado
            If xlSheet.Range(sCelda).Value = scodProve Then
                bEncontrado = True
            Else
                sLetra = DevolverLetra(sLetra, 1)
                sCelda = sLetra & "1"
            End If
        Wend
        If bEncontrado Then
            BuscarLetraProve = Left(sCelda, 1)
        Else
            xlSheet.Range(sCelda).NumberFormat = "@"
            xlSheet.Range(sCelda).Value = scodProve
            BuscarLetraProve = Left(sCelda, 1)
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "BuscarLetraProve", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function ExisteLaHoja(ByVal xlBook As Object, ByVal sName As String) As Boolean
On Error GoTo noExiste

    If xlBook.Sheets.Item(sName) <> "" Then
        ExisteLaHoja = True
    End If
    Exit Function
noExiste:
    ExisteLaHoja = False
End Function

Private Function DevolverAdjudicadoProveedorGrupo(ByVal sProve As String, ByVal sGrupo As String) As Double
    Dim oGrupo As CGrupo
    Dim oOferta As COferta
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sGrupo)
                    
    If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
        Set oOferta = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve)
        sCod = oGrupo.Codigo & sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
        
        If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
            DevolverAdjudicadoProveedorGrupo = g_oOrigen.m_oAdjs.Item(sCod).Adjudicado / g_oOrigen.m_oAdjs.Item(sCod).Cambio
        Else
            DevolverAdjudicadoProveedorGrupo = 0
        End If
    Else
        DevolverAdjudicadoProveedorGrupo = 0
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "DevolverAdjudicadoProveedorGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function DevolverAhorradoProveedorGrupo(ByVal sProve As String, ByVal sGrupo As String) As Double
Dim oGrupo As CGrupo
Dim oOferta As COferta
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set oGrupo = g_oOrigen.m_oProcesoSeleccionado.Grupos.Item(sGrupo)
           
If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
Set oOferta = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve)
sCod = oGrupo.Codigo & sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
    If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
        DevolverAhorradoProveedorGrupo = g_oOrigen.m_oAdjs.Item(sCod).Ahorrado / g_oOrigen.m_oAdjs.Item(sCod).Cambio
    Else
        DevolverAhorradoProveedorGrupo = 0
    End If
Else
    DevolverAhorradoProveedorGrupo = 0
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "DevolverAhorradoProveedorGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function DevolverPresupuestadoProveedorGrupo(ByVal sProve As String, ByVal sGrupo As String) As Double
Dim oOferta As COferta
Dim sCod As String
           
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve) Is Nothing Then
Set oOferta = g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve)
sCod = sGrupo & sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
    If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
        DevolverPresupuestadoProveedorGrupo = g_oOrigen.m_oAdjs.Item(sCod).Consumido / g_oOrigen.m_oAdjs.Item(sCod).Cambio
    Else
        DevolverPresupuestadoProveedorGrupo = 0
    End If
Else
    DevolverPresupuestadoProveedorGrupo = 0
End If
Set oOferta = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "DevolverPresupuestadoProveedorGrupo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>Indica si existen adjudicaciones para un proveedor en un grupo</summary>
''' <param name="oGrupo">Objeto grupo</param>
''' <param name="sProve">Cod. proveedor</param>
''' <returns>Booleano indicando si existen adjudicaciones</returns>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
''' <revision>LTG 28/11/2011</revision>
Private Function ExistenAdjs(ByVal oGrupo As CGrupo, ByVal sProve As String) As Boolean
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    Dim oItem As CItem
    Dim oEsc As CEscalado
    Dim cont As Integer
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oAdjs = oGrupo.Adjudicaciones
                                    
    For Each oItem In oGrupo.Items
        If oGrupo.UsarEscalados Then
            For Each oEsc In oGrupo.Escalados
                sCod = KeyEscalado(sProve, oItem.Id, oEsc.Id)
                Set oAdj = oAdjs.Item(sCod)
                If Not oAdj Is Nothing Then
                    cont = cont + 1
                    Exit For
                End If
            Next
            Set oEsc = Nothing
        Else
            cont = 0
            sCod = sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
            Set oAdj = oAdjs.Item(CStr(oItem.Id) & sCod)
            If Not oAdj Is Nothing Then
                If NullToDbl0(oAdjs.Item(CStr(oItem.Id) & sCod).Porcentaje) <> 0 Then
                    cont = cont + 1
                    Exit For
                End If
            End If
        End If
    Next
    Set oAdjs = Nothing
    Set oAdj = Nothing
        
    ExistenAdjs = (cont > 0)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ExistenAdjs", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function IncrementarColumna() As String
Dim sCod As String
sCod = "Public Function IncrementarColumna(ByVal sCol As String, Optional ByVal iIncr As Integer) As String"
sCod = sCod & vbLf & "Dim sDecena As String"
sCod = sCod & vbLf & "Dim sUnidad As String"
sCod = sCod & vbLf & "Dim idesvA As Integer"
sCod = sCod & vbLf & "If Len(sCol) = 1 Then"
sCod = sCod & vbLf & "    idesvA = iIncr + (Asc(sCol) - 65)"
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    sDecena = Left(sCol, 1)"
sCod = sCod & vbLf & "    sUnidad = Right(sCol, 1)"
sCod = sCod & vbLf & "    idesvA = (Asc(sDecena) - 64) * 26 + Asc(sUnidad) - 65 + iIncr"
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "If idesvA \ 26 > 0 Then"
sCod = sCod & vbLf & "    sDecena = Chr(65 + idesvA \ 26 - 1)"
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    sDecena = """
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "sUnidad = Chr(65 + idesvA Mod 26)"
sCod = sCod & vbLf & "IncrementarColumna = sDecena & sUnidad"
sCod = sCod & vbLf & "End Function"
IncrementarColumna = sCod
End Function

Private Function ComprobarAdjudicacionesDeProve(ByVal sProve As String) As Boolean
Dim oGrupo As CGrupo
Dim sCod As String
Dim bProvVisible As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oOrigen.m_oVistaSeleccionada.ConfVistasProceProv.Item(CStr(sProve)).Visible = TipoProvVisible.NoVisible Then
        'no visible
        bProvVisible = False
    Else
        'Depende de "Ocultar proveedores sin ofertas" y "Ocultar proveedores no adjudicables"
        bProvVisible = True
        If g_oOrigen.m_oVistaSeleccionada.OcultarProvSinOfe = True Then
            If Not g_oOrigen.m_oProcesoSeleccionado.Ofertas Is Nothing Then
                If g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(CStr(sProve)) Is Nothing Then
                    bProvVisible = False
                End If
            Else
                bProvVisible = False
            End If
        End If
        
        If bProvVisible = True And g_oOrigen.m_oVistaSeleccionada.OcultarNoAdj = True Then
            For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
                sCod = CStr(oGrupo.Codigo) & sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
                If Not g_oOrigen.m_oAdjs Is Nothing Then
                    If Not g_oOrigen.m_oAdjs.Item(sCod) Is Nothing Then
                        If g_oOrigen.m_oAdjs.Item(sCod).Adjudicado <> 0 Then
                            bProvVisible = True
                            Exit For
                        End If
                    End If
                End If
            Next
        End If
    End If
    
    ComprobarAdjudicacionesDeProve = bProvVisible
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ComprobarAdjudicacionesDeProve", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function TraducirPrecioFormula(ByVal sPrecio As String) As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case sPrecio
Case "+"
    TraducirPrecioFormula = "@suma@"
Case "-"
    TraducirPrecioFormula = "@resta@"
Case "*"
    TraducirPrecioFormula = "@mult@"
Case "/"
    TraducirPrecioFormula = "@div@"
Case "+%"
    TraducirPrecioFormula = "@pormas@"
Case "-%"
    TraducirPrecioFormula = "@pormenos@"
End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "TraducirPrecioFormula", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function TraducirPrecio(ByVal oatrib As CAtributo, ByVal oOfer As COferta, ByVal sProve As String, Optional ByVal sGrupo As String, Optional ByVal lItem As Long) As Variant
Dim vValor As Variant
Dim sCod As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Select Case oatrib.ambito
Case 1
    If oOfer.AtribProcOfertados Is Nothing Then
        TraducirPrecio = ""
        Exit Function
    End If
    If oOfer.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
        TraducirPrecio = ""
        Exit Function
    End If
    If Not IsNumeric(oOfer.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum) Then
          vValor = ""
    Else
        Select Case oatrib.PrecioFormula
        Case "+", "-"
            vValor = oOfer.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum / oOfer.Cambio
        Case Else
            vValor = oOfer.AtribProcOfertados.Item(CStr(oatrib.idAtribProce)).valorNum
        End Select
    End If
Case 2
    sCod = CStr(sGrupo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(sGrupo)))
    If oOfer.AtribGrOfertados Is Nothing Then
        TraducirPrecio = ""
        Exit Function
    End If
    If oOfer.AtribGrOfertados.Item(sCod & CStr(oatrib.idAtribProce)) Is Nothing Then
        TraducirPrecio = ""
        Exit Function
    End If
    If Not IsNumeric(oOfer.AtribGrOfertados.Item(sCod & CStr(oatrib.idAtribProce)).valorNum) Then
          vValor = ""
    Else
        Select Case oatrib.PrecioFormula
        Case "+", "-"
            vValor = oOfer.AtribGrOfertados.Item(sCod & CStr(oatrib.idAtribProce)).valorNum / oOfer.Cambio
        Case Else
            vValor = oOfer.AtribGrOfertados.Item(sCod & CStr(oatrib.idAtribProce)).valorNum
        End Select
    End If
Case 3
    sCod = CStr(lItem) & "$" & CStr(oatrib.idAtribProce)
    If oOfer.AtribItemOfertados Is Nothing Then
        TraducirPrecio = ""
        Exit Function
    End If
    If oOfer.AtribItemOfertados.Item(sCod) Is Nothing Then
        TraducirPrecio = ""
        Exit Function
    End If
    If Not IsNumeric(oOfer.AtribItemOfertados.Item(sCod).valorNum) Then
          vValor = ""
    Else
        Select Case oatrib.PrecioFormula
        Case "+", "-"
            vValor = oOfer.AtribItemOfertados.Item(sCod).valorNum / oOfer.Cambio
        Case Else
            vValor = oOfer.AtribItemOfertados.Item(sCod).valorNum
        End Select
    End If
End Select

If vValor = "" Then
    TraducirPrecio = ""
Else
    TraducirPrecio = ConvertirParaUsarEnMacro(vValor)
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "TraducirPrecio", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function ProvVisible(ByVal sProv As String) As Boolean
    Dim i As Integer
    'Si en el formulario de origen est� visible la vista inicial,para saber si
    'tenemos que mostrar o no el proveedor comprobamos si est� visible en la
    'grid sdbgGruposProve
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 1 To g_oOrigen.sdbgGruposProve.Groups.Count - 1
        If g_oOrigen.sdbgGruposProve.Groups(i).TagVariant = sProv Then
            ProvVisible = g_oOrigen.sdbgGruposProve.Groups(i).Visible
            Exit Function
        End If
    Next i
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "ProvVisible", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
Private Function EstaAplicado(ByVal lAtrib As Long, Optional ByVal sCod As String) As Boolean
Dim oAtG As CAtribTotalGrupo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not g_oOrigen.m_oAtribsFormulas.Item(CStr(lAtrib)) Is Nothing Then
    With g_oOrigen.m_oAtribsFormulas.Item(CStr(lAtrib))
        If .PrecioAplicarA = 1 Then
            For Each oAtG In .AtribTotalGrupo
                If oAtG.codgrupo = sCod Then
                    If oAtG.UsarPrec = 1 Then
                        EstaAplicado = True
                    Else
                        EstaAplicado = False
                    End If
                    Exit For
                End If
            Next
        Else
            If .UsarPrec = 1 Then
                EstaAplicado = True
            Else
                EstaAplicado = False
            End If
        End If
    End With
Else
    EstaAplicado = False
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "EstaAplicado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function Comp_Item_ObtenerValorAtrib() As String
Dim sCod As String

    '@@PENDIENTE PARA M�S DE LA COLUMNA Z
    
sCod = "Function ObtenerValorAtrib(ByVal sCodAtrib As String, ByVal sCodProve As String, ByRef sTipoOperacion As String, ByRef sComentario As String, ByVal iAmbito As Integer) As Variant"
sCod = sCod & vbLf & "Dim sCelda As String"
sCod = sCod & vbLf & "Dim I As Integer"
sCod = sCod & vbLf & "Dim j As Integer"
sCod = sCod & vbLf & "Dim iNumGrupos As Integer"
sCod = sCod & vbLf & "Dim bEncontrado As Boolean"
sCod = sCod & vbLf & "Dim xlsheet As Object"
sCod = sCod & vbLf & "Dim vValor As Variant"
sCod = sCod & vbLf & "Dim sLetra As String"
sCod = sCod & vbLf & ""
sCod = sCod & vbLf & "iNumGrupos = 1"
sCod = sCod & vbLf & "Select Case iAmbito"
sCod = sCod & vbLf & "Case 1"
sCod = sCod & vbLf & "    Set xlsheet = Worksheets(""@P@R@O@C@E@"")"
sCod = sCod & vbLf & "    sCelda = ""B1"""
sCod = sCod & vbLf & "    sLetra = ""B"""
sCod = sCod & vbLf & "    I = 1"
sCod = sCod & vbLf & "    With xlsheet"
sCod = sCod & vbLf & "        While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "            If .Range(sCelda).Value = sCodProve Then"
sCod = sCod & vbLf & "                bEncontrado = True"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                sLetra = IncrementarColumna(sLetra, 1)"
sCod = sCod & vbLf & "                sCelda = sLetra & ""1"""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Wend"
sCod = sCod & vbLf & "        If bEncontrado Then"
sCod = sCod & vbLf & "            bEncontrado = False"
sCod = sCod & vbLf & "          sCelda = sLetra & ""2"""
sCod = sCod & vbLf & "          I = 2"
sCod = sCod & vbLf & "            While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "                If .Range(sCelda).Value = sCodAtrib Then"
sCod = sCod & vbLf & "                    bEncontrado = True"
sCod = sCod & vbLf & "                Else"
sCod = sCod & vbLf & "                    I = I + 2"
sCod = sCod & vbLf & "                    sCelda = sLetra & I"
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Wend"
sCod = sCod & vbLf & "            If bEncontrado Then"
sCod = sCod & vbLf & "                sCelda = sLetra & CStr(I + 1)"
sCod = sCod & vbLf & "                vValor = .Range(sCelda).Value"
sCod = sCod & vbLf & "                sTipoOperacion = .Range(""A"" & I)"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                vValor = """""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "          vValor = """""
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    End With"
sCod = sCod & vbLf & "Case 2 'Ambito grupo"
sCod = sCod & vbLf & "    Set xlsheet = Worksheets(""@G@R@U@"")"
sCod = sCod & vbLf & "    sCelda = ""C1"""
sCod = sCod & vbLf & "    sLetra = ""C"""
sCod = sCod & vbLf & "    I = 1"
sCod = sCod & vbLf & "    With xlsheet"
sCod = sCod & vbLf & "        While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "            If .Range(sCelda).Value = sCodProve Then"
sCod = sCod & vbLf & "                bEncontrado = True"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                sLetra = IncrementarColumna(sLetra, 1)"
sCod = sCod & vbLf & "                sCelda = sLetra & ""1"""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Wend"
sCod = sCod & vbLf & "        If bEncontrado Then"
sCod = sCod & vbLf & "            bEncontrado = False"
sCod = sCod & vbLf & "            sCelda = sLetra & ""2"""
sCod = sCod & vbLf & "            I = 2"
sCod = sCod & vbLf & "            While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "                If .Range(sCelda).Value = sCodAtrib Then"
sCod = sCod & vbLf & "                    bEncontrado = True"
sCod = sCod & vbLf & "                Else"
sCod = sCod & vbLf & "                    I = I + iNumGrupos + 1"
sCod = sCod & vbLf & "                    sCelda = sLetra & I"
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Wend"
sCod = sCod & vbLf & "            If bEncontrado Then"
sCod = sCod & vbLf & "                    If .Range(""B"" & I + 1).Value = """ & g_oOrigen.g_ogrupo.Codigo & """ Then"
sCod = sCod & vbLf & "                        sCelda = sLetra & CStr(I + 1)"
sCod = sCod & vbLf & "                        vValor = .Range(sCelda).Value"
sCod = sCod & vbLf & "                        sTipoOperacion = .Range(""A"" & I + 1)"
sCod = sCod & vbLf & "                    End If"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                vValor = """""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "          vValor = """""
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    End With"
sCod = sCod & vbLf & "Case 3"
sCod = sCod & vbLf & "    Set xlsheet = Worksheets(""@I@T@E@M@"")"
sCod = sCod & vbLf & "    If Not xlsheet Is Nothing Then"
sCod = sCod & vbLf & "    I = 1"
sCod = sCod & vbLf & "    sCelda = ""C1"""
sCod = sCod & vbLf & "    sLetra = ""C"""
sCod = sCod & vbLf & "    With xlsheet"
sCod = sCod & vbLf & "        While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "            If .Range(sCelda).Value = sCodProve Then"
sCod = sCod & vbLf & "                bEncontrado = True"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                sLetra = IncrementarColumna(sLetra, 1)"
sCod = sCod & vbLf & "                sCelda = sLetra & ""1"""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Wend"
sCod = sCod & vbLf & "        If bEncontrado Then"
sCod = sCod & vbLf & "            bEncontrado = False"
sCod = sCod & vbLf & "            sCelda =  ""A2"""
sCod = sCod & vbLf & "            I = 2"
sCod = sCod & vbLf & "            While .Range(sCelda).Value <> """" And Not bEncontrado"
sCod = sCod & vbLf & "                If .Range(sCelda).Value = sCodAtrib Then"
sCod = sCod & vbLf & "                    bEncontrado = True"
sCod = sCod & vbLf & "                Else"
sCod = sCod & vbLf & "                    I = I + 1"
sCod = sCod & vbLf & "                    sCelda = ""A"" & I"
sCod = sCod & vbLf & "                End If"
sCod = sCod & vbLf & "            Wend"
sCod = sCod & vbLf & "            If bEncontrado Then"
sCod = sCod & vbLf & "                        sCelda = sLetra & CStr(I)"
sCod = sCod & vbLf & "                        vValor = .Range(sCelda).Value"
sCod = sCod & vbLf & "                        sTipoOperacion = .Range(""B"" & I)"
sCod = sCod & vbLf & "            Else"
sCod = sCod & vbLf & "                vValor = """""
sCod = sCod & vbLf & "            End If"
sCod = sCod & vbLf & "        Else"
sCod = sCod & vbLf & "          vValor = """""
sCod = sCod & vbLf & "        End If"
sCod = sCod & vbLf & "    End With"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        vValor = """""
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "End Select"
sCod = sCod & vbLf & "If vValor <> """" Then"
sCod = sCod & vbLf & "    ObtenerValorAtrib = vValor"
sCod = sCod & vbLf & "    sComentario = ""X"""
sCod = sCod & vbLf & "Else"
sCod = sCod & vbLf & "    Select Case sTipoOperacion"
sCod = sCod & vbLf & "    Case ""@suma@"", ""@resta@"""
sCod = sCod & vbLf & "        ObtenerValorAtrib = 0"
sCod = sCod & vbLf & "    Case ""@mult@"", ""@div@"""
sCod = sCod & vbLf & "        ObtenerValorAtrib = 1"
sCod = sCod & vbLf & "    Case ""@pormas@"", ""@pormenos@"""
sCod = sCod & vbLf & "        ObtenerValorAtrib = 0"
sCod = sCod & vbLf & "    Case Else"
sCod = sCod & vbLf & "        ObtenerValorAtrib = """""
sCod = sCod & vbLf & "    End Select"
sCod = sCod & vbLf & "    sComentario = """""
sCod = sCod & vbLf & "End If"
sCod = sCod & vbLf & "End Function"

Comp_Item_ObtenerValorAtrib = sCod
End Function

Private Function DevolverItemsConfirmados(ByVal oGrup As CGrupo) As Integer
Dim oItem As CItem
Dim iNumItemsEnHoja As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
iNumItemsEnHoja = 0
For Each oItem In oGrup.Items
    If oItem.Confirmado Then
        iNumItemsEnHoja = iNumItemsEnHoja + 1
    End If
Next
DevolverItemsConfirmados = iNumItemsEnHoja
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "DevolverItemsConfirmados", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Prop�sito de la funci�n
''' </summary>
''' <param name="ParametroEntrada1">Explicaci�n par�metro 1</param>
''' <param name=" ParametroEntrada2">Explicaci�n par�metro 2</param>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
''' <revision>LTG 21/11/2011</revision>

Private Sub DevolverImporteParcialCerrado(ByVal sProve As String, ByVal oItems As CItems, ByVal oAdjs As CAdjudicaciones _
, ByRef dAdjudicado As Double, ByRef dAhorrado As Double, ByRef dPresupuestado As Double)
    Dim oItem As CItem
    Dim oAdj As CAdjudicacion
    Dim sCod As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    dAdjudicado = 0
    dAhorrado = 0
    dPresupuestado = 0
    
    If oItems.Count = 0 Then
    Else
        For Each oItem In oItems
            If oItem.Confirmado And oItem.Cerrado Then
                sCod = sProve & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProve))
                Set oAdj = oAdjs.Item(CStr(oItem.Id) & sCod)
                If Not oAdj Is Nothing Then
                    If NullToDbl0(oAdj.Porcentaje) <> 0 Then
                        dAdjudicado = dAdjudicado + (oAdj.ImporteAdj / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve).Cambio)
                        dAhorrado = dAhorrado + ((oItem.Precio * oItem.Cantidad) * (oAdj.Porcentaje / 100)) - (oAdj.ImporteAdj / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve).Cambio)
                        dPresupuestado = dPresupuestado + (oItem.Precio * oItem.Cantidad) * (oAdj.Porcentaje / 100) ' / g_oOrigen.m_oProcesoSeleccionado.Ofertas.Item(sProve).Cambio
                    End If
                End If
            End If
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "DevolverImporteParcialCerrado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' A�ade al m�dulo: Option Explicit y las variables de m�dulo.
''' </summary>
''' <param name></param>
''' <returns>El c�digo a copiar en el m�dulo de la hoja excel</returns>
''' <remarks>Llamada desde:ObtenerHojaComparativa; Tiempo m�ximo:0,1</remarks>
Private Function CabeceraModulo() As String
Dim sCodigo As String

sCodigo = "Option Explicit"
sCodigo = sCodigo & vbLf & "Private m_oGruposProve As CGruposProve"
sCodigo = sCodigo & vbLf & "Private m_arrProve() As String"

CabeceraModulo = sCodigo
End Function

''' <summary>
''' A�ade al m�dulo: ObtenerProveedorItem.
''' </summary>
''' <param name></param>
''' <returns>El c�digo a copiar en el m�dulo de la hoja excel</returns>
''' <remarks>Llamada desde:ObtenerHojaComparativa; Tiempo m�ximo:0,1</remarks>

Function ObtenerProveedorItem() As String
Dim sCodigo As String

sCodigo = "Function ObtenerProveedorItem(sCodgrupo, iFila, sLetra) As String"
sCodigo = sCodigo & vbLf & "Dim sCodProve As String"
sCodigo = sCodigo & vbLf & "Dim bCondicion As Boolean"
sCodigo = sCodigo & vbLf & "Dim sCeldaProve As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaPrimerProve As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaCant As String"
sCodigo = sCodigo & vbLf & "Dim sCeldaPrecio As String"
sCodigo = sCodigo & vbLf & "Dim xlsheet As Worksheet"
sCodigo = sCodigo & vbLf & "Dim iFilaProve As Integer"
sCodigo = sCodigo & vbLf & "Dim vAux As Variant"
sCodigo = sCodigo & vbLf & "Dim sAux As String"
sCodigo = sCodigo & vbLf & "    Set xlsheet = Excel.Sheets.Item(sCodgrupo)"
sCodigo = sCodigo & vbLf & "    iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 7"
sCodigo = sCodigo & vbLf & "    sCeldaProve = sLetra & iFilaProve"
sCodigo = sCodigo & vbLf & "    If xlsheet.Range(sCeldaProve).Value <> """" Then"
sCodigo = sCodigo & vbLf & "        vAux = Split97(xlsheet.Range(sCeldaProve).Value, "" - "")"
sCodigo = sCodigo & vbLf & "        sAux = vAux(0)"
sCodigo = sCodigo & vbLf & "        ObtenerProveedorItem = sAux"
sCodigo = sCodigo & vbLf & "    Else"
sCodigo = sCodigo & vbLf & "        ObtenerProveedorItem = """""
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "End Function"

ObtenerProveedorItem = sCodigo
End Function

''' <summary>
''' A�ade al m�dulo: CalcularImporteAdjudicadoGrupoProve.
''' </summary>
''' <param name></param>
''' <returns>El c�digo a copiar en el m�dulo de la hoja excel</returns>
''' <remarks>Llamada desde:ObtenerHojaComparativa; Tiempo m�ximo:0,1</remarks>

Function CalcularImporteAdjudicadoGrupoProve() As String
Dim sCodigo As String
Dim oGrupo As CGrupo
Dim bMostrarGr As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
sCodigo = "Function CalcularImporteAdjudicadoGrupoProve(sCodgrupo, sLetra) As Double"
sCodigo = sCodigo & vbLf & "Dim dImporte As Double"
sCodigo = sCodigo & vbLf & "Dim dImporteAux As Double"
sCodigo = sCodigo & vbLf & "Dim iNumItems As Integer"
sCodigo = sCodigo & vbLf & "Dim i As Integer"
sCodigo = sCodigo & vbLf & "Set xlsheet = Excel.Sheets.Item(sCodgrupo)"
sCodigo = sCodigo & vbLf & "Select Case sCodGrupo"
        
For Each oGrupo In g_oOrigen.m_oProcesoSeleccionado.Grupos
    bMostrarGr = True
    If g_oOrigen.m_oProcesoSeleccionado.AdminPublica = True Then
        If g_oOrigen.m_oProcesoSeleccionado.Sobres.Item(CStr(oGrupo.Sobre)).Estado = 0 Then
            bMostrarGr = False
        End If
    End If
    If bMostrarGr = True Then
        sCodigo = sCodigo & vbLf & "    Case """ & oGrupo.Codigo & """"
        sCodigo = sCodigo & vbLf & "        iNumItems =" & DevolverItemsConfirmados(oGrupo)
    End If
Next
sCodigo = sCodigo & vbLf & "End Select"
sCodigo = sCodigo & vbLf & "iFilaProve = " & basPublic.gParametrosInstalacion.giLongCabComp & "+ 7"
sCodigo = sCodigo & vbLf & "sCeldaProve = sLetra & iFilaProve"
sCodigo = sCodigo & vbLf & "If xlsheet.Range(sCeldaProve).Value <> """" Then"
sCodigo = sCodigo & vbLf & "    iFila = " & basPublic.gParametrosInstalacion.giLongCabComp & " + 11 "
sCodigo = sCodigo & vbLf & "    dImporteAux = 0"
sCodigo = sCodigo & vbLf & "    bAplicar = False"
sCodigo = sCodigo & vbLf & "    For i = 0 To iNumItems - 1"
sCodigo = sCodigo & vbLf & "        If xlsheet.Range(sLetra & iFila).Value <> """" Then"
sCodigo = sCodigo & vbLf & "            dImporteAux = dImporteAux + AdjudicadoItem(sCodgrupo, iFila, sLetra)"
sCodigo = sCodigo & vbLf & "            bAplicar = True"
sCodigo = sCodigo & vbLf & "        End If"
sCodigo = sCodigo & vbLf & "        iFila = iFila + 3"
sCodigo = sCodigo & vbLf & "    Next i"
sCodigo = sCodigo & vbLf & "    sCodProve = ObtenerProveedorItem(sCodgrupo, iFila, sLetra)"
sCodigo = sCodigo & vbLf & "    If bAplicar Then"
sCodigo = sCodigo & vbLf & "        dImporteAux = dImporteAux + AplicarAtributosGrupo_Proveedor(sCodgrupo, dImporteAux, sCodProve, iFila)"
sCodigo = sCodigo & vbLf & "    End If"
sCodigo = sCodigo & vbLf & "End If"
sCodigo = sCodigo & vbLf & "    CalcularImporteAdjudicadoGrupoProve = dImporte"
sCodigo = sCodigo & vbLf & "End Function"

CalcularImporteAdjudicadoGrupoProve
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "CalcularImporteAdjudicadoGrupoProve", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Incrementa una columna las veces que le indiquemos seg�n parametro.
''' </summary>
''' <param name="sCol">Columna</param>
''' <param name="iIncr">Incremento</param>
''' <returns>La nueva columna</returns>
''' <remarks>Llamada desde:ObtenerHojaComparativaItem; Tiempo m�ximo:0,1</remarks>

Private Function SiguienteColumna(ByVal sCol As String, Optional ByVal iIncr As Integer) As String
Dim sDecena As String
Dim sUnidad As String
Dim idesvA As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sCol) = 1 Then
        idesvA = iIncr + (Asc(sCol) - 65)
    Else
        sDecena = Left(sCol, 1)
        sUnidad = Right(sCol, 1)
        idesvA = (Asc(sDecena) - 64) * 26 + Asc(sUnidad) - 65 + iIncr
    End If
    
    If idesvA \ 26 > 0 Then
        sDecena = Chr(65 + idesvA \ 26 - 1)
    Else
        sDecena = ""
    End If
    
    sUnidad = Chr(65 + idesvA Mod 26)
    SiguienteColumna = sDecena & sUnidad
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmRESREUHojaAdj", "SiguienteColumna", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' A�ade al m�dulo: ObtenerProveedorItem.
''' </summary>
''' <param name></param>
''' <returns>El c�digo a copiar en el m�dulo de la hoja excel</returns>
''' <remarks>Llamada desde:ObtenerHojaComparativaItem; Tiempo m�ximo:0,1</remarks>

Private Function Comp_Item_ObtenerProveedorItem() As String
Dim sCod As String

sCod = "Function ObtenerProveedorItem(iFilaProve) As String"
sCod = sCod & vbLf & "Dim xlsheet As Worksheet"
sCod = sCod & vbLf & "Dim vAux As Variant"
sCod = sCod & vbLf & "Dim sAux As String"
sCod = sCod & vbLf & "    Set xlsheet = Excel.Sheets.Item(4)"
sCod = sCod & vbLf & "    If xlsheet.Range(""A"" & iFilaProve).Value <> """" Then"
sCod = sCod & vbLf & "        vAux = Split97(xlsheet.Range(""A"" & iFilaProve).Value, "" - "")"
sCod = sCod & vbLf & "        sAux = vAux(0)"
sCod = sCod & vbLf & "        ObtenerProveedorItem = sAux"
sCod = sCod & vbLf & "    Else"
sCod = sCod & vbLf & "        ObtenerProveedorItem = """""
sCod = sCod & vbLf & "    End If"
sCod = sCod & vbLf & "End Function"

Comp_Item_ObtenerProveedorItem = sCod
End Function
