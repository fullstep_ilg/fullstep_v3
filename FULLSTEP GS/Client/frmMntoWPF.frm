VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{57A1F96E-5A81-4063-8193-6E7BB254EDBD}#1.0#0"; "DXAnimatedGIF.ocx"
Begin VB.Form frmMntoWPF 
   Caption         =   "Monedas"
   ClientHeight    =   8115
   ClientLeft      =   5970
   ClientTop       =   2340
   ClientWidth     =   12525
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmMntoWPF.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8115
   ScaleWidth      =   12525
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5115
      Left            =   0
      Picture         =   "frmMntoWPF.frx":014A
      ScaleHeight     =   5115
      ScaleWidth      =   7365
      TabIndex        =   1
      Top             =   0
      Width           =   7365
      Begin DXAnimatedGIF.DXGif DXGif1 
         Height          =   300
         Left            =   2070
         TabIndex        =   2
         Top             =   2280
         Width           =   3435
         _ExtentX        =   6059
         _ExtentY        =   529
         BackColor       =   12632256
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "La p�gina se est� cargando"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   960
         TabIndex        =   6
         Top             =   840
         Width           =   5655
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Espere, por favor."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   960
         TabIndex        =   5
         Top             =   1200
         Width           =   5655
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "www.fullstep.com - � 1997-2010 FULLSTEP NETWORKS"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   4500
         Width           =   4965
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Todos los derechos reservados"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   180
         TabIndex        =   3
         Top             =   4740
         Width           =   3615
      End
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   7500
      Top             =   30
   End
   Begin SHDocVwCtl.WebBrowser WebBrowser1 
      Height          =   4215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   12255
      ExtentX         =   21616
      ExtentY         =   7435
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
End
Attribute VB_Name = "frmMntoWPF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmMON
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 07/07/2010 (EPB)

Public Enum TipoMntoWPF
    Monedas = 0
    Paises = 1
    FormasPago = 2
    ViasPago = 3
    Unidades = 4
    TiposImpuestos = 5
End Enum

Public g_TipoMnto As TipoMntoWPF

Option Explicit

'Private Sub WriteToLogFile(logentry As String)
'
'Dim fn As Integer
'fn = FreeFile
'Open App.Path & "\MonedasLog.txt" For Append As #fn
'Write #fn, Now & ": " & logentry
'Close #fn
'
'End Sub
'' Coleccion de monedas

Private Sub Arrange()

    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario

    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 14250 Then   'de tama�o de la ventana
            Me.Width = 14250        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        'If Me.Height <= 2000 Then  'cuando no se maximiza ni
        '    Me.Height = 2100       'minimiza la ventana,
        '    Exit Sub               'WindowState=0, pues cuando esto
        'End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width


End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TRANSICION, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        Label1.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        Label3.caption = Replace(Ador(0).Value, "@#@", Year(Now))
        Ador.MoveNext
        Label4.caption = Ador(0).Value
        Ador.MoveNext
        If g_TipoMnto = Monedas Then
            caption = Ador(0).Value '5 Monedas
        ElseIf g_TipoMnto = Paises Then
            Ador.MoveNext
            caption = Ador(0).Value '6 Paises
        ElseIf g_TipoMnto = FormasPago Then
            Ador.MoveNext
            Ador.MoveNext
            caption = Ador(0).Value '7 Pagos
        ElseIf g_TipoMnto = ViasPago Then
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            caption = Ador(0).Value '8 Vias Pago
        ElseIf g_TipoMnto = Unidades Then
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            caption = Ador(0).Value '9 Unidades
        ElseIf g_TipoMnto = TiposImpuestos Then
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            caption = Ador(0).Value '10 Tipos de impuestos
        End If

        Ador.Close
    End If
    Set Ador = Nothing

End Sub

Private Sub Form_Load()
    Dim sCadena As String
    Dim sPantalla As String

    DXGif1.filename = App.Path & "\BarraCargando.gif"

    WebBrowser1.Visible = True

    Picture1.Refresh

    CargarRecursos
    Timer1.Interval = 2000
'    WriteToLogFile "-------------------------------------------"
    sCadena = gParametrosGenerales.gcolRutas("GSClientUI") & "?usu=" & oUsuarioSummit.Cod & "&session=" & oSesionSummit.Id & "&pantalla="
    Select Case g_TipoMnto
        Case TipoMntoWPF.Monedas
            sPantalla = "Monedas"
        Case TipoMntoWPF.Paises
            sPantalla = "Paises"
        Case TipoMntoWPF.FormasPago
            sPantalla = "FormaPago"
        Case TipoMntoWPF.ViasPago
            sPantalla = "ViaPago"
        Case TipoMntoWPF.Unidades
            sPantalla = "Unidades"
        Case TipoMntoWPF.TiposImpuestos
            sPantalla = "Impuestos"
    End Select
    sCadena = sCadena & sPantalla
'    WriteToLogFile "Antes de llamar Navigate VB6"
    WebBrowser1.Navigate2 sCadena
    DoEvents
    Timer1.Enabled = True

End Sub



Private Sub Form_Resize()
    WebBrowser1.Left = 0
    WebBrowser1.Top = 0
    WebBrowser1.Width = Me.Width
    WebBrowser1.Height = Me.Height
    Picture1.Top = WebBrowser1.Height / 2 - Picture1.Height / 2
    Picture1.Left = WebBrowser1.Width / 2 - Picture1.Width / 2
End Sub

Private Sub Timer1_Timer()
    Picture1.Refresh
    If WebBrowser1.ReadyState = READYSTATE_COMPLETE Then
        Picture1.Visible = False
        Timer1.Enabled = False
        'WriteToLogFile "Termina la carga READYSTATE_COMPLETE"
    End If
End Sub


