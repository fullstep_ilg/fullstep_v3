VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmVARCalCompuestaNivel4 
   Caption         =   "DVariables de calidad"
   ClientHeight    =   4965
   ClientLeft      =   480
   ClientTop       =   1770
   ClientWidth     =   19530
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmVARCalCompuestaNivel4.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4965
   ScaleWidth      =   19530
   Begin VB.Frame fraConfiguracion 
      Caption         =   "DSelecci�n del modo de c�lculo para unidades de negocio de nivel superior"
      ForeColor       =   &H00FF0000&
      Height          =   855
      Left            =   10000
      TabIndex        =   14
      Top             =   3650
      Width           =   8500
      Begin VB.OptionButton optOpcionConf 
         Caption         =   "DEvaluar f�rmula"
         Height          =   255
         Index           =   0
         Left            =   100
         TabIndex        =   17
         Top             =   360
         Value           =   -1  'True
         Width           =   2000
      End
      Begin VB.OptionButton optOpcionConf 
         Caption         =   "DMedia ponderada seg�n:"
         Height          =   255
         Index           =   1
         Left            =   2000
         TabIndex        =   16
         Top             =   360
         Width           =   2500
      End
      Begin VB.OptionButton optOpcionConf 
         Caption         =   "DNo calcular"
         Height          =   255
         Index           =   2
         Left            =   6400
         TabIndex        =   15
         Top             =   360
         Width           =   2000
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcVarPond 
         Height          =   285
         Left            =   4500
         TabIndex        =   18
         Top             =   360
         Width           =   1785
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "COD"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6800
         Columns(1).Caption=   "DEN"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3149
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   345
      Left            =   135
      TabIndex        =   5
      Top             =   4590
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddOrigen 
      Height          =   915
      Left            =   405
      TabIndex        =   7
      Top             =   1395
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVARCalCompuestaNivel4.frx":014A
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   6165
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "ID"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "FORM"
      Columns(3).Name =   "FORM"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipo 
      Height          =   915
      Left            =   2655
      TabIndex        =   6
      Top             =   1440
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmVARCalCompuestaNivel4.frx":0166
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3545
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
   End
   Begin VB.Frame fraSubVariables 
      Caption         =   "DSubvariables"
      ForeColor       =   &H00FF0000&
      Height          =   3550
      Left            =   135
      TabIndex        =   9
      Top             =   45
      Width           =   10500
      Begin SSDataWidgets_B.SSDBDropDown sdbddSubTipo 
         Height          =   915
         Left            =   5160
         TabIndex        =   12
         Top             =   1440
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmVARCalCompuestaNivel4.frx":0182
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3916
         Columns(0).Name =   "NOMBRE"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "ID"
         Columns(1).Name =   "ID"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
      End
      Begin VB.PictureBox picBotonesSubvar 
         BorderStyle     =   0  'None
         Height          =   325
         Left            =   120
         ScaleHeight     =   330
         ScaleWidth      =   1275
         TabIndex        =   10
         Top             =   240
         Width           =   1275
         Begin VB.CommandButton cmdBajaSubvar 
            Height          =   300
            Left            =   840
            Picture         =   "frmVARCalCompuestaNivel4.frx":019E
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   0
            Width           =   350
         End
         Begin VB.CommandButton cmdAnyaSubvar 
            Height          =   300
            Left            =   0
            Picture         =   "frmVARCalCompuestaNivel4.frx":04E0
            Style           =   1  'Graphical
            TabIndex        =   1
            Top             =   0
            Width           =   350
         End
         Begin VB.CommandButton cmdElimSubvar 
            Height          =   300
            Left            =   420
            Picture         =   "frmVARCalCompuestaNivel4.frx":0562
            Style           =   1  'Graphical
            TabIndex        =   2
            Top             =   0
            Width           =   350
         End
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgSubVariables 
         Height          =   2880
         Left            =   120
         TabIndex        =   0
         Top             =   600
         Width           =   10245
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   15
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmVARCalCompuestaNivel4.frx":05F4
         stylesets(1).Name=   "StringTachado"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   -1  'True
         EndProperty
         stylesets(1).Picture=   "frmVARCalCompuestaNivel4.frx":0610
         stylesets(2).Name=   "Bloqueado"
         stylesets(2).BackColor=   12632256
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmVARCalCompuestaNivel4.frx":062C
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   15
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1773
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   5
         Columns(2).Width=   3201
         Columns(2).Caption=   "TIPO"
         Columns(2).Name =   "TIPO"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   1535
         Columns(3).Name =   "SUB"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   4
         Columns(3).ButtonsAlways=   -1  'True
         Columns(4).Width=   3122
         Columns(4).Caption=   "SUBTIPO"
         Columns(4).Name =   "SUBTIPO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   2593
         Columns(5).Caption=   "ORIGEN"
         Columns(5).Name =   "ORIGEN"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   3200
         Columns(6).Caption=   "VALOR_DEF"
         Columns(6).Name =   "VALOR_DEF"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "Val. cert. sin cumpl."
         Columns(7).Name =   "CERT_VALOR_SINCUMPL"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   1720
         Columns(8).Caption=   "Puntuaci�n"
         Columns(8).Name =   "PUNT"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).Style=   4
         Columns(8).ButtonsAlways=   -1  'True
         Columns(9).Width=   1720
         Columns(9).Caption=   "Materiales"
         Columns(9).Name =   "MAT"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Style=   4
         Columns(9).ButtonsAlways=   -1  'True
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "ID_TIPO"
         Columns(10).Name=   "ID_TIPO"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "ID_ORIGEN"
         Columns(11).Name=   "ID_ORIGEN"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "ID_FORM"
         Columns(12).Name=   "ID_FORM"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "ID_SUBTIPO"
         Columns(13).Name=   "ID_SUBTIPO"
         Columns(13).DataField=   "Column 13"
         Columns(13).DataType=   8
         Columns(13).FieldLen=   256
         Columns(14).Width=   3200
         Columns(14).Visible=   0   'False
         Columns(14).Caption=   "BAJALOG"
         Columns(14).Name=   "BAJALOG"
         Columns(14).DataField=   "Column 14"
         Columns(14).DataType=   8
         Columns(14).FieldLen=   256
         _ExtentX        =   18071
         _ExtentY        =   5080
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFormula 
      Caption         =   "DF�rmula para calcular la puntuaci�n de la variable"
      ForeColor       =   &H00FF0000&
      Height          =   855
      Left            =   135
      TabIndex        =   8
      Top             =   3645
      Width           =   10500
      Begin VB.TextBox txtFormula 
         Height          =   300
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   8850
      End
      Begin VB.CommandButton cmdAyuda 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   9090
         Picture         =   "frmVARCalCompuestaNivel4.frx":0648
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   360
         Width           =   350
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalCompuestaNivel4.frx":0879
            Key             =   "BAJA"
            Object.Tag             =   "BAJA"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVARCalCompuestaNivel4.frx":0BCB
            Key             =   "NOBAJA"
            Object.Tag             =   "NOBAJA"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblCambios 
      AutoSize        =   -1  'True
      Caption         =   "Para guardar los cambios efectuados cierre esta ventana y pulse Guadar cambios en la ventada principal de Variables de calidad."
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   1305
      TabIndex        =   11
      Top             =   4635
      Width           =   9255
   End
End
Attribute VB_Name = "frmVARCalCompuestaNivel4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnValCertNoCumplDefecto As Integer = 0

'Variables de coleccion
Public g_oVarCal3 As CVariableCalidad
Public g_oVarCal3Mod As CVariableCalidad
Public g_bMultiIdioma As Boolean
Public g_oIdiomas As CIdiomas
Public g_bVerBaja As Boolean

'Variables privadas

'Variables de gesti�n
Private m_bRespetarColor As Boolean
Private m_bError As Boolean

'Variables de idiomas
Private m_sTipo(0 To 1) As String
Private m_sSubTipo(0 To 8) As String
Private m_sIdiErrorFormula(12) As String
Private m_sNombreDen As String
Private m_sBaja As String
Private m_sDeshacerBaja As String

Private Sub cmdAnyaSubvar_Click()
    Dim oIdioma As CIdioma
    
    'A�ade una fila para una nueva variable de nivel 2:
    sdbgSubVariables.AllowAddNew = True
    sdbgSubVariables.Scroll 0, sdbgSubVariables.Rows - sdbgSubVariables.Row
    
    If sdbgSubVariables.VisibleRows > 0 Then
        If sdbgSubVariables.VisibleRows >= sdbgSubVariables.Rows Then
            If sdbgSubVariables.VisibleRows = sdbgSubVariables.Rows Then
                sdbgSubVariables.Row = sdbgSubVariables.Rows - 1
            Else
                sdbgSubVariables.Row = sdbgSubVariables.Rows
            End If
        Else
            sdbgSubVariables.Row = sdbgSubVariables.Rows - (sdbgSubVariables.Rows - sdbgSubVariables.VisibleRows) - 1
        End If
    End If
    
    Dim i, iPosition As Byte
    
    i = sdbgSubVariables.Columns.Count - 1
    iPosition = 2
    
   
    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n):
  
    'If g_bMultiIdioma Then
       For Each oIdioma In g_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                If sdbgSubVariables.Columns(oIdioma.Cod) Is Nothing Then
                    i = i + 1
                    iPosition = iPosition + 1
                    sdbgSubVariables.Columns.Add i
                    sdbgSubVariables.Columns(i).Name = oIdioma.Cod
                    sdbgSubVariables.Columns(i).caption = oIdioma.Den
                    sdbgSubVariables.Columns(i).Position = iPosition
                    sdbgSubVariables.Columns(i).Visible = True
                End If
            Else
                sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).caption = oIdioma.Den
            End If
        Next
    'End If
    Dim multiIdiomaAnt As Boolean
    multiIdiomaAnt = g_bMultiIdioma
    g_bMultiIdioma = True
    CargarVariablesCalidad4
    g_bMultiIdioma = multiIdiomaAnt
    
    If Me.Visible Then sdbgSubVariables.SetFocus
    
    Set oIdioma = Nothing
    
End Sub

Private Sub cmdAyuda_Click(Index As Integer)

    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then
            If Me.Visible Then sdbgSubVariables.SetFocus
            Exit Sub
        End If
    End If

    MostrarFormSOLAyudaCalculos oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub cmdCerrar_Click()

If sdbgSubVariables.DataChanged Then
    sdbgSubVariables.Update
    If m_bError = True Then
        Exit Sub
    End If
End If
Unload Me
End Sub

Private Sub cmdElimSubvar_Click()
'mpg:=03/04/2009
''' <summary>
'''                 Comprueba si la variable de calidad tiene algun tipo de puntuacion (En curso o en el historico)
'''                 Si tiene en el historico --> Da de baja logica la variable
'''                 Sino si tiene en curso muestra mensaje de que tiene puntuaciones en curso y de si quieres eliminarlos tb.

''' </summary>
''' <returns>Nothing</returns>
''' <remarks>Llamada desde=Propio formulario; Tiempo m�ximo=0,3</remarks>

Dim irespuesta As Integer
Dim sCod As String
Dim oVarCal4 As CVariableCalidad
Dim oVarCal5 As CVariableCalidad
Dim rs As ADODB.Recordset
Dim bPuntCurso, bPuntHist As Boolean

    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then
            If Me.Visible Then sdbgSubVariables.SetFocus
            Exit Sub
        End If
    End If
    
    If sdbgSubVariables.Rows = 0 Then Exit Sub
    If sdbgSubVariables.SelBookmarks.Count = 0 Then sdbgSubVariables.SelBookmarks.Add sdbgSubVariables.Bookmark
    
    Set oVarCal4 = g_oVarCal3Mod.VariblesCal.Item("4" & CStr(sdbgSubVariables.Columns("ID").Value))
    If Not oVarCal4 Is Nothing Then
        Set rs = oVarCal4.ExistenPuntuaciones
        If Not rs.EOF Then
            bPuntCurso = (rs("CONT").Value > 0)
            rs.MoveNext
            bPuntHist = (rs("CONT").Value > 0)
        End If
    End If
    
    irespuesta = oMensajes.PreguntaEliminarVariableCalidad(sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Value, bPuntHist, bPuntCurso)
    If irespuesta = vbNo Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bPuntHist Then
    '***********************************************
    'Realizar la baja logica de la variable y todas sus hijas.
            
        oVarCal4.BajaLog = True
        oVarCal4.modificado = True
        If Not oVarCal4.VariblesCal Is Nothing Then
            For Each oVarCal5 In oVarCal4.VariblesCal
                oVarCal5.BajaLog = True
                oVarCal5.modificado = True
            Next
        End If

        sCod = sdbgSubVariables.Columns("COD").Value
        If g_bVerBaja = True Then
            sdbgSubVariables.Columns("BAJALOG").Value = BooleanToSQLBinary(oVarCal4.BajaLog)
            sdbgSubVariables.Refresh
        Else

            sCod = sdbgSubVariables.Columns("COD").Value
            sdbgSubVariables.DeleteSelected

        End If
        Set oVarCal4 = Nothing
        Set oVarCal5 = Nothing
    Else
        'Elimina el elemento de la collecion
        If Not g_oVarCal3Mod.VariblesCal.Item("4" & CStr(sdbgSubVariables.Columns("ID").Value)) Is Nothing Then
            g_oVarCal3Mod.VariblesCal.Remove "4" & CStr(sdbgSubVariables.Columns("ID").Value)
        End If
        sCod = sdbgSubVariables.Columns("COD").Value
        sdbgSubVariables.DeleteSelected
    End If
    
    frmVARCalidad.VisualizarGuardar
    frmVARCalCompuestaNivel3.lblCambios.Visible = True
    lblCambios.Visible = True

    If InStr(1, txtFormula.Text, sCod) <> 0 Then
        txtFormula.Forecolor = vbRed
    End If
        
    sdbgSubVariables.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgSubVariables.SetFocus
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Load()
Dim oIdioma As CIdioma
Dim i, iPosition As Byte

    If Not gParametrosGenerales.gbQAVariableMaterialAsig Then
        sdbgSubVariables.Columns("MAT").Visible = False
    End If
        
    CargarRecursos
    CargarVariablesHermanas
    PonerFieldSeparator Me
        
    i = sdbgSubVariables.Columns.Count
    iPosition = 2
    
    sdbgSubVariables.Columns.Add i
    sdbgSubVariables.Columns(i).Name = gParametrosInstalacion.gIdioma
    sdbgSubVariables.Columns(i).caption = m_sNombreDen
    sdbgSubVariables.Columns(i).Position = iPosition
    
    'Carga las columnas de los diferentes idiomas de la grid (la 1� ser� la del idioma de la aplicaci�n)
    If g_bMultiIdioma Then
        'Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
        For Each oIdioma In g_oIdiomas
            If oIdioma.Cod <> gParametrosInstalacion.gIdioma Then
                i = i + 1
                iPosition = iPosition + 1
                sdbgSubVariables.Columns.Add i
                sdbgSubVariables.Columns(i).Name = oIdioma.Cod
                sdbgSubVariables.Columns(i).caption = oIdioma.Den
                sdbgSubVariables.Columns(i).Position = iPosition
                sdbgSubVariables.Columns(i).Visible = True
            Else
                sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).caption = oIdioma.Den
            End If
        Next
    End If
        
    lblCambios.Visible = False
    sdbddTipo.AddItem ""
    sdbgSubVariables.Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
    sdbddSubTipo.AddItem ""
    sdbgSubVariables.Columns("SUBTIPO").DropDownHwnd = sdbddSubTipo.hWnd
    sdbddOrigen.AddItem ""
    sdbgSubVariables.Columns("ORIGEN").DropDownHwnd = sdbddOrigen.hWnd
    sdbgSubVariables.Columns("COD").FieldLen = gLongitudesDeCodigos.giLongCodVarCal
    
    CargarVariablesCalidad4
        
    optOpcionConf(g_oVarCal3.Opcion_Conf).Value = True
    If g_oVarCal3.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Columns("ID").Value = g_oVarCal3.IdVar_Pond
    End If
                
    Me.Width = frmVARCalidad.sdbgSubVariables.Width '13145
    Me.Height = 7000
        
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    Set oIdioma = Nothing
    
End Sub

Private Sub Form_Resize()
    'Redimensiona el formulario
    If Me.Width < 2000 Then Exit Sub
    If Me.Height < 4000 Then Exit Sub
       
    fraSubVariables.Width = Me.Width - 500
    fraSubVariables.Height = Me.Height - fraSubVariables.Top - fraFormula.Height - cmdCerrar.Height - 700
    sdbgSubVariables.Width = fraSubVariables.Width - 255
    sdbgSubVariables.Height = fraSubVariables.Height - 790 + IIf(picBotonesSubvar.Visible, 0, picBotonesSubvar.Height)
    If Not picBotonesSubvar.Visible Then
        sdbgSubVariables.Top = fraSubVariables.Top + picBotonesSubvar.Height
    Else
        sdbgSubVariables.Top = fraSubVariables.Top + picBotonesSubvar.Height + 200
    End If
    
    fraFormula.Width = fraSubVariables.Width / 2
    fraFormula.Top = fraSubVariables.Top + fraSubVariables.Height
    cmdCerrar.Top = fraFormula.Top + fraFormula.Height + 60
    lblCambios.Top = cmdCerrar.Top + 60

    fraConfiguracion.Width = fraSubVariables.Width / 2
    fraConfiguracion.Left = fraFormula.Width + 50
    fraConfiguracion.Top = fraFormula.Top
    fraConfiguracion.Height = fraFormula.Height
    sdbcVarPond.Top = optOpcionConf(1).Top + 50
    sdbcVarPond.Width = fraConfiguracion.Width / 4 - 200
    optOpcionConf(0).Width = fraConfiguracion.Width / 4 - 200
    optOpcionConf(1).Width = fraConfiguracion.Width / 4 - 200
    optOpcionConf(2).Width = fraConfiguracion.Width / 4 - 600
    optOpcionConf(0).Height = 400
    optOpcionConf(1).Height = 400
    optOpcionConf(2).Height = 400
    optOpcionConf(0).Left = 100
    optOpcionConf(1).Left = optOpcionConf(0).Left + optOpcionConf(0).Width + 100
    sdbcVarPond.Left = optOpcionConf(1).Left + optOpcionConf(1).Width + 100
    optOpcionConf(2).Left = sdbcVarPond.Left + sdbcVarPond.Width + 600
    
    If sdbgSubVariables.Columns("MAT").Visible = True Then
        sdbgSubVariables.Columns("COD").Width = sdbgSubVariables.Width * 0.08
        If Not g_bMultiIdioma Then
             sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Width = (sdbgSubVariables.Width * 0.329) - 580
        End If
        sdbgSubVariables.Columns("TIPO").Width = sdbgSubVariables.Width * 0.1
        sdbgSubVariables.Columns("SUB").Width = sdbgSubVariables.Width * 0.05
        sdbgSubVariables.Columns("SUBTIPO").Width = sdbgSubVariables.Width * 0.12
        sdbgSubVariables.Columns("ORIGEN").Width = sdbgSubVariables.Width * 0.11
        sdbgSubVariables.Columns("VALOR_DEF").Width = sdbgSubVariables.Width * 0.07
        sdbgSubVariables.Columns("PUNT").Width = sdbgSubVariables.Width * 0.08
        sdbgSubVariables.Columns("MAT").Width = sdbgSubVariables.Width * 0.079
    Else
        sdbgSubVariables.Columns("COD").Width = sdbgSubVariables.Width * 0.08
        If Not g_bMultiIdioma Then
             sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Width = (sdbgSubVariables.Width * 0.399) - 580
        End If
        sdbgSubVariables.Columns("TIPO").Width = sdbgSubVariables.Width * 0.1
        sdbgSubVariables.Columns("SUB").Width = sdbgSubVariables.Width * 0.05
        sdbgSubVariables.Columns("SUBTIPO").Width = sdbgSubVariables.Width * 0.12
        sdbgSubVariables.Columns("ORIGEN").Width = sdbgSubVariables.Width * 0.119
        sdbgSubVariables.Columns("VALOR_DEF").Width = sdbgSubVariables.Width * 0.07
        sdbgSubVariables.Columns("PUNT").Width = sdbgSubVariables.Width * 0.08
    End If
    
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_VAR_CALIDAD, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        With Ador
            Me.caption = Ador(0).Value  '1 Variables de calidad
            .MoveNext
            .MoveNext
            .MoveNext
            fraSubVariables.caption = Ador(0).Value '4 Subvariables
            .MoveNext
            fraFormula.caption = Ador(0).Value '5 F�rmula para calcular la puntuaci�n de la variable
            .MoveNext
            .MoveNext
            .MoveNext
            sdbgSubVariables.Columns("COD").caption = Ador(0).Value '8 Identificador
            .MoveNext
            m_sNombreDen = Ador(0).Value '9 Nombre
            .MoveNext
            sdbgSubVariables.Columns("TIPO").caption = Ador(0).Value '10 Tipo
            .MoveNext
            sdbgSubVariables.Columns("SUBTIPO").caption = Ador(0).Value '11 SubTipo
            .MoveNext
            sdbgSubVariables.Columns("ORIGEN").caption = Ador(0).Value '12 Origen de datos
            .MoveNext
            sdbgSubVariables.Columns("PUNT").caption = Ador(0).Value '13 Puntuaci�n
            .MoveNext
            sdbgSubVariables.Columns("MAT").caption = Ador(0).Value '14 Materiales
            .MoveNext
            .MoveNext
            .MoveNext
            m_sTipo(0) = Ador(0).Value '17 Simple
            .MoveNext
            m_sTipo(1) = Ador(0).Value '18 Compuesta
            .MoveNext
            m_sSubTipo(1) = Ador(0).Value '19 Integraci�n
            .MoveNext
            m_sSubTipo(2) = Ador(0).Value '20 Manual
            .MoveNext
            m_sSubTipo(3) = Ador(0).Value '21 Certificado
            .MoveNext
            m_sSubTipo(4) = Ador(0).Value '22 No conformidad
            For i = 1 To 11
                .MoveNext
                m_sIdiErrorFormula(i) = Ador(0).Value
            Next i
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            lblCambios.caption = Ador(0).Value '38
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            sdbgSubVariables.Columns("VALOR_DEF").caption = Ador(0).Value 'Val.defecto
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            .MoveNext
            ''Nuevos textos
            m_sSubTipo(5) = Ador(0).Value '48 PPM
            .MoveNext
            m_sSubTipo(6) = Ador(0).Value '49 Cargo a Proveedores
            .MoveNext
            m_sSubTipo(7) = Ador(0).Value '50  Tasa de Servicio
            .MoveNext
            .MoveNext   '51 ver bajas l�gicas
            m_sBaja = Ador(0).Value '52 "Baja l�gica"
            .MoveNext
            m_sDeshacerBaja = Ador(0).Value '53 "dDeshacer baja l�gica"
            .MoveNext
            m_sIdiErrorFormula(12) = Ador(0).Value '54 Existen variables de calidad dados de baja con el mismo c�digo
            .MoveNext
            .MoveNext
            .MoveNext
            sdbgSubVariables.Columns("CERT_VALOR_SINCUMPL").caption = Ador(0).Value 'Val. cert. sin cumpl.
            .MoveNext
            m_sSubTipo(8) = Ador(0).Value   'Encuesta
                        
            Ador.MoveNext
            fraConfiguracion.caption = Ador(0).Value ' Selecci�n del modo de c�lculo para unidades de negocio de nivel superior:
            Ador.MoveNext
            optOpcionConf(0).caption = Ador(0).Value ' Evaluar f�rmula
            Ador.MoveNext
            optOpcionConf(1).caption = Ador(0).Value ' Media ponderada seg�n:
            Ador.MoveNext
            optOpcionConf(2).caption = Ador(0).Value ' No calcular
            
            Ador.Close
        End With
    End If

    Set Ador = Nothing
End Sub

''' <summary>
''' Antes de cerrar se comprueba q lo introducido sea coherente
''' </summary>
''' <param name="Cancel">Cancelar o no el cierre de pantalla</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Dim oVarCal As CVariableCalidad
    
    If sdbgSubVariables.DataChanged Then
        sdbgSubVariables.Update
        If m_bError = True Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    If optOpcionConf(1).Value Then
        If sdbcVarPond.Text = "" Then
            oMensajes.NoValido Left(fraConfiguracion.caption, Len(fraConfiguracion.caption) - 1)
            Cancel = True
            Exit Sub
        End If
    End If
    
    If optOpcionConf(VarCalOpcionConf.EvaluarFormula).Value = True Then
        g_oVarCal3Mod.Opcion_Conf = VarCalOpcionConf.EvaluarFormula
        g_oVarCal3Mod.IdVar_Pond = ""
    ElseIf optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True Then
        g_oVarCal3Mod.Opcion_Conf = VarCalOpcionConf.MediaPonderadaSegunVarHermana
        g_oVarCal3Mod.IdVar_Pond = sdbcVarPond.Columns(0).Value
    Else
        g_oVarCal3Mod.Opcion_Conf = VarCalOpcionConf.NoCalcular
        g_oVarCal3Mod.IdVar_Pond = ""
    End If
    
    If Not g_oVarCal3Mod.VariblesCal Is Nothing Then
        For Each oVarCal In g_oVarCal3Mod.VariblesCal
            If oVarCal.Subtipo = CalidadSubtipo.CalCertificado Then 'Resto tienen formula por defecto Ej NC: X1- NoConfs abiertas en periodo 6 meses
                If NullToStr(oVarCal.Formula) = "" Then
                    oMensajes.NoValido fraFormula.caption & " " & oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                    Cancel = True
                    Exit Sub
                End If
            End If
        Next
    End If

    
    If Not ValidarFormula Then
        oMensajes.NoValido fraFormula.caption
        Cancel = True
        Exit Sub
    End If
    
    If g_oVarCal3Mod.VariblesCal.Count = 0 Then
       Set g_oVarCal3Mod.VariblesCal = Nothing
       g_oVarCal3Mod.Formula = Null
    End If
    
    Set g_oVarCal3 = Nothing
    Set g_oVarCal3Mod = Nothing

End Sub

Private Sub sdbddOrigen_CloseUp()
    sdbgSubVariables.Columns("ORIGEN").Value = sdbddOrigen.Columns("COD").Value
    sdbgSubVariables.Columns("ID_ORIGEN").Value = sdbddOrigen.Columns("ID").Value
    sdbgSubVariables.Columns("ID_FORM").Value = sdbddOrigen.Columns("FORM").Value
End Sub

Private Sub sdbddOrigen_DropDown()
    Dim oSolicitudes As CSolicitudes
    Dim Ador As ADODB.Recordset

    Screen.MousePointer = vbHourglass
    
    sdbddOrigen.RemoveAll
    
    'Carga las solicitudes de tipo certificado o no conformidad,seg�n lo que se haya seleccionado en subtipo
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    
    Select Case sdbgSubVariables.Columns("ID_SUBTIPO").Value
        Case CalidadSubtipo.CalCertificado
            Set Ador = oSolicitudes.DevolverSolicitudesDeTipo(TipoSolicitud.Certificados)
        Case CalidadSubtipo.CalNoConformidad
            Set Ador = oSolicitudes.DevolverSolicitudesDeTipo(TipoSolicitud.NoConformidades)
        Case CalidadSubtipo.CalEncuesta
            Set Ador = oSolicitudes.DevolverSolicitudesDeTipo(TipoSolicitud.Encuesta)
    End Select
    
    While Not Ador.EOF
        sdbddOrigen.AddItem Ador.Fields("COD").Value & Chr(m_lSeparador) & Ador.Fields("COD").Value & "-" & Ador.Fields("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & Ador.Fields("ID").Value & Chr(m_lSeparador) & Ador.Fields("FORMULARIO").Value
        Ador.MoveNext
    Wend
    
    If sdbddOrigen.Rows = 0 Then sdbddOrigen.AddItem ""

    Ador.Close
    Set Ador = Nothing
    Set oSolicitudes = Nothing
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddOrigen_InitColumnProps()
    sdbddOrigen.DataFieldList = "Column 1"
    sdbddOrigen.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddOrigen_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddOrigen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOrigen.Rows - 1
            bm = sdbddOrigen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOrigen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgSubVariables.Columns("ORIGEN").Value = Mid(sdbddOrigen.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOrigen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddSubTipo_CloseUp()
    With sdbgSubVariables
        If .Columns("ID_TIPO").Value = "0" And sdbddSubTipo.Columns("ID").Value = CStr(CalCertificado) Then
            If .Columns("ID_SUBTIPO").Value <> sdbddSubTipo.Columns("ID").Value Then .Columns("CERT_VALOR_SINCUMPL").Value = cnValCertNoCumplDefecto
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "", .Row
        Else
            .Columns("CERT_VALOR_SINCUMPL").Value = ""
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado", .Row
        End If
        
        .Columns("ID_SUBTIPO").Value = sdbddSubTipo.Columns("ID").Value
        .Columns("SUBTIPO").Value = sdbddSubTipo.Columns("NOMBRE").Value
        .Columns("ORIGEN").Value = ""
        .Columns("ID_ORIGEN").Value = ""
        .Columns("ID_FORM").Value = ""
        
        If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalEncuesta Then
            .Columns("ORIGEN").CellStyleSet "", sdbgSubVariables.Row
        Else
            .Columns("ORIGEN").CellStyleSet "Bloqueado", sdbgSubVariables.Row
        End If
    End With
End Sub

Private Sub sdbddSubTipo_DropDown()
    Screen.MousePointer = vbHourglass
    
    With sdbddSubTipo
        .RemoveAll
        
        .AddItem m_sSubTipo(1) & Chr(m_lSeparador) & CalidadSubtipo.CalIntegracion
        .AddItem m_sSubTipo(2) & Chr(m_lSeparador) & CalidadSubtipo.CalManual
        If basParametros.gParametrosGenerales.gbAccesoQACertificados Then .AddItem m_sSubTipo(3) & Chr(m_lSeparador) & CalidadSubtipo.CalCertificado
        If basParametros.gParametrosGenerales.gbAccesoQANoConformidad Then .AddItem m_sSubTipo(4) & Chr(m_lSeparador) & CalidadSubtipo.CalNoConformidad
        .AddItem m_sSubTipo(5) & Chr(m_lSeparador) & CalidadSubtipo.CalPPM
        .AddItem m_sSubTipo(6) & Chr(m_lSeparador) & CalidadSubtipo.CalCargoProveedores
        .AddItem m_sSubTipo(7) & Chr(m_lSeparador) & CalidadSubtipo.CalTasaServicios
        .AddItem m_sSubTipo(8) & Chr(m_lSeparador) & CalidadSubtipo.CalEncuesta
    End With
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddSubTipo_InitColumnProps()
    sdbddSubTipo.DataFieldList = "Column 0"
    sdbddSubTipo.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddSubTipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddSubTipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddSubTipo.Rows - 1
            bm = sdbddSubTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddSubTipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgSubVariables.Columns("SUBTIPO").Value = Mid(sdbddSubTipo.Columns(0).CellText(bm), 1, Len(Text))
                sdbddSubTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbddTipo_CloseUp()
    With sdbgSubVariables
        .Columns("ID_TIPO").Value = sdbddTipo.Columns("ID").Value
        .Columns("TIPO").Value = sdbddTipo.Columns("NOMBRE").Value
        
        If sdbddTipo.Columns("ID").Value = 0 Then  'Es simple
            .Columns("SUB").Value = ""
            .Columns("SUBTIPO").CellStyleSet "", .Row
            
            If .Columns("ID_SUBTIPO").Value = CStr(CalCertificado) Then
                If .Columns("ID_TIPO").Value <> sdbddTipo.Columns("ID").Value Then .Columns("CERT_VALOR_SINCUMPL").Value = cnValCertNoCumplDefecto
                .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "", .Row
            Else
                .Columns("CERT_VALOR_SINCUMPL").Value = ""
                .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado", .Row
            End If
        Else
            .Columns("SUB").Value = "..."  'Si es compuesta
            .Columns("ID_SUBTIPO").Value = ""
            .Columns("SUBTIPO").Value = ""
            .Columns("ORIGEN").Value = ""
            .Columns("ID_ORIGEN").Value = ""
            .Columns("ID_FORM").Value = ""
            .Columns("CERT_VALOR_SINCUMPL").Value = ""
            .Columns("SUBTIPO").CellStyleSet "Bloqueado", .Row
            .Columns("ORIGEN").CellStyleSet "Bloqueado", .Row
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado", .Row
        End If
    End With
End Sub

Private Sub sdbddTipo_DropDown()
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbddTipo.RemoveAll
    
    For i = 0 To 1
        sdbddTipo.AddItem m_sTipo(i) & Chr(m_lSeparador) & i
    Next
            
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddTipo_InitColumnProps()
    sdbddTipo.DataFieldList = "Column 0"
    sdbddTipo.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddTipo_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddTipo.Rows - 1
            bm = sdbddTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipo.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgSubVariables.Columns("TIPO").Value = Mid(sdbddTipo.Columns(0).CellText(bm), 1, Len(Text))
                sdbddTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbgSubVariables_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = 0
End Sub

''' <summary>Evento que salta cuando se modifica una fila de la grid y pierde el foco. Modifica la variable de nivel 4. Si no existia antes lo a�ade y sino lo modifica.</summary>
''' <param name="Cancel">Cancelar o no la modificaci�n</param>
''' <remarks>Llamada desde=propio formulario; Tiempo m�ximo=0,2seg</remarks>

Private Sub sdbgSubVariables_BeforeUpdate(Cancel As Integer)
    Dim oVarCal4 As CVariableCalidad
    Dim i As Byte
    Dim sCod As String
    Dim sDen As String
    
    m_bError = False
    
    With sdbgSubVariables
        If .col < 0 Then Exit Sub
            
        If .Columns("COD").Value = "" Then
            oMensajes.NoValido .Columns("COD").caption
            Cancel = True
            m_bError = True
            Exit Sub
        End If
        
        'Solo con letras y n�meros
        If Not NombreDeVariableValido(.Columns("COD").Value) Then
            oMensajes.NoValido 176
            Cancel = True
            m_bError = True
            Exit Sub
        End If
        
        If g_bMultiIdioma Then
            For i = 1 To g_oIdiomas.Count
                If .Columns(g_oIdiomas.Item(i).Cod).Value = "" Then
                    m_bError = True
                    oMensajes.NoValido .Columns(g_oIdiomas.Item(i).Cod).caption
                    Cancel = True
                    Exit Sub
                End If
            Next
        Else
            If .Columns(gParametrosInstalacion.gIdioma).Value = "" Then
                m_bError = True
                oMensajes.NoValido .Columns(gParametrosInstalacion.gIdioma).caption
                Cancel = True
                Exit Sub
            End If
        End If
        
        If .Columns("TIPO").Value = "" Then
            oMensajes.NoValido .Columns("TIPO").caption
            m_bError = True
            Cancel = True
            Exit Sub
        End If
        
        If .Columns("ID_TIPO").Value = 0 Then
            If .Columns("SUBTIPO").Value = "" Then
                m_bError = True
                oMensajes.NoValido .Columns("SUBTIPO").caption
                Cancel = True
                Exit Sub
            End If
            If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Then
                If .Columns("ORIGEN").Value = "" Then
                    oMensajes.NoValido .Columns("ORIGEN").caption
                    m_bError = True
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
        
        If .IsAddRow Then
            Set oVarCal4 = oFSGSRaiz.Generar_CVariableCalidad
            oVarCal4.Id = frmVARCalidad.g_oVarsCalidad1Mod.MaxID4
            frmVARCalidad.g_oVarsCalidad1Mod.MaxID4 = frmVARCalidad.g_oVarsCalidad1Mod.MaxID4 + 1
            oVarCal4.IdVarCal1 = g_oVarCal3Mod.IdVarCal1
            oVarCal4.IdVarCal2 = g_oVarCal3Mod.IdVarCal2
            oVarCal4.IdVarCal3 = g_oVarCal3Mod.Id
            oVarCal4.Nivel = 4
            oVarCal4.Cod = .Columns("COD").Value
            'Denominaciones multiIdioma
            Set oVarCal4.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
            For i = 1 To g_oIdiomas.Count
                sCod = g_oIdiomas.Item(i).Cod
                sDen = .Columns(sCod).Value
                oVarCal4.Denominaciones.Add sCod, sDen
            Next
            oVarCal4.Tipo = .Columns("ID_TIPO").Value
            If oVarCal4.Tipo = 1 Then
                oVarCal4.Subtipo = Null
                oVarCal4.Origen = Null
                oVarCal4.OrigenCod = Null
                oVarCal4.FormularioCOD = Null
                oVarCal4.FormularioID = Null
                oVarCal4.TipoPonderacion = PondNoPonderacion
                oVarCal4.NCPeriodo = Null
            Else
                oVarCal4.Subtipo = StrToNull(.Columns("ID_SUBTIPO").Value)
                If oVarCal4.Subtipo = CalidadSubtipo.CalCertificado Or oVarCal4.Subtipo = CalidadSubtipo.CalNoConformidad Or oVarCal4.Subtipo = CalidadSubtipo.CalEncuesta Then
                    oVarCal4.Origen = StrToNull(.Columns("ID_ORIGEN").Value)
                    oVarCal4.OrigenCod = StrToNull(.Columns("ORIGEN").Value)
                    oVarCal4.FormularioID = StrToNull(.Columns("ID_FORM").Value)
                    oVarCal4.FormularioCOD = Null
                Else
                    oVarCal4.Origen = Null
                    oVarCal4.OrigenCod = Null
                    oVarCal4.FormularioID = Null
                    oVarCal4.FormularioCOD = Null
                End If
                Select Case oVarCal4.Subtipo
                    Case CalidadSubtipo.CalEncuesta
                        oVarCal4.TipoPonderacion = PondEncuesta
                    Case CalidadSubtipo.CalCertificado
                        oVarCal4.TipoPonderacion = PondCertificado
                    Case CalidadSubtipo.CalNoConformidad
                        oVarCal4.TipoPonderacion = PondNCMediaPesos
                        oVarCal4.NCPeriodo = 6
                        oVarCal4.Formula = "X1"
                        CargaListaVariablesFormula oVarCal4
                    Case CalidadSubtipo.CalIntegracion
                        oVarCal4.TipoPonderacion = PondIntFormula
                        oVarCal4.Formula = "X"
                    Case CalidadSubtipo.CalManual
                        oVarCal4.TipoPonderacion = PondManual
                    Case CalidadSubtipo.CalPPM
                        oVarCal4.TipoPonderacion = PondPPM
                        oVarCal4.NCPeriodo = 6
                        oVarCal4.Formula = "X1"
                        CargaListaVariablesFormula oVarCal4
                    Case CalidadSubtipo.CalCargoProveedores
                        oVarCal4.TipoPonderacion = PondCargoProveedores
                        oVarCal4.NCPeriodo = 6
                        oVarCal4.Formula = "X1"
                        CargaListaVariablesFormula oVarCal4
                    Case CalidadSubtipo.CalTasaServicios
                        oVarCal4.TipoPonderacion = PondTasaServicios
                        oVarCal4.NCPeriodo = 6
                        oVarCal4.Formula = "X1"
                        CargaListaVariablesFormula oVarCal4
                End Select
            End If
            If Not IsNumeric(.Columns("VALOR_DEF").Value) Then .Columns("VALOR_DEF").Value = 0
            oVarCal4.ValorDefecto = StrToDblOrNull(.Columns("VALOR_DEF").Value)
            
            If Not IsNumeric(.Columns("CERT_VALOR_SINCUMPL").Value) Then .Columns("CERT_VALOR_SINCUMPL").Value = 0
            oVarCal4.ValorCertSinCumpl = StrToDblOrNull(.Columns("CERT_VALOR_SINCUMPL").Value)
            
            oVarCal4.modificado = True
            g_oVarCal3Mod.VariblesCal.Add oVarCal4.Id, 4, oVarCal4.Cod, oVarCal4.Denominaciones, oVarCal4.Tipo, oVarCal4.Subtipo, oVarCal4.Formula, oVarCal4.Origen, , , oVarCal4.IdVarCal1, oVarCal4.IdVarCal2, oVarCal4.IdVarCal3, , oVarCal4.OrigenCod, True, , oVarCal4.FormularioID, , , , oVarCal4.TipoPonderacion, oVarCal4.NCPeriodo, , , , , , , oVarCal4.UnidadNegQA, oVarCal4.UnidadNegQADen, oVarCal4.ValorDefecto, , , , , , , , , , , oVarCal4.VariablesXi, oVarCal4.ValorCertSinCumpl
            .Columns("ID").Value = oVarCal4.Id
            
            frmVARCalidad.VisualizarGuardar
            frmVARCalCompuestaNivel3.lblCambios.Visible = True
            lblCambios.Visible = True
            
            Set oVarCal4 = Nothing
        Else
            Set oVarCal4 = g_oVarCal3Mod.VariblesCal.Item("4" & CStr(.Columns("ID").Value))
            
            oVarCal4.Cod = .Columns("COD").Value
            'Denominaciones multiIdioma
            For i = 1 To oVarCal4.Denominaciones.Count
                sCod = oVarCal4.Denominaciones.Item(i).Cod
                If Not .Columns(sCod) Is Nothing Then
                    oVarCal4.Denominaciones.Item(sCod).Den = .Columns(sCod).Value
                End If
            Next
            oVarCal4.Tipo = CInt(.Columns("ID_TIPO").Value)
            
            If oVarCal4.Tipo = 1 Then
                oVarCal4.Subtipo = Null
                oVarCal4.Origen = Null
                oVarCal4.OrigenCod = Null
                oVarCal4.FormularioCOD = Null
                oVarCal4.FormularioID = Null
                oVarCal4.TipoPonderacion = PondNoPonderacion
                oVarCal4.NCPeriodo = Null
            Else
                If NullToStr(oVarCal4.Subtipo) <> .Columns("ID_SUBTIPO").Value Then
                    Select Case oVarCal4.Subtipo
                        Case CalidadSubtipo.CalCertificado, CalidadSubtipo.CalEncuesta
                            Set oVarCal4.CamposPond = Nothing
                            oVarCal4.Formula = Null
                        Case CalidadSubtipo.CalNoConformidad, CalidadSubtipo.CalPPM, CalidadSubtipo.CalCargoProveedores, CalidadSubtipo.CalTasaServicios
                            oVarCal4.NCPeriodo = Null
                            oVarCal4.Formula = Null
                            Set oVarCal4.VariablesXi = Nothing
                        Case CalidadSubtipo.CalIntegracion
                            oVarCal4.Formula = Null
                    End Select
                    oVarCal4.Subtipo = .Columns("ID_SUBTIPO").Value
                    Select Case oVarCal4.Subtipo
                        Case CalidadSubtipo.CalEncuesta
                            oVarCal4.TipoPonderacion = PondEncuesta
                        Case CalidadSubtipo.CalCertificado
                            oVarCal4.TipoPonderacion = PondCertificado
                        Case CalidadSubtipo.CalNoConformidad
                            oVarCal4.TipoPonderacion = PondNCMediaPesos
                            oVarCal4.NCPeriodo = 6
                            oVarCal4.Formula = "X1"
                            CargaListaVariablesFormula oVarCal4
                        Case CalidadSubtipo.CalIntegracion
                            oVarCal4.TipoPonderacion = PondIntFormula
                            oVarCal4.Formula = "X"
                        Case CalidadSubtipo.CalManual
                            oVarCal4.TipoPonderacion = PondManual
                        Case CalidadSubtipo.CalPPM
                            oVarCal4.TipoPonderacion = PondPPM
                            oVarCal4.NCPeriodo = 6
                            oVarCal4.Formula = "X1"
                            CargaListaVariablesFormula oVarCal4
                        Case CalidadSubtipo.CalCargoProveedores
                            oVarCal4.TipoPonderacion = PondCargoProveedores
                            oVarCal4.NCPeriodo = 6
                            oVarCal4.Formula = "X1"
                            CargaListaVariablesFormula oVarCal4
                        Case CalidadSubtipo.CalTasaServicios
                            oVarCal4.TipoPonderacion = PondTasaServicios
                            oVarCal4.NCPeriodo = 6
                            oVarCal4.Formula = "X1"
                            CargaListaVariablesFormula oVarCal4
                    End Select
                End If
            
                If oVarCal4.Subtipo = CalidadSubtipo.CalCertificado Or oVarCal4.Subtipo = CalidadSubtipo.CalNoConformidad Or oVarCal4.Subtipo = CalidadSubtipo.CalEncuesta Then
                    oVarCal4.Origen = CDbl(.Columns("ID_ORIGEN").Value)
                    oVarCal4.OrigenCod = StrToNull(.Columns("ORIGEN").Value)
                    If VarToDec0(oVarCal4.FormularioID) <> VarToDec0(.Columns("ID_FORM").Value) Then
                        oVarCal4.FormularioID = StrToNull(.Columns("ID_FORM").Value)
                        oVarCal4.FormularioCOD = Null
                        Set oVarCal4.CamposPond = Nothing
                    End If
                Else
                    oVarCal4.Origen = Null
                    oVarCal4.OrigenCod = Null
                    oVarCal4.FormularioID = Null
                    oVarCal4.FormularioCOD = Null
                End If
            End If
        
            If Not IsNumeric(.Columns("VALOR_DEF").Value) Then .Columns("VALOR_DEF").Value = 0
            oVarCal4.ValorDefecto = StrToDblOrNull(.Columns("VALOR_DEF").Value)
            
            If Not IsNumeric(.Columns("CERT_VALOR_SINCUMPL").Value) Then .Columns("CERT_VALOR_SINCUMPL").Value = 0
            oVarCal4.ValorCertSinCumpl = StrToDblOrNull(.Columns("CERT_VALOR_SINCUMPL").Value)
            
            'si exist�a en el original
            If ComprobarModificadoVar4(oVarCal4.Id) Then
                frmVARCalidad.VisualizarGuardar
                frmVARCalCompuestaNivel3.lblCambios.Visible = True
                lblCambios.Visible = True
            End If
        End If
    End With
End Sub

Private Sub sdbgSubVariables_BtnClick()
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal4Orig As CVariableCalidad
    Dim bCargarMultiIdioma As Boolean
    
    With sdbgSubVariables
        If .Rows = 0 Then Exit Sub
        
        If .DataChanged Then
            .Update
            If m_bError Then Exit Sub
        End If
        
        If .Columns(.col).Name = "SUB" Then
            If .Columns("ID_TIPO").Value = 1 Then
                CargarVariablesNivel5 frmVARCalCompuestaNivel3.g_oVarCal2Mod.IdVarCal1, frmVARCalCompuestaNivel3.g_oVarCal2Mod.Id, frmVARCalCompuestaNivel3.sdbgSubVariables.Columns("ID").Value, .Columns("ID").Value
            End If
            Exit Sub
        End If
        
        Select Case .Columns(.col).Name
            Case "MAT"
                Set oVarCal4 = g_oVarCal3Mod.VariblesCal.Item("4" & CStr(.Columns("ID").Value))
                If Not oVarCal4 Is Nothing Then
                    If oVarCal4.Tipo = 0 Then
                        Dim bExiste As Boolean
                        If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(g_oVarCal3Mod.IdVarCal1)) Is Nothing Then
                            If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(g_oVarCal3Mod.IdVarCal1)).VariblesCal Is Nothing Then
                                If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(g_oVarCal3Mod.IdVarCal1)).VariblesCal.Item("2" & CStr(g_oVarCal3Mod.IdVarCal2)) Is Nothing Then
                                    If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(g_oVarCal3Mod.IdVarCal1)).VariblesCal.Item("2" & CStr(g_oVarCal3Mod.IdVarCal2)).VariblesCal Is Nothing Then
                                        If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(g_oVarCal3Mod.IdVarCal1)).VariblesCal.Item("2" & CStr(g_oVarCal3Mod.IdVarCal2)).VariblesCal.Item("3" & CStr(g_oVarCal3Mod.Id)) Is Nothing Then
                                            If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(g_oVarCal3Mod.IdVarCal1)).VariblesCal.Item("2" & CStr(g_oVarCal3Mod.IdVarCal2)).VariblesCal.Item("3" & CStr(g_oVarCal3Mod.Id)).VariblesCal Is Nothing Then
                                                If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(g_oVarCal3Mod.IdVarCal1)).VariblesCal.Item("2" & CStr(g_oVarCal3Mod.IdVarCal2)).VariblesCal.Item("3" & CStr(g_oVarCal3Mod.Id)).VariblesCal.Item("4" & CStr(sdbgSubVariables.Columns("ID").Value)) Is Nothing Then
                                                    bExiste = True
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        
                        If bExiste Then
                            'Comprobar si ha habido cambios en la f�rmula antes de abrir la pantalla de asignaci�n de materiales
                            If Not VarCalHayCambiosFormula(oVarCal4, g_oVarCal3.VariblesCal.Item("4" & CStr(.Columns("ID").Value))) Then
                                MostrarFormVARCalMaterial oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, basParametros.gLongitudesDeCodigos, oVarCal4, bExiste, _
                                    oMensajes, gParametrosGenerales, oGestorSeguridad, oUsuarioSummit.Cod
                            Else
                                oMensajes.VarCalHayCambiosObjetivosSuelosEnFormula
                            End If
                        Else
                            MostrarFormVARCalMaterial oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, basParametros.gLongitudesDeCodigos, oVarCal4, bExiste, _
                                    oMensajes, gParametrosGenerales, oGestorSeguridad, oUsuarioSummit.Cod
                        End If
                    End If
                End If
                Set oVarCal4 = Nothing
        
            Case "PUNT"
                Set oVarCal4 = g_oVarCal3Mod.VariblesCal.Item("4" & CStr(.Columns("ID").Value))
                If Not oVarCal4 Is Nothing Then
                    If oVarCal4.Tipo = 0 Then
                        If Not g_oVarCal3 Is Nothing Then
                            If Not g_oVarCal3.VariblesCal Is Nothing Then
                                Set oVarCal4Orig = g_oVarCal3.VariblesCal.Item("4" & CStr(.Columns("ID").Value))
                            End If
                        End If
                        If oVarCal4.Subtipo = CalidadSubtipo.CalCertificado Then
                            MostrarFormVARCalPondCert oGestorIdiomas, oGestorParametros, gParametrosInstalacion.gIdioma, oMensajes, oFSGSRaiz, oGestorSeguridad, gParametrosGenerales, oUsuarioSummit.Cod, oVarCal4, oVarCal4Orig, _
                                frmVARCalidad, Nothing, Me, frmVARCalCompuestaNivel3
                        ElseIf oVarCal4.Subtipo = CalidadSubtipo.CalEncuesta Then
                            MostrarFormVARCalPondEnc oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oFSGSRaiz, oGestorSeguridad, gParametrosGenerales, oUsuarioSummit.Cod, oVarCal4, oVarCal4Orig, _
                                frmVARCalidad, Nothing, Me, frmVARCalCompuestaNivel3
                        ElseIf oVarCal4.Subtipo = CalidadSubtipo.CalIntegracion Then
                            MostrarFormVARCalPondInt oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oFSGSRaiz, oVarCal4, oVarCal4Orig, frmVARCalidad, Nothing, Me, frmVARCalCompuestaNivel3
                        ElseIf oVarCal4.Subtipo = CalidadSubtipo.CalNoConformidad Then
                            MostrarFormVARCalPondNC oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oVarCal4, oVarCal4Orig, frmVARCalidad, Nothing, Me, frmVARCalCompuestaNivel3
                            bCargarMultiIdioma = True
                        ElseIf (oVarCal4.Subtipo = CalidadSubtipo.CalPPM) Or (oVarCal4.Subtipo = CalidadSubtipo.CalCargoProveedores) Or (oVarCal4.Subtipo = CalidadSubtipo.CalTasaServicios) Then
                            MostrarFormVARCalSubtipo oGestorIdiomas, gParametrosInstalacion.gIdioma, oMensajes, oVarCal4, frmVARCalidad, Nothing, Me, frmVARCalCompuestaNivel3, oVarCal4.Subtipo
                            bCargarMultiIdioma = True
                        End If
                    End If
                    
                    If oVarCal4.VariablesXi Is Nothing And bCargarMultiIdioma Then CargaListaVariablesFormula oVarCal4
                End If
                Set oVarCal4 = Nothing
        End Select
    End With
End Sub

Private Sub sdbgSubVariables_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim i As Integer
    
    With sdbgSubVariables
        If .Columns("BAJALOG").Value = "1" Then
            cmdBajaSubvar.Picture = ImageList1.ListImages("NOBAJA").Picture
            cmdBajaSubvar.Tag = "BAJA"
            cmdElimSubvar.Enabled = False
        Else
            cmdBajaSubvar.Picture = ImageList1.ListImages("BAJA").Picture
            cmdBajaSubvar.Tag = "NOBAJA"
            cmdElimSubvar.Enabled = True
        End If
        
        If .col < 0 Then Exit Sub
        
        If .Columns(.col).Name = "TIPO" Then
            If .Columns("BAJALOG").Value = "1" Then
                .Columns("TIPO").DropDownHwnd = 0
            Else
                .Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
            End If
        End If
            
        If .Columns(sdbgSubVariables.col).Name = "SUBTIPO" Then
            If .Columns("ID_TIPO").Value = 0 Then
                If .Columns("BAJALOG").Value = "1" Then
                    .Columns("SUBTIPO").DropDownHwnd = 0
                Else
                    .Columns("SUBTIPO").DropDownHwnd = sdbddSubTipo.hWnd
                End If
            Else
                .Columns("SUBTIPO").DropDownHwnd = 0
            End If
            
        ElseIf .Columns(sdbgSubVariables.col).Name = "ORIGEN" Then
            If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalEncuesta Then
                If .Columns("BAJALOG").Value = "1" Then
                    .Columns("ORIGEN").DropDownHwnd = 0
                Else
                     .Columns("ORIGEN").DropDownHwnd = sdbddOrigen.hWnd
                End If
            Else
                .Columns("ORIGEN").DropDownHwnd = 0
            End If
        End If
                    
        .Columns("SUBTIPO").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("ORIGEN").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("COD").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("TIPO").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("VALOR_DEF").Locked = (.Columns("BAJALOG").Value = "1")
        .Columns("CERT_VALOR_SINCUMPL").Locked = Not (.Columns("ID_TIPO").Value = "0" And .Columns("ID_SUBTIPO").Value = CStr(CalCertificado))
        For i = 1 To g_oIdiomas.Count
            If Not .Columns(g_oIdiomas.Item(i).Cod) Is Nothing Then
                .Columns(g_oIdiomas.Item(i).Cod).Locked = (.Columns("BAJALOG").Value = "1")
            End If
        Next i
    End With
End Sub

Private Sub sdbgSubVariables_RowLoaded(ByVal Bookmark As Variant)
    Dim i As Integer
        
    With sdbgSubVariables
        If .Columns("ID_TIPO").Value = 1 Then
            .Columns("SUBTIPO").CellStyleSet "Bloqueado"
            'sdbgSubVariables.Columns("NEGOCIO").CellStyleSet "Bloqueado"
            .Columns("SUB").Value = "..."
            .Columns("MAT").Value = ""
        Else
            If .Columns("BAJALOG").Value = 1 Then
                .Columns("SUBTIPO").CellStyleSet "StringTachado"
                .Columns("ORIGEN").CellStyleSet "StringTachado"
            Else
                .Columns("SUBTIPO").CellStyleSet ""
                .Columns("ORIGEN").CellStyleSet ""
            End If
            .Columns("SUB").Value = ""
            .Columns("MAT").Value = "..."
        End If
        
        If .Columns("ID_TIPO").Value = "0" And .Columns("ID_SUBTIPO").Value = CStr(CalCertificado) Then
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet ""
        Else
            .Columns("CERT_VALOR_SINCUMPL").CellStyleSet "Bloqueado"
        End If
            
       'Si selecciona una no conformidad o certificado
        If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalCertificado Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalNoConformidad Or .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalEncuesta Then
            If .Columns("BAJALOG").Value = 1 Then
                .Columns("ORIGEN").CellStyleSet "StringTachado"
            Else
                .Columns("ORIGEN").CellStyleSet ""
            End If
        Else
            .Columns("ORIGEN").CellStyleSet "Bloqueado"
        End If
            
        .Columns("MAT").Value = "..."
        
        If .Columns("ID_SUBTIPO").Value = CalidadSubtipo.CalManual Or .Columns("ID_SUBTIPO").Value = 0 Then
            .Columns("PUNT").Value = ""
        Else
            .Columns("PUNT").Value = "..."
        End If
        
        If .Columns("BAJALOG").Value = 1 Then
            .Columns("COD").CellStyleSet "StringTachado"
            .Columns("TIPO").CellStyleSet "StringTachado"
            .Columns("VALOR_DEF").CellStyleSet "StringTachado"
    
            For i = 1 To g_oIdiomas.Count
                If Not .Columns(g_oIdiomas.Item(i).Cod) Is Nothing Then
                    .Columns(g_oIdiomas.Item(i).Cod).CellStyleSet "StringTachado"
                End If
            Next i
        End If
    End With
End Sub

Private Sub txtFormula_Change()
If m_bRespetarColor Then Exit Sub
txtFormula.Forecolor = vbRed
frmVARCalidad.VisualizarGuardar
lblCambios.Visible = True

End Sub

Private Sub txtFormula_Validate(Cancel As Boolean)
'Se comprueban al guardar todas
g_oVarCal3Mod.Formula = StrToNull(txtFormula.Text)

If frmVARCalidad.ComprobarModificadoVar3(g_oVarCal3Mod.IdVarCal1, g_oVarCal3Mod.IdVarCal2, g_oVarCal3Mod.Id) Then
    frmVARCalidad.VisualizarGuardar
End If

End Sub

Private Sub CargarVariablesCalidad4()
''' <summary>
''' Cargar las Variables de calidad de Nivel 4. Comprueba si Muestra o no las variables que estan dados de baja
''' </summary>
''' <param name="ParametroEntrada1">Explicaci�n par�metro 1</param>
''' <param name=" ParametroEntrada2">Explicaci�n par�metro 2</param>
''' <returns>Explicaci�n retorno de la funci�n</returns>
''' <remarks>Llamada desde=cmdAnyaSubvar_Click//form_load; Tiempo m�ximo=0,1seg.</remarks>
'MPG (08/04/2009)

'Dim inicio As Double, final As Double, tiempoTranscurrido As Double
'inicio = Timer

    Dim oVarCal As CVariableCalidad
    Dim sCadenaDenominaciones As String
    Dim i As Byte
    Dim sw As Boolean

    Screen.MousePointer = vbHourglass
    
    m_bRespetarColor = True
    txtFormula.Text = NullToStr(g_oVarCal3Mod.Formula)
    m_bRespetarColor = False
    
    sdbgSubVariables.RemoveAll
    
    If Not g_oVarCal3Mod.VariblesCal Is Nothing Then
        For Each oVarCal In g_oVarCal3Mod.VariblesCal
            If (g_bVerBaja And oVarCal.BajaLog) Or (Not oVarCal.BajaLog) Then
                sCadenaDenominaciones = Chr(m_lSeparador) & oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                If g_bMultiIdioma Then
                    For i = 1 To oVarCal.Denominaciones.Count
                        If oVarCal.Denominaciones.Item(i).Cod <> gParametrosInstalacion.gIdioma Then
                            sCadenaDenominaciones = sCadenaDenominaciones & Chr(m_lSeparador) & oVarCal.Denominaciones.Item(i).Den
                        End If
                        
                    Next
                End If
            
                sdbgSubVariables.AddItem oVarCal.Id & Chr(m_lSeparador) & oVarCal.Cod & Chr(m_lSeparador) & m_sTipo(oVarCal.Tipo) & _
                    Chr(m_lSeparador) & IIf(oVarCal.Tipo = 0, "", "...") & _
                    Chr(m_lSeparador) & IIf(oVarCal.Subtipo = 0, "", m_sSubTipo(NullToDbl0(oVarCal.Subtipo))) & _
                    Chr(m_lSeparador) & NullToStr(oVarCal.OrigenCod) & Chr(m_lSeparador) & NullToStr(oVarCal.ValorDefecto) & _
                    Chr(m_lSeparador) & IIf((oVarCal.Tipo = 0 And oVarCal.Subtipo = CalCertificado), NullToStr(oVarCal.ValorCertSinCumpl), "") & _
                    Chr(m_lSeparador) & IIf((oVarCal.Subtipo = 0 Or oVarCal.Subtipo = CalManual), "", "...") & Chr(m_lSeparador) & "..." & _
                    Chr(m_lSeparador) & oVarCal.Tipo & Chr(m_lSeparador) & oVarCal.Origen & Chr(m_lSeparador) & oVarCal.FormularioID & _
                    Chr(m_lSeparador) & oVarCal.Subtipo & Chr(m_lSeparador) & IIf(oVarCal.BajaLog, 1, 0) & sCadenaDenominaciones
                If oVarCal.modificado = True Then
                    lblCambios.Visible = True
                End If
                

                If Not sw Then
                    If oVarCal.BajaLog = True Then
                        'cmdBajaSubvar.caption = m_sDeshacerBaja
                        cmdBajaSubvar.Picture = ImageList1.ListImages("NOBAJA").Picture
                        cmdBajaSubvar.Tag = "BAJA"
                        cmdElimSubvar.Enabled = False
                    Else
                        'cmdBajaSubvar.caption = m_sBaja
                        cmdBajaSubvar.Picture = ImageList1.ListImages("BAJA").Picture
                        cmdBajaSubvar.Tag = "NOBAJA"
                        cmdElimSubvar.Enabled = True
                    End If
                    sw = True
                End If
            End If
        Next
    Else
        Set g_oVarCal3Mod.VariblesCal = oFSGSRaiz.Generar_CVariablesCalidad
    End If
    
    picBotonesSubvar.Visible = Not (g_oVarCal3Mod.BajaLog)
    txtFormula.Enabled = Not (g_oVarCal3Mod.BajaLog)
    
    Screen.MousePointer = vbNormal
'    final = Timer
'    tiempoTranscurrido = (final - inicio)
End Sub

''' <summary>
''' Comprueba si se ha modificado la coleccion oVarCal4Mod en comparacion con la copia de la variable de calidad de nivel 4 que no se ha tocado
''' </summary>
''' <param name="lID4">Id de la variable de nivel 4</param>
''' <returns>true --> Si se ha modificao la variable // false (lo contrario) </returns>
''' <remarks>Llamada desde=sdbgSubVariables_BeforeUpdate; Tiempo m�ximo=0,1seg.</remarks>

Private Function ComprobarModificadoVar4(ByVal lID4 As Long) As Boolean
    Dim oVarCal4 As CVariableCalidad
    Dim oVarCal4Mod As CVariableCalidad
    Dim bEncontrado As Boolean
    Dim i As Byte
    Dim oVarCal5 As CVariableCalidad
    Dim rs As Recordset
    Dim bPuntHist As Boolean
    
    If Not g_oVarCal3 Is Nothing Then
        Set oVarCal4 = g_oVarCal3.VariblesCal.Item("4" & CStr(lID4))
    End If
    Set oVarCal4Mod = g_oVarCal3Mod.VariblesCal.Item("4" & CStr(lID4))
    
    If oVarCal4 Is Nothing Then
        oVarCal4Mod.modificado = True
    Else
        If (oVarCal4Mod.Cod <> oVarCal4.Cod) Or (NullToDbl0(oVarCal4Mod.Origen) <> NullToDbl0(oVarCal4.Origen)) Or _
            (NullToDbl0(oVarCal4Mod.Subtipo) <> NullToDbl0(oVarCal4.Subtipo)) Or (NullToDbl0(oVarCal4Mod.Tipo) <> NullToDbl0(oVarCal4.Tipo)) Or _
            (NullToDbl0(oVarCal4Mod.Formula) <> NullToDbl0(oVarCal4.Formula)) Or (NullToDbl0(oVarCal4Mod.ValorDefecto) <> NullToDbl0(oVarCal4.ValorDefecto)) Or _
            (NullToDbl0(oVarCal4Mod.ValorCertSinCumpl) <> NullToDbl0(oVarCal4.ValorCertSinCumpl)) Or _
            (oVarCal4Mod.Opcion_Conf <> oVarCal4.Opcion_Conf) Or (oVarCal4Mod.IdVar_Pond <> oVarCal4.IdVar_Pond) Then
            oVarCal4Mod.modificado = True
            If oVarCal4.Tipo = 1 And oVarCal4Mod.Tipo = 0 Then
                Set rs = oVarCal4Mod.ExistenPuntuaciones
                If Not rs.EOF Then
                    rs.MoveNext
                    bPuntHist = (rs("CONT").Value > 0)
                End If
                rs.Close
                Set rs = Nothing
                If bPuntHist Then
                    For Each oVarCal5 In oVarCal4Mod.VariblesCal
                        If Not oVarCal5.BajaLog Then
                            oVarCal5.BajaLog = True
                            oVarCal5.modificado = True
                        End If
                    Next
                
                Else
                    Set oVarCal4Mod.VariblesCal = Nothing
                    oVarCal4Mod.Formula = Null
                End If
            End If
        Else
            bEncontrado = False
            For i = 1 To oVarCal4Mod.Denominaciones.Count
                If oVarCal4Mod.Denominaciones.Item(i).Den <> oVarCal4.Denominaciones.Item(i).Den Then
                    bEncontrado = True
                    Exit For
                End If
            Next
            If bEncontrado Then
                oVarCal4Mod.modificado = True
            End If
        End If
    End If
    
    ComprobarModificadoVar4 = oVarCal4Mod.modificado
    
    Set oVarCal4Mod = Nothing
    Set oVarCal4 = Nothing
End Function

Private Function ValidarFormula() As Boolean
''' <summary>
''' Comprueba si la formula introducida es correcta o no (tiene correcto el identificador,operador matematico correcto...
''' </summary>
''' <returns>True/false si la formula introducida es correcta o no</returns>
''' <remarks>Llamada desde=Form_unload; Tiempo m�ximo=0,1seg</remarks>
'mpg(08/04/2009)
Dim sVariables() As String
Dim sVariablesBaja() As String
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim sCaracter As String
Dim i, cont As Integer
Dim vbm As Variant
Dim oVarCal As CVariableCalidad
Dim bMensajeMostrado, bVariablesBaja As Boolean

If sdbgSubVariables.Rows = 0 Then
    ValidarFormula = True
    Exit Function
End If

If g_oVarCal3Mod.BajaLog = True Then
    ValidarFormula = True
    Exit Function
End If

If txtFormula.Text <> "" Then
    Set iEq = New USPExpression
    
    If sdbgSubVariables.Rows > 0 Then
        ReDim sVariables(sdbgSubVariables.Rows - 1)
        
        cont = 0
        i = 0
        If Not g_oVarCal3Mod.VariblesCal Is Nothing Then
            ReDim sVariablesBaja(g_oVarCal3Mod.VariblesCal.Count - 1)
            For Each oVarCal In g_oVarCal3Mod.VariblesCal
                If oVarCal.BajaLog = False Then
                    sVariables(cont) = oVarCal.Cod
                    cont = cont + 1
                Else
                    bVariablesBaja = True
                End If
                sVariablesBaja(i) = oVarCal.Cod
                i = i + 1
            Next
        End If
        
    Else
        ReDim sVariables(0)
    End If
    lIndex = iEq.Parse(txtFormula.Text, sVariables, lErrCode)

    If lErrCode <> USPEX_NO_ERROR Then
        ' Parsing error handler
        Select Case lErrCode
            Case USPEX_DIVISION_BY_ZERO
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(1))
            Case USPEX_EMPTY_EXPRESSION
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(2))
            Case USPEX_MISSING_OPERATOR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(3))
            Case USPEX_SYNTAX_ERROR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(4))
            Case USPEX_UNKNOWN_FUNCTION
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(5))
            Case USPEX_UNKNOWN_OPERATOR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(6))
            Case USPEX_WRONG_PARAMS_NUMBER
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(7))
            Case USPEX_UNKNOWN_IDENTIFIER
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(8))
            Case USPEX_UNKNOWN_VAR
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(9))
            Case USPEX_VARIABLES_NOTUNIQUE
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(10))
                bMensajeMostrado = True
            Case Else
                sCaracter = Mid(txtFormula.Text, lIndex)
                oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(11) & vbCrLf & sCaracter)
        End Select
        Set iEq = Nothing
        ValidarFormula = False
        Exit Function
    End If
    
    If bVariablesBaja Then
        If Not bMensajeMostrado Then
            lIndex = iEq.Parse(txtFormula.Text, sVariablesBaja, lErrCode)
            If lErrCode <> USPEX_NO_ERROR Then
                Select Case lErrCode
                    Case USPEX_VARIABLES_NOTUNIQUE
                        oMensajes.FormulaIncorrecta (m_sIdiErrorFormula(10) & Chr(10) & m_sIdiErrorFormula(12))
                End Select

                Set iEq = Nothing
                ValidarFormula = False
                Exit Function
            End If
        End If
    End If
    
    Set iEq = Nothing
Else
    If sdbgSubVariables.Rows > 0 Then
        For i = 0 To sdbgSubVariables.Rows - 1
            vbm = sdbgSubVariables.AddItemBookmark(i)
            If sdbgSubVariables.Columns("BAJALOG").CellValue(vbm) = 0 Then
                ValidarFormula = False
                Exit Function
            End If
        Next i
    End If
End If

g_oVarCal3Mod.Formula = StrToNull(txtFormula.Text)

If frmVARCalidad.ComprobarModificadoVar3(g_oVarCal3Mod.IdVarCal1, g_oVarCal3Mod.IdVarCal2, g_oVarCal3Mod.Id) Then
    frmVARCalidad.VisualizarGuardar
    lblCambios.Visible = True
End If

txtFormula.Forecolor = vbBlack

ValidarFormula = True


End Function

Private Sub CargarVariablesNivel5(ByVal lIdCal1 As Long, ByVal lIdCal2 As Long, ByVal lIdCal3 As Long, ByVal lIdCal4 As Long)
''' <summary>
''' Llama al formulario de las variables de calidad de nivel 5 cuando se pincha en una variable de nivel 4 de tipo compuesto
''' </summary>
''' <param name="lIdCal1">Id de la variable de calidad de nivel 1</param>
''' <param name="lIdCal2">Id de la variable de calidad de nivel 2</param>
''' <param name="lIdCal3">Id de la variable de calidad de nivel 3</param>
''' <param name="lIdCal3">Id de la variable de calidad de nivel 4</param>

''' <returns>Ninguno</returns>
''' <remarks>Llamada desde=sdbgSubVariables_BtnClick; Tiempo m�ximo=0seg.</remarks>
    Screen.MousePointer = vbHourglass
        
    If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(lIdCal1)) Is Nothing Then
        If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(lIdCal1)).VariblesCal.Item("2" & CStr(lIdCal2)) Is Nothing Then
            If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(lIdCal1)).VariblesCal.Item("2" & CStr(lIdCal2)).VariblesCal.Item("3" & CStr(lIdCal3)) Is Nothing Then
                If Not frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(lIdCal1)).VariblesCal.Item("2" & CStr(lIdCal2)).VariblesCal.Item("3" & CStr(lIdCal3)).VariblesCal.Item("4" & CStr(lIdCal4)) Is Nothing Then
                    Set frmVARCalCompuestaNivel5.g_oVarCal4 = frmVARCalidad.g_oVarsCalidad1.Item("1" & CStr(lIdCal1)).VariblesCal.Item("2" & CStr(lIdCal2)).VariblesCal.Item("3" & CStr(lIdCal3)).VariblesCal.Item("4" & CStr(lIdCal4))
                End If
            End If
        End If
    End If
    Set frmVARCalCompuestaNivel5.g_oVarCal4Mod = frmVARCalidad.g_oVarsCalidad1Mod.Item("1" & CStr(lIdCal1)).VariblesCal.Item("2" & CStr(lIdCal2)).VariblesCal.Item("3" & CStr(lIdCal3)).VariblesCal.Item("4" & CStr(lIdCal4))
    frmVARCalCompuestaNivel5.g_bMultiIdioma = g_bMultiIdioma
    frmVARCalCompuestaNivel5.g_bVerBaja = g_bVerBaja
    
    Set frmVARCalCompuestaNivel5.g_oIdiomas = g_oIdiomas

    frmVARCalCompuestaNivel5.Show 1
            
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdBajaSubvar_Click()
''' <summary>
''' Proceso que realiza la baja logica o deshace la baja logica de de una variable de calidad de nivel 2
''' </summary>
''' <returns>Ninguno</returns>
''' <remarks>Llamada desde=Evento que salta cuando se pincha en el boton de Baja/deshacer baja logica; Tiempo m�ximo=0,5seg.</remarks>
'mpg(08/04/2009)

Dim irespuesta As Integer
Dim oVarCal4 As CVariableCalidad
Dim oVarCal5 As CVariableCalidad

Dim sCod As String
Dim bHijasSinBaja As Boolean

    'si hay cambios los guarda
    If sdbgSubVariables.DataChanged = True Then
        sdbgSubVariables.Update
        If m_bError = True Then
            If Me.Visible Then sdbgSubVariables.SetFocus
            Exit Sub
        End If
    End If

    If sdbgSubVariables.Rows = 0 Then Exit Sub
    If sdbgSubVariables.SelBookmarks.Count = 0 Then sdbgSubVariables.SelBookmarks.Add sdbgSubVariables.Bookmark
    
    Set oVarCal4 = g_oVarCal3Mod.VariblesCal.Item("4" & CStr(sdbgSubVariables.Columns("ID").Value))
    If oVarCal4 Is Nothing Then Exit Sub
        
    If cmdBajaSubvar.Tag = "BAJA" Then
        Screen.MousePointer = vbHourglass
        oVarCal4.BajaLog = False
        oVarCal4.modificado = True
        'cmdBajaSubvar.caption = m_sBaja
        cmdBajaSubvar.Picture = ImageList1.ListImages("BAJA").Picture
        cmdBajaSubvar.Tag = "NOBAJA"
        cmdElimSubvar.Enabled = True
         sdbgSubVariables.Columns("BAJALOG").Value = 0
        sdbgSubVariables.Refresh
    Else
        If Not oVarCal4.VariblesCal Is Nothing Then
            For Each oVarCal5 In oVarCal4.VariblesCal
                If Not (oVarCal5.BajaLog) Then
                    bHijasSinBaja = True
                    Exit For
                End If
            Next
        End If
        
        irespuesta = oMensajes.PreguntaBajaLogVariableCalidad(sdbgSubVariables.Columns(gParametrosInstalacion.gIdioma).Value, bHijasSinBaja)
        If irespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        If bHijasSinBaja Then
            For Each oVarCal5 In oVarCal4.VariblesCal
                oVarCal5.BajaLog = True
                oVarCal5.modificado = True
            Next
        End If
        oVarCal4.BajaLog = True
        oVarCal4.modificado = True
        'cmdBajaSubvar.caption = m_sDeshacerBaja
        cmdBajaSubvar.Picture = ImageList1.ListImages("NOBAJA").Picture
        cmdBajaSubvar.Tag = "BAJA"
        cmdElimSubvar.Enabled = False
        sCod = sdbgSubVariables.Columns("COD").Value
        If g_bVerBaja Then
            sdbgSubVariables.Columns("BAJALOG").Value = 1
        Else
            sdbgSubVariables.DeleteSelected
        End If
    
    End If
    sdbgSubVariables.col = 0
    sdbgSubVariables.Update

    frmVARCalidad.VisualizarGuardar
    lblCambios.Visible = True

    If InStr(1, txtFormula.Text, sCod) <> 0 Then
        txtFormula.Forecolor = vbRed
    End If

    sdbgSubVariables.SelBookmarks.RemoveAll
    If Me.Visible Then sdbgSubVariables.SetFocus

    Set oVarCal4 = Nothing
    Set oVarCal5 = Nothing
    Screen.MousePointer = vbNormal
        
End Sub

Private Sub optOpcionConf_Click(Index As Integer)
    If Index <> VarCalOpcionConf.MediaPonderadaSegunVarHermana Then
        sdbcVarPond.Text = ""
    End If
    If Index <> g_oVarCal3.Opcion_Conf Then HayCambios
End Sub

Private Sub sdbcVarPond_Change()
  If g_oVarCal3.IdVar_Pond <> sdbcVarPond.Columns("ID").Value Then HayCambios
End Sub

Private Sub sdbcVarPond_Click()
    HayCambios
    optOpcionConf(VarCalOpcionConf.MediaPonderadaSegunVarHermana).Value = True
End Sub

Private Sub sdbcVarPond_InitColumnProps()
    sdbcVarPond.DataFieldList = "Column 0"
    sdbcVarPond.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbcVarPond_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcVarPond.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcVarPond.Rows - 1
            bm = sdbcVarPond.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcVarPond.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcVarPond.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub HayCambios()
    If Not lblCambios.Visible Then
        frmVARCalidad.VisualizarGuardar
        frmVARCalCompuestaNivel3.lblCambios.Visible = True
        lblCambios.Visible = True
    End If
End Sub

Private Sub CargarVariablesHermanas()
    Screen.MousePointer = vbHourglass
    Dim oVarCal As CVariableCalidad
    Dim g_oVariablesCalidadHermanas As CVariablesCalidad
    sdbcVarPond.RemoveAll

    Set g_oVariablesCalidadHermanas = frmVARCalCompuestaNivel3.g_oVarCal2Mod.VariblesCal
    For Each oVarCal In g_oVariablesCalidadHermanas
        If oVarCal.Id <> g_oVarCal3.Id And oVarCal.BajaLog = False Then
            sdbcVarPond.AddItem oVarCal.Id & Chr(9) & oVarCal.Cod & " - " & oVarCal.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            If oVarCal.Id = g_oVarCal3.IdVar_Pond Then
                sdbcVarPond.Text = oVarCal.Cod
            End If
        End If
    Next
    Screen.MousePointer = vbNormal
End Sub



