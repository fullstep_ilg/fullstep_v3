VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmCATDatExtAdjun 
   BackColor       =   &H00808000&
   Caption         =   "Especificaciones"
   ClientHeight    =   4170
   ClientLeft      =   675
   ClientTop       =   4455
   ClientWidth     =   9750
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCATDatExtAdjun.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4170
   ScaleWidth      =   9750
   Begin VB.Frame fraEspArt 
      Height          =   4020
      Left            =   60
      TabIndex        =   23
      Top             =   60
      Width           =   4455
      Begin VB.PictureBox picEditArt 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   1980
         ScaleHeight     =   375
         ScaleWidth      =   2400
         TabIndex        =   26
         Top             =   3600
         Width           =   2400
         Begin VB.CommandButton cmdAbrirEspArt 
            Height          =   300
            Left            =   1905
            Picture         =   "frmCATDatExtAdjun.frx":0CB2
            Style           =   1  'Graphical
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEspArt 
            Height          =   300
            Left            =   1425
            Picture         =   "frmCATDatExtAdjun.frx":0D2E
            Style           =   1  'Graphical
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarEspArt 
            Height          =   300
            Left            =   480
            Picture         =   "frmCATDatExtAdjun.frx":0DAF
            Style           =   1  'Graphical
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirEspArt 
            Height          =   300
            Left            =   0
            Picture         =   "frmCATDatExtAdjun.frx":0E35
            Style           =   1  'Graphical
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdModificarEspArt 
            Height          =   300
            Left            =   960
            Picture         =   "frmCATDatExtAdjun.frx":0E96
            Style           =   1  'Graphical
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.TextBox txtArticuloEsp 
         ForeColor       =   &H80000012&
         Height          =   1275
         Left            =   60
         MaxLength       =   800
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   0
         Top             =   420
         Width           =   4260
      End
      Begin MSComctlLib.ListView lstvwArticuloEsp 
         Height          =   1555
         Left            =   60
         TabIndex        =   1
         Top             =   1980
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   2752
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Path"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin VB.Label lblArticuloFich 
         Caption         =   "Archivos adjuntos"
         Height          =   240
         Left            =   120
         TabIndex        =   25
         Top             =   1740
         Width           =   1410
      End
      Begin VB.Label lblArticuloEsp 
         Caption         =   "Especificaciones del art�culo:"
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   180
         Width           =   3795
      End
   End
   Begin VB.CommandButton cmdIncluirFichEspArt 
      Height          =   360
      Left            =   4620
      Picture         =   "frmCATDatExtAdjun.frx":0FE0
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   2010
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirFichEspArt 
      Height          =   345
      Left            =   4620
      Picture         =   "frmCATDatExtAdjun.frx":1031
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   2410
      Width           =   495
   End
   Begin VB.CommandButton cmdIncluirEspArt 
      Height          =   435
      Left            =   4620
      Picture         =   "frmCATDatExtAdjun.frx":1083
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   600
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirEspArt 
      Height          =   435
      Left            =   4620
      Picture         =   "frmCATDatExtAdjun.frx":10D4
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   1200
      Width           =   495
   End
   Begin VB.Frame fraEspProve 
      Height          =   4020
      Left            =   5220
      TabIndex        =   9
      Top             =   60
      Width           =   4455
      Begin VB.PictureBox picEditProve 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2020
         ScaleHeight     =   375
         ScaleWidth      =   2400
         TabIndex        =   27
         Top             =   3600
         Width           =   2400
         Begin VB.CommandButton cmdAbrirEspProve 
            Height          =   300
            Left            =   1905
            Picture         =   "frmCATDatExtAdjun.frx":1126
            Style           =   1  'Graphical
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdSalvarEspProve 
            Height          =   300
            Left            =   1425
            Picture         =   "frmCATDatExtAdjun.frx":11A2
            Style           =   1  'Graphical
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdEliminarEspProve 
            Height          =   300
            Left            =   480
            Picture         =   "frmCATDatExtAdjun.frx":1223
            Style           =   1  'Graphical
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdAnyadirEspProve 
            Height          =   300
            Left            =   0
            Picture         =   "frmCATDatExtAdjun.frx":12A9
            Style           =   1  'Graphical
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
         Begin VB.CommandButton cmdModificarEspProve 
            Height          =   300
            Left            =   960
            Picture         =   "frmCATDatExtAdjun.frx":130A
            Style           =   1  'Graphical
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   0
            UseMaskColor    =   -1  'True
            Width           =   420
         End
      End
      Begin VB.TextBox txtProveEsp 
         ForeColor       =   &H80000012&
         Height          =   1275
         Left            =   120
         MaxLength       =   2000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   2
         Top             =   420
         Width           =   4260
      End
      Begin MSComctlLib.ListView lstvwProveEsp 
         Height          =   1555
         Left            =   120
         TabIndex        =   3
         Top             =   1980
         Width           =   4275
         _ExtentX        =   7541
         _ExtentY        =   2752
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   0   'False
         HotTracking     =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483630
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Fichero"
            Object.Width           =   3233
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tamanyo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Path"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Comentario"
            Object.Width           =   6920
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Fecha"
            Object.Width           =   2868
         EndProperty
      End
      Begin VB.Label lblProveEsp 
         Caption         =   "Especificaciones del proveedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   180
         Width           =   4035
      End
      Begin VB.Label lblProveFich 
         Caption         =   "Archivos adjuntos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   135
         TabIndex        =   21
         Top             =   1740
         Width           =   2775
      End
   End
   Begin VB.CommandButton cmdIncluirTodosFichArt 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   4620
      Picture         =   "frmCATDatExtAdjun.frx":1454
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   2900
      Width           =   495
   End
   Begin VB.CommandButton cmdExcluirTodosFichArt 
      Caption         =   "ALL"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   4620
      Picture         =   "frmCATDatExtAdjun.frx":14A5
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   3450
      Width           =   495
   End
   Begin MSComDlg.CommonDialog cmmdEsp 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DialogTitle     =   "Seleccione el fichero para adjuntarlo a la especificaci�n"
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmCATDatExtAdjun.frx":14F7
            Key             =   "ESP"
            Object.Tag             =   "ESP"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCATDatExtAdjun"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_oArtProve As CArticulo
Public g_oArticulo As CArticulo
Public g_oProve As CProveedor
Public g_oIBaseDatos As IBaseDatos
Public g_bCancelarEsp As Boolean
Public g_sComentario As String
Public g_bRespetarCombo As Boolean
Public g_sArticulo As String
Public g_bSoloRuta As Boolean


'Variables privadas:
Private sayFileNames() As String
Private Accion As accionessummit
Private m_oEsp As CEspecificacion

'Idioma:
Private m_sIdioma() As String
Private m_skb As String

'Restricciones
Private m_bModifArt      As Boolean
Private m_bRMatArt       As Boolean
Private m_bModifArtProve As Boolean
Private m_bRMatArtProve  As Boolean

Public Sub AnyadirEspsAListaArt()
Dim oEsp As CEspecificacion

    Screen.MousePointer = vbHourglass
    
    lstvwArticuloEsp.ListItems.clear
    
    For Each oEsp In g_oArticulo.especificaciones
        lstvwArticuloEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
        
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", NullToStr(oEsp.Ruta)
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
        lstvwArticuloEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
    Next
    
    If g_oArticulo.especificaciones.Count = 0 And txtArticuloEsp.Locked Then
        cmdSalvarEspArt.Enabled = False
        cmdAbrirEspArt.Enabled = False
    End If
    
    Screen.MousePointer = vbNormal

End Sub

Public Sub AnyadirEspsAListaProve()
Dim oEsp As CEspecificacion
    
    Screen.MousePointer = vbHourglass
    
    lstvwProveEsp.ListItems.clear
    
    If Not g_oArtProve.especificaciones Is Nothing Then
        For Each oEsp In g_oArtProve.especificaciones
            lstvwProveEsp.ListItems.Add , "ESP" & CStr(oEsp.Id), oEsp.nombre, , "ESP"
            
            lstvwProveEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oEsp.DataSize / 1024) & " " & m_skb
            lstvwProveEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Path", NullToStr(oEsp.Ruta)
            lstvwProveEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ToolTipText = NullToStr(oEsp.Comentario)
            lstvwProveEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Com", NullToStr(oEsp.Comentario)
            lstvwProveEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).ListSubItems.Add , "Fec", oEsp.Fecha
            lstvwProveEsp.ListItems.Item("ESP" & CStr(oEsp.Id)).Tag = oEsp.Id
        Next
    
        If g_oArtProve.especificaciones.Count = 0 And txtProveEsp.Locked = True Then
            cmdSalvarEspProve.Enabled = False
            cmdAbrirEspProve.Enabled = False
        End If
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub

Public Sub CargarRecursos()

Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CAT_DATOS_EXT_ADJUN, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        'Me.Caption = Ador(0).Value
        Ador.MoveNext
        lblArticuloEsp.caption = Ador(0).Value & " " & g_sArticulo
        Ador.MoveNext
        lblProveEsp.caption = Ador(0).Value
        Ador.MoveNext
        lblArticuloFich.caption = Ador(0).Value
        lblProveFich.caption = Ador(0).Value
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(1).Text = Ador(0).Value
        lstvwProveEsp.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(4).Text = Ador(0).Value
        lstvwProveEsp.ColumnHeaders(4).Text = Ador(0).Value
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(5).Text = Ador(0).Value
        lstvwProveEsp.ColumnHeaders(5).Text = Ador(0).Value
        Ador.MoveNext
        ReDim m_sIdioma(1 To 13)
        For i = 1 To 12
            m_sIdioma(i) = Ador(0).Value
            Ador.MoveNext
        Next
        lstvwArticuloEsp.ColumnHeaders(3).Text = Ador(0).Value
        lstvwProveEsp.ColumnHeaders(3).Text = Ador(0).Value
        
        Ador.MoveNext
        lstvwArticuloEsp.ColumnHeaders(2).Text = Ador(0).Value
        lstvwProveEsp.ColumnHeaders(2).Text = Ador(0).Value
        Ador.MoveNext
        m_skb = Ador(0).Value
        
        Ador.Close
    
    End If

End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Public Sub ConfigurarSeguridad()
    Dim oIMaterialAsignado As IMaterialAsignado
    Dim oArt As CArticulo
    Dim oArticulos As CArticulos

    m_bModifArt = True
    m_bModifArtProve = True
    
    'Art�culo
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
        m_bModifArt = False
    Else
        If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestMatComp)) Is Nothing) Then
            'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material, sino no puede modificarlo
            Set oIMaterialAsignado = oUsuarioSummit.comprador
            Set oArticulos = oIMaterialAsignado.DevolverArticulos(g_oArticulo.Cod, , True)
            m_bModifArt = False
            m_bRMatArt = True
            For Each oArt In oArticulos
                If oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And _
                    oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod Then
                    m_bRMatArt = True
                    m_bRMatArt = False
                    Exit For
                End If
            Next
        End If
    End If

    'Articulo/proveedor
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtGestion)) Is Nothing) Then
        m_bModifArtProve = False
    Else
        If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtRestMAtComp)) Is Nothing) Then
            m_bModifArtProve = False
            m_bRMatArtProve = True
            'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material/prove
            Set oIMaterialAsignado = g_oProve
            Set oIMaterialAsignado.ARTICULOS = oFSGSRaiz.Generar_CArticulos
            oIMaterialAsignado.ARTICULOS.Add g_oArticulo.GMN1Cod, g_oArticulo.GMN2Cod, g_oArticulo.GMN3Cod, g_oArticulo.GMN4Cod, g_oArticulo.Cod, g_oArticulo.Den, , , 1
            Set oArticulos = oIMaterialAsignado.DevolverArticulos(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True, False, False)
            If oArticulos.Count > 0 Then m_bModifArtProve = True: m_bRMatArtProve = False
        End If
    End If

    If m_bModifArt = False Then
        cmdAnyadirEspArt.Enabled = False
        cmdEliminarEspArt.Enabled = False
        cmdModificarEspArt.Enabled = False
        cmdExcluirFichEspArt.Enabled = False
        cmdExcluirEspArt.Enabled = False
        cmdExcluirTodosFichArt.Enabled = False
        txtArticuloEsp.Locked = True
        
        If m_bRMatArt Then
            cmdSalvarEspArt.Visible = False
            cmdAbrirEspArt.Visible = False
        End If
    End If

    If Not m_bModifArtProve Then
        cmdAnyadirEspProve.Visible = False
        cmdEliminarEspProve.Visible = False
        cmdModificarEspProve.Visible = False
        cmdIncluirFichEspArt.Enabled = False
        cmdIncluirEspArt.Enabled = False
        cmdIncluirTodosFichArt.Enabled = False
        txtProveEsp.Locked = True
        
        If m_bRMatArtProve Then
            cmdSalvarEspProve.Visible = False
            cmdAbrirEspProve.Visible = False
        End If
    End If

End Sub


Private Sub cmdAbrirEspArt_Click()
Dim Item As MSComctlLib.listItem
Dim udtTipoEsp As TipoEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit

On Error GoTo Error

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
        
    Else
        
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
    
        ' Cargamos el contenido en la esp.
        Set m_oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        Set m_oEsp.Articulo = g_oArticulo
        udtTipoEsp = TipoEspecificacion.EspArticulo
        
        If IsNull(m_oEsp.Ruta) Then 'Archivo adjunto, se lee de la base de datos
            
            Screen.MousePointer = vbHourglass
            teserror = m_oEsp.ComenzarLecturaData(udtTipoEsp)
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Set m_oEsp = Nothing
                Exit Sub
            End If
            
            ArchivoAbrir False, sFileName
        
        Else 'Archivo vinculado
            ArchivoAbrir True, sFileName
        
        End If
        
    End If
    
    Set m_oEsp = Nothing
    Screen.MousePointer = vbNormal

    Exit Sub
    
Error:
    If err.Number = 70 Then
        Resume Next
    End If
End Sub

Private Sub cmdAbrirEspProve_Click()
Dim Item As MSComctlLib.listItem
Dim sFileName As String
Dim sFileTitle As String
Dim oAdjun As CAdjunto
On Error GoTo Error

    Set Item = lstvwProveEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        sFileName = FSGSLibrary.DevolverPathFichTemp
        sFileName = sFileName & Item.Text
        sFileTitle = Item.Text
        'Comprueba si existe el fichero y si existe se borra:
        Dim FOSFile As Scripting.FileSystemObject
        Set FOSFile = New Scripting.FileSystemObject
        If FOSFile.FileExists(sFileName) Then
            FOSFile.DeleteFile sFileName, True
        End If
        Set FOSFile = Nothing
        ' Cargamos el contenido en la esp.
        Set m_oEsp = g_oArtProve.especificaciones.Item(CStr(lstvwProveEsp.selectedItem.Tag))
        Set m_oEsp.Articulo = g_oArtProve
        Set m_oEsp.Proveedor = g_oProve
        If IsNull(m_oEsp.Ruta) Then 'Archivo adjunto, se lee de la base de datos
            Set oAdjun = oFSGSRaiz.generar_cadjunto
            oAdjun.Tipo = TipoAdjunto.ProveedorArticulo
            oAdjun.Id = m_oEsp.Id
            oAdjun.DataSize = m_oEsp.DataSize
            oAdjun.LeerAdjunto sFileName, g_oProve.Cod & "#" & g_oArtProve.Cod
            ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
        Else 'Archivo vinculado
            ArchivoAbrir True, sFileName
        
        End If
        
    End If
    
    Set m_oEsp = Nothing
    Screen.MousePointer = vbNormal

    Exit Sub
    
Error:
    If err.Number = 70 Then
        Resume Next
    End If
End Sub


Private Sub cmdAnyadirEspArt_Click()
Dim teserror As TipoErrorSummit
Dim sRuta As String
Dim sFileName As String
Dim sFileTitle As String
Dim arrFileNames As Variant
Dim oFos As FileSystemObject
Dim iFile As Integer

On Error GoTo Cancelar:
    
    cmmdEsp.filename = ""
    cmmdEsp.DialogTitle = m_sIdioma(1)
    cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    
    cmmdEsp.ShowOpen
    
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then Exit Sub
    
    arrFileNames = ExtraerFicheros(sFileName)
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = True
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.chkProcFich.Top
        frmPROCEComFich.chkProcFich.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2000
    End If
    frmPROCEComFich.sOrigen = "frmCATDatExtAdjun"
    frmPROCEComFich.Show 1
    
    If Not g_bCancelarEsp Then
        Set oFos = New Scripting.FileSystemObject
        If g_bSoloRuta = False Then 'Si es adjunto no pasa por aqui
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                Set m_oEsp = oFSGSRaiz.generar_CEspecificacion
                Set m_oEsp.Articulo = g_oArticulo
                m_oEsp.nombre = sFileTitle
                m_oEsp.Comentario = g_sComentario
                m_oEsp.DataSize = FileLen(sFileName)

                m_oEsp.Ruta = Null
                Screen.MousePointer = vbHourglass
                teserror = m_oEsp.ComenzarEscrituraData(TipoEspecificacion.EspArticulo)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set m_oEsp = Nothing
                    Exit Sub
                End If
                
                Dim sAdjunto As String
                Dim ArrayAdjunto() As String
                Dim oFile As File
                Dim bites As Long
                
                Set oFile = oFos.GetFile(sFileName)
                bites = oFile.Size
                oFos.CopyFile sFileName, FSGSLibrary.DevolverPathFichTemp & arrFileNames(iFile)
                
                sAdjunto = m_oEsp.GrabarAdjunto(FSGSLibrary.DevolverPathFichTemp & CStr(arrFileNames(iFile)), "", CStr(arrFileNames(iFile)), bites, TipoEspecificacion.EspArticulo)
                
                'Creamos un array, cada "substring" se asignar�
                'a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                m_oEsp.Id = ArrayAdjunto(0)
                m_oEsp.DataSize = bites
                m_oEsp.Fecha = Date & " " & Time
                
                g_oArticulo.especificaciones.Add m_oEsp.nombre, m_oEsp.Fecha, m_oEsp.Id, , , m_oEsp.Comentario, , m_oEsp.Articulo, , , m_oEsp.Ruta, , m_oEsp.DataSize
                
                g_oArticulo.EspAdj = 1
                 
                lstvwArticuloEsp.ListItems.Add , "ART" & CStr(m_oEsp.Id), sFileTitle, , "ESP"
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ToolTipText = m_oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(m_oEsp.DataSize / 1024) & " " & m_skb
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Path", NullToStr(m_oEsp.Ruta)
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Com", m_oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Fec", m_oEsp.Fecha
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).Tag = m_oEsp.Id
            Next
    
        Else ' Guardamos la ruta en lugar del archivo
            Screen.MousePointer = vbHourglass
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                sRuta = ArchivoComprobarRuta(sFileName, sFileTitle)
                If sRuta = "" Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set m_oEsp = Nothing
                    Exit Sub
                End If
            
                Set m_oEsp = oFSGSRaiz.generar_CEspecificacion
                Set m_oEsp.Articulo = g_oArticulo
                m_oEsp.nombre = sFileTitle
                m_oEsp.Comentario = g_sComentario
                m_oEsp.DataSize = FileLen(sFileName)
                m_oEsp.Ruta = sRuta
                
                Set g_oIBaseDatos = m_oEsp
                teserror = g_oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set m_oEsp = Nothing
                    Set g_oIBaseDatos = Nothing
                    Exit Sub
                End If
                Set g_oIBaseDatos = Nothing
                g_oArticulo.especificaciones.Add m_oEsp.nombre, m_oEsp.Fecha, m_oEsp.Id, , , m_oEsp.Comentario, , m_oEsp.Articulo, , , m_oEsp.Ruta, , m_oEsp.DataSize
                
                g_oArticulo.EspAdj = 1
                 
                lstvwArticuloEsp.ListItems.Add , "ART" & CStr(m_oEsp.Id), sFileTitle, , "ESP"
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ToolTipText = m_oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(m_oEsp.DataSize / 1024) & " " & m_skb
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Path", NullToStr(m_oEsp.Ruta)
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Com", m_oEsp.Comentario
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).ListSubItems.Add , "Fec", m_oEsp.Fecha
                lstvwArticuloEsp.ListItems.Item("ART" & CStr(m_oEsp.Id)).Tag = m_oEsp.Id
            Next
        End If
        
        basSeguridad.RegistrarAccion accionessummit.ACCArtAdjAnya, "Cod:" & g_oArticulo.Cod & " Archivo:" & m_oEsp.nombre
        lstvwArticuloEsp.Refresh
        Set m_oEsp = Nothing
        Screen.MousePointer = vbNormal
    
    End If

    Exit Sub
    
Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        MsgBox err.Description
        Set m_oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdAnyadirEspProve_Click()
Dim teserror As TipoErrorSummit
Dim sRuta As String
Dim sFileName As String
Dim sFileTitle As String
Dim arrFileNames As Variant
Dim iFile As Integer
Dim oAdjun As CAdjunto
Dim ArrayAdjunto() As String
Dim sPathTemp As String
Dim sAdjunto As String
Dim sNombre As String
On Error GoTo Cancelar:
    
    cmmdEsp.filename = ""
    cmmdEsp.DialogTitle = m_sIdioma(1)
    cmmdEsp.Filter = m_sIdioma(2) & "|*.*"
    cmmdEsp.FLAGS = cdlOFNAllowMultiselect + cdlOFNExplorer
    
    cmmdEsp.ShowOpen
    
    sFileName = cmmdEsp.filename
    sFileTitle = cmmdEsp.FileTitle
    If sFileName = "" Then Exit Sub
    
    arrFileNames = ExtraerFicheros(sFileName)
    ' Ahora obtenemos el comentario para la especificacion
    frmPROCEComFich.chkProcFich.Visible = True
    If UBound(arrFileNames) = 1 Then
        frmPROCEComFich.lblFich = sFileTitle
    Else
        frmPROCEComFich.lblFich = ""
        frmPROCEComFich.Label1.Visible = False
        frmPROCEComFich.lblFich.Visible = False
        frmPROCEComFich.txtCom.Top = frmPROCEComFich.chkProcFich.Top + frmPROCEComFich.chkProcFich.Height + 30
        frmPROCEComFich.chkProcFich.Top = frmPROCEComFich.Label1.Top
        frmPROCEComFich.txtCom.Height = 2000
    End If
    frmPROCEComFich.sOrigen = "frmCATDatExtAdjun"
    frmPROCEComFich.Show 1
    
    If Not g_bCancelarEsp Then
        If g_bSoloRuta = False Then 'Si es adjunto no pasa por aqui
            sPathTemp = FSGSLibrary.DevolverPathFichTemp
            For iFile = 1 To UBound(arrFileNames)
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                'Grabamos en PROVE_ART4
                g_oProve.GenerarPROVE_ART4 g_oArticulo.Cod
                Set oAdjun = oFSGSRaiz.generar_cadjunto
                oAdjun.nombre = sFileTitle
                oAdjun.Comentario = g_sComentario
                oAdjun.Tipo = TipoAdjunto.ProveedorArticulo
                sAdjunto = oAdjun.GrabarAdjunto(arrFileNames(0) & "\", sPathTemp, g_oProve.Cod & "#" & g_oArticulo.Cod)
                
                'Creamos un array, cada "substring" se asignar� a un elemento del array
                ArrayAdjunto = Split(sAdjunto, "#", , vbTextCompare)
                oAdjun.Id = ArrayAdjunto(0)
                oAdjun.SacarFecAct g_oProve.Cod & "#" & g_oArticulo.Cod
                g_oArtProve.especificaciones.Add oAdjun.nombre, oAdjun.FECACT, oAdjun.Id, , , oAdjun.Comentario, , g_oArticulo, g_oProve, , arrFileNames(0) & "\", , oAdjun.DataSize
                
                g_oArtProve.EspAdj = 1
                
                lstvwProveEsp.ListItems.Add , "PROVE" & CStr(oAdjun.Id), sFileTitle, , "ESP"
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(oAdjun.Id)).ToolTipText = oAdjun.Comentario
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(oAdjun.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(oAdjun.DataSize / 1024) & " " & m_skb
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(oAdjun.Id)).ListSubItems.Add , "Path", NullToStr(arrFileNames(0) & "\")
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(oAdjun.Id)).ListSubItems.Add , "Com", oAdjun.Comentario
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(oAdjun.Id)).ListSubItems.Add , "Fec", oAdjun.FECACT
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(oAdjun.Id)).Tag = oAdjun.Id
                sNombre = oAdjun.nombre
                Set oAdjun = Nothing
            Next
        Else ' Guardamos la ruta en lugar del archivo
            For iFile = 1 To UBound(arrFileNames)
                Screen.MousePointer = vbHourglass
                sFileName = arrFileNames(0) & "\" & arrFileNames(iFile)
                sFileTitle = arrFileNames(iFile)
                Set m_oEsp = oFSGSRaiz.generar_CEspecificacion
                Set m_oEsp.Articulo = g_oArtProve
                Set m_oEsp.Proveedor = g_oProve
                m_oEsp.nombre = sFileTitle
                m_oEsp.Comentario = g_sComentario
                m_oEsp.DataSize = FileLen(sFileName)
                sRuta = ArchivoComprobarRuta(sFileName, sFileTitle)
                If sRuta = "" Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set m_oEsp = Nothing
                    Exit Sub
                End If
                
                m_oEsp.Ruta = sRuta
                
                Set g_oIBaseDatos = m_oEsp
                teserror = g_oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set m_oEsp = Nothing
                    Set g_oIBaseDatos = Nothing
                    Exit Sub
                End If
                Set g_oIBaseDatos = Nothing
                If g_oArtProve.especificaciones Is Nothing Then
                    Set g_oArtProve.especificaciones = oFSGSRaiz.Generar_CEspecificaciones
                End If
                g_oArtProve.especificaciones.Add m_oEsp.nombre, m_oEsp.Fecha, m_oEsp.Id, , , m_oEsp.Comentario, , m_oEsp.Articulo, g_oProve, , m_oEsp.Ruta, , m_oEsp.DataSize
                
                g_oArtProve.EspAdj = 1
                 
                lstvwProveEsp.ListItems.Add , "PROVE" & CStr(m_oEsp.Id), sFileTitle, , "ESP"
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(m_oEsp.Id)).ToolTipText = m_oEsp.Comentario
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(m_oEsp.Id)).ListSubItems.Add , "Tamanyo", TamanyoAdjuntos(m_oEsp.DataSize / 1024) & " " & m_skb
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(m_oEsp.Id)).ListSubItems.Add , "Path", NullToStr(m_oEsp.Ruta)
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(m_oEsp.Id)).ListSubItems.Add , "Com", m_oEsp.Comentario
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(m_oEsp.Id)).ListSubItems.Add , "Fec", m_oEsp.Fecha
                lstvwProveEsp.ListItems.Item("PROVE" & CStr(m_oEsp.Id)).Tag = m_oEsp.Id
                sNombre = m_oEsp.nombre
            Next
        End If
            
        basSeguridad.RegistrarAccion accionessummit.ACCMatPorProveAdjAnya, "Prove:" & g_oProve.Cod & " Art:" & g_oArtProve.Cod & " Archivo:" & sNombre
        lstvwProveEsp.Refresh
        Set m_oEsp = Nothing
        Screen.MousePointer = vbNormal
    
    End If

    Exit Sub
    
Cancelar:
    
    If err.Number <> 32755 Then '32755 = han pulsado cancel
        MsgBox err.Description
        Set m_oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdeliminarespart_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
        
On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(3) & ": " & lstvwArticuloEsp.selectedItem.Text)
        If irespuesta = vbNo Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        Set oEsp = g_oArticulo.especificaciones.Item(lstvwArticuloEsp.selectedItem.Index)
        Set oEsp.Articulo = g_oArticulo
        
        Set g_oIBaseDatos = oEsp
        teserror = g_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            g_oArticulo.especificaciones.Remove (lstvwArticuloEsp.selectedItem.Index)
            If g_oArticulo.especificaciones.Count = 0 Then
                If txtArticuloEsp = "" Then
                    g_oArticulo.EspAdj = 0
                End If
            End If
            lstvwArticuloEsp.ListItems.Remove (CStr(lstvwArticuloEsp.selectedItem.key))
        End If
        
    End If
    
Cancelar:
    
    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal

End Sub
Private Sub cmdIncluirFichEspArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer
Dim vVar As Variant
Dim vNum As Variant
    
    Set Item = lstvwArticuloEsp.selectedItem
    
    If Not Item Is Nothing Then
        
        'se a�aden las especificaciones del art�culo al proveedor
        Screen.MousePointer = vbHourglass
        Set oEsp = oFSGSRaiz.generar_CEspecificacion
        Set oEsp.Proveedor = g_oProve
        Set oEsp.Articulo = g_oArticulo
        
        teserror = oEsp.CopiarEspecificacionesArticulo(EspArticulo, EspArticuloProveedor, g_oArticulo.especificaciones.Item(lstvwArticuloEsp.selectedItem.Index).Id)
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Accion = ACCMatPorProveAdjAnya
            oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
            Exit Sub
        End If
                 
        'Buscamos el mayor �ndice de las especificaciones de los �tems
        For i = 1 To lstvwProveEsp.ListItems.Count
            If g_oArtProve.especificaciones.Item(i).Id > maxId Then
                maxId = g_oArtProve.especificaciones.Item(i).Id
            End If
        Next i
        
        maxId = maxId + 1
        lstvwProveEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
        lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
        lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(2) = Item.SubItems(2)
        lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Item.SubItems(3)
        lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(4) = Now
        lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
         
        vVar = Split(Item.SubItems(1), " ")
        If IsNumeric(vVar(0)) Then
            vNum = vVar(0)
        Else
            vNum = Null
        End If
        g_oArtProve.especificaciones.Add Item.Text, Now, maxId, , , StrToNull(Item.SubItems(3)), , g_oArtProve, g_oProve, , StrToNull(Item.SubItems(2)), , vNum
        g_oArtProve.EspAdj = 1
    
        Screen.MousePointer = vbNormal
    
    End If
    
End Sub
Private Sub cmdExcluirFichEspArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer
Dim vVar As Variant
Dim vNum As Variant

    Set Item = lstvwProveEsp.selectedItem

    If Item Is Nothing Then Exit Sub

    'se a�aden las especificaciones del proveedor al art�culo
    
    Screen.MousePointer = vbHourglass
    Set oEsp = oFSGSRaiz.generar_CEspecificacion
    Set oEsp.Articulo = g_oArtProve
    Set oEsp.Proveedor = g_oProve
    
    teserror = oEsp.CopiarEspecificacionesArticulo(EspArticuloProveedor, EspArticulo, g_oArtProve.especificaciones.Item(lstvwProveEsp.selectedItem.Index).Id)

    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Accion = ACCArtAdjAnya
        oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
        Exit Sub
    End If

    'Buscamos el mayor �ndice de las especificaciones de los art�culos
    For i = 1 To lstvwArticuloEsp.ListItems.Count
        If g_oArticulo.especificaciones.Item(i).Id > maxId Then
            maxId = g_oArticulo.especificaciones.Item(i).Id
        End If
    Next i

    maxId = maxId + 1
    lstvwArticuloEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(2) = Item.SubItems(2)
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Item.SubItems(3)
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(4) = Now
    lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
    
    vVar = Split(Item.SubItems(1), " ")
    If IsNumeric(vVar(0)) Then
        vNum = vVar(0)
    Else
        vNum = Null
    End If
    g_oArticulo.especificaciones.Add Item.Text, Now, maxId, , , StrToNull(Item.SubItems(3)), , g_oArticulo, , , StrToNull(Item.SubItems(2)), , vNum
    g_oArticulo.EspAdj = 1

    Screen.MousePointer = vbNormal
        
End Sub
Private Sub cmdIncluirTodosFichArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer
Dim vVar As Variant
Dim vNum As Variant

    If lstvwArticuloEsp.ListItems.Count = 0 Then
        Exit Sub
    End If

    Set Item = lstvwArticuloEsp.ListItems.Item(1)
    If Item Is Nothing Then
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass
    
    'se a�aden las especificaciones del art�culo al proveedor:
    Set oEsp = oFSGSRaiz.generar_CEspecificacion
    Set oEsp.Articulo = g_oArticulo
    Set oEsp.Proveedor = g_oProve
    
    teserror = oEsp.CopiarEspecificacionesArticulo(EspArticulo, EspArticuloProveedor)
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Accion = ACCMatPorProveAdjAnya
        oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
        Exit Sub
    End If

    'Buscamos el mayor �ndice de las especificaciones de los �tems
    For i = 1 To lstvwProveEsp.ListItems.Count
        If g_oArtProve.especificaciones.Item(i).Id > maxId Then
            maxId = g_oArtProve.especificaciones.Item(i).Id
        End If
    Next

    For i = 1 To lstvwArticuloEsp.ListItems.Count

        Set Item = lstvwArticuloEsp.ListItems.Item(i)

        If Not Item Is Nothing Then

            maxId = maxId + 1
            lstvwProveEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
            lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
            lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(2) = Item.SubItems(2)
            lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Item.SubItems(3)
            lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(4) = Now
            lstvwProveEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
            
            vVar = Split(Item.SubItems(1), " ")
            If IsNumeric(vVar(0)) Then
                vNum = vVar(0)
            Else
                vNum = Null
            End If
            g_oArtProve.especificaciones.Add Item.Text, Now, maxId, , , StrToNull(Item.SubItems(3)), , g_oArtProve, g_oProve, , StrToNull(Item.SubItems(2)), , vNum

        End If

    Next

    g_oArtProve.EspAdj = 1
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub cmdExcluirTodosFichArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim maxId As Integer
Dim vVar As Variant
Dim vNum As Variant

    If lstvwProveEsp.ListItems.Count = 0 Then Exit Sub

    Set Item = lstvwProveEsp.ListItems.Item(1)
    If Item Is Nothing Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    'se a�aden las especificaciones de los �tems a los art�culos
    Set oEsp = oFSGSRaiz.generar_CEspecificacion
    Set oEsp.Articulo = g_oArtProve
    Set oEsp.Proveedor = g_oProve
            
    teserror = oEsp.CopiarEspecificacionesArticulo(EspArticuloProveedor, EspArticulo)
             
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Accion = ACCArtAdjAnya
        oMensajes.ImposibleAnyadirEspecificaciones teserror.Arg1
        Exit Sub
    End If
            
    'Buscamos el mayor �ndice de las especificaciones de los art�culos
    For i = 1 To lstvwArticuloEsp.ListItems.Count
        If g_oArticulo.especificaciones.Item(i).Id > maxId Then
            maxId = g_oArticulo.especificaciones.Item(i).Id
        End If
    Next
    
    For i = 1 To lstvwProveEsp.ListItems.Count
        Set Item = lstvwProveEsp.ListItems.Item(i)
        If Not Item Is Nothing Then
            maxId = maxId + 1
            lstvwArticuloEsp.ListItems.Add , "ITEM" & CStr(maxId), Item.Text, , "ESP"
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(1) = Item.SubItems(1)
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(2) = Item.SubItems(2)
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(3) = Item.SubItems(3)
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).SubItems(4) = Now
            lstvwArticuloEsp.ListItems.Item("ITEM" & CStr(maxId)).Tag = maxId
            vVar = Split(Item.SubItems(1), " ")
            If IsNumeric(vVar(0)) Then
                vNum = vVar(0)
            Else
                vNum = Null
            End If
            g_oArticulo.especificaciones.Add Item.Text, Now, maxId, , , StrToNull(Item.SubItems(3)), , g_oArticulo, , , StrToNull(Item.SubItems(2)), , vNum
        End If
    Next i
    
    g_oArticulo.EspAdj = 1

    Screen.MousePointer = vbNormal
    
End Sub



Private Sub cmdSalvarEspArt_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim teserror As TipoErrorSummit
Dim oFos As FileSystemObject
    
On Error GoTo Cancelar:

    Set Item = lstvwArticuloEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    
    Else
        
        cmmdEsp.DialogTitle = m_sIdioma(6)
        cmmdEsp.Filter = m_sIdioma(7) & "|*.*"
    
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdioma(8)
            Exit Sub
        End If
                
        ' Cargamos el contenido en la esp.
        Set oEsp = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        
        If IsNull(oEsp.Ruta) Then 'Archivo adjunto
            Screen.MousePointer = vbHourglass
            teserror = oEsp.ComenzarLecturaData(TipoEspecificacion.EspArticulo)
            If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    basErrores.TratarError teserror
                    Set oEsp = Nothing
                    Exit Sub
            End If
                
            oEsp.LeerAdjunto oEsp.Id, TipoEspecificacion.EspArticulo, oEsp.DataSize, sFileName
        Else 'Archivo vinculado
            Set oFos = New FileSystemObject
            If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
                oFos.CopyFile oEsp.Ruta & oEsp.nombre, sFileName, False
                Exit Sub
            Else
                oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
            End If
            
            Set oFos = Nothing
            Set oEsp = Nothing
        End If
    End If
    
Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdSalvarEspProve_click()
Dim Item As MSComctlLib.listItem
Dim DataFile As Integer
Dim oEsp As CEspecificacion
Dim sFileName As String
Dim sFileTitle As String
Dim oFos As FileSystemObject
Dim oAdjun As CAdjunto
On Error GoTo Cancelar:
    Set Item = lstvwProveEsp.selectedItem
    
    If Item Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        cmmdEsp.DialogTitle = m_sIdioma(6)
        cmmdEsp.Filter = m_sIdioma(7) & "|*.*"
    
        cmmdEsp.filename = Item.Text
        cmmdEsp.ShowSave
                
        sFileName = cmmdEsp.filename
        sFileTitle = cmmdEsp.FileTitle
        If sFileTitle = "" Then
            oMensajes.NoValido m_sIdioma(8)
            Exit Sub
        End If
        DataFile = 1
        
        Screen.MousePointer = vbHourglass
            
        ' Cargamos el contenido en la esp.
        Set oEsp = g_oArtProve.especificaciones.Item(CStr(lstvwProveEsp.selectedItem.Tag))
        Set oEsp.Proveedor = g_oProve
        
        If IsNull(oEsp.Ruta) Then 'Archivo adjunto
            Set oAdjun = oFSGSRaiz.generar_cadjunto
            oAdjun.Tipo = TipoAdjunto.ProveedorArticulo
            oAdjun.Id = oEsp.Id
            oAdjun.DataSize = oEsp.DataSize
            oAdjun.LeerAdjunto sFileName, g_oProve.Cod & "#" & g_oArtProve.Cod
        Else
            'Archivo vinculado
            Set oFos = New FileSystemObject
            If oFos.FolderExists(oEsp.Ruta) And oFos.FileExists(oEsp.Ruta & oEsp.nombre) Then
                Screen.MousePointer = vbNormal
                oFos.CopyFile oEsp.Ruta & oEsp.nombre, sFileName, False
                Exit Sub
            Else
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarEsp (oEsp.nombre & " - " & oEsp.Ruta)
            End If
            
            Set oFos = Nothing
            Set oEsp = Nothing

        End If
        
    End If
    
    Screen.MousePointer = vbNormal
Cancelar:

    If err.Number <> 32755 Then '32755 = han pulsado cancel
        If DataFile <> 0 Then
            Close DataFile
        End If
        Set oEsp = Nothing
        Set g_oIBaseDatos = Nothing
    End If
    Screen.MousePointer = vbNormal
    
End Sub
Private Sub cmdModificarEspArt_click()
Dim teserror As TipoErrorSummit
 
    If lstvwArticuloEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        
        Screen.MousePointer = vbHourglass
        
        Set g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag)).Articulo = g_oArticulo
        Set g_oIBaseDatos = g_oArticulo.especificaciones.Item(CStr(lstvwArticuloEsp.selectedItem.Tag))
        teserror = g_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
        
        basSeguridad.RegistrarAccion accionessummit.ACCArtadjMod, "Art:" & g_oArticulo.Cod & "Archivo:" & CStr(lstvwArticuloEsp.selectedItem.Text)
        
        Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
        frmPROCEEspMod.g_sOrigen = "frmCATDatExtAdjunArt"
        Screen.MousePointer = vbNormal
        frmPROCEEspMod.Show 1
    
    End If

End Sub
Private Sub cmdModificarEspProve_click()
Dim teserror As TipoErrorSummit

    If lstvwProveEsp.selectedItem Is Nothing Then
        oMensajes.SeleccioneFichero
    Else
        
        Screen.MousePointer = vbHourglass
         
        Set g_oArtProve.especificaciones.Item(CStr(lstvwProveEsp.selectedItem.Tag)).Articulo = g_oArtProve
        Set g_oArtProve.especificaciones.Item(CStr(lstvwProveEsp.selectedItem.Tag)).Proveedor = g_oProve
        Set g_oIBaseDatos = g_oArtProve.especificaciones.Item(CStr(lstvwProveEsp.selectedItem.Tag))
        
        teserror = g_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Set g_oIBaseDatos = Nothing
            Exit Sub
        End If
        
        basSeguridad.RegistrarAccion accionessummit.ACCMatPorProveAdjMod, "Prove:" & g_oProve.Cod & " Art:" & g_oArticulo.Cod & "Archivo:" & CStr(lstvwProveEsp.selectedItem.Text)
        
        Set frmPROCEEspMod.g_oIBaseDatos = g_oIBaseDatos
        frmPROCEEspMod.g_sOrigen = "frmCATDatExtAdjunProve"
        Screen.MousePointer = vbNormal
        frmPROCEEspMod.Show 1
    
    End If
    
End Sub
Private Sub cmdEliminarEspProve_click()
Dim Item As MSComctlLib.listItem
Dim oEsp As CEspecificacion
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

On Error GoTo Cancelar:

    Set Item = lstvwProveEsp.selectedItem

    If Item Is Nothing Then
        oMensajes.SeleccioneFichero

    Else

        irespuesta = oMensajes.PreguntaEliminar(m_sIdioma(3) & ": " & lstvwProveEsp.selectedItem.Text)
        If irespuesta = vbNo Then
            Exit Sub
        End If
        Screen.MousePointer = vbHourglass
        
        Set oEsp = g_oArtProve.especificaciones.Item(lstvwProveEsp.selectedItem.Index)
        Set oEsp.Proveedor = g_oProve
        Set oEsp.Articulo = g_oArtProve
        
        Set g_oIBaseDatos = oEsp
        teserror = g_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            g_oArtProve.especificaciones.Remove (lstvwProveEsp.selectedItem.Index)
            If g_oArtProve.especificaciones.Count = 0 Then
                If txtProveEsp = "" Then
                    g_oArtProve.EspAdj = 0
                End If
            End If

            lstvwProveEsp.ListItems.Remove (CStr(lstvwProveEsp.selectedItem.key))
        End If

    End If

Cancelar:

    Set oEsp = Nothing
    Set g_oIBaseDatos = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub cmdExcluirEspArt_Click()
    If txtProveEsp.Text <> "" Then
        txtArticuloEsp.Text = txtProveEsp.Text
        Accion = ACCArtadjMod
        txtArticuloEsp_Validate False
    End If
End Sub

Private Sub cmdIncluirEspArt_Click()
    g_bRespetarCombo = True
    txtProveEsp.Text = txtArticuloEsp.Text
    g_bRespetarCombo = False
    Accion = ACCMatPorProveAdjMod
    txtProveEsp_Validate False
End Sub


Private Sub Form_Activate()
    If txtProveEsp.Enabled Then
        If Me.Visible Then txtProveEsp.SetFocus
    End If
End Sub

Private Sub Form_Load()

    Me.Width = 9870
    Me.Height = 4575
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    'Array que contendra los ficheros eliminados
    ReDim sayFileNames(0)
    
    CargarEspecificaciones
    
    CargarRecursos
    
    ConfigurarSeguridad

    'Configurar parametros del commondialog
    cmmdEsp.FLAGS = cdlOFNHideReadOnly
End Sub


Private Sub Form_Resize()

    If Me.Height < 3150 Then Exit Sub
    If Me.Width < 500 Then Exit Sub
    
    fraEspArt.Width = Me.Width / 2 - 480
    fraEspArt.Height = Me.Height - 555
    
    fraEspProve.Width = fraEspArt.Width
    fraEspProve.Height = fraEspArt.Height
    fraEspProve.Left = fraEspArt.Width + 765
    
    lstvwArticuloEsp.Width = fraEspArt.Width - 180
    txtArticuloEsp.Width = lstvwArticuloEsp.Width
    lstvwArticuloEsp.Height = fraEspArt.Height * 0.35
    txtArticuloEsp.Height = fraEspArt.Height - lstvwArticuloEsp.Height - 1200
    lblArticuloFich.Top = txtArticuloEsp.Top + txtArticuloEsp.Height + 45
    lstvwArticuloEsp.Top = lblArticuloFich.Top + 240
    
    lstvwProveEsp.Width = lstvwArticuloEsp.Width
    lstvwProveEsp.Height = lstvwArticuloEsp.Height
    lstvwProveEsp.Top = lstvwArticuloEsp.Top
    txtProveEsp.Width = txtArticuloEsp.Width
    txtProveEsp.Height = txtArticuloEsp.Height
    lblProveFich.Top = lblArticuloFich.Top
    
    cmdIncluirEspArt.Left = fraEspArt.Width + 165
    cmdIncluirFichEspArt.Left = cmdIncluirEspArt.Left
    cmdExcluirEspArt.Left = cmdIncluirEspArt.Left
    cmdExcluirFichEspArt.Left = cmdIncluirEspArt.Left
    cmdIncluirTodosFichArt.Left = cmdIncluirEspArt.Left
    cmdExcluirTodosFichArt.Left = cmdIncluirEspArt.Left
    
    cmdIncluirFichEspArt.Top = lstvwArticuloEsp.Top + 60
    cmdExcluirFichEspArt.Top = cmdIncluirFichEspArt.Top + cmdIncluirFichEspArt.Height + 40
    cmdIncluirTodosFichArt.Top = cmdExcluirFichEspArt.Top + cmdExcluirFichEspArt.Height + 145
    cmdExcluirTodosFichArt.Top = cmdIncluirTodosFichArt.Top + cmdIncluirTodosFichArt.Height + 40
    
    
    lstvwArticuloEsp.ColumnHeaders(1).Width = lstvwArticuloEsp.Width * 0.25
    lstvwArticuloEsp.ColumnHeaders(2).Width = lstvwArticuloEsp.Width * 0.2
    lstvwArticuloEsp.ColumnHeaders(3).Width = lstvwArticuloEsp.Width * 0.25
    lstvwArticuloEsp.ColumnHeaders(4).Width = lstvwArticuloEsp.Width * 0.28

    lstvwProveEsp.ColumnHeaders(1).Width = lstvwProveEsp.Width * 0.25
    lstvwProveEsp.ColumnHeaders(2).Width = lstvwProveEsp.Width * 0.2
    lstvwProveEsp.ColumnHeaders(3).Width = lstvwProveEsp.Width * 0.25
    lstvwProveEsp.ColumnHeaders(4).Width = lstvwProveEsp.Width * 0.28


    picEditArt.Left = fraEspArt.Width - 2475
    picEditArt.Top = lstvwArticuloEsp.Top + lstvwArticuloEsp.Height + 65
    picEditProve.Left = fraEspProve.Width - 2435
    picEditProve.Top = picEditArt.Top
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim i As Integer
    Dim FOSFile As Scripting.FileSystemObject
    Dim bBorrando As Boolean
    
    On Error GoTo Error:
    
    Select Case Accion
        Case accionessummit.ACCArtadjMod
            txtArticuloEsp_Validate False

        Case accionessummit.ACCMatPorProveAdjMod
            txtProveEsp_Validate False

    End Select
    
    'Borramos los archivos temporales que hayamos creado
    Set FOSFile = New Scripting.FileSystemObject
    i = 0
    While i < UBound(sayFileNames)
        bBorrando = True
        If FOSFile.FileExists(sayFileNames(i)) Then
            FOSFile.DeleteFile sayFileNames(i), True
        End If
        
        i = i + 1
    Wend
    bBorrando = False
    Set FOSFile = Nothing
    
    'Para mostrar el icono al refrescar la grid de Im�genes y atributos:
    If lstvwProveEsp.ListItems.Count = 0 Then
        g_oArtProve.ConEspecific = False
    Else
        g_oArtProve.ConEspecific = True
    End If
    
    Exit Sub

Error:
    
    If bBorrando Then
        basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete)) = sayFileNames(i)
        ReDim Preserve basPublic.g_aFileNamesToDelete(UBound(basPublic.g_aFileNamesToDelete) + 1)
        Resume Next
    End If
    
End Sub


Private Sub txtArticuloEsp_Change()
        
    If Not g_bRespetarCombo Then
        If Accion <> ACCArtadjMod Then
            Accion = ACCArtadjMod
        End If
    End If

End Sub


Private Sub txtArticuloEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit

    If Accion = ACCArtadjMod Then
    
        If StrComp(NullToStr(g_oArticulo.esp), txtArticuloEsp.Text, vbTextCompare) <> 0 Then
                
            g_oArticulo.esp = StrToNull(txtArticuloEsp.Text)
            If txtArticuloEsp <> "" Then
                g_oArticulo.EspAdj = 1
            Else
                If g_oArticulo.especificaciones.Count = 0 Then
                    g_oArticulo.EspAdj = 0
                End If
            End If
            
            Screen.MousePointer = vbHourglass
            ''usuario para la integraci�n
            g_oArticulo.Usuario = basOptimizacion.gvarCodUsuario
            teserror = g_oArticulo.ModificarEspecificacion
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCArtAdjCon
                Exit Sub
            End If
            
            'Registrar accion
            basSeguridad.RegistrarAccion Accion, " Gmn1:" & g_oArticulo.GMN1Cod & " Gmn2:" & g_oArticulo.GMN2Cod & " Gmn3:" & g_oArticulo.GMN3Cod & " Gmn4:" & g_oArticulo.GMN4Cod & " Art:" & g_oArticulo.Cod & " Esp:" & Left(g_oArticulo.esp, 50)
            Accion = ACCMatPorProveAdjMod
            Screen.MousePointer = vbNormal
        
        End If
    
    End If

End Sub


Private Sub txtProveEsp_Change()
        
    If Accion <> ACCMatPorProveAdjMod Then
        Accion = ACCMatPorProveAdjMod
    End If

End Sub


Private Sub txtProveEsp_Validate(Cancel As Boolean)
Dim teserror As TipoErrorSummit

    If Accion = ACCMatPorProveAdjMod Then

        If StrComp(NullToStr(g_oArtProve.esp), txtProveEsp.Text, vbTextCompare) <> 0 Then

            g_oArtProve.esp = StrToNull(txtProveEsp.Text)
            If txtProveEsp.Text <> "" Then
                g_oArtProve.EspAdj = 1
            Else
                If g_oArtProve.especificaciones.Count = 0 Then
                    g_oArtProve.EspAdj = 0
                End If
            End If

            Screen.MousePointer = vbHourglass
            teserror = g_oArtProve.ModificarEspecificacion(g_oProve.Cod)
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Cancel = True
                Accion = ACCMatPorProveCon
                Exit Sub
            End If

            'Registrar accion
            basSeguridad.RegistrarAccion Accion, "Prove:" & g_oProve.Cod & " Gmn1:" & g_oArticulo.GMN1Cod & " Gmn2:" & g_oArticulo.GMN2Cod & " Gmn3:" & g_oArticulo.GMN3Cod & " Gmn4:" & g_oArticulo.GMN4Cod & " Art:" & g_oArticulo.Cod & " Esp:" & Left(g_oArticulo.esp, 50)
            Accion = ACCMatPorProveAdjMod
            Screen.MousePointer = vbNormal
            g_bRespetarCombo = False

        End If
    End If
    
End Sub


Private Sub CargarEspecificaciones()

    'Carga las especificaciones del art�culo:
    If g_oArticulo Is Nothing Then Unload Me

    Set g_oArticulo = oFSGSRaiz.Generar_CArticulo
    g_oArticulo.GMN1Cod = g_oArtProve.GMN1Cod
    g_oArticulo.GMN2Cod = g_oArtProve.GMN2Cod
    g_oArticulo.GMN3Cod = g_oArtProve.GMN3Cod
    g_oArticulo.GMN4Cod = g_oArtProve.GMN4Cod
    g_oArticulo.Cod = g_oArtProve.Cod
    g_oArticulo.Den = g_oArtProve.Den
    
    g_oArticulo.CargarTodasLasEspecificaciones , , True
    
    Set g_oIBaseDatos = g_oArticulo
    g_oIBaseDatos.IniciarEdicion
    Set g_oIBaseDatos = Nothing
    
    g_bRespetarCombo = True
    txtArticuloEsp.Text = NullToStr(g_oArticulo.esp)
    g_bRespetarCombo = False
    AnyadirEspsAListaArt


    'Ahora a�ade las especificaciones del proveedor para el art�culo:
    If g_oArtProve Is Nothing Then Unload Me
    g_oArtProve.CargarTodasLasEspecificaciones , , True, g_oProve.Cod
    g_bRespetarCombo = True
    txtProveEsp.Text = NullToStr(g_oArtProve.esp)
    g_bRespetarCombo = False
    AnyadirEspsAListaProve

End Sub



Private Sub ArchivoAbrir(ByVal bSoloRuta As Boolean, ByVal sFileName As String)
    Dim oFos As FileSystemObject
    
    On Error GoTo Cancelar:

    If bSoloRuta Then
        m_oEsp.LeerAdjunto m_oEsp.Id, TipoEspecificacion.EspArticulo, m_oEsp.DataSize, sFileName
        
        'Lanzamos la aplicacion
        ShellExecute MDI.hWnd, "Open", sFileName, 0&, "C:\", 1
    Else
        Set oFos = New FileSystemObject
        If oFos.FolderExists(m_oEsp.Ruta) And oFos.FileExists(m_oEsp.Ruta & m_oEsp.nombre) Then
            ShellExecute MDI.hWnd, "Open", m_oEsp.Ruta & m_oEsp.nombre, 0&, m_oEsp.Ruta, 1
        Else
            oMensajes.ImposibleModificarEsp (m_oEsp.Ruta & m_oEsp.nombre)
        End If
    End If
    Set m_oEsp = Nothing
    Exit Sub
    
Cancelar:
   
    Set m_oEsp = Nothing
End Sub


Private Function ArchivoComprobarRuta(ByVal sFileFull As String, ByVal sFile As String) As String
Dim sdrive As String
Dim sdrive2 As String
Dim sRuta As String
Dim oFos As FileSystemObject
Dim oDrive As Scripting.Drive

    Set oFos = New FileSystemObject

    sdrive = oFos.GetDriveName(sFileFull)
    sRuta = Left(sFileFull, Len(sFileFull) - Len(sFile))
    sRuta = Right(sRuta, Len(sRuta) - Len(sdrive))
    Set oDrive = oFos.GetDrive(sdrive)
    sdrive2 = oDrive.ShareName
    
    If sdrive2 = "" Then
        sRuta = sdrive & sRuta
    Else
        sRuta = sdrive2 & sRuta
    End If
    Set oFos = Nothing
    Set oDrive = Nothing
    ArchivoComprobarRuta = sRuta
    
End Function

