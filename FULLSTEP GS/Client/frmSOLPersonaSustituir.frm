VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSOLPersonaSustituir 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DSustituir persona"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5910
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLPersonaSustituir.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6615
   ScaleWidth      =   5910
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton optTipoSustTodas 
      BackColor       =   &H00808000&
      Caption         =   "DSustituir en todas las solicitudes (incluyendo finalizadas)"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   100
      TabIndex        =   8
      Top             =   5500
      Value           =   -1  'True
      Width           =   5500
   End
   Begin VB.OptionButton optTipoSustEnCurso 
      BackColor       =   &H00808000&
      Caption         =   "DSustituir solo en las solicitudes en curso"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   100
      TabIndex        =   7
      Top             =   5200
      Width           =   5500
   End
   Begin VB.TextBox tbBuscarCodigo 
      Height          =   285
      Left            =   2000
      TabIndex        =   5
      Top             =   740
      Width           =   1980
   End
   Begin VB.CommandButton cmdBuscarCodigo 
      Height          =   295
      Left            =   4000
      Picture         =   "frmSOLPersonaSustituir.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   740
      Width           =   335
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Continuar >>"
      Default         =   -1  'True
      Height          =   315
      Left            =   4300
      TabIndex        =   2
      Top             =   5900
      Width           =   1500
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   100
      TabIndex        =   1
      Top             =   5900
      Width           =   1005
   End
   Begin MSComctlLib.TreeView tvwEstrOrg 
      Height          =   3900
      Left            =   100
      TabIndex        =   0
      Top             =   1200
      Width           =   5700
      _ExtentX        =   10054
      _ExtentY        =   6879
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2040
      Top             =   5900
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":0D03
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":10CB
            Key             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":11D5
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":1529
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":187D
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":1BD1
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":1F65
            Key             =   "PerUsu"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":206F
            Key             =   "PersonaBaja"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLPersonaSustituir.frx":20FA
            Key             =   "PerUsuBaja"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblBusqueda 
      BackColor       =   &H00808000&
      Caption         =   "B�squeda por c�digo :"
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   100
      TabIndex        =   6
      Top             =   740
      Width           =   1665
   End
   Begin VB.Label lblMensaje 
      BackColor       =   &H00808000&
      Caption         =   "xxx est� incluida en alg�n workflow de aprobaci�n.Seleccione otra persona para sustituirla en los workflows."
      ForeColor       =   &H00FFFFFF&
      Height          =   600
      Left            =   100
      TabIndex        =   3
      Top             =   120
      Width           =   5700
   End
End
Attribute VB_Name = "frmSOLPersonaSustituir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables para interactuar con otros forms
Public bRUO As Boolean
Public bRDep As Boolean

Public g_sCodPersona As String
Public g_sDenominacion As String
Public g_sOrigen As String
Public g_sUON1 As String
Public g_sUON2 As String
Public g_sUON3 As String
Public g_sDEP As String

' Variable de control de flujo
Public Accion As AccionesSummit

'Variables privadas
Private m_sTituloPer As String
Private m_sTituloUON As String
Private m_sMensajePer As String
Private m_sMensajeUON As String
Private m_sMensajePerAprobador As String


Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String

    Set nodx = tvwestrorg.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    If nodx.Tag = "UON0" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Select Case g_sOrigen
        Case "frmESTRORG_UON"  'Va a sustituir una unidad organizativa o un departamento
            If Left(nodx.key, 3) = "UON" Then   'Ha seleccionado una unidad organizativa
                scod1 = DevolverCod(nodx)
                
                Select Case nodx.Image
                    Case "UON1"
                        If g_sUON1 = scod1 And g_sUON2 = "" And g_sUON3 = "" Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOtraUnidadOrg
                            If Me.Visible Then tvwestrorg.SetFocus
                            Exit Sub
                        End If
                        frmESTRORG.g_sSustUON1 = scod1
                        frmESTRORG.g_sSustUON2 = ""
                        frmESTRORG.g_sSustUON3 = ""
                        
                    Case "UON2"
                        If g_sUON1 = DevolverCod(nodx.Parent) And g_sUON2 = scod1 And g_sUON3 = "" Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOtraUnidadOrg
                            If Me.Visible Then tvwestrorg.SetFocus
                            Exit Sub
                        End If
                        frmESTRORG.g_sSustUON1 = DevolverCod(nodx.Parent)
                        frmESTRORG.g_sSustUON2 = scod1
                        frmESTRORG.g_sSustUON3 = ""
    
                    Case "UON3"
                        If g_sUON1 = DevolverCod(nodx.Parent.Parent) And g_sUON2 = DevolverCod(nodx.Parent) And g_sUON3 = scod1 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOtraUnidadOrg
                            If Me.Visible Then tvwestrorg.SetFocus
                            Exit Sub
                        End If
                        frmESTRORG.g_sSustUON1 = DevolverCod(nodx.Parent.Parent)
                        frmESTRORG.g_sSustUON2 = DevolverCod(nodx.Parent)
                        frmESTRORG.g_sSustUON3 = scod1
                End Select
        
            ElseIf Left(nodx.key, 3) = "DEP" Then   'Ha seleccionado un departamento
                scod1 = DevolverCod(nodx)
                Select Case Left(nodx.Tag, 4)
                    Case "DEP0"
                        If g_sUON1 = "" And g_sUON2 = "" And g_sUON3 = "" And g_sDEP = scod1 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOtroDepartamento
                            If Me.Visible Then tvwestrorg.SetFocus
                            Exit Sub
                        End If
                        
                        frmESTRORG.g_sSustUON1 = ""
                        frmESTRORG.g_sSustUON2 = ""
                        frmESTRORG.g_sSustUON3 = ""
                        
                    Case "DEP1"
                        If g_sUON1 = DevolverCod(nodx.Parent) And g_sUON2 = "" And g_sUON3 = "" And g_sDEP = scod1 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOtroDepartamento
                            If Me.Visible Then tvwestrorg.SetFocus
                            Exit Sub
                        End If
                        
                        frmESTRORG.g_sSustUON1 = DevolverCod(nodx.Parent)
                        frmESTRORG.g_sSustUON2 = ""
                        frmESTRORG.g_sSustUON3 = ""
                        
                    Case "DEP2"
                        If g_sUON1 = DevolverCod(nodx.Parent.Parent) And g_sUON2 = DevolverCod(nodx.Parent) And g_sUON3 = "" And g_sDEP = scod1 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOtroDepartamento
                            If Me.Visible Then tvwestrorg.SetFocus
                            Exit Sub
                        End If
                        
                        frmESTRORG.g_sSustUON1 = DevolverCod(nodx.Parent.Parent)
                        frmESTRORG.g_sSustUON2 = DevolverCod(nodx.Parent)
                        frmESTRORG.g_sSustUON3 = ""
                
                    Case "DEP3"
                        If g_sUON1 = DevolverCod(nodx.Parent.Parent.Parent) And g_sUON2 = DevolverCod(nodx.Parent.Parent) And g_sUON3 = DevolverCod(nodx.Parent) And g_sDEP = scod1 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.SeleccioneOtroDepartamento
                            If Me.Visible Then tvwestrorg.SetFocus
                            Exit Sub
                        End If
                        
                        frmESTRORG.g_sSustUON1 = DevolverCod(nodx.Parent.Parent.Parent)
                        frmESTRORG.g_sSustUON2 = DevolverCod(nodx.Parent.Parent)
                        frmESTRORG.g_sSustUON3 = DevolverCod(nodx.Parent)
                End Select
                              
                frmESTRORG.g_sSustDep = scod1
                        
            
            Else   'Ha seleccionado una persona
                frmESTRORG.g_sSustUON1 = ""
                frmESTRORG.g_sSustUON2 = ""
                frmESTRORG.g_sSustUON3 = ""
                frmESTRORG.g_sSustDep = ""
                scod1 = DevolverCod(nodx)
                If nodx.Image = "PerUsu" Then
                    frmESTRORG.g_sSustituto = scod1
                Else
                    Screen.MousePointer = vbNormal
                    oMensajes.PersonaNoValida
                    If Me.Visible Then tvwestrorg.SetFocus
                    Exit Sub
                End If
            End If
            
        Case "frmESTRORG_PER"   'Va a sustituir una persona
            scod1 = DevolverCod(nodx)
            
            If nodx.Image = "PerUsu" Then
                If scod1 = g_sCodPersona Then  'No le deja seleccionar la misma persona que va a dar de baja
                    Screen.MousePointer = vbNormal
                    oMensajes.SeleccioneOtraPersona
                    If Me.Visible Then tvwestrorg.SetFocus
                    Exit Sub
                End If
                frmESTRORG.g_sSustituto = scod1
                frmESTRORG.bSustituirTodas = optTipoSustTodas.Value
            Else
                Screen.MousePointer = vbNormal
                oMensajes.PersonaNoValida
                If Me.Visible Then tvwestrorg.SetFocus
                Exit Sub
            End If
            
        Case "frmESTRORG_PER_CAT"   'Va a sustituir una persona de aprobador en el catalogo
            scod1 = DevolverCod(nodx)
            
            If nodx.Image = "PerUsu" Then
                If scod1 = g_sCodPersona Then  'No le deja seleccionar la misma persona que va a dar de baja
                    Screen.MousePointer = vbNormal
                    oMensajes.SeleccioneOtraPersona
                    If Me.Visible Then tvwestrorg.SetFocus
                    Exit Sub
                End If
                frmESTRORG.g_sSustitutoEnCat = scod1
            Else
                Screen.MousePointer = vbNormal
                oMensajes.PersonaNoValida
                If Me.Visible Then tvwestrorg.SetFocus
                Exit Sub
            End If
            
    End Select
    
    Screen.MousePointer = vbNormal
    
    Unload Me

End Sub

Private Sub cmdBuscarCodigo_Click()
    Dim blnEncontrado As Boolean
    Dim oNodo As node
    
    blnEncontrado = False
    
    For Each oNodo In tvwestrorg.Nodes
        If UCase(Mid(oNodo.Tag, 5)) = UCase(tbBuscarCodigo.Text) Then
            blnEncontrado = True
            Exit For
        End If
    Next
    
    If blnEncontrado Then
        Set tvwestrorg.selectedItem = oNodo
        oNodo.EnsureVisible
    Else
        Set tvwestrorg.selectedItem = tvwestrorg.Nodes(1)
    End If
    tvwestrorg.SetFocus
    
    Set oNodo = Nothing
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim sAprobador As String


' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_SUSTITUIR_PER, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        m_sTituloPer = Ador(0).Value   '1 Seleccionar persona
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '2 Con&tinuar >>
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '3 &Cancelar
        Ador.MoveNext
        m_sMensajePer = Ador(0).Value & " " & g_sCodPersona & "-" & g_sDenominacion  '4 La persona
        m_sMensajePerAprobador = Ador(0).Value & " " & g_sCodPersona & "-" & g_sDenominacion  '4 La persona
        Ador.MoveNext
        m_sMensajePer = m_sMensajePer & " " & Ador(0).Value   '5 est� incluida en alg�n flujo de trabajo.
        Ador.MoveNext
        m_sMensajePer = m_sMensajePer & Ador(0).Value  '6 Seleccione otra persona para sustituirla en dichos flujos de trabajo.
        sAprobador = Ador(0).Value  '6 Seleccione otra persona para sustituirla en dichos flujos de trabajo.
        Ador.MoveNext
        m_sTituloUON = Ador(0).Value   '7 Seleccione persona,departamento o unidad organizativa
        Ador.MoveNext
        m_sMensajeUON = Ador(0).Value '8 est� incluido en alg�n flujo de trabajo.
        Ador.MoveNext
        m_sMensajeUON = m_sMensajeUON & Ador(0).Value '9 Seleccione otra unidad organizativa,departamento persona para sustituir en dichos flujos.
        Ador.MoveNext
        m_sMensajePerAprobador = m_sMensajePerAprobador & Ador(0).Value '10 es un aprobador en el cat�logo FS_EP.
        Ador.MoveNext
        m_sMensajePerAprobador = m_sMensajePerAprobador & Ador(0).Value '11 Seleccione otra persona para sustituirla en dicho catalogo.
        Ador.MoveNext
        lblBusqueda.caption = Ador(0).Value & " :" '12 B�squeda por c�digo :.
        Ador.MoveNext
        optTipoSustEnCurso.caption = Ador(0).Value '13 Sustituir solo en las solicitudes en curso
        Ador.MoveNext
        optTipoSustTodas.caption = Ador(0).Value '14 Sustituir en todas las solicitudes (incluyendo finalizadas)
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
    Me.Width = 6000
    Me.Height = 6800
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    GenerarEstructuraOrg False
        
    If g_sOrigen = "frmESTRORG_UON" Then
        Me.caption = m_sTituloUON
        Me.lblMensaje.caption = g_sDenominacion & " " & m_sMensajeUON
    Else
        Me.caption = m_sTituloPer
        
        If g_sOrigen = "frmESTRORG_PER_CAT" Then
            Me.lblMensaje.caption = m_sMensajePerAprobador
        Else
            Me.lblMensaje.caption = m_sMensajePer
        End If
    End If
End Sub

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados
Dim oDepAsoc As CDepAsociado

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Personas
Dim oPersN0 As CPersonas
Dim oPersN1 As CPersonas
Dim oPersN2 As CPersonas
Dim oPersN3 As CPersonas
Dim oPer As CPersona

' Otras
Dim nodx As node
Dim varCodPersona As Variant

    
    
    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or oUsuarioSummit.Tipo = TIpoDeUsuario.comprador) And _
       (bRUO Or bRDep) Then
        
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False
        
        ' Cargamos las personas  de esos departamentos.
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next
            
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                    
                Next
        End Select
        
        
    Else
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, , True
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True
                oPersN3.CargarTodasLasPersonas , , , , 3, , True, , , False, , True
                
        End Select
        
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    tvwestrorg.Nodes.clear

    ' Unidades organizativas
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) _
        And (bRUO Or bRDep) Then
    
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
        Next
                   
                
        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
            Next
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        Next
    
    Else
    
        'Departamentos
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
        
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwestrorg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
                
        'Personas
        
        For Each oPer In oPersN0
            scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER0" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN1
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER1" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN2
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER2" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN3
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
            scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwestrorg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER3" & CStr(oPer.Cod)
        Next
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
    If node Is Nothing Then Exit Function
    
    If Len(node.Tag) < 4 Then
        DevolverCod = node.Tag
        Exit Function
    End If
    DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
        
End Function


Private Sub Form_Unload(Cancel As Integer)
    g_sCodPersona = ""
    g_sDenominacion = ""
    g_sOrigen = ""
    g_sUON1 = ""
    g_sUON2 = ""
    g_sUON3 = ""
    g_sUON2 = ""
    g_sDEP = ""
End Sub
