VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.UserControl ctlCostesDescuentos 
   ClientHeight    =   5100
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10755
   ScaleHeight     =   5100
   ScaleWidth      =   10755
   Begin VB.PictureBox picDatosGen 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   1305
      Left            =   30
      ScaleHeight     =   1305
      ScaleWidth      =   10695
      TabIndex        =   0
      Top             =   30
      Width           =   10695
      Begin VB.PictureBox PicDatosSinCabecera 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   675
         Left            =   90
         ScaleHeight     =   675
         ScaleWidth      =   10545
         TabIndex        =   19
         Top             =   600
         Width           =   10545
         Begin VB.TextBox txtImpNeto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E1FFFF&
            Height          =   285
            Left            =   7650
            TabIndex        =   23
            Top             =   330
            Width           =   1995
         End
         Begin VB.TextBox txtGrupo 
            BackColor       =   &H00E1FFFF&
            Height          =   285
            Left            =   7650
            TabIndex        =   22
            Top             =   0
            Width           =   2475
         End
         Begin VB.TextBox txtImpBruto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00E1FFFF&
            Height          =   285
            Left            =   1650
            TabIndex        =   21
            Top             =   330
            Width           =   1995
         End
         Begin VB.TextBox txtRefProc 
            BackColor       =   &H00E1FFFF&
            Height          =   285
            Left            =   1650
            TabIndex        =   20
            Top             =   0
            Width           =   4275
         End
         Begin VB.Label lblMon2 
            Caption         =   "Mon"
            Height          =   255
            Left            =   9750
            TabIndex        =   29
            Top             =   360
            Width           =   525
         End
         Begin VB.Label lblImpNeto 
            Caption         =   "DImporte neto:"
            Height          =   255
            Left            =   6150
            TabIndex        =   28
            Top             =   360
            Width           =   1275
         End
         Begin VB.Label lblGrupo 
            Caption         =   "DGrupo:"
            Height          =   255
            Left            =   6150
            TabIndex        =   27
            Top             =   30
            Width           =   855
         End
         Begin VB.Label lblMon 
            Caption         =   "Mon"
            Height          =   255
            Left            =   3720
            TabIndex        =   26
            Top             =   360
            Width           =   525
         End
         Begin VB.Label lblImpBruto 
            Caption         =   "DImporte bruto:"
            Height          =   255
            Left            =   0
            TabIndex        =   25
            Top             =   360
            Width           =   1275
         End
         Begin VB.Label lblRefProc 
            Caption         =   "DReferencia proceso:"
            Height          =   255
            Left            =   0
            TabIndex        =   24
            Top             =   30
            Width           =   1665
         End
      End
      Begin VB.Frame fraDescripcion 
         Height          =   555
         Left            =   90
         TabIndex        =   1
         Top             =   0
         Width           =   10545
         Begin VB.Label lblDescripcion 
            Caption         =   "Descripci�n Descripci�n Descripci�n Descripci�n Descripci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   2
            Top             =   210
            Width           =   10275
         End
      End
   End
   Begin TabDlg.SSTab sstabCD 
      Height          =   3615
      Left            =   60
      TabIndex        =   3
      Top             =   1380
      Width           =   10665
      _ExtentX        =   18812
      _ExtentY        =   6376
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DCostes"
      TabPicture(0)   =   "ctlCostesDescuentos.ctx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "picCostes"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "sdbddValor"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "DDescuentos"
      TabPicture(1)   =   "ctlCostesDescuentos.ctx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picDescuentos"
      Tab(1).Control(1)=   "sdbddDescuentos"
      Tab(1).ControlCount=   2
      Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
         Height          =   915
         Left            =   7050
         TabIndex        =   4
         Top             =   1740
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "ctlCostesDescuentos.ctx":0038
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DESC"
         Columns(1).Name =   "DESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BackColor       =   8421376
      End
      Begin VB.PictureBox picDescuentos 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   2775
         Left            =   -74910
         ScaleHeight     =   2775
         ScaleWidth      =   9525
         TabIndex        =   13
         Top             =   510
         Width           =   9525
         Begin VB.CommandButton cmdDeshacerD 
            Caption         =   "DDeshacer"
            Enabled         =   0   'False
            Height          =   345
            Left            =   2820
            TabIndex        =   17
            Top             =   2160
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminarD 
            Caption         =   "DEliminar"
            Height          =   345
            Left            =   1500
            TabIndex        =   16
            Top             =   2130
            Width           =   1005
         End
         Begin VB.CommandButton cmdA�adirD 
            Caption         =   "DA�adir"
            Height          =   345
            Left            =   210
            TabIndex        =   15
            Top             =   2160
            Width           =   1005
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOperDesc 
            Height          =   915
            Left            =   6450
            TabIndex        =   14
            Top             =   1200
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "ctlCostesDescuentos.ctx":0054
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BackColor       =   8421376
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgDescuentos 
            Height          =   1935
            Left            =   210
            TabIndex        =   18
            Top             =   90
            Width           =   9150
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   13
            stylesets.count =   8
            stylesets(0).Name=   "gris"
            stylesets(0).ForeColor=   12632256
            stylesets(0).BackColor=   12632256
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "ctlCostesDescuentos.ctx":0070
            stylesets(1).Name=   "FondoGris"
            stylesets(1).BackColor=   12632256
            stylesets(1).Picture=   "ctlCostesDescuentos.ctx":008C
            stylesets(2).Name=   "Tahoma"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "ctlCostesDescuentos.ctx":00A8
            stylesets(3).Name=   "NormalInterno"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "ctlCostesDescuentos.ctx":00C4
            stylesets(3).AlignmentPicture=   3
            stylesets(4).Name=   "MultiplePres"
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "ctlCostesDescuentos.ctx":0311
            stylesets(5).Name=   "red"
            stylesets(5).BackColor=   8421631
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "ctlCostesDescuentos.ctx":054F
            stylesets(6).Name=   "StringTachado"
            stylesets(6).HasFont=   -1  'True
            BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   -1  'True
            EndProperty
            stylesets(6).Picture=   "ctlCostesDescuentos.ctx":056B
            stylesets(7).Name=   "green"
            stylesets(7).BackColor=   8454016
            stylesets(7).HasFont=   -1  'True
            BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(7).Picture=   "ctlCostesDescuentos.ctx":0587
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            HeadStyleSet    =   "Tahoma"
            StyleSet        =   "Tahoma"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   13
            Columns(0).Width=   2408
            Columns(0).Caption=   "DCodigo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).ButtonsAlways=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777184
            Columns(1).Width=   4551
            Columns(1).Caption=   "DNombre"
            Columns(1).Name =   "NOM"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777184
            Columns(2).Width=   1217
            Columns(2).Caption=   "DDesc"
            Columns(2).Name =   "DESC"
            Columns(2).CaptionAlignment=   0
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   4
            Columns(2).ButtonsAlways=   -1  'True
            Columns(3).Width=   3200
            Columns(3).Caption=   "DAplicar en cada factura"
            Columns(3).Name =   "APLICFAC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Style=   2
            Columns(4).Width=   1693
            Columns(4).Caption=   "DOperacion"
            Columns(4).Name =   "OP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   1931
            Columns(5).Caption=   "DVal"
            Columns(5).Name =   "VAL"
            Columns(5).Alignment=   1
            Columns(5).CaptionAlignment=   0
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).NumberFormat=   "#,##0.00"
            Columns(5).FieldLen=   256
            Columns(6).Width=   2831
            Columns(6).Caption=   "DImporte a sumar"
            Columns(6).Name =   "IMP"
            Columns(6).Alignment=   1
            Columns(6).CaptionAlignment=   0
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).NumberFormat=   "#,##0.00"
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(6).HasBackColor=   -1  'True
            Columns(6).BackColor=   14811135
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ID_ATRIB"
            Columns(7).Name =   "ID_ATRIB"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "DESCINT"
            Columns(8).Name =   "DESCINT"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "GENERICO"
            Columns(9).Name =   "GENERICO"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   11
            Columns(9).FieldLen=   256
            Columns(9).Style=   2
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "INTRO"
            Columns(10).Name=   "INTRO"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ID_CD"
            Columns(11).Name=   "ID_CD"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "MODIF"
            Columns(12).Name=   "MODIF"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            _ExtentX        =   16140
            _ExtentY        =   3413
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.PictureBox picCostes 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   2895
         Left            =   90
         ScaleHeight     =   2895
         ScaleWidth      =   10515
         TabIndex        =   6
         Top             =   450
         Width           =   10515
         Begin VB.CommandButton cmdDeshacerC 
            Caption         =   "DDeshacer"
            Enabled         =   0   'False
            Height          =   345
            Left            =   2670
            TabIndex        =   11
            Top             =   2370
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminarC 
            Caption         =   "DEliminar"
            Height          =   345
            Left            =   1410
            TabIndex        =   10
            Top             =   2370
            Width           =   1005
         End
         Begin VB.CommandButton cmdA�adirC 
            Caption         =   "DA�adir"
            Height          =   345
            Left            =   120
            TabIndex        =   9
            Top             =   2370
            Width           =   1005
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOperCostes 
            Height          =   915
            Left            =   4830
            TabIndex        =   7
            Top             =   1290
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "ctlCostesDescuentos.ctx":05A3
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
            BackColor       =   8421376
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddCostes 
            Height          =   1065
            Left            =   750
            TabIndex        =   8
            Top             =   1110
            Width           =   3735
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            DividerStyle    =   3
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   185
            Columns.Count   =   7
            Columns(0).Width=   2381
            Columns(0).Caption=   "DC�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4
            Columns(1).Width=   5503
            Columns(1).Caption=   "DDenominaci�n"
            Columns(1).Name =   "NOM"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   2540
            Columns(2).Caption=   "DValor"
            Columns(2).Name =   "VAL"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   2540
            Columns(3).Caption=   "DOperaci�n"
            Columns(3).Name =   "OP"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "ID_ATRIB"
            Columns(4).Name =   "ID_ATRIB"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "DESCINT"
            Columns(5).Name =   "DESCINT"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "INTRO"
            Columns(6).Name =   "INTRO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   6588
            _ExtentY        =   1879
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgCostes 
            Height          =   2025
            Left            =   90
            TabIndex        =   12
            Top             =   90
            Width           =   10320
            _Version        =   196617
            DataMode        =   2
            Col.Count       =   14
            stylesets.count =   9
            stylesets(0).Name=   "Eliminado"
            stylesets(0).BackColor=   12632256
            stylesets(0).Picture=   "ctlCostesDescuentos.ctx":05BF
            stylesets(1).Name=   "gris"
            stylesets(1).ForeColor=   12632256
            stylesets(1).BackColor=   12632256
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "ctlCostesDescuentos.ctx":05DB
            stylesets(2).Name=   "FondoGris"
            stylesets(2).BackColor=   12632256
            stylesets(2).Picture=   "ctlCostesDescuentos.ctx":05F7
            stylesets(3).Name=   "NormalInterno"
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "ctlCostesDescuentos.ctx":0613
            stylesets(3).AlignmentPicture=   3
            stylesets(4).Name=   "Tahoma"
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "ctlCostesDescuentos.ctx":0860
            stylesets(5).Name=   "MultiplePres"
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "ctlCostesDescuentos.ctx":087C
            stylesets(6).Name=   "StringTachado"
            stylesets(6).HasFont=   -1  'True
            BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   -1  'True
            EndProperty
            stylesets(6).Picture=   "ctlCostesDescuentos.ctx":0ABA
            stylesets(7).Name=   "red"
            stylesets(7).BackColor=   8421631
            stylesets(7).HasFont=   -1  'True
            BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(7).Picture=   "ctlCostesDescuentos.ctx":0AD6
            stylesets(8).Name=   "green"
            stylesets(8).BackColor=   8454016
            stylesets(8).HasFont=   -1  'True
            BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(8).Picture=   "ctlCostesDescuentos.ctx":0AF2
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            HeadStyleSet    =   "Tahoma"
            StyleSet        =   "Tahoma"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   14
            Columns(0).Width=   2408
            Columns(0).Caption=   "DCodigo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).ButtonsAlways=   -1  'True
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777184
            Columns(1).Width=   4339
            Columns(1).Caption=   "DNombre"
            Columns(1).Name =   "NOM"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777184
            Columns(2).Width=   1217
            Columns(2).Caption=   "DDesc"
            Columns(2).Name =   "DESC"
            Columns(2).Alignment=   2
            Columns(2).CaptionAlignment=   0
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   4
            Columns(2).ButtonsAlways=   -1  'True
            Columns(3).Width=   3200
            Columns(3).Caption=   "DAplicar en cada factura"
            Columns(3).Name =   "APLICFAC"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).Style=   2
            Columns(4).Width=   1693
            Columns(4).Caption=   "DOperacion"
            Columns(4).Name =   "OP"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   1931
            Columns(5).Caption=   "DVal"
            Columns(5).Name =   "VAL"
            Columns(5).Alignment=   1
            Columns(5).CaptionAlignment=   0
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).NumberFormat=   "#,##0.00"
            Columns(5).FieldLen=   256
            Columns(6).Width=   2831
            Columns(6).Caption=   "DImporte a sumar"
            Columns(6).Name =   "IMP"
            Columns(6).Alignment=   1
            Columns(6).CaptionAlignment=   0
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).NumberFormat=   "#,##0.00"
            Columns(6).FieldLen=   256
            Columns(6).Locked=   -1  'True
            Columns(6).HasBackColor=   -1  'True
            Columns(6).BackColor=   14811135
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "ID_ATRIB"
            Columns(7).Name =   "ID_ATRIB"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "DESCINT"
            Columns(8).Name =   "DESCINT"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "GENERICO"
            Columns(9).Name =   "GENERICO"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   11
            Columns(9).FieldLen=   256
            Columns(9).Style=   3
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "INTRO"
            Columns(10).Name=   "INTRO"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "ID_CD"
            Columns(11).Name=   "ID_CD"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Caption=   "Impuestos"
            Columns(12).Name=   "IMPUESTOS"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Style=   4
            Columns(12).ButtonsAlways=   -1  'True
            Columns(13).Width=   3200
            Columns(13).Visible=   0   'False
            Columns(13).Caption=   "MODIF"
            Columns(13).Name=   "MODIF"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   11
            Columns(13).FieldLen=   256
            _ExtentX        =   18203
            _ExtentY        =   3572
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddDescuentos 
         Height          =   1065
         Left            =   -72450
         TabIndex        =   5
         Top             =   1620
         Width           =   3735
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   185
         Columns.Count   =   7
         Columns(0).Width=   2381
         Columns(0).Caption=   "DC�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4
         Columns(1).Width=   5503
         Columns(1).Caption=   "DDenominaci�n"
         Columns(1).Name =   "NOM"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2540
         Columns(2).Caption=   "DValor"
         Columns(2).Name =   "VAL"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   2540
         Columns(3).Caption=   "DOperaci�n"
         Columns(3).Name =   "OP"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "ID_ATRIB"
         Columns(4).Name =   "ID_ATRIB"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "DESCINT"
         Columns(5).Name =   "DESCINT"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "INTRO"
         Columns(6).Name =   "INTRO"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   1879
         _StockProps     =   77
      End
   End
End
Attribute VB_Name = "ctlCostesDescuentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const cnSep As Integer = 60

Public Event OnModifCostesDescuentosLinea()

Public Enum enumTabInicial
    Costes = 0
    Descuentos = 1
End Enum

Public Enum enumAmbitoDatos
    cabecera = 0
    Linea = 1
End Enum

Public g_oOrdenEntrega As COrdenEntrega
Public TabInicial As enumTabInicial
Public AmbitoDatos As enumAmbitoDatos
Public g_vAnyo As Variant
Public g_vGMN1 As Variant
Public g_vProce As Variant
Public g_vItem As Variant
Public g_vArt As Variant
Public g_vIdLP As Variant
'SOLO SE LE DAR� VALOR DESDE LA EMISI�N DE PEDIDOS*****************************
'Es la variable que nos meter� en la colecci�n a la hora de grabar el coste/descuento el importe en la moneda de la oferta
Public g_dblCambioMonedaOferta As Double
'******************************************************************************
Public g_dblCambioMoneda As Double
'Para cuando se cambia la moneda desde la emisi�n de pedido poder mostrar los valores en la moneda seleccionada en el pedido
Public g_dcProcesosOrden As Dictionary
Public bCDLineaImpuestos As Boolean
Public g_sOrigen As String
Private VentanaCostes As Boolean

Public Impuesto_Cambio As Double
Public Impuesto_VentanaSeguimiento As Boolean
Public Impuesto_PermisoAunSinRecepc As Boolean
Public Impuesto_Orden As COrdenEntrega
Public Impuesto_LineaId  As Long

Private m_oAtributos As CAtributos
Private m_oCostesDescuentos As CAtributos
Private m_arOpCostes As Variant
Private m_arOpDescuentos As Variant
Private m_bCargaDatos As Boolean
Private m_iNumGenerico As Integer
Private m_bSoloLectura As Boolean
Private m_bActualizarBD As Boolean
Private m_bModoEdicion As Boolean
Private m_sCostes As String
Private m_sDescuentos As String
Private m_bFormAtribAbierta As Boolean
Private m_bLineaBajaLOG As Boolean
Private m_bUnload As Boolean
Private m_bCDsLineaModif As Boolean

Public g_bMostrandoLOG As Boolean
Public g_bDesdeEmision As Boolean

Public Sub ActulizarDatosGrids()
If sdbgCostes.DataChanged Then
    sdbgCostes.Update
End If

If sdbgDescuentos.DataChanged Then
    sdbgDescuentos.Update
End If
End Sub

Public Sub GuardarCambios()

If sdbgCostes.DataChanged Then sdbgCostes.Update
If sdbgDescuentos.DataChanged Then sdbgDescuentos.Update

End Sub

Public Property Get NumGenerico() As Integer
    NumGenerico = m_iNumGenerico
End Property

Public Property Let NumGenerico(ByVal vNewValue As Integer)
    m_iNumGenerico = vNewValue
End Property

Public Property Get ActualizarBD() As Boolean
    ActualizarBD = m_bActualizarBD
End Property

Public Property Let ActualizarBD(ByVal vNewValue As Boolean)
    m_bActualizarBD = vNewValue
End Property

Public Property Get oSdbgCostes() As Object
    Set oSdbgCostes = sdbgCostes
End Property

Public Property Let oSdbgCostes(ByRef objeto As Object)
    Set sdbgCostes = objeto
End Property

Public Property Get oSdbgDescuentos() As Object
    Set oSdbgDescuentos = sdbgDescuentos
End Property

Public Property Let oSdbgDescuentos(ByRef objeto As Object)
    Set sdbgDescuentos = objeto
End Property

Public Property Get oSstabCD() As Object
    Set oSstabCD = sstabCD
End Property

Private Sub pmInitCombos()

sdbddCostes.DataFieldList = "Column 0"
sdbddCostes.DataFieldToDisplay = "Column 0"

sdbddDescuentos.DataFieldList = "Column 0"
sdbddDescuentos.DataFieldToDisplay = "Column 0"

sdbddOperCostes.DataFieldList = "Column 0"
sdbddOperCostes.DataFieldToDisplay = "Column 1"

sdbddOperDesc.DataFieldList = "Column 0"
sdbddOperDesc.DataFieldToDisplay = "Column 1"

sdbddValor.DataFieldList = "Column 0"
sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Public Property Get SoloLectura() As Boolean
    SoloLectura = m_bSoloLectura
End Property

Public Property Let SoloLectura(ByVal vNewValue As Boolean)
    m_bSoloLectura = vNewValue
End Property

Public Property Get CostesDescuentos() As CAtributos
    Set CostesDescuentos = m_oCostesDescuentos
End Property

Private Sub cmdA�adirC_Click()

    AbrirFormAtrib enumTabInicial.Costes
End Sub

Private Sub cmdA�adirD_Click()
    AbrirFormAtrib enumTabInicial.Descuentos
End Sub
Public Sub PonerFieldSeparator()
    Dim sdg As Control
    Dim Name4 As String
    Dim Name5 As String
        
    For Each sdg In UserControl.Controls
        
        Name4 = LCase(Left(sdg.Name, 4))
        Name5 = LCase(Left(sdg.Name, 5))
        
        If Name4 = "sdbg" Or Name4 = "sdbc" Or Name5 = "sdbdd" Or Name4 = "ssdb" Then
            If sdg.DataMode = ssDataModeAddItem Then
                sdg.FieldSeparator = Chr(m_lSeparador)
            End If
        End If
    Next
End Sub


''' <summary>Abre el formulario de atributos</summary>
''' <param name="iCD">Indica si se trata de un coste o descuento</param>
''' <remarks>Llamada desde: cmdA�adirC_Click y cmdA�adirD_Click</remarks>
''' <revision>LTG 12/04/2012</revision>

Private Sub AbrirFormAtrib(ByVal iCD As enumTabInicial)
    Dim ofrmATRIB As frmAtribMod
    Dim bCancelUpdate As Integer
    If iCD = Costes Then
        If sdbgCostes.DataChanged Then Grid_BeforeUpdate sdbgCostes, bCancelUpdate
    Else
        If sdbgDescuentos.DataChanged Then Grid_BeforeUpdate sdbgDescuentos, bCancelUpdate
    End If
    
    If bCancelUpdate <> -1 Then
        m_bFormAtribAbierta = True
        
        Set ofrmATRIB = New frmAtribMod
        ofrmATRIB.g_sOrigen = g_sOrigen
        ofrmATRIB.optOption2.Value = True
        If iCD = Costes Then
            ofrmATRIB.chkCostes.Value = vbChecked
        Else
            ofrmATRIB.chkDtos.Value = vbChecked
        End If
        ofrmATRIB.fraTipo.Enabled = False
        ofrmATRIB.sstabGeneral.Tab = 1
        ofrmATRIB.cmdSeleccionar.Visible = True
        ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
        ofrmATRIB.g_bSoloSeleccion = True
        
        ofrmATRIB.Show vbModal
        
        m_bFormAtribAbierta = False
    End If
End Sub

Private Sub cmdDeshacerC_Click()

    With sdbgCostes
        If .DataChanged Then
            .DataChanged = False
            .CancelUpdate
            DoEvents
        Else
            .Bookmark = .RowBookmark(.Row - 1)
        End If
    End With
    
    cmdDeshacerC.Enabled = False
End Sub

Private Sub cmdDeshacerD_Click()

    With sdbgDescuentos
        If .DataChanged Then
            .DataChanged = False
            .CancelUpdate
            DoEvents
        Else
            .Bookmark = .RowBookmark(.Row - 1)
        End If
    End With
    
    cmdDeshacerD.Enabled = False

End Sub

Private Sub cmdEliminarC_Click()
    EliminarCD sdbgCostes, True
End Sub

Private Sub cmdEliminarD_Click()
    EliminarCD sdbgDescuentos, True
End Sub

''' <summary>Elimina los costes descuentos seleccionados</summary>
''' <param name="oGrid">Grid del que hay que eliminar</param>
''' <param name="bEliminarDelGrid">Indica si hay que eliminar las filas del grid</param>
''' <returns>Booleano indicando si se ha producido la eliminaci�n</returns>
''' <remarks>Llamada desde: cmdEliminarC_Click y cmdEliminarD_Click</remarks>
''' <revision>LTG 10/05/2012</revision>

Private Function EliminarCD(ByRef oGrid As SSDBGrid, ByVal bEliminarDelGrid As Boolean) As TipoErrorSummit
    Dim arIDAtrb() As String
    Dim i As Integer
    Dim dblImpCD As Double
    Dim oCostesDesc As CAtributos
    Dim oLineaPedido As CLineaPedido
    Dim teserror As TipoErrorSummit
    Dim sCod As String
    teserror.NumError = TESnoerror
    
    With oGrid
        If .Rows = 0 Then Exit Function
        If .SelBookmarks.Count = 0 Then Exit Function
        If .IsAddRow Then Exit Function
        
        ReDim arIDAtrb(.SelBookmarks.Count - 1)
        For i = 0 To .SelBookmarks.Count - 1
            'Actualizar el importe del atributo
            arIDAtrb(i) = .Columns("ID_ATRIB").CellValue(.SelBookmarks.Item(i))
        Next
                    
        If Not m_bActualizarBD Then
            For i = 0 To UBound(arIDAtrb)
                ActualizarImportes (.Name = "sdbgCostes"), arIDAtrb(i), 0
                
                'Eliminar de las colecciones
                m_oCostesDescuentos.Remove arIDAtrb(i)
            Next
        Else
            'Eliminaci�n de la BD
            If AmbitoDatos = Linea Then
                sCod = KeyLineaPedido
                            
                If sCod <> "" Then
                    Set oLineaPedido = g_oOrdenEntrega.LineasPedido.Item(sCod)
                    teserror = oLineaPedido.EliminarCostesDescuentosLineaPedido(g_oOrdenEntrega, arIDAtrb, False)
                    Set oLineaPedido = Nothing
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                    Else
                        m_bCDsLineaModif = True
                    End If
                End If
            Else
                teserror = g_oOrdenEntrega.EliminarCostesDescuentosPedido(arIDAtrb, False)
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                End If
            End If
        End If
        MostrarImportes
        
        If teserror.NumError = TESnoerror Then
            'Eliminar del grid
            If bEliminarDelGrid Then
                For i = .SelBookmarks.Count - 1 To 0 Step -1
                    dblImpCD = dblImpCD + StrToDbl0(.Columns("IMP").Value)
                    .RemoveItem .AddItemRowIndex(.SelBookmarks.Item(i))
                Next
            End If
        End If
    End With
    
    EliminarCD = teserror
    
    Set oCostesDesc = Nothing
End Function

''' <summary>Devuelve la Key de la l�nea de pedido en la colecci�n de l�neas de pedido</summary>
''' <remarks>Llamada desde: EliminarCd</remarks>
''' <revision>LTG 12/06/2012</revision>

Private Function KeyLineaPedido() As String
    Dim sCod As String
    If NullToStr(g_vIdLP) = "" Then
        If NullToStr(g_vAnyo) <> "" And NullToStr(g_vGMN1) <> "" And NullToStr(g_vProce) <> "" And NullToStr(g_vItem) <> "" And NullToStr(g_vArt) <> "" Then
            sCod = g_vGMN1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(g_vGMN1))
            sCod = g_vAnyo & sCod & g_vProce & g_vItem & g_vArt
        End If
    Else
        sCod = CStr(g_vIdLP)
    End If
    
    KeyLineaPedido = sCod
End Function


Public Sub Initialize()
    
    m_bUnload = False
    m_bLineaBajaLOG = False
    sdbgCostes.RemoveAll
    sdbgDescuentos.RemoveAll
    bCDLineaImpuestos = False
    m_iNumGenerico = 0
    If g_dblCambioMoneda = 0 Then g_dblCambioMoneda = 1
    PonerFieldSeparator
    
    pmInitCombos
    
    VisibilidadControles
    CargarRecursos
    CargarCombosOperaciones
    ObtenerPosiblesCostesDescuentos
    MostrarDatos
    
    Select Case UCase(g_sOrigen)
        Case "FRMPEDIDOS", "FRMSEGUIMIENTO", "FRMSEGUIMIENTOLINEA"
            fraDescripcion.Visible = False
            PicDatosSinCabecera.Top = 0
            picDatosGen.Height = PicDatosSinCabecera.Height + 20
    End Select
    
    ConfigurarSeguridad
    
    Select Case TabInicial
        Case enumTabInicial.Costes
            sstabCD.Tab = 0
        Case enumTabInicial.Descuentos
            sstabCD.Tab = 1
    End Select
    
    'Inicializar dropdowns costes/descuentos
    sdbddCostes.AddItem ""
    sdbddDescuentos.AddItem ""
End Sub

''' <summary>Configura la seguridad de los controles del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 08/06/2012</revision>

Public Sub ConfigurarSeguridad()
    Dim vlb As Boolean
    vlb = True
    
    'Si el pedido tiene recepciones y/o facturas no se podr� a�adir eliminar o modificar los costes y descuentos
    If Not g_bDesdeEmision Then
        If m_bSoloLectura Or g_oOrdenEntrega.TieneFacturas Or _
            (g_oOrdenEntrega.Tipo <> PedidoAbierto And g_oOrdenEntrega.Estado >= TipoEstadoOrdenEntrega.EnRecepcion) Or _
            (g_oOrdenEntrega.Tipo = PedidoAbierto And g_oOrdenEntrega.Estado >= TipoEstadoPedidoAbierto.PACerrado) Then
                vlb = False
        End If
    End If
    m_bModoEdicion = vlb
    
    cmdA�adirC.Visible = vlb
    cmdEliminarC.Visible = vlb
    cmdDeshacerC.Visible = vlb
    
    cmdA�adirD.Visible = vlb
    cmdEliminarD.Visible = vlb
    cmdDeshacerD.Visible = vlb
    
    With sdbgCostes
        .AllowAddNew = vlb
        .AllowDelete = vlb
        
        .Columns("COD").Locked = Not vlb
        .Columns("APLICFAC").Locked = Not vlb
        .Columns("OP").Locked = Not vlb
        If vlb Then
            .Columns("OP").DropDownHwnd = sdbddOperCostes.hWnd
        Else
            .Columns("OP").DropDownHwnd = 0
        End If
        .Columns("VAL").Locked = Not vlb
    End With
    
    With sdbgDescuentos
        .AllowAddNew = vlb
        .AllowDelete = vlb
        
        .Columns("COD").Locked = Not vlb
        .Columns("APLICFAC").Locked = Not vlb
        .Columns("OP").Locked = Not vlb
        If vlb Then
            .Columns("OP").DropDownHwnd = sdbddOperDesc.hWnd
        Else
            .Columns("OP").DropDownHwnd = 0
        End If
        .Columns("VAL").Locked = Not vlb
    End With

    Arrange
End Sub

''' <summary>establece la visibilidad de los controles en funci�n del �mbito de los costes/descuentos</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 19/04/2012</revision>

Private Sub VisibilidadControles()
    Dim bVis As Boolean
    bVis = (AmbitoDatos = Linea)
    fraDescripcion.Visible = bVis
    lblRefProc.Visible = bVis
    txtRefProc.Visible = bVis
    lblGrupo.Visible = bVis
    txtGrupo.Visible = bVis
    
    If Not bVis Then
        lblImpNeto.Top = lblGrupo.Top
        txtImpNeto.Top = txtGrupo.Top
        lblMon2.Top = lblImpNeto.Top
        lblImpBruto.Top = lblImpNeto.Top
        txtImpBruto.Top = txtImpNeto.Top
        lblMon.Top = lblMon2.Top
        PicDatosSinCabecera.Height = txtImpBruto.Top + txtImpBruto.Height + 20
    End If
    sdbgCostes.Columns("APLICFAC").Visible = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM)
    sdbgDescuentos.Columns("APLICFAC").Visible = (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.AccesoFSIM)
End Sub

''' <summary>Carga las cadenas de idiomas</summary>
''' <remarks>Llamada desde: Form_Load; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 23/03/2012</revision>

Private Sub CargarRecursos()
    Dim rsRec As Ador.Recordset
    On Error Resume Next
    
    Set rsRec = oGestorIdiomas.DevolverTextosDelModulo(FRM_DESCUENTOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not rsRec Is Nothing Then
        lblRefProc.caption = rsRec(0).Value & ":"
        rsRec.MoveNext
        lblGrupo.caption = rsRec(0).Value & ":"
        rsRec.MoveNext
        lblImpNeto.caption = rsRec(0).Value & ":"
        rsRec.MoveNext
        lblImpBruto.caption = rsRec(0).Value & ":"
        rsRec.MoveNext
        m_sCostes = rsRec(0).Value
        rsRec.MoveNext
        m_sDescuentos = rsRec(0).Value
        rsRec.MoveNext
        cmdA�adirC.caption = rsRec(0).Value
        cmdA�adirD.caption = rsRec(0).Value
        rsRec.MoveNext
        cmdEliminarC.caption = rsRec(0).Value
        cmdEliminarD.caption = rsRec(0).Value
        rsRec.MoveNext
        cmdDeshacerC.caption = rsRec(0).Value
        cmdDeshacerD.caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgCostes.Columns("COD").caption = rsRec(0).Value
        sdbgDescuentos.Columns("COD").caption = rsRec(0).Value
        sdbddCostes.Columns("COD").caption = rsRec(0).Value
        sdbddDescuentos.Columns("COD").caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgCostes.Columns("NOM").caption = rsRec(0).Value
        sdbgDescuentos.Columns("NOM").caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgCostes.Columns("DESC").caption = rsRec(0).Value
        sdbgDescuentos.Columns("DESC").caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgCostes.Columns("OP").caption = rsRec(0).Value
        sdbgDescuentos.Columns("OP").caption = rsRec(0).Value
        sdbddCostes.Columns("OP").caption = rsRec(0).Value
        sdbddDescuentos.Columns("OP").caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgCostes.Columns("VAL").caption = rsRec(0).Value
        sdbgDescuentos.Columns("VAL").caption = rsRec(0).Value
        sdbddCostes.Columns("VAL").caption = rsRec(0).Value
        sdbddDescuentos.Columns("VAL").caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgCostes.Columns("IMP").caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgDescuentos.Columns("IMP").caption = rsRec(0).Value
        rsRec.MoveNext
        'If AmbitoDatos = Linea Then Me.caption = rsRec(0).Value
        rsRec.MoveNext
        sdbgCostes.Columns("APLICFAC").caption = rsRec(0).Value
        sdbgDescuentos.Columns("APLICFAC").caption = rsRec(0).Value
        rsRec.MoveNext
        sdbddCostes.Columns("NOM").caption = rsRec(0).Value
        sdbddDescuentos.Columns("NOM").caption = rsRec(0).Value
        rsRec.MoveNext
        'If AmbitoDatos = cabecera Then Me.caption = rsRec(0).Value
    End If
End Sub

''' <summary>Carga los combos de operaciones</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 08/05/2012</revision>

Private Sub CargarCombosOperaciones()
    Dim i As Integer
    m_arOpCostes = Array("+", "+%")
    m_arOpDescuentos = Array("-", "-%")
    
    sdbddOperCostes.RemoveAll
    For i = 0 To UBound(m_arOpCostes)
        sdbddOperCostes.AddItem m_arOpCostes(i) & Chr(m_lSeparador) & m_arOpCostes(i)
    Next
    sdbgCostes.Columns("OP").DropDownHwnd = sdbddOperCostes.hWnd
    
    sdbddOperDesc.RemoveAll
    For i = 0 To UBound(m_arOpDescuentos)
        sdbddOperDesc.AddItem m_arOpDescuentos(i) & Chr(m_lSeparador) & m_arOpDescuentos(i)
    Next
    sdbgDescuentos.Columns("OP").DropDownHwnd = sdbddOperDesc.hWnd
End Sub

''' <summary>Muestra los datos en pantalla</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 23/03/2012</revision>

Private Sub MostrarDatos()
    Dim oLineaPedido As CLineaPedido
    Dim oProce As cProceso
    Dim oGrupo As CGrupo
    Dim l As Long
     
    If AmbitoDatos = Linea Then
        Set oLineaPedido = g_oOrdenEntrega.LineasPedido.Item(KeyLineaPedido)
    
        lblDescripcion.caption = Format(oLineaPedido.Num, "000") & " - " & oLineaPedido.ArtCod_Interno & " - " & oLineaPedido.ArtDen
        
         If NullToStr(g_vAnyo) <> "" And NullToStr(g_vGMN1) <> "" And NullToStr(g_vProce) <> "" Then
            Set oProce = oFSGSRaiz.Generar_CProceso
            oProce.Anyo = g_vAnyo
            oProce.GMN1Cod = g_vGMN1
            oProce.Cod = g_vProce
            oProce.CargarDatosGeneralesProceso bSinpres:=True, bSinGRupos:=True
            
            txtRefProc.Text = g_vAnyo & "/" & g_vGMN1 & "/" & g_vProce & " " & oProce.Den
            
            oProce.CargarTodosLosItems OrdItemPorOrden, oLineaPedido.ArtCod_Interno, , , True
            Set oGrupo = oFSGSRaiz.generar_cgrupo
            Set oGrupo.proceso = oProce
            'Si se selecciona solo un item, en vez de un grupo de items
            If oProce.Items.Count <> 0 Then
                oGrupo.Id = oProce.Items.Item(CStr(oLineaPedido.Item)).GrupoID
            End If
            oGrupo.CargarTodosLosDatos
            
            txtGrupo.Text = oGrupo.Codigo & " - " & oGrupo.Den
        End If
        If oLineaPedido.CantidadPedida = 0 Then
            txtImpBruto.Text = ""
            txtImpNeto.Text = ""
        Else
            txtImpBruto.Text = Format(oLineaPedido.ImporteBruto * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard")
            txtImpNeto.Text = Format(oLineaPedido.ImporteNeto * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard")
        End If
        
        sstabCD.TabCaption(0) = m_sCostes & " = " & Format(oLineaPedido.ImporteCostes * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
        sstabCD.TabCaption(1) = m_sDescuentos & " = " & Format(oLineaPedido.ImporteDescuentos * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
        
        Set m_oCostesDescuentos = oLineaPedido.CostesDescuentos
        
        If oLineaPedido.Baja_LOG And Not g_bMostrandoLOG Then
            l = &H8000000F
            m_bLineaBajaLOG = True
        Else
            l = &HE1FFFF
            m_bLineaBajaLOG = False
        End If
        'Ponemos todo en gris
        txtRefProc.Backcolor = l
        txtGrupo.Backcolor = l
        txtImpBruto.Backcolor = l
        txtImpNeto.Backcolor = l
        
        
        CargarGrids m_oCostesDescuentos
        
        Set oLineaPedido = Nothing
        Set oProce = Nothing
        Set oGrupo = Nothing
    Else
        If g_oOrdenEntrega.importe = 0 Then
            txtImpBruto.Text = ""
        Else
            If Impuesto_VentanaSeguimiento Then
                txtImpBruto.Text = Format(g_oOrdenEntrega.importe * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard")
            Else
                'Si el cambio es diferente de 1 quiere decir que la moneda es la de la oferta,
                'antes se podia aplicar un cambio pero ahora puede tener cada item un cambio diferente
                'con lo cual no se puede aplicar al importe bruto un cambio concreto, como est� guardado en la colecci�n lo recojemos de ah�
                If g_dblCambioMoneda <> 1 Then
                    txtImpBruto.Text = Format(g_oOrdenEntrega.ImporteMonProce, "standard")
                Else
                    txtImpBruto.Text = Format(g_oOrdenEntrega.importe, "standard")
                End If
            End If
        End If
        If g_oOrdenEntrega.ImporteNeto = 0 Then
            txtImpNeto.Text = ""
        Else
            If Impuesto_VentanaSeguimiento Then
                txtImpNeto.Text = Format(g_oOrdenEntrega.ImporteNeto * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard")
            Else
                If g_dblCambioMoneda <> 1 Then
                    txtImpNeto.Text = Format(g_oOrdenEntrega.ImporteNetoMonProce, "standard")
                Else
                    txtImpNeto.Text = Format(g_oOrdenEntrega.ImporteNeto, "standard")
                End If
            End If
        End If
        
        sstabCD.TabCaption(0) = m_sCostes & " = " & Format(g_oOrdenEntrega.ImporteCostes * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
        sstabCD.TabCaption(1) = m_sDescuentos & " = " & Format(g_oOrdenEntrega.ImporteDescuentos * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
        Set m_oCostesDescuentos = g_oOrdenEntrega.CostesDescuentos
        CargarGrids m_oCostesDescuentos
    End If
    
    sdbgCostes.Columns("IMP").caption = sdbgCostes.Columns("IMP").caption & " " & g_oOrdenEntrega.Moneda
    sdbgDescuentos.Columns("IMP").caption = sdbgDescuentos.Columns("IMP").caption & " " & g_oOrdenEntrega.Moneda
    If g_bMostrandoLOG Then
        If sdbgCostes.Rows > 0 Then
            sstabCD.TabCaption(0) = m_sCostes & " = " & Format(sdbgCostes.Columns("IMP").Value / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
        Else
            sstabCD.TabCaption(0) = m_sCostes & " = " & Format(0, "standard") & " " & g_oOrdenEntrega.Moneda
        End If
        If sdbgDescuentos.Rows > 0 Then
            sstabCD.TabCaption(1) = m_sDescuentos & " = " & Format(sdbgDescuentos.Columns("IMP").Value / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
        Else
            sstabCD.TabCaption(1) = m_sDescuentos & " = " & Format(0, "standard") & " " & g_oOrdenEntrega.Moneda
        End If
    End If
    lblMon.caption = g_oOrdenEntrega.Moneda
    lblMon2.caption = g_oOrdenEntrega.Moneda
End Sub

''' <summary>Carga los grids de costes y descuentos</summary>
''' <remarks>Llamada desde: MostrarDatos</remarks>
''' <revision>LTG 23/04/2012</revision>

Private Sub CargarGrids(ByVal oCostesDesc As CAtributos)
    Dim oCosteDesc As CAtributo
    
    m_bCargaDatos = True
    
    m_iNumGenerico = 0
    If Not oCostesDesc Is Nothing Then
        If AmbitoDatos = cabecera Then
            '1� aplicables al total del grupo
            For Each oCosteDesc In oCostesDesc
                CargarLineaGrid oCosteDesc, TipoAplicarAPrecio.TotalGrupo
            Next
            
            '2� aplicables al total de la oferta
            For Each oCosteDesc In oCostesDesc
                CargarLineaGrid oCosteDesc, TipoAplicarAPrecio.TotalOferta
            Next
        Else
            For Each oCosteDesc In oCostesDesc
                CargarLineaGrid oCosteDesc, TipoAplicarAPrecio.TotalItem
            Next
        End If
                
        Set oCostesDesc = Nothing
    End If
    
    m_bCargaDatos = False
End Sub

''' <summary>Carga una l�nea en el grid de costes o descuentos</summary>
''' <param name="oCosteDesc">Coste o descuento</param>
''' <param name="TipoAplica">Precio al que se aplica el coste/descuento</param>
''' <remarks>Llamada desde: CargarGrids</remarks>
''' <revision>LTG 03/05/2012</revision>

Private Sub CargarLineaGrid(ByVal oCosteDesc As CAtributo, ByVal TipoAplica As TipoAplicarAPrecio)
    Dim sIdAtrib As String
    Dim sImp As String
    Dim vValorNum As Variant
    If oCosteDesc.PrecioAplicarA = TipoAplica Then
        sIdAtrib = oCosteDesc.Id
        If oCosteDesc.Generico Then
            m_iNumGenerico = m_iNumGenerico + 1
            sIdAtrib = sIdAtrib & "G" & CStr(m_iNumGenerico)
        End If
        
        If oCosteDesc.importe = 0 Then
            sImp = ""
        Else
            sImp = oCosteDesc.importe * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1)
        End If
        If oCosteDesc.PrecioFormula = "+" Or oCosteDesc.PrecioFormula = "-" Then
            If Not IsNull(oCosteDesc.valorNum) Then vValorNum = oCosteDesc.valorNum * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1)
        Else
            vValorNum = oCosteDesc.valorNum
        End If
        
        If oCosteDesc.EsCoste() Then
            sdbgCostes.AddItem oCosteDesc.Cod & Chr(m_lSeparador) & oCosteDesc.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCosteDesc.Facturar & Chr(m_lSeparador) & oCosteDesc.PrecioFormula & _
                Chr(m_lSeparador) & vValorNum & Chr(m_lSeparador) & sImp & Chr(m_lSeparador) & sIdAtrib & Chr(m_lSeparador) & oCosteDesc.Descripcion & Chr(m_lSeparador) & "0" & _
                Chr(m_lSeparador) & oCosteDesc.TipoIntroduccion & Chr(m_lSeparador) & oCosteDesc.IdCD & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & False
        Else
            sdbgDescuentos.AddItem oCosteDesc.Cod & Chr(m_lSeparador) & oCosteDesc.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oCosteDesc.Facturar & Chr(m_lSeparador) & oCosteDesc.PrecioFormula & _
                Chr(m_lSeparador) & vValorNum & Chr(m_lSeparador) & sImp & Chr(m_lSeparador) & sIdAtrib & Chr(m_lSeparador) & oCosteDesc.Descripcion & Chr(m_lSeparador) & "0" & _
                Chr(m_lSeparador) & oCosteDesc.TipoIntroduccion & Chr(m_lSeparador) & oCosteDesc.IdCD
        End If
    End If
End Sub

''' <summary>Redimensiona los controles dentro del formulario</summary>
''' <remarks>Llamada desde: Form_Resize; Tiempo m�ximo:0</remarks>
''' <revision>LTG 23/03/2012</revision>

Private Sub Arrange()

On Error Resume Next
        'POSICIONES
        
        'Datos generales
        sstabCD.Top = picDatosGen.Height + picDatosGen.Top
        sstabCD.Left = cnSep
        
        'Tab costes
        sdbgCostes.Top = 0
        sdbgCostes.Left = 0
        
        'Tab descuentos
  
        sdbgDescuentos.Top = 0
        sdbgDescuentos.Left = 0
        
        'DIMENSIONES en horizontal
        'Datos generales
        picDatosGen.Width = UserControl.Width - 10
        fraDescripcion.Width = picDatosGen.Width - 120
        lblDescripcion.Width = fraDescripcion.Width - 220
        
        sstabCD.Width = UserControl.Width - 120
        picDescuentos.Top = picCostes.Top
        'Tab costes
        picCostes.Width = sstabCD.Width - 150
        sdbgCostes.Width = picCostes.Width - 50
        RedimensionarColumnasGrid sdbgCostes
        
        'Tab descuentos
        picDescuentos.Width = sstabCD.Width - 150
        sdbgDescuentos.Width = picCostes.Width - 50
        RedimensionarColumnasGrid sdbgDescuentos
        
        'DIMENSIONES en vertical
        If UserControl.Height - picDatosGen.Top - picDatosGen.Height > 0 Then
            sstabCD.Height = UserControl.Height - picDatosGen.Top - picDatosGen.Height - 10
        End If
        'Tab costes
        If (sstabCD.Height - picCostes.Top - 100) > 0 Then
            picCostes.Height = sstabCD.Height - picCostes.Top - 100
        End If
        If m_bModoEdicion Then
            If (picCostes.Height - cmdA�adirC.Height - 150) > 0 Then
                sdbgCostes.Height = picCostes.Height - cmdA�adirC.Height - cnSep
            End If
        Else
            If (picCostes.Height - cnSep) > 0 Then
                sdbgCostes.Height = picCostes.Height - cnSep
            End If
        End If
        
        'Tab descuentos
        If (sstabCD.Height - picDescuentos.Top - 100) > 0 Then
            picDescuentos.Height = sstabCD.Height - picDescuentos.Top - 100
        End If
        If m_bModoEdicion Then
            If (picDescuentos.Height - cmdA�adirD.Height - cnSep) > 0 Then
                sdbgDescuentos.Height = picDescuentos.Height - cmdA�adirD.Height - cnSep
            End If
        Else
            If (picCostes.Height - cnSep) > 0 Then
                sdbgDescuentos.Height = picCostes.Height - cnSep
            End If
        End If
        
        
        cmdA�adirC.Top = picCostes.Height - cmdA�adirC.Height
        cmdA�adirC.Left = 0
        cmdEliminarC.Top = cmdA�adirC.Top
        cmdEliminarC.Left = cmdA�adirC.Left + cmdA�adirC.Width + cnSep
        cmdDeshacerC.Top = cmdA�adirC.Top
        cmdDeshacerC.Left = cmdEliminarC.Left + cmdEliminarC.Width + cnSep
        
        cmdA�adirD.Top = picCostes.Height - cmdA�adirC.Height
        cmdA�adirD.Left = 0
        cmdEliminarD.Top = cmdA�adirD.Top
        cmdEliminarD.Left = cmdA�adirD.Left + cmdA�adirD.Width + cnSep
        cmdDeshacerD.Top = cmdA�adirD.Top
        cmdDeshacerD.Left = cmdEliminarD.Left + cmdEliminarD.Width + cnSep
End Sub

''' <summary>Redimensiona las columnas del grid en funci�n de su ancho</summary>
''' <param name="sdbgGrid">Grid cuyas columas hay que redimensionar</param>
''' <remarks>Llamada desde: Arrange; Tiempo m�ximo:0</remarks>
''' <revision>LTG 23/03/2012</revision>

Private Sub RedimensionarColumnasGrid(ByRef sdbgGrid As SSDBGrid)
    If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Then
        With sdbgGrid
            .Columns("COD").Width = (.Width - (6 * cnSep)) * 0.15
            .Columns("NOM").Width = (.Width - (6 * cnSep)) * 0.22
            .Columns("DESC").Width = (.Width - (6 * cnSep)) * 0.06
            .Columns("OP").Width = (.Width - (6 * cnSep)) * 0.1
            .Columns("VAL").Width = (.Width - (6 * cnSep)) * 0.2
            .Columns("IMP").Width = (.Width - (6 * cnSep)) * 0.2
        End With
    Else
        With sdbgGrid
            .Columns("COD").Width = (.Width - (6 * cnSep)) * 0.12
            .Columns("NOM").Width = (.Width - (6 * cnSep)) * 0.22
            .Columns("DESC").Width = (.Width - (6 * cnSep)) * 0.06
            .Columns("APLICFAC").Width = (.Width - (6 * cnSep)) * 0.19
            .Columns("OP").Width = (.Width - (6 * cnSep)) * 0.1
            .Columns("VAL").Width = (.Width - (6 * cnSep)) * 0.11
            .Columns("IMP").Width = (.Width - (6 * cnSep)) * 0.21
        End With
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    sdbgCostes.Update
    sdbgDescuentos.Update

    'Liberar objetos
    Set m_oAtributos = Nothing
    Set m_oCostesDescuentos = Nothing
    Set g_oOrdenEntrega = Nothing
    Set g_dcProcesosOrden = Nothing
    Erase m_arOpCostes
    Erase m_arOpDescuentos
    g_vAnyo = Empty
    g_vGMN1 = Empty
    g_vProce = Empty
    g_vItem = Empty
    g_vIdLP = Empty
    frmImpuestoPedido.ctlImpuestos.VentanaCostes = False
End Sub

Private Sub sdbddCostes_Click()
    DropDownCD_Click sdbgCostes, sdbddCostes
End Sub

''' <summary>Gestiona el Click de un combo de costes/descuentos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="oCombo">DropDown</param>
''' <remarks>Llamada desde: sdbddCostes_Click, sdbddDescuentos_Click</remarks>
''' <revision>LTG 10/05/2012</revision>

Private Sub DropDownCD_Click(ByRef oGrid As SSDBGrid, ByRef oCombo As SSDBDropDown)

    With oGrid
        .Columns("COD").Value = oCombo.Columns("COD").Value
        .Columns("NOM").Value = oCombo.Columns("NOM").Value
        .Columns("OP").Value = oCombo.Columns("OP").Value
        .Columns("VAL").Value = oCombo.Columns("VAL").Value
        .Columns("ID_ATRIB").Value = oCombo.Columns("ID_ATRIB").Value
        .Columns("DESCINT").Value = oCombo.Columns("DESCINT").Value
        .Columns("INTRO").Value = oCombo.Columns("INTRO").Value
        .Columns("APLICFAC").Value = False
        .Columns("GENERICO").Value = False
    End With
End Sub

Private Sub sdbddCostes_CloseUp()
If sdbgCostes.DataChanged Then sdbgCostes.Update
End Sub

Private Sub sdbddCostes_DropDown()
    Dim oatrib As CAtributo
    Dim bA�adir As Boolean
    

    'Cargar costes y atributos num�ricos del proceso que no est�n ya en el grid
    sdbddCostes.RemoveAll
    
    If Not m_oAtributos Is Nothing Then
        If m_oAtributos.Count > 0 Then
            For Each oatrib In m_oAtributos
                bA�adir = (m_oCostesDescuentos.Item(CStr(oatrib.Id)) Is Nothing)
                
                If bA�adir And (IsNull(oatrib.PrecioFormula) Or oatrib.EsCoste) Then
                    If oatrib.PrecioFormula = "*" Or oatrib.PrecioFormula = "/" Then
                        oatrib.PrecioFormula = "+"
                        oatrib.valorNum = Null
                    End If
                    
                    sdbddCostes.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valorNum & Chr(m_lSeparador) & oatrib.PrecioFormula & _
                        Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Descripcion & Chr(m_lSeparador) & oatrib.TipoIntroduccion
                End If
            Next
            Set oatrib = Nothing
        End If
    End If
End Sub

''' <summary>Obtiene los posibles costes y descuentos</summary>
''' <remarks>Llamada desde: sdbddCostes_DropDown, sdbgDescuentos_BeforeColUpdate</remarks>
''' <revision>LTG 10/05/2012</revision>

Private Sub ObtenerPosiblesCostesDescuentos()
    Dim sCod As String
    Dim oLineaPedido As CLineaPedido

    Select Case AmbitoDatos
        Case enumAmbitoDatos.cabecera
            If Not IsMissing(g_vAnyo) And Not IsMissing(g_vGMN1) And Not IsMissing(g_vProce) Then
                If NullToStr(g_vAnyo) <> "" And NullToStr(g_vGMN1) <> "" And NullToStr(g_vProce) <> "" Then
                    Set m_oAtributos = g_oOrdenEntrega.DevolverPosiblesCostesDescuentos(g_vAnyo, g_vGMN1, g_vProce)
                End If
            End If
        Case enumAmbitoDatos.Linea
            sCod = KeyLineaPedido
                        
            If sCod <> "" And NullToStr(g_vAnyo) <> "" And NullToStr(g_vGMN1) <> "" And NullToStr(g_vProce) <> "" Then
                Set oLineaPedido = g_oOrdenEntrega.LineasPedido.Item(sCod)
                Set m_oAtributos = oLineaPedido.DevolverPosiblesCostesDescuentos
            End If
    End Select
    
    Set oLineaPedido = Nothing
End Sub



Private Sub sdbddCostes_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    
    On Error Resume Next

    sdbddCostes.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddCostes.Rows - 1
            bm = sdbddCostes.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddCostes.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddCostes.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddCostes_ValidateList(Text As String, RtnPassed As Integer)
    Dim oatrib As CAtributo
    Dim rsDatos As Recordset
    
    If Text = "" Then
        RtnPassed = True
    Else
        ''' Comprobar la existencia
        If sdbddCostes.Columns("COD").Value = Text Then
            RtnPassed = True
            Exit Sub
        End If
        
        RtnPassed = False
        
        For Each oatrib In m_oAtributos
            If oatrib.Cod = Text Then
                RtnPassed = True
                Exit For
            End If
        Next
        
        If Not RtnPassed Then
            'Comprobar si es un atributo num�rico del repositorio de atributos
            If ExisteCosteDescuento(Text, rsDatos) Then
                RtnPassed = True
            Else
                oMensajes.CosteDescuentoNoExiste
            End If
        End If
    End If
End Sub

Private Sub sdbddDescuentos_Click()
    DropDownCD_Click sdbgDescuentos, sdbddDescuentos
End Sub

Private Sub sdbddDescuentos_DropDown()
    Dim oatrib As CAtributo

    'Cargar costes y atributos num�ricos del proceso que no est�n ya en el grid

    sdbddDescuentos.RemoveAll
                    
    If Not m_oAtributos Is Nothing Then
        If m_oAtributos.Count > 0 Then
            For Each oatrib In m_oAtributos
                If m_oCostesDescuentos.Item(CStr(oatrib.Id)) Is Nothing And (IsNull(oatrib.PrecioFormula) Or Not oatrib.EsCoste) Then
                    If oatrib.PrecioFormula = "*" Or oatrib.PrecioFormula = "/" Then
                        oatrib.PrecioFormula = "-"
                        oatrib.valorNum = Null
                    End If
                    
                    sdbddDescuentos.AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valorNum & Chr(m_lSeparador) & oatrib.PrecioFormula & _
                        Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Descripcion & Chr(m_lSeparador) & oatrib.TipoIntroduccion
                End If
            Next
            Set oatrib = Nothing
        End If
    End If
End Sub



Private Sub sdbddDescuentos_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    
    On Error Resume Next

    sdbddDescuentos.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddDescuentos.Rows - 1
            bm = sdbddDescuentos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddDescuentos.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddDescuentos.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddDescuentos_ValidateList(Text As String, RtnPassed As Integer)
    Dim oatrib As CAtributo
    If Text = "" Then
        RtnPassed = True
    Else
        ''' Comprobar la existencia
        If sdbddDescuentos.Columns("COD").Value = Text Then
            RtnPassed = True
            Exit Sub
        End If
        
        RtnPassed = False
        
        If m_oAtributos Is Nothing Then ObtenerPosiblesCostesDescuentos
        
        For Each oatrib In m_oAtributos
            If oatrib.Cod = Text Then
                RtnPassed = True
                Exit For
            End If
        Next
    End If
End Sub

Private Sub sdbddOperCostes_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    
    On Error Resume Next
   
    sdbddOperCostes.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOperCostes.Rows - 1
            bm = sdbddOperCostes.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOperCostes.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgCostes.Columns(sdbgCostes.col).Value = Mid(sdbddOperCostes.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOperCostes.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddOperCostes_ValidateList(Text As String, RtnPassed As Integer)
    Oper_ValidateList sdbgCostes, m_arOpCostes, Text, RtnPassed
End Sub

Private Sub Oper_ValidateList(ByRef oGrid As SSDBGrid, ByVal arOperaciones As Variant, ByRef Text As String, ByRef RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim i As Integer
    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        
        With oGrid
            ''' Comprobar la existencia en la lista
            For i = 0 To UBound(arOperaciones)
                If arOperaciones(i) = .Columns(.col).Text Then
                    bExiste = True
                    Exit For
                End If
            Next
            
            If Not bExiste Then
                oMensajes.NoValido .Columns(.col).caption
                .Columns(.col).Text = ""
                .Columns("IMP").Text = ""
                
                ActualizarImportes (oGrid.Name = "sdbgCostes"), .Columns("ID_ATRIB").Value, 0
                MostrarImportes
                
                RtnPassed = False
                Exit Sub
            End If
            RtnPassed = True
        End With
    End If
End Sub



Private Sub sdbddOperDesc_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    
    On Error Resume Next
   
    sdbddOperDesc.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOperDesc.Rows - 1
            bm = sdbddOperDesc.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOperDesc.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgDescuentos.Columns(sdbgDescuentos.col).Value = Mid(sdbddOperDesc.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOperDesc.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddOperDesc_ValidateList(Text As String, RtnPassed As Integer)
    Oper_ValidateList sdbgDescuentos, m_arOpDescuentos, Text, RtnPassed
End Sub

Private Sub sdbddValor_DropDown()
    If sstabCD.Tab = 0 Then
        Valor_DropDown sdbgCostes
    Else
        Valor_DropDown sdbgDescuentos
    End If
End Sub

''' <summary>Gestiona el evento DropDown del combo de valores de atributos</summary>
''' <param name="oGrid">Grid</param>
''' <remarks>Llamada desde: sdbddValor_DropDown</remarks>
''' <revision>LTG 07/06/2012</revision>

Private Sub Valor_DropDown(ByVal oGrid As SSDBGrid)
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    
    sdbddValor.RemoveAll
    
    sdbddValor.AddItem ""

    iIdAtrib = val(oGrid.Columns("ID_ATRIB").Value)

    Set oatrib = m_oCostesDescuentos.Item(CStr(iIdAtrib))
    If Not oatrib Is Nothing Then
        If oGrid.Columns("INTRO").Value = "1" Then
            Set oLista = oatrib.ListaPonderacion
            If oLista Is Nothing Then
                oatrib.CargarListaDeValores
            ElseIf oLista.Count = 0 Then
                oatrib.CargarListaDeValores
            End If
            For Each oElem In oLista
                sdbddValor.AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        End If
    
        Set oatrib = Nothing
    End If
End Sub


''' <summary>Gestiona el evento PositionList del combo de valores de atributos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="Text">Texto</param>
''' <remarks>Llamada desde: sdbddValor_PositionList</remarks>
''' <revision>LTG 07/06/2012</revision>

Private Sub sdbddValor_PositionList(ByVal Text As String)
    If sstabCD.Tab = 0 Then
        Valor_PositionList sdbgCostes, Text
    Else
        Valor_PositionList sdbgDescuentos, Text
    End If
End Sub

Private Sub Valor_PositionList(ByVal oGrid As SSDBGrid, ByVal Text As String)
    Dim i As Long
    Dim bm As Variant
    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                oGrid.Columns(oGrid.col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    If sstabCD.Tab = 0 Then
        Valor_ValidateList sdbgCostes, Text, RtnPassed
    Else
        Valor_ValidateList sdbgDescuentos, Text, RtnPassed
    End If
End Sub

''' <summary>Gestiona el evento ValidateList del combo de valores de atributos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="Text">Texto</param>
''' <param name="RtnPassed">RtnPassed</param>
''' <remarks>Llamada desde: Valor_ValidateList</remarks>
''' <revision>LTG 07/06/2012</revision>

Private Sub Valor_ValidateList(ByVal oGrid As SSDBGrid, ByVal Text As String, ByRef RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If oGrid.Columns("INTRO").Value = "1" Then
            iIdAtrib = val(oGrid.Columns("ID_ATRIB").Value)

            Set oatrib = m_oCostesDescuentos.Item(CStr(iIdAtrib))
            If oatrib Is Nothing Then Exit Sub
            
            Set oLista = oatrib.ListaPonderacion
            If oLista Is Nothing Then
                oatrib.CargarListaDeValores
            ElseIf oLista.Count = 0 Then
                oatrib.CargarListaDeValores
            End If
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(oGrid.Columns(oGrid.col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        End If
        If Not bExiste Then
            oGrid.Columns(oGrid.col).Text = ""
            oMensajes.NoValido oGrid.Columns(oGrid.col).caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub

Private Sub sdbgCostes_AfterDelete(RtnDispErrMsg As Integer)
    Grid_AfterDelete RtnDispErrMsg
End Sub

Private Sub sdbgCostes_AfterInsert(RtnDispErrMsg As Integer)
    Grid_AfterInsert sdbgCostes
End Sub

''' <summary>Gestiona el evento AfterInsert de los grids de costes y descuentos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="RtnDispErrMsg">RtnDispErrMsg</param>
''' <remarks>Llamada desde: sdbgCostes_AfterInsert, sdbgDescuentos_AfterInsert</remarks>
''' <revision>LTG 10/05/2012</revision>

Private Sub Grid_AfterInsert(ByRef oGrid As SSDBGrid)
    Dim TipoAplicar As TipoAplicarAPrecio
    Dim oatrib As CAtributo
    
    
    If m_bCargaDatos Then Exit Sub
       
    With oGrid
        Select Case AmbitoDatos
            Case enumAmbitoDatos.cabecera
                TipoAplicar = TipoAplicarAPrecio.TotalOferta
            Case enumAmbitoDatos.Linea
                TipoAplicar = TipoAplicarAPrecio.TotalItem
        End Select
        Set oatrib = m_oCostesDescuentos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("NOM").Value, TipoNumerico, ambito:=AmbItem, _
                    valor_num:=.Columns("VAL").Value, PrecioAplicarA:=TipoAplicar, PrecioFormula:=.Columns("OP").Value, bGenerico:=.Columns("Generico").Value, _
                    TipoIntroduccion:=.Columns("INTRO").Value)
        oatrib.Facturar = .Columns("APLICFAC").Value
        If oatrib.Generico Then oatrib.Id = Left(.Columns("ID_ATRIB").Value, InStr(1, .Columns("ID_ATRIB").Value, "G") - 1)
                  
        If Not m_bActualizarBD Then
            ActualizarImportes (.Name = "sdbgCostes"), .Columns("ID_ATRIB").Value, IIf(.Columns("IMP").Value <> "", .Columns("IMP").Value, 0)
        Else
            'No es necesario programar la inserci�n en BD porque desde la pantalla de seguimiento no se da el caso en que
            'el c�digo pase por aqu�. S�lo se pasa por aqu� en el caso de que se de de alta un nuevo CD mediante el combo de la
            'columna COD, y este est� deshabilitado para la pantalla de seguimiento.
            'Al a�adir mediante el formulario de atributos pasa por esta funci�n, pero todav�a el atributo nuevo no tiene
            'operaci�n ni valor, as� que no se inserta en BD hasta que se introducen estos datos, es decir, en el BeforeUpdate
        End If
        MostrarImportes
    End With
    
    cmdDeshacerC.Enabled = False
    cmdDeshacerD.Enabled = False
    
    Set oatrib = Nothing
End Sub

Private Sub sdbgCostes_AfterUpdate(RtnDispErrMsg As Integer)
    Grid_AfterUpdate RtnDispErrMsg
End Sub

Private Sub sdbgCostes_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)

    Grid_BeforeColUpdate sdbgCostes, ColIndex, Cancel
End Sub

''' <summary>Gestiona el evento BeforeColUpdate de los grids de costes y descuentos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="ColIndex">�ndice columna</param>
''' <param name="OldValue">Valor viejo</param>
''' <param name="Cancel">Indica si hay que cancelar el borrado</param>
''' <remarks>Llamada desde: sdbgCostes_BeforeColUpdate, sdbgDescuentos_BeforeColUpdate</remarks>
''' <revision>LTG 10/05/2012</revision>

Private Sub Grid_BeforeColUpdate(ByRef oGrid As SSDBGrid, ByRef ColIndex As Integer, ByRef Cancel As Integer)
    Dim dblImp As Double
    Dim rsDatos As Recordset
    
    With oGrid
        Select Case .Columns(ColIndex).Name
            Case "COD"
                If .Columns(ColIndex).Value <> "" Then
                    If ExisteCosteDescuento(.Columns(ColIndex).Value, rsDatos) Then
                        If Not SQLBinaryToBoolean(rsDatos("GENERICO")) Then
                            .Columns("COD").Value = rsDatos("COD")
                            .Columns("NOM").Value = rsDatos("DEN")
                            .Columns("OP").Value = rsDatos("FORMULA")
                            .Columns("VAL").Value = rsDatos("VALOR_NUM")
                            .Columns("ID_ATRIB").Value = rsDatos("ID")
                            .Columns("DESCINT").Value = rsDatos("DESCR")
                            .Columns("INTRO").Value = rsDatos("INTRO")
                            .Columns("APLICFAC").Value = False
                            .Columns("GENERICO").Value = False
                        Else
                            CargarCDGenerico oGrid
                        End If
                    Else
                        Cancel = True
                        oMensajes.CosteDescuentoNoExiste
                    End If
                End If
            Case "NOM"
                If .Columns(ColIndex).Value <> "" Then
                    CargarCDGenerico oGrid
                End If
            Case "OP"
                If .Columns(ColIndex).Value <> "" Then
                    If .Columns("VAL").Value <> "" Then
                        If txtImpBruto.Text <> "" Then
                            dblImp = Abs(CDbl(txtImpBruto.Text) - AplicarAtributo(.Columns(ColIndex).Value, CDbl(txtImpBruto.Text), .Columns("VAL").Value))
                            If dblImp > 0 Then
                                .Columns("IMP").Value = dblImp
                            Else
                                .Columns("IMP").Value = ""
                            End If
                        Else
                            .Columns("IMP").Value = ""
                        End If
                    End If
                Else
                    .Columns("IMP").Value = ""
                End If
            Case "VAL"
                If .Columns(ColIndex).Value <> "" Then
                    If Not IsNumeric(.Columns(ColIndex).Value) Then
                        oMensajes.NoValido .Columns(ColIndex).caption
                        Cancel = True
                    Else
                        If .Columns("OP").Value <> "" Then
                            'No se admiten n�meros negativos
                            If CDbl(.Columns(ColIndex).Value) < 0 Then
                                oMensajes.NoValido .Columns(ColIndex).caption
                                Cancel = True
                                Exit Sub
                            End If
                            
                            'Si la op. es de porcentaje el valor no puede exceder 100
                            If InStr(1, .Columns("OP").Value, "%") > 0 Then
                                If CDbl(.Columns(ColIndex).Value) > 100 Then
                                    oMensajes.NoValido .Columns(ColIndex).caption
                                    Cancel = True
                                    Exit Sub
                                End If
                            End If
                            
                            If txtImpBruto.Text <> "" Then
                                dblImp = Abs(CDbl(txtImpBruto.Text) - AplicarAtributo(.Columns("OP").Value, CDbl(txtImpBruto.Text), .Columns(ColIndex).Value))
                                If dblImp > 0 Then
                                    .Columns("IMP").Value = dblImp
                                Else
                                    .Columns("IMP").Value = ""
                                End If
                            Else
                                .Columns("IMP").Value = ""
                            End If
                        End If
                    End If
                Else
                    .Columns("IMP").Value = ""
                End If
        End Select
    End With
    
    Set rsDatos = Nothing
End Sub

''' <summary>Carga los datos del coste/descuento gen�rico</summary>
''' <param name="oGrid">Grid</param>
''' <remarks>Llamada desde: Grid_BeforeColUpdate</remarks>
''' <revision>LTG 17/05/2012</revision>

Private Sub CargarCDGenerico(ByRef oGrid As SSDBGrid)
    Dim oAtribs As CAtributos
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
    If sstabCD.Tab = 0 Then
        oAtribs.CargarTodosLosAtributos "", vTipo:=TipoNumerico, udtIntroduccion:=TAtributoIntroduccion.IntroLibre, vGenerico:=True, vCoste:=True
    Else
        oAtribs.CargarTodosLosAtributos "", vTipo:=TipoNumerico, udtIntroduccion:=TAtributoIntroduccion.IntroLibre, vGenerico:=True, vDescuento:=True
    End If
    
    If oAtribs.Count > 0 Then
        m_iNumGenerico = m_iNumGenerico + 1
        
        oGrid.Columns("COD").Value = oAtribs.Item(1).Cod
        'Al ID del gen�rico se le a�ade un sufijo dependiente del n� de gen�ricos introducidos para que se puedan meter m�s de 1
        oGrid.Columns("ID_ATRIB").Value = oAtribs.Item(1).Id & "G" & CStr(m_iNumGenerico)
        oGrid.Columns("APLICFAC").Value = 0
        oGrid.Columns("GENERICO").Value = True
        oGrid.Columns("INTRO").Value = oAtribs.Item(1).TipoIntroduccion
    End If
    
    Set oAtribs = Nothing
End Sub


''' <summary>Actualiza los importes de los ojetos despu�s de un cambio en los CDs</summary>
''' <param name="bCoste">Indica si la actializaci�n es debida a costes o descuentos</param>
''' <param name="IdAtrib">Id atributo</param>
''' <param name="dblNuevoImp">Nuevo importe del atributo</param>
''' <remarks>Llamada desde: Grid_BeforeColUpdate</remarks>
''' <revision>LTG 14/05/2012</revision>

Private Sub ActualizarImportes(ByVal bCostes As Boolean, ByVal idAtrib As Variant, ByVal dblNuevoImp As Double)
    Dim dblImpOld As Double
    Dim sCod As String
    
    
    'Actualizar el objeto
    If Not m_oCostesDescuentos.Item(CStr(idAtrib)) Is Nothing Then
        dblImpOld = m_oCostesDescuentos.Item(CStr(idAtrib)).importe
                
        m_oCostesDescuentos.Item(CStr(idAtrib)).importe = dblNuevoImp
        
        'Actualizar importes de costes/descuentos y neto
        Select Case AmbitoDatos
            Case enumAmbitoDatos.cabecera
                If bCostes Then
                    g_oOrdenEntrega.ImporteCostes = g_oOrdenEntrega.ImporteCostes - dblImpOld + dblNuevoImp
                Else
                    g_oOrdenEntrega.ImporteDescuentos = g_oOrdenEntrega.ImporteDescuentos - dblImpOld + dblNuevoImp
                End If
                g_oOrdenEntrega.ImporteNeto = g_oOrdenEntrega.importe + g_oOrdenEntrega.ImporteCostes - g_oOrdenEntrega.ImporteDescuentos
                g_oOrdenEntrega.CalcularImportes False
            Case enumAmbitoDatos.Linea
                sCod = KeyLineaPedido
                
                If sCod <> "" Then
                    With g_oOrdenEntrega.LineasPedido.Item(sCod)
                        g_oOrdenEntrega.importe = g_oOrdenEntrega.importe - .ImporteNeto
                        If bCostes Then
                            .ImporteCostes = .ImporteCostes - dblImpOld + dblNuevoImp
                        Else
                            .ImporteDescuentos = .ImporteDescuentos - dblImpOld + dblNuevoImp
                        End If
                        .ImporteNeto = .ImporteBruto + .ImporteCostes - .ImporteDescuentos
                        
                        g_oOrdenEntrega.importe = g_oOrdenEntrega.importe + .ImporteNeto
                        If g_oOrdenEntrega.CostesDescuentos Is Nothing Then g_oOrdenEntrega.CargarCostesDescuentos
                        g_oOrdenEntrega.CalcularImportes False
                    End With
                End If
        End Select
    End If
End Sub

''' <summary>Muestra los importes en pantalla</summary>
''' <remarks>Llamada desde: Grid_BeforeColUpdate</remarks>
''' <revision>LTG 14/06/2012</revision>

Private Sub MostrarImportes()
    Dim dblImpNeto As Double
    Dim sCod As String
    
    Select Case AmbitoDatos
        Case enumAmbitoDatos.cabecera
            dblImpNeto = g_oOrdenEntrega.ImporteNeto
            sstabCD.TabCaption(0) = m_sCostes & " = " & Format(g_oOrdenEntrega.ImporteCostes * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
            sstabCD.TabCaption(1) = m_sDescuentos & " = " & Format(g_oOrdenEntrega.ImporteDescuentos * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
        Case enumAmbitoDatos.Linea
            sCod = KeyLineaPedido
            
            If sCod <> "" Then
                With g_oOrdenEntrega.LineasPedido.Item(sCod)
                    dblImpNeto = .ImporteNeto
                    
                    'Mostrar  los importes en las pesta�as
                    sstabCD.TabCaption(0) = m_sCostes & " = " & Format(.ImporteCostes * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
                    sstabCD.TabCaption(1) = m_sDescuentos & " = " & Format(.ImporteDescuentos * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard") & " " & g_oOrdenEntrega.Moneda
                End With
            End If
    End Select
    
    If dblImpNeto = 0 Then
        txtImpNeto.Text = ""
    Else
        txtImpNeto.Text = Format(dblImpNeto * g_dblCambioMoneda / IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1), "standard")
    End If
End Sub

Private Sub sdbgCostes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    Grid_BeforeDelete sdbgCostes, Cancel, DispPromptMsg
End Sub

''' <summary>Comprueba si existe el coste descuento con el c�digo indicado</summary>
''' <param name="sCod">Cod. atributo</param>
''' <param name="rsDatos">recordset con los datos del atributo</param>
''' <remarks>Llamada desde: Grid_BeforeColUpdate</remarks>
''' <revision>LTG 14/05/2012</revision>

Private Function ExisteCosteDescuento(ByVal sCod As String, ByRef rsDatos As Recordset) As Boolean
    Dim oAtribs As CAtributos
    Dim rsAtrib As Recordset
    ExisteCosteDescuento = False
    
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
    Set rsAtrib = oAtribs.DevolverAtributosDesde(sCod)
    If Not rsAtrib Is Nothing Then
        If rsAtrib.RecordCount > 0 Then
            ExisteCosteDescuento = (rsAtrib("TIPO") = TiposDeAtributos.TipoNumerico)
            Set rsDatos = rsAtrib
        End If
    End If
    
    Set rsAtrib = Nothing
    Set oAtribs = Nothing
End Function

''' <summary>Gestiona el evento BeforeDelete de los grids de costes y descuentos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="Cancel">Indica si hay que cancelar el borrado</param>
''' <param name="DispPromptMsg">DispPromptMsg</param>
''' <remarks>Llamada desde: sdbgCostes_BeforeDelete, sdbgDescuentos_BeforeDelete</remarks>
''' <revision>LTG 10/05/2012</revision>

Private Sub Grid_BeforeDelete(ByRef oGrid As SSDBGrid, ByRef Cancel As Integer, ByRef DispPromptMsg As Integer)

    DispPromptMsg = 0
            
    Cancel = (EliminarCD(oGrid, False).NumError <> TESnoerror)
End Sub

Private Sub Grid_AfterDelete(RtnDispErrMsg As Integer)
    If m_bCDsLineaModif Then
        m_bCDsLineaModif = False
        RaiseEvent OnModifCostesDescuentosLinea
    End If
End Sub

''' <summary>Comprueba los datos obligatorios</summary>
''' <param name="oGrid">Grid</param>
''' <remarks>Llamada desde: Grid_BeforeUpdate</remarks>
''' <revision>LTG 14/05/2012</revision>

Private Function ComprobarDatos(ByRef oGrid As SSDBGrid) As Boolean
    ComprobarDatos = True

    With oGrid
        If .Columns("COD").Value = "" Then
            oMensajes.NoValido .Columns("COD").caption
            ComprobarDatos = False
        End If
    End With
End Function

Private Sub sdbgCostes_BeforeUpdate(Cancel As Integer)
    Grid_BeforeUpdate sdbgCostes, Cancel
End Sub

''' <summary>Gestiona el evento BeforeUpdate de los grids de costes y descuentos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="Cancel">Indica si hay que cancelar la actualizaci�n</param>
''' <remarks>Llamada desde: sdbgCostes_BeforeUpdate, sdbgDescuentos_BeforeUpdate</remarks>
''' <revision>LTG 10/05/2012</revision>

Private Sub Grid_BeforeUpdate(ByRef oGrid As SSDBGrid, ByRef Cancel As Integer)
    Dim vOpOld As Variant
    Dim vValorNumOld As Variant
    Dim bFacturarOld As Boolean
    Dim oatrib As CAtributo
    Dim teserror As TipoErrorSummit
    Dim oLineaPedido As CLineaPedido
    Dim sCod As String
    Dim dblImporte As Double
    
    With oGrid
        If Not ComprobarDatos(oGrid) Then
            Cancel = True
            Exit Sub
        End If
                
        Set oatrib = m_oCostesDescuentos.Item(.Columns("ID_ATRIB").Value)
 
        If Not oatrib Is Nothing Then
            vOpOld = oatrib.PrecioFormula
            vValorNumOld = oatrib.valorNum
            bFacturarOld = oatrib.Facturar
            
            oatrib.PrecioFormula = StrToNull(.Columns("OP").Value)
            If .Columns("VAL").Value <> "" Then
                If oatrib.PrecioFormula = "+" Or oatrib.PrecioFormula = "-" Then
                    oatrib.valorNum = NullToDbl0(.Columns("VAL").Value) / g_dblCambioMoneda * IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1)
                Else
                    oatrib.valorNum = NullToDbl0(.Columns("VAL").Value)
                End If
            Else
                oatrib.valorNum = Null
            End If
            oatrib.Facturar = .Columns("APLICFAC").Value
            oatrib.Descripcion = .Columns("DESCINT").Value
            'g_dblCambioMonedaOferta es la variable que muestra el cambio de la moneda de la oferta con respecto a la del proceso
            'a la hora de emitir el pedido es necesario grabar en la moneda central y se hace el paso en funcionn de la moneda de la oferta
            'por eso siempre convertimos los importes a la moneda de la oferta con el IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1)
            
            dblImporte = IIf(.Columns("IMP").Value = "", 0, NullToDbl0(.Columns("IMP").Value)) * IIf(g_dblCambioMonedaOferta > 0, g_dblCambioMonedaOferta, 1)
            If Not m_bActualizarBD Then
                ActualizarImportes (.Name = "sdbgCostes"), .Columns("ID_ATRIB").Value, dblImporte
            Else
                'Actualizaci�n en BD
                If Not m_bFormAtribAbierta Then
                    With oGrid
                        If .Columns("OP").Value <> "" And .Columns("VAL").Value <> "" Then
                            If AmbitoDatos = Linea Then
                                sCod = KeyLineaPedido
                                If sCod <> "" Then
                                    Set oLineaPedido = g_oOrdenEntrega.LineasPedido.Item(sCod)
                                    If NullToStr(.Columns("ID_CD").Value) = "" Then
                                        teserror = oatrib.InsertarCosteDescuentoLineaPedido(g_oOrdenEntrega, oLineaPedido, (dblImporte / g_dblCambioMoneda), True, False)
                                        If teserror.NumError = TESnoerror Then
                                            .Columns("ID_CD").Value = oatrib.IdCD
                                            m_bCDsLineaModif = True
                                        End If
                                    Else
                                        teserror = oatrib.ModificarCosteDescuentoLineaPedido(g_oOrdenEntrega, oLineaPedido, (dblImporte / g_dblCambioMoneda), False)
                                        m_bCDsLineaModif = True
                                    End If
                                    Set oLineaPedido = Nothing
                                End If
                            Else
                                If NullToStr(.Columns("ID_CD").Value) = "" Then
                                    teserror = oatrib.InsertarCosteDescuentoPedido(g_oOrdenEntrega, (dblImporte / g_dblCambioMoneda), True, False)
                                    If teserror.NumError = TESnoerror Then
                                        .Columns("ID_CD").Value = oatrib.IdCD
                                    End If
                                Else
                                    teserror = oatrib.ModificarCosteDescuentoPedido(g_oOrdenEntrega, (dblImporte / g_dblCambioMoneda), False)
                                End If
                            End If
                            
                            If teserror.NumError <> TESnoerror Then
                                TratarError teserror
                                Cancel = True
                                
                                oatrib.PrecioFormula = vOpOld
                                oatrib.valorNum = vValorNumOld
                                oatrib.Facturar = bFacturarOld
                            End If
                        End If
                    End With
                End If
            End If
            
            MostrarImportes
            
            Set oatrib = Nothing
        End If
        .Columns("MODIF").Value = False
    End With
End Sub

Private Sub Grid_AfterUpdate(RtnDispErrMsg As Integer)
    If m_bCDsLineaModif Then
        m_bCDsLineaModif = False
        RaiseEvent OnModifCostesDescuentosLinea
    End If
End Sub

Private Sub sdbgCostes_BtnClick()
    Grid_BtnClick sdbgCostes
End Sub

Private Sub Grid_BtnClick(ByRef oGrid As SSDBGrid)

    With oGrid
        Select Case .Columns(.col).Name
            Case "DESC"
                frmATRIBDescr.g_bEdicion = Not m_bSoloLectura
                frmATRIBDescr.g_sOrigen = g_sOrigen
                frmATRIBDescr.txtDescr.Text = NullToStr(.Columns("DESCINT").Value)
                frmATRIBDescr.Show 1
                
            Case "IMPUESTOS"
                VentanaCostes = True
                If UCase(g_sOrigen) = "FRMSEGUIMIENTO" Or UCase(g_sOrigen) = "FRMSEGUIMIENTOLINEA" Then
                    frmImpuestoPedido.ctlImpuestos.VentanaSeguimiento = Impuesto_VentanaSeguimiento
                    frmImpuestoPedido.ctlImpuestos.PermisoAunSinRecepc = Impuesto_PermisoAunSinRecepc
                    frmImpuestoPedido.ctlImpuestos.Cambio = Impuesto_Cambio
                    Set frmImpuestoPedido.ctlImpuestos.Orden = Impuesto_Orden
                    frmImpuestoPedido.ctlImpuestos.LineaId = Impuesto_LineaId
                End If
                frmImpuestoPedido.ctlImpuestos.VentanaCostes = VentanaCostes
                frmImpuestoPedido.ctlImpuestos.CabeceraOLinea = AmbitoDatos
                frmImpuestoPedido.ctlImpuestos.ProveCostes = g_oOrdenEntrega.ProveCod
                frmImpuestoPedido.ctlImpuestos.m_bModificar = m_bModoEdicion
                frmImpuestoPedido.ctlImpuestos.g_bMostrandoLOG = g_bMostrandoLOG
                'Si se introduce un coste nuevo e inmediatamente se introducen los impuestos, a�n ese coste no est� introducido en BD.
                'Hasta ahora se introduc�an al cambiar de fila o una vez cerrado el formulario
                sdbgCostes.Update
                
                'Con m_oCostesDescuentos.Item(sdbgCostes.Row + 1) coge el primer CD en edici�n, pero sin distinguir coste de descuento
                Set frmImpuestoPedido.ctlImpuestos.g_Coste = m_oCostesDescuentos.Item(.Columns("ID_ATRIB").Value)
                Set frmImpuestoPedido.ctlImpuestos.g_CosteCabecera = m_oCostesDescuentos.Item(.Columns("ID_ATRIB").Value)
                frmImpuestoPedido.ctlImpuestos.NuevaLinea = Not m_bActualizarBD
                
                'Si el pedido tiene recepciones o facturas, no se van a poder a�adir costes, por lo que tampoco tendr� sentido a�adir impuestos a dichos costes
                If cmdA�adirC.Visible = False Then
                    frmImpuestoPedido.ctlImpuestos.PermiteModif = False
                'Si se pueden a�adir costes, se comprueba que se pueda a�adir impuestos:' Permisos de modificaci�n de impuestos: 10401 - 10710
                Else
                    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATModificarImpuesto)) Is Nothing) Or Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.TipImpModificar)) Is Nothing) Then
                        frmImpuestoPedido.ctlImpuestos.PermiteModif = True
                    Else
                        frmImpuestoPedido.ctlImpuestos.PermiteModif = False
                    End If
                End If
                
                'Cabecera o Linea
                If AmbitoDatos = 1 Then
                    frmImpuestoPedido.ctlImpuestos.ArtCostes = g_oOrdenEntrega.LineasPedido.Item(KeyLineaPedido).ArtCod_Interno
                    bCDLineaImpuestos = True
                End If
                'Pantalla Impuestos
                frmImpuestoPedido.Show vbModal
                If Not m_bSoloLectura Then
                    If frmImpuestoPedido.g_bImpuestosModificados Then
                        .Columns("MODIF").Value = True
                        sdbgCostes.Update
                    End If
                End If
            Exit Sub
        End Select
    End With
End Sub

Private Sub sdbgCostes_Change()

    cmdDeshacerC.Enabled = True
End Sub

Private Sub sdbgCostes_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Grid_RowColChange sdbgCostes, sdbddCostes.hWnd
End Sub

''' <summary>Gestiona el evento RowColChange de los grids de costes y descuentos</summary>
''' <param name="oGrid">Grid</param>
''' <param name="DropDownHwnd">Hwnd del control drop down correspondiente</param>
''' <param name="LastRow">Fila anterior</param>
''' <param name="LastCol">Columna anterior</param>
''' <remarks>Llamada desde: sdbgCostes_RowColChange, sdbgDescuentos_RowColChange</remarks>
''' <revision>LTG 11/05/2012</revision>

Private Sub Grid_RowColChange(ByRef oGrid As SSDBGrid, ByVal DropDownHwnd As Long)
    Dim bMostrarCombo As Boolean
    Dim oatrib As CAtributo
    
    If m_bModoEdicion Then
        With oGrid
            If .col > -1 Then
                Select Case .Columns(.col).Name
                    Case "COD"
                        bMostrarCombo = False
                        
                        If .IsAddRow And Not m_bActualizarBD Then
                            If Not m_oAtributos Is Nothing Then
                                If m_oAtributos.Count > 0 Then
                                    'Comprobar que hay costes o descuentos distintos de los que ya hay asignados
                                    For Each oatrib In m_oAtributos
                                        If oGrid.Name = "sdbgCostes" Then
                                            If (IsNull(oatrib.PrecioFormula) Or oatrib.EsCoste) And m_oCostesDescuentos.Item(CStr(oatrib.Id)) Is Nothing Then
                                                bMostrarCombo = True
                                                Exit For
                                            End If
                                        Else
                                            If (IsNull(oatrib.PrecioFormula) Or Not oatrib.EsCoste) And m_oCostesDescuentos.Item(CStr(oatrib.Id)) Is Nothing Then
                                                bMostrarCombo = True
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    Set oatrib = Nothing
                                End If
                            End If
                        End If
                        
                        If bMostrarCombo Then
                            .Columns("COD").DropDownHwnd = DropDownHwnd
                        Else
                            .Columns("COD").DropDownHwnd = 0
                        End If
                        .Columns("COD").Locked = False
                    Case "NOM"
                        If NullToStr(.Columns("GENERICO").Value) = "" Or .Columns("GENERICO").Value Then
                            .Columns(.col).Locked = False
                        Else
                            .Columns(.col).Locked = True
                        End If
                    Case "VAL"
                        'Combo de valores
                        If .Columns("INTRO").Value = 1 Then
                            sdbddValor.RemoveAll
                            .Columns("VAL").DropDownHwnd = sdbddValor.hWnd
                            sdbddValor.Enabled = True
                            sdbddValor_DropDown
                        Else
                            .Columns("VAL").DropDownHwnd = 0
                        End If
                    
                End Select
            End If
        End With
    End If
End Sub

Private Sub sdbgCostes_RowLoaded(ByVal Bookmark As Variant)
    With sdbgCostes
        If m_bLineaBajaLOG And Not g_bMostrandoLOG Then
            'Ponemos todo en gris
            .Columns("COD").CellStyleSet "FondoGris"
            .Columns("NOM").CellStyleSet "FondoGris"
            .Columns("APLICFAC").CellStyleSet "FondoGris"
            .Columns("OP").CellStyleSet "FondoGris"
            .Columns("VAL").CellStyleSet "FondoGris"
            .Columns("IMP").CellStyleSet "FondoGris"
        Else
            If .Columns("DESCINT").Value <> "" Then
               .Columns("DESC").Value = "..."
            End If
            If g_oOrdenEntrega.TipoLogPedido = Eliminar_CD_Cabecera Or g_oOrdenEntrega.TipoLogPedido = Eliminar_CD_Linea Then
                .Columns("COD").CellStyleSet "Eliminado"
                .Columns("NOM").CellStyleSet "Eliminado"
                .Columns("APLICFAC").CellStyleSet "Eliminado"
                .Columns("OP").CellStyleSet "Eliminado"
                .Columns("VAL").CellStyleSet "Eliminado"
                .Columns("IMP").CellStyleSet "Eliminado"
            End If
        End If
    End With
End Sub

Private Sub sdbgDescuentos_AfterDelete(RtnDispErrMsg As Integer)
    Grid_AfterDelete RtnDispErrMsg
End Sub

Private Sub sdbgDescuentos_AfterInsert(RtnDispErrMsg As Integer)
    Grid_AfterInsert sdbgDescuentos
End Sub

Private Sub sdbgDescuentos_AfterUpdate(RtnDispErrMsg As Integer)
    Grid_AfterUpdate RtnDispErrMsg
End Sub

Private Sub sdbgDescuentos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Grid_BeforeColUpdate sdbgDescuentos, ColIndex, Cancel
End Sub

Private Sub sdbgDescuentos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    Grid_BeforeDelete sdbgDescuentos, Cancel, DispPromptMsg
End Sub

Private Sub sdbgDescuentos_BeforeUpdate(Cancel As Integer)
    Grid_BeforeUpdate sdbgDescuentos, Cancel
End Sub

Private Sub sdbgDescuentos_BtnClick()
    Grid_BtnClick sdbgDescuentos
End Sub

Private Sub sdbgDescuentos_Change()
    cmdDeshacerD.Enabled = True
End Sub

Private Sub sdbgDescuentos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Grid_RowColChange sdbgDescuentos, sdbddDescuentos.hWnd
End Sub

Private Sub sdbgDescuentos_RowLoaded(ByVal Bookmark As Variant)
With sdbgDescuentos
    If m_bLineaBajaLOG And Not g_bMostrandoLOG Then
        'Ponemos todo en gris
        .Columns("COD").CellStyleSet "FondoGris"
        .Columns("NOM").CellStyleSet "FondoGris"
        .Columns("APLICFAC").CellStyleSet "FondoGris"
        .Columns("OP").CellStyleSet "FondoGris"
        .Columns("VAL").CellStyleSet "FondoGris"
    Else
        If .Columns("DESCINT").Value <> "" Then
           .Columns("DESC").Value = "..."
        End If
        If g_oOrdenEntrega.TipoLogPedido = Eliminar_CD_Cabecera Or g_oOrdenEntrega.TipoLogPedido = Eliminar_CD_Linea Then
            .Columns("COD").CellStyleSet "Eliminado"
            .Columns("NOM").CellStyleSet "Eliminado"
            .Columns("APLICFAC").CellStyleSet "Eliminado"
            .Columns("OP").CellStyleSet "Eliminado"
            .Columns("VAL").CellStyleSet "Eliminado"
        End If
    End If
End With
End Sub

Private Sub sstabCD_Click(PreviousTab As Integer)

    If PreviousTab = 0 Then
        If sdbgCostes.DataChanged Then sdbgCostes.Update
    Else
        If sdbgDescuentos.DataChanged Then sdbgDescuentos.Update
    End If
End Sub


Private Sub UserControl_Resize()
Arrange
End Sub
