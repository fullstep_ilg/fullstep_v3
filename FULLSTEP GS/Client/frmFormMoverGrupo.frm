VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFormMoverGrupo 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "DMover o copiar"
   ClientHeight    =   3150
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   3855
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3150
   ScaleWidth      =   3855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.ListView lstGrupos 
      Height          =   2010
      Left            =   120
      TabIndex        =   0
      Top             =   300
      Width           =   3675
      _ExtentX        =   6482
      _ExtentY        =   3545
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "grupo"
         Object.Width           =   6351
      EndProperty
   End
   Begin VB.CheckBox chkCopiar 
      Caption         =   "DCrear una copia"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   2400
      Width           =   4335
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "D&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   820
      TabIndex        =   2
      Top             =   2760
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "D&Cancelar"
      Height          =   315
      Left            =   1900
      TabIndex        =   3
      Top             =   2760
      Width           =   975
   End
   Begin VB.Label lblAntesDe 
      Caption         =   "DAntes de "
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   60
      Width           =   1755
   End
End
Attribute VB_Name = "frmFormMoverGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables privadas
Private m_sIdiFinal As String
Private m_sIdiCopia As String

Private Sub CargarRecursos()
Dim ador As ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FORM_MOVER_GRUPO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        Me.Caption = ador(0).Value   '1 Mover o copiar
        ador.MoveNext
        lblAntesDe.Caption = ador(0).Value  '2 Antes del grupo
        ador.MoveNext
        chkCopiar.Caption = ador(0).Value  '3 Crear una copia
        ador.MoveNext
        cmdAceptar.Caption = ador(0).Value  '4 &Aceptar
        ador.MoveNext
        cmdCancelar.Caption = ador(0).Value  '5 &Cancelar
        ador.MoveNext
        m_sIdiFinal = ador(0).Value '6 (Mover al final)
        ador.MoveNext
        m_sIdiCopia = ador(0).Value '7 (Copia)
        
        ador.Close
    End If
    
    Set ador = Nothing
End Sub

Private Sub cmdAceptar_Click()
    Dim i As Integer
    Dim teserror As TipoErrorSummit
    Dim oGrupo As CFormGrupo
    Dim lOrden As Long
    Dim lOrdenAnt As Long
    Dim sCod As String
    Dim bSuma As Boolean
    
    'Guarda el orden de los grupos o crea una copia de uno de ellos:
    
    Screen.MousePointer = vbHourglass
    
    If chkCopiar.Value = vbChecked Then
        'Copia un grupo nuevo y lo pone en la posici�n indicada:
        sCod = Mid(lstGrupos.SelectedItem.key, 2)
        If sCod = "0" Then  'Se copia al final
            sCod = Mid(lstGrupos.ListItems.Item(lstGrupos.SelectedItem.Index - 1).key, 2)
            lOrden = frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sCod)).Orden + 1
        Else 'Se posiciona antes del grupo seleccionado
            lOrden = frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sCod)).Orden
        End If
        
        For Each oGrupo In frmFormularios.g_oFormSeleccionado.Grupos
            If oGrupo.Orden >= lOrden Then
                oGrupo.Orden = oGrupo.Orden + 1
            End If
        Next
        
        teserror = frmFormularios.g_oFormSeleccionado.Grupos.CopiarGrupo(frmFormularios.g_oGrupoSeleccionado.Id, lOrden, m_sIdiCopia, frmFormularios.g_oFormSeleccionado.Id)
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
        
    
    Else
        'Mueve un grupo a otra posici�n:
        sCod = Mid(lstGrupos.SelectedItem.key, 2)
        If sCod = "0" Then  'Se mueve al final
            sCod = Mid(lstGrupos.ListItems.Item(lstGrupos.SelectedItem.Index - 1).key, 2)
            lOrden = frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sCod)).Orden
        Else 'Se posiciona antes del grupo seleccionado
            If frmFormularios.g_oGrupoSeleccionado.Id = frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sCod)).Id Then
                Screen.MousePointer = vbNormal
                Unload Me
                Exit Sub
            Else
                If frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sCod)).Orden > frmFormularios.g_oGrupoSeleccionado.Orden Then
                    lOrden = frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sCod)).Orden - 1
                Else
                    lOrden = frmFormularios.g_oFormSeleccionado.Grupos.Item(CStr(sCod)).Orden
                End If
            End If
        End If
        
        If lOrden = 0 Then lOrden = 1
        If lOrden = frmFormularios.g_oGrupoSeleccionado.Orden Then
            Screen.MousePointer = vbNormal
            Unload Me
            Exit Sub
        ElseIf lOrden > frmFormularios.g_oGrupoSeleccionado.Orden Then
            bSuma = False
        Else
            bSuma = True
        End If
        
        lOrdenAnt = frmFormularios.g_oGrupoSeleccionado.Orden
        frmFormularios.g_oGrupoSeleccionado.Orden = lOrden
        For Each oGrupo In frmFormularios.g_oFormSeleccionado.Grupos
            If oGrupo.Id <> frmFormularios.g_oGrupoSeleccionado.Id Then
                If bSuma = False Then
                    If (oGrupo.Orden <= lOrden) And (oGrupo.Orden > lOrdenAnt) Then
                        oGrupo.Orden = oGrupo.Orden - 1
                    End If
                Else
                    If (oGrupo.Orden >= lOrden) And (oGrupo.Orden < lOrdenAnt) Then
                        oGrupo.Orden = oGrupo.Orden + 1
                    End If
                End If
            End If
        Next
        
        teserror = frmFormularios.g_oFormSeleccionado.Grupos.GuardarOrdenGrupos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
    
    '  ***** Habr� que actualizar el tab de frmFormularios ****
    frmFormularios.ModificarOrdenGrupos
    
    Screen.MousePointer = vbNormal
    
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
Dim oGrupo As CFormGrupo

    Me.Height = 3570
    Me.Width = 3975
    
    CargarRecursos
    
    'carga los grupos en la lista:
    For Each oGrupo In frmFormularios.g_oFormSeleccionado.Grupos
        lstGrupos.ListItems.Add , "#" & CStr(oGrupo.Id), NullToStr(oGrupo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
    Next
    lstGrupos.ListItems.Add , "#0", m_sIdiFinal
    
End Sub



