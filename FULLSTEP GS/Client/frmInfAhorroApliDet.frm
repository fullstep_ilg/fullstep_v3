VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form frmInfAhorroApliDet 
   BackColor       =   &H00808000&
   Caption         =   "Detalle de informe de ahorros aplicados"
   ClientHeight    =   2685
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8715
   ForeColor       =   &H00808000&
   Icon            =   "frmInfAhorroApliDet.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2685
   ScaleWidth      =   8715
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2535
      Left            =   120
      ScaleHeight     =   2535
      ScaleWidth      =   8475
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   90
      Width           =   8475
      Begin VB.PictureBox picTipoGrafico 
         BackColor       =   &H00808000&
         BorderStyle     =   0  'None
         Height          =   315
         Left            =   420
         ScaleHeight     =   315
         ScaleWidth      =   2595
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   2595
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoGrafico 
            Height          =   285
            Left            =   0
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   0
            Width           =   1515
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Row.Count       =   5
            Row(0)          =   "Barras 2D"
            Row(1)          =   "Barras 3D"
            Row(2)          =   "Lineas 2D"
            Row(3)          =   "Lineas 3D"
            Row(4)          =   "Tarta"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   3200
            Columns(0).Caption=   "TIPO"
            Columns(0).Name =   "TIPO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   2672
            _ExtentY        =   503
            _StockProps     =   93
            ForeColor       =   -2147483630
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.CommandButton cmdImprimir 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroApliDet.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   780
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.CommandButton cmdActualizar 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroApliDet.frx":0D37
         Style           =   1  'Graphical
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   450
         Width           =   315
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgRes 
         Height          =   2235
         Left            =   420
         TabIndex        =   1
         Top             =   120
         Width           =   7875
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   5
         stylesets.count =   2
         stylesets(0).Name=   "Red"
         stylesets(0).BackColor=   4744445
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmInfAhorroApliDet.frx":0DC2
         stylesets(1).Name=   "Green"
         stylesets(1).BackColor=   10409634
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmInfAhorroApliDet.frx":0DDE
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2064
         Columns(0).Caption=   "Mes"
         Columns(0).Name =   "FEC"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   3413
         Columns(1).Caption=   "Consumido"
         Columns(1).Name =   "PRES"
         Columns(1).Alignment=   1
         Columns(1).CaptionAlignment=   2
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).NumberFormat=   "Standard"
         Columns(1).FieldLen=   256
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   15400959
         Columns(2).Width=   2963
         Columns(2).Caption=   "Adjudicado"
         Columns(2).Name =   "ADJ"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   2
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "Standard"
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   15400959
         Columns(3).Width=   2831
         Columns(3).Caption=   "Ahorro"
         Columns(3).Name =   "AHO"
         Columns(3).Alignment=   1
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "Standard"
         Columns(3).FieldLen=   256
         Columns(4).Width=   1667
         Columns(4).Caption=   "%"
         Columns(4).Name =   "PORCEN"
         Columns(4).Alignment=   1
         Columns(4).CaptionAlignment=   2
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "0.0#\%"
         Columns(4).FieldLen=   256
         _ExtentX        =   13891
         _ExtentY        =   3942
         _StockProps     =   79
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSChart20Lib.MSChart MSChart1 
         Height          =   1995
         Left            =   420
         OleObjectBlob   =   "frmInfAhorroApliDet.frx":0DFA
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   360
         Width           =   7875
      End
      Begin VB.CommandButton cmdGrafico 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroApliDet.frx":2820
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   120
         Width           =   315
      End
      Begin VB.CommandButton cmdGrid 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         Picture         =   "frmInfAhorroApliDet.frx":2B62
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   315
      End
   End
End
Attribute VB_Name = "frmInfAhorroApliDet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private sIdiPlanta As String
Private sIdiTipoGrafico(6) As String


Private Sub cmdActualizar_Click()
    If cmdGrid.Visible Then
        MostrarGrafico sdbcTipoGrafico.value
    End If
End Sub


Private Sub Form_Load()
    
    Me.Height = 3090
    Me.Width = 8835
    
    CargarRecursos
    PonerFieldSeparator Me
End Sub

Private Sub cmdGrafico_Click()
        
        If sdbgRes.Rows = 0 Then
            Exit Sub
        End If
                
        Screen.MousePointer = vbHourglass
        picTipoGrafico.Visible = True
'        MostrarGrafico "Barras 3D"
        sdbcTipoGrafico.value = sIdiTipoGrafico(2)
        MostrarGrafico sIdiTipoGrafico(2)
        cmdGrafico.Visible = False
        cmdGrid.Visible = True
        sdbgRes.Visible = False
        MSChart1.Visible = True
        Screen.MousePointer = vbNormal

End Sub

Private Sub cmdGrid_Click()
        picTipoGrafico.Visible = False
        cmdGrafico.Visible = True
        cmdGrid.Visible = False
        sdbgRes.Visible = True
        MSChart1.Visible = False
End Sub

Private Sub Form_Resize()
    
    If Me.Height > 1200 Then
        Picture1.Height = Me.Height - 555
        sdbgRes.Height = Picture1.Height - 300
        MSChart1.Height = sdbgRes.Height - 300
    End If
    
    If Me.Width > 600 Then
        Picture1.Width = Me.Width - 360
        sdbgRes.Width = Picture1.Width - 600
        MSChart1.Width = sdbgRes.Width
    End If
    
    sdbgRes.Columns(0).Width = sdbgRes.Width * 0.1
    sdbgRes.Columns(1).Width = sdbgRes.Width * 0.25
    sdbgRes.Columns(2).Width = sdbgRes.Width * 0.25
    sdbgRes.Columns(3).Width = sdbgRes.Width * 0.25
    sdbgRes.Columns(4).Width = sdbgRes.Width * 0.15 - 500
        
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Me.Visible = False
End Sub

Private Sub sdbcTipoGrafico_Click()
    MostrarGrafico sdbcTipoGrafico.value
End Sub

Private Sub sdbgRes_InitColumnProps()
    
    sdbgRes.ScrollBars = ssScrollBarsVertical
    
End Sub

Private Sub sdbgRes_RowLoaded(ByVal Bookmark As Variant)
    If sdbgRes.Columns("AHO").value < 0 Then
        sdbgRes.Columns("AHO").CellStyleSet "Red"
        sdbgRes.Columns("PORCEN").CellStyleSet "Red"
    Else
        If sdbgRes.Columns("AHO").value > 0 Then
            sdbgRes.Columns("AHO").CellStyleSet "Green"
            sdbgRes.Columns("PORCEN").CellStyleSet "Green"
        End If
    End If
End Sub

Private Sub sdbcTipoGrafico_CloseUp()
    MostrarGrafico sdbcTipoGrafico.value
End Sub

Private Sub sdbcTipoGrafico_InitColumnProps()
    
    sdbcTipoGrafico.DataFieldList = "Column 0"
    sdbcTipoGrafico.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub MostrarGrafico(ByVal Tipo As String)
Dim lbl As MSChart20Lib.Label
Dim ar() As Variant
Dim i As Integer
    
    If sdbgRes.Rows = 0 Then
        cmdGrid_Click
        Exit Sub
    End If
    
    MSChart1.Visible = True
    Select Case Tipo
    
'        Case "Barras 2D", "Barras 3D"
        Case sIdiTipoGrafico(1), sIdiTipoGrafico(2)
                
                'Necesitamos cinco series
                ' Ahorro negativo
                ' Ahorro positivo
                ' Adjudicado
                ' Presupuestado
                'Adjudicado
                
                    
                ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                
                sdbgRes.MoveFirst
                While i <= sdbgRes.Rows
                        
                        ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").value) > CDbl(sdbgRes.Columns("ADJ").value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value) - CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
'                If Tipo = "Barras 3D" Then
                If Tipo = sIdiTipoGrafico(2) Then
                    
                    MSChart1.chartType = VtChChartType3dBar
                    MSChart1.SeriesType = VtChSeriesType3dBar
                Else
                    
                    MSChart1.chartType = VtChChartType2dBar
                    MSChart1.SeriesType = VtChSeriesType2dBar
                
                End If
                
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.Backdrop.Shadow.Style = VtShadowStyleDrop
                MSChart1.Legend.Backdrop.Frame.Style = VtFrameStyleDoubleLine
                    
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.VtFont.Size = 12
                    lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                    
'        Case "Lineas 2D", "Lineas 3D"
        Case sIdiTipoGrafico(3), sIdiTipoGrafico(4)
                
                'Necesitamos tres series
                ' Adjudicado
                ' Presupuesto
                ' Ahorro
                
                
'                If Tipo = "Lineas 3D" Then
                If Tipo = sIdiTipoGrafico(4) Then
                    MSChart1.chartType = VtChChartType3dLine
                    MSChart1.SeriesType = VtChSeriesType3dLine
                    MSChart1.Stacking = False
                Else
                    MSChart1.chartType = VtChChartType2dLine
                    MSChart1.SeriesType = VtChSeriesType2dLine
                    MSChart1.Stacking = False
                End If
                
                ReDim ar(1 To sdbgRes.Rows, 1 To 4)
                
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
                    
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                    ar(i, 2) = CDbl(sdbgRes.Columns("ADJ").value)
                    ar(i, 3) = CDbl(sdbgRes.Columns("PRES").value)
                    ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value)
                    
                    i = i + 1
                    sdbgRes.MoveNext
                Wend
                
                MSChart1.ChartData = ar
                
                MSChart1.ShowLegend = False
                
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Presupuestado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Ahorrado
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
'        Case "Tarta"
        Case sIdiTipoGrafico(5)
            
                'Necesitamos cuatro series
                ' Adjudicado positivo +
                ' Presupuesto
                ' Ahorro positivo
                ' Ahorro negativo
                
                  ReDim ar(1 To sdbgRes.Rows, 1 To 7)
                i = 1
                
                sdbgRes.MoveFirst
                
                While i <= sdbgRes.Rows
               
                    ar(i, 1) = sdbgRes.Columns(0).Text & " "
                        'Si ahorro +
                        If CDbl(sdbgRes.Columns("AHO").value) > 0 Then
                            If CDbl(sdbgRes.Columns("AHO").value) > CDbl(sdbgRes.Columns("ADJ").value) Then
                                ar(i, 2) = Null
                                ar(i, 3) = CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value) - CDbl(sdbgRes.Columns("ADJ").value)
                                ar(i, 5) = Null
                                ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            Else
                                ar(i, 2) = Null
                                ar(i, 3) = Null
                                ar(i, 4) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 5) = CDbl(sdbgRes.Columns("ADJ").value) - CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 6) = CDbl(sdbgRes.Columns("AHO").value)
                                ar(i, 7) = Null
                            End If
                        Else
                        'Si ahorro-
                            ar(i, 2) = CDbl(sdbgRes.Columns("AHO").value)
                            ar(i, 3) = Null
                            ar(i, 4) = Null
                            ar(i, 5) = Null
                            ar(i, 6) = CDbl(sdbgRes.Columns("PRES").value)
                            ar(i, 7) = -CDbl(sdbgRes.Columns("AHO").value)
                        End If
                    
                        i = i + 1
                        sdbgRes.MoveNext
                Wend
                
                    
                MSChart1.chartType = VtChChartType2dPie
                MSChart1.SeriesType = VtChSeriesType2dPie
                MSChart1.ChartData = ar
                MSChart1.ShowLegend = False
                MSChart1.Stacking = True
                MSChart1.Plot.View3d.Rotation = 60
                MSChart1.Legend.VtFont.Size = 8.25
                'Ahorro negativo
                MSChart1.Plot.SeriesCollection.Item(1).DataPoints.Item(-1).Brush.FillColor.Set 255, 0, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(2).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Ahorro positivo
                MSChart1.Plot.SeriesCollection.Item(3).DataPoints.Item(-1).Brush.FillColor.Set 0, 255, 0
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(4).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                'Prespuestado
                MSChart1.Plot.SeriesCollection.Item(5).DataPoints.Item(-1).Brush.FillColor.Set 178, 4, 70
                'Adjudicado
                MSChart1.Plot.SeriesCollection.Item(6).DataPoints.Item(-1).Brush.FillColor.Set 9, 44, 115
                 
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdX).Labels
                    
                lbl.VtFont.Style = VtFontStyleBold
                lbl.VtFont.Size = 12
                lbl.TextLayout.Orientation = VtOrientationHorizontal
                
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
                For Each lbl In MSChart1.Plot.Axis(VtChAxisIdY2).Labels
                    lbl.VtFont.Size = 12
                    lbl.VtFont.Style = VtFontStyleBold
                    lbl.Format = "#.00#"
                Next
                
    End Select
            
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_INFAHORRO_APLIDET, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).value
        Ador.MoveNext
        
        
        sdbgRes.Columns(0).caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(1).caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(2).caption = Ador(0).value
        Ador.MoveNext
        sdbgRes.Columns(3).caption = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.RemoveAll
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(1) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(2) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(3) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(4) = Ador(0).value
        Ador.MoveNext
        sdbcTipoGrafico.AddItem Ador(0).value
        sIdiTipoGrafico(5) = Ador(0).value
        Ador.MoveNext
        sIdiPlanta = Ador(0).value
        sIdiTipoGrafico(6) = Ador(0).value
        Ador.Close
    
    End If

    Set Ador = Nothing

End Sub
