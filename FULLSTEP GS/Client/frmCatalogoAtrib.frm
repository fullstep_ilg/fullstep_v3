VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmCatalogoAtrib 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   7650
   ClientLeft      =   1290
   ClientTop       =   2775
   ClientWidth     =   12480
   Icon            =   "frmCatalogoAtrib.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7650
   ScaleWidth      =   12480
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   360
      Left            =   1440
      TabIndex        =   23
      Top             =   6960
      Width           =   3015
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmCatalogoAtrib.frx":0CB2
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "VALOR"
      Columns(0).Name =   "VALOR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "DESC"
      Columns(1).Name =   "DESC"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5318
      _ExtentY        =   635
      _StockProps     =   77
      BackColor       =   -2147483633
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   2500
      Index           =   2
      Left            =   6720
      TabIndex        =   18
      Top             =   5040
      Width           =   5655
      Begin VB.CommandButton cmdPasarTodosALinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   2
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":0CCE
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   1365
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarALinea 
         Height          =   315
         Index           =   2
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":1027
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   945
         Width           =   495
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCamposEsp 
         Height          =   1860
         Index           =   2
         Left            =   720
         TabIndex        =   22
         Top             =   480
         Width           =   4860
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   9
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoAtrib.frx":1380
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogoAtrib.frx":139C
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   -2147483630
         ForeColorOdd    =   -2147483630
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   2090
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   4445
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   900
         Columns(2).Caption=   "Valor"
         Columns(2).Name =   "VAL"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16776960
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "TIPO_INTRODUCCION"
         Columns(3).Name =   "TIPO_INTRODUCCION"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "TIPO_DATOS"
         Columns(4).Name =   "TIPO_DATOS"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "MIN"
         Columns(5).Name =   "MIN"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "MAX"
         Columns(6).Name =   "MAX"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "ATRIBID"
         Columns(7).Name =   "ATRIBID"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   8572
         _ExtentY        =   3281
         _StockProps     =   79
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtribEsp 
         BackColor       =   &H00808000&
         Caption         =   "DCampos de especificaci�n del proceso"
         ForeColor       =   &H8000000E&
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   19
         Top             =   240
         Width           =   5400
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   2500
      Index           =   1
      Left            =   6720
      TabIndex        =   11
      Top             =   2520
      Width           =   5655
      Begin VB.CommandButton cmdPasarTodosALinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":13B8
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   1830
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarTodosDesdeLinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":1711
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1365
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarALinea 
         Height          =   315
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":1A6B
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   945
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarDesdeLinea 
         Height          =   300
         Index           =   1
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":1DC4
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   600
         Width           =   495
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCamposEsp 
         Height          =   1860
         Index           =   1
         Left            =   720
         TabIndex        =   17
         Top             =   480
         Width           =   4860
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   9
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoAtrib.frx":211E
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogoAtrib.frx":213A
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   -2147483630
         ForeColorOdd    =   -2147483630
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   2090
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   4445
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   900
         Columns(2).Caption=   "Valor"
         Columns(2).Name =   "VAL"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16776960
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "TIPO_INTRODUCCION"
         Columns(3).Name =   "TIPO_INTRODUCCION"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "TIPO_DATOS"
         Columns(4).Name =   "TIPO_DATOS"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "MIN"
         Columns(5).Name =   "MIN"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "MAX"
         Columns(6).Name =   "MAX"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "ATRIBID"
         Columns(7).Name =   "ATRIBID"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   8572
         _ExtentY        =   3281
         _StockProps     =   79
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtribEsp 
         BackColor       =   &H00808000&
         Caption         =   "DCampos de especificaci�n del proveedor / art�culo"
         ForeColor       =   &H8000000E&
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   5400
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   7550
      Index           =   3
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6615
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "DEliminar"
         Height          =   375
         Left            =   5040
         TabIndex        =   3
         Top             =   7020
         Width           =   1335
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCamposEsp 
         Height          =   6420
         Index           =   3
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   6420
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   11
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoAtrib.frx":2156
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogoAtrib.frx":2172
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   -2147483630
         ForeColorOdd    =   -2147483630
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns.Count   =   11
         Columns(0).Width=   2090
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   4445
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   900
         Columns(2).Caption=   "Valor"
         Columns(2).Name =   "VAL"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   900
         Columns(3).Caption=   "DOrigen"
         Columns(3).Name =   "ORI"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   16776960
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "TIPO_INTRODUCCION"
         Columns(4).Name =   "TIPO_INTRODUCCION"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "TIPO_DATOS"
         Columns(5).Name =   "TIPO_DATOS"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "MIN"
         Columns(6).Name =   "MIN"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "MAX"
         Columns(7).Name =   "MAX"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ATRIBID"
         Columns(8).Name =   "ATRIBID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "ID"
         Columns(9).Name =   "ID"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "ORICOD"
         Columns(10).Name=   "ORICOD"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   2
         Columns(10).FieldLen=   256
         _ExtentX        =   11324
         _ExtentY        =   11324
         _StockProps     =   79
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtribEsp 
         BackColor       =   &H00808000&
         Caption         =   "DCampos de especificaci�n de la l�nea del cat�logo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   6375
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      Height          =   2500
      Index           =   0
      Left            =   6720
      TabIndex        =   4
      Top             =   0
      Width           =   5655
      Begin VB.CommandButton cmdPasarTodosALinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":218E
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   1830
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarTodosDesdeLinea 
         Caption         =   "ALL"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":24E7
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   1365
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarALinea 
         Height          =   315
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":2841
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   945
         Width           =   495
      End
      Begin VB.CommandButton cmdPasarDesdeLinea 
         Height          =   300
         Index           =   0
         Left            =   120
         Picture         =   "frmCatalogoAtrib.frx":2B9A
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   600
         Width           =   495
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgCamposEsp 
         Height          =   1860
         Index           =   0
         Left            =   720
         TabIndex        =   10
         Top             =   480
         Width           =   4860
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   9
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmCatalogoAtrib.frx":2EF4
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmCatalogoAtrib.frx":2F10
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   -2147483630
         ForeColorOdd    =   -2147483630
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         Columns.Count   =   9
         Columns(0).Width=   2090
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16776960
         Columns(1).Width=   4445
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16776960
         Columns(2).Width=   900
         Columns(2).Caption=   "Valor"
         Columns(2).Name =   "VAL"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   16776960
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "TIPO_INTRODUCCION"
         Columns(3).Name =   "TIPO_INTRODUCCION"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "TIPO_DATOS"
         Columns(4).Name =   "TIPO_DATOS"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "MIN"
         Columns(5).Name =   "MIN"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "MAX"
         Columns(6).Name =   "MAX"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "ATRIBID"
         Columns(7).Name =   "ATRIBID"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "ID"
         Columns(8).Name =   "ID"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   8572
         _ExtentY        =   3281
         _StockProps     =   79
         BackColor       =   -2147483643
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblAtribEsp 
         BackColor       =   &H00808000&
         Caption         =   "DCampos de especificaci�n del maestro de art�culos"
         ForeColor       =   &H8000000E&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   5400
      End
   End
End
Attribute VB_Name = "frmCatalogoAtrib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public g_oLinea         As CLineaCatalogo
Public g_oArticulo      As CArticulo
Public g_oArticuloProve As CArticulo
Public g_oProveedor     As CProveedor
Public g_oIBaseDatos    As IBaseDatos

Private m_oArticulos    As CArticulos
Private m_oEsp          As CEspecificacion

Public g_bCancelarEsp   As Boolean
Public g_sComentario    As String
Public g_bRespetarCombo As Boolean
Public g_bSoloRuta      As Boolean

'Restricciones
Private m_bModifArt      As Boolean
Private m_bRMatArt       As Boolean
Private m_bModifArtProve As Boolean
Private m_bRMatArtProve  As Boolean
Private m_bModifLinea    As Boolean

Private sayFileNames()   As String
Private Accion           As accionessummit
'Idiomas
Private m_sIdiArticulo      As String
Private m_sIdiProveedor     As String
Private m_sIdiDialogTitle   As String
Private m_sIdiAllFiles      As String
Private m_sIdiArchivo       As String
Private m_sIdiGuardarEsp    As String
Private m_sIdiTipoOrig      As String
Private m_sIdiNombre        As String
Private m_sIdiProveedorArt  As String
Private m_sIdiProceso       As String
Private m_sCaption          As String
Private m_oAtributosLinea   As CAtributos
Private m_oAtributosArt     As CAtributos
Private m_oAtributosProveArt As CAtributos
Private m_oAtributosProce   As CAtributos
Private g_oAtributoEnEdicion As CAtributo
Private sCamposNoSeleccionados As String
Private sConfirmEliminarCampos As String
Private m_sMaestroAtributos As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String

''' <summary>Carga del formulario</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub Form_Load()

    If g_oLinea Is Nothing Then Unload Me
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarAtrib
    
    If g_oLinea.ArtCod_Interno = "" Then
        Me.caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & _
             " / " & m_sIdiArticulo & ": " & g_oLinea.ArtDen
    Else
        Me.caption = m_sCaption & " - " & m_sIdiProveedor & ": " & g_oProveedor.Cod & _
            " / " & m_sIdiArticulo & ": " & g_oLinea.ArtCod_Interno
    End If
    
    ConfigurarSeguridad
    
End Sub

''' <summary>Establece las dimensiones y posici�n de los elementos del formulario</summary>
''' <remarks>Llamada desde evento</remarks>
Private Sub Form_Resize()
    Dim hAux As Integer
    Dim iSeparacionEntreControles As Integer
    Dim numControles As Integer
    iSeparacionEntreControles = 100
    Dim bLineaProcedeAdjudicacion As Boolean
    bLineaProcedeAdjudicacion = LineaProcedeAdjudicacion()
    If bLineaProcedeAdjudicacion Then
        numControles = 3
    Else
        numControles = 2
    End If
    Frame1(3).Height = Me.Height - 600
    hAux = ((Frame1(3).Height - (iSeparacionEntreControles * (numControles - 1))) / numControles)
    Frame1(0).Height = hAux
    Frame1(1).Top = Frame1(0).Height + Frame1(0).Top + iSeparacionEntreControles
    Frame1(1).Height = hAux
    Frame1(2).Top = Frame1(1).Height + Frame1(1).Top + iSeparacionEntreControles
    Frame1(2).Height = hAux
    
    Frame1(3).Width = (Me.Width - 500) * 0.55
    Frame1(0).Width = (Me.Width - 500) * 0.45
    Frame1(1).Width = Frame1(0).Width
    Frame1(2).Width = Frame1(0).Width
    Frame1(0).Left = Frame1(3).Left + Frame1(3).Width + iSeparacionEntreControles
    Frame1(1).Left = Frame1(0).Left
    Frame1(2).Left = Frame1(0).Left

    If Not bLineaProcedeAdjudicacion Then
        Frame1(2).Visible = False
    Else
        Frame1(2).Visible = True
    End If
    
    If g_oLinea.ArtCod_Interno = "" Then
        Frame1(0).Visible = False
        Frame1(1).Visible = False
        Frame1(2).Visible = False
        Frame1(3).Width = Me.Width - 250
    End If

    sdbgCamposEsp(3).Width = Frame1(3).Width - 250
    sdbgCamposEsp(3).Height = Frame1(3).Height - lblAtribEsp(3).Height - cmdEliminar.Height - (iSeparacionEntreControles * 4)
    cmdEliminar.Left = sdbgCamposEsp(3).Left + sdbgCamposEsp(3).Width - cmdEliminar.Width - iSeparacionEntreControles
    cmdEliminar.Top = sdbgCamposEsp(3).Top + sdbgCamposEsp(3).Height + iSeparacionEntreControles
    
    sdbgCamposEsp(0).Width = Frame1(0).Width - 850
    sdbgCamposEsp(1).Width = Frame1(1).Width - 850
    sdbgCamposEsp(2).Width = Frame1(2).Width - 850
    
    sdbgCamposEsp(0).Height = Frame1(0).Height - lblAtribEsp(0).Height - (iSeparacionEntreControles * 4)
    sdbgCamposEsp(1).Height = Frame1(1).Height - lblAtribEsp(1).Height - (iSeparacionEntreControles * 4)
    sdbgCamposEsp(2).Height = Frame1(2).Height - lblAtribEsp(2).Height - (iSeparacionEntreControles * 4)
    
    lblAtribEsp(0).Width = Frame1(0).Width - (iSeparacionEntreControles * 2)
    lblAtribEsp(1).Width = Frame1(1).Width - (iSeparacionEntreControles * 2)
    lblAtribEsp(2).Width = Frame1(2).Width - (iSeparacionEntreControles * 2)
    lblAtribEsp(3).Width = Frame1(3).Width - (iSeparacionEntreControles * 2)
    
    formatoAtributos sdbgCamposEsp(0)
    formatoAtributos sdbgCamposEsp(1)
    formatoAtributos sdbgCamposEsp(2)
    formatoAtributos sdbgCamposEsp(3)
    
    sdbgCamposEsp(3).Columns("VAL").DropDownHwnd = 0
End Sub

''' <summary>Ancho de las columnas de la grid de campos de especificaci�n (atributos)</summary>
''' <remarks>Llamada desde Load</remarks>
Private Sub formatoAtributos(ByRef oGrid As SSDBGrid)
    With oGrid
        .Columns("COD").Width = (.Width - 570) * 0.15
        .Columns("DEN").Width = (.Width - 570) * 0.4
        If oGrid.Index = 3 Then
            .Columns("VAL").Width = (.Width - 570) * 0.3
            .Columns("ORI").Width = (.Width - 570) * 0.15
        Else
            .Columns("VAL").Width = (.Width - 570) * 0.45
        End If
    End With
End Sub

''' <summary>Indica si la l�nea viene de adjudicaci�n o no</summary>
''' <remarks>Llamada desde el resize</remarks>
Private Function LineaProcedeAdjudicacion() As Boolean
    If IsNull(g_oLinea.Anyo) Or g_oLinea.GMN1Cod = "" Or IsNull(g_oLinea.ProceCod) Then
        LineaProcedeAdjudicacion = False
    Else
        LineaProcedeAdjudicacion = True
    End If

End Function

''' <summary>Carga los textos de pantalla</summary>
''' <remarks>Llamada desde el Load</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_CATALOGO_ATRIB, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
        lblAtribEsp(0).caption = Ador(0).Value '1   Campos de especificaci�n del maestro de art�culos
        Ador.MoveNext
        lblAtribEsp(1).caption = Ador(0).Value '2   Campos de especificaci�n del proveedor / art�culo
        Ador.MoveNext
        lblAtribEsp(2).caption = Ador(0).Value '3   Campos de especificaci�n del proceso
        Ador.MoveNext
        sdbgCamposEsp(0).Columns("COD").caption = Ador(0).Value '4   C�digo
        sdbgCamposEsp(1).Columns("COD").caption = Ador(0).Value '4   C�digo
        sdbgCamposEsp(2).Columns("COD").caption = Ador(0).Value '4   C�digo
        sdbgCamposEsp(3).Columns("COD").caption = Ador(0).Value '4   C�digo
        Ador.MoveNext
        sdbgCamposEsp(0).Columns("DEN").caption = Ador(0).Value '5   Denominaci�n
        sdbgCamposEsp(1).Columns("DEN").caption = Ador(0).Value '5   Denominaci�n
        sdbgCamposEsp(2).Columns("DEN").caption = Ador(0).Value '5   Denominaci�n
        sdbgCamposEsp(3).Columns("DEN").caption = Ador(0).Value '5   Denominaci�n
        Ador.MoveNext
        sdbgCamposEsp(0).Columns("VAL").caption = Ador(0).Value '6   valor
        sdbgCamposEsp(1).Columns("VAL").caption = Ador(0).Value '6   valor
        sdbgCamposEsp(2).Columns("VAL").caption = Ador(0).Value '6   valor
        sdbgCamposEsp(3).Columns("VAL").caption = Ador(0).Value '6   valor
        Ador.MoveNext
        sdbgCamposEsp(0).Columns("ORI").caption = Ador(0).Value '7   Origen
        sdbgCamposEsp(1).Columns("ORI").caption = Ador(0).Value '7   Origen
        sdbgCamposEsp(2).Columns("ORI").caption = Ador(0).Value '7   Origen
        sdbgCamposEsp(3).Columns("ORI").caption = Ador(0).Value '7   Origen
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value '8   Eliminar
        Ador.MoveNext
        lblAtribEsp(3).caption = Ador(0).Value '9   Campos de especificaci�n de la l�nea del cat�logo
        m_sCaption = Ador(0).Value '9   Campos de especificaci�n de la l�nea del cat�logo
        Ador.MoveNext
        m_sIdiArticulo = Ador(0).Value '10  Art�culo
        Ador.MoveNext
        m_sIdiProveedor = Ador(0).Value '11  Proveedor
        Ador.MoveNext
        m_sIdiProveedorArt = Ador(0).Value '12  Proveedor/Art.
        Ador.MoveNext
        m_sIdiProceso = Ador(0).Value '13  Proceso
        Ador.MoveNext
        sCamposNoSeleccionados = Ador(0).Value '14 No ha seleccionado ning�n registro
        Ador.MoveNext
        sConfirmEliminarCampos = Ador(0).Value '15 �Desea eliminar los campos de especificaci�n seleccionados?
        Ador.MoveNext
        m_sMaestroAtributos = Ador(0).Value '16 Maestro Atrib.
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value  '17 S�
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value '18 No
        Ador.Close
    End If

    Set Ador = Nothing
    
End Sub

''' <summary>Carga datos</summary>
''' <remarks>Llamada desde el Load</remarks>
Private Sub CargarAtrib()
    Dim oProves As CProveedores
    Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
    Set m_oAtributosArt = oFSGSRaiz.Generar_CAtributos
    Set m_oAtributosProveArt = oFSGSRaiz.Generar_CAtributos
    Set m_oAtributosProce = oFSGSRaiz.Generar_CAtributos

    If g_oLinea Is Nothing Then Unload Me

    Set oProves = oFSGSRaiz.generar_CProveedores
    oProves.CargarTodosLosProveedoresDesde3 1, g_oLinea.ProveCod, , True
    If oProves.Count = 0 Then
        oMensajes.DatoEliminado m_sIdiProveedor
        Set oProves = Nothing
        Set g_oProveedor = Nothing
    End If
    Set g_oProveedor = oProves.Item(1)
    'ATRIBUTOS DE ARTICULO
    Set m_oAtributosArt = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Articulo, g_oProveedor)
    AnyadirEspsALista (AtributosLineaCatalogo.Articulo)
        
    If m_oAtributosArt Is Nothing Then
        cmdPasarALinea(0).Enabled = False
        cmdPasarTodosALinea(0).Enabled = False
    Else
        cmdPasarALinea(0).Enabled = True
        cmdPasarTodosALinea(0).Enabled = True
    End If
    'ATRIBUTOS DE OFERTA DE PROCESO Y ATRIBUTOS DE PROVEEDOR / ART�CULO
    Set m_oAtributosProveArt = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.ProveArtYProceOfer, g_oProveedor)
    AnyadirEspsALista (AtributosLineaCatalogo.ProveArtYProceOfer)
    
    If m_oAtributosProveArt Is Nothing Then
        cmdPasarALinea(1).Enabled = False
        cmdPasarTodosALinea(1).Enabled = False
    Else
        cmdPasarALinea(1).Enabled = True
        cmdPasarTodosALinea(1).Enabled = True
    End If
    'ATRIBUTOS DE ESPECIFICACI�N DE PROCESO
    Set m_oAtributosProce = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.ProceEsp, g_oProveedor)
    AnyadirEspsALista (AtributosLineaCatalogo.ProceEsp)
    
    If m_oAtributosProveArt Is Nothing Then
        cmdPasarALinea(2).Enabled = False
        cmdPasarTodosALinea(2).Enabled = False
    Else
        cmdPasarALinea(2).Enabled = True
        cmdPasarTodosALinea(2).Enabled = True
    End If
    'ATRIBUTOS DE LA L�NEA DE CAT�LOGO
    Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, g_oProveedor)
    AnyadirEspsALista (AtributosLineaCatalogo.Linea)
    
    If m_oAtributosLinea Is Nothing Then
        cmdPasarDesdeLinea(0).Enabled = False
        cmdPasarTodosDesdeLinea(0).Enabled = False
        cmdPasarDesdeLinea(1).Enabled = False
        cmdPasarTodosDesdeLinea(1).Enabled = False
    Else
        cmdPasarDesdeLinea(0).Enabled = True
        cmdPasarTodosDesdeLinea(0).Enabled = True
        cmdPasarDesdeLinea(1).Enabled = True
        cmdPasarTodosDesdeLinea(1).Enabled = True
    End If
    
    If g_oProveedor Is Nothing Then
        oMensajes.NoValido m_sIdiProveedor
        Unload Me
    End If

End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Public Sub ConfigurarSeguridad()
    Dim oIMaterialAsignado As IMaterialAsignado
    Dim oArticulos As CArticulos
    Dim oArt As CArticulo
    
    m_bModifArt = True
    m_bModifArtProve = True
    m_bModifLinea = True
    
    'Articulo
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
        m_bModifArt = False
    Else
        If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestMatComp)) Is Nothing) Then
            'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material, sino no puede modificarlo
            If g_oLinea.ArtCod_Interno <> "" Then
                Set oIMaterialAsignado = oUsuarioSummit.comprador
                Set oArticulos = oIMaterialAsignado.DevolverArticulos(g_oArticulo.Cod, , True)
                m_bModifArt = False
                m_bRMatArt = True
                For Each oArt In oArticulos
                    If oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod And _
                        oArt.GMN1Cod = g_oArticulo.GMN1Cod And oArt.GMN1Cod = g_oArticulo.GMN1Cod Then
                        m_bModifArt = True
                        m_bRMatArt = False
                        Exit For
                    End If
                Next
            Else
                m_bModifArt = False
                m_bRMatArt = True
            End If
        End If
    End If
    'Articulo/proveedor
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtGestion)) Is Nothing) Then
        m_bModifArtProve = False
    Else
        If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtRestMAtComp)) Is Nothing) Then
            If g_oLinea.ArtCod_Interno <> "" Then
                m_bModifArtProve = False
                m_bRMatArtProve = True
                'El usu tiene restricci�n de material, miramos si el articulo pertenece a su material/prove
                Set oIMaterialAsignado = g_oProveedor
                Set oIMaterialAsignado.ARTICULOS = oFSGSRaiz.Generar_CArticulos
                oIMaterialAsignado.ARTICULOS.Add g_oArticulo.GMN1Cod, g_oArticulo.GMN2Cod, g_oArticulo.GMN3Cod, g_oArticulo.GMN4Cod, g_oArticulo.Cod, g_oArticulo.Den, , , 1
                Set oArticulos = oIMaterialAsignado.DevolverArticulos(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True, False, False)
                If oArticulos.Count > 0 Then m_bModifArtProve = True: m_bRMatArtProve = False
            End If
        End If
    End If
    
    'Linea
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGAdjModificar)) Is Nothing) Then
        m_bModifLinea = False
    End If

    If Not m_bModifArt Then
        cmdPasarDesdeLinea(0).Visible = False
        cmdPasarTodosDesdeLinea(0).Visible = False
    End If
    
    If Not m_bModifArtProve Then
        cmdPasarDesdeLinea(1).Visible = False
        cmdPasarTodosDesdeLinea(1).Visible = False
    End If
    
    If Not m_bModifLinea Then
        cmdEliminar.Visible = False
    End If
End Sub

''' <summary>Carga las grids</summary>
''' <remarks>Llamada desde CargarAtrib</remarks>
Public Sub AnyadirEspsALista(Index As AtributosLineaCatalogo)
Dim oatrib As CAtributo

    Screen.MousePointer = vbHourglass
Select Case Index
Case AtributosLineaCatalogo.Articulo
    sdbgCamposEsp(0).RemoveAll
    If Not m_oAtributosArt Is Nothing Then
        For Each oatrib In m_oAtributosArt
            sdbgCamposEsp(0).AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Id
        Next
    End If
Case AtributosLineaCatalogo.ProveArtYProceOfer
    sdbgCamposEsp(1).RemoveAll
    If Not m_oAtributosProveArt Is Nothing Then
        For Each oatrib In m_oAtributosProveArt
            sdbgCamposEsp(1).AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Id
        Next
    End If
Case AtributosLineaCatalogo.ProceEsp
    sdbgCamposEsp(2).RemoveAll
    If Not m_oAtributosProce Is Nothing Then
        For Each oatrib In m_oAtributosProce
            sdbgCamposEsp(2).AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Id
        Next
    End If
Case AtributosLineaCatalogo.Linea
    sdbgCamposEsp(3).RemoveAll
    'frmCatalogoAtribEspYAdj.sdbgCamposEsp.RemoveAll
    If Not m_oAtributosLinea Is Nothing Then
    For Each oatrib In m_oAtributosLinea
        Dim sOrigen As String
        sOrigen = ""
        Select Case oatrib.Origen
            Case AtributosLineaCatalogo.Articulo:
                sOrigen = m_sIdiArticulo
            Case AtributosLineaCatalogo.ProveArtYProceOfer:
                sOrigen = m_sIdiProveedorArt
            Case AtributosLineaCatalogo.ProceEsp:
                sOrigen = m_sIdiProceso
            Case AtributosLineaCatalogo.MaestroAtributos:
                sOrigen = m_sMaestroAtributos
        End Select
        sdbgCamposEsp(3).AddItem oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.valor & Chr(m_lSeparador) & sOrigen & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Origen
    Next
    End If
End Select
Screen.MousePointer = vbNormal
End Sub

''' <summary>Elimina los campos de especificaci�n (atributos) seleccionados</summary>
''' <remarks>Llamada desde Evento</remarks>
Private Sub cmdEliminar_Click()
    Screen.MousePointer = vbHourglass
    Dim inum As Integer
    Dim v As Variant
    Dim oAtributos As CAtributos
    Dim tsError As TipoErrorSummit
    Dim i As Integer
    
    sdbgCamposEsp(3).Columns("VAL").DropDownHwnd = 0
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    With sdbgCamposEsp(3)
        If .SelBookmarks.Count > 0 Then
            i = oMensajes.PreguntaEliminar(sConfirmEliminarCampos)
            If i = vbYes Then
                inum = 0
                While inum < .SelBookmarks.Count
                    Set g_oAtributoEnEdicion = m_oAtributosLinea.Item(CStr(.Columns("ID").Value))
                    '1.ELIMINAR DEL GRID --> Con deleteSelected es m�s sencillo
                    .Bookmark = .SelBookmarks(inum)
                    oAtributos.Add .Columns("ID").Value, .Columns("COD").Text, .Columns("DEN").Text, .Columns("TIPO_DATOS").Value
                inum = inum + 1
                Wend
                '2.ELIMINAR DEL GRID
                .DeleteSelected
                .Update
                .Refresh
                '3. ELIMINAR DE BDD
                tsError = g_oLinea.EliminarAtributos(oAtributos)
                '4. RECARGAR LA VARIABLE DE LOS ATRIBUTOS
                Set m_oAtributosLinea = Nothing
                Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
                Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, g_oProveedor)
            Else
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox sCamposNoSeleccionados, vbInformation, lblAtribEsp(3).caption
            Exit Sub
        End If
    End With
    'frmCatalogo.g_sOrigen = ""
    sdbgCamposEsp(0).MoveLast
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Pasa a la l�nea los campos de especificaci�n (atributos) seleccionados</summary>
''' <remarks>Llamada desde Evento</remarks>
Private Sub cmdPasarALinea_Click(Index As Integer)
    Dim tsError As TipoErrorSummit
    Select Case Index
        Case 0:
            tsError = copiarAtributosALinea(sdbgCamposEsp(0), AtributosLineaCatalogo.Articulo, False)
        Case 1:
            tsError = copiarAtributosALinea(sdbgCamposEsp(1), AtributosLineaCatalogo.ProveArtYProceOfer, False)
        Case 2:
            tsError = copiarAtributosALinea(sdbgCamposEsp(2), AtributosLineaCatalogo.ProceEsp, False)
    End Select
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

''' <summary>Pasa a la l�nea todos los campos de especificaci�n (atributos)</summary>
''' <remarks>Llamada desde Evento</remarks>
Private Sub cmdPasarTodosALinea_Click(Index As Integer)
    Dim tsError As TipoErrorSummit
    Select Case Index
        Case 0:
            tsError = copiarAtributosALinea(sdbgCamposEsp(0), AtributosLineaCatalogo.Articulo, True)
        Case 1:
            tsError = copiarAtributosALinea(sdbgCamposEsp(1), AtributosLineaCatalogo.ProveArtYProceOfer, True)
        Case 2:
            tsError = copiarAtributosALinea(sdbgCamposEsp(2), AtributosLineaCatalogo.ProceEsp, True)
    End Select
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

''' <summary>Pasa desde la l�nea los campos de especificaci�n (atributos) seleccionados</summary>
''' <remarks>Llamada desde Evento</remarks>
Private Sub cmdPasarDesdeLinea_Click(Index As Integer)
    Dim tsError As TipoErrorSummit
    Select Case Index
        Case 0:
            tsError = copiarAtributosDesdeLinea(sdbgCamposEsp(0), m_oAtributosArt, AtributosLineaCatalogo.Articulo, False)
        Case 1:
            tsError = copiarAtributosDesdeLinea(sdbgCamposEsp(1), m_oAtributosProveArt, AtributosLineaCatalogo.ProveArtYProceOfer, False)
    End Select
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

''' <summary>Pasa desde la l�nea todos los campos de especificaci�n (atributos)</summary>
''' <remarks>Llamada desde Evento</remarks>
Private Sub cmdPasarTodosDesdeLinea_Click(Index As Integer)
    Dim tsError As TipoErrorSummit
    Select Case Index
        Case 0:
            tsError = copiarAtributosDesdeLinea(sdbgCamposEsp(0), m_oAtributosArt, AtributosLineaCatalogo.Articulo, True)
        Case 1:
            tsError = copiarAtributosDesdeLinea(sdbgCamposEsp(1), m_oAtributosProveArt, AtributosLineaCatalogo.ProveArtYProceOfer, True)
    End Select
    
    'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
    If tsError.NumError <> TESnoerror Then
        TratarError tsError
    End If
End Sub

''' Copiar los atributos de la l�nea a articulo o prove/art
''' grid: grid A LA que copiamos los datos DESDE LA l�nea
''' tipoAtributo: El tipo de atributo en el que a�adimos los registros de l�nea
Private Function copiarAtributosDesdeLinea(ByRef Grid As SSDBGrid, ByRef oatrib As CAtributos, ByVal tipoAtributo As AtributosLineaCatalogo, ByVal copiarTodos As Boolean) As TipoErrorSummit
    Dim inum As Integer
    Dim v As Variant
    Dim oAtributos As CAtributos
    Dim tsError As TipoErrorSummit
    Dim inum2 As Integer
    Dim vbm As Variant
    Dim i As Integer
    
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    If Not copiarTodos Then
        If sdbgCamposEsp(3).SelBookmarks.Count = 0 Then
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox sCamposNoSeleccionados, vbInformation, lblAtribEsp(3).caption
            Exit Function
        End If
    Else
                sdbgCamposEsp(3).SelBookmarks.RemoveAll
                For i = 0 To sdbgCamposEsp(3).Rows - 1
                    vbm = sdbgCamposEsp(3).AddItemBookmark(i)
                    sdbgCamposEsp(3).SelBookmarks.Add vbm
                Next i
    End If
    If sdbgCamposEsp(3).SelBookmarks.Count > 0 Then
        inum = 0
        With Grid
            If copiarTodos Then
                Set oAtributos = m_oAtributosLinea
            Else
                While inum < sdbgCamposEsp(3).SelBookmarks.Count
                    Set g_oAtributoEnEdicion = m_oAtributosLinea.Item(CStr(sdbgCamposEsp(3).Columns("ID").Value))
                    sdbgCamposEsp(3).Bookmark = sdbgCamposEsp(3).SelBookmarks(inum)
                    If .SelBookmarks.Count > 0 Then
                        For inum2 = 0 To .Rows - 1
                            vbm = .AddItemBookmark(inum2)
                            If g_oAtributoEnEdicion.Atrib = .Columns("ID").CellValue(vbm) Then
                                GoTo Siguiente
                            End If
                        Next inum2
                    End If
                    '1. CREAR VARIABLE DE ATRIBUTOS DE ART�CULO, PROVE/ART O PROCE
                    Dim valorText As Variant
                    Dim valorNum As Variant
                    Dim valorFec As Variant
                    Dim valorBool As Variant
                    valorText = Null
                    valorNum = Null
                    valorFec = Null
                    valorBool = Null
                    Select Case sdbgCamposEsp(3).Columns("TIPO_DATOS").Value
                        Case TiposDeAtributos.TipoString
                            valorText = sdbgCamposEsp(3).Columns("VALOR").Value
                        Case TiposDeAtributos.TipoNumerico
                            valorNum = sdbgCamposEsp(3).Columns("VALOR").Value
                        Case TiposDeAtributos.TipoFecha
                            valorFec = sdbgCamposEsp(3).Columns("VALOR").Value
                        Case TiposDeAtributos.TipoBoolean
                            valorBool = sdbgCamposEsp(3).Columns("VALOR").Value
                    End Select
                    
                    oAtributos.Add 0, sdbgCamposEsp(3).Columns("COD").Value, sdbgCamposEsp(3).Columns("DEN").Value, sdbgCamposEsp(3).Columns("TIPO_DATOS").Value, _
                                        , , , , , , , inum, sdbgCamposEsp(3).Columns("VAL").Value, , , , , , , sdbgCamposEsp(3).Columns("TIPO_INTRODUCCION").Value, _
                                        sdbgCamposEsp(3).Columns("MIN").Value, sdbgCamposEsp(3).Columns("MAX").Value, , , , , , , , , , , , , , , , , _
                                        , , , , valorNum, valorText, valorFec, valorBool, , , , sdbgCamposEsp(3).Columns("ATRIBID").Value
Siguiente:
                    inum = inum + 1
                Wend
            End If
            '2. A�ADIR A BDD
            tsError = g_oLinea.AnyadirAtributos(tipoAtributo, oAtributos)
            If tsError.NumError <> TESnoerror Then
                GoTo Error
            End If
            inum = 0
            Set vbm = Nothing
            While inum < sdbgCamposEsp(3).SelBookmarks.Count
                .AddNew
                Set g_oAtributoEnEdicion = m_oAtributosLinea.Item(CStr(sdbgCamposEsp(3).Columns("ID").Value))
                sdbgCamposEsp(3).Bookmark = sdbgCamposEsp(3).SelBookmarks(inum)
                If .SelBookmarks.Count > 0 Then
                    For inum2 = 0 To .Rows - 1
                        vbm = .AddItemBookmark(inum2)
                        If g_oAtributoEnEdicion.Atrib = .Columns("ID").CellValue(vbm) Then
                            .CancelUpdate
                            GoTo Siguiente2
                        End If
                    Next inum2
                End If
                '3. A�ADIR AL GRID DE ATRIBUTOS DE ART�CULO O PROVE / ART
                .Columns("COD").Text = sdbgCamposEsp(3).Columns("COD").Value
                .Columns("DEN").Value = sdbgCamposEsp(3).Columns("DEN").Value
                .Columns("VAL").Value = sdbgCamposEsp(3).Columns("VAL").Value
                .Columns("VAL").Text = sdbgCamposEsp(3).Columns("VAL").Text
                .Columns("TIPO_INTRODUCCION").Value = sdbgCamposEsp(3).Columns("TIPO_INTRODUCCION").Value
                .Columns("TIPO_DATOS").Value = sdbgCamposEsp(3).Columns("TIPO_DATOS").Value
                .Columns("MIN").Value = sdbgCamposEsp(3).Columns("MIN").Value
                .Columns("MAX").Value = sdbgCamposEsp(3).Columns("MIN").Value
                .Columns("ATRIBID").Value = sdbgCamposEsp(3).Columns("ATRIBID").Value
                .Update
                .Refresh
Siguiente2:
                inum = inum + 1
            Wend
        End With

        '4. RECARGAR LA VARIABLE DE ATRIBUTOS (DE ART�CULO, PROVE/ART O PROCE)
        Set oatrib = Nothing
        Set oatrib = oFSGSRaiz.Generar_CAtributos
        Set oatrib = g_oLinea.DevolverAtributos(tipoAtributo, g_oProveedor)
    End If
    sdbgCamposEsp(3).SelBookmarks.RemoveAll
    Grid.SelBookmarks.RemoveAll
Error:
    If tsError.NumError = TESDatoDuplicado Then
        Select Case tipoAtributo
            Case AtributosLineaCatalogo.Articulo
                tsError.Arg1 = lblAtribEsp(0).caption
            Case AtributosLineaCatalogo.ProveArtYProceOfer
                tsError.Arg1 = lblAtribEsp(1).caption
        End Select
    End If
    copiarAtributosDesdeLinea = tsError
End Function

''' Copiar atributos de art�culo, prove/art o proceso a la l�nea
''' grid: grid DESDE LA que copiamos los datos A LA l�nea
''' tipoAtributo: El tipo de atributo DESDE el que a�adimos los registros
Private Function copiarAtributosALinea(ByRef Grid As SSDBGrid, ByVal tipoAtributo As AtributosLineaCatalogo, ByVal copiarTodos As Boolean) As TipoErrorSummit
    Dim inum As Integer
    Dim v As Variant
    Dim oAtributos As CAtributos
    Dim tsError As TipoErrorSummit
    Dim inum2 As Integer
    Dim vbm As Variant
    Dim i As Integer
    
    Set oAtributos = oFSGSRaiz.Generar_CAtributos
    If Not copiarTodos Then
        If Grid.SelBookmarks.Count = 0 Then
            Screen.MousePointer = vbNormal
            'NO HAY SELECCIONADO NING�N ATRIBUTO
            MsgBox sCamposNoSeleccionados, vbInformation, lblAtribEsp(3).caption
            Exit Function
        End If
    Else
        Grid.SelBookmarks.RemoveAll
        For i = 0 To Grid.Rows - 1
            vbm = Grid.AddItemBookmark(i)
            Grid.SelBookmarks.Add vbm
        Next i
    End If
    If Grid.SelBookmarks.Count > 0 Then
        inum = 0
        With sdbgCamposEsp(3)
            If copiarTodos Then
                Select Case tipoAtributo
                    Case AtributosLineaCatalogo.Articulo
                        Set oAtributos = m_oAtributosArt
                    Case AtributosLineaCatalogo.ProveArtYProceOfer
                        Set oAtributos = m_oAtributosProveArt
                    Case AtributosLineaCatalogo.ProceEsp
                        Set oAtributos = m_oAtributosProce
                End Select
            Else
                While inum < Grid.SelBookmarks.Count
                    Select Case tipoAtributo
                        Case AtributosLineaCatalogo.Articulo:
                            Set g_oAtributoEnEdicion = m_oAtributosArt.Item(CStr(Grid.Columns("ID").Value))
                        Case AtributosLineaCatalogo.ProveArtYProceOfer:
                            Set g_oAtributoEnEdicion = m_oAtributosProveArt.Item(CStr(Grid.Columns("ID").Value))
                        Case AtributosLineaCatalogo.ProceEsp:
                            Set g_oAtributoEnEdicion = m_oAtributosProce.Item(CStr(Grid.Columns("ID").Value))
                    End Select
                    Grid.Bookmark = Grid.SelBookmarks(inum)
                    If .Rows > 0 Then
                        For inum2 = 0 To .Rows - 1
                            vbm = .AddItemBookmark(inum2)
                            If g_oAtributoEnEdicion.Id = CLng(.Columns("ATRIBID").CellValue(vbm)) And CInt(.Columns("ORICOD").CellValue(vbm)) = tipoAtributo Then
                                GoTo Siguiente
                            End If
                        Next inum2
                    End If
                    
                    '1. CREAR VARIABLE DE ATRIBUTOS PARA A�ADIR A BDD DE LINEA
                    Dim valorText As Variant
                    Dim valorNum As Variant
                    Dim valorFec As Variant
                    Dim valorBool As Variant
                    valorText = Null
                    valorNum = Null
                    valorFec = Null
                    valorBool = Null
                    Select Case g_oAtributoEnEdicion.Tipo
                        Case TiposDeAtributos.TipoString
                            valorText = g_oAtributoEnEdicion.valor
                        Case TiposDeAtributos.TipoNumerico
                            valorNum = g_oAtributoEnEdicion.valor
                        Case TiposDeAtributos.TipoFecha
                            valorFec = g_oAtributoEnEdicion.valor
                        Case TiposDeAtributos.TipoBoolean
                            valorBool = g_oAtributoEnEdicion.valor
                    End Select
                                        
                    oAtributos.Add 0, g_oAtributoEnEdicion.Cod, g_oAtributoEnEdicion.Den, g_oAtributoEnEdicion.Tipo, _
                                        , , , , , , , inum, g_oAtributoEnEdicion.valor, , , , , , , g_oAtributoEnEdicion.TipoIntroduccion, _
                                        g_oAtributoEnEdicion.Minimo, g_oAtributoEnEdicion.Maximo, , , , , , , , , , , , , , , , , _
                                        , , , , valorNum, valorText, valorFec, valorBool, , , , g_oAtributoEnEdicion.Atrib, , , , , , tipoAtributo
Siguiente:
                    inum = inum + 1
                Wend
            End If
            
            '2. A�ADIR A BDD
            tsError = g_oLinea.AnyadirAtributos(AtributosLineaCatalogo.Linea, oAtributos)
            If tsError.NumError <> TESnoerror Then
                GoTo Error
            End If
            
            '3. A�ADIR AL GRID DE ATRIBUTOS DE L�NEA
            Set vbm = Nothing
            inum = 0
            While inum < Grid.SelBookmarks.Count
                .AddNew
                
                Grid.Bookmark = Grid.SelBookmarks(inum)
                Select Case tipoAtributo
                    Case AtributosLineaCatalogo.Articulo:
                        Set g_oAtributoEnEdicion = m_oAtributosArt.Item(CStr(Grid.Columns("ID").Value))
                    Case AtributosLineaCatalogo.ProveArtYProceOfer:
                        Set g_oAtributoEnEdicion = m_oAtributosProveArt.Item(CStr(Grid.Columns("ID").Value))
                    Case AtributosLineaCatalogo.ProceEsp:
                        Set g_oAtributoEnEdicion = m_oAtributosProce.Item(CStr(Grid.Columns("ID").Value))
                End Select
                
                If .Rows > 0 Then
                    For inum2 = 0 To .Rows - 1
                        vbm = .AddItemBookmark(inum2)
                        If Not IsEmpty(vbm) Then
                            If g_oAtributoEnEdicion.Id = CLng(.Columns("ATRIBID").CellValue(vbm)) And CInt(.Columns("ORICOD").CellValue(vbm)) = tipoAtributo Then
                                .CancelUpdate
                                GoTo Siguiente2
                            End If
                        End If
                    Next inum2
                End If
                .Columns("COD").Text = g_oAtributoEnEdicion.Cod
                .Columns("DEN").Value = g_oAtributoEnEdicion.Den
                .Columns("VAL").Value = g_oAtributoEnEdicion.valor
                '.Columns("VAL").Text = Grid.Columns("VAL").CellText(vSelBm)
                Dim sOrigen As String
                sOrigen = ""
                Select Case tipoAtributo
                    Case AtributosLineaCatalogo.Articulo:
                        sOrigen = m_sIdiArticulo
                    Case AtributosLineaCatalogo.ProveArtYProceOfer:
                        sOrigen = m_sIdiProveedorArt
                    Case AtributosLineaCatalogo.ProceEsp:
                        sOrigen = m_sIdiProceso
                    Case AtributosLineaCatalogo.MaestroAtributos:
                        sOrigen = m_sMaestroAtributos
                End Select
                .Columns("ORI").Text = sOrigen
                .Columns("ORICOD").Text = tipoAtributo
                .Columns("TIPO_INTRODUCCION").Value = g_oAtributoEnEdicion.TipoIntroduccion
                .Columns("TIPO_DATOS").Value = g_oAtributoEnEdicion.Tipo
                .Columns("MIN").Value = g_oAtributoEnEdicion.Minimo
                .Columns("MAX").Value = g_oAtributoEnEdicion.Maximo
                .Columns("ATRIBID").Value = g_oAtributoEnEdicion.Atrib
                .Update
                .Refresh
Siguiente2:
                inum = inum + 1
            Wend
        End With
        
        'RECARGAR VARIABLE DE ATRIBUTOS
        Set m_oAtributosLinea = Nothing
        Set m_oAtributosLinea = oFSGSRaiz.Generar_CAtributos
        Set m_oAtributosLinea = g_oLinea.DevolverAtributos(AtributosLineaCatalogo.Linea, g_oProveedor)
        'Ponemos el ID a cada l�nea del grid
        With sdbgCamposEsp(3)
            .MoveFirst
            If .Rows > 0 Then
                Dim oAtr As CAtributo
                For i = 0 To .Rows
                    If (.Columns("ID").Value = CStr(0) Or .Columns("ID").Value = "") Then
                        For Each oAtr In m_oAtributosLinea
                            If .Columns("COD").Value = oAtr.Cod And .Columns("ATRIBID").Value = CStr(oAtr.Atrib) Then
                                .Columns("ID").Value = oAtr.Id
                                Exit For
                            End If
                        Next
                    End If
                    
                    .MoveNext
                Next
            End If
        End With
    End If
    Grid.SelBookmarks.RemoveAll
    sdbgCamposEsp(3).MoveLast
Error:
    copiarAtributosALinea = tsError
End Function

Private Sub Form_Unload(Cancel As Integer)
    Set frmCatalogoAtribEspYAdj.m_oAtributosLinea = m_oAtributosLinea
    frmCatalogoAtribEspYAdj.AnyadirAtribEspsALista
End Sub

'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
'           FUNCIONALIDAD DEL GRID DE CAMPOS DE ESPECIFICACI�N DE L�NEA
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
''' <summary>
''' Evento que salta al hacer cambios en la grid
''' </summary>
''' <param name="Cancel">Cancelacion del cambio</param>
''' <returns></returns>
''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>
Private Sub sdbgCamposEsp_BeforeColUpdate(Index As Integer, ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Select Case Index
        Case 3
            Dim teserror As TipoErrorSummit
            Dim m_oAtributoAnyadir As CAtributo
            Dim m_oAtribEnEdicion As CAtributo
            Cancel = False
            If sdbgCamposEsp(3).Columns("VAL").Text <> "" Then
                Select Case UCase(sdbgCamposEsp(3).Columns("TIPO_DATOS").Text)
                    Case 2 'Numero
                        If (Not IsNumeric(sdbgCamposEsp(3).Columns("VAL").Text)) Then
                            oMensajes.AtributoValorNoValido ("TIPO2")
                            Cancel = True
                            GoTo Salir
                        Else
                            If sdbgCamposEsp(3).Columns("MIN").Text <> "" And sdbgCamposEsp(3).Columns("MAX").Text <> "" Then
                                If StrToDbl0(sdbgCamposEsp(3).Columns("MIN").Text) > StrToDbl0(sdbgCamposEsp(3).Columns("VAL").Text) Or StrToDbl0(sdbgCamposEsp(3).Columns("MAX").Text) < StrToDbl0(sdbgCamposEsp(3).Columns("VAL").Text) Then
                                    oMensajes.ValorEntreMaximoYMinimo sdbgCamposEsp(3).Columns("MIN").Text, sdbgCamposEsp(3).Columns("MAX").Text
                                    Cancel = True
                                    GoTo Salir
                                End If
                            End If
                        End If
                    Case 3 'Fecha
                        If (Not IsDate(sdbgCamposEsp(3).Columns("VAL").Text) And sdbgCamposEsp(3).Columns("VAL").Text <> "") Then
                            oMensajes.AtributoValorNoValido ("TIPO3")
                            Cancel = True
                            GoTo Salir
                        Else
                            If sdbgCamposEsp(3).Columns("MIN").Text <> "" And sdbgCamposEsp(3).Columns("MAX").Text <> "" Then
                                If CDate(sdbgCamposEsp(3).Columns("MIN").Text) > CDate(sdbgCamposEsp(3).Columns("VAL").Text) Or CDate(sdbgCamposEsp(3).Columns("MAX").Text) < CDate(sdbgCamposEsp(3).Columns("VAL").Text) Then
                                 oMensajes.ValorEntreMaximoYMinimo sdbgCamposEsp(3).Columns("MIN").Text, sdbgCamposEsp(3).Columns("MAX").Text
                                    Cancel = True
                                    GoTo Salir
                                End If
                            End If
                        End If
                End Select
            End If
            Exit Sub
Salir:
            If Me.Visible Then sdbgCamposEsp(3).SetFocus
    End Select
End Sub

Private Sub sdbgCamposEsp_AfterDelete(Index As Integer, RtnDispErrMsg As Integer)
    Select Case Index
        Case 3
            RtnDispErrMsg = 0
            If IsEmpty(sdbgCamposEsp(3).RowBookmark(sdbgCamposEsp(3).Row)) Then
                sdbgCamposEsp(3).Bookmark = sdbgCamposEsp(3).RowBookmark(sdbgCamposEsp(3).Row - 1)
            Else
                sdbgCamposEsp(3).Bookmark = sdbgCamposEsp(3).RowBookmark(sdbgCamposEsp(3).Row)
            End If
    End Select
End Sub

Private Sub sdbgCamposEsp_BeforeDelete(Index As Integer, Cancel As Integer, DispPromptMsg As Integer)
    Select Case Index
        Case 3
            DispPromptMsg = 0
    End Select
End Sub

Private Sub sdbgCamposEsp_BeforeUpdate(Index As Integer, Cancel As Integer)
    Select Case Index
        Case 3
            sdbgCamposEsp(3).Columns("VAL").DropDownHwnd = 0
    End Select
End Sub

'''' <summary>
'''' Evento que salta al hacer click en el boton de un atruto de texto largo
'''' </summary>
'''' <param name="Cancel">Click en un atributo de tipo texto largo</param>
'''' <returns></returns>
'''' <remarks>Llamada desde;Evento Tiempo m�ximo</remarks>
Private Sub sdbgCamposEsp_BtnClick(Index As Integer)
    Select Case Index
        Case 3
            If sdbgCamposEsp(3).Col < 0 Then Exit Sub
        
            If sdbgCamposEsp(3).Columns(sdbgCamposEsp(3).Col).Name = "VAL" Then
                frmATRIBDescr.g_bEdicion = True
                frmATRIBDescr.caption = sdbgCamposEsp(3).Columns("COD").Value & ": " & sdbgCamposEsp(3).Columns("DEN").Value
                frmATRIBDescr.txtDescr.Text = sdbgCamposEsp(3).Columns(sdbgCamposEsp(3).Col).Value
                frmATRIBDescr.g_sOrigen = "frmCatalogoAtrib"
                frmATRIBDescr.Show 1
                If sdbgCamposEsp(3).DataChanged Then
                    sdbgCamposEsp(3).Update
                End If
            End If
    End Select
End Sub

Private Sub sdbgCamposEsp_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
        Case 3
            If KeyAscii = vbKeyEscape Then
                If sdbgCamposEsp(3).DataChanged = False Then
                    sdbgCamposEsp(3).CancelUpdate
                    sdbgCamposEsp(3).DataChanged = False
                End If
            End If
    End Select
End Sub

Private Sub sdbgCamposEsp_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
    Select Case Index
        Case 3
            Dim oElem As CValorPond
            Dim oLista As CValoresPond
            Dim oatrib As CAtributo
            If sdbgCamposEsp(3).Columns("TIPO_INTRODUCCION").Value = 0 Then 'Libre
                If sdbgCamposEsp(3).Columns("TIPO_DATOS").Value = TipoBoolean Then
                    sdbddValor.RemoveAll
                    sdbddValor.AddItem ""
                    sdbgCamposEsp(3).Columns("VAL").DropDownHwnd = sdbddValor.hWnd
                    sdbddValor.Enabled = True
                Else
                    sdbgCamposEsp(3).Columns("VAL").DropDownHwnd = 0
                    sdbddValor.Enabled = False
                    If sdbgCamposEsp(3).Columns("TIPO_DATOS").Value = 1 Then
                        sdbgCamposEsp(3).Columns("VAL").Style = ssStyleEditButton
                    End If
                End If
            Else 'Lista
                sdbddValor.RemoveAll
                sdbddValor.AddItem ""
                sdbgCamposEsp(3).Columns("VAL").DropDownHwnd = sdbddValor.hWnd
                sdbddValor.Enabled = True
                sdbddValor.DroppedDown = True
                sdbddValor_DropDown
                sdbddValor.DroppedDown = True
            End If
    End Select
End Sub

Private Sub sdbgCamposEsp_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    If sdbgCamposEsp(Index).Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
        If sdbgCamposEsp(Index).Columns("VALOR").CellValue(Bookmark) = 1 Then
            sdbgCamposEsp(Index).Columns("VALOR").Text = m_sIdiTrue
        ElseIf sdbgCamposEsp(Index).Columns("VALOR").CellValue(Bookmark) = 0 Then
            sdbgCamposEsp(Index).Columns("VALOR").Text = m_sIdiFalse
        End If
    End If
End Sub

Private Sub sdbgCamposEsp_Scroll(Index As Integer, Cancel As Integer)
    Select Case Index
        Case 3
            sdbgCamposEsp(3).Columns("VAL").DropDownHwnd = 0
    End Select
End Sub

Private Sub sdbgCamposEsp_AfterColUpdate(Index As Integer, ByVal ColIndex As Integer)
    Select Case Index
        Case 3
            Dim teserror As TipoErrorSummit
            Dim lIdA As Long
            Dim v As Variant
            Dim sCod As String
            Dim oatrib As CAtributoOfertado
            Dim oLinea As CPrecioItem
            Dim ogroup As SSDataWidgets_B.Group
            Dim oOferta As COferta
            Dim oAtribEsc As CEscalado
            Dim iAccion As Integer
            Dim bEspera As Boolean
            Dim i As Integer
            
            ''' Modificamos en la base de datos
            ''Comprobacion de atributos de oferta obligatorios
            With sdbgCamposEsp(3)
                '1. ACTUALIZAR BDD
                Dim tsError As TipoErrorSummit
                tsError = g_oLinea.ActualizarAtributo(.Columns("ID").Value, .Columns("VAL").Value, .Columns("TIPO_DATOS").Value)
                '2.ACTUALIZAR VARIABLE ATRIBUTOS L�NEA
                'A
                For i = 1 To m_oAtributosLinea.Count
                    If m_oAtributosLinea.Item(i).Id = .Columns("ID").Value Then
                        m_oAtributosLinea.Item(i).valor = .Columns("VAL").Value
                    End If
                Next
                
                'SI HA HABIDO ERROR AL GUARDAR, AVISAMOS
                If tsError.NumError <> TESnoerror Then
                    TratarError tsError
                    If Me.Visible Then .SetFocus
                    .DataChanged = False
                End If
            End With
    End Select
End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    Dim i As Long
    Dim bm As Variant
    On Error Resume Next
    sdbddValor.MoveFirst
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgCamposEsp(3).Columns("VAL").Value = Mid(sdbddValor.Columns(1).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oAtributo As CAtributo
    Dim oLista As CValorPond
    Dim iIndice As Integer
    iIndice = 1
    
    If sdbgCamposEsp(3).Rows = 0 Then Exit Sub
   
    If sdbgCamposEsp(3).Columns("VAL").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    End If
    sdbddValor.RemoveAll
    sdbddValor.DroppedDown = True
    
    Set oAtributo = oFSGSRaiz.Generar_CAtributo
    oAtributo.Id = sdbgCamposEsp(3).Columns("ATRIBID").Value
    
    If oAtributo.Id > 0 Then
        If sdbgCamposEsp(3).Columns("TIPO_INTRODUCCION").Value = "1" Then
            oAtributo.CargarDatosAtributo
            oAtributo.CargarListaDeValores AtributoParent.Linea_Catalogo
            For Each oLista In oAtributo.ListaPonderacion
                sdbddValor.AddItem oLista.ValorLista & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        Else
            If sdbgCamposEsp(3).Columns("TIPO_DATOS").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem "1" & Chr(m_lSeparador) & m_sIdiTrue
                sdbddValor.AddItem "0" & Chr(m_lSeparador) & m_sIdiFalse
            End If
        End If
    End If
    
    sdbddValor.Width = sdbgCamposEsp(3).Columns("VAL").Width
    sdbddValor.Columns("DESC").Width = sdbddValor.Width
End Sub
'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
