VERSION 5.00
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmDesgloseValores 
   Caption         =   "DValores por defecto para el desglose de material"
   ClientHeight    =   5415
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11370
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDesgloseValores.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5415
   ScaleWidth      =   11370
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin UltraGrid.SSUltraGrid ssLineas 
      Height          =   4100
      Left            =   120
      TabIndex        =   10
      Top             =   840
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   7223
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   68157440
      Override        =   "frmDesgloseValores.frx":0CB2
      Caption         =   "DDesglose"
   End
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   120
      ScaleHeight     =   420
      ScaleWidth      =   7215
      TabIndex        =   12
      Top             =   5000
      Width           =   7215
      Begin VB.CommandButton cmdCalcular 
         Caption         =   "DRecalcular"
         Height          =   345
         Left            =   4500
         TabIndex        =   9
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "DRestaurar"
         Height          =   345
         Left            =   3375
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "DDeshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2250
         TabIndex        =   7
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "DEliminar"
         Height          =   345
         Left            =   1125
         TabIndex        =   6
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdAnyadir 
         Caption         =   "DAnyadir"
         Height          =   345
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Width           =   1005
      End
   End
   Begin VB.PictureBox picCabecera 
      BorderStyle     =   0  'None
      Height          =   700
      Left            =   120
      ScaleHeight     =   705
      ScaleWidth      =   11055
      TabIndex        =   0
      Top             =   120
      Width           =   11055
      Begin VB.CommandButton cmdSelMat 
         Height          =   285
         Left            =   9745
         Picture         =   "frmDesgloseValores.frx":0D08
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   0
         Width           =   315
      End
      Begin VB.CommandButton cmdBorrar 
         Height          =   285
         Left            =   9385
         Picture         =   "frmDesgloseValores.frx":0D74
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   315
      End
      Begin VB.TextBox txtMaterial 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   4480
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   0
         Width           =   4870
      End
      Begin VB.CheckBox chkLineas 
         Caption         =   "DLimitar la introducci�n de datos a las l�neas configuradas por defecto"
         Height          =   255
         Left            =   0
         TabIndex        =   4
         Top             =   430
         Width           =   6615
      End
      Begin VB.CheckBox chkMaterial 
         Caption         =   "DLimitar  los art�culos al siguiente grupo de material"
         Height          =   255
         Left            =   0
         TabIndex        =   1
         Top             =   30
         Width           =   4400
      End
   End
End
Attribute VB_Name = "frmDesgloseValores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_oCampoDesglose As CFormItem
Public g_sOrigen As String
Public g_sCodOrganizacionCompras As String
Public g_sCodCentro As String
Public g_oCampoFormula As CFormItem
Public g_sMoneda As String
Public g_strPK As String
Public m_sPerCod As String

'Variables privadas
Private m_sValorCTablaExterna As String
Private m_strPK_old As String
Private m_Accion As accionessummit
Private m_bModif As Boolean
Private m_sProveCod As String
Private m_sCodCentroCoste As String
Private m_sValorPres As String
Private m_sMat As String
Private m_bCargando As Boolean
Private m_bCalculando As Boolean
Private m_bDeshaciendo As Boolean
Private m_bValorListaGridSeleccionado As Boolean 'Esta variable estara a true en el momento que se seleccione un elemento de un campo que sea lista, asi en el evento CloseUp se sabra que realmente se ha seleccionado un valor y no se ha desplegado la lista y no se ha seleccionado nada o dejado el valor que ya estaba

'Variables de idiomas
Private m_sIdiForm As String
Private m_sIdiGrupo As String
Private m_sIdiDesgloseMat As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sMensaje(9) As String
Private m_sIdiTipoFecha(17) As String
Private m_sIdiKB As String
Private m_sIdiErrorEvaluacion(2) As String
Private m_sSeleccionarMatFormulario As String

'Para el filtro de material
Private m_sGMN1Cod As String
Private m_sGMN2Cod As String
Private m_sGMN3Cod As String
Private m_sGMN4Cod As String

'Variables de seguridad
Private m_bRestrMat As Boolean
Private m_bRestrDest As Boolean
Private m_bRestrProve As Boolean
Private m_bRestrPresUO As Boolean

Private m_sMensajeDenArt(2) As String

Private m_oTablaExterna As CTablaExterna

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_DESGLOSE_VALORES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        chkMaterial.caption = Ador(0).Value  '1 Limitar los art�culos al siguiente grupo de material
        Ador.MoveNext
        chkLineas.caption = Ador(0).Value   '2 Limitar la introducci�n de datos a las l�neas configuradas por defecto
        Ador.MoveNext
        ssLineas.caption = Ador(0).Value  '3 Desglose
        Ador.MoveNext
        cmdAnyadir.caption = Ador(0).Value  '4 &A�adir
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value  '5 &Eliminar
        Ador.MoveNext
        m_sIdiForm = Ador(0).Value  '6 Formulario
        Ador.MoveNext
        m_sIdiGrupo = Ador(0).Value  '7 Grupo
        Ador.MoveNext
        m_sIdiDesgloseMat = Ador(0).Value '8 Valores por defecto para el desglose de material
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value  '9 &Deshacer
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value  '10 S�
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value  '11 No
        
        For i = 1 To 7
            Ador.MoveNext
            m_sMensaje(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        
        For i = 1 To 17
            Ador.MoveNext
            m_sIdiTipoFecha(i) = Ador(0).Value
        Next i
        
        Ador.MoveNext
        m_sIdiKB = Ador(0).Value  '38 kb
        
        Ador.MoveNext
        cmdCalcular.caption = Ador(0).Value  ' 39 Recalcular
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '40 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '41 Error al realizar el c�lculo:Valores incorrectos.
        
        For i = 1 To 2
            Ador.MoveNext
            m_sMensajeDenArt(i) = Ador(0).Value
        Next i
        
        m_sMensaje(8) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(9) = Ador(0).Value
        Ador.MoveNext
        m_sSeleccionarMatFormulario = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub chkLineas_Click()
Dim teserror As TipoErrorSummit

    'Limita o no la introducci�n de l�neas por defecto:
    If m_bCargando = True Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    If chkLineas.Value = vbChecked Then
        g_oCampoDesglose.LineasPreconf = True
    Else
        g_oCampoDesglose.LineasPreconf = False
    End If
    
    teserror = g_oCampoDesglose.ModificarLineasPreconf
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
End Sub

Private Sub chkMaterial_Click()
    If m_bCargando = True Then Exit Sub
    
    LimitarArticulosAGrupoMat
End Sub

Private Sub cmdAnyadir_Click()
Dim lLinea As Long
Dim oFila As SSRow
Dim oCampo As CFormItem
Dim bEdit As Boolean

    If ssLineas.Bands(0).Columns.Count = 1 Then Exit Sub
    
    If ssLineas.HasRows = True Then
        Set oFila = ssLineas.GetRow(ssChildRowLast)
        lLinea = oFila.Cells("LINEA").Value + 1
    Else
        lLinea = 1
    End If
    
    'a�ade una nueva fila a la grid
    ssLineas.Bands(0).AddNew
    
    'Pone el foco en el campo c�digo de la nueva fila:
    ssLineas.Selected.Cells.clear
    If Me.Visible Then ssLineas.SetFocus
    ssLineas.PerformAction ssKeyActionEnterEditMode
        
    ssLineas.ActiveRow.Cells("LINEA").Value = lLinea
    
    If chkMaterial.Value = vbChecked And txtMaterial.Text <> "" Then
        'si hay campo de material le pone el de por defecto que se ha seleccionado:
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.material Then
                m_sMat = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod)) & m_sGMN1Cod
                m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod)) & m_sGMN2Cod
                m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod)) & m_sGMN3Cod
                m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod)) & m_sGMN4Cod

                bEdit = ssLineas.IsInEditMode
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionExitEditMode
                End If
                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = txtMaterial.Text
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionEnterEditMode
                End If
                ssLineas.Update
                Exit For
            End If
        Next
    End If
    
    m_Accion = ACCLineaDesgloseAnyadir
    
    cmdDeshacer.Enabled = True
End Sub

Private Sub cmdBorrar_Click()
    If txtMaterial.Text = "" Then Exit Sub
    Me.limpiarFiltroMaterial
    
    LimitarArticulosAGrupoMat
    DoEvents
End Sub

Private Sub cmdCalcular_Click()
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim i As Integer
Dim sVariables() As String
Dim dValues() As Double
Dim teserror As TipoErrorSummit
Dim dValor As Double
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim sCod As String

    If ssLineas.HasRows = False Then Exit Sub

    'Recalcula las f�rmulas:
    Set iEq = New USPExpression
    
    'Almacena las variables en un array:
    ReDim sVariables(g_oCampoDesglose.Formulas.Count)
    ReDim dValues(g_oCampoDesglose.Formulas.Count)
    
    m_bCalculando = True
    
    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Do While Not oRow Is Nothing
        i = 1
        For Each oCampo In g_oCampoDesglose.Formulas
            sVariables(i) = oCampo.IdCalculo
            If oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
                dValues(i) = 0
            Else
                dValues(i) = NullToDbl0(oRow.Cells("C_" & oCampo.Id).Value)
            End If
            i = i + 1
        Next
    
        'Va recalculando cada f�rmula para cada l�nea
        i = 1
        For Each oCampo In g_oCampoDesglose.Formulas
            If oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
                If IsNull(oCampo.OrigenCalcDesglose) And Not IsNull(oCampo.Formula) Then
                    'Se calculan las f�rmulas que no sean de desglose:
                    lIndex = iEq.Parse(oCampo.Formula, sVariables, lErrCode)
                    
                    If lErrCode = USPEX_NO_ERROR Then
                        On Error GoTo EvaluationError
                        dValues(i) = iEq.Evaluate(dValues)
                         
                        'Almacena el valor calculado en la colecci�n:
                        sCod = CStr(oRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                        If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                            g_oCampoDesglose.LineasDesglose.Add oRow.Cells("LINEA").Value, oCampo, dValues(i)
                            g_oCampoDesglose.LineasDesglose.Item(sCod).modificado = True
                        Else
                            g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum = dValues(i)
                        End If
                        
                    Else
                        oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                        m_bCalculando = False
                        Exit Sub
                    End If
                End If
            End If
            i = i + 1
        Next
        
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
    
    
    'Ahora calcula el valor de desglose:
    If Not IsNull(g_oCampoFormula.Formula) Then
        i = 1
        For Each oCampo In g_oCampoDesglose.Formulas
            sVariables(i) = oCampo.IdCalculo
            dValues(i) = 0
            Set oRow = ssLineas.GetRow(ssChildRowFirst)
            Do While Not oRow Is Nothing
                sCod = CStr(oRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                If Not g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                    dValues(i) = dValues(i) + NullToDbl0(g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum)
                End If
                If oRow.HasNextSibling = True Then
                    Set oRow = oRow.GetSibling(ssSiblingRowNext)
                Else
                    Set oRow = Nothing
                End If
            Loop
            i = i + 1
        Next
        lIndex = iEq.Parse(g_oCampoFormula.Formula, sVariables, lErrCode)
        If lErrCode = USPEX_NO_ERROR Then
             On Error GoTo EvaluationError
             dValor = iEq.Evaluate(dValues)
             g_oCampoFormula.valorNum = dValor
        Else
             oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
             m_bCalculando = False
             Exit Sub
        End If
    End If
    
    'Ahora almacena los resultados en BD:
    teserror = g_oCampoDesglose.ModificarValorCamposCalculados(g_oCampoFormula)
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        
    Else
        'Actualiza la grid con los campos calculados
        Set oRow = ssLineas.GetRow(ssChildRowFirst)
        Do While Not oRow Is Nothing
            ssLineas.ActiveRow = oRow
            For Each oCampo In g_oCampoDesglose.Formulas
                If oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
                    sCod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                    oRow.Cells("C_" & oCampo.Id).Value = g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum
                    ssLineas.Update
                End If
            Next
            If oRow.HasNextSibling = True Then
                Set oRow = oRow.GetSibling(ssSiblingRowNext)
            Else
                Set oRow = Nothing
            End If
        Loop
        
        g_oCampoFormula.RecuperarFecAct  'Recupera la fecha de act. (lo hago aqui,xk se hacen las modificaciones en una sola transacci�n y por la estructura de las clases las 2 clases de campos no tienen relaci�n )
        
        If g_sOrigen = "frmFormularioSimulacion" Then
            frmFormularioSimulacion.sdbgCalculados.Columns("VALOR_CALCULADO").Value = g_oCampoFormula.valorNum
            frmFormularioSimulacion.sdbgCalculados.Update
        End If
        
    End If
    
    Set iEq = Nothing
    
    m_bCalculando = False
    Exit Sub
    
EvaluationError:
    oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
    m_bCalculando = False
End Sub

''' <summary>
''' Deshacer la edici�n. Si a�ades linea y sin hacer nada mas deshaces el cambio, la linea desaparece de pantalla
''' y bbdd
''' </summary>
''' <remarks>Llamada desde: cmdDeshacer; Tiempo m�ximo: 0</remarks>
Private Sub cmdDeshacer_Click()
    m_bDeshaciendo = True
    ssLineas.CancelUpdate
    m_bDeshaciendo = False
    
    m_Accion = ACCDesgloseCons
    
    cmdDeshacer.Enabled = False
End Sub

Private Sub cmdEliminar_Click()

    'Elimina la l�nea de desglose de la grid, posteriormente se eliminar� de la base de datos
    If g_oCampoDesglose.LineasDesglose Is Nothing Then Exit Sub
    If ssLineas.ActiveRow Is Nothing Then Exit Sub
     

    ssLineas.ActiveRow.Delete

End Sub

Private Sub cmdRestaurar_Click()
    CargarValoresDesglose
    CargarTagTablasExternas
End Sub

Private Sub cmdSelMat_Click()
    Dim oCampoMaterial As CFormItem
    frmSELMAT.sOrigen = "frmDesgloseValores"
    frmSELMAT.bRComprador = m_bRestrMat
    If frmFormularios.g_oFormSeleccionado.ExisteNivelSeleccionMatObligatorio Then
        Set oCampoMaterial = frmFormularios.g_oFormSeleccionado.getCampoTipoNivelSeleccionObligatoria()
        If NoHayParametro(oCampoMaterial.GMN1Cod) Then
            oMensajes.mensajeGenericoOkOnly m_sSeleccionarMatFormulario
            Exit Sub
        Else
            frmSELMAT.sGMN1 = NullToStr(oCampoMaterial.GMN1Cod)
            frmSELMAT.sGMN2 = NullToStr(oCampoMaterial.GMN2Cod)
            frmSELMAT.sGMN3 = NullToStr(oCampoMaterial.GMN3Cod)
            frmSELMAT.sGMN4 = NullToStr(oCampoMaterial.GMN4Cod)
        End If
    End If
    frmSELMAT.Show vbModal
End Sub

Private Sub Form_Load()
    
    Set m_oTablaExterna = oFSGSRaiz.Generar_CTablaExterna
    

    Me.Height = 5952
    Me.Width = 11490
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    Me.caption = m_sIdiForm & ":" & g_oCampoDesglose.Grupo.Formulario.Den & "/" & m_sIdiGrupo & ":" & NullToStr(g_oCampoDesglose.Grupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & "/" & m_sIdiDesgloseMat
    
    CargarValoresDesglose
    
    CargarTagTablasExternas
    
    'Carga las f�rmulas por si hay campos calculados:
    If basParametros.gParametrosGenerales.gsAccesoFSWS = AccesoFSWSCompleto Then
        g_oCampoDesglose.CargarCamposCalculadosDesglose
    End If
    
    'Si se llama desde la simulaci�n carga las f�rmulas:
    If g_sOrigen = "frmFormularioDesgloseCalculo" Or g_sOrigen = "frmFormularioSimulacion" Then
        cmdCalcular.Visible = True
    Else
        cmdCalcular.Visible = False
    End If

    m_Accion = ACCDesgloseCons

End Sub

Private Sub Form_Resize()
Dim oCol As SSColumn

    If Me.Width < 500 Then Exit Sub
    If Me.Height < 2000 Then Exit Sub
    
    ssLineas.Width = Me.Width - 315
    
    If txtMaterial.Visible = True Then
        ssLineas.Height = Me.Height - picNavigate.Height - 1400
    Else
        ssLineas.Height = Me.Height - 1450
    End If
    
    If ssLineas.Bands.Count > 0 Then
        For Each oCol In ssLineas.Bands(0).Columns
            If g_oCampoDesglose.Desglose.Count > 0 Then
                If ((ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count) < 1000 Then
                    oCol.Width = 1000
                Else
                    oCol.Width = (ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count
                End If
            Else
                oCol.Width = 1000
            End If
        Next
    End If
    
    picNavigate.Top = ssLineas.Top + ssLineas.Height + 65
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not ssLineas.ActiveRow Is Nothing Then
        If ssLineas.ActiveRow.DataChanged Then
            ssLineas.Update
        End If
    End If
    Set g_oCampoDesglose = Nothing
    Set g_oCampoFormula = Nothing
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    g_sOrigen = ""
    g_sCodOrganizacionCompras = ""
    g_sMoneda = ""
End Sub
'�public?
Public Sub PonerMatSeleccionado(ByVal bGrid As Boolean)
    Dim tArray() As String
    Dim i As Byte
    Dim oFila As SSRow
    Dim blnBorrame As Boolean
    Dim oGmn As Variant
    
    Dim sNuevoCod1 As String
    Dim sNuevoCod2 As String
    Dim sNuevoCod3 As String
    Dim sNuevoCod4 As String
    Dim sGridValor As String
    Dim oCampo As CFormItem
    Dim bEdit As Boolean
    
    Set oGmn = frmSELMAT.oGMNSeleccionado
    
    If Not oGmn Is Nothing Then
        sNuevoCod1 = oGmn.GMN1Cod
        sNuevoCod2 = oGmn.GMN2Cod
        sNuevoCod3 = oGmn.GMN3Cod
        sNuevoCod4 = oGmn.GMN4Cod
    End If
    
    If bGrid = False Then
        If m_sGMN1Cod = sNuevoCod1 And m_sGMN2Cod = sNuevoCod2 And m_sGMN3Cod = sNuevoCod3 And m_sGMN4Cod = sNuevoCod4 Then
            Exit Sub
        End If
        m_sGMN1Cod = ""
        m_sGMN2Cod = ""
        m_sGMN3Cod = ""
        m_sGMN4Cod = ""
    End If
    
    If Not oGmn Is Nothing Then
        If bGrid = True Then
            sGridValor = oGmn.titulo
            m_sMat = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sNuevoCod1)) & sNuevoCod1
            m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(sNuevoCod2)) & sNuevoCod2
            m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(sNuevoCod3)) & sNuevoCod3
            m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(sNuevoCod4)) & sNuevoCod4
        Else
            m_sGMN1Cod = oGmn.GMN1Cod
            txtMaterial.Text = oGmn.titulo
        End If
    End If
        
    
    Set oGmn = Nothing
    
    If bGrid = True Then
        bEdit = ssLineas.IsInEditMode
        If bEdit = True Then
            ssLineas.PerformAction ssKeyActionExitEditMode
        End If
        ssLineas.ActiveCell.Value = sGridValor
        If bEdit = True Then
            ssLineas.PerformAction ssKeyActionEnterEditMode
        End If
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        
        For Each oCampo In g_oCampoDesglose.Desglose  'Quita el valor de los art�culos // Denominacion
            If (oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Then
                If Not IsNull(ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value) Then
                    'oArticulos.CargarTodosLosArticulos ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value, , True, , , , sNuevoCod1, sNuevoCod2, sNuevoCod3, sNuevoCod4
                    oArticulos.CargarTodosLosArticulos ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value, , True
                    If oArticulos.Count > 0 Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id + 1).Value = ""
                        blnBorrame = True
                        'Exit For
                    End If
                End If
            ElseIf oCampo.CampoGS = TipoCampoGS.CodArticulo Then
                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                blnBorrame = True
                'Exit For
            ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                    Set oFila = ssLineas.ActiveRow
                    m_sValorCTablaExterna = ValorColumTablaExterna(0)
                    Set ssLineas.ActiveRow = oFila
                    If m_sValorCTablaExterna <> "" Then
                        tArray = Split(m_sValorCTablaExterna, "#")
                        For i = LBound(tArray) To UBound(tArray)
                            ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                            ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                        Next
                    End If
                End If
            End If
         Next
    Else
        LimitarArticulosAGrupoMat
    End If
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012< 1seg </revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIOModificar)) Is Nothing) Then
        m_bModif = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIORestDest)) Is Nothing) Then
        m_bRestrDest = True
    End If

    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIORestMat)) Is Nothing) Then
        m_bRestrMat = True
    End If

    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIORestProve)) Is Nothing) Then
        m_bRestrProve = True
    End If

    'Comprueba los datos del usuario
    Dim oUsuario As CUsuario
    Set oUsuario = oFSGSRaiz.generar_cusuario
    oUsuario.Cod = basOptimizacion.gvarCodUsuario
    oUsuario.ExpandirUsuario
    If oUsuario.FSWSPresUO = True Then
        m_bRestrPresUO = True
    End If
    Set oUsuario = Nothing
    
    If m_bModif = False Then
        cmdAnyadir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Left = cmdAnyadir.Left
        picCabecera.Enabled = False
    End If
End Sub

Private Sub CargarValoresDesglose()
Dim oCampo As CFormItem
Dim bArt As Boolean
Dim oGmn As Variant

    m_bCargando = True
    
    'Indica si se limita la introducci�n a las l�neas configuradas por defecto:
    If g_oCampoDesglose.LineasPreconf = True Then
        chkLineas.Value = vbChecked
    Else
        chkLineas.Value = vbUnchecked
    End If
    
    'Indica si los art�culos est�n limitados a un grupo de material
    If (NullToStr(g_oCampoDesglose.GMN1Cod) = "" And NullToStr(g_oCampoDesglose.GMN2Cod) = "" And NullToStr(g_oCampoDesglose.GMN3Cod) = "" And NullToStr(g_oCampoDesglose.GMN4Cod) = "") And Not (IIf(IsNull(g_oCampoDesglose.nivelSeleccion), 0, g_oCampoDesglose.nivelSeleccion) <> 0) Then
        chkMaterial.Value = vbUnchecked
    Else
        chkMaterial.Value = vbChecked
        Set oGmn = createGMN(NullToStr(g_oCampoDesglose.GMN1Cod), NullToStr(g_oCampoDesglose.GMN2Cod), NullToStr(g_oCampoDesglose.GMN3Cod), NullToStr(g_oCampoDesglose.GMN4Cod))
        oGmn.cargarDenominacion
        txtMaterial.Text = oGmn.titulo
        Set oGmn = Nothing
    End If
    
    
    'Carga los campos de desglose:
    g_oCampoDesglose.CargarDesglose
    
    'Asigna a la grid los valores de las l�neas de desglose:
    Set ssLineas.DataSource = g_oCampoDesglose.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
    
    'Crea las listas de dropdown:
    ssLineas.ValueLists.clear

    ssLineas.Override.SelectTypeRow = ssSelectTypeSingle
        
    CrearValueList
    
    'si no existe el campo art�culo no saldr� el filtro de material:
    bArt = False
    For Each oCampo In g_oCampoDesglose.Desglose
        If ((oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo)) Then
            bArt = True
            Exit For
        End If
    Next

    If bArt = False Then
        chkMaterial.Visible = False
        txtMaterial.Visible = False
        cmdBorrar.Visible = False
        cmdSelMat.Visible = False
        chkLineas.Top = chkMaterial.Top
        ssLineas.Top = 460
    End If
    
    m_bCargando = False
End Sub

Private Sub ssLineas_AfterCellActivate()
Dim oCellArt As SSCell
Dim bAnyadirArt As Boolean

    If Not IsNull(g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS) Then
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
            Set oCellArt = ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.Index - 1)
            If Not IsNull(oCellArt.Value) Then
                bAnyadirArt = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellArt.Column.key, 3))).AnyadirArt
                If Not esGenerico(oCellArt.Value, bAnyadirArt) Then
                    ssLineas.ActiveCell.Column.Style = ssStyleDropDownList
                Else
                    ssLineas.ActiveCell.Column.Style = ssStyleDropDown
                End If
            
            End If
        End If
    End If

End Sub


''' <summary>
''' Evento de sistema q responde a un cambio en una celda que se depliega de un desglose
''' </summary>
''' <param name="Cell">Celda modificada</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub ssLineas_AfterCellListCloseUp(ByVal Cell As UltraGrid.SSCell)
Dim tArray() As String
Dim blnBorrame As Boolean
Dim oCellSiguiente As SSCell
Dim sMoneda As String
Dim sCodArt As String
Dim sArt As Variant
Dim i, contTerminar As Integer
Dim oCampo As CFormItem
Dim oFila As SSRow
Dim contTerminarHasta As Integer
    

    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))) Is Nothing Then Exit Sub
        
    '--------------------------------------------------------------------------------------------------------------------------------
    'Cuando seleccione un pa�s se quita la provincia seleccionada anteriormente,si es que hay alguna:
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Pais Then
        Set oCellSiguiente = ssLineas.ActiveRow.Cells(Cell.Column.Index + 1)
          
        If oCellSiguiente.Value <> "" And Not IsNull(oCellSiguiente.Value) Then
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.provincia Then
                oCellSiguiente.Value = ""
            End If
        End If
    End If
    
    '--------------------------------------------------------------------------------------------------------------------------------
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    'Cuando seleccionamos un c�digo de art�culo, asignar a la siguiente celda la denominaci�n
    If (g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo) And Cell.GetText <> "" Then
        sMoneda = "EUR"
        If g_sMoneda <> "" Then
            sMoneda = g_sMoneda
        End If
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            If (Cell.Column.Index + 1) < ssLineas.ActiveRow.Cells.Count Then
                Set oCellSiguiente = ssLineas.ActiveRow.Cells(Cell.Column.Index + 1)
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
                    If InStr(1, Cell.GetText, " - ") > 0 Then
                        oCellSiguiente.Value = Mid$(Cell.GetText, InStr(1, Cell.GetText, " - ") + 3)
                    End If
                End If
            End If
        End If
        sArt = Split(Cell.GetText, " - ")
        If UBound(sArt) > 0 Then
            sCodArt = sArt(0)
        Else
            sCodArt = Cell.GetText
        End If
          
        
        If esGenerico(sCodArt) Then
            contTerminarHasta = 1 'si es gen�rico solo hay que buscar la unidad
        Else
            contTerminarHasta = 3 'hay que buscar el precio, proveedor y unidad
        End If
        
        
        i = 1
        contTerminar = 0
               
        'Buscar el PRECIO / PROVEEDOR / UNIDAD
        While (i < ssLineas.ActiveRow.Cells.Count) And (contTerminar <> contTerminarHasta)
            Set oCellSiguiente = ssLineas.ActiveRow.Cells(i)
            If Not esGenerico(sCodArt) Then
                'PRECIO
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoSC.PrecioUnitario And _
                g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoPrecioADJ(sCodArt, sMoneda)
                    contTerminar = contTerminar + 1
                End If
                'PROVEEDOR
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Proveedor And _
                g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoProveedorADJ(sCodArt)
                    contTerminar = contTerminar + 1
                End If
            End If
            'Cargar la UNIDAD
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Unidad Then
                oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUnidadArticulo(sCodArt)
                contTerminar = contTerminar + 1
            End If
            i = i + 1
        Wend
    End If
    
    '--------------------------------------------------------------------------------------------------------------------------------
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    'Cuando seleccionamos una denominaci�n de un art�culo, asignar a la celda  anterior el c�digo
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo And Cell.GetText <> "" Then
        sMoneda = "EUR"
        If g_sMoneda <> "" Then
            sMoneda = g_sMoneda
        End If

        sArt = Split(Cell.GetText, " - ")
        If UBound(sArt) > 0 Then
            sCodArt = sArt(0)
        Else
            sCodArt = Cell.GetText
        End If

        Set oCellSiguiente = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            oCellSiguiente.Value = sCodArt
        End If
       
        If esGenerico(sCodArt) Then
            contTerminarHasta = 1 'si es gen�rico solo hay que buscar la unidad
        Else
            contTerminarHasta = 3 'hay que buscar el precio, proveedor y unidad
        End If

        i = 1
        contTerminar = 0
        'Buscar el PRECIO / PROVEEDOR / UNIDAD
        While (i < ssLineas.ActiveRow.Cells.Count) And (contTerminar <> contTerminarHasta)
            Set oCellSiguiente = ssLineas.ActiveRow.Cells(i)
            If Not esGenerico(sCodArt) Then
                'PRECIO
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoSC.PrecioUnitario And _
                g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoPrecioADJ(sCodArt, sMoneda)
                    contTerminar = contTerminar + 1
                End If
                'PROVEEDOR
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Proveedor And _
                g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoProveedorADJ(sCodArt)
                    contTerminar = contTerminar + 1
                End If
            End If
            'Cargar la UNIDAD
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Unidad Then
                oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUnidadArticulo(sCodArt)
                contTerminar = contTerminar + 1
            End If
            
            i = i + 1
        Wend
        
    End If
    
    '--------------------------------------------------------------------------------------------------------------------------------
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    'Cuando seleccionamos OrganizacionCompras --> Borrar el articulo, el Centro, el almacen y el proveedor ERP si estuviera
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.OrganizacionCompras Then
        If Not ((ssLineas.ActiveRow.Band.Columns.Count - 1) < (Cell.Column.Index + 1)) Then
            'Borrar el articulo, el Centro y el Almacen
            Dim bCentroEliminado As Boolean
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.CampoGS = TipoCampoGS.Centro And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    bCentroEliminado = True
                ElseIf oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                    If m_bValorListaGridSeleccionado Then 'Si se ha seleccionado algo en la combo unicamente
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    End If
                ElseIf oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 2 And bCentroEliminado Then
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    'Exit For
                ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                     ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                     blnBorrame = True
                ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                    If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                        Set oFila = ssLineas.ActiveRow
                        m_sValorCTablaExterna = ValorColumTablaExterna(0)
                        Set ssLineas.ActiveRow = oFila
                        If m_sValorCTablaExterna <> "" Then
                            tArray = Split(m_sValorCTablaExterna, "#")
                            For i = LBound(tArray) To UBound(tArray)
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                            Next
                        End If
                    End If
                End If
            Next
        Else
            If m_bValorListaGridSeleccionado Then 'Si se ha seleccionado algo en la combo unicamente
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        Exit For
                    End If
                Next
            End If
        End If
    '--------------------------------------------------------------------------------------------------------------------------------
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    'Cuando seleccionamos Centro --> Borrar el articulo y el Almacen
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
        If Not ((ssLineas.ActiveRow.Band.Columns.Count - 1) < (Cell.Column.Index + 1)) Then
            'Borra el Articulo y el Almacen
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                     ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                     blnBorrame = True
                ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                     If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                        Set oFila = ssLineas.ActiveRow
                        m_sValorCTablaExterna = ValorColumTablaExterna(0)
                        Set ssLineas.ActiveRow = oFila
                        If m_sValorCTablaExterna <> "" Then
                            tArray = Split(m_sValorCTablaExterna, "#")
                            For i = LBound(tArray) To UBound(tArray)
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                            Next
                        End If
                     End If
                End If
            Next
        End If
    End If
    
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ssLineas.Update
    cmdDeshacer.Enabled = True
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    
    '--------------------------------------------------------------------------------------------------------------------------------
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    'Cuando seleccionamos Articulo --> borramos TablaExterna relacionada con articulo
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
        ssLineas.ValueLists.Item("ListaArt").ValueListItems.clear
        If ssLineas.ValueLists.Exists("ListaArt") = False Then
            ssLineas.ValueLists.Add "ListaArt"
        End If
        ssLineas.Bands(0).Columns(Cell.Column.key).ValueList = "ListaArt"
        '_______________________________________________________________________
        If Not ((ssLineas.ActiveRow.Band.Columns.Count - 1) < (Cell.Column.Index + 1)) Then
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.TablaExterna <> "" And ssLineas.HasRows = True Then
                    If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                        Set oFila = ssLineas.ActiveRow
                        m_sValorCTablaExterna = ValorColumTablaExterna(0)
                        Set ssLineas.ActiveRow = oFila
                        If m_sValorCTablaExterna <> "" Then
                            tArray = Split(m_sValorCTablaExterna, "#")
                            For i = LBound(tArray) To UBound(tArray)
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                            Next
                        End If
                    End If
                End If
            Next
        End If
        '�����������������������������������������������������������������������
    '--------------------------------------------------------------------------------------------------------------------------------
    '* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    'Cuando seleccionamos DenArticulo -->
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        ssLineas.ValueLists.Item("ListaDen").ValueListItems.clear
        If ssLineas.ValueLists.Exists("ListaDen") = False Then
            ssLineas.ValueLists.Add "ListaDen"
        End If
        ssLineas.Bands(0).Columns(Cell.Column.key).ValueList = "ListaDen"
        
        If Not IsNull(sCodArt) Then
            If sCodArt <> "" Then
                If Not esGenerico(sCodArt) Then
                    ssLineas.ActiveCell.Column.Style = ssStyleDropDownList
                Else
                    ssLineas.ActiveCell.Column.Style = ssStyleDropDown
                End If
            End If
        End If
    End If
    
    m_bValorListaGridSeleccionado = False 'Una vez ya cerrado el combo se pone a false
End Sub

''' <summary>
''' Evento de sistema q responde a un cambio en una celda de un desglose
''' </summary>
''' <param name="Cell">Celda modificada</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub ssLineas_AfterCellUpdate(ByVal Cell As UltraGrid.SSCell)
Dim teserror As TipoErrorSummit
Dim sCod As String
Dim oIBaseDatos As IBaseDatos
Dim oLinea As CLineaDesglose
Dim oCampo As CFormItem
Dim bFormula As Boolean

    If ssLineas.ActiveRow Is Nothing Then Exit Sub
    If Cell.DataChanged = False Then Exit Sub
    If m_bCalculando = True Then Exit Sub
    
    If Cell.Column.key = "LINEA" Then
        Set oCampo = g_oCampoDesglose.Desglose.Item(1)
    Else
        Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3)))
    End If
    
    If oCampo Is Nothing Then Exit Sub
    
    
    Screen.MousePointer = vbHourglass
    
    sCod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
    
    Set oLinea = g_oCampoDesglose.LineasDesglose.Item(sCod)
    If oLinea Is Nothing Or Cell.Column.key = "LINEA" Then
        'Solo hay un caso en q se modifica la linea. A�ades, no tocas nada, deshaces y a�ades. Esto es en realidad
        'una inserci�n simple de linea no una modificaci�n.
        m_Accion = ACCLineaDesgloseAnyadir
        Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
        oLinea.Linea = Cell.Row.Cells("LINEA").Value
    Else
        m_Accion = ACCLineaDesgloseModif
    End If
    
    Set oLinea.CampoHijo = oCampo
    oLinea.valorBool = Null
    oLinea.valorFec = Null
    oLinea.valorText = Null
    oLinea.valorNum = Null
    
    If Cell.Column.key <> "LINEA" Then
        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
            Select Case oCampo.Tipo
                Case TipoTextoMedio
                    oLinea.valorText = Cell.TagVariant
                    oLinea.valorBool = Null: oLinea.valorNum = Null: oLinea.valorFec = Null
                Case TipoNumerico
                    oLinea.valorNum = Cell.TagVariant
                    oLinea.valorBool = Null: oLinea.valorText = Null: oLinea.valorFec = Null
                Case TipoFecha
                    oLinea.valorFec = Cell.TagVariant
                    oLinea.valorBool = Null: oLinea.valorText = Null: oLinea.valorNum = Null
            End Select
        Else
            If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                Select Case oCampo.CampoGS
                    Case TipoCampoSC.DescrBreve, TipoCampoSC.DescrDetallada, TipoCampoGS.Factura
                        oLinea.valorText = Cell.Value
                        
                    Case TipoCampoSC.importe, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario
                        oLinea.valorNum = Cell.Value
                        
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                        If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                            If oCampo.FECHAVALORDEFECTO = True Then
                                oLinea.valorFec = Cell.Value
                                oLinea.valorNum = Null
                            Else
                                oLinea.valorFec = Null
                                oLinea.valorNum = Cell.Value
                            End If
                        End If
                    
                    Case TipoCampoGS.Proveedor
                        If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                            oLinea.valorText = m_sProveCod
                        End If
                    Case TipoCampoGS.ProveedorERP
                        If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                            oLinea.valorText = Cell.Value
                        End If
                    Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                            oLinea.valorText = m_sValorPres
                        End If
                        
                    Case TipoCampoGS.Dest, TipoCampoGS.FormaPago, TipoCampoGS.Unidad, TipoCampoGS.Pais, TipoCampoGS.provincia, TipoCampoGS.Moneda, TipoCampoGS.UnidadPedido
                        oLinea.valorText = Cell.Value
                    
                    Case TipoCampoGS.material
                        If Cell.Value <> "" Then
                            oLinea.valorText = m_sMat
                        Else
                            oLinea.valorText = ""
                        End If
                    
                    Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                        If Not IsNull(Cell.Value) Then
                            If (UBound(Split(Cell.Value, " - ")) > 0) Then
                                oLinea.valorText = Trim(Mid$(Cell.Value, 1, InStr(1, Cell.Value, " - ")))
                            Else
                                oLinea.valorText = Cell.Value
                            End If
                        End If
                    Case TipoCampoGS.DenArticulo
                        If Not IsNull(Cell.Value) Then
                            oLinea.valorText = Cell.Value
                        End If
        
                    Case TipoCampoSC.ArchivoEspecific
                        oLinea.valorText = Cell.Value
                        
                    Case TipoCampoGS.CampoPersona
                        If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                            oLinea.valorText = m_sPerCod
                        End If
                    Case TipoCampoGS.UnidadOrganizativa, TipoCampoGS.Departamento
                        oLinea.valorText = Cell.Value
                    Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro
                        oLinea.valorText = Cell.Value
                    Case TipoCampoGS.Almacen, TipoCampoGS.Empresa
                        oLinea.valorNum = Cell.Value
                    Case TipoCampoGS.CentroCoste
                        If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                            oLinea.valorText = m_sCodCentroCoste
                        End If
                End Select
                
            Else
                oCampo.RecuperarFormatoPorId
                Select Case oCampo.Tipo
                    Case TiposDeAtributos.TipoBoolean
                        If Cell.Value = m_sIdiTrue Then
                            oLinea.valorBool = 1
                        ElseIf Cell.Value = m_sIdiFalse Then
                            oLinea.valorBool = 0
                        End If
                    Case TiposDeAtributos.TipoFecha
                        oLinea.valorFec = Cell.Value
                    Case TiposDeAtributos.TipoNumerico
                        oLinea.valorNum = Cell.Value
                    Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                        If oCampo.TipoIntroduccion = TAtributoIntroduccion.Introselec Then
                            oLinea.valorText = Cell.GetText(ssMaskModeIncludeBoth)
                            oLinea.valorNum = Cell.Value
                        Else
                            oLinea.valorText = Cell.Value
                        End If
                    Case TiposDeAtributos.TipoArchivo, TiposDeAtributos.TipoEditor, TiposDeAtributos.TipoEnlace
                        oLinea.valorText = Cell.Value
                End Select
            End If
        End If
    
        'Si se ha modificado un campo num�rico y hay f�rmulas se calculan sus valores:
        bFormula = False
        If basParametros.gParametrosGenerales.gsAccesoFSWS = AccesoFSWSCompleto Then
            If oCampo.Tipo = TiposDeAtributos.TipoNumerico And oCampo.TipoPredef <> TipoCampoPredefinido.Calculado And Not g_oCampoDesglose.Formulas Is Nothing Then
                If g_oCampoDesglose.Formulas.Count > 0 Then
                    If m_Accion = ACCLineaDesgloseAnyadir Then
                        g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oLinea.CampoHijo, oLinea.valorNum, oLinea.valorText, oLinea.valorFec, oLinea.valorBool, oLinea.FECACT
                    End If
            
                    bFormula = RecalcularValoresFormulas
                End If
            End If
        End If
    End If
    
    'Almacenan en base de datos
    If bFormula = True Then
        If m_Accion = ACCLineaDesgloseAnyadir Then
            g_oCampoDesglose.LineasDesglose.Item(sCod).Anyadido = True
        Else
            oLinea.modificado = True
        End If
        
        teserror = g_oCampoDesglose.LineasDesglose.ModificarValoresCalculados
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            Set oIBaseDatos = Nothing
            Exit Sub
        Else
            If m_Accion = ACCLineaDesgloseAnyadir Then
                RegistrarAccion ACCLineaDesgloseAnyadir, "Linea:" & oLinea.Linea & ",Campo=" & oLinea.CampoHijo.Id
            Else
                RegistrarAccion ACCLineaDesgloseModif, "Linea:" & oLinea.Linea & ",Campo=" & oLinea.CampoHijo.Id
            End If
            
        
            oLinea.modificado = False
            oLinea.Anyadido = False
            For Each oCampo In g_oCampoDesglose.Formulas
                sCod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                If Not g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                    If g_oCampoDesglose.LineasDesglose.Item(sCod).modificado = True Or g_oCampoDesglose.LineasDesglose.Item(sCod).Anyadido = True Then
                        g_oCampoDesglose.LineasDesglose.Item(sCod).modificado = False
                        g_oCampoDesglose.LineasDesglose.Item(sCod).Anyadido = False
                        m_bCalculando = True
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum
                        m_bCalculando = False
                    End If
                End If
            Next
            
            CalculoFormulaOrigen
        End If
        
    Else
    
        Set oIBaseDatos = oLinea
        If m_Accion = ACCLineaDesgloseAnyadir Then
            'Inserta en BD
            teserror = oIBaseDatos.AnyadirABaseDatos
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Exit Sub
            Else
                RegistrarAccion ACCLineaDesgloseAnyadir, "Linea:" & oLinea.Linea & ",Campo=" & oLinea.CampoHijo.Id
                If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                    g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oLinea.CampoHijo, oLinea.valorNum, oLinea.valorText, oLinea.valorFec, oLinea.valorBool, oLinea.FECACT
                End If
            End If
        
        Else
            'Modifica en BD
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Exit Sub
            Else
                teserror = oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCLineaDesgloseModif, "Linea:" & oLinea.Linea & ",Campo=" & oLinea.CampoHijo.Id
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Exit Sub
                End If
            End If
        End If
    End If
    
    Set oLinea = Nothing
    m_Accion = ACCDesgloseCons
    cmdDeshacer.Enabled = False
    Screen.MousePointer = vbNormal

End Sub


Private Sub ssLineas_BeforeCellListDropDown(ByVal Cell As UltraGrid.SSCell, ByVal Cancel As UltraGrid.SSReturnBoolean)

Dim oGMN4 As CGrupoMatNivel4
Dim oArticulos As CArticulos
Dim oArt As CArticulo
Dim i, contTerminar As Integer
Dim sMat As String
Dim arrMat As Variant
Dim oCampo As CFormItem
Dim oPais As CPais
Dim oProvi As CProvincia
Dim oDepartamentos As CDepartamentos
Dim oDepartamento As CDepartamento
Dim oCentro As CCentro
Dim oAlmacen As CAlmacen
Dim oCellAnt As SSCell
Dim sCodOrgCompras, sCodCentro As String
Dim bUsarOrgCompras As Boolean
Dim vAux As Variant
Dim oCampoMaterial As CFormItem
Dim oUON As IUon
    ReDim arrMat(4)
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))) Is Nothing Then Exit Sub
    
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        If gParametrosGenerales.gbUsarOrgCompras Then
            'Comprobar si a nivel del desglose hay campo de Organizacion de compras y Centro
            i = 1
            contTerminar = 0
            bUsarOrgCompras = False
            While i <= g_oCampoDesglose.Desglose.Count And contTerminar <> 1
                If g_oCampoDesglose.Desglose.Item(i).CampoGS = TipoCampoGS.OrganizacionCompras Then
                    If Not IsNull(ssLineas.ActiveRow.Cells(i).Value) Then
                        vAux = Split(ssLineas.ActiveRow.Cells(i).Value, " - ")
                        If UBound(vAux) > 0 Then
                            sCodOrgCompras = vAux(0)
                        Else
                            sCodOrgCompras = ssLineas.ActiveRow.Cells(i).Value
                        End If
                    End If
                    
                    If g_oCampoDesglose.Desglose.Item(i + 1).CampoGS = TipoCampoGS.Centro Then
                        If Not IsNull(ssLineas.ActiveRow.Cells(i + 1).Value) Then
                            vAux = Split(ssLineas.ActiveRow.Cells(i + 1).Value, " - ")
                            If UBound(vAux) > 0 Then
                                sCodCentro = vAux(0)
                            Else
                                sCodCentro = ssLineas.ActiveRow.Cells(i + 1).Value
                            End If
                        End If
                        
                        bUsarOrgCompras = True
                    End If
                    contTerminar = contTerminar + 1
                End If
                i = i + 1
            Wend
            'Sino Comprobar a nivel de Formularios
            If Not bUsarOrgCompras And g_sCodOrganizacionCompras <> "-1" Then
                i = 1
                contTerminar = 0
                If g_sCodOrganizacionCompras <> "-1" Then
                    sCodOrgCompras = g_sCodOrganizacionCompras
                    If g_sCodCentro <> "-1" Then
                        sCodCentro = g_sCodCentro
                        contTerminar = 1
                        bUsarOrgCompras = True
                    End If
                End If
                While i <= g_oCampoDesglose.Desglose.Count And contTerminar <> 1
                    If g_oCampoDesglose.Desglose.Item(i).CampoGS = TipoCampoGS.Centro Then
                        If Not IsNull(ssLineas.ActiveRow.Cells(i).Value) Then
                            vAux = Split(ssLineas.ActiveRow.Cells(i).Value, " - ")
                            If UBound(vAux) > 0 Then
                                sCodCentro = vAux(0)
                            Else
                                sCodCentro = ssLineas.ActiveRow.Cells(i).Value
                            End If
                        End If
                        contTerminar = contTerminar + 1
                        bUsarOrgCompras = True
                    End If
                    i = i + 1
                Wend
            
            End If
        End If
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            ssLineas.ValueLists.Item("ListaArt").ValueListItems.clear
        Else
            ssLineas.ValueLists.Item("ListaDen").ValueListItems.clear
        End If
        
        If Not (txtMaterial.Text <> "" And chkMaterial.Value = vbChecked) Then
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.CampoGS = TipoCampoGS.material Then
                    If g_oCampoDesglose.LineasDesglose.Item(CStr(ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id))) Is Nothing Then Exit Sub
                    sMat = NullToStr(g_oCampoDesglose.LineasDesglose.Item(CStr(ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id))).valorText)
                    
                    If sMat <> "" Then
                        arrMat = basPublic.DevolverMaterial(sMat)
                    Else
                        'Tarea 3495 - Campo Material a nivel de formulario
                        'Si no hemos seleccionado ning�n material en la linea pero tenemos un nivel de selecci�n obligatorio a nivel de formulario  solo deber�amos
                        'mostrar los art�culos de la familia seleccionada en el material del formulario.
                        'Si existe pero no tiene material seleccionado mostramos un mensaje para que el usuario rellene el material obligatorio.
                        If frmFormularios.g_oFormSeleccionado.ExisteNivelSeleccionMatObligatorio Then
                            Set oCampoMaterial = frmFormularios.g_oFormSeleccionado.getCampoTipoNivelSeleccionObligatoria()
                            If NoHayParametro(oCampoMaterial.GMN1Cod) Then
                                oMensajes.mensajeGenericoOkOnly m_sSeleccionarMatFormulario
                                Exit Sub
                            Else
                                
                                arrMat(1) = NullToStr(oCampoMaterial.GMN1Cod)
                                arrMat(2) = NullToStr(oCampoMaterial.GMN2Cod)
                                arrMat(3) = NullToStr(oCampoMaterial.GMN3Cod)
                                arrMat(4) = NullToStr(oCampoMaterial.GMN4Cod)
                            End If
                        End If
                    End If
                    Exit For
                End If
            Next
        Else
            arrMat(1) = m_sGMN1Cod
            arrMat(2) = m_sGMN2Cod
            arrMat(3) = m_sGMN3Cod
            arrMat(4) = m_sGMN4Cod
        End If
        
        
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGMN4.GMN1Cod = IIf(NoHayParametro(arrMat(1)), "", arrMat(1))
        oGMN4.GMN2Cod = IIf(NoHayParametro(arrMat(2)), "", arrMat(2))
        oGMN4.GMN3Cod = IIf(NoHayParametro(arrMat(3)), "", arrMat(3))
        oGMN4.GMN4Cod = IIf(NoHayParametro(arrMat(4)), "", arrMat(4))
        
        If Not IsEmpty(arrMat(4)) Then
            If bUsarOrgCompras Then
                oGMN4.CargarTodosLosArticulos , , , , , False, , , , , , , , , sCodOrgCompras, sCodCentro
            Else
                oGMN4.CargarTodosLosArticulos , , , , , False
            End If
        Else
            oGMN4.CargarTodosLosArticulos
        End If
        Set oArticulos = oGMN4.ARTICULOS
        Set oGMN4 = Nothing
                
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            For Each oArt In oArticulos
                ssLineas.ValueLists.Item("ListaArt").ValueListItems.Add oArt.Cod, oArt.Cod & " - " & oArt.Den
                ssLineas.ValueLists.Item("ListaArt").DisplayStyle = ssValueListDisplayStyleDisplayText
            Next
            Set Cell.Column.ValueList = ssLineas.ValueLists("ListaArt")
        Else
            For Each oArt In oArticulos
                ssLineas.ValueLists.Item("ListaDen").ValueListItems.Add oArt.Den, oArt.Cod & " - " & oArt.Den
                ssLineas.ValueLists.Item("ListaDen").DisplayStyle = ssValueListDisplayStyleDisplayText
                
            Next
            Set Cell.Column.ValueList = ssLineas.ValueLists("ListaDen")

        End If
        Set oArticulos = Nothing

    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.provincia Then
        ssLineas.ValueLists.Item("ListaProvi").ValueListItems.clear
    
        Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
          
        If oCellAnt.Value <> "" And Not IsNull(oCellAnt.Value) Then
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.Pais Then
                Set oPais = oFSGSRaiz.generar_CPais
                oPais.Cod = oCellAnt.Value
                oPais.CargarTodasLasProvincias
                    
                For Each oProvi In oPais.Provincias
                    ssLineas.ValueLists.Item("ListaProvi").ValueListItems.Add oProvi.Cod, oProvi.Cod & " - " & oProvi.Den
                Next
                Set oPais = Nothing
            End If
        End If
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaProvi")
        
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Departamento Then
        ssLineas.ValueLists.Item("ListaDepartamentos").ValueListItems.clear
        
        If (Cell.Row.Cells.Count - 1) > 1 Then
            Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
            If oCellAnt.Value <> "" And Not IsNull(oCellAnt.Value) Then
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.UnidadOrganizativa Then
                    Dim vUnidadOrganizativa As Variant
                    vUnidadOrganizativa = Split(oCellAnt.Value, " - ")
                    ReDim Preserve vUnidadOrganizativa(UBound(vUnidadOrganizativa) - 1)
                    ReDim Preserve vUnidadOrganizativa(3)
                    Set oUON = createUon(vUnidadOrganizativa(0), vUnidadOrganizativa(1), vUnidadOrganizativa(2))
                    
                    oUON.CargarTodosLosDepartamentos
                    For Each oDepartamento In oUON.Departamentos
                        ssLineas.ValueLists.Item("ListaDepartamentos").ValueListItems.Add oDepartamento.Cod, oDepartamento.Cod & " - " & oDepartamento.Den
                    Next
                    Set oUON = Nothing
                Else
                    'El campo Departamento es independiente a las unidades Organizativas
                    Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
                    oDepartamentos.CargarTodosLosDepartamentos
                    For Each oDepartamento In oDepartamentos
                        ssLineas.ValueLists.Item("ListaDepartamentos").ValueListItems.Add oDepartamento.Cod & " - " & oDepartamento.Den
                    Next
                    
                    Set oDepartamentos = Nothing
                End If
            End If
        Else
            'El campo Departamento es independiente a las unidades Organizativas
            Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
            oDepartamentos.CargarTodosLosDepartamentos
            For Each oDepartamento In oDepartamentos
                ssLineas.ValueLists.Item("ListaDepartamentos").ValueListItems.Add oDepartamento.Cod & " - " & oDepartamento.Den
            Next
                    
            Set oDepartamentos = Nothing
        End If
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaDepartamentos")
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.ProveedorERP Then
        Dim sCodProveedor As String
        Dim sOrgCompras As String
        sCodProveedor = DevuelveValueCampoGS(TipoCampoGS.Proveedor)
        sOrgCompras = DevuelveValueCampoGS(TipoCampoGS.OrganizacionCompras)
        ssLineas.ValueLists.Item("ListaProveedoresERP").ValueListItems.clear
        Dim oProvesERP As CProveERPs
        Dim oProveERP As CProveERP
        Set oProvesERP = oFSGSRaiz.Generar_CProveERPs
        If sCodProveedor <> "" And sOrgCompras <> "" Then
            oProvesERP.CargarProveedoresERP , sCodProveedor, , , sOrgCompras, True
        End If
        For Each oProveERP In oProvesERP
            ssLineas.ValueLists.Item("ListaProveedoresERP").ValueListItems.Add oProveERP.Cod, oProveERP.Cod & " - " & oProveERP.Den
        Next
        Set oProvesERP = Nothing
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaProveedoresERP")
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
        'Carga el Combo con los CENTROS
        '------------------------------
        ssLineas.ValueLists.Item("ListaCentros").ValueListItems.clear
        
        Dim oCentros As CCentros
        Set oCentros = oFSGSRaiz.Generar_CCentros
        
        
        If (Cell.Column.Index) > 1 Then
            Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
            
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.OrganizacionCompras Then
                If Not IsNull(oCellAnt.Value) Then
                    vAux = Split(oCellAnt.Value, " - ")
                    If UBound(vAux) > 0 Then
                        oCentros.CargarTodosLosCentros , vAux(0)
                    Else
                        oCentros.CargarTodosLosCentros , oCellAnt.Value
                    End If
                End If
            Else
                If g_sCodOrganizacionCompras <> "-1" Then
                    oCentros.CargarTodosLosCentros , g_sCodOrganizacionCompras
                Else
                    oCentros.CargarTodosLosCentros
                End If
            End If
            
        Else
            'El campo anterior al desglose es de tipo ORGANIZACION DE COMPRAS
            If g_sCodOrganizacionCompras <> "-1" Then
                oCentros.CargarTodosLosCentros , g_sCodOrganizacionCompras
            Else
                oCentros.CargarTodosLosCentros
            End If
        End If
        
        
               
        For Each oCentro In oCentros.Centros
            ssLineas.ValueLists.Item("ListaCentros").ValueListItems.Add oCentro.Cod, oCentro.Cod & " - " & oCentro.Den
        Next
        Set oCentros = Nothing
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaCentros")
        
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Almacen Then
        'Carga el Combo con los ALMACENES
        '------------------------------
        Dim oAlmacenes As CAlmacenes
        Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
        
        ssLineas.ValueLists.Item("ListaAlmacenes").ValueListItems.clear
        
        If (Cell.Column.Index) > 1 Then
            Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
                If Not IsNull(oCellAnt.Value) Then
                    vAux = Split(oCellAnt.Value, " - ")
                    If UBound(vAux) > 0 Then
                        oAlmacenes.CargarTodosLosAlmacenes , vAux(0)
                    Else
                        oAlmacenes.CargarTodosLosAlmacenes , oCellAnt.Value
                    End If
                End If
            ElseIf g_sCodCentro <> "-1" Then
                oAlmacenes.CargarTodosLosAlmacenes , g_sCodCentro
            Else
                oAlmacenes.CargarTodosLosAlmacenes
            End If
            
        ElseIf g_sCodCentro <> "-1" Then
            oAlmacenes.CargarTodosLosAlmacenes , g_sCodCentro
        Else
             oAlmacenes.CargarTodosLosAlmacenes
        End If
        
               
        For Each oAlmacen In oAlmacenes
            ssLineas.ValueLists.Item("ListaAlmacenes").ValueListItems.Add oAlmacen.Id, oAlmacen.Cod & " - " & oAlmacen.Den
        Next
        Set oAlmacenes = Nothing
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaAlmacenes")
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Empresa Then
        Dim oEmpresas As CEmpresas
        Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
        
        ssLineas.ValueLists.Item("ListaEmpresas").ValueListItems.clear
        
        Dim rs As Recordset
        Set rs = oEmpresas.DevolverTodasEmpresas(0)

        While Not rs.EOF
            ssLineas.ValueLists.Item("ListaEmpresas").ValueListItems.Add rs("ID").Value, rs("NIF").Value & " - " & rs("DEN").Value
            rs.MoveNext
        Wend
        Set oEmpresas = Nothing
    End If
End Sub
Private Sub ssLineas_BeforeCellUpdate(ByVal Cell As UltraGrid.SSCell, NewValue As Variant, ByVal Cancel As UltraGrid.SSReturnBoolean)
Dim oCampo As CFormItem
    
    If Me.ActiveControl.Name = "cmdDeshacer" Then
        If m_Accion <> ACCLineaDesgloseAnyadir Then
            Cancel = True
            cmdDeshacer.Enabled = False
            m_Accion = ACCDesgloseCons
        End If
        Exit Sub
    End If
    
    
    '******** REALIZA LAS COMPROBACIONES CORRESPONDIENTES ANTES DE GUARDAR **************
    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3)))
    If oCampo Is Nothing Then Exit Sub
    
    If Not (NewValue = "" Or IsNull(NewValue)) Then
        'Si es una lista comprueba que el valor introducido sea correcto:
        If Cell.Column.Style = ssStyleDropDown And Not (oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Or oCampo.CampoGS = TipoCampoGS.DenArticulo Or oCampo.CampoGS = TipoCampoGS.Unidad Or oCampo.CampoGS = TipoCampoGS.UnidadPedido) Then
            If ssLineas.ActiveCell.Column.ValueList.Find(NewValue, ssValueListFindDataValue) Is Nothing Then
                Cancel = True
                Exit Sub
            End If
        End If
         
        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
            'todo ok
            Exit Sub
         
        ElseIf oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            'Si son los campos de GS importe o cantidad:
            If oCampo.CampoGS = TipoCampoSC.importe Or oCampo.CampoGS = TipoCampoSC.Cantidad Or oCampo.CampoGS = TipoCampoSC.PrecioUnitario Then
                If Not IsNumeric(NewValue) Then
                    Screen.MousePointer = vbNormal
                    oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(1)
                    Cancel = True
                    Exit Sub
                End If
            End If
        ElseIf oCampo.TipoIntroduccion <> TAtributoIntroduccion.Introselec Then   'Introducci�n libre
            Select Case oCampo.Tipo
                Case TiposDeAtributos.TipoFecha
                    If Not IsDate(NewValue) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(2)
                        Cancel = True
                        Exit Sub
                    End If
                    'comprueba que el valor introducido est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(oCampo.Maximo) Then
                        If CDate(NewValue) > CDate(oCampo.Maximo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    If Not IsNull(oCampo.Minimo) Then
                        If CDate(NewValue) < CDate(oCampo.Minimo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    'La fecha no puede ser anterior a la fecha del sistema
                    If oCampo.NoFecAntSis Then
                        If IsDate(NewValue) Then
                            If CDbl(CVDate(NewValue)) < CDbl(CVDate(Date)) Then
                                Screen.MousePointer = vbNormal
                                oMensajes.NoFecAntSis
                                Cancel = True
                                Exit Sub
                            End If
                        End If
                    End If
                Case TiposDeAtributos.TipoNumerico
                    If Not IsNumeric(NewValue) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(1)
                        Cancel = True
                        Exit Sub
                    End If
                    'comprueba que el valor introducido est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(oCampo.Maximo) Then
                        If CDbl(NewValue) > CDbl(oCampo.Maximo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    If Not IsNull(oCampo.Minimo) Then
                        If CDbl(NewValue) < CDbl(oCampo.Minimo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    
                Case TiposDeAtributos.TipoBoolean
                    If UCase(NewValue) <> UCase(m_sIdiTrue) And UCase(NewValue) <> UCase(m_sIdiFalse) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(3)
                        Cancel = True
                        Exit Sub
                    End If
                    
            End Select
            If (oCampo.Formato <> "") Then
                If Not (validarFormato(oCampo.Formato, NewValue)) Then
                    Screen.MousePointer = vbNormal
                    oMensajes.NoValido NewValue & ". " & m_sMensaje(9)
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
    End If

End Sub

''' <summary>
''' Evento de sistema para q "Antes de que se mueva a otra fila hago las comprobaciones". Es decir, para no dejar
''' q meta una linea vacia.
''' </summary>
''' <param name="Cancel">Para cancelar el movimiento de linea</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub ssLineas_BeforeRowDeactivate(ByVal Cancel As UltraGrid.SSReturnBoolean)
    Dim oCelda As SSCell
    Dim bVacio As Boolean
    
    If ssLineas.ActiveRow.DataChanged = False Then Exit Sub
    
    bVacio = True
    If m_Accion = ACCLineaDesgloseAnyadir Then
        For Each oCelda In ssLineas.ActiveRow.Cells
            If oCelda.GetText(ssMaskModeIncludeBoth) <> "" Then
                If Not m_bDeshaciendo Then
                    bVacio = False
                    Exit For
                ElseIf oCelda.Column.key <> "LINEA" Then
                    bVacio = False
                    Exit For
                End If
            End If
        Next
        
        If bVacio = True Then
            If Not m_bDeshaciendo Then
                oMensajes.LineaVacia
                Cancel = True
                If Me.Visible Then ssLineas.SetFocus
                Exit Sub
            Else
                'No hacer ssLineas.ActiveRow.Delete da out of stack si a�ades tras deshacer
                Screen.MousePointer = vbHourglass
                Call g_oCampoDesglose.LineasDesglose.EliminarLineasDesglose(ssLineas.ActiveRow.Cells("LINEA").Value, g_oCampoDesglose.Id)
                Screen.MousePointer = vbNormal
            End If
        End If
        
        cmdDeshacer.Enabled = False
    End If
End Sub

Private Sub ssLineas_BeforeRowsDeleted(ByVal Rows As UltraGrid.SSSelectedRows, ByVal DisplayPromptMsg As UltraGrid.SSReturnBoolean, ByVal Cancel As UltraGrid.SSReturnBoolean)
    Dim FilaSiguiente As SSRow
    Dim sCod As String
    Dim oLinea As CLineaDesglose
    Dim teserror As TipoErrorSummit
    
    'para que no salte el mensaje de confirmaci�n de borrado de las filas
    DisplayPromptMsg = False

    'Elimina la l�nea de desglose de base de datos:
    If g_oCampoDesglose.LineasDesglose Is Nothing Then Exit Sub
    If ssLineas.ActiveRow Is Nothing Then Exit Sub
    
   
     
    Screen.MousePointer = vbHourglass
    teserror = g_oCampoDesglose.LineasDesglose.EliminarLineasDesglose(ssLineas.ActiveRow.Cells("LINEA").Value, g_oCampoDesglose.Id)
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
        
    Else
        'Elimina las lineas de la colecci�n de l�neas de desglose:
        For Each oLinea In g_oCampoDesglose.LineasDesglose
            If oLinea.Linea = ssLineas.ActiveRow.Cells("LINEA").Value Then
                sCod = oLinea.Linea & "$" & g_oCampoDesglose.Id & "$" & oLinea.CampoHijo.Id
                g_oCampoDesglose.LineasDesglose.Remove (sCod)
            End If
        Next
        
        RegistrarAccion ACCLineaDesgloseEliminar, "Linea:" & CStr(ssLineas.ActiveRow.Cells("LINEA").Value)
        
        'elimina una fila de la grid
        If ssLineas.ActiveRow.HasNextSibling = True Then
            'se situa en la siguiente del mismo nivel
            Set FilaSiguiente = ssLineas.ActiveRow.GetSibling(ssSiblingRowNext)
        ElseIf ssLineas.ActiveRow.HasPrevSibling = True Then
            Set FilaSiguiente = ssLineas.ActiveRow.GetSibling(ssSiblingRowPrevious)
        End If

        'ssLineas.ActiveRow.Delete

        If Not FilaSiguiente Is Nothing Then
            ssLineas.ActiveRow = FilaSiguiente
        End If

        If Me.Visible Then ssLineas.SetFocus

        'Calcula el total de la f�rmula origen
        If basParametros.gParametrosGenerales.gsAccesoFSWS = AccesoFSWSCompleto Then
            CalculoFormulaOrigen
        End If
        
        CargarValoresDesglose
        CargarTagTablasExternas
        
    End If


    m_Accion = ACCDesgloseCons
End Sub

Private Sub ssLineas_CellChange(ByVal Cell As UltraGrid.SSCell)
    Dim vMaxLength As Variant
    Dim bEdit As Boolean
    'Controlar los tipos TipoTextoCorto, TipoTextoMedio, TipoTextoLargo para que no superen el maximo permitido cuando se hace copy/paste
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
        If IsNull(vMaxLength) Or vMaxLength = "" Then
            vMaxLength = basParametros.gLongitudesDeCodigos.giLongCodDENART
        End If
        
        If (Len(ssLineas.ActiveCell.GetText) > CInt(vMaxLength)) Then
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If

            ssLineas.ActiveCell.Value = Left(ssLineas.ActiveCell.GetText, CInt(vMaxLength))

            If bEdit = True Then
                 ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        End If
    Else
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoMedio Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoLargo Then
            vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
            If IsNull(vMaxLength) Or vMaxLength = "" Then
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Then
                    vMaxLength = 100
                ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo Then
                    vMaxLength = 800
                Else
                    vMaxLength = 4000
                End If
            End If
            
            If (Len(ssLineas.ActiveCell.GetText) > CInt(vMaxLength)) Then
                bEdit = ssLineas.IsInEditMode
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionExitEditMode
                End If
    
                ssLineas.ActiveCell.Value = Left(ssLineas.ActiveCell.GetText, CInt(vMaxLength))
    
                If bEdit = True Then
                     ssLineas.PerformAction ssKeyActionEnterEditMode
                End If
            End If
        End If
    End If

    cmdDeshacer.Enabled = True
End Sub

''' <summary>
''' Evento que responde a cuando se selecciona un valor de un combo de la grid
''' </summary>
Private Sub ssLineas_CellListSelect(ByVal Cell As UltraGrid.SSCell)
    m_bValorListaGridSeleccionado = True
End Sub

''' <summary>
''' Evento que responde a un click en una celda
''' </summary>
''' <param name="Cell">Celda cliqueada</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub ssLineas_ClickCellButton(ByVal Cell As UltraGrid.SSCell)
    Dim oCampo As CFormItem
    Dim sCod As String
    Dim oLinea As CLineaDesglose
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim valor As Variant
    Dim sObs As String
    Dim sIdART As String
    
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))) Is Nothing Then Exit Sub
    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3)))
    
    
    If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
        Dim intTabla As Integer
        Dim strNombreTabla As String, strPrimerCampoNoPK As String, strPK As String
        Dim TipoDeDatoPK As TiposDeAtributos
        intTabla = oCampo.TablaExterna
        strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
        strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(intTabla)
        strPK = m_oTablaExterna.SacarPK(intTabla)
        TipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
        '_______________________________________________________________________
        If ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value <> "" Then
            valor = ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant
            Select Case TipoDeDatoPK
                Case TipoFecha
                    valor = CDate(valor)
                Case TipoNumerico
                    valor = CDbl(valor)
                'Case TipoTextoMedio
                '    valor = valor
            End Select
        End If
    
        '�����������������������������������������������������������������������
        '************************************************************************************
        
        
        Dim intLinea As Integer
        intLinea = ssLineas.ActiveRow.Cells("LINEA").Value
        
        Dim bookMarkAct As Variant
        bookMarkAct = ssLineas.ActiveRow.Bookmark
        
        If ExisteAtributoArticulo(intLinea) Then
            sIdART = DevuelveIdArticulo(intLinea)
        Else
            sIdART = ""
        End If
        
        Dim oRow As SSRow
        Set oRow = ssLineas.GetRowFromBookmark(bookMarkAct)
        
        ssLineas.ActiveRow = oRow
    
        m_strPK_old = g_strPK

        FSGSForm.MostrarFormTablaExterna oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, g_strPK, valor, _
                                         m_oTablaExterna.SacarDenominacionTabla(intTabla), strNombreTabla, strPK, sIdART, intTabla, "frmDesgloseValores"
                                         
        'aqui devuelve la primary key del registro en g_strPK
        'hay que sacar la primary key + el primer campo no primary key .lo buscamos:
        'colocamos en la columna la clave y el primer valor que no sea clave
        '_______________________________________________________________________
        If g_strPK = "vbFormControlMenu" Then
            'lo dejo como esta
            g_strPK = m_strPK_old
        ElseIf g_strPK <> "" Then
            Select Case TipoDeDatoPK
                Case TipoTextoMedio
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = g_strPK
                Case TipoFecha
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CDate(g_strPK)
                Case TipoNumerico
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CDbl(g_strPK)
            End Select
            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = g_strPK & " " & m_oTablaExterna.ValorCampo(g_strPK, strPK, strPrimerCampoNoPK, strNombreTabla, TipoDeDatoPK)
        Else    '?
            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = ""
        End If
        '�����������������������������������������������������������������������
    Else

        If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            'Si son campos de GS:
            Select Case oCampo.CampoGS
                Case TipoCampoSC.FecNecesidad, TipoCampoSC.FinSuministro, TipoCampoSC.IniSuministro
                    ''miramos si es FECHA_VALOR_DEFECTO O NO
                        Set frmCalendar.frmDestination = frmDesgloseValores
                        Set frmCalendar.ctrDestination = Nothing
                        frmCalendar.addtotop = 900 + 360
                        frmCalendar.addtoleft = 180
                        frmCalendar.Show vbModal
                Case TipoCampoSC.DescrDetallada
                    sCod = ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                    If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                        sObs = ""
                    Else
                        sObs = NullToStr(g_oCampoDesglose.LineasDesglose.Item(sCod).valorText)
                    End If
                    
                    If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den), m_bModif, -1, sObs) Then
                        ssLineas.ActiveCell.Value = Trim(sObs)
                    End If
                    
                Case TipoCampoGS.Proveedor
                    frmPROVEBuscar.sOrigen = "frmDesgloseValores"
                    If m_bRestrProve = True Then
                        frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
                    End If
                    frmPROVEBuscar.Show vbModal
                    
                Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                    Dim oCell As UltraGrid.SSCell
                    Dim oCampoAux As CFormItem
                    Dim dCantidad As Double
                    dCantidad = 0
                    For Each oCell In Cell.Row.Cells
                        If Not g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCell.Column.key, 3))) Is Nothing Then
                        
                        Set oCampoAux = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCell.Column.key, 3)))
                        If oCampoAux.CampoGS = TipoCampoSC.Cantidad Then
                            If Not IsNull(oCell.Value) Then
                                dCantidad = oCell.Value
                            End If
                            Exit For
                        End If
                        End If
                    Next
                                                            
                    sCod = CStr(Cell.Row.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & CStr(oCampo.Id)
                    m_sValorPres = ""
                    
                    If Not g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                        If Not IsNull(g_oCampoDesglose.LineasDesglose.Item(sCod).valorText) Then
                            m_sValorPres = g_oCampoDesglose.LineasDesglose.Item(sCod).valorText
                        End If
                    End If
                    If dCantidad > 0 Then
                        frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(m_sValorPres) * dCantidad / 100
                    Else
                        frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(m_sValorPres)
                    End If
                    frmPRESAsig.g_bHayPres = (frmPRESAsig.g_dblAsignado > 0)
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_sOrigen = "frmDesgloseValores"
                    frmPRESAsig.g_dblAbierto = dCantidad
                    Select Case oCampo.CampoGS
                        Case TipoCampoGS.Pres1
                            frmPRESAsig.g_iTipoPres = 1
                        Case TipoCampoGS.Pres2
                            frmPRESAsig.g_iTipoPres = 2
                        Case TipoCampoGS.Pres3
                            frmPRESAsig.g_iTipoPres = 3
                        Case TipoCampoGS.Pres4
                            frmPRESAsig.g_iTipoPres = 4
                    End Select
                    
                    
                    frmPRESAsig.g_bModif = m_bModif
                    frmPRESAsig.g_bSinPermisos = False
                    frmPRESAsig.g_bRUO = m_bRestrPresUO
                    frmPRESAsig.g_sValorPresFormulario = m_sValorPres
                    frmPRESAsig.Show vbModal
                    
                    
                                 
                Case TipoCampoGS.material
                    frmSELMAT.sOrigen = "frmDesgloseValoresGrid"
                    frmSELMAT.bRComprador = m_bRestrMat
                    If oCampo.Grupo.Formulario.ExisteNivelSeleccionMatObligatorio() Then
                        Dim oCampoMaterial As CFormItem
                        Set oCampoMaterial = frmFormularios.g_oFormSeleccionado.getCampoTipoNivelSeleccionObligatoria()
                        frmSELMAT.nivelSeleccion = oCampoMaterial.nivelSeleccion
                        
                        If NoHayParametro(oCampoMaterial.GMN1Cod) Then
                            oMensajes.mensajeGenericoOkOnly m_sSeleccionarMatFormulario
                            Exit Sub
                        Else
                            frmSELMAT.sGMN1 = NullToStr(oCampoMaterial.GMN1Cod)
                            frmSELMAT.sGMN2 = NullToStr(oCampoMaterial.GMN2Cod)
                            frmSELMAT.sGMN3 = NullToStr(oCampoMaterial.GMN3Cod)
                            frmSELMAT.sGMN4 = NullToStr(oCampoMaterial.GMN4Cod)
                        End If

                        Set oCampoMaterial = Nothing
                    Else
                        Me.limpiarFiltroMaterial
                        frmSELMAT.nivelSeleccion = 0
                    End If
                    
                    frmSELMAT.Show vbModal
                    
                Case TipoCampoSC.ArchivoEspecific
                    frmFormAdjuntos.g_sOrigen = "frmDesgloseValores"
                    sCod = CStr(Cell.Row.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & CStr(oCampo.Id)
                    If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                        Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
                        oLinea.Linea = Cell.Row.Cells("LINEA").Value
                        Set oLinea.CampoHijo = oCampo
                        Set oIBaseDatos = oLinea
                        teserror = oIBaseDatos.AnyadirABaseDatos
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Exit Sub
                        End If
                        g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oCampo, Null, Null, Null, Null, oLinea.FECACT
                        Set oLinea = Nothing
                        Set oIBaseDatos = Nothing
                    End If
                    Set frmFormAdjuntos.g_oLineaSeleccionada = g_oCampoDesglose.LineasDesglose.Item(sCod)
                    frmFormAdjuntos.g_bModif = m_bModif
                    frmFormAdjuntos.Show vbModal
                    
                Case TipoCampoGS.CampoPersona
                    frmSOLSelPersona.g_sOrigen = "frmDesgloseValores"
                    frmSOLSelPersona.bAllowSelUON = False
                    frmSOLSelPersona.bRDep = False 'No vamos a poner restricciones.
                    frmSOLSelPersona.bRUO = False 'No tiene sentido restringir al UO o Dep del que configura el Formulario.
                    frmSOLSelPersona.Show vbModal
                
                Case TipoCampoGS.UnidadOrganizativa
                    frmSELUO.sOrigen = "frmDesgloseValores"
                    frmSELUO.Show vbModal
                 
                Case TipoCampoGS.CentroCoste
                    frmSelCenCoste.g_sOrigen = "frmDesgloseValores"
                    frmSelCenCoste.g_bCentrosSM = False
                    frmSelCenCoste.g_bSaltarComprobacionArbol = True
                    Set frmSelCenCoste.g_oOrigen = Me
                    frmSelCenCoste.Show vbModal
                    
            End Select
                    
        Else
            'Si es un campo de tipo archivo
            If oCampo.Tipo = TiposDeAtributos.TipoArchivo Then
                frmFormAdjuntos.g_sOrigen = "frmDesgloseValores"
                sCod = CStr(Cell.Row.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & CStr(oCampo.Id)
                If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                    Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
                    oLinea.Linea = Cell.Row.Cells("LINEA").Value
                    Set oLinea.CampoHijo = oCampo
                    Set oIBaseDatos = oLinea
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oCampo, Null, Null, Null, Null, oLinea.FECACT
                    Set oLinea = Nothing
                    Set oIBaseDatos = Nothing
                End If
                Set frmFormAdjuntos.g_oLineaSeleccionada = g_oCampoDesglose.LineasDesglose.Item(sCod)
                frmFormAdjuntos.g_bModif = m_bModif
                frmFormAdjuntos.Show vbModal
                
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoFecha Then
                Set frmCalendar.frmDestination = frmDesgloseValores
                Set frmCalendar.ctrDestination = Nothing
                frmCalendar.addtotop = 900 + 360
                frmCalendar.addtoleft = 180
                If ssLineas.ActiveCell.Value <> "" Then
                    frmCalendar.Calendar.Value = ssLineas.ActiveCell.Value
                Else
                    frmCalendar.Calendar.Value = Date
                End If
                frmCalendar.Show vbModal
                ssLineas.Update
                
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoTextoLargo Then
                sCod = ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                    sObs = ""
                Else
                    sObs = NullToStr(g_oCampoDesglose.LineasDesglose.Item(sCod).valorText)
                End If
                                                                                                                           
                If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den), m_bModif, _
                                             IIf(IsNull(oCampo.MaxLength), -1, oCampo.MaxLength), sObs) Then
                    ssLineas.ActiveCell.Value = Trim(sObs)
                End If
                
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoTextoMedio Then
                sCod = ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                    sObs = ""
                Else
                    sObs = NullToStr(g_oCampoDesglose.LineasDesglose.Item(sCod).valorText)
                End If
                
                If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den), m_bModif, _
                                             IIf(IsNull(oCampo.MaxLength), 800, oCampo.MaxLength), sObs) Then
                    ssLineas.ActiveCell.Value = Trim(sObs)
                End If
                
            ElseIf oCampo.Tipo = TipoEditor Then
                Dim params As String
                Dim strSessionId As String
                Dim bReadOnly As Boolean
                strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
                                    
                params = "?sessionId=" & strSessionId & "&desdeGS=1&campo_hijo=" & CStr(oCampo.Id) & "&linea=" & ssLineas.ActiveRow.Cells("LINEA").Value
                If m_bModif Then
                    params = params & "&readOnly=0"
                    bReadOnly = False
                Else
                    params = params & "&readOnly=1"
                    bReadOnly = True
                End If
                            
                With frmEditor
                    .caption = NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                    .g_sOrigen = "frmDesgloseValores"
                    .g_sRuta = gParametrosGenerales.gsURLCampoEditor & params
                    .g_bReadOnly = bReadOnly
                    .Show vbModal
                End With
            End If
        End If
    End If
    'fin Tarea 811
    Set oCampo = Nothing
End Sub

Private Sub ssLineas_Error(ByVal ErrorInfo As UltraGrid.SSError)

    ErrorInfo.DisplayErrorDialog = False

End Sub

Private Sub ssLineas_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
Dim oCampo As CFormItem

    ssLineas.Bands(0).Columns("LINEA").Hidden = True
    ssLineas.Override.DefaultRowHeight = 239
    
     'Carga las cabeceras y los tipos de las columnas
    For Each oCampo In g_oCampoDesglose.Desglose
        'Cabecera:
        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Header.caption = NullToStr(oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
        
        If m_bModif = True Then
            
            If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Servicio Then
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = ssActivationDisabled
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                Select Case oCampo.CampoGS
                    Case TipoCampoSC.ArchivoEspecific, TipoCampoSC.DescrDetallada, TipoCampoGS.Proveedor, TipoCampoGS.CampoPersona
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                        
                    Case TipoCampoSC.DescrBreve
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        
                    Case TipoCampoSC.Cantidad, TipoCampoSC.importe, TipoCampoSC.PrecioUnitario
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        
                    Case TipoCampoGS.Dest, TipoCampoGS.FormaPago, TipoCampoGS.Unidad, TipoCampoGS.CodArticulo, TipoCampoGS.Pais, TipoCampoGS.provincia, TipoCampoGS.Moneda, TipoCampoGS.UnidadPedido, TipoCampoGS.ProveedorERP
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                        'ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        oCampo.CargarFECHAVALORDEFECTO
                        If oCampo.FECHAVALORDEFECTO = True Then
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        Else
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        End If
                        
                    Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                        
                    Case TipoCampoGS.material
                        If chkMaterial.Value = vbChecked Then
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        Else
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        End If
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    
                    Case TipoCampoGS.NuevoCodArticulo
                        oCampo.CargarAnyadirArt
                        If (oCampo.AnyadirArt) Then
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown  'deja modificar sin seleccionar del combo para introducir art�culos no codificados
                        Else
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDownList
                        End If
                    
                    Case TipoCampoGS.DenArticulo
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        
                    Case TipoCampoGS.UnidadOrganizativa
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        
                        'ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    Case TipoCampoGS.Departamento
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                    Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro, TipoCampoGS.Almacen, TipoCampoGS.ProveedorERP, TipoCampoGS.Empresa
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                    Case TipoCampoSC.PrecioUnitarioAdj, TipoCampoSC.TotalLineaAdj, TipoCampoSC.TotalLineaPreadj
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    Case TipoCampoSC.CantidadAdj
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    Case TipoCampoSC.ProveedorAdj
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    Case TipoCampoGS.Activo, TipoCampoGS.PartidaPresupuestaria, TipoCampoGS.AnyoImputacion
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = ssActivationDisabled
                    Case TipoCampoGS.CentroCoste
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                End Select
            
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
                'Si es un campo calculado no se podr� modificar:
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                
            Else  'es un campo normal o de selecci�n de lista de valores
                If oCampo.TipoIntroduccion = TAtributoIntroduccion.IntroLibre Then
                    Select Case oCampo.Tipo
                        Case TiposDeAtributos.TipoFecha
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Short Date"
                        Case TiposDeAtributos.TipoBoolean
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        Case TiposDeAtributos.TipoArchivo, TiposDeAtributos.TipoEditor
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                        Case TiposDeAtributos.TipoNumerico
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        Case TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        Case TiposDeAtributos.TipoEnlace
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                        Case Else
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                    End Select
                ElseIf oCampo.TipoIntroduccion = TAtributoIntroduccion.Introselec Then
                    'Selecci�n mediante una lista
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                    If oCampo.Tipo = TiposDeAtributos.TipoNumerico Then
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).CellAppearance.TextAlign = UltraGrid.ssAlignRight
                    Else
                        If oCampo.Tipo = TiposDeAtributos.TipoFecha Then
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Short Date"
                        End If
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).CellAppearance.TextAlign = UltraGrid.ssAlignLeft
                    End If
                Else
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = ssStyleEdit
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                End If
            End If
            
        Else
            'si no hay permisos de modificaci�n solo podr� ver los botones de archivos y descripci�n detallada:
            If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                If oCampo.CampoGS = TipoCampoSC.ArchivoEspecific Or oCampo.CampoGS = TipoCampoSC.DescrDetallada Or oCampo.CampoGS = TipoCampoGS.Desglose Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                Else
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                End If
            Else
                If oCampo.Tipo = TiposDeAtributos.TipoArchivo Or oCampo.Tipo = TiposDeAtributos.TipoTextoLargo Or oCampo.Tipo = TipoTextoMedio Or oCampo.Tipo = TipoEditor Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                Else
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                End If
            End If

        End If
        
        
        If ((ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count) < 1000 Then
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Width = 1000
        Else
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Width = (ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count
        End If
    Next

    ssLineas.Override.AllowGroupMoving = ssAllowGroupMovingNotAllowed
    ssLineas.Override.AllowColMoving = ssAllowColMovingNotAllowed
    
    If m_bModif = False Then
        ssLineas.Override.AllowUpdate = ssAllowUpdateNo
    End If
End Sub


Private Sub CrearValueList()
Dim oCampo As CFormItem
Dim oValorLista As CCampoValorLista
Dim ADORs As Ador.Recordset
Dim oPagos As CPagos
Dim oDestinos As CDestinos
Dim oPago As CPago
Dim oUnidades As CUnidades
Dim oUni As CUnidad
Dim i As Integer
Dim oPaises As CPaises
Dim oPais As CPais
Dim oMonedas As CMonedas
Dim oMoneda As CMoneda
Dim oOrganizacionCompras As COrganizacionCompras

    With ssLineas.ValueLists
        If ssLineas.ValueLists.Exists("ListaBool") = False Then
            .Add "ListaBool"
            .Item("ListaBool").ValueListItems.Add m_sIdiTrue
            .Item("ListaBool").ValueListItems.Add m_sIdiFalse
        End If
    End With
     
    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
        
            'todo ok
        ElseIf oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            Select Case oCampo.CampoGS
                Case TipoCampoGS.Dest
                    If ssLineas.ValueLists.Exists("ListaDestinos") = False Then
                        ssLineas.ValueLists.Add "ListaDestinos"
                        
                        Set oDestinos = oFSGSRaiz.Generar_CDestinos
                        If Not oUsuarioSummit.Persona Is Nothing Then
                            Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
                        Else
                            Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest)
                        End If
                        While Not ADORs.EOF
                            ssLineas.ValueLists.Item("ListaDestinos").ValueListItems.Add ADORs("DESTINOCOD").Value, ADORs("DESTINOCOD").Value & " - " & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value
                            ADORs.MoveNext
                        Wend
                        ADORs.Close
                        Set ADORs = Nothing
                        Set oDestinos = Nothing
                    End If
        
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaDestinos"
                    
                Case TipoCampoGS.FormaPago
                    If ssLineas.ValueLists.Exists("ListaFPago") = False Then
                        ssLineas.ValueLists.Add "ListaFPago"
                    
                        Set oPagos = oFSGSRaiz.generar_CPagos
                        oPagos.CargarTodosLosPagos
                        For Each oPago In oPagos
                            ssLineas.ValueLists.Item("ListaFPago").ValueListItems.Add oPago.Cod, oPago.Cod & " - " & oPago.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Next
                        Set oPagos = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaFPago"
                    
                Case TipoCampoGS.Unidad, TipoCampoGS.UnidadPedido
                    If ssLineas.ValueLists.Exists("ListaUnidad") = False Then
                        ssLineas.ValueLists.Add "ListaUnidad"
                        
                        Set oUnidades = oFSGSRaiz.Generar_CUnidades
                        oUnidades.CargarTodasLasUnidades , , , , False
                        For Each oUni In oUnidades
                            ssLineas.ValueLists.Item("ListaUnidad").ValueListItems.Add oUni.Cod, oUni.Cod & " - " & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Next
                        Set oUnidades = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaUnidad"
                
                Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                    If ssLineas.ValueLists.Exists("ListaArt") = False Then
                        ssLineas.ValueLists.Add "ListaArt"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaArt"
                    
                Case TipoCampoGS.DenArticulo
                    If ssLineas.ValueLists.Exists("ListaDen") = False Then
                        ssLineas.ValueLists.Add "ListaDen"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaDen"
                    
                    
                Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                    oCampo.CargarFECHAVALORDEFECTO
                    If oCampo.FECHAVALORDEFECTO = False Then
                        If ssLineas.ValueLists.Exists("ListaFecha") = False Then
                            ssLineas.ValueLists.Add "ListaFecha"
                            
                            For i = 1 To 17
                                ssLineas.ValueLists.Item("ListaFecha").ValueListItems.Add i, m_sIdiTipoFecha(i)
                            Next
                        End If
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaFecha"
                    End If
                    
                Case TipoCampoGS.Pais
                    If ssLineas.ValueLists.Exists("ListaPais") = False Then
                        ssLineas.ValueLists.Add "ListaPais"
                        
                        Set oPaises = oFSGSRaiz.Generar_CPaises
                        oPaises.CargarTodosLosPaises , , , True
                        For Each oPais In oPaises
                            ssLineas.ValueLists.Item("ListaPais").ValueListItems.Add oPais.Cod, oPais.Cod & " - " & oPais.Den
                        Next
                        Set oPaises = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaPais"
                    
                Case TipoCampoGS.provincia
                    If ssLineas.ValueLists.Exists("ListaProvi") = False Then
                        ssLineas.ValueLists.Add "ListaProvi"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaProvi"
                    
                Case TipoCampoGS.Moneda
                    If ssLineas.ValueLists.Exists("ListaMoneda") = False Then
                        ssLineas.ValueLists.Add "ListaMoneda"
                        
                        Set oMonedas = oFSGSRaiz.Generar_CMonedas
                        oMonedas.CargarTodasLasMonedas
                        For Each oMoneda In oMonedas
                            ssLineas.ValueLists.Item("ListaMoneda").ValueListItems.Add oMoneda.Cod, oMoneda.Cod & " - " & oMoneda.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Next
                        Set oMonedas = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaMoneda"
                    
                Case TipoCampoGS.Departamento
                    If ssLineas.ValueLists.Exists("ListaDepartamentos") = False Then
                        ssLineas.ValueLists.Add "ListaDepartamentos"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaDepartamentos"
                
                Case TipoCampoGS.OrganizacionCompras
                    If ssLineas.ValueLists.Exists("ListaOrganizacionCompras") = False Then
                        ssLineas.ValueLists.Add "ListaOrganizacionCompras"
                        
                        Dim oOrganizacionesCompras As COrganizacionesCompras
                        Set oOrganizacionesCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                        oOrganizacionesCompras.CargarOrganizacionesCompra
                        For Each oOrganizacionCompras In oOrganizacionesCompras.OrganizacionesCompras
                            ssLineas.ValueLists.Item("ListaOrganizacionCompras").ValueListItems.Add oOrganizacionCompras.Cod, oOrganizacionCompras.Cod & " - " & oOrganizacionCompras.Den
                        Next
                        Set oOrganizacionesCompras = Nothing
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaOrganizacionCompras"
                    
                Case TipoCampoGS.Centro
                    If ssLineas.ValueLists.Exists("ListaCentros") = False Then
                        ssLineas.ValueLists.Add "ListaCentros"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaCentros"
                Case TipoCampoGS.ProveedorERP
                    If ssLineas.ValueLists.Exists("ListaProveedoresERP") = False Then
                        ssLineas.ValueLists.Add "ListaProveedoresERP"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaProveedoresERP"
                Case TipoCampoGS.Almacen
                    If ssLineas.ValueLists.Exists("ListaAlmacenes") = False Then
                        ssLineas.ValueLists.Add "ListaAlmacenes"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaAlmacenes"
                Case TipoCampoGS.Empresa
                    If ssLineas.ValueLists.Exists("ListaEmpresas") = False Then
                        ssLineas.ValueLists.Add "ListaEmpresas"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaEmpresas"
            End Select
            
        Else
            If oCampo.TipoIntroduccion = TAtributoIntroduccion.Introselec Then  'Es una lista
                oCampo.CargarValoresLista
                If ssLineas.ValueLists.Exists("Lista" & oCampo.Id) = False Then
                    ssLineas.ValueLists.Add "Lista" & oCampo.Id
                End If
                For Each oValorLista In oCampo.ValoresLista
                    Select Case oCampo.Tipo
                        Case TiposDeAtributos.TipoFecha
                            ssLineas.ValueLists.Item("Lista" & oCampo.Id).ValueListItems.Add CDate(oValorLista.valorFec)
                        Case TiposDeAtributos.TipoNumerico
                            ssLineas.ValueLists.Item("Lista" & oCampo.Id).ValueListItems.Add oValorLista.valorNum
                        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            ssLineas.ValueLists.Item("Lista" & oCampo.Id).ValueListItems.Add oValorLista.Orden, oValorLista.valorText.Item(gParametrosInstalacion.gIdioma).Den
                    End Select
                Next
                
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "Lista" & oCampo.Id
                
            Else  'Introducci�n libre de tipo boolean
                If oCampo.Tipo = TiposDeAtributos.TipoBoolean Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaBool"
                End If
            End If
        End If
    Next
        
End Sub

Public Sub CargarProveedorConBusqueda()
    Dim oProves As CProveedores
    Dim oCampo As CFormItem
    
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing

    m_sProveCod = oProves.Item(1).Cod
    ssLineas.ActiveCell.Value = oProves.Item(1).Cod & " - " & oProves.Item(1).Den
    
    Set oProves = Nothing
    'Si al seleccionar un proveedor, en la linea del desglose esta el campo PROVEEDOR ERP se borrara si este tuviera un valor
    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
            Exit For
        End If
    Next
End Sub

Private Sub ssLineas_KeyDown(KeyCode As UltraGrid.SSReturnShort, Shift As Integer)

Dim tArray() As String
Dim i As Byte
Dim blnBorrame As Boolean
Dim bEdit As Boolean
Dim oCampo As CFormItem
Dim oFila As SSRow

    On Error Resume Next
    If m_bModif = False Then Exit Sub
    If ssLineas.ActiveCell Is Nothing Then Exit Sub
    If ssLineas.ActiveCell.Value = "" Then Exit Sub
    If ssLineas.Bands(0).Columns.Count < 0 Then Exit Sub
    
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Desglose Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoArchivo Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoEditor Then Exit Sub
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
 

        
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).TablaExterna <> "" Then
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If
            ssLineas.ActiveCell.TagVariant = ""
            ssLineas.ActiveCell.Value = ""
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        End If
        
        If ssLineas.ActiveCell.Column.Style = UltraGrid.ssStyleDropDown Or ssLineas.ActiveCell.Column.Style = UltraGrid.ssStyleEditButton Then
            
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then Exit Sub
            
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If
                
            '**************************************************************************************************************************************************************************************
            '**************************************************************************************************************************************************************************************
            'Si borramos el material borraremos tambi�n los art�culos:
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.material Then
              If Not IsNull(ssLineas.ActiveCell.Value) Then
                Dim arrMat As Variant
                Dim oArticulos As CArticulos
                Set oArticulos = oFSGSRaiz.Generar_CArticulos
                
                arrMat = DevolverMaterial(ssLineas.ActiveCell.Value)
                ssLineas.ActiveCell.Value = ""  'esto es para borrar material que a veces no lo hace
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                        If Not IsNull(ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value) Then
                            oArticulos.CargarTodosLosArticulos ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value, , True, , , , arrMat(1), arrMat(2), arrMat(3), arrMat(4)
                            If oArticulos.Count > 0 Then
                                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                                'Puesta a blanco de la denominaci�n
                                ssLineas.ActiveRow.Cells("C_" & (oCampo.Id + 1)).Value = ""
                                blnBorrame = True
                            End If
                        Else
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                            blnBorrame = True
                        End If
                        'Exit For
                    ElseIf oCampo.CampoGS = TipoCampoGS.CodArticulo Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        'Exit For
                    ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                        If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                            Set oFila = ssLineas.ActiveRow
                            m_sValorCTablaExterna = ValorColumTablaExterna(0)
                            Set ssLineas.ActiveRow = oFila
                            If m_sValorCTablaExterna <> "" Then
                                tArray = Split(m_sValorCTablaExterna, "#")
                                For i = LBound(tArray) To UBound(tArray)
                                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                                Next
                            End If
                        End If
                    End If
                Next
                Set oArticulos = Nothing
              End If
            '-------------------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.UnidadOrganizativa Then
                'Si borramos la UNIDAD ORGANIZATIVA borraremos tambi�n el DEPARTAMENTO:
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.Departamento And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        Exit For
                    End If
                Next
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Proveedor Then
                'Si borramos el campo PROVEEDOR y existe un campo PROVEEDOR ERP lo borraremos
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        Exit For
                    End If
                Next
            '-------------------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.OrganizacionCompras Then
                'Si borramos la ORGANIZACION de COMPRAS borraremos tambi�n el CENTRO  y  ALMAC�N, y si existe el campo de sistema PROVEEDOR ERP tambien lo borraremos:
                Dim bCentroEliminado As Boolean
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.Centro And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        bCentroEliminado = True
                    ElseIf oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    ElseIf oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 2 And bCentroEliminado Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        'Exit For
                    ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                         ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                         blnBorrame = True
                    ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                            If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                                Set oFila = ssLineas.ActiveRow
                                m_sValorCTablaExterna = ValorColumTablaExterna(0)
                                Set ssLineas.ActiveRow = oFila
                                If m_sValorCTablaExterna <> "" Then
                                    tArray = Split(m_sValorCTablaExterna, "#")
                                    For i = LBound(tArray) To UBound(tArray)
                                        ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                        ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                                    Next
                                End If
                            End If
                    End If
                Next
            '-------------------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
                'Si borramos el CENTRO borraremos tambi�n el ALMAC�N:
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        'Exit For
                    ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                         ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                         blnBorrame = True
                    ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                        If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                            Set oFila = ssLineas.ActiveRow
                            m_sValorCTablaExterna = ValorColumTablaExterna(0)
                            Set ssLineas.ActiveRow = oFila
                            If m_sValorCTablaExterna <> "" Then
                                tArray = Split(m_sValorCTablaExterna, "#")
                                For i = LBound(tArray) To UBound(tArray)
                                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                                Next
                            End If
                        End If
                    End If
                Next
            End If
            
            ssLineas.ActiveCell.Value = Null
            '**************************************************************************************************************************************************************************************
            '**************************************************************************************************************************************************************************************
        '���������������������������������������������������������������������������������������������������������������������������������������
        ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Pais Then
            'Si borramos el pa�s borraremos tambi�n la provincia:
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.CampoGS = TipoCampoGS.provincia Then
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    Exit For
                End If
            Next
            
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        '��������������������������������������������������������������������������������������������������������������������������������������
        ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If
            Set oFila = ssLineas.ActiveRow
            m_sValorCTablaExterna = ValorColumTablaExterna(0)
            Set ssLineas.ActiveRow = oFila
            If m_sValorCTablaExterna <> "" Then
                tArray = Split(m_sValorCTablaExterna, "#")
                For i = LBound(tArray) To UBound(tArray)
                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                Next
            End If
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        '���������������������������������������������������������������������������������������������������������������������������������������
        ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
                 
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If
            
            ssLineas.ActiveCell.Value = ""

            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
            ssLineas.ActiveCell.Column.Style = ssStyleDropDown
            
            Dim oCellSiguiente As SSCell
            Set oCellSiguiente = ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.Index - 1)
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
                oCellSiguiente.Value = ""
            End If
        End If
    End If
End Sub

Private Sub ModificarMaterial()
Dim oCampo As CFormItem
Dim oFila As SSRow
Dim oCampoArt  As CFormItem
Dim oCampoDen  As CFormItem
Dim oCampoMat As CFormItem
Dim arrMat As Variant
Dim sMat As String
    ReDim arrMat(4)
    If chkMaterial.Value = vbChecked And txtMaterial.Text <> "" Then
        'Limita los art�culos a un material en concreto
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.material Then
                Set oCampoMat = oCampo
            ElseIf oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                Set oCampoArt = oCampo
            ElseIf oCampo.CampoGS = TipoCampoGS.DenArticulo Then
                Set oCampoDen = oCampo
            End If
        Next
        
        If Not oCampoMat Is Nothing Then
            If Not ssLineas.Bands(0).Columns("C_" & oCampoMat.Id).Style = UltraGrid.ssStyleEdit Then
                ssLineas.Bands(0).Columns("C_" & oCampoMat.Id).Style = UltraGrid.ssStyleEdit
            End If
        End If
        
        'borra los materiales existentes y los art�culos correspondientes
        Set oFila = ssLineas.GetRow(ssChildRowFirst)
                
        While Not oFila Is Nothing
            Set ssLineas.ActiveRow = oFila
            
            If oCampoMat Is Nothing Then
                sMat = ""
            ElseIf Not g_oCampoDesglose.LineasDesglose.Item(CStr(oFila.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & oCampoMat.Id) Is Nothing Then
                sMat = NullToStr(g_oCampoDesglose.LineasDesglose.Item(CStr(oFila.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & oCampoMat.Id).valorText)
                If sMat <> "" Then
                    arrMat = basPublic.DevolverMaterial(sMat)
     
                    If arrMat(1) <> m_sGMN1Cod Or arrMat(2) <> m_sGMN2Cod Or arrMat(3) <> m_sGMN3Cod Or arrMat(4) <> m_sGMN4Cod Then
                        m_sMat = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod)) & m_sGMN1Cod
                        m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod)) & m_sGMN2Cod
                        m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod)) & m_sGMN3Cod
                        m_sMat = m_sMat & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod)) & m_sGMN4Cod
                        
                        If Not oCampoMat Is Nothing Then
                            oFila.Cells("C_" & oCampoMat.Id).Value = txtMaterial.Text
                        End If
                        oFila.Cells("C_" & oCampoArt.Id).Value = ""
                        oFila.Cells("C_" & oCampoDen.Id).Value = ""
                        ssLineas.Update
                    End If
                End If
            End If
            If oFila.HasNextSibling = True Then
                Set oFila = oFila.GetSibling(ssSiblingRowNext)
            Else
                Set oFila = Nothing
            End If
        Wend
        
    Else
        'Se podr� seleccionar cualquier material:
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.material Then
                If Not ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                End If
                Exit For
            End If
        Next
    End If
End Sub

Private Sub LimitarArticulosAGrupoMat()
    Dim teserror As TipoErrorSummit

    'Limita o no la introducci�n de l�neas por defecto:
    If m_bCargando = True Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    g_oCampoDesglose.GMN1Cod = Null
    g_oCampoDesglose.GMN2Cod = Null
    g_oCampoDesglose.GMN3Cod = Null
    g_oCampoDesglose.GMN4Cod = Null
        
    If chkMaterial.Value = vbChecked Then
        If Trim(txtMaterial.Text) <> "" Then
            g_oCampoDesglose.GMN1Cod = m_sGMN1Cod
            g_oCampoDesglose.GMN2Cod = m_sGMN2Cod
            g_oCampoDesglose.GMN3Cod = m_sGMN3Cod
            g_oCampoDesglose.GMN4Cod = m_sGMN4Cod
        End If
    End If
    
    teserror = g_oCampoDesglose.ModificarLimitarGrupoMat
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    'Modifica la grid:
    ModificarMaterial
    
End Sub


Private Function LiteralAdjuntos(ByVal oLinea As CLineaDesglose) As String
Dim sAdjuntos As String
Dim oAdjunto As CCampoAdjunto
    
    If m_bModif = False Then Exit Function
    
    sAdjuntos = ""
    For Each oAdjunto In oLinea.Adjuntos
        sAdjuntos = sAdjuntos & oAdjunto.nombre & " (" & TamanyoAdjuntos(oAdjunto.DataSize / 1024) & m_sIdiKB & ");"
    Next
    If sAdjuntos <> "" Then
        sAdjuntos = Mid$(sAdjuntos, 1, Len(sAdjuntos) - 1)
    End If
    
    LiteralAdjuntos = sAdjuntos
End Function


Public Function ModificarAdjuntos(ByVal oLinea As CLineaDesglose)
Dim sCod As String
    
    sCod = oLinea.Linea & "$" & CStr(oLinea.CampoHijo.CampoPadre.Id) & "$" & CStr(oLinea.CampoHijo.Id)
    
    If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
        g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oLinea.CampoHijo, Null, Null, Null, Null, oLinea.FECACT
    End If
    
    ssLineas.ActiveCell.Value = LiteralAdjuntos(oLinea)
End Function


Private Sub CalculoFormulaOrigen()
Dim sCod As String
Dim oCampo As CFormItem
Dim oCalculo As CFormItem
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim sVariables() As String
Dim dValues() As Double
Dim i As Integer
Dim oRow As SSRow
Dim j As Integer
Dim vbm As Variant
                    
    Set iEq = New USPExpression
    i = 1
    
    'Almacena las variables en un array:
    ReDim sVariables(g_oCampoDesglose.Formulas.Count)
    ReDim dValues(g_oCampoDesglose.Formulas.Count)
                    
    For Each oCalculo In g_oCampoDesglose.Formulas
        sVariables(i) = oCalculo.IdCalculo
        dValues(i) = 0
        Set oRow = ssLineas.GetRow(ssChildRowFirst)
        Do While Not oRow Is Nothing
            sCod = CStr(oRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCalculo.Id)
            If Not g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
                dValues(i) = dValues(i) + NullToDbl0(g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum)
            End If
            If oRow.HasNextSibling = True Then
                Set oRow = oRow.GetSibling(ssSiblingRowNext)
            Else
                Set oRow = Nothing
            End If
        Loop
        i = i + 1
    Next
    
    For Each oCampo In g_oCampoDesglose.Grupo.Campos
        If Not IsNull(oCampo.IdCalculo) And Not IsNull(oCampo.Formula) And oCampo.OrigenCalcDesglose = g_oCampoDesglose.Id Then
            lIndex = iEq.Parse(oCampo.Formula, sVariables, lErrCode)
            If lErrCode = USPEX_NO_ERROR Then
                 oCampo.valorNum = iEq.Evaluate(dValues)
                 'Actualiza en la grid de detalle:
                 If frmFormularios.g_oGrupoSeleccionado.Id = oCampo.Grupo.Id Then
                    For j = 0 To frmFormularios.sdbgCampos.Rows - 1
                        vbm = frmFormularios.sdbgCampos.AddItemBookmark(j)
                        If CLng(frmFormularios.sdbgCampos.Columns("ID").CellValue(vbm)) = CLng(oCampo.Id) Then
                            frmFormularios.sdbgCampos.Bookmark = vbm
                            frmFormularios.sdbgCampos.Columns("VALOR").Value = oCampo.valorNum
                            frmFormularios.sdbgCampos_Change
                            frmFormularios.sdbgCampos.Update
                            Exit For
                        End If
                    Next j
                 End If
            Else
                 oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                 Screen.MousePointer = vbNormal
                 Exit Sub
            End If
        End If
    Next
                    
    Set iEq = Nothing
    
End Sub


Private Function RecalcularValoresFormulas() As Boolean
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim sVariables() As String
Dim dValues() As Double
Dim i As Integer
Dim oCalculo As CFormItem
Dim Adores As Ador.Recordset
Dim bHayModif As Boolean
Dim sCod As String
Dim oLinea As CLineaDesglose

    'Si se ha modificado un campo num�rico se recalculan los campos calculados (si hay):
    
    Set iEq = New USPExpression
    i = 1
    'Almacena las variables en un array:
    ReDim sVariables(g_oCampoDesglose.Formulas.Count)
    ReDim dValues(g_oCampoDesglose.Formulas.Count)

    For Each oCalculo In g_oCampoDesglose.Formulas
        sVariables(i) = oCalculo.IdCalculo
        sCod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCalculo.Id)
        If Not g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then
            dValues(i) = NullToDbl0(g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum)
        Else
            dValues(i) = 0
        End If
        i = i + 1
    Next

    'Ahora realiza el c�lculo de cada f�rmula para la l�nea actual que se acaba de modificar:
    i = 1
    For Each oCalculo In g_oCampoDesglose.Formulas
        If oCalculo.TipoPredef = TipoCampoPredefinido.Calculado Then
            lIndex = iEq.Parse(oCalculo.Formula, sVariables, lErrCode)
            If lErrCode = USPEX_NO_ERROR Then
                dValues(i) = iEq.Evaluate(dValues)
                'almacena en la colecci�n:
                sCod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCalculo.Id)
                If g_oCampoDesglose.LineasDesglose.Item(sCod) Is Nothing Then  'Insertar
                    Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
                    oLinea.Linea = ssLineas.ActiveRow.Cells("LINEA").Value
                    Set oLinea.CampoHijo = oCalculo
                    g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oLinea.CampoHijo, dValues(i)
                    g_oCampoDesglose.LineasDesglose.Item(sCod).Anyadido = True
                    bHayModif = True
                Else  'Modificar
                    If g_oCampoDesglose.LineasDesglose.Item(sCod).Anyadido = False And g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum <> dValues(i) Then
                        g_oCampoDesglose.LineasDesglose.Item(sCod).modificado = True
                        g_oCampoDesglose.LineasDesglose.Item(sCod).valorNum = dValues(i)
                        bHayModif = True
                    End If
                End If
            End If
        End If
        i = i + 1
    Next

    Set iEq = Nothing
    
    RecalcularValoresFormulas = bHayModif
    
    Exit Function

EvaluationError:
    oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
    Set Adores = Nothing
    RecalcularValoresFormulas = False
End Function





 
Public Sub MostrarPresSeleccionado(ByVal sValor As String, ByVal iTipo As Integer)


m_sValorPres = sValor
ssLineas.ActiveCell.Value = GenerarDenominacionPresupuesto(sValor, iTipo)


End Sub



Private Function ObtenerPresupAsignado(ByVal sValor As Variant) As Double
Dim arrPresupuestos() As String

arrPresupuestos = Split(sValor, "#")

Dim oPresup As Variant
Dim arrPresup As Variant
Dim dPorcent As Double

For Each oPresup In arrPresupuestos
    arrPresup = Split(oPresup, "_")

    dPorcent = dPorcent + Numero(arrPresup(2), ".")
Next

ObtenerPresupAsignado = dPorcent * 100
End Function

Private Function DevolverMaterial(ByVal sMat As String) As Variant
Dim i As Integer
Dim arrMat(4) As String
Dim aux As Variant

    aux = Split(sMat, " - ")
    
    For i = 1 To 4
        arrMat(i) = ""
    Next i

    For i = 0 To UBound(aux) - 1
        arrMat(i + 1) = aux(i)
    Next i

    DevolverMaterial = arrMat
    
End Function

Public Sub MostrarUOSeleccionada()
Dim oCellSiguiente As SSCell

    If frmSELUO.sUON3 <> "" Then
        ssLineas.ActiveCell.Value = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
    ElseIf frmSELUO.sUON2 <> "" Then
        ssLineas.ActiveCell.Value = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
    ElseIf frmSELUO.sUON1 <> "" Then
        ssLineas.ActiveCell.Value = frmSELUO.sUON1 & " - " & frmSELUO.sDen
    ElseIf frmSELUO.sDen <> "" Then
        ssLineas.ActiveCell.Value = frmSELUO.sDen
    End If
   
   If (ssLineas.ActiveRow.Cells.Count - 1) > 1 Then
        Set oCellSiguiente = ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.Index + 1)
   
        'Se borra el contenido del departamento
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Departamento Then
        
            oCellSiguiente.Value = ""
        End If
    End If

    ssLineas.Update
    
End Sub

Private Function esGenerico(sCodArt As String, Optional bAnyadirArt As Boolean = False) As Boolean
    Dim oArticulos As CArticulos
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
 
    oArticulos.CargarTodosLosArticulos sCodArt, , True
    
    If oArticulos.Count > 0 Then
        esGenerico = CBool(oArticulos.Item(1).Generico)
    Else
        If bAnyadirArt Then
            esGenerico = True
        Else
            esGenerico = False
        End If
    End If
    
    Set oArticulos = Nothing
End Function


Private Sub ssLineas_KeyPress(KeyAscii As UltraGrid.SSReturnShort)
Dim vMaxLength As Variant

    If (ssLineas.ActiveCell Is Nothing) Or IsNull(ssLineas.ActiveCell) Then Exit Sub

    If KeyAscii = vbKeyBack Then Exit Sub
    If KeyAscii = vbKeyEscape Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub
    If IsNull(ssLineas.ActiveCell.GetText) Then Exit Sub
    
    
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
        If IsNull(vMaxLength) Or vMaxLength = "" Then
            vMaxLength = basParametros.gLongitudesDeCodigos.giLongCodDENART
        End If
        
        If (Len(ssLineas.ActiveCell.GetText) >= CInt(vMaxLength)) And ssLineas.ActiveCell.SelLength = 0 Then
            KeyAscii = 0
        End If
    
    Else
        'Control para que los tipos TipoTextoCorto, TipoTextoMedio y TipoTextoLargo no superen el m�ximo permitido
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoMedio Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoLargo Then
            vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
            If IsNull(vMaxLength) Or vMaxLength = "" Then
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Then
                    vMaxLength = 100
                ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo Then
                    vMaxLength = 800
                Else
                    vMaxLength = 4000
                End If
            End If
            If (Len(ssLineas.ActiveCell.GetText) >= CInt(vMaxLength)) And ssLineas.ActiveCell.SelLength = 0 Then
                KeyAscii = 0
            End If
        End If
    End If
    
End Sub


Private Function ExisteAtributoArticulo(piLinea As Integer) As Boolean
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim intContLineas As Integer
Dim strArticulo As String

    intContLineas = 0
    'ssLineas.ActiveRow.Cells("LINEA").Value = piLinea


    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                intContLineas = intContLineas + 1
                strArticulo = "" & ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value
                If piLinea = intContLineas And strArticulo <> "" Then
                    ExisteAtributoArticulo = True
                    Exit Function
                Else
                    ExisteAtributoArticulo = False
                End If
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
End Function



Private Function DevuelveIdArticulo(piLinea As Integer) As String
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim intContLineas As Integer
Dim strArticulo As String

    intContLineas = 0
    'ssLineas.ActiveRow.Cells("LINEA").Value = piLinea

    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                intContLineas = intContLineas + 1
                strArticulo = "" & ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value
                If piLinea = intContLineas And strArticulo <> "" Then
                    DevuelveIdArticulo = strArticulo
                    Exit Function
                Else
                    DevuelveIdArticulo = ""
                End If
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
End Function



'Funcion que devuelve el valor de esa linea del desglose del campo Gs que se pasa por parametro
Private Function DevuelveValueCampoGS(tipoGs As TipoCampoGS) As Variant
Dim oLineaDesglose As CLineaDesglose
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim ValueCampoGS As Variant


    Set oRow = ssLineas.ActiveRow
    
    ssLineas.ActiveRow = oRow
    'Comprobamos que el valor este en la linea del desglose
    For Each oLineaDesglose In g_oCampoDesglose.LineasDesglose
        If oLineaDesglose.CampoHijo.CampoGS = tipoGs And oRow.Cells("LINEA").Value = oLineaDesglose.Linea Then
            Select Case oLineaDesglose.CampoHijo.Tipo
                Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                    ValueCampoGS = NullToStr(oLineaDesglose.valorText)
                Case TiposDeAtributos.TipoBoolean
                    ValueCampoGS = oLineaDesglose.valorBool
                Case TiposDeAtributos.TipoNumerico
                    ValueCampoGS = NullToDbl0(oLineaDesglose.valorNum)
                Case TiposDeAtributos.TipoFecha
                    ValueCampoGS = oLineaDesglose.valorFec
            End Select
            Exit For
        End If
    Next
    'Si no se encuentra el valor en la linea del desglose, segun que campoGS sea,
    'se mirara si tiene valor a nivel de formulario en el grupo del desglose
    If ValueCampoGS = Empty Then
        Select Case tipoGs
            Case TipoCampoGS.OrganizacionCompras
                For Each oCampo In g_oCampoDesglose.Grupo.Campos
                    If oCampo.CampoGS = tipoGs Then
                        Select Case oCampo.Tipo
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                ValueCampoGS = NullToStr(oCampo.valorText)
                            Case TiposDeAtributos.TipoBoolean
                                ValueCampoGS = oCampo.valorBool
                            Case TiposDeAtributos.TipoNumerico
                                ValueCampoGS = NullToDbl0(oCampo.valorNum)
                            Case TiposDeAtributos.TipoFecha
                                ValueCampoGS = oCampo.valorFec
                        End Select
                        Exit For
                    End If
                Next
        End Select
    End If
    
    'Si no se ha encontrado en los campos del formulario del grupo seleccionado, ni en el propio desglose
    'se buscaria en el resto de grupos
    If ValueCampoGS = Empty Then
        Dim g As CFormGrupo
        Dim c As CFormItem
        
        If Not g_oCampoDesglose.Grupo.Formulario.Grupos Is Nothing Then
            For Each g In g_oCampoDesglose.Grupo.Formulario.Grupos
                If g.Campos Is Nothing Then
                    g.CargarTodosLosCampos
                End If
                
                If Not g.Campos Is Nothing And Not g.Id = g_oCampoDesglose.Grupo.Id Then
                    For Each c In g.Campos
                        If c.CampoGS = tipoGs Then
                            Select Case c.Tipo
                                Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                    ValueCampoGS = NullToStr(c.valorText)
                                Case TiposDeAtributos.TipoBoolean
                                    ValueCampoGS = c.valorBool
                                Case TiposDeAtributos.TipoNumerico
                                    ValueCampoGS = NullToDbl0(c.valorNum)
                                Case TiposDeAtributos.TipoFecha
                                    ValueCampoGS = c.valorFec
                            End Select
                            Exit For
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    DevuelveValueCampoGS = ValueCampoGS
    
End Function

'Esta funcion devuelve un string con los ids de los campos de tabla externa separados por "#"
Private Function ValorColumTablaExterna(pLinea) As String
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim intTabla As Integer
Dim strCampos As String

    On Error GoTo Error
    Set oRow = ssLineas.GetRow(pLinea)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.TablaExterna <> "" Then
                intTabla = CInt(oCampo.TablaExterna)
                If m_oTablaExterna.SacarTablaTieneART(intTabla) Then
                    If strCampos = "" Then
                        strCampos = CStr(oCampo.Id)
                    Else
                        strCampos = strCampos & "#" & CStr(oCampo.Id)
                    End If
                End If
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
    ValorColumTablaExterna = strCampos
    Exit Function
Error:
    ValorColumTablaExterna = ""
End Function

'Es necesario para el posicionamiento en frmTablasExternas
'y ponemos bien el value
Private Sub CargarTagTablasExternas()
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim sValorCampo
Dim tArray() As String
Dim intTabla As Integer
Dim strNombreTabla As String, strPK As String
Dim TipoDeDatoPK As TiposDeAtributos
    
    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.TablaExterna <> "" Then
                intTabla = CInt(oCampo.TablaExterna)
                strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
                strPK = m_oTablaExterna.SacarPK(intTabla)
                TipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
                '_______________________________________________________________________
                sValorCampo = ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value
                If Trim(sValorCampo) <> "#" Then
                    tArray = Split(sValorCampo, "#")
                    Select Case TipoDeDatoPK
                        Case TipoTextoMedio
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = tArray(1)
                        Case TipoFecha
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CDate(tArray(1))
                        Case TipoNumerico
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CDbl(tArray(1))
                    End Select
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = tArray(0)
                Else
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = ""
                End If
                '�����������������������������������������������������������������������
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
    ssLineas.Update
    
End Sub

Public Sub MostrarCentroCosteSeleccionado(ByRef oCCoste As CCentroCoste)
    Dim sValor As String
    Dim sCodValor As String
    
    oCCoste.ValorCentroCoste sValor, sCodValor
    
    m_sCodCentroCoste = sCodValor
    ssLineas.ActiveCell.Value = sValor
    ssLineas.Update
End Sub

Private Function validarFormato(ByVal tFormato As String, ByVal tValor As String) As Boolean

Dim rex As RegExp
Set rex = New RegExp

If (tFormato <> "") Then

    rex.Pattern = tFormato
    
    ' Set Case Insensitivity.
    rex.IgnoreCase = True
    
    'Set global applicability.
    rex.Global = True
    
    'Test whether the String can be compared.
    If (rex.Test(tValor) = True) Then
    
        validarFormato = True
    
    Else
        
        validarFormato = False
        
    End If

Else

        validarFormato = False

End If

End Function

Public Sub limpiarFiltroMaterial()

    txtMaterial.Text = ""
    
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    
    

End Sub
