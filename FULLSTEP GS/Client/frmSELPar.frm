VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSELPar 
   BackColor       =   &H00808000&
   Caption         =   "Selecci�n de partida contable"
   ClientHeight    =   5580
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7080
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSELPar.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5580
   ScaleWidth      =   7080
   Begin VB.PictureBox picSepar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   60
      ScaleHeight     =   285
      ScaleWidth      =   6795
      TabIndex        =   5
      Top             =   4770
      Width           =   6855
      Begin VB.TextBox txtObj 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   5880
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   0
         Width           =   915
      End
      Begin VB.TextBox txtPres 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   3420
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   0
         Width           =   1935
      End
      Begin VB.TextBox txtPartida 
         BackColor       =   &H80000018&
         Height          =   285
         Left            =   0
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   0
         Width           =   2775
      End
      Begin VB.Label lblObj 
         Caption         =   "Obj.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   5460
         TabIndex        =   7
         Top             =   60
         Width           =   495
      End
      Begin VB.Label lblPres 
         Caption         =   "Pres.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00400000&
         Height          =   225
         Left            =   2880
         TabIndex        =   6
         Top             =   60
         Width           =   495
      End
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2460
      TabIndex        =   1
      Top             =   5205
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   3645
      TabIndex        =   0
      Top             =   5205
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
      Height          =   315
      Left            =   750
      TabIndex        =   2
      Top             =   0
      Width           =   960
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   1693
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1693
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin MSComctlLib.TreeView tvwEstrPres 
      Height          =   4665
      Left            =   60
      TabIndex        =   3
      Top             =   90
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   8229
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2280
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPar.frx":0CB2
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPar.frx":177C
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPar.frx":1BCE
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPar.frx":2020
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPar.frx":2472
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSELPar.frx":28C4
            Key             =   "PRESBAJALOGICA"
            Object.Tag             =   "PRESBAJALOGICA"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblAnyo 
      BackColor       =   &H00808000&
      Caption         =   "A�o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   105
      TabIndex        =   4
      Top             =   75
      Width           =   570
   End
End
Attribute VB_Name = "frmSELPar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private iNivelMaximo As Integer

Public oPresupuestos As CPresContablesNivel1
' Variables para interactuar con otros forms

Public sOrigen As String
Public iAnyo As Integer
Public iAnyoDesde As Integer
Public iAnyoHasta As Integer

Public oParCon1Seleccionado As CPresConNivel1
Public oParCon2Seleccionado As CPresconNivel2
Public oParCon3Seleccionado As CPresConNivel3
Public oParCon4Seleccionado As CPresconNivel4

Private sIdiSel As String
Private sIdiDet As String

' Unidad organizativa seleccionada
Public m_sUON1Sel As String
Public m_sUON2Sel As String
Public m_sUON3Sel As String

Public m_bVerBajaLog As Boolean

' Variable de control de flujo
Public Accion As AccionesSummit

Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Screen.MousePointer = vbHourglass
        Select Case Left(nodx.Tag, 5)
        
            Case "PRES1"
                
                scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx)))
                Set oParCon1Seleccionado = oPresupuestos.Item(scod1)
                
            Case "PRES2"
                
                scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent)))
                scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx)))
                Set oParCon2Seleccionado = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2)
                                
            Case "PRES3"
                
                scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent)))
                scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx)))
                Set oParCon3Seleccionado = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3)
                
            Case "PRES4"
                
                scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent.Parent)))
                scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx.Parent)))
                scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(DevolverCod(nodx)))
                Set oParCon4Seleccionado = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4)
                
        End Select
        
        Screen.MousePointer = vbNormal
        
        Select Case sOrigen
                
            Case "InfAhorroApliPar"
                
                frmInfAhorroApliParCon.MostrarParConSeleccionada
            
            Case "frmLstPRESPorParCon"
                
                frmLstPRESPorParCon.MostrarParConSeleccionada
                
            Case "frmLstINFAhorroAplA4B2C4"
                frmListados.ofrmLstApliParCon.MostrarParConSeleccionada
                
            Case "frmLstINFAhorroAplfrmPARCON"
                frmInfAhorroApliParCon.ofrmLstApliParCon.MostrarParConSeleccionada
    
            Case "InfEvolPres"
                MDI.g_ofrmInfEvolPres2.MostrarParConSeleccionada
    
            Case "LstInfEvolPres2"
                MDI.g_ofrmInfEvolPres2.ofrmLstInfEvolPres2.MostrarParConSeleccionada
    
            Case "LstInfEvolPres4"
                frmListados.ofrmLstInfEvolPres4.MostrarParConSeleccionada
    
    End Select
    
    Unload Me
    
    End If

End Sub

Private Sub cmdCancelar_Click()
    
    
    Unload Me
    
End Sub

Private Sub Form_Activate()
    tvwEstrPres_NodeClick tvwEstrPres.selectedItem
    If Me.Visible Then tvwEstrPres.SetFocus
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SELPAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
        sIdiSel = Ador(0).Value
        Ador.MoveNext
        sIdiDet = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value
        Ador.MoveNext
        lblObj.caption = Ador(0).Value
        Ador.MoveNext
        lblPres.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    Me.caption = sIdiSel & " " & gParametrosGenerales.gsSingPres2
    
    CargarAnyos
    
    tvwEstrPres.Top = 315
    tvwEstrPres.Height = 4425
    
    If iAnyo = 0 Then
        sdbcAnyo.Text = Year(Date)
    Else
        sdbcAnyo.Text = iAnyo
    End If
    
    sdbcAnyo.ListAutoPosition = True
    
    DoEvents

    GenerarEstructuraPresupuestos False
        

End Sub

Private Sub GenerarEstructuraPresupuestos(ByVal bOrdenadoPorDen As Boolean)
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim oParCon1 As CPresConNivel1
Dim oParCon2 As CPresconNivel2
Dim oParCon3 As CPresConNivel3
Dim oParCon4 As CPresconNivel4

Dim nodx As MSComctlLib.node

iNivelMaximo = 0

tvwEstrPres.Nodes.clear

Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", gParametrosGenerales.gsPlurPres2, "Raiz")
nodx.Tag = "Raiz "

nodx.Expanded = True
    
    Set oPresupuestos = oFSGSRaiz.Generar_CPresContablesNivel1
   
    oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, m_sUON1Sel, m_sUON2Sel, m_sUON3Sel
    
    Select Case gParametrosGenerales.giNEPP
        
        Case 1
                
            For Each oParCon1 In oPresupuestos
                If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                    scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oParCon1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oParCon1.Cod
                    If oParCon1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                    
                    If iNivelMaximo < 1 Then
                        iNivelMaximo = 1
                    End If
                End If
            Next
        
        Case 2
            
            For Each oParCon1 In oPresupuestos
                If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                    scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oParCon1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oParCon1.Cod
                    If oParCon1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                
                For Each oParCon2 In oParCon1.PresContablesNivel2
                    If Not ((Not m_bVerBajaLog) And oParCon2.BajaLog) Then
                        scod2 = oParCon2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oParCon2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oParCon2.Cod & " - " & oParCon2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oParCon2.Cod
                        If oParCon2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                        If iNivelMaximo < 2 Then
                            iNivelMaximo = 2
                        End If
                    End If
                Next
            Next
        
        Case 3
            
            For Each oParCon1 In oPresupuestos
                If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                    scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oParCon1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oParCon1.Cod
                    If oParCon1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                
                For Each oParCon2 In oParCon1.PresContablesNivel2
                    If Not ((Not m_bVerBajaLog) And oParCon2.BajaLog) Then
                        scod2 = oParCon2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oParCon2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oParCon2.Cod & " - " & oParCon2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oParCon2.Cod
                        If oParCon2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                    End If
                    
                    For Each oParCon3 In oParCon2.PresContablesNivel3
                        If Not ((Not m_bVerBajaLog) And oParCon3.BajaLog) Then
                            scod3 = oParCon3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oParCon3.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oParCon3.Cod & " - " & oParCon3.Den, "PRES3")
                            nodx.Tag = "PRES3" & oParCon3.Cod
                            If oParCon3.BajaLog Then
                                nodx.Image = "PRESBAJALOGICA"
                                nodx.BackColor = &H8000000F 'color gris
                            End If

                            If iNivelMaximo < 3 Then
                                iNivelMaximo = 3
                            End If
                        End If
                    Next
                Next
            Next

        Case 4

            For Each oParCon1 In oPresupuestos
                If Not ((Not m_bVerBajaLog) And oParCon1.BajaLog) Then
                    scod1 = oParCon1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(oParCon1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, oParCon1.Cod & " - " & oParCon1.Den, "PRES1")
                    nodx.Tag = "PRES1" & oParCon1.Cod
                    If oParCon1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                
                For Each oParCon2 In oParCon1.PresContablesNivel2
                    If Not ((Not m_bVerBajaLog) And oParCon2.BajaLog) Then
                        scod2 = oParCon2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oParCon2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oParCon2.Cod & " - " & oParCon2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oParCon2.Cod
                        If oParCon2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                    End If

                    For Each oParCon3 In oParCon2.PresContablesNivel3
                        If Not ((Not m_bVerBajaLog) And oParCon3.BajaLog) Then
                            scod3 = oParCon3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oParCon3.Cod))
                            Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oParCon3.Cod & " - " & oParCon3.Den, "PRES3")
                            nodx.Tag = "PRES3" & oParCon3.Cod
                            If oParCon3.BajaLog Then
                                nodx.Image = "PRESBAJALOGICA"
                                nodx.BackColor = &H8000000F 'color gris
                            End If
                        End If
                        
                        For Each oParCon4 In oParCon3.PresContablesNivel4
                            If Not ((Not m_bVerBajaLog) And oParCon4.BajaLog) Then
                                scod4 = oParCon4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oParCon4.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oParCon4.Cod & " - " & oParCon4.Den, "PRES4")
                                nodx.Tag = "PRES4" & oParCon4.Cod
                                If oParCon4.BajaLog Then
                                    nodx.Image = "PRESBAJALOGICA"
                                    nodx.BackColor = &H8000000F 'color gris
                                End If

                                If iNivelMaximo < 4 Then
                                    iNivelMaximo = 4
                                End If
                            End If
                        Next
                    Next
                Next
            Next
    End Select

    Set oParCon1 = Nothing
    Set oParCon2 = Nothing
    Set oParCon3 = Nothing
    Set oParCon4 = Nothing

Exit Sub

Error:
    Set nodx = Nothing
    Resume Next
End Sub
Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    If sOrigen = "InfEvolPres" Or sOrigen = "LstInfEvolPres2" Or sOrigen = "LstInfEvolPres4" Then
        For iInd = iAnyoDesde To iAnyoHasta
            sdbcAnyo.AddItem iInd
        Next
        
        sdbcAnyo.Text = iAnyoDesde
        sdbcAnyo.ListAutoPosition = True
        
    Else
      
        iAnyoActual = Year(Date)
            
        For iInd = iAnyoActual - 10 To iAnyoActual + 10
            
            sdbcAnyo.AddItem iInd
            
        Next

        If iAnyo = 0 Then
            sdbcAnyo.Text = iAnyoActual
        Else
            sdbcAnyo.Text = iAnyo
        End If
        
        sdbcAnyo.ListAutoPosition = True

    End If
    
End Sub

Private Sub Form_Resize()

    If Me.Width > 250 Then tvwEstrPres.Width = Me.Width - 250
    If Me.Height >= 1600 Then tvwEstrPres.Height = Me.Height - 1600
    picSepar.Top = tvwEstrPres.Top + tvwEstrPres.Height
    tvwEstrPres.Left = 75
    picSepar.Left = tvwEstrPres.Left
    picSepar.Width = tvwEstrPres.Width

    cmdAceptar.Top = tvwEstrPres.Top + tvwEstrPres.Height + picSepar.Height + 50
    cmdCancelar.Top = cmdAceptar.Top

    cmdAceptar.Left = tvwEstrPres.Left + tvwEstrPres.Width / 2 - 1100
    cmdCancelar.Left = tvwEstrPres.Left + tvwEstrPres.Width / 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
        
    Set oPresupuestos = Nothing
    Set oParCon1Seleccionado = Nothing
    Set oParCon2Seleccionado = Nothing
    Set oParCon3Seleccionado = Nothing
    Set oParCon4Seleccionado = Nothing
    
    sOrigen = ""
    iAnyo = 0
    iAnyoDesde = 0
    iAnyoHasta = 0
    
    m_sUON1Sel = ""
    m_sUON2Sel = ""
    m_sUON3Sel = ""
        
End Sub

Private Sub sdbcAnyo_Click()
    
    tvwEstrPres.Nodes.clear
    GenerarEstructuraPresupuestos (False)
   
    
End Sub

Private Sub tvwEstrPres_Collapse(ByVal node As MSComctlLib.node)
    
    Set oParCon1Seleccionado = Nothing
    Set oParCon2Seleccionado = Nothing
    Set oParCon3Seleccionado = Nothing
    
    
    
    MostrarDatosBarraInf
    
End Sub

Private Sub tvwEstrPres_NodeClick(ByVal node As MSComctlLib.node)
    
    Set oParCon1Seleccionado = Nothing
    Set oParCon2Seleccionado = Nothing
    Set oParCon3Seleccionado = Nothing
    
    MostrarDatosBarraInf
    
End Sub
Public Sub MostrarDatosBarraInf()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String
Dim vImporte As Variant
Dim vObjetivo As Variant

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 5)
        
            Case "Raiz "
            
                txtPartida = ""
                txtPres = ""
                txtObj = ""
                
            Case "PRES1"
                scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx) & " (" & oPresupuestos.Item(scod1).Den & ")"
                vImporte = oPresupuestos.Item(scod1).importe
                vObjetivo = oPresupuestos.Item(scod1).Objetivo

                
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                        txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
            
            Case "PRES2"
                
                scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent)))
                scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx)))
                
                txtPartida = DevolverCod(nodx.Parent) & " (" & oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe
                vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Objetivo
                
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                    txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
                
            Case "PRES3"
                
                scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent)))
                scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx.Parent.Parent) & " (" & oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe
                vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Objetivo
                    
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                    txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
                
                
            Case "PRES4"
                
                scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent.Parent)))
                scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx.Parent)))
                scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx.Parent.Parent.Parent) & " (" & oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                
                If Not IsMissing(vImporte) And Not IsNull(vImporte) Then
                    txtPres = Format(DblToStr(vImporte), "Standard")
                Else
                    txtPres = ""
                End If
                
                If Not IsMissing(vObjetivo) And Not IsNull(vObjetivo) Then
                    txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
                Else
                    txtObj = ""
                End If
                
        End Select
    
    End If

End Sub
Private Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function

Select Case Left(node.Tag, 5)

Case "PRES1"
        
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)
    
Case "PRES2"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)
    
Case "PRES3"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)

Case "PRES4"
    
        DevolverCod = Right(node.Tag, Len(node.Tag) - 5)

End Select

End Function

