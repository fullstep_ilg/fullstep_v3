VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosEtapasObservador 
   BackColor       =   &H00808000&
   Caption         =   "Form1"
   ClientHeight    =   4530
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5670
   ForeColor       =   &H00000000&
   Icon            =   "frmFlujosEtapasObservador.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4530
   ScaleWidth      =   5670
   StartUpPosition =   2  'CenterScreen
   Begin SSDataWidgets_B.SSDBGrid sdbgEtapas 
      Height          =   3975
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   5340
      _Version        =   196617
      DataMode        =   2
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosEtapasObservador.frx":014A
      UseGroups       =   -1  'True
      DividerType     =   0
      BevelColorHighlight=   16777215
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Groups(0).Width =   9419
      Groups(0).Caption=   "Etapas"
      Groups(0).CaptionAlignment=   0
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   873
      Groups(0).Columns(0).Name=   "ETAPA_CHK"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   11
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).Style=   2
      Groups(0).Columns(1).Width=   3200
      Groups(0).Columns(1).Visible=   0   'False
      Groups(0).Columns(1).Caption=   "ETAPA_ID"
      Groups(0).Columns(1).Name=   "ETAPA_ID"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(2).Width=   8546
      Groups(0).Columns(2).Caption=   "ETAPA_DEN"
      Groups(0).Columns(2).Name=   "ETAPA_DEN"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(2).Locked=   -1  'True
      _ExtentX        =   9419
      _ExtentY        =   7011
      _StockProps     =   79
      ForeColor       =   -2147483630
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblTexto 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      Caption         =   "dIndique a que etapas tendr� acceso el rol observador:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   3915
   End
End
Attribute VB_Name = "frmFlujosEtapasObservador"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_lIdWF As Long
Public m_lIdRol As Long
Public m_sDenRol As String


Private Sub Form_Load()
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    CargarEtapas
    
End Sub


Private Sub CargarEtapas()

    Dim oRS As New ADODB.Recordset
    Dim oBloques As CBloques
    Set oBloques = oFSGSRaiz.Generar_CBloques
    
    Set oRS = oBloques.DevolverBloquesObservador(m_lIdRol, basPublic.gParametrosInstalacion.gIdioma, m_lIdWF)
    While Not oRS.EOF
        sdbgEtapas.AddItem oRS("SELEC") & Chr(m_lSeparador) & oRS("ID") & Chr(m_lSeparador) & oRS("DEN").Value
        oRS.MoveNext
    Wend

    oRS.Close
    Set oRS = Nothing
    Set oBloques = Nothing

End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSETAPASOBSERVADOR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value & " " & m_sDenRol '1 Etapas para el rol observador:
        Ador.MoveNext
        Me.lblTexto.caption = Ador(0).Value '2 Indique a que etapas tendr� acceso el rol observador:
        Ador.MoveNext
        Me.sdbgEtapas.Groups(0).caption = Ador(0).Value '3 Etapas
                
        Ador.Close
    End If

End Sub

Private Sub sdbgEtapas_Change()
Dim oBloques As CBloques
Dim TESError As TipoErrorSummit
    Set oBloques = oFSGSRaiz.Generar_CBloques
    Select Case sdbgEtapas.Columns(sdbgEtapas.Col).Name
        Case "ETAPA_CHK"
            If sdbgEtapas.Columns("ETAPA_CHK").Value <> "" Then
                If CBool(sdbgEtapas.Columns("ETAPA_CHK").Value) Then
                    'A�adir la etapa al observador
                    TESError = oBloques.AsignarEtapasObservador(m_lIdRol, m_lIdWF, sdbgEtapas.Columns("ETAPA_ID").Value)
                Else
                    'Eliminar la etapa al observador
                    TESError = oBloques.DesAsignarEtapaObservador(m_lIdRol, sdbgEtapas.Columns("ETAPA_ID").Value)
                End If
                If TESError.NumError = TESnoerror Then
                    'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                    frmFlujos.HayCambios
                End If
            End If
    End Select
    
Set oBloques = Nothing
End Sub

