Attribute VB_Name = "basExcel"

''' <summary>
''' Generacion del excel de una oferta
''' </summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde:cmdgenerarXLS Tiempo m�ximo:0</remarks>

Public Function FSXLSOfertaOffline(ByRef oProcesoSeleccionado As CProceso, oProveSeleccionado As String, ByRef oAsigs As CAsignaciones, sTarget As String, Optional ByRef oOfertasProceso As COfertas, Optional UTC As String = "") As Boolean
Dim sConnect As String
Dim oExcelAdoConn As ADODB.Connection

Dim adoComm As ADODB.Command
Dim adoParam As ADODB.Parameter
Dim SQL As String

Dim xls() As String

Dim iNumCols As Integer
Dim oGrupo As CGrupo
Dim oOfeGrupo As COfertaGrupo
Dim oatrib As CAtributo
Dim oItem As CItem
Dim oEsp As CEspecificacion
Dim oValor As CValorPond
Dim oAtribsOfe As CAtributosOfertados
Dim i As Long
Dim oMon As CMoneda
Dim iFila As Integer
Dim iFilaInicio As Integer
Dim iNumFilasPorAtrib As Integer
Dim oAtribEsp As CAtributo
Dim lCountAtrEspeGrupo As Long

Dim oProceso As CProceso
Dim oOferta As COferta
Dim oPrecioItem As CPrecioItem

Dim lCountEspecsGrupo  As Long
Dim sPathXls As String
Dim sCod As String
Dim scodProve As String
Dim bCargar As Boolean
Dim lNumGrupos As Long

Dim oEscalado As CEscalado

Dim FechaHora As Date
Dim sUsuario As String
Dim sUsuarioAux As String
Dim sUsuariom As String
Dim sUsuarioh As String
Dim lUsuario As Long

On Error GoTo Error


iNumFilasPorAtrib = 13

Set oProceso = oProcesoSeleccionado


Screen.MousePointer = vbHourglass

Set oOferta = oProceso.CargarUltimaOfertaProveedor(oProveSeleccionado)

If oOferta Is Nothing Then
    Set oOferta = oFSGSRaiz.Generar_COferta
    oOferta.Anyo = oProceso.Anyo
    oOferta.GMN1Cod = oProceso.GMN1Cod
    oOferta.Proce = oProceso.Cod
    oOferta.Prove = oProveSeleccionado
    oOferta.Num = 1
    oOferta.CodMon = oProceso.MonCod
Else
    sCod = oProveSeleccionado & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProveSeleccionado))
    sCod = sCod & CStr(oOferta.Num)
    
    If Not oOfertasProceso Is Nothing Then
        Set oOferta = oOfertasProceso.Item(sCod)
    End If
End If

oProceso.CargarTodasLasEspecificaciones
oProceso.CargarAEspecificacionesP
oProceso.CargarTodosLosGrupos bSinpres:=True
oOferta.CargarDatosGeneralesOferta basPublic.gParametrosInstalacion.gIdioma
oOferta.CargarAtributosProcesoOfertados
oProceso.CargarAtributos bsoloexternos:=True
oOferta.CargarAdjuntos
scodProve = oProveSeleccionado & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProveSeleccionado))
lNumGrupos = 0
For Each oGrupo In oProceso.Grupos
    bCargar = True
    If gParametrosGenerales.gbProveGrupos Then
        If oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
            bCargar = False
        End If
    End If
    If bCargar Then
        lNumGrupos = lNumGrupos + 1
        oGrupo.CargarTodosLosItems OrdItemPorOrden, bSoloConfirmados:=True
        oGrupo.CargarEspGeneral
        oGrupo.CargarTodasLasEspecificaciones
        oGrupo.CargarAEspecificacionesG
    End If
Next

If oOferta.Grupos Is Nothing Then
    Set oOferta.Grupos = oFSGSRaiz.Generar_COfertaGrupos
    For Each oGrupo In oProceso.Grupos
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            If oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
            oOferta.Grupos.Add oOferta, oGrupo
        End If
    Next
End If

For Each oOfeGrupo In oOferta.Grupos
    oOfeGrupo.Grupo.CargarAtributos ambito:=AmbGrupo, bsoloexternos:=True
    oOfeGrupo.CargarAtributosOfertados
    oOfeGrupo.Grupo.CargarAtributos ambito:=AmbItem, bsoloexternos:=True
    oOfeGrupo.Grupo.CargarEscalados
    oOfeGrupo.CargarPrecios False, (oOfeGrupo.Grupo.UsarEscalados = 1)
 Next

iFila = 2

iNumCols = 6 + lNumGrupos

ReDim xls(0 To iNumCols - 1, 0 To 68)

Dim Ador As Ador.Recordset

Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_OFEREC_IDIS, basPublic.gParametrosInstalacion.gIdioma)

i = 0
While Not Ador.EOF
    xls(0, i) = Ador(0).Value
    i = i + 1
    Ador.MoveNext
Wend

Ador.Close
Set Ador = Nothing

'Datos del proceso
xls(1, 0) = oProceso.Anyo
xls(1, 1) = oProceso.GMN1Cod
xls(1, 2) = oProceso.Cod
xls(1, 3) = oProceso.Den

Dim dt As Date
If dt <> oOferta.FechaRec Then
    xls(1, 4) = "" & Format(oOferta.FechaRec, "DD\/MM\/YYYY")
Else
    xls(1, 4) = ""
End If

xls(1, 5) = NullToStr(oOferta.TotEnviadas) ' N� ofertas enviadas
xls(1, 6) = 1 ' Es �ltima oferta

xls(1, 7) = oProceso.DefDestino
xls(1, 8) = NullToStr(oProceso.DestCod)
xls(1, 9) = NullToStr(oProceso.DestDen)
xls(1, 10) = NullToStr(oProceso.DefFechasSum)

xls(1, 11) = "" & Format(oProceso.FechaInicioSuministro, "DD\/MM\/YYYY")
xls(1, 12) = "" & Format(oProceso.FechaFinSuministro, "DD\/MM\/YYYY")
xls(1, 13) = NullToStr(oProceso.DefFormaPago)
xls(1, 14) = NullToStr(oProceso.PagCod)
xls(1, 15) = NullToStr(oProceso.PagDen)
xls(1, 16) = lNumGrupos

If Not (UTC = "") Then
    sUsuario = Replace(UTC, "UTC", "")
    sUsuarioAux = Right(sUsuario, Len(sUsuario) - 1)
    sUsuariom = Right(sUsuarioAux, 2)
    sUsuarioh = Left(sUsuarioAux, 2)
    lUsuario = CLng(sUsuarioh) * 60 + CLng(sUsuariom)
    
    If Left(sUsuario, 1) = "-" Then
        lUsuario = lUsuario * (-1)
    End If
Else
    lUsuario = 0
End If

If Not IsNull(oProceso.FechaMinimoLimOfertas) Then
    FechaHora = oProceso.FechaMinimoLimOfertas + (lUsuario / 1440)

    xls(1, 17) = "" & Format(FechaHora, "DD\/MM\/YYYY HH\:NN") & IIf(UTC = "", "", " (" & UTC & ")")
Else
    xls(1, 17) = ""
End If

xls(1, 18) = NullToStr(oProceso.esp)
xls(1, 19) = oProceso.especificaciones.Count


'DPD En el portal se a�aden dos nuevos campos lFifServ y lAuxH,(para el control de diferencia horaria) ocupando las posiciones 20 y 21
' Por lo tanto respetamos esas dos posiciones y desplzamos las filas siguientes dos posiciones
'i = 20
i = 22

If Not oProceso.especificaciones Is Nothing Then
    For Each oEsp In oProceso.especificaciones
        xls(1, i) = oEsp.nombre
        i = i + 1
        xls(1, i) = oEsp.Comentario
        i = i + 1
    Next
End If

Dim oPagos As CPagos
Set oPagos = oFSGSRaiz.generar_CPagos
oPagos.CargarTodosLosPagos

Dim oUnidades As CUnidades

Set oUnidades = oFSGSRaiz.Generar_CUnidades
oUnidades.CargarTodasLasUnidades


Dim oPaises As CPaises
Set oPaises = oFSGSRaiz.Generar_CPaises
oPaises.CargarTodosLosPaises


Dim oDestino As CDestino
Dim oDestinos As CDestinos
Set oDestinos = oFSGSRaiz.Generar_CDestinos
oDestinos.CargarTodosLosDestinosDesde gParametrosInstalacion.giCargaMaximaCombos, sintrans:=True

If oProceso.DefDestino = EnProceso Then
    Set oDestino = oDestinos.Item(oProceso.DestCod)
    
    xls(1, i) = oDestino.Cod
    i = i + 1
    xls(1, i) = oDestino.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    i = i + 1
    xls(1, i) = oDestino.dir
    i = i + 1
    xls(1, i) = oDestino.POB
    i = i + 1
    xls(1, i) = oDestino.cP
    i = i + 1
    
    If oPaises.Item(oDestino.Pais).Provincias Is Nothing Then
        oPaises.Item(oDestino.Pais).CargarTodasLasProvincias
    End If
    
    xls(1, i) = oPaises.Item(oDestino.Pais).Provincias.Item(oDestino.Provi).Den
    i = i + 1
    xls(1, i) = oPaises.Item(oDestino.Pais).Den
    i = i + 1
End If
    
    
xls(1, i) = IIf(oProceso.AdminPublica, "1", "0")
i = i + 1
xls(1, i) = IIf(oProceso.NoPublicarFinSum, "0", "1")
i = i + 1
xls(1, i) = NullToStr(oProceso.Cambio)
i = i + 1
xls(1, i) = NullToStr(oOferta.Cambio)



If g_bIncluir Then
    i = i + 1
    xls(1, i) = oProceso.AtributosEspecificacion.Count
    i = i + 1
    If Not oProceso.AtributosEspecificacion Is Nothing Then
        For Each oAtribEsp In oProceso.AtributosEspecificacion
            xls(1, i) = NullToStr(oAtribEsp.Den): i = i + 1
            Select Case oAtribEsp.Tipo
                Case 1:
                    xls(1, i) = NullToStr(oAtribEsp.valorText)
                Case 2:
                    xls(1, i) = NullToStr(oAtribEsp.valorNum)
                Case 3:
                    xls(1, i) = Format(oAtribEsp.valorFec, "dd\/mm\/yyyy")
                Case 4:
                    xls(1, i) = IIf(oAtribEsp.valorBool, xls(0, 45), xls(0, 46))
            End Select
            i = i + 1
        Next
    End If
Else

    i = i + 1
    xls(1, i) = 0
    i = i + 1
    
End If

xls(1, i) = IIf(oProceso.AtributosOfeObl, "1", "0")


'DATOS DE LA OFERTA

Dim oMonedas As CMonedas

Set oMonedas = oFSGSRaiz.Generar_CMonedas

If oProceso.CambiarMonOferta Then
    oMonedas.CargarTodasLasMonedas
Else
    oMonedas.CargarTodasLasMonedas oProceso.MonCod, , True
End If

xls(2, 0) = "" & Format(oOferta.FechaHasta, "DD\/MM\/YYYY")
Set oMon = oMonedas.Item(oOferta.CodMon)
xls(2, 1) = oMon.Cod & Left("      ", 6 - Len(oMon.Cod)) & "-" & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
xls(2, 2) = NullToStr(oOferta.obs)
i = 1
xls(2, 3) = NullToStr(oProceso.ATRIBUTOS.Count)

For Each oatrib In oProceso.ATRIBUTOS
    
    xls(2, i + 3) = NullToStr(oatrib.Id)
    i = i + 1
    
    xls(2, i + 3) = NullToStr(oatrib.idAtribProce)
    i = i + 1
    
    xls(2, i + 3) = NullToStr(oatrib.Cod)
    i = i + 1
    
    xls(2, i + 3) = NullToStr(oatrib.Den)
    i = i + 1
    
    Set oAtribsOfe = oOferta.AtribProcOfertados
    
    If oAtribsOfe Is Nothing Then
        xls(2, i + 3) = ""
    Else
        If oAtribsOfe.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
            xls(2, i + 3) = ""
        Else
            Select Case oatrib.Tipo
                Case 1:
                    xls(2, i + 3) = NullToStr(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorText)
                Case 2:
                    xls(2, i + 3) = NullToStr(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorNum)
                Case 3:
                    xls(2, i + 3) = Format(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorFec, "dd\/mm\/yyyy")
                Case 4:
                    xls(2, i + 3) = IIf(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorBool, xls(0, 45), xls(0, 46))
            End Select
        End If
    End If
    i = i + 1
    
    
    iFilaInicio = iFila
    If Not oatrib.ListaPonderacion Is Nothing Then
        If oatrib.ListaPonderacion.Count > 0 Then
            For Each oValor In oatrib.ListaPonderacion
                If oatrib.Tipo = TiposDeAtributos.TipoFecha Then
                    xls(3 + lNumGrupos, iFila - 2) = Format(oValor.ValorLista, "dd\/mm\/yyyy")
                Else
                    xls(3 + lNumGrupos, iFila - 2) = NullToStr(oValor.ValorLista)
                End If
                iFila = iFila + 1
            Next
            xls(2, i + 3) = "=$IV$" & iFilaInicio & ":" & "$IV$" & (iFila - 1)
        Else
            xls(2, i + 3) = ""
        End If
    Else
        xls(2, i + 3) = ""
    End If
    i = i + 1
    
    xls(2, i + 3) = NullToStr(oatrib.Tipo)
    i = i + 1
    
    Select Case oatrib.Tipo
        Case 1, 2, 4:
            xls(2, i + 3) = NullToStr(oatrib.Minimo)
            i = i + 1
            xls(2, i + 3) = NullToStr(oatrib.Maximo)
        Case 3:
            xls(2, i + 3) = Format(oatrib.Minimo, "dd\/mm\/yyyy")
            i = i + 1
            xls(2, i + 3) = Format(oatrib.Maximo, "dd\/mm\/yyyy")
    End Select
    i = i + 1
    
    xls(2, i + 3) = NullToStr(oatrib.PrecioFormula)
    i = i + 1
    
    xls(2, i + 3) = NullToStr(oatrib.PrecioAplicarA)
    i = i + 1
    
    xls(2, i + 3) = IIf(oatrib.Obligatorio, "1", "0")
    i = i + 1
    
    xls(2, i + 3) = NullToStr(oatrib.Descripcion)
    i = i + 1
Next

'LOS GRUPOS

i = 1

Dim j As Long

j = 3


For Each oGrupo In oProceso.Grupos
    
    
    For Each oOfeGrupo In oOferta.Grupos
        If oOfeGrupo.Grupo.Codigo = oGrupo.Codigo Then
            Exit For
        End If
    Next
    
    bCargar = True
    If gParametrosGenerales.gbProveGrupos Then
        If oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
            bCargar = False
        End If
    End If

    If bCargar Then
    
        xls(j, 0) = oGrupo.Codigo
        If oProceso.AdminPublica Then
            xls(j, 1) = oGrupo.Den & " (" & xls(0, 70) & CStr(oGrupo.Sobre) & ")"
        Else
            xls(j, 1) = oGrupo.Den
        End If
        xls(j, 2) = NullToStr(oGrupo.Descripcion)
        xls(j, 3) = IIf(oGrupo.DefDestino, 1, 0)
        xls(j, 4) = NullToStr(oGrupo.DestCod)
        xls(j, 5) = oDestinos.Item(oGrupo.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        xls(j, 6) = IIf(oGrupo.DefFechasSum, 1, 0)
        xls(j, 7) = "" & Format(oGrupo.FechaInicioSuministro, "DD\/MM\/YYYY")
        xls(j, 8) = "" & Format(oGrupo.FechaFinSuministro, "DD\/MM\/YYYY")
        xls(j, 9) = IIf(oGrupo.DefFormaPago, 1, 0)
        xls(j, 10) = NullToStr(oGrupo.PagCod)
        xls(j, 11) = NullToStr(oGrupo.PagDen)
        xls(j, 12) = NullToStr(oGrupo.esp)
        xls(j, 13) = IIf(oProceso.DefDestino = EnItem Or (oProceso.DefDestino = EnGrupo And oGrupo.DefDestino = False), 1, 0)
        xls(j, 14) = IIf(oProceso.DefFechasSum = EnItem Or (oProceso.DefFechasSum = EnGrupo And oGrupo.DefFechasSum = False), 1, 0)
        xls(j, 15) = IIf(oProceso.DefFormaPago = EnItem Or (oProceso.DefFormaPago = EnGrupo And oGrupo.DefFormaPago = False), 1, 0)
        xls(j, 16) = IIf(oProceso.SolicitarCantMax, 1, 0)
        xls(j, 17) = IIf(oProceso.PedirAlterPrecios, 1, 0)
        xls(j, 18) = NullToStr(oOfeGrupo.Lineas.Count)
        xls(j, 19) = NullToStr(oOfeGrupo.Grupo.NumAtribsGrupo)
        xls(j, 20) = NullToStr(oOfeGrupo.Grupo.NumAtribsItem)

        Dim lFilaComienzoAtribsGrupo As Long
        Dim lFilaComienzoAtribsItem  As Long
        Dim lfilacomienzoitems As Long
        Dim lfilacomienzoEscalados As Long
        Dim lFilaComienzoAtrEspeGrupo As Long
        
        If oGrupo.especificaciones Is Nothing Then
            lCountEspecsGrupo = 0
        Else
            lCountEspecsGrupo = oGrupo.especificaciones.Count
        End If
        
        lFilaComienzoAtribsGrupo = 28 + lCountEspecsGrupo * 2
        xls(j, 21) = lFilaComienzoAtribsGrupo + 2
        lFilaComienzoAtrEspeGrupo = lFilaComienzoAtribsGrupo + oOfeGrupo.Grupo.NumAtribsGrupo * iNumFilasPorAtrib
        xls(j, 22) = lFilaComienzoAtrEspeGrupo + 2
        If g_bIncluir Then
            lCountAtrEspeGrupo = oGrupo.AtributosEspecificacion.Count
        Else
            lCountAtrEspeGrupo = 0
        End If
        xls(j, 23) = lCountAtrEspeGrupo
        lFilaComienzoAtribsItem = lFilaComienzoAtrEspeGrupo + lCountAtrEspeGrupo * 2
        xls(j, 24) = lFilaComienzoAtribsItem + 2
        
        'Escalados
        If (oOfeGrupo.Grupo.UsarEscalados) Then
            '@@DPD (Portal) oGrupo.CargarEscalados lCiaComp
            lfilacomienzoEscalados = lFilaComienzoAtribsItem + (oOfeGrupo.Grupo.NumAtribsItem * iNumFilasPorAtrib)
            lfilacomienzoitems = lfilacomienzoEscalados + (oOfeGrupo.Grupo.Escalados.Count * 3) + 1  ' id,desde,hasta
        Else
            lfilacomienzoEscalados = lFilaComienzoAtribsItem + (oOfeGrupo.Grupo.NumAtribsItem * iNumFilasPorAtrib)
            lfilacomienzoitems = lFilaComienzoAtribsItem + (oOfeGrupo.Grupo.NumAtribsItem * iNumFilasPorAtrib) + 1
        End If
        xls(j, 25) = lfilacomienzoEscalados + 2
        
        xls(j, 26) = lfilacomienzoitems + 2
        xls(j, 27) = lCountEspecsGrupo
        i = 0
        For Each oEsp In oGrupo.especificaciones
            xls(j, i + 28) = oEsp.nombre
            i = i + 1
            xls(j, i + 28) = oEsp.Comentario
            i = i + 1
        Next
        
        i = 0
        Set oAtribsOfe = oOfeGrupo.AtribOfertados
        For Each oatrib In oOfeGrupo.Grupo.ATRIBUTOS
            
                
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.Id)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.idAtribProce)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.Cod)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.Den)
            i = i + 1
            
            
            If oAtribsOfe Is Nothing Then
                xls(j, i + lFilaComienzoAtribsGrupo) = ""
            Else
                If oAtribsOfe.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                    xls(j, i + lFilaComienzoAtribsGrupo) = ""
                Else
                    Select Case oatrib.Tipo
                        Case 1:
                           xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorText)
                        Case 2:
                            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorNum)
                        Case 3:
                            xls(j, i + lFilaComienzoAtribsGrupo) = Format(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorFec, "dd\/mm\/yyyy")
                        Case 4:
                            xls(j, i + lFilaComienzoAtribsGrupo) = IIf(oAtribsOfe.Item(CStr(oatrib.idAtribProce)).valorBool, xls(0, 45), xls(0, 46))
                    End Select
                End If
            End If
            i = i + 1
            
            iFilaInicio = iFila
            If Not oatrib.ListaPonderacion Is Nothing Then
                If oatrib.ListaPonderacion.Count > 0 Then
                    For Each oValor In oatrib.ListaPonderacion
                        If oatrib.Tipo = TiposDeAtributos.TipoFecha Then
                            xls(3 + lNumGrupos, iFila - 2) = Format(oValor.ValorLista, "dd\/mm\/yyyy")
                        Else
                            xls(3 + lNumGrupos, iFila - 2) = NullToStr(oValor.ValorLista)
                        End If
                        iFila = iFila + 1
                    Next
                    xls(j, i + lFilaComienzoAtribsGrupo) = "=$IV$" & iFilaInicio & ":" & "$IV$" & (iFila - 1)
                Else
                    xls(j, i + lFilaComienzoAtribsGrupo) = ""
                End If
            Else
                xls(j, i + lFilaComienzoAtribsGrupo) = ""
            End If
            
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.Tipo)
            i = i + 1
            
            
            Select Case oatrib.Tipo
                Case 1, 2, 4:
                    xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.Minimo)
                    i = i + 1
                    xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.Maximo)
                Case 3:
                    xls(j, i + lFilaComienzoAtribsGrupo) = Format(oatrib.Minimo, "dd\/mm\/yyyy")
                    i = i + 1
                    xls(j, i + lFilaComienzoAtribsGrupo) = Format(oatrib.Maximo, "dd\/mm\/yyyy")
            End Select
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.PrecioFormula)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.PrecioAplicarA)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = IIf(oatrib.Obligatorio, "1", "0")
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsGrupo) = NullToStr(oatrib.Descripcion)
            i = i + 1
            
        Next
        
        If g_bIncluir Then
            i = lFilaComienzoAtrEspeGrupo
            For Each oAtribEsp In oGrupo.AtributosEspecificacion
                xls(j, i) = NullToStr(oAtribEsp.Den)
                i = i + 1
                Select Case oAtribEsp.Tipo
                    Case 1:
                        xls(j, i) = NullToStr(oAtribEsp.valorText)
                    Case 2:
                        xls(j, i) = NullToStr(oAtribEsp.valorNum)
                    Case 3:
                        xls(j, i) = Format(oAtribEsp.valorFec, "dd\/mm\/yyyy")
                    Case 4:
                        xls(j, i) = IIf(oAtribEsp.valorBool, xls(0, 45), xls(0, 46))
                End Select
                i = i + 1
            Next
        End If
        
        i = 0
        For Each oatrib In oOfeGrupo.Grupo.AtributosItem
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.Id)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.idAtribProce)
            i = i + 1
            
            
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.Cod)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.Den)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = ""
            
            i = i + 1
            
            iFilaInicio = iFila
            If Not oatrib.ListaPonderacion Is Nothing Then
                If oatrib.ListaPonderacion.Count > 0 Then
                
                    For Each oValor In oatrib.ListaPonderacion
                        If oatrib.Tipo = TiposDeAtributos.TipoFecha Then
                            xls(3 + lNumGrupos, iFila - 2) = Format(oValor.ValorLista, "dd\/mm\/yyyy")
                        Else
                            xls(3 + lNumGrupos, iFila - 2) = NullToStr(oValor.ValorLista)
                        End If
                        iFila = iFila + 1
                    Next
                    xls(j, i + lFilaComienzoAtribsItem) = "=$IV$" & iFilaInicio & ":" & "$IV$" & (iFila - 1)
                Else
                    xls(j, i + lFilaComienzoAtribsItem) = ""
                End If
            Else
                xls(j, i + lFilaComienzoAtribsItem) = ""
            End If
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.Tipo)
            i = i + 1
            
            Select Case oatrib.Tipo
                Case 1, 2, 4:
                    xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.Minimo)
                    i = i + 1
                    xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.Maximo)
                Case 3:
                    xls(j, i + lFilaComienzoAtribsItem) = Format(oatrib.Minimo, "dd\/mm\/yyyy")
                    i = i + 1
                    xls(j, i + lFilaComienzoAtribsItem) = Format(oatrib.Maximo, "dd\/mm\/yyyy")
            End Select
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.PrecioFormula)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.PrecioAplicarA)
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = IIf(oatrib.Obligatorio, "1", "0")
            i = i + 1
            
            xls(j, i + lFilaComienzoAtribsItem) = NullToStr(oatrib.Descripcion)
            i = i + 1
        Next
        i = 0
        ' Escalados Grupo
        
        If oOfeGrupo.Grupo.UsarEscalados Then
            xls(j, i + lfilacomienzoEscalados) = NullToStr(oOfeGrupo.Grupo.Escalados.Count)
            i = i + 1
            For Each oEscalado In oOfeGrupo.Grupo.Escalados
                xls(j, i + lfilacomienzoEscalados) = NullToStr(oEscalado.Id)
                i = i + 1
                
                xls(j, i + lfilacomienzoEscalados) = NullToStr(oEscalado.Inicial)
                i = i + 1
                
                xls(j, i + lfilacomienzoEscalados) = NullToStr(oEscalado.final)
                i = i + 1
            Next
        Else
            xls(j, i + lfilacomienzoEscalados) = 0
            i = i + 1
        End If
                
                
        i = 0
        For Each oPrecioItem In oOfeGrupo.Lineas
            With oPrecioItem
            xls(j, i + lfilacomienzoitems) = NullToStr(.Id)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.ArticuloCod)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.Descr)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.DestCod)
            i = i + 1
            If oPrecioItem.DestCod <> "" Then
                xls(j, i + lfilacomienzoitems) = oDestinos.Item(.DestCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            Else
                xls(j, i + lfilacomienzoitems) = ""
            End If
            i = i + 1
            xls(j, i + lfilacomienzoitems) = "" & Format(.FechaInicioSuministro, "DD\/MM\/YYYY")
            i = i + 1
            xls(j, i + lfilacomienzoitems) = "" & Format(.FechaFinSuministro, "DD\/MM\/YYYY")
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.PagCod)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(oPagos.Item(.PagCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.Cantidad)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.UniCod)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(oUnidades.Item(.UniCod).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
            i = i + 1
            
            'Escalados en el �tem
            If oOfeGrupo.Grupo.UsarEscalados Then
                For Each oEscalado In .Escalados
                    xls(j, i + lfilacomienzoitems) = NullToStr(oEscalado.Precio)
                    i = i + 1
                Next
            Else
                xls(j, i + lfilacomienzoitems) = NullToStr(.Precio)
                i = i + 1
            End If
            xls(j, i + lfilacomienzoitems) = NullToStr(.CantidadMaxima)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.Precio1Obs)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.Precio2)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.Precio2Obs)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.Precio3)
            i = i + 1
            xls(j, i + lfilacomienzoitems) = NullToStr(.Precio3Obs)
            i = i + 1
            Set oItem = oGrupo.Items.Item(CStr(.Id))
            xls(j, i + lfilacomienzoitems) = NullToStr(oItem.esp)
            i = i + 1
            
            'PrecioOferta (con escalados)
            
            
            If oOfeGrupo.Grupo.UsarEscalados Then
                For Each oEscalado In .Escalados
                    xls(j, i + lfilacomienzoitems) = NullToStr(oEscalado.PrecioOferta)
                    i = i + 1
                Next
            Else
                xls(j, i + lfilacomienzoitems) = NullToStr(.Precio)
                i = i + 1
            End If
            
            
            
            'Especificaciones Atrib
            
            If g_bIncluir Then
                oItem.CargarTodosLosAtributosEspecificacion
                If Not oItem.AtributosEspecificacion Is Nothing Then
                    sTexto = ""
                    For Each oAtribEsp In oItem.AtributosEspecificacion
                        If Not oAtribEsp.interno Then
                            Select Case oAtribEsp.Tipo
                                Case 1:
                                    If Not IsNull(oAtribEsp.valorText) Then
                                        If sTexto <> "" Then sTexto = sTexto & vbCrLf
                                        sTexto = sTexto & oAtribEsp.Den & vbTab & vbTab & NullToStr(oAtribEsp.valorText)
                                    End If
                                Case 2:
                                    If Not IsNull(oAtribEsp.valorNum) Then
                                        If sTexto <> "" Then sTexto = sTexto & vbCrLf
                                        sTexto = sTexto & oAtribEsp.Den & vbTab & vbTab & NullToStr(oAtribEsp.valorNum)
                                    End If
                                Case 3:
                                    If Not IsNull(oAtribEsp.valorFec) Then
                                        If sTexto <> "" Then sTexto = sTexto & vbCrLf
                                        sTexto = sTexto & oAtribEsp.Den & vbTab & vbTab & Format(oAtribEsp.valorFec, "dd\/mm\/yyyy")
                                    End If
                                Case 4:
                                    If Not IsNull(oAtribEsp.valorBool) Then
                                        If sTexto <> "" Then sTexto = sTexto & vbCrLf
                                        sTexto = sTexto & oAtribEsp.Den & vbTab & vbTab & IIf(oAtribEsp.valorBool, xls(0, 45), xls(0, 46))
                                    End If
                            End Select
                        End If
                    Next
                    xls(j, i + lfilacomienzoitems) = sTexto
                End If
            End If
            i = i + 1
            
            'Escalados en el Objetivo
            If oOfeGrupo.Grupo.UsarEscalados Then
                For Each oEscalado In .Escalados
                    xls(j, i + lfilacomienzoitems) = NullToStr(oEscalado.Objetivo)
                    i = i + 1
                Next
            Else
                xls(j, i + lfilacomienzoitems) = NullToStr(oItem.Objetivo)
                i = i + 1
            End If
            
            Dim AtribEscalado As Boolean
            
            For Each oatrib In oOfeGrupo.Grupo.AtributosItem
                If Not oPrecioItem.AtribOfertados.Item(CStr(oPrecioItem.Id) & "$" & CStr(oatrib.idAtribProce)) Is Nothing Then
                    With oPrecioItem.AtribOfertados.Item(CStr(oPrecioItem.Id) & "$" & CStr(oatrib.idAtribProce))
                    
                    AtribEscalado = False
                    If oOfeGrupo.Grupo.UsarEscalados Then
                        If oatrib.PrecioAplicarA >= 2 And Not IsNull(oatrib.PrecioFormula) Then         ' Aplicar
                            AtribEscalado = True
                        End If
                    End If
                    
                    
                    
                    If AtribEscalado Then
                        For Each oEscalado In oPrecioItem.AtribOfertados.Item(CStr(oPrecioItem.Id) & "$" & CStr(oatrib.idAtribProce)).Escalados
                            'If Not oItem.PrecioItem.AtributosOfertado.Item(CStr(oatrib.PAID)).Escalados.Item(CStr(oEscalado.Id)) Is Nothing Then
                                xls(j, i + lfilacomienzoitems) = (oEscalado.valorNum)
                            'Else
                                'xls(j, i + lfilacomienzoitems) = ""
                            'End If
                            i = i + 1
                        Next
                        'xls(j, i + lfilacomienzoitems) = NullToStr(.ValorNum)
                    Else
                        Select Case oatrib.Tipo
                            Case 1:
                               xls(j, i + lfilacomienzoitems) = NullToStr(.valorText)
                            Case 2:
                                xls(j, i + lfilacomienzoitems) = NullToStr(.valorNum)
                            Case 3:
                                xls(j, i + lfilacomienzoitems) = Format(.valorFec, "dd\/mm\/yyyy")
                            Case 4:
                                If IsNull(.valorBool) Then
                                    xls(j, i + lfilacomienzoitems) = ""
                                Else
                                    xls(j, i + lfilacomienzoitems) = IIf(.valorBool, xls(0, 45), xls(0, 46))
                                End If
                        End Select
                        i = i + 1
                        
                    End If
                    End With
                Else
                    xls(j, i + lfilacomienzoitems) = ""
                    i = i + 1
                End If
            Next
            End With
        Next

        Set oDestino = oDestinos.Item(oGrupo.DestCod)
        xls(j, i + lfilacomienzoitems) = oDestino.Cod
        i = i + 1
        xls(j, i + lfilacomienzoitems) = oDestino.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        i = i + 1
        xls(j, i + lfilacomienzoitems) = oDestino.dir
        i = i + 1
        xls(j, i + lfilacomienzoitems) = oDestino.POB
        i = i + 1
        xls(j, i + lfilacomienzoitems) = oDestino.cP
        i = i + 1
        
        If oPaises.Item(oDestino.Pais).Provincias Is Nothing Then
            oPaises.Item(oDestino.Pais).CargarTodasLasProvincias
        End If
        
        xls(j, i + lfilacomienzoitems) = oPaises.Item(oDestino.Pais).Provincias.Item(oDestino.Provi).Den
        i = i + 1
        xls(j, i + lfilacomienzoitems) = oPaises.Item(oDestino.Pais).Den
        i = i + 1
        

        j = j + 1
    End If
Next

i = 0
j = j + 1
For Each oMon In oMonedas
   xls(j, i) = oMon.Cod & Left("      ", 6 - Len(oMon.Cod)) & "-" & oMon.Denominaciones.Item(gParametrosInstalacion.giAnyadirEspecArticulo).Den
   i = i + 1
Next

i = 0
j = j + 1


For Each oDestino In oDestinos
    xls(j, i) = NullToStr(oDestino.Cod)
    i = i + 1
    xls(j, i) = NullToStr(oDestino.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
    i = i + 1
    xls(j, i) = NullToStr(oDestino.dir)
    i = i + 1
    xls(j, i) = NullToStr(oDestino.POB)
    i = i + 1
    xls(j, i) = NullToStr(oDestino.cP)
    i = i + 1
    
    If oPaises.Item(oDestino.Pais).Provincias Is Nothing Then
        oPaises.Item(oDestino.Pais).CargarTodasLasProvincias
    End If
    
    xls(j, i) = oPaises.Item(oDestino.Pais).Provincias.Item(oDestino.Provi).Den
    i = i + 1
    xls(j, i) = oPaises.Item(oDestino.Pais).Den
    i = i + 1
    
Next


sPathXls = App.Path & "\FSXLSOfertaOffline.xls"


Dim oFSO As Object
Dim oFile As Scripting.File

Set oFSO = CreateObject("Scripting.filesystemobject")
oFSO.CopyFile sPathXls, sTarget

Set oFile = oFSO.GetFile(sTarget)
If oFile.Attributes And 1 Then
    oFile.Attributes = oFile.Attributes Xor 1
End If


Set oFSO = Nothing



''' UNA VEZ QUE EL ARRAY EST� CARGADO, SE PASA A LA XLS.
Set oExcelAdoConn = New ADODB.Connection
sConnect = "Provider=MSDASQL.1;" _
         & "Extended Properties=""DBQ=" & sTarget & ";" _
         & "Driver={Microsoft Excel Driver (*.xls)};" _
         & "ReadOnly=0;" _
         & "UID=admin;"""
         
oExcelAdoConn.Open sConnect

SQL = "CREATE TABLE [Proceso] (MULTILENGUAJE memo, PROCESO memo, OFERTA memo, "
For Each oGrupo In oProceso.Grupos
    With oGrupo
        bCargar = True
        If gParametrosGenerales.gbProveGrupos Then
            If oAsigs.Item(scodProve).Grupos.Item(oGrupo.Codigo) Is Nothing Then
                bCargar = False
            End If
        End If
        If bCargar Then
            SQL = SQL & "G_" & oGrupo.Codigo & " memo, "
        End If
    End With
Next
SQL = SQL & "LISTAS memo, PARAMETROS memo , CODMON memo,DESTINO memo, FILA memo)"


oExcelAdoConn.Execute SQL



Set adoComm = New ADODB.Command
Set adoComm.ActiveConnection = oExcelAdoConn

SQL = "INSERT INTO [Proceso$] values ("
For j = 0 To UBound(xls, 1) - 2
    SQL = SQL & "?,"
    Set adoParam = adoComm.CreateParameter("", adLongVarChar, adParamInput, 2000, Null)
    adoComm.Parameters.Append adoParam
Next
    
SQL = SQL & "?,?,?,?)"
Set adoParam = adoComm.CreateParameter("", adInteger, adParamInput, , Null)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("", adLongVarChar, adParamInput, 2000, Null)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("", adLongVarChar, adParamInput, 2000, Null)
adoComm.Parameters.Append adoParam
Set adoParam = adoComm.CreateParameter("", adInteger, adParamInput, , Null)
adoComm.Parameters.Append adoParam
adoComm.CommandText = SQL
adoComm.CommandType = adCmdText
adoComm.Prepared = True

For i = 0 To UBound(xls, 2)
    For j = 0 To UBound(xls, 1) - 2
        adoComm.Parameters(j).Size = IIf(Len(xls(j, i)) = 0, 0, Len(xls(j, i))) + 1
        adoComm.Parameters(j).Value = StrToNull(xls(j, i))
    Next
    If i = 0 Then
        adoComm.Parameters(j).Value = UBound(xls, 2)
    ElseIf i = 1 Then
        adoComm.Parameters(j).Value = oMonedas.Count
    ElseIf i = 2 Then
        adoComm.Parameters(j).Value = oDestinos.Count
    ElseIf i = 3 Then
        adoComm.Parameters(j).Value = "4" 'Versi�n
    Else
        adoComm.Parameters(j).Value = Null
    End If
    adoComm.Parameters(j + 1).Size = IIf(Len(xls(j, i)) = 0, 0, Len(xls(j, i))) + 1
    adoComm.Parameters(j + 1).Value = StrToNull(xls(j, i))
    adoComm.Parameters(j + 2).Size = IIf(Len(xls(j + 1, i)) = 0, 0, Len(xls(j + 1, i))) + 1
    adoComm.Parameters(j + 2).Value = StrToNull(xls(j + 1, i))
    adoComm.Parameters(j + 3).Value = i
    adoComm.Execute
    
Next
Set adoComm = Nothing

oExcelAdoConn.Close

Set oExcelAdoConn = Nothing

Set oDestinos = Nothing
Set oMonedas = Nothing
Set oPaises = Nothing

Screen.MousePointer = vbNormal


fin:
FSXLSOfertaOffline = True
Exit Function

Error:

If err.Number = 9 Then
    ReDim Preserve xls(0 To UBound(xls, 1), 0 To UBound(xls, 2) + 10) As String
    Resume 0
ElseIf err.Number = 32755 Then
    FSXLSOfertaOffline = False
    Resume fin
Else
    Resume Next
    Resume 0
End If

End Function

