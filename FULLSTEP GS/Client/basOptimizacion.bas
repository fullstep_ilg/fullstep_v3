Attribute VB_Name = "basOptimizacion"
Option Explicit

Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long

' Variables para reducir el trafico de red DCOM.

Public gvarCodUsuario As Variant
Public gTipoDeUsuario As TipoDeUsuario
Public gCodEqpUsuario As Variant
Public gCodCompradorUsuario As Variant
Public gCodPersonaUsuario As Variant
Public gUON1Usuario As Variant
Public gUON2Usuario As Variant
Public gUON3Usuario As Variant
Public gUON4Usuario As Variant
Public gCodDepUsuario As Variant
Public g_oEmpresaUsuario As CEmpresa
Public gPYMEUsuario As Variant
'Funciones para que no se redibuje la pantalla mientras se cargan
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Public Function StringDePresupuestos(ByVal oPresupuestos As Object, ByVal iTipo As Integer, Optional bConPorcen As Boolean = False) As String
Dim oPres As Object
Dim sPresup As String

    If oPresupuestos Is Nothing Then
        StringDePresupuestos = ""
        Exit Function
    End If
    
    For Each oPres In oPresupuestos
        If sPresup <> "" Then sPresup = sPresup & "; "
        If iTipo < 3 Then
            sPresup = sPresup & oPres.Anyo & "-"
        End If
        sPresup = sPresup & oPres.CodPRES1
        If oPres.CodPRES2 <> "" Then
            sPresup = sPresup & "-" & oPres.CodPRES2
            If oPres.CodPRES3 <> "" Then
                sPresup = sPresup & "-" & oPres.CodPRES3
                If oPres.Cod <> "" Then
                    sPresup = sPresup & "-" & oPres.Cod
                End If
            End If
        End If
        If bConPorcen Then
            sPresup = sPresup & " (" & Format(oPres.Porcen, "0.00%") & ")"
        End If
    Next

StringDePresupuestos = sPresup

End Function

Public Function MostrarAtributo(ByVal oValorAtrib As CAtributoOfertado, ByVal udtTipo As TiposDeAtributos, ByVal m_sIdiFalse As String, ByVal m_sIdiTrue As String) As Variant

Select Case udtTipo
    Case TiposDeAtributos.TipoString
        MostrarAtributo = EliminarTab(NullToStr(oValorAtrib.valorText))
    Case TiposDeAtributos.TipoNumerico
        MostrarAtributo = oValorAtrib.valorNum
    Case TiposDeAtributos.TipoFecha
        MostrarAtributo = oValorAtrib.valorFec
    Case TiposDeAtributos.TipoBoolean
        Select Case oValorAtrib.valorBool
        Case 0, False
            MostrarAtributo = m_sIdiFalse
        Case 1, True
            MostrarAtributo = m_sIdiTrue
        Case Else
            MostrarAtributo = ""
        End Select
End Select
        
End Function

Public Function BajaLogicaDePresupuestos(ByVal oPresupuestos As Object) As Boolean
    Dim oPres As Object
    Dim bBaja As Boolean

     'Si no hay presupuestos, devuelve false
    If oPresupuestos Is Nothing Then
        BajaLogicaDePresupuestos = False
        Exit Function
    End If
    If oPresupuestos.Count = 0 Then
        BajaLogicaDePresupuestos = False
        Exit Function
    End If
    
    'Si hay, todos tienen que estar de baja l�gica para devolver true
    bBaja = True
    For Each oPres In oPresupuestos
        bBaja = bBaja And oPres.BajaLog
    Next
    
    BajaLogicaDePresupuestos = bBaja
End Function

