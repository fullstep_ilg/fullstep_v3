VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosCumplValorLista 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DValores de la lista"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5280
   Icon            =   "frmFlujosCumplValorLista.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   5280
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraValores 
      Height          =   2775
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   4995
      Begin SSDataWidgets_B.SSDBGrid sdbgValores 
         Height          =   2325
         Left            =   180
         TabIndex        =   1
         Top             =   240
         Width           =   4590
         _Version        =   196617
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   3
         stylesets.count =   7
         stylesets(0).Name=   "Calculado"
         stylesets(0).BackColor=   16766421
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmFlujosCumplValorLista.frx":0CB2
         stylesets(0).AlignmentPicture=   1
         stylesets(1).Name=   "No"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmFlujosCumplValorLista.frx":0D19
         stylesets(1).AlignmentPicture=   2
         stylesets(2).Name=   "S�"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmFlujosCumplValorLista.frx":0D35
         stylesets(2).AlignmentPicture=   2
         stylesets(3).Name=   "Gris"
         stylesets(3).BackColor=   -2147483633
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmFlujosCumplValorLista.frx":0D51
         stylesets(4).Name=   "Normal"
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "frmFlujosCumplValorLista.frx":0D6D
         stylesets(5).Name=   "ActiveRow"
         stylesets(5).ForeColor=   16777215
         stylesets(5).BackColor=   8388608
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "frmFlujosCumplValorLista.frx":0D89
         stylesets(6).Name=   "Amarillo"
         stylesets(6).BackColor=   12648447
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "frmFlujosCumplValorLista.frx":0DA5
         DividerType     =   2
         BevelColorHighlight=   16777215
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   53
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5768
         Columns(1).Caption=   "dValor"
         Columns(1).Name =   "VALOR"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1720
         Columns(2).Caption=   "dVis."
         Columns(2).Name =   "VISIBLE"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Style=   2
         _ExtentX        =   8096
         _ExtentY        =   4101
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   7.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmFlujosCumplValorLista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Variables Publicas
Public mlBloque As Long
Public mlRol As Long
Public mlCampo As Long
Public mlSubTipo As Long

Private msValor As String
Private msVisible As String

Private m_bErrorCumplValoresLista As Boolean
Private moCumplValoresLista As CPMConfCumplValoresLista

Private Sub Form_Load()
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    PonerFieldSeparator Me
    CargarCumplimentacionValoresLista
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSCUMPLVALORLISTA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1 Valores de la lista
        Ador.MoveNext
        msValor = Ador(0).Value '2 Valor
        Ador.MoveNext
        msVisible = Ador(0).Value '3 Vis.
                
        Ador.Close
    End If
End Sub

Private Sub CargarCumplimentacionValoresLista()
    Dim oBloque As CBloque
    Dim oCumplValorLista As CPMConfCumplValorLista
    Dim vValor As Variant
    
    sdbgValores.RemoveAll
    If mlBloque > 0 Then
        Set oBloque = oFSGSRaiz.Generar_CBloque
        oBloque.Id = mlBloque

        If mlRol > 0 And mlCampo > 0 Then
            oBloque.CargarCumplimentacionValoresLista mlRol, mlCampo
            Set moCumplValoresLista = oBloque.CumplValoresLista
            If Not moCumplValoresLista Is Nothing Then
                For Each oCumplValorLista In moCumplValoresLista
                    Select Case mlSubTipo
                    Case TiposDeAtributos.TipoNumerico
                        vValor = oCumplValorLista.valorNum
                    Case TiposDeAtributos.TipoFecha
                        vValor = oCumplValorLista.valorFec
                    Case Else
                        vValor = oCumplValorLista.valorText
                    End Select
                
                    sdbgValores.AddItem oCumplValorLista.Id & Chr(m_lSeparador) & vValor & Chr(m_lSeparador) & oCumplValorLista.Visible
                Next
            End If
        End If
    End If
End Sub

Private Sub Form_Resize()
    If Me.Width < 8220 Or Me.Height < 5880 Then Exit Sub
    sdbgValores.Width = Me.Width - 630
    sdbgValores.Height = Me.Height - 615
    sdbgValores.Columns("VALOR").Width = sdbgValores.Width - sdbgValores.Columns("VISIBLE").Width - 340
End Sub

Private Sub Form_Unload(Cancel As Integer)
    mlBloque = 0
    mlRol = 0
End Sub

Private Sub sdbgValores_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim oCumplValorLista As CPMConfCumplValorLista
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit

    If sdbgValores.Columns("ID").Value <> "" And sdbgValores.Columns("ID").Value <> "0" Then
        Set oCumplValorLista = moCumplValoresLista.Item(CStr(sdbgValores.Columns("ID").Value))
        If Not oCumplValorLista Is Nothing Then
            oCumplValorLista.Visible = CBool(sdbgValores.Columns("VISIBLE").Value)
            
            Set oIBaseDatos = oCumplValorLista
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
            
                basErrores.TratarError teserror
                sdbgValores.CancelUpdate
                If Me.Visible Then sdbgValores.SetFocus
                Set oIBaseDatos = Nothing
                m_bErrorCumplValoresLista = True
                Exit Sub
            End If
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
            Set oIBaseDatos = Nothing
            m_bErrorCumplValoresLista = False
        End If
    End If
End Sub

Private Sub sdbgValores_Change()
    If sdbgValores.Col < 0 Then Exit Sub
                    
    Select Case sdbgValores.Columns(sdbgValores.Col).Name
        Case "VISIBLE"
            sdbgValores.Update
    End Select
End Sub

Private Sub sdbgValores_InitColumnProps()
    sdbgValores.Columns("VALOR").caption = msValor
    sdbgValores.Columns("VISIBLE").caption = msVisible
    
    If mlSubTipo = TiposDeAtributos.TipoNumerico Then
        sdbgValores.Columns("VALOR").Alignment = ssCaptionAlignmentRight
    Else
        sdbgValores.Columns("VALOR").Alignment = ssCaptionAlignmentLeft
    End If
End Sub

Private Sub sdbgValores_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgValores.DataChanged Then
                sdbgValores.Update
                If m_bErrorCumplValoresLista Then
                    Exit Sub
                End If
            End If
    End Select
End Sub

Private Sub sdbgValores_LostFocus()
    If sdbgValores.DataChanged Then
        sdbgValores.Update
    End If
End Sub
