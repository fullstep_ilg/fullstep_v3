VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRORGBuscarDep 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar departamentos"
   ClientHeight    =   3405
   ClientLeft      =   1005
   ClientTop       =   3210
   ClientWidth     =   7440
   ForeColor       =   &H00000000&
   Icon            =   "frmESTRORGBuscarDep.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   7440
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00808000&
      Height          =   690
      Left            =   90
      ScaleHeight     =   630
      ScaleWidth      =   6825
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   90
      Width           =   6885
      Begin VB.TextBox txtCod 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   5385
         MaxLength       =   50
         TabIndex        =   1
         Top             =   180
         Width           =   1335
      End
      Begin VB.TextBox txtDen 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1200
         MaxLength       =   150
         TabIndex        =   0
         Top             =   180
         Width           =   3255
      End
      Begin VB.Label lblCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   4620
         TabIndex        =   8
         Top             =   240
         Width           =   690
      End
      Begin VB.Label lblDen 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   60
         TabIndex        =   7
         Top             =   240
         Width           =   1125
      End
   End
   Begin VB.CommandButton cmdCargar 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7035
      Picture         =   "frmESTRORGBuscarDep.frx":014A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Cargar"
      Top             =   480
      Width           =   315
   End
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Cerrar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   3765
      TabIndex        =   5
      Top             =   3000
      Width           =   1125
   End
   Begin VB.CommandButton cmdAceptar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2385
      TabIndex        =   4
      Top             =   3000
      Width           =   1125
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgDepartamentos 
      Height          =   1995
      Left            =   60
      TabIndex        =   3
      Top             =   900
      Width           =   7305
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   5
      stylesets.count =   1
      stylesets(0).Name=   "ActiveRowBlue"
      stylesets(0).ForeColor=   16777215
      stylesets(0).BackColor=   8388608
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmESTRORGBuscarDep.frx":01D6
      UseGroups       =   -1  'True
      BevelColorFrame =   0
      BevelColorHighlight=   16777215
      AllowUpdate     =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ActiveRowStyleSet=   "ActiveRowBlue"
      Groups.Count    =   2
      Groups(0).Width =   4366
      Groups(0).Caption=   "Unidades organizativas"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns.Count=   3
      Groups(0).Columns(0).Width=   1455
      Groups(0).Columns(0).Caption=   "UON1"
      Groups(0).Columns(0).Name=   "UON1"
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   16449500
      Groups(0).Columns(1).Width=   1455
      Groups(0).Columns(1).Caption=   "UON2"
      Groups(0).Columns(1).Name=   "UON2"
      Groups(0).Columns(1).DataField=   "Column 1"
      Groups(0).Columns(1).DataType=   8
      Groups(0).Columns(1).FieldLen=   256
      Groups(0).Columns(1).HasBackColor=   -1  'True
      Groups(0).Columns(1).BackColor=   16449500
      Groups(0).Columns(2).Width=   1455
      Groups(0).Columns(2).Caption=   "UON3"
      Groups(0).Columns(2).Name=   "UON3"
      Groups(0).Columns(2).DataField=   "Column 2"
      Groups(0).Columns(2).DataType=   8
      Groups(0).Columns(2).FieldLen=   256
      Groups(0).Columns(2).HasBackColor=   -1  'True
      Groups(0).Columns(2).BackColor=   16449500
      Groups(1).Width =   8123
      Groups(1).Caption=   "Departamentos"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   2
      Groups(1).Columns(0).Width=   1561
      Groups(1).Columns(0).Caption=   "C�digo"
      Groups(1).Columns(0).Name=   "COD"
      Groups(1).Columns(0).DataField=   "Column 3"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(1).Width=   6562
      Groups(1).Columns(1).Caption=   "Denominaci�n"
      Groups(1).Columns(1).Name=   "DEN"
      Groups(1).Columns(1).DataField=   "Column 4"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      TabNavigation   =   1
      _ExtentX        =   12885
      _ExtentY        =   3519
      _StockProps     =   79
      BackColor       =   -2147483633
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmESTRORGBuscarDep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private oDeps0 As CDepAsociados
Private oDeps1 As CDepAsociados
Private oDeps2 As CDepAsociados
Private oDeps3 As CDepAsociados


Private Sub cmdAceptar_Click()

If sdbgDepartamentos.Rows = 0 Then
    Exit Sub
End If

Dim nodx As node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

On Error GoTo Error

       
       If Trim(sdbgDepartamentos.Columns(2).Value) <> "" Then
            'Dep de nivel 3
            scod1 = sdbgDepartamentos.Columns(0).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgDepartamentos.Columns(0).Value))
            scod2 = scod1 & sdbgDepartamentos.Columns(1).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgDepartamentos.Columns(1).Value))
            scod3 = scod2 & sdbgDepartamentos.Columns(2).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(sdbgDepartamentos.Columns(2).Value))
            scod4 = scod3 & sdbgDepartamentos.Columns(3).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(sdbgDepartamentos.Columns(3).Value))
            
            Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("DEPA" & scod4)
        Else
            If Trim(sdbgDepartamentos.Columns(1).Value) <> "" Then
                'Dep de nivel 2
                scod1 = sdbgDepartamentos.Columns(0).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgDepartamentos.Columns(0).Value))
                scod2 = scod1 & sdbgDepartamentos.Columns(1).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(sdbgDepartamentos.Columns(1).Value))
                scod3 = scod2 & sdbgDepartamentos.Columns(3).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(sdbgDepartamentos.Columns(3).Value))
                Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("DEPA" & scod3)
            Else
                If Trim(sdbgDepartamentos.Columns(0).Value) <> "" Then
                    'Dep de nivel 1
                    scod1 = sdbgDepartamentos.Columns(0).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(sdbgDepartamentos.Columns(0).Value))
                    scod2 = scod1 & sdbgDepartamentos.Columns(3).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(sdbgDepartamentos.Columns(3).Value))
                    Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("DEPA" & scod2)
                Else
                    'Dep de nivel 0
                    scod1 = sdbgDepartamentos.Columns(3).Value & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(sdbgDepartamentos.Columns(3).Value))
                    Set nodx = frmESTRORG.tvwestrorg.Nodes.Item("DEPA" & scod1)
                End If
            End If
        End If
        
        
            


nodx.Parent.Expanded = True
nodx.Selected = True


Unload Me
If frmESTRORG.Visible Then frmESTRORG.tvwestrorg.SetFocus
frmESTRORG.ConfigurarInterfazSeguridad nodx


Exit Sub

Error:
    oMensajes.DatosModificados
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdCargar_Click()

CargarDep True
CargarGrid

End Sub

Private Sub Form_Load()

    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2

Select Case gParametrosGenerales.giNEO

    Case 0
        
        Set oDeps0 = oFSGSRaiz.generar_CDepAsociados
  
    Case 1
        Set oDeps0 = oFSGSRaiz.generar_CDepAsociados
      
        Set oDeps1 = oFSGSRaiz.generar_CDepAsociados
       
    Case 2
        
        Set oDeps0 = oFSGSRaiz.generar_CDepAsociados
       
        Set oDeps1 = oFSGSRaiz.generar_CDepAsociados
        
        Set oDeps2 = oFSGSRaiz.generar_CDepAsociados
      
        
    Case 3
        
        Set oDeps0 = oFSGSRaiz.generar_CDepAsociados

        Set oDeps1 = oFSGSRaiz.generar_CDepAsociados
  
        Set oDeps2 = oFSGSRaiz.generar_CDepAsociados
     
        Set oDeps3 = oFSGSRaiz.generar_CDepAsociados
 
        
End Select

CargarRecursos

    PonerFieldSeparator Me

End Sub
Private Sub CargarGrid()
Dim oDep As CDepAsociado
Dim varUO As Variant

    sdbgDepartamentos.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
                For Each oDep In oDeps0
                     
                    sdbgDepartamentos.AddItem "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oDep.Cod & Chr(m_lSeparador) & oDep.Den
                    
                Next
                
                If gParametrosGenerales.giNEO <= 0 Then Exit Sub
                
                For Each oDep In oDeps1
                     
                    sdbgDepartamentos.AddItem oDep.CodUON1 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oDep.Cod & Chr(m_lSeparador) & oDep.Den
                    
                Next
                
                If gParametrosGenerales.giNEO <= 1 Then Exit Sub
                
                For Each oDep In oDeps2
                     
                    sdbgDepartamentos.AddItem oDep.CodUON1 & Chr(m_lSeparador) & oDep.CodUON2 & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oDep.Cod & Chr(m_lSeparador) & oDep.Den
                    
                Next
                
                If gParametrosGenerales.giNEO <= 2 Then Exit Sub
                
                For Each oDep In oDeps3
                     
                    sdbgDepartamentos.AddItem oDep.CodUON1 & Chr(m_lSeparador) & oDep.CodUON2 & Chr(m_lSeparador) & oDep.CodUON3 & Chr(m_lSeparador) & oDep.Cod & Chr(m_lSeparador) & oDep.Den
                    
                Next
  
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oDeps0 = Nothing
    Set oDeps1 = Nothing
    Set oDeps2 = Nothing
    Set oDeps3 = Nothing
End Sub

Private Sub sdbgDepartamentos_DblClick()
    
    cmdAceptar_Click
    
End Sub

Private Sub sdbgDepartamentos_HeadClick(ByVal ColIndex As Integer)
    
    Select Case gParametrosGenerales.giNEO
    
        Case 0
                
        Case 1
        
        Case 2
        
        Case 3
                If ColIndex = 3 Then
                    CargarDep True
                    CargarGrid
                End If
                
                If ColIndex = 4 Then
                    CargarDep False
                    CargarGrid
                End If
    End Select
    
        
End Sub

Private Sub sdbgDepartamentos_InitColumnProps()
    
    sdbgDepartamentos.Columns(0).caption = gParametrosGenerales.gsABR_UON1 & "."
    sdbgDepartamentos.Columns(1).caption = gParametrosGenerales.gsABR_UON2 & "."
    sdbgDepartamentos.Columns(2).caption = gParametrosGenerales.gsABR_UON3 & "."
    
    Select Case gParametrosGenerales.giNEO
    
        Case 0
                sdbgDepartamentos.Groups(0).Visible = False
                sdbgDepartamentos.Columns(0).Width = sdbgDepartamentos.Columns(0).Width * 20 / 100
                sdbgDepartamentos.Columns(1).Width = sdbgDepartamentos.Columns(1).Width * 80 / 100
        Case 1
                sdbgDepartamentos.Columns(1).Visible = False
                sdbgDepartamentos.Columns(2).Visible = False
                sdbgDepartamentos.Columns(0).Width = sdbgDepartamentos.Width * 20 / 100
                sdbgDepartamentos.Columns(1).Width = sdbgDepartamentos.Width * 20 / 100
                sdbgDepartamentos.Columns(2).Width = sdbgDepartamentos.Width * 60 / 100
        Case 2
                sdbgDepartamentos.Columns(2).Visible = False
                sdbgDepartamentos.Columns(0).Width = sdbgDepartamentos.Width * 20 / 100
                sdbgDepartamentos.Columns(1).Width = sdbgDepartamentos.Width * 20 / 100
                sdbgDepartamentos.Columns(2).Width = sdbgDepartamentos.Width * 20 / 100
                sdbgDepartamentos.Columns(3).Width = sdbgDepartamentos.Width * 40 / 100
        Case 3
        
    End Select
    
    
    
End Sub
Private Sub CargarDep(ByVal bOrdPorDen As Boolean)
    
    Select Case gParametrosGenerales.giNEO

    Case 0
            oDeps0.CargarTodosLosDepAsociados , , , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 0, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
            
    Case 1
        
        oDeps0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, , , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 0, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        oDeps1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, , , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 1, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        
    Case 2
        
        oDeps0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 0, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        oDeps1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 1, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        oDeps2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 2, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
    
    Case 3
        
        oDeps0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 0, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        oDeps1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 1, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        oDeps2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 2, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        oDeps3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, frmESTRORG.bRUO, frmESTRORG.bRDep, 3, Trim(txtCod), Trim(txtDen), False, bOrdPorDen, False
        
End Select

End Sub


Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRORG_BUSCARDEP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblCod.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbgDepartamentos.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgDepartamentos.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgDepartamentos.Columns(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgDepartamentos.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        
        Ador.Close
    
    End If

    Set Ador = Nothing



End Sub


