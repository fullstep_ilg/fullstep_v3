VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPARFirmas 
   Caption         =   "Firmas "
   ClientHeight    =   3930
   ClientLeft      =   1230
   ClientTop       =   2415
   ClientWidth     =   5265
   Icon            =   "frmPARFirmas.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   3930
   ScaleWidth      =   5265
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   0
      ScaleHeight     =   555
      ScaleWidth      =   5265
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   3375
      Width           =   5265
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   5460
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   180
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2340
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdCodigo 
         Caption         =   "C�&digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3480
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgUnidades 
      Height          =   3315
      Left            =   60
      TabIndex        =   7
      Top             =   60
      Width           =   5100
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   1
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmPARFirmas.frx":014A
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      BalloonHelp     =   0   'False
      MaxSelectedRows =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1323
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   5
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777152
      Columns(1).Width=   4577
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   100
      _ExtentX        =   8996
      _ExtentY        =   5847
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPARFirmas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmPARFirmas
''' *** Creacion: 9/09/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit

''' Coleccion de Firmas
Public oFirmas As CFirmas
''' Firma en edicion
Private oFirmaEnEdicion As CFirma
Private oIBAseDatosEnEdicion As IBaseDatos
''' Listado de Firmas
Public sOrdenListadoDen As Boolean
''' Propiedades de seguridad
Private bModif  As Boolean
''' Control de errores
Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean
''' Variables de control
Public Accion As accionessummit
Private bModoEdicion As Boolean
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
''' Posicion para UnboundReadData
Private p As Long
'Multilenguaje
Private sIdiTitCodigo As String
Private sIdiCodigo As String
Private sIdiTitEdicion As String
Private sIdiConsulta As String
Private sIdiTitConsulta As String
Private sIdiEdicion As String
Private sIdiLaFirma As String
Private sIdiDenominacion As String
Private sIdiOrdenando As String

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgUnidades.DataChanged = True Then
        bValError = False
        bAnyaError = False
        bModError = False
        If sdbgUnidades.Row = 0 Then
            sdbgUnidades.MoveNext
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgUnidades.MovePrevious
            End If
        Else
            sdbgUnidades.MovePrevious
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgUnidades.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

Private Sub ConfigurarSeguridad()

    ''' * Objetivo: Configurar el formulario segun los permisos
    ''' * Objetivo: del usuario
    
    
    bModif = True
        
    ''' Aplicar variables
    
    cmdModoEdicion.Visible = True
    
End Sub

Private Sub Arrange()
    
    ''' * Objetivo: Ajustar el tamanyo y posicion de los controles
    ''' * Objetivo: al tamanyo del formulario
    
    
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 5770 Then   'de tama�o de la ventana
            Me.Width = 5970        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 1730 Then  'cuando no se maximiza ni
            Me.Height = 1830       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
    
    
    If Height >= 900 Then sdbgUnidades.Height = Height - 1050
    If Width >= 250 Then sdbgUnidades.Width = Width - 250
    
    sdbgUnidades.Columns(0).Width = sdbgUnidades.Width * 15 / 100
    sdbgUnidades.Columns(1).Width = sdbgUnidades.Width * 85 / 100 - 580
    
    cmdModoEdicion.Left = sdbgUnidades.Left + sdbgUnidades.Width - cmdModoEdicion.Width
    
End Sub
Private Sub cmdA�adir_Click()
    
    ''' * Objetivo: Situarnos en la fila de adicion
    
    sdbgUnidades.Scroll 0, sdbgUnidades.Rows - sdbgUnidades.Row
    
    If sdbgUnidades.VisibleRows > 0 Then
        
        If sdbgUnidades.VisibleRows > sdbgUnidades.Rows Then
            sdbgUnidades.Row = sdbgUnidades.Rows
        Else
            sdbgUnidades.Row = sdbgUnidades.Rows - (sdbgUnidades.Rows - sdbgUnidades.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgUnidades.SetFocus
    
End Sub
Private Sub cmdCodigo_Click()
    
    ''' * Objetivo: Cambiar de codigo la Firma actual
    
    Dim teserror As TipoErrorSummit
        
    If sdbgUnidades.Rows = 0 Then Exit Sub
    ''' Resaltar la Firma actual
    
    sdbgUnidades.SelBookmarks.Add sdbgUnidades.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    
    Set oFirmaEnEdicion = oFirmas.Item(CStr(sdbgUnidades.Bookmark))
    Set oIBAseDatosEnEdicion = oFirmaEnEdicion
    
    teserror = oIBAseDatosEnEdicion.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If Me.Visible Then sdbgUnidades.SetFocus
        Exit Sub
    End If
        
    oIBAseDatosEnEdicion.CancelarEdicion
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
'    frmMODCOD.Caption = "Firmas (C�digo)"
    frmMODCOD.caption = sIdiTitCodigo
    frmMODCOD.Left = frmPARFirmas.Left + 500
    frmMODCOD.Top = frmPARFirmas.Top + 1000
    frmMODCOD.txtCodNue = ""
    frmMODCOD.txtCodAct.Text = oFirmaEnEdicion.Cod
    Set frmMODCOD.fOrigen = frmPARFirmas
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
'        oMensajes.NoValido "C�digo"
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set oFirmaEnEdicion = Nothing
        Exit Sub
    End If
        
    If Not IsNumeric(g_sCodigoNuevo) Then
'        oMensajes.NoValido "C�digo"
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set oFirmaEnEdicion = Nothing
        Exit Sub
    End If
    
    If UCase(g_sCodigoNuevo) = UCase(oFirmaEnEdicion.Cod) Then
'        oMensajes.NoValido "Codigo"
        oMensajes.NoValido sIdiCodigo
        Set oIBAseDatosEnEdicion = Nothing
        Set oFirmaEnEdicion = Nothing
        Exit Sub
    End If
            
    ''' Cambiar el codigo
    
    Screen.MousePointer = vbHourglass
    teserror = oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
    Screen.MousePointer = vbNormal
             
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
             
    ''' Actualizar los datos
    
    sdbgUnidades.MoveFirst
    sdbgUnidades.Refresh
    sdbgUnidades.Bookmark = sdbgUnidades.SelBookmarks(0)
    sdbgUnidades.SelBookmarks.RemoveAll
        
    Set oIBAseDatosEnEdicion = Nothing
    Set oFirmaEnEdicion = Nothing
    
End Sub
Private Sub cmdDeshacer_Click()

    ''' * Objetivo: Deshacer la edicion en la Firma actual
    
    sdbgUnidades.CancelUpdate
    sdbgUnidades.DataChanged = False
    
    If Not oFirmaEnEdicion Is Nothing Then
        oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set oFirmaEnEdicion = Nothing
    End If
    
    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    cmdCodigo.Enabled = True
    
    Accion = ACCFirCon
        
End Sub
Private Sub cmdEliminar_Click()

    ''' * Objetivo: Eliminar la Firma actual
    
    If sdbgUnidades.Rows = 0 Then Exit Sub
    
    sdbgUnidades.SelBookmarks.Add sdbgUnidades.Bookmark
    sdbgUnidades.DeleteSelected
    sdbgUnidades.SelBookmarks.RemoveAll
    
End Sub
Private Sub cmdListado_Click()
    AbrirLstParametros "frmPARFirmas", sOrdenListadoDen
End Sub
Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
        sdbgUnidades.AllowAddNew = True
        sdbgUnidades.AllowUpdate = True
        sdbgUnidades.AllowDelete = True
        
        frmPARFirmas.caption = sIdiTitEdicion
        
        cmdModoEdicion.caption = sIdiConsulta
        
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
        cmdCodigo.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCFirCon
    Else
        If sdbgUnidades.DataChanged = True Then
            v = sdbgUnidades.ActiveCell.Value
            If Me.Visible Then sdbgUnidades.SetFocus
            If (v <> "") Then
                sdbgUnidades.ActiveCell.Value = v
            End If
                        
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbgUnidades.AllowAddNew = False
        sdbgUnidades.AllowUpdate = False
        sdbgUnidades.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdCodigo.Visible = False
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
        
        If oUsuarioSummit.Tipo <> TipoDeUsuario.Administrador Then
            cmdCodigo.Visible = False
        End If
    
        frmPARFirmas.caption = sIdiTitConsulta
        
        cmdModoEdicion.caption = sIdiEdicion
        
        cmdRestaurar_Click
        
        bModoEdicion = False
    End If
    
    If Me.Visible Then sdbgUnidades.SetFocus
End Sub
Private Sub cmdRestaurar_Click()

    ''' * Objetivo: Restaurar el contenido de la grid
    ''' * Objetivo: desde la base de datos
    
    Screen.MousePointer = vbHourglass
    
    Me.caption = sIdiTitConsulta
    
    Set oFirmas = Nothing
    
    Set oFirmas = oFSGSRaiz.generar_CFirmas
  
    oFirmas.cargartodaslasfirmas , , , , True
    
    sdbgUnidades.ReBind
    
    Screen.MousePointer = vbNormal
    
    If Me.Visible Then sdbgUnidades.SetFocus
    
End Sub
Private Sub Form_Load()

    ''' * Objetivo: Cargar las Firmas e iniciar
    ''' * Objetivo: el formulario
    
    Me.Width = 6630
    Me.Height = 4785
    CargarRecursos
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If

    Show
    
    Set oFirmas = oFSGSRaiz.generar_CFirmas
   
    Screen.MousePointer = vbHourglass
    oFirmas.cargartodaslasfirmas , , , , True
    Screen.MousePointer = vbNormal
    
    bModoEdicion = False
    
    Accion = ACCFirCon
        
    sOrdenListadoDen = False
    
    ConfigurarSeguridad
    
End Sub
Private Sub Form_Resize()
    
    ''' * Objetivo: Adecuar los controles
    
    Arrange
    
End Sub

''' * Objetivo: Descargar el formulario si no
''' * Objetivo: hay cambios pendientes
Private Sub Form_Unload(Cancel As Integer)
    Dim v As Variant
    
    If sdbgUnidades.DataChanged Then
        v = sdbgUnidades.ActiveCell.Value
        If Me.Visible Then sdbgUnidades.SetFocus
        sdbgUnidades.ActiveCell.Value = v
        
        If (actualizarYSalir() = True) Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    Set oFirmas = Nothing
    Set oFirmaEnEdicion = Nothing
    Set oIBAseDatosEnEdicion = Nothing
    Me.Visible = False
End Sub
Private Sub sdbgUnidades_AfterDelete(RtnDispErrMsg As Integer)

    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    If Me.Visible Then sdbgUnidades.SetFocus
    sdbgUnidades.Bookmark = sdbgUnidades.RowBookmark(sdbgUnidades.Row)
    
End Sub
Private Sub sdbgUnidades_AfterInsert(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
End Sub
Private Sub sdbgUnidades_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
End Sub
Private Sub sdbgUnidades_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

    ''' * Objetivo: Confirmacion antes de eliminar
    
    Dim irespuesta As Integer
    
    DispPromptMsg = 0
    
    If sdbgUnidades.IsAddRow Or Accion = accFirmod Then
        Cancel = True
        Exit Sub
    End If
    
'    irespuesta = oMensajes.PreguntaEliminar("la Firma " & sdbgUnidades.Columns(0).Value & " (" & sdbgUnidades.Columns(1).Value & ")")
    irespuesta = oMensajes.PreguntaEliminar(sIdiLaFirma & " " & sdbgUnidades.Columns(0).Value & " (" & sdbgUnidades.Columns(1).Value & ")")
    
    If irespuesta = vbNo Then Cancel = True
    
End Sub
Private Sub sdbgUnidades_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    
    bValError = False
    Cancel = False
    
    If Trim(sdbgUnidades.Columns(0).Value) = "" Then
'        oMensajes.NoValido "C�digo"
        oMensajes.NoValido sIdiCodigo
        Cancel = True
        GoTo Salir
    Else
        If Not IsNumeric(sdbgUnidades.Columns(0).Value) Then
'            oMensajes.NoValido "C�digo"
            oMensajes.NoValido sIdiCodigo
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If Trim(sdbgUnidades.Columns(1).Value) = "" Then
'        oMensajes.NoValida "Denominacion"
        oMensajes.NoValida sIdiDenominacion
        Cancel = True
        GoTo Salir
    End If
    
Salir:
        
    bValError = Cancel
    If Me.Visible Then sdbgUnidades.SetFocus
        
End Sub
Private Sub sdbgUnidades_Change()

    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacer.Enabled = False Then
    
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
        cmdCodigo.Enabled = False
    
    End If
    
    If Accion = ACCFirCon And Not sdbgUnidades.IsAddRow Then
    
        Set oFirmaEnEdicion = Nothing
        Set oFirmaEnEdicion = oFirmas.Item(CStr(sdbgUnidades.Bookmark))
       
        Set oIBAseDatosEnEdicion = oFirmaEnEdicion
        
        teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            
            TratarError teserror
            sdbgUnidades.DataChanged = False
            sdbgUnidades.Columns(0).Value = oFirmaEnEdicion.Cod
            sdbgUnidades.Columns(1).Value = oFirmaEnEdicion.Den
            teserror.NumError = TESnoerror
            
        End If
        
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgUnidades.SetFocus
        Else
            Accion = accFirmod
        End If
        
            
    End If
    
End Sub
Private Sub sdbgUnidades_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    
    Dim sCodigo As String
    Dim sDenominacion As String
    Dim sHeadCaption As String
    
    If bModoEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgUnidades.Columns(ColIndex).caption
'    sdbgUnidades.Columns(ColIndex).Caption = "Ordenando...."
    sdbgUnidades.Columns(ColIndex).caption = sIdiOrdenando
    
    ''' Volvemos a cargar las Firmas, ordenadas segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    
    Set oFirmas = Nothing
    Set oFirmas = oFSGSRaiz.generar_CFirmas
        
    Select Case ColIndex
        Case 0
            oFirmas.cargartodaslasfirmas , , , , True
        Case 1
            oFirmas.cargartodaslasfirmas , , , True, True
    End Select
    
    Select Case ColIndex
    Case 0
        sOrdenListadoDen = False
    Case 1
        sOrdenListadoDen = True
    End Select
    
    sdbgUnidades.ReBind
    sdbgUnidades.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgUnidades_InitColumnProps()
    
    sdbgUnidades.Columns(0).FieldLen = 4
        
End Sub

Private Sub sdbgUnidades_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
  
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgUnidades.DataChanged = False Then
            
            sdbgUnidades.CancelUpdate
            sdbgUnidades.DataChanged = False
            
            If Not oFirmaEnEdicion Is Nothing Then
                oIBAseDatosEnEdicion.CancelarEdicion
                Set oIBAseDatosEnEdicion = Nothing
                Set oFirmaEnEdicion = Nothing
            Else
                cmdA�adir.Enabled = False
                cmdEliminar.Enabled = False
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = False
            End If
            
            Accion = ACCFirCon
            
        End If
        
    End If
    
End Sub

Private Sub sdbgUnidades_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgUnidades.IsAddRow Then
        sdbgUnidades.Columns(0).Locked = True
        If sdbgUnidades.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
            cmdCodigo.Enabled = True
        End If
    Else
        If sdbgUnidades.DataChanged = True Then cmdDeshacer.Enabled = True
        sdbgUnidades.Columns(0).Locked = False
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgUnidades.Row) Then
                sdbgUnidades.col = 0
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdCodigo.Enabled = False
        
    End If
        
End Sub
Private Sub sdbgUnidades_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)

    ''' * Objetivo: Hemos anyadido una fila al grid,
    ''' * Objetivo: ahora hay que anyadirla a la
    ''' * Objetivo: coleccion
    ''' * Recibe: Buffer con los datos a anyadir y bookmark de la fila
        
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    
    bAnyaError = False
    
    ''' Anyadir a la coleccion
    
    oFirmas.Add "-1", "", oFirmas.Count
    
    ''' Anyadir a la base de datos
    
    Set oFirmaEnEdicion = oFirmas.Item(CStr(oFirmas.Count - 1))
    
    
    oFirmaEnEdicion.Cod = sdbgUnidades.Columns(0).Value
    oFirmaEnEdicion.Den = sdbgUnidades.Columns(1).Value
        
    Set oIBAseDatosEnEdicion = oFirmaEnEdicion
    
    teserror = oIBAseDatosEnEdicion.AnyadirABaseDatos
    
    If teserror.NumError <> TESnoerror Then
        
        v = sdbgUnidades.ActiveCell.Value
        oFirmas.Remove (CStr(oFirmas.Count - 1))
        TratarError teserror
        If Me.Visible Then sdbgUnidades.SetFocus
        bAnyaError = True
        RowBuf.RowCount = 0
        sdbgUnidades.ActiveCell.Value = v
        
    Else
        ''' Registro de acciones
        If gParametrosGenerales.gbActivLog Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.accFiranya, "Cod:" & oFirmaEnEdicion.Cod
        End If
        Accion = ACCFirCon
    End If
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oFirmaEnEdicion = Nothing
        
End Sub
Private Sub sdbgUnidades_UnboundDeleteRow(Bookmark As Variant)

    ''' * Objetivo: Hemos eliminado una fila al grid,
    ''' * Objetivo: ahora hay que eliminarla de la
    ''' * Objetivo: coleccion
    ''' * Recibe: Bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim IndFor As Long, IndFir As Long
        
    ''' Eliminamos de la base de datos
    
    Set oFirmaEnEdicion = oFirmas.Item(CStr(Bookmark))
    
    
    Set oIBAseDatosEnEdicion = oFirmaEnEdicion
    teserror = oIBAseDatosEnEdicion.EliminarDeBaseDatos
        
    If teserror.NumError <> TESnoerror Then
    
        
        TratarError teserror
        sdbgUnidades.Refresh
        If Me.Visible Then sdbgUnidades.SetFocus
        
    Else
    
        ''' Registro de acciones
        
        If gParametrosGenerales.gbActivLog Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.ACCFirEli, "Cod:" & oFirmaEnEdicion.Cod
        End If
            
        ''' Eliminar de la coleccion
        
        IndFir = val(Bookmark)
        
        For IndFor = IndFir To oFirmas.Count - 2
            
            oFirmas.Remove (CStr(IndFor))
            Set oFirmaEnEdicion = oFirmas.Item(CStr(IndFor + 1))
            oFirmas.Add oFirmaEnEdicion.Cod, oFirmaEnEdicion.Den, IndFor
            Set oFirmaEnEdicion = Nothing
            
        Next IndFor
        
        oFirmas.Remove (CStr(IndFor))
       
        Accion = ACCFirCon
                
    End If
    
    Set oIBAseDatosEnEdicion = Nothing
    Set oFirmaEnEdicion = Nothing
    
End Sub
Private Sub sdbgUnidades_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim I As Integer
    Dim j As Integer
    
    Dim oFir As CFirma
    
    Dim iNumFirmas As Integer

    If oFirmas Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    iNumFirmas = oFirmas.Count
    
    If IsNull(StartLocation) Then       'If the grid is empty then
        If ReadPriorRows Then               'If moving backwards through grid then
            p = iNumFirmas - 1                             'pointer = # of last grid row
        Else                                        'else
            p = 0                                       'pointer = # of first grid row
        End If
    Else                                        'If the grid already has data in it then
        p = StartLocation                       'pointer = location just before or after the row where data will be added
    
        If ReadPriorRows Then               'If moving backwards through grid then
                p = p - 1                               'move pointer back one row
        Else                                        'else
                p = p + 1                               'move pointer ahead one row
        End If
    End If
    
    'The pointer (p) now points to the row of the grid where you will start adding data.
    
    For I = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
        
        If p < 0 Or p > iNumFirmas - 1 Then Exit For           'If the pointer is outside the grid then stop this
    
        Set oFir = Nothing
        Set oFir = oFirmas.Item(CStr(p))
    
        For j = 0 To 1
      
            Select Case j
                    
                    Case 0:
                            RowBuf.Value(I, 0) = oFir.Cod
                    Case 1:
                            RowBuf.Value(I, 1) = oFir.Den
                    
            End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
        Next j
    
        RowBuf.Bookmark(I) = p                              'set the value of the bookmark for the current row in the rowbuffer
    
        If ReadPriorRows Then                               'move the pointer forward or backward, depending
            p = p - 1                                           'on which way it's supposed to move
        Else
            p = p + 1
        End If
            r = r + 1                                               'increment the number of rows read
        Next I
    
    RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read

    Set oFir = Nothing
    
End Sub
Private Sub sdbgUnidades_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
    
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    
    bModError = False
    
    ''' Modificamos en la base de datos
    
    oFirmaEnEdicion.Cod = sdbgUnidades.Columns(0).Value
    oFirmaEnEdicion.Den = sdbgUnidades.Columns(1).Value
    
    teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
       
    If teserror.NumError <> TESnoerror Then
        v = sdbgUnidades.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgUnidades.SetFocus
        bModError = True
        RowBuf.RowCount = 0
        sdbgUnidades.ActiveCell.Value = v
    Else
        ''' Registro de acciones
        
        If gParametrosGenerales.gbActivLog Then
            oGestorSeguridad.RegistrarAccion oUsuarioSummit.Cod, accionessummit.accFiranya, "Cod:" & oFirmaEnEdicion.Cod
        End If
        
        Accion = ACCFirCon
        
        Set oIBAseDatosEnEdicion = Nothing
        Set oFirmaEnEdicion = Nothing
    End If
End Sub


Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PARFIRMAS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    caption = Ador(0).Value
    Ador.MoveNext
    sdbgUnidades.Columns(0).caption = Ador(0).Value
    Ador.MoveNext
    sdbgUnidades.Columns(1).caption = Ador(0).Value
    Ador.MoveNext
    cmdA�adir.caption = Ador(0).Value
    Ador.MoveNext
    cmdCodigo.caption = Ador(0).Value
    Ador.MoveNext
    cmdDeshacer.caption = Ador(0).Value
    Ador.MoveNext
    cmdEliminar.caption = Ador(0).Value
    Ador.MoveNext
    cmdListado.caption = Ador(0).Value
    Ador.MoveNext
    cmdModoEdicion.caption = Ador(0).Value
    Ador.MoveNext
    cmdRestaurar.caption = Ador(0).Value
    Ador.MoveNext
    
    sIdiTitCodigo = Ador(0).Value
    Ador.MoveNext
    sIdiCodigo = Ador(0).Value
    Ador.MoveNext
    sIdiTitEdicion = Ador(0).Value
    Ador.MoveNext
    sIdiConsulta = Ador(0).Value
    Ador.MoveNext
    sIdiTitConsulta = Ador(0).Value
    Ador.MoveNext
    sIdiEdicion = Ador(0).Value
    Ador.MoveNext
    sIdiLaFirma = Ador(0).Value
    Ador.MoveNext
    sIdiDenominacion = Ador(0).Value
    Ador.MoveNext
    sIdiOrdenando = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub



