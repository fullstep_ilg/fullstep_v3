VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.UserControl ctlImpuestos 
   ClientHeight    =   4020
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10065
   ScaleHeight     =   4020
   ScaleWidth      =   10065
   Begin SSDataWidgets_B.SSDBDropDown sdbddValorImp 
      Height          =   915
      Left            =   6300
      TabIndex        =   6
      Top             =   2460
      Width           =   3255
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlImpuestos.ctx":0000
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "PPVALOR_ID"
      Columns(0).Name =   "PPVALOR_ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "PAIPROVI"
      Columns(1).Name =   "PAIPROVI"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "VIGENTE"
      Columns(2).Name =   "VIGENTE"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3201
      Columns(3).Caption=   "VAL"
      Columns(3).Name =   "VAL"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox PicBotones 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   10065
      TabIndex        =   1
      Top             =   3645
      Width           =   10065
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   3375
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1125
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   2250
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   0
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
   End
   Begin VB.CheckBox chkAplicarTodos 
      Caption         =   "Aplicar en todas las lineas del pedido"
      Height          =   255
      Left            =   25
      TabIndex        =   0
      Top             =   0
      Width           =   5055
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddImpuestos 
      Height          =   915
      Left            =   1860
      TabIndex        =   7
      Top             =   2370
      Width           =   6615
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlImpuestos.ctx":001C
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   185
      Columns.Count   =   9
      Columns(0).Width=   1773
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5292
      Columns(1).Caption=   "DESCR"
      Columns(1).Name =   "DESCR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "VALOR"
      Columns(2).Name =   "VALOR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1773
      Columns(3).Caption=   "RETENIDO"
      Columns(3).Name =   "RETENIDO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "VIGENTE"
      Columns(4).Name =   "VIGENTE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "IMPUESTO_PAI"
      Columns(5).Name =   "IMPUESTO_PAIPROVI"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "IMPUESTO_ID"
      Columns(6).Name =   "IMPUESTO_ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "IMPUESTO_VALOR"
      Columns(7).Name =   "IMPUESTO_VALOR"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "CONCEPTO"
      Columns(8).Name =   "CONCEPTO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      _ExtentX        =   11668
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddProvincias 
      Height          =   915
      Left            =   5940
      TabIndex        =   8
      Top             =   1200
      Width           =   3255
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlImpuestos.ctx":0038
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   26
      Columns.Count   =   3
      Columns(0).Width=   2381
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3096
      Columns(1).Caption=   "NOMBRE"
      Columns(1).Name =   "NOMBRE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_DEN"
      Columns(2).Name =   "COD_DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddPaises 
      Height          =   915
      Left            =   1260
      TabIndex        =   9
      Top             =   1370
      Width           =   3255
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlImpuestos.ctx":0054
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   2381
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3096
      Columns(1).Caption=   "NOMBRE"
      Columns(1).Name =   "NOMBRE"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_DEN"
      Columns(2).Name =   "COD_DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5741
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgImpuestos 
      Height          =   3315
      Left            =   0
      TabIndex        =   10
      Top             =   300
      Width           =   10035
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      Col.Count       =   18
      stylesets.count =   6
      stylesets(0).Name=   "Heredado"
      stylesets(0).BackColor=   16777147
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "ctlImpuestos.ctx":0070
      stylesets(1).Name=   "gris"
      stylesets(1).BackColor=   12632256
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "ctlImpuestos.ctx":008C
      stylesets(2).Name=   "FondoGris"
      stylesets(2).BackColor=   12632256
      stylesets(2).Picture=   "ctlImpuestos.ctx":00A8
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "ctlImpuestos.ctx":00C4
      stylesets(4).Name=   "NoHeredado"
      stylesets(4).BackColor=   8454143
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "ctlImpuestos.ctx":00E0
      stylesets(5).Name=   "NoVigente"
      stylesets(5).BackColor=   14671839
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "ctlImpuestos.ctx":00FC
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   3
      MaxSelectedRows =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   18
      Columns(0).Width=   1085
      Columns(0).Name =   "USAR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   3200
      Columns(1).Caption=   "IMPUESTO"
      Columns(1).Name =   "IMPUESTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   4233
      Columns(2).Caption=   "DESCR"
      Columns(2).Name =   "DESCR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3043
      Columns(3).Caption=   "PAIS"
      Columns(3).Name =   "PAIS"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3731
      Columns(4).Caption=   "PROVI"
      Columns(4).Name =   "PROVI"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   2170
      Columns(5).Caption=   "VALOR"
      Columns(5).Name =   "VALOR"
      Columns(5).Alignment=   1
      Columns(5).CaptionAlignment=   0
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "ID"
      Columns(6).Name =   "ID"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "HEREDADO"
      Columns(7).Name =   "HEREDADO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "PAIS_ID"
      Columns(8).Name =   "PAIS_ID"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "PROVI_ID"
      Columns(9).Name =   "PROVI_ID"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "VIGENTE"
      Columns(10).Name=   "VIGENTE"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "IMPUESTO_PAI"
      Columns(11).Name=   "IMPUESTO_PAIPROVI"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      Columns(12).Width=   3200
      Columns(12).Visible=   0   'False
      Columns(12).Caption=   "RETENIDO"
      Columns(12).Name=   "RETENIDO"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "IMPUESTO_ID"
      Columns(13).Name=   "IMPUESTO_ID"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   8
      Columns(13).FieldLen=   256
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "IMPUESTO_ALTA"
      Columns(14).Name=   "IMPUESTO_ALTA"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "IMPUESTO_VALOR"
      Columns(15).Name=   "IMPUESTO_VALOR"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   8
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "Concepto"
      Columns(16).Name=   "CONCEPTO"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(17).Width=   3200
      Columns(17).Visible=   0   'False
      Columns(17).Caption=   "COMPATIBILIDAD"
      Columns(17).Name=   "COMPATIBILIDAD"
      Columns(17).DataField=   "Column 17"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      _ExtentX        =   17701
      _ExtentY        =   5847
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "ctlImpuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public GMN1Cod As String
Public GMN2Cod As String
Public GMN3Cod As String
Public GMN4Cod As String

Public ArticuloCod As String

Public Prove As String
Public ConceptoArticuloLinea As Integer
Public ConceptoTipoPedido As Integer
'Variable para saber si tiene permisos para modificar el Maestro de Impuestos
Public PermiteModif As Boolean
'Variable para saber si viene de consulta o de modificaci�n
Public m_bModificar As Boolean
Public FormOrigen As String
Public LinPedido As CLineaPedido
Public VentanaSeguimiento As Boolean
Public NuevaLinea As Boolean
Public VentanaCostes As Boolean
Public CabeceraOLinea As Integer
Public ProveCostes As String
Public ArtCostes As String
Public LineaId As Long
Public Orden As COrdenEntrega
Public Cambio As Double
Public CosteId As Integer
Public PermisoAunSinRecepc As Boolean
Public ImpTotSinImp As Double
Public Caption1 As String
Public Caption2 As String
Public ImporteActualizado As Double
Public ImpuestoActualizado As Double
Public Moneda As String

Public g_Coste As CAtributo
Public g_CosteCabecera As CAtributo
Public g_sOrigen As String
Public g_lAtribId As Long
Public g_iNivelCategoria As Integer
Public g_ConceptoLineaCatalogo As Variant

Private textoMsgBox1 As String
Private textoMsgBox2 As String

Private m_iTipo As tipoimpuesto

''Impuestos en grid
Private oImpuestos As CImpuestos

'' Coleccion de paises
Private m_oPaises As CPaises
Private m_sPais As String
Private m_bCargarComboDesdePai As Boolean

'' Coleccion de provincias
Private m_oProvincias As CProvincias
Private m_sProvi As String
Private m_bCargarComboDesdeProvi As Boolean

'' Coleccion de Impuestos para el combo por pais o pais + provi
Private m_oComboImpuestos As CImpuestos
Private m_sImpuesto As String

'' Control de errores
Private bValError As Boolean

Private oIBaseDatos As IBaseDatos

Private m_oImpuestosExistentes As CImpuestos

Private m_bCambioSelCloseUpTab As Boolean
Private m_bCambioSelCloseUpLocked As Boolean
Public g_bMostrandoLOG As Boolean
Public m_bImpuestosModificados As Boolean

Public Sub ConfigurarSeguridad()
cmdA�adir.Visible = m_bModificar
cmdEliminar.Visible = m_bModificar
cmdDeshacer.Visible = m_bModificar

sdbgImpuestos.AllowAddNew = m_bModificar
sdbgImpuestos.AllowUpdate = m_bModificar
sdbgImpuestos.AllowDelete = m_bModificar

If m_bModificar Then
    cmdRestaurar.Left = cmdDeshacer.Left + cmdDeshacer.Width + 100
Else
    cmdRestaurar.Left = cmdA�adir.Left
End If
CargarGrid
End Sub

Private Sub DatosGridModificados(Optional ByVal b As Boolean = False)
If Not b Then
    b = sdbgImpuestos.DataChanged
End If
Select Case UCase(g_sOrigen)
    Case UCase("frmImpuestoPedido")
        frmImpuestoPedido.g_bImpuestosModificados = b
End Select
End Sub

Public Sub GuardarCambios(Optional Cancel As Integer = 0)
Dim i As Integer
Dim dblImpuest As Double
Dim dblImpuestOrigen As Double
Dim oLineaImpuesto As CLineaDesgloseImpuesto
Dim oLineasImpuesto As CLineasDesgloseImpuesto
Dim oLineaImpuestoOrigen As CLineaDesgloseImpuesto
Dim oLineasImpuestoOrigen As CLineasDesgloseImpuesto
Dim oLinea As CLineaPedido
Dim oImpuesto As CImpuesto
Dim oImpuestosUsar As CImpuestos
Dim vbm1 As Variant

If Not m_bImpuestosModificados Then Exit Sub

If sdbgImpuestos.DataChanged Then
    bValError = False
    DatosGridModificados
    sdbgImpuestos.Update
    If bValError Then
        Cancel = True
        Exit Sub
    End If
End If

Set oImpuestosUsar = oFSGSRaiz.Generar_CImpuestos
For Each oImpuesto In oImpuestos
    For i = 0 To sdbgImpuestos.Rows - 1
        vbm1 = sdbgImpuestos.AddItemBookmark(i)
        If sdbgImpuestos.Columns("USAR").CellValue(vbm1) <> 0 And (sdbgImpuestos.Columns("IMPUESTO").CellValue(vbm1) = oImpuesto.CodImpuesto And sdbgImpuestos.Columns("VALOR").CellValue(vbm1) = CStr(oImpuesto.valor) _
        And sdbgImpuestos.Columns("PAIS_ID").CellValue(vbm1) = oImpuesto.CodPais And sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").CellValue(vbm1) = CStr(oImpuesto.ImpuestoPaisProvincia)) Then
            oImpuestosUsar.Add oImpuesto.IDMat, oImpuesto.CodImpuesto, oImpuesto.Descripcion, oImpuesto.IDImpuesto, oImpuesto.IDValor, oImpuesto.GMN1Cod _
            , oImpuesto.GMN2Cod, oImpuesto.GMN3Cod, oImpuesto.GMN4Cod, oImpuesto.Art, oImpuesto.valor, oImpuesto.Vigencia, oImpuesto.ImpuestoPaisProvincia _
            , oImpuesto.Heredado, oImpuesto.Retenido, oImpuesto.CodPais, oImpuesto.CodProvincia, oImpuesto.DenPais, oImpuesto.DenProvincia, oImpuesto.FECACT, , , , , oImpuesto.GrupoCompatibilidad
        End If
    Next
Next

GMN1Cod = ""
GMN2Cod = ""
GMN3Cod = ""
GMN4Cod = ""

Select Case m_iTipo
    Case tipoimpuesto.CosteCategoria
        If sdbgImpuestos.Rows > 0 Then
            frmCatalogo.sdbgCostesCategoria.Columns("IMPUESTOS").Value = True
        Else
            frmCatalogo.sdbgCostesCategoria.Columns("IMPUESTOS").Value = False
        End If
        frmCatalogo.sdbgCostesCategoria.Refresh
    Case tipoimpuesto.CosteLineaCatalogo
        If UserControl.sdbgImpuestos.Rows > 0 Then
            frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("IMPUESTOS").Value = True
        Else
            frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Columns("IMPUESTOS").Value = False
        End If
        frmCatalogoCostesDescuentos.sdbgCostesLineaCatalogo.Refresh
    Case tipoimpuesto.LineaCatalogo
        If UserControl.sdbgImpuestos.Rows > 0 Then
            frmCatalogo.sdbgAdjudicaciones.Columns("IMPUESTOS_HIDDEN").Value = True
        Else
            frmCatalogo.sdbgAdjudicaciones.Columns("IMPUESTOS_HIDDEN").Value = False
        End If
        frmCatalogo.sdbgAdjudicaciones.Refresh
End Select

If Not oImpuestos Is Nothing Then
    ArticuloCod = IIf(ArticuloCod <> "", CStr(oImpuestos.Count), "")
End If
'Al cerrar la ventana, los impuestos est�n guardados en oImpuestos
If Not VentanaSeguimiento And Not VentanaCostes Then
    Set LinPedido.Impuestos = oImpuestosUsar
Else
    If VentanaSeguimiento Then
        Dim bAct As Boolean
        
        bAct = False
        If VentanaCostes Then
            If PermisoAunSinRecepc Then
                'Impuestos asociados a los costes
                'No se modifican impuestos, si no hay coste asociado
                If Not g_Coste Is Nothing Then
                    If Not NuevaLinea Then
                        'Si solo hay un impuesto y se da a eliminar, con esta condici�n, no llegar�a a actualizarse en BD
                        If CabeceraOLinea = 1 Then
                            bAct = True
                            '(Atributo)g_Coste.Id = 1420 ; (Identificador)g_Coste.IdCD = 8
                            oImpuestos.ModificarInsertarCostesLineaSeguimiento g_Coste.IdCD, LineaId, oImpuestos
                        Else
                            bAct = (CabeceraOLinea = 0)
                            oImpuestos.ModificarInsertarCostesCabeceraSeguimiento g_Coste.IdCD, Orden.Id, oImpuestos
                        End If
                        'End If
                    End If
                End If
            End If
        Else
            bAct = (PermisoAunSinRecepc And Not NuevaLinea)
        End If
        
        If bAct Then
            If Not VentanaCostes Then
                'Por si se abre la ventana de impuestos pero realmente no se modifica nada, no habr�a que sumarle luego los nuevos impuestos
                Set oLineasImpuestoOrigen = oFSGSRaiz.Generar_CLineasDesgloseImpuesto
                oLineasImpuestoOrigen.cargarLineasDesgloseImpuestoOrden Orden.Id
                dblImpuestOrigen = 0
                For Each oLineaImpuestoOrigen In oLineasImpuestoOrigen
                    dblImpuestOrigen = dblImpuestOrigen + oLineaImpuestoOrigen.cuota
                Next
            
                Set oLinea = Orden.LineasPedido.Item(LineaId & "")
                oImpuestos.ModificarInsertarEnSeguimiento LineaId, oImpuestos, oLinea, Orden
                Set oLinea = Nothing
            End If
                     
    
            'Actualizar al instante de modificar el porcentaje de un impuesto
            Set oLineasImpuesto = oFSGSRaiz.Generar_CLineasDesgloseImpuesto
            oLineasImpuesto.cargarLineasDesgloseImpuestoOrden Orden.Id
            dblImpuest = 0
            For Each oLineaImpuesto In oLineasImpuesto
                dblImpuest = dblImpuest + oLineaImpuesto.cuota
            Next
            Orden.Impuestos = dblImpuest
            dblImpuest = CDec(dblImpuest * Cambio)
            frmSeguimiento.sdbgSeguimiento.Columns("IMPUESTOS").Value = FormateoNumerico(dblImpuest, "#,##0.00") & " " & NullToStr(Orden.Moneda)
            
            'Orden.ImporteNeto viene sin el cambio aplicado // dblImpuest ya se le ha aplicado el cambio justo arriba
            frmSeguimiento.sdbgSeguimiento.Columns("IMPTOT").Value = FormateoNumerico(((Orden.ImporteNeto * Cambio) + dblImpuest), "#,##0.00") & " " & NullToStr(Orden.Moneda)
            frmSeguimiento.sdbgSeguimiento.caption = Caption1 & " " & Caption2 & " " & FormateoNumerico((Orden.ImporteNeto + (dblImpuest / Cambio)), "#,##0.00") & " " & gParametrosGenerales.gsMONCEN
           
            ImporteActualizado = FormateoNumerico((Orden.ImporteNeto * Cambio) + dblImpuest, "#,##0.00")
            'ImpuestoActualizado = dblImpuest

            If Cambio <> 0 Then
                Orden.ModificarImporteTotalOrden CDec(ImporteActualizado / Cambio), Orden.Id
            Else
                Orden.ModificarImporteTotalOrden ImporteActualizado, Orden.Id
            End If
        End If
    Else
        If VentanaCostes Then
            If CabeceraOLinea = 1 Then   'Linea
                'Por si en emisi�n se introduce primero algun impuesto, sin haber seleccionado previamente un coste
                If frmPEDIDOS.ctlCostesDescuentos.CostesDescuentos.Count <> 0 Then
                    'Por si se introduce coste en una fila, y se da a impuestos de la siguiente fila
                    If Not g_Coste Is Nothing Then
                        Set g_Coste.ImpuestosCostes = Nothing
                        Set g_Coste.ImpuestosCostes = oFSGSRaiz.Generar_CImpuestos
                            Dim oImpuestoCD As CImpuesto
                            For Each oImpuestoCD In oImpuestos
                                    g_Coste.ImpuestosCostes.Add oImpuestoCD.IDMat, oImpuestoCD.CodImpuesto, oImpuestoCD.Descripcion, oImpuestoCD.IDImpuesto, oImpuestoCD.IDValor, , , , , , oImpuestoCD.valor, oImpuestoCD.Vigencia, , , oImpuestoCD.Retenido, oImpuestoCD.CodPais, oImpuestoCD.CodProvincia, , , , , , , , oImpuestoCD.GrupoCompatibilidad
                            Next
                    End If
                End If
            'Costes cabecera
            Else
                If frmCostesDescuentos.ctlCostesDescuentos.CostesDescuentos.Count <> 0 Then
                    If Not g_CosteCabecera Is Nothing Then
                        Set g_CosteCabecera.ImpuestosCostes = Nothing
                        Set g_CosteCabecera.ImpuestosCostes = oFSGSRaiz.Generar_CImpuestos
                            Dim oImpuestoCDCabecera As CImpuesto
                            For Each oImpuestoCDCabecera In oImpuestos
                                g_CosteCabecera.ImpuestosCostes.Add oImpuestoCDCabecera.IDMat, oImpuestoCDCabecera.CodImpuesto, oImpuestoCDCabecera.Descripcion, oImpuestoCDCabecera.IDImpuesto, oImpuestoCDCabecera.IDValor, , , , , , oImpuestoCDCabecera.valor, oImpuestoCDCabecera.Vigencia, , , oImpuestoCDCabecera.Retenido, oImpuestoCDCabecera.CodPais, oImpuestoCDCabecera.CodProvincia, , , , , , , , oImpuestoCDCabecera.GrupoCompatibilidad
                            Next
                    End If
                End If
            End If
        End If
    End If
End If

'Chk Aplicar A Todos
If chkAplicarTodos.Value = vbChecked Then
    frmPEDIDOS.AplicarImpuestosTodasLineas oImpuestosUsar
End If
m_bImpuestosModificados = False
End Sub

Public Sub Terminate(Optional ByRef Cancel As Integer = 0)
    GuardarCambios Cancel
    If Cancel Then
        Exit Sub
    End If
    Set oIBaseDatos = Nothing
    Set m_oPaises = Nothing
    Set m_oProvincias = Nothing
    Set m_oComboImpuestos = Nothing
    Set m_oImpuestosExistentes = Nothing
    Set oImpuestos = Nothing

End Sub

Private Sub chkAplicarTodos_Click()
If chkAplicarTodos.Value = vbChecked Then
    m_bImpuestosModificados = True
    GuardarCambios
End If
End Sub

'' <summary>
'' Situarnos en la fila de adicion
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdA�adir_Click()
    If sdbgImpuestos.DataChanged Then
        m_bImpuestosModificados = True
        bValError = False
        DatosGridModificados
        sdbgImpuestos.Update
        If bValError = True Then
            Exit Sub
        End If
    End If

    sdbgImpuestos.Scroll 0, sdbgImpuestos.Rows - sdbgImpuestos.Row

    If sdbgImpuestos.VisibleRows > 0 Then

        If sdbgImpuestos.VisibleRows > sdbgImpuestos.Rows Then
            sdbgImpuestos.Row = sdbgImpuestos.Rows
        Else
            sdbgImpuestos.Row = sdbgImpuestos.Rows - (sdbgImpuestos.Rows - sdbgImpuestos.VisibleRows) - 1
        End If

    End If

    sdbgImpuestos.Col = 4
    sdbgImpuestos.Columns("PAIS_ID").Value = gParametrosInstalacion.gsPais

    m_oPaises.CargarTodosLosPaises , , , , , False
    CargarGridConPaises

    sdbgImpuestos.Columns("PAIS").Text = m_oPaises.Item(gParametrosInstalacion.gsPais).Cod & " - " & m_oPaises.Item(gParametrosInstalacion.gsPais).Den
    sdbgImpuestos.Col = 4
    If sdbgImpuestos.Visible Then
        sdbgImpuestos.SetFocus
    End If
    UserControl.cmdDeshacer.Enabled = True
    
    
End Sub

'' <summary>
'' Deshacer la edicion en la grid
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdDeshacer_Click()
    sdbgImpuestos.CancelUpdate
    sdbgImpuestos.DataChanged = False

    UserControl.cmdDeshacer.Enabled = False

    m_bCargarComboDesdePai = False
    m_bCargarComboDesdeProvi = False
End Sub

'' <summary>
'' Elimina el/los impuesto/s seleccionado/s
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdEliminar_Click()
    Dim aIdentificadores As Variant
    Dim aHeredados As Variant
    Dim udtTeserror As TipoErrorSummit
    Dim i As Integer
    
    If sdbgImpuestos.Rows = 0 Then Exit Sub
    If sdbgImpuestos.Row < 0 Then Exit Sub

    If sdbgImpuestos.SelBookmarks.Count = 0 Then sdbgImpuestos.SelBookmarks.Add sdbgImpuestos.Bookmark

    Screen.MousePointer = vbHourglass

    ReDim aIdentificadores(0)
    ReDim aHeredados(0)

    i = 0
    'Guardado en memoria, pero aun no en BD
    While i < sdbgImpuestos.SelBookmarks.Count
        If sdbgImpuestos.Columns("ID").CellValue(sdbgImpuestos.SelBookmarks(i)) = "" Then
            ''No en bbdd, luego quitar del grid
        Else
            If sdbgImpuestos.Columns("HEREDADO").CellValue(sdbgImpuestos.SelBookmarks(i)) Then
            ''A tabla IMPUESTO_NO_MAT
                ReDim Preserve aHeredados(UBound(aHeredados) + 1)

                aHeredados(UBound(aHeredados)) = sdbgImpuestos.Columns("IMPUESTO_VALOR").CellValue(sdbgImpuestos.SelBookmarks(i))
            Else
                'Eliminar
                ReDim Preserve aIdentificadores(UBound(aIdentificadores) + 1)

                aIdentificadores(UBound(aIdentificadores)) = sdbgImpuestos.Columns("ID").CellValue(sdbgImpuestos.SelBookmarks(i))
            End If
        End If

        i = i + 1
    Wend

    'Si se a�ade un impuesto y se elimina inmediatamente despu�s sin hacer cerrado el formulario o haber cambiado de linea
'    If (sdbgImpuestos.Rows > oImpuestos.Count) Then
'        sdbgImpuestos_BeforeUpdate (0)
'    End If
    
    'Se autocompleta el pa�s pero a falta de rellenar los datos del impuesto (Aun no est� la �ltima fila completa en la colecci�n de impuestos)
    If sdbgImpuestos.Rows > oImpuestos.Count Then
        'Se est� eliminando una fila con todos los valores ya bien incluidos pero aun no guardada en la colecci�n (no se ha cambiado de fila o cerrado el formulario)
        If sdbgImpuestos.Columns("IMPUESTO").Value <> "" And sdbgImpuestos.Columns("VALOR").Value <> "" Then
            sdbgImpuestos.DeleteSelected
        End If
    Else
        'Se elimina el impuesto de la colecci�n y se borra de la grid
        oImpuestos.Remove (sdbgImpuestos.AddItemRowIndex(sdbgImpuestos.Bookmark) + 1)   'Se borra el impuesto seleccionado en la grid
        sdbgImpuestos.DeleteSelected
    End If
    
    
    udtTeserror = oImpuestos.EliminarImpuestos(aIdentificadores, aHeredados, GMN1Cod, GMN2Cod, GMN3Cod, GMN4Cod, ArticuloCod, m_iTipo, g_iNivelCategoria)
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        TratarError udtTeserror
        
        Exit Sub
    End If

    DoEvents
    m_bImpuestosModificados = True
    DatosGridModificados m_bImpuestosModificados
    
    sdbgImpuestos.SelBookmarks.RemoveAll
    
    If FormOrigen = "frmSeguimiento" Then
        GuardarCambios
    End If
    Screen.MousePointer = vbNormal
End Sub

'' <summary>
'' Restaurar el contenido de la grid desde la base de datos
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdRestaurar_Click()
    cmdDeshacer_Click

    UserControl.sdbgImpuestos.RemoveAll

    CargarGrid
End Sub



''' <summary>
''' Inicializa el user control
''' </summary>
''' <param name="ParametroEntrada1">Explicaci�npar�metro1</param>
''' <param name="ParametroEntrada2">Explicaci�npar�metro2</param>
''' <returns>Explicaci�nretornodelafunci�n</returns>
''' <remarks>Llamada desde: xxx ; Tiempo m�ximo: 0</remarks>
Public Sub Initialize()
sdbgImpuestos.RemoveAll
Screen.MousePointer = vbHourglass
'Textos de las cabeceras de los grids (TEXTOS WHERE MODULO=443)
CargarRecursos

PonerFieldSeparator

'Llamada desde Seguimiento de Pedidos
If VentanaSeguimiento And Not NuevaLinea Then
    chkAplicarTodos.Visible = False
    If PermisoAunSinRecepc Then
        sdbgImpuestos.Columns("VALOR").Locked = Not PermiteModif
        sdbgImpuestos.Columns("VALOR").CellStyleSet ""
    Else
        sdbgImpuestos.Columns("VALOR").Locked = True
        sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"
        'Si el estado del pedido no permite modificar en seguimiento, es como el caso en el que el usuario no tiene permisos de modificaci�n
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        
        cmdRestaurar.Left = UserControl.cmdA�adir.Left
        
        sdbgImpuestos.AllowAddNew = False
    End If
End If

'LLamada desde Impuestos de los Costes/descuentos
If VentanaCostes Then
    chkAplicarTodos.Visible = False
    If CabeceraOLinea = 1 Then
        frmImpuestoPedido.caption = "Impuestos de costes de l�neas de pedido"
    Else
        If CabeceraOLinea = 0 Then
            frmImpuestoPedido.caption = "Impuestos de costes de cabecera de pedido"
        End If
    End If
        
End If

ConfigurarSeguridad

If VentanaCostes Or VentanaSeguimiento Then
    sdbgImpuestos.Columns("USAR").Visible = False
End If
'Desplegables para Tipo Impuesto, Pais, Provincia
sdbddPaises.AddItem ""
sdbddProvincias.AddItem ""
sdbddImpuestos.AddItem ""
sdbddValorImp.AddItem ""
Set m_oPaises = oFSGSRaiz.Generar_CPaises

'Carga los paises con sus monedas de BD
m_oPaises.CargarTodosLosPaises , , , , , False

CargarGridConPaises

'Desplegables del grid
sdbgImpuestos.Columns("PAIS").DropDownHwnd = sdbddPaises.hWnd
sdbgImpuestos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = sdbddImpuestos.hWnd
sdbgImpuestos.Columns("DESCR").DropDownHwnd = sdbddImpuestos.hWnd
sdbgImpuestos.Columns("VALOR").DropDownHwnd = sdbddValorImp.hWnd

'Llamada a CargarTodosLosImpuestos de CImpuestos
pmCargarTodosLosImpuestos


Screen.MousePointer = vbNormal
End Sub

Private Sub pmCargarTodosLosImpuestos()
Set m_oImpuestosExistentes = oFSGSRaiz.Generar_CImpuestos
m_oImpuestosExistentes.CargarTodosLosImpuestos gParametrosInstalacion.gIdioma
End Sub

Public Sub PonerFieldSeparator()
    Dim sdg As Control
    Dim Name4 As String
    Dim Name5 As String
        
    For Each sdg In UserControl.Controls
        
        Name4 = LCase(Left(sdg.Name, 4))
        Name5 = LCase(Left(sdg.Name, 5))
        
        If Name4 = "sdbg" Or Name4 = "sdbc" Or Name5 = "sdbdd" Or Name4 = "ssdb" Then
            If sdg.DataMode = ssDataModeAddItem Then
                sdg.FieldSeparator = Chr(m_lSeparador)
            End If
        End If
    Next
End Sub

'' <summary>
'' Procedimiento que carga los textos que aparecer�n en el formulario en el idioma que corresponda
'' </summary>
'' <remarks>Llamada desde: form_load ; Tiempo m�ximo: 0</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_IMPUESTO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        sdbgImpuestos.Columns("PAIS").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("PROVI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("IMPUESTO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("DESCR").caption = Ador(0).Value
        Ador.MoveNext
        sdbgImpuestos.Columns("VALOR").caption = Ador(0).Value
        sdbddImpuestos.Columns("VALOR").caption = Ador(0).Value

        Ador.MoveNext
        UserControl.cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value

        Ador.MoveNext
        sdbddPaises.Columns(0).caption = Ador(0).Value
        sdbddProvincias.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddPaises.Columns(1).caption = Ador(0).Value
        sdbddProvincias.Columns(1).caption = Ador(0).Value
        sdbddImpuestos.Columns("DESCR").caption = Ador(0).Value

        Ador.MoveNext
        UserControl.sdbddImpuestos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        UserControl.sdbddImpuestos.Columns("RETENIDO").caption = Ador(0).Value
        
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        textoMsgBox1 = Ador(0).Value    'El concepto asociado al impuesto no es v�lido
        Ador.MoveNext
        textoMsgBox2 = Ador(0).Value    'El grupo de compatibilidad asociado al impuesto no coincide con el resto de impuestos

        Ador.Close
    End If

    Set Ador = Nothing

End Sub

'' <summary>
'' Al cerrar el combo, si ha habido cambio de impuesto, carga descripci�n y valor
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_CloseUp()

    'Al cerrar el desplegable de abajo tras haber elegido uno, traspasa la linea seleccionada al grid de arriba sdbgImpuestos
    If sdbgImpuestos.Columns("IMPUESTO").Value <> "" Or sdbgImpuestos.Columns("DESCR").Value <> "" Then
        If sdbddImpuestos.Columns(0).Value = m_sImpuesto Then
            Exit Sub
        Else
            If sdbgImpuestos.Col = sdbgImpuestos.Columns("DESCR").Position Then
                sdbgImpuestos.Columns("IMPUESTO").Value = sdbddImpuestos.Columns(0).Value
            End If
            If sdbgImpuestos.Col = sdbgImpuestos.Columns("IMPUESTO").Position Then
                sdbgImpuestos.Columns("DESCR").Value = sdbddImpuestos.Columns(1).Value
            End If

            sdbgImpuestos.Columns("RETENIDO").Value = sdbddImpuestos.Columns(3).Value
            sdbgImpuestos.Columns("VIGENTE").Value = sdbddImpuestos.Columns(4).Value
            sdbgImpuestos.Columns("IMPUESTO_ID").Value = sdbddImpuestos.Columns(6).Value
            sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"
            sdbgImpuestos.Columns("CONCEPTO").Value = sdbddImpuestos.Columns(8).Value
             
        End If
    Else
        sdbgImpuestos.Columns("IMPUESTO").Value = ""
        sdbgImpuestos.Columns("DESCR").Value = ""

        sdbgImpuestos.Columns("IMPUESTO_ID").Value = ""
        sdbgImpuestos.Columns("CONCEPTO").Value = ""
    End If
    
    'No poder modificar el valor de un impuesto que ya viene con valor
    sdbgImpuestos.Columns("VALOR").Locked = True
    m_sImpuesto = ""
End Sub

'' <summary>
'' Abrir el combo de Impuestos de la forma adecuada
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddImpuestos_DropDown()
 Dim oComboDesplegable As CImpuestos
 Dim oImpuesto As CImpuesto
 Dim i As Long
 Dim bDistinto As Boolean
 Dim bMismoGrupo As Boolean
 Dim bHaySeleccionados As Boolean
 Dim vbmu As Variant
 Dim l As Long

 bHaySeleccionados = False
 
 ConceptoTipoPedido = ConceptoTipoPedido
 ConceptoArticuloLinea = ConceptoArticuloLinea

 If sdbgImpuestos.Columns("IMPUESTO").Locked = True Then
     sdbddImpuestos.DroppedDown = False
     Exit Sub
 End If

 Screen.MousePointer = vbHourglass
 sdbddImpuestos.RemoveAll

 Set m_oComboImpuestos = m_oImpuestosExistentes
 If sdbgImpuestos.Columns("PAIS_ID").Value <> "" Then
    Set oComboDesplegable = oFSGSRaiz.Generar_CImpuestos
    oComboDesplegable.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, sdbgImpuestos.Columns("PROVI_ID").Value, gParametrosInstalacion.gIdioma
 Else
    Set oComboDesplegable = m_oComboImpuestos
 End If


 For Each oImpuesto In oComboDesplegable
     bDistinto = True 'Por defecto, al empezar a comparar cada elemento de m_oImpuestosExistentes, marcamos como es que es bDistinto
     bMismoGrupo = False
     'A�adir solo los bDistintos a los ya precargados
     For i = 1 To oImpuestos.Count
       If oImpuesto.CodImpuesto = oImpuestos.Item(i).CodImpuesto Then
           bDistinto = False 'Marcar a false porque se ha encontrado un caso en el que son iguales
           Exit For  'Al ser iguales, no hace falta seguir
       End If
     Next
     'Chk USAR (Primera columna)
     With sdbgImpuestos
       For l = 0 To .Rows - 1
           vbmu = .AddItemBookmark(l)
           If .Columns("USAR").CellValue(vbmu) = -1 Or .Columns("USAR").CellValue(vbmu) = 1 Then
               bHaySeleccionados = True
               If CStr(oImpuesto.GrupoCompatibilidad) = .Columns("COMPATIBILIDAD").CellValue(vbmu) Then
                   bMismoGrupo = True
                   Exit For
               End If
           End If
       Next
     End With

      If (bDistinto) Then
          If ConceptoTipoPedido = ConceptoArticuloLinea Then
              If oImpuesto.Concepto = ConceptoArticuloLinea Or oImpuesto.Concepto = 2 Or ConceptoTipoPedido = 2 Then
                  If bHaySeleccionados = False Then
                          sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
                          & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
                          & Chr(m_lSeparador) & oImpuesto.IDImpuesto & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oImpuesto.Concepto
                  Else
                      If bMismoGrupo = True Then
                          sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
                          & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
                          & Chr(m_lSeparador) & oImpuesto.IDImpuesto & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oImpuesto.Concepto
                      End If
                  End If
              End If
          Else
              If ConceptoTipoPedido < ConceptoArticuloLinea Then
                  If oImpuesto.Concepto = ConceptoTipoPedido Or oImpuesto.Concepto = 2 Or ConceptoTipoPedido = 2 Then
                      If bHaySeleccionados = False Then
                              sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
                              & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
                              & Chr(m_lSeparador) & oImpuesto.IDImpuesto & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oImpuesto.Concepto
                      Else
                          If bMismoGrupo = True Then
                              sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
                              & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
                              & Chr(m_lSeparador) & oImpuesto.IDImpuesto & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oImpuesto.Concepto
                          End If
                      End If
     
                  End If
     
              Else
                  If oImpuesto.Concepto = ConceptoArticuloLinea Or oImpuesto.Concepto = 2 Or ConceptoArticuloLinea = 2 Then
                      If bHaySeleccionados = False Then
                              sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
                              & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
                              & Chr(m_lSeparador) & oImpuesto.IDImpuesto & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oImpuesto.Concepto
                      Else
                          If bMismoGrupo = True Then
                              sdbddImpuestos.AddItem oImpuesto.CodImpuesto & Chr(m_lSeparador) & oImpuesto.Descripcion & Chr(m_lSeparador) & oImpuesto.valor _
                              & Chr(m_lSeparador) & oImpuesto.Retenido & Chr(m_lSeparador) & oImpuesto.Vigencia & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia _
                              & Chr(m_lSeparador) & oImpuesto.IDImpuesto & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oImpuesto.Concepto
                          End If
                      End If
                  
                  End If
              End If
          End If
     End If
 Next
 
 'Este segundo bloque, es por si hay algun precargado que no est� en el total de impuestos, por si acaso
 'Se Mira que los elementos de oImpuestos no esten en m_oImpuestosExistentes
For i = 1 To oImpuestos.Count
   bDistinto = True
   For Each oImpuesto In m_oImpuestosExistentes
     If oImpuesto.CodImpuesto = oImpuestos.Item(i).CodImpuesto Then
         bDistinto = False
         Exit For
     End If
   Next
   
   If (bDistinto) Then
        sdbddImpuestos.AddItem oImpuestos.Item(i).CodImpuesto & Chr(m_lSeparador) & oImpuestos.Item(i).Descripcion & Chr(m_lSeparador) & oImpuestos.Item(i).valor _
        & Chr(m_lSeparador) & oImpuestos.Item(i).Retenido & Chr(m_lSeparador) & oImpuestos.Item(i).Vigencia & Chr(m_lSeparador) & oImpuestos.Item(i).ImpuestoPaisProvincia _
        & Chr(m_lSeparador) & oImpuestos.Item(i).IDImpuesto & Chr(m_lSeparador) & ""
        '& ""
   End If
Next
          
If sdbddImpuestos.Rows = 0 Then
    sdbddImpuestos.AddItem ""
End If

If m_sImpuesto = "@@@" Then
    m_sImpuesto = ""
    Screen.MousePointer = vbNormal
    Exit Sub
End If

sdbgImpuestos.ActiveCell.SelStart = 0
sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)

Screen.MousePointer = vbNormal
m_sImpuesto = sdbgImpuestos.Columns("IMPUESTO_VALOR").Value

End Sub
'' <summary>
'' Definir que columna es la de busqueda y seleccion, y cual llevaremos a la grid
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_InitColumnProps()
    If sdbgImpuestos.Col = sdbgImpuestos.Columns("IMPUESTO").Position Then
        sdbddImpuestos.DataFieldList = "Column 0"
        sdbddImpuestos.DataFieldToDisplay = "Column 0"
    ElseIf sdbgImpuestos.Col = sdbgImpuestos.Columns("DESCR").Position Then
        sdbddImpuestos.DataFieldList = "Column 1"
        sdbddImpuestos.DataFieldToDisplay = "Column 1"
    End If
End Sub

'' <summary>
'' Posicionarse en el combo segun la seleccion
'' </summary>
'' <param name="Text">seleccion</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddImpuestos.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddImpuestos.Rows - 1
            bm = sdbddImpuestos.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddImpuestos.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddImpuestos.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
'' <summary>
'' Validar la seleccion (nulo o existente)
'' </summary>
'' <param name="Text">seleccion</param>
'' <param name="RtnPassed">correcto o no</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddImpuestos_ValidateList(Text As String, RtnPassed As Integer)
    Dim oImpuesto As CImpuesto
    Dim oImpuestoCloseUp As CImpuesto
    Dim bEncontrado As Boolean
    If Trim(Text) = "" Then
        RtnPassed = True

        sdbgImpuestos.Columns("IMPUESTO").Value = ""

        sdbgImpuestos.Columns("DESCR").Value = ""
        sdbgImpuestos.Columns("VALOR").Value = ""

        sdbgImpuestos.Columns("VIGENTE").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

        sdbgImpuestos.Columns("IMPUESTO_ID").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""

        sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"
    Else

        '' Comprobar la existencia en la lista
        Text = UCase(Text)

        '' Existencia en bd
        bEncontrado = False
        For Each oImpuesto In m_oImpuestosExistentes
            If UCase(oImpuesto.CodImpuesto) = UCase(sdbgImpuestos.Columns("IMPUESTO").Value) Then
                bEncontrado = True
                Exit For
            End If
        Next

        If bEncontrado Then
            RtnPassed = True

            If sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "1" Then Exit Sub

            bEncontrado = False

            For Each oImpuestoCloseUp In oImpuestos
                If NullToStr(oImpuestoCloseUp.CodImpuesto) = UserControl.sdbgImpuestos.Columns("IMPUESTO").Value Then
                    bEncontrado = True
                    Exit For
                End If
            Next

            If Not bEncontrado Then
                sdbgImpuestos.Columns("DESCR").Value = oImpuesto.Descripcion
                sdbgImpuestos.Columns("VIGENTE").Value = oImpuesto.Vigencia
                sdbgImpuestos.Columns("IMPUESTO_ID").Value = oImpuesto.IDImpuesto
                sdbgImpuestos.Columns("COMPATIBILIDAD").Value = oImpuesto.GrupoCompatibilidad
                sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"
            Else
                sdbgImpuestos.Columns("IMPUESTO").Value = ""
                sdbgImpuestos.Columns("DESCR").Value = ""
                sdbgImpuestos.Columns("COMPATIBILIDAD").Value = ""
                sdbgImpuestos.Columns("IMPUESTO_ID").Value = ""
            End If
            sdbgImpuestos.Columns("VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
        Else
            sdbgImpuestos.Columns("DESCR").Value = ""
            sdbgImpuestos.Columns("VALOR").Value = ""
            sdbgImpuestos.Columns("COMPATIBILIDAD").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_ID").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

            If sdbgImpuestos.Col = sdbgImpuestos.Columns("IMPUESTO").Position Then
                If oMensajes.PreguntaNuevoImpuesto = vbYes Then
                    frmNuevoImpuesto.FormOrigen = "frmImpuestoPedido_NuevoImpuesto"
                    frmNuevoImpuesto.m_sCodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value
                    frmNuevoImpuesto.Show vbModal
                    RtnPassed = False
                    pmCargarTodosLosImpuestos
                Else
                    sdbgImpuestos.Columns("IMPUESTO").Value = ""
                    sdbgImpuestos.Col = 1
                    RtnPassed = False
                End If
            End If
        End If

    End If
End Sub
'' <summary>
'' Al cerrar el combo, si ha habido cambio de provincia, limpia el impuesto
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_CloseUp()
    Dim oImpuesto As CImpuesto

    If sdbgImpuestos.Columns("PROVI").Value <> "" Then
        If sdbddProvincias.Columns(0).Value = m_sProvi Then
            Exit Sub
        Else
            sdbgImpuestos.Columns("PROVI_ID").Value = sdbddProvincias.Columns(0).Value

            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

            If ((sdbgImpuestos.Columns("IMPUESTO_ID").Value = "") Or (sdbgImpuestos.Columns("PAIS_ID").Value = "")) Then
                Exit Sub
            End If
        
            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosValoresDeImpuesto sdbgImpuestos.Columns("IMPUESTO_ID").Value, sdbgImpuestos.Columns("PAIS_ID").Value, sdbgImpuestos.Columns("PROVI_ID").Value, gParametrosInstalacion.gIdioma
              
            If m_oComboImpuestos.Count = 1 Then
                sdbgImpuestos.Columns("VALOR").DropDownHwnd = 0
                sdbgImpuestos.Columns("VALOR").Style = ssStyleEditButton
                sdbgImpuestos.Columns("VALOR").Locked = True
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = m_oComboImpuestos.Item(1).ImpuestoPaisProvincia
                sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = m_oComboImpuestos.Item(1).IDValor
                sdbgImpuestos.Columns("VALOR").Value = m_oComboImpuestos.Item(1).valor
            ElseIf m_oComboImpuestos.Count > 0 Then
                sdbgImpuestos.Columns("VALOR").DropDownHwnd = sdbddValorImp.hWnd
                sdbgImpuestos.Columns("VALOR").Style = ssStyleComboBox
                sdbgImpuestos.Columns("VALOR").Locked = False
                sdbgImpuestos.Columns("VALOR").CellStyleSet ""
                For Each oImpuesto In m_oComboImpuestos
                    sdbddValorImp.AddItem oImpuesto.IDValor & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia & Chr(m_lSeparador) & oImpuesto.Vigencia _
                    & Chr(m_lSeparador) & oImpuesto.valor
                Next
            Else
                sdbgImpuestos.Columns("VALOR").DropDownHwnd = 0
                sdbgImpuestos.Columns("VALOR").Style = ssStyleEditButton
                If Me.PermiteModif Then
                    sdbgImpuestos.Columns("VALOR").Locked = False
                    sdbgImpuestos.Columns("VALOR").CellStyleSet ""
                Else
                    sdbgImpuestos.Columns("VALOR").Locked = True
                    sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"



                End If
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
                sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
                sdbgImpuestos.Columns("VALOR").Value = ""
            End If

        End If

    Else
        sdbgImpuestos.Columns("PROVI_ID").Value = ""
    End If
    m_sProvi = ""
End Sub

'' <summary>
'' Abrir el combo de Paises de la forma adecuada
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_DropDown()

    Dim oPais As CPais

'    If UserControl.sdbgImpuestos.Columns("PROVI").Locked = True Then
'        sdbddProvincias.DroppedDown = False
'        Exit Sub
'    End If

    If sdbgImpuestos.Columns("PAIS").Text = "" Then
        sdbddProvincias.RemoveAll
        sdbddProvincias.AddItem ""
        sdbddProvincias.Refresh

        sdbgImpuestos.ActiveCell.SelStart = 0
        sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)
        Screen.MousePointer = vbNormal

        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    If m_oPaises.Item(sdbgImpuestos.Columns("PAIS_ID").Value) Is Nothing Then
        If m_bCargarComboDesdePai Then
            m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgImpuestos.ActiveCell.Value), , , False
        Else
            m_oPaises.CargarTodosLosPaises , , , , , False
        End If
    End If

    Set oPais = m_oPaises.Item(sdbgImpuestos.Columns("PAIS_ID").Value)

    If m_bCargarComboDesdeProvi And sdbgImpuestos.ActiveCell.Value <> "" Then
        oPais.CargarTodasLasProvinciasDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgImpuestos.ActiveCell.Value), , , False
    Else
        oPais.CargarTodasLasProvincias
    End If

    Set m_oProvincias = oPais.Provincias

    Set oPais = Nothing

    CargarGridConProvincias

    sdbgImpuestos.ActiveCell.SelStart = 0
    sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)

    Screen.MousePointer = vbNormal
    m_sProvi = sdbgImpuestos.Columns("PROVI").Value
End Sub

'' <summary>
'' Definir que columna es la de busqueda y seleccion, y cual llevaremos a la grid
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_InitColumnProps()

    sdbddProvincias.DataFieldList = "Column 0"
    sdbddProvincias.DataFieldToDisplay = "Column 2"

End Sub

'' <summary>
'' Posicionarse en el combo segun la seleccion
'' </summary>
'' <param name="Text">seleccion</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_PositionList(ByVal Text As String)

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddProvincias.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddProvincias.Rows - 1
            bm = sdbddProvincias.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddProvincias.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddProvincias.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

'' <summary>
'' Validar la seleccion (nulo o existente)
'' </summary>
'' <param name="Text">seleccion</param>
'' <param name="RtnPassed">correcto o no</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddProvincias_ValidateList(Text As String, RtnPassed As Integer)
    Dim oPais As CPais
    Dim oImpuesto As CImpuesto
    Dim sProvincia As String

    '' Si es nulo, correcto

    If Text = "" Then
        RtnPassed = True

        sdbgImpuestos.Columns("PROVI").Value = ""
        sdbgImpuestos.Columns("PROVI_ID").Value = ""

        If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
    Else

        '' Comprobar la existencia en la lista

        Set oPais = m_oPaises.Item(sdbgImpuestos.Columns("PAIS_ID").Value)

        'Si no hay pais elegida, no se permite seleccionar provincia
        If oPais Is Nothing Then
            sdbgImpuestos.Columns("PAIS").Value = ""
            sdbgImpuestos.Columns("PAIS_ID").Value = ""

            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""

            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

            Exit Sub
        End If

        If oPais.Provincias Is Nothing Then
            oPais.CargarTodasLasProvincias
            If oPais.Provincias Is Nothing Then
                sdbgImpuestos.Columns("PROVI").Value = ""
                sdbgImpuestos.Columns("PROVI_ID").Value = ""

                If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

                Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
                m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma

                For Each oImpuesto In m_oComboImpuestos
                    If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                        If sdbgImpuestos.Columns("VALOR").Value = "" Then
                            Exit For
                        Else
                            If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                                sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor

                                Exit For
                            End If
                        End If
                    End If
                Next

                Exit Sub
            End If
        End If

        Set m_oProvincias = oPais.Provincias
        CargarGridConProvincias

        sProvincia = Text
        If Not (sdbgImpuestos.Columns("PROVI").Value = "") Then sProvincia = sdbgImpuestos.Columns("PROVI_ID").Value

        If Not oPais.Provincias.Item(sProvincia) Is Nothing Then
            RtnPassed = True
            sdbgImpuestos.Columns("PROVI_ID").Value = oPais.Provincias.Item(sProvincia).Cod
            sdbgImpuestos.Columns("PROVI").Value = oPais.Provincias.Item(sProvincia).Cod & " - " & oPais.Provincias.Item(sProvincia).Den

            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, sdbgImpuestos.Columns("PROVI_ID").Value, gParametrosInstalacion.gIdioma

            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor

                            Exit For
                        End If
                    End If
                End If
            Next
        Else
            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""

            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma

            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor

                            Exit For
                        End If
                    End If
                End If
            Next
        End If

    End If

End Sub


'' <summary>
'' Al cerrar el combo, si ha habido cambio de pais, limpia la provincia e impuesto
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_CloseUp()
    Dim oImpuesto As CImpuesto

    If sdbgImpuestos.Columns("PAIS").Value <> "" Then
        
        If sdbddPaises.Columns(0).Value = m_sPais Then
            Exit Sub
        Else
            sdbgImpuestos.Columns("PAIS_ID").Value = sdbddPaises.Columns(0).Value
            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""

            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("VALOR").Value = ""

            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            If sdbgImpuestos.Columns("IMPUESTO_ID").Value <> "" Then
                m_oComboImpuestos.CargarTodosLosValoresDeImpuesto sdbgImpuestos.Columns("IMPUESTO_ID").Value, sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma
            End If
              
            If m_oComboImpuestos.Count = 1 Then
                sdbgImpuestos.Columns("VALOR").DropDownHwnd = 0
                sdbgImpuestos.Columns("VALOR").Style = ssStyleEditButton
                sdbgImpuestos.Columns("VALOR").Locked = True
                sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = m_oComboImpuestos.Item(1).ImpuestoPaisProvincia
                sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = m_oComboImpuestos.Item(1).IDValor
                sdbgImpuestos.Columns("VALOR").Value = m_oComboImpuestos.Item(1).valor
            ElseIf m_oComboImpuestos.Count > 0 Then
                sdbgImpuestos.Columns("VALOR").DropDownHwnd = sdbddValorImp.hWnd
                sdbgImpuestos.Columns("VALOR").Style = ssStyleComboBox
                sdbgImpuestos.Columns("VALOR").Locked = False
                sdbgImpuestos.Columns("VALOR").CellStyleSet ""
                For Each oImpuesto In m_oComboImpuestos
                    sdbddValorImp.AddItem oImpuesto.IDValor & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia & Chr(m_lSeparador) & oImpuesto.Vigencia _
                    & Chr(m_lSeparador) & oImpuesto.valor
                Next


            Else
                sdbgImpuestos.Columns("VALOR").DropDownHwnd = 0
                sdbgImpuestos.Columns("VALOR").Style = ssStyleEditButton
                If Me.PermiteModif Then
                    sdbgImpuestos.Columns("VALOR").Locked = False
                    sdbgImpuestos.Columns("VALOR").CellStyleSet ""
                End If
                sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
                sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
                sdbgImpuestos.Columns("VALOR").Value = ""
            End If

        End If


    Else
        sdbgImpuestos.Columns("PAIS_ID").Value = ""
    End If
    m_sPais = ""
End Sub

'' <summary>
'' Abrir el combo de Paises de la forma adecuada
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddPaises_DropDown()

    Screen.MousePointer = vbHourglass

'    If UserControl.sdbgImpuestos.Columns("PAIS").Locked = True Then
'        sdbddPaises.DroppedDown = False
'        Exit Sub
'    End If

    If m_bCargarComboDesdePai Then
        m_oPaises.CargarTodosLosPaisesDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgImpuestos.ActiveCell.Value), , , False
    Else
        m_oPaises.CargarTodosLosPaises , , , , , False
    End If

    CargarGridConPaises

    sdbgImpuestos.ActiveCell.SelStart = 0
    sdbgImpuestos.ActiveCell.SelLength = Len(sdbgImpuestos.ActiveCell.Text)

    Screen.MousePointer = vbNormal
    m_sPais = sdbgImpuestos.Columns("PAIS").Value
End Sub

'' <summary>
'' Definir que columna es la de busqueda y seleccion, y cual llevaremos a la grid
'' </summary>
'' <param name="ParametroEntrada1">Explicaci�npar�metro1</param>
'' <param name="ParametroEntrada2">Explicaci�npar�metro2</param>
'' <returns>Explicaci�nretornodelafunci�n</returns>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_InitColumnProps()

    sdbddPaises.DataFieldList = "Column 0"
    sdbddPaises.DataFieldToDisplay = "Column 2"

End Sub

'' <summary>
'' Posicionarse en el combo segun la seleccion
'' </summary>
'' <param name="Text">seleccion</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_PositionList(ByVal Text As String)

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddPaises.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbddPaises.Rows - 1
            bm = sdbddPaises.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddPaises.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddPaises.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

'' <summary>
'' Validar la selecci�n (nulo o existente)
'' </summary>
'' <param name="Text">seleccion</param>
'' <param name="RtnPassed">correcto o no</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbddPaises_ValidateList(Text As String, RtnPassed As Integer)
    Dim oImpuesto As CImpuesto

    If Text = "..." Then RtnPassed = False

    '' Si es nulo, correcto

    If Text = "" Then
        RtnPassed = True

        sdbgImpuestos.Columns("PAIS").Value = ""
        sdbgImpuestos.Columns("PAIS_ID").Value = ""

        sdbgImpuestos.Columns("PROVI").Value = ""
        sdbgImpuestos.Columns("PROVI_ID").Value = ""

        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
    Else

        '' Comprobar la existencia en la lista

        If InStr(Text, " -") > 0 Then Text = Left(Text, InStr(Text, " -") - 1)

        m_oPaises.CargarTodosLosPaises Text, , True, , , False
        If Not m_oPaises.Item(Text) Is Nothing Then
            RtnPassed = True
            sdbgImpuestos.Columns("PAIS_ID").Value = m_oPaises.Item(Text).Cod
            sdbgImpuestos.Columns("PAIS").Value = m_oPaises.Item(Text).Cod & " - " & m_oPaises.Item(Text).Den

            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""

            If Not (sdbgImpuestos.Columns("VALOR").Value = "") Then sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""

            Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
            m_oComboImpuestos.CargarTodosLosImpuestosDePaisProvincia sdbgImpuestos.Columns("PAIS_ID").Value, "", gParametrosInstalacion.gIdioma

            For Each oImpuesto In m_oComboImpuestos
                If oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value Then
                    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia

                    If sdbgImpuestos.Columns("VALOR").Value = "" Then
                        Exit For
                    Else
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor

                            Exit For
                        End If
                    End If
                End If
            Next

        Else
            sdbgImpuestos.Columns("PAIS").Value = ""
            sdbgImpuestos.Columns("PAIS_ID").Value = ""

            sdbgImpuestos.Columns("PROVI").Value = ""
            sdbgImpuestos.Columns("PROVI_ID").Value = ""

            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
            sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
        End If

    End If

End Sub

'' <summary>
'' Cargar combo con la coleccion de Paises
'' </summary>
'' <remarks>Llamada desde: sdbddProvincias_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConProvincias()

    Dim oProvi As CProvincia

    sdbddProvincias.RemoveAll

    For Each oProvi In m_oProvincias
        sdbddProvincias.AddItem oProvi.Cod & Chr(m_lSeparador) & oProvi.Den & Chr(m_lSeparador) & oProvi.Cod & " - " & oProvi.Den
    Next

    If sdbddProvincias.Rows = 0 Then
        sdbddProvincias.AddItem ""
    Else
        If m_bCargarComboDesdeProvi And Not m_oProvincias.EOF Then
            sdbddProvincias.AddItem "..."
        End If
    End If

End Sub

'' <summary>
'' Cargar combo con la coleccion de Paises
'' </summary>
'' <remarks>Llamada desde: sdbddPaises_DropDown ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGridConPaises()

    Dim oPai As CPais

    sdbddPaises.RemoveAll

    For Each oPai In m_oPaises
        sdbddPaises.AddItem oPai.Cod & Chr(m_lSeparador) & oPai.Den & Chr(m_lSeparador) & oPai.Cod & " - " & oPai.Den
    Next

    If sdbddPaises.Rows = 0 Then
        sdbddPaises.AddItem ""
    Else
        If m_bCargarComboDesdePai And Not m_oPaises.EOF Then
            sdbddPaises.AddItem "..."
        End If
    End If
End Sub
'' <summary>
'' Cargar los impuestos
'' </summary>
'' <remarks>Llamada desde: Form_load   cmdRestaurar_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarGrid()
Dim Impuesto As CImpuesto
Dim oImpLin As CImpuesto
Dim oOrden As COrdenEntrega
Dim sLinea As String
Dim i As Long
Dim j As Long
Dim k As Long
Dim sIDValor As String
Dim marcarImpuestos As Integer
Dim oImpuestosLin As CImpuestos

Screen.MousePointer = vbHourglass
sdbgImpuestos.RemoveAll
Set oImpuestos = Nothing
Set oImpuestosLin = Nothing
Set oImpuestos = oFSGSRaiz.Generar_CImpuestos

marcarImpuestos = 0
'Columnas con combos en sdbgImpuestos
sdbgImpuestos.Columns("PAIS").DropDownHwnd = 0
sdbgImpuestos.Columns("PROVI").DropDownHwnd = 0
sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = 0
sdbgImpuestos.Columns("DESCR").DropDownHwnd = 0
Select Case g_sOrigen
    Case "frmCatalogo_ImpuestosCategorias"
        m_iTipo = CosteCategoria
        oImpuestos.CargarTodosLosImpuestosDeCosteDeCategoria g_lAtribId, gParametrosInstalacion.gIdioma, g_iNivelCategoria
    Case "frmCatalogo_ImpuestosCostesLineas"
        m_iTipo = CosteLineaCatalogo
        oImpuestos.CargarTodosLosImpuestosDeCosteDeLineaDeCatalogo g_lAtribId, gParametrosInstalacion.gIdioma
    Case "frmCatalogo_ImpuestosLineas"
        m_iTipo = LineaCatalogo
        oImpuestos.CargarTodosLosImpuestosDeLineaDeCatalogo g_lAtribId, gParametrosInstalacion.gIdioma
    Case Else
        If VentanaSeguimiento Then
            If VentanaCostes Then
                If CabeceraOLinea = 1 Then
                    If NuevaLinea And Not g_bMostrandoLOG Then
                        Set oImpuestos = g_Coste.ImpuestosCostes
                        If oImpuestos Is Nothing Then
                            Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
                            Set g_Coste.ImpuestosCostes = oImpuestos
                        End If
                    Else
                        'En caso de que haya mas de un coste , habr� que cargar los impuestos solo del coste en edici�n: g_Coste.IdCD
                        If Not g_Coste Is Nothing Then
                            oImpuestos.CargarImpuestoCostesLinea LineaId, gParametrosInstalacion.gIdioma, g_Coste.IdCD, g_bMostrandoLOG
                        End If
                    End If
                Else
                    If CabeceraOLinea = 0 Then
                        If Not g_Coste Is Nothing Then
                            oImpuestos.CargarImpuestoCostesCabecera Orden.Id, gParametrosInstalacion.gIdioma, g_Coste.IdCD, g_bMostrandoLOG
                        End If
                    End If
                End If
            Else
                If NuevaLinea Then
                    Set oImpuestos = LinPedido.Impuestos
                    If oImpuestos Is Nothing Then
                        Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
                        Set LinPedido.Impuestos = oImpuestos
                    End If
                Else
                    oImpuestos.CargarImpuestosDesdeBD LineaId, gParametrosInstalacion.gIdioma, g_bMostrandoLOG
                End If
            End If
        Else
            If VentanaCostes Then
                'Bot�n costes
                If CabeceraOLinea = 1 Then 'Linea
                    'Recorremos las lineas que tenga la orden de entrega y me quedo con la linea en tratamiento
                    For i = 1 To frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Count
                        If (frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).Num = frmPEDIDOS.g_oLineaSeleccionada.Num) And (frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).Item = frmPEDIDOS.g_oLineaSeleccionada.Item) Then
                            'Si no  hay costes,no hay impuestos. Se precargan
                            If frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).CostesDescuentos Is Nothing Then
                                Set oImpuestos = Nothing
                                Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
                            Else
                                If Not frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).CostesDescuentos Is Nothing Then
                                    For j = 1 To frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).CostesDescuentos.Count
                                        'Asegurarse primero que se est� en los impuestos del coste seleccionado
                                        If Not g_Coste Is Nothing Then
                                            If frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).CostesDescuentos.Item(j).Cod = g_Coste.Cod Then
                                                'Hay costes e impuestos. Se recogen
                                                If Not frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).CostesDescuentos.Item(j).ImpuestosCostes Is Nothing Then
                                                        Set oImpuestos = frmPEDIDOS.g_oOrdenEntrega.LineasPedido.Item(i).CostesDescuentos.Item(j).ImpuestosCostes
                                                        marcarImpuestos = 1
                                                Else
                                                    'Si hay costes, pero no hay impuestos. Se precargan
                                                    Set oImpuestos = Nothing
                                                    Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
                                                End If
                                            End If
                                        End If
                                    Next
                                End If
                            End If
                        End If
                     Next
                'Costes cabecera
                Else
                    If frmPEDIDOS.g_oOrdenEntrega.CostesDescuentos Is Nothing Then
                        'Si no hay costes, no hay impuestos. Se precargan
                        Set oImpuestos = Nothing
                        Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
                    Else
                        If Not frmPEDIDOS.g_oOrdenEntrega.CostesDescuentos Is Nothing Then
                            For k = 1 To frmPEDIDOS.g_oOrdenEntrega.CostesDescuentos.Count
                                'Asegurarse primero que se est� en los impuestos del coste seleccionado
                                If Not g_CosteCabecera Is Nothing Then
                                    If frmPEDIDOS.g_oOrdenEntrega.CostesDescuentos.Item(k).Cod = g_CosteCabecera.Cod Then
                                        'Hay costes e impuestos. Se recogen
                                        If Not frmPEDIDOS.g_oOrdenEntrega.CostesDescuentos.Item(k).ImpuestosCostes Is Nothing Then
                                            Set oImpuestos = frmPEDIDOS.g_oOrdenEntrega.CostesDescuentos.Item(k).ImpuestosCostes
                                            marcarImpuestos = 1
                                        Else
                                            'Si hay costes, pero no hay impuestos. Se precargan
                                            Set oImpuestos = Nothing
                                            Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            Else
                'Emisi�n de Pedidos-Impuestos de linea
                'Recorremos las lineas que tenga la orden de entrega
                'Y me quedo con la linea en tratamiento
                Set oOrden = frmPEDIDOS.g_oOrdenEntrega
                For i = 1 To oOrden.LineasPedido.Count
                    If (oOrden.LineasPedido.Item(i).Num = frmPEDIDOS.g_oLineaSeleccionada.Num) And (oOrden.LineasPedido.Item(i).Item = frmPEDIDOS.g_oLineaSeleccionada.Item) Then
                        'Primera vez que se entra, se precargan por defecto
                        oImpuestos.CargarTodosLosImpuestosDeMatArt GMN1Cod, GMN2Cod, GMN3Cod, GMN4Cod, ArticuloCod, gParametrosInstalacion.gIdioma, Prove
                        If Not oOrden.LineasPedido.Item(i).Impuestos Is Nothing Then
                        'Se pasan los impuestos que tiene la linea al cerrar la ventana a oImpuestos
                            Set oImpuestosLin = oOrden.LineasPedido.Item(i).Impuestos
                            If oImpuestos.Count = 0 Then
                                Set oImpuestos = oImpuestosLin
                            End If
                        End If
                    End If
                Next
            End If
        End If
End Select
For Each Impuesto In oImpuestos
    If Not oImpuestosLin Is Nothing Then
        For Each oImpLin In oImpuestosLin
            marcarImpuestos = 0
            If oImpLin.CodImpuesto = Impuesto.CodImpuesto And oImpLin.valor = Impuesto.valor And NullToStr(oImpLin.CodPais) = NullToStr(Impuesto.CodPais) And NullToStr(oImpLin.ImpuestoPaisProvincia) = NullToStr(Impuesto.ImpuestoPaisProvincia) Then
                marcarImpuestos = 1
                Exit For
            End If
        Next
    End If

    sIDValor = Impuesto.IDValor & ""
    sLinea = marcarImpuestos & Chr(m_lSeparador) & Impuesto.CodImpuesto & Chr(m_lSeparador) & Impuesto.Descripcion & Chr(m_lSeparador)
    sLinea = sLinea & Impuesto.DenPais & Chr(m_lSeparador) & Impuesto.DenProvincia & Chr(m_lSeparador) & Impuesto.valor & Chr(m_lSeparador) & Impuesto.IDMat & Chr(m_lSeparador)
    sLinea = sLinea & Impuesto.Heredado & Chr(m_lSeparador) & Impuesto.CodPais & Chr(m_lSeparador) & Impuesto.CodProvincia
    sLinea = sLinea & Chr(m_lSeparador) & Impuesto.Vigencia & Chr(m_lSeparador) & Impuesto.ImpuestoPaisProvincia & Chr(m_lSeparador)
    sLinea = sLinea & Impuesto.Retenido & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & sIDValor & Chr(m_lSeparador) & Impuesto.Concepto & Chr(m_lSeparador) & Impuesto.GrupoCompatibilidad
    sdbgImpuestos.AddItem sLinea
Next
Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddValorImp_CloseUp()
    sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = sdbddValorImp.Columns(0).Value
    sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = sdbddValorImp.Columns(1).Value
    sdbgImpuestos.Columns("VIGENTE").Value = sdbddValorImp.Columns(2).Value
    sdbgImpuestos.Columns("VALOR").Value = sdbddValorImp.Columns(3).Value
    sdbgImpuestos.Columns("VALOR").Locked = True
    sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"
End Sub

Private Sub sdbddValorImp_DropDown()
    Dim oImpuesto As CImpuesto
    
    sdbddValorImp.RemoveAll
    If ((sdbgImpuestos.Columns("IMPUESTO_ID").Value = "") Or (sdbgImpuestos.Columns("PAIS_ID").Value = "")) Then
        Exit Sub
    End If

    Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos
    m_oComboImpuestos.CargarTodosLosValoresDeImpuesto sdbgImpuestos.Columns("IMPUESTO_ID").Value, sdbgImpuestos.Columns("PAIS_ID").Value, sdbgImpuestos.Columns("PROVI_ID").Value, gParametrosInstalacion.gIdioma
      
    If m_oComboImpuestos.Count = 1 Then
        sdbgImpuestos.Columns("VALOR").DropDownHwnd = 0
        sdbgImpuestos.Columns("VALOR").Style = ssStyleEditButton
        sdbgImpuestos.Columns("VALOR").Locked = True
        sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = m_oComboImpuestos.Item(1).ImpuestoPaisProvincia
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = m_oComboImpuestos.Item(1).IDValor
        sdbgImpuestos.Columns("VALOR").Value = m_oComboImpuestos.Item(1).valor
    ElseIf m_oComboImpuestos.Count > 0 Then
        sdbgImpuestos.Columns("VALOR").DropDownHwnd = sdbddValorImp.hWnd
        sdbgImpuestos.Columns("VALOR").Style = ssStyleComboBox
        sdbgImpuestos.Columns("VALOR").Locked = False
        sdbgImpuestos.Columns("VALOR").CellStyleSet ""
        For Each oImpuesto In m_oComboImpuestos
            sdbddValorImp.AddItem oImpuesto.IDValor & Chr(m_lSeparador) & oImpuesto.ImpuestoPaisProvincia & Chr(m_lSeparador) & oImpuesto.Vigencia _
            & Chr(m_lSeparador) & oImpuesto.valor
        Next
    Else
        sdbgImpuestos.Columns("VALOR").DropDownHwnd = 0
        sdbgImpuestos.Columns("VALOR").Style = ssStyleEditButton
        If Me.PermiteModif Then
            sdbgImpuestos.Columns("VALOR").Locked = False
            sdbgImpuestos.Columns("VALOR").CellStyleSet ""
        Else
            sdbgImpuestos.Columns("VALOR").Locked = True
            sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"
        End If
        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = ""
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        sdbgImpuestos.Columns("VALOR").Value = ""
        sdbddValorImp.AddItem ""
    End If

End Sub

Private Sub sdbddValorImp_InitColumnProps()
    sdbddValorImp.DataFieldList = "Column 3"
    sdbddValorImp.DataFieldToDisplay = "Column 3"
End Sub

Private Sub sdbddValorImp_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    'On Error Resume Next
    
    sdbddValorImp.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValorImp.Rows - 1
            bm = sdbddValorImp.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValorImp.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddValorImp.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
'' <summary>
'' Posicionar el bookmark en la fila actual despues de eliminar
'' </summary>
'' <param name="RtnDispErrMsg">Si hay error o no</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgImpuestos.Rows = 0) Then
        Exit Sub
    End If
    If sdbgImpuestos.Visible Then sdbgImpuestos.SetFocus
    sdbgImpuestos.Bookmark = sdbgImpuestos.RowBookmark(sdbgImpuestos.Row)
End Sub
'' <summary>
'' Si no hay error, volver a la situacion normal
'' </summary>
'' <param name="RtnDispErrMsg">Si hay error o no</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_AfterInsert(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0

    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
End Sub
'' <summary>
'' Si no hay error, volver a la situacion normal
'' </summary>
'' <param name="RtnDispErrMsg">Si hay error o no</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0

    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False
    
    If FormOrigen = "frmSeguimiento" Then
        GuardarCambios
    End If
End Sub
'' <summary>
'' Evita q se meta texto en la columna valor
'' </summary>
'' <param name="ColIndex">Indice de la columna cambiada</param>
'' <param name="OldValue">Valor anterior</param>
'' <param name="Cancel">Para deshacer el cambio</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgImpuestos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
   If sdbgImpuestos.Columns(ColIndex).Name = "VALOR" Then
        If m_bCambioSelCloseUpLocked Then
            m_bCambioSelCloseUpTab = False
            m_bCambioSelCloseUpLocked = False
            Exit Sub
        End If

        If Not IsNumeric(sdbgImpuestos.Columns(ColIndex).Value) Then
            If Not m_bCambioSelCloseUpTab Then
                oMensajes.NoValido sdbgImpuestos.Columns(ColIndex).caption
            End If
            Cancel = True
            Exit Sub
        End If
        
        'Cerciorarse de que en la columna valor los valores est�n en el rango correcto
        If (CDbl(sdbgImpuestos.Columns(ColIndex).Value)) >= 0 And (CDbl(sdbgImpuestos.Columns(ColIndex).Value)) <= 100 Then
                'No es texto, y es > 0 o <=100
        Else
            oMensajes.NoValido sdbgImpuestos.Columns(ColIndex).caption
            Cancel = True
            Exit Sub
        End If

        m_bCambioSelCloseUpTab = False
    End If
End Sub

'' <summary>
'' No Confirmacion antes de eliminar
'' </summary>
'' <param name="Cancel">Si se cancela o no el borrado</param>
'' <param name="DispPromptMsg">si se`pregunat antes de eliminar</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub
'' <summary>
'' Guarda los cambios producidos el grid
'' </summary>
'' <param name="Cancel">Si se cancela o no el update</param>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oImpuesto As CImpuesto

    bValError = False

    Cancel = False

    If m_bCambioSelCloseUpTab Then
        sdbgImpuestos.Col = 3

        Cancel = True

        m_bCambioSelCloseUpTab = False

        Exit Sub
    End If

    If (sdbgImpuestos.Columns("IMPUESTO").Value = "") Then
        oMensajes.NoValido sdbgImpuestos.Columns("IMPUESTO").caption
        Cancel = True
        bValError = Cancel
        If sdbgImpuestos.Visible Then sdbgImpuestos.SetFocus
        cmdDeshacer.Enabled = True
        Exit Sub
    End If
    
    If (sdbgImpuestos.Columns("DESCR").Value = "") Then
        oMensajes.NoValido sdbgImpuestos.Columns("DESCR").caption
        Cancel = True
        bValError = Cancel
        If sdbgImpuestos.Visible Then sdbgImpuestos.SetFocus
        cmdDeshacer.Enabled = True
        Exit Sub
    End If

    If (sdbgImpuestos.Columns("VALOR").Value = "") Then
        oMensajes.NoValido sdbgImpuestos.Columns("VALOR").caption
        Cancel = True
        bValError = Cancel
        If sdbgImpuestos.Visible Then sdbgImpuestos.SetFocus
        cmdDeshacer.Enabled = True
        Exit Sub
    End If
    
    If (CDbl(sdbgImpuestos.Columns("VALOR").Value)) >= 0 And (CDbl(sdbgImpuestos.Columns("VALOR").Value)) <= 100 Then
                'No es texto, y es > 0 o <=100
    Else
        oMensajes.NoValido sdbgImpuestos.Columns("VALOR").caption
        Cancel = True
        Exit Sub
    End If
    

    If sdbgImpuestos.IsAddRow Then
        'Selecc en el combo IVA 16% pero escribes a mano 20% ->IMPUESTO_VALOR es nulo si no exist�a o X si ya exist�a
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = ""
        For Each oImpuesto In m_oComboImpuestos
            If NullToStr(oImpuesto.CodPais) = sdbgImpuestos.Columns("PAIS_ID").Value Then
                If NullToStr(oImpuesto.CodProvincia) = sdbgImpuestos.Columns("PROVI_ID").Value Then
                    If NullToStr(oImpuesto.IDImpuesto) = sdbgImpuestos.Columns("IMPUESTO_ID").Value Then
                        If NullToStr(oImpuesto.valor) = sdbgImpuestos.Columns("VALOR").Value Then
                            sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor
                            Exit For
                        End If
                    End If
                End If
            End If
        Next

        'No repetidos.
        For Each oImpuesto In oImpuestos
            If sdbgImpuestos.Columns("IMPUESTO_VALOR").Value <> "" Then 'Si has seleccionado pais/provincia y luego impuesto. Es q ya esta en bd.
                If NullToStr(oImpuesto.IDValor) = sdbgImpuestos.Columns("IMPUESTO_VALOR").Value Then
                    oMensajes.DatoDuplicado 200
                    Cancel = True
                    bValError = Cancel
                    If sdbgImpuestos.Visible Then sdbgImpuestos.SetFocus
                    cmdDeshacer.Enabled = True
                    Exit Sub
                End If
            End If
        Next
        
                

        Set oImpuesto = oFSGSRaiz.Generar_CImpuesto

        oImpuesto.Art = ArticuloCod

        oImpuesto.GMN1Cod = GMN1Cod
        oImpuesto.GMN2Cod = GMN2Cod
        oImpuesto.GMN3Cod = GMN3Cod
        oImpuesto.GMN4Cod = GMN4Cod

        'Los impuestos puedes haberlos cogido de pais � pais+provincia � todos.
        '  En caso de todos, estas dando de alta en impuestos_mat y impuestos_pais/impuestos_provi
        '  Eoc, estas dando de alta en impuestos_mat
        oImpuesto.ImpuestoPaisProvincia = sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value
        oImpuesto.IDImpuesto = sdbgImpuestos.Columns("IMPUESTO_ID").Value
        oImpuesto.IDValor = sdbgImpuestos.Columns("IMPUESTO_VALOR").Value

        oImpuesto.CodPais = sdbgImpuestos.Columns("PAIS_ID").Value
        oImpuesto.CodProvincia = sdbgImpuestos.Columns("PROVI_ID").Value

        oImpuesto.DenPais = sdbgImpuestos.Columns("PAIS").Text
        oImpuesto.DenProvincia = sdbgImpuestos.Columns("PROVI").Text

        oImpuesto.CodImpuesto = sdbgImpuestos.Columns("IMPUESTO").Value

        oImpuesto.Descripcion = sdbgImpuestos.Columns("DESCR").Value
        oImpuesto.Heredado = 0
        oImpuesto.valor = sdbgImpuestos.Columns("VALOR").Value
        oImpuesto.Vigencia = sdbgImpuestos.Columns("VIGENTE").Value
        oImpuesto.Retenido = sdbgImpuestos.Columns("RETENIDO").Value
        oImpuesto.Concepto = sdbgImpuestos.Columns("CONCEPTO").Value
        oImpuesto.GrupoCompatibilidad = sdbgImpuestos.Columns("COMPATIBILIDAD").Value
        
        If Prove <> "" Then
            oImpuesto.Tipo = ""
        Else
            oImpuesto.Tipo = m_iTipo
        End If
        oImpuesto.NivelCategoria = g_iNivelCategoria
        oImpuesto.AtribID = g_lAtribId
        
        If PermiteModif Then
            Set oIBaseDatos = oImpuesto
            teserror = oIBaseDatos.AnyadirABaseDatos
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Cancel = True
                bValError = Cancel
                If sdbgImpuestos.Visible Then sdbgImpuestos.SetFocus
                Set oImpuesto = Nothing
                Exit Sub
            End If
        End If
        
        'Asignar valor si o si a .Columns("ID")
        If Not IsMissing(oImpuesto.IDImpuesto) Then
             sdbgImpuestos.Columns("ID").Value = oImpuesto.IDImpuesto
        Else
             sdbgImpuestos.Columns("ID").Value = oImpuesto.IDMat
        End If
        
        sdbgImpuestos.Columns("HEREDADO").Value = 0

        sdbgImpuestos.Columns("IMPUESTO_ID").Value = oImpuesto.IDImpuesto
        sdbgImpuestos.Columns("IMPUESTO_VALOR").Value = oImpuesto.IDValor

        sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "0"

        sdbgImpuestos.Columns("HEREDADO").Value = oImpuesto.Heredado
        sdbgImpuestos.Columns("VIGENTE").Value = oImpuesto.Vigencia

        sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = oImpuesto.ImpuestoPaisProvincia
        
        sdbgImpuestos.Columns("CONCEPTO").Value = oImpuesto.Concepto
        sdbgImpuestos.Columns("COMPATIBILIDAD").Value = oImpuesto.GrupoCompatibilidad

        If (oImpuestos.Item(oImpuesto.IDMat) Is Nothing) Then
            oImpuestos.Add oImpuesto.IDMat, oImpuesto.CodImpuesto, oImpuesto.Descripcion, oImpuesto.IDImpuesto, oImpuesto.IDValor, oImpuesto.GMN1Cod _
            , oImpuesto.GMN2Cod, oImpuesto.GMN3Cod, oImpuesto.GMN4Cod, oImpuesto.Art, oImpuesto.valor, oImpuesto.Vigencia, oImpuesto.ImpuestoPaisProvincia _
            , oImpuesto.Heredado, oImpuesto.Retenido, oImpuesto.CodPais, oImpuesto.CodProvincia, oImpuesto.DenPais, oImpuesto.DenProvincia, oImpuesto.FECACT, , , , , oImpuesto.GrupoCompatibilidad
        End If

        Set oImpuesto = Nothing
        m_bImpuestosModificados = True
        DatosGridModificados m_bImpuestosModificados
    End If
End Sub

'' <summary>
'' Evento que salta al producirse un cambio en el grid
'' </summary>
'' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_Change()

    Dim teserror As TipoErrorSummit
    Dim oImpuesto As CImpuesto

    m_bImpuestosModificados = True
    If cmdDeshacer.Enabled = False Then
        cmdDeshacer.Enabled = True
    End If
    If sdbgImpuestos.Col = 0 Then
        'Si estoy en impuestos de emisi�n actualizo la pantalla
        If FormOrigen = "frmPEDIDOS" Then
            GuardarCambios
            UserControl.Parent.g_oOrdenEntrega.CalcularImportes
            UserControl.Parent.MostrarImportesPedido
        End If
    End If
    
    If Not (sdbgImpuestos.Columns("ID").Value = "") Then
        Set oImpuesto = oImpuestos.Item(sdbgImpuestos.Columns("ID").Value)
    End If

    If oImpuesto Is Nothing Then Exit Sub
    
    If VentanaSeguimiento Then
        oImpuesto.valor = sdbgImpuestos.Columns("VALOR").Value
    Else
        'mirar en bd q no lo hayan tocado
        Set oIBaseDatos = oImpuesto
        teserror = oIBaseDatos.IniciarEdicion

        If (teserror.NumError = TESDatoEliminado) Or (teserror.NumError = TESInfModificada) Then
            TratarError teserror
            sdbgImpuestos.DataChanged = False
            Exit Sub
        End If
    End If
    
End Sub

'' <summary>
'' Captura la tecla Supr (Delete) y cuando �sta es presionada se lanza el evento Click asociado al bot�n eliminar para borrar de la BD y de la grid las lineas seleccionadas.
'' </summary>
'' <param name="KeyCode">Contiene el c�digo interno de la tecla pulsada.</param>
'' <param name="Shift">Es la m�scara, sin uso en esta subrutina.</param>
'' <remarks>Llamada desde: sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgImpuestos_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        If sdbgImpuestos.SelBookmarks.Count > 0 Then
            cmdEliminar_Click
        End If
    End If

    Dim sImpuesto As String
    Dim bm As Variant
    Dim i As Integer
    If (KeyCode < 112 Or KeyCode > 123) And (KeyCode < 37 Or KeyCode > 40) And KeyCode <> 27 Then
        With sdbgImpuestos
            If .Col >= 0 Then
                Select Case .Columns(.Col).Name
                    Case "IMPUESTO"
                        If Not sdbddImpuestos.DroppedDown Then
                            m_sImpuesto = "@@@"
                            'SendKeys "{F4}"
                        End If
                        sImpuesto = .Columns("IMPUESTO").Text
                        sImpuesto = EliminarCaracteresEspeciales(sImpuesto)

                        sdbddImpuestos.MoveFirst
                        If sImpuesto <> "" Then
                            For i = 0 To sdbddImpuestos.Rows - 1
                                bm = sdbddImpuestos.AddItemBookmark(i)
                                If InStr(1, UCase(EliminarCaracteresEspeciales(sdbddImpuestos.Columns("COD").CellText(bm))), UCase(sImpuesto)) > 0 Then
                                    sdbddImpuestos.Bookmark = bm
                                    Exit For
                                End If
                            Next i
                        End If

                        If KeyCode = vbKeyTab Then
                            m_bCambioSelCloseUpTab = True
                        End If
                End Select
            End If
        End With
    End If

End Sub

'' <summary>
'' Evento al moverse de columna o de fila en la grid
'' </summary>
'' <param name="lastRow">Fila desde la que se mueve</param>
'' <param name="LastCol">Columna desde la que se mueve</param>
'' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgImpuestos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    
    'Si se est� a�adiendo un impuesto nuevo, los desplegables activos
    If sdbgImpuestos.IsAddRow Then
        sdbgImpuestos.Columns("PAIS").Locked = Not PermiteModif
        sdbgImpuestos.Columns("PROVI").Locked = Not PermiteModif
        sdbgImpuestos.Columns("IMPUESTO").Locked = Not PermiteModif
    
        sdbgImpuestos.Columns("PAIS").DropDownHwnd = sdbddPaises.hWnd
        sdbgImpuestos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
        sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = sdbddImpuestos.hWnd
        sdbgImpuestos.Columns("DESCR").DropDownHwnd = sdbddImpuestos.hWnd
        sdbgImpuestos.Columns("VALOR").DropDownHwnd = sdbddValorImp.hWnd
    Else
        'En un impuesto ya a�adido, los desplegables bloqueados
        If sdbgImpuestos.Columns("ID").Value <> "" Or sdbgImpuestos.Columns("IMPUESTO").Value <> "" Then
            sdbgImpuestos.Columns("PAIS").Locked = True
            sdbgImpuestos.Columns("PROVI").Locked = True
            sdbgImpuestos.Columns("IMPUESTO").Locked = True
            
            sdbgImpuestos.Columns("PAIS").DropDownHwnd = 0
            sdbgImpuestos.Columns("PROVI").DropDownHwnd = 0
            sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = 0
            sdbgImpuestos.Columns("DESCR").DropDownHwnd = 0
        Else
            UserControl.sdbgImpuestos.Columns("PAIS").Locked = Not PermiteModif
            UserControl.sdbgImpuestos.Columns("PROVI").Locked = Not PermiteModif
            UserControl.sdbgImpuestos.Columns("IMPUESTO").Locked = Not PermiteModif
            sdbgImpuestos.Columns("PAIS").DropDownHwnd = sdbddPaises.hWnd
            sdbgImpuestos.Columns("PROVI").DropDownHwnd = sdbddProvincias.hWnd
            sdbgImpuestos.Columns("IMPUESTO").DropDownHwnd = sdbddImpuestos.hWnd
            sdbgImpuestos.Columns("DESCR").DropDownHwnd = sdbddImpuestos.hWnd
        End If
        sdbgImpuestos.Columns("VALOR").Locked = True
        sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"
        sdbgImpuestos.Columns("VALOR").DropDownHwnd = 0
    End If

    m_bCambioSelCloseUpLocked = sdbgImpuestos.Columns("VALOR").Locked

    
    If sdbgImpuestos.Columns("IMPUESTO_PAIPROVI").Value = "" And sdbgImpuestos.Columns("IMPUESTO_ID").Value <> "" Then
        sdbgImpuestos.Columns("VALOR").Locked = False
        sdbgImpuestos.Columns("VALOR").CellStyleSet ""
        If sdbgImpuestos.Col = 3 Then sdbgImpuestos.Col = 3
    ElseIf sdbgImpuestos.IsAddRow Then
        sdbgImpuestos.Columns("VALOR").Locked = False
        sdbgImpuestos.Columns("VALOR").CellStyleSet ""
        If sdbgImpuestos.Col = 3 Then sdbgImpuestos.Col = 3
    Else
        If Not VentanaSeguimiento Then
            sdbgImpuestos.Columns("VALOR").Locked = True
            sdbgImpuestos.Columns("VALOR").CellStyleSet "gris"
        End If
    End If
    
    If sdbgImpuestos.Columns("HEREDADO").Value = "1" And ArticuloCod = "" Then
        cmdEliminar.Enabled = False
    Else
        cmdEliminar.Enabled = True
    End If
    
    If sdbgImpuestos.Col = sdbgImpuestos.Columns("IMPUESTO").Position Or sdbgImpuestos.Col = sdbgImpuestos.Columns("DESCR").Position Then
        sdbddImpuestos_InitColumnProps
    End If
End Sub

'' <summary>
'' Tras dar de alta un nuevo impuesto, cargar los datos proporcionadoso en la grid
'' </summary>
'' <param name="Id">ID del nuevo impuesto</param>
'' <param name="Tipo">Codigo del nuevo impuesto</param>
'' <param name="Descripcion">Descripcion del nuevo impuesto</param>
'' <param name="Retenido">Si es Retenido el nuevo impuesto</param>
'' <param name="Concepto">Tipo de concepto del nuevo impuesto</param>
'' <param name="GrupoCompatibilidad">Grupo de compatibilidad del nuevo impuesto</param>
'' <param name="FormOrigen">Formulario origen</param>
'' <remarks>Llamada desde: frmNuevoImpuesto/cmdAceptar_Click ; Tiempo m�ximo: 0</remarks>
Public Sub CargaIdImpuesto(ByVal Id As Variant, ByVal Tipo As Variant, ByVal Descripcion As Variant, ByVal Retenido As Variant, ByVal Concepto As Variant, Optional ByVal GrupoCompatibilidad As Variant, Optional ByVal FormOrigen As Variant)
    Set m_oComboImpuestos = oFSGSRaiz.Generar_CImpuestos ' Nothing

    sdbgImpuestos.Columns("IMPUESTO_ID").Value = Id

    sdbgImpuestos.Columns("IMPUESTO").Value = Tipo

    sdbgImpuestos.Columns("DESCR").Value = Descripcion

    sdbgImpuestos.Columns("IMPUESTO_ALTA").Value = "1"

    sdbgImpuestos.Columns("RETENIDO").Value = Retenido
    
    sdbgImpuestos.Columns("CONCEPTO").Value = Concepto
    
    If FormOrigen = "frmImpuestoPedido_NuevoImpuesto" Then
        sdbgImpuestos.Columns("COMPATIBILIDAD").Value = GrupoCompatibilidad
        'se deja marcado como seleccionado para la l�nea de pedido
        sdbgImpuestos.Columns("USAR").Value = "1"
    End If
    
    
    m_oImpuestosExistentes.Add Id, Tipo, Descripcion, Id, , , , , , , , , , , Retenido, , , , , , , , , Concepto, GrupoCompatibilidad

    m_oComboImpuestos.CargarTodosLosImpuestos gParametrosInstalacion.gIdioma
    
    sdbgImpuestos.Columns("VALOR").Locked = Not PermiteModif
    sdbgImpuestos.Columns("VALOR").CellStyleSet ""
End Sub

'' <summary>
'' Tras querer dar de alta un nuevo impuesto, se comprueba que cumple con los requisitos antes de a�adir a BD
'' </summary>
'' <param name="Tipo">Codigo del nuevo impuesto</param>
'' <param name="Retenido">Si es Retenido el nuevo impuesto</param>
'' <param name="Concepto">Tipo de concepto del nuevo impuesto</param>
'' <param name="Grupo Compatibilidad">Grupo Compatibilidad del nuevo impuesto</param>
'' <param name="Descripcion">Descripcion del nuevo impuesto</param>
'' <remarks>Llamada desde: frmNuevoImpuesto/cmdAceptar_Click ; Tiempo m�ximo: 0</remarks>
Public Function ComprobarImpuestoAInsertar(ByVal Concepto As Variant, ByVal GrupoCompatibilidad As Variant) As Boolean
    Dim ConceptoArticuloLinea As Integer
    Dim ConceptoAInsertar As Integer
    Dim CoincideConcepto As Boolean
    Dim CoincideCompatib As Boolean
    Dim oImpuestos As CImpuestos
    Dim InsertarImpuesto As Boolean
    Dim l As Long
    Dim k As Long
    Dim vbmu As Variant
    Dim HaySeleccionados As Boolean
    CoincideConcepto = False
    CoincideCompatib = False
    InsertarImpuesto = False
    HaySeleccionados = False
    Set oImpuestos = Nothing
    Set oImpuestos = oFSGSRaiz.Generar_CImpuestos
    
    'Comprobaci�n concepto impuesto nuevo a introducir con concepto Articulo Linea y Concepto TipoPedido
    ConceptoArticuloLinea = oImpuestos.CargarConceptoArticuloLinea(ArticuloCod)
    ConceptoTipoPedido = ConceptoTipoPedido
    ConceptoAInsertar = Concepto
    
    If ConceptoTipoPedido = ConceptoArticuloLinea Then
            If Concepto = ConceptoArticuloLinea Or Concepto = 2 Or ConceptoTipoPedido = 2 Then
                CoincideConcepto = True
            End If
    Else
        If ConceptoTipoPedido < ConceptoArticuloLinea Then
            If Concepto = ConceptoTipoPedido Or Concepto = 2 Or ConceptoTipoPedido = 2 Then
                CoincideConcepto = True
            End If
        Else
            If Concepto = ConceptoArticuloLinea Or Concepto = 2 Or ConceptoArticuloLinea = 2 Then
                CoincideConcepto = True
            End If
        End If
    End If
    'Comprobaci�n compatibilidad impuesto nuevo con las compatibilidades del combo precargado
    'Si hay seleccionados, se comprueba comparandolo con el grupo de compatibilidad de los seleccionados
    'Si no hay seleccionados, se mira con todos los precargados
    With sdbgImpuestos
     For l = 0 To .Rows - 1
                    vbmu = .AddItemBookmark(l)
                    If .Columns("USAR").CellValue(vbmu) = -1 Or .Columns("USAR").CellValue(vbmu) = 1 Then
                        HaySeleccionados = True
                        If GrupoCompatibilidad = .Columns("COMPATIBILIDAD").CellValue(vbmu) Then
                            CoincideCompatib = True
                            Exit For
                        End If
                    End If
     Next
    End With
    If HaySeleccionados = False Then
            'Caso en el que unicamente exista el impuesto nuevo a insertar
            'No hay ninguno precargado o adicional que ya existiese o por si se hubiese querido eliminar
            'Existiendo solo el nuevo a insertar, �ste cumpliria por mismo la condici�n de compatibilidad
            If sdbgImpuestos.Rows = 1 Then
                CoincideCompatib = True
            Else
                With sdbgImpuestos
                    For k = 0 To .Rows - 2  'la ultima fila no se compara, es el nuevo introducido
                        If GrupoCompatibilidad = .Columns("COMPATIBILIDAD").CellValue(k) Then
                            CoincideCompatib = True
                        End If
                    Next
                End With
            End If
    End If
    If Not CoincideConcepto Then
        MsgBox textoMsgBox1, vbInformation + vbOKOnly, "FULLSTEP"
    End If
    
    If Not CoincideCompatib Then
        MsgBox textoMsgBox2, vbInformation + vbOKOnly, "FULLSTEP"
    End If
    'Si coincide tanto el concepto, como el grupo de compatibilidad, entonces s� se a�ade tambien en BD
    If CoincideConcepto And CoincideCompatib Then
        InsertarImpuesto = True
    End If
    'valor que va a devolver la funci�n
    ComprobarImpuestoAInsertar = InsertarImpuesto
End Function


Private Sub sdbgImpuestos_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer
If VentanaSeguimiento And Not Orden Is Nothing Then
    If NullToDbl0(Orden.LineasPedido.Item(LineaId & "").Baja_LOG) = 1 And Not g_bMostrandoLOG Then
        For i = 1 To sdbgImpuestos.Columns.Count - 1
            sdbgImpuestos.Columns(i).CellStyleSet "FondoGris"
        Next i
        Exit Sub
    End If
End If
For i = 1 To sdbgImpuestos.Columns.Count - 1
    sdbgImpuestos.Columns(i).CellStyleSet "Heredado"
Next i
End Sub

Private Sub UserControl_Resize()
If UserControl.Height < 1700 Then Exit Sub

    If UserControl.Width < 5980 Then
        UserControl.Width = 5980
   End If

    If UserControl.Height < 1000 Then
        UserControl.Height = 1000
    End If

    If sdbgImpuestos.Width > 400 Then
        sdbgImpuestos.Width = Width - 400
    End If
    
    If (picBotones.Top - (chkAplicarTodos.Top + chkAplicarTodos.Height) - 20) > 1815 Then
        sdbgImpuestos.Height = picBotones.Top - (chkAplicarTodos.Top + chkAplicarTodos.Height) - 100
    End If
    
    sdbgImpuestos.Columns("IMPUESTO").Width = sdbgImpuestos.Width * 0.2
    sdbgImpuestos.Columns("DESCR").Width = sdbgImpuestos.Width * 0.26
    sdbgImpuestos.Columns("VALOR").Width = sdbgImpuestos.Width * 0.13
    sdbgImpuestos.Columns("PAIS").Width = sdbgImpuestos.Width * 0.15
End Sub


