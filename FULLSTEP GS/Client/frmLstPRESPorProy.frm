VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstPRESPorProy 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de presupuestos por proyecto (Opciones)"
   ClientHeight    =   5640
   ClientLeft      =   255
   ClientTop       =   1650
   ClientWidth     =   7155
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstPRESPorProy.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   7155
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   7155
      TabIndex        =   3
      Top             =   5265
      Width           =   7155
      Begin VB.CommandButton cmdObtener 
         Caption         =   "&Obtener"
         Height          =   375
         Left            =   5835
         TabIndex        =   2
         Top             =   0
         Width           =   1335
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5285
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   7170
      _ExtentX        =   12647
      _ExtentY        =   9313
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstPRESPorProy.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "frmSel"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Orden"
      TabPicture(1)   =   "frmLstPRESPorProy.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame1"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2220
         Left            =   -74730
         TabIndex        =   12
         Top             =   525
         Width           =   6585
         Begin VB.OptionButton opOrdObj 
            Caption         =   "Objetivo"
            Height          =   240
            Left            =   3465
            TabIndex        =   16
            Top             =   1455
            Width           =   2310
         End
         Begin VB.OptionButton opOrdPres 
            Caption         =   "Presupuesto"
            Height          =   195
            Left            =   690
            TabIndex        =   15
            Top             =   1485
            Width           =   2220
         End
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   3465
            TabIndex        =   14
            Top             =   630
            Width           =   2205
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   690
            TabIndex        =   13
            Top             =   630
            Value           =   -1  'True
            Width           =   2235
         End
      End
      Begin VB.Frame frmSel 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4675
         Left            =   150
         TabIndex        =   5
         Top             =   360
         Width           =   6825
         Begin VB.Frame Frame4 
            Height          =   1320
            Left            =   240
            TabIndex        =   18
            Top             =   315
            Width           =   6375
            Begin VB.CommandButton cmdSelUO 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5910
               Picture         =   "frmLstPRESPorProy.frx":0CEA
               Style           =   1  'Graphical
               TabIndex        =   22
               TabStop         =   0   'False
               Top             =   720
               Width           =   315
            End
            Begin VB.CommandButton cmdBorrarUO 
               Height          =   285
               Left            =   5580
               Picture         =   "frmLstPRESPorProy.frx":0D56
               Style           =   1  'Graphical
               TabIndex        =   21
               Top             =   720
               Width           =   315
            End
            Begin VB.Label lblFiltroUOSel 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   900
               TabIndex        =   20
               Top             =   720
               Width           =   4590
            End
            Begin VB.Label lblFiltroUO 
               Caption         =   "DFiltro por unidad organizativa:"
               Height          =   240
               Left            =   180
               TabIndex        =   19
               Top             =   270
               Width           =   2625
            End
         End
         Begin VB.Frame Frame3 
            Height          =   1270
            Left            =   240
            TabIndex        =   10
            Top             =   3210
            Width           =   6375
            Begin VB.CheckBox chkBajaLog 
               Caption         =   "Incluir bajas l�gicas"
               Height          =   225
               Left            =   240
               TabIndex        =   23
               Top             =   325
               Width           =   2000
            End
            Begin VB.ComboBox cmbOblPP 
               Height          =   315
               Left            =   2400
               Style           =   2  'Dropdown List
               TabIndex        =   1
               Top             =   775
               Width           =   855
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Caption         =   "Desglosar listado a nivel:"
               Height          =   195
               Left            =   240
               TabIndex        =   11
               Top             =   825
               Width           =   1785
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Listar presupuesto del proyecto"
            Height          =   1455
            Left            =   240
            TabIndex        =   6
            Top             =   1710
            Width           =   6375
            Begin VB.CommandButton cmdBorrar 
               Height          =   285
               Left            =   5580
               Picture         =   "frmLstPRESPorProy.frx":0DFB
               Style           =   1  'Graphical
               TabIndex        =   17
               Top             =   840
               Width           =   315
            End
            Begin VB.CommandButton cmdSelProy 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5910
               Picture         =   "frmLstPRESPorProy.frx":0EA0
               Style           =   1  'Graphical
               TabIndex        =   7
               TabStop         =   0   'False
               Top             =   840
               Width           =   315
            End
            Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
               Height          =   285
               Left            =   885
               TabIndex        =   0
               Top             =   375
               Width           =   960
               ScrollBars      =   2
               DataFieldList   =   "Column 0"
               ListAutoValidate=   0   'False
               AllowInput      =   0   'False
               _Version        =   196617
               DataMode        =   2
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns(0).Width=   1693
               Columns(0).Name =   "COD"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               _ExtentX        =   1693
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   16777215
            End
            Begin VB.Label lblAnyo 
               Caption         =   "A�o:"
               ForeColor       =   &H00000000&
               Height          =   225
               Left            =   240
               TabIndex        =   9
               Top             =   420
               Width           =   585
            End
            Begin VB.Label lblProy 
               BackColor       =   &H80000018&
               BorderStyle     =   1  'Fixed Single
               Height          =   285
               Left            =   900
               TabIndex        =   8
               Top             =   855
               Width           =   4590
            End
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4425
         Top             =   120
      End
   End
End
Attribute VB_Name = "frmLstPRESPorProy"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sProy1 As String
Public sProy2 As String
Public sProy3 As String
Public sProy4 As String



'Variables de idioma
Private sIdi1 As String
'Private sIdi3 As String
'Private sIdi4 As String
Private sIdi5 As String
Private sIdi6 As String
Private sEspera(1 To 3) As String
Private txtPag As String
Private txtDe As String
Private txtSeleccion As String
Private txtAnio As String
'Private txtPres As String
Private txtImp As String
Private txtObj As String
Private txtTitulo As String
Private txtBajaLog As String

'unidad organizativa a la que est� restringido (en caso de estarlo)
Public m_sUON1 As String
Public m_sUON2 As String
Public m_sUON3 As String
Public m_sUODescrip As String
Public m_bRuo As Boolean
Public m_sUON1Sel As String
Public m_sUON2Sel As String
Public m_sUON3Sel As String
Public m_bVerBajaLog As Boolean

Function CargarNivelesDeDesglose(iNivel As Integer)
    Dim i As Integer
    
    cmbOblPP.clear
    
    For i = gParametrosGenerales.giNEPC To iNivel Step -1
        If i = 0 Then Exit For
        cmbOblPP.AddItem i
    Next i

End Function


Public Sub MostrarProySeleccionado()
            
    Dim iNivelDesglose As Integer
        
        sdbcAnyo.Text = CStr(frmSELProy.iAnyo)
        If Not frmSELProy.oProy4Seleccionado Is Nothing Then
            lblProy = frmSELProy.oProy4Seleccionado.CodPRES1 & " - " & frmSELProy.oProy4Seleccionado.CodPRES2 & " - " & frmSELProy.oProy4Seleccionado.CodPRES3 & " - " & frmSELProy.oProy4Seleccionado.Cod & " - " & frmSELProy.oProy4Seleccionado.Den
            sProy1 = frmSELProy.oProy4Seleccionado.CodPRES1
            sProy2 = frmSELProy.oProy4Seleccionado.CodPRES2
            sProy3 = frmSELProy.oProy4Seleccionado.CodPRES3
            sProy4 = frmSELProy.oProy4Seleccionado.Cod
            'cmbOblPP.Text = gParametrosGenerales.giNEPP
            iNivelDesglose = 4
            CargarNivelesDeDesglose (iNivelDesglose)
            cmbOblPP.Text = iNivelDesglose
            Exit Sub
        End If
    
        If Not frmSELProy.oProy3Seleccionado Is Nothing Then
            lblProy = frmSELProy.oProy3Seleccionado.CodPRES1 & " - " & frmSELProy.oProy3Seleccionado.CodPRES2 & " - " & frmSELProy.oProy3Seleccionado.Cod & " - " & frmSELProy.oProy3Seleccionado.Den
            sProy1 = frmSELProy.oProy3Seleccionado.CodPRES1
            sProy2 = frmSELProy.oProy3Seleccionado.CodPRES2
            sProy3 = frmSELProy.oProy3Seleccionado.Cod
            sProy4 = ""
            'cmbOblPP.Text = gParametrosGenerales.giNEPP
            iNivelDesglose = 3
            CargarNivelesDeDesglose (iNivelDesglose)
            cmbOblPP.Text = iNivelDesglose
            Exit Sub
        End If
    
        If Not frmSELProy.oProy2Seleccionado Is Nothing Then
            lblProy = frmSELProy.oProy2Seleccionado.CodPRES1 & " - " & frmSELProy.oProy2Seleccionado.Cod & " - " & frmSELProy.oProy2Seleccionado.Den
            sProy1 = frmSELProy.oProy2Seleccionado.CodPRES1
            sProy2 = frmSELProy.oProy2Seleccionado.Cod
            sProy3 = ""
            sProy4 = ""
            'cmbOblPP.Text = gParametrosGenerales.giNEPP
            iNivelDesglose = 2
            CargarNivelesDeDesglose (iNivelDesglose)
            cmbOblPP.Text = iNivelDesglose
            Exit Sub
        End If
    
        If Not frmSELProy.oProy1Seleccionado Is Nothing Then
            lblProy = frmSELProy.oProy1Seleccionado.Cod & " - " & frmSELProy.oProy1Seleccionado.Den
            sProy1 = frmSELProy.oProy1Seleccionado.Cod
            sProy2 = ""
            sProy3 = ""
            sProy4 = ""
            'cmbOblPP.Text = gParametrosGenerales.giNEPP
            iNivelDesglose = 1
            CargarNivelesDeDesglose (iNivelDesglose)
            cmbOblPP.Text = iNivelDesglose
            Exit Sub
        End If
    
        lblProy = ""
        sProy1 = ""
        sProy2 = ""
        sProy3 = ""
        sProy4 = ""
        
End Sub


Private Sub cmdBorrar_Click()
    lblProy.caption = ""
    sProy1 = ""
    sProy2 = ""
    sProy3 = ""
    sProy4 = ""
    CargarNivelesDeDesglose (1)
End Sub

Private Sub cmdBorrarUO_Click()
    lblFiltroUOSel.caption = ""
    m_sUON1Sel = ""
    m_sUON2Sel = ""
    m_sUON3Sel = ""
End Sub

Private Sub cmdObtener_Click()
   ''' * Objetivo: Obtener un listado de Presupuestos por proyectos
    ' ARRAY SelectionTextRpt, Elemento(2, i) = Nombre f�rmula en rpt ; Elemento(1, i) = valor f�rmula (FILAS (Elementos), COLUMNAS (nombre y valor)
    Dim SelectionTextRpt(1 To 2, 1 To 4) As String
    Dim oReport As CRAXDRT.Report
    Dim oCRPresPorProy As CRPresPorProy
    Dim oFos As FileSystemObject
    Dim RepPath As String
    Dim pv As Preview
    Dim i As Integer
    Dim sNivel As String
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Set oCRPresPorProy = GenerarCRPresPorProy
    
    If lblFiltroUOSel.caption = "" Then
        oMensajes.SeleccioneUO
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
           oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptPRESPorPres1.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Set oReport = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass

    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    '*********** FORMULA FIELDS REPORT
    
    SelectionTextRpt(2, 1) = "SEL"
    If lblProy = "" Then
        SelectionTextRpt(1, 1) = ""
    Else
        SelectionTextRpt(1, 1) = gParametrosGenerales.gsPlurPres1 & ": " & lblProy
    End If
               
                
    SelectionTextRpt(2, 2) = "NIVEL"
    If lblProy = "" Then
        If cmbOblPP.Text = "" Then
            SelectionTextRpt(1, 2) = CStr(gParametrosGenerales.giNEPP)
        Else
            SelectionTextRpt(1, 2) = cmbOblPP.Text
        End If
    Else
        SelectionTextRpt(1, 2) = cmbOblPP.Text
    End If
    
    sNivel = SelectionTextRpt(1, 2)
    
    SelectionTextRpt(2, 3) = "TITULO"
    SelectionTextRpt(1, 3) = txtTitulo
                
    SelectionTextRpt(2, 4) = "ANIO"
    SelectionTextRpt(1, 4) = sdbcAnyo.Text
    
    For i = 1 To UBound(SelectionTextRpt, 2)
        oReport.FormulaFields(crs_FormulaIndex(oReport, SelectionTextRpt(2, i))).Text = """" & SelectionTextRpt(1, i) & """"
    Next i
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & txtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAnio")).Text = """" & txtAnio & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPres")).Text = """" & gParametrosGenerales.gsPlurPres1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtImp")).Text = """" & txtImp & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtObj")).Text = """" & txtObj & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtBajaLog")).Text = """" & txtBajaLog & """"
    
   
    oCRPresPorProy.Listado oReport, sNivel, CInt(sdbcAnyo.Text), sProy1, sProy2, sProy3, sProy4, opOrdDen, opOrdCod, opOrdPres, opOrdObj, m_sUON1Sel, m_sUON2Sel, m_sUON3Sel, m_sUODescrip, CBool(chkBajaLog.Value)
 
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
            
    Me.Hide
    
    frmESPERA.lblGeneral.caption = sEspera(2) & " - " & gParametrosGenerales.gsPlurPres1
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.ProgressBar2.Value = 10
    frmESPERA.ProgressBar1.Value = 1
    frmESPERA.lblContacto = sEspera(2)
    frmESPERA.lblDetalle = sEspera(3)
    
    Set pv = New Preview
    pv.Hide
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True
    
    pv.caption = txtTitulo
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    Timer1.Enabled = False
    
    Unload frmESPERA
    Unload Me
    Screen.MousePointer = vbNormal
    Set oReport = Nothing
    Set oCRPresPorProy = Nothing
End Sub

Private Sub cmdSelProy_Click()
    
    If sdbcAnyo = "" Then
        oMensajes.NoValido sIdi5
        If Me.Visible Then sdbcAnyo.SetFocus
        Exit Sub
    End If
    
    frmSELProy.sOrigen = Me.Name
    frmSELProy.iAnyo = sdbcAnyo.Text
    frmSELProy.m_sUON1Sel = m_sUON1Sel
    frmSELProy.m_sUON2Sel = m_sUON2Sel
    frmSELProy.m_sUON3Sel = m_sUON3Sel
    frmSELProy.m_bVerBajaLog = m_bVerBajaLog
    'frmSELProy.Hide
    frmSELProy.Show 1
    
End Sub
Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

Private Sub cmdSelUO_Click()
    frmSELUO.sOrigen = Me.Name
    frmSELUO.bRUO = m_bRuo
    frmSELUO.sUON1 = m_sUON1
    frmSELUO.sUON2 = m_sUON2
    frmSELUO.sUON3 = m_sUON3
    frmSELUO.Show 1
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Me.Height = 6020
    Me.Width = 7245
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
        
    CargarRecursos
        
    txtTitulo = sIdi1 & " - " & gParametrosGenerales.gsPlurPres1
    Me.caption = sIdi1 & " - " & gParametrosGenerales.gsPlurPres1 & " (" & sIdi6 & ")"
    
    Me.Frame2.caption = gParametrosGenerales.gsSingPres1
    
    CargarAnyos
    
    sdbcAnyo.Text = Year(Date)
    sdbcAnyo.ListAutoPosition = True
    DoEvents
            
    For i = gParametrosGenerales.giNEPC To 1 Step -1
        cmbOblPP.AddItem i
    Next i
    
    sProy1 = ""
    sProy2 = ""
    sProy3 = ""
    sProy4 = ""
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTPRESPORPROY, basPublic.gParametrosInstalacion.gIdioma)
   
    If Not Ador Is Nothing Then
    
        SSTab1.TabCaption(0) = Ador(0).Value      '1
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        lblAnyo.caption = Ador(0).Value '4
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        opOrdCod.caption = Ador(0).Value
        Ador.MoveNext
        opOrdDen.caption = Ador(0).Value '7 Denominacion
        Ador.MoveNext
        opOrdPres.caption = Ador(0).Value
        Ador.MoveNext
        opOrdObj.caption = Ador(0).Value
        Ador.MoveNext
        sIdi1 = Ador(0).Value '10
        Ador.MoveNext
        chkBajaLog.caption = Ador(0).Value '11
        'Ador.MoveNext
        'sIdi2 = Ador(0).Value
        'Ador.MoveNext
        'sIdi3 = Ador(0).Value
        'Ador.MoveNext
        'sIdi4 = Ador(0).Value
        Ador.MoveNext
        sIdi5 = Ador(0).Value
        Ador.MoveNext
        sIdi6 = Ador(0).Value '15 Opciones
        Ador.MoveNext
        sEspera(1) = Ador(0).Value
        Ador.MoveNext
        sEspera(2) = Ador(0).Value
        Ador.MoveNext
        sEspera(3) = Ador(0).Value
        Ador.MoveNext
        lblFiltroUO.caption = Ador(0).Value
        
        'Idiomas del RPT
        Ador.MoveNext
        txtPag = Ador(0).Value '200
        Ador.MoveNext
        txtDe = Ador(0).Value
        Ador.MoveNext
        txtSeleccion = Ador(0).Value
        Ador.MoveNext
        txtAnio = Ador(0).Value
        'Ador.MoveNext
        'txtPres = Ador(0).Value
        Ador.MoveNext
        txtImp = Ador(0).Value '205
        Ador.MoveNext
        txtObj = Ador(0).Value
        Ador.MoveNext
        txtBajaLog = Ador(0).Value '207
        
        Ador.Close
    End If
    
    Set Ador = Nothing

        
End Sub

Public Sub MostrarUOSeleccionada()
'*****************************************************************************
'*** Descripci�n: Muestra en la ventana, en el control lblFiltroUOSel la   ***
'***              unidad organizativa seleccionada.                        ***
'***                                                                       ***
'*** Par�metros:  ---------                                                ***
'***                                                                       ***
'*** Valor que devuelve: Simplemente rellena la propiedad Caption del      ***
'***                     control lblFiltroUOSel.                           ***
'*****************************************************************************

    lblFiltroUOSel.caption = ""
    If m_sUON1Sel <> "" Then
        lblFiltroUOSel.caption = m_sUON1Sel
    End If
    If m_sUON2Sel <> "" Then
        lblFiltroUOSel.caption = lblFiltroUOSel.caption & " - " & m_sUON2Sel
    End If
    If m_sUON3Sel <> "" Then
        lblFiltroUOSel.caption = lblFiltroUOSel.caption & " - " & m_sUON3Sel
    End If
    If m_sUODescrip <> "" Then
        If lblFiltroUOSel.caption <> "" Then
            lblFiltroUOSel.caption = lblFiltroUOSel.caption & " - " & m_sUODescrip
        Else
            lblFiltroUOSel.caption = m_sUODescrip
        End If
    End If
End Sub

Private Sub Timer1_Timer()
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If
End Sub
