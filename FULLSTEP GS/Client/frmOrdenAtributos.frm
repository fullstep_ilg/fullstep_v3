VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmOrdenAtributos 
   BackColor       =   &H00808000&
   Caption         =   "DOrden en que se aplicar�n las operaciones de los atributos que influyen sobre el precio"
   ClientHeight    =   4905
   ClientLeft      =   2100
   ClientTop       =   3240
   ClientWidth     =   8940
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmOrdenAtributos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4905
   ScaleWidth      =   8940
   Begin VB.PictureBox Picture2 
      BorderStyle     =   0  'None
      ForeColor       =   &H00000000&
      Height          =   4215
      Left            =   60
      Picture         =   "frmOrdenAtributos.frx":0CB2
      ScaleHeight     =   4215
      ScaleWidth      =   375
      TabIndex        =   6
      Top             =   60
      Width           =   375
      Begin VB.Shape Shape2 
         BackColor       =   &H00FFFFFF&
         BorderColor     =   &H00000000&
         Height          =   4215
         Left            =   0
         Top             =   0
         Width           =   375
      End
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      ForeColor       =   &H00000000&
      Height          =   4215
      Left            =   75
      Picture         =   "frmOrdenAtributos.frx":0EBF
      ScaleHeight     =   4215
      ScaleWidth      =   375
      TabIndex        =   5
      Top             =   60
      Width           =   375
      Begin VB.Shape Shape1 
         BackColor       =   &H00FFFFFF&
         BorderColor     =   &H00000000&
         Height          =   4215
         Left            =   0
         Top             =   0
         Width           =   375
      End
   End
   Begin VB.CommandButton cmdSortAsc 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   8250
      Picture         =   "frmOrdenAtributos.frx":10E7
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   60
      Width           =   495
   End
   Begin VB.CommandButton cmdSortDesc 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Left            =   8250
      Picture         =   "frmOrdenAtributos.frx":1141
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2220
      Width           =   495
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   3300
      MaskColor       =   &H00C9D2D6&
      TabIndex        =   0
      Top             =   4380
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4575
      MaskColor       =   &H00C9D2D6&
      TabIndex        =   1
      Top             =   4380
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgFormulas 
      Height          =   4215
      Left            =   450
      TabIndex        =   2
      Top             =   60
      Width           =   7740
      ScrollBars      =   2
      _Version        =   196617
      DataMode        =   2
      Row.Count       =   9
      Col.Count       =   58
      Row(0).Col(0)   =   "DTO"
      Row(0).Col(1)   =   "-%"
      Row(0).Col(2)   =   "Importe total del proceso"
      Row(0).Col(3)   =   "1"
      Row(1).Col(0)   =   "IMP"
      Row(1).Col(1)   =   "*"
      Row(1).Col(2)   =   "Importe total del proceso"
      Row(1).Col(3)   =   "1"
      Row(2).Col(0)   =   "DTO2"
      Row(2).Col(1)   =   "-%"
      Row(2).Col(2)   =   "Importe total de los grupos: EXC; HOR"
      Row(2).Col(3)   =   "2"
      Row(3).Col(0)   =   "IVA"
      Row(3).Col(1)   =   "+%"
      Row(3).Col(2)   =   "Importe total de los grupos: EXC; HOR"
      Row(3).Col(3)   =   "2"
      Row(4).Col(0)   =   "IMP2"
      Row(4).Col(1)   =   "-"
      Row(4).Col(2)   =   "Importe total de los grupos: HOR"
      Row(4).Col(3)   =   "2"
      Row(5).Col(0)   =   "X2"
      Row(5).Col(1)   =   "*"
      Row(5).Col(2)   =   "Precio total (Unitario*Cantidad) de todos los items del proceso"
      Row(5).Col(3)   =   "3"
      Row(6).Col(0)   =   "X3"
      Row(6).Col(1)   =   "*"
      Row(6).Col(2)   =   "Precio unitario de los items de los grupos: EXC"
      Row(6).Col(3)   =   "4"
      Row(7).Col(0)   =   "X4"
      Row(7).Col(1)   =   "*"
      Row(7).Col(2)   =   "Precio unitario de los items de los grupos: EXC;HOR"
      Row(7).Col(3)   =   "4"
      Row(8).Col(0)   =   "X5"
      Row(8).Col(1)   =   "-%"
      Row(8).Col(3)   =   "4"
      stylesets.count =   6
      stylesets(0).Name=   "Item"
      stylesets(0).BackColor=   10944511
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmOrdenAtributos.frx":119B
      stylesets(1).Name=   "Grupo"
      stylesets(1).BackColor=   49601
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmOrdenAtributos.frx":11B7
      stylesets(2).Name=   "Proceso"
      stylesets(2).BackColor=   11579392
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmOrdenAtributos.frx":11D3
      stylesets(3).Name=   "Selection"
      stylesets(3).ForeColor=   16777215
      stylesets(3).BackColor=   8388608
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmOrdenAtributos.frx":11EF
      stylesets(4).Name=   "Normal"
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmOrdenAtributos.frx":120B
      stylesets(5).Name=   "TotalItem"
      stylesets(5).BackColor=   6467839
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmOrdenAtributos.frx":1227
      BevelColorFrame =   0
      BevelColorHighlight=   16777215
      BevelColorShadow=   8421504
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   1852
      Columns(0).Caption=   "DAtributo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1667
      Columns(1).Caption=   "DOperaci�n"
      Columns(1).Name =   "OPERACION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   9234
      Columns(2).Caption=   "DOperaci�n aplicada a"
      Columns(2).Name =   "APLICARA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "DTipo"
      Columns(3).Name =   "TIPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "ORDEN"
      Columns(4).Name =   "ORDEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "IDATRIBPROCE"
      Columns(5).Name =   "IDATRIBPROCE"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Visible=   0   'False
      Columns(6).Caption=   "USARPREC"
      Columns(6).Name =   "USARPREC"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "APLICARA_NUM"
      Columns(7).Name =   "APLICARA_NUM"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      _ExtentX        =   13652
      _ExtentY        =   7435
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmOrdenAtributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Pantalla que muestra el orden de los atributos aplicados a precio
'24/05/2002

Public g_oProceso As CProceso
Public g_oPlantilla As CPlantilla
Public g_adoRes As Ador.Recordset
Public g_oOrigen As Form
Public g_sOrigen As String
Public g_bNoModif As Boolean

Private m_oAtributos As CAtributos
Private m_sMensaje(5) As String
Private m_sProceso As String
Private m_sGrupo As String

Private m_oAtribsModOrden As CAtributos   'Guardo los atributos aplicados que han cambiado de orden.

Private m_sMensajeModifAtrib As String
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub cmdAceptar_Click()
Dim iIndice As Integer
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Dim bRecalcularOfertas As Boolean
Dim oAtributo As CAtributo
Dim oAtribControl As CAtributo

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        sdbgFormulas.MoveLast
    
        For iIndice = 1 To sdbgFormulas.Rows
            m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).Orden = iIndice
            sdbgFormulas.MovePrevious
            
        Next iIndice
    
        If g_sOrigen = "frmPROCE" And sdbgFormulas.Rows > 0 Then
        
            bRecalcularOfertas = False
            
            'Si tiene ofertas y se ha modificado el orden de alg�n atributo le avisamos antes.
            If Not m_oAtribsModOrden Is Nothing Then
                If m_oAtribsModOrden.Count > 0 And frmPROCE.g_oProcesoSeleccionado.tieneofertas > 1 Then
                    irespuesta = oMensajes.AvisoConConfirmacion(m_sMensajeModifAtrib)
                    If irespuesta = vbNo Then
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
            
                'Guardamos en oAtribControl el atributo con menor orden que se ha modificado para iniciar el recalculo desde ese punto.
                If m_oAtribsModOrden.Count > 0 Then
                    bRecalcularOfertas = True
                    For Each oAtributo In m_oAtribsModOrden
                        If oAtribControl Is Nothing Then
                            Set oAtribControl = m_oAtributos.Item(CStr(oAtributo.idAtribProce))
                        Else
                            If oAtribControl.Orden > m_oAtributos.Item(CStr(oAtributo.idAtribProce)).Orden Then
                                Set oAtribControl = m_oAtributos.Item(CStr(oAtributo.idAtribProce))
                            End If
                        End If
                    Next
                End If
            End If
            
            Set g_oProceso.ATRIBUTOS = m_oAtributos
            
            teserror = g_oProceso.GuardarOrden(oAtribControl)
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Unload frmESPERA
                Screen.MousePointer = vbNormal
                Exit Sub
            Else
                If Not m_oAtribsModOrden Is Nothing Then
                    If m_oAtribsModOrden.Count > 0 Then
                        For Each oAtributo In m_oAtribsModOrden
                            'Registro la acci�n (modificaci�n en el orden de los atributos)
                            basSeguridad.RegistrarAccion ACCCondOfeAtrOrden, "Anyo:" & oAtributo.AnyoProce & " GMN1:" & oAtributo.GMN1Proce & " Cod:" & oAtributo.CodProce & "Atributo:" & oAtributo.idAtribProce & "Asc/Desc:" & oAtributo.Den & "Operacion:" & oAtributo.PrecioFormula
                        Next
                    End If
                End If
            End If
            
        ElseIf g_sOrigen = "PLANTILLAS" Then
            Set g_oPlantilla.ATRIBUTOS = m_oAtributos
            teserror = g_oPlantilla.GuardarOrden
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Unload frmESPERA
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
        ElseIf g_sOrigen = "frmADJ" Then
            'Si se aplica en la comparativa
            frmADJ.ModificarOrdenAtributos m_oAtributos
        
        ElseIf g_sOrigen = "frmRESREU" Then
            'si se aplica en el panel de toma de decisiones
            frmRESREU.ModificarOrdenAtributos m_oAtributos
            
        ElseIf g_sOrigen = "frmADJItem" Then
            'Si se aplica en la comparativa de item
            g_oOrigen.g_oOrigen.ModificarOrdenAtributos m_oAtributos
            g_oOrigen.itemSeleccionado
            g_oOrigen.ModificarOrdenAtributos m_oAtributos
        End If
    
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSortAsc_Click()
Dim sText As String
Dim sText2 As String
Dim sAplicar1 As String
Dim sAplicar2 As String
Dim sOrd1 As String
Dim sOrd2 As String
Dim sOp1 As String
Dim sOp2 As String
Dim sIdAtribProce1 As String
Dim sIdAtribProce2 As String
Dim sTipo As String
Dim sUsarPrec1 As String
Dim sUsarPrec2 As String
Dim oUsar As CAtribTotalGrupo


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgFormulas.Rows = 0 Then Exit Sub

If sdbgFormulas.Row < 1 Then Exit Sub
    sText = sdbgFormulas.Columns("COD").Value
    sAplicar1 = sdbgFormulas.Columns("APLICARA").Value
    sOp1 = sdbgFormulas.Columns("OPERACION").Value
    sOrd1 = sdbgFormulas.Columns("ORDEN").Value
    sTipo = sdbgFormulas.Columns("TIPO").Value
    sIdAtribProce1 = sdbgFormulas.Columns("IDATRIBPROCE").Value
    sUsarPrec1 = sdbgFormulas.Columns("USARPREC").Value
    sdbgFormulas.MovePrevious
    If sTipo <> sdbgFormulas.Columns("TIPO").Value Then Exit Sub
    sText2 = sdbgFormulas.Columns("COD").Value
    sAplicar2 = sdbgFormulas.Columns("APLICARA").Value
    sOp2 = sdbgFormulas.Columns("OPERACION").Value
    sOrd2 = sdbgFormulas.Columns("ORDEN").Value
    sIdAtribProce2 = sdbgFormulas.Columns("IDATRIBPROCE").Value
    sUsarPrec2 = sdbgFormulas.Columns("USARPREC").Value
    sdbgFormulas.Columns("COD").Value = sText
    sdbgFormulas.Columns("APLICARA").Value = sAplicar1
    sdbgFormulas.Columns("ORDEN").Value = sOrd1
    sdbgFormulas.Columns("OPERACION").Value = sOp1
    sdbgFormulas.Columns("IDATRIBPROCE").Value = sIdAtribProce1
    sdbgFormulas.Columns("USARPREC").Value = sUsarPrec1
'    m_bRespetarLista = True
    sdbgFormulas.MoveNext
    sdbgFormulas.Columns("COD").Value = sText2
    sdbgFormulas.Columns("APLICARA").Value = sAplicar2
    sdbgFormulas.Columns("ORDEN").Value = sOrd2
    sdbgFormulas.Columns("OPERACION").Value = sOp2
    sdbgFormulas.Columns("IDATRIBPROCE").Value = sIdAtribProce2
    sdbgFormulas.Columns("USARPREC").Value = sUsarPrec2
    
    sdbgFormulas.SelBookmarks.RemoveAll
    sdbgFormulas.MovePrevious
'    m_bRespetarLista = False
    sdbgFormulas.SelBookmarks.Add sdbgFormulas.Bookmark
    
    'Solo se realizar� el recalculo si se cambia el orden a un atributo aplicado.
    Dim bComprobar As Boolean
    If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).Tipo = TipoNumerico And Not IsNull(m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).PrecioAplicarA) Then
        If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).PrecioAplicarA = TotalGrupo Then   'es de �mbito grupo.Miramos en la tabla USAR_GR_ATRIB para ver si se est� aplicando
            If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).AtribTotalGrupo Is Nothing Then
                m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).CargarUsarPrecTotalGrupo
            End If
            If Not m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).AtribTotalGrupo Is Nothing Then
                For Each oUsar In m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).AtribTotalGrupo
                    If oUsar.UsarPrec = 1 Then
                        bComprobar = True
                        Exit For
                    End If
                Next
            End If
        Else
            If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).UsarPrec = 1 Then bComprobar = True
        End If
    End If
       
    If (g_oProceso.tieneofertas > 0) And bComprobar Then
        If m_oAtribsModOrden.Item(CStr(sIdAtribProce1)) Is Nothing Then
            m_oAtribsModOrden.Add sIdAtribProce1, sText, "A", sTipo, idAtribProce:=sIdAtribProce1, PrecioFormula:=sOp1, PrecioAplicarA:=sTipo, bUsarIdProceAtrib:=True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "cmdSortAsc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub

Private Sub cmdSortDesc_Click()
Dim sText As String
Dim sText2 As String
Dim sAplicar1 As String
Dim sAplicar2 As String
Dim sOrd1 As String
Dim sOrd2 As String
Dim sOp1 As String
Dim sOp2 As String
Dim sTipo As String
Dim sIdAtribProce1 As String
Dim sIdAtribProce2 As String
Dim sUsarPrec1 As String
Dim sUsarPrec2 As String
Dim oUsar As CAtribTotalGrupo

'If sdbgFormulas.SelBookmarks.Count = 0 Then Exit Sub
'If val(sdbgFormulas.SelBookmarks.Item(0)) = sdbgFormulas.Rows - 1 Then Exit Sub
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If sdbgFormulas.Row = sdbgFormulas.Rows - 1 Then Exit Sub

    sText = sdbgFormulas.Columns("COD").Value
    sAplicar1 = sdbgFormulas.Columns("APLICARA").Value
    sOp1 = sdbgFormulas.Columns("OPERACION").Value
    sOrd1 = sdbgFormulas.Columns("ORDEN").Value
    sTipo = sdbgFormulas.Columns("TIPO").Value
    sIdAtribProce1 = sdbgFormulas.Columns("IDATRIBPROCE").Value
    sUsarPrec1 = sdbgFormulas.Columns("USARPREC").Value
    sdbgFormulas.MoveNext
    If sTipo <> sdbgFormulas.Columns("TIPO").Value Then Exit Sub
    sText2 = sdbgFormulas.Columns("COD").Value
    sAplicar2 = sdbgFormulas.Columns("APLICARA").Value
    sOp2 = sdbgFormulas.Columns("OPERACION").Value
    sOrd2 = sdbgFormulas.Columns("ORDEN").Value
    sIdAtribProce2 = sdbgFormulas.Columns("IDATRIBPROCE").Value
    sUsarPrec2 = sdbgFormulas.Columns("USARPREC").Value
    sdbgFormulas.Columns("COD").Value = sText
    sdbgFormulas.Columns("APLICARA").Value = sAplicar1
    sdbgFormulas.Columns("ORDEN").Value = sOrd1
    sdbgFormulas.Columns("OPERACION").Value = sOp1
    sdbgFormulas.Columns("IDATRIBPROCE").Value = sIdAtribProce1
    sdbgFormulas.Columns("USARPREC").Value = sUsarPrec1
'    m_bRespetarLista = True
    sdbgFormulas.MovePrevious
    sdbgFormulas.Columns("COD").Value = sText2
    sdbgFormulas.Columns("APLICARA").Value = sAplicar2
    sdbgFormulas.Columns("OPERACION").Value = sOp2
    sdbgFormulas.Columns("ORDEN").Value = sOrd2
    sdbgFormulas.Columns("IDATRIBPROCE").Value = sIdAtribProce2
    sdbgFormulas.Columns("USARPREC").Value = sUsarPrec2
    sdbgFormulas.SelBookmarks.RemoveAll
    sdbgFormulas.MoveNext
'    m_bRespetarLista = False
    sdbgFormulas.SelBookmarks.Add sdbgFormulas.Bookmark

    'Solo se realizar� el recalculo si se cambia el orden a un atributo aplicado.
    Dim bComprobar As Boolean
    If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).Tipo = TipoNumerico And Not IsNull(m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).PrecioAplicarA) Then
        If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).PrecioAplicarA = TotalGrupo Then   'es de �mbito grupo.Miramos en la tabla USAR_GR_ATRIB para ver si se est� aplicando
            If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).AtribTotalGrupo Is Nothing Then
                m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).CargarUsarPrecTotalGrupo
            End If
            If Not m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).AtribTotalGrupo Is Nothing Then
                For Each oUsar In m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).AtribTotalGrupo
                    If oUsar.UsarPrec = 1 Then
                        bComprobar = True
                        Exit For
                    End If
                Next
            End If
        Else
            If m_oAtributos.Item(CStr(sdbgFormulas.Columns("IDATRIBPROCE").Value)).UsarPrec = 1 Then bComprobar = True
        End If
    End If
       
    If (g_oProceso.tieneofertas > 0) And bComprobar Then
        If m_oAtribsModOrden.Item(CStr(sIdAtribProce1)) Is Nothing Then
            m_oAtribsModOrden.Add sIdAtribProce1, sText, "D", sTipo, idAtribProce:=sIdAtribProce1, PrecioFormula:=sOp1, PrecioAplicarA:=sTipo, bUsarIdProceAtrib:=True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "cmdSortDesc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Me.Height = 5310
    Me.Width = 9060
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    If gParametrosInstalacion.gIdioma = "SPA" Then
        Picture1.Visible = True
        Shape1.Visible = True
        Picture2.Visible = False
        Shape2.Visible = False
    Else
        Picture1.Visible = False
        Shape1.Visible = False
        Picture2.Visible = True
        Shape2.Visible = True
    End If
    
    Set m_oAtribsModOrden = oFSGSRaiz.Generar_CAtributos
    
    CargarRecursos
    
    If g_bNoModif = True Then
        cmdAceptar.Visible = False
        cmdCancelar.Visible = False
        cmdSortAsc.Enabled = False
        cmdSortDesc.Enabled = False
    End If
    
    Select Case g_sOrigen
        Case "frmPROCE"
            CargarGrid
        
        Case "PLANTILLAS"
            CargarGridPlantillas
            
        Case "frmADJ"
            CargarGridComparativa
            
        Case "frmADJItem"
            CargarGridComparativa
            
        Case "frmRESREU"
            CargarGridComparativa
    End Select
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Resize()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

    If Me.Width > 500 And Me.Height > 2900 Then
        Picture1.Height = Me.Height - 1100
        Shape1.Height = Picture1.Height
        sdbgFormulas.Width = Me.Width - 1400
        sdbgFormulas.Height = Picture1.Height
        sdbgFormulas.Columns("COD").Width = sdbgFormulas.Width * 0.15
        sdbgFormulas.Columns("OPERACION").Width = sdbgFormulas.Width * 0.15
        sdbgFormulas.Columns("APLICARA").Width = sdbgFormulas.Width * 0.7
        cmdAceptar.Top = sdbgFormulas.Height + 200
        cmdCancelar.Top = sdbgFormulas.Height + 200
        cmdAceptar.Left = Me.Width / 2 - cmdAceptar.Width - 150
        cmdCancelar.Left = Me.Width / 2 + 150
        cmdSortAsc.Left = sdbgFormulas.Width + 600
        cmdSortDesc.Left = sdbgFormulas.Width + 600
        cmdSortAsc.Height = sdbgFormulas.Height / 2 - 200
        cmdSortDesc.Height = sdbgFormulas.Height / 2 - 200
        cmdSortDesc.Top = sdbgFormulas.Height / 2 + 260
        
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

     Set m_oAtributos = Nothing
     g_bNoModif = False
     Set m_oAtribsModOrden = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgFormulas_RowLoaded(ByVal Bookmark As Variant)
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgFormulas.Columns("TIPO").Value = 0 Then
        sdbgFormulas.Columns(0).CellStyleSet "Proceso"
        sdbgFormulas.Columns(1).CellStyleSet "Proceso"
        sdbgFormulas.Columns(2).CellStyleSet "Proceso"
    End If
    
    If sdbgFormulas.Columns("TIPO").Value = 1 Then
        sdbgFormulas.Columns(0).CellStyleSet "Grupo"
        sdbgFormulas.Columns(1).CellStyleSet "Grupo"
        sdbgFormulas.Columns(2).CellStyleSet "Grupo"
    End If
    
    If sdbgFormulas.Columns("TIPO").Value = 2 Then
        sdbgFormulas.Columns(0).CellStyleSet "TotalItem"
        sdbgFormulas.Columns(1).CellStyleSet "TotalItem"
        sdbgFormulas.Columns(2).CellStyleSet "TotalItem"
    End If
    
    If sdbgFormulas.Columns("TIPO").Value = 3 Then
        sdbgFormulas.Columns(0).CellStyleSet "Item"
        sdbgFormulas.Columns(1).CellStyleSet "Item"
        sdbgFormulas.Columns(2).CellStyleSet "Item"
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "sdbgFormulas_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ORDENATRIBUTOS, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then

        frmOrdenAtributos.caption = Ador(0).Value
        Ador.MoveNext
        sdbgFormulas.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgFormulas.Columns("OPERACION").caption = Ador(0).Value
        Ador.MoveNext
        sdbgFormulas.Columns("APLICARA").caption = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(0) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(1) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(2) = Ador(0).Value
        Ador.MoveNext
        m_sMensaje(3) = Ador(0).Value
        Ador.MoveNext
'        Picture1.Name = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        m_sProceso = Ador(0).Value
        Ador.MoveNext
        m_sGrupo = Ador(0).Value
        Ador.MoveNext
        
        m_sMensajeModifAtrib = Ador(0).Value
        Ador.MoveNext
        m_sMensajeModifAtrib = m_sMensajeModifAtrib & vbCrLf & Ador(0).Value
        Ador.MoveNext
        m_sMensajeModifAtrib = m_sMensajeModifAtrib & vbCrLf & Ador(0).Value

        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub
Private Sub CargarGridAdjItem()
Dim sAplicar As String
Dim iAtrib As String
Dim iIdAtrib As String
Dim iAplic As String
Dim sOpPrec As String
Dim sCod As String
Dim bPrimero As Boolean
Dim iAmbito As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProceso Is Nothing Then Exit Sub
    
    Set m_oAtributos = Nothing
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos

    sdbgFormulas.RemoveAll

    Set g_adoRes = g_oProceso.CargarOrdenAtributoConPrecio(True, g_oOrigen.g_oItemSeleccionado.GrupoCod)

    While Not g_adoRes.EOF
        Select Case g_adoRes("APLIC_PREC").Value
          Case 2:
                If IsNull(g_adoRes("GRUPO").Value) Then
                    sAplicar = m_sMensaje(2) & " " & m_sProceso & ": "
                Else
                    sAplicar = m_sMensaje(2) & " " & m_sGrupo & ": "
                End If
          Case 3:
                sAplicar = m_sMensaje(3)
        End Select
        
        If IsNull(g_adoRes("GRUPO").Value) Then
            sdbgFormulas.AddItem g_adoRes("COD").Value & Chr(9) & g_adoRes("OP_PREC").Value & Chr(9) & m_sMensaje(g_adoRes("APLIC_PREC").Value) & Chr(9) & g_adoRes("APLIC_PREC").Value & Chr(9) & g_adoRes("ORDEN").Value & Chr(9) & g_adoRes("ID").Value & Chr(9) & g_adoRes("ATRIB").Value
            m_oAtributos.Add g_adoRes("ATRIB").Value, g_adoRes("COD").Value, "1", "2", False, g_oProceso.GMN1Cod, , , , False, , g_adoRes("ID").Value, , , , g_oProceso.Anyo, g_oProceso.Cod, g_adoRes("GRUPO").Value, g_adoRes("ID").Value, , , , , , g_adoRes("AMBITO").Value, , g_adoRes("OP_PREC").Value, g_adoRes("APLIC_PREC").Value
            g_adoRes.MoveNext
            
        Else
            If g_adoRes("APLIC_PREC").Value <> 2 Then
                sAplicar = m_sMensaje(g_adoRes("APLIC_PREC").Value) & ": "
            End If
            iAtrib = g_adoRes("ATRIB").Value
            iIdAtrib = g_adoRes("ID").Value
            iAplic = g_adoRes("APLIC_PREC").Value
            sOpPrec = g_adoRes("OP_PREC").Value
            sCod = g_adoRes("COD").Value
            iAmbito = g_adoRes("AMBITO").Value
            
            bPrimero = True
            While Not g_adoRes.EOF
            
                If iAtrib = g_adoRes("ATRIB").Value And iAplic = CStr(g_adoRes("APLIC_PREC").Value) And sOpPrec = g_adoRes("OP_PREC").Value Then
                    If bPrimero Then
                        sAplicar = sAplicar & " " & g_adoRes("GRUPO").Value
                        bPrimero = False
                    Else
                        sAplicar = sAplicar & ", " & g_adoRes("GRUPO").Value
                    End If
                    g_adoRes.MoveNext
                Else
                    GoTo Seguir
                End If
            Wend
Seguir:
            sdbgFormulas.AddItem sCod & Chr(9) & sOpPrec & Chr(9) & sAplicar & Chr(9) & iAplic & Chr(9) & Chr(9) & iIdAtrib & Chr(9) & iAtrib
            m_oAtributos.Add iAtrib, sCod, "1", "2", False, g_oProceso.GMN1Cod, , , , False, , iIdAtrib, , , , g_oProceso.Anyo, g_oProceso.Cod, "A", iIdAtrib, , , , , , iAmbito, , sOpPrec, iAplic
        End If

    Wend

    'Cierra el recordset
    g_adoRes.Close
    Set g_adoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "CargarGridAdjItem", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CargarGridComparativa()
Dim sAplicar As String
Dim oatrib As CAtributo
Dim lMayor As Long
Dim iOrden As Integer
Dim bSalir As Boolean
Dim iOrdenSiguiente As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProceso Is Nothing Then Exit Sub
    
    Set m_oAtributos = Nothing
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos

    sdbgFormulas.RemoveAll
    
    'No cargo los atributos de BD,sino de las colecciones de la comparativa o el panel,para que muestre los
    '�ltimos cambios,aunque no se haya salvado en BD
    Select Case g_sOrigen
        Case "frmADJ"
            If frmADJ.m_oAtribsFormulas Is Nothing Then Exit Sub
            If frmADJ.m_oAtribsFormulas.Count = 0 Then Exit Sub
            Set m_oAtributos = frmADJ.m_oAtribsFormulas

        Case "frmRESREU"
            If frmRESREU.m_oAtribsFormulas Is Nothing Then Exit Sub
            If frmRESREU.m_oAtribsFormulas.Count = 0 Then Exit Sub
            Set m_oAtributos = frmRESREU.m_oAtribsFormulas
        
        Case "frmADJItem"
            If g_oOrigen.g_oOrigen.Name = "frmADJ" Then
                If frmADJ.m_oAtribsFormulas Is Nothing Then Exit Sub
                If frmADJ.m_oAtribsFormulas.Count = 0 Then Exit Sub
                Set m_oAtributos = frmADJ.m_oAtribsFormulas
            Else
                If frmRESREU.m_oAtribsFormulas Is Nothing Then Exit Sub
                If frmRESREU.m_oAtribsFormulas.Count = 0 Then Exit Sub
                Set m_oAtributos = frmRESREU.m_oAtribsFormulas
            End If
            
    End Select

    'ahora cargo la grid
    bSalir = False
    iOrdenSiguiente = 0

    While bSalir = False
        iOrden = 0
        lMayor = 0

        For Each oatrib In m_oAtributos
            If lMayor = 0 And iOrden = 0 Then
                If iOrdenSiguiente = 0 Then
                    lMayor = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                ElseIf oatrib.Orden <= iOrdenSiguiente Then
                    lMayor = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                End If
            ElseIf (oatrib.Orden > iOrden) Then
                If (oatrib.Orden <= iOrdenSiguiente And iOrdenSiguiente > 0) Or iOrdenSiguiente = 0 Then
                    lMayor = oatrib.idAtribProce
                    iOrden = oatrib.Orden
                End If
            End If
        Next
        If lMayor = 0 Then Exit Sub
        Set oatrib = m_oAtributos.Item(CStr(lMayor))
        iOrdenSiguiente = oatrib.Orden - 1

        Select Case oatrib.PrecioAplicarA
          Case 0:
                sAplicar = m_sMensaje(0)
          Case 1:
                sAplicar = m_sMensaje(1)
          Case 2:
                If IsNull(oatrib.codgrupo) Then
                    sAplicar = m_sMensaje(2) & " " & m_sProceso
                Else
                    sAplicar = m_sMensaje(2) & " " & m_sGrupo & ": "
                End If
          Case 3:
                sAplicar = m_sMensaje(3)
        End Select

        If IsNull(oatrib.codgrupo) Then
            If oatrib.PrecioAplicarA = 2 Then
                sdbgFormulas.AddItem oatrib.Cod & Chr(9) & oatrib.PrecioFormula & Chr(9) & sAplicar & Chr(9) & oatrib.PrecioAplicarA & Chr(9) & oatrib.Orden & Chr(9) & oatrib.idAtribProce & Chr(9) & oatrib.Id
            Else
                sdbgFormulas.AddItem oatrib.Cod & Chr(9) & oatrib.PrecioFormula & Chr(9) & m_sMensaje(oatrib.PrecioAplicarA) & Chr(9) & oatrib.PrecioAplicarA & Chr(9) & oatrib.Orden & Chr(9) & oatrib.idAtribProce & Chr(9) & oatrib.Id
            End If
        Else
            If oatrib.PrecioAplicarA <> 2 Then
                sAplicar = m_sMensaje(oatrib.PrecioAplicarA) & ": "
            End If
            sAplicar = sAplicar & oatrib.codgrupo
            sdbgFormulas.AddItem oatrib.Cod & Chr(9) & oatrib.PrecioFormula & Chr(9) & sAplicar & Chr(9) & oatrib.PrecioAplicarA & Chr(9) & Chr(9) & oatrib.idAtribProce & Chr(9) & oatrib.Id
        End If

        Set oatrib = Nothing
        If iOrden = 1 Then
            bSalir = True
        End If

    Wend
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "CargarGridComparativa", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub CargarGridPlantillas()
Dim sAplicar As String
Dim iAtrib As String
Dim iIdAtrib As String
Dim iAplic As String
Dim sOpPrec As String
Dim sCod As String
Dim sGrupo As Variant
Dim bPrimero As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oPlantilla Is Nothing Then Exit Sub
    
    Set m_oAtributos = Nothing
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos

    sdbgFormulas.RemoveAll

    Set g_adoRes = g_oPlantilla.CargarOrdenAtrib

    While Not g_adoRes.EOF
        Select Case g_adoRes("APLIC_PREC").Value
          Case 0:
                sAplicar = m_sMensaje(0)
          Case 1:
                sAplicar = m_sMensaje(1)
          Case 2:
                sAplicar = m_sMensaje(2)
          Case 3:
                sAplicar = m_sMensaje(3)
        End Select
        If IsNull(g_adoRes("GRUPO").Value) Then
            sdbgFormulas.AddItem g_adoRes("COD").Value & Chr(9) & g_adoRes("OP_PREC").Value & Chr(9) & m_sMensaje(g_adoRes("APLIC_PREC").Value) & Chr(9) & g_adoRes("APLIC_PREC").Value & Chr(9) & g_adoRes("ORDEN").Value & Chr(9) & g_adoRes("ID").Value & Chr(9) & g_adoRes("ATRIB").Value
            m_oAtributos.Add g_adoRes("ATRIB").Value, g_adoRes("COD").Value, "1", "2", False, , , , , , , g_adoRes("ID").Value, , , g_oPlantilla.Id, , , g_adoRes("GRUPO").Value, , , , , , , , , g_adoRes("OP_PREC").Value, g_adoRes("APLIC_PREC").Value
            g_adoRes.MoveNext
        Else
            sAplicar = m_sMensaje(g_adoRes("APLIC_PREC").Value) & ": "
            iAtrib = g_adoRes("ATRIB").Value
            iIdAtrib = g_adoRes("ID").Value
            iAplic = g_adoRes("APLIC_PREC").Value
            sOpPrec = g_adoRes("OP_PREC").Value
            sCod = g_adoRes("COD").Value
            bPrimero = True
            While Not g_adoRes.EOF
                sGrupo = g_adoRes("GRUPO").Value
                If iAtrib = g_adoRes("ATRIB").Value And iAplic = CStr(g_adoRes("APLIC_PREC").Value) And sOpPrec = g_adoRes("OP_PREC").Value Then
                    If bPrimero Then
                        sAplicar = sAplicar & " " & g_adoRes("GRUPO").Value
                        bPrimero = False
                    Else
                        sAplicar = sAplicar & ", " & g_adoRes("GRUPO").Value
                    End If
                    g_adoRes.MoveNext
                Else
                    GoTo Seguir
                End If
            Wend
Seguir:
            sdbgFormulas.AddItem sCod & Chr(9) & sOpPrec & Chr(9) & sAplicar & Chr(9) & iAplic & Chr(9) & Chr(9) & iIdAtrib & Chr(9) & iAtrib
            
            m_oAtributos.Add iAtrib, sCod, "1", "2", False, , , , , False, , iIdAtrib, , , g_oPlantilla.Id, , , sGrupo, , , , , , , , , sOpPrec, iAplic
        End If

    Wend

    g_adoRes.Close
    Set g_adoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "CargarGridPlantillas", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Sub CargarGrid()
Dim sAplicar As String
Dim iAtrib As String
Dim iIdAtrib As String
Dim iAplic As String
Dim sOpPrec As String
Dim sCod As String
Dim bPrimero As Boolean
Dim iAmbito As Integer
Dim sUsarPrec As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oProceso Is Nothing Then Exit Sub
    
    Set m_oAtributos = Nothing
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos

    sdbgFormulas.RemoveAll

    Set g_adoRes = g_oProceso.CargarOrdenAtributoConPrecio

    While Not g_adoRes.EOF
        Select Case g_adoRes("APLIC_PREC").Value
          Case 0:
                sAplicar = m_sMensaje(0)
          Case 1:
                sAplicar = m_sMensaje(1)
          Case 2:
                If IsNull(g_adoRes("GRUPO").Value) Then
                    sAplicar = m_sMensaje(2) & " " & m_sProceso & ": "
                Else
                    sAplicar = m_sMensaje(2) & " " & m_sGrupo & ": "
                End If
          Case 3:
                sAplicar = m_sMensaje(3)
        End Select
        
        If IsNull(g_adoRes("GRUPO").Value) Then
            sdbgFormulas.AddItem g_adoRes("COD").Value & Chr(9) & g_adoRes("OP_PREC").Value & Chr(9) & m_sMensaje(g_adoRes("APLIC_PREC").Value) & Chr(9) & g_adoRes("APLIC_PREC").Value & Chr(9) & g_adoRes("ORDEN").Value & Chr(9) & g_adoRes("ID").Value & Chr(9) & g_adoRes("USAR_PREC").Value
            m_oAtributos.Add g_adoRes("ATRIB").Value, g_adoRes("COD").Value, "1", "2", False, g_oProceso.GMN1Cod, , , , False, , g_adoRes("ID").Value, , , , g_oProceso.Anyo, g_oProceso.Cod, g_adoRes("GRUPO").Value, g_adoRes("ID").Value, , , , , , g_adoRes("AMBITO").Value, , g_adoRes("OP_PREC").Value, g_adoRes("APLIC_PREC").Value, UsarPrec:=g_adoRes("USAR_PREC").Value
            g_adoRes.MoveNext
            
        Else
            If g_adoRes("APLIC_PREC").Value <> 2 Then
                sAplicar = m_sMensaje(g_adoRes("APLIC_PREC").Value) & ": "
            End If
            iAtrib = g_adoRes("ATRIB").Value
            iIdAtrib = g_adoRes("ID").Value
            iAplic = g_adoRes("APLIC_PREC").Value
            sOpPrec = g_adoRes("OP_PREC").Value
            sCod = g_adoRes("COD").Value
            iAmbito = g_adoRes("AMBITO").Value
            sUsarPrec = NullToStr(g_adoRes("USAR_PREC").Value)
            
            
            bPrimero = True
            While Not g_adoRes.EOF
            
                If iAtrib = g_adoRes("ATRIB").Value And iAplic = CStr(g_adoRes("APLIC_PREC").Value) And sOpPrec = g_adoRes("OP_PREC").Value Then
                    If bPrimero Then
                        sAplicar = sAplicar & " " & g_adoRes("GRUPO").Value
                        bPrimero = False
                    Else
                        sAplicar = sAplicar & ", " & g_adoRes("GRUPO").Value
                    End If
                    g_adoRes.MoveNext
                Else
                    GoTo Seguir
                End If
            Wend
Seguir:
            sdbgFormulas.AddItem sCod & Chr(9) & sOpPrec & Chr(9) & sAplicar & Chr(9) & iAplic & Chr(9) & Chr(9) & iIdAtrib & Chr(9) & sUsarPrec
            m_oAtributos.Add iAtrib, sCod, "1", "2", False, g_oProceso.GMN1Cod, , , , False, , iIdAtrib, , , , g_oProceso.Anyo, g_oProceso.Cod, "A", iIdAtrib, , , , , , iAmbito, , sOpPrec, iAplic, UsarPrec:=sUsarPrec
        End If

    Wend

    'Cierra el recordset
    g_adoRes.Close
    Set g_adoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmOrdenAtributos", "CargarGrid", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
