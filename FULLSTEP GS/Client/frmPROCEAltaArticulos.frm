VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPROCEAltaArticulos 
   Caption         =   "Alta de art�culos en el sistema"
   ClientHeight    =   7920
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11340
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEAltaArticulos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7920
   ScaleWidth      =   11340
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAnyadir 
      Caption         =   "A�adir"
      Height          =   795
      Left            =   5055
      Picture         =   "frmPROCEAltaArticulos.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2510
      Width           =   990
   End
   Begin VB.CommandButton cmdQuitar 
      Caption         =   "Quitar"
      Height          =   795
      Left            =   5055
      Picture         =   "frmPROCEAltaArticulos.frx":0EAC
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3700
      Width           =   990
   End
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar ->"
      Height          =   330
      Left            =   3870
      TabIndex        =   2
      Top             =   7340
      Width           =   1515
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   330
      Left            =   5710
      TabIndex        =   1
      Top             =   7340
      Width           =   1515
   End
   Begin MSComctlLib.ListView lstvwArticulos 
      Height          =   6390
      Left            =   75
      TabIndex        =   0
      Top             =   450
      Width           =   4785
      _ExtentX        =   8440
      _ExtentY        =   11271
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Den"
         Object.Width           =   8202
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Cod"
         Object.Width           =   0
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwEstrMat 
      Height          =   6390
      Left            =   6255
      TabIndex        =   5
      Top             =   450
      Width           =   4995
      _ExtentX        =   8811
      _ExtentY        =   11271
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5895
      Top             =   300
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":10A6
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":14FA
            Key             =   "ACT"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":160C
            Key             =   "ACTASIG"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":171E
            Key             =   "Raiz2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":1B70
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":1C20
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":1CE2
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":1D92
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":1E54
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":1F04
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":1FC6
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROCEAltaArticulos.frx":208E
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   $"frmPROCEAltaArticulos.frx":2145
      Height          =   375
      Left            =   60
      TabIndex        =   7
      Top             =   0
      Width           =   10815
   End
   Begin VB.Label Label2 
      Caption         =   "(Ctrl � Shift para selecci�n m�ltiple)"
      Height          =   300
      Left            =   45
      TabIndex        =   6
      Top             =   7035
      Width           =   2715
   End
End
Attribute VB_Name = "frmPROCEAltaArticulos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_oIBaseDatos As IBaseDatos
Private sEstructura As String
Public oArt As CArticulo

Public oArticulos As CArticulos

Public oArticulosNocodif As CArticulos

Public oGMN1 As CGrupoMatNivel1
Public oGMN2 As CGrupoMatNivel2
Public oGMN3 As CGrupoMatNivel3
Public oGMN4 As CGrupoMatNivel4
Public sDenGrupo As String

Public bRComprador As Boolean
Public oArtSel As CArticulos 'Articulos seleccionados
Public oArtAnyadidos As CArticulos
Public oArtAnyadidosNoCodif As CArticulos

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
Private Sub cmdAnyadir_Click()
    'Los articulos a�adidos se a�aden a la rama de materiales seleccionada.
    Dim nodx As MSComctlLib.node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim oGrupsMN1 As CGruposMatNivel1
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Set oArtSel = Nothing
    Set oArtSel = oFSGSRaiz.Generar_CArticulos
    For i = 1 To lstvwArticulos.ListItems.Count
        'si se ha pulsado la tecla CTRL o Shift
        If lstvwArticulos.ListItems.Item(i).Selected = True Then
            oArtSel.Add "", "", "", "", lstvwArticulos.ListItems.Item(i).Tag, lstvwArticulos.ListItems.Item(i).Text
       End If
    Next

    If oArtSel Is Nothing Then Exit Sub
           
    Set nodx = tvwEstrMat.selectedItem
    
    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    Set oArt = oFSGSRaiz.Generar_CArticulo
    
    If Left(nodx.Tag, 4) <> "GMN4" Then
        oMensajes.SeleccioneMatNivel4
    Else
        
        For Each oArt In oArtSel
            lstvwArticulos.selectedItem.Tag = oArt.Cod
            scod1 = DevolverCod(nodx.Parent.Parent.Parent)
            scod2 = DevolverCod(nodx.Parent.Parent)
            scod3 = DevolverCod(nodx.Parent)
            scod4 = DevolverCod(nodx)
            
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(scod1))
            nodx.Tag = "GMN1" & scod1
    
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(scod2))
            nodx.Tag = "GMN2" & scod2
    
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(scod3))
            nodx.Tag = "GMN3" & scod3
    
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(scod4))
            nodx.Tag = "GMN4" & scod4
            
            If Left(oArt.Cod, 7) = "NOCODIF" Then
                Set nodx = tvwEstrMat.Nodes.Add("GMN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "", oArt.Den)
                oArtAnyadidosNoCodif.Add Trim(scod1), Trim(scod2), Trim(scod3), Trim(scod4), Trim(oArt.Cod), oArt.Den
            Else
                Set nodx = tvwEstrMat.Nodes.Add("GMN4" & scod1 & scod2 & scod3 & scod4, tvwChild, "", oArt.Den)
                oArtAnyadidos.Add Trim(scod1), Trim(scod2), Trim(scod3), Trim(scod4), Trim(oArt.Cod), oArt.Den
            End If
            nodx.Tag = oArt.Cod
                        
            nodx.Parent.Expanded = True
                
            lstvwArticulos.ListItems.Remove ("ART" & lstvwArticulos.selectedItem.Tag)
            
            Set nodx = nodx.Parent
            nodx.Parent.Expanded = True
            nodx.Child.Selected = True
        Next
    End If
    Set nodx = Nothing
    Set oArtSel = Nothing
    Set oArt = Nothing
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "cmdAnyadir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmPROCE.g_bCancelar = True
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdContinuar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "cmdContinuar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdQuitar_Click()
    Dim sCod As String
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim nodx As MSComctlLib.node
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set nodx = tvwEstrMat.selectedItem
    
    If Left(nodx.Tag, 3) = "GMN" Then Exit Sub
    
    sCod = nodx.Tag & Mid$("                    ", 1, basParametros.gLongitudesDeCodigos.giLongCodART - Len(nodx.Tag))
    'Se quita el articulo de la estructura de materiales
        
    scod1 = DevolverCod(nodx.Parent.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(DevolverCod(nodx.Parent.Parent.Parent.Parent)))
    scod2 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
    scod3 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(DevolverCod(nodx.Parent.Parent)))
    scod4 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(DevolverCod(nodx.Parent)))
    
    tvwEstrMat.Nodes.Remove (nodx.Index)
    
    If Left(Trim(sCod), 7) = "NOCODIF" Then
        oArtAnyadidosNoCodif.Remove scod1 & scod2 & scod3 & scod4 & Trim(sCod)
        lstvwArticulos.ListItems.Add , "ART" & CStr(Trim(sCod)), nodx.Text
        lstvwArticulos.ListItems.Item("ART" & CStr(Trim(sCod))).Tag = Trim(sCod)
    Else
        oArtAnyadidos.Remove scod1 & scod2 & scod3 & scod4 & Trim(sCod)
        lstvwArticulos.ListItems.Add , "ART" & CStr(Trim(sCod)), nodx.Text
        lstvwArticulos.ListItems.Item("ART" & CStr(Trim(sCod))).Tag = Trim(sCod)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "cmdQuitar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Set oArtSel = oFSGSRaiz.Generar_CArticulos
    Set oArtAnyadidos = oFSGSRaiz.Generar_CArticulos
    Set oArtAnyadidosNoCodif = oFSGSRaiz.Generar_CArticulos
    
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    Screen.MousePointer = Normal

    CargarRecursos
    
    lstvwArticulos.ListItems.clear
    'Cargar la lista con los articulos codificados
    
    For Each oArt In oArticulos
        lstvwArticulos.ListItems.Add , "ART" & CStr(oArt.Cod), oArt.Cod & " - " & oArt.Den
        lstvwArticulos.ListItems.Item("ART" & CStr(oArt.Cod)).Tag = oArt.Cod
        lstvwArticulos.ListItems.Item("ART" & CStr(oArt.Cod)).ListSubItems.Add , "ART" & CStr(oArt.Cod), oArt.Den
        
    Next
    
    'Items no codificados
    For Each oArt In oArticulosNocodif
        lstvwArticulos.ListItems.Add , "ART" & "NOCODIF" & CStr(oArt.Cod), oArt.Den
        lstvwArticulos.ListItems.Item("ART" & "NOCODIF" & CStr(oArt.Cod)).Tag = "NOCODIF" & oArt.Cod
        lstvwArticulos.ListItems.Item("ART" & "NOCODIF" & CStr(oArt.Cod)).ListSubItems.Add , "ART" & "NOCODIF" & CStr(oArt.Cod), oArt.Den
    Next
    
    lstvwArticulos_ItemClick lstvwArticulos.ListItems(1)
    
    GenerarEstructuraMat
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
        
End Sub
Private Sub CargarRecursos()
    
    Dim Ador As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCEALTAARTICULOS, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        Label1.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyadir.caption = Ador(0).Value
        Ador.MoveNext
        cmdQuitar.caption = Ador(0).Value
        Ador.MoveNext
        Label2.caption = Ador(0).Value
        Ador.MoveNext
        cmdContinuar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sEstructura = Ador(0).Value 'Materiales
        Ador.MoveNext
        Me.caption = Me.caption & " " & Ador(0).Value & " " & sDenGrupo
    End If
    Ador.Close
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Private Sub GenerarEstructuraMat()

Dim oGrupsMN1 As CGruposMatNivel1

Dim oGMN1 As CGrupoMatNivel1
Dim oGMN2 As CGrupoMatNivel2
Dim oGMN3 As CGrupoMatNivel3
Dim oGMN4 As CGrupoMatNivel4

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Dim nodx As MSComctlLib.node

Dim oIMaterialComprador As IMaterialAsignado
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
tvwEstrMat.Nodes.clear

Set nodx = tvwEstrMat.Nodes.Add(, , "Raiz", sEstructura, "Raiz")
nodx.Tag = "Raiz"
nodx.Expanded = True
nodx.Selected = True

Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    
If bRComprador Then
    Set oIMaterialComprador = oUsuarioSummit.comprador
    Set oGrupsMN1 = oIMaterialComprador.DevolverEstructuraMat
Else
    Set oGrupsMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
    oGrupsMN1.GenerarEstructuraMateriales False
End If

For Each oGMN1 In oGrupsMN1
        scod1 = oGMN1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oGMN1.Cod))
        Set nodx = tvwEstrMat.Nodes.Add("Raiz", tvwChild, "GMN1" & scod1, oGMN1.Cod & " - " & oGMN1.Den, "GMN1")
        nodx.Tag = "GMN1" & oGMN1.Cod
        nodx.Expanded = False
        For Each oGMN2 In oGMN1.GruposMatNivel2
            scod2 = oGMN2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oGMN2.Cod))
            Set nodx = tvwEstrMat.Nodes.Add("GMN1" & scod1, tvwChild, "GMN2" & scod1 & scod2, oGMN2.Cod & " - " & oGMN2.Den, "GMN2")
            nodx.Tag = "GMN2" & oGMN2.Cod
            
            For Each oGMN3 In oGMN2.GruposMatNivel3
                scod3 = oGMN3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oGMN3.Cod))
                Set nodx = tvwEstrMat.Nodes.Add("GMN2" & scod1 & scod2, tvwChild, "GMN3" & scod1 & scod2 & scod3, oGMN3.Cod & " - " & oGMN3.Den, "GMN3")
                nodx.Tag = "GMN3" & oGMN3.Cod
                            
                For Each oGMN4 In oGMN3.GruposMatNivel4
                    scod4 = oGMN4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oGMN4.Cod))
                    Set nodx = tvwEstrMat.Nodes.Add("GMN3" & scod1 & scod2 & scod3, tvwChild, "GMN4" & scod1 & scod2 & scod3 & scod4, oGMN4.Cod & " - " & oGMN4.Den, "GMN4")
                    nodx.Tag = "GMN4" & oGMN4.Cod
                Next
            Next
        Next
    Next

Set oGrupsMN1 = Nothing
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "GenerarEstructuraMat", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If node Is Nothing Then Exit Function
    DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "DevolverCod", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub Form_Resize()
  
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "Form_Resize", err, Erl)
      Exit Sub
   End If
End Sub

Private Sub Arrange()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 1100 Then   'de tama�o de la ventana
            Me.Width = 11580       'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 8000 Then  'cuando no se maximiza ni
            Me.Height = 8430       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If
    
    Label1.Top = 90
    lstvwArticulos.Left = 100
    lstvwArticulos.Top = Label1.Top + 450
    lstvwArticulos.Height = Me.Height - lstvwArticulos.Top - 1500
    lstvwArticulos.Width = Me.Width / 2 - 1000
    cmdAnyadir.Left = lstvwArticulos.Left + lstvwArticulos.Width + 200
    cmdQuitar.Left = lstvwArticulos.Left + lstvwArticulos.Width + 200
    cmdAnyadir.Top = lstvwArticulos.Height / 2 - cmdAnyadir.Height + 200
    cmdQuitar.Top = lstvwArticulos.Height / 2 + cmdQuitar.Height - 200
    tvwEstrMat.Top = Label1.Top + 450
    tvwEstrMat.Width = lstvwArticulos.Width + 200
    tvwEstrMat.Height = Me.Height - tvwEstrMat.Top - 1500
    tvwEstrMat.Left = cmdAnyadir.Left + cmdAnyadir.Width + 200
    Label2.Width = lstvwArticulos.Width
    Label2.Top = lstvwArticulos.Top + lstvwArticulos.Height + 150
    cmdContinuar.Top = lstvwArticulos.Top + lstvwArticulos.Height + 500
    cmdCancelar.Top = lstvwArticulos.Top + lstvwArticulos.Height + 500
    cmdContinuar.Left = Me.Width / 2 - cmdContinuar.Width - 250
    cmdCancelar.Left = Me.Width / 2 + 100
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "Arrange", err, Erl, , m_bActivado)
      Exit Sub
   End If
   
End Sub


Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set m_oIBaseDatos = Nothing
sEstructura = ""
Set oArt = Nothing
oFSGSRaiz.pg_sFrmCargado Me.Name, False

Set oGMN1 = Nothing
Set oGMN2 = Nothing
Set oGMN3 = Nothing
Set oGMN4 = Nothing
sDenGrupo = ""

bRComprador = False
Set oArtSel = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub lstvwArticulos_ItemClick(ByVal Item As MSComctlLib.listItem)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "lstvwArticulos_ItemClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub lstvwArticulos_KeyDown(KeyCode As Integer, Shift As Integer)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEAltaArticulos", "lstvwArticulos_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



