VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosDetalleAccion 
   BackColor       =   &H00808000&
   Caption         =   "DDetalle accion"
   ClientHeight    =   10950
   ClientLeft      =   60
   ClientTop       =   3360
   ClientWidth     =   12045
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosDetalleAccion.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   10950
   ScaleWidth      =   12045
   Begin VB.CheckBox chkNoAprobarSiDiscrepancias 
      BackColor       =   &H00808000&
      Caption         =   "DNo permitir aprobar si existen discrepancias"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   45
      Top             =   2490
      Width           =   5055
   End
   Begin VB.CheckBox chkFicheroEFactura 
      BackColor       =   &H00808000&
      Caption         =   "DLa acci�n generar� el fichero e-factura"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   44
      Top             =   2250
      Width           =   5055
   End
   Begin VB.CheckBox chkLlamadaExterna 
      BackColor       =   &H00808000&
      Caption         =   "DLa acci�n realizar� una llamada externa"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   5790
      TabIndex        =   41
      Top             =   2250
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.CheckBox chkRechazar 
      BackColor       =   &H00808000&
      Caption         =   "DLa acci�n se asociar� al bot�n Rechazar en el Visor de Tareas"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   5280
      TabIndex        =   40
      Top             =   1260
      Visible         =   0   'False
      Width           =   6495
   End
   Begin VB.CheckBox chkAprobar 
      BackColor       =   &H00808000&
      Caption         =   "DLa acci�n se asociar� al bot�n Aprobar en el Visor de Tareas"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   5280
      TabIndex        =   39
      Top             =   1500
      Width           =   8415
   End
   Begin VB.PictureBox picIncrementarId 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   5280
      ScaleHeight     =   195
      ScaleWidth      =   3255
      TabIndex        =   34
      Top             =   1980
      Width           =   3255
      Begin VB.CheckBox chkIncrementarId 
         BackColor       =   &H00808000&
         Caption         =   "DIncrementar identificador de solicitud"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   35
         Top             =   0
         Visible         =   0   'False
         Width           =   3210
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipoPrecondicion 
      Height          =   555
      Left            =   1800
      TabIndex        =   33
      Top             =   5640
      Width           =   2505
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   4419
      Columns(1).Caption=   "TIPO"
      Columns(1).Name =   "TIPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4410
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPrecondiciones 
      Height          =   1455
      Left            =   720
      TabIndex        =   32
      Top             =   5400
      Width           =   11115
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   8
      stylesets.count =   6
      stylesets(0).Name=   "No"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosDetalleAccion.frx":0CB2
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "S�"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosDetalleAccion.frx":0CCE
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Gris"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFlujosDetalleAccion.frx":0CEA
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFlujosDetalleAccion.frx":0D06
      stylesets(4).Name=   "ActiveRow"
      stylesets(4).ForeColor=   16777215
      stylesets(4).BackColor=   8388608
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFlujosDetalleAccion.frx":0D22
      stylesets(5).Name=   "Amarillo"
      stylesets(5).BackColor=   12648447
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFlujosDetalleAccion.frx":0D3E
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      Columns.Count   =   8
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ORDEN"
      Columns(1).Name =   "ORDEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   2
      Columns(1).FieldLen=   256
      Columns(2).Width=   1402
      Columns(2).Caption=   "ID"
      Columns(2).Name =   "COD"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   5
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "TIPO"
      Columns(3).Name =   "TIPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   2
      Columns(3).FieldLen=   256
      Columns(4).Width=   3519
      Columns(4).Caption=   "TIPO_DEN"
      Columns(4).Name =   "TIPO_DEN"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "FORMULA"
      Columns(5).Name =   "FORMULA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2646
      Columns(6).Caption=   "CONDICIONES "
      Columns(6).Name =   "CONDICIONES"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   4
      Columns(6).ButtonsAlways=   -1  'True
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "FECACT"
      Columns(7).Name =   "FECACT"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      _ExtentX        =   19606
      _ExtentY        =   2566
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picSoloRechazadas 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   225
      Left            =   120
      ScaleHeight     =   225
      ScaleWidth      =   6015
      TabIndex        =   29
      Top             =   1980
      Width           =   6015
      Begin VB.CheckBox chkSoloRechazadas 
         BackColor       =   &H00808000&
         Caption         =   "DLa acci�n solamente se realizar� cuando la solicitud est� en estado rechazada"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   31
         Top             =   15
         Width           =   6015
      End
   End
   Begin VB.PictureBox picAnulacion 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   6015
      TabIndex        =   27
      Top             =   1740
      Width           =   6015
      Begin VB.CheckBox chkAnulacion 
         BackColor       =   &H00808000&
         Caption         =   "DLa acci�n representa una anulaci�n"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   0
         TabIndex        =   30
         Top             =   -20
         Width           =   6015
      End
   End
   Begin VB.PictureBox picRechazoDefinitivo 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   6015
      TabIndex        =   25
      Top             =   1500
      Width           =   6015
      Begin VB.CheckBox chkRechazoDefinitivo 
         BackColor       =   &H00808000&
         Caption         =   "DLa acci�n representa un rechazo definitivo"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   28
         Top             =   0
         Width           =   6015
      End
   End
   Begin VB.PictureBox picRechazoTemporal 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   6015
      TabIndex        =   24
      Top             =   1260
      Width           =   6015
      Begin VB.CheckBox chkRechazoTemporal 
         BackColor       =   &H00808000&
         Caption         =   "DLa acci�n representa un rechazo temporal"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   26
         Top             =   0
         Width           =   6015
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTiposNotificado 
      Height          =   555
      Left            =   1005
      TabIndex        =   14
      Top             =   7800
      Width           =   2505
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   4419
      Columns(1).Caption=   "TIPO"
      Columns(1).Name =   "TIPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4410
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddNotificados 
      Height          =   555
      Left            =   3480
      TabIndex        =   15
      Top             =   7800
      Width           =   8280
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   14605
      Columns(1).Caption=   "NOTIFICADO"
      Columns(1).Name =   "NOTIFICADO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   14605
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddAcciones 
      Height          =   555
      Left            =   6240
      TabIndex        =   17
      Top             =   480
      Width           =   3690
      ListAutoValidate=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6509
      Columns(1).Caption=   "ACCION"
      Columns(1).Name =   "ACCION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6509
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   5880
      ScaleHeight     =   420
      ScaleWidth      =   1065
      TabIndex        =   16
      Top             =   9960
      Width           =   1065
      Begin VB.CommandButton cmdCerrar 
         Caption         =   "D&Cerrar"
         Height          =   315
         Left            =   0
         TabIndex        =   36
         Top             =   0
         Width           =   1050
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddTipoEnlace 
      Height          =   555
      Left            =   4800
      TabIndex        =   5
      Top             =   3840
      Width           =   3405
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   6006
      Columns(0).Caption=   "TIPO"
      Columns(0).Name =   "TIPO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5997
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.CommandButton cmdBajarPrecondicion 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":0D5A
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   5760
      Width           =   432
   End
   Begin VB.CommandButton cmdSubirPrecondicion 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":0DB4
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   5400
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirPrecondicion 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":0E0E
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   6120
      Width           =   432
   End
   Begin VB.CommandButton cmdEliminarPrecondicion 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":0E90
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   6480
      Width           =   432
   End
   Begin VB.CommandButton cmdEliminarNotificado 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":0F22
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   7680
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirNotificado 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":0FB4
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   7320
      Width           =   432
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddDestino 
      Height          =   555
      Left            =   960
      TabIndex        =   4
      Top             =   3720
      Width           =   3750
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6615
      Columns(1).Caption=   "DESTINO"
      Columns(1).Name =   "DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6615
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.CommandButton cmdEliminarEnlace 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":1036
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   3720
      Width           =   432
   End
   Begin VB.CommandButton cmdAnyadirEnlace 
      Height          =   312
      Left            =   240
      Picture         =   "frmFlujosDetalleAccion.frx":10C8
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   3360
      Width           =   432
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgNotificados 
      Height          =   1455
      Left            =   720
      TabIndex        =   18
      Top             =   7320
      Width           =   11115
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   7
      stylesets.count =   6
      stylesets(0).Name=   "No"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosDetalleAccion.frx":114A
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "S�"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosDetalleAccion.frx":1166
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Gris"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFlujosDetalleAccion.frx":1182
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFlujosDetalleAccion.frx":119E
      stylesets(4).Name=   "ActiveRow"
      stylesets(4).ForeColor=   16777215
      stylesets(4).BackColor=   8388608
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFlujosDetalleAccion.frx":11BA
      stylesets(5).Name=   "Amarillo"
      stylesets(5).BackColor=   12648447
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFlujosDetalleAccion.frx":11D6
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   79
      Columns.Count   =   7
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "TIPO"
      Columns(1).Name =   "TIPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   2
      Columns(1).FieldLen=   256
      Columns(2).Width=   4419
      Columns(2).Caption=   "DTipo de notificado"
      Columns(2).Name =   "TIPO_DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   3
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "COD_NOTIFICADO"
      Columns(3).Name =   "COD_NOTIFICADO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   11086
      Columns(4).Caption=   "DNotificado"
      Columns(4).Name =   "NOTIFICADO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   3
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "FECACT"
      Columns(5).Name =   "FECACT"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   7
      Columns(5).FieldLen=   256
      Columns(6).Width=   3519
      Columns(6).Caption=   "DConfiguraci�n campos"
      Columns(6).Name =   "CONFCAMPOS"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   1
      _ExtentX        =   19606
      _ExtentY        =   2566
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgEtapasDestino 
      Height          =   1575
      Left            =   720
      TabIndex        =   19
      Top             =   3360
      Width           =   11115
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   10
      stylesets.count =   6
      stylesets(0).Name=   "No"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosDetalleAccion.frx":11F2
      stylesets(0).AlignmentPicture=   2
      stylesets(1).Name=   "S�"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosDetalleAccion.frx":120E
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Gris"
      stylesets(2).BackColor=   -2147483633
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFlujosDetalleAccion.frx":122A
      stylesets(3).Name=   "Normal"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFlujosDetalleAccion.frx":1246
      stylesets(4).Name=   "ActiveRow"
      stylesets(4).ForeColor=   16777215
      stylesets(4).BackColor=   8388608
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFlujosDetalleAccion.frx":1262
      stylesets(5).Name=   "Amarillo"
      stylesets(5).BackColor=   12648447
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFlujosDetalleAccion.frx":127E
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   159
      Columns.Count   =   10
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID_DESTINO"
      Columns(1).Name =   "ID_DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   3
      Columns(1).FieldLen=   256
      Columns(2).Width=   5821
      Columns(2).Caption=   "DDestino"
      Columns(2).Name =   "DESTINO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   3
      Columns(3).Width=   6006
      Columns(3).Caption=   "Dtipo"
      Columns(3).Name =   "TIPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Style=   3
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "FORMULA"
      Columns(4).Name =   "FORMULA"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "DCondici�n del camino"
      Columns(5).Name =   "CONDICION"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   4
      Columns(5).ButtonsAlways=   -1  'True
      Columns(6).Width=   3200
      Columns(6).Caption=   "DNotificados"
      Columns(6).Name =   "NOTIFICADOS"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   4
      Columns(6).ButtonsAlways=   -1  'True
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "FECACT"
      Columns(7).Name =   "FECACT"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   7
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "ID_EST_FACTURA"
      Columns(8).Name =   "ID_EST_FACTURA"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   2
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "Estado de factura asociado"
      Columns(9).Name =   "EST_FACTURA"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      _ExtentX        =   19606
      _ExtentY        =   2778
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picCumpOblRol 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   120
      ScaleHeight     =   255
      ScaleWidth      =   6015
      TabIndex        =   20
      Top             =   720
      Width           =   6015
      Begin VB.CheckBox chkCumpOblRol 
         BackColor       =   &H00808000&
         Caption         =   "DLa acci�n requiere que se cumplimenten los campos obligatorios del ROL"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   0
         TabIndex        =   22
         Top             =   -10
         Width           =   6015
      End
   End
   Begin VB.PictureBox picGuarda 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   195
      Left            =   120
      ScaleHeight     =   195
      ScaleWidth      =   6015
      TabIndex        =   21
      Top             =   1020
      Width           =   6015
      Begin VB.CheckBox chkGuarda 
         BackColor       =   &H00808000&
         Caption         =   "DLa acci�n almacenar� los datos de la solicitud"
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   0
         TabIndex        =   23
         Top             =   0
         Width           =   6015
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgSubject 
      Height          =   945
      Left            =   1680
      TabIndex        =   37
      Top             =   8760
      Width           =   5535
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD_IDIOMA"
      Columns(0).Name =   "COD_IDIOMA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "DEN_IDIOMA"
      Columns(1).Name =   "DEN_IDIOMA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).ForeColor=   16777215
      Columns(1).BackColor=   8421376
      Columns(2).Width=   6509
      Columns(2).Caption=   "VALOR_IDIOMA"
      Columns(2).Name =   "VALOR_IDIOMA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      _ExtentX        =   9763
      _ExtentY        =   1667
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcAccionesExternas 
      Height          =   265
      Left            =   5280
      TabIndex        =   42
      Top             =   2220
      Visible         =   0   'False
      Width           =   2715
      DataFieldList   =   "Column 1"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   -2147483640
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4736
      Columns(1).Caption=   "TIPO"
      Columns(1).Name =   "TIPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4789
      _ExtentY        =   467
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddEstadosFactura 
      Height          =   555
      Left            =   8400
      TabIndex        =   46
      Top             =   3840
      Width           =   3750
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   6615
      Columns(1).Caption=   "DESTINO"
      Columns(1).Name =   "DESTINO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6615
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgDen 
      Height          =   945
      Left            =   6120
      TabIndex        =   47
      Top             =   60
      Width           =   5550
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   3
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD_IDIOMA"
      Columns(0).Name =   "COD_IDIOMA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "DEN_IDIOMA"
      Columns(1).Name =   "DEN_IDIOMA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).ForeColor=   16777215
      Columns(1).BackColor=   8421376
      Columns(2).Width=   6509
      Columns(2).Caption=   "VALOR_IDIOMA"
      Columns(2).Name =   "VALOR_IDIOMA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   50
      _ExtentX        =   9790
      _ExtentY        =   1667
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDen 
      BackColor       =   &H00808000&
      Caption         =   "DNombre de la acci�n a mostrar en el bot�n de comando:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   120
      TabIndex        =   43
      Top             =   60
      Width           =   4155
   End
   Begin VB.Label lblSubject 
      BackColor       =   &H00808000&
      Caption         =   "DAsunto del email:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   120
      TabIndex        =   38
      Top             =   8760
      Width           =   1515
   End
   Begin VB.Label lblNotificados 
      BackColor       =   &H00808000&
      Caption         =   "DLa acci�n ser� notificada a los siguientes participantes:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   405
      TabIndex        =   11
      Top             =   7080
      Width           =   11415
   End
   Begin VB.Label lblPrecondiciones 
      BackColor       =   &H00808000&
      Caption         =   "DLa acci�n necesita comprobar las siguientes precondiciones:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   5160
      Width           =   11415
   End
   Begin VB.Label lblCambioEtapa 
      BackColor       =   &H00808000&
      Caption         =   "DLa acci�n provocar� los siguientes cambio de estapa:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   3120
      Width           =   11415
   End
   Begin VB.Line Line3 
      BorderColor     =   &H00FFFFFF&
      X1              =   60
      X2              =   11760
      Y1              =   6960
      Y2              =   6960
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      X1              =   120
      X2              =   11820
      Y1              =   5040
      Y2              =   5040
   End
   Begin VB.Label lblMaxDen 
      BackColor       =   &H00808000&
      Caption         =   "D(M�ximo 50 caract.)"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   2100
      TabIndex        =   0
      Top             =   360
      Width           =   2115
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      X1              =   120
      X2              =   11820
      Y1              =   3000
      Y2              =   3000
   End
End
Attribute VB_Name = "frmFlujosDetalleAccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Variables Privadas
Private m_oDenominaciones As CMultiidiomas
Private m_oSubject As CMultiidiomas
Private m_Idi As CMultiidioma
Private m_oEnlaceEnEdicion As CEnlace
Private m_oEnlaceAnyadir As CEnlace
Private m_oIBaseDatosEnlaceEnEdicion As IBaseDatos

Private m_oIBaseDatos As IBaseDatos

Private m_bModEnlaceError As Boolean
Private m_bAnyaEnlaceError As Boolean
Private m_bEnlaceError As Boolean

Private m_oPrecondicionEnEdicion As CPrecondicion
Private m_oPrecondicionAnyadir As CPrecondicion
Private m_oIBaseDatosPrecondicionEnEdicion As IBaseDatos
Private m_oPrecondiciones As CPrecondiciones 'Si a CAccionBloque le a�adimos una coleccion de Precondiciones, habra que quitar este parametro y trabajar con la coleccion de CAccionBloque
Private m_bPrecondicionesBtn As Boolean 'Controla si se ha pulsado el boton de Condiciones para la precondicion

Private m_bModPrecondicionError As Boolean
Private m_bAnyaPrecondicicionError As Boolean
Private m_bPrecondicionError As Boolean

Private m_oNotificadoEnEdicion As CNotificadoAccion
Private m_oNotificadoAnyadir As CNotificadoAccion
Private m_oIBaseDatosNotificadoEnEdicion As IBaseDatos
Private m_oNotificados As CNotificadosAccion 'Si a CAccionBloque le a�adimos una coleccion de Notificados, habra que quitar este parametro y trabajar con la coleccion de CAccionBloque

Private m_bModNotificadoError As Boolean
Private m_bAnyaNotificadoError As Boolean
Private m_bNotificadoError As Boolean

Private m_sCaptionAnyadir As String
Private m_sCaptionDetalle As String

Private m_sDestino As String
Private m_sTipoEnlace As String
Private m_sCondicion As String
Private m_sNotificados As String
Private m_sEstadoFactura As String

Private m_sMensajesEnlace(2) As String  'literales para basmensajes
Private m_sTipoCondicional As String
Private m_sTipoObligatorio As String

Private m_sIDPrecond As String
Private m_sTipoPrecond As String
Private m_sCondicionesPrecond As String
Private m_sMensajePrecond As String

Private m_arTipoPrecondicion(1 To 2) As String
Private m_sMensajesPrecondicion(1 To 4) As String

Private m_sTipoNotificado As String
Private m_sNotificado As String
Private m_sConfCampos As String
Private m_sMensajesNotificado(1 To 2) As String
Private m_arTipoNotificado(1 To 3) As String
Private m_arNotificadoGenerico(3 To 8) As String
Private m_sMensajeDenominacion As String
Private prefijo As String

' Acci�n de etapa peticionario
Private m_bBloquePeticionario As Boolean

'Variables Publicas
Public lIdFlujo As Long
Public lIdFormulario As Long

Public lIdAccion As Long
Public oAccion As CAccionBloque
Public oEnlaces_Nuevos As CEnlaces
Public oEnlaces_Modificados As CEnlaces
Public oEnlaces_Eliminados As CEnlaces
Public lIdBloque_Orig As Long
Public lIdBloque_Dest As Long
Public iTipoSolicitud As Integer

Public m_AccionSummit As AccionesSummit
Public m_bSelAccion As Boolean
Public m_bModifFlujo As Boolean
Private m_GridSubjectUpdated As Boolean

Dim m_bSalir As Boolean

Public m_bErrorCondsEt As Boolean
Public m_bBtnClkEt As Boolean
Public m_bErrorCondsPr As Boolean
Public m_bBtnClkPr As Boolean
Private m_bErrorEstadosFactura As Boolean
Private m_bNoDestruirRechazo As Boolean

Private m_oFormulario As CFormulario
Private oEnlace As CEnlace

Private m_bAnyaPrecondicionError As Boolean

Dim m_bActivado As Boolean
Private m_bCambiadoEnLoad As Boolean
Private Sub chkAnulacion_Click()
    Dim bCheck As Boolean
    Dim bCheckMantenerExtrapoint As Boolean
    Dim oEnlace As CEnlace
    Dim oExtraPoint As CEnlaceExtraPoints
    Dim oBloque As CBloque
    Dim oBloques As CBloques
    Dim oBloque_Pet As CBloque
    Dim teserror As TipoErrorSummit
    Dim sEstadoFactura As String

    If chkAnulacion.Value = Checked Then
        If Not m_bBloquePeticionario Then
            chkRechazar.Visible = True
        End If
        bCheck = True And (Not m_bNoDestruirRechazo)
        If (sdbgEtapasDestino.Rows > 0) And (Not m_bNoDestruirRechazo) Then
            If oMensajes.PreguntaEliminarEnlacesAccionRechazoDefinitivo = vbNo Then
                bCheck = False
            End If
        End If
        If bCheck Then
            'Se a�ade un enlace a la etapa PETICIONARIO
            Set oBloques = oFSGSRaiz.Generar_CBloques
            oBloques.CargarBloques basPublic.gParametrosInstalacion.gIdioma, lIdFlujo
            For Each oBloque In oBloques
                If oBloque.Tipo = TipoBloque.Peticionario Then
                    Set oBloque_Pet = oBloque
                    Exit For
                End If
            Next
            Set oBloque = Nothing
            Set oBloques = Nothing
            
            '
            bCheckMantenerExtrapoint = (oAccion.Enlaces.Count = 1)
            '
            
            'Borrar todos los enlaces:
            For Each oEnlace In oAccion.Enlaces
                ''
                If (m_AccionSummit = ACCAccionBloqueModif) And bCheckMantenerExtrapoint Then
                    'Ya exist�a no te carges los extrapoints
                    '   Solo si lo que reemplazas iba a peticionario
                    '   Si era paralelo nada, te los cargas aun con peticionario
                    If oEnlace.BloqueDestino = oBloque_Pet.Id Then
                        bCheckMantenerExtrapoint = True
                        
                        oEnlace.CargarExtraPoints
                        
                        Set oExtraPoint = oFSGSRaiz.Generar_CEnlaceExtraPoints
                        Set oExtraPoint = oEnlace.ExtraPoints
                    Else
                        bCheckMantenerExtrapoint = False
                    End If
                End If
                ''
                Set m_oIBaseDatos = oEnlace
                teserror = m_oIBaseDatos.EliminarDeBaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set m_oIBaseDatos = Nothing
                    Set oEnlace = Nothing
                    Exit Sub
                Else
                    basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceElim, "Accion:" & oAccion.Id & ",Id:" & oEnlace.Id
                End If
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
                oEnlaces_Eliminados.AddEnlace oAccion.Enlaces.Item(CStr(oEnlace.Id))
                oAccion.Enlaces.Remove (CStr(oEnlace.Id))
            Next
        
            oEnlaces_Nuevos.clear
            oEnlaces_Modificados.clear
            sdbgEtapasDestino.CancelUpdate
            sdbgEtapasDestino.RemoveAll
            
            Set oEnlace = oFSGSRaiz.Generar_CEnlace
            oEnlace.BloqueOrigen = oAccion.Bloque
            oEnlace.BloqueDestino = oBloque_Pet.Id
            oEnlace.Accion = oAccion.Id
            Set oEnlace.DenAccion = oAccion.Denominaciones
            oEnlace.TipoAccion = oAccion.Tipo
            Set oEnlace.Subject = SubjectEnlacePorDefecto()
            If bCheckMantenerExtrapoint Then
                Set oEnlace.ExtraPoints = oExtraPoint
            End If
                            
            Set m_oIBaseDatosEnlaceEnEdicion = oEnlace
            teserror = m_oIBaseDatosEnlaceEnEdicion.AnyadirABaseDatos
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                m_bAnyaEnlaceError = True
                Set m_oIBaseDatosEnlaceEnEdicion = Nothing
                Set oEnlace = Nothing
                Exit Sub
            Else
                basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceAnya, "Accion:" & oAccion.Id & ",Id:" & oEnlace.Id
            End If
            Set m_oIBaseDatosEnlaceEnEdicion = Nothing
            
            oAccion.Enlaces.AddEnlace oEnlace
            oEnlaces_Nuevos.AddEnlace oEnlace
            
            sEstadoFactura = DevolverDenominacionEstadoFactura(oEnlace.EstadoFactura)
            sdbgEtapasDestino.AddItem oEnlace.Id & Chr(m_lSeparador) & oBloque_Pet.Id & Chr(m_lSeparador) & oBloque_Pet.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & m_sTipoObligatorio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oEnlace.FecAct & Chr(m_lSeparador) & oEnlace.EstadoFactura & Chr(m_lSeparador) & sEstadoFactura
            Set oEnlace = Nothing
            
            'Se deshabilita la Grid y los botones
            sdbgEtapasDestino.Enabled = False
            cmdAnyadirEnlace.Enabled = False
            cmdEliminarEnlace.Enabled = False
            
            chkRechazoTemporal.Value = Unchecked
            chkRechazoDefinitivo.Value = Unchecked
            chkRechazar.Top = picAnulacion.Top
            chkRechazar.Visible = Not m_bBloquePeticionario

        ElseIf m_bNoDestruirRechazo Then
            'No hacer nada que pierdes los extrapoints
        Else
            chkAnulacion.Value = Unchecked
        End If
        
        If chkAnulacion.Value = Checked Then
            oAccion.Tipo = TipoAccionBloque.Rechazo
            oAccion.TipoRechazo = TipoRechazoAccionBloque.Anulacion
            oAccion.ModificarAccion_optRechazos
        End If
    Else
        sdbgEtapasDestino.Enabled = True
        cmdAnyadirEnlace.Enabled = True
        cmdEliminarEnlace.Enabled = True

        If sdbgEtapasDestino.Rows > 0 Then
            oAccion.Tipo = TipoAccionBloque.Aprobacion
        Else
            oAccion.Tipo = 0
        End If
        oAccion.TipoRechazo = 0
        oAccion.ModificarAccion_optRechazos
    End If
    If oAccion.TipoRechazo <> 0 Then
        Select Case oAccion.TipoRechazo
            Case TipoRechazoAccionBloque.RechazoTemporal
                chkRechazar.Top = picRechazoTemporal.Top
            Case TipoRechazoAccionBloque.RechazoDefinitivo
                chkRechazar.Top = picRechazoDefinitivo.Top
            Case TipoRechazoAccionBloque.Anulacion
                chkRechazar.Top = picAnulacion.Top
        End Select
        chkRechazar.Visible = Not m_bBloquePeticionario
    Else
        chkRechazar.Visible = False
    End If
    If Not chkRechazar.Visible Then chkRechazar.Value = Unchecked
End Sub

Private Sub chkAprobar_Click()
    Dim oBloque As CBloque
    Dim oAccionApro As CAccionBloque
    Dim lIdAccion As Long
    Dim teserror As TipoErrorSummit
    If chkAprobar.Value = 1 Then
        Set oBloque = oFSGSRaiz.Generar_CBloque
        oBloque.Id = oAccion.Bloque
        lIdAccion = oBloque.AccionAprobar()
        If lIdAccion >= 0 And lIdAccion <> oAccion.Id Then
            Set oAccionApro = oFSGSRaiz.Generar_CAccionBloque
            oAccionApro.Id = lIdAccion
            Set m_oIBaseDatos = oAccionApro
            teserror = m_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oAccionApro = Nothing
                Set m_oIBaseDatos = Nothing
                Exit Sub
            End If
            If oMensajes.AvisoAprobarRechazar(oAccionApro.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den, True) = vbNo Then
                chkAprobar.Value = 0
            End If
        End If
    End If
    If chkAprobar.Value = 1 Then chkRechazar.Value = 0
    If (chkAprobar.Value = 1 Xor oAccion.Aprobar) Or (chkRechazar.Value = 1 Xor oAccion.Rechazar) Then
        Dim bActRoles As Boolean
        Dim sRoles As String
        bActRoles = False
        If Not oAccion.Aprobar And chkAprobar.Value = 1 Then
            If oBloque Is Nothing Then
                Set oBloque = oFSGSRaiz.Generar_CBloque
                oBloque.Id = oAccion.Bloque
            End If
            sRoles = oBloque.RolesSinAccionAprobar(oAccion.Id)
            If sRoles <> "" Then
                If oMensajes.PreguntaActAccionRoles(oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den, sRoles, True) = vbYes Then bActRoles = True
            End If
        End If
        oAccion.Aprobar = (chkAprobar.Value = 1)
        oAccion.Rechazar = (chkRechazar.Value = 1)
        teserror = oAccion.ModificarAccion_AprobarRechazar(bActRoles)
        If teserror.NumError = TESnoerror Then
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
End Sub

Private Sub chkRechazar_Click()
    Dim oBloque As CBloque
    Dim oAccionRech As CAccionBloque
    Dim lIdAccion As Long
    Dim teserror As TipoErrorSummit
    If chkRechazar.Value = 1 Then
        Set oBloque = oFSGSRaiz.Generar_CBloque
        oBloque.Id = oAccion.Bloque
        lIdAccion = oBloque.AccionRechazar()
        If lIdAccion >= 0 And lIdAccion <> oAccion.Id Then
            Set oAccionRech = oFSGSRaiz.Generar_CAccionBloque
            oAccionRech.Id = lIdAccion
            Set m_oIBaseDatos = oAccionRech
            teserror = m_oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oAccionRech = Nothing
                Set m_oIBaseDatos = Nothing
                Exit Sub
            End If
            If oMensajes.AvisoAprobarRechazar(oAccionRech.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den, False) = vbNo Then
                chkRechazar.Value = 0
            End If
        End If
    End If
    If chkRechazar.Value = 1 Then chkAprobar.Value = 0
    If (chkAprobar.Value = 1 Xor oAccion.Aprobar) Or (chkRechazar.Value = 1 Xor oAccion.Rechazar) Then
        Dim bActRoles As Boolean
        Dim sRoles As String
        bActRoles = False
        If Not oAccion.Rechazar And chkRechazar.Value = 1 Then
            If oBloque Is Nothing Then
                Set oBloque = oFSGSRaiz.Generar_CBloque
                oBloque.Id = oAccion.Bloque
                sRoles = oBloque.RolesSinAccionRechazar(oAccion.Id)
                If sRoles <> "" Then
                    If oMensajes.PreguntaActAccionRoles(oAccion.Id, sRoles, False) = vbYes Then bActRoles = True
                End If
            End If
        End If
        oAccion.Aprobar = (chkAprobar.Value = 1)
        oAccion.Rechazar = (chkRechazar.Value = 1)
        teserror = oAccion.ModificarAccion_AprobarRechazar(bActRoles)
        If teserror.NumError = TESnoerror Then
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
End Sub

Private Sub chkIncrementarId_Click()
    oAccion.IncrementarId = chkIncrementarId.Value
    If m_bActivado Then
        oAccion.ModificarAccion_optIncrementarID
        frmFlujos.HayCambios
    End If
End Sub

Private Sub chkRechazoDefinitivo_Click()
    Dim bCheck As Boolean
    Dim bCheckMantenerExtrapoint As Boolean
    Dim oEnlace As CEnlace
    Dim oExtraPoint As CEnlaceExtraPoints
    Dim oBloque As CBloque
    Dim oBloques As CBloques
    Dim oBloque_Pet As CBloque
    Dim teserror As TipoErrorSummit
    Dim sEstadoFactura As String

    If chkRechazoDefinitivo.Value = Checked Then
        If chkSoloRechazadas.Value = Checked Then
            chkRechazoDefinitivo.Value = Unchecked
        Else
            bCheck = True And (Not m_bNoDestruirRechazo)
            If (sdbgEtapasDestino.Rows > 0) And (Not m_bNoDestruirRechazo) Then
                If oMensajes.PreguntaEliminarEnlacesAccionRechazoDefinitivo = vbNo Then
                    bCheck = False
                End If
            End If
            If bCheck Then
                'Se a�ade un enlace a la etapa PETICIONARIO
                Set oBloques = oFSGSRaiz.Generar_CBloques
                oBloques.CargarBloques basPublic.gParametrosInstalacion.gIdioma, lIdFlujo
                For Each oBloque In oBloques
                    If oBloque.Tipo = TipoBloque.Peticionario Then
                        Set oBloque_Pet = oBloque
                        Exit For
                    End If
                Next
                Set oBloque = Nothing
                Set oBloques = Nothing
                
                '
                bCheckMantenerExtrapoint = (oAccion.Enlaces.Count = 1)
                '
                
                'Borrar todos los enlaces:
                For Each oEnlace In oAccion.Enlaces
                    ''
                    If (m_AccionSummit = ACCAccionBloqueModif) And bCheckMantenerExtrapoint Then
                        'Ya exist�a no te carges los extrapoints
                        '   Solo si lo que reemplazas iba a peticionario
                        '   Si era paralelo nada, te los cargas aun con peticionario
                        If oEnlace.BloqueDestino = oBloque_Pet.Id Then
                            bCheckMantenerExtrapoint = True
                            
                            oEnlace.CargarExtraPoints
                            
                            Set oExtraPoint = oFSGSRaiz.Generar_CEnlaceExtraPoints
                            Set oExtraPoint = oEnlace.ExtraPoints
                        Else
                            bCheckMantenerExtrapoint = False
                        End If
                    End If
                    ''
                    Set m_oIBaseDatos = oEnlace
                    teserror = m_oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set m_oIBaseDatos = Nothing
                        Set oEnlace = Nothing
                        Exit Sub
                    Else
                        basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceElim, "Accion:" & oAccion.Id & ",Id:" & oEnlace.Id
                    End If
                    oEnlaces_Eliminados.AddEnlace oAccion.Enlaces.Item(CStr(oEnlace.Id))
                    oAccion.Enlaces.Remove (CStr(oEnlace.Id))
                Next
                oEnlaces_Nuevos.clear
                oEnlaces_Modificados.clear
                sdbgEtapasDestino.CancelUpdate
                sdbgEtapasDestino.RemoveAll
                
                Set oEnlace = oFSGSRaiz.Generar_CEnlace
                oEnlace.BloqueOrigen = oAccion.Bloque
                oEnlace.BloqueDestino = oBloque_Pet.Id
                oEnlace.Accion = oAccion.Id
                Set oEnlace.DenAccion = oAccion.Denominaciones
                oEnlace.TipoAccion = oAccion.Tipo
                Set oEnlace.Subject = SubjectEnlacePorDefecto()
                If bCheckMantenerExtrapoint Then
                    Set oEnlace.ExtraPoints = oExtraPoint
                End If
                                
                Set m_oIBaseDatosEnlaceEnEdicion = oEnlace
                teserror = m_oIBaseDatosEnlaceEnEdicion.AnyadirABaseDatos
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    m_bAnyaEnlaceError = True
                    Set m_oIBaseDatosEnlaceEnEdicion = Nothing
                    Set oEnlace = Nothing
                    Exit Sub
                Else
                    basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceAnya, "Accion:" & oAccion.Id & ",Id:" & oEnlace.Id
                    'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                    frmFlujos.HayCambios
                End If
                Set m_oIBaseDatosEnlaceEnEdicion = Nothing
                
                oAccion.Enlaces.AddEnlace oEnlace
                oEnlaces_Nuevos.AddEnlace oEnlace
                
                sEstadoFactura = DevolverDenominacionEstadoFactura(oEnlace.EstadoFactura)
                sdbgEtapasDestino.AddItem oEnlace.Id & Chr(m_lSeparador) & oBloque_Pet.Id & Chr(m_lSeparador) & oBloque_Pet.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & m_sTipoObligatorio & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oEnlace.FecAct & Chr(m_lSeparador) & oEnlace.EstadoFactura & Chr(m_lSeparador) & sEstadoFactura
                Set oEnlace = Nothing
                
                'Se deshabilita la Grid y los botones
                sdbgEtapasDestino.Enabled = False
                cmdAnyadirEnlace.Enabled = False
                cmdEliminarEnlace.Enabled = False
                    
                chkRechazoTemporal.Value = Unchecked
                chkAnulacion.Value = Unchecked
                chkRechazar.Top = picRechazoDefinitivo.Top
                chkRechazar.Visible = Not m_bBloquePeticionario
                If oAccion.TipoRechazo <> RechazoDefinitivo And chkRechazar.Visible Then chkRechazar.Value = Checked
            ElseIf m_bNoDestruirRechazo Then
                'No hacer nada que pierdes los extrapoints
            Else
                chkRechazoDefinitivo.Value = Unchecked
            End If
        End If
        If chkRechazoDefinitivo.Value = Checked Then
            oAccion.Tipo = TipoAccionBloque.Rechazo
            oAccion.TipoRechazo = TipoRechazoAccionBloque.RechazoDefinitivo
            oAccion.ModificarAccion_optRechazos
        End If
        
    ElseIf (chkRechazoTemporal.Value = Unchecked And chkAnulacion.Value = Unchecked) Then
        sdbgEtapasDestino.Enabled = True
        cmdAnyadirEnlace.Enabled = True
        cmdEliminarEnlace.Enabled = True
        
        If sdbgEtapasDestino.Rows > 0 Then
            oAccion.Tipo = TipoAccionBloque.Aprobacion
        Else
            oAccion.Tipo = 0
        End If
        oAccion.TipoRechazo = 0
        teserror = oAccion.ModificarAccion_optRechazos
        If teserror.NumError = TESnoerror Then
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
    If oAccion.TipoRechazo <> 0 Then
        Select Case oAccion.TipoRechazo
            Case TipoRechazoAccionBloque.RechazoTemporal
                chkRechazar.Top = picRechazoTemporal.Top
            Case TipoRechazoAccionBloque.RechazoDefinitivo
                chkRechazar.Top = picRechazoDefinitivo.Top
            Case TipoRechazoAccionBloque.Anulacion
                chkRechazar.Top = picAnulacion.Top
        End Select
        chkRechazar.Visible = Not m_bBloquePeticionario
    Else
        chkRechazar.Visible = False
    End If
    If Not chkRechazar.Visible Then chkRechazar.Value = Unchecked
End Sub

Private Sub chkRechazoTemporal_Click()
       
    If chkRechazoTemporal.Value = Checked Then
        If chkSoloRechazadas.Value = Checked Then
            chkRechazoTemporal.Value = Unchecked
        Else
            chkRechazoDefinitivo.Value = Unchecked
            sdbgEtapasDestino.Enabled = True
            chkAnulacion.Value = Unchecked
            chkRechazar.Top = picRechazoTemporal.Top
            chkRechazar.Visible = Not m_bBloquePeticionario
            If oAccion.TipoRechazo <> TipoRechazoAccionBloque.RechazoTemporal And chkRechazar.Visible Then chkRechazar.Value = Checked
        End If
        If chkRechazoTemporal.Value = Checked Then
            oAccion.Tipo = TipoAccionBloque.Rechazo
            oAccion.TipoRechazo = TipoRechazoAccionBloque.RechazoTemporal
            oAccion.ModificarAccion_optRechazos
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    Else
        If sdbgEtapasDestino.Rows > 0 Then
            oAccion.Tipo = TipoAccionBloque.Aprobacion
        Else
            oAccion.Tipo = 0
        End If
        oAccion.TipoRechazo = 0
        oAccion.ModificarAccion_optRechazos
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
    If oAccion.TipoRechazo <> 0 Then
        Select Case oAccion.TipoRechazo
            Case TipoRechazoAccionBloque.RechazoTemporal
                chkRechazar.Top = picRechazoTemporal.Top
            Case TipoRechazoAccionBloque.RechazoDefinitivo
                chkRechazar.Top = picRechazoDefinitivo.Top
            Case TipoRechazoAccionBloque.Anulacion
                chkRechazar.Top = picAnulacion.Top
        End Select
        chkRechazar.Visible = Not m_bBloquePeticionario
    Else
        chkRechazar.Visible = False
    End If
    If Not chkRechazar.Visible Then chkRechazar.Value = Unchecked
End Sub

Private Sub chkSoloRechazadas_Click()
    If chkSoloRechazadas.Value = Checked Then
        chkRechazoDefinitivo.Value = Unchecked
        chkRechazoTemporal.Value = Unchecked
        chkIncrementarId.Visible = True
    Else
        chkIncrementarId.Visible = False
        chkIncrementarId.Value = Unchecked
        oAccion.IncrementarId = chkIncrementarId.Value
        oAccion.ModificarAccion_optIncrementarID
    End If
    oAccion.SoloRechazadas = chkSoloRechazadas.Value
    If m_bActivado Then
        oAccion.ModificarAccion_optSoloRechazadas
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
    If oAccion.TipoRechazo <> 0 Then
        Select Case oAccion.TipoRechazo
            Case TipoRechazoAccionBloque.RechazoTemporal
                chkRechazar.Top = picRechazoTemporal.Top
            Case TipoRechazoAccionBloque.RechazoDefinitivo
                chkRechazar.Top = picRechazoDefinitivo.Top
            Case TipoRechazoAccionBloque.Anulacion
                chkRechazar.Top = picAnulacion.Top
        End Select
        chkRechazar.Visible = Not m_bBloquePeticionario
    Else
        chkRechazar.Visible = False
    End If
    If Not chkRechazar.Visible Then chkRechazar.Value = Unchecked
End Sub

Private Function GuardarAccion() As Boolean
    Dim teserror As TipoErrorSummit
    Dim oEnlace As CEnlace
    
    'Forzar Updates
    Set oAccion.Denominaciones = m_oDenominaciones
    
    Set oAccion.Subject = m_oSubject
    
    If sdbgEtapasDestino.DataChanged Then
        sdbgEtapasDestino.Update
        If m_bEnlaceError Then
            GuardarAccion = False
            Exit Function
        End If
    End If
    If sdbgPrecondiciones.DataChanged Then
        sdbgPrecondiciones.Update
        If m_bPrecondicionError Then
            GuardarAccion = False
            Exit Function
        End If
    End If
    If sdbgNotificados.DataChanged Then
        sdbgNotificados.Update
        If m_bNotificadoError Then
            GuardarAccion = False
            Exit Function
        End If
    End If
    
    If oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den = "" Then
        oMensajes.NoValido m_sMensajeDenominacion & " " & oGestorParametros.DevolverIdiomas.Item(basPublic.gParametrosInstalacion.gIdioma).Den
        GuardarAccion = False
        Exit Function
    End If
    
    oAccion.CumpOblRol = (chkCumpOblRol.Value <> Unchecked)
    oAccion.Guarda = (chkGuarda.Value <> Unchecked)
    If chkRechazoTemporal.Value = Unchecked And chkRechazoDefinitivo.Value = Unchecked And chkAnulacion.Value = Unchecked Then
        If sdbgEtapasDestino.Rows > 0 Then
            oAccion.Tipo = TipoAccionBloque.Aprobacion
        Else
            oAccion.Tipo = 0
        End If
        oAccion.TipoRechazo = 0
    Else
        oAccion.Tipo = TipoAccionBloque.Rechazo
        If chkRechazoTemporal.Value = Checked Then
            oAccion.TipoRechazo = TipoRechazoAccionBloque.RechazoTemporal
        ElseIf chkRechazoDefinitivo.Value = Checked Then
            oAccion.TipoRechazo = TipoRechazoAccionBloque.RechazoDefinitivo
        Else
            oAccion.TipoRechazo = TipoRechazoAccionBloque.Anulacion
        End If
    End If
    oAccion.SoloRechazadas = (chkSoloRechazadas.Value <> Unchecked)
    oAccion.IncrementarId = (chkIncrementarId.Value <> Unchecked)
    For Each oEnlace In oAccion.Enlaces
        Set oEnlace.DenAccion = oAccion.Denominaciones
        oEnlace.TipoAccion = oAccion.Tipo
        oEnlace.LlamadaExterna = oAccion.idLlamadaExterna > 0
    Next
    For Each oEnlace In oEnlaces_Modificados
        Set oEnlace.DenAccion = oAccion.Denominaciones
        oEnlace.TipoAccion = oAccion.Tipo
        oEnlace.LlamadaExterna = oAccion.idLlamadaExterna > 0
    Next
    For Each oEnlace In oEnlaces_Nuevos
        Set oEnlace.DenAccion = oAccion.Denominaciones
        oEnlace.TipoAccion = oAccion.Tipo
        oEnlace.LlamadaExterna = oAccion.idLlamadaExterna > 0
    Next
    Set m_oIBaseDatos = oAccion
    teserror = m_oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Set m_oIBaseDatos = Nothing
        GuardarAccion = False
        Exit Function
    Else
        basSeguridad.RegistrarAccion AccionesSummit.ACCAccionBloqueModif, "ID:" & oAccion.Id
        Set m_oIBaseDatos = Nothing
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
    m_AccionSummit = 0
    
    frmFlujos.reflejarAccionEnDiagrama
    
    GuardarAccion = True
End Function

Private Sub cmdAnyadirEnlace_Click()
    
    sdbgEtapasDestino.Scroll 0, sdbgEtapasDestino.Rows - sdbgEtapasDestino.Row
    
    If sdbgEtapasDestino.VisibleRows > 0 Then
        
        If sdbgEtapasDestino.VisibleRows >= sdbgEtapasDestino.Rows Then
            If sdbgEtapasDestino.VisibleRows = sdbgEtapasDestino.Rows Then
                sdbgEtapasDestino.Row = sdbgEtapasDestino.Rows - 1
            Else
                sdbgEtapasDestino.Row = sdbgEtapasDestino.Rows
    
            End If
        Else
            sdbgEtapasDestino.Row = sdbgEtapasDestino.Rows - (sdbgEtapasDestino.Rows - sdbgEtapasDestino.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgEtapasDestino.SetFocus
End Sub

Private Sub cmdAnyadirNotificado_Click()
    
    sdbgNotificados.Scroll 0, sdbgNotificados.Rows - sdbgNotificados.Row
    
    If sdbgNotificados.VisibleRows > 0 Then
        
        If sdbgNotificados.VisibleRows >= sdbgNotificados.Rows Then
            If sdbgNotificados.VisibleRows = sdbgNotificados.Rows Then
                sdbgNotificados.Row = sdbgNotificados.Rows - 1
            Else
                sdbgNotificados.Row = sdbgNotificados.Rows
    
            End If
        Else
            sdbgNotificados.Row = sdbgNotificados.Rows - (sdbgNotificados.Rows - sdbgNotificados.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgNotificados.SetFocus
End Sub


Private Sub cmdAnyadirPrecondicion_Click()
    cmdAnyadirPrecondicion.Enabled = False
    cmdSubirPrecondicion.Enabled = False
    cmdBajarPrecondicion.Enabled = False
    
    sdbgPrecondiciones.Scroll 0, sdbgPrecondiciones.Rows - sdbgPrecondiciones.Row
    
    If sdbgPrecondiciones.VisibleRows > 0 Then
        
        If sdbgPrecondiciones.VisibleRows >= sdbgPrecondiciones.Rows Then
            If sdbgPrecondiciones.VisibleRows = sdbgPrecondiciones.Rows Then
                sdbgPrecondiciones.Row = sdbgPrecondiciones.Rows - 1
            Else
                sdbgPrecondiciones.Row = sdbgPrecondiciones.Rows
    
            End If
        Else
            sdbgPrecondiciones.Row = sdbgPrecondiciones.Rows - (sdbgPrecondiciones.Rows - sdbgPrecondiciones.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgPrecondiciones.SetFocus
End Sub

Private Sub cmdBajarPrecondicion_Click()
    CambiarOrdenCumplimentacion False
End Sub


Private Sub cmdEliminarEnlace_Click()
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit
    Dim bEliminarAccion As Boolean
    Dim vbm As Variant
    Dim i As Integer
    
    If sdbgEtapasDestino.IsAddRow Then
        If sdbgEtapasDestino.DataChanged Then
            sdbgEtapasDestino.CancelUpdate
        End If
    Else
        If sdbgEtapasDestino.Rows = 0 Then Exit Sub

        If sdbgEtapasDestino.DataChanged Then
            sdbgEtapasDestino.CancelUpdate
        End If
        
        Screen.MousePointer = vbHourglass
        
        If sdbgEtapasDestino.SelBookmarks.Count = 1 Then
            bEliminarAccion = False
            
            If sdbgEtapasDestino.Rows = 1 Then
                 irespuesta = oMensajes.PreguntaEliminarAccionPorUnicoEnlace
                 If irespuesta = vbNo Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                 End If
                 bEliminarAccion = True
            Else
                irespuesta = oMensajes.PreguntaEliminar(sdbgEtapasDestino.Columns("DESTINO").Value)
                If irespuesta = vbNo Then
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
            
           
                Set m_oEnlaceEnEdicion = oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value))
                Set m_oIBaseDatosEnlaceEnEdicion = m_oEnlaceEnEdicion
                teserror = m_oIBaseDatosEnlaceEnEdicion.EliminarDeBaseDatos()
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    Screen.MousePointer = vbNormal
                    
                    If Me.Visible Then sdbgEtapasDestino.SetFocus
                    Exit Sub
                Else
                    oEnlaces_Nuevos.Remove (CStr(sdbgEtapasDestino.Columns("ID").Value))
                    oEnlaces_Modificados.Remove (CStr(sdbgEtapasDestino.Columns("ID").Value))
                    oEnlaces_Eliminados.AddEnlace oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value))
                    oAccion.Enlaces.Remove (CStr(sdbgEtapasDestino.Columns("ID").Value))
                    If sdbgEtapasDestino.AddItemRowIndex(sdbgEtapasDestino.Bookmark) > -1 Then
                        sdbgEtapasDestino.RemoveItem (sdbgEtapasDestino.AddItemRowIndex(sdbgEtapasDestino.Bookmark))
                    Else
                        sdbgEtapasDestino.RemoveItem (0)
                    End If
                    basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceElim, "Accion:" & oAccion.Id & ",Id:" & m_oEnlaceEnEdicion.Id
                    'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                    frmFlujos.HayCambios
                End If
                Set m_oIBaseDatosEnlaceEnEdicion = Nothing
                Set m_oEnlaceEnEdicion = Nothing
        End If

        sdbgEtapasDestino.SelBookmarks.RemoveAll
        
            m_bErrorCondsEt = False
            For i = 0 To sdbgEtapasDestino.Rows - 1
                vbm = sdbgEtapasDestino.AddItemBookmark(i)
                If sdbgEtapasDestino.Columns("TIPO").CellValue(vbm) = m_sTipoCondicional Then
                    m_bErrorCondsEt = True
                    Exit For
                End If
            Next i
        
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub cmdEliminarNotificado_Click()
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit

    If sdbgNotificados.IsAddRow Then
        If sdbgNotificados.DataChanged Then
            sdbgNotificados.CancelUpdate
        End If
    Else
        If sdbgNotificados.Rows = 0 Then Exit Sub
        
        If sdbgNotificados.DataChanged Then
            sdbgNotificados.CancelUpdate
        End If
        
        Screen.MousePointer = vbHourglass
        
        If sdbgNotificados.SelBookmarks.Count = 1 Then
            irespuesta = oMensajes.PreguntaEliminar(sdbgNotificados.Columns("NOTIFICADO").Value)
            
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set m_oNotificadoEnEdicion = m_oNotificados.Item(CStr(sdbgNotificados.Columns("ID").Value))
            
            Set m_oIBaseDatosNotificadoEnEdicion = m_oNotificadoEnEdicion
            teserror = m_oIBaseDatosNotificadoEnEdicion.EliminarDeBaseDatos
            
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                If Me.Visible Then sdbgNotificados.SetFocus
                Exit Sub
            Else
                m_oNotificados.Remove (CStr(sdbgNotificados.Columns("ID").Value))
                If sdbgNotificados.AddItemRowIndex(sdbgNotificados.Bookmark) > -1 Then
                    sdbgNotificados.RemoveItem (sdbgNotificados.AddItemRowIndex(sdbgNotificados.Bookmark))
                Else
                    sdbgNotificados.RemoveItem (0)
                End If
                basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionElim, "Accion:" & oAccion.Id & ",ID:" & m_oNotificadoEnEdicion.Id
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If
            
            Set m_oIBaseDatosNotificadoEnEdicion = Nothing
            Set m_oNotificadoEnEdicion = Nothing
            
        End If
            
        sdbgNotificados.SelBookmarks.RemoveAll
    
        Screen.MousePointer = vbNormal
    
    End If
End Sub

Private Sub cmdEliminarPrecondicion_Click()
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit

    If sdbgPrecondiciones.IsAddRow Then
        If sdbgPrecondiciones.DataChanged Then
            sdbgPrecondiciones.CancelUpdate
        End If
    Else
        If sdbgPrecondiciones.Rows = 0 Then Exit Sub
        
        If sdbgPrecondiciones.DataChanged Then
            sdbgPrecondiciones.CancelUpdate
        End If
        
        Screen.MousePointer = vbHourglass
        
        If sdbgPrecondiciones.SelBookmarks.Count = 1 Then
            irespuesta = oMensajes.PreguntaEliminar(sdbgPrecondiciones.Columns("COD").Value)
            
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set m_oPrecondicionEnEdicion = m_oPrecondiciones.Item(CStr(sdbgPrecondiciones.Columns("ID").Value))
            
            Set m_oIBaseDatosPrecondicionEnEdicion = m_oPrecondicionEnEdicion
            teserror = m_oIBaseDatosPrecondicionEnEdicion.EliminarDeBaseDatos
            
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                
                If Me.Visible Then sdbgPrecondiciones.SetFocus
                Exit Sub
            
            Else
                m_oPrecondiciones.Remove (CStr(sdbgPrecondiciones.Columns("ID").Value))
                If sdbgPrecondiciones.AddItemRowIndex(sdbgPrecondiciones.Bookmark) > -1 Then
                    sdbgPrecondiciones.RemoveItem (sdbgPrecondiciones.AddItemRowIndex(sdbgPrecondiciones.Bookmark))
                Else
                    sdbgPrecondiciones.RemoveItem (0)
                End If
                basSeguridad.RegistrarAccion AccionesSummit.ACCPrecondicionElim, "Accion:" & oAccion.Id & ",ID:" & m_oPrecondicionEnEdicion.Id
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If
            
            Set m_oIBaseDatosPrecondicionEnEdicion = Nothing
            Set m_oPrecondicionEnEdicion = Nothing
            
        End If
            
        sdbgPrecondiciones.SelBookmarks.RemoveAll
        m_bErrorCondsPr = False
       
        Screen.MousePointer = vbNormal
    
        CargarPrecondiciones
    End If
End Sub

Private Sub cmdSubirPrecondicion_Click()
    CambiarOrdenCumplimentacion True
End Sub

Private Sub CambiarOrdenCumplimentacion(ByVal bSubir As Boolean)
Dim i As Integer
Dim arrValores() As Variant
Dim arrValores2() As Variant
Dim oIBaseDatos As IBaseDatos
Dim teserror As TipoErrorSummit
Dim lineaAct As Integer
Dim bookMK As Variant

    If sdbgPrecondiciones.DataChanged Then
        sdbgPrecondiciones.Update
        If m_bPrecondicionError Then
            Exit Sub
        End If
    End If

    If sdbgPrecondiciones.SelBookmarks.Count = 0 Then Exit Sub
    If bSubir Then
        If sdbgPrecondiciones.AddItemRowIndex(sdbgPrecondiciones.SelBookmarks.Item(0)) = 0 Then Exit Sub
    Else
        If sdbgPrecondiciones.AddItemRowIndex(sdbgPrecondiciones.SelBookmarks.Item(0)) = sdbgPrecondiciones.Rows - 1 Then Exit Sub
    End If

    i = 0
    ReDim arrValores(sdbgPrecondiciones.Columns.Count - 1)
    ReDim arrValores2(sdbgPrecondiciones.Columns.Count - 1)

    'Cambio el Orden de la Precondicion a Mover
    Set m_oPrecondicionEnEdicion = m_oPrecondiciones.Item(CStr(sdbgPrecondiciones.Columns("ID").Value))
    If bSubir Then
        m_oPrecondicionEnEdicion.Orden = m_oPrecondicionEnEdicion.Orden - 1
    Else
        m_oPrecondicionEnEdicion.Orden = m_oPrecondicionEnEdicion.Orden + 1
    End If
    Set oIBaseDatos = m_oPrecondicionEnEdicion
    teserror = oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oPrecondicionEnEdicion = Nothing
        Set oIBaseDatos = Nothing
        Exit Sub
    End If

    For i = 0 To sdbgPrecondiciones.Columns.Count - 1
        arrValores(i) = sdbgPrecondiciones.Columns(i).Value
    Next i

    sdbgPrecondiciones.DataChanged = False

    If bSubir Then
        sdbgPrecondiciones.MovePrevious
    Else
        sdbgPrecondiciones.MoveNext
    End If

    'Cambio el Orden de la la otra Precondicion a Mover
    Set m_oPrecondicionEnEdicion = m_oPrecondiciones.Item(CStr(sdbgPrecondiciones.Columns("ID").Value))
    If bSubir Then
        m_oPrecondicionEnEdicion.Orden = m_oPrecondicionEnEdicion.Orden + 1
    Else
        m_oPrecondicionEnEdicion.Orden = m_oPrecondicionEnEdicion.Orden - 1
    End If
    Set oIBaseDatos = m_oPrecondicionEnEdicion
    teserror = oIBaseDatos.FinalizarEdicionModificando
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set m_oPrecondicionEnEdicion = Nothing
        Set oIBaseDatos = Nothing
        Exit Sub
    End If
    'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
    frmFlujos.HayCambios
    
    For i = 0 To sdbgPrecondiciones.Columns.Count - 1
        arrValores2(i) = sdbgPrecondiciones.Columns(i).Value
        sdbgPrecondiciones.Columns(i).Value = arrValores(i)
    Next i
    
    sdbgPrecondiciones.DataChanged = False

    If bSubir Then
        sdbgPrecondiciones.MoveNext
    Else
        sdbgPrecondiciones.MovePrevious
    End If

    For i = 0 To sdbgPrecondiciones.Columns.Count - 1
        sdbgPrecondiciones.Columns(i).Value = arrValores2(i)
    Next i
    
    sdbgPrecondiciones.DataChanged = False

    sdbgPrecondiciones.SelBookmarks.RemoveAll
    If bSubir Then
        sdbgPrecondiciones.MovePrevious
    Else
        sdbgPrecondiciones.MoveNext
    End If
    
    lineaAct = sdbgPrecondiciones.Row
    bookMK = sdbgPrecondiciones.Bookmark
    
    CargarPrecondiciones
    
    sdbgPrecondiciones.SelBookmarks.RemoveAll
    sdbgPrecondiciones.SelBookmarks.Add bookMK
    sdbgPrecondiciones.Row = lineaAct
End Sub


Private Sub Form_Activate()
If Not m_bActivado Then
    m_bActivado = True
End If
End Sub

Private Sub Form_Load()
    m_bErrorCondsEt = False
    m_bErrorCondsPr = False
    m_bErrorEstadosFactura = False
    m_bBtnClkEt = False
    m_bBtnClkPr = False
    m_bActivado = False
    m_bCambiadoEnLoad = False
    Me.Height = 9580
    Me.Width = 12060
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    Me.caption = m_sCaptionDetalle
      
    PonerFieldSeparator Me
      
    ConfigurarSeguridad
    If m_AccionSummit = ACCAccionBloqueAnya Then
        CrearAccion
    End If
    m_bNoDestruirRechazo = True
    CargarAccion
    m_bNoDestruirRechazo = False
    
    m_bSalir = False
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSDETALLEACCION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        m_sCaptionAnyadir = Ador(0).Value '1
        Ador.MoveNext
        m_sCaptionDetalle = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value
        Ador.MoveNext
        lblMaxDen.caption = Ador(0).Value
        Ador.MoveNext
        chkCumpOblRol.caption = Ador(0).Value '5
        Ador.MoveNext
        chkGuarda.caption = Ador(0).Value
        Ador.MoveNext
        chkRechazoTemporal.caption = Ador(0).Value
        Ador.MoveNext
        chkRechazoDefinitivo.caption = Ador(0).Value
        Ador.MoveNext
        chkAnulacion.caption = Ador(0).Value
        Ador.MoveNext
        lblCambioEtapa.caption = Ador(0).Value '10
        Ador.MoveNext
        cmdAnyadirEnlace.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarEnlace.ToolTipText = Ador(0).Value
        Ador.MoveNext
        m_sDestino = Ador(0).Value
        m_sMensajesEnlace(0) = Ador(0).Value
        Ador.MoveNext
        m_sTipoEnlace = Ador(0).Value
        m_sMensajesEnlace(1) = Ador(0).Value
        Ador.MoveNext
        m_sCondicion = Ador(0).Value '15
        Ador.MoveNext
        m_sNotificados = Ador(0).Value
        Ador.MoveNext
        m_sMensajesEnlace(2) = Ador(0).Value
        Ador.MoveNext
        m_sTipoCondicional = Ador(0).Value
        Ador.MoveNext
        m_sTipoObligatorio = Ador(0).Value
        Ador.MoveNext
        lblPrecondiciones.caption = Ador(0).Value '20
        Ador.MoveNext
        cmdSubirPrecondicion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdBajarPrecondicion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirPrecondicion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarPrecondicion.ToolTipText = Ador(0).Value
        Ador.MoveNext
        m_sIDPrecond = Ador(0).Value '25
        m_sMensajesPrecondicion(1) = Ador(0).Value
        Ador.MoveNext
        m_sTipoPrecond = Ador(0).Value
        m_sMensajesPrecondicion(2) = Ador(0).Value
        Ador.MoveNext
        m_sCondicionesPrecond = Ador(0).Value
        m_sMensajesPrecondicion(3) = Ador(0).Value
        Ador.MoveNext
        m_sMensajePrecond = Ador(0).Value
        m_sMensajesPrecondicion(4) = Ador(0).Value
        Ador.MoveNext
        lblNotificados.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirNotificado.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarNotificado.ToolTipText = Ador(0).Value
        Ador.MoveNext
        m_sTipoNotificado = Ador(0).Value
        m_sMensajesNotificado(1) = Ador(0).Value
        Ador.MoveNext
        m_sNotificado = Ador(0).Value
        m_sMensajesNotificado(2) = Ador(0).Value  '35
                
        Ador.MoveNext
        m_arTipoNotificado(1) = Ador(0).Value
        Ador.MoveNext
        m_arTipoNotificado(2) = Ador(0).Value
        Ador.MoveNext
        m_arTipoNotificado(3) = Ador(0).Value
        
                                        
        Ador.MoveNext
        m_arNotificadoGenerico(3) = Ador(0).Value
        Ador.MoveNext
        m_arNotificadoGenerico(4) = Ador(0).Value '40
        Ador.MoveNext
        m_arNotificadoGenerico(5) = Ador(0).Value
        Ador.MoveNext
        m_arNotificadoGenerico(6) = Ador(0).Value
        Ador.MoveNext
        m_arNotificadoGenerico(7) = Ador(0).Value
        
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        m_sMensajeDenominacion = Ador(0).Value
        
        Ador.MoveNext
        chkSoloRechazadas.caption = Ador(0).Value
        
        Ador.MoveNext
        m_arTipoPrecondicion(1) = Ador(0).Value
        Ador.MoveNext
        m_arTipoPrecondicion(2) = Ador(0).Value

        Ador.MoveNext
        chkIncrementarId.caption = Ador(0).Value
        
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value
        
        Ador.MoveNext
        m_sConfCampos = Ador(0).Value
        
        Ador.MoveNext
        lblSubject.caption = Ador(0).Value
        
        Ador.MoveNext
        ' Dejar uno vac�o para textos por defecto de emails
        
        Ador.MoveNext
        chkAprobar.caption = Ador(0).Value
        
        Ador.MoveNext
        chkRechazar.caption = Ador(0).Value
        
        Ador.MoveNext
        chkLlamadaExterna.caption = Ador(0).Value '55
        
        Ador.MoveNext
        'chkActualizarEstadoFactura.caption = Ador(0).Value '56
        
        Ador.MoveNext
        chkFicheroEFactura.caption = Ador(0).Value '57
                
        Ador.MoveNext
        chkNoAprobarSiDiscrepancias.caption = Ador(0).Value '58
        
        Ador.MoveNext
        m_sEstadoFactura = Ador(0).Value '59
        Ador.MoveNext
        m_arNotificadoGenerico(8) = Ador(0).Value
        Ador.Close
    End If
    Select Case iTipoSolicitud
        Case tipoSolicitud.Certificados, tipoSolicitud.NoConformidades
            prefijo = "FSQA: "
        Case tipoSolicitud.SolicitudDePedidoCatalogo, tipoSolicitud.SolicitudDePedidoContraPedidoAbierto
            prefijo = "FSEP: "
        Case tipoSolicitud.AUTOFACTURA, tipoSolicitud.Factura
            prefijo = "FSIM: "
        Case tipoSolicitud.Contrato
            prefijo = "FSCM: "
        Case Else 'OtrasSolicitudes/SolicitudCompras/Abono/Proyecto/Express/pedido/Encuesta/Solicitud QA
            prefijo = "FSPM: "
    End Select
End Sub

Private Sub ConfigurarSeguridad()
    Dim i As Integer
    
    If m_bModifFlujo Then
        If m_bSelAccion Then
            sdbgDen.Columns("VALOR_IDIOMA").Style = ssStyleComboBox
            sdbddAcciones.AddItem 0 & Chr(m_lSeparador) & ""
            sdbgDen.Columns("VALOR_IDIOMA").DropDownHwnd = sdbddAcciones.hWnd
        Else
            sdbgDen.Columns("VALOR_IDIOMA").Style = ssStyleEdit
        End If
        
        sdbddDestino.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgEtapasDestino.Columns("DESTINO").DropDownHwnd = sdbddDestino.hWnd
        sdbddTipoEnlace.AddItem ""
        sdbgEtapasDestino.Columns("TIPO").DropDownHwnd = sdbddTipoEnlace.hWnd
        
        sdbddTipoPrecondicion.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgPrecondiciones.Columns("TIPO_DEN").DropDownHwnd = sdbddTipoPrecondicion.hWnd
        
        sdbddTiposNotificado.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgNotificados.Columns("TIPO_DEN").DropDownHwnd = sdbddTiposNotificado.hWnd
        sdbddNotificados.AddItem "0" & Chr(m_lSeparador) & ""
        sdbgNotificados.Columns("NOTIFICADO").DropDownHwnd = sdbddNotificados.hWnd
        
        If iTipoSolicitud = tipoSolicitud.AUTOFACTURA Then
            sdbgEtapasDestino.Columns("EST_FACTURA").Visible = True
            sdbddEstadosFactura.AddItem 0 & Chr(m_lSeparador) & ""
            sdbgEtapasDestino.Columns("EST_FACTURA").DropDownHwnd = sdbddEstadosFactura.hWnd
        End If
    Else
        sdbgDen.Columns("VALOR_IDIOMA").Style = ssStyleEdit
        sdbgDen.Columns("VALOR_IDIOMA").Locked = True
        picCumpOblRol.Enabled = False
        picGuarda.Enabled = False
        picRechazoTemporal.Enabled = False
        picRechazoDefinitivo.Enabled = False
        picAnulacion.Enabled = False
        picSoloRechazadas.Enabled = False
        
        cmdAnyadirEnlace.Enabled = False
        cmdEliminarEnlace.Enabled = False
        sdbgEtapasDestino.AllowAddNew = False
        sdbgEtapasDestino.AllowDelete = False
        If iTipoSolicitud = tipoSolicitud.AUTOFACTURA Then
            sdbgEtapasDestino.Columns("EST_FACTURA").Visible = True
        End If
        For i = 0 To sdbgEtapasDestino.Cols - 1
            sdbgEtapasDestino.Columns(i).Locked = True
            If sdbgEtapasDestino.Columns(i).Style = ssStyleComboBox Then
                sdbgEtapasDestino.Columns(i).DropDownHwnd = 0
            End If
        Next
        
        cmdSubirPrecondicion.Enabled = False
        cmdBajarPrecondicion.Enabled = False
        cmdAnyadirPrecondicion.Enabled = False
        cmdEliminarPrecondicion.Enabled = False
        sdbgPrecondiciones.AllowAddNew = False
        sdbgPrecondiciones.AllowUpdate = False
        sdbgPrecondiciones.AllowDelete = False
        
        cmdAnyadirNotificado.Enabled = False
        cmdEliminarNotificado.Enabled = False
        sdbgNotificados.AllowAddNew = False
        sdbgNotificados.AllowUpdate = False
        sdbgNotificados.AllowDelete = False
                
        picEdit.Visible = False
    End If
End Sub

Private Sub CrearAccion()
    Dim teserror As TipoErrorSummit
    Dim oIdioma As CIdioma
    Dim oIdiomas As CIdiomas
    
    Set oAccion = oFSGSRaiz.Generar_CAccionBloque
    Set oAccion.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    Set oAccion.Subject = oFSGSRaiz.Generar_CMultiidiomas
    
    For Each oIdioma In oIdiomas
        oAccion.Denominaciones.Add oIdioma.Cod, ""
        oAccion.Subject.Add oIdioma.Cod, prefijo & oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSDETALLEACCION, oIdioma.Cod, 52)(0).Value
    Next
    oAccion.Bloque = lIdBloque_Orig
    oAccion.Guarda = True 'DEMO Ocaso / 2017 / 12: Si miras CargarAccion() veras q debido a "m_AccionSummit = ACCAccionBloqueAnya" marca el check de "La acci�n almacenar� los datos de la solicitud". Eso debe ir a bbdd.
    Set m_oIBaseDatos = oAccion
    teserror = m_oIBaseDatos.AnyadirABaseDatos
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Set m_oIBaseDatos = Nothing
        Set oAccion = Nothing
        Exit Sub
    Else
        basSeguridad.RegistrarAccion AccionesSummit.ACCAccionBloqueAnya, "ID:" & oAccion.Id
        Set m_oIBaseDatos = Nothing
        lIdAccion = oAccion.Id
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
End Sub


Private Sub CargarAccion()
    Dim oBloque As CBloque
    Dim oEnlace As CEnlace
    Dim teserror As TipoErrorSummit
    
    Set oEnlaces_Nuevos = oFSGSRaiz.Generar_CEnlaces
    Set oEnlaces_Modificados = oFSGSRaiz.Generar_CEnlaces
    Set oEnlaces_Eliminados = oFSGSRaiz.Generar_CEnlaces
    
    Set oAccion = oFSGSRaiz.Generar_CAccionBloque
    oAccion.Id = lIdAccion
    
    Set m_oIBaseDatos = oAccion
    teserror = m_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oAccion = Nothing
        Set m_oIBaseDatos = Nothing
        Exit Sub
    End If
    
    CargarDenominaciones
    
    CargarSubjects
    
    If oAccion.CumpOblRol Then
        chkCumpOblRol.Value = Checked
    Else
        chkCumpOblRol.Value = Unchecked
    End If
    
    Set oBloque = oFSGSRaiz.Generar_CBloque
    oBloque.Id = oAccion.Bloque
    
    Set m_oIBaseDatos = oBloque
    teserror = m_oIBaseDatos.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Set oBloque = Nothing
        Set m_oIBaseDatos = Nothing
        Exit Sub
    End If
    m_bBloquePeticionario = (oBloque.Tipo = TipoBloque.Peticionario)
    If m_bBloquePeticionario Then
        If Not oAccion.Guarda Then m_bCambiadoEnLoad = True
        oAccion.Guarda = True
        picGuarda.Enabled = False
        chkAprobar.Visible = False
    End If
    If iTipoSolicitud = tipoSolicitud.AUTOFACTURA Then
        chkNoAprobarSiDiscrepancias.Visible = True
        If oAccion.NoAprobarSiDiscrepancias Then
            chkNoAprobarSiDiscrepancias.Value = Checked
        End If
        
        chkFicheroEFactura.Visible = True
        If oAccion.FicheroEFactura Then
            chkFicheroEFactura.Value = Checked
        End If
    Else
        chkNoAprobarSiDiscrepancias.Visible = False
        chkFicheroEFactura.Visible = False
    End If
    If oAccion.idLlamadaExterna > 0 Then
        Me.chkLlamadaExterna.Value = Checked
    Else
        Me.chkLlamadaExterna.Value = Unchecked
    End If
    If oAccion.Guarda Or m_AccionSummit = ACCAccionBloqueAnya Then
        chkGuarda.Value = Checked
    Else
        chkGuarda.Value = Unchecked
    End If
    
    If oAccion.Tipo = TipoAccionBloque.Rechazo Then
        Select Case oAccion.TipoRechazo
            Case TipoRechazoAccionBloque.RechazoTemporal
                chkRechazoTemporal.Value = Checked
                chkRechazoDefinitivo.Value = Unchecked
                sdbgEtapasDestino.Enabled = True
                chkAnulacion.Value = Unchecked
            Case TipoRechazoAccionBloque.RechazoDefinitivo
                chkRechazoTemporal.Value = Unchecked
                chkRechazoDefinitivo.Value = Checked
                sdbgEtapasDestino.Enabled = False
                cmdAnyadirEnlace.Enabled = False
                cmdEliminarEnlace.Enabled = False
                chkAnulacion.Value = Unchecked
            Case TipoRechazoAccionBloque.Anulacion
                chkRechazoTemporal.Value = Unchecked
                chkRechazoDefinitivo.Value = Unchecked
                sdbgEtapasDestino.Enabled = False
                cmdAnyadirEnlace.Enabled = False
                cmdEliminarEnlace.Enabled = False
                chkAnulacion.Value = Checked
        End Select
    Else
        chkRechazoTemporal.Value = Unchecked
        chkRechazoDefinitivo.Value = Unchecked
        sdbgEtapasDestino.Enabled = True
        chkAnulacion.Value = Unchecked
    End If
    If oAccion.SoloRechazadas Then
        chkSoloRechazadas.Value = Checked
    Else
        chkSoloRechazadas.Value = Unchecked
    End If
    If oAccion.IncrementarId Then
        chkIncrementarId.Value = Checked
    Else
        chkIncrementarId.Value = Unchecked
    End If
    If oAccion.Aprobar And Not m_bBloquePeticionario Then chkAprobar.Value = Checked Else chkAprobar.Value = Unchecked
    If oAccion.Rechazar And Not m_bBloquePeticionario Then chkRechazar.Value = Checked Else chkRechazar.Value = Unchecked
    If lIdBloque_Dest > 0 Then
        Set oEnlace = oFSGSRaiz.Generar_CEnlace
        oEnlace.BloqueOrigen = oAccion.Bloque
        oEnlace.BloqueDestino = lIdBloque_Dest
        oEnlace.Accion = oAccion.Id
        Set oEnlace.DenAccion = oAccion.Denominaciones
        oEnlace.TipoAccion = oAccion.Tipo
        oEnlace.LlamadaExterna = oAccion.idLlamadaExterna > 0
        Set oEnlace.Subject = SubjectEnlacePorDefecto()
        Set m_oIBaseDatos = oEnlace
        m_oIBaseDatos.AnyadirABaseDatos
        
        oAccion.Enlaces.AddEnlace oEnlace
        oEnlaces_Nuevos.AddEnlace oEnlace
        Set oEnlace = Nothing
    End If
    Set m_oIBaseDatos = Nothing
    
    CargarDestinos
    CargarPrecondiciones
    CargarNotificados
End Sub

Private Sub CargarDenominaciones()
    Set m_oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
    Dim oIdiomas As CIdiomas
    
    'Que salgan solo los idiomas activos en la aplicaci�n. 3�par�metro a False
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , False)
    
    sdbgDen.RemoveAll
    For Each m_Idi In oAccion.Denominaciones
        m_oDenominaciones.Add m_Idi.Cod, m_Idi.Den
        sdbgDen.AddItem m_Idi.Cod & Chr(m_lSeparador) & oIdiomas.Item(m_Idi.Cod).Den & Chr(m_lSeparador) & m_Idi.Den
    Next
    Set oIdiomas = Nothing
End Sub

Private Sub CargarSubjects()
    Set m_oSubject = oFSGSRaiz.Generar_CMultiidiomas
    Dim oIdiomas As CIdiomas
    m_GridSubjectUpdated = False

    'Que salgan solo los idiomas activos en la aplicaci�n. 3�par�metro a False
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , False)
    
    sdbgSubject.RemoveAll
    For Each m_Idi In oAccion.Subject
        m_oSubject.Add m_Idi.Cod, m_Idi.Den
        sdbgSubject.AddItem m_Idi.Cod & Chr(m_lSeparador) & oIdiomas.Item(m_Idi.Cod).Den & Chr(m_lSeparador) & m_Idi.Den
    Next
    Set oIdiomas = Nothing
End Sub

Private Sub CargarDestinos()
    Dim oEnlace As CEnlace
    Dim sTipo As String
    Dim oBloque_Dest As CBloque
    Dim teserror As TipoErrorSummit
    Dim sBloque_Dest As String
    Dim sCondicion As String
    Dim oNotificados As CNotificadosAccion
    Dim sNotificados As String
    Dim sEstadoFactura As String
        
    sdbgEtapasDestino.RemoveAll
    For Each oEnlace In oAccion.Enlaces
        If oEnlaces_Nuevos.Item(CStr(oEnlace.Id)) Is Nothing Then
            oEnlaces_Modificados.AddEnlace oEnlace
        End If
            
        Set oBloque_Dest = oFSGSRaiz.Generar_CBloque
        oBloque_Dest.Id = oEnlace.BloqueDestino
        Set m_oIBaseDatos = oBloque_Dest
        teserror = m_oIBaseDatos.IniciarEdicion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set m_oIBaseDatos = Nothing
            Set oBloque_Dest = Nothing
            Exit Sub
        End If
        sBloque_Dest = oBloque_Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        
        If oEnlace.Formula <> "" Then
            sTipo = m_sTipoCondicional
            sCondicion = "..."
        Else
            sTipo = m_sTipoObligatorio
            sCondicion = " "
        End If
        
        Set oNotificados = oFSGSRaiz.Generar_CNotificadosAccion
        oNotificados.CargarNotificados OrigenNotificadoAccion.Enlace, oEnlace.Id
        If oNotificados.Count > 0 Then
            sNotificados = "..."
        Else
            sNotificados = ""
        End If
        
        sEstadoFactura = DevolverDenominacionEstadoFactura(oEnlace.EstadoFactura)
                
        sdbgEtapasDestino.AddItem oEnlace.Id & Chr(m_lSeparador) & oBloque_Dest.Id & Chr(m_lSeparador) & sBloque_Dest & Chr(m_lSeparador) & sTipo & Chr(m_lSeparador) & oEnlace.Formula & Chr(m_lSeparador) & sCondicion & Chr(m_lSeparador) & sNotificados & Chr(m_lSeparador) & oEnlace.FecAct & Chr(m_lSeparador) & oEnlace.EstadoFactura & Chr(m_lSeparador) & sEstadoFactura
    Next
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub CargarPrecondiciones()
    Dim oPrecondicion As CPrecondicion
    Dim oIdiomas As CIdiomas
    Dim oIdioma As CIdioma
    Dim sItem As String
    Dim iNewCol As Integer
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    If sdbgPrecondiciones.Columns.Count = 8 Then
        For Each oIdioma In oIdiomas
            iNewCol = sdbgPrecondiciones.Columns.Count
            sdbgPrecondiciones.Columns.Add iNewCol
            sdbgPrecondiciones.Columns(iNewCol).Name = "MENSAJE_" & oIdioma.Cod
            sdbgPrecondiciones.Columns(iNewCol).caption = m_sMensajePrecond & " (" & oIdioma.Den & ")"
            sdbgPrecondiciones.Columns(iNewCol).DataType = 8 'Text
            sdbgPrecondiciones.Columns(iNewCol).FieldLen = 500
        Next
    End If
        
    sdbgPrecondiciones.RemoveAll
    
    Set m_oPrecondiciones = oFSGSRaiz.Generar_CPrecondiciones
    m_oPrecondiciones.CargarPrecondiciones oIdiomas, oAccion.Id
    For Each oPrecondicion In m_oPrecondiciones
        sItem = oPrecondicion.Id & Chr(m_lSeparador) & oPrecondicion.Orden & Chr(m_lSeparador) & oPrecondicion.Codigo & Chr(m_lSeparador) & oPrecondicion.TipoPrecondicion & Chr(m_lSeparador) & m_arTipoPrecondicion(oPrecondicion.TipoPrecondicion) & Chr(m_lSeparador) & oPrecondicion.Formula
        If oPrecondicion.Formula <> "" Then
            sItem = sItem & Chr(m_lSeparador) & "..."
        Else
            sItem = sItem & Chr(m_lSeparador) & ""
        End If
        sItem = sItem & Chr(m_lSeparador) & oPrecondicion.FecAct
        For Each oIdioma In oIdiomas
            sItem = sItem & Chr(m_lSeparador) & oPrecondicion.Denominaciones.Item(oIdioma.Cod).Den
        Next
        sdbgPrecondiciones.AddItem sItem
    Next
    
    Set oIdioma = Nothing
    Set oIdiomas = Nothing
    Set oPrecondicion = Nothing
End Sub

Private Sub CargarNotificados()
    Dim oNotificado As CNotificadoAccion
    Dim oPersona As CPersona
    Dim oRol As CPMRol
    
    
    Dim iTipo As TipoNotificadoAccion
    Dim sCodNotificado As String
    Dim sDenNotificado As String
    
    sdbgNotificados.RemoveAll
    
    Set m_oNotificados = oFSGSRaiz.Generar_CNotificadosAccion
    m_oNotificados.CargarNotificados OrigenNotificadoAccion.Accion, oAccion.Id
    For Each oNotificado In m_oNotificados
        Select Case oNotificado.Tipo
            Case TipoNotificadoAccion.Especifico
                iTipo = TipoNotificadoAccion.Especifico
                
                Set oPersona = oFSGSRaiz.Generar_CPersona
                oPersona.Cod = oNotificado.Per
                oPersona.CargarTodosLosDatos
                
                sCodNotificado = oPersona.Cod
                sDenNotificado = oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
            Case TipoNotificadoAccion.Rol
                iTipo = TipoNotificadoAccion.Rol
                
                Set oRol = oFSGSRaiz.Generar_CPMRol
                oRol.Id = oNotificado.Rol
                Set m_oIBaseDatos = oRol
                m_oIBaseDatos.IniciarEdicion
                
                sCodNotificado = oRol.Id
                sDenNotificado = oRol.Den
            Case Else 'Generico
                iTipo = TipoNotificadoAccion.Peticionario 'Que realmente indica que es GENERICO
                sCodNotificado = CStr(oNotificado.Tipo)
                sDenNotificado = m_arNotificadoGenerico(oNotificado.Tipo)
        End Select
               
        sdbgNotificados.AddItem oNotificado.Id & Chr(m_lSeparador) & iTipo & Chr(m_lSeparador) & m_arTipoNotificado(iTipo) & Chr(m_lSeparador) & sCodNotificado & Chr(m_lSeparador) & sDenNotificado
    Next
    
    Set oNotificado = Nothing
    Set oPersona = Nothing
    Set oRol = Nothing
    Set m_oIBaseDatos = Nothing
End Sub

'Recalcula las posiciones de todos los objetos del formulario
Private Sub Form_Resize()
    Dim alturaGrid As Single
    Dim oIdiomas As CIdiomas
    Dim oIdioma As CIdioma
    Dim iNewColWidth As Single
    Dim iNumIdiomas As Integer
    
    If Me.Width < 12060 Or Me.Height < 9580 Then Exit Sub
    
    'Establezco el ancho de los Elementos
    Line1.X2 = Me.Width - 300
    Line2.X2 = Me.Width - 300
    Line3.X2 = Me.Width - 300
    
    sdbgEtapasDestino.Width = Me.Width - 945
    sdbgEtapasDestino.Columns("DESTINO").Width = sdbgEtapasDestino.Width - sdbgEtapasDestino.Columns("TIPO").Width - sdbgEtapasDestino.Columns("CONDICION").Width - sdbgEtapasDestino.Columns("NOTIFICADOS").Width - 340
    If iTipoSolicitud = tipoSolicitud.AUTOFACTURA Then
        sdbgEtapasDestino.Columns("DESTINO").Width = sdbgEtapasDestino.Columns("DESTINO").Width - sdbgEtapasDestino.Columns("EST_FACTURA").Width
        sdbgEtapasDestino.Columns("TIPO").Width = sdbgEtapasDestino.Columns("TIPO").Width - 230
    End If
    sdbddDestino.Columns("DESTINO").Width = sdbgEtapasDestino.Columns("DESTINO").Width
    sdbddEstadosFactura.Columns(1).Width = sdbgEtapasDestino.Columns("EST_FACTURA").Width
    
    sdbgPrecondiciones.Width = Me.Width - 945
    sdbgPrecondiciones.Columns("COD").Width = 800
    sdbgPrecondiciones.Columns("TIPO_DEN").Width = 2000
    sdbgPrecondiciones.Columns("CONDICIONES").Width = 1500
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    iNewColWidth = (sdbgPrecondiciones.Width - 4650) / oIdiomas.Count
    For Each oIdioma In oIdiomas
        If Not sdbgPrecondiciones.Columns("MENSAJE_" & oIdioma.Cod) Is Nothing Then
            sdbgPrecondiciones.Columns("MENSAJE_" & oIdioma.Cod).Width = iNewColWidth
        End If
    Next
        
    sdbgNotificados.Width = Me.Width - 945
    sdbgNotificados.Columns("NOTIFICADO").Width = sdbgNotificados.Width - sdbgNotificados.Columns("TIPO_DEN").Width - 2340
    sdbddNotificados.Columns("NOTIFICADO").Width = sdbgNotificados.Columns("NOTIFICADO").Width
    sdbgNotificados.Columns("CONFCAMPOS").Width = 2000
    
    'Muevo los botones de aceptar y cancelar
    picEdit.Left = (Me.Width / 2) - (picEdit.Width / 2)
    picEdit.Top = Me.Height - 900
    
    '''Calculo de altura de Gid:
    '''Altura del Formulario
    '''menos
    '''posicion del 1� Grid
    '''menos
    '''Posicion donde debe quedar el final del ultimo grid (justo encima de los botones Aceptar y Cancelar)
    '''menos
    '''un coeficiente de correcci�n por cada Gird
    alturaGrid = (Me.ScaleHeight - sdbgEtapasDestino.Top - 1525 - (195 * 3)) / 3
    
    'Muevo en altura los elementos de Etapas Destino
    sdbgEtapasDestino.Height = alturaGrid - 200
    

    'Muevo en altura los elementos de Precondiciones
    Line2.Y1 = sdbgEtapasDestino.Top + sdbgEtapasDestino.Height + 105
    Line2.Y2 = Line2.Y1
    lblPrecondiciones.Top = Line2.Y1 + 120
    cmdSubirPrecondicion.Top = lblPrecondiciones.Top + lblPrecondiciones.Height + 45
    cmdBajarPrecondicion.Top = cmdSubirPrecondicion.Top + cmdSubirPrecondicion.Height + 42
    cmdAnyadirPrecondicion.Top = cmdBajarPrecondicion.Top + cmdBajarPrecondicion.Height + 42
    cmdEliminarPrecondicion.Top = cmdAnyadirPrecondicion.Top + cmdAnyadirPrecondicion.Height + 42
    sdbgPrecondiciones.Top = lblPrecondiciones.Top + lblPrecondiciones.Height + 45
    sdbgPrecondiciones.Height = alturaGrid + 200
    
    'Muevo en altura los elementos de Notificados
    Line3.Y1 = sdbgPrecondiciones.Top + sdbgPrecondiciones.Height + 105
    Line3.Y2 = Line3.Y1
    lblNotificados.Top = Line3.Y1 + 120
    cmdAnyadirNotificado.Top = lblNotificados.Top + lblNotificados.Height + 45
    cmdEliminarNotificado.Top = cmdAnyadirNotificado.Top + cmdAnyadirNotificado.Height + 42
    sdbgNotificados.Top = lblNotificados.Top + lblNotificados.Height + 45
    sdbgNotificados.Height = alturaGrid - 200
    
    'Se mostrar� scroll vertical solo si hay m�s de 4 idiomas
    iNumIdiomas = oIdiomas.Count
    
    sdbgDen.ScrollBars = 0      '0-ssScrollBarsNone
    sdbgSubject.ScrollBars = 0
    If iNumIdiomas > 4 Then
    sdbgDen.ScrollBars = 2      '2-ssScrollBarsVertical
    sdbgSubject.ScrollBars = 2
    End If
    
    
    ' Altura de los elementos de subject de emails
    lblSubject.Top = Me.Height - 1690
    sdbgSubject.Top = lblSubject.Top - 60
    
    picEdit.Top = sdbgSubject.Top + sdbgSubject.Height + 35

End Sub

''' <summary>
''' Graba lo q quede por grabar, impide el cierre si falla alguna grabaci�n y descarga las variables de pantalla.
''' </summary>
''' <param name="Cancel">Cancelar el cierre de pantalla</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Dim v As Variant
    Dim vbm As Variant
    Dim j As Integer
    Dim oRS As ADODB.Recordset
    Dim m_oCondiciones As CCondicionesEnlace
    
    m_bSalir = True
    If sdbgEtapasDestino.DataChanged = True Then
        v = sdbgEtapasDestino.ActiveCell.Value
        If sdbgEtapasDestino.Enabled And Me.Visible Then sdbgEtapasDestino.SetFocus
        sdbgEtapasDestino.ActiveCell.Value = v
    
        If (actualizarYSalirEtapasDestino = True) Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    For j = 0 To sdbgEtapasDestino.Rows - 1
        vbm = sdbgEtapasDestino.AddItemBookmark(j)
        If Not NoHayParametro(sdbgEtapasDestino.Columns("ID").CellValue(vbm)) And sdbgEtapasDestino.Columns("TIPO").CellValue(vbm) = m_sTipoCondicional Then
            Set m_oCondiciones = oFSGSRaiz.Generar_CCondicionesEnlace
            Set oRS = m_oCondiciones.DevolverCondiciones(sdbgEtapasDestino.Columns("ID").CellValue(vbm))

            If oRS.EOF Then
                oMensajes.NoValido m_sMensajesPrecondicion(3)
                If Me.Visible Then sdbgEtapasDestino.SetFocus
                sdbgEtapasDestino.Bookmark = vbm
                Set m_oCondiciones = Nothing
                Set oRS = Nothing
                Cancel = True
                Exit Sub
            End If
        End If
    Next j
    
    Set m_oCondiciones = Nothing
    Set oRS = Nothing
    
    If sdbgPrecondiciones.DataChanged = True Then
    
        v = sdbgPrecondiciones.ActiveCell.Value
        If Me.Visible Then sdbgPrecondiciones.SetFocus
        sdbgPrecondiciones.ActiveCell.Value = v
        
        If (actualizarYSalirPrecondiciones = True) Then
            Cancel = True
            Exit Sub
        End If
        
    End If
    
    For j = 0 To sdbgPrecondiciones.Rows - 1
        vbm = sdbgPrecondiciones.AddItemBookmark(j)
        If Not NoHayParametro(sdbgPrecondiciones.Columns("ID").CellValue(vbm)) Then
            Set m_oCondiciones = oFSGSRaiz.Generar_CCondicionesEnlace
            Set oRS = m_oCondiciones.DevolverCondicionesPrecondicion(sdbgPrecondiciones.Columns("ID").CellValue(vbm))

            If oRS.EOF Then
                oMensajes.NoValido m_sMensajesPrecondicion(3)
                If Me.Visible Then sdbgPrecondiciones.SetFocus
                sdbgPrecondiciones.Bookmark = vbm
                Set m_oCondiciones = Nothing
                Set oRS = Nothing
                Cancel = True
                Exit Sub
            End If
        End If
    Next j
    
    Set m_oCondiciones = Nothing
    Set oRS = Nothing
    
    If sdbgNotificados.DataChanged = True Then
    
        v = sdbgNotificados.ActiveCell.Value
        If Me.Visible Then sdbgNotificados.SetFocus
        sdbgNotificados.ActiveCell.Value = v
        
        If (actualizarYSalirNotificados = True) Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    If sdbgDen.DataChanged Then
        sdbgDen.Update
        Set oAccion.Denominaciones = m_oDenominaciones
        oAccion.ModificarAccion_GrabarDenominacion
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
    
    If sdbgSubject.DataChanged Or m_GridSubjectUpdated Then
        sdbgSubject.Update
        Set oAccion.Subject = m_oSubject
        oAccion.ModificarAccion_GrabarSubject
    End If
    
    If oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den = "" Then
        If sdbgEtapasDestino.Rows > 0 Or sdbgPrecondiciones.Rows > 0 Or sdbgNotificados.Rows > 0 Then
            oMensajes.NoValido m_sMensajeDenominacion & " " & oGestorParametros.DevolverIdiomas.Item(basPublic.gParametrosInstalacion.gIdioma).Den
            Cancel = True
            Exit Sub
        End If
    End If
    
    'Si selecciona que la acci�n tendra una llamada externa tendra que seleccionarla
    If Me.chkLlamadaExterna.Value = 1 And Me.sdbcAccionesExternas.Value = "" Then
        oMensajes.MensajeLlamadaExternaDeAccion 1214
        Cancel = True
        Exit Sub
    End If
    
    lIdFlujo = 0
    lIdAccion = 0
    
    If oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den = "" And sdbgEtapasDestino.Rows = 0 And sdbgPrecondiciones.Rows = 0 And sdbgNotificados.Rows = 0 Then
        Call EliminarAccion
    End If
    
    Set oAccion = Nothing
    m_AccionSummit = 0
    
    Set m_oIBaseDatos = Nothing

    Set m_oEnlaceEnEdicion = Nothing
    Set m_oEnlaceAnyadir = Nothing
    Set m_oIBaseDatosEnlaceEnEdicion = Nothing
    
    Set m_oPrecondicionEnEdicion = Nothing
    Set m_oPrecondicionAnyadir = Nothing
    Set m_oIBaseDatosPrecondicionEnEdicion = Nothing
    Set m_oPrecondiciones = Nothing
    
    Set m_oNotificadoEnEdicion = Nothing
    Set m_oNotificadoAnyadir = Nothing
    Set m_oIBaseDatosNotificadoEnEdicion = Nothing
    Set m_oNotificados = Nothing
            
    Set m_oFormulario = Nothing
    lIdBloque_Dest = 0
    
    m_bSelAccion = False
End Sub


''' <summary>
''' evento que salta cuando se cambia de valor en el combo de Acciones externas y que graba la llamada elegida
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde:frmFlujosDetalleAccion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub sdbcAccionesExternas_CloseUp()
    
    If Me.sdbcAccionesExternas.Columns(0).Value <> "" Then
        oAccion.idLlamadaExterna = sdbcAccionesExternas.Columns(0).Value
        oAccion.ModificarAccion_GrabarLlamadaExterna
        
    Else
        oAccion.idLlamadaExterna = 0
        oAccion.ModificarAccion_GrabarLlamadaExterna
    End If
End Sub
''' <summary>
''' evento que inicializa el combo
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde:frmFlujosDetalleAccion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub sdbcAccionesExternas_InitColumnProps()
    sdbcAccionesExternas.DataFieldList = "Column 0"
    sdbcAccionesExternas.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddAcciones_CloseUp()
    Dim teserror As TipoErrorSummit
    
    If CLng(sdbddAcciones.Columns(0).Value) <> oAccion.Id Then
        If m_AccionSummit = ACCAccionBloqueAnya Then
            Set m_oIBaseDatos = oAccion
            teserror = m_oIBaseDatos.EliminarDeBaseDatos
            If teserror.NumError <> TESnoerror Then
                Set m_oIBaseDatos = Nothing
                Exit Sub
            Else
                basSeguridad.RegistrarAccion AccionesSummit.ACCAccionBloqueElim, "ID:" & oAccion.Id
            End If
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
            Set oAccion = Nothing
            Set oEnlaces_Nuevos = oFSGSRaiz.Generar_CEnlaces
            Set oEnlaces_Modificados = oFSGSRaiz.Generar_CEnlaces
            Set oEnlaces_Eliminados = oFSGSRaiz.Generar_CEnlaces
        Else
            GuardarAccion
        End If
        lIdAccion = CLng(sdbddAcciones.Columns(0).Value)
        CargarAccion
        m_AccionSummit = ACCAccionBloqueModif
    End If
End Sub

Private Sub sdbddAcciones_DropDown()
    Dim oAcciones As CAccionesBloque
    Dim oAcc As CAccionBloque
    
    sdbddAcciones.RemoveAll
    Set oAcciones = oFSGSRaiz.Generar_CAccionesBloque
    oAcciones.CargarAcciones sdbgDen.Columns("COD_IDIOMA").Value, lIdBloque_Orig
    For Each oAcc In oAcciones
        If lIdAccion = oAcc.Id Then
            sdbddAcciones.AddItem oAcc.Id & Chr(m_lSeparador) & sdbgDen.Columns("VALOR_IDIOMA").Value
        Else
            sdbddAcciones.AddItem oAcc.Id & Chr(m_lSeparador) & oAcc.Denominaciones.Item(sdbgDen.Columns("COD_IDIOMA").Value).Den
        End If
    Next
    If sdbddAcciones.Rows = 0 Then
        sdbddAcciones.AddItem 0 & Chr(m_lSeparador) & ""
    End If
End Sub

Private Sub sdbddAcciones_InitColumnProps()
    sdbddAcciones.DataFieldList = "Column 0"
    sdbddAcciones.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddAcciones_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddAcciones.MoveFirst

    If sdbgDen.Columns("DESTINO").Value <> "" Then
        For i = 0 To sdbddAcciones.Rows - 1
            bm = sdbddAcciones.GetBookmark(i)
            If UCase(sdbgDen.Columns("DESTINO").Value) = UCase(Mid(sdbddAcciones.Columns(1).CellText(bm), 1, Len(sdbgDen.Columns("DESTINO").Value))) Then
                sdbgDen.Columns("DESTINO").Value = Mid(sdbddAcciones.Columns(1).CellText(bm), 1, Len(sdbgDen.Columns("DESTINO").Value))
                sdbddAcciones.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub sdbddDestino_CloseUp()
    If sdbgEtapasDestino.Columns("DESTINO").Value = "" Then Exit Sub

    sdbgEtapasDestino.Columns("ID_DESTINO").Value = sdbddDestino.Columns(0).Value
    sdbgEtapasDestino.Columns("DESTINO").Value = sdbddDestino.Columns(1).Value

End Sub

Private Sub sdbddDestino_DropDown()
    Dim oBloques As CBloques
    Dim oBloque As CBloque
    Dim sDenBloque As String
    
    Screen.MousePointer = vbHourglass
    sdbddDestino.RemoveAll

    Set oBloques = oFSGSRaiz.Generar_CBloques
    oBloques.CargarBloques basPublic.gParametrosInstalacion.gIdioma, lIdFlujo
    For Each oBloque In oBloques
        sDenBloque = oBloque.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbddDestino.AddItem oBloque.Id & Chr(m_lSeparador) & sDenBloque
    Next
    Set oBloque = Nothing
    Set oBloques = Nothing

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddDestino_InitColumnProps()
    sdbddDestino.DataFieldList = "Column 0"
    sdbddDestino.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddDestino_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddDestino.MoveFirst

    If sdbgEtapasDestino.Columns("DESTINO").Value <> "" Then
        For i = 0 To sdbddDestino.Rows - 1
            bm = sdbddDestino.GetBookmark(i)
            If UCase(sdbgEtapasDestino.Columns("DESTINO").Value) = UCase(Mid(sdbddDestino.Columns(1).CellText(bm), 1, Len(sdbgEtapasDestino.Columns("DESTINO").Value))) Then
                sdbgEtapasDestino.Columns("DESTINO").Value = Mid(sdbddDestino.Columns(1).CellText(bm), 1, Len(sdbgEtapasDestino.Columns("DESTINO").Value))
                sdbddDestino.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub sdbddEstadosFactura_CloseUp()
    If sdbgEtapasDestino.Columns("EST_FACTURA").Value = "" Then Exit Sub

    sdbgEtapasDestino.Columns("ID_EST_FACTURA").Value = sdbddEstadosFactura.Columns(0).Value
    sdbgEtapasDestino.Columns("EST_FACTURA").Value = sdbddEstadosFactura.Columns(1).Value

End Sub

Private Sub sdbddEstadosFactura_DropDown()
    Dim rs As Recordset
    Dim m_oFacturas As CFacturas
    
    Set m_oFacturas = oFSGSRaiz.Generar_CFacturas
    Set rs = m_oFacturas.devolverTipoEstados(basPublic.gParametrosInstalacion.gIdioma)
    
    Screen.MousePointer = vbHourglass
    sdbddEstadosFactura.RemoveAll
    
    While Not rs.EOF
        sdbddEstadosFactura.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value
        rs.MoveNext
    Wend

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddEstadosFactura_InitColumnProps()
    sdbddEstadosFactura.DataFieldList = "Column 0"
    sdbddEstadosFactura.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddEstadosFactura_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddEstadosFactura.MoveFirst

    If sdbgEtapasDestino.Columns("EST_FACTURA").Value <> "" Then
        For i = 0 To sdbddEstadosFactura.Rows - 1
            bm = sdbddEstadosFactura.GetBookmark(i)
            If UCase(sdbgEtapasDestino.Columns("EST_FACTURA").Value) = UCase(Mid(sdbddEstadosFactura.Columns(1).CellText(bm), 1, Len(sdbgEtapasDestino.Columns("EST_FACTURA").Value))) Then
                sdbgEtapasDestino.Columns("EST_FACTURA").Value = Mid(sdbddEstadosFactura.Columns(1).CellText(bm), 1, Len(sdbgEtapasDestino.Columns("EST_FACTURA").Value))
                sdbddEstadosFactura.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Function DevolverDenominacionEstadoFactura(ByVal iEstadoFactura As Integer)
    Dim rs As Recordset
    Dim m_oFacturas As CFacturas
    Dim sEstadoFactura As String
    sEstadoFactura = ""
    
    Set m_oFacturas = oFSGSRaiz.Generar_CFacturas
    Set rs = m_oFacturas.devolverTipoEstados(basPublic.gParametrosInstalacion.gIdioma)
       
    Do While Not rs.EOF
        If iEstadoFactura = rs("ID").Value Then
            sEstadoFactura = rs("DEN").Value
            Exit Do
        End If
        rs.MoveNext
    Loop
    DevolverDenominacionEstadoFactura = sEstadoFactura
End Function

Private Sub sdbddNotificados_CloseUp()
    
    
    Dim teserror As TipoErrorSummit
    
    If sdbgNotificados.Columns("NOTIFICADO").Value = "" Then Exit Sub
    
    sdbgNotificados.Columns("COD_NOTIFICADO").Value = sdbddNotificados.Columns(0).Value
    sdbgNotificados.Columns("NOTIFICADO").Value = sdbddNotificados.Columns(1).Value
    
    If Not m_oNotificadoEnEdicion Is Nothing Then
        m_oNotificadoEnEdicion.Configuracion_Rol = False
        m_oNotificadoEnEdicion.CambioDeRoloTipoNotificado sdbgNotificados.Columns("ID").Value, OrigenNotificadoAccion.Accion
        Set m_oIBaseDatosNotificadoEnEdicion = m_oNotificadoEnEdicion
        teserror = m_oIBaseDatosNotificadoEnEdicion.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            If Me.Visible Then sdbgNotificados.SetFocus
            sdbgNotificados.DataChanged = False
        Else
            ''' Registro de acciones
            sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FecAct
            basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionModif, "ID:" & m_oNotificadoEnEdicion.Id
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
End Sub

Private Sub sdbddNotificados_DropDown()
    Dim oPersonas As CPersonas
    Dim oPersona As CPersona
    Dim oRoles As CPMRoles
    Dim oRol As CPMRol
    Dim i As Integer
    Dim bAnyadido As Boolean

    Screen.MousePointer = vbHourglass
        
    If sdbgNotificados.Columns("TIPO").Value <> "" Then
        sdbddNotificados.RemoveAll
        bAnyadido = False
        Select Case CInt(sdbgNotificados.Columns("TIPO").Value)
            Case TipoNotificadoAccion.Especifico
                Set oPersonas = oFSGSRaiz.Generar_CPersonas
                oPersonas.CargarTodasLasPersonas , , , , 4, , True, True, , , , True
                For Each oPersona In oPersonas
                    sdbddNotificados.AddItem oPersona.Cod & Chr(m_lSeparador) & oPersona.nombre & " " & oPersona.Apellidos & " (" & oPersona.mail & ")"
                    bAnyadido = True
                Next
            Case TipoNotificadoAccion.Rol
                Set oRoles = oFSGSRaiz.Generar_CPMRoles
                oRoles.CargarRolesWorkflow lIdFlujo
                For Each oRol In oRoles
                    sdbddNotificados.AddItem oRol.Id & Chr(m_lSeparador) & oRol.Den
                    bAnyadido = True
                Next
            Case TipoNotificadoAccion.Peticionario 'que en realidad se refiere a 'GENERICO' como concepto global
                For i = TipoNotificadoAccion.Peticionario To TipoNotificadoAccion.TNAcomprador
                    If iTipoSolicitud = tipoSolicitud.NoConformidades Then
                        If i >= TipoNotificadoAccion.Peticionario And i <= TipoNotificadoAccion.TodosLosAprobadores Then
                            sdbddNotificados.AddItem i & Chr(m_lSeparador) & m_arNotificadoGenerico(i)
                            bAnyadido = True
                        End If
                    Else
                        If i <> TipoNotificadoAccion.TNAcomprador Or Formulario.ExisteCampoGSFormulario(CodComprador) Then
                            sdbddNotificados.AddItem i & Chr(m_lSeparador) & m_arNotificadoGenerico(i)
                            bAnyadido = True
                        End If
                    End If
                Next
        End Select
    End If
    
    If Not bAnyadido Then
        sdbddNotificados.AddItem "" & Chr(m_lSeparador) & ""
    End If
        
    Set oPersonas = Nothing
    Set oPersona = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddNotificados_InitColumnProps()
    sdbddNotificados.DataFieldList = "Column 0"
    sdbddNotificados.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddNotificados_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddNotificados.MoveFirst

    If sdbgNotificados.Columns("NOTIFICADO").Value <> "" Then
        For i = 0 To sdbddNotificados.Rows - 1
            bm = sdbddNotificados.GetBookmark(i)
            If UCase(sdbgNotificados.Columns("NOTIFICADO").Value) = UCase(Mid(sdbddNotificados.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("NOTIFICADO").Value))) Then
                sdbgNotificados.Columns("NOTIFICADO").Value = Mid(sdbddNotificados.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("NOTIFICADO").Value))
                sdbddNotificados.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub sdbddTipoEnlace_CloseUp()
    If sdbgEtapasDestino.Columns("TIPO").Value = "" Then Exit Sub

    sdbgEtapasDestino.Columns("TIPO").Value = sdbddTipoEnlace.Columns(0).Value
        
End Sub

Private Sub sdbddTipoEnlace_DropDown()
    
    Screen.MousePointer = vbHourglass
    sdbddTipoEnlace.RemoveAll

    sdbddTipoEnlace.AddItem m_sTipoCondicional
    sdbddTipoEnlace.AddItem m_sTipoObligatorio
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddTipoEnlace_InitColumnProps()
    sdbddTipoEnlace.DataFieldList = "Column 0"
    sdbddTipoEnlace.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddTipoEnlace_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipoEnlace.MoveFirst

    If sdbddTipoEnlace.Columns("TIPO").Value <> "" Then
        For i = 0 To sdbddTipoEnlace.Rows - 1
            bm = sdbddTipoEnlace.GetBookmark(i)
            If UCase(sdbgEtapasDestino.Columns("TIPO").Value) = UCase(Mid(sdbddTipoEnlace.Columns(0).CellText(bm), 1, Len(sdbgEtapasDestino.Columns("TIPO").Value))) Then
                sdbgEtapasDestino.Columns("TIPO").Value = Mid(sdbddTipoEnlace.Columns(1).CellText(bm), 1, Len(sdbgEtapasDestino.Columns("TIPO").Value))
                sdbddTipoEnlace.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddTipoPrecondicion_CloseUp()
    If sdbgPrecondiciones.Columns("TIPO_DEN").Value = "" Then Exit Sub

    sdbgPrecondiciones.Columns("TIPO").Value = sdbddTipoPrecondicion.Columns(0).Value
    sdbgPrecondiciones.Columns("TIPO_DEN").Value = sdbddTipoPrecondicion.Columns(1).Value
        
End Sub

Private Sub sdbddTipoPrecondicion_DropDown()
    
    Screen.MousePointer = vbHourglass
    sdbddTipoPrecondicion.RemoveAll

    sdbddTipoPrecondicion.AddItem 1 & Chr(m_lSeparador) & m_arTipoPrecondicion(1)
    sdbddTipoPrecondicion.AddItem 2 & Chr(m_lSeparador) & m_arTipoPrecondicion(2)
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddTipoPrecondicion_InitColumnProps()
    sdbddTipoPrecondicion.DataFieldList = "Column 0"
    sdbddTipoPrecondicion.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddTipoPrecondicion_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipoPrecondicion.MoveFirst

    If sdbddTipoPrecondicion.Columns("TIPO_DEN").Value <> "" Then
        For i = 0 To sdbddTipoPrecondicion.Rows - 1
            bm = sdbddTipoPrecondicion.GetBookmark(i)
            If UCase(sdbgPrecondiciones.Columns("TIPO_DEN").Value) = UCase(Mid(sdbddTipoPrecondicion.Columns(0).CellText(bm), 1, Len(sdbgPrecondiciones.Columns("TIPO_DEN").Value))) Then
                sdbgPrecondiciones.Columns("TIPO_DEN").Value = Mid(sdbddTipoPrecondicion.Columns(1).CellText(bm), 1, Len(sdbgPrecondiciones.Columns("TIPO_DEN").Value))
                sdbddTipoPrecondicion.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddTiposNotificado_CloseUp()
    Dim teserror As TipoErrorSummit
    
    If sdbgNotificados.Columns("TIPO_DEN").Value = "" Then Exit Sub
    
    sdbgNotificados.Columns("TIPO").Value = sdbddTiposNotificado.Columns(0).Value
    sdbgNotificados.Columns("TIPO_DEN").Value = sdbddTiposNotificado.Columns(1).Value
    
    If Not m_oNotificadoEnEdicion Is Nothing Then
        m_oNotificadoEnEdicion.Configuracion_Rol = False
        m_oNotificadoEnEdicion.CambioDeRoloTipoNotificado sdbgNotificados.Columns("ID").Value, OrigenNotificadoAccion.Accion
        Set m_oIBaseDatosNotificadoEnEdicion = m_oNotificadoEnEdicion
        teserror = m_oIBaseDatosNotificadoEnEdicion.FinalizarEdicionModificando
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            If Me.Visible Then sdbgNotificados.SetFocus
            sdbgNotificados.DataChanged = False
        Else
            ''' Registro de acciones
            sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FecAct
            basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionModif, "ID:" & m_oNotificadoEnEdicion.Id
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
    End If
End Sub

Private Sub sdbddTiposNotificado_DropDown()
    
    Screen.MousePointer = vbHourglass
    sdbddTiposNotificado.RemoveAll
    
    sdbddTiposNotificado.AddItem TipoNotificadoAccion.Especifico & Chr(m_lSeparador) & m_arTipoNotificado(TipoNotificadoAccion.Especifico)
    sdbddTiposNotificado.AddItem TipoNotificadoAccion.Rol & Chr(m_lSeparador) & m_arTipoNotificado(TipoNotificadoAccion.Rol)
    sdbddTiposNotificado.AddItem TipoNotificadoAccion.Peticionario & Chr(m_lSeparador) & m_arTipoNotificado(TipoNotificadoAccion.Peticionario) 'En realidad se est� metiendo el texto "Generico"

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddTiposNotificado_InitColumnProps()
    sdbddTiposNotificado.DataFieldList = "Column 0"
    sdbddTiposNotificado.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddTiposNotificado_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTiposNotificado.MoveFirst

    If sdbgNotificados.Columns("TIPO_DEN").Value <> "" Then
        For i = 0 To sdbddTiposNotificado.Rows - 1
            bm = sdbddTiposNotificado.GetBookmark(i)
            If UCase(sdbgNotificados.Columns("TIPO_DEN").Value) = UCase(Mid(sdbddTiposNotificado.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("TIPO_DEN").Value))) Then
                sdbgNotificados.Columns("TIPO_DEN").Value = Mid(sdbddTiposNotificado.Columns(1).CellText(bm), 1, Len(sdbgNotificados.Columns("TIPO_DEN").Value))
                sdbddTiposNotificado.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgDen_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

Private Sub sdbgDen_BeforeUpdate(Cancel As Integer)
    'Dim oenlace As CEnlace
    
    m_oDenominaciones.Item(sdbgDen.Columns("COD_IDIOMA").Value).Den = sdbgDen.Columns("VALOR_IDIOMA").Value
    'For Each oenlace In oAccion.Enlaces
    '    Set oenlace.DenAccion = oAccion.Denominaciones
    'Next
End Sub

Private Sub sdbgDen_LostFocus()
    If sdbgDen.DataChanged Then
        sdbgDen.Update
        
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
    
    Set oAccion.Denominaciones = m_oDenominaciones
    oAccion.ModificarAccion_GrabarDenominacion
    
End Sub

Private Sub sdbgEtapasDestino_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgEtapasDestino.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgEtapasDestino.GetBookmark(0)) Then
        sdbgEtapasDestino.Bookmark = sdbgEtapasDestino.GetBookmark(-1)
    Else
        sdbgEtapasDestino.Bookmark = sdbgEtapasDestino.GetBookmark(0)
    End If
    If Me.Visible Then sdbgEtapasDestino.SetFocus
End Sub

Private Sub sdbgEtapasDestino_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
        
    If IsEmpty(sdbgEtapasDestino.GetBookmark(0)) Then
        sdbgEtapasDestino.Bookmark = sdbgEtapasDestino.GetBookmark(-1)
    Else
        sdbgEtapasDestino.Bookmark = sdbgEtapasDestino.GetBookmark(0)
    End If
End Sub

Private Sub sdbgEtapasDestino_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
       
    If Not m_oEnlaceEnEdicion Is Nothing Then
        sdbgEtapasDestino.Columns("FECACT").Value = m_oEnlaceEnEdicion.FecAct
    End If
    
    Set m_oEnlaceEnEdicion = Nothing
End Sub

Private Sub sdbgEtapasDestino_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgEtapasDestino_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    Dim vbm As Variant
       
    'Comprobar datos Obligatorios
    If sdbgEtapasDestino.Columns("DESTINO").Value = "" Then
        oMensajes.NoValido m_sMensajesEnlace(0)
        Cancel = True
        GoTo Salir
    End If
    If sdbgEtapasDestino.Columns("TIPO").Value = "" Then
        oMensajes.NoValido m_sMensajesEnlace(1)
        Cancel = True
        GoTo Salir
    End If
    If iTipoSolicitud = tipoSolicitud.AUTOFACTURA And sdbgEtapasDestino.Rows >= 2 Then
        Select Case sdbgEtapasDestino.Columns(sdbgEtapasDestino.col).Name
            Case "TIPO"
                If sdbgEtapasDestino.Columns("TIPO").Value = m_sTipoObligatorio Then
                    If sdbgEtapasDestino.Columns("EST_FACTURA").Value <> "" Then
                        m_bErrorEstadosFactura = False
                        For i = 0 To sdbgEtapasDestino.Rows - 1
                            vbm = sdbgEtapasDestino.AddItemBookmark(i)
                            If sdbgEtapasDestino.Columns("TIPO").CellValue(vbm) = m_sTipoObligatorio And sdbgEtapasDestino.Columns("ID_EST_FACTURA").CellValue(vbm) <> sdbgEtapasDestino.Columns("ID_EST_FACTURA").Value Then
                                m_bErrorEstadosFactura = True
                                Exit For
                            End If
                        Next i
                        
                        If m_bErrorEstadosFactura Then
                            oMensajes.CambiosObligatorios
                            Cancel = True
                            GoTo Salir
                        End If
                    End If
                End If
        End Select
    End If
    
    'Guardar Datos
     If sdbgEtapasDestino.IsAddRow Then
        Set m_oEnlaceAnyadir = oFSGSRaiz.Generar_CEnlace
        m_oEnlaceAnyadir.BloqueOrigen = oAccion.Bloque
        m_oEnlaceAnyadir.BloqueDestino = sdbgEtapasDestino.Columns("ID_DESTINO").Value
        m_oEnlaceAnyadir.Formula = sdbgEtapasDestino.Columns("FORMULA").Value
        m_oEnlaceAnyadir.Accion = oAccion.Id
        Set m_oEnlaceAnyadir.DenAccion = oAccion.Denominaciones
        m_oEnlaceAnyadir.TipoAccion = oAccion.Tipo
        m_oEnlaceAnyadir.LlamadaExterna = oAccion.idLlamadaExterna > 0
        m_oEnlaceAnyadir.EstadoFactura = sdbgEtapasDestino.Columns("ID_EST_FACTURA").Value
        Set m_oEnlaceAnyadir.Subject = SubjectEnlacePorDefecto()
            
        Set m_oIBaseDatosEnlaceEnEdicion = m_oEnlaceAnyadir
        teserror = m_oIBaseDatosEnlaceEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bAnyaEnlaceError = True
            If Me.Visible Then sdbgEtapasDestino.SetFocus
            GoTo Salir
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceAnya, "Accion:" & oAccion.Id & ",Id:" & m_oEnlaceAnyadir.Id
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
        
        oAccion.Enlaces.AddEnlace m_oEnlaceAnyadir
        oEnlaces_Nuevos.AddEnlace m_oEnlaceAnyadir
        sdbgEtapasDestino.Columns("ID").Value = m_oEnlaceAnyadir.Id
        sdbgEtapasDestino.Columns("FECACT").Value = m_oEnlaceAnyadir.FecAct
        m_bAnyaEnlaceError = False
    
    Else
        If Not m_oEnlaceEnEdicion Is Nothing Then
            m_oEnlaceEnEdicion.BloqueDestino = sdbgEtapasDestino.Columns("ID_DESTINO").Value
            m_oEnlaceEnEdicion.Formula = sdbgEtapasDestino.Columns("FORMULA").Value
            Set m_oEnlaceEnEdicion.DenAccion = oAccion.Denominaciones
            m_oEnlaceEnEdicion.TipoAccion = oAccion.Tipo
            m_oEnlaceEnEdicion.LlamadaExterna = oAccion.idLlamadaExterna > 0
            sdbgEtapasDestino.Columns("ID_EST_FACTURA").Value = sdbddEstadosFactura.Columns(0).Value
            m_oEnlaceEnEdicion.EstadoFactura = sdbddEstadosFactura.Columns(0).Value
            Set m_oIBaseDatosEnlaceEnEdicion = m_oEnlaceEnEdicion
            teserror = m_oIBaseDatosEnlaceEnEdicion.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                m_bModEnlaceError = True
                If Me.Visible Then sdbgEtapasDestino.SetFocus
                sdbgEtapasDestino.DataChanged = False
                GoTo Salir
                           
            Else
                ''' Registro de acciones
                sdbgEtapasDestino.Columns("FECACT").Value = m_oEnlaceEnEdicion.FecAct
                basSeguridad.RegistrarAccion AccionesSummit.ACCEnlaceModif, "Accion:" & oAccion.Id & ",Id:" & m_oEnlaceEnEdicion.Id
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
                m_bModEnlaceError = False
            End If
            
            Set m_oEnlaceEnEdicion = Nothing
        End If
    End If
    
    Set m_oEnlaceAnyadir = Nothing
    
    m_bEnlaceError = False
    Exit Sub
    
Salir:
    If Me.Visible Then sdbgEtapasDestino.SetFocus
    m_bEnlaceError = True
End Sub

Private Sub sdbgEtapasDestino_BtnClick()
    Dim teserror As TipoErrorSummit
    Dim oBloque_Dest As CBloque
    Dim sBloque_Dest As String
    Dim oNotificados As CNotificadosAccion

    If sdbgEtapasDestino.col < 0 Then Exit Sub
    
     If Not sdbgEtapasDestino.IsAddRow And m_oEnlaceEnEdicion Is Nothing Then
        Set m_oEnlaceEnEdicion = oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value))
        Set m_oIBaseDatosEnlaceEnEdicion = m_oEnlaceEnEdicion
        teserror = m_oIBaseDatosEnlaceEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            sdbgEtapasDestino.DataChanged = False
                
            sdbgEtapasDestino.Columns("ID").Value = m_oEnlaceEnEdicion.Id
            sdbgEtapasDestino.Columns("ID_DESTINO").Value = m_oEnlaceEnEdicion.BloqueDestino
            sdbgEtapasDestino.Columns("ID_EST_FACTURA").Value = m_oEnlaceEnEdicion.EstadoFactura
            Set oBloque_Dest = oFSGSRaiz.Generar_CBloque
            oBloque_Dest.Id = oEnlace.BloqueDestino
            Set m_oIBaseDatos = oBloque_Dest
            m_oIBaseDatos.IniciarEdicion
            sBloque_Dest = oBloque_Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sdbgEtapasDestino.Columns("DESTINO").Value = sBloque_Dest
            sdbgEtapasDestino.Columns("EST_FACTURA").Value = DevolverDenominacionEstadoFactura(m_oEnlaceEnEdicion.EstadoFactura)
            sdbgEtapasDestino.Columns("FECACT") = m_oEnlaceEnEdicion.FecAct
            Set m_oEnlaceEnEdicion = Nothing
            teserror.NumError = TESnoerror
            Exit Sub
        ElseIf teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgEtapasDestino.SetFocus
            Set m_oEnlaceEnEdicion = Nothing
            Exit Sub
        End If
     End If
  
    If sdbgEtapasDestino.Columns(sdbgEtapasDestino.col).Name = "CONDICION" Then
        If sdbgEtapasDestino.Columns("TIPO").Value = m_sTipoCondicional Then
        
            'acabo con la edicion del Enlace
            If sdbgEtapasDestino.IsAddRow And sdbgEtapasDestino.DataChanged Then
                sdbgEtapasDestino.Update
                If m_bEnlaceError Then
                    Exit Sub
                End If
                Set m_oEnlaceEnEdicion = oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value))
            End If
                     
            'Muestro el formulario de Condiciones
            m_bBtnClkEt = True
            frmFlujosCondicionEnlace.m_bModifFlujo = m_bModifFlujo
            frmFlujosCondicionEnlace.lIdFormulario = lIdFormulario
            frmFlujosCondicionEnlace.lIdEnlace = sdbgEtapasDestino.Columns("ID").Value
            frmFlujosCondicionEnlace.sFormula = sdbgEtapasDestino.Columns("FORMULA").Value
            frmFlujosCondicionEnlace.sDestino = sdbgEtapasDestino.Columns("DESTINO").Value
            
            frmFlujosCondicionEnlace.Show vbModal
            sdbgEtapasDestino.Columns("FORMULA").Value = frmFlujosCondicionEnlace.sFormula
            If frmFlujosCondicionEnlace.sFormula <> "" And sdbgEtapasDestino.Columns("CONDICION").Value <> "..." Then
                sdbgEtapasDestino.Columns("CONDICION").Value = "..."
            ElseIf frmFlujosCondicionEnlace.sFormula = "" And sdbgEtapasDestino.Columns("CONDICION").Value = "..." Then
                sdbgEtapasDestino.Columns("CONDICION").Value = " "
            End If
            Unload frmFlujosCondicionEnlace
            
            If sdbgEtapasDestino.DataChanged Then
                sdbgEtapasDestino.Update
                If m_bEnlaceError Then
                    Exit Sub
                End If
            End If
            Set m_oEnlaceEnEdicion = Nothing
        End If
    ElseIf sdbgEtapasDestino.Columns(sdbgEtapasDestino.col).Name = "NOTIFICADOS" Then
        'acabo con la edicion del Enlace
        If sdbgEtapasDestino.IsAddRow And sdbgEtapasDestino.DataChanged Then
            sdbgEtapasDestino.Update
            If m_bEnlaceError Then
                Exit Sub
            End If
            Set m_oEnlaceEnEdicion = oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value))
        End If
        
        If sdbgEtapasDestino.Columns("ID").Value = "" Then Exit Sub
        
        'Muestro el formulario de Notificados
        m_bBtnClkEt = True
        frmFlujosNotificadosEnlace.m_bModifFlujo = m_bModifFlujo
        frmFlujosNotificadosEnlace.lIdEnlace = sdbgEtapasDestino.Columns("ID").Value
        frmFlujosNotificadosEnlace.lIdFlujo = lIdFlujo
        frmFlujosNotificadosEnlace.lIdFormulario = lIdFormulario
        frmFlujosNotificadosEnlace.iTipoSolicitud = iTipoSolicitud
        Set frmFlujosNotificadosEnlace.m_oEnlace = m_oEnlaceEnEdicion
        Set frmFlujosNotificadosEnlace.Formulario = Me.Formulario
        frmFlujosNotificadosEnlace.Show vbModal
        
        Set oNotificados = oFSGSRaiz.Generar_CNotificadosAccion
        oNotificados.CargarNotificados OrigenNotificadoAccion.Enlace, CLng(sdbgEtapasDestino.Columns("ID").Value)
        If oNotificados.Count > 0 And sdbgEtapasDestino.Columns("NOTIFICADOS").Value <> "..." Then
            sdbgEtapasDestino.Columns("NOTIFICADOS").Value = "..."
        ElseIf oNotificados.Count = 0 And sdbgEtapasDestino.Columns("NOTIFICADOS").Value = "..." Then
            sdbgEtapasDestino.Columns("NOTIFICADOS").Value = " "
        End If
        
        If sdbgEtapasDestino.DataChanged Then
            sdbgEtapasDestino.Update
            If m_bEnlaceError Then
                Exit Sub
            End If
        End If
        Set m_oEnlaceEnEdicion = Nothing
    End If
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub sdbgEtapasDestino_Change()
    Dim teserror As TipoErrorSummit
    Dim vbm As Variant
    Dim oBloque_Dest As CBloque
    Dim sBloque_Dest As String
    Dim sEstadoFacturaAnterior As String
    
    If Not sdbgEtapasDestino.IsAddRow Then
        Set m_oEnlaceEnEdicion = Nothing
        Set m_oEnlaceEnEdicion = oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value))
        
        Set m_oIBaseDatosEnlaceEnEdicion = m_oEnlaceEnEdicion
            
        teserror = m_oIBaseDatosEnlaceEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
                
            TratarError teserror
            sdbgEtapasDestino.DataChanged = False
                
            sdbgEtapasDestino.Columns("ID").Value = m_oEnlaceEnEdicion.Id
            sdbgEtapasDestino.Columns("ID_DESTINO").Value = m_oEnlaceEnEdicion.BloqueDestino
            sdbgEtapasDestino.Columns("ID_EST_FACTURA").Value = m_oEnlaceEnEdicion.EstadoFactura
            Set oBloque_Dest = oFSGSRaiz.Generar_CBloque
            oBloque_Dest.Id = oEnlace.BloqueDestino
            Set m_oIBaseDatos = oBloque_Dest
            m_oIBaseDatos.IniciarEdicion

            sBloque_Dest = oBloque_Dest.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
            sdbgEtapasDestino.Columns("DESTINO").Value = sBloque_Dest
            sEstadoFacturaAnterior = sdbgEtapasDestino.Columns("EST_FACTURA").Value
            sdbgEtapasDestino.Columns("EST_FACTURA").Value = DevolverDenominacionEstadoFactura(m_oEnlaceEnEdicion.EstadoFactura)
            sdbgEtapasDestino.Columns("FECACT").Value = m_oEnlaceEnEdicion.FecAct
            
            teserror.NumError = TESnoerror
                
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgEtapasDestino.SetFocus
        End If
               
    End If
    
    If sdbgEtapasDestino.col >= 0 Then
        Select Case sdbgEtapasDestino.Columns(sdbgEtapasDestino.col).Name
            Case "TIPO"
                If sdbgEtapasDestino.Columns("TIPO").Value = m_sTipoObligatorio Then
                    If sdbgEtapasDestino.Columns("FORMULA").Value <> "" Then
                        If oMensajes.PreguntaEliminarFormula = vbNo Then
                            sdbgEtapasDestino.Columns("TIPO").Value = m_sTipoCondicional
                        Else
                            'Eliminar la formula de la BBDD
                            Dim oCondicionesEnlace As CCondicionesEnlace
                            Set oCondicionesEnlace = oFSGSRaiz.Generar_CCondicionesEnlace
                            oCondicionesEnlace.EliminarCondicionesEnlace (sdbgEtapasDestino.Columns("ID").Value)
                            Set oCondicionesEnlace = Nothing
                                                   
                            sdbgEtapasDestino.Columns("FORMULA").Value = ""
                            sdbgEtapasDestino.Columns("CONDICION").Value = " "
                            
                        End If
                    End If
                End If
            Case "EST_FACTURA"
                If iTipoSolicitud = tipoSolicitud.AUTOFACTURA And sdbgEtapasDestino.Rows >= 2 Then
                    If sdbgEtapasDestino.Columns("EST_FACTURA").Value <> "" And sdbgEtapasDestino.Columns("TIPO").Value = m_sTipoObligatorio Then
                        m_bErrorEstadosFactura = False
                        For i = 0 To sdbgEtapasDestino.Rows - 1
                            vbm = sdbgEtapasDestino.AddItemBookmark(i)
                            If sdbgEtapasDestino.Columns("TIPO").CellValue(vbm) = m_sTipoObligatorio And sdbgEtapasDestino.Columns("ID_EST_FACTURA").CellValue(vbm) <> sdbddEstadosFactura.Columns(0).Value Then
                                m_bErrorEstadosFactura = True
                                Exit For
                            End If
                        Next i
                        
                        If m_bErrorEstadosFactura Then
                            oMensajes.CambiosObligatorios
                            sdbgEtapasDestino.Columns("EST_FACTURA").Value = sEstadoFacturaAnterior
                        End If
                    End If
                End If
        End Select
    End If
    Set m_oIBaseDatos = Nothing
End Sub


Private Sub sdbgEtapasDestino_InitColumnProps()
    sdbgEtapasDestino.Columns("DESTINO").caption = m_sDestino
    sdbgEtapasDestino.Columns("TIPO").caption = m_sTipoEnlace
    sdbgEtapasDestino.Columns("CONDICION").caption = m_sCondicion
    sdbgEtapasDestino.Columns("NOTIFICADOS").caption = m_sNotificados
    sdbgEtapasDestino.Columns("EST_FACTURA").caption = m_sEstadoFactura
End Sub

Private Sub sdbgEtapasDestino_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete Then
            cmdEliminarEnlace_Click
        Exit Sub

    End If
End Sub

Private Sub sdbgEtapasDestino_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgEtapasDestino.DataChanged Then
                sdbgEtapasDestino.Update
                If m_bEnlaceError Then
                    Exit Sub
                End If
            End If
        Case vbKeyBack
            If sdbgEtapasDestino.col >= 0 Then
                Select Case sdbgEtapasDestino.Columns(sdbgEtapasDestino.col).Name
                    Case "DESTINO"
                        sdbgEtapasDestino.Columns("ID_DESTINO").Value = 0
                        sdbgEtapasDestino.Columns("DESTINO").Value = ""
                    Case "EST_FACTURA"
                        sdbgEtapasDestino.Columns("ID_EST_FACTURA").Value = 0
                        sdbgEtapasDestino.Columns("EST_FACTURA").Value = ""
                End Select
            End If
            
        Case vbKeyEscape
            If sdbgEtapasDestino.DataChanged = False Then
                sdbgEtapasDestino.CancelUpdate
                sdbgEtapasDestino.DataChanged = False
            End If
            cmdAnyadirEnlace.Enabled = True
            cmdEliminarEnlace.Enabled = True
    End Select
End Sub

Private Sub sdbgEtapasDestino_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgEtapasDestino.IsAddRow Then
        cmdAnyadirEnlace.Enabled = False
    ElseIf Not sdbgEtapasDestino.Enabled Then
        cmdAnyadirEnlace.Enabled = False
    Else
        cmdAnyadirEnlace.Enabled = True
    End If
End Sub

Private Sub sdbgNotificados_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgNotificados.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgNotificados.GetBookmark(0)) Then
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(-1)
    Else
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(0)
    End If
    If Me.Visible Then sdbgNotificados.SetFocus
End Sub

Private Sub sdbgNotificados_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
        
    If IsEmpty(sdbgNotificados.GetBookmark(0)) Then
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(-1)
    Else
        sdbgNotificados.Bookmark = sdbgNotificados.GetBookmark(0)
    End If
End Sub

Private Sub sdbgNotificados_AfterUpdate(RtnDispErrMsg As Integer)
    
    RtnDispErrMsg = 0
       
    If Not m_oNotificadoEnEdicion Is Nothing Then
        sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FecAct
    End If
    
    Set m_oNotificadoEnEdicion = Nothing
End Sub

Private Sub sdbgNotificados_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub


Private Sub sdbgNotificados_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    
    'Comprobar datos Obligatorios
    If sdbgNotificados.Columns("TIPO").Value = "" Or sdbgNotificados.Columns("TIPO").Value = "0" Then
        oMensajes.NoValido m_sMensajesNotificado(1)
        Cancel = True
        GoTo Salir
    End If
    If sdbgNotificados.Columns("COD_NOTIFICADO").Value = "" Then
        oMensajes.NoValido m_sMensajesNotificado(2)
        Cancel = True
        GoTo Salir
    End If
    
    'Guardar Datos
     If sdbgNotificados.IsAddRow Then
        Set m_oNotificadoAnyadir = oFSGSRaiz.Generar_CNotificadoAccion
        m_oNotificadoAnyadir.Origen = OrigenNotificadoAccion.Accion
        m_oNotificadoAnyadir.Accion = oAccion.Id
        Select Case CInt(sdbgNotificados.Columns("TIPO").Value)
            Case TipoNotificadoAccion.Especifico
                m_oNotificadoAnyadir.Tipo = TipoNotificadoAccion.Especifico
                m_oNotificadoAnyadir.Per = sdbgNotificados.Columns("COD_NOTIFICADO").Value
            Case TipoNotificadoAccion.Rol
                m_oNotificadoAnyadir.Tipo = TipoNotificadoAccion.Rol
                m_oNotificadoAnyadir.Rol = CLng(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
            Case TipoNotificadoAccion.Peticionario 'En Realidad se refiere a 'Generico'
                m_oNotificadoAnyadir.Tipo = CInt(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
        End Select
        
        Set m_oIBaseDatosNotificadoEnEdicion = m_oNotificadoAnyadir
        teserror = m_oIBaseDatosNotificadoEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bAnyaNotificadoError = True
            If Me.Visible Then sdbgNotificados.SetFocus
            GoTo Salir
        Else
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
            basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionAnya, "Accion:" & oAccion.Id & ",ID:" & m_oNotificadoAnyadir.Id
        End If

        sdbgNotificados.Columns("ID").Value = m_oNotificadoAnyadir.Id
        sdbgNotificados.Columns("FECACT").Value = m_oNotificadoAnyadir.FecAct
        m_oNotificados.AddNotificadoEnlace m_oNotificadoAnyadir
        m_bAnyaNotificadoError = False

    Else
        If Not m_oNotificadoEnEdicion Is Nothing Then
            Select Case CInt(sdbgNotificados.Columns("TIPO").Value)
                Case TipoNotificadoAccion.Especifico
                    m_oNotificadoEnEdicion.Tipo = TipoNotificadoAccion.Especifico
                    m_oNotificadoEnEdicion.Per = sdbgNotificados.Columns("COD_NOTIFICADO").Value
                    m_oNotificadoEnEdicion.Configuracion_Rol = False
                Case TipoNotificadoAccion.Rol
                    m_oNotificadoEnEdicion.Tipo = TipoNotificadoAccion.Rol
                    m_oNotificadoEnEdicion.Rol = CLng(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
                Case TipoNotificadoAccion.Peticionario 'En Realidad se refiere a 'Generico'
                    m_oNotificadoEnEdicion.Tipo = CInt(sdbgNotificados.Columns("COD_NOTIFICADO").Value)
                Case TipoNotificadoAccion.RespProceCompraAsoc
                    m_oNotificadoEnEdicion.Configuracion_Rol = False
                Case TipoNotificadoAccion.GestorSolicitud
                    m_oNotificadoEnEdicion.Configuracion_Rol = False
            End Select
            
            Set m_oIBaseDatosNotificadoEnEdicion = m_oNotificadoEnEdicion
            teserror = m_oIBaseDatosNotificadoEnEdicion.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                m_bModNotificadoError = True
                If Me.Visible Then sdbgNotificados.SetFocus
                sdbgNotificados.DataChanged = False
                GoTo Salir
            Else
                ''' Registro de acciones
                sdbgNotificados.Columns("FECACT").Value = m_oNotificadoEnEdicion.FecAct
                basSeguridad.RegistrarAccion AccionesSummit.ACCNotificadoAccionModif, "Accion:" & oAccion.Id & ",ID:" & m_oNotificadoEnEdicion.Id
                m_bModNotificadoError = False
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If
    
            Set m_oNotificadoEnEdicion = Nothing
        End If
    End If

    Set m_oNotificadoAnyadir = Nothing
    Exit Sub
    
Salir:
    If Me.Visible Then sdbgNotificados.SetFocus
    m_bNotificadoError = True
    Exit Sub

End Sub

Private Sub sdbgNotificados_BtnClick()
    With sdbgNotificados
        If .IsAddRow Or .DataChanged Then
            .Update
            If .IsAddRow Or .DataChanged Then Exit Sub
        End If
        
        Set m_oNotificadoEnEdicion = Nothing
        Set m_oNotificadoEnEdicion = m_oNotificados.Item(CStr(sdbgNotificados.Columns("ID").Value))
        
        frmFlujosConfigCampos.lIdFormulario = lIdFormulario
        frmFlujosConfigCampos.lIdNotificado = .Columns("ID").Value
        frmFlujosConfigCampos.iOrigenNotificado = OrigenNotificadoAccion.Accion
        frmFlujosConfigCampos.bConfiguracionCamposRolVisible = ValidarVisibilidadConfiguracionCamposRol
        Set frmFlujosConfigCampos.oNotificadoEnEdicion = m_oNotificadoEnEdicion
        frmFlujosConfigCampos.bConfiguracionRol = m_oNotificadoEnEdicion.Configuracion_Rol
        frmFlujosConfigCampos.Show vbModal
    End With
End Sub

Private Sub sdbgNotificados_Change()
    Dim teserror As TipoErrorSummit
    
'    cmdAnyadirNotificado.Enabled = False
    
    If Not sdbgNotificados.IsAddRow Then
        Set m_oNotificadoEnEdicion = Nothing
        Set m_oNotificadoEnEdicion = m_oNotificados.Item(CStr(sdbgNotificados.Columns("ID").Value))
        
        Set m_oIBaseDatosNotificadoEnEdicion = m_oNotificadoEnEdicion
            
        teserror = m_oIBaseDatosNotificadoEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            sdbgNotificados.DataChanged = False
            
            'CargarCondiciones
            
            teserror.NumError = TESnoerror
                
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgNotificados.SetFocus
        End If
               
    End If
    
    If sdbgNotificados.col >= 0 Then
        Select Case sdbgNotificados.Columns(sdbgNotificados.col).Name
            Case "TIPO_DEN"
                sdbgNotificados.Columns("COD_NOTIFICADO").Value = ""
                sdbgNotificados.Columns("NOTIFICADO").Value = ""
        End Select
    End If
    
End Sub

Private Sub sdbgNotificados_InitColumnProps()
    sdbgNotificados.Columns("TIPO_DEN").caption = m_sTipoNotificado
    sdbgNotificados.Columns("NOTIFICADO").caption = m_sNotificado
    sdbgNotificados.Columns("CONFCAMPOS").caption = m_sConfCampos
End Sub

Private Sub sdbgNotificados_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete Then
            cmdEliminarNotificado_Click
        Exit Sub

    End If
End Sub

Private Sub sdbgNotificados_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgNotificados.DataChanged Then
                sdbgNotificados.Update
                If m_bNotificadoError Then
                    Exit Sub
                End If
            End If
        Case vbKeyBack
            If sdbgNotificados.col >= 0 Then
                Select Case sdbgNotificados.Columns(sdbgNotificados.col).Name
                    Case "TIPO_DEN"
                        sdbgNotificados.Columns("TIPO").Value = 0
                        sdbgNotificados.Columns("TIPO_DEN").Value = ""
                        sdbgNotificados.Columns("COD_NOTIFICADO").Value = ""
                        sdbgNotificados.Columns("NOTIFICADO").Value = ""
                    Case "NOTIFICADO"
                        sdbgNotificados.Columns("COD_NOTIFICADO").Value = ""
                        sdbgNotificados.Columns("NOTIFICADO").Value = ""
                        sdbgNotificados.col = sdbgNotificados.Columns("TIPO_DEN").Position
                End Select
            End If
            
        Case vbKeyEscape
            If sdbgNotificados.DataChanged = False Then
                sdbgNotificados.CancelUpdate
                sdbgNotificados.DataChanged = False
            End If
            cmdAnyadirNotificado.Enabled = True
            cmdEliminarNotificado.Enabled = True
    End Select
End Sub

Private Sub sdbgNotificados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bMalaColumna As Boolean
    Dim iIndexMensaje As Integer
    Dim sColumnaARellenar As String
    
    If sdbgNotificados.IsAddRow Then
        cmdAnyadirNotificado.Enabled = False
    Else
        cmdAnyadirNotificado.Enabled = True
    End If

    If sdbgNotificados.col < 0 Then
        Exit Sub
    End If
    
    bMalaColumna = False
    Select Case sdbgNotificados.Columns(sdbgNotificados.col).Name
        Case "NOTIFICADO"
            bMalaColumna = True
            iIndexMensaje = 2
            sColumnaARellenar = "TIPO_DEN"
            If sdbgNotificados.Columns("TIPO").Value <> "" Then
                If CInt(sdbgNotificados.Columns("TIPO").Value) > 0 Then
                    bMalaColumna = False
                End If
            End If
    End Select
    If bMalaColumna And Not m_bSalir Then
        oMensajes.NoValido m_sMensajesNotificado(iIndexMensaje)
        sdbgNotificados.col = sdbgNotificados.Columns(sColumnaARellenar).Position
    End If
End Sub

Private Sub sdbgPrecondiciones_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgPrecondiciones.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgPrecondiciones.GetBookmark(0)) Then
        sdbgPrecondiciones.Bookmark = sdbgPrecondiciones.GetBookmark(-1)
    Else
        sdbgPrecondiciones.Bookmark = sdbgPrecondiciones.GetBookmark(0)
    End If
    If Me.Visible Then sdbgPrecondiciones.SetFocus
End Sub

Private Sub sdbgPrecondiciones_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
        
    If IsEmpty(sdbgPrecondiciones.GetBookmark(0)) Then
        sdbgPrecondiciones.Bookmark = sdbgPrecondiciones.GetBookmark(-1)
    Else
        sdbgPrecondiciones.Bookmark = sdbgPrecondiciones.GetBookmark(0)
    End If
End Sub

Private Sub sdbgPrecondiciones_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
       
    If Not m_oPrecondicionEnEdicion Is Nothing Then
        sdbgPrecondiciones.Columns("FECACT").Value = m_oPrecondicionEnEdicion.FecAct
    End If
    
    Set m_oPrecondicionEnEdicion = Nothing
End Sub

Private Sub sdbgPrecondiciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgPrecondiciones_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oIdioma As CIdioma
    Dim oIdiomas As CIdiomas
    
    'Comprobar datos Obligatorios
    If sdbgPrecondiciones.Columns("COD").Value = "" Then
        oMensajes.NoValido m_sMensajesPrecondicion(1)
        GoTo Salir
    End If
    If sdbgPrecondiciones.Columns("TIPO").Value = "" Or sdbgNotificados.Columns("TIPO").Value = "0" Then
        oMensajes.NoValido m_sMensajesPrecondicion(2)
        GoTo Salir
    End If
    If Not m_bPrecondicionesBtn Then 'Si no es que acabamos de pulsar el boton de las condiciones para la precondicion:
        If sdbgPrecondiciones.Columns("MENSAJE_" & basPublic.gParametrosInstalacion.gIdioma).Value = "" Then
            oMensajes.NoValido m_sMensajesPrecondicion(4)
            GoTo Salir
        End If
    End If
    
    If sdbgPrecondiciones.Columns("MENSAJE_" & basPublic.gParametrosInstalacion.gIdioma).Value = "" Then
        oMensajes.IntroducirCampo m_sMensajesPrecondicion(4)
        Cancel = True
        GoTo Salir
    End If
        
    'Guardar Datos
     If sdbgPrecondiciones.IsAddRow Then
        Set m_oPrecondicionAnyadir = oFSGSRaiz.Generar_CPrecondicion
        m_oPrecondicionAnyadir.Codigo = sdbgPrecondiciones.Columns("COD").Value
        m_oPrecondicionAnyadir.Orden = sdbgPrecondiciones.Rows
        m_oPrecondicionAnyadir.Accion = oAccion.Id
        m_oPrecondicionAnyadir.TipoPrecondicion = sdbgPrecondiciones.Columns("TIPO").Value
        m_oPrecondicionAnyadir.Formula = sdbgPrecondiciones.Columns("FORMULA").Value
        Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
        Set m_oPrecondicionAnyadir.Denominaciones = oFSGSRaiz.Generar_CMultiidiomas
        For Each oIdioma In oIdiomas
            m_oPrecondicionAnyadir.Denominaciones.Add oIdioma.Cod, sdbgPrecondiciones.Columns("MENSAJE_" & oIdioma.Cod).Value
        Next
        
        Set m_oIBaseDatosPrecondicionEnEdicion = m_oPrecondicionAnyadir
        teserror = m_oIBaseDatosPrecondicionEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bAnyaPrecondicionError = True
            If Me.Visible Then sdbgPrecondiciones.SetFocus
            GoTo Salir
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCPrecondicionAnya, "Accion:" & oAccion.Id & ",ID:" & m_oPrecondicionAnyadir.Id
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If

        sdbgPrecondiciones.Columns("ID").Value = m_oPrecondicionAnyadir.Id
        sdbgPrecondiciones.Columns("FECACT").Value = m_oPrecondicionAnyadir.FecAct
        m_oPrecondiciones.AddPrecondicion m_oPrecondicionAnyadir
        m_bAnyaPrecondicionError = False

    Else
        If Not m_oPrecondicionEnEdicion Is Nothing Then
            If Not sdbgPrecondiciones.IsAddRow And m_oPrecondicionEnEdicion Is Nothing Then
                Set m_oPrecondicionEnEdicion = m_oPrecondiciones.Item(CStr(sdbgPrecondiciones.Columns("ID").Value))
                Set m_oIBaseDatosPrecondicionEnEdicion = m_oPrecondicionEnEdicion
                teserror = m_oIBaseDatosPrecondicionEnEdicion.IniciarEdicion
                If teserror.NumError = TESInfModificada Then
                   TratarError teserror
                   sdbgPrecondiciones.DataChanged = False
                   CargarPrecondiciones
                   teserror.NumError = TESnoerror
                End If
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    If Me.Visible Then sdbgPrecondiciones.SetFocus
                    Exit Sub
                Else
                    'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                    frmFlujos.HayCambios
                End If
            End If
    
            m_oPrecondicionEnEdicion.Codigo = sdbgPrecondiciones.Columns("COD").Value
            m_oPrecondicionEnEdicion.Orden = sdbgPrecondiciones.Columns("ORDEN").Value
            m_oPrecondicionEnEdicion.Accion = oAccion.Id
            m_oPrecondicionEnEdicion.TipoPrecondicion = sdbgPrecondiciones.Columns("TIPO").Value
            m_oPrecondicionEnEdicion.Formula = sdbgPrecondiciones.Columns("FORMULA").Value
            Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
            For Each oIdioma In oIdiomas
                m_oPrecondicionEnEdicion.Denominaciones.Item(oIdioma.Cod).Den = sdbgPrecondiciones.Columns("MENSAJE_" & oIdioma.Cod).Value
            Next
            
            Set m_oIBaseDatosPrecondicionEnEdicion = m_oPrecondicionEnEdicion
            teserror = m_oIBaseDatosPrecondicionEnEdicion.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                m_bModPrecondicionError = True
                If Me.Visible Then sdbgPrecondiciones.SetFocus
                sdbgPrecondiciones.DataChanged = False
                GoTo Salir
            Else
                ''' Registro de acciones
                sdbgPrecondiciones.Columns("FECACT").Value = m_oPrecondicionEnEdicion.FecAct
                basSeguridad.RegistrarAccion AccionesSummit.ACCPrecondicionModif, "Accion:" & oAccion.Id & ",ID:" & m_oPrecondicionEnEdicion.Id
                m_bModPrecondicionError = False
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If
    
            Set m_oPrecondicionEnEdicion = Nothing
        End If
    End If
    
    m_bPrecondicionError = False
    m_bAnyaPrecondicionError = False
    m_bModPrecondicionError = False

    Set m_oPrecondicionAnyadir = Nothing
    Exit Sub
    
Salir:
    If Me.Visible Then sdbgPrecondiciones.SetFocus
    Cancel = True
    m_bPrecondicionError = True
    Exit Sub
End Sub

Private Sub sdbgPrecondiciones_BtnClick()
    Dim teserror As TipoErrorSummit
    If sdbgPrecondiciones.col < 0 Then Exit Sub
    If Not sdbgPrecondiciones.IsAddRow And m_oPrecondicionEnEdicion Is Nothing Then
       Set m_oPrecondicionEnEdicion = m_oPrecondiciones.Item(CStr(sdbgPrecondiciones.Columns("ID").Value))
       Set m_oIBaseDatosPrecondicionEnEdicion = m_oPrecondicionEnEdicion

       teserror = m_oIBaseDatosPrecondicionEnEdicion.IniciarEdicion

       If teserror.NumError = TESInfModificada Then
           TratarError teserror
           sdbgPrecondiciones.DataChanged = False

           CargarPrecondiciones

           teserror.NumError = TESnoerror
       End If
       If teserror.NumError <> TESnoerror Then
           TratarError teserror
           If Me.Visible Then sdbgPrecondiciones.SetFocus
           Set m_oPrecondicionEnEdicion = Nothing
           Exit Sub
       End If
    End If
  
    If sdbgPrecondiciones.Columns(sdbgPrecondiciones.col).Name = "CONDICIONES" Then
            m_bPrecondicionesBtn = True
            'acabo con la edicion del Enlace
            sdbgPrecondiciones.Update
            DoEvents
            Set m_oPrecondicionEnEdicion = m_oPrecondiciones.Item(CStr(sdbgPrecondiciones.Columns("ID").Value))
            
            If m_bPrecondicionError Or m_bAnyaPrecondicionError Or m_bModPrecondicionError Then
                Set m_oPrecondicionEnEdicion = Nothing
                Exit Sub
            End If
            
            If sdbgPrecondiciones.Columns("ID").Value <> "" Then
                m_bBtnClkPr = True
                frmFlujosCondicionEnlace.m_bModifFlujo = m_bModifFlujo
                frmFlujosCondicionEnlace.lIdFormulario = lIdFormulario
                frmFlujosCondicionEnlace.lIdEnlace = 0
                frmFlujosCondicionEnlace.lIdPrecondicion = sdbgPrecondiciones.Columns("ID").Value
                frmFlujosCondicionEnlace.sFormula = sdbgPrecondiciones.Columns("FORMULA").Value
                frmFlujosCondicionEnlace.sCodPrecondicion = sdbgPrecondiciones.Columns("COD").Value
                frmFlujosCondicionEnlace.Show vbModal
                
                sdbgPrecondiciones.Columns("FORMULA").Value = frmFlujosCondicionEnlace.sFormula
                
                If sdbgPrecondiciones.DataChanged Then
                    sdbgPrecondiciones.Update
                    If m_bPrecondicionError Then
                        Exit Sub
                    End If
                End If
            End If
    End If
    Set m_oIBaseDatosPrecondicionEnEdicion = Nothing
    Set m_oPrecondicionEnEdicion = Nothing
End Sub

Private Sub sdbgPrecondiciones_Change()
    Dim teserror As TipoErrorSummit
    If Not sdbgPrecondiciones.IsAddRow Then
        Set m_oPrecondicionEnEdicion = Nothing
        Set m_oPrecondicionEnEdicion = m_oPrecondiciones.Item(CStr(sdbgPrecondiciones.Columns("ID").Value))
        Set m_oIBaseDatosPrecondicionEnEdicion = m_oPrecondicionEnEdicion
            
        teserror = m_oIBaseDatosPrecondicionEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            sdbgPrecondiciones.DataChanged = False
            
            teserror.NumError = TESnoerror
                
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgPrecondiciones.SetFocus
        End If
               
    End If
End Sub

Private Sub sdbgPrecondiciones_InitColumnProps()
        
    sdbgPrecondiciones.Columns("COD").caption = m_sIDPrecond
    sdbgPrecondiciones.Columns("TIPO_DEN").caption = m_sTipoPrecond
    sdbgPrecondiciones.Columns("CONDICIONES").caption = m_sCondicionesPrecond
    
    Form_Resize
End Sub

Private Sub sdbgPrecondiciones_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete Then
            cmdEliminarPrecondicion_Click
        Exit Sub

    End If

End Sub

Private Sub sdbgPrecondiciones_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgPrecondiciones.DataChanged Then
                sdbgPrecondiciones.Update
                If m_bPrecondicionError Then
                    Exit Sub
                End If
            End If
        Case vbKeyBack
            If sdbgPrecondiciones.col >= 0 Then
                Select Case sdbgPrecondiciones.Columns(sdbgPrecondiciones.col).Name
                    Case "TIPO_DEN"
                        sdbgPrecondiciones.Columns("TIPO").Value = 0
                        sdbgPrecondiciones.Columns("TIPO_DEN").Value = ""
                End Select
            End If
            
        Case vbKeyEscape
            If sdbgPrecondiciones.DataChanged = False Then
                sdbgPrecondiciones.CancelUpdate
                sdbgPrecondiciones.DataChanged = False
            End If
            cmdAnyadirPrecondicion.Enabled = True
            cmdSubirPrecondicion.Enabled = True
            cmdBajarPrecondicion.Enabled = True
    End Select
End Sub

Private Sub sdbgPrecondiciones_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgPrecondiciones.IsAddRow Then
        cmdAnyadirPrecondicion.Enabled = False
        cmdSubirPrecondicion.Enabled = False
        cmdBajarPrecondicion.Enabled = False
    Else
        cmdAnyadirPrecondicion.Enabled = True
        cmdSubirPrecondicion.Enabled = True
        cmdBajarPrecondicion.Enabled = True
    End If
End Sub

Private Function actualizarYSalirPrecondiciones() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error.
        
    If sdbgPrecondiciones.DataChanged = True Then
        m_bModPrecondicionError = False
        m_bAnyaPrecondicicionError = False
        m_bPrecondicionError = False

        If sdbgPrecondiciones.Row = 0 Then
            sdbgPrecondiciones.MoveNext
            DoEvents
            If m_bModPrecondicionError Or m_bAnyaPrecondicicionError Or m_bPrecondicionError Then
                actualizarYSalirPrecondiciones = True
                Exit Function
            Else
                sdbgPrecondiciones.MovePrevious
            End If
        Else
            sdbgPrecondiciones.MovePrevious
            DoEvents
            If m_bModPrecondicionError Or m_bAnyaPrecondicicionError Or m_bPrecondicionError Then
                actualizarYSalirPrecondiciones = True
                Exit Function
            Else
                sdbgPrecondiciones.MoveNext
            End If
        End If
    Else
        actualizarYSalirPrecondiciones = False
    End If
    
End Function

Private Function actualizarYSalirNotificados() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error.
        
    If sdbgNotificados.DataChanged = True Then
        m_bModNotificadoError = False
        m_bAnyaNotificadoError = False
        m_bNotificadoError = False

        If sdbgNotificados.Row = 0 Then
            sdbgNotificados.MoveNext
            DoEvents
            If m_bModNotificadoError Or m_bAnyaNotificadoError Or m_bNotificadoError Then
                actualizarYSalirNotificados = True
                Exit Function
            Else
                sdbgNotificados.MovePrevious
            End If
        Else
            sdbgNotificados.MovePrevious
            DoEvents
            If m_bModNotificadoError Or m_bAnyaNotificadoError Or m_bNotificadoError Then
                actualizarYSalirNotificados = True
                Exit Function
            Else
                sdbgNotificados.MoveNext
            End If
        End If
    Else
        actualizarYSalirNotificados = False
    End If
    
End Function

Private Function actualizarYSalirEtapasDestino() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error.
    
    If sdbgEtapasDestino.DataChanged = True Then
        m_bModEnlaceError = False
        m_bAnyaEnlaceError = False
        m_bEnlaceError = False

        If sdbgEtapasDestino.Row = 0 Then
            sdbgEtapasDestino.MoveNext
            DoEvents
            If m_bModEnlaceError Or m_bAnyaEnlaceError Or m_bEnlaceError Then
                actualizarYSalirEtapasDestino = True
                Exit Function
            Else
                sdbgEtapasDestino.MovePrevious
            End If
        Else
            sdbgEtapasDestino.MovePrevious
            DoEvents
            If m_bModEnlaceError Or m_bAnyaEnlaceError Or m_bEnlaceError Then
                actualizarYSalirEtapasDestino = True
                Exit Function
            Else
                sdbgEtapasDestino.MoveNext
            End If
        End If
    Else
        actualizarYSalirEtapasDestino = False
    End If
    
End Function

Private Sub chkCumpOblRol_Click()
    oAccion.CumpOblRol = chkCumpOblRol.Value
    oAccion.ModificarAccion_optCumpObl
End Sub

Private Sub chkGuarda_Click()
    oAccion.Guarda = chkGuarda.Value
    If m_bActivado Or m_bCambiadoEnLoad Then
        oAccion.ModificarAccion_optGuardar
        frmFlujos.HayCambios
    End If
End Sub

Private Sub cmdCerrar_Click()

    If oAccion.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den = "" Then
        If sdbgEtapasDestino.Rows > 0 Or sdbgPrecondiciones.Rows > 0 Or sdbgNotificados.Rows > 0 Then
            oMensajes.NoValido m_sMensajeDenominacion & " " & oGestorParametros.DevolverIdiomas.Item(basPublic.gParametrosInstalacion.gIdioma).Den
            Exit Sub
        End If
    End If

    sdbgEtapasDestino.Update
    
    For Each oEnlace In oAccion.Enlaces
        Set oEnlace.DenAccion = oAccion.Denominaciones
        oEnlace.TipoAccion = oAccion.Tipo
    Next
    For Each oEnlace In oEnlaces_Modificados
        Set oEnlace.DenAccion = oAccion.Denominaciones
        oEnlace.TipoAccion = oAccion.Tipo
    Next
    For Each oEnlace In oEnlaces_Nuevos
        Set oEnlace.DenAccion = oAccion.Denominaciones
        oEnlace.TipoAccion = oAccion.Tipo
    Next

    basSeguridad.RegistrarAccion AccionesSummit.ACCAccionBloqueModif, "ID:" & oAccion.Id
    
    frmFlujos.reflejarAccionEnDiagrama
    
    If Not m_bEnlaceError Then
        Unload Me
    End If
End Sub

Private Sub EliminarAccion()
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    If oAccion.Id <> 0 Then
        Set m_oIBaseDatos = oAccion
        teserror = m_oIBaseDatos.EliminarDeBaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Set oIBaseDatos = Nothing
            Exit Sub
        End If
        'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
        frmFlujos.HayCambios
    End If
End Sub

Private Sub sdbgSubject_AfterUpdate(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
End Sub

Private Sub sdbgSubject_BeforeUpdate(Cancel As Integer)
    m_oSubject.Item(sdbgSubject.Columns("COD_IDIOMA").Value).Den = sdbgSubject.Columns("VALOR_IDIOMA").Value
        m_GridSubjectUpdated = True
End Sub

Private Function SubjectEnlacePorDefecto() As CMultiidiomas
    Dim oIdiomas As CIdiomas
    Dim oIdioma As CIdioma
    Dim oSubject As CMultiidiomas
    Set oIdiomas = oGestorParametros.DevolverIdiomas(, , True)
    Set oSubject = oFSGSRaiz.Generar_CMultiidiomas
    
    For Each oIdioma In oIdiomas
        oSubject.Add oIdioma.Cod, prefijo & oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSNOTIFICADOSENLACE, oIdioma.Cod, 16)(0).Value
    Next
    Set SubjectEnlacePorDefecto = oSubject
End Function
''' <summary>
''' evento que salta cuando se procude el click en el check para habilitar la llamada externa de una acci�n
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde:frmFlujosDetalleAccion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub chkLlamadaExterna_Click()
    If chkLlamadaExterna.Value = 1 Then
        CargarAccionesExternas
        'Me.sdbcAccionesExternas.Visible = True
        If Me.sdbcAccionesExternas.Value <> "" Then
            If Not oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value)) Is Nothing Then
                oAccion.idLlamadaExterna = 1
                oAccion.ModificarAccion_GrabarLlamadaExterna
                oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value)).LlamadaExterna = True
            End If
        End If
    Else
        Me.sdbcAccionesExternas.Visible = False
        oAccion.idLlamadaExterna = 0
        oAccion.ModificarAccion_GrabarLlamadaExterna
        If Not oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value)) Is Nothing Then
            oAccion.Enlaces.Item(CStr(sdbgEtapasDestino.Columns("ID").Value)).LlamadaExterna = False
        End If
    End If
End Sub
''' <summary>
''' Carga en el combo todas las llamadas externas que hay disponibles para las acciones
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde:frmFlujosDetalleAccion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub CargarAccionesExternas()
    Dim m_oAcciones As CAccionesBloque
    Dim rs As Recordset
    Dim ind As Long
    Dim vbm As Variant
    
    sdbcAccionesExternas.RemoveAll
    
    Set m_oAcciones = oFSGSRaiz.Generar_CAccionesBloque
    Set rs = m_oAcciones.DevolverAccionesExternas(basPublic.gParametrosInstalacion.gIdioma)
    While Not rs.EOF
        sdbcAccionesExternas.AddItem rs("ID").Value & Chr(m_lSeparador) & rs("DEN").Value
        vbm = sdbcAccionesExternas.AddItemBookmark(ind)
        If oAccion.idLlamadaExterna = rs("ID").Value Then
            sdbcAccionesExternas.Bookmark = vbm
            sdbcAccionesExternas.Value = rs("ID").Value
        End If
        rs.MoveNext
        ind = ind + 1
    Wend
    
End Sub

''' <summary>
''' evento que salta cuando se procude el click en el check para habilitar el fichero de factura
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde:frmFlujosDetalleAccion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub chkFicheroEFactura_Click()
    oAccion.FicheroEFactura = chkFicheroEFactura.Value
    oAccion.ModificarAccion_optFicheroEFactura
End Sub

''' <summary>
''' evento que salta cuando se procude el click en el check para no aprobar si hay discrepancias
''' </summary>
''' <param name></param>
''' <returns></returns>
''' <remarks>Llamada desde:frmFlujosDetalleAccion </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
Private Sub chkNoAprobarSiDiscrepancias_Click()
    oAccion.NoAprobarSiDiscrepancias = chkNoAprobarSiDiscrepancias.Value
    If m_bActivado Then
        oAccion.ModificarAccion_optNoAprobarSiDiscrepancias
        frmFlujos.HayCambios
    End If
End Sub

Private Function ValidarVisibilidadConfiguracionCamposRol() As Boolean
    With sdbgNotificados
        Select Case .Columns("TIPO").Value
            Case TipoNotificadoAccion.Peticionario
                Select Case .Columns("COD_NOTIFICADO").Value
                    Case TipoNotificadoAccion.Peticionario, TipoNotificadoAccion.UltimoAprobador
                        ValidarVisibilidadConfiguracionCamposRol = m_oNotificadoEnEdicion.ValidarVisibilidadConfiguracionCamposRol(.Columns("ID").Value, .Columns("COD_NOTIFICADO").Value, OrigenNotificadoAccion.Accion)
                    Case Else
                        ValidarVisibilidadConfiguracionCamposRol = False
                End Select
            Case TipoNotificadoAccion.Rol
                ValidarVisibilidadConfiguracionCamposRol = True
            Case Else
                ValidarVisibilidadConfiguracionCamposRol = False
        End Select
    End With
End Function

Public Function Formulario() As CFormulario
    If m_oFormulario Is Nothing Then
        Set m_oFormulario = oFSGSRaiz.Generar_CFormulario
        m_oFormulario.Id = lIdFormulario
        m_oFormulario.CargarTodosLosGrupos
    End If
    Set Formulario = m_oFormulario
End Function

