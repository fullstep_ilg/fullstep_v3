VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosRoles 
   Caption         =   "DRoles"
   ClientHeight    =   6855
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   13635
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFlujosRoles.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6855
   ScaleWidth      =   13635
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBDropDown sdbddBloqueDefecto 
      Height          =   555
      Left            =   12240
      TabIndex        =   7
      Top             =   5520
      Width           =   2535
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "BLOQUE_ASIGNA"
      Columns(0).Name =   "BLOQUE_ASIGNA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   13573
      Columns(1).Caption=   "BLOQUE_ASIGNA_DEN"
      Columns(1).Name =   "BLOQUE_ASIGNA_DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4471
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddContactos 
      Height          =   555
      Left            =   10200
      TabIndex        =   6
      Top             =   4800
      Width           =   3735
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   13573
      Columns(1).Caption=   "CONTACTO"
      Columns(1).Name =   "CONTACTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddRestringir 
      Height          =   555
      Left            =   9120
      TabIndex        =   5
      Top             =   3960
      Width           =   3735
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "RESTRINGIR"
      Columns(0).Name =   "RESTRINGIR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   13573
      Columns(1).Caption=   "RESTRINGIR_DEN"
      Columns(1).Name =   "RESTRINGIR_DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddComoAsignar 
      Height          =   555
      Left            =   6600
      TabIndex        =   4
      Top             =   3360
      Width           =   3135
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COMO_ASIGNAR"
      Columns(0).Name =   "COMO_ASIGNAR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   13573
      Columns(1).Caption=   "COMO_ASIGNAR_DEN"
      Columns(1).Name =   "COMO_ASIGNAR_DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "ID_ROL_ASIGNA"
      Columns(2).Name =   "ID_ROL_ASIGNA"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddBloqueAsigna 
      Height          =   555
      Left            =   5520
      TabIndex        =   3
      Top             =   2760
      Width           =   2535
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "BLOQUE_ASIGNA"
      Columns(0).Name =   "BLOQUE_ASIGNA"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   13573
      Columns(1).Caption=   "BLOQUE_ASIGNA_DEN"
      Columns(1).Name =   "BLOQUE_ASIGNA_DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4471
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddCuandoAsigna 
      Height          =   555
      Left            =   4080
      TabIndex        =   2
      Top             =   2160
      Width           =   3975
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "CUANDO_ASIGNAR"
      Columns(0).Name =   "CUANDO_ASIGNAR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   13573
      Columns(1).Caption=   "CUANDO_ASIGNAR_DEN"
      Columns(1).Name =   "CUANDO_ASIGNAR_DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   7011
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin VB.CommandButton cmdAnyadirRol 
      Height          =   312
      Left            =   0
      Picture         =   "frmFlujosRoles.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   60
      Width           =   432
   End
   Begin VB.CommandButton cmdEliminarRol 
      Height          =   312
      Left            =   480
      Picture         =   "frmFlujosRoles.frx":0D34
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   60
      Width           =   432
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddVisibilidadObservador 
      Height          =   555
      Left            =   3720
      TabIndex        =   8
      Top             =   4080
      Width           =   3000
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "CUANDO_ASIGNAR"
      Columns(0).Name =   "CUANDO_ASIGNAR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   2
      Columns(0).FieldLen=   256
      Columns(1).Width=   5292
      Columns(1).Caption=   "CUANDO_ASIGNAR_DEN"
      Columns(1).Name =   "CUANDO_ASIGNAR_DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5292
      _ExtentY        =   979
      _StockProps     =   77
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgRoles 
      Height          =   6390
      Left            =   -30
      TabIndex        =   9
      Top             =   420
      Width           =   12720
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HeadLines       =   3
      Col.Count       =   47
      stylesets.count =   2
      stylesets(0).Name=   "Gris"
      stylesets(0).BackColor=   -2147483633
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosRoles.frx":0DC6
      stylesets(1).Name=   "Normal"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosRoles.frx":0DE2
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   2
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      SplitterPos     =   1
      SplitterVisible =   -1  'True
      Columns.Count   =   47
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "DROL"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "TIPO"
      Columns(2).Name =   "TIPO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   2
      Columns(2).FieldLen=   256
      Columns(3).Width=   1773
      Columns(3).Caption=   "DPeticionario"
      Columns(3).Name =   "PETICIONARIO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   11
      Columns(3).FieldLen=   256
      Columns(3).Style=   2
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "DComprador"
      Columns(4).Name =   "COMPRADOR"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   1640
      Columns(5).Caption=   "DProveedor"
      Columns(5).Name =   "PROVEEDOR"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   11
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(6).Width=   1852
      Columns(6).Caption=   "DObservador"
      Columns(6).Name =   "OBSERVADOR"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   11
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "CUANDO_ASIGNAR"
      Columns(7).Name =   "CUANDO_ASIGNAR"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   2
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "GESTOR"
      Columns(8).Name =   "GESTOR"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   11
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "RECEPTOR"
      Columns(9).Name =   "RECEPTOR"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   11
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(10).Width=   2646
      Columns(10).Caption=   "D�Cuando se asigna?"
      Columns(10).Name=   "CUANDO_ASIGNAR_DEN"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(10).Style=   3
      Columns(11).Width=   3200
      Columns(11).Visible=   0   'False
      Columns(11).Caption=   "BLOQUE_ASIGNA"
      Columns(11).Name=   "BLOQUE_ASIGNA"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   3
      Columns(11).FieldLen=   256
      Columns(12).Width=   1931
      Columns(12).Caption=   "DEtapa"
      Columns(12).Name=   "BLOQUE_ASIGNA_DEN"
      Columns(12).DataField=   "Column 12"
      Columns(12).DataType=   8
      Columns(12).FieldLen=   256
      Columns(12).Style=   3
      Columns(13).Width=   3200
      Columns(13).Visible=   0   'False
      Columns(13).Caption=   "COMO_ASIGNAR"
      Columns(13).Name=   "COMO_ASIGNAR"
      Columns(13).DataField=   "Column 13"
      Columns(13).DataType=   2
      Columns(13).FieldLen=   256
      Columns(14).Width=   2593
      Columns(14).Caption=   "D�C�mo se asigna?"
      Columns(14).Name=   "COMO_ASIGNAR_DEN"
      Columns(14).DataField=   "Column 14"
      Columns(14).DataType=   8
      Columns(14).FieldLen=   256
      Columns(14).Style=   3
      Columns(15).Width=   3200
      Columns(15).Visible=   0   'False
      Columns(15).Caption=   "ROL_ASIGNA"
      Columns(15).Name=   "ROL_ASIGNA"
      Columns(15).DataField=   "Column 15"
      Columns(15).DataType=   3
      Columns(15).FieldLen=   256
      Columns(16).Width=   3200
      Columns(16).Visible=   0   'False
      Columns(16).Caption=   "CAMPO"
      Columns(16).Name=   "CAMPO"
      Columns(16).DataField=   "Column 16"
      Columns(16).DataType=   3
      Columns(16).FieldLen=   256
      Columns(17).Width=   1667
      Columns(17).Caption=   "DCampo del formulario"
      Columns(17).Name=   "CAMPO_BTN"
      Columns(17).DataField=   "Column 17"
      Columns(17).DataType=   8
      Columns(17).FieldLen=   256
      Columns(17).Style=   4
      Columns(17).ButtonsAlways=   -1  'True
      Columns(18).Width=   3200
      Columns(18).Visible=   0   'False
      Columns(18).Caption=   "RESTRINGIR"
      Columns(18).Name=   "RESTRINGIR"
      Columns(18).DataField=   "Column 18"
      Columns(18).DataType=   2
      Columns(18).FieldLen=   256
      Columns(19).Width=   2672
      Columns(19).Caption=   "DRestringir la asignaci�n:"
      Columns(19).Name=   "RESTRINGIR_DEN"
      Columns(19).DataField=   "Column 19"
      Columns(19).DataType=   8
      Columns(19).FieldLen=   256
      Columns(19).Style=   3
      Columns(20).Width=   2064
      Columns(20).Caption=   "DUnidad org. / Departamento / Equipo / Materiales"
      Columns(20).Name=   "RESTRINGIR_BTN"
      Columns(20).DataField=   "Column 20"
      Columns(20).DataType=   8
      Columns(20).FieldLen=   256
      Columns(20).Style=   4
      Columns(20).ButtonsAlways=   -1  'True
      Columns(21).Width=   3200
      Columns(21).Visible=   0   'False
      Columns(21).Caption=   "PER"
      Columns(21).Name=   "PER"
      Columns(21).DataField=   "Column 21"
      Columns(21).DataType=   8
      Columns(21).FieldLen=   256
      Columns(22).Width=   3200
      Columns(22).Visible=   0   'False
      Columns(22).Caption=   "PROVE"
      Columns(22).Name=   "PROVE"
      Columns(22).DataField=   "Column 22"
      Columns(22).DataType=   8
      Columns(22).FieldLen=   256
      Columns(23).Width=   1667
      Columns(23).Caption=   "DPersona / Proveedor"
      Columns(23).Name=   "PET_PER_PROVE_DEN"
      Columns(23).DataField=   "Column 23"
      Columns(23).DataType=   8
      Columns(23).FieldLen=   256
      Columns(23).Style=   1
      Columns(24).Width=   3200
      Columns(24).Visible=   0   'False
      Columns(24).Caption=   "CONTACTO"
      Columns(24).Name=   "CONTACTO"
      Columns(24).DataField=   "Column 24"
      Columns(24).DataType=   3
      Columns(24).FieldLen=   256
      Columns(25).Width=   3200
      Columns(25).Caption=   "DContacto del Proveedor"
      Columns(25).Name=   "CONTACTO_DEN"
      Columns(25).DataField=   "Column 25"
      Columns(25).DataType=   17
      Columns(25).FieldLen=   256
      Columns(26).Width=   3200
      Columns(26).Visible=   0   'False
      Columns(26).Caption=   "VISIBILIDAD_OBSERVADOR"
      Columns(26).Name=   "VISIBILIDAD_OBSERVADOR"
      Columns(26).DataField=   "Column 26"
      Columns(26).DataType=   8
      Columns(26).FieldLen=   256
      Columns(27).Width=   3200
      Columns(27).Caption=   "VISIBILIDAD_OBSERVADOR_DEN"
      Columns(27).Name=   "VISIBILIDAD_OBSERVADOR_DEN"
      Columns(27).DataField=   "Column 27"
      Columns(27).DataType=   8
      Columns(27).FieldLen=   256
      Columns(27).Locked=   -1  'True
      Columns(27).Style=   3
      Columns(28).Width=   1588
      Columns(28).Caption=   "dEtapas rol observador"
      Columns(28).Name=   "ETAPAS_OBSERVADOR_BTN"
      Columns(28).Alignment=   1
      Columns(28).CaptionAlignment=   0
      Columns(28).DataField=   "Column 28"
      Columns(28).DataType=   8
      Columns(28).FieldLen=   256
      Columns(28).Style=   4
      Columns(28).ButtonsAlways=   -1  'True
      Columns(29).Width=   3200
      Columns(29).Visible=   0   'False
      Columns(29).Caption=   "CONF_BLOQUE_DEF"
      Columns(29).Name=   "CONF_BLOQUE_DEF"
      Columns(29).DataField=   "Column 29"
      Columns(29).DataType=   3
      Columns(29).FieldLen=   256
      Columns(30).Width=   1614
      Columns(30).Caption=   "DVisibilidad por defecto"
      Columns(30).Name=   "CONF_BLOQUE_DEF_DEN"
      Columns(30).Alignment=   1
      Columns(30).CaptionAlignment=   0
      Columns(30).DataField=   "Column 30"
      Columns(30).DataType=   8
      Columns(30).FieldLen=   256
      Columns(30).Style=   4
      Columns(30).ButtonsAlways=   -1  'True
      Columns(31).Width=   3200
      Columns(31).Visible=   0   'False
      Columns(31).Caption=   "FECACT"
      Columns(31).Name=   "FECACT"
      Columns(31).DataField=   "Column 31"
      Columns(31).DataType=   7
      Columns(31).FieldLen=   256
      Columns(32).Width=   3200
      Columns(32).Visible=   0   'False
      Columns(32).Caption=   "UON1"
      Columns(32).Name=   "UON1"
      Columns(32).DataField=   "Column 32"
      Columns(32).DataType=   8
      Columns(32).FieldLen=   256
      Columns(33).Width=   3200
      Columns(33).Visible=   0   'False
      Columns(33).Caption=   "UON2"
      Columns(33).Name=   "UON2"
      Columns(33).DataField=   "Column 33"
      Columns(33).DataType=   8
      Columns(33).FieldLen=   256
      Columns(34).Width=   3200
      Columns(34).Visible=   0   'False
      Columns(34).Caption=   "UON3"
      Columns(34).Name=   "UON3"
      Columns(34).DataField=   "Column 34"
      Columns(34).DataType=   8
      Columns(34).FieldLen=   256
      Columns(35).Width=   3200
      Columns(35).Visible=   0   'False
      Columns(35).Caption=   "DEP"
      Columns(35).Name=   "DEP"
      Columns(35).DataField=   "Column 35"
      Columns(35).DataType=   8
      Columns(35).FieldLen=   256
      Columns(36).Width=   3200
      Columns(36).Visible=   0   'False
      Columns(36).Caption=   "EQP"
      Columns(36).Name=   "EQP"
      Columns(36).DataField=   "Column 36"
      Columns(36).DataType=   8
      Columns(36).FieldLen=   256
      Columns(37).Width=   3200
      Columns(37).Caption=   "DVER_FLUJO"
      Columns(37).Name=   "VER_FLUJO"
      Columns(37).DataField=   "Column 37"
      Columns(37).DataType=   11
      Columns(37).FieldLen=   256
      Columns(37).Style=   2
      Columns(38).Width=   3200
      Columns(38).Caption=   "DVER_DETALLE_PER"
      Columns(38).Name=   "VER_DETALLE_PER"
      Columns(38).DataField=   "Column 38"
      Columns(38).DataType=   11
      Columns(38).FieldLen=   256
      Columns(38).Style=   2
      Columns(39).Width=   3200
      Columns(39).Caption=   "SUSTITUCION"
      Columns(39).Name=   "SUSTITUCION"
      Columns(39).Alignment=   2
      Columns(39).CaptionAlignment=   2
      Columns(39).DataField=   "Column 39"
      Columns(39).DataType=   8
      Columns(39).FieldLen=   256
      Columns(39).Style=   4
      Columns(39).ButtonsAlways=   -1  'True
      Columns(40).Width=   3200
      Columns(40).Visible=   0   'False
      Columns(40).Caption=   "SUSTITUTO"
      Columns(40).Name=   "SUSTITUTO"
      Columns(40).DataField=   "Column 40"
      Columns(40).DataType=   8
      Columns(40).FieldLen=   256
      Columns(41).Width=   3200
      Columns(41).Visible=   0   'False
      Columns(41).Caption=   "TIPO_SUSTITUCION"
      Columns(41).Name=   "TIPO_SUSTITUCION"
      Columns(41).DataField=   "Column 41"
      Columns(41).DataType=   2
      Columns(41).FieldLen=   256
      Columns(42).Width=   3200
      Columns(42).Visible=   0   'False
      Columns(42).Caption=   "COD_EMPRESAS"
      Columns(42).Name=   "COD_EMPRESAS"
      Columns(42).DataField=   "Column 42"
      Columns(42).DataType=   8
      Columns(42).FieldLen=   256
      Columns(43).Width=   3200
      Columns(43).Caption=   "EMPRESAS"
      Columns(43).Name=   "EMPRESAS"
      Columns(43).DataField=   "Column 43"
      Columns(43).DataType=   8
      Columns(43).FieldLen=   256
      Columns(43).Style=   4
      Columns(43).ButtonsAlways=   -1  'True
      Columns(44).Width=   3200
      Columns(44).Caption=   "PERMITIR_TRASLADOS"
      Columns(44).Name=   "PERMITIR_TRASLADOS"
      Columns(44).DataField=   "Column 44"
      Columns(44).DataType=   11
      Columns(44).FieldLen=   256
      Columns(44).Style=   2
      Columns(45).Width=   3200
      Columns(45).Visible=   0   'False
      Columns(45).Caption=   "UON0"
      Columns(45).Name=   "UON0"
      Columns(45).DataField=   "Column 45"
      Columns(45).DataType=   8
      Columns(45).FieldLen=   256
      Columns(46).Width=   3200
      Columns(46).Visible=   0   'False
      Columns(46).Caption=   "TIPO_APROBADOR"
      Columns(46).Name=   "TIPO_APROBADOR"
      Columns(46).DataField=   "Column 46"
      Columns(46).DataType=   8
      Columns(46).FieldLen=   256
      _ExtentX        =   22428
      _ExtentY        =   11271
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFlujosRoles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Variables Publicas
Public lIdFlujo As Long
Public lIdFormulario As Long
Public m_bModifFlujo As Boolean
Public g_iSolicitudTipoTipo As Integer
Public g_lSolicitud As Long
Public g_bActualizarAprobadores As Boolean
Public g_vFecAct As Variant
'Variable Privadas
Private m_oRolEnEdicion As CPMRol
Private m_oRolAnyadir As CPMRol
Private m_oParticipantesAnyadir As CPMParticipantes
Private m_oIBAseDatosEnEdicion As IBaseDatos
Private m_oIBaseDatos As IBaseDatos

Private m_oRoles As CPMRoles

Private m_sDen As String
Private m_sPeticionario As String
Private m_sComprador As String
Private m_sProveedor As String
Private m_sObservador As String
Private m_sCuandoAsignar As String
Private m_sBloqueAsigna As String
Private m_sComoAsignar As String
Private m_sCampo As String
Private m_sRestringir As String
Private m_sRestringirBtn As String
Private m_sPetPerProve As String
Private m_sContacto As String
Private m_sVisibilidad As String
Private m_sVer_flujo As String
Private m_sSustitucion As String
Private m_sVerDetallePer As String
Private m_sTituloPermitirTraslados As String
Private m_arrAprobadoresPorEmpresa() As String 'Para las solicitudes de pedido(10), este array contendra el aprobador por cada empresa
Private m_arrAprobadoresPorPeticionario() As String 'Para las solicitudes de pedido(10), este array contendra el aprobador por cada peticionario de esa solicitud
Private m_arrAprobadoresPorCentroCoste() As String 'Para las solicitudes de pedido(10), este array contendra el aprobador por cada centro de coste de esa solicitud

Private m_bRUON As Boolean
Private m_bRDep As Boolean

Private m_bModError As Boolean
Private m_bAnyaError As Boolean
Private m_bError As Boolean
Private m_bHayReceptor As Boolean
Private m_bHayGestor As Boolean
Private m_bHayPeticionario As Boolean 'Se utiliza para las solicitudes SolicitudDePedidoCatalogo(10), en las que solo se permite tener un peticionario

Private m_arMensajesRol(6) As String  'literales para basmensajes
Private m_arCuandoAsignar(0 To 3) As String
Private m_arComoAsignar(0 To 4) As String
Private m_arRestringir(0 To 7) As String
Private m_arVisibilidadObservador(0 To 2) As String
Private m_sVisibilidadRolObservador As String
Private m_sEtapasRolObservador As String
Private m_sTituloEmpresa As String
Private m_sTituloGestor As String
Private m_sTituloReceptor As String
Private m_sNoMasPeticionarios As String

Private m_sEmpresas As String 'Cadena con las empresas
Private m_iRestringir As Integer
Private m_sSelecObservador As String 'Texto 'Seleccione Observador' para Caption de frmSOLSelPersona
Private m_bNoModificaFlujo As Boolean 'En el caso de que un cambio en la grid de roles no deba modificar el flujo usaremos esta variable
Public Property Get arrAprobadoresPorEmpresa() As String()
    arrAprobadoresPorEmpresa = m_arrAprobadoresPorEmpresa
End Property

Public Property Let arrAprobadoresPorEmpresa(ByRef Datos() As String)
    m_arrAprobadoresPorEmpresa = Datos
End Property

Public Property Get arrAprobadoresPorPeticionario() As String()
    arrAprobadoresPorPeticionario = m_arrAprobadoresPorPeticionario
End Property

Public Property Let arrAprobadoresPorPeticionario(ByRef Datos() As String)
    m_arrAprobadoresPorPeticionario = Datos
End Property

Public Property Get arrAprobadoresPorCentroCoste() As String()
    arrAprobadoresPorCentroCoste = m_arrAprobadoresPorCentroCoste
End Property

Public Property Let arrAprobadoresPorCentroCoste(ByRef Datos() As String)
    m_arrAprobadoresPorCentroCoste = Datos
End Property

Private Sub cmdAnyadirRol_Click()
'    cmdAnyadirRol.Enabled = False
    
    sdbgRoles.Scroll 0, sdbgRoles.Rows - sdbgRoles.Row
    
    If sdbgRoles.VisibleRows > 0 Then
        
        If sdbgRoles.VisibleRows >= sdbgRoles.Rows Then
            If sdbgRoles.VisibleRows = sdbgRoles.Rows Then
                sdbgRoles.Row = sdbgRoles.Rows - 1
            Else
                sdbgRoles.Row = sdbgRoles.Rows
    
            End If
        Else
            sdbgRoles.Row = sdbgRoles.Rows - (sdbgRoles.Rows - sdbgRoles.VisibleRows) - 1
        End If
        
    End If
    sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
    If Me.Visible Then sdbgRoles.SetFocus
End Sub

Private Sub cmdEliminarRol_Click()
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit
    
    If sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolReceptor Then
        m_bHayReceptor = False
    End If
    
    If sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolGestor Then
        m_bHayGestor = False
    End If
    
    If sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolPeticionario Then
        m_bHayPeticionario = False
    End If
    
    If sdbgRoles.IsAddRow Then
        If sdbgRoles.DataChanged Then
            sdbgRoles.CancelUpdate
        End If
    Else
        If sdbgRoles.Rows = 0 Then Exit Sub
        
        Screen.MousePointer = vbHourglass
        
        If sdbgRoles.SelBookmarks.Count = 1 Then
            irespuesta = oMensajes.PreguntaEliminar(sdbgRoles.Columns("DEN").Value)
            
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
            
            Set m_oRolEnEdicion = m_oRoles.Item(CStr(sdbgRoles.Columns("ID").Value))
            
            Set m_oIBAseDatosEnEdicion = m_oRolEnEdicion
            teserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos
            
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                Screen.MousePointer = vbNormal
                
                If Me.Visible Then sdbgRoles.SetFocus
                Exit Sub
            
            Else
                If sdbgRoles.DataChanged Then
                    sdbgRoles.CancelUpdate
                End If
                m_oRoles.Remove (CStr(sdbgRoles.Columns("ID").Value))
                If sdbgRoles.AddItemRowIndex(sdbgRoles.Bookmark) > -1 Then
                    sdbgRoles.RemoveItem (sdbgRoles.AddItemRowIndex(sdbgRoles.Bookmark))
                Else
                    sdbgRoles.RemoveItem (0)
                End If
                If IsEmpty(sdbgRoles.GetBookmark(0)) Then
                    sdbgRoles.Bookmark = sdbgRoles.GetBookmark(-1)
                Else
                    sdbgRoles.Bookmark = sdbgRoles.GetBookmark(0)
                End If
                basSeguridad.RegistrarAccion AccionesSummit.ACCPMRolElim, "ID:" & m_oRolEnEdicion.Id
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                frmFlujos.HayCambios
            End If

            Set m_oIBAseDatosEnEdicion = Nothing
            Set m_oRolEnEdicion = Nothing
            
        End If
            
        sdbgRoles.SelBookmarks.RemoveAll
    
        Screen.MousePointer = vbNormal
    
    End If
    cmdAnyadirRol.Enabled = True And m_bModifFlujo
End Sub

Private Sub Form_Load()
    m_bNoModificaFlujo = False
    m_bHayGestor = False
    m_bHayReceptor = False
    m_bHayPeticionario = False
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    sdbgRoles.RowHeight = 464.8819
    
    PonerFieldSeparator Me
    
    CargarRoles
    If g_iSolicitudTipoTipo = TipoSolicitud.Contrato Then
        CargarEmpresas
    Else
        sdbgRoles.Columns("EMPRESAS").Visible = False
    End If
    
    'Si el tipo de solicitud es factura:
    '           - si el m�dulo SM est� activado, se muestra la columna GESTOR
    '           - se muestra la columna RECEPTOR
    If g_iSolicitudTipoTipo = TipoSolicitud.AUTOFACTURA Then
        If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
            sdbgRoles.Columns("GESTOR").Visible = True
        End If
        sdbgRoles.Columns("RECEPTOR").Visible = True
    End If
End Sub

Private Sub ActualizarHayReceptor(Optional ByVal Valor_Old As Boolean)
    If sdbgRoles.Columns("RECEPTOR").Value = True And m_bHayReceptor = True Then
        sdbgRoles.Columns("RECEPTOR").Value = False
    ElseIf sdbgRoles.Columns("RECEPTOR").Value = True And m_bHayReceptor = False Then
        m_bHayReceptor = True
    ElseIf sdbgRoles.Columns("RECEPTOR").Value = False And m_bHayReceptor = True And Valor_Old = True Then
        m_bHayReceptor = False
    End If
End Sub

Private Sub ActualizarHayGestor(Optional ByVal Valor_Old As Boolean)
    If sdbgRoles.Columns("GESTOR").Value = True And m_bHayGestor = True Then
        sdbgRoles.Columns("GESTOR").Value = False
    ElseIf sdbgRoles.Columns("GESTOR").Value = True And m_bHayGestor = False Then
        m_bHayGestor = True
    ElseIf sdbgRoles.Columns("GESTOR").Value = False And m_bHayGestor = True And Valor_Old = True Then
        m_bHayGestor = False
    End If
End Sub


'Revisado por: SRA (08/11/2011)
'Descripcion: carga las descripciones en el idioma correspondiente
'Llamada desde:Form_Load
'Tiempo ejecucion:0,2seg.
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSROLES, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        Me.caption = Ador(0).Value '1
        Ador.MoveNext
        m_sDen = Ador(0).Value
        m_arMensajesRol(0) = Ador(0).Value
        Ador.MoveNext
        m_sComprador = Ador(0).Value
        Ador.MoveNext
        m_sProveedor = Ador(0).Value
        Ador.MoveNext
        m_sCuandoAsignar = Ador(0).Value '5
        m_arMensajesRol(1) = Ador(0).Value
        Ador.MoveNext
        m_sBloqueAsigna = Ador(0).Value
        m_arMensajesRol(2) = Ador(0).Value
        Ador.MoveNext
        m_sComoAsignar = Ador(0).Value
        m_arMensajesRol(3) = Ador(0).Value
        Ador.MoveNext
        m_sCampo = Ador(0).Value
        m_arMensajesRol(4) = Ador(0).Value
        Ador.MoveNext
        m_sRestringir = Ador(0).Value
        Ador.MoveNext
        m_sRestringirBtn = Ador(0).Value '10
        m_arMensajesRol(5) = Ador(0).Value
        Ador.MoveNext
        m_sPetPerProve = Ador(0).Value
        m_arMensajesRol(6) = Ador(0).Value
        Ador.MoveNext
        m_sContacto = Ador(0).Value
        Ador.MoveNext
        m_sVisibilidad = Ador(0).Value
        Ador.MoveNext
        cmdAnyadirRol.ToolTipText = Ador(0).Value
        Ador.MoveNext
        cmdEliminarRol.ToolTipText = Ador(0).Value '15
        
        Ador.MoveNext
        m_arCuandoAsignar(1) = Ador(0).Value
        Ador.MoveNext
        m_arCuandoAsignar(2) = Ador(0).Value
        Ador.MoveNext
        m_arComoAsignar(1) = Ador(0).Value
        Ador.MoveNext
        m_arComoAsignar(2) = Ador(0).Value
        Ador.MoveNext
        m_arComoAsignar(3) = Ador(0).Value '20
        Ador.MoveNext
        m_arRestringir(1) = Ador(0).Value
        Ador.MoveNext
        m_arRestringir(2) = Ador(0).Value
        Ador.MoveNext
        m_arRestringir(3) = Ador(0).Value
        Ador.MoveNext
        m_arRestringir(4) = Ador(0).Value
        Ador.MoveNext
        m_arRestringir(5) = Ador(0).Value '25
        Ador.MoveNext
        m_arRestringir(6) = Ador(0).Value
        Ador.MoveNext
        m_arRestringir(7) = Ador(0).Value
        Ador.MoveNext
        m_sPeticionario = Ador(0).Value
        Ador.MoveNext
        m_sVer_flujo = Ador(0).Value
        Ador.MoveNext
        m_sSustitucion = Ador(0).Value  '30 - Sustituci�n
        Ador.MoveNext
        m_arVisibilidadObservador(1) = Ador(0).Value '31  Visibilidad completa
        Ador.MoveNext
        m_arVisibilidadObservador(2) = Ador(0).Value '32 Visibilidad parcial
        Ador.MoveNext
        m_sVisibilidadRolObservador = Ador(0).Value '33 Visibilidad etapas rol observador
        Ador.MoveNext
        m_sEtapasRolObservador = Ador(0).Value '34 Etapas rol observador
        Ador.MoveNext
        m_sObservador = Ador(0).Value '35 Observador
        Ador.MoveNext
        m_sVerDetallePer = Ador(0).Value '36 - Ver datos participantes flujo
        Ador.MoveNext
        m_sTituloEmpresa = Ador(0).Value '37 - Empresas
        Ador.MoveNext
        m_sTituloPermitirTraslados = Ador(0).Value '38 - Permitir traslados
        Ador.MoveNext
        m_arCuandoAsignar(3) = Ador(0).Value '39 - Proveedor del contrato
        Ador.MoveNext
        m_sSelecObservador = Ador(0).Value '40 - Seleccione Observador
        Ador.MoveNext
        m_sTituloGestor = Ador(0).Value '41 - Gestor
        Ador.MoveNext
        m_sTituloReceptor = Ador(0).Value '42 - Receptor
        Ador.MoveNext
        m_sNoMasPeticionarios = Ador(0).Value '43
        Ador.MoveNext
        m_arComoAsignar(4) = Ador(0).Value '44 - Est� asociado a un servicio externo
        Ador.Close
    End If
End Sub

''' <summary>Configuracion de las acciones de seguridad del formulario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFRestUOPetic)) Is Nothing) Then
        m_bRUON = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFRestDepPetic)) Is Nothing) Then
        m_bRDep = True
    End If
    
    If m_bModifFlujo Then
         sdbddCuandoAsigna.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").DropDownHwnd = sdbddCuandoAsigna.hWnd
        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Style = ssStyleComboBox
        sdbddBloqueAsigna.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").DropDownHwnd = sdbddBloqueAsigna.hWnd
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleComboBox
        sdbddComoAsignar.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").DropDownHwnd = sdbddComoAsignar.hWnd
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleComboBox
        sdbddRestringir.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = sdbddRestringir.hWnd
        sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleComboBox
        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Style = ssStyleButton
        sdbddContactos.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgRoles.Columns("CONTACTO_DEN").DropDownHwnd = sdbddContactos.hWnd
        sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleComboBox
        sdbddVisibilidadObservador.AddItem 0 & Chr(m_lSeparador) & ""
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").DropDownHwnd = sdbddVisibilidadObservador.hWnd
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleComboBox
    Else
        cmdAnyadirRol.Enabled = False
        cmdEliminarRol.Enabled = False
        
        sdbgRoles.AllowAddNew = False
        
        sdbgRoles.Columns("DEN").Locked = True
        sdbgRoles.Columns("PETICIONARIO").Locked = True
        sdbgRoles.Columns("COMPRADOR").Locked = True
        sdbgRoles.Columns("PROVEEDOR").Locked = True
        sdbgRoles.Columns("OBSERVADOR").Locked = True
        sdbgRoles.Columns("GESTOR").Locked = True
        sdbgRoles.Columns("RECEPTOR").Locked = True
        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = True
        
        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
        sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
        sdbgRoles.Columns("CONTACTO_DEN").Locked = True
        sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Locked = True
        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEdit
        
        sdbgRoles.AllowDelete = False
    End If
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: carga los roles definidos
'Llamada desde:Form_Load, sdbgRoles_Change, sdbgRoles_click
'Tiempo ejecucion:0,2seg.
Private Sub CargarRoles()
    Dim oRol As CPMRol
    Dim bPeticionario As Boolean
    Dim bPeticionarioProve As Boolean
    Dim bComprador As Boolean
    Dim bProveedor As Boolean
    Dim bObservador As Boolean
    Dim bGestor As Boolean
    Dim bReceptor As Boolean
    Dim bVer_flujo As Boolean
    Dim oBloque As CBloque
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    Dim sBloque As String
    Dim sComoAsignar As String
    Dim oRol_aux As CPMRol
    Dim sCampo As String
    Dim sRestringir As String
    Dim sRestringirDen As String
    Dim oPer As CPersona
    Dim oProve As CProveedor
    Dim sContacto As String
    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3
    Dim oDep As CDepartamento
    Dim sPetPerProve As String
    Dim sBloqueDef As String
    Dim sSustitucion As String
    Dim sVisibilidad As String
    
    sdbgRoles.RemoveAll
    
    Set m_oRoles = oFSGSRaiz.Generar_CPMRoles
    m_oRoles.CargarRolesWorkflow (lIdFlujo)

    
    For Each oRol In m_oRoles
        If oRol.VER_FLUJO = True Then
            bVer_flujo = True
        Else
            bVer_flujo = False
        End If
            
        sPetPerProve = ""
        Select Case oRol.Tipo
            Case TipoPMRol.RolComprador, TipoPMRol.RolUsuario
                bPeticionario = False
                bComprador = (oRol.Tipo = TipoPMRol.RolComprador)
                bProveedor = False
                bObservador = False
                bGestor = False
                bReceptor = False
                bPeticionarioProve = False
                sContacto = ""
                If oRol.CuandoAsignar = CuandoAsignarPMRol.Ahora Then
                    If g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoCatalogo Or g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto Then
                        'Si es este tipo de solicitud, indicamos que tiene que tener aprobadores
                            If oRol.TipoAprobador = TipoAprobadorSolicitudPedido.PersonaDirecta Then
                                'Si tipo de aprobador Persona Directa, se sacara la denominacion de la persona, o departamento o UON
                                If oRol.Per <> "" Then
                                    Set oPer = oFSGSRaiz.Generar_CPersona
                                    oPer.Cod = oRol.Per
                                    Set oIBaseDatos = oPer
                                    teserror = oIBaseDatos.IniciarEdicion
                                    If teserror.NumError <> TESnoerror Then
                                        basErrores.TratarError teserror
                                        Set oIBaseDatos = Nothing
                                        Set oPer = Nothing
                                        Exit Sub
                                    Else
                                        sPetPerProve = oPer.nombre & " " & oPer.Apellidos & " ( " & oPer.mail & " )"
                                    End If
                                ElseIf oRol.Dep <> "" Then
                                    Set oDep = oFSGSRaiz.Generar_CDepartamento
                                    oDep.Cod = oRol.Dep
                                    Set oIBaseDatos = oDep
                                    teserror = oIBaseDatos.IniciarEdicion
                                    If teserror.NumError <> TESnoerror Then
                                        basErrores.TratarError teserror
                                        Set oIBaseDatos = Nothing
                                        Set oDep = Nothing
                                        Exit Sub
                                    Else
                                        sPetPerProve = ""
                                        If oRol.UON1 <> "" Then
                                            sPetPerProve = sPetPerProve & oRol.UON1 & " - "
                                            If oRol.UON2 <> "" Then
                                                sPetPerProve = sPetPerProve & oRol.UON2 & " - "
                                                If oRol.UON3 <> "" Then
                                                    sPetPerProve = sPetPerProve & oRol.UON3 & " - "
                                                End If
                                            End If
                                        End If
                                        sPetPerProve = sPetPerProve & oDep.Cod & " - " & oDep.Den
                                    End If
                                ElseIf oRol.UON3 <> "" Then
                                    Set oUON3 = oFSGSRaiz.generar_CUnidadOrgNivel3
                                    oUON3.CodUnidadOrgNivel1 = oRol.UON1
                                    oUON3.CodUnidadOrgNivel2 = oRol.UON2
                                    oUON3.Cod = oRol.UON3
                                    Set oIBaseDatos = oUON3
                                    teserror = oIBaseDatos.IniciarEdicion
                                    If teserror.NumError <> TESnoerror Then
                                        basErrores.TratarError teserror
                                        Set oIBaseDatos = Nothing
                                        Set oUON3 = Nothing
                                        Exit Sub
                                    Else
                                        sPetPerProve = oUON3.CodUnidadOrgNivel1 & " - " & oUON3.CodUnidadOrgNivel2 & " - " & oUON3.Cod & " - " & oUON3.Den
                                    End If
                                ElseIf oRol.UON2 <> "" Then
                                    Set oUON2 = oFSGSRaiz.generar_CUnidadOrgNivel2
                                    oUON2.CodUnidadOrgNivel1 = oRol.UON1
                                    oUON2.Cod = oRol.UON2
                                    Set oIBaseDatos = oUON2
                                    teserror = oIBaseDatos.IniciarEdicion
                                    If teserror.NumError <> TESnoerror Then
                                        basErrores.TratarError teserror
                                        Set oIBaseDatos = Nothing
                                        Set oUON2 = Nothing
                                        Exit Sub
                                    Else
                                        sPetPerProve = oUON2.CodUnidadOrgNivel1 & " - " & oUON2.Cod & " - " & oUON2.Den
                                    End If
                                ElseIf oRol.UON1 <> "" Then
                                    Set oUON1 = oFSGSRaiz.generar_CUnidadOrgNivel1
                                    oUON1.Cod = oRol.UON1
                                    Set oIBaseDatos = oUON1
                                    teserror = oIBaseDatos.IniciarEdicion
                                    If teserror.NumError <> TESnoerror Then
                                        basErrores.TratarError teserror
                                        Set oIBaseDatos = Nothing
                                        Set oUON1 = Nothing
                                        Exit Sub
                                    Else
                                        sPetPerProve = oUON1.Cod & " - " & oUON1.Den
                                    End If
                                End If
                            Else
                                'Si es otro tipo de aprobador, se indica que tiene datos con los puntos suspensivos
                                sPetPerProve = "..."
                            End If
                    ElseIf Not Trim(oRol.Per) = "" Then
                        Set oPer = oFSGSRaiz.Generar_CPersona
                        oPer.Cod = oRol.Per
                        Set oIBaseDatos = oPer
                        teserror = oIBaseDatos.IniciarEdicion
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Set oIBaseDatos = Nothing
                            Set oPer = Nothing
                            Exit Sub
                        Else
                            sPetPerProve = oPer.nombre & " " & oPer.Apellidos & " ( " & oPer.mail & " )"
                        End If
                    Else
                        sPetPerProve = ""
                    End If
                Else
                    sPetPerProve = ""
                End If
            Case TipoPMRol.RolProveedor
                bPeticionario = False
                bComprador = False
                bProveedor = True
                bObservador = False
                bGestor = False
                bReceptor = False
                bPeticionarioProve = False
                sContacto = ""
                If oRol.CuandoAsignar = CuandoAsignarPMRol.Ahora Then
                    Set oProve = oFSGSRaiz.generar_CProveedor
                    oProve.Cod = oRol.Prove
                    Set oIBaseDatos = oProve
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oProve = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oProve.Den
                        oProve.CargarTodosLosContactos , , , , , , , , , , , , , , , , , , oRol.Contacto
                        If Not oProve.Contactos.Item(CStr(oRol.Contacto)) Is Nothing Then
                            sContacto = oProve.Contactos.Item(CStr(oRol.Contacto)).nombre & " " & oProve.Contactos.Item(CStr(oRol.Contacto)).Apellidos
                        End If
                    End If
                Else
                    sPetPerProve = ""
                End If
                
            Case TipoPMRol.RolPeticionario
                bPeticionario = True
                bComprador = False
                bProveedor = False
                bObservador = False
                bGestor = False
                bReceptor = False
                bPeticionarioProve = False
                sContacto = ""
                If oRol.Per <> "" Then
                    Set oPer = oFSGSRaiz.Generar_CPersona
                    oPer.Cod = oRol.Per
                    Set oIBaseDatos = oPer
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oPer = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oPer.nombre & " " & oPer.Apellidos & " ( " & oPer.mail & " )"
                    End If
                ElseIf oRol.Dep <> "" Then
                    Set oDep = oFSGSRaiz.Generar_CDepartamento
                    oDep.Cod = oRol.Dep
                    Set oIBaseDatos = oDep
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oDep = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = ""
                        If oRol.UON1 <> "" Then
                            sPetPerProve = sPetPerProve & oRol.UON1 & " - "
                            If oRol.UON2 <> "" Then
                                sPetPerProve = sPetPerProve & oRol.UON2 & " - "
                                If oRol.UON3 <> "" Then
                                    sPetPerProve = sPetPerProve & oRol.UON3 & " - "
                                End If
                            End If
                        End If
                        sPetPerProve = sPetPerProve & oDep.Cod & " - " & oDep.Den
                    End If
                ElseIf oRol.UON3 <> "" Then
                    Set oUON3 = oFSGSRaiz.generar_CUnidadOrgNivel3
                    oUON3.CodUnidadOrgNivel1 = oRol.UON1
                    oUON3.CodUnidadOrgNivel2 = oRol.UON2
                    oUON3.Cod = oRol.UON3
                    Set oIBaseDatos = oUON3
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oUON3 = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oUON3.CodUnidadOrgNivel1 & " - " & oUON3.CodUnidadOrgNivel2 & " - " & oUON3.Cod & " - " & oUON3.Den
                    End If
                ElseIf oRol.UON2 <> "" Then
                    Set oUON2 = oFSGSRaiz.generar_CUnidadOrgNivel2
                    oUON2.CodUnidadOrgNivel1 = oRol.UON1
                    oUON2.Cod = oRol.UON2
                    Set oIBaseDatos = oUON2
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oUON2 = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oUON2.CodUnidadOrgNivel1 & " - " & oUON2.Cod & " - " & oUON2.Den
                    End If
                ElseIf oRol.UON1 <> "" Then
                    Set oUON1 = oFSGSRaiz.generar_CUnidadOrgNivel1
                    oUON1.Cod = oRol.UON1
                    Set oIBaseDatos = oUON1
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oUON1 = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oUON1.Cod & " - " & oUON1.Den
                    End If
                End If
            
            Case TipoPMRol.RolObservador
                bPeticionario = False
                bComprador = False
                bProveedor = False
                bObservador = True
                bGestor = False
                bReceptor = False
                bPeticionarioProve = False
                sContacto = ""
                If oRol.Per <> "" Then
                    Set oPer = oFSGSRaiz.Generar_CPersona
                    oPer.Cod = oRol.Per
                    Set oIBaseDatos = oPer
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oPer = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oPer.nombre & " " & oPer.Apellidos & " ( " & oPer.mail & " )"
                    End If
                ElseIf oRol.Dep <> "" Then
                    Set oDep = oFSGSRaiz.Generar_CDepartamento
                    oDep.Cod = oRol.Dep
                    Set oIBaseDatos = oDep
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oDep = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = ""
                        If oRol.UON1 <> "" Then
                            sPetPerProve = sPetPerProve & oRol.UON1 & " - "
                            If oRol.UON2 <> "" Then
                                sPetPerProve = sPetPerProve & oRol.UON2 & " - "
                                If oRol.UON3 <> "" Then
                                    sPetPerProve = sPetPerProve & oRol.UON3 & " - "
                                End If
                            End If
                        End If
                        sPetPerProve = sPetPerProve & oDep.Cod & " - " & oDep.Den
                    End If
                ElseIf oRol.UON3 <> "" Then
                    Set oUON3 = oFSGSRaiz.generar_CUnidadOrgNivel3
                    oUON3.CodUnidadOrgNivel1 = oRol.UON1
                    oUON3.CodUnidadOrgNivel2 = oRol.UON2
                    oUON3.Cod = oRol.UON3
                    Set oIBaseDatos = oUON3
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oUON3 = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oUON3.CodUnidadOrgNivel1 & " - " & oUON3.CodUnidadOrgNivel2 & " - " & oUON3.Cod & " - " & oUON3.Den
                    End If
                ElseIf oRol.UON2 <> "" Then
                    Set oUON2 = oFSGSRaiz.generar_CUnidadOrgNivel2
                    oUON2.CodUnidadOrgNivel1 = oRol.UON1
                    oUON2.Cod = oRol.UON2
                    Set oIBaseDatos = oUON2
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oUON2 = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oUON2.CodUnidadOrgNivel1 & " - " & oUON2.Cod & " - " & oUON2.Den
                    End If
                ElseIf oRol.UON1 <> "" Then
                    Set oUON1 = oFSGSRaiz.generar_CUnidadOrgNivel1
                    oUON1.Cod = oRol.UON1
                    Set oIBaseDatos = oUON1
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oUON1 = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oUON1.Cod & " - " & oUON1.Den
                    End If
                ElseIf oRol.UON0 Then
                    sPetPerProve = gParametrosGenerales.gsDEN_UON0 'oRol.UON0
                End If
            Case TipoPMRol.RolGestor
                bPeticionario = False
                bComprador = False
                bProveedor = False
                bObservador = False
                bGestor = True
                bReceptor = False
                bPeticionarioProve = False
            Case TipoPMRol.RolReceptor
                bPeticionario = False
                bComprador = False
                bProveedor = False
                bObservador = False
                bGestor = False
                bReceptor = True
                bPeticionarioProve = False
            Case TipoPMRol.RolPeticionarioProveedor
                bPeticionario = True
                bProveedor = True
                bComprador = False
                bObservador = False
                bGestor = False
                bReceptor = False
                bPeticionarioProve = True
                sContacto = ""
                If oRol.Prove <> "" And oRol.CuandoAsignar = CuandoAsignarPMRol.Ahora Then
                    Set oProve = oFSGSRaiz.generar_CProveedor
                    oProve.Cod = oRol.Prove
                    Set oIBaseDatos = oProve
                    teserror = oIBaseDatos.IniciarEdicion
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Set oIBaseDatos = Nothing
                        Set oProve = Nothing
                        Exit Sub
                    Else
                        sPetPerProve = oProve.Den
                        If oRol.Contacto <> 0 Then
                            oProve.CargarTodosLosContactos , , , , , , , , , , , , , , , , , , oRol.Contacto
                            If Not oProve.Contactos.Item(CStr(oRol.Contacto)) Is Nothing Then
                                sContacto = oProve.Contactos.Item(CStr(oRol.Contacto)).nombre & " " & oProve.Contactos.Item(CStr(oRol.Contacto)).Apellidos
                            End If
                        End If
                    End If
                Else
                    sPetPerProve = ""
                End If
        End Select
                       
        sBloque = ""
        If oRol.CuandoAsignar = CuandoAsignarPMRol.EnEtapa And oRol.BloqueAsigna > 0 Then
            Set oBloque = oFSGSRaiz.Generar_CBloque
            oBloque.Id = oRol.BloqueAsigna
            Set oIBaseDatos = oBloque
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oIBaseDatos = Nothing
                Set oBloque = Nothing
                Exit Sub
            Else
                sBloque = oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
            End If
        End If
        
        sComoAsignar = ""
        If oRol.ComoAsignar = ComoAsignarPMRol.PorPMRol Then
            sComoAsignar = m_arComoAsignar(ComoAsignarPMRol.PorPMRol)
            Set oRol_aux = oFSGSRaiz.Generar_CPMRol
            oRol_aux.Id = oRol.RolAsigna
            Set oIBaseDatos = oRol_aux
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oIBaseDatos = Nothing
                Set oRol_aux = Nothing
                Exit Sub
            Else
                sComoAsignar = sComoAsignar & " " & oRol_aux.Den
            End If
        Else
            sComoAsignar = m_arComoAsignar(oRol.ComoAsignar)
        End If
                
        If oRol.Campo > 0 Then
            sCampo = "..."
        Else
            sCampo = ""
        End If
        
        sRestringir = ""
        If oRol.Restringir > 0 Then
            Select Case oRol.Restringir
                Case RestringirPMRol.ProvesEquipo
                    If oRol.Eqp <> "" Then
                        sRestringir = "..."
                    End If
                Case RestringirPMRol.ProvesMaterial, RestringirPMRol.Lista
                    If oRol.Participantes.Count > 0 Then
                        sRestringir = "..."
                    End If
                Case RestringirPMRol.Departamento
                    If oRol.Dep <> "" Then
                        sRestringir = "..."
                    End If
                Case RestringirPMRol.UON
                    If oRol.UON1 <> "" Then
                        sRestringir = "..."
                    End If
            End Select
        End If
        
        sBloqueDef = ""
        If oRol.ConfBloqueDef > 0 Then
            Set oBloque = oFSGSRaiz.Generar_CBloque
            oBloque.Id = oRol.ConfBloqueDef
            Set oIBaseDatos = oBloque
            teserror = oIBaseDatos.IniciarEdicion
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Set oIBaseDatos = Nothing
                Set oBloque = Nothing
                Exit Sub
            Else
                sBloqueDef = oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
            End If
        End If
        
        sSustitucion = ""
        If oRol.Sustituto <> "" Then
            sSustitucion = "..."
        Else
            sSustitucion = ""
        End If
        
        sVisibilidad = "..."

        Dim sEtapasVisibilidad  As String
        sEtapasVisibilidad = ""
        If oRol.VisibilidadObservador = VisibilidadParcial Then
            sEtapasVisibilidad = "..."
        End If
        
        
        Dim sRolEmpresas As String
        sRolEmpresas = ""
        If oRol.DevolverNumEmpresas > 0 Then
            sRolEmpresas = "..."
        End If
        If oRol.Tipo = RolPeticionario And oRol.Restringir = Lista Then
            sRestringirDen = ""
        Else
            sRestringirDen = m_arRestringir(oRol.Restringir)
        End If
                
        sdbgRoles.AddItem oRol.Id & Chr(m_lSeparador) & oRol.Den & Chr(m_lSeparador) & oRol.Tipo & Chr(m_lSeparador) & IIf(bPeticionario, 1, 0) & Chr(m_lSeparador) _
        & IIf(bComprador, 1, 0) & Chr(m_lSeparador) & IIf(bProveedor, 1, 0) & Chr(m_lSeparador) & IIf(bObservador, 1, 0) _
        & Chr(m_lSeparador) & oRol.CuandoAsignar & Chr(m_lSeparador) & bGestor & Chr(m_lSeparador) & bReceptor & Chr(m_lSeparador) & m_arCuandoAsignar(oRol.CuandoAsignar) _
        & Chr(m_lSeparador) & oRol.BloqueAsigna & Chr(m_lSeparador) & sBloque & Chr(m_lSeparador) & oRol.ComoAsignar & Chr(m_lSeparador) & sComoAsignar & Chr(m_lSeparador) _
        & oRol.RolAsigna & Chr(m_lSeparador) & oRol.Campo & Chr(m_lSeparador) & sCampo & Chr(m_lSeparador) & oRol.Restringir & Chr(m_lSeparador) & sRestringirDen _
        & Chr(m_lSeparador) & sRestringir & Chr(m_lSeparador) & oRol.Per & Chr(m_lSeparador) & oRol.Prove & Chr(m_lSeparador) & sPetPerProve & Chr(m_lSeparador) & oRol.Contacto _
        & Chr(m_lSeparador) & sContacto & Chr(m_lSeparador) & oRol.VisibilidadObservador & Chr(m_lSeparador) & m_arVisibilidadObservador(oRol.VisibilidadObservador) _
        & Chr(m_lSeparador) & sEtapasVisibilidad & Chr(m_lSeparador) & oRol.ConfBloqueDef & Chr(m_lSeparador) & sVisibilidad & Chr(m_lSeparador) & oRol.FECACT & Chr(m_lSeparador) _
        & oRol.UON1 & Chr(m_lSeparador) & oRol.UON2 & Chr(m_lSeparador) & oRol.UON3 & Chr(m_lSeparador) & oRol.Dep & Chr(m_lSeparador) & oRol.Eqp & Chr(m_lSeparador) & bVer_flujo _
        & Chr(m_lSeparador) & oRol.VerDetallePer & Chr(m_lSeparador) & sSustitucion & Chr(m_lSeparador) & oRol.Sustituto & Chr(m_lSeparador) & oRol.TipoSustitucion & Chr(m_lSeparador) _
        & oRol.CadenaEmpresas & Chr(m_lSeparador) & sRolEmpresas & Chr(m_lSeparador) & oRol.PermitirTraslados & Chr(m_lSeparador) & oRol.UON0 & Chr(m_lSeparador) & oRol.TipoAprobador
    Next
    Set oRol = Nothing
    Set oBloque = Nothing
    Set oIBaseDatos = Nothing
    Set oPer = Nothing
    Set oProve = Nothing
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    Set oDep = Nothing
End Sub

'Revisado por: SRA (08/11/2011)
'Descripcion: redimensiona los controles del formulario
'Llamada desde: al modificar el tama�o del formulario
'Tiempo ejecucion:0,1seg.
Private Sub Form_Resize()
    If Me.Height - 900 > 0 Then
        sdbgRoles.Height = Me.Height - 960
    End If
    If Me.Width - 150 > 0 Then
        sdbgRoles.Width = Me.Width - 150
    End If
    
    sdbgRoles.Columns("DEN").Width = sdbgRoles.Width * 0.1329
    sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Width = sdbgRoles.Width * 0.1099
    sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Width = sdbgRoles.Width * 0.0802
    sdbgRoles.Columns("COMO_ASIGNAR_DEN").Width = sdbgRoles.Width * 0.1077
    sdbgRoles.Columns("CAMPO_BTN").Width = sdbgRoles.Width * 0.0714
    sdbgRoles.Columns("RESTRINGIR_DEN").Width = sdbgRoles.Width * 0.111
    sdbgRoles.Columns("RESTRINGIR_BTN").Width = sdbgRoles.Width * 0.0857
    sdbgRoles.Columns("PET_PER_PROVE_DEN").Width = sdbgRoles.Width * 0.0692
    sdbgRoles.Columns("CONTACTO_DEN").Width = sdbgRoles.Width * 0.0802
    sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Width = sdbgRoles.Width * 0.0692
    sdbgRoles.Columns("VER_FLUJO").Width = sdbgRoles.Width * 0.0692
    sdbgRoles.Columns("VER_DETALLE_PER").Width = sdbgRoles.Width * 0.0692
    sdbgRoles.Columns("SUSTITUCION").Width = sdbgRoles.Width * 0.0714
    sdbgRoles.Columns("EMPRESAS").Width = sdbgRoles.Width * 0.0692
    sdbgRoles.Columns("PERMITIR_TRASLADOS").Width = sdbgRoles.Width * 0.0692
    sdbgRoles_RowColChange Null, 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim oRol As CPMRol
    Dim bSalir As Boolean
    m_bError = False
    If Me.sdbgRoles.DataChanged Then
        'Si la ventana est� minimizada se devuelve al estado normal porque si hay alg�n error en el sdbgRoles_BeforeUpdate y se saca un mensaje de error
        'estando la ventana minimizada la ventana del mensaje de error se queda inactiva y la aplicaci�n se queda bloqueada
        If Me.WindowState = vbMinimized Then Me.WindowState = vbNormal
        sdbgRoles.Update
        If m_bError Then
          Cancel = True
          If Me.Visible Then sdbgRoles.SetFocus
          Exit Sub
        End If
    End If
    
    If m_oRoles.Count > 0 Then
        bSalir = False
    Else
        bSalir = True
    End If
    For Each oRol In m_oRoles
        If oRol.Tipo = TipoPMRol.RolPeticionario Or oRol.Tipo = TipoPMRol.RolPeticionarioProveedor Then
            bSalir = True
            Exit For
        End If
    Next
    If Not bSalir Then
        oMensajes.FaltaPeticionario
        Cancel = 1
        Exit Sub
    End If
    
    Set m_oRolEnEdicion = Nothing
    Set m_oRolAnyadir = Nothing
    Set m_oParticipantesAnyadir = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    Set m_oIBaseDatos = Nothing
    
    Set m_oRoles = Nothing
    
    m_bModError = False
    m_bAnyaError = False
    m_bError = False

End Sub

Private Sub sdbddBloqueAsigna_CloseUp()
    If sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = "" Then Exit Sub

    sdbgRoles.Columns("BLOQUE_ASIGNA").Value = sdbddBloqueAsigna.Columns(0).Value
    sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = sdbddBloqueAsigna.Columns(1).Value
End Sub

Private Sub sdbddBloqueAsigna_DropDown()
    Dim oBloques As CBloques
    Dim oBloque As CBloque
    
    Screen.MousePointer = vbHourglass
    sdbddBloqueAsigna.RemoveAll
    
    Set oBloques = oFSGSRaiz.Generar_CBloques
    oBloques.CargarBloques basPublic.gParametrosInstalacion.gIdioma, lIdFlujo
    
    If oBloques.Count > 0 Then
        For Each oBloque In oBloques
            If oBloque.Tipo <> TipoBloque.final Then
                sdbddBloqueAsigna.AddItem oBloque.Id & Chr(m_lSeparador) & oBloque.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den
            End If
        Next
    Else
        sdbddBloqueAsigna.AddItem 0 & Chr(m_lSeparador) & ""
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddBloqueAsigna_InitColumnProps()
    sdbddBloqueAsigna.DataFieldList = "Column 0"
    sdbddBloqueAsigna.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddBloqueAsigna_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddBloqueAsigna.MoveFirst

    If sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value <> "" Then
        For i = 0 To sdbddBloqueAsigna.Rows - 1
            bm = sdbddBloqueAsigna.GetBookmark(i)
            If UCase(sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value) = UCase(Mid(sdbddBloqueAsigna.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value))) Then
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = Mid(sdbddBloqueAsigna.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value))
                sdbddBloqueAsigna.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddComoAsignar_CloseUp()
    If sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = "" Then Exit Sub
    
    sdbgRoles.Columns("COMO_ASIGNAR").Value = sdbddComoAsignar.Columns(0).Value
    sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = sdbddComoAsignar.Columns(1).Value
    sdbgRoles.Columns("ROL_ASIGNA").Value = sdbddComoAsignar.Columns(2).Value
    
    sdbgRoles.col = sdbgRoles.Columns("COMO_ASIGNAR_DEN").Position
    sdbgRoles_Change
End Sub

Private Sub sdbddComoAsignar_DropDown()
    Dim oRol As CPMRol
    Dim bAnyadirLista As Boolean
    
    Screen.MousePointer = vbHourglass
    sdbddComoAsignar.RemoveAll
    
    sdbddComoAsignar.AddItem ComoAsignarPMRol.PorCampo & Chr(m_lSeparador) & m_arComoAsignar(ComoAsignarPMRol.PorCampo) & Chr(m_lSeparador) & 0
    For Each oRol In m_oRoles
        If CStr(oRol.Id) <> sdbgRoles.Columns("ID").Value Then
            sdbddComoAsignar.AddItem ComoAsignarPMRol.PorPMRol & Chr(m_lSeparador) & m_arComoAsignar(ComoAsignarPMRol.PorPMRol) & " " & oRol.Den & Chr(m_lSeparador) & oRol.Id
        End If
    Next
    bAnyadirLista = True
    If sdbgRoles.Columns("PROVEEDOR").Value <> "" Then
        If CBool(sdbgRoles.Columns("PROVEEDOR").Value) Then
            bAnyadirLista = False
        End If
    End If
    
    If bAnyadirLista Then
        sdbddComoAsignar.AddItem ComoAsignarPMRol.PorLista & Chr(m_lSeparador) & m_arComoAsignar(ComoAsignarPMRol.PorLista) & Chr(m_lSeparador) & 0
    End If
    sdbddComoAsignar.AddItem ComoAsignarPMRol.PorServicioExt & Chr(m_lSeparador) & m_arComoAsignar(ComoAsignarPMRol.PorServicioExt) & Chr(m_lSeparador) & 0
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddComoAsignar_InitColumnProps()
    sdbddComoAsignar.DataFieldList = "Column 0"
    sdbddComoAsignar.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddComoAsignar_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddComoAsignar.MoveFirst

    If sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value <> "" Then
        For i = 0 To sdbddComoAsignar.Rows - 1
            bm = sdbddComoAsignar.GetBookmark(i)
            If UCase(sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value) = UCase(Mid(sdbddComoAsignar.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value))) Then
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = Mid(sdbddComoAsignar.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value))
                sdbddComoAsignar.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddContactos_CloseUp()
    If sdbgRoles.Columns("CONTACTO_DEN").Value = "" Then Exit Sub
    
    sdbgRoles.Columns("CONTACTO").Value = sdbddContactos.Columns(0).Value
    sdbgRoles.Columns("CONTACTO_DEN").Value = sdbddContactos.Columns(1).Value
    
End Sub

Private Sub sdbddContactos_DropDown()
    Dim oProve As CProveedor
    Dim oContacto As CContacto

    Screen.MousePointer = vbHourglass
    sdbddContactos.RemoveAll
    
    If sdbgRoles.Columns("PROVE").Value <> "" Then
        Set oProve = oFSGSRaiz.generar_CProveedor
        oProve.Cod = sdbgRoles.Columns("PROVE").Value
        oProve.CargarTodosLosContactos , , , True
        sdbddContactos.AddItem 0 & Chr(m_lSeparador) & " "
        For Each oContacto In oProve.Contactos
            sdbddContactos.AddItem oContacto.Id & Chr(m_lSeparador) & oContacto.nombre & " " & oContacto.Apellidos
        Next
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddContactos_InitColumnProps()
    sdbddContactos.DataFieldList = "Column 0"
    sdbddContactos.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddContactos_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddContactos.MoveFirst

    If sdbgRoles.Columns("CONTACTO_DEN").Value <> "" Then
        For i = 0 To sdbddContactos.Rows - 1
            bm = sdbddContactos.GetBookmark(i)
            If UCase(sdbgRoles.Columns("CONTACTO_DEN").Value) = UCase(Mid(sdbddContactos.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("CONTACTO_DEN").Value))) Then
                sdbgRoles.Columns("CONTACTO_DEN").Value = Mid(sdbddContactos.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("CONTACTO_DEN").Value))
                sdbddContactos.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddCuandoAsigna_CloseUp()
    Dim i, iCol As Integer
    If sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = "" Then Exit Sub
    
    sdbgRoles.Columns("CUANDO_ASIGNAR").Value = sdbddCuandoAsigna.Columns(0).Value
    sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = sdbddCuandoAsigna.Columns(1).Value
    
    For i = 0 To sdbgRoles.Cols
     If sdbgRoles.Columns(i).Name = "CUANDO_ASIGNAR_DEN" Then
        iCol = i
        Exit For
     End If
    Next
    
    sdbgRoles.col = iCol
    sdbgRoles_Change
End Sub

'Revisado por: blp. Fecha:03/08/2012
'Descripcion: asigna opciones al combo "Cuando Asigna" en el evento de desplegar
'Llamada desde:evento
'Tiempo ejecucion:0,2seg.
Private Sub sdbddCuandoAsigna_DropDown()

    Screen.MousePointer = vbHourglass
    sdbddCuandoAsigna.RemoveAll
    
    If Not sdbgRoles.Columns("OBSERVADOR").Value Then
                sdbddCuandoAsigna.AddItem CuandoAsignarPMRol.EnEtapa & Chr(m_lSeparador) & m_arCuandoAsignar(CuandoAsignarPMRol.EnEtapa)
    End If
    sdbddCuandoAsigna.AddItem CuandoAsignarPMRol.Ahora & Chr(m_lSeparador) & m_arCuandoAsignar(CuandoAsignarPMRol.Ahora)
    If (Me.sdbgRoles.Columns("PROVEEDOR").Value) And g_iSolicitudTipoTipo = TipoSolicitud.Contrato Then
        sdbddCuandoAsigna.AddItem CuandoAsignarPMRol.ProveedorDelContrato & Chr(m_lSeparador) & m_arCuandoAsignar(CuandoAsignarPMRol.ProveedorDelContrato)
    End If

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddCuandoAsigna_InitColumnProps()
    sdbddCuandoAsigna.DataFieldList = "Column 0"
    sdbddCuandoAsigna.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddCuandoAsigna_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddCuandoAsigna.MoveFirst

    If sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value <> "" Then
        For i = 0 To sdbddCuandoAsigna.Rows - 1
            bm = sdbddCuandoAsigna.GetBookmark(i)
            If UCase(sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value) = UCase(Mid(sdbddCuandoAsigna.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value))) Then
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = Mid(sdbddCuandoAsigna.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value))
                sdbddCuandoAsigna.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddRestringir_CloseUp()
    If sdbgRoles.Columns("RESTRINGIR_DEN").Value = "" Then Exit Sub
    If m_iRestringir = sdbddRestringir.Columns(0).Value Then Exit Sub 'Si no ha cambiado la seleccion del combo
    
    sdbgRoles.Columns("RESTRINGIR").Value = sdbddRestringir.Columns(0).Value
    sdbgRoles.Columns("RESTRINGIR_DEN").Value = sdbddRestringir.Columns(0).Value
    m_iRestringir = sdbddRestringir.Columns(0).Value
    sdbgRoles.col = sdbgRoles.Columns("RESTRINGIR_DEN").Position
    sdbgRoles_Change
End Sub

Private Sub sdbddRestringir_DropDown()
    Screen.MousePointer = vbHourglass
    sdbddRestringir.RemoveAll
    
    If sdbgRoles.Columns("TIPO").Value <> "" Then
        sdbddRestringir.AddItem 0 & Chr(m_lSeparador) & " "
        If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolProveedor Or CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionarioProveedor Then
            If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolProveedor And gParametrosGenerales.gbOblProveEqp Then
                sdbddRestringir.AddItem RestringirPMRol.ProvesEquipo & Chr(m_lSeparador) & m_arRestringir(RestringirPMRol.ProvesEquipo)
            End If
            sdbddRestringir.AddItem RestringirPMRol.ProvesMaterial & Chr(m_lSeparador) & m_arRestringir(RestringirPMRol.ProvesMaterial)
        Else
            sdbddRestringir.AddItem RestringirPMRol.Departamento & Chr(m_lSeparador) & m_arRestringir(RestringirPMRol.Departamento)
            sdbddRestringir.AddItem RestringirPMRol.UON & Chr(m_lSeparador) & m_arRestringir(RestringirPMRol.UON)
            sdbddRestringir.AddItem RestringirPMRol.DepartamentoPMRol & Chr(m_lSeparador) & m_arRestringir(RestringirPMRol.DepartamentoPMRol)
            sdbddRestringir.AddItem RestringirPMRol.UONPMRol & Chr(m_lSeparador) & m_arRestringir(RestringirPMRol.UONPMRol)
            sdbddRestringir.AddItem RestringirPMRol.Lista & Chr(m_lSeparador) & m_arRestringir(RestringirPMRol.Lista)
        End If
    End If
    
    If sdbgRoles.Columns("RESTRINGIR_DEN").Value = "" Then
        m_iRestringir = 0
    Else
         m_iRestringir = sdbgRoles.Columns("RESTRINGIR_DEN").Value
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddRestringir_InitColumnProps()
    sdbddRestringir.DataFieldList = "Column 0"
    sdbddRestringir.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddRestringir_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddRestringir.MoveFirst

    If sdbgRoles.Columns("RESTRINGIR_DEN").Value <> "" Then
        For i = 0 To sdbddRestringir.Rows - 1
            bm = sdbddRestringir.GetBookmark(i)
            If UCase(sdbgRoles.Columns("RESTRINGIR_DEN").Value) = UCase(Mid(sdbddRestringir.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("RESTRINGIR_DEN").Value))) Then
                sdbgRoles.Columns("RESTRINGIR_DEN").Value = Mid(sdbddRestringir.Columns(1).CellText(bm), 1, Len(sdbgRoles.Columns("RESTRINGIR_DEN").Value))
                sdbddRestringir.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbddVisibilidadObservador_CloseUp()
    If sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = "" Then Exit Sub
    If CInt(sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value) = sdbddVisibilidadObservador.Columns(0).Value Then Exit Sub
    
    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = sdbddVisibilidadObservador.Columns(0).Value
    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = sdbddVisibilidadObservador.Columns(1).Value
    
    If sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = VisibilidadObservadorPMRol.VisibilidadParcial Then
        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = False
        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = "..."
    Else
        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = ""
        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
    End If
    If sdbgRoles.IsAddRow And sdbgRoles.DataChanged Then
        'Se guarda para tener el id del rol
        sdbgRoles.Update
        If m_bError Then
            Exit Sub
        End If
    End If
    ''A�adir a la tabla PM_ROL_OBS_ETAPAS las etapas a las que el observador tiene visibilidad.
    Dim oBloques As CBloques
    Set oBloques = oFSGSRaiz.Generar_CBloques
    oBloques.AsignarEtapasObservador sdbgRoles.Columns("ID").Value, lIdFlujo, , True
    Set oBloques = Nothing

End Sub

Private Sub sdbddVisibilidadObservador_DropDown()
    Screen.MousePointer = vbHourglass
    sdbddVisibilidadObservador.RemoveAll
    
    sdbddVisibilidadObservador.AddItem VisibilidadObservadorPMRol.VisibilidadCompleta & Chr(m_lSeparador) & m_arVisibilidadObservador(VisibilidadObservadorPMRol.VisibilidadCompleta)
    sdbddVisibilidadObservador.AddItem VisibilidadObservadorPMRol.VisibilidadParcial & Chr(m_lSeparador) & m_arVisibilidadObservador(VisibilidadObservadorPMRol.VisibilidadParcial)
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddVisibilidadObservador_InitColumnProps()
    sdbddVisibilidadObservador.DataFieldList = "Column 0"
    sdbddVisibilidadObservador.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbgRoles_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgRoles.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgRoles.GetBookmark(0)) Then
        sdbgRoles.Bookmark = sdbgRoles.GetBookmark(-1)
    Else
        sdbgRoles.Bookmark = sdbgRoles.GetBookmark(0)
    End If
    If Me.Visible Then sdbgRoles.SetFocus
End Sub

Private Sub sdbgRoles_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
        
    If m_bAnyaError = False And m_bModifFlujo Then
        cmdAnyadirRol.Enabled = True
    End If
    
    If IsEmpty(sdbgRoles.GetBookmark(0)) Then
        sdbgRoles.Bookmark = sdbgRoles.GetBookmark(-1)
    Else
        sdbgRoles.Bookmark = sdbgRoles.GetBookmark(0)
    End If
End Sub

Private Sub sdbgRoles_AfterUpdate(RtnDispErrMsg As Integer)
    
    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False And m_bModifFlujo Then
        cmdAnyadirRol.Enabled = True
    End If
       
    If Not m_oRolEnEdicion Is Nothing Then
        sdbgRoles.Columns("FECACT").Value = m_oRolEnEdicion.FECACT
    End If
    
    Set m_oRolEnEdicion = Nothing
End Sub

Private Sub sdbgRoles_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub



'Revisado por: SRA (08/11/2011)
'Descripcion: realiza una comprobaciones antes de realizar la actualizaci�n
'Llamada desde: al modificar sdbgRoles
'Tiempo ejecucion:0,3seg.
Private Sub sdbgRoles_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim oRol As CPMRol
    Dim bNoValido As Boolean
    
    sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
    
    'Comprobar datos Obligatorios
    If sdbgRoles.Columns("DEN").Value = "" Then
        oMensajes.NoValido m_arMensajesRol(0)
        Cancel = True
        GoTo Salir
    End If
    
    'Comprobar duplicidad de nombre
    For Each oRol In m_oRoles
        If oRol.Id <> sdbgRoles.Columns("ID").Value And oRol.Den = sdbgRoles.Columns("DEN").Value Then
            oMensajes.DatoDuplicado2 m_arMensajesRol(0), ""
            Cancel = True
            GoTo Salir
        End If
    Next
    
    If sdbgRoles.Columns("GESTOR").Value = "0" And sdbgRoles.Columns("RECEPTOR").Value = "0" Then
        If sdbgRoles.Columns("CUANDO_ASIGNAR").Value = "" Or sdbgRoles.Columns("CUANDO_ASIGNAR").Value = "0" Then
            oMensajes.NoValido m_arMensajesRol(1)
            Cancel = True
            GoTo Salir
        End If
    End If
    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa And _
            (sdbgRoles.Columns("BLOQUE_ASIGNA").Value = "" Or sdbgRoles.Columns("BLOQUE_ASIGNA").Value = "0") Then
        oMensajes.NoValido m_arMensajesRol(2)
        Cancel = True
        GoTo Salir
    End If
    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa And _
            (sdbgRoles.Columns("COMO_ASIGNAR").Value = "" Or sdbgRoles.Columns("COMO_ASIGNAR").Value = "0") Then
        oMensajes.NoValido m_arMensajesRol(3)
        Cancel = True
        GoTo Salir
    End If
    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa And _
            CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorPMRol And _
            (sdbgRoles.Columns("ROL_ASIGNA").Value = "" Or sdbgRoles.Columns("ROL_ASIGNA").Value = "0") Then
        oMensajes.NoValido m_arMensajesRol(3)
        Cancel = True
        GoTo Salir
    End If
    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa And _
            CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo And _
            (sdbgRoles.Columns("CAMPO").Value = "" Or sdbgRoles.Columns("CAMPO").Value = "0") Then
        oMensajes.NoValido m_arMensajesRol(4)
        Cancel = True
        GoTo Salir
    End If
    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa And _
            CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) <> ComoAsignarPMRol.PorCampo And _
            sdbgRoles.Columns("RESTRINGIR").Value <> "" And sdbgRoles.Columns("RESTRINGIR").Value <> "0" Then
        bNoValido = False
        Select Case CInt(sdbgRoles.Columns("RESTRINGIR").Value)
            Case RestringirPMRol.ProvesEquipo
                If sdbgRoles.Columns("EQP").Value = "" Then
                    bNoValido = True
                End If
            Case RestringirPMRol.ProvesMaterial
                If sdbgRoles.IsAddRow Then
                    If Not m_oParticipantesAnyadir Is Nothing Then
                        If m_oParticipantesAnyadir.Count = 0 Then
                            bNoValido = True
                        End If
                    Else
                        bNoValido = True
                    End If
                Else
                    If Not m_oRolEnEdicion Is Nothing Then
                        If Not m_oRolEnEdicion.Participantes Is Nothing Then
                            If m_oRolEnEdicion.Participantes.Count = 0 Then
                                bNoValido = True
                            End If
                        Else
                            bNoValido = True
                        End If
                    End If
                End If
            Case RestringirPMRol.Departamento
                If sdbgRoles.Columns("DEP").Value = "" Then
                    bNoValido = True
                End If
            Case RestringirPMRol.UON
                If sdbgRoles.Columns("UON1").Value = "" Then
                    bNoValido = True
                End If
            Case RestringirPMRol.DepartamentoPMRol
            Case RestringirPMRol.UONPMRol
            Case RestringirPMRol.Lista
                If sdbgRoles.IsAddRow Then
                    If Not m_oParticipantesAnyadir Is Nothing Then
                        If m_oParticipantesAnyadir.Count = 0 Then
                            bNoValido = True
                        End If
                    Else
                        bNoValido = True
                    End If
                Else
                    If Not m_oRolEnEdicion Is Nothing Then
                        If Not m_oRolEnEdicion.Participantes Is Nothing Then
                            If m_oRolEnEdicion.Participantes.Count = 0 Then
                                bNoValido = True
                            End If
                        Else
                            bNoValido = True
                        End If
                    End If
                End If
        End Select
        If bNoValido Then
            oMensajes.NoValido m_arMensajesRol(5)
            Cancel = True
            GoTo Salir
        End If
    End If

    If CInt(sdbgRoles.Columns("TIPO").Value) = 0 Then 'se ha quedado empty pq es 1ra linea no pet y no prove
        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
    End If
    
    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.Ahora Then
        Select Case CInt(sdbgRoles.Columns("TIPO").Value)
            Case TipoPMRol.RolComprador, TipoPMRol.RolUsuario
                Select Case g_iSolicitudTipoTipo
                    Case TipoSolicitud.SolicitudDePedidoCatalogo, TipoSolicitud.SolicitudDePedidoContraPedidoAbierto
                        If sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = "" Then
                            oMensajes.NoValido m_arMensajesRol(6)
                            Cancel = True
                            GoTo Salir
                        End If
                    Case Else
                        If sdbgRoles.Columns("PER").Value = "" Then
                            oMensajes.NoValido m_arMensajesRol(6)
                            Cancel = True
                            GoTo Salir
                        End If
                End Select
                
            Case TipoPMRol.RolProveedor
                If sdbgRoles.Columns("PROVE").Value = "" Then
                    oMensajes.NoValido m_arMensajesRol(6)
                    Cancel = True
                    GoTo Salir
                End If
            Case TipoPMRol.RolPeticionario, TipoPMRol.RolObservador
                If sdbgRoles.Columns("RESTRINGIR").Value <> RestringirPMRol.Lista Then
                    If sdbgRoles.Columns("PER").Value = "" And _
                    sdbgRoles.Columns("UON1").Value = "" And _
                    sdbgRoles.Columns("UON2").Value = "" And _
                    sdbgRoles.Columns("UON3").Value = "" And _
                    sdbgRoles.Columns("DEP").Value = "" Then
                        If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolObservador Then
                            If sdbgRoles.Columns("UON0").Value = "" Then
                                oMensajes.NoValido m_arMensajesRol(6)
                                Cancel = True
                                GoTo Salir
                            End If
                        Else
                            'Es peticionario
                            Select Case g_iSolicitudTipoTipo
                                Case TipoSolicitud.SolicitudDePedidoCatalogo, TipoSolicitud.SolicitudDePedidoContraPedidoAbierto
                                    'Permite no introducir una persona/unidad organizativa/departamento como peticionario
                                Case Else
                                    oMensajes.NoValido m_arMensajesRol(6)
                                    Cancel = True
                                    GoTo Salir
                            End Select
                        End If
                    End If
                End If
            Case TipoPMRol.RolPeticionarioProveedor
                If sdbgRoles.Columns("RESTRINGIR").Value <> "" And sdbgRoles.Columns("RESTRINGIR").Value <> "0" Then
                    bNoValido = False
                    Select Case CInt(sdbgRoles.Columns("RESTRINGIR").Value)
                        Case RestringirPMRol.ProvesMaterial
                            If sdbgRoles.IsAddRow Then
                                If Not m_oParticipantesAnyadir Is Nothing Then
                                    If m_oParticipantesAnyadir.Count = 0 Then
                                        bNoValido = True
                                    End If
                                Else
                                    bNoValido = True
                                End If
                            Else
                                If Not m_oRolEnEdicion Is Nothing Then
                                    If Not m_oRolEnEdicion.Participantes Is Nothing Then
                                        If m_oRolEnEdicion.Participantes.Count = 0 Then
                                            bNoValido = True
                                        End If
                                    Else
                                        bNoValido = True
                                    End If
                                End If
                            End If
                    End Select
                    If bNoValido Then
                        oMensajes.NoValido m_arMensajesRol(5)
                        Cancel = True
                        GoTo Salir
                    End If
                End If
        End Select
    End If
    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa And _
            CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) <> ComoAsignarPMRol.PorCampo And _
            sdbgRoles.Columns("RESTRINGIR").Value <> "" And sdbgRoles.Columns("RESTRINGIR").Value <> "0" Then
        If ((CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolComprador Or CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolUsuario) And _
                (CInt(sdbgRoles.Columns("RESTRINGIR").Value) = RestringirPMRol.ProvesEquipo Or CInt(sdbgRoles.Columns("RESTRINGIR").Value) = RestringirPMRol.ProvesMaterial)) Or _
           (CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolProveedor And _
                (CInt(sdbgRoles.Columns("RESTRINGIR").Value) <> RestringirPMRol.ProvesEquipo And CInt(sdbgRoles.Columns("RESTRINGIR").Value) <> RestringirPMRol.ProvesMaterial)) Then
            oMensajes.NoValido m_arMensajesRol(5)
            Cancel = True
            GoTo Salir
        End If
    End If
    
    If sdbgRoles.Columns("PETICIONARIO").Value And Not sdbgRoles.Columns("PROVEEDOR").Value Then
        If m_bHayPeticionario And (g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoCatalogo Or g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto) Then
            'Si es este tipo de solicitus, solo se podra tener un peticionario
            sdbgRoles.Columns("PETICIONARIO").Value = False
            MsgBox m_sNoMasPeticionarios, vbExclamation
            Cancel = True
            Exit Sub
        End If
    End If
    
    'Guardar Datos
     If sdbgRoles.IsAddRow Then
        Set m_oRolAnyadir = oFSGSRaiz.Generar_CPMRol
        m_oRolAnyadir.Workflow = lIdFlujo
        m_oRolAnyadir.Den = sdbgRoles.Columns("DEN").Value
        m_oRolAnyadir.Tipo = CInt(sdbgRoles.Columns("TIPO").Value)
        If m_oRolAnyadir.Tipo = TipoPMRol.RolObservador Then
            m_oRolAnyadir.VisibilidadObservador = CInt(sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value)
        End If
        m_oRolAnyadir.CuandoAsignar = CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value)
        m_oRolAnyadir.Restringir = CInt(sdbgRoles.Columns("RESTRINGIR").Value)
        'Aprobadores de una solicitud de tipo pedido(Tipo 10)
        If sdbgRoles.Columns("TIPO_APROBADOR").Value = "" Or sdbgRoles.Columns("TIPO_APROBADOR").Value = 0 Then
            m_oRolAnyadir.TipoAprobador = 0
        Else
            m_oRolAnyadir.ActualizarAprobadores = g_bActualizarAprobadores 'Se le indica al rol si tiene que actualizar los aprobadores
            m_oRolAnyadir.TipoAprobador = sdbgRoles.Columns("TIPO_APROBADOR").Value
            m_oRolAnyadir.arrAprobadoresPorEmpresa = arrAprobadoresPorEmpresa
            m_oRolAnyadir.arrAprobadoresPorPeticionario = arrAprobadoresPorPeticionario
            m_oRolAnyadir.arrAprobadoresPorCentroCoste = arrAprobadoresPorCentroCoste
            Erase m_arrAprobadoresPorEmpresa, m_arrAprobadoresPorPeticionario, m_arrAprobadoresPorCentroCoste
            g_bActualizarAprobadores = False 'Una vez indicado que se actualicen se pone a False para no volver a grabarlos en una posterior modificacion del rol
        End If
        Select Case m_oRolAnyadir.CuandoAsignar
            Case CuandoAsignarPMRol.Ahora, CuandoAsignarPMRol.ProveedorDelContrato
                Select Case m_oRolAnyadir.Tipo
                    Case TipoPMRol.RolComprador, TipoPMRol.RolUsuario
                        m_oRolAnyadir.Per = sdbgRoles.Columns("PER").Value
                    Case TipoPMRol.RolProveedor
                        m_oRolAnyadir.Prove = sdbgRoles.Columns("PROVE").Value
                        If sdbgRoles.Columns("CONTACTO").Value <> "" Then
                            m_oRolAnyadir.Contacto = CLng(sdbgRoles.Columns("CONTACTO").Value)
                        End If
                    Case TipoPMRol.RolPeticionario, TipoPMRol.RolObservador
                        If m_oRolAnyadir.Restringir = Lista Then
                            Set m_oRolAnyadir.Participantes = m_oParticipantesAnyadir
                        Else
                            m_oRolAnyadir.Per = sdbgRoles.Columns("PER").Value
                            m_oRolAnyadir.UON3 = sdbgRoles.Columns("UON3").Value
                            m_oRolAnyadir.UON2 = sdbgRoles.Columns("UON2").Value
                            m_oRolAnyadir.UON1 = sdbgRoles.Columns("UON1").Value
                            If m_oRolAnyadir.Tipo = TipoPMRol.RolObservador Then _
                                m_oRolAnyadir.UON0 = CBool(sdbgRoles.Columns("UON0").Value)
                            m_oRolAnyadir.Dep = sdbgRoles.Columns("DEP").Value
                        End If
                    Case TipoPMRol.RolPeticionarioProveedor
                        If m_oRolAnyadir.Restringir = RestringirPMRol.ProvesMaterial Then
                            Set m_oRolAnyadir.Participantes = m_oParticipantesAnyadir
                        Else
                            If sdbgRoles.Columns("PROVE").Value <> "" Then
                                m_oRolAnyadir.Prove = sdbgRoles.Columns("PROVE").Value
                                If sdbgRoles.Columns("CONTACTO").Value <> "" Then
                                    m_oRolAnyadir.Contacto = CLng(sdbgRoles.Columns("CONTACTO").Value)
                                End If
                            End If
                        End If
                End Select
            Case CuandoAsignarPMRol.EnEtapa
                m_oRolAnyadir.BloqueAsigna = CLng(sdbgRoles.Columns("BLOQUE_ASIGNA").Value)
                m_oRolAnyadir.ComoAsignar = CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value)
                Select Case m_oRolAnyadir.ComoAsignar
                    Case ComoAsignarPMRol.PorCampo
                        m_oRolAnyadir.Campo = CLng(sdbgRoles.Columns("CAMPO").Value)
                    Case ComoAsignarPMRol.PorPMRol
                        m_oRolAnyadir.RolAsigna = CLng(sdbgRoles.Columns("ROL_ASIGNA").Value)
                    Case ComoAsignarPMRol.PorLista
                        m_oRolAnyadir.Restringir = RestringirPMRol.Lista 'No deber�a hacer falta
                End Select
                m_oRolAnyadir.Restringir = CInt(sdbgRoles.Columns("RESTRINGIR").Value)
                Select Case m_oRolAnyadir.Restringir
                    Case RestringirPMRol.ProvesEquipo
                        m_oRolAnyadir.Eqp = sdbgRoles.Columns("EQP").Value
                    Case RestringirPMRol.ProvesMaterial
                        Set m_oRolAnyadir.Participantes = m_oParticipantesAnyadir
                    Case RestringirPMRol.Departamento
                        m_oRolAnyadir.UON1 = sdbgRoles.Columns("UON1").Value
                        m_oRolAnyadir.UON2 = sdbgRoles.Columns("UON2").Value
                        m_oRolAnyadir.UON3 = sdbgRoles.Columns("UON3").Value
                        m_oRolAnyadir.Dep = sdbgRoles.Columns("DEP").Value
                    Case RestringirPMRol.UON
                        m_oRolAnyadir.UON1 = sdbgRoles.Columns("UON1").Value
                        m_oRolAnyadir.UON2 = sdbgRoles.Columns("UON2").Value
                        m_oRolAnyadir.UON3 = sdbgRoles.Columns("UON3").Value
                    Case RestringirPMRol.DepartamentoPMRol
                    Case RestringirPMRol.UONPMRol
                    Case RestringirPMRol.Lista
                        Set m_oRolAnyadir.Participantes = m_oParticipantesAnyadir
                End Select
        End Select
        m_oRolAnyadir.ConfBloqueDef = sdbgRoles.Columns("CONF_BLOQUE_DEF").Value
        m_oRolAnyadir.Sustituto = sdbgRoles.Columns("SUSTITUTO").Value
        m_oRolAnyadir.TipoSustitucion = sdbgRoles.Columns("TIPO_SUSTITUCION").Value
        m_oRolAnyadir.VerDetallePer = sdbgRoles.Columns("VER_DETALLE_PER").Value
        m_oRolAnyadir.VER_FLUJO = sdbgRoles.Columns("VER_FLUJO").Value
        m_oRolAnyadir.PermitirTraslados = sdbgRoles.Columns("PERMITIR_TRASLADOS").Value
        m_oRolAnyadir.Gestor = sdbgRoles.Columns("GESTOR").Value
        m_oRolAnyadir.Receptor = sdbgRoles.Columns("RECEPTOR").Value
        
        Set m_oIBAseDatosEnEdicion = m_oRolAnyadir
        teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            m_bAnyaError = True
            Cancel = True
            GoTo Salir
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCPMRolAnya, "ID:" & m_oRolAnyadir.Id
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            If Not m_bNoModificaFlujo Then frmFlujos.HayCambios
        End If
        
        m_oRolAnyadir.CadenaEmpresas = sdbgRoles.Columns("COD_EMPRESAS").Value
        m_oRolAnyadir.ActualizarEmpresas
        
        sdbgRoles.Columns("ID").Value = m_oRolAnyadir.Id
        If m_oRolAnyadir.Tipo = TipoPMRol.RolObservador Then
        ''A�adir a la tabla PM_ROL_OBS_ETAPAS las etapas a las que el observador tiene visibilidad.
            Dim oBloques As CBloques
            Set oBloques = oFSGSRaiz.Generar_CBloques
            oBloques.AsignarEtapasObservador sdbgRoles.Columns("ID").Value, lIdFlujo
            Set oBloques = Nothing
        End If
        
        sdbgRoles.Columns("FECACT").Value = m_oRolAnyadir.FECACT
        m_oRoles.AddRol m_oRolAnyadir
        m_bAnyaError = False
        'Si el Rol es de Tipo Peticionario, lo asignamos a la etapa PETICIONARIO
        

    Else
        If Not m_oRolEnEdicion Is Nothing Then
            m_oRolEnEdicion.Den = sdbgRoles.Columns("DEN").Value
            m_oRolEnEdicion.Tipo = CInt(sdbgRoles.Columns("TIPO").Value)
            m_oRolEnEdicion.CuandoAsignar = CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value)
            m_oRolEnEdicion.Restringir = CInt(sdbgRoles.Columns("RESTRINGIR").Value)
            'Aprobadores de una solicitud de tipo pedido(Tipo 10)
            If sdbgRoles.Columns("TIPO_APROBADOR").Value = "" Or sdbgRoles.Columns("TIPO_APROBADOR").Value = 0 Then
                m_oRolEnEdicion.TipoAprobador = 0
            Else
                m_oRolEnEdicion.ActualizarAprobadores = g_bActualizarAprobadores 'Se le indica al rol si tiene que actualizar los aprobadores
                m_oRolEnEdicion.TipoAprobador = sdbgRoles.Columns("TIPO_APROBADOR").Value
                m_oRolEnEdicion.arrAprobadoresPorEmpresa = Me.arrAprobadoresPorEmpresa
                m_oRolEnEdicion.arrAprobadoresPorPeticionario = Me.arrAprobadoresPorPeticionario
                m_oRolEnEdicion.arrAprobadoresPorCentroCoste = Me.arrAprobadoresPorCentroCoste
                Erase m_arrAprobadoresPorEmpresa, m_arrAprobadoresPorPeticionario, m_arrAprobadoresPorCentroCoste
                g_bActualizarAprobadores = False 'Una vez indicado que se actualicen se pone a False para no volver a grabarlos en una posterior modificacion del rol
            End If
            Select Case m_oRolEnEdicion.CuandoAsignar
                Case CuandoAsignarPMRol.Ahora, CuandoAsignarPMRol.ProveedorDelContrato
                    Select Case m_oRolEnEdicion.Tipo
                        Case TipoPMRol.RolComprador, TipoPMRol.RolUsuario
                            m_oRolEnEdicion.Per = sdbgRoles.Columns("PER").Value
                            If sdbgRoles.Columns("TIPO_APROBADOR").Value = TipoAprobadorSolicitudPedido.PersonaDirecta Then
                                m_oRolEnEdicion.UON3 = sdbgRoles.Columns("UON3").Value
                                m_oRolEnEdicion.UON2 = sdbgRoles.Columns("UON2").Value
                                m_oRolEnEdicion.UON1 = sdbgRoles.Columns("UON1").Value
                                m_oRolEnEdicion.Dep = sdbgRoles.Columns("DEP").Value
                            End If
                        Case TipoPMRol.RolProveedor
                            m_oRolEnEdicion.Prove = sdbgRoles.Columns("PROVE").Value
                            If sdbgRoles.Columns("CONTACTO").Value <> "" Then
                                m_oRolEnEdicion.Contacto = CLng(sdbgRoles.Columns("CONTACTO").Value)
                            End If
                        Case TipoPMRol.RolPeticionario, TipoPMRol.RolObservador
                            If m_oRolEnEdicion.Restringir <> Lista Then
                                m_oRolEnEdicion.Per = sdbgRoles.Columns("PER").Value
                                m_oRolEnEdicion.UON3 = sdbgRoles.Columns("UON3").Value
                                m_oRolEnEdicion.UON2 = sdbgRoles.Columns("UON2").Value
                                m_oRolEnEdicion.UON1 = sdbgRoles.Columns("UON1").Value
                                If m_oRolEnEdicion.Tipo = TipoPMRol.RolObservador Then _
                                    m_oRolEnEdicion.UON0 = CBool(sdbgRoles.Columns("UON0").Value)
                                m_oRolEnEdicion.Dep = sdbgRoles.Columns("DEP").Value
                            End If
                        Case TipoPMRol.RolPeticionarioProveedor
                            If m_oRolEnEdicion.Restringir = 0 Then
                                'Si Restringir=ProvesMaterial ya tiene el valor en m_oRolEnEdicion.Participantes, no hay que asignarle nada
                                If sdbgRoles.Columns("PROVE").Value <> "" Then
                                    m_oRolEnEdicion.Prove = sdbgRoles.Columns("PROVE").Value
                                    If sdbgRoles.Columns("CONTACTO").Value <> "" Then
                                        m_oRolEnEdicion.Contacto = CLng(sdbgRoles.Columns("CONTACTO").Value)
                                    End If
                                End If
                            End If
                    End Select
                    If sdbgRoles.Columns("VER_FLUJO").Value = True Then
                        m_oRolEnEdicion.VER_FLUJO = True
                    Else
                        m_oRolEnEdicion.VER_FLUJO = False
                    End If
                Case CuandoAsignarPMRol.EnEtapa
                    If sdbgRoles.Columns("VER_FLUJO").Value = True Then
                        m_oRolEnEdicion.VER_FLUJO = True
                    Else
                        m_oRolEnEdicion.VER_FLUJO = False
                    End If
                    
                    m_oRolEnEdicion.BloqueAsigna = CLng(sdbgRoles.Columns("BLOQUE_ASIGNA").Value)
                    m_oRolEnEdicion.ComoAsignar = CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value)
                    Select Case m_oRolEnEdicion.ComoAsignar
                        Case ComoAsignarPMRol.PorCampo
                            m_oRolEnEdicion.Campo = CLng(sdbgRoles.Columns("CAMPO").Value)
                        Case ComoAsignarPMRol.PorPMRol
                            m_oRolEnEdicion.RolAsigna = CLng(sdbgRoles.Columns("ROL_ASIGNA").Value)
                        Case ComoAsignarPMRol.PorLista
                            m_oRolEnEdicion.Restringir = RestringirPMRol.Lista 'No deber�a hacer falta
                    End Select
                    Select Case m_oRolEnEdicion.Restringir
                        Case RestringirPMRol.ProvesEquipo
                            m_oRolEnEdicion.Eqp = sdbgRoles.Columns("EQP").Value
                        Case RestringirPMRol.ProvesMaterial
                        Case RestringirPMRol.Departamento
                            m_oRolEnEdicion.UON1 = sdbgRoles.Columns("UON1").Value
                            m_oRolEnEdicion.UON2 = sdbgRoles.Columns("UON2").Value
                            m_oRolEnEdicion.UON3 = sdbgRoles.Columns("UON3").Value
                            m_oRolEnEdicion.Dep = sdbgRoles.Columns("DEP").Value
                        Case RestringirPMRol.UON
                            m_oRolEnEdicion.UON1 = sdbgRoles.Columns("UON1").Value
                            m_oRolEnEdicion.UON2 = sdbgRoles.Columns("UON2").Value
                            m_oRolEnEdicion.UON3 = sdbgRoles.Columns("UON3").Value
                        Case RestringirPMRol.DepartamentoPMRol
                        Case RestringirPMRol.UONPMRol
                        Case RestringirPMRol.Lista
                    End Select
            End Select
            m_oRolEnEdicion.ConfBloqueDef = sdbgRoles.Columns("CONF_BLOQUE_DEF").Value
            m_oRolEnEdicion.Sustituto = sdbgRoles.Columns("SUSTITUTO").Value
            m_oRolEnEdicion.TipoSustitucion = sdbgRoles.Columns("TIPO_SUSTITUCION").Value
            If m_oRolEnEdicion.Tipo = RolObservador Then
                m_oRolEnEdicion.VisibilidadObservador = sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value
            Else
                m_oRolEnEdicion.VisibilidadObservador = 0
            End If
            m_oRolEnEdicion.VerDetallePer = sdbgRoles.Columns("VER_DETALLE_PER").Value
            m_oRolEnEdicion.PermitirTraslados = sdbgRoles.Columns("PERMITIR_TRASLADOS").Value
            m_oRolEnEdicion.Gestor = sdbgRoles.Columns("GESTOR").Value
            m_oRolEnEdicion.Receptor = sdbgRoles.Columns("RECEPTOR").Value
            
            Set m_oIBAseDatosEnEdicion = m_oRolEnEdicion
            teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                m_bModError = True
                Cancel = True
                GoTo Salir
            Else
                ''' Registro de acciones
                sdbgRoles.Columns("FECACT").Value = m_oRolEnEdicion.FECACT
                basSeguridad.RegistrarAccion AccionesSummit.ACCPMRolModif, "ID:" & m_oRolEnEdicion.Id
                m_bModError = False
                'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
                If Not m_bNoModificaFlujo Then frmFlujos.HayCambios
            End If
            'Si el Rol es de Tipo Peticionario, lo asignamos a la etapa PETICIONARIO
            
            'Almacenar las empresas si se trata de una solicitud de contrato
            m_oRolEnEdicion.CadenaEmpresas = sdbgRoles.Columns("COD_EMPRESAS").Value
            m_oRolEnEdicion.ActualizarEmpresas
            Set m_oRolEnEdicion = Nothing
        End If
    End If

    Set m_oRolAnyadir = Nothing
    Set m_oParticipantesAnyadir = Nothing
    Exit Sub
    
Salir:
    If Me.Visible Then sdbgRoles.SetFocus
    m_bError = True
    Exit Sub

End Sub

''' Revisado por: blp. Fecha: 23/07/2012
''' <summary>
''' Abre las pantallas de seleccion bien de departamento,UON... para seleccionar las restricciones del rol
''' </summary>
''' <remarks>Llamada desde: frmflujosRoles; Tiempo m�ximo: 0</remarks>
Private Sub sdbgRoles_BtnClick()
    
    Dim oParticipantes As CPMParticipantes
    Dim oParticipante As CPMParticipante
    Dim Id As Integer
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim teserror As TipoErrorSummit
    Dim esPetProveFactura As Boolean
    
    If sdbgRoles.col < 0 Then Exit Sub
     If Not sdbgRoles.IsAddRow Then
        Set m_oRolEnEdicion = m_oRoles.Item(CStr(sdbgRoles.Columns("ID").Value))
        
        Set m_oIBAseDatosEnEdicion = m_oRolEnEdicion
            
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
            
        If teserror.NumError = TESInfModificada Then
                
            TratarError teserror
            sdbgRoles.DataChanged = False
                
            CargarRoles

            Exit Sub
        End If
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgRoles.SetFocus
            Exit Sub
        End If
     End If
     
     esPetProveFactura = (CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionarioProveedor And g_iSolicitudTipoTipo = TipoSolicitud.Factura)
     Select Case sdbgRoles.Columns(sdbgRoles.col).Name
        Case "CAMPO_BTN"
            If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" And CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa Then
                If sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" And CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo Then
                
                    'Muestro el formulario de Seleccion de Campo
                    frmFlujosRolesCampoAsigna.m_bModifFlujo = m_bModifFlujo
                    frmFlujosRolesCampoAsigna.lIdFormulario = lIdFormulario
                    If sdbgRoles.Columns("TIPO").Value <> "" Then
                        frmFlujosRolesCampoAsigna.iTipoRol = CInt(sdbgRoles.Columns("TIPO").Value)
                    Else
                        frmFlujosRolesCampoAsigna.iTipoRol = 0
                    End If
                    If sdbgRoles.Columns("CAMPO").Value <> "" And sdbgRoles.Columns("CAMPO").Value <> "0" Then
                        frmFlujosRolesCampoAsigna.lIdCampo = CLng(sdbgRoles.Columns("CAMPO").Value)
                    End If
                    frmFlujosRolesCampoAsigna.sRol = sdbgRoles.Columns("DEN").Value
                    frmFlujosRolesCampoAsigna.Show vbModal
                    
                    If frmFlujosRolesCampoAsigna.lIdCampo > 0 Then
                        sdbgRoles.Columns("CAMPO").Value = frmFlujosRolesCampoAsigna.lIdCampo
                        sdbgRoles.Columns("CAMPO_BTN").Value = "..."
                    End If
                    Unload frmFlujosRolesCampoAsigna
                        
                    If sdbgRoles.DataChanged Then
                        sdbgRoles.Update
                        If m_bError Then
                            Exit Sub
                        End If
                    End If
                End If
            End If
        Case "RESTRINGIR_BTN"
            If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" _
            And (CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa _
                Or (CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.Ahora And esPetProveFactura)) Then
                If sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" And CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) <> ComoAsignarPMRol.PorCampo Then
                
                    'Muestro el formulario de Seleccion de Restriccion
                    If sdbgRoles.Columns("RESTRINGIR").Value <> "" Then
                        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                        Select Case CInt(sdbgRoles.Columns("RESTRINGIR").Value)
                            Case RestringirPMRol.ProvesEquipo
                                Dim sEqp As String
                                sEqp = MostrarFormSELEQP(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, m_bModifFlujo, sdbgRoles.Columns("EQP").Value)
                                
                                If sEqp <> "" Then
                                    sdbgRoles.Columns("EQP").Value = sEqp
                                    sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                                End If
                            Case RestringirPMRol.ProvesMaterial
                                sdbgRoles.Columns("RESTRINGIR_BTN").Value = " "
                                sdbgRoles_Change
                                If sdbgRoles.IsAddRow Then
                                    Set oParticipantes = m_oParticipantesAnyadir
                                Else
                                    Set oParticipantes = m_oRolEnEdicion.Participantes
                                End If
                                frmSELMATMultiple.m_bModif = m_bModifFlujo
                                Set frmSELMATMultiple.oGruposMN1Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel1
                                Set frmSELMATMultiple.oGruposMN2Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel2
                                Set frmSELMATMultiple.oGruposMN3Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel3
                                Set frmSELMATMultiple.oGruposMN4Seleccionados = oFSGSRaiz.Generar_CGruposMatNivel4
                                If Not oParticipantes Is Nothing Then
                                    For Each oParticipante In oParticipantes
                                        If oParticipante.GMN1 <> "" Then
                                            If oParticipante.GMN2 <> "" Then
                                                If oParticipante.GMN3 <> "" Then
                                                    If oParticipante.GMN4 <> "" Then
                                                        frmSELMATMultiple.oGruposMN4Seleccionados.Add oParticipante.GMN1, oParticipante.GMN2, oParticipante.GMN3, "", "", "", oParticipante.GMN4, ""
                                                    Else
                                                        frmSELMATMultiple.oGruposMN3Seleccionados.Add oParticipante.GMN1, oParticipante.GMN2, oParticipante.GMN3, "", "", ""
                                                    End If
                                                Else
                                                    frmSELMATMultiple.oGruposMN2Seleccionados.Add oParticipante.GMN1, "", oParticipante.GMN2, ""
                                                End If
                                            Else
                                                frmSELMATMultiple.oGruposMN1Seleccionados.Add oParticipante.GMN1, ""
                                            End If
                                        End If
                                    Next
                                Else
                                    Set oParticipantes = New CPMParticipantes
                                End If
                                frmSELMATMultiple.sOrigen = "frmFlujosRoles"
                                frmSELMATMultiple.Show vbModal
                                If Not frmSELMATMultiple.oGruposMN1Seleccionados Is Nothing Or _
                                    Not frmSELMATMultiple.oGruposMN2Seleccionados Is Nothing Or _
                                    Not frmSELMATMultiple.oGruposMN3Seleccionados Is Nothing Or _
                                    Not frmSELMATMultiple.oGruposMN4Seleccionados Is Nothing Then
                                        
                                    oParticipantes.clear
                                    Id = 1
                                    
                                    If Not frmSELMATMultiple.oGruposMN1Seleccionados Is Nothing Then
                                        For Each oGMN1 In frmSELMATMultiple.oGruposMN1Seleccionados
                                            Set oParticipante = New CPMParticipante
                                            oParticipante.GMN1 = oGMN1.Cod
                                            oParticipante.Id = Id
                                            oParticipantes.AddParticipante oParticipante
                                            Id = Id + 1
                                        Next
                                    End If
                                    If Not frmSELMATMultiple.oGruposMN2Seleccionados Is Nothing Then
                                        For Each oGMN2 In frmSELMATMultiple.oGruposMN2Seleccionados
                                            Set oParticipante = New CPMParticipante
                                            oParticipante.GMN1 = oGMN2.GMN1Cod
                                            oParticipante.GMN2 = oGMN2.Cod
                                            oParticipante.Id = Id
                                            oParticipantes.AddParticipante oParticipante
                                            Id = Id + 1
                                        Next
                                    End If
                                    If Not frmSELMATMultiple.oGruposMN3Seleccionados Is Nothing Then
                                        For Each oGMN3 In frmSELMATMultiple.oGruposMN3Seleccionados
                                            Set oParticipante = New CPMParticipante
                                            oParticipante.GMN1 = oGMN3.GMN1Cod
                                            oParticipante.GMN2 = oGMN3.GMN2Cod
                                            oParticipante.GMN3 = oGMN3.Cod
                                            oParticipante.Id = Id
                                            oParticipantes.AddParticipante oParticipante
                                            Id = Id + 1
                                        Next
                                    End If
                                    If Not frmSELMATMultiple.oGruposMN4Seleccionados Is Nothing Then
                                        For Each oGMN4 In frmSELMATMultiple.oGruposMN4Seleccionados
                                            Set oParticipante = New CPMParticipante
                                            oParticipante.GMN1 = oGMN4.GMN1Cod
                                            oParticipante.GMN2 = oGMN4.GMN2Cod
                                            oParticipante.GMN3 = oGMN4.GMN3Cod
                                            oParticipante.GMN4 = oGMN4.Cod
                                            oParticipante.Id = Id
                                            oParticipantes.AddParticipante oParticipante
                                            Id = Id + 1
                                        Next
                                    End If
                                End If
                                
                                If sdbgRoles.IsAddRow Then
                                    Set m_oParticipantesAnyadir = oParticipantes
                                Else
                                    Set m_oRolEnEdicion.Participantes = oParticipantes
                                End If
                                
                                If sdbgRoles.IsAddRow Then
                                    If m_oParticipantesAnyadir.Count > 0 Then
                                        sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                                    End If
                                Else
                                    If m_oRolEnEdicion.Participantes.Count > 0 Then
                                        sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                                    End If
                                End If
                                Unload frmSELMATMultiple
                            Case RestringirPMRol.Departamento
                                frmSELDEP.m_bModif = m_bModifFlujo
                                frmSELDEP.sOrigen = "frmFlujosRoles"
                                frmSELDEP.sCodDep = sdbgRoles.Columns("DEP").Value
                                frmSELDEP.sUON1 = sdbgRoles.Columns("UON1").Value
                                frmSELDEP.sUON2 = sdbgRoles.Columns("UON2").Value
                                frmSELDEP.sUON3 = sdbgRoles.Columns("UON3").Value
                                frmSELDEP.Show vbModal
                                
                                If frmSELDEP.sCodDep <> "" Then
                                    sdbgRoles.Columns("DEP").Value = frmSELDEP.sCodDep
                                    sdbgRoles.Columns("UON1").Value = frmSELDEP.sUON1
                                    sdbgRoles.Columns("UON2").Value = frmSELDEP.sUON2
                                    sdbgRoles.Columns("UON3").Value = frmSELDEP.sUON3
                                    sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                                End If
                                Unload frmSELDEP
                            Case RestringirPMRol.UON
                                frmSELUO.m_bModif = m_bModifFlujo
                                frmSELUO.sOrigen = "frmFlujosRoles"
                                frmSELUO.bRUO = m_bRUON
                                frmSELUO.sUON1 = sdbgRoles.Columns("UON1").Value
                                frmSELUO.sUON2 = sdbgRoles.Columns("UON2").Value
                                frmSELUO.sUON3 = sdbgRoles.Columns("UON3").Value
                                frmSELUO.Show vbModal
                            
                                If frmSELUO.sUON1 <> "" Then
                                    sdbgRoles.Columns("UON1").Value = frmSELUO.sUON1
                                    sdbgRoles.Columns("UON2").Value = frmSELUO.sUON2
                                    sdbgRoles.Columns("UON3").Value = frmSELUO.sUON3
                                    
                                    sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                                End If
                                Unload frmSELUO
                                'MsgBox ("Formulario de seleccion de UON")
                            Case RestringirPMRol.Lista
                                sdbgRoles_Change
                                frmFlujosRolesListaPersonas.m_bModifFlujo = m_bModifFlujo
                                frmFlujosRolesListaPersonas.sRol = sdbgRoles.Columns("DEN").Value
                                frmFlujosRolesListaPersonas.m_bRDep = m_bRDep
                                frmFlujosRolesListaPersonas.m_bRUON = m_bRUON
                                If sdbgRoles.IsAddRow Then
                                    Set frmFlujosRolesListaPersonas.oParticipantes = m_oParticipantesAnyadir
                                Else
                                    Set frmFlujosRolesListaPersonas.oParticipantes = m_oRolEnEdicion.Participantes
                                End If
                                frmFlujosRolesListaPersonas.Show vbModal
                                
                                If Not frmFlujosRolesListaPersonas.oParticipantes Is Nothing Then
                                    If sdbgRoles.IsAddRow Then
                                        Set m_oParticipantesAnyadir = frmFlujosRolesListaPersonas.oParticipantes
                                    Else
                                        Set m_oRolEnEdicion.Participantes = frmFlujosRolesListaPersonas.oParticipantes
                                    End If
        
                                    If sdbgRoles.IsAddRow Then
                                        If m_oParticipantesAnyadir.Count > 0 Then
                                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                                        Else
                                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = " "
                                        End If
                                    Else
                                        If m_oRolEnEdicion.Participantes.Count > 0 Then
                                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                                        Else
                                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = " "
                                        End If
                                    End If
                                End If
                                Unload frmFlujosRolesListaPersonas
                        End Select
                    End If
                    
                    If sdbgRoles.DataChanged Then
                        sdbgRoles.Update
                        If m_bError Then
                            Exit Sub
                        End If
                    End If
                End If
            End If
            If sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolPeticionario Then
                sdbgRoles.Columns("RESTRINGIR").Value = RestringirPMRol.Lista
                sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                sdbgRoles.Columns("PER").Value = ""
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                sdbgRoles.Columns("UON1").Value = ""
                sdbgRoles.Columns("UON2").Value = ""
                sdbgRoles.Columns("UON3").Value = ""
                sdbgRoles.Columns("DEP").Value = ""
                sdbgRoles_Change
                frmFlujosRolesListaPersonas.m_bModifFlujo = m_bModifFlujo
                frmFlujosRolesListaPersonas.sRol = sdbgRoles.Columns("DEN").Value
                frmFlujosRolesListaPersonas.m_bRDep = m_bRDep
                frmFlujosRolesListaPersonas.m_bRUON = m_bRUON
                If sdbgRoles.IsAddRow Then
                    Set frmFlujosRolesListaPersonas.oParticipantes = m_oParticipantesAnyadir
                Else
                    Set frmFlujosRolesListaPersonas.oParticipantes = m_oRolEnEdicion.Participantes
                End If
                frmFlujosRolesListaPersonas.Show vbModal
                
                If Not frmFlujosRolesListaPersonas.oParticipantes Is Nothing Then
                    If sdbgRoles.IsAddRow Then
                        Set m_oParticipantesAnyadir = frmFlujosRolesListaPersonas.oParticipantes
                    Else
                        Set m_oRolEnEdicion.Participantes = frmFlujosRolesListaPersonas.oParticipantes
                    End If

                    If sdbgRoles.IsAddRow Then
                        If m_oParticipantesAnyadir.Count > 0 Then
                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                        Else
                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = " "
                        End If
                    Else
                        If m_oRolEnEdicion.Participantes.Count > 0 Then
                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = "..."
                        Else
                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = " "
                        End If
                    End If
                End If
                Unload frmFlujosRolesListaPersonas
                If sdbgRoles.DataChanged Then
                    sdbgRoles.Update
                    If m_bError Then
                        Exit Sub
                    End If
                End If
            End If
        Case "PET_PER_PROVE_DEN"
            If Not sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked Then
                If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" And CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.Ahora Then
                    'Muestro el formulario de Seleccion Peticionario, Persona o Proveedor
                    If sdbgRoles.Columns("TIPO").Value <> "" Then
                        Select Case CInt(sdbgRoles.Columns("TIPO").Value)
                            Case TipoPMRol.RolComprador, TipoPMRol.RolUsuario
                                If g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoCatalogo Or g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto Then
                                    'Se permite elegir a personas/departamentos/Uons en funcion de empresas, centros de coste...
                                    frmSOLSelPersonaEnFuncionDe.g_sOrigen = "frmFlujosRoles"
                                    If sdbgRoles.Columns("TIPO_APROBADOR").Value = "" Then
                                        frmSOLSelPersonaEnFuncionDe.g_iTipoAprobador = 0
                                    Else
                                        frmSOLSelPersonaEnFuncionDe.g_iTipoAprobador = sdbgRoles.Columns("TIPO_APROBADOR").Value
                                    End If
                                    Set frmSOLSelPersonaEnFuncionDe.g_oRolEnEdicion = m_oRolEnEdicion
                                    frmSOLSelPersonaEnFuncionDe.bAllowSelUON = False
                                    frmSOLSelPersonaEnFuncionDe.bRDep = m_bRDep
                                    frmSOLSelPersonaEnFuncionDe.bRUO = m_bRUON
                                    frmSOLSelPersonaEnFuncionDe.g_sPer = sdbgRoles.Columns("PER").Value
                                    frmSOLSelPersonaEnFuncionDe.g_sUON1 = sdbgRoles.Columns("UON1").Value
                                    frmSOLSelPersonaEnFuncionDe.g_sUON2 = sdbgRoles.Columns("UON2").Value
                                    frmSOLSelPersonaEnFuncionDe.g_sUON3 = sdbgRoles.Columns("UON3").Value
                                    frmSOLSelPersonaEnFuncionDe.g_sDEP = sdbgRoles.Columns("DEP").Value
                                    frmSOLSelPersonaEnFuncionDe.g_sEmpresas = m_sEmpresas
                                    frmSOLSelPersonaEnFuncionDe.g_lIdSolicitud = g_lSolicitud
                                    frmSOLSelPersonaEnFuncionDe.g_TipoSolicitud = g_iSolicitudTipoTipo
                                    frmSOLSelPersonaEnFuncionDe.Show vbModal
                                Else
                                    frmSOLSelPersona.g_sOrigen = "frmFlujosRoles"
                                    frmSOLSelPersona.bAllowSelUON = False
                                    frmSOLSelPersona.bRDep = m_bRDep
                                    frmSOLSelPersona.bRUO = m_bRUON
                                    frmSOLSelPersona.g_sPer = sdbgRoles.Columns("PER").Value
                                    frmSOLSelPersona.g_sUON1 = sdbgRoles.Columns("UON1").Value
                                    frmSOLSelPersona.g_sUON2 = sdbgRoles.Columns("UON2").Value
                                    frmSOLSelPersona.g_sUON3 = sdbgRoles.Columns("UON3").Value
                                    frmSOLSelPersona.g_sDEP = sdbgRoles.Columns("DEP").Value
                                    frmSOLSelPersona.g_sEmpresas = m_sEmpresas
                                    frmSOLSelPersona.Show vbModal
                                End If
                                
                                'Se establecen todos los datos desde el formulario llamado
                            Case TipoPMRol.RolProveedor, TipoPMRol.RolPeticionarioProveedor
                                frmPROVEBuscar.sOrigen = "frmFlujosRoles"
                                frmPROVEBuscar.Show vbModal
                                If sdbgRoles.Columns("PROVE").Value <> frmPROVEBuscar.sdbgProveedores.Columns("COD").Value Then
                                    sdbgRoles.Columns("CONTACTO").Value = 0
                                    sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                                End If
                                If frmPROVEBuscar.sdbgProveedores.Columns("COD").Value <> "" Then
                                    sdbgRoles.Columns("PROVE").Value = frmPROVEBuscar.sdbgProveedores.Columns("COD").Value
                                    sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = frmPROVEBuscar.sdbgProveedores.Columns("DEN").Value & " " & frmUSUBuscar.sdbgPersonas.Columns("APE").Value
                                End If
                                
                                Unload frmPROVEBuscar
                            Case TipoPMRol.RolPeticionario, TipoPMRol.RolObservador
                                If sdbgRoles.Columns("RESTRINGIR").Value <> "" And sdbgRoles.Columns("RESTRINGIR").Value <> 0 Then sdbgRoles.Columns("RESTRINGIR").Value = ""
                                If sdbgRoles.Columns("RESTRINGIR_DEN").Value <> "" Then sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                                If sdbgRoles.Columns("RESTRINGIR_BTN").Value <> "" Then sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                                frmSOLSelPersona.g_sOrigen = "frmFlujosRoles"
                                frmSOLSelPersona.bAllowSelUON = True
                                frmSOLSelPersona.bRDep = m_bRDep
                                frmSOLSelPersona.bRUO = m_bRUON
                                frmSOLSelPersona.g_sPer = sdbgRoles.Columns("PER").Value
                                If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolObservador Then _
                                    frmSOLSelPersona.g_sUON0 = sdbgRoles.Columns("UON0").Value
                                frmSOLSelPersona.g_sUON1 = sdbgRoles.Columns("UON1").Value
                                frmSOLSelPersona.g_sUON2 = sdbgRoles.Columns("UON2").Value
                                frmSOLSelPersona.g_sUON3 = sdbgRoles.Columns("UON3").Value
                                frmSOLSelPersona.g_sDEP = sdbgRoles.Columns("DEP").Value
                                frmSOLSelPersona.g_sEmpresas = m_sEmpresas
                                If (CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionario) Then
                                    frmSOLSelPersona.bAllowSelUON0 = False
                                Else
                                    frmSOLSelPersona.bAllowSelUON0 = True
                                    frmSOLSelPersona.caption = m_sSelecObservador 'Seleccione Observador
                                End If
                                
                                frmSOLSelPersona.Show vbModal
                                
                                'Se establecen todos los datos desde el formulario llamado
                        End Select
                    End If
                End If
            End If
        
        Case "SUSTITUCION"
            If Not sdbgRoles.Columns("SUSTITUCION").Locked Then
                'Muestro el formulario de Seleccion Sustituto
                If sdbgRoles.Columns("TIPO").Value <> "" And sdbgRoles.Columns("TIPO").Value <> 2 Then
                    frmSOLSelSustituto.bRDep = m_bRDep
                    frmSOLSelSustituto.bRUO = m_bRUON
                    frmSOLSelSustituto.sSustituto = sdbgRoles.Columns("SUSTITUTO").Value
                    frmSOLSelSustituto.iTipoSustitucion = sdbgRoles.Columns("TIPO_SUSTITUCION").Value
                    frmSOLSelSustituto.lRol = sdbgRoles.Columns("ID").Value
                    frmSOLSelSustituto.Show vbModal
                End If
                If Not IsEmpty(g_vFecAct) Then
                    If Not m_oRolEnEdicion Is Nothing Then
                        m_oRolEnEdicion.FECACT = g_vFecAct
                    End If
                End If
                
                If sdbgRoles.DataChanged Then
                    m_bNoModificaFlujo = True
                    sdbgRoles.Update
                    m_bNoModificaFlujo = False
                    If m_bError Then
                        Exit Sub
                    End If
                End If
            End If
            
        Case "ETAPAS_OBSERVADOR_BTN"
            If sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = VisibilidadObservadorPMRol.VisibilidadParcial Then
                
                frmFlujosEtapasObservador.m_lIdWF = lIdFlujo
                frmFlujosEtapasObservador.m_lIdRol = sdbgRoles.Columns("ID").Value
                frmFlujosEtapasObservador.m_sDenRol = sdbgRoles.Columns("DEN").Value
                frmFlujosEtapasObservador.Show vbModal
            End If
        Case "CONF_BLOQUE_DEF_DEN"
            If sdbgRoles.IsAddRow Then
                If sdbgRoles.DataChanged Then
                    sdbgRoles.Update
                    If m_bError Then
                        Exit Sub
                    End If
                End If
            End If

            Dim oRol As CPMRol
            Set oRol = oFSGSRaiz.Generar_CPMRol
            oRol.Id = sdbgRoles.Columns("ID").Value
            
            If oRol.comprobarVisibilidadDefecto = False Then
                'Crear visibilidad por defecto
                oRol.Tipo = sdbgRoles.Columns("TIPO").Value
                oRol.CrearCumplimentacionDef (lIdFormulario)
            End If
            Set oRol = Nothing

            frmFlujosConfVisibilidadDef.m_lIdRol = sdbgRoles.Columns("ID").Value
            frmFlujosConfVisibilidadDef.m_sRol = sdbgRoles.Columns("DEN").Value
            frmFlujosConfVisibilidadDef.m_udtTipo = sdbgRoles.Columns("TIPO").Value
            frmFlujosConfVisibilidadDef.m_bModifFlujo = m_bModifFlujo
            frmFlujosConfVisibilidadDef.m_lIdFormulario = lIdFormulario
            frmFlujosConfVisibilidadDef.Show vbModal
            
        Case "EMPRESAS"
            frmSelEmpresas.g_lSolicitud = g_lSolicitud
            frmSelEmpresas.g_sEmpresas = sdbgRoles.Columns("COD_EMPRESAS").Value
            frmSelEmpresas.Show vbModal
            Dim sAux As String
            sAux = frmSelEmpresas.g_sEmpresas
            If sAux <> sdbgRoles.Columns("COD_EMPRESAS").Value Then
                sdbgRoles.Columns("COD_EMPRESAS").Value = sAux
                If sAux <> "" And sAux <> "," Then
                    sdbgRoles.Columns("EMPRESAS").Value = "..."
                Else
                    sdbgRoles.Columns("EMPRESAS").Value = " "
                End If
            End If
           
            If sAux <> "" Then
                m_sEmpresas = sAux
            Else
                CargarEmpresas
            End If
     End Select
  
    Set m_oIBaseDatos = Nothing
End Sub

Private Sub sdbgRoles_Change()
    Dim teserror As TipoErrorSummit
    Dim Valor_Receptor_Old As Boolean
    Dim Valor_Gestor_Old As Boolean
    Dim oBloques As CBloques
    
        If Not sdbgRoles.IsAddRow Then
        Set m_oRolEnEdicion = Nothing
        Set m_oRolEnEdicion = m_oRoles.Item(CStr(sdbgRoles.Columns("ID").Value))
        Set m_oIBAseDatosEnEdicion = m_oRolEnEdicion
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
        If teserror.NumError = TESInfModificada Then
            TratarError teserror
            sdbgRoles.DataChanged = False
            CargarRoles
            teserror.NumError = TESnoerror
        End If
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            If Me.Visible Then sdbgRoles.SetFocus
        End If
    End If
    
    If sdbgRoles.col >= 0 Then
        Select Case sdbgRoles.Columns(sdbgRoles.col).Name
            Case "PETICIONARIO", "COMPRADOR", "PROVEEDOR", "OBSERVADOR", "GESTOR", "RECEPTOR"
                sdbgRoles.Columns("VER_FLUJO").Locked = False
                sdbgRoles.Columns("VER_FLUJO").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("VER_DETALLE_PER").Locked = False
                sdbgRoles.Columns("VER_DETALLE_PER").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = False
                sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = False
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = False
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Locked = False
                sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = False
                sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = False
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = False
                sdbgRoles.Columns("RESTRINGIR_DEN").Locked = False
                sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = False
        End Select
    
        Select Case sdbgRoles.Columns(sdbgRoles.col).Name
            Case "PETICIONARIO"
                If sdbgRoles.Columns("PETICIONARIO").Value <> "" Then
                    If CBool(sdbgRoles.Columns("PETICIONARIO").Value) Then
                        If sdbgRoles.Columns("PETICIONARIO").Value And Not sdbgRoles.Columns("PROVEEDOR").Value Then
                            If g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoCatalogo Or g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto Then
                                'Si es este tipo de solicitud, cuando se marca el rol como peticionario se debe bloquear esta columna
                                'ya que el peticionario, sera el aprovisionador del pedido
                                sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                                m_bHayPeticionario = True
                            End If
                        End If
    
                        If sdbgRoles.Columns("PROVEEDOR").Value Then
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolPeticionarioProveedor
                        Else
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolPeticionario
                        End If
                        sdbgRoles.Columns("COMPRADOR").Value = False
                        sdbgRoles.Columns("OBSERVADOR").Value = False
                        Valor_Gestor_Old = sdbgRoles.Columns("GESTOR").Value
                        sdbgRoles.Columns("GESTOR").Value = False
                        ActualizarHayGestor (Valor_Gestor_Old)
                        Valor_Receptor_Old = sdbgRoles.Columns("RECEPTOR").Value
                        sdbgRoles.Columns("RECEPTOR").Value = False
                        ActualizarHayReceptor (Valor_Receptor_Old)
                        sdbgRoles.Columns("CUANDO_ASIGNAR").Value = CuandoAsignarPMRol.Ahora
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = m_arCuandoAsignar(CuandoAsignarPMRol.Ahora)
                        sdbgRoles.Columns("BLOQUE_ASIGNA").Value = 0
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = ""
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
                        sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
                        sdbgRoles.Columns("ROL_ASIGNA").Value = 0
                        sdbgRoles.Columns("CAMPO").Value = 0
                        sdbgRoles.Columns("CAMPO_BTN").Value = ""
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = True
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = True
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").Value = 0
                        If sdbgRoles.Columns("PROVEEDOR").Value Then
                            sdbgRoles.Columns("PER").Value = ""
                            sdbgRoles.Columns("UON1").Value = ""
                            sdbgRoles.Columns("UON2").Value = ""
                            sdbgRoles.Columns("UON3").Value = ""
                            sdbgRoles.Columns("DEP").Value = ""
                        Else
                            If sdbgRoles.Columns("PER").Value = "" Then
                                sdbgRoles.Columns("UON1").Value = ""
                                sdbgRoles.Columns("UON2").Value = ""
                                sdbgRoles.Columns("UON3").Value = ""
                                sdbgRoles.Columns("DEP").Value = ""
                            Else
                                Dim oPersona As CPersona
                                Set oPersona = oFSGSRaiz.Generar_CPersona
                                oPersona.Cod = sdbgRoles.Columns("PER").Value
                                oPersona.CargarTodosLosDatos
                                sdbgRoles.Columns("UON1").Value = oPersona.UON1
                                sdbgRoles.Columns("UON2").Value = oPersona.UON2
                                sdbgRoles.Columns("UON3").Value = oPersona.UON3
                                sdbgRoles.Columns("DEP").Value = oPersona.CodDep
                                Set oPersona = Nothing
                            End If
                        End If
                    Else
                        If Not sdbgRoles.Columns("PROVEEDOR").Value Then
                            m_bHayPeticionario = False
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
                        Else
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolProveedor
                        End If
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = False
                        sdbgRoles.Columns("UON1").Value = ""
                        sdbgRoles.Columns("UON2").Value = ""
                        sdbgRoles.Columns("UON3").Value = ""
                        sdbgRoles.Columns("DEP").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = False
                    End If
                    sdbgRoles.Columns("RESTRINGIR").Value = 0
                    sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                    sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                    If sdbgRoles.Columns("PROVE").Value <> "" Then
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                    End If
                    sdbgRoles.Columns("CONTACTO").Value = ""
                    sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                    sdbgRoles.Columns("EQP").Value = ""
                    sdbgRoles.Columns("SUSTITUCION").Value = ""
                    sdbgRoles.Columns("SUSTITUTO").Value = ""
                    sdbgRoles.Columns("TIPO_SUSTITUCION").Value = 0
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = 0
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = ""
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                    sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = ""
                    If Not sdbgRoles.IsAddRow Then
                        ''A�adir a la tabla PM_ROL_OBS_ETAPAS las etapas a las que el observador tiene visibilidad.
                        Set oBloques = oFSGSRaiz.Generar_CBloques
                        oBloques.DesAsignarEtapaObservador sdbgRoles.Columns("ID").Value
                        Set oBloques = Nothing
                    End If
                Else
                    If m_bHayPeticionario And (g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoCatalogo Or g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto) Then
                        m_bHayPeticionario = False
                    End If
                End If
            Case "COMPRADOR"
                If sdbgRoles.Columns("COMPRADOR").Value <> "" Then
                    If CBool(sdbgRoles.Columns("COMPRADOR").Value) Then
                        If sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" Then
                            If CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorLista Then
                                sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                            ElseIf sdbgRoles.Columns("TIPO").Value <> TipoPMRol.RolUsuario And CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo Then
                                sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                                sdbgRoles.Columns("CAMPO").Value = 0
                                sdbgRoles.Columns("CAMPO_BTN").Value = ""
                            End If
                        End If
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolComprador
                        If sdbgRoles.Columns("PETICIONARIO").Value And Not sdbgRoles.Columns("PROVEEDOR").Value Then
                            'Solo se pone a False cuando estes en el rol peticionario, ya que podrias tener un rol
                            'peticionario, y en otro rol, marcar la columna Observador por ejemplo y esta variable deberia seguir a True
                            m_bHayPeticionario = False
                        End If
                        sdbgRoles.Columns("PETICIONARIO").Value = False
                        sdbgRoles.Columns("PROVEEDOR").Value = False
                        Valor_Gestor_Old = sdbgRoles.Columns("GESTOR").Value
                        sdbgRoles.Columns("GESTOR").Value = False
                        ActualizarHayGestor (Valor_Gestor_Old)
                        Valor_Receptor_Old = sdbgRoles.Columns("RECEPTOR").Value
                        sdbgRoles.Columns("RECEPTOR").Value = False
                        ActualizarHayReceptor (Valor_Receptor_Old)
                        sdbgRoles.Columns("RESTRINGIR").Value = 0
                        sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                        sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                        sdbgRoles.Columns("PER").Value = ""
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                        sdbgRoles.Columns("CONTACTO").Value = ""
                        sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                        sdbgRoles.Columns("UON1").Value = ""
                        sdbgRoles.Columns("UON2").Value = ""
                        sdbgRoles.Columns("UON3").Value = ""
                        sdbgRoles.Columns("DEP").Value = ""
                        sdbgRoles.Columns("EQP").Value = ""
                        sdbgRoles.Columns("SUSTITUCION").Value = ""
                        sdbgRoles.Columns("SUSTITUTO").Value = ""
                        sdbgRoles.Columns("TIPO_SUSTITUCION").Value = 0
                    Else
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
                    End If
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = 0
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = ""
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                    sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = ""
                    
                    If Not sdbgRoles.IsAddRow Then
                        ''A�adir a la tabla PM_ROL_OBS_ETAPAS las etapas a las que el observador tiene visibilidad.
                        Set oBloques = oFSGSRaiz.Generar_CBloques
                        oBloques.DesAsignarEtapaObservador sdbgRoles.Columns("ID").Value
                        Set oBloques = Nothing
                    End If
                End If
            Case "PROVEEDOR"
                If sdbgRoles.Columns("PROVEEDOR").Value <> "" Then
                    If CBool(sdbgRoles.Columns("PROVEEDOR").Value) Then
                        If sdbgRoles.Columns("PETICIONARIO").Value Then
                            'Solo se pone a False cuando estes en el rol peticionario, ya que podrias tener un rol
                            'peticionario, y en otro rol, marcar la columna Observador por ejemplo y esta variable deberia seguir a True
                            m_bHayPeticionario = False
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolPeticionarioProveedor
                        Else
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolProveedor
                        End If
                        sdbgRoles.Columns("CUANDO_ASIGNAR").Value = CuandoAsignarPMRol.Ahora
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = m_arCuandoAsignar(CuandoAsignarPMRol.Ahora)
                        sdbgRoles.Columns("COMPRADOR").Value = False
                        sdbgRoles.Columns("OBSERVADOR").Value = False
                        Valor_Gestor_Old = sdbgRoles.Columns("GESTOR").Value
                        sdbgRoles.Columns("GESTOR").Value = False
                        ActualizarHayGestor (Valor_Gestor_Old)
                        Valor_Receptor_Old = sdbgRoles.Columns("RECEPTOR").Value
                        sdbgRoles.Columns("RECEPTOR").Value = False
                        ActualizarHayReceptor (Valor_Receptor_Old)
                        
                        If sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" Then
                            If CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorLista Then
                                sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                            ElseIf CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo Then
                                sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                                sdbgRoles.Columns("CAMPO").Value = 0
                                sdbgRoles.Columns("CAMPO_BTN").Value = ""
                            End If
                        End If
                    Else
                        If sdbgRoles.Columns("PETICIONARIO").Value Then
                            m_bHayPeticionario = True
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolPeticionario
                            sdbgRoles.Columns("CUANDO_ASIGNAR").Value = CuandoAsignarPMRol.Ahora
                            sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = m_arCuandoAsignar(CuandoAsignarPMRol.Ahora)
                        Else
                            sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
                            sdbgRoles.Columns("CUANDO_ASIGNAR").Value = 0
                            sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = ""
                        End If
                    End If
                    sdbgRoles.Columns("RESTRINGIR").Value = 0
                    sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                    sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                    sdbgRoles.Columns("PER").Value = ""
                    sdbgRoles.Columns("PROVE").Value = ""
                    sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                    sdbgRoles.Columns("CONTACTO").Value = ""
                    sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                    sdbgRoles.Columns("UON1").Value = ""
                    sdbgRoles.Columns("UON2").Value = ""
                    sdbgRoles.Columns("UON3").Value = ""
                    sdbgRoles.Columns("DEP").Value = ""
                    sdbgRoles.Columns("EQP").Value = ""
                    sdbgRoles.Columns("SUSTITUCION").Value = ""
                    sdbgRoles.Columns("SUSTITUTO").Value = ""
                    sdbgRoles.Columns("TIPO_SUSTITUCION").Value = 0
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = 0
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = ""
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                    sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                    sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = False
                    sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = ""
                    
                    If Not sdbgRoles.IsAddRow Then
                        ''A�adir a la tabla PM_ROL_OBS_ETAPAS las etapas a las que el observador tiene visibilidad.
                        Set oBloques = oFSGSRaiz.Generar_CBloques
                        oBloques.DesAsignarEtapaObservador sdbgRoles.Columns("ID").Value
                        Set oBloques = Nothing
                    End If
                End If
                
            Case "OBSERVADOR"
                If sdbgRoles.Columns("OBSERVADOR").Value <> "" Then
                    If CBool(sdbgRoles.Columns("OBSERVADOR").Value) Then
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolObservador
                        sdbgRoles.Columns("COMPRADOR").Value = False
                        If sdbgRoles.Columns("PETICIONARIO").Value And Not sdbgRoles.Columns("PROVEEDOR").Value Then
                            'Solo se pone a False cuando estes en el rol peticionario, ya que podrias tener un rol
                            'peticionario, y en otro rol, marcar la columna Observador por ejemplo y esta variable deberia seguir a True
                            m_bHayPeticionario = False
                        End If
                        sdbgRoles.Columns("PROVEEDOR").Value = False
                        sdbgRoles.Columns("PETICIONARIO").Value = False
                        Valor_Gestor_Old = sdbgRoles.Columns("GESTOR").Value
                        sdbgRoles.Columns("GESTOR").Value = False
                        ActualizarHayGestor (Valor_Gestor_Old)
                        Valor_Receptor_Old = sdbgRoles.Columns("RECEPTOR").Value
                        sdbgRoles.Columns("RECEPTOR").Value = False
                        ActualizarHayReceptor (Valor_Receptor_Old)
                        If sdbgRoles.IsAddRow Then
                            sdbgRoles.Columns("CUANDO_ASIGNAR").Value = CuandoAsignarPMRol.Ahora
                            sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = m_arCuandoAsignar(CuandoAsignarPMRol.Ahora)
                            sdbgRoles.Columns("BLOQUE_ASIGNA").Value = 0
                            sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = ""
                            sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                            sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                            sdbgRoles.Columns("ROL_ASIGNA").Value = 0
                            sdbgRoles.Columns("CAMPO").Value = 0
                            sdbgRoles.Columns("CAMPO_BTN").Value = ""
                        End If

                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = False
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = VisibilidadObservadorPMRol.VisibilidadCompleta
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = m_arVisibilidadObservador(VisibilidadObservadorPMRol.VisibilidadCompleta)
                    
                        'Si se cambia de un tipo de Rol a observador quitar el permiso de escitura y obligatorio
                        If Not sdbgRoles.IsAddRow Then
                            Dim oRol As CPMRol
                            Set oRol = oFSGSRaiz.Generar_CPMRol
                            oRol.Id = sdbgRoles.Columns("ID").Value
                            oRol.ModificarVisibilidadObservador
                            Set oRol = Nothing
                            
                            'Al volver elegir el rol como observador, a�adir las visibilidad etapas del observador
                            Set oBloques = oFSGSRaiz.Generar_CBloques
                            oBloques.AsignarEtapasObservador sdbgRoles.Columns("ID").Value, lIdFlujo, , True
                            Set oBloques = Nothing
                        End If
                    
                    Else
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = 0
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = ""
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = False
                        
                        If Not sdbgRoles.IsAddRow Then
                            ''A�adir a la tabla PM_ROL_OBS_ETAPAS las etapas a las que el observador tiene visibilidad.
                            Set oBloques = oFSGSRaiz.Generar_CBloques
                            oBloques.DesAsignarEtapaObservador sdbgRoles.Columns("ID").Value
                            Set oBloques = Nothing
                        End If
                    End If
                    
                    If sdbgRoles.Columns("PROVE").Value <> "" Then
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                    End If
                    If sdbgRoles.IsAddRow Then
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = False
                    End If
                    sdbgRoles.Columns("CONTACTO").Value = ""
                    sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                    sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = ""
                End If
            Case "GESTOR"
                ActualizarHayGestor (Not sdbgRoles.Columns("GESTOR").Value)
                If sdbgRoles.Columns("GESTOR").Value <> "" Then
                    If CBool(sdbgRoles.Columns("GESTOR").Value) Then
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolGestor
                        sdbgRoles.Columns("COMPRADOR").Value = False
                        If sdbgRoles.Columns("PETICIONARIO").Value And Not sdbgRoles.Columns("PROVEEDOR").Value Then m_bHayPeticionario = False
                        sdbgRoles.Columns("PROVEEDOR").Value = False
                        sdbgRoles.Columns("PETICIONARIO").Value = False
                        sdbgRoles.Columns("OBSERVADOR").Value = False
                        Valor_Receptor_Old = sdbgRoles.Columns("RECEPTOR").Value
                        sdbgRoles.Columns("RECEPTOR").Value = False
                        ActualizarHayReceptor (Valor_Receptor_Old)
                    
                        sdbgRoles.Columns("CUANDO_ASIGNAR").Value = ""
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = ""
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = True
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("RESTRINGIR").Value = 0
                        sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                        sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                        sdbgRoles.Columns("PER").Value = ""
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                        sdbgRoles.Columns("UON1").Value = ""
                        sdbgRoles.Columns("UON2").Value = ""
                        sdbgRoles.Columns("UON3").Value = ""
                        sdbgRoles.Columns("DEP").Value = ""
                        sdbgRoles.Columns("EQP").Value = ""
                        sdbgRoles.Columns("SUSTITUCION").Value = ""
                        sdbgRoles.Columns("SUSTITUTO").Value = ""
                        sdbgRoles.Columns("TIPO_SUSTITUCION").Value = 0
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = 0
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = ""
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = ""
                        sdbgRoles.Columns("CONTACTO").Value = ""
                        sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                        sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("CONTACTO_DEN").Locked = True
                        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Locked = True
                        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Value = ""
                        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Value = ""
                        
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = ""
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").DropDownHwnd = 0
                
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                                
                        sdbgRoles.Columns("VER_FLUJO").Locked = True
                        sdbgRoles.Columns("VER_FLUJO").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("VER_FLUJO").Value = 0
                        
                        sdbgRoles.Columns("VER_DETALLE_PER").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("VER_DETALLE_PER").Locked = True
                        sdbgRoles.Columns("VER_DETALLE_PER").Value = 0
                        
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = True
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").Value = 0
                    Else
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
                    End If
                End If
                
            Case "RECEPTOR"
                ActualizarHayReceptor (Not sdbgRoles.Columns("RECEPTOR").Value)
                If sdbgRoles.Columns("RECEPTOR").Value <> "" Then
                    If CBool(sdbgRoles.Columns("RECEPTOR").Value) Then
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolReceptor
                        sdbgRoles.Columns("COMPRADOR").Value = False
                        If sdbgRoles.Columns("PETICIONARIO").Value And Not sdbgRoles.Columns("PROVEEDOR").Value Then m_bHayPeticionario = False
                        sdbgRoles.Columns("PROVEEDOR").Value = False
                        sdbgRoles.Columns("PETICIONARIO").Value = False
                        sdbgRoles.Columns("OBSERVADOR").Value = False
                        Valor_Gestor_Old = sdbgRoles.Columns("GESTOR").Value
                        sdbgRoles.Columns("GESTOR").Value = False
                        ActualizarHayGestor (Valor_Gestor_Old)
                    Else
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
                    End If
                End If
            Case "CUANDO_ASIGNAR", "CUANDO_ASIGNAR_DEN"
                If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" Then
                    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa Then
                        sdbgRoles.Columns("PER").Value = ""
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                        sdbgRoles.Columns("CONTACTO").Value = ""
                        sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                    ElseIf CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.Ahora Then
                        sdbgRoles.Columns("BLOQUE_ASIGNA").Value = 0
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = ""
                        sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                        sdbgRoles.Columns("ROL_ASIGNA").Value = 0
                        If sdbgRoles.Columns("TIPO").Value <> "" Then
                            If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolProveedor Then
''                                sdbgRoles.Columns("CONTACTO_DEN").Locked = False
''                                sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Normal", sdbgRoles.Row
                            End If
                        End If
                    ElseIf CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.ProveedorDelContrato Then
                        sdbgRoles.Columns("BLOQUE_ASIGNA").Value = 0
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = ""
                        sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                        sdbgRoles.Columns("ROL_ASIGNA").Value = 0
                        sdbgRoles.Columns("PER").Value = ""
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("CONTACTO").Value = ""
                        sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                    End If
                    sdbgRoles.Columns("CAMPO").Value = 0
                    sdbgRoles.Columns("CAMPO_BTN").Value = ""
                    sdbgRoles.Columns("RESTRINGIR").Value = 0
                    sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                    sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                End If
            Case "COMO_ASIGNAR", "COMO_ASIGNAR_DEN"
                If sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" Then
                    If CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo Then
                        sdbgRoles.Columns("RESTRINGIR").Value = 0
                        sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                        sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                    ElseIf CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorLista Then
                        sdbgRoles.Columns("CAMPO").Value = 0
                        sdbgRoles.Columns("CAMPO_BTN").Value = ""
                        sdbgRoles.Columns("RESTRINGIR").Value = RestringirPMRol.Lista
                        sdbgRoles.Columns("RESTRINGIR_DEN").Value = m_arRestringir(RestringirPMRol.Lista)
                    ElseIf CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorServicioExt Then
                        sdbgRoles.Columns("CAMPO").Value = 0
                        sdbgRoles.Columns("CAMPO_BTN").Value = ""
                        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
                        sdbgRoles.Columns("CUANDO_ASIGNAR").Value = 1
                    Else 'Por Rol
                        sdbgRoles.Columns("CAMPO").Value = 0
                        sdbgRoles.Columns("CAMPO_BTN").Value = ""
                    End If
                End If
            Case "RESTRINGIR", "RESTRINGIR_DEN"
                If sdbgRoles.Columns("RESTRINGIR").Value <> "" Then
                    'Borrar los participantes que ten�a para poder crearle unos nuevos
                    sdbgRoles.Columns("UON1").Value = ""
                    sdbgRoles.Columns("UON2").Value = ""
                    sdbgRoles.Columns("UON3").Value = ""
                    sdbgRoles.Columns("EQP").Value = ""
                    sdbgRoles.Columns("DEP").Value = ""
                    If sdbgRoles.IsAddRow Then
                        If Not m_oParticipantesAnyadir Is Nothing Then
                            m_oParticipantesAnyadir.clear
                        End If
                    Else
                        m_oRolEnEdicion.Participantes.clear
                    End If
                    
                    If Not CInt(sdbgRoles.Columns("RESTRINGIR").Value) > 0 Then
                        sdbgRoles.Columns("RESTRINGIR").Value = 0
                    End If
                    sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                    
                    If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionarioProveedor And g_iSolicitudTipoTipo = TipoSolicitud.Factura _
                    And CInt(sdbgRoles.Columns("RESTRINGIR").Value) > 0 Then
                        sdbgRoles.Columns("PER").Value = ""
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                        sdbgRoles.Columns("CONTACTO").Value = ""
                        sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    End If
                End If
            Case "PET_PER_PROVE_DEN"
                sdbgRoles.Columns("CONTACTO").Value = ""
                sdbgRoles.Columns("CONTACTO_DEN").Value = ""
            Case "VER_DETALLE_PER", "VER_FLUJO", "PERMITIR_TRASLADOS"
                sdbgRoles.Update
        End Select
    End If
    
End Sub

Private Sub sdbgRoles_GotFocus()
    If sdbgRoles.IsAddRow Then
        sdbgRoles.col = 1
    End If
End Sub

Private Sub sdbgRoles_InitColumnProps()
    sdbgRoles.Columns("DEN").caption = m_sDen
    sdbgRoles.Columns("PETICIONARIO").caption = m_sPeticionario
    sdbgRoles.Columns("COMPRADOR").caption = m_sComprador
    sdbgRoles.Columns("PROVEEDOR").caption = m_sProveedor
    sdbgRoles.Columns("OBSERVADOR").caption = m_sObservador
    sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").caption = m_sCuandoAsignar
    sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").caption = m_sBloqueAsigna
    sdbgRoles.Columns("COMO_ASIGNAR_DEN").caption = m_sComoAsignar
    sdbgRoles.Columns("CAMPO_BTN").caption = m_sCampo
    sdbgRoles.Columns("RESTRINGIR_DEN").caption = m_sRestringir
    sdbgRoles.Columns("RESTRINGIR_BTN").caption = m_sRestringirBtn
    sdbgRoles.Columns("PET_PER_PROVE_DEN").caption = m_sPetPerProve
    sdbgRoles.Columns("CONTACTO_DEN").caption = m_sContacto
    sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").caption = m_sVisibilidad
    sdbgRoles.Columns("VER_FLUJO").caption = m_sVer_flujo
    sdbgRoles.Columns("VER_DETALLE_PER").caption = m_sVerDetallePer
    sdbgRoles.Columns("SUSTITUCION").caption = m_sSustitucion
    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").caption = m_sVisibilidadRolObservador
    sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").caption = m_sEtapasRolObservador
    sdbgRoles.Columns("EMPRESAS").caption = m_sTituloEmpresa
    sdbgRoles.Columns("PERMITIR_TRASLADOS").caption = m_sTituloPermitirTraslados
    sdbgRoles.Columns("GESTOR").caption = m_sTituloGestor
    sdbgRoles.Columns("RECEPTOR").caption = m_sTituloReceptor
End Sub

Private Sub sdbgRoles_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete Then
            cmdEliminarRol_Click
        Exit Sub

    End If
End Sub

Private Sub sdbgRoles_KeyPress(KeyAscii As Integer)
    
    Select Case KeyAscii
        Case vbKeySpace
            If sdbgRoles.IsAddRow And m_bModifFlujo Then
                cmdAnyadirRol.Enabled = True
            End If
        Case vbKeyReturn
            If sdbgRoles.DataChanged Then
                sdbgRoles.Update
                If m_bError Then
                    Exit Sub
                End If
                cmdAnyadirRol.Enabled = True And m_bModifFlujo
            End If
        Case vbKeyBack
            If sdbgRoles.col >= 0 Then
                Select Case sdbgRoles.Columns(sdbgRoles.col).Name
                    Case "CUANDO_ASIGNAR_DEN"
                        sdbgRoles.Columns("CUANDO_ASIGNAR").Value = 0
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = ""
                    Case "BLOQUE_ASIGNA_DEN"
                        sdbgRoles.Columns("BLOQUE_ASIGNA").Value = 0
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Value = " "
                    Case "COMO_ASIGNAR_DEN"
                        If Not sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" Then
                            If CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) <> ComoAsignarPMRol.PorCampo Then
                                sdbgRoles.Columns("RESTRINGIR").Value = 0
                                sdbgRoles.Columns("RESTRINGIR_DEN").Value = ""
                                sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                                sdbgRoles.Columns("UON1").Value = ""
                                sdbgRoles.Columns("UON2").Value = ""
                                sdbgRoles.Columns("UON3").Value = ""
                                sdbgRoles.Columns("DEP").Value = ""
                                sdbgRoles.Columns("EQP").Value = ""
                            End If
                        End If
                        sdbgRoles.Columns("COMO_ASIGNAR").Value = 0
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Value = ""
                        sdbgRoles.Columns("CAMPO").Value = 0
                        sdbgRoles.Columns("CAMPO_BTN").Value = ""
                    Case "RESTRINGIR_DEN"
                        If Not sdbgRoles.Columns("RESTRINGIR").Locked Then
                            sdbgRoles.Columns("RESTRINGIR").Value = 0
                            sdbgRoles.Columns("RESTRINGIR_DEN").Value = " "
                            sdbgRoles.Columns("RESTRINGIR_BTN").Value = ""
                        End If
                    Case "PET_PER_PROVE_DEN"
                        sdbgRoles.Columns("PER").Value = ""
                        sdbgRoles.Columns("PROVE").Value = ""
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Value = ""
                        sdbgRoles.Columns("CONTACTO").Value = 0
                        sdbgRoles.Columns("CONTACTO_DEN").Value = ""
                        If sdbgRoles.Columns("PETICIONARIO").Value <> "" Then
                            If CBool(sdbgRoles.Columns("PETICIONARIO").Value) Then
                                sdbgRoles.Columns("UON1").Value = ""
                                sdbgRoles.Columns("UON2").Value = ""
                                sdbgRoles.Columns("UON3").Value = ""
                                sdbgRoles.Columns("DEP").Value = ""
                                sdbgRoles.Columns("EQP").Value = ""
                            End If
                        End If
                    Case "CONTACTO_DEN"
                        sdbgRoles.Columns("CONTACTO").Value = 0
                        sdbgRoles.Columns("CONTACTO_DEN").Value = " "
                End Select
                sdbgRoles_Change
            End If
    End Select
    
    If sdbgRoles.col > 0 Then
        If sdbgRoles.Columns(sdbgRoles.col).Locked Then KeyAscii = 0
    End If
End Sub

Private Sub sdbgRoles_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bMalaColumna As Boolean
    Dim iIndexMensaje As Integer
    Dim sColumnaARellenar As String
    Dim i As Integer
    
    If sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd <> 0 Then
        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
    End If
    
    If sdbgRoles.IsAddRow Then
        cmdAnyadirRol.Enabled = False
    Else
        cmdAnyadirRol.Enabled = True
    End If

    If sdbgRoles.col < 0 Then
        Exit Sub
    End If
    
    If sdbgRoles.Columns("TIPO").Value & "" = "" Then
        sdbgRoles.Columns("TIPO").Value = TipoPMRol.RolUsuario
    End If
    
    
    If sdbgRoles.IsAddRow Then  'Cambiamos de fila para a�adir
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("CAMPO_BTN").Locked = True
        sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
        sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
        sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("RESTRINGIR_BTN").Style = ssStyleButton
        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
        sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("CONTACTO_DEN").Locked = True
        sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("SUSTITUCION").Locked = True
        sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("SUSTITUCION").Style = ssStyleButton
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
        
        If sdbgRoles.Columns("GESTOR").Value = True Or sdbgRoles.Columns("RECEPTOR").Value = True Then
            sdbgRoles.Columns("VER_DETALLE_PER").Value = 0
            sdbgRoles.Columns("VER_DETALLE_PER").Locked = True
            sdbgRoles.Columns("VER_DETALLE_PER").CellStyleSet "Gris", sdbgRoles.Row
        Else
            sdbgRoles.Columns("VER_DETALLE_PER").Value = 1
        End If
        If sdbgRoles.Columns("PROVEEDOR").Value = True Or sdbgRoles.Columns("GESTOR").Value = True Or sdbgRoles.Columns("RECEPTOR").Value = True Then
            sdbgRoles.Columns("VER_FLUJO").Value = 0
            If sdbgRoles.Columns("GESTOR").Value = True Or sdbgRoles.Columns("RECEPTOR").Value = True Then
                sdbgRoles.Columns("VER_FLUJO").Locked = True
                sdbgRoles.Columns("VER_FLUJO").CellStyleSet "Gris", sdbgRoles.Row
            End If
        Else
            sdbgRoles.Columns("VER_FLUJO").Value = 1
        End If
        If sdbgRoles.Columns("GESTOR").Value = True Or sdbgRoles.Columns("RECEPTOR").Value = True Then
            sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Value = ""
        Else
            sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Value = "..."
        End If
        If sdbgRoles.Columns("PETICIONARIO").Value = True Or sdbgRoles.Columns("GESTOR").Value = True Or sdbgRoles.Columns("RECEPTOR").Value = True Then
            sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Gris", sdbgRoles.Row
            sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = True
            sdbgRoles.Columns("PERMITIR_TRASLADOS").Value = 0
        Else
            sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Normal", sdbgRoles.Row
            sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = False
            sdbgRoles.Columns("PERMITIR_TRASLADOS").Value = 1
        End If
    End If
    
   
    If sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = VisibilidadObservadorPMRol.VisibilidadParcial Then
        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = False
    Else
        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
    End If
    
    If Not m_bModifFlujo Then
        For i = 0 To sdbgRoles.Cols - 1
            sdbgRoles.Columns(i).Locked = True
            If sdbgRoles.Columns(i).Style = ssStyleComboBox Then
                sdbgRoles.Columns(i).Style = ssStyleEdit
            End If
        Next
    Else
        If Not sdbgRoles.IsAddRow Or IsNull(LastRow) Then 'Si no estamos a�adiendo o Si estamos a�adiendo pero cambiamos de columna
            If sdbgRoles.Columns(sdbgRoles.col).Name <> "CAMPO_BTN" And sdbgRoles.Columns(sdbgRoles.col).Name <> "RESTRINGIR_BTN" And sdbgRoles.Columns(sdbgRoles.col).Name <> "CON_BLOQUE_DEF_DEN" Then
                If sdbgRoles.Columns("TIPO").Value <> "" And sdbgRoles.Columns("TIPO").Value <> "0" Then
                    If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionario Or CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolGestor _
                    Or CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolReceptor Or CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionarioProveedor Then
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = True
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = True
                    Else
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = False
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").DropDownHwnd = sdbddCuandoAsigna.hWnd
                        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Style = ssStyleComboBox
                        If CInt(sdbgRoles.Columns("TIPO").Value) <> TipoPMRol.RolObservador Then
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEdit
                        Else
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleComboBox
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").DropDownHwnd = sdbddVisibilidadObservador.hWnd
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                        End If
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = False
                    End If
                End If
                If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" And sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> 0 Then
                    If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa Then
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = False
                        If sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleEdit Then
                            sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").DropDownHwnd = sdbddBloqueAsigna.hWnd
                            sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleComboBox
                        End If
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = False
                        If sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleEdit Then
                            sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleComboBox
                        End If
                        If sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" Then
                            If CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo Then
                                sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Normal", sdbgRoles.Row
                                sdbgRoles.Columns("CAMPO_BTN").Locked = False
                                sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                                sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                                sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
                                sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                            ElseIf CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorLista Then
                                sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
                                sdbgRoles.Columns("CAMPO_BTN").Locked = True
                                sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                                sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                                sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
                                sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False
                                sdbgRoles.Columns("RESTRINGIR_BTN").Style = ssStyleButton
                            ElseIf CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorServicioExt Then
                                sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
                                sdbgRoles.Columns("CAMPO_BTN").Locked = True
                                sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                                sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                                sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
                                sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False
                                sdbgRoles.Columns("RESTRINGIR_BTN").Style = ssStyleButton
                            Else 'Por rol
                                sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
                                sdbgRoles.Columns("CAMPO_BTN").Locked = True
                                sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_DEN").Locked = False
                                If sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit Then
                                    sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleComboBox
                                End If
                                sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                                sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False
                            End If
                        End If
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                        sdbgRoles.Columns("CONTACTO_DEN").Locked = True
                        sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEditButton
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = False
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = False
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Style = ssStyleButton
                        sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("SUSTITUCION").Style = ssStyleEditButton
                        sdbgRoles.Columns("SUSTITUCION").Locked = False
                    ElseIf CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.Ahora Then
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("CAMPO_BTN").Locked = True
                        If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionarioProveedor And g_iSolicitudTipoTipo = TipoSolicitud.Factura Then
                            sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("RESTRINGIR_DEN").Locked = False
                            sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = sdbddRestringir.hWnd
                            sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEdit
                            sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
                            sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("SUSTITUCION").Locked = True
                        Else
                            sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                            sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                            sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = False
                            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEditButton
                            sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = False
                            sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("SUSTITUCION").Locked = False
                        End If
                        sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("RESTRINGIR_BTN").Style = ssStyleButton
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Style = ssStyleButton
                        sdbgRoles.Columns("SUSTITUCION").Style = ssStyleEditButton
                        
                        If (g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoCatalogo Or g_iSolicitudTipoTipo = TipoSolicitud.SolicitudDePedidoContraPedidoAbierto) And CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionario _
                        Or (CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionarioProveedor And g_iSolicitudTipoTipo = TipoSolicitud.Factura And sdbgRoles.Columns("RESTRINGIR").Value > 0) Then
                            sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEdit
                            sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                        Else
                            sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEditButton
                            sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = False
                        End If
                        If sdbgRoles.Columns("PROVE").Value <> "" Then
                            sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("CONTACTO_DEN").Locked = False
                            If m_bModifFlujo Then
                                sdbgRoles.Columns("CONTACTO_DEN").DropDownHwnd = sdbddContactos.hWnd
                                sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleComboBox
                            End If
                        Else
                            sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("CONTACTO_DEN").Locked = True
                            sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleEdit
                        End If
                        
                    ElseIf CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.ProveedorDelContrato Then
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
                        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("CAMPO_BTN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_BTN").Style = ssStyleButton
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEditButton
                        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                        If sdbgRoles.Columns("PROVE").Value <> "" Then
                            sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Normal", sdbgRoles.Row
                            sdbgRoles.Columns("CONTACTO_DEN").Locked = False
                            If m_bModifFlujo Then
                                sdbgRoles.Columns("CONTACTO_DEN").DropDownHwnd = sdbddContactos.hWnd
                                sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleComboBox
                            End If
                        Else
                            sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                            sdbgRoles.Columns("CONTACTO_DEN").Locked = True
                            sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleEdit
                        End If
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
                        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Style = ssStyleButton
                        sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("SUSTITUCION").Style = ssStyleEditButton
                        sdbgRoles.Columns("SUSTITUCION").Locked = True
                    End If
                Else
                    sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
                    sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Style = ssStyleEdit
                    sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
                    sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("COMO_ASIGNAR_DEN").Style = ssStyleEdit
                    sdbgRoles.Columns("CAMPO_BTN").Locked = True
                    sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                    sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                    sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
                    sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                    sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("RESTRINGIR_BTN").Style = ssStyleButton
                    sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                    sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEdit
                    sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("CONTACTO_DEN").Locked = True
                    sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("CONTACTO_DEN").Style = ssStyleEdit
                    sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("SUSTITUCION").Locked = True
                End If
            End If
        End If
        If (Not sdbgRoles.IsAddRow Or IsNull(LastRow)) And CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) <> CuandoAsignarPMRol.ProveedorDelContrato Then
            sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Normal", sdbgRoles.Row
            sdbgRoles.Columns("SUSTITUCION").Locked = False
            sdbgRoles.Columns("SUSTITUCION").Style = ssStyleButton
        End If
        
        If sdbgRoles.Columns(sdbgRoles.col).Name = "VISIBILIDAD_OBSERVADOR_DEN" Then
            If sdbgRoles.Columns("TIPO").Value <> "" Then
                If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolObservador Then
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleComboBox
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").DropDownHwnd = sdbddVisibilidadObservador.hWnd
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
                End If
            End If
        End If
        
    
        bMalaColumna = False
        Select Case sdbgRoles.Columns(sdbgRoles.col).Name
            Case "BLOQUE_ASIGNA_DEN"
                If Not sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked Then
                    bMalaColumna = True
                    iIndexMensaje = 1
                    sColumnaARellenar = "CUANDO_ASIGNAR_DEN"
                    If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" Then
                        If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) > 0 Then
                            bMalaColumna = False
                        End If
                    End If
                End If
            Case "COMO_ASIGNAR_DEN"
                If Not sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked Then
                    bMalaColumna = True
                    iIndexMensaje = 1
                    sColumnaARellenar = "CUANDO_ASIGNAR_DEN"
                    If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" Then
                        If CLng(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) > 0 Then
                            bMalaColumna = False
                        End If
                    End If
                End If
            Case "RESTRINGIR_DEN"
                If Not sdbgRoles.Columns("RESTRINGIR_DEN").Locked Then
                    If CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo Or CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorLista Or CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.ProveedorDelContrato Then
                        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                    Else
                        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = sdbddRestringir.hWnd
                    End If
                
                    bMalaColumna = True
                    iIndexMensaje = 1
                    sColumnaARellenar = "CUANDO_ASIGNAR_DEN"
                    If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" Then
                        If CLng(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) > 0 Then
                            bMalaColumna = False
                        End If
                    End If
                End If
            Case "PET_PER_PROVE_DEN"
                If Not sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked Then
                    bMalaColumna = True
                    iIndexMensaje = 1
                    sColumnaARellenar = "CUANDO_ASIGNAR_DEN"
                    If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" Then
                        If CLng(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) > 0 Then
                            bMalaColumna = False
                        End If
                    End If
                End If
                
            Case "VISIBILIDAD_OBSERVADOR_DEN"
                If Not sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked And (CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolObservador) Then
                    bMalaColumna = True
                    iIndexMensaje = 6
                    sColumnaARellenar = "VISIBILIDAD_OBSERVADOR_DEN"
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEdit
                    sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").DropDownHwnd = 0
                    If sdbgRoles.Columns("PET_PER_PROVE_DEN").Value <> "" Then
                        bMalaColumna = False
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").DropDownHwnd = sdbddVisibilidadObservador.hWnd
                    End If
                ElseIf CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa And _
                    CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) <> ComoAsignarPMRol.PorCampo And _
                    sdbgRoles.Columns("RESTRINGIR").Value <> "" And sdbgRoles.Columns("RESTRINGIR").Value <> "0" Then
                    Dim bNoValido As Boolean
                    bNoValido = False
                    Select Case CInt(sdbgRoles.Columns("RESTRINGIR").Value)
                        Case RestringirPMRol.ProvesEquipo
                            If sdbgRoles.Columns("EQP").Value = "" Then
                                bNoValido = True
                            End If
                        Case RestringirPMRol.ProvesMaterial
                            If sdbgRoles.IsAddRow Then
                                If Not m_oParticipantesAnyadir Is Nothing Then
                                    If m_oParticipantesAnyadir.Count = 0 Then
                                        bNoValido = True
                                    End If
                                Else
                                    bNoValido = True
                                End If
                            Else
                                If Not m_oRolEnEdicion Is Nothing Then
                                    If Not m_oRolEnEdicion.Participantes Is Nothing Then
                                        If m_oRolEnEdicion.Participantes.Count = 0 Then
                                            bNoValido = True
                                        End If
                                    Else
                                        bNoValido = True
                                    End If
                                End If
                            End If
                            Case RestringirPMRol.Departamento
                                If sdbgRoles.Columns("DEP").Value = "" Then
                                    bNoValido = True
                                End If
                            Case RestringirPMRol.UON
                                If sdbgRoles.Columns("UON1").Value = "" Then
                                    bNoValido = True
                                End If
                            Case RestringirPMRol.DepartamentoPMRol
                            Case RestringirPMRol.UONPMRol
                            Case RestringirPMRol.Lista
                                If sdbgRoles.IsAddRow Then
                                    If Not m_oParticipantesAnyadir Is Nothing Then
                                        If m_oParticipantesAnyadir.Count = 0 Then
                                            bNoValido = True
                                        End If
                                    Else
                                        bNoValido = True
                                    End If
                                Else
                                    If Not m_oRolEnEdicion Is Nothing Then
                                        If Not m_oRolEnEdicion.Participantes Is Nothing Then
                                            If m_oRolEnEdicion.Participantes.Count = 0 Then
                                                bNoValido = True
                                            End If
                                        Else
                                            bNoValido = True
                                        End If
                                    End If
                                End If
                        End Select
                    If bNoValido Then
                        bMalaColumna = True
                        iIndexMensaje = 5
                        sColumnaARellenar = "VISIBILIDAD_OBSERVADOR_DEN"
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Style = ssStyleEdit
                        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").DropDownHwnd = 0
                    End If
                End If


        End Select
        If bMalaColumna Then
            oMensajes.NoValido m_arMensajesRol(iIndexMensaje)
            sdbgRoles.col = sdbgRoles.Columns(sColumnaARellenar).Position
        End If
    End If

End Sub

Private Sub sdbgRoles_RowLoaded(ByVal Bookmark As Variant)
    If sdbgRoles.Columns("RECEPTOR").Value = True Then
        m_bHayReceptor = True
    End If
    If sdbgRoles.Columns("GESTOR").Value = True Then
        m_bHayGestor = True
    End If
    If sdbgRoles.Columns("PETICIONARIO").Value = True Then
        m_bHayPeticionario = True
    End If
    
    If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolGestor Or CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolReceptor Then
        sdbgRoles.Columns("VER_FLUJO").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("VER_FLUJO").Locked = True
        sdbgRoles.Columns("VER_FLUJO").Value = 0
        sdbgRoles.Columns("VER_DETALLE_PER").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("VER_DETALLE_PER").Locked = True
        sdbgRoles.Columns("VER_DETALLE_PER").Value = 0
        sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("SUSTITUCION").Locked = True
        sdbgRoles.Columns("SUSTITUCION").DropDownHwnd = 0
        sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = True
        sdbgRoles.Columns("PERMITIR_TRASLADOS").Value = 0
        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = True
        sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
        sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
        sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("CAMPO_BTN").Locked = True
        sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEdit
        sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
        sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("CONTACTO_DEN").Locked = True
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
        sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = ""
        sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").CellStyleSet "Gris", sdbgRoles.Row
        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Locked = True
        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").Value = ""
        sdbgRoles.Columns("CONF_BLOQUE_DEF_DEN").DropDownHwnd = 0
        
    Else
        
        sdbgRoles.Columns("VER_FLUJO").CellStyleSet "Normal", sdbgRoles.Row
        sdbgRoles.Columns("VER_FLUJO").Locked = False
        If sdbgRoles.Columns("PROVEEDOR").Value = True Then
            sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Gris", sdbgRoles.Row
            sdbgRoles.Columns("SUSTITUCION").Locked = True
        Else
            sdbgRoles.Columns("SUSTITUCION").CellStyleSet "Normal", sdbgRoles.Row
            sdbgRoles.Columns("SUSTITUCION").Locked = False
        End If
        If sdbgRoles.Columns("PETICIONARIO").Value = True Then
            sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Gris", sdbgRoles.Row
            sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = True
            sdbgRoles.Columns("PERMITIR_TRASLADOS").Value = 0
        Else
            sdbgRoles.Columns("PERMITIR_TRASLADOS").CellStyleSet "Normal", sdbgRoles.Row
            sdbgRoles.Columns("PERMITIR_TRASLADOS").Locked = False
        End If
        If sdbgRoles.Columns("TIPO").Value <> "" And sdbgRoles.Columns("TIPO").Value <> "0" Then
            If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionario Then
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = True
            Else
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Locked = False Or (Not m_bModifFlujo)
            End If
        End If
        If sdbgRoles.Columns("CUANDO_ASIGNAR").Value <> "" Then
            If CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.EnEtapa Then
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = False Or (Not m_bModifFlujo)
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = False Or (Not m_bModifFlujo)
                If sdbgRoles.Columns("COMO_ASIGNAR").Value <> "" Then
                    If CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorCampo Then
                        sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("CAMPO_BTN").Locked = False Or (Not m_bModifFlujo)
                        sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                    ElseIf CInt(sdbgRoles.Columns("COMO_ASIGNAR").Value) = ComoAsignarPMRol.PorLista Then
                        sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("CAMPO_BTN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                        sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False Or (Not m_bModifFlujo)
                    Else 'Por rol
                        sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Gris", sdbgRoles.Row
                        sdbgRoles.Columns("CAMPO_BTN").Locked = True
                        sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_DEN").Locked = False Or (Not m_bModifFlujo)
                        sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                        sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False Or (Not m_bModifFlujo)
                    End If
                End If
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = m_arCuandoAsignar(CuandoAsignarPMRol.EnEtapa)
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEdit
                sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = True
                sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("CONTACTO_DEN").Locked = True
            ElseIf CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.Ahora Then
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
                sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("CAMPO_BTN").Locked = True
                If CInt(sdbgRoles.Columns("TIPO").Value) = TipoPMRol.RolPeticionarioProveedor And g_iSolicitudTipoTipo = TipoSolicitud.Factura Then
                    sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Normal", sdbgRoles.Row
                    sdbgRoles.Columns("RESTRINGIR_DEN").Locked = False
                    sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Normal", sdbgRoles.Row
                    sdbgRoles.Columns("RESTRINGIR_BTN").Locked = False
                Else
                    sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                    sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                    sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                End If
                sdbgRoles.Columns("RESTRINGIR_DEN").Style = ssStyleEdit
                sdbgRoles.Columns("RESTRINGIR_BTN").Style = ssStyleButton
                
                sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEditButton
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = False Or (Not m_bModifFlujo)
                If sdbgRoles.Columns("PROVE").Value <> "" Then
                    sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Normal", sdbgRoles.Row
                    sdbgRoles.Columns("CONTACTO_DEN").Locked = False Or (Not m_bModifFlujo)
                Else
                    sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("CONTACTO_DEN").Locked = True
                End If
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = m_arCuandoAsignar(CuandoAsignarPMRol.Ahora)
            ElseIf CInt(sdbgRoles.Columns("CUANDO_ASIGNAR").Value) = CuandoAsignarPMRol.ProveedorDelContrato Then
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("BLOQUE_ASIGNA_DEN").Locked = True
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("COMO_ASIGNAR_DEN").Locked = True
                sdbgRoles.Columns("CAMPO_BTN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("CAMPO_BTN").Locked = True
                sdbgRoles.Columns("RESTRINGIR_DEN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("RESTRINGIR_DEN").Locked = True
                sdbgRoles.Columns("RESTRINGIR_DEN").DropDownHwnd = 0
                sdbgRoles.Columns("RESTRINGIR_BTN").CellStyleSet "Gris", sdbgRoles.Row
                sdbgRoles.Columns("RESTRINGIR_BTN").Locked = True
                sdbgRoles.Columns("PET_PER_PROVE_DEN").CellStyleSet "Normal", sdbgRoles.Row
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Style = ssStyleEditButton
                sdbgRoles.Columns("PET_PER_PROVE_DEN").Locked = False Or (Not m_bModifFlujo)
                If sdbgRoles.Columns("PROVE").Value <> "" Then
                    sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Normal", sdbgRoles.Row
                    sdbgRoles.Columns("CONTACTO_DEN").Locked = False Or (Not m_bModifFlujo)
                Else
                    sdbgRoles.Columns("CONTACTO_DEN").CellStyleSet "Gris", sdbgRoles.Row
                    sdbgRoles.Columns("CONTACTO_DEN").Locked = True
                End If
                sdbgRoles.Columns("CUANDO_ASIGNAR_DEN").Value = m_arCuandoAsignar(CuandoAsignarPMRol.ProveedorDelContrato)
            End If
        End If
        
        If sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value > 0 And sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value <> "" Then
            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Normal", sdbgRoles.Row
            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Value = m_arVisibilidadObservador(CInt(sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value))
            
            If sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR").Value = VisibilidadObservadorPMRol.VisibilidadParcial Then
                sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = False
            Else
                sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
            End If
        Else
            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").CellStyleSet "Gris", sdbgRoles.Row
            sdbgRoles.Columns("VISIBILIDAD_OBSERVADOR_DEN").Locked = True
            sdbgRoles.Columns("ETAPAS_OBSERVADOR_BTN").Locked = True
        End If
    End If
End Sub

Private Sub CargarEmpresas()
Dim oEmpresas As CEmpresas
Dim rs As New Recordset
Set oEmpresas = oFSGSRaiz.Generar_CEmpresas

m_sEmpresas = ""
Set rs = oEmpresas.DevolverEmpresasSolicitud(g_lSolicitud)

While Not rs.EOF
    m_sEmpresas = m_sEmpresas & rs("EMP_ID").Value & ","
    rs.MoveNext
Wend

rs.Close
Set rs = Nothing
Set oEmpresas = Nothing
End Sub




