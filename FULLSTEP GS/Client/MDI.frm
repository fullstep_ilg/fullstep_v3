VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.MDIForm MDI 
   BackColor       =   &H8000000C&
   Caption         =   "FULLSTEP GS"
   ClientHeight    =   8070
   ClientLeft      =   5025
   ClientTop       =   5355
   ClientWidth     =   9675
   Icon            =   "MDI.frx":0000
   LinkTopic       =   "MDIForm1"
   NegotiateToolbars=   0   'False
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog cmmdAyuda 
      Left            =   960
      Top             =   1680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   5000
      Left            =   3510
      Top             =   690
   End
   Begin VB.Menu mnuMant 
      Caption         =   "&Mantenimiento"
      Begin VB.Menu mnuMON 
         Caption         =   "Monedas"
      End
      Begin VB.Menu mnuPAI 
         Caption         =   "Pa�ses"
      End
      Begin VB.Menu mnuESTRORGCab 
         Caption         =   "Estructura de la organizaci�n"
      End
      Begin VB.Menu mnuEstrMatCab 
         Caption         =   "Estructura de materiales"
         Begin VB.Menu mnuEstrMat 
            Caption         =   "Materiales"
            Index           =   1
         End
         Begin VB.Menu mnuEstrMat 
            Caption         =   "Unidades"
            Index           =   2
         End
         Begin VB.Menu mnuEstrMat 
            Caption         =   "Atributos"
            Index           =   3
         End
      End
      Begin VB.Menu mnuESTRCOMPCab 
         Caption         =   "Estructura de compras"
         Begin VB.Menu mnuESTRCOMP 
            Caption         =   "Compradores"
            Index           =   1
         End
         Begin VB.Menu mnuESTRCOMP 
            Caption         =   "Material por comprador"
            Index           =   2
         End
         Begin VB.Menu mnuESTRCOMP 
            Caption         =   "Compradores por material"
            Index           =   3
         End
      End
      Begin VB.Menu mnuPROVECab 
         Caption         =   "Proveedores"
         Begin VB.Menu mnuPROVE 
            Caption         =   "Datos b�sicos"
            Index           =   1
         End
         Begin VB.Menu mnuPROVE 
            Caption         =   "Proveedores del portal"
            Index           =   2
         End
         Begin VB.Menu mnuPROVE 
            Caption         =   "Material por proveedor"
            Index           =   3
         End
         Begin VB.Menu mnuPROVE 
            Caption         =   "Proveedores por material"
            Index           =   4
         End
         Begin VB.Menu mnuPROVE 
            Caption         =   "Equipos por proveedor"
            Index           =   5
         End
         Begin VB.Menu mnuPROVE 
            Caption         =   "Proveedores por equipo"
            Index           =   6
         End
      End
      Begin VB.Menu mnuPres 
         Caption         =   "Presupuestos"
         Begin VB.Menu mnuPRESMAT 
            Caption         =   "Por material"
         End
         Begin VB.Menu mnuPresAnuCab 
            Caption         =   "Anuales"
            Begin VB.Menu mnuPresAnu 
               Caption         =   "Por proyecto"
               Index           =   1
            End
            Begin VB.Menu mnuPresAnu 
               Caption         =   "Por partida contable"
               Index           =   2
            End
         End
         Begin VB.Menu mnuConcepto3 
            Caption         =   "Concepto 3"
         End
         Begin VB.Menu mnuConcepto4 
            Caption         =   "Concepto4"
         End
      End
      Begin VB.Menu mnuPAG 
         Caption         =   "Formas de pago"
      End
      Begin VB.Menu mnuVIAPAG 
         Caption         =   "V�as de pago"
      End
      Begin VB.Menu mnuTIPOSIMP 
         Caption         =   "DTipos de impuestos"
      End
      Begin VB.Menu mnuTIPOSPED 
         Caption         =   "DTipos de pedido"
      End
   End
   Begin VB.Menu mnuSolicit 
      Caption         =   "&Solicitudes"
      Begin VB.Menu mnuSolicitudes 
         Caption         =   "Formularios"
         Index           =   0
      End
      Begin VB.Menu mnuSolicitudes 
         Caption         =   "Cumplimentaci�n"
         Index           =   2
      End
   End
   Begin VB.Menu mnuGEST 
      Caption         =   "&Procesos"
      Begin VB.Menu mnuPROCE 
         Caption         =   "Solicitudes de compra"
         Index           =   0
      End
      Begin VB.Menu mnuPROCE 
         Caption         =   "Apertura "
         Index           =   1
      End
      Begin VB.Menu mnuPROCE 
         Caption         =   "Selecci�n de proveedores"
         Index           =   2
      End
      Begin VB.Menu mnuPROCE 
         Caption         =   "Comunicaci�n con proveedores"
         Index           =   3
      End
      Begin VB.Menu mnuPROCE 
         Caption         =   "Recepci�n de ofertas"
         Index           =   4
         Begin VB.Menu mnuOFER 
            Caption         =   "Buz�n de ofertas"
            Index           =   1
         End
         Begin VB.Menu mnuOFER 
            Caption         =   "Ofertas por proceso"
            Index           =   2
         End
      End
      Begin VB.Menu mnuPROCE 
         Caption         =   "Comparativa y adjudicaciones"
         Index           =   5
      End
      Begin VB.Menu mnuPROCE 
         Caption         =   "Contratos"
         Index           =   6
      End
   End
   Begin VB.Menu mnuREU 
      Caption         =   "&Reuniones"
      Begin VB.Menu mnuRes 
         Caption         =   "Administrador de reuniones"
         Index           =   1
      End
      Begin VB.Menu mnuRes 
         Caption         =   "Panel de toma de decisiones"
         Index           =   2
      End
   End
   Begin VB.Menu mnuPED 
      Caption         =   "&Pedidos"
      Begin VB.Menu mnuPEDSub 
         Caption         =   "Catalogo"
         Index           =   0
         Begin VB.Menu mnuPEDCATConf 
            Caption         =   "Configuracion"
            Index           =   0
         End
         Begin VB.Menu mnuPEDCATConf 
            Caption         =   "Gesti�n de Datos Externos"
            Index           =   1
         End
      End
      Begin VB.Menu mnuPEDSub 
         Caption         =   "Pedidos Directos"
         Index           =   1
      End
      Begin VB.Menu mnuPEDSub 
         Caption         =   "Seguimiento"
         Index           =   2
      End
      Begin VB.Menu mnuPEDREC 
         Caption         =   "Recepci�n"
      End
   End
   Begin VB.Menu mnuInf 
      Caption         =   "&Informes"
      Begin VB.Menu mnuInformes 
         Caption         =   "Ahorros negociados"
         Index           =   0
         Begin VB.Menu mnuInfGen 
            Caption         =   "General"
            Begin VB.Menu mnuInfGenReu 
               Caption         =   "En reuni�n"
            End
            Begin VB.Menu mnuInfGenDesde 
               Caption         =   "Desde / hasta fecha"
            End
         End
         Begin VB.Menu mnuinfMat 
            Caption         =   "Por material"
            Begin VB.Menu mnuInfMatReu 
               Caption         =   "En reuni�n"
            End
            Begin VB.Menu mnuInfMatDesde 
               Caption         =   "Desde / hasta fecha"
            End
         End
         Begin VB.Menu mnuInfEqpRes 
            Caption         =   "Por equipos responsables"
            Begin VB.Menu mnuInfEqpResReu 
               Caption         =   "En reuni�n"
            End
            Begin VB.Menu mnuInfEqpResDesde 
               Caption         =   "Desde hasta / fecha"
            End
         End
         Begin VB.Menu mnuInfEqpNeg 
            Caption         =   "Por equipos negociadores"
            Begin VB.Menu mnuInfEqpNegReu 
               Caption         =   "En reuni�n"
            End
            Begin VB.Menu mnuInfEqpNegDesde 
               Caption         =   "Desde hasta / fecha"
            End
         End
         Begin VB.Menu mnuInfConcep3 
            Caption         =   "Por concepto 3"
            Begin VB.Menu mnuInfConcep3Det 
               Caption         =   "En reuni�n"
               Index           =   1
            End
            Begin VB.Menu mnuInfConcep3Det 
               Caption         =   "Desde hasta / fecha"
               Index           =   2
            End
         End
         Begin VB.Menu mnuInfConcep4 
            Caption         =   "Por concepto 4"
            Begin VB.Menu mnuInfConcep4Det 
               Caption         =   "En reuni�n"
               Index           =   1
            End
            Begin VB.Menu mnuInfConcep4Det 
               Caption         =   "Desde hasta / fecha"
               Index           =   2
            End
         End
      End
      Begin VB.Menu mnuInformes 
         Caption         =   "Ahorros aplicados"
         Index           =   1
         Begin VB.Menu mnuInfApl 
            Caption         =   "General"
            Index           =   1
         End
         Begin VB.Menu mnuInfApl 
            Caption         =   "Por material"
            Index           =   2
         End
         Begin VB.Menu mnuInfApl 
            Caption         =   "Por proyectos"
            Index           =   3
         End
         Begin VB.Menu mnuInfApl 
            Caption         =   "Por partidas contables"
            Index           =   4
         End
         Begin VB.Menu mnuInfApl 
            Caption         =   "Por concepto 3"
            Index           =   5
         End
         Begin VB.Menu mnuInfApl 
            Caption         =   "Por concepto 4"
            Index           =   6
         End
         Begin VB.Menu mnuInfApl 
            Caption         =   "Por unidades organizativas"
            Index           =   7
         End
      End
      Begin VB.Menu mnuInformes 
         Caption         =   "Evoluci�n de PROY1"
         Index           =   2
      End
      Begin VB.Menu mnuInformes 
         Caption         =   "Evoluci�n de PROY2"
         Index           =   3
      End
      Begin VB.Menu mnuInformes 
         Caption         =   "&Listados"
         Index           =   4
      End
      Begin VB.Menu mnuInformes 
         Caption         =   "Listados &personalizados"
         Index           =   5
      End
      Begin VB.Menu mnuInformes 
         Caption         =   "Actualizaci�n de ahorros"
         Index           =   6
      End
   End
   Begin VB.Menu mnuPAR 
      Caption         =   "P&ar�metros"
      Begin VB.Menu mnuParametros 
         Caption         =   "Generales"
         Index           =   0
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Configuraci�n general"
            Index           =   0
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Roles persona / proceso"
            Index           =   1
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Calificaciones de proveedores"
            Index           =   2
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Plantillas para procesos"
            Index           =   3
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Estados de ofertas"
            Index           =   4
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Firmas"
            Index           =   5
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Asistentes fijos a reuniones"
            Index           =   6
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Listados personalizados"
            Index           =   8
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Tipos de solicitudes"
            Index           =   9
         End
         Begin VB.Menu mnuCONFGEN 
            Caption         =   "Tipos de relaci�n entre proveedores"
            Index           =   10
         End
      End
      Begin VB.Menu mnuParametros 
         Caption         =   "Integraci�n"
         Index           =   1
      End
      Begin VB.Menu mnuParametros 
         Caption         =   "Configuraci�n de la instalaci�n"
         Index           =   2
      End
      Begin VB.Menu mnuParametros 
         Caption         =   "Calidad"
         Index           =   3
         Begin VB.Menu mnuPARCalidad 
            Caption         =   "Configuraci�n de variables"
            Index           =   0
         End
         Begin VB.Menu mnuPARCalidad 
            Caption         =   "Calificaci�n de las puntuaciones"
            Index           =   1
         End
         Begin VB.Menu mnuPARCalidad 
            Caption         =   "Restricciones de adjudicaci�n"
            Index           =   2
         End
         Begin VB.Menu mnuPARCalidad 
            Caption         =   "Publicaci�n de puntuaciones en el portal"
            Index           =   3
         End
      End
   End
   Begin VB.Menu mnuSEGUR 
      Caption         =   "&Seguridad"
      Begin VB.Menu mnuSeguridad 
         Caption         =   "Usuarios"
         Index           =   0
      End
      Begin VB.Menu mnuSeguridad 
         Caption         =   "Perfiles"
         Index           =   1
      End
      Begin VB.Menu mnuSeguridad 
         Caption         =   "Listados personalizados"
         Index           =   2
      End
      Begin VB.Menu mnuSeguridad 
         Caption         =   "Registro de actividad"
         Index           =   3
         Begin VB.Menu mnuHISWEB 
            Caption         =   "Ofertas recibidas a trav�s del web"
         End
      End
      Begin VB.Menu mnuSeguridad 
         Caption         =   "Cambio de contrase�a"
         Index           =   4
      End
      Begin VB.Menu mnuSeguridad 
         Caption         =   "Cambio de contrase�a para el servidor"
         Index           =   5
      End
   End
   Begin VB.Menu mnuColaboracion 
      Caption         =   "&Colaboraci�n"
   End
   Begin VB.Menu mnuVer 
      Caption         =   "&Ver"
      Begin VB.Menu mnuProceEst 
         Caption         =   "Visor de procesos"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuVentana 
      Caption         =   "Ven&tana"
      WindowList      =   -1  'True
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "A&yuda"
      Begin VB.Menu mnuAcercade 
         Caption         =   "Abrir manual"
         Index           =   0
      End
      Begin VB.Menu mnuAcercade 
         Caption         =   "Descargar manual"
         Index           =   1
      End
      Begin VB.Menu mnuAcercade 
         Caption         =   "Acerca de FullStep GS"
         Index           =   2
      End
   End
   Begin VB.Menu mnuPopUpEstrOrgNivel0 
      Caption         =   "POPUP EstrOrgNivel0"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrOrgNiv0 
         Caption         =   "A�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv0 
         Caption         =   "A�adir departamento"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv0 
         Caption         =   "-"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv0 
         Caption         =   "Ordenado por c�digo"
         Checked         =   -1  'True
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv0 
         Caption         =   "Ordenado por denominaci�n"
         Index           =   5
      End
   End
   Begin VB.Menu mnuPopUpEstrOrgNivel1 
      Caption         =   "POPUP EstrOrgNivel1"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "A�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "A�adir departamento"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "Gestionar Centro de Coste"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "Modificar"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "Eliminar"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "Baja l�gica"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "Deshacer baja l�gica"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "Cambiar c�digo"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv1 
         Caption         =   "Detalle"
         Index           =   11
      End
   End
   Begin VB.Menu mnuPopUpEstrOrgNivel2 
      Caption         =   "POPUP EstrOrgNivel2"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "A�adir "
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "A�adir departamento"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "Gestionar Centro de Coste"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "Modificar"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "Eliminar"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "Baja l�gica"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "Deshacer baja l�gica"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "Cambiar c�digo"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv2 
         Caption         =   "Detalle"
         Index           =   11
      End
   End
   Begin VB.Menu mnuPopUpEstrOrgNivel3 
      Caption         =   "POPUP EstrOrgNivel3"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "A�adir Centro de Coste"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "A�adir departamento"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "Gestionar Centro de Coste"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "Modificar"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "Eliminar"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "Baja l�gica"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "Deshacer baja l�gica"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "Cambiar c�digo"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv3 
         Caption         =   "Detalle"
         Index           =   11
      End
   End
   Begin VB.Menu mnuPopUpEstrOrgNivel4 
      Caption         =   "POPUP EstrOrgNivel4"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "Gestionar Centro de Coste"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "Modificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "Eliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "Baja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrOrgNiv4 
         Caption         =   "Detalle"
         Index           =   8
      End
   End
   Begin VB.Menu mnuPopUpEstrOrgDep 
      Caption         =   "POPUP EstrOrgDep"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrOrgDepart 
         Caption         =   "A�adir persona"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrOrgDepart 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrOrgDepart 
         Caption         =   "Modificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrOrgDepart 
         Caption         =   "Eliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrOrgDepart 
         Caption         =   "Cambiar c�digo"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrOrgDepart 
         Caption         =   "-"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrOrgDepart 
         Caption         =   "Detalle"
         Index           =   7
      End
   End
   Begin VB.Menu mnuPopUpEstrOrgPer 
      Caption         =   "POPUP EstrOrgPer"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrOrgModPer 
         Caption         =   "Modificar"
      End
      Begin VB.Menu mnuPopUpEstrOrgReuPer 
         Caption         =   "Reubicar"
      End
      Begin VB.Menu mnuPopUpEstrOrgEliPer 
         Caption         =   "Eliminar "
      End
      Begin VB.Menu mnuPopUpEstrOrgBajaLogPer 
         Caption         =   "Baja l�gica"
      End
      Begin VB.Menu mnuPopUpEstrOrgDesBajaLogPer 
         Caption         =   "Deshacer baja l�gica"
      End
      Begin VB.Menu mnuPopUpEstrOrgCamCodPer 
         Caption         =   "Cambiar c�digo"
      End
      Begin VB.Menu mnuPopUpEstrOrgPerSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrOrgDetPer 
         Caption         =   "Detalle"
      End
   End
   Begin VB.Menu mnuPopUpUsuarios 
      Caption         =   "POPUP Usuarios"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpUsuAnya 
         Caption         =   "A�adir "
      End
   End
   Begin VB.Menu mnuPopUpUsuario 
      Caption         =   "POPUP Usuario"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpUsu 
         Caption         =   "Modificar"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpUsu 
         Caption         =   "Eliminar"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpUsu 
         Caption         =   "Detalle"
         Index           =   3
      End
   End
   Begin VB.Menu mnuPopUpEstrComp 
      Caption         =   "POPUP EstrComp"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrCompAnyaEqp 
         Caption         =   "A�adir equipo"
      End
      Begin VB.Menu mnuPopUpEstrCompSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrCompOrdPorCod 
         Caption         =   "Ordenado por c�digo"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuPopUpEstrCompOrdPorDen 
         Caption         =   "Ordenado por denominaci�n"
      End
   End
   Begin VB.Menu mnuPopUpEstrCompEqp 
      Caption         =   "POPUP EstrCompEqp"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrCompAnyaComp 
         Caption         =   "A�adir comprador"
      End
      Begin VB.Menu mnuPopUpEstrCompEqpSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrCompModEqp 
         Caption         =   "Modificar equipo"
      End
      Begin VB.Menu mnuPopUpEstrCompEliEqp 
         Caption         =   "Eliminar equipo"
      End
      Begin VB.Menu mnuPopUpEstrCompEqpCamCod 
         Caption         =   "Cambiar c�digo"
      End
      Begin VB.Menu mnuPopUpEstrCompEqpSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrCompDetEqp 
         Caption         =   "Detalle"
      End
   End
   Begin VB.Menu mnuPopUpEstrCompCom 
      Caption         =   "POPUP EstrCompCom"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrCompModCom 
         Caption         =   "Modificar comprador"
      End
      Begin VB.Menu mnuPOpUpEstrCompEliCom 
         Caption         =   "Eliminar comprador"
      End
      Begin VB.Menu mnuPopUpEstrCompComSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrCompDetCom 
         Caption         =   "Detalle"
      End
   End
   Begin VB.Menu mnuPopUpEstrPresProyN0Cab 
      Caption         =   "POPUP EstrPresProy"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresProyN0 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresProyN0 
         Caption         =   "DCopiar a otro a�o"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresProyN0 
         Caption         =   "DCopiar a otra U.O."
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresProyN0 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresProyN0 
         Caption         =   "DOrdenado por c�digo"
         Checked         =   -1  'True
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresProyN0 
         Caption         =   "DOrdenado por denominaci�n"
         Index           =   6
      End
   End
   Begin VB.Menu mnuPopUpEstrPresConN0Cab 
      Caption         =   "POPUP EstrPresCon"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresConN0 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresConN0 
         Caption         =   "DCopiar a otro a�o"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresConN0 
         Caption         =   "DCopiar a otra U.O."
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresConN0 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresConN0 
         Caption         =   "DOrdenar por c�digo"
         Checked         =   -1  'True
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresConN0 
         Caption         =   "DOrdenar por denominaci�n"
         Index           =   6
      End
   End
   Begin VB.Menu mnuPopUpEstrPresProyN1Cab 
      Caption         =   "POPUPEstrPresProyN1"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DBajaL�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DCambiar c�digo"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DCopiar a otro a�o"
         Index           =   11
      End
      Begin VB.Menu mnuPopUpEstrPresProyN1 
         Caption         =   "DCopiar a otra U.O."
         Index           =   12
      End
   End
   Begin VB.Menu mnuPopUpEstrPresConN1Cab 
      Caption         =   "POPUPEstrPresConN1"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DBaja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DCambiar c�digo"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DCopiar a otro a�o"
         Index           =   11
      End
      Begin VB.Menu mnuPopUpEstrPresConN1 
         Caption         =   "DCopiar a otra U.O."
         Index           =   12
      End
   End
   Begin VB.Menu mnuPopUpEstrPresProyN2Cab 
      Caption         =   "POPUPEstrPresProyN2"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DBaja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DCambiar c�digo"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DCopiar a otro a�o"
         Index           =   11
      End
      Begin VB.Menu mnuPopUpEstrPresProyN2 
         Caption         =   "DCopiar a otra U.O."
         Index           =   12
      End
   End
   Begin VB.Menu mnuPopUpEstrPresConN2Cab 
      Caption         =   "POPUPEstrPresConN2"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DBaja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DCambiar c�digo"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DCopiar a otro a�o"
         Index           =   11
      End
      Begin VB.Menu mnuPopUpEstrPresConN2 
         Caption         =   "DCopiar a otra U.O."
         Index           =   12
      End
   End
   Begin VB.Menu mnuPopUpEstrPresProyN3Cab 
      Caption         =   "POPUPEstrPresProyN3"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DBaja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DCambiar c�digo"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DCopiar a otro a�o"
         Index           =   11
      End
      Begin VB.Menu mnuPopUpEstrPresProyN3 
         Caption         =   "DCopiar a otra U.O."
         Index           =   12
      End
   End
   Begin VB.Menu mnuPopUpEstrPresConN3Cab 
      Caption         =   "POPUPEstrPresConN3"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DBaja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DCambiar c�digo"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DCopiar a otro a�o"
         Index           =   11
      End
      Begin VB.Menu mnuPopUpEstrPresConN3 
         Caption         =   "DCopiar a otra U.O."
         Index           =   12
      End
   End
   Begin VB.Menu mnuPopUpEstrPresProyN4Cab 
      Caption         =   "POPUPEstrPresDetN4"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "DModificar"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "DEliminar"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "DBaja l�gica"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "Deshacer baja l�gica"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "DCambiar c�digo"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "DDetalle"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "-"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "DCopiar a otro a�o"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresProyN4 
         Caption         =   "DCopiar a otra U.O."
         Index           =   10
      End
   End
   Begin VB.Menu mnuPopUpEstrPresConN4Cab 
      Caption         =   "POPUPEstrPresConN4"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "DModificar"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "DEliminar"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "DBaja l�gica"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "Deshacer baja l�gica"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "DCambiar"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "DDetalle"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "-"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "DCopiar a otro a�o"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpEstrPresConN4 
         Caption         =   "DCopiar a otra U.O."
         Index           =   10
      End
   End
   Begin VB.Menu mnuPopUpEstrMat 
      Caption         =   "POPUP EstrMat"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrMatAnyaGMN1 
         Caption         =   "A�adir commodity"
      End
      Begin VB.Menu mnuPopUpEstrMatSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrMatOrdPorCod 
         Caption         =   "Ordenado por c�digo"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuPopUpEstrMatOrdPorDen 
         Caption         =   "Ordenado por denominaci�n"
      End
   End
   Begin VB.Menu mnuPopUpEstrMatGMN1 
      Caption         =   "POPUP EstrMatGMN1"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrMatAnyaGMN2 
         Caption         =   "A�adir familia"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv1Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrMatEliGMN1 
         Caption         =   "Eliminar commodity"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv1CamCod 
         Caption         =   "Cambiar c�digo"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv1Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrMatDetGMN1 
         Caption         =   "Detalle"
      End
      Begin VB.Menu mnuPopUpEstrMatImpuestoGMN1 
         Caption         =   "Impuestos"
      End
   End
   Begin VB.Menu mnuPopUpEstrMatGMN2 
      Caption         =   "POPUPEstrMatFam"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrMatAnyaGMN3 
         Caption         =   "A�adir subfamilia"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv2Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrMatEliGMN2 
         Caption         =   "Eliminar familia"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv2CamCod 
         Caption         =   "Cambiar c�digo"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv2Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrMatDetGMN2 
         Caption         =   "Detalle"
      End
      Begin VB.Menu mnuPopUpEstrMatImpuestoGMN2 
         Caption         =   "Impuestos"
      End
   End
   Begin VB.Menu mnuPopUpEstrMatGMN3 
      Caption         =   "POPUPEstrMatSubFam"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpEstrMatAnyaGMN4 
         Caption         =   "A�adir grupo"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv3Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrMatEliGMN3 
         Caption         =   "Eliminar subfamilia"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv3CamCod 
         Caption         =   "Cambiar c�digo"
      End
      Begin VB.Menu mnuPopUpEstrMatNiv3Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPopUpEstrMatDetGMN3 
         Caption         =   "Detalle"
      End
      Begin VB.Menu mnuPopUpEstrMatImpuestoGMN3 
         Caption         =   "Impuestos"
      End
   End
   Begin VB.Menu mnuPopUpEstrMatGMN4 
      Caption         =   "POPUPEstrMatGru"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpGMN4EstrMat 
         Caption         =   "Eliminar grupo"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpGMN4EstrMat 
         Caption         =   "Cambiar c�digo"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpGMN4EstrMat 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpGMN4EstrMat 
         Caption         =   "Detalle"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpGMN4EstrMat 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpGMN4EstrMat 
         Caption         =   "Atributos"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpGMN4EstrMat 
         Caption         =   "Impuestos"
         Index           =   6
      End
   End
   Begin VB.Menu mnuVisor 
      Caption         =   "Visor de procesos"
      Visible         =   0   'False
      Begin VB.Menu mnuVisorApertura 
         Caption         =   "Apertura"
      End
      Begin VB.Menu mnuVisorSelProve 
         Caption         =   "Seleccion de proveedores"
      End
      Begin VB.Menu mnuVisorComuniProve 
         Caption         =   "Comunicaci�n con proveedores"
      End
      Begin VB.Menu mnuVisorRecOfe 
         Caption         =   "Recepci�n de ofertas"
      End
      Begin VB.Menu mnuVisorComparativa 
         Caption         =   "Comparativa y adjudicaciones"
      End
      Begin VB.Menu mnuVisorPanel 
         Caption         =   "Panel de toma de decisiones"
      End
   End
   Begin VB.Menu mnuPopUpOfe 
      Caption         =   "POPUPOfe"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpOfeMenu 
         Caption         =   "Detalle"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpOfeMenu 
         Caption         =   "Eliminar"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpOfeMenu 
         Caption         =   "A�adir"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpOfeMenu 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpOfeMenu 
         Caption         =   "Marcar como le�da"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpOfeMenu 
         Caption         =   "Marcar como no le�da"
         Index           =   6
      End
   End
   Begin VB.Menu mnuPopUpBuzOfe 
      Caption         =   "POPUPBuzOfe"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpIrBuzOfe 
         Caption         =   "Ir al buz�n de ofertas"
      End
   End
   Begin VB.Menu mnuEstrPresConcep3N0Cab 
      Caption         =   "POPUPEstrPresConcep3N0"
      Visible         =   0   'False
      Begin VB.Menu mnuEstrPresConcep3N0 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuEstrPresConcep3N0 
         Caption         =   "DCopiar a otra U.O."
         Index           =   2
      End
      Begin VB.Menu mnuEstrPresConcep3N0 
         Caption         =   "-"
         Index           =   3
      End
      Begin VB.Menu mnuEstrPresConcep3N0 
         Caption         =   "DOrdenar por c�digo"
         Checked         =   -1  'True
         Index           =   4
      End
      Begin VB.Menu mnuEstrPresConcep3N0 
         Caption         =   "DOrdenar por denominaci�n"
         Index           =   5
      End
   End
   Begin VB.Menu mnuEstrPresConcep4N0Cab 
      Caption         =   "POPUPEstrPresConcep4N0"
      Visible         =   0   'False
      Begin VB.Menu mnuEstrPresConcep4N0 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuEstrPresConcep4N0 
         Caption         =   "DCopiar a otra U.O."
         Index           =   2
      End
      Begin VB.Menu mnuEstrPresConcep4N0 
         Caption         =   "-"
         Index           =   3
      End
      Begin VB.Menu mnuEstrPresConcep4N0 
         Caption         =   "DOrdenar por c�digo"
         Checked         =   -1  'True
         Index           =   4
      End
      Begin VB.Menu mnuEstrPresConcep4N0 
         Caption         =   "DOrdenar por denominaci�n"
         Index           =   5
      End
   End
   Begin VB.Menu mnuEstrPresConcep3N1Cab 
      Caption         =   "POPUPEstrPresConcep3N1"
      Visible         =   0   'False
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "DBaja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "DCambiar c�digo"
         Index           =   8
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuEstrPresConcep3N1 
         Caption         =   "DCopiar a otra U.O."
         Index           =   11
      End
   End
   Begin VB.Menu mnuEstrPresConcep4N1Cab 
      Caption         =   "POPUPEstrPresConcep4N1"
      Visible         =   0   'False
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "DA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "DModificar"
         Index           =   3
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "DEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "DBaja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "DCambiar C�digo"
         Index           =   8
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "DDetalle"
         Index           =   9
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "-"
         Index           =   10
      End
      Begin VB.Menu mnuEstrPresConcep4N1 
         Caption         =   "DCopiar a otra U.O."
         Index           =   11
      End
   End
   Begin VB.Menu mnuEstrPresConcep3N4Cab 
      Caption         =   "POPOUPEstrPresConcep3N4"
      Visible         =   0   'False
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "DModificar"
         Index           =   1
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "DEliminar"
         Index           =   2
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "DBaja l�gica"
         Index           =   3
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "Deshacer baja l�gica"
         Index           =   4
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "DCambiar c�digo"
         Index           =   6
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "DDetalle"
         Index           =   7
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "-"
         Index           =   8
      End
      Begin VB.Menu mnuEstrPresConcep3N4 
         Caption         =   "DCopiar a otra U.O."
         Index           =   9
      End
   End
   Begin VB.Menu mnuEstrPresConcep4N4Cab 
      Caption         =   "POPUPEstrPresConcep4N4"
      Visible         =   0   'False
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "DModificar"
         Index           =   1
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "DEliminar"
         Index           =   2
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "DBaja l�gica"
         Index           =   3
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "Deshacer baja l�gica"
         Index           =   4
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "DCambiar c�digo"
         Index           =   6
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "DDetalle"
         Index           =   7
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "-"
         Index           =   8
      End
      Begin VB.Menu mnuEstrPresConcep4N4 
         Caption         =   "Copiar a otra U.O."
         Index           =   9
      End
   End
   Begin VB.Menu mnuPopUpComp 
      Caption         =   "POPUPComparativa"
      Visible         =   0   'False
      Begin VB.Menu mnuPopCompar 
         Caption         =   "()"
         Index           =   0
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "-"
         Index           =   1
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Datos del proveedor"
         Index           =   2
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Notificaciones"
         Index           =   3
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Datos de la oferta"
         Index           =   4
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Ficheros adjuntos"
         Index           =   5
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Contratos"
         Index           =   6
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Comentarios"
         Index           =   7
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Comparativa de calidad"
         Index           =   8
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Crear contrato"
         Index           =   9
         Visible         =   0   'False
      End
      Begin VB.Menu mnuPopCompar 
         Caption         =   "Ver contrato"
         Index           =   10
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuPopUpCatCatalogo 
      Caption         =   "POPUPCatCatalogo"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Modificar"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Eliminar"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Crear categor�a"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Crear subcategor�a"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Baja l�gica"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Deshacer baja l�gica"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Configurar seguridad"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Copiar seguridad"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Eliminar seguridad"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "Consultar seguridad"
         Index           =   11
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "-"
         Index           =   12
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "DMarcar categor�a integraci�n"
         Index           =   13
      End
      Begin VB.Menu mnuPopUpCatCatalog 
         Caption         =   "DConfiguracion"
         Index           =   14
      End
   End
   Begin VB.Menu mnuPopUpCATSeguridad 
      Caption         =   "POPUPCatSeguridadAdjud"
      Index           =   0
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpCATSeguridadAdj 
         Caption         =   "Marcar como aprobador"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpCATSeguridadAdj 
         Caption         =   "Marcar como notificado"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpCATSeguridadAdj 
         Caption         =   "Detalle"
         Index           =   2
      End
   End
   Begin VB.Menu mnuPopUpCATSeguridad 
      Caption         =   "POPUPCATSeguridadPedido"
      Index           =   1
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpCATSeguridadPed 
         Caption         =   "A�adir aprovisionador"
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu mnuPopUpCATSeguridadPed 
         Caption         =   "A�adir notificado"
         Index           =   1
         Visible         =   0   'False
      End
      Begin VB.Menu mnuPopUpCATSeguridadPed 
         Caption         =   "Sustituir"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpCATSeguridadPed 
         Caption         =   "Eliminar"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpCATSeguridadPed 
         Caption         =   "Detalle"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpCATSeguridadPed 
         Caption         =   "Modificar"
         Index           =   5
      End
   End
   Begin VB.Menu POPUPCrearProceso 
      Caption         =   "POPUPCrearProceso"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpCrearProceso 
         Caption         =   "Nuevo"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpCrearProceso 
         Caption         =   "Usar plantilla"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpCrearProceso 
         Caption         =   "Copiar"
         Index           =   3
      End
   End
   Begin VB.Menu POPUPGuardarPlant 
      Caption         =   "POPUPGuardarPlant"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpGuardarPlant 
         Caption         =   "Nueva"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpGuardarPlant 
         Caption         =   "Modificar"
         Index           =   2
      End
   End
   Begin VB.Menu POPUPGrupos 
      Caption         =   "POPUPGrupos"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpGrupos 
         Caption         =   "Configuraci�n"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpGrupos 
         Caption         =   "A�adir"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpGrupos 
         Caption         =   "A�adir grupo"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpGrupos 
         Caption         =   "Copiar grupo"
         Index           =   3
      End
   End
   Begin VB.Menu POPUPGrupo 
      Caption         =   "POPUPGrupo"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpGrupo 
         Caption         =   "Modificar"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpGrupo 
         Caption         =   "Eliminar"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpGrupo 
         Caption         =   "Cambiar codigo"
         Index           =   2
      End
   End
   Begin VB.Menu POPUPItemsModif 
      Caption         =   "POPUPItemsModif"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Modificar valores"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Destino"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Unidad"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Proveedor"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Cantidad"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Presupuesto"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Fecha inicio"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Fecha fin"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Forma de pago"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Solicitud"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Distribucion"
         Index           =   12
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Presupuestos"
         Index           =   13
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Especificaciones"
         Index           =   14
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "-"
         Index           =   15
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Anyadir"
         Index           =   16
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Copiar"
         Index           =   17
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Eliminar"
         Index           =   18
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "-"
         Index           =   19
      End
      Begin VB.Menu mnuPopUpItemsModif 
         Caption         =   "Cambiar de grupo"
         Index           =   20
      End
   End
   Begin VB.Menu POPUPplantillas 
      Caption         =   "POPUPPlantillas"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpPlantillas 
         Caption         =   "Modificar"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpPlantillas 
         Caption         =   "Eliminar"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpPlantillas 
         Caption         =   "Detalle"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpPlantillas 
         Caption         =   "Configurar vistas"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpPlantillas 
         Caption         =   "Eliminar vistas"
         Index           =   5
      End
   End
   Begin VB.Menu POPUPAnyadirAtr 
      Caption         =   "POPUPAnyadirAtr"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpAtr 
         Caption         =   "Existente"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpAtr 
         Caption         =   "Nuevo"
         Index           =   1
      End
   End
   Begin VB.Menu POPUPModifPresup 
      Caption         =   "POPUPModifPresup"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpModifPresup 
         Caption         =   "Pres Anu1"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpModifPresup 
         Caption         =   "Pres Anu2"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpModifPresup 
         Caption         =   "Pres1"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpModifPresup 
         Caption         =   "Pres2"
         Index           =   3
      End
   End
   Begin VB.Menu POPUPMostrarGrafico 
      Caption         =   "POPUPMostrarGrafico"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Precios por proceso / proveedor"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Puntos por proceso / proveedor"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Precios por grupo / proveedor"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Puntos por grupo / proveedor"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Evolucion de precios"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Precios por item / proveedor"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Puntos por item / proveedor"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Evoluci�n de precios"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpMostrarGrafico 
         Caption         =   "Evoluci�n por escalados"
         Index           =   8
      End
   End
   Begin VB.Menu POPUPSolicitudes 
      Caption         =   "POPUPSolicitudes"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Detalle"
         Index           =   0
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "dDetalle del desglose"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Modificar"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Asignar comprador"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Aprobar"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Rechazar"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Cerrar"
         Index           =   6
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Reabrir"
         Index           =   7
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Anular"
         Index           =   8
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "Eliminar"
         Index           =   9
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "DActivar monitorizacion"
         Index           =   10
      End
      Begin VB.Menu mnuPopUpSolicitudes 
         Caption         =   "DDesactivar monitorizacion"
         Index           =   11
      End
   End
   Begin VB.Menu mnuZoomP 
      Caption         =   "Zoom"
      Visible         =   0   'False
      Begin VB.Menu mnuZoom 
         Caption         =   "400%"
         Index           =   0
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "200%"
         Index           =   1
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "150%"
         Index           =   2
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "125%"
         Index           =   3
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "100%"
         Index           =   4
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "75%"
         Index           =   5
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "50%"
         Index           =   6
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "25%"
         Index           =   7
      End
      Begin VB.Menu mnuZoom 
         Caption         =   "10%"
         Index           =   8
      End
   End
   Begin VB.Menu mnuPOPUPContratos 
      Caption         =   "Contratos"
      Visible         =   0   'False
      Begin VB.Menu mnuNuevoContrato 
         Caption         =   "Generar contrato"
         Index           =   0
      End
      Begin VB.Menu mnuNuevoContrato 
         Caption         =   "Adjuntar contrato"
         Index           =   1
      End
   End
   Begin VB.Menu mnuPopUpCompItem 
      Caption         =   "POPUPComparativaItem"
      Visible         =   0   'False
      Begin VB.Menu mnuCompItemComent 
         Caption         =   "Comentarios"
      End
   End
   Begin VB.Menu mnuPopUpPresArtPres 
      Caption         =   "POPUPPresArtPres"
      Visible         =   0   'False
      Begin VB.Menu mnuPresArtPres 
         Caption         =   "Media del a�o anterior"
         Index           =   0
      End
      Begin VB.Menu mnuPresArtPres 
         Caption         =   "�tima adjudicaci�n del a�o anterior"
         Index           =   1
      End
   End
   Begin VB.Menu mnuPopUpPresArtMod 
      Caption         =   "POPUPPresArtMod"
      Visible         =   0   'False
      Begin VB.Menu mnuPresArtMod 
         Caption         =   "Modificar presupuesto unitario"
         Index           =   0
      End
      Begin VB.Menu mnuPresArtMod 
         Caption         =   "Modificar cantidad"
         Index           =   1
      End
      Begin VB.Menu mnuPresArtMod 
         Caption         =   "Modificar objetivo unitario"
         Index           =   2
      End
      Begin VB.Menu mnuPresArtMod 
         Caption         =   "Modificar presupuesto total"
         Index           =   3
      End
      Begin VB.Menu mnuPresArtMod 
         Caption         =   "Modificar objetivo total"
         Index           =   4
      End
   End
   Begin VB.Menu mnuPOPUPResponsable 
      Caption         =   "POPUPResponsable"
      Visible         =   0   'False
      Begin VB.Menu mnuResponsable 
         Caption         =   "Ver detalle"
         Index           =   0
      End
      Begin VB.Menu mnuResponsable 
         Caption         =   "Sustituir"
         Index           =   1
      End
   End
   Begin VB.Menu mnuPOPUPVisorPres 
      Caption         =   "POPUPVisorPres"
      Visible         =   0   'False
      Begin VB.Menu mnuVisorPresup 
         Caption         =   "DPresupuesto1"
         Index           =   0
      End
      Begin VB.Menu mnuVisorPresup 
         Caption         =   "DPresupuesto2"
         Index           =   1
      End
      Begin VB.Menu mnuVisorPresup 
         Caption         =   "DPresupuesto3"
         Index           =   2
      End
      Begin VB.Menu mnuVisorPresup 
         Caption         =   "DPresupuesto4"
         Index           =   3
      End
   End
   Begin VB.Menu mnuPOPUPMoverGrupo 
      Caption         =   "POPUPMoverGrupo"
      Visible         =   0   'False
      Begin VB.Menu mnuFormMoverGrupo 
         Caption         =   "DMover o copiar"
      End
   End
   Begin VB.Menu mnuPOPUPCamposGS 
      Caption         =   "POPUPMenuCamposGS"
      Visible         =   0   'False
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Proveedor"
         Index           =   1
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Forma pago"
         Index           =   2
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Moneda"
         Index           =   3
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Material"
         Index           =   4
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Art�culo"
         Index           =   5
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Unidad"
         Index           =   6
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Desglose"
         Index           =   7
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Pa�s"
         Index           =   8
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Provincia"
         Index           =   9
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Destino"
         Index           =   10
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Pres1"
         Index           =   11
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Pres2"
         Index           =   12
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Pres3"
         Index           =   13
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Pres4"
         Index           =   14
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "Persona"
         Index           =   15
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "NumSolicitERP"
         Index           =   16
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DUnidad organizativa"
         Index           =   17
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DDepartamento"
         Index           =   18
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DOrganizaci�n de compras"
         Index           =   19
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DCentro"
         Index           =   20
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DAlmac�n"
         Index           =   21
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DImporte solicitudes vinculadas"
         Index           =   22
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DReferencia a solicitud"
         Index           =   23
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DTipo de pedido"
         Index           =   24
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DCentro de coste"
         Index           =   25
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DActivo"
         Index           =   26
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DDesglose de actividad"
         Index           =   27
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DDesglose de factura"
         Index           =   28
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DFactura"
         Index           =   29
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DInicio abono"
         Index           =   30
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DFin abono"
         Index           =   31
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DRetencion en garantia"
         Index           =   32
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DUnidad de pedido"
         Index           =   33
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DProveedor ERP"
         Index           =   34
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DComprador"
         Index           =   35
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DDesglose de Pedido"
         Index           =   36
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DEmpresa"
         Index           =   37
      End
      Begin VB.Menu mnuCamposGS 
         Caption         =   "DA�o de imputaci�n"
         Index           =   38
      End
   End
   Begin VB.Menu mnuPOPUPAnyaCampo 
      Caption         =   "POPUPAnyaCampo"
      Visible         =   0   'False
      Begin VB.Menu mnuAnyaCampo 
         Caption         =   "DNuevo"
         Index           =   1
      End
      Begin VB.Menu mnuAnyaCampo 
         Caption         =   "DSolicitud de compra"
         Index           =   2
      End
      Begin VB.Menu mnuAnyaCampo 
         Caption         =   "DPredefinido"
         Index           =   3
         Begin VB.Menu mnuAnyaCampoPredef 
            Caption         =   "DSolicitud de compra"
            Index           =   1
         End
      End
      Begin VB.Menu mnuAnyaCampo 
         Caption         =   "DAtributo de GS"
         Index           =   4
      End
      Begin VB.Menu mnuAnyaCampo 
         Caption         =   "DCampo del sistema"
         Index           =   5
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DProveedor"
            Index           =   1
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "dForma de pago"
            Index           =   2
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DMoneda"
            Index           =   3
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DMaterial"
            Index           =   4
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DArticulo"
            Index           =   5
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DUnidad"
            Index           =   6
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "Desglose"
            Index           =   7
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DPais"
            Index           =   8
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DProvincia"
            Index           =   9
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DDestino"
            Index           =   10
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "Pres1"
            Index           =   11
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "Pres2"
            Index           =   12
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "Pres3"
            Index           =   13
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "Pres4"
            Index           =   14
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DPersona"
            Index           =   15
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "NumSolicitERP"
            Index           =   16
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DUnidad Organizativa"
            Index           =   17
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DDepartamento"
            Index           =   18
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DOrganizaci�n de compras"
            Index           =   19
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DCentro"
            Index           =   20
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DAlmacen"
            Index           =   21
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DImporte solicitudes vinculadas"
            Index           =   22
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DReferencia a solicitud"
            Index           =   23
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DTipo de pedido"
            Index           =   24
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DCentro de coste"
            Index           =   25
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DActivo"
            Index           =   26
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DDesglose de actividad"
            Index           =   27
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DDesglose de factura"
            Index           =   28
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DFactura"
            Index           =   29
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DInicio abono"
            Index           =   30
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DFin abono"
            Index           =   31
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DRetencion en garantia"
            Index           =   32
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DUnidad de pedido"
            Index           =   33
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DProveedor ERP"
            Index           =   34
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DComprador"
            Index           =   35
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DDesglose de Pedido"
            Index           =   36
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DEmpresa"
            Index           =   37
         End
         Begin VB.Menu mnuAnyaCampoGS 
            Caption         =   "DA�o de imputaci�n"
            Index           =   38
         End
      End
      Begin VB.Menu mnuAnyaCampo 
         Caption         =   "Campo externo"
         Index           =   6
         Begin VB.Menu mnuAnyaCampoExterno 
            Caption         =   "DCarga bajo demanda"
            Index           =   1
            Begin VB.Menu mnuAnyaCampoExternoDemanda 
               Caption         =   "DNuevo"
               Index           =   1
            End
         End
         Begin VB.Menu mnuAnyaCampoExterno 
            Caption         =   "DCarga periodica"
            Index           =   2
            Begin VB.Menu mnuAnyaCampoExternoPeriodico 
               Caption         =   "nose"
               Index           =   1
            End
         End
      End
   End
   Begin VB.Menu mnuPOPUPCopiarForm 
      Caption         =   "POPUPCopiarForm"
      Visible         =   0   'False
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DNuevoForm"
         Index           =   0
      End
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DCopiarForm"
         Index           =   1
      End
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DMarcarTitulo"
         Index           =   2
      End
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DDesmarcarTitulo"
         Index           =   3
      End
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DMarcarDenominacionProceso"
         Index           =   4
      End
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DDesmarcarDenominacionProceso"
         Index           =   5
      End
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DMarcarPeso"
         Index           =   6
      End
      Begin VB.Menu mnuPOPUPNewCopyForm 
         Caption         =   "DDesmarcarPeso"
         Index           =   7
      End
   End
   Begin VB.Menu mnuPOPUPTipoAtributoEsp 
      Caption         =   "POPUPTipoAtributoEsp"
      Visible         =   0   'False
      Begin VB.Menu mnuTipoAtributoEsp 
         Caption         =   "DDetalleAtributo"
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu mnuTipoAtributoEsp 
         Caption         =   "DMarcarComoInterno"
         Index           =   1
      End
      Begin VB.Menu mnuTipoAtributoEsp 
         Caption         =   "DMarcarComoExterno"
         Index           =   2
      End
      Begin VB.Menu mnuTipoAtributoEsp 
         Caption         =   "DMarcarParaPedido"
         Index           =   3
      End
      Begin VB.Menu mnuTipoAtributoEsp 
         Caption         =   "DDesmarcarParaPedido"
         Index           =   4
      End
      Begin VB.Menu mnuTipoAtributoEsp 
         Caption         =   "DMarcarSincronizar"
         Index           =   5
      End
      Begin VB.Menu mnuTipoAtributoEsp 
         Caption         =   "DDesmarcarSincronizar"
         Index           =   6
      End
   End
   Begin VB.Menu mnuPOPUPAtribInterno 
      Caption         =   "POPUPAtribInterno"
      Visible         =   0   'False
      Begin VB.Menu mnuAtribInterno 
         Caption         =   "DMarcarInterno"
         Index           =   0
      End
      Begin VB.Menu mnuAtribInterno 
         Caption         =   "DDesmarcarInterno"
         HelpContextID   =   1
         Index           =   1
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuEstrPres5Cab 
      Caption         =   "POPUPEstrPres5"
      Visible         =   0   'False
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "dA�adir"
         Index           =   1
      End
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "-"
         Index           =   2
      End
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "dModificar"
         Index           =   3
      End
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "dEliminar"
         Index           =   4
      End
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "dBajaLogica"
         Index           =   5
      End
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "dDeshacer Baja logica"
         Index           =   6
      End
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "-"
         Index           =   7
      End
      Begin VB.Menu mnuEstrPres5 
         Caption         =   "dDetalle"
         Index           =   8
      End
   End
   Begin VB.Menu mnuPopUpFSQAVarCalCab 
      Caption         =   "PopUpFSQAVarCal"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpFSQAVarCal 
         Caption         =   "Piezas defectuosas"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpFSQAVarCal 
         Caption         =   "Envios Correctos"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpFSQAVarCal 
         Caption         =   "Desmarcar como piezas defectuosas"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpFSQAVarCal 
         Caption         =   "Desmarcar como env�os correctos"
         Index           =   4
      End
      Begin VB.Menu mnuPopUpFSQAVarCal 
         Caption         =   "DMarcarPeso"
         Index           =   5
      End
      Begin VB.Menu mnuPopUpFSQAVarCal 
         Caption         =   "DDesmarcarPeso"
         Index           =   6
      End
   End
   Begin VB.Menu mnuPopUpFSQAVarCalDesgCab 
      Caption         =   "popUpFSQAVarCalDesg"
      Visible         =   0   'False
      Begin VB.Menu mnuPopUpFSQAVarCalDesg 
         Caption         =   "Piezas defectuosas Desglose"
         Index           =   1
      End
      Begin VB.Menu mnuPopUpFSQAVarCalDesg 
         Caption         =   "Envios Correctos Desglose"
         Index           =   2
      End
      Begin VB.Menu mnuPopUpFSQAVarCalDesg 
         Caption         =   "Desmarcar como piezas defectuosas DESG"
         Index           =   3
      End
      Begin VB.Menu mnuPopUpFSQAVarCalDesg 
         Caption         =   "Desmarcar como env�os correctos DESG"
         Index           =   4
      End
   End
   Begin VB.Menu mnuPOPUPSincroArtiCab 
      Caption         =   "POPUPSincroArti"
      Visible         =   0   'False
      Begin VB.Menu mnuPOPUPSincroArti 
         Caption         =   "DSincronizar con el art�culo"
         Index           =   1
      End
      Begin VB.Menu mnuPOPUPSincroArti 
         Caption         =   "DDessincronizar con el art�culo"
         Index           =   2
      End
   End
   Begin VB.Menu mnuPOPUPOptima 
      Caption         =   "POPUPOptima"
      Visible         =   0   'False
      Begin VB.Menu mnuOptima 
         Caption         =   "DCalcular �ptima por proceso"
         Index           =   0
      End
      Begin VB.Menu mnuOptima 
         Caption         =   "DCalcular �ptima por grupos"
         Index           =   1
      End
      Begin VB.Menu mnuOptima 
         Caption         =   "DCalcular �ptima por �tem"
         Index           =   2
      End
      Begin VB.Menu mnuOptima 
         Caption         =   "-"
         Index           =   3
      End
      Begin VB.Menu mnuOptima 
         Caption         =   "DConfigurar �ptima..."
         Index           =   4
      End
   End
End
Attribute VB_Name = "MDI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Para detectar que se va a otra aplicaci�n
Private Declare Function GetForegroundWindow Lib "user32" () As Long
Private Declare Function GetCurrentProcessId Lib "kernel32" () As Long

Private m_sTituloVisor As String
Private m_sIdiTipoOrig As String
Private m_sIdiDialogAyuda As String

' variables para Informes de Evoluci�n
Public g_ofrmInfEvolPres1 As frmInfEvolPres   'Proyectos
Public g_ofrmInfEvolPres2 As frmInfEvolPres   'Partidas

Public g_ofrmATRIB As frmAtrib 'formulario de mantenimiento de atributos
Public g_ofrmOrigenGraficos As Form
Public g_ofrmOrigenResponsable As Form

''' <summary>
''' Inicializa el menu cuando hay ep
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde:Form_Load Tiempo m�ximo: 0,1</remarks>
''' <revisado>JVS 10/04/2012</revisado>
Private Sub InicializarMenuConPermisosFSEP()
  Dim bTemp1 As Boolean
    Dim bTemp2 As Boolean
    Dim bTemp3 As Boolean
    Dim bTemp4 As Boolean
    Dim bTemp5 As Boolean
    Dim bTemp6 As Boolean
    Dim Ador As Ador.Recordset

    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True

    mnuESTRCOMPCab.Visible = False 'Estructura de compras
    mnuPROVE(5).Visible = False     'Proves por equipo
    mnuPROVE(6).Visible = False
    mnuPres.Visible = False
    mnuGEST.Visible = False
    mnuREU.Visible = False
    mnuPEDSub(1).Visible = False
    mnuInformes(0).Visible = False 'Informes negociados
    mnuInformes(1).Visible = False 'Informes aplicados
    mnuInformes(2).Visible = False 'Inform evolucion 1
    mnuInformes(3).Visible = False 'Inform evolucion 2
    mnuInformes(6).Visible = False
    mnuCONFGEN(1).Visible = False 'Roles
    mnuCONFGEN(3).Visible = False  'PlantillasProce
    mnuCONFGEN(4).Visible = False 'OfeEst
    mnuCONFGEN(5).Visible = False 'Firmas
    mnuCONFGEN(6).Visible = False 'Asistentes reu
    mnuSeguridad(3).Visible = False 'Seguridad - Reg. actividad
    mnuVer.Visible = False

    If Not oUsuarioSummit.EsAdmin Then
        If oUsuarioSummit.Acciones Is Nothing Then
            oMensajes.UsuarioNoAutorizado
            Set oGestorSeguridad = Nothing
            Set oUsuarioSummit = Nothing
            End
        ElseIf oUsuarioSummit.Acciones.Count = 0 Then
            oMensajes.UsuarioNoAutorizado
            Set oGestorSeguridad = Nothing
            Set oUsuarioSummit = Nothing
            End
        End If
        
        mnuSeguridad(0).Visible = False 'Seguridad - Usuarios
        mnuSeguridad(1).Visible = False 'Seguridad - Perfiles
        mnuCONFGEN(8).Visible = False 'PArametros Gen - Listados personalizados
        mnuSeguridad(2).Visible = False 'Seguridad - Listados personalizados
        mnuSeguridad(5).Visible = False 'Seguridad  - Cambio contrase�a servidor
           
        ' ************** Seguridad **************
        
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CAMBContrasenya)) Is Nothing Then
            mnuSEGUR.Visible = False
        End If
    End If
    
    ' ************** Monedas *********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MONConsultar)) Is Nothing Then Me.mnuMON.Visible = False
    ' ************** Paises **************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAIConsultar)) Is Nothing Then Me.mnuPAI.Visible = False
        
       ' **************  Estructura de materiales **************
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATConsultar)) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.UNIConsultar)) Is Nothing) Then bTemp2 = False
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    
    ' **************  Estructura de materiales **************
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATConsultar)) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.UNIConsultar)) Is Nothing) Then bTemp2 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBConsultar)) Is Nothing) Then bTemp3 = False
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 Then
        Me.mnuEstrMatCab.Visible = False
    Else
        Me.mnuEstrMat(1).Visible = bTemp1
        Me.mnuEstrMat(2).Visible = bTemp2
        Me.mnuEstrMat(3).Visible = bTemp3
    End If
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    
    mnuPopUpGMN4EstrMat(5).Visible = True 'Atri
    mnuPopUpGMN4EstrMat(4).Visible = False 'Sep2
                       
    '  ************** Proveedores **************
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEConsultar)) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVEConsultar)) Is Nothing) Then bTemp2 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorGRUPConsultar)) Is Nothing) Then bTemp3 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPortalConsultar)) Is Nothing) Then bTemp4 = False
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 Then
        Me.mnuPROVECab.Visible = False
    Else
        Me.mnuPROVE(1).Visible = bTemp1
        Me.mnuPROVE(3).Visible = bTemp2
        Me.mnuPROVE(4).Visible = bTemp3
        Me.mnuPROVE(2).Visible = bTemp4
    End If
    
    bTemp1 = True
        
    ' ************** Formas de Pago *********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAGConsultar)) Is Nothing Then _
        Me.mnuPAG.Visible = False
        
    ' ************** V�as de Pago ***********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.VIAPAGConsultar)) Is Nothing Then
        Me.mnuVIAPAG.Visible = False
    End If
    
    ' ************** Tipos de impuestos ***********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.TipImpConsultar)) Is Nothing Then
        If Not Me.mnuMON.Visible And Not mnuPAI.Visible And Not Me.mnuESTRORGCab.Visible And Not Me.mnuEstrMatCab.Visible And Not Me.mnuPROVECab.Visible And Not Me.mnuPAG.Visible And Not Me.mnuVIAPAG.Visible Then
           Me.mnuMant.Visible = False
        Else
            Me.mnuTIPOSIMP.Visible = False
        End If
        
        Me.mnuPopUpEstrMatImpuestoGMN1.Visible = False
        Me.mnuPopUpEstrMatImpuestoGMN2.Visible = False
        Me.mnuPopUpEstrMatImpuestoGMN3.Visible = False
        Me.mnuPopUpGMN4EstrMat(6).Visible = False
    End If
    
    '3328 Ocultar menu si no se tiene el permiso de consulta
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.TIPPEDConsultar)) Is Nothing Then
        Me.mnuTIPOSPED.Visible = False
    End If
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True
               
    ' ************** Pedidos**************
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGConsultar)) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtConsultar)) Is Nothing) Then bTemp2 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGConsultar)) Is Nothing) Then bTemp3 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECConsultar)) Is Nothing) Then bTemp4 = False
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 Then
        Me.mnuPED.Visible = False
    Else
        If Not bTemp1 And Not bTemp2 Then
            mnuPEDSub(0).Visible = False
        Else
            mnuPEDCATConf(0).Visible = bTemp1
            mnuPEDCATConf(1).Visible = bTemp2
        End If
        mnuPEDSub(2).Visible = bTemp3
        Me.mnuPEDREC.Visible = bTemp4
    End If
             
    ' ************** Informes**************
    If oUsuarioSummit.Perfil Is Nothing Then
        Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(oUsuarioSummit.Cod)
    Else
        Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(oUsuarioSummit.Cod, NullToStr(oUsuarioSummit.Perfil.Cod))
    End If
    If Ador Is Nothing Then
        mnuInformes(5).Visible = False
    Else
        mnuInformes(5).Visible = True
        Ador.Close
        Set Ador = Nothing
    End If

        
    ' ************** Par�metros *************
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PARGEN)) Is Nothing Then bTemp1 = False
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONFINST)) Is Nothing Then bTemp2 = False
    If Not basParametros.gParametrosGenerales.gbIntegracion Then
        bTemp3 = False
    Else
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PARGENInt)) Is Nothing Then bTemp3 = False
    End If
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not oUsuarioSummit.EsAdmin Then
        mnuPAR.Visible = False
    Else
        mnuParametros(0).Visible = bTemp1 Or oUsuarioSummit.EsAdmin
        mnuParametros(2).Visible = bTemp2
        mnuParametros(1).Visible = bTemp4
    End If
    mnuParametros(3).Visible = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FSQAMantVarCal)) Is Nothing)
        
    mnuPopUpEstrCompEqpCamCod.Visible = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPModificarCodigo)) Is Nothing)
        
    ' Administrador
    If Me.mnuInf.Visible Then
        Set oGestorInformes = oFSGSRaiz.Generar_CGestorInformes
    End If
      
    If basParametros.gParametrosGenerales.giINSTWEB <> ConPortal Then
        mnuPROVE(2).Visible = False
    End If
End Sub

''' Revisado por: blp. Fecha: 23/10/2012
''' <summary>
''' Inicializa el menu
''' </summary>
''' <remarks>Llamada desde:Form_Load Tiempo m�ximo: 0,1</remarks>
Private Sub InicializarMenuConPermisos()
    Dim bTemp1 As Boolean
    Dim bTemp2 As Boolean
    Dim bTemp3 As Boolean
    Dim bTemp4 As Boolean
    Dim bTemp5 As Boolean
    Dim bTemp6 As Boolean
    Dim bTemp7 As Boolean
    Dim bTemp8 As Boolean
    Dim bNoAcceso As Boolean
    Dim Ador As Ador.Recordset
    Dim oSolicitudes As CInstancias
    Set oSolicitudes = oFSGSRaiz.Generar_CInstancias
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True

    If oUsuarioSummit.Acciones Is Nothing Then Set oUsuarioSummit.Acciones = oFSGSRaiz.Generar_CAcciones

    'PRESUPUESTOS
    If Not basParametros.gParametrosGenerales.gbUsarPres1 And Not basParametros.gParametrosGenerales.gbUsarPres2 And _
       Not basParametros.gParametrosGenerales.gbUsarPres3 And Not basParametros.gParametrosGenerales.gbUsarPres4 Then
        Me.mnuPresAnuCab.Visible = False
        Me.mnuInfApl(3).Visible = False
        Me.mnuInfApl(4).Visible = False
        Me.mnuInformes(2).Visible = False
        Me.mnuInformes(3).Visible = False
        Me.mnuConcepto3.Visible = False
        Me.mnuInfApl(5).Visible = False
        Me.mnuInfConcep3.Visible = False
        Me.mnuConcepto4.Visible = False
        Me.mnuInfApl(6).Visible = False
        Me.mnuInfConcep4.Visible = False
        
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATConsultar)) Is Nothing) Then Me.mnuPres.Visible = False
    Else
        If Not basParametros.gParametrosGenerales.gbUsarPres1 And Not basParametros.gParametrosGenerales.gbUsarPres2 Then
            Me.mnuPresAnuCab.Visible = False
            Me.mnuInfApl(3).Visible = False
            Me.mnuInformes(2).Visible = False
            Me.mnuInfApl(4).Visible = False
            Me.mnuInformes(3).Visible = False
        Else
            If Not basParametros.gParametrosGenerales.gbUsarPres1 Then
                Me.mnuPresAnu(1).Visible = False
                Me.mnuInfApl(3).Visible = False
                Me.mnuInformes(2).Visible = False
            End If
            If Not basParametros.gParametrosGenerales.gbUsarPres2 Then
                Me.mnuPresAnu(2).Visible = False
                Me.mnuInfApl(4).Visible = False
                Me.mnuInformes(3).Visible = False
            End If
        End If
        If Not basParametros.gParametrosGenerales.gbUsarPres3 Then
            Me.mnuConcepto3.Visible = False
            Me.mnuInfApl(5).Visible = False
            Me.mnuInfConcep3.Visible = False
        End If
    
        If Not basParametros.gParametrosGenerales.gbUsarPres4 Then
            Me.mnuConcepto4.Visible = False
            Me.mnuInfApl(6).Visible = False
            Me.mnuInfConcep4.Visible = False
        End If
    End If
           
    'Pedidos
    If Not gParametrosGenerales.gbPedidosAprov And Not gParametrosGenerales.gbPedidosDirectos And Not gParametrosGenerales.gbPedidosERP Then
        mnuPED.Visible = False
    Else
        If Not gParametrosGenerales.gbPedidosAprov Then
            mnuPEDSub(0).Visible = False
        End If
        If Not gParametrosGenerales.gbPedidosDirectos Then
            mnuPEDSub(1).Visible = False
        End If
    End If
    
    If gParametrosGenerales.giINSTWEB = SinWeb Then
        mnuSeguridad(3).Visible = False
    End If

    'Compruebo si hay autenticaci�n de windows en GS y WEB para mostrar la opci�n de Cambio de contrase�a
    'Adem�s compruebo si hay autenticaci�n de LDAP en GS y WEB para mostrar la opci�n de Cambio de contrase�a
    If ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
            And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
        mnuSeguridad(4).Visible = False
    End If

    'INTEGRACION
    If Not basParametros.gParametrosGenerales.gbIntegracion Then
        mnuParametros(1).Visible = False ' Si no hay integraci�n, no se ve
    Else
        mnuParametros(1).Visible = True 'si hay integraci�n y es el administrador ve el menu
    End If
    
    'Solicitudes de compras:
    If Not basParametros.gParametrosGenerales.gbSolicitudesCompras Then
        mnuPROCE(0).Visible = False  'si no hay solicitudes de compras no se ve el men�
    Else
        mnuPROCE(0).Visible = True   'si hay solic.compras y es el administrador ve el menu
    End If
    
    'Solicitudes de tipo Factura, campos de sistema de Factura de abono
    If gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Then
        mnuCamposGS.Item(28).Visible = False
        mnuAnyaCampoGS.Item(28).Visible = False
        mnuCamposGS.Item(29).Visible = False
        mnuAnyaCampoGS.Item(29).Visible = False
        mnuCamposGS.Item(30).Visible = False
        mnuAnyaCampoGS.Item(30).Visible = False
        mnuCamposGS.Item(31).Visible = False
        mnuAnyaCampoGS.Item(31).Visible = False
    End If
     'Vamos a mirar si est� activo el par�metro plurianual en alguna partida presupuestaria y si no lo est� el menu lo ocultamos
    Dim oParam As CParametroSM
    mnuAnyaCampoGS.Item(38).Visible = False
    mnuCamposGS.Item(38).Visible = False
    If Not g_oParametrosSM Is Nothing Then
        For Each oParam In g_oParametrosSM
            If oParam.PluriAnual Then
                mnuAnyaCampoGS.Item(38).Visible = True
                mnuCamposGS.Item(38).Visible = True
                Exit For
            End If
        Next
    End If
    Set oParam = Nothing
    
    ' Si no hay licencia de GS se ocultan:
    ' - Men�s de procesos (menos solicitudes de compras)
    ' - Men� de reuniones
    ' - Informes: Ahorros negociados, Ahorros aplicados, Informe de Evoluci�n de Concepto1, Informe de Evoluci�n de Concepto2, Actualizaci�n de ahorros
    ' - Par�metros - Generales: Roles persona/proceso, Plantillas para procesos, Estados de ofertas, Asistentes fijos a reuniones
    ' - Seguridad: Perfiles, Registro de actividad
    ' - Men� Ver
    
    
     If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        ' Si no hay solicitudes de compras no se ve el men� principal de procesos
        If Not basParametros.gParametrosGenerales.gbSolicitudesCompras Then
            mnuGEST.Visible = False
        ' Si hay solicitudes de compras, s�lo se va a ver la opci�n de solic.compras
        Else
            mnuPROCE(1).Visible = False 'Apertura
            mnuPROCE(2).Visible = False 'Selecci�n de proveedores
            mnuPROCE(3).Visible = False 'Comunicaci�n con proveedores
            mnuPROCE(4).Visible = False 'Recepci�n de ofertas
            mnuPROCE(5).Visible = False 'Comparativa y adjudicaciones
        End If
        mnuREU.Visible = False 'reuniones
        mnuInformes(0).Visible = False 'Ahorros negociados
        mnuInformes(1).Visible = False 'Ahorros aplicados
        mnuInformes(2).Visible = False 'Evoluci�n de PROY1
        mnuInformes(3).Visible = False 'Evoluci�n de PROY2
        mnuInformes(6).Visible = False 'Actualizaci�n de ahorros
        mnuCONFGEN(1).Visible = False 'Roles persona/proceso
        mnuCONFGEN(3).Visible = False 'Plantillas para procesos
        mnuCONFGEN(4).Visible = False 'Estados de ofertas
        mnuCONFGEN(6).Visible = False 'Asistentes fijos a reuniones
        mnuSeguridad(1).Visible = oUsuarioSummit.EsAdmin  'Perfiles
        mnuSeguridad(3).Visible = oUsuarioSummit.EsAdmin 'Registro de actividad
        mnuVer.Visible = False 'Ver
    End If
    
    
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.SinAcceso _
    And gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso _
    And gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.SinAcceso _
    And gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso Then
        mnuSolicit.Visible = False
        mnuSolicitudes(2).Visible = bTemp1
        mnuSolicitudes(0).Visible = bTemp3
    End If
    
    'Para comprobar si se muestran las opciones de men�s de contratos
    Dim bVerContratos As Boolean
    'se mira el permiso del usuario para ver Contratos y que el m�dulo de contratos est� activado
    Dim bAccesoContratos As Boolean
    If Not oUsuarioSummit.Perfil Is Nothing Then
        bAccesoContratos = oUsuarioSummit.Perfil.FSCM
    Else
        bAccesoContratos = oUsuarioSummit.AccesoFSContratos
    End If
    bVerContratos = bAccesoContratos And (gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.SinAcceso)
    mnuPROCE(6).Visible = bVerContratos 'Contratos
    mnuPopCompar(6).Visible = bVerContratos 'crear contrato
    mnuPopCompar(9).Visible = bVerContratos 'crear contrato
    mnuPopCompar(10).Visible = bVerContratos 'ver contrato
        
    If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.AccesoFSWSCompleto _
    And gParametrosGenerales.gsAccesoContrato <> TipoAccesoContrato.AccesoContrato _
    And gParametrosGenerales.gsAccesoFSIM <> TipoAccesoFSIM.AccesoFSIM Then
        mnuCONFGEN(9).Visible = False 'Tipos de solicitudes
    End If
    
    If Not oUsuarioSummit.EsAdmin Then
        mnuSeguridad(0).Visible = False 'Seguridad  - Usuarios
        mnuSeguridad(1).Visible = False 'Seguridad  - Perfiles
        mnuSeguridad(3).Visible = False 'Seguridad  - Reg. actividad
        mnuCONFGEN(8).Visible = False 'Parametros Gen - Listados personalizados
        mnuSeguridad(2).Visible = False 'Seguridad  - Listados personalizados
        If ((gParametrosGenerales.giWinSecurity = Windows Or gParametrosGenerales.giWinSecurity = LDAP) _
        And (gParametrosGenerales.giWinSecurityWeb = Windows Or gParametrosGenerales.giWinSecurityWeb = LDAP)) Then
            mnuSEGUR.Visible = False
        Else
            mnuSeguridad(5).Visible = False 'Seguridad  - Cambio contr. servidor
        End If
        
        bNoAcceso = False
        If oUsuarioSummit.Acciones Is Nothing Then
            bNoAcceso = True
        ElseIf oUsuarioSummit.Acciones.Count = 0 Then
            bNoAcceso = True
        End If
        
        If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
            oUsuarioSummit.EsInvitadoAProcesos
        End If
            
        If bNoAcceso Then
            If Not oUsuarioSummit.EsInvitado Then
                oMensajes.UsuarioNoAutorizado
                Set oGestorSeguridad = Nothing
                Set oUsuarioSummit = Nothing
                End
            End If
        End If
        
        ' ************** Seguridad **************
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CAMBContrasenya)) Is Nothing Then
            Me.mnuSEGUR.Visible = False
        End If
        
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceConsulta)) Is Nothing Then
            If oUsuarioSummit.EsInvitado Then
                Me.mnuVer.Visible = True
            Else
                Me.mnuVer.Visible = False
            End If
        End If
    End If
        
    ' ************** Monedas *********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MONConsultar)) Is Nothing Then Me.mnuMON.Visible = False
    ' ************** Paises **************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAIConsultar)) Is Nothing Then Me.mnuPAI.Visible = False
    
    
    '  ************** Estructura de la organizacion **************
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultar)) Is Nothing) Then Me.mnuESTRORGCab.Visible = False
    
    ' **************  Estructura de materiales **************
            
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATConsultar)) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.UNIConsultar)) Is Nothing) Then bTemp2 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBConsultar)) Is Nothing) Then bTemp4 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtributoModif)) Is Nothing) Then bTemp3 = False
            
    If Not bTemp1 And Not bTemp2 And Not bTemp4 Then
            Me.mnuEstrMatCab.Visible = False
    Else
            Me.mnuEstrMat(1).Visible = bTemp1
            Me.mnuEstrMat(2).Visible = bTemp2
            Me.mnuEstrMat(3).Visible = bTemp4
    End If
    
    If Not bTemp3 Then
            Me.mnuPopUpGMN4EstrMat(5).Visible = False 'Atr
    End If
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
            
        
    ' **************  Estructura de compra **************
    
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPConsultar)) Is Nothing) And (oUsuarioSummit.Acciones.Item(AccionesDeSeguridad.COMPPorGRUPConsultar) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPPorGRUPConsultar)) Is Nothing) Then bTemp3 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorCOMPConsultar)) Is Nothing) Then bTemp2 = False
            
    If Not bTemp1 And Not bTemp2 And Not bTemp3 Then
            Me.mnuESTRCOMPCab.Visible = False
    Else
            Me.mnuESTRCOMP(1).Visible = bTemp1
            Me.mnuESTRCOMP(2).Visible = bTemp2
            Me.mnuESTRCOMP(3).Visible = bTemp3
    End If
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True
    
    '  ************** Proveedores **************
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEConsultar)) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.EQPPorPROVEConsultar)) Is Nothing) Then bTemp2 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorEQPConsultar)) Is Nothing) Then bTemp3 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.GRUPPorPROVEConsultar)) Is Nothing) Then bTemp4 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPorGRUPConsultar)) Is Nothing) Then bTemp5 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PROVEPortalConsultar)) Is Nothing) Then bTemp6 = False
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 Then
        Me.mnuPROVECab.Visible = False
    Else
        Me.mnuPROVE(1).Visible = bTemp1
        Me.mnuPROVE(5).Visible = bTemp2
        Me.mnuPROVE(6).Visible = bTemp3
        Me.mnuPROVE(3).Visible = bTemp4
        Me.mnuPROVE(4).Visible = bTemp5
        Me.mnuPROVE(2).Visible = bTemp6
    End If
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True
        
    '  ************** Presupuestos  **************

    If Not basParametros.gParametrosGenerales.gbUsarPres1 And Not basParametros.gParametrosGenerales.gbUsarPres2 And _
       Not basParametros.gParametrosGenerales.gbUsarPres3 And Not basParametros.gParametrosGenerales.gbUsarPres4 Then
    
    Else
        bTemp1 = True: bTemp2 = True: bTemp3 = True: bTemp4 = True: bTemp5 = True: bTemp6 = True
                           
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorMATConsultar)) Is Nothing) Then bTemp1 = False
        If Me.mnuPresAnuCab.Visible Then
                If Me.mnuPresAnu(1).Visible = True Then
                   If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorPROYConsultar)) Is Nothing) Then bTemp2 = False
                Else
                        bTemp2 = False
                End If
                If Me.mnuPresAnu(2).Visible = True Then
                   If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorParConConsultar)) Is Nothing) Then bTemp3 = False
                Else
                        bTemp3 = False
                End If
        Else
                bTemp2 = False: bTemp3 = False
        End If
        If Me.mnuConcepto3.Visible = True Then
           If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESConcepto3Consultar)) Is Nothing) Then bTemp4 = False
        Else
                bTemp4 = False
        End If
        If Me.mnuConcepto4.Visible = True Then
           If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESConcepto4Consultar)) Is Nothing) Then bTemp5 = False
        Else
                bTemp5 = False
        End If
                                           
        If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 Then
                Me.mnuPres.Visible = False
        Else
                Me.mnuPRESMAT.Visible = bTemp1
                
                If Not bTemp2 And Not bTemp3 Then
                         Me.mnuPresAnuCab.Visible = False
                Else
                          Me.mnuPresAnu(1).Visible = bTemp2
                          Me.mnuPresAnu(2).Visible = bTemp3
                End If
                Me.mnuConcepto3.Visible = bTemp4
                Me.mnuConcepto4.Visible = bTemp5
        End If
    End If
                
    ' ************** Formas de Pago *********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PAGConsultar)) Is Nothing Then _
            Me.mnuPAG.Visible = False
            
    ' ************** V�as de Pago *********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.VIAPAGConsultar)) Is Nothing Then
            Me.mnuVIAPAG.Visible = False
    End If
    
    ' ************** Tipos de impuestos ***********************
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.TipImpConsultar)) Is Nothing Then
        If Not Me.mnuMON.Visible And Not Me.mnuPAI.Visible And Not Me.mnuESTRCOMPCab.Visible And Not Me.mnuESTRORGCab.Visible And Not Me.mnuEstrMatCab.Visible _
                And Not Me.mnuPROVECab.Visible And Not Me.mnuPres.Visible And Not Me.mnuPAG.Visible And Not mnuVIAPAG.Visible Then
           Me.mnuMant.Visible = False
        Else
                Me.mnuTIPOSIMP.Visible = False
        End If
        
        Me.mnuPopUpEstrMatImpuestoGMN1.Visible = False
        Me.mnuPopUpEstrMatImpuestoGMN2.Visible = False
        Me.mnuPopUpEstrMatImpuestoGMN3.Visible = False
        Me.mnuPopUpGMN4EstrMat(6).Visible = False
    End If
               
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True
    bTemp8 = True
                        
    '  ************** Procesos **************
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        bTemp1 = False: bTemp2 = False: bTemp3 = False: bTemp4 = False: bTemp5 = False: bTemp6 = False
    Else
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEConsultar)) Is Nothing) Then bTemp1 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SELPROVEConsultar)) Is Nothing) Then bTemp2 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMUNIPROVEConsulta)) Is Nothing) Then bTemp3 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RECOFEConsultar)) Is Nothing) Then bTemp4 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.BUZOFEConsultar)) Is Nothing) Then bTemp5 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.OFECOMPAdjConsulta)) Is Nothing) Then bTemp6 = False
    End If
    If Not basParametros.gParametrosGenerales.gbSolicitudesCompras Then
            bTemp8 = False
    Else
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICConsultar)) Is Nothing) Then bTemp8 = False
    End If
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 And Not bTemp8 Then
        If Not oUsuarioSummit.EsInvitado Then
            Me.mnuGEST.Visible = False
        Else
            Me.mnuPROCE(0).Visible = False
            Me.mnuPROCE(1).Visible = True 'Apertura
            Me.mnuPROCE(2).Visible = True  'SelProve
            Me.mnuPROCE(3).Visible = True 'PetOfe
            Me.mnuOFER(2).Visible = True
            Me.mnuOFER(1).Visible = False
            Me.mnuPROCE(5).Visible = True 'ADJ
        End If
    Else
        Me.mnuPROCE(0).Visible = bTemp8
        If oUsuarioSummit.EsInvitado Then
            Me.mnuPROCE(1).Visible = True 'Apertura
            Me.mnuPROCE(2).Visible = True  'SelProve
            Me.mnuPROCE(3).Visible = True 'PetOfe
            Me.mnuOFER(2).Visible = True 'RecOfe
            Me.mnuOFER(1).Visible = bTemp5 'Buzon
            Me.mnuPROCE(5).Visible = True 'ADJ
        Else
            Me.mnuPROCE(1).Visible = bTemp1 'Apertura
            Me.mnuPROCE(2).Visible = bTemp2  'SelProve
            Me.mnuPROCE(3).Visible = bTemp3 'PetOfe
            If Not bTemp4 And Not bTemp5 Then
                    Me.mnuPROCE(4).Visible = False 'Ofertas
            Else
                    Me.mnuOFER(2).Visible = bTemp4
                    Me.mnuOFER(1).Visible = bTemp5
            End If
            Me.mnuPROCE(5).Visible = bTemp6 'ADJ
        End If
    End If
    
    bTemp1 = True
    bTemp2 = True
                           
    ' ************** Reuniones **************
    
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANREUConsultar)) Is Nothing) Then bTemp1 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.RESREUConsultar)) Is Nothing) Then bTemp2 = False
                
   If Not bTemp1 And Not bTemp2 Then
        If Not oUsuarioSummit.EsInvitado Then
            Me.mnuREU.Visible = False
        Else
            Me.mnuRes(1).Visible = False
            Me.mnuRes(2).Visible = True
        End If
    Else
        Me.mnuRes(1).Visible = bTemp1
        If oUsuarioSummit.EsInvitado Then
            Me.mnuRes(2).Visible = True

        Else
            Me.mnuRes(2).Visible = bTemp2
        End If
    End If
                                        
    ' ************** Pedidos**************
    If Not gParametrosGenerales.gbPedidosAprov And _
       Not gParametrosGenerales.gbPedidosDirectos Then
            mnuPED.Visible = False
    Else
        bTemp1 = True
        bTemp2 = True
        bTemp3 = True
        bTemp4 = True
        bTemp5 = True
        '   Aprovisionamiento
        If mnuPEDSub(0).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATALOGConsultar)) Is Nothing) Then bTemp1 = False
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CATDatExtConsultar)) Is Nothing) Then bTemp2 = False
        Else
                bTemp1 = False: bTemp2 = False
        End If
        'Ped directos
        If mnuPEDSub(1).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRConsultar)) Is Nothing) Then bTemp3 = False
        Else
                bTemp3 = False
        End If
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSEGConsultar)) Is Nothing) Then bTemp4 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDRECConsultar)) Is Nothing) Then bTemp5 = False
        If Not bTemp1 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp2 Then
            mnuPED.Visible = False
        Else
            If Not bTemp1 And Not bTemp2 Then 'DATOS EXT
                    mnuPEDSub(0).Visible = False
            Else
                    mnuPEDCATConf(0).Visible = bTemp1
                    mnuPEDCATConf(1).Visible = bTemp2
            End If
            mnuPEDSub(1).Visible = bTemp3
            mnuPEDSub(2).Visible = bTemp4
            mnuPEDREC.Visible = bTemp5
        End If
    End If
        
    ' ************** Informes**************
    
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True
    'Negociados
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        bTemp1 = False: bTemp2 = False: bTemp3 = False: bTemp4 = False: bTemp5 = False: bTemp6 = False
    Else
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegGenConsultar)) Is Nothing) Then bTemp1 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegmatConsultar)) Is Nothing) Then bTemp2 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegEqpResConsultar)) Is Nothing) Then bTemp3 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegEqpConsultar)) Is Nothing) Then bTemp4 = False
        If Me.mnuInfConcep3.Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon3Consultar)) Is Nothing) Then bTemp5 = False
        Else
                bTemp5 = False
        End If
        If Me.mnuInfConcep4.Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon4Consultar)) Is Nothing) Then bTemp6 = False
        Else
                bTemp6 = False
        End If
    End If
            
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 Then
        Me.mnuInformes(0).Visible = False
    Else
        Me.mnuInfGen.Visible = bTemp1
        Me.mnuinfMat.Visible = bTemp2
        Me.mnuInfEqpRes.Visible = bTemp3
        Me.mnuInfEqpNeg.Visible = bTemp4
        Me.mnuInfConcep3.Visible = bTemp5
        Me.mnuInfConcep4.Visible = bTemp6
    End If
        
   'Aplicados
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    bTemp5 = True
    bTemp6 = True
    bTemp7 = True
    
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        bTemp1 = False: bTemp2 = False: bTemp3 = False: bTemp4 = False: bTemp5 = False: bTemp6 = False: bTemp7 = False
    Else
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplGenConsultar)) Is Nothing) Then bTemp1 = False
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplMatConsultar)) Is Nothing) Then bTemp2 = False
        If Me.mnuInfApl(3).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplProyConsultar)) Is Nothing) Then bTemp3 = False
        Else
                bTemp3 = False
        End If
        If Me.mnuInfApl(4).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplPartConsultar)) Is Nothing) Then bTemp4 = False
        Else
                bTemp4 = False
        End If
        If Me.mnuInfApl(5).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplCon3Consultar)) Is Nothing) Then bTemp5 = False
        Else
                bTemp5 = False
        End If
        If Me.mnuInfApl(6).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplCon4Consultar)) Is Nothing) Then bTemp6 = False
        Else
                bTemp6 = False
        End If
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfAplUOConsultar)) Is Nothing) Then bTemp7 = False
    End If
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not bTemp5 And Not bTemp6 And Not bTemp7 Then
        Me.mnuInformes(1).Visible = False
    Else
        Me.mnuInfApl(1).Visible = bTemp1
        Me.mnuInfApl(2).Visible = bTemp2
        Me.mnuInfApl(3).Visible = bTemp3
        Me.mnuInfApl(4).Visible = bTemp4
        Me.mnuInfApl(5).Visible = bTemp5
        Me.mnuInfApl(6).Visible = bTemp6
        Me.mnuInfApl(7).Visible = bTemp7
    End If
    
    'Informe de Evoluci�n de Concepto1
    bTemp1 = True
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        bTemp1 = False
    Else
        If mnuInformes(2).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto1Consultar)) Is Nothing) Then bTemp1 = False
        Else
                bTemp1 = False
        End If
    End If
    Me.mnuInformes(2).Visible = bTemp1

    'Informe de Evoluci�n de Concepto2
    bTemp1 = True
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        bTemp1 = False
    Else
        If Me.mnuInformes(3).Visible Then
                If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfEvolucionConcepto2Consultar)) Is Nothing) Then bTemp1 = False
        Else
                bTemp1 = False
        End If
    End If
    Me.mnuInformes(3).Visible = bTemp1
    'Listados, siempre se ve
    'Listados personalizados
    If oUsuarioSummit.Perfil Is Nothing Then
        Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(oUsuarioSummit.Cod)
    Else
        Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(oUsuarioSummit.Cod, NullToStr(oUsuarioSummit.Perfil.Cod))
    End If
    If Ador Is Nothing Then
        mnuInformes(5).Visible = False
    Else
        mnuInformes(5).Visible = True
        Ador.Close
        Set Ador = Nothing
    End If
    'Actualizar ahorros
    bTemp1 = True
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        bTemp1 = False
    Else
        If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.AhorrosActualizar)) Is Nothing) Then bTemp1 = False
    End If
    
    mnuInformes(6).Visible = bTemp1
    
    ' ************** Par�metros *************
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    bTemp4 = True
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        bTemp1 = False
        bTemp3 = False
    Else
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PARGEN)) Is Nothing Then bTemp1 = False
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PLANTILLASConsultar)) Is Nothing Then bTemp3 = False
    End If
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.CONFINST)) Is Nothing Then bTemp2 = False
    If Not basParametros.gParametrosGenerales.gbIntegracion Then
        bTemp4 = False
    Else
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PARGENInt)) Is Nothing Then bTemp4 = False
    End If
    
    If Not bTemp1 And Not bTemp2 And Not bTemp3 And Not bTemp4 And Not oUsuarioSummit.EsAdmin Then
        mnuPAR.Visible = False
    Else
        mnuParametros(0).Visible = bTemp1 Or bTemp3 Or oUsuarioSummit.EsAdmin  ' las plantillas de procesos est�n metidas dentro de par�metros generales
        mnuParametros(2).Visible = bTemp2
        mnuParametros(1).Visible = bTemp4
    End If
        
    If bTemp3 Or bTemp1 Or oUsuarioSummit.EsAdmin Then
        If Not bTemp3 Then
            mnuCONFGEN(3).Visible = False  'PlantillasProce
            
            If Not bTemp1 Then
                mnuCONFGEN(0).Visible = oUsuarioSummit.EsAdmin 'ConfGen
                mnuCONFGEN(2).Visible = False
                mnuCONFGEN(5).Visible = oUsuarioSummit.EsAdmin 'Firmas
                'mnuCONFGEN(7).Visible = bVerContratos
                mnuCONFGEN(8).Visible = oUsuarioSummit.EsAdmin 'LisPer
                mnuCONFGEN(10).Visible = oUsuarioSummit.EsAdmin 'Tipos de relaci�n entre proves.
            End If
        Else
            If Not bTemp1 Then
                mnuCONFGEN(0).Visible = oUsuarioSummit.EsAdmin 'ConfGen
                mnuCONFGEN(1).Visible = False 'Roles
                mnuCONFGEN(3).Visible = True 'PlantillasProce
                mnuCONFGEN(2).Visible = False 'Homol
                mnuCONFGEN(4).Visible = False 'OfeEst
                mnuCONFGEN(5).Visible = oUsuarioSummit.EsAdmin 'Firmas
                mnuCONFGEN(6).Visible = False 'Asistentes
                mnuCONFGEN(8).Visible = oUsuarioSummit.EsAdmin 'LisPer
                mnuCONFGEN(9).Visible = False 'Tipos de solicitudes
            End If
        End If
    End If
        
    mnuParametros(3).Visible = ((gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso) And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FSQAMantVarCal)) Is Nothing)
                        
    'Solicitudes de compra
    bTemp1 = True
    bTemp2 = True
    bTemp3 = True
    
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.FORMULARIOConsultar)) Is Nothing) Then bTemp2 = False
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLCONFConsultar)) Is Nothing) Then bTemp3 = False
    If Not bTemp2 And Not bTemp3 Then
            mnuSolicit.Visible = False
    Else
            mnuSolicitudes(0).Visible = bTemp2
            mnuSolicitudes(2).Visible = bTemp3
    End If

    'Equipos
    mnuPopUpEstrCompEqpCamCod.Visible = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.COMPModificarCodigo)) Is Nothing)

    '''cambiarlo para la gesti�n de datos externos
    ' Administrador
    If Me.mnuInf.Visible Or Me.mnuVer.Visible Then
       Set oGestorInformes = oFSGSRaiz.Generar_CGestorInformes
    End If

    If basParametros.gParametrosGenerales.giINSTWEB <> ConPortal Then
       If Me.mnuPROVE(1).Visible = True Or Me.mnuPROVE(3).Visible = True Or Me.mnuPROVE(4).Visible = True Or Me.mnuPROVE(5).Visible = True Or Me.mnuPROVE(6).Visible = True Then
           mnuPROVE(2).Visible = False
       Else
           If Not Me.mnuMON.Visible And Not Me.mnuPAI.Visible And Not Me.mnuESTRCOMPCab.Visible And Not Me.mnuESTRORGCab.Visible And Not Me.mnuEstrMatCab.Visible And Not Me.mnuPAG.Visible And Not Me.mnuPres.Visible Then
              Me.mnuMant.Visible = False
           Else
               Me.mnuPROVECab.Visible = False
           End If
       End If
    End If
    
    If Not basParametros.gParametrosGenerales.gbOblProveEqp Then
       If Me.mnuPROVE(1).Visible = False And mnuPROVE(2).Visible = False And Me.mnuPROVE(3).Visible = False And Me.mnuPROVE(4).Visible = False Then
           If Not Me.mnuMON.Visible And Not Me.mnuPAI.Visible And Not Me.mnuESTRCOMPCab.Visible And Not Me.mnuESTRORGCab.Visible And Not Me.mnuEstrMatCab.Visible And Not Me.mnuPAG.Visible And Not Me.mnuPres.Visible Then
              Me.mnuMant.Visible = False
           Else
               Me.mnuPROVECab.Visible = False
           End If
       Else
           Me.mnuPROVE(6).Visible = False
           Me.mnuPROVE(5).Visible = False
       End If
    End If
 
    If mnuPROCE(1).Visible Then 'Apertura
        mnuVisorApertura.Visible = True
    Else
        mnuVisorApertura.Visible = False
    End If

    If mnuPROCE(2).Visible Then 'Selprove
        mnuVisorSelProve.Visible = True
    Else
        mnuVisorSelProve.Visible = False
    End If
    
    If mnuPROCE(3).Visible Then 'Petofe
        mnuVisorComuniProve.Visible = True
    Else
        mnuVisorComuniProve.Visible = False
    End If
    
    If mnuOFER(2).Visible And mnuPROCE(4).Visible Then 'frmOfeRec
        mnuVisorRecOfe.Visible = True
    Else
        mnuVisorRecOfe.Visible = False
    End If
    
    If mnuPROCE(5).Visible Then 'ADJ
        mnuVisorComparativa.Visible = True
    Else
        mnuVisorComparativa.Visible = False
    End If
    
    If mnuRes(2).Visible And Me.mnuREU.Visible Then
        mnuVisorPanel.Visible = True
    Else
        'Solo se hace invisible si alguno de los demas esta visible
        If mnuVisorApertura.Visible Or mnuVisorSelProve.Visible Or mnuVisorComuniProve.Visible Or mnuVisorRecOfe.Visible Or mnuVisorComparativa.Visible Then
            mnuVisorPanel.Visible = False
        End If
    End If
    
     'Campo Gs "C�digo de proveedor en ERP"
    If oSolicitudes.HayIntegracionSentidoSalida And basParametros.gParametrosGenerales.gbUsarOrgCompras Then
         mnuCamposGS.Item(34).Visible = True
         mnuAnyaCampoGS.Item(34).Visible = True
    Else
         mnuCamposGS.Item(34).Visible = False
         mnuAnyaCampoGS.Item(34).Visible = False
    End If
End Sub
Public Sub CambiarIdioma()
    ConfigurarNombresEnMenu
End Sub

''' <summary>
''' Procedimiento que recupera los textos descriptivos los controles del formulario en el idioma que corresponda
''' y se los asigna a cada uno de los controles
''' </summary>
''' <remarks>Llamada desde MDIForm_Load, CambiarIdioma; Tiempo m�ximo < 1 seg.</remarks>
Private Sub ConfigurarNombresEnMenu()
    'YA INDICE POPCOMPAR
    Dim Adores As Ador.Recordset
    Dim i As Integer
    
    Me.mnuPresAnu(1).caption = gParametrosGenerales.gsPlurPres1
    Me.mnuPresAnu(2).caption = gParametrosGenerales.gsPlurPres2
    Me.mnuConcepto3.caption = gParametrosGenerales.gsPlurPres3
    Me.mnuConcepto4.caption = gParametrosGenerales.gsplurpres4
    
    mnuVisorPresup(0).caption = gParametrosGenerales.gsPlurPres1
    mnuVisorPresup(1).caption = gParametrosGenerales.gsPlurPres2
    mnuVisorPresup(2).caption = gParametrosGenerales.gsPlurPres3
    mnuVisorPresup(3).caption = gParametrosGenerales.gsplurpres4
    
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'Informes aplicados y negociados
    mnuInfApl(4).caption = gParametrosGenerales.gsPlurPres2
    mnuInfApl(3).caption = gParametrosGenerales.gsPlurPres1
    mnuInfApl(5).caption = gParametrosGenerales.gsPlurPres3
    mnuInfApl(6).caption = gParametrosGenerales.gsplurpres4
    mnuInfConcep3.caption = gParametrosGenerales.gsPlurPres3
    mnuInfConcep4.caption = gParametrosGenerales.gsplurpres4
    
    'Apertura
    mnuPopUpModifPresup(0).caption = gParametrosGenerales.gsSingPres1
    mnuPopUpModifPresup(1).caption = gParametrosGenerales.gsSingPres2
    mnuPopUpModifPresup(2).caption = gParametrosGenerales.gsSingPres3
    mnuPopUpModifPresup(3).caption = gParametrosGenerales.gsSingPres4

    mnuCamposGS(11).caption = gParametrosGenerales.gsSingPres1
    mnuCamposGS(12).caption = gParametrosGenerales.gsSingPres2
    mnuCamposGS(13).caption = gParametrosGenerales.gsSingPres3
    mnuCamposGS(14).caption = gParametrosGenerales.gsSingPres4
    
    mnuAnyaCampoGS(11).caption = gParametrosGenerales.gsSingPres1
    mnuAnyaCampoGS(12).caption = gParametrosGenerales.gsSingPres2
    mnuAnyaCampoGS(13).caption = gParametrosGenerales.gsSingPres3
    mnuAnyaCampoGS(14).caption = gParametrosGenerales.gsSingPres4

    
    'Cargar los men�s en el idioma correspondiente
    
    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(162, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Adores Is Nothing Then
    
        mnuMant.caption = Adores(0).Value
        Adores.MoveNext
        mnuMON.caption = Adores(0).Value
        Adores.MoveNext
        mnuPAI.caption = Adores(0).Value
        Adores.MoveNext
        Adores.MoveNext
        mnuESTRORGCab.caption = Adores(0).Value
        Adores.MoveNext
        Adores.MoveNext
        Adores.MoveNext
        mnuEstrMatCab.caption = Adores(0).Value
        Adores.MoveNext
        mnuEstrMat(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuEstrMat(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuESTRCOMPCab.caption = Adores(0).Value
        Adores.MoveNext
        mnuESTRCOMP(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuESTRCOMP(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuESTRCOMP(3).caption = Adores(0).Value
        Adores.MoveNext
        mnuPROVECab.caption = Adores(0).Value
        Adores.MoveNext
        mnuPROVE(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPROVE(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuPROVE(3).caption = Adores(0).Value
        Adores.MoveNext
        mnuPROVE(4).caption = Adores(0).Value
        Adores.MoveNext
        mnuPROVE(5).caption = Adores(0).Value '20
        Adores.MoveNext
        mnuPROVE(6).caption = Adores(0).Value
        Adores.MoveNext
        mnuPres.caption = Adores(0).Value
        mnuPopUpItemsModif(13).caption = Adores(0).Value
        Adores.MoveNext
        mnuPRESMAT.caption = Adores(0).Value
        Adores.MoveNext
        mnuGEST.caption = Adores(0).Value
        Adores.MoveNext
        mnuPROCE(1).caption = Adores(0).Value 'Apertura
        Adores.MoveNext
        mnuPROCE(2).caption = Adores(0).Value 'SelProve
        Adores.MoveNext
        mnuPROCE(3).caption = Adores(0).Value 'PetOfe
        Adores.MoveNext
        mnuPROCE(4).caption = Adores(0).Value 'Ofertas
        Adores.MoveNext
        mnuPROCE(5).caption = Adores(0).Value 'Adj
        Adores.MoveNext
        mnuREU.caption = Adores(0).Value '30
        Adores.MoveNext
        mnuRes(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuRes(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuInf.caption = Adores(0).Value
        Adores.MoveNext
        mnuInformes(0).caption = Adores(0).Value 'Ahorros negociados
        Adores.MoveNext
        mnuInfGen.caption = Adores(0).Value
        Adores.MoveNext
        mnuInfGenReu.caption = Adores(0).Value
        Adores.MoveNext
        mnuInfGenDesde.caption = Adores(0).Value
        Adores.MoveNext
        mnuinfMat.caption = Adores(0).Value
        Adores.MoveNext
        mnuInfMatReu.caption = Adores(0).Value
        mnuInfConcep3Det(1).caption = Adores(0).Value
        mnuInfConcep4Det(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuInfMatDesde.caption = Adores(0).Value  '40
        mnuInfConcep3Det(2).caption = Adores(0).Value
        mnuInfConcep4Det(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuInfEqpRes.caption = Adores(0).Value
        Adores.MoveNext
        mnuInfEqpResReu.caption = Adores(0).Value
        Adores.MoveNext
        mnuInfEqpResDesde.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuInfEqpNeg.caption = Adores(0).Value
        Adores.MoveNext
           
        mnuInfEqpNegReu.caption = Adores(0).Value
        Adores.MoveNext
          
        mnuInfEqpNegDesde.caption = Adores(0).Value
        Adores.MoveNext
             
        mnuInformes(1).caption = Adores(0).Value
        Adores.MoveNext
       
        mnuInfApl(1).caption = Adores(0).Value
        Adores.MoveNext
        
        mnuInfApl(2).caption = Adores(0).Value '50
        Adores.MoveNext
        
        mnuInfApl(7).caption = Adores(0).Value
        Adores.MoveNext
             
        mnuInformes(4).caption = Adores(0).Value
        Adores.MoveNext
          
        mnuPAR.caption = Adores(0).Value
        Adores.MoveNext
             
        mnuParametros(0).caption = Adores(0).Value
        Adores.MoveNext
       
        mnuCONFGEN(0).caption = Adores(0).Value 'ConfGen
        Adores.MoveNext
               
        mnuPAG.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuCONFGEN(1).caption = Adores(0).Value 'Roles
        Adores.MoveNext
        
        mnuCONFGEN(2).caption = Adores(0).Value 'Hom
        Adores.MoveNext
       
        mnuCONFGEN(4).caption = Adores(0).Value 'OfeEst
        Adores.MoveNext
        
        mnuCONFGEN(5).caption = Adores(0).Value 'Firmas
        Adores.MoveNext
        
        mnuCONFGEN(6).caption = Adores(0).Value '60 Asist
        Adores.MoveNext
             
        mnuParametros(2).caption = Adores(0).Value
        Adores.MoveNext
           
        mnuSEGUR.caption = Adores(0).Value
        Adores.MoveNext
              
        mnuSeguridad(0).caption = Adores(0).Value 'Usuarios
        Adores.MoveNext
              
        mnuSeguridad(1).caption = Adores(0).Value 'Perfiles
        Adores.MoveNext
              
        mnuSeguridad(3).caption = Adores(0).Value 'Registro Actividad
        Adores.MoveNext
        Adores.MoveNext
      
        mnuHISWEB.caption = Adores(0).Value
        Adores.MoveNext
             
        mnuSeguridad(4).caption = Adores(0).Value 'Cambio contrase�a
        Adores.MoveNext
              
        mnuSeguridad(5).caption = Adores(0).Value 'Cambio contrase�a server
        Adores.MoveNext
          
        mnuVer.caption = Adores(0).Value '70
        Adores.MoveNext
             
        mnuProceEst.caption = Adores(0).Value
        Adores.MoveNext
           
        mnuVentana.caption = Adores(0).Value
        Adores.MoveNext
           
        mnuAyuda.caption = Adores(0).Value
        Adores.MoveNext
             
        mnuAcercade(2).caption = Adores(0).Value
        Adores.MoveNext
        'A�adir
        mnuPopUpEstrOrgNiv0(1).caption = Adores(0).Value & " " & gParametrosGenerales.gsDEN_UON1
        mnuPopUpEstrOrgNiv1(1).caption = Adores(0).Value & " " & gParametrosGenerales.gsDEN_UON2
        mnuPopUpEstrOrgNiv2(1).caption = Adores(0).Value & " " & gParametrosGenerales.gsDEN_UON3
        
        mnuPopUpUsuAnya.caption = Adores(0).Value
        mnuPopUpEstrOrgNiv2(1).caption = Adores(0).Value & " " & gParametrosGenerales.gsDEN_UON3
        mnuPopUpEstrPresProyN0(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN1(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN2(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN3(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresConN0(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN1(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN2(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN3(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuEstrPresConcep3N0(1).caption = Adores(0).Value
        mnuEstrPresConcep3N1(1).caption = Adores(0).Value
        mnuEstrPresConcep4N0(1).caption = Adores(0).Value
        mnuEstrPresConcep4N1(1).caption = Adores(0).Value
        mnuPopUpEstrMatAnyaGMN1.caption = Adores(0).Value '& " " & gParametrosGenerales.gsDEN_GMN1
        mnuPopUpEstrMatAnyaGMN2.caption = Adores(0).Value
        mnuPopUpEstrMatAnyaGMN3.caption = Adores(0).Value
        mnuPopUpEstrMatAnyaGMN4.caption = Adores(0).Value
        mnuPopUpOfeMenu(3).caption = Adores(0).Value
        mnuPopUpGrupos(1).caption = Adores(0).Value
        mnuPopUpItemsModif(16).caption = Adores(0).Value
        
        mnuEstrPres5(1).caption = Adores(0).Value ' 80 A�adir
        
        Adores.MoveNext
        
        mnuPopUpEstrCompAnyaComp.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuPopUpEstrOrgNiv0(2).caption = Adores(0).Value 'A�adir Departamento
        mnuPopUpEstrOrgNiv1(2).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv2(2).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3(2).caption = Adores(0).Value
                
        Adores.MoveNext
        
        mnuPopUpEstrCompAnyaEqp.caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpEstrOrgDepart(1).caption = Adores(0).Value
                
        Adores.MoveNext
        mnuVisorApertura.caption = Adores(0).Value
        Adores.MoveNext
        'Cambiar c�digo
        mnuPopUpEstrOrgNiv1(9).caption = Adores(0).Value '80
        mnuPopUpEstrOrgNiv2(9).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3(9).caption = Adores(0).Value

        mnuPopUpEstrOrgDepart(5).caption = Adores(0).Value
                
        mnuPopUpEstrOrgCamCodPer.caption = Adores(0).Value
        mnuPopUpEstrCompEqpCamCod.caption = Adores(0).Value
        mnuPopUpEstrPresProyN1(8).caption = Adores(0).Value
        mnuPopUpEstrPresConN1(8).caption = Adores(0).Value
        mnuPopUpEstrPresProyN2(8).caption = Adores(0).Value
        mnuPopUpEstrPresConN2(8).caption = Adores(0).Value
        mnuPopUpEstrPresProyN3(8).caption = Adores(0).Value
        mnuPopUpEstrPresConN3(8).caption = Adores(0).Value
        mnuPopUpEstrPresProyN4(6).caption = Adores(0).Value
        mnuPopUpEstrPresConN4(6).caption = Adores(0).Value
        mnuEstrPresConcep3N1(8).caption = Adores(0).Value
        mnuEstrPresConcep4N1(8).caption = Adores(0).Value
        mnuEstrPresConcep3N4(6).caption = Adores(0).Value
        mnuEstrPresConcep4N4(6).caption = Adores(0).Value
        mnuPopUpEstrMatNiv1CamCod.caption = Adores(0).Value
        mnuPopUpEstrMatNiv2CamCod.caption = Adores(0).Value
        mnuPopUpEstrMatNiv3CamCod.caption = Adores(0).Value
        mnuPopUpGMN4EstrMat(1).caption = Adores(0).Value 'CamCod
        mnuPopUpGrupo(2).caption = Adores(0).Value
        Adores.MoveNext
        
        
        mnuVisorComparativa.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuVisorComuniProve.caption = Adores(0).Value
        Adores.MoveNext
        
        'Copiar a otro a�o
        mnuPopUpEstrPresConN0(2).caption = Adores(0).Value
        mnuPopUpEstrPresConN1(11).caption = Adores(0).Value
        mnuPopUpEstrPresConN2(11).caption = Adores(0).Value
        mnuPopUpEstrPresConN3(11).caption = Adores(0).Value
        mnuPopUpEstrPresConN4(9).caption = Adores(0).Value
        mnuPopUpEstrPresProyN0(2).caption = Adores(0).Value
        mnuPopUpEstrPresProyN1(11).caption = Adores(0).Value
        mnuPopUpEstrPresProyN2(11).caption = Adores(0).Value
        mnuPopUpEstrPresProyN3(11).caption = Adores(0).Value
        mnuPopUpEstrPresProyN4(9).caption = Adores(0).Value
        
        Adores.MoveNext
        Adores.MoveNext
        'Detalle
        mnuPopUpSolicitudes(0).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv1(11).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv2(11).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3(11).caption = Adores(0).Value
        mnuPopUpEstrOrgDepart(7).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv4(8).caption = Adores(0).Value

        mnuPopUpEstrOrgDetPer.caption = Adores(0).Value
        mnuPopUpUsu(3).caption = Adores(0).Value
        mnuPopUpEstrCompDetEqp.caption = Adores(0).Value
        mnuPopUpEstrCompDetCom.caption = Adores(0).Value
        mnuPopUpEstrPresConN1(9).caption = Adores(0).Value
        mnuPopUpEstrPresConN2(9).caption = Adores(0).Value
        mnuPopUpEstrPresConN3(9).caption = Adores(0).Value
        mnuPopUpEstrPresConN4(7).caption = Adores(0).Value
        mnuPopUpEstrPresProyN1(9).caption = Adores(0).Value
        mnuPopUpEstrPresProyN2(9).caption = Adores(0).Value
        mnuPopUpEstrPresProyN3(9).caption = Adores(0).Value
        mnuPopUpEstrPresProyN4(7).caption = Adores(0).Value
        
        mnuEstrPresConcep3N1(9).caption = Adores(0).Value
        mnuEstrPresConcep3N4(7).caption = Adores(0).Value
        mnuEstrPresConcep4N1(9).caption = Adores(0).Value
        mnuEstrPresConcep4N4(7).caption = Adores(0).Value
        
        mnuPopUpEstrMatDetGMN1.caption = Adores(0).Value
        mnuPopUpEstrMatDetGMN2.caption = Adores(0).Value
        mnuPopUpEstrMatDetGMN3.caption = Adores(0).Value
        mnuPopUpGMN4EstrMat(3).caption = Adores(0).Value 'Det
        Me.mnuPopUpOfeMenu(1).caption = Adores(0).Value
        mnuPopUpPlantillas(3).caption = Adores(0).Value
        
        mnuEstrPres5(8).caption = Adores(0).Value
        
        Adores.MoveNext
        mnuPopUpEstrOrgNiv1(6).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv2(6).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3(6).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv4(4).caption = Adores(0).Value

        mnuPopUpEstrOrgDepart(4).caption = Adores(0).Value
        mnuPopUpUsu(2).caption = Adores(0).Value
        mnuPopUpEstrOrgEliPer.caption = Adores(0).Value
        mnuPopUpEstrPresProyN1(4).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN2(4).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN3(4).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN4(2).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresConN1(4).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN2(4).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN3(4).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN4(2).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuEstrPresConcep3N1(4).caption = Adores(0).Value
        mnuEstrPresConcep3N4(2).caption = Adores(0).Value
        mnuEstrPresConcep4N1(4).caption = Adores(0).Value
        mnuEstrPresConcep4N4(2).caption = Adores(0).Value
        mnuPopUpEstrMatEliGMN1.caption = Adores(0).Value
        mnuPopUpEstrMatEliGMN2.caption = Adores(0).Value
        mnuPopUpGMN4EstrMat(0).caption = Adores(0).Value 'Eli
        mnuPopUpEstrMatEliGMN3.caption = Adores(0).Value
                
        mnuPopUpOfeMenu(2).caption = Adores(0).Value
        mnuPopUpCatCatalog(2).caption = Adores(0).Value
        mnuPopUpGrupo(1).caption = Adores(0).Value
        mnuPopUpItemsModif(18).caption = Adores(0).Value
        mnuPopUpPlantillas(2).caption = Adores(0).Value
        
        mnuPopUpSolicitudes(9).caption = Adores(0).Value
        mnuEstrPres5(4).caption = Adores(0).Value   'Eliminar
        
        Adores.MoveNext
        
        
        mnuPOpUpEstrCompEliCom.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuPopUpEstrCompEliEqp.caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpEstrOrgNiv1(5).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv2(5).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3(5).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv4(3).caption = Adores(0).Value
        mnuPopUpEstrOrgDepart(3).caption = Adores(0).Value
        mnuPopUpEstrOrgModPer.caption = Adores(0).Value
        mnuPopUpUsu(1).caption = Adores(0).Value
        mnuPopUpEstrPresProyN1(3).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN2(3).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN3(3).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresProyN4(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres1
        mnuPopUpEstrPresConN1(3).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN2(3).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN3(3).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuPopUpEstrPresConN4(1).caption = Adores(0).Value '& " " & gParametrosGenerales.gsSingPres2
        mnuEstrPresConcep3N1(3).caption = Adores(0).Value
        mnuEstrPresConcep3N4(1).caption = Adores(0).Value
        mnuEstrPresConcep4N1(3).caption = Adores(0).Value
        mnuEstrPresConcep4N4(1).caption = Adores(0).Value
        mnuPopUpCatCatalog(1).caption = Adores(0).Value
        mnuPopUpGuardarPlant(2).caption = Adores(0).Value
        mnuPopUpGrupo(0).caption = Adores(0).Value
        mnuPopUpPlantillas(1).caption = Adores(0).Value
        mnuPopUpSolicitudes(2).caption = Adores(0).Value
        mnuEstrPres5(3).caption = Adores(0).Value
        Adores.MoveNext
        
        
        mnuPopUpEstrCompModCom.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuPopUpEstrCompModEqp.caption = Adores(0).Value
        Adores.MoveNext
        'Ordenar por c�digo
        Me.mnuPopUpEstrOrgNiv0(4).caption = Adores(0).Value
        mnuPopUpEstrCompOrdPorCod.caption = Adores(0).Value
        mnuPopUpEstrPresConN0(5).caption = Adores(0).Value
        mnuPopUpEstrPresProyN0(5).caption = Adores(0).Value
        mnuPopUpEstrMatOrdPorCod.caption = Adores(0).Value
        mnuEstrPresConcep3N0(4).caption = Adores(0).Value
        mnuEstrPresConcep4N0(4).caption = Adores(0).Value
        
        Adores.MoveNext
        'Ordenar por denominaci�n
        Me.mnuPopUpEstrOrgNiv0(5).caption = Adores(0).Value
        mnuPopUpEstrCompOrdPorDen.caption = Adores(0).Value
        mnuPopUpEstrPresConN0(6).caption = Adores(0).Value
        mnuPopUpEstrPresProyN0(6).caption = Adores(0).Value
       
        mnuEstrPresConcep3N0(5).caption = Adores(0).Value
        mnuEstrPresConcep4N0(5).caption = Adores(0).Value
        
        mnuPopUpEstrMatOrdPorDen.caption = Adores(0).Value
        Adores.MoveNext

        
        mnuVisorPanel.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuVisorRecOfe.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuVisorSelProve.caption = Adores(0).Value
        Adores.MoveNext
        
        mnuVisor.caption = Adores(0).Value
        Adores.MoveNext
        
        m_sTituloVisor = Adores(0).Value
        Adores.MoveNext
        
        mnuOFER(1).caption = Adores(0).Value
        Adores.MoveNext
        
        mnuOFER(2).caption = Adores(0).Value
        Adores.MoveNext
                
        mnuPopUpOfeMenu(5).caption = Adores(0).Value
        Adores.MoveNext
        
        mnuPopUpOfeMenu(6).caption = Adores(0).Value
        Adores.MoveNext
        
        mnuInformes(5).caption = Adores(0).Value
        mnuCONFGEN(8).caption = Adores(0).Value '118-Param. gen. - Listados personalizados
        mnuSeguridad(2).caption = Adores(0).Value ''Seguridad  - Listados personalizados
        Adores.MoveNext
        
        mnuPopUpBuzOfe.caption = Adores(0).Value
        mnuPopUpIrBuzOfe.caption = Adores(0).Value
        Adores.MoveNext
        
        If InStr(1, Adores(0).Value, "PROY1") = 1 Then
            mnuInformes(2).caption = Replace(Adores(0).Value, "PROY1", gParametrosGenerales.gsPlurPres1)
        Else
            mnuInformes(2).caption = Replace(Adores(0).Value, "PROY1", LCase(gParametrosGenerales.gsPlurPres1))
        End If
        Adores.MoveNext
        If InStr(1, Adores(0).Value, "PROY2") = 1 Then
            mnuInformes(3).caption = Replace(Adores(0).Value, "PROY2", gParametrosGenerales.gsPlurPres2)
        Else
            mnuInformes(3).caption = Replace(Adores(0).Value, "PROY2", LCase(gParametrosGenerales.gsPlurPres2))
        End If
        'Men� para la comparativa
        Adores.MoveNext
        mnuPopCompar(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopCompar(4).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopCompar(5).caption = Adores(0).Value
        Adores.MoveNext
        
        
        mnuPED.caption = Adores(0).Value
        Adores.MoveNext
        mnuPEDSub(0).caption = Adores(0).Value
        Adores.MoveNext
        mnuPEDCATConf(0).caption = Adores(0).Value
        Adores.MoveNext
        mnuPEDSub(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(3).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(4).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(5).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv1(7).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv2(7).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3(7).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv4(5).caption = Adores(0).Value
        mnuPopUpEstrOrgBajaLogPer.caption = Adores(0).Value  '138 Baja l�gica
        'Baja L�gica
        mnuPopUpEstrPresProyN1(5).caption = Adores(0).Value
        mnuPopUpEstrPresProyN2(5).caption = Adores(0).Value
        mnuPopUpEstrPresProyN3(5).caption = Adores(0).Value
        mnuPopUpEstrPresProyN4(3).caption = Adores(0).Value
        mnuPopUpEstrPresConN1(5).caption = Adores(0).Value
        mnuPopUpEstrPresConN2(5).caption = Adores(0).Value
        mnuPopUpEstrPresConN3(5).caption = Adores(0).Value
        mnuPopUpEstrPresConN4(3).caption = Adores(0).Value
        mnuEstrPresConcep3N1(5).caption = Adores(0).Value
        mnuEstrPresConcep3N4(3).caption = Adores(0).Value
        mnuEstrPresConcep4N1(5).caption = Adores(0).Value
        mnuEstrPresConcep4N4(3).caption = Adores(0).Value
        mnuEstrPres5(5).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(8).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(9).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(11).caption = Adores(0).Value
        Adores.MoveNext
        mnuPEDSub(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPEDREC.caption = Adores(0).Value

        Adores.MoveNext
        'Deshacer baja l�gica
        mnuPopUpCatCatalog(6).caption = Adores(0).Value
        mnuPopUpEstrOrgDesBajaLogPer.caption = Adores(0).Value
        mnuPopUpEstrPresProyN1(6).caption = Adores(0).Value
        mnuPopUpEstrPresProyN2(6).caption = Adores(0).Value
        mnuPopUpEstrPresProyN3(6).caption = Adores(0).Value
        mnuPopUpEstrPresProyN4(4).caption = Adores(0).Value
        mnuPopUpEstrPresConN1(6).caption = Adores(0).Value
        mnuPopUpEstrPresConN2(6).caption = Adores(0).Value
        mnuPopUpEstrPresConN3(6).caption = Adores(0).Value
        mnuPopUpEstrPresConN4(4).caption = Adores(0).Value
        mnuEstrPresConcep3N1(6).caption = Adores(0).Value
        mnuEstrPresConcep3N4(4).caption = Adores(0).Value
        mnuEstrPresConcep4N1(6).caption = Adores(0).Value
        mnuEstrPresConcep4N4(4).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv1(8).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv2(8).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3(8).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv4(6).caption = Adores(0).Value
        mnuEstrPres5(6).caption = Adores(0).Value
        Adores.MoveNext
        mnuPEDCATConf(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(10).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpGMN4EstrMat(5).caption = Adores(0).Value 'Atr
        mnuEstrMat(3).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCrearProceso(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCrearProceso(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCrearProceso(3).caption = Adores(0).Value
        mnuPopUpItemsModif(17).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpGuardarPlant(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpGrupos(0).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpGrupos(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(3).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(9).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(7).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(8).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(12).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(14).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(20).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(5).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(6).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpItemsModif(4).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpAtr(0).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpAtr(1).caption = Adores(0).Value
        Adores.MoveNext
        'Copiar a otra U.O.
        mnuPopUpEstrPresProyN0(3).caption = Adores(0).Value
        mnuPopUpEstrPresProyN1(12).caption = Adores(0).Value
        mnuPopUpEstrPresProyN2(12).caption = Adores(0).Value
        mnuPopUpEstrPresProyN3(12).caption = Adores(0).Value
        mnuPopUpEstrPresProyN4(10).caption = Adores(0).Value
        mnuPopUpEstrPresConN0(3).caption = Adores(0).Value
        mnuPopUpEstrPresConN1(12).caption = Adores(0).Value
        mnuPopUpEstrPresConN2(12).caption = Adores(0).Value
        mnuPopUpEstrPresConN3(12).caption = Adores(0).Value
        mnuPopUpEstrPresConN4(10).caption = Adores(0).Value
        mnuEstrPresConcep3N0(2).caption = Adores(0).Value
        mnuEstrPresConcep3N1(11).caption = Adores(0).Value
        mnuEstrPresConcep3N4(9).caption = Adores(0).Value
        mnuEstrPresConcep4N0(2).caption = Adores(0).Value
        mnuEstrPresConcep4N1(11).caption = Adores(0).Value
        mnuEstrPresConcep4N4(9).caption = Adores(0).Value
        Adores.MoveNext
        mnuParametros(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpMostrarGrafico(0).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpMostrarGrafico(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpMostrarGrafico(2).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpMostrarGrafico(3).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpMostrarGrafico(4).caption = Adores(0).Value
        mnuPopUpMostrarGrafico(7).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpMostrarGrafico(5).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpMostrarGrafico(6).caption = Adores(0).Value
        Adores.MoveNext
        mnuCONFGEN(3).caption = Adores(0).Value 'Plantillas proce
        Adores.MoveNext
        mnuPresAnuCab.caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(13).caption = Adores(0).Value
        Adores.MoveNext
        Adores.MoveNext
        mnuPROCE(0).caption = Adores(0).Value 'Solicitudes de compra
        Adores.MoveNext
        mnuPROCE(6).caption = Adores(0).Value 'Contratos
        mnuPopCompar(6).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpSolicitudes(3).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpSolicitudes(4).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpSolicitudes(5).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpSolicitudes(6).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpSolicitudes(7).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpSolicitudes(8).caption = Adores(0).Value
        Adores.MoveNext
        mnuNuevoContrato(0).caption = Adores(0).Value
        Adores.MoveNext
        mnuNuevoContrato(1).caption = Adores(0).Value
        Adores.MoveNext

        Adores.MoveNext
        mnuPopUpPlantillas(4).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpPlantillas(5).caption = Adores(0).Value
        Adores.MoveNext
        mnuInformes(6).caption = Adores(0).Value
        
        Adores.MoveNext
        mnuCompItemComent.caption = Adores(0).Value
        
        Adores.MoveNext
        mnuPresArtPres(0).caption = Adores(0).Value   '196 Media del a�o anterior
        Adores.MoveNext
        mnuPresArtPres(1).caption = Adores(0).Value   '197 �ltima adjudicaci�n del a�o anterior
        Adores.MoveNext
        mnuPresArtMod(0).caption = Adores(0).Value  '198 Modificar presupuesto unitario
        Adores.MoveNext
        mnuPresArtMod(1).caption = Adores(0).Value  '199  Modificar cantidad
        Adores.MoveNext
        mnuPresArtMod(2).caption = Adores(0).Value  '200 Modificar objetivo unitario
        Adores.MoveNext
        mnuPresArtMod(3).caption = Adores(0).Value  '201 Modificar presupuesto total
        Adores.MoveNext
        mnuPresArtMod(4).caption = Adores(0).Value '202 Modificar objetivo total
        Adores.MoveNext
        mnuPopCompar(7).caption = Adores(0).Value  '203 Comentarios
        
        Adores.MoveNext
        mnuResponsable(0).caption = Adores(0).Value  '204 Ver detalle
        Adores.MoveNext
        mnuResponsable(1).caption = Adores(0).Value  '205 Sustituir
        Adores.MoveNext
        mnuPopCompar(3).caption = Adores(0).Value   '206 Notificaciones
        
        Adores.MoveNext
        mnuAcercade(0).caption = Adores(0).Value  '207 Abrir manual
        Adores.MoveNext
        mnuAcercade(1).caption = Adores(0).Value  '208 Descargar manual
        Adores.MoveNext
        m_sIdiTipoOrig = Adores(0).Value '209 tipo original
        Adores.MoveNext
        m_sIdiDialogAyuda = Adores(0).Value '210 Guardar archivo de ayuda
        
        Adores.MoveNext
        mnuSolicit.caption = Adores(0).Value  ' 211 Solicitudes
        Adores.MoveNext
        mnuSolicitudes(2).caption = Adores(0).Value  ' 212 Cumplimentaci�n
        Adores.MoveNext
        mnuSolicitudes(0).caption = Adores(0).Value   '213 Formularios
        Adores.MoveNext
        
        Adores.MoveNext
        mnuFormMoverGrupo.caption = Adores(0).Value '215 Mover o copiar
        
        Adores.MoveNext
        mnuCONFGEN(9).caption = Adores(0).Value '216 Tipos de solicitudes
        
        For i = 1 To 10
            Adores.MoveNext
            mnuCamposGS.Item(i).caption = Adores(0).Value
            mnuAnyaCampoGS.Item(i).caption = Adores(0).Value
        Next i
    
        Adores.MoveNext
        mnuAnyaCampo(1).caption = Adores(0).Value  '227 Nuevo
        mnuAnyaCampoExternoDemanda(1).caption = Adores(0).Value '227 Nuevo
        Adores.MoveNext
        mnuAnyaCampo(2).caption = Adores(0).Value  '228 Solicitud de compra
        mnuAnyaCampoPredef(1).caption = Adores(0).Value
        Adores.MoveNext
        mnuAnyaCampo(3).caption = Adores(0).Value  '229 Predefinido
        Adores.MoveNext
        mnuAnyaCampo(4).caption = Adores(0).Value  '230 Atributo de GS
        Adores.MoveNext
        mnuAnyaCampo(5).caption = Adores(0).Value  '231 Campo del sistema
        
        Adores.MoveNext
        mnuParametros(3).caption = Adores(0).Value '232 Calidad
        Adores.MoveNext
        mnuPARCalidad(0).caption = Adores(0).Value '233 Configuraci�n de variables
        Adores.MoveNext
        mnuPARCalidad(1).caption = Adores(0).Value '234 Calificaci�n de las puntuaciones
        Adores.MoveNext
        mnuPARCalidad(2).caption = Adores(0).Value '235 Restricciones de adjudicaci�n
        Adores.MoveNext
        Adores.MoveNext
        mnuPARCalidad(3).caption = Adores(0).Value '237 Publicaci�n de puntuaciones en el portal
        Adores.MoveNext

        'Opciones del men� Contextual copiar menu
        mnuPOPUPNewCopyForm(0).caption = Adores(0).Value
        Adores.MoveNext
        mnuPOPUPNewCopyForm(1).caption = Adores(0).Value
        Adores.MoveNext

        mnuCamposGS.Item(15).caption = Adores(0).Value
        mnuAnyaCampoGS.Item(15).caption = Adores(0).Value '240 Persona
        Adores.MoveNext
        
        mnuCamposGS.Item(16).caption = Adores(0).Value '241 N�mero de solicitud ERP
        mnuAnyaCampoGS.Item(16).caption = Adores(0).Value
        
        Adores.MoveNext
        mnuCamposGS.Item(17).caption = Adores(0).Value
        mnuAnyaCampoGS.Item(17).caption = Adores(0).Value '242 Unidad Organizativa
        Adores.MoveNext
        mnuCamposGS.Item(18).caption = Adores(0).Value
        mnuAnyaCampoGS.Item(18).caption = Adores(0).Value '243 Departamento
        Adores.MoveNext
        mnuCamposGS.Item(19).caption = Adores(0).Value
        mnuAnyaCampoGS.Item(19).caption = Adores(0).Value '244 Organizaci�n de compras
        Adores.MoveNext
        mnuCamposGS.Item(20).caption = Adores(0).Value
        mnuAnyaCampoGS.Item(20).caption = Adores(0).Value '245 Centro
        
        Adores.MoveNext
        mnuCamposGS.Item(21).caption = Adores(0).Value
        mnuAnyaCampoGS.Item(21).caption = Adores(0).Value '246 Almac�n
            
        Adores.MoveNext
        mnuPOPUPNewCopyForm(2).caption = Adores(0).Value '247 Marcar como titulo
        mnuPOPUPNewCopyForm(2).Visible = False
        Adores.MoveNext
        mnuPOPUPNewCopyForm(3).caption = Adores(0).Value '248 Desmarcar como titulo
        On Error Resume Next
        mnuPOPUPNewCopyForm(3).Visible = False
        On Error GoTo 0
        Adores.MoveNext
        mnuCamposGS.Item(22).caption = Adores(0).Value      '249 Importe solicitudes vinculadas
        mnuAnyaCampoGS.Item(22).caption = Adores(0).Value
        Adores.MoveNext
        mnuCamposGS.Item(23).caption = Adores(0).Value      '250 Referencia a solicitud
        mnuAnyaCampoGS.Item(23).caption = Adores(0).Value   '251
        
        Adores.MoveNext
        mnuPopUpEstrOrgReuPer.caption = Adores(0).Value     '252

        Adores.MoveNext                                     '
        Adores.MoveNext                                     '
        Adores.MoveNext                                     '
        Adores.MoveNext                                     '
        Adores.MoveNext                                     '
        Adores.MoveNext                                     '
        mnuAnyaCampo(6).caption = Adores(0).Value           '257 Campos externos
        
        Adores.MoveNext
        
        Adores.MoveNext
        mnuPopCompar(8).caption = Adores(0).Value           '259 Comparativa de calidad
        
        Adores.MoveNext
        mnuPopUpEstrOrgNiv3(1).caption = Adores(0).Value  '260 "A�adir Centro de Coste"

        Adores.MoveNext
        mnuVIAPAG.caption = Adores(0).Value                 '261 V�as de pago

        For i = 1 To 4
            Adores.MoveNext
            mnuPopUpFSQAVarCal(i).caption = Adores(0).Value
            mnuPopUpFSQAVarCalDesg(i).caption = Adores(0).Value
        Next i

        Adores.MoveNext
        mnuCamposGS.Item(25).caption = Adores(0).Value      '266 Centro de coste
        mnuAnyaCampoGS.Item(25).caption = Adores(0).Value   '266 Centro de coste
              
        Adores.MoveNext
        mnuCamposGS.Item(26).caption = Adores(0).Value      '267 Activo
        mnuAnyaCampoGS.Item(26).caption = Adores(0).Value

        Adores.MoveNext
        mnuPopUpSolicitudes.Item(10).caption = Adores(0).Value  '268 Activar monitorizaci�n
        Adores.MoveNext
        mnuPopUpSolicitudes.Item(11).caption = Adores(0).Value  '269 Desactivar monitorizaci�n
        
        Adores.MoveNext
        mnuPopUpEstrOrgNiv1.Item(3).caption = Adores(0).Value '270 Gestionar Centro de Coste
        mnuPopUpEstrOrgNiv2.Item(3).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv3.Item(3).caption = Adores(0).Value
        mnuPopUpEstrOrgNiv4.Item(1).caption = Adores(0).Value
        'En funci�n de si el usuario puede gestionar o no los Centro de Coste
        'Podr� Gestionar o s�lo Consultar, por lo que el nombre de la opci�n var�a
        Adores.MoveNext
        If Not oUsuarioSummit.EsAdmin Then
            If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGGestionarCC)) Is Nothing) _
                And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ORGConsultarCC)) Is Nothing) Then
                mnuPopUpEstrOrgNiv1.Item(3).caption = Adores(0).Value '271 Consultar Centro de Coste
                mnuPopUpEstrOrgNiv2.Item(3).caption = Adores(0).Value
                mnuPopUpEstrOrgNiv3.Item(3).caption = Adores(0).Value
                mnuPopUpEstrOrgNiv4.Item(1).caption = Adores(0).Value
            End If
        End If
        
        Adores.MoveNext
        mnuCamposGS.Item(24).caption = Adores(0).Value      '272 Tipo de pedido
        mnuAnyaCampoGS.Item(24).caption = Adores(0).Value   '272 Tipo de pedido
        
        Adores.MoveNext
        mnuPopCompar(9).caption = Adores(0).Value   '273 Crear contrato
        
        Adores.MoveNext
        mnuPOPUPSincroArti(1).caption = Adores(0).Value   '274 Modificar atributo del art�culo.
        Adores.MoveNext
        mnuPOPUPSincroArti(2).caption = Adores(0).Value   '275 No modificar atributo del art�culo.
        
        Adores.MoveNext
        mnuOptima(0).caption = Adores(0).Value '276 Calcular �ptima por proceso.
        Adores.MoveNext
        mnuOptima(1).caption = Adores(0).Value '277 Calcular �ptima por grupos.
        Adores.MoveNext
        mnuOptima(2).caption = Adores(0).Value '278 Calcular �ptima por item.
        Adores.MoveNext
        mnuOptima(4).caption = Adores(0).Value '279 Configurar �ptima...
        Adores.MoveNext
        mnuAnyaCampoGS.Item(27).caption = Adores(0).Value '280 "Desglose de actividad"
        mnuCamposGS.Item(27).caption = Adores(0).Value '280  "Desglose de actividad"

        Adores.MoveNext
        mnuPopCompar(10).caption = Adores(0).Value   '281 Ver contrato
        
        Adores.MoveNext
        mnuCONFGEN(10).caption = Adores(0).Value '282 Tipos de relaci�n entre proveedores
        
        Adores.MoveNext
        mnuPopUpMostrarGrafico(8).caption = Adores(0).Value
                
        Adores.MoveNext
        mnuColaboracion.caption = Adores(0).Value '284 Colaboraci�n

        Adores.MoveNext
        mnuCamposGS.Item(28).caption = Adores(0).Value      '285 Desglose de factura
        mnuAnyaCampoGS.Item(28).caption = Adores(0).Value   '285 Desglose de factura
        
        Adores.MoveNext
        mnuPopUpEstrMatImpuestoGMN1.caption = Adores(0).Value      '286 Impuestos
        mnuPopUpEstrMatImpuestoGMN2.caption = Adores(0).Value      '286 Impuestos
        mnuPopUpEstrMatImpuestoGMN3.caption = Adores(0).Value      '286 Impuestos
        mnuPopUpGMN4EstrMat(6).caption = Adores(0).Value   '286 Impuestos

        
        If (gParametrosGenerales.gsAccesoFSIM = TipoAccesoFSIM.SinAcceso) Then
            mnuPopUpEstrMatImpuestoGMN1.Visible = False
            mnuPopUpEstrMatImpuestoGMN2.Visible = False
            mnuPopUpEstrMatImpuestoGMN3.Visible = False
            mnuPopUpGMN4EstrMat(6).Visible = False
        End If
                
        Adores.MoveNext
        mnuCamposGS.Item(29).caption = Adores(0).Value      '287 Factura
        mnuAnyaCampoGS.Item(29).caption = Adores(0).Value   '287 Factura
        
        Adores.MoveNext
        mnuCamposGS.Item(30).caption = Adores(0).Value      '288 Inicio de abono
        mnuAnyaCampoGS.Item(30).caption = Adores(0).Value   '288 Inicio de abono
        
        Adores.MoveNext
        mnuCamposGS.Item(31).caption = Adores(0).Value      '289 Fin de abono
        mnuAnyaCampoGS.Item(31).caption = Adores(0).Value   '289 Fin de abono
        Adores.MoveNext
        mnuTIPOSIMP.caption = Adores(0).Value               '290 Tipos de impuestos
        Adores.MoveNext
        mnuCamposGS(32).caption = Adores(0).Value           '291 Retenci�n en garant�a
        mnuAnyaCampoGS(32).caption = Adores(0).Value
        
        Adores.MoveNext
        mnuCamposGS(33).caption = Adores(0).Value           '292 Unidad de negocio
        mnuAnyaCampoGS(33).caption = Adores(0).Value
        
        Adores.MoveNext
        mnuPOPUPNewCopyForm(4).caption = Adores(0).Value '293 Marcar como Denominaci�n proceso
        mnuPOPUPNewCopyForm(4).Visible = False
        Adores.MoveNext
        mnuPOPUPNewCopyForm(5).caption = Adores(0).Value '294 Desmarcar como Denominaci�n proceso
        mnuPOPUPNewCopyForm(5).Visible = False
        Adores.MoveNext
        mnuPopUpSolicitudes.Item(1).caption = Adores(0).Value  '295 Detalle de desglose
        Adores.MoveNext
        mnuAnyaCampoExterno(1).caption = Adores(0).Value  '296 Carga bajo demanda
        Adores.MoveNext
        mnuAnyaCampoExterno(2).caption = Adores(0).Value  '297 Carga peri�dica
        Adores.MoveNext
        mnuCamposGS(34).caption = Adores(0).Value           '298 Proveedor ERP
        mnuAnyaCampoGS(34).caption = Adores(0).Value
        Adores.MoveNext
        mnuCamposGS(35).caption = Adores(0).Value           '299 Comprador
        mnuAnyaCampoGS(35).caption = Adores(0).Value
        Adores.MoveNext
        mnuTIPOSPED.caption = Adores(0).Value 'Tipos de pedido
        Adores.MoveNext
        mnuCamposGS(36).caption = Adores(0).Value           '300 Desglose de pedido
        mnuAnyaCampoGS(36).caption = Adores(0).Value
        Adores.MoveNext
        mnuPopUpCatCatalog(14).caption = Adores(0).Value '301 Configuracion
        Adores.MoveNext
        mnuPOPUPNewCopyForm(6).caption = Adores(0).Value    'Marcar como peso
        MDI.mnuPopUpFSQAVarCal.Item(5).caption = Adores(0).Value
        Adores.MoveNext
        mnuPOPUPNewCopyForm(7).caption = Adores(0).Value    'Desmarcar como peso
        MDI.mnuPopUpFSQAVarCal.Item(6).caption = Adores(0).Value
        Adores.MoveNext
        mnuCamposGS(37).caption = Adores(0).Value           '300
        mnuAnyaCampoGS(37).caption = Adores(0).Value
        Adores.MoveNext
        mnuCamposGS(38).caption = Adores(0).Value
        mnuAnyaCampoGS(38).caption = Adores(0).Value
        Adores.Close
        Set Adores = Nothing
    End If
    
    'Tag de los campos de solicitudes predefinidos:
    For i = 1 To 38
        Select Case i
            Case 1  'Proveedor
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Proveedor
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Proveedor
            Case 2  'F.Pago
                mnuCamposGS.Item(i).Tag = TipoCampoGS.FormaPago
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.FormaPago
            Case 3  'Moneda
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Moneda
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Moneda
            Case 4  'Material
                mnuCamposGS.Item(i).Tag = TipoCampoGS.material
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.material
            Case 5  'Art�culo
                mnuCamposGS.Item(i).Tag = TipoCampoGS.NuevoCodArticulo
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.NuevoCodArticulo
            Case 6  'Unidad
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Unidad
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Unidad
            Case 7  'Desglose
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Desglose
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Desglose
            Case 8  'Pa�s
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Pais
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Pais
            Case 9  'Provincia
                mnuCamposGS.Item(i).Tag = TipoCampoGS.provincia
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.provincia
            Case 10 'Destino
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Dest
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Dest
            Case 11 'Pres1
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Pres1
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Pres1
            Case 12 'Pres2
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Pres2
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Pres2
            Case 13 'Pres3
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Pres3
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Pres3
            Case 14 'Pres4
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Pres4
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Pres4
            Case 15 'Persona
                mnuCamposGS.Item(i).Tag = TipoCampoGS.CampoPersona
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.CampoPersona
            Case 16 'Numero Solicitud ERP
                mnuCamposGS.Item(i).Tag = TipoCampoGS.NumSolicitERP
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.NumSolicitERP
            Case 17 'Unidad Organizativa
                mnuCamposGS.Item(i).Tag = TipoCampoGS.UnidadOrganizativa
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.UnidadOrganizativa
            Case 18 'Departamento
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Departamento
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Departamento
            Case 19 'Organizaci�n de compras
                mnuCamposGS.Item(i).Tag = TipoCampoGS.OrganizacionCompras
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.OrganizacionCompras
            Case 20 'Centro
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Centro
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Centro
            Case 21 'Almac�n
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Almacen
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Almacen
            Case 22 'Importe solicitudes vinculadas
                mnuCamposGS.Item(i).Tag = TipoCampoGS.ImporteSolicitudesVinculadas
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.ImporteSolicitudesVinculadas
            Case 23 'Referencia a solicitud
                mnuCamposGS.Item(i).Tag = TipoCampoGS.RefSolicitud
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.RefSolicitud
            Case 24 'Tipo de pedido
                mnuCamposGS.Item(i).Tag = TipoCampoGS.TipoPedido
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.TipoPedido
            Case 25 'Centro de coste
                mnuCamposGS.Item(i).Tag = TipoCampoGS.CentroCoste
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.CentroCoste
            Case 26 'Activo
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Activo
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Activo
            Case 27 'Desglose de actividad
                mnuCamposGS.Item(i).Tag = TipoCampoGS.DesgloseActividad
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.DesgloseActividad
            Case 28 'Desglose de factura
                mnuCamposGS.Item(i).Tag = TipoCampoGS.DesgloseFactura
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.DesgloseFactura
            Case 29 'Factura
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Factura
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Factura
            Case 30 'Fecha de inicio per�odo contable abono
                mnuCamposGS.Item(i).Tag = TipoCampoGS.InicioAbono
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.InicioAbono
            Case 31 'Fecha de fin per�odo contable abono
                mnuCamposGS.Item(i).Tag = TipoCampoGS.FinAbono
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.FinAbono
            Case 32 'Retenci�n en garant�a
                mnuCamposGS.Item(i).Tag = TipoCampoGS.RetencionEnGarantia
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.RetencionEnGarantia
            Case 33 'Unidad de pedido
                mnuCamposGS.Item(i).Tag = TipoCampoGS.UnidadPedido
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.UnidadPedido
            Case 34 'Proveedor ERP
                mnuCamposGS.Item(i).Tag = TipoCampoGS.ProveedorERP
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.ProveedorERP
            Case 35 'Comprador
                mnuCamposGS.Item(i).Tag = TipoCampoGS.CodComprador
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.CodComprador
            Case 36 'Desglose de pedido
                mnuCamposGS.Item(i).Tag = TipoCampoGS.DesgloseDePedido
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.DesgloseDePedido
            Case 37 'Empresa
                mnuCamposGS.Item(i).Tag = TipoCampoGS.Empresa
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.Empresa
            Case 38 'A�o de Imputaci�n
                mnuCamposGS.Item(i).Tag = TipoCampoGS.AnyoImputacion
                mnuAnyaCampoGS.Item(i).Tag = TipoCampoGS.AnyoImputacion
        End Select
    Next i
    
End Sub

Private Sub MDIForm_Load()

    Screen.MousePointer = vbHourglass
    
    g_bMDICerrar = False
        
    If FSEPConf Then
        InicializarMenuConPermisosFSEP
    Else
        InicializarMenuConPermisos
    End If
    
    If basParametros.gParametrosGenerales.gsAccesoFSCN = TipoAccesoFSCN.SinAcceso Then
        mnuColaboracion.Visible = False 'Colaboracion
    End If
    
    ConfigurarNombresEnMenu
    
    On Error GoTo imposible
    
    If Not IsNull(gParametrosInstalacion.Fondo) And gParametrosInstalacion.Fondo <> "" Then
        Set Picture = LoadPicture(gParametrosInstalacion.Fondo)
    End If
    
    Me.Show
    
    If Not FSEPConf And gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.AccesoFSGS Then
        If oUsuarioSummit.EsAdmin Then
            Load frmEST
            frmEST.SetFocus
        Else
            If Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceConsulta)) Is Nothing Then
                Load frmEST
                frmEST.SetFocus
            Else
                If oUsuarioSummit.EsInvitado Then
                    Load frmEST
                    frmEST.SetFocus
                End If
            End If
        End If
    End If
    
    Set g_dicBloqueos = New Dictionary 'Creamos la variable dictionary para los bloqueos

#If VERSION >= 31200 Then
    mnuPARCalidad(3).Visible = True
#Else
    mnuPARCalidad(3).Visible = False
#End If
    
    Screen.MousePointer = vbNormal
    
    Exit Sub
    
imposible:

    oGestorParametros.GuardarFondoInstalacion "", oUsuarioSummit.Cod
    Screen.MousePointer = vbNormal
    
    Exit Sub
    
End Sub
Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
   g_bMDICerrar = True
   
End Sub
Private Sub MDIForm_Unload(Cancel As Integer)
    
   Dim teserror As TipoErrorSummit
   
    If gParametrosInstalacion.gbMensajeCierre = True Then
        If Me.WindowState = 1 Then
            Me.WindowState = 2
        End If
        If oMensajes.AvisoCerrarGS = False Then
            If FSEPConf Then
                Cancel = True
                g_bMDICerrar = False
                Exit Sub
            Else
                If oUsuarioSummit.Tipo <> Administrador Then
                    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ESTProceConsulta)) Is Nothing Then
                        Cancel = True
                        g_bMDICerrar = False
                        Exit Sub
                    End If
                End If
            End If
            frmEST.lblSaludo.caption = m_sTituloVisor
            MostrarFormulario frmEST
            Cancel = True
            g_bMDICerrar = False
            Exit Sub
        End If
    End If
    
    If g_GuardarParametrosIns Then
        oGestorParametros.GuardarParametrosInstalacion gParametrosInstalacion, oUsuarioSummit.Cod
        g_GuardarParametrosIns = False
    End If
    
    
    'Si se est� mostrando el manual de ayuda lo cerramos:
    CerrarManualAyuda
    
    teserror = oSesionSummit.DesRegistrarSesion
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
    End If
    
    Set oSesionSummit = Nothing
      
    Set g_dicBloqueos = Nothing
    Set g_FormulariosInternet = Nothing
    
    FinDeSesion
    
End Sub

Private Sub mnuAcercade_Click(Index As Integer)
Dim sFileName As String
Dim oFos As Scripting.FileSystemObject
Dim sFileHelp As String
Dim sPath As String

    Select Case Index
        Case 0  'Abrir manual de la ayuda
            Set oFos = New Scripting.FileSystemObject
            If oFos.FileExists(gParametrosGenerales.gsHelpPDF) = True Then
                'Reemplaza el manual de ayuda por el del idioma de la aplicaci�n
                sFileHelp = oFos.GetFileName(gParametrosGenerales.gsHelpPDF)
                If sFileHelp <> "" Then
                    sFileHelp = basPublic.gParametrosInstalacion.gIdioma & "_" & Mid(sFileHelp, 5)
                End If
                sPath = oFos.GetParentFolderName(gParametrosGenerales.gsHelpPDF)
                If oFos.FileExists(oFos.BuildPath(sPath, sFileHelp)) = True Then
                    glPid = Shell(gParametrosGenerales.gsReader & " " & oFos.BuildPath(sPath, sFileHelp), vbNormalFocus)
                    If glPid = 0 Then
                        oMensajes.ImposibleMostrarAyuda (False)
                    End If
                Else
                    oMensajes.ImposibleMostrarAyuda (True)
                End If
            Else
                oMensajes.ImposibleMostrarAyuda (True)
            End If
            Set oFos = Nothing
            
        Case 1  'Descargar manual de la ayuda
            Set oFos = New Scripting.FileSystemObject
            If oFos.FileExists(gParametrosGenerales.gsHelpPDF) = True Then
                'Reemplaza el manual de ayuda por el del idioma de la aplicaci�n
                sFileHelp = oFos.GetFileName(gParametrosGenerales.gsHelpPDF)
                If sFileHelp <> "" Then
                    sFileHelp = basPublic.gParametrosInstalacion.gIdioma & "_" & Mid(sFileHelp, 5)
                End If
                sPath = oFos.GetParentFolderName(gParametrosGenerales.gsHelpPDF)
                
                If oFos.FileExists(oFos.BuildPath(sPath, sFileHelp)) = True Then
                    cmmdAyuda.DialogTitle = m_sIdiDialogAyuda
                    cmmdAyuda.CancelError = False
                    cmmdAyuda.Filter = m_sIdiTipoOrig & "|*.*"
                    cmmdAyuda.FilterIndex = 0
                    cmmdAyuda.filename = sFileHelp
                    cmmdAyuda.ShowSave
                    
                    sFileName = cmmdAyuda.filename
                    If sFileName = "" Then
                        Set oFos = Nothing
                        Exit Sub
                    End If
                    
                    'Copia el manual de ayuda en el path indicado:
                    oFos.CopyFile oFos.BuildPath(sPath, sFileHelp), sFileName, True
                
                Else
                    oMensajes.ImposibleDescargarAyuda
                End If
            Else
                oMensajes.ImposibleDescargarAyuda
            End If
            Set oFos = Nothing
            

        Case 2  'Acerca de
            Screen.MousePointer = vbHourglass
            MostrarFormAbout oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, App.Comments, MDI.ScaleHeight, MDI.ScaleWidth
    End Select
End Sub

Private Sub mnuAnyaCampo_Click(Index As Integer)
    'a�ade un nuevo campo a un formulario,del tipo seleccionado
    Select Case Index
    
        Case 1  'Nuevo
            If MDI.ActiveForm.Name = "frmFormularios" Then
                frmFormularios.AnyadirCampoNuevo
            Else
                frmFormularios.g_ofrmDesglose.AnyadirCampoNuevo
            End If

        Case 2  'Solicitud de compra
            If MDI.ActiveForm.Name = "frmFormularios" Then
                frmFormularios.AnyadirCampoPredefinido TipoSolicitud.SolicitudCompras
            Else
                frmFormularios.g_ofrmDesglose.AnyadirCampoPredefinido TipoSolicitud.SolicitudCompras
            End If
            
        Case 4  'Atributo de GS
            If MDI.ActiveForm.Name = "frmFormularios" Then
                frmFormularios.AnyadirCampoAtributoGS
            Else
                frmFormularios.g_ofrmDesglose.AnyadirCampoAtributoGS
            End If
            
    End Select
End Sub

Private Sub mnuAnyaCampoExternoPeriodico_Click(Index As Integer)
    Dim idTabla As Integer
    
    idTabla = mnuAnyaCampoExternoPeriodico(Index).Tag

    If MDI.ActiveForm.Name = "frmFormularios" Then
        frmFormularios.AnyadirCampoExterno idTabla
    Else
        frmFormularios.g_ofrmDesglose.AnyadirCampoExterno idTabla
    End If
End Sub

Private Sub mnuAnyaCampoExternoDemanda_Click(Index As Integer)
    
    Screen.MousePointer = vbHourglass
    frmServicio.lIdFormulario = frmFormularios.g_oFormSeleccionado.Id
    frmServicio.lGrupoFormulario = frmFormularios.g_oGrupoSeleccionado.Id
    frmServicio.g_sOrigen = MDI.ActiveForm.Name
    frmServicio.g_multiidioma = frmFormularios.g_oFormSeleccionado.Multiidioma
    Set frmServicio.g_oIdiomas = frmFormularios.m_oIdiomas
    frmServicio.lCampoForm = 0
    frmServicio.g_Instancia = 0
    
    Select Case Index
        Case 1 'Carga bajo demanda/Nuevo
            frmServicio.iServAntiguo = 0
        Case Else 'Carga bajo demanda/Servicios
            Dim idServicio As Integer
            idServicio = mnuAnyaCampoExternoDemanda(Index).Tag
            frmServicio.iServAntiguo = idServicio
    End Select
    frmServicio.Show vbModal
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuAnyaCampoGS_Click(Index As Integer)
    Dim iTipoCampoGS As Integer
    Dim sPRES5 As String
    
    If InStr(1, mnuAnyaCampoGS(Index).Tag, "#") > 0 Then
        iTipoCampoGS = Split(mnuAnyaCampoGS(Index).Tag, "#")(0)
        sPRES5 = Split(mnuAnyaCampoGS(Index).Tag, "#")(1)
    Else
        iTipoCampoGS = mnuAnyaCampoGS(Index).Tag
    End If
        
    If MDI.ActiveForm.Name = "frmFormularios" Then
        If iTipoCampoGS = TipoCampoGS.PartidaPresupuestaria Then
            frmFormularios.AnyadirCampoGS iTipoCampoGS, sPRES5
        Else
            frmFormularios.AnyadirCampoGS iTipoCampoGS
        End If
    Else
        'Le llama desde el desglose
        If iTipoCampoGS = TipoCampoGS.PartidaPresupuestaria Then
            frmFormularios.g_ofrmDesglose.AnyadirCampoGS iTipoCampoGS, sPRES5
        Else
            frmFormularios.g_ofrmDesglose.AnyadirCampoGS iTipoCampoGS
        End If
    End If
    
End Sub

Private Sub mnuAnyaCampoPredef_Click(Index As Integer)

    'A�ade a un formulario un campo predefinido del tipo seleccionado en el men�:
    If MDI.ActiveForm.Name = "frmFormularios" Then
        frmFormularios.AnyadirCampoPredefinido mnuAnyaCampoPredef(Index).Tag
    Else
        frmFormularios.g_ofrmDesglose.AnyadirCampoPredefinido mnuAnyaCampoPredef(Index).Tag
    End If
      
End Sub

Private Sub mnuAtribInterno_Click(Index As Integer)
Select Case Index
Case 0
    g_ofrmOrigenResponsable.MarcarDesmarcarAtribInterno 1
Case 1
    g_ofrmOrigenResponsable.MarcarDesmarcarAtribInterno 0
End Select
End Sub

Private Sub mnuCamposGS_Click(Index As Integer)
    Dim iTipoCampoGS As Integer
    Dim sPRES5 As String
    
    If InStr(1, mnuCamposGS(Index).Tag, "#") > 0 Then
        iTipoCampoGS = Split(mnuCamposGS(Index).Tag, "#")(0)
        sPRES5 = Split(mnuCamposGS(Index).Tag, "#")(1)
    Else
        iTipoCampoGS = mnuCamposGS(Index).Tag
    End If
        
    If MDI.ActiveForm.Name = "frmPARTipoSolicit" Then
        If iTipoCampoGS = TipoCampoGS.PartidaPresupuestaria Then
            frmPARTipoSolicit.AnyadirCampoDeGS iTipoCampoGS, sPRES5
        Else
            frmPARTipoSolicit.AnyadirCampoDeGS iTipoCampoGS
        End If
    Else
        If iTipoCampoGS = TipoCampoGS.PartidaPresupuestaria Then
            frmPARTipoSolicit.g_ofrmDesglose.AnyadirCampoDeGS iTipoCampoGS, sPRES5
        Else
            frmPARTipoSolicit.g_ofrmDesglose.AnyadirCampoDeGS iTipoCampoGS
        End If
    End If
End Sub

Private Sub mnuColaboracion_Click()
    MostrarPaginaWeb Me, mnuColaboracion.Name, gParametrosGenerales.gsURLCN & "cn_MuroUsuario.aspx"
End Sub

Private Sub mnuCompItemComent_Click()
    'Muestra el formulario de los adjuntos a nivel de item
    Dim sForm As String
    
    sForm = mnuPopUpCompItem.Tag
    If sForm = "frmADJ" Then
        frmADJ.VisualizarFicherosAdjuntosItem
    Else
        frmRESREU.VisualizarFicherosAdjuntosItem
    End If
End Sub

Private Sub mnuConcepto3_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmPresupuestos3
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuConcepto4_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmPresupuestos4
    Screen.MousePointer = vbNormal
End Sub

''' <summary>Click en el men� de configuraci�n general</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 07/09/2011</revision>
Private Sub mnuCONFGEN_Click(Index As Integer)

    Screen.MousePointer = vbHourglass
    Select Case Index
    Case 0
        MostrarFormulario frmCONFGEN
    Case 1
        MostrarFormulario frmPARRoles
    Case 2
        MostrarFormulario frmPARHom
    Case 3
        MostrarFormulario frmPlantillasProce
    Case 4
        MostrarFormulario frmPAROfeEst
    Case 5
        MostrarFormulario frmPARFirmas
    Case 6
        MostrarFormulario frmPARAsis
    Case 8
        MostrarFormulario frmLISPERMant
    Case 9
        MostrarFormulario frmPARTipoSolicit
    Case 10
        MostrarFormulario frmPARTipoRelac
    End Select

    Screen.MousePointer = vbNormal
End Sub


Private Sub mnuESTRCOMP_Click(Index As Integer)
    Select Case Index
        Case 1
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmESTRCOMP
            Screen.MousePointer = vbNormal
        Case 2
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmESTRCOMPMatPorCom
            Screen.MousePointer = vbNormal
        Case 3
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmESTRCOMPComPorMat
            Screen.MousePointer = vbNormal
    End Select
End Sub

Private Sub mnuEstrMat_Click(Index As Integer)
    Select Case Index
        Case 1
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmESTRMAT
            Screen.MousePointer = vbNormal
        Case 2
            AbrirMntoWPF Unidades
        Case 3
            Screen.MousePointer = vbHourglass
            frmAtrib.g_sOrigen = "MDI"
            frmAtrib.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeSingleSelect
            MostrarFormulario frmAtrib
            Screen.MousePointer = vbNormal
    End Select
End Sub

Private Sub mnuESTRORGCab_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmESTRORG
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuFormMoverGrupo_Click()
    frmFormularios.CambiarOrdenGrupos

End Sub

Private Sub mnuHISWEB_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmOFEHistWeb
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfApl_Click(Index As Integer)
        Select Case Index
            Case 1
                Screen.MousePointer = vbHourglass
                MostrarFormulario frmInfAhorroApliGen
                Screen.MousePointer = vbNormal
            Case 2
                Screen.MousePointer = vbHourglass
                MostrarFormulario frmInfAhorroApliMat
                Screen.MousePointer = vbNormal
            Case 3
                Screen.MousePointer = vbHourglass
                MostrarFormulario frmInfAhorroApliProy
                Screen.MousePointer = vbNormal
            Case 4
                Screen.MousePointer = vbHourglass
                MostrarFormulario frmInfAhorroApliParCon
                Screen.MousePointer = vbNormal
            Case 5
                Screen.MousePointer = vbHourglass
                MostrarFormulario frmInfAhorroApliConcep3
                Screen.MousePointer = vbNormal
            Case 6
                Screen.MousePointer = vbHourglass
                MostrarFormulario frmInfAhorroApliConcep4
                Screen.MousePointer = vbNormal
            Case 7
                Screen.MousePointer = vbHourglass
                MostrarFormulario frmInfAhorroApliUO
                Screen.MousePointer = vbNormal
        End Select
End Sub

Private Sub mnuInfConcep3Det_Click(Index As Integer)
    Screen.MousePointer = vbHourglass
    If Index = 1 Then
        MostrarFormulario frmInfAhorroNegConcep3Reu
    Else
        MostrarFormulario frmInfAhorroNegConcep3Desde
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfConcep4Det_Click(Index As Integer)
    Screen.MousePointer = vbHourglass
    If Index = 1 Then
        MostrarFormulario frmInfAhorroNegConcep4Reu
    Else
        MostrarFormulario frmInfAhorroNegConcep4Desde
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfEqpNegDesde_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmInfAhorroNegEqpDesde
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfEqpNegReu_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmInfAhorroNegEqpReu
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfEqpResDesde_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmInfAhorroNegEqpResDesde
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfEqpResReu_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmInfAhorroNegEqpResReu
    Screen.MousePointer = vbNormal
End Sub


Private Sub mnuInfGenDesde_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmInfAhorroNegDesde
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfGenReu_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmINFAhorroNegReu
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfMatDesde_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmInfAhorroNegMatDesde
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInfMatReu_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmInfAhorroNegMatReu
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuInformes_Click(Index As Integer)

    Select Case Index
        Case 2
            Screen.MousePointer = vbHourglass
            Set g_ofrmInfEvolPres1 = New frmInfEvolPres
            g_ofrmInfEvolPres1.iConcepto = 1
            MostrarFormulario g_ofrmInfEvolPres1
            Screen.MousePointer = vbNormal
        
        Case 3
            Screen.MousePointer = vbHourglass
            Set g_ofrmInfEvolPres2 = New frmInfEvolPres
            g_ofrmInfEvolPres2.iConcepto = 2
            MostrarFormulario g_ofrmInfEvolPres2
            Screen.MousePointer = vbNormal
        
        Case 4
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmListados
            Screen.MousePointer = vbNormal
        
        Case 5
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmLISPER
            Screen.MousePointer = vbNormal
        
        Case 6
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmActualizarAhorros
            Screen.MousePointer = vbNormal
    
    End Select

End Sub


Private Sub mnuMON_Click()
    AbrirMntoWPF Monedas
End Sub


Private Sub mnuOFER_Click(Index As Integer)
    Select Case Index
        Case 1
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmOFEBuzon
            Screen.MousePointer = vbNormal
        Case 2
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmOFERec
            Screen.MousePointer = vbNormal
    End Select
End Sub

Private Sub mnuOptima_Click(Index As Integer)
    Select Case Index
        Case 0
            'Calcular �ptima por proceso
            Screen.MousePointer = vbHourglass
            g_ofrmOrigenResponsable.CalcularOptima TipoOptimaProceso.OptimaProceso
            Screen.MousePointer = vbNormal
        Case 1
            'Calcular �ptima por grupos
            Screen.MousePointer = vbHourglass
            g_ofrmOrigenResponsable.CalcularOptima TipoOptimaProceso.OptimaGrupo
            Screen.MousePointer = vbNormal
        Case 2
            'Calcular �ptima por item
            Screen.MousePointer = vbHourglass
            g_ofrmOrigenResponsable.CalcularOptima TipoOptimaProceso.OptimaItem
            Screen.MousePointer = vbNormal
        Case 4
            'Configurar �ptima...
            Screen.MousePointer = vbHourglass
            g_ofrmOrigenResponsable.ConfigurarOptima
            Screen.MousePointer = vbNormal
        
    End Select
    Set g_ofrmOrigenResponsable = Nothing
End Sub

Private Sub mnuPAG_Click()
    AbrirMntoWPF FormasPago
End Sub

Private Sub mnuPEDREC_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmRecepGral
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Carga la pantalla para asociar impuestos al material
''' </summary>
''' <remarks>Llamada desde: frmESTRMAT ; Tiempo m�ximo: 0</remarks>
Private Sub mnuPopUpEstrMatImpuestoGMN1_Click()
    frmESTRMAT.Impuesto
End Sub
''' <summary>
''' Carga la pantalla para asociar impuestos al material
''' </summary>
''' <remarks>Llamada desde: frmESTRMAT ; Tiempo m�ximo: 0</remarks>
Private Sub mnuPopUpEstrMatImpuestoGMN2_Click()
    frmESTRMAT.Impuesto
End Sub
''' <summary>
''' Carga la pantalla para asociar impuestos al material
''' </summary>
''' <remarks>Llamada desde: frmESTRMAT ; Tiempo m�ximo: 0</remarks>
Private Sub mnuPopUpEstrMatImpuestoGMN3_Click()
    frmESTRMAT.Impuesto
End Sub
''' <summary>
''' En la pantalla de estructura de materiales al dar boton derecho sale este menu mnuPopUpGMN4EstrMat.
''' Aqui se realiza la funcion correspondiente a cada entrada del menu
''' </summary>
''' <param name="Index">Q boton se pulso</param>
''' <remarks>Llamada desde: frmESTRMAT ; Tiempo m�ximo: 0</remarks>
Private Sub mnuPopUpGMN4EstrMat_Click(Index As Integer)
Select Case Index
Case 0 'eli
    frmESTRMAT.cmdEli_Click
Case 1 'cambio
    frmESTRMAT.CambiarCodigo
Case 3 'detalle
    frmESTRMAT.mnuPopUpEstrMatDet
Case 5 'Atrib
    frmESTRMAT.mnuPopUpEstrMatAtrGMN1
Case 6 'impuesto
    frmESTRMAT.Impuesto
End Select
End Sub

Private Sub mnuTIPOSIMP_Click()
    AbrirMntoWPF TiposImpuestos
End Sub

Private Sub mnuVIAPAG_Click()
    AbrirMntoWPF ViasPago
End Sub

Private Sub mnuPAI_Click()
    AbrirMntoWPF Paises
End Sub

''' <summary>Abre un Mnto. WPF</summary>
''' <param name="TipoMnto">Mnto. a abrir</param>
''' <remarks>Llamada desde: mnuPAI_Click, mnuMON_Click, mnuEstrMat_Click, mnuPAG_Click, mnuVIAPAG_Click, mnuTIPOSIMP_Click</remarks>

Private Sub AbrirMntoWPF(ByVal TipoMnto As TipoMntoWPF)
    Dim oFrmMnto As frmMntoWPF

    Screen.MousePointer = vbHourglass

    Dim oFrm As Form
    Dim bAbierto As Boolean
    For Each oFrm In Forms
        If oFrm.Name = "frmMntoWPF" Then
            Set oFrmMnto = oFrm
            If oFrmMnto.g_TipoMnto = TipoMnto Then
                bAbierto = True
                Exit For
            End If
        End If
    Next
    If Not bAbierto Then
        Set oFrmMnto = New frmMntoWPF
        oFrmMnto.g_TipoMnto = TipoMnto
    End If

    MostrarFormulario oFrmMnto, , True
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuParametros_Click(Index As Integer)
    Select Case Index
        Case 0  'Parametros generales
            
        Case 1  'Integraci�n
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmCONFIntegracion
            Screen.MousePointer = vbNormal
    
        Case 2  'Configuraci�n de la instalaci�n
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmCONFINST
            Screen.MousePointer = vbNormal
        
        
    End Select
End Sub

Private Sub mnuPARCalidad_Click(Index As Integer)
    Select Case Index
        Case 0  'Configuraci�n de variables
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmVARCalidad
            Screen.MousePointer = vbNormal
            
        Case 1  'Calificaci�n de las puntuaciones
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmVarCALIFICACION
            Screen.MousePointer = vbNormal
        
        Case 2  'Restricciones de adjudicaci�n
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmRestricCalidadAdj
            Screen.MousePointer = vbNormal
            
        Case 3  'Publicaci�n de puntuaciones en el portal
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmCALPuntuacionesPortal
            Screen.MousePointer = vbNormal
            
    End Select
End Sub

Private Sub mnuPEDCATConf_Click(Index As Integer)
    Select Case Index
        Case 0 'Catalogo->Configuracion
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmCatalogo
            Screen.MousePointer = vbNormal
        Case 1 'Catalogo->Gestion de Datos Externos
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmCatalogoDatosExt
            Screen.MousePointer = vbNormal
    End Select
    
End Sub

Private Sub mnuPEDSub_Click(Index As Integer)
    Dim i As Integer
    
    Select Case Index
        Case 1  'Est�ndar
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPEDIDOS
            Screen.MousePointer = vbNormal
        Case 2  'Seguimiento
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmSeguimiento
            Screen.MousePointer = vbNormal
    End Select
End Sub

Private Sub mnuPopCompar_Click(Index As Integer)
    Dim sForm As String
    Dim iGrupo As Integer
    Dim iorigen As Integer
    Dim sTag As String
    
    'Oculta la columna seleccionada de la grid
    mnuPopCompar(Index).Checked = False
    
    sForm = Mid(mnuPopUpComp.Tag, 1, InStr(1, mnuPopUpComp.Tag, "$") - 1)
    sTag = Mid(mnuPopUpComp.Tag, InStr(1, mnuPopUpComp.Tag, "$") + 1)
    iGrupo = Left(sTag, InStr(1, sTag, "$") - 1)
    iorigen = CInt(Mid(sTag, InStr(1, sTag, "$") + 1))
    
    If sForm = "frmADJ" Then
        Select Case Index
            Case 0
                frmADJ.VisualizarDetallePersona iGrupo, iorigen
            Case 2
                frmADJ.VisualizarDetalleProv iGrupo, iorigen
            Case 3
                frmADJ.VisualizarHistoricoComunicaciones iGrupo, iorigen
            Case 4
                frmADJ.IrARecepcionOfertas (iGrupo)
            Case 5
                frmADJ.VisualizarFicherosAdjuntos iGrupo, iorigen
            Case 6
                frmADJ.IrAContratos (iGrupo)
            Case 7
                frmADJ.MostrarComentarios iGrupo, iorigen
            Case 8
                frmADJ.ComparativaQA
            Case 9
                frmADJ.IrAContratos (iGrupo)
            Case 10
                frmADJ.IrADetalleContrato
        End Select
    Else
        Select Case Index
            Case 0
                frmRESREU.VisualizarDetallePersona iGrupo, iorigen
            Case 2
                frmRESREU.VisualizarDetalleProv iGrupo, iorigen
            Case 3
                frmRESREU.VisualizarHistoricoComunicaciones iGrupo, iorigen
            Case 4
                frmRESREU.IrARecepcionOfertas (iGrupo)
            Case 5
                frmRESREU.VisualizarFicherosAdjuntos iGrupo, iorigen
            Case 6
                frmRESREU.IrAContratos (iGrupo)
            Case 7
                frmRESREU.MostrarComentarios iGrupo, iorigen
            Case 8
                frmRESREU.ComparativaQA
            Case 9
                frmRESREU.IrAContratos (iGrupo)
            Case 10
                frmRESREU.IrADetalleContrato
        End Select
        
    End If
End Sub





Private Sub mnuPopUpCatCatalog_Click(Index As Integer)

    Select Case Index
            Case 1
                frmCatalogo.cmdModifCat_Click
            Case 2
                frmCatalogo.cmdEliCat_Click (0)
            Case 3, 4
                frmCatalogo.cmdAnyaCat_Click (0)
            Case 5
                frmCatalogo.cmdBajaLogica_Click
            Case 6
                frmCatalogo.DeshacerBajaLogica
            Case 8, 11 'Configurar/consultar seguridad
                frmCatalogo.MostrarSeguridadCatalogo
            Case 9 'Copiar seguridad
                frmCatalogo.CopiarSeguridad
            Case 10 'Eliminar seguridad
               frmCatalogo.EliminarSeguridad
            Case 13 'Marcar categoria por defecto para integraci�n
               'frmCatalogo.MarcarCatIntegracion
            Case 14 'Configuracion desde el nodo raiz de las categorias
                frmCatalogo.MostrarConfiguracion
    End Select
End Sub

Private Sub mnuPopUpCATSeguridadAdj_Click(Index As Integer)
    frmCATSeguridad.mnuPopupLimiteAdjudicacion (Index)
End Sub

Private Sub mnuPopUpCATSeguridadPed_Click(Index As Integer)
    frmCATSeguridad.mnuPopupLimitePedido (Index)
End Sub




Private Sub mnuPopUpCrearProceso_Click(Index As Integer)
        Select Case Index
            Case 1
                    frmPROCE.NuevoProceso
            Case 2
                    frmPROCE.GenerarProcesoDesdePlantilla
            Case 3
                    frmPROCE.CopiarProceso
        End Select
    
End Sub



Private Sub mnuPopUpEstrOrgBajaLogPer_Click()
    frmESTRORG.cmdBajaLog_Click
End Sub

Private Sub mnuPopUpEstrOrgDesBajaLogPer_Click()
    frmESTRORG.DeshacerBajaLog
End Sub

''' <summary>
''' Procedimiento que controla la opci�n elegida en el men� mnuPopUpEstrOrgNivel0
''' y ejecuta el m�todo correspondiente a la opci�n elegida
''' </summary>
''' <param name="Index">�ndice de la opci�n seleccionada en el men�</param>
''' <remarks>Llamada desde el evento de cliqueo en el men� contextual mnuPopUpEstrOrgNivel0 ; Tiempo m�ximo: 0,2</remarks>
Private Sub mnuPopUpEstrOrgNiv0_Click(Index As Integer)
    Dim bOrdDen As Boolean
    
    Select Case Index
        Case 1
            frmESTRORG.mnuPopUpAnyadirNivel1
        
        Case 2
            frmESTRORG.mnuPopUpAnyadirDep
        
        Case 4
            bOrdDen = mnuPopUpEstrOrgNiv0(5).Checked
        
            mnuPopUpEstrOrgNiv0(4).Checked = vbChecked
            mnuPopUpEstrOrgNiv0(5).Checked = vbUnchecked
            If bOrdDen Then frmESTRORG.Ordenar False
        
        Case 5
            bOrdDen = mnuPopUpEstrOrgNiv0(5).Checked
        
            mnuPopUpEstrOrgNiv0(4).Checked = vbUnchecked
            mnuPopUpEstrOrgNiv0(5).Checked = vbChecked
            If Not bOrdDen Then frmESTRORG.Ordenar True
    End Select
    
End Sub



'3328
Private Sub mnuTIPOSPED_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmMantPedidos
    Screen.MousePointer = vbNormal
End Sub




Private Sub mnuPopUpEstrOrgReuPer_Click()
    frmESTRORG.cmdReubicar_Click
End Sub

Private Sub mnuPopUpGrupo_Click(Index As Integer)
    Select Case Index
        Case 0
            frmPROCE.cmdModif_Click 1 'Grupo
        Case 1
            frmPROCE.cmdEliminarGrupo_Click
        Case 2
            frmPROCE.cmdCodigoGrupo_Click
    End Select
End Sub

Private Sub mnuPopUpGrupos_Click(Index As Integer)
    frmPROCE.MenuRaiz Index, mnuPopUpGrupos.Item(Index).caption
End Sub

Private Sub mnuPopUpGuardarPlant_Click(Index As Integer)
    Select Case Index
        Case 1
            frmPROCE.GuardarNuevaPlantilla
        Case 2
            frmPROCE.ModificarPlantilla
    End Select
End Sub


Private Sub mnuPopUpIrBuzOfe_Click()
    Screen.MousePointer = vbHourglass
    frmOFEBuzon.opNoLeidas = True
    frmOFEBuzon.sdbcAnyo = ""
    frmOFEBuzon.ProceSelector1.Seleccion = PSTodos
    frmOFEBuzon.cmdActualizar_Click
    MostrarFormulario frmOFEBuzon
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuPopUpEstrCompAnyaComp_Click()
    frmESTRCOMP.cmdA�adir_Click
End Sub

Private Sub mnuPopUpEstrCompAnyaEqp_Click()
    frmESTRCOMP.cmdA�adir_Click
End Sub

Private Sub mnuPopUpEstrCompDetCom_Click()
    frmESTRCOMP.cmdDet_Click
End Sub

Private Sub mnuPopUpEstrCompDetEqp_Click()
    frmESTRCOMP.cmdDet_Click
End Sub

Private Sub mnuPOpUpEstrCompEliCom_Click()
    frmESTRCOMP.cmdEli_Click
End Sub

Private Sub mnuPopUpEstrCompEliEqp_Click()
    frmESTRCOMP.cmdEli_Click
End Sub

Private Sub mnuPopUpEstrCompEqpCamCod_Click()
    frmESTRCOMP.CambiarCodigo
End Sub

Private Sub mnuPopUpEstrCompModCom_Click()
    frmESTRCOMP.cmdModif_Click
End Sub

Private Sub mnuPopUpEstrCompModEqp_Click()
    frmESTRCOMP.cmdModif_Click
End Sub

Private Sub mnuPopUpEstrCompOrdPorCod_Click()
    mnuPopUpEstrCompOrdPorCod.Checked = Not mnuPopUpEstrCompOrdPorCod.Checked
    mnuPopUpEstrCompOrdPorDen.Checked = Not mnuPopUpEstrCompOrdPorDen.Checked
    frmESTRCOMP.Ordenar Not mnuPopUpEstrCompOrdPorCod.Checked
End Sub

Private Sub mnuPopUpEstrCompOrdPorDen_Click()
    mnuPopUpEstrCompOrdPorCod.Checked = Not mnuPopUpEstrCompOrdPorCod.Checked
    mnuPopUpEstrCompOrdPorDen.Checked = Not mnuPopUpEstrCompOrdPorDen.Checked
    frmESTRCOMP.Ordenar Not mnuPopUpEstrCompOrdPorCod.Checked
End Sub

Private Sub mnuPopUpEstrMatAnyaGMN1_Click()

    frmESTRMAT.cmdA�adir_Click
    
End Sub

Private Sub mnuPopUpEstrMatAnyaGMN2_Click()

    frmESTRMAT.cmdA�adir_Click
    
End Sub

Private Sub mnuPopUpEstrMatAnyaGMN4_Click()

    frmESTRMAT.cmdA�adir_Click
    
End Sub

Private Sub mnuPopUpEstrMatAnyaGMN3_Click()

    frmESTRMAT.cmdA�adir_Click
    
End Sub

Private Sub mnuPopUpEstrMatDetGMN1_Click()

    frmESTRMAT.mnuPopUpEstrMatDet
    
End Sub

Private Sub mnuPopUpEstrMatDetGMN2_Click()

    frmESTRMAT.mnuPopUpEstrMatDet
    
End Sub



Private Sub mnuPopUpEstrMatDetGMN3_Click()
    
    frmESTRMAT.mnuPopUpEstrMatDet
    
End Sub

Private Sub mnuPopUpEstrMatEliGMN1_Click()
    
    frmESTRMAT.cmdEli_Click
    
End Sub

Private Sub mnuPopUpEstrMatEliGMN2_Click()
    
    frmESTRMAT.cmdEli_Click
    
End Sub



Private Sub mnuPopUpEstrMatEliGMN3_Click()
    
    frmESTRMAT.cmdEli_Click
    
End Sub

Private Sub mnuPopUpEstrMatNiv1CamCod_Click()
    frmESTRMAT.CambiarCodigo
End Sub
Private Sub mnuPopUpEstrMatNiv2CamCod_Click()
    frmESTRMAT.CambiarCodigo
End Sub

Private Sub mnuPopUpEstrMatNiv3CamCod_Click()
    frmESTRMAT.CambiarCodigo
End Sub



Private Sub mnuPopUpEstrMatOrdPorCod_Click()
    
    mnuPopUpEstrMatOrdPorCod.Checked = Not mnuPopUpEstrMatOrdPorCod.Checked
    mnuPopUpEstrMatOrdPorDen.Checked = Not mnuPopUpEstrMatOrdPorDen.Checked
    frmESTRMAT.Ordenar Not mnuPopUpEstrMatOrdPorCod.Checked
    
End Sub

Private Sub mnuPopUpEstrMatOrdPorDen_Click()

    mnuPopUpEstrMatOrdPorCod.Checked = Not mnuPopUpEstrMatOrdPorCod.Checked
    mnuPopUpEstrMatOrdPorDen.Checked = Not mnuPopUpEstrMatOrdPorDen.Checked
    frmESTRMAT.Ordenar Not mnuPopUpEstrMatOrdPorCod.Checked
    
End Sub

''' <summary>
''' Procedimiento que controla la opci�n elegida en el men� mnuPopUpEstrOrgNivel1
''' y ejecuta el m�todo correspondiente a la opci�n elegida
''' </summary>
''' <param name="Index">�ndice de la opci�n seleccionada en el men�</param>
''' <remarks>Llamada desde el evento de cliqueo en el men� contextual mnuPopUpEstrOrgNivel1; Tiempo m�ximo < 1 seg.</remarks>
Private Sub mnuPopUpEstrOrgNiv1_Click(Index As Integer)
    
    Select Case Index
        Case 1
            frmESTRORG.mnuPopUpAnyadirNivel2
        
        Case 2
            frmESTRORG.mnuPopUpAnyadirDep
        
        Case 3 'BLP
            frmESTRORG.GestionarCCoste

        Case 5
            frmESTRORG.cmdModif_Click
        
        Case 6
            frmESTRORG.cmdEli_Click
                
        Case 7
            frmESTRORG.cmdBajaLog_Click
        
        Case 8
            frmESTRORG.DeshacerBajaLog

        Case 9
            frmESTRORG.CambiarCodigo
            
        Case 11
        
            frmESTRORG.cmdDet_Click
    
    End Select
    
    
End Sub

Private Sub mnuPopUpEstrOrgCamCodPer_Click()
    
    frmESTRORG.CambiarCodigo

End Sub


Private Sub mnuPopUpEstrOrgDetPer_Click()
    
    frmESTRORG.cmdDet_Click
    
End Sub


Private Sub mnuPopUpEstrOrgEliPer_Click()
    
    frmESTRORG.cmdEli_Click

End Sub

Private Sub mnuPopUpEstrOrgModPer_Click()
    frmESTRORG.cmdModif_Click
End Sub

Private Sub mnuPopUpEstrPresConN0_Click(Index As Integer)
    Select Case Index
        Case 1
            frmPresupuestos2.cmdA�adir_Click
        Case 2
            frmPresupuestos2.mnuPopUpEstrPresConCopiarEnAnyo
        Case 3 'Copiar a otra U.O.
            frmPresupuestos2.mnuPopUpEstrPresConCopiarEnUON
        Case 5, 6
            mnuPopUpEstrPresConN0(5).Checked = Not mnuPopUpEstrPresConN0(5).Checked
            mnuPopUpEstrPresConN0(6).Checked = Not mnuPopUpEstrPresConN0(6).Checked
            frmPresupuestos2.Ordenar Not mnuPopUpEstrPresConN0(5).Checked
    End Select
End Sub

Private Sub mnuPopUpEstrPresConN1_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos2.cmdA�adir_Click
        Case 3
            frmPresupuestos2.cmdModif_Click
        Case 4
            frmPresupuestos2.cmdEli_Click
        Case 5
            frmPresupuestos2.BajaLogica
        Case 6
            frmPresupuestos2.BajaLogica
        Case 8
            frmPresupuestos2.CambiarCodigo
        Case 9
            frmPresupuestos2.mnuPopUpEstrPresConDet
        Case 11
            frmPresupuestos2.mnuPopUpEstrPresConCopiar True
        Case 12 'Copiar a otra U.O.
            frmPresupuestos2.mnuPopUpEstrPresConCopiar False
    End Select
    
End Sub

Private Sub mnuPopUpEstrPresConN2_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos2.cmdA�adir_Click
        Case 3
            frmPresupuestos2.cmdModif_Click
        Case 4
            frmPresupuestos2.cmdEli_Click
        Case 5
            frmPresupuestos2.BajaLogica
        Case 6
            frmPresupuestos2.BajaLogica
        Case 8
            frmPresupuestos2.CambiarCodigo
        Case 9
            frmPresupuestos2.mnuPopUpEstrPresConDet
        Case 11
            frmPresupuestos2.mnuPopUpEstrPresConCopiar True
        Case 12 'Copiar a otra U.O.
            frmPresupuestos2.mnuPopUpEstrPresConCopiar False
    End Select
    
End Sub

Private Sub mnuPopUpEstrPresConN3_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos2.cmdA�adir_Click
        Case 3
            frmPresupuestos2.cmdModif_Click
        Case 4
            frmPresupuestos2.cmdEli_Click
        Case 5
            frmPresupuestos2.BajaLogica
        Case 6
            frmPresupuestos2.BajaLogica
        Case 8
            frmPresupuestos2.CambiarCodigo
        Case 9
            frmPresupuestos2.mnuPopUpEstrPresConDet
        Case 11
            frmPresupuestos2.mnuPopUpEstrPresConCopiar True
        Case 12 'Copiar a otra U.O.
            frmPresupuestos2.mnuPopUpEstrPresConCopiar False
    End Select
    
End Sub

Private Sub mnuPopUpEstrPresConN4_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos2.cmdModif_Click
        Case 2
            frmPresupuestos2.cmdEli_Click
        Case 3
            frmPresupuestos2.BajaLogica
        Case 4
            frmPresupuestos2.BajaLogica
        Case 6
            frmPresupuestos2.CambiarCodigo
        Case 7
            frmPresupuestos2.mnuPopUpEstrPresConDet
        Case 9
            frmPresupuestos2.mnuPopUpEstrPresConCopiar True
        Case 10 'Copiar a otra U.O.
            frmPresupuestos2.mnuPopUpEstrPresConCopiar False
    End Select
    
End Sub

Private Sub mnuPopUpEstrPresProyN0_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos1.cmdA�adir_Click
        Case 2
            frmPresupuestos1.mnuPopUpEstrPresProyCopiarEnAnyo
        Case 3 'Copiar a otra U.O.
            frmPresupuestos1.mnuPopUpEstrPresProyCopiarEnUON
        Case 5, 6
            mnuPopUpEstrPresProyN0(5).Checked = Not mnuPopUpEstrPresProyN0(5).Checked
            mnuPopUpEstrPresProyN0(6).Checked = Not mnuPopUpEstrPresProyN0(6).Checked
            frmPresupuestos1.Ordenar Not mnuPopUpEstrPresProyN0(5).Checked
    End Select

End Sub

Private Sub mnuPopUpEstrPresProyN1_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos1.cmdA�adir_Click
        Case 3
            frmPresupuestos1.cmdModif_Click
        Case 4
            frmPresupuestos1.cmdEli_Click
        Case 5
            frmPresupuestos1.BajaLogica
        Case 6
            frmPresupuestos1.BajaLogica
        Case 8
            frmPresupuestos1.CambiarCodigo
        Case 9
            frmPresupuestos1.mnuPopUpEstrPresProyDet
        Case 11
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar True
        Case 12 'Copiar a otra U.O.
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar False
    End Select

End Sub

Private Sub mnuPopUpEstrPresProyN2_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos1.cmdA�adir_Click
        Case 3
            frmPresupuestos1.cmdModif_Click
        Case 4
            frmPresupuestos1.cmdEli_Click
        Case 5
            frmPresupuestos1.BajaLogica
        Case 6
            frmPresupuestos1.BajaLogica
        Case 8
            frmPresupuestos1.CambiarCodigo
        Case 9
            frmPresupuestos1.mnuPopUpEstrPresProyDet
        Case 11
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar True
        Case 12 'Copiar a otra U.O.
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar False
    End Select

End Sub

Private Sub mnuPopUpEstrPresProyN3_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos1.cmdA�adir_Click
        Case 3
            frmPresupuestos1.cmdModif_Click
        Case 4
            frmPresupuestos1.cmdEli_Click
        Case 5
            frmPresupuestos1.BajaLogica
        Case 6
            frmPresupuestos1.BajaLogica
        Case 8
            frmPresupuestos1.CambiarCodigo
        Case 9
            frmPresupuestos1.mnuPopUpEstrPresProyDet
        Case 11
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar True
        Case 12 'Copiar a otra U.O.
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar False
    End Select

End Sub

Private Sub mnuPopUpEstrPresProyN4_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos1.cmdModif_Click
        Case 2
            frmPresupuestos1.cmdEli_Click
        Case 3
            frmPresupuestos1.BajaLogica
        Case 4
            frmPresupuestos1.BajaLogica
        Case 6
            frmPresupuestos1.CambiarCodigo
        Case 7
            frmPresupuestos1.mnuPopUpEstrPresProyDet
        Case 9
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar True
        Case 10 'Copiar a otra U.O.
            frmPresupuestos1.mnuPopUpEstrPresProyCopiar False
    End Select

End Sub
Private Sub mnuEstrPresConcep3N0_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos3.cmdA�adir_Click
        Case 2 'Copiar a otra U.O.
            frmPresupuestos3.mnuPopUpEstrPresConcep3CopiarEnUON
        Case 4, 5
            mnuEstrPresConcep3N0(4).Checked = Not mnuEstrPresConcep3N0(4).Checked
            mnuEstrPresConcep3N0(5).Checked = Not mnuEstrPresConcep3N0(5).Checked
            frmPresupuestos3.Ordenar Not mnuEstrPresConcep3N0(4).Checked
    End Select
   
End Sub

Private Sub mnuEstrPresConcep3N1_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos3.cmdA�adir_Click
        Case 3
            frmPresupuestos3.cmdModif_Click
        Case 4
            frmPresupuestos3.cmdEli_Click
        Case 5
            frmPresupuestos3.BajaLogica
        Case 6
            frmPresupuestos3.BajaLogica
        Case 8
            frmPresupuestos3.CambiarCodigo
        Case 9
            frmPresupuestos3.mnuPopUpEstrPresConcep3Det
        Case 11 'Copiar a otra U.O.
            frmPresupuestos3.mnuPopUpEstrPresConcep3Copiar False
    End Select
    
End Sub


Private Sub mnuEstrPresConcep3N4_Click(Index As Integer)

    Select Case Index
        Case 1
            frmPresupuestos3.cmdModif_Click
        Case 2
            frmPresupuestos3.cmdEli_Click
        Case 3
            frmPresupuestos3.BajaLogica
        Case 4
            frmPresupuestos3.BajaLogica
        Case 6
            frmPresupuestos3.CambiarCodigo
        Case 7
            frmPresupuestos3.mnuPopUpEstrPresConcep3Det
        Case 9 'Copiar a otra U.O.
            frmPresupuestos3.mnuPopUpEstrPresConcep3Copiar False
    End Select
    
End Sub

Private Sub mnuEstrPresConcep4N0_Click(Index As Integer)
    Select Case Index
        Case 1
            frmPresupuestos4.cmdA�adir_Click
        Case 2 'Copiar a otra U.O.
            frmPresupuestos4.mnuPopUpEstrPresConcep4CopiarEnUON
        Case 4, 5
            mnuEstrPresConcep4N0(4).Checked = Not mnuEstrPresConcep4N0(4).Checked
            mnuEstrPresConcep4N0(5).Checked = Not mnuEstrPresConcep4N0(5).Checked
            frmPresupuestos4.Ordenar Not mnuEstrPresConcep4N0(4).Checked
    End Select
End Sub

Private Sub mnuEstrPresConcep4N1_Click(Index As Integer)
    Select Case Index
        Case 1
            frmPresupuestos4.cmdA�adir_Click
        Case 3
            frmPresupuestos4.cmdModif_Click
        Case 4
            frmPresupuestos4.cmdEli_Click
        Case 5
            frmPresupuestos4.BajaLogica
        Case 6
            frmPresupuestos4.BajaLogica
        Case 8
            frmPresupuestos4.CambiarCodigo
        Case 9
            frmPresupuestos4.mnuPopUpEstrPresConcep4Det
        Case 11 'Copiar a otra U.O.
            frmPresupuestos4.mnuPopUpEstrPresConcep4Copiar False
    End Select
End Sub


Private Sub mnuEstrPresConcep4N4_Click(Index As Integer)
    Select Case Index
        Case 1
            frmPresupuestos4.cmdModif_Click
        Case 2
            frmPresupuestos4.cmdEli_Click
        Case 3
            frmPresupuestos4.BajaLogica
        Case 4
            frmPresupuestos4.BajaLogica
        Case 6
            frmPresupuestos4.CambiarCodigo
        Case 7
            frmPresupuestos4.mnuPopUpEstrPresConcep4Det
        Case 9 'Copiar a otra U.O.
            frmPresupuestos4.mnuPopUpEstrPresConcep4Copiar False
    End Select
End Sub




Private Sub mnuPopUpItemsModif_Click(Index As Integer)
    Select Case Index
    
        Case 1  'Modificar valores
                frmPROCE.ModificarValoresMultiplesItems 1
        
        Case 2  'Destino
                frmPROCE.ModificarValoresMultiplesItems 2
        
        Case 3  'Unidad
                frmPROCE.ModificarValoresMultiplesItems 3
                
        Case 4  'Proveedor
                frmPROCE.ModificarValoresMultiplesItems 4
        
        Case 5 'Cantidad
                 frmPROCE.ModificarValoresMultiplesItems 5
        
        Case 6 'Presupuesto unitario
                frmPROCE.ModificarValoresMultiplesItems 6
        
        Case 7 'Fecha inicio
                frmPROCE.ModificarValoresMultiplesItems 7
        
        Case 8 'Fecha fin
                frmPROCE.ModificarValoresMultiplesItems 8
        
        Case 9  'Forma de pago
                frmPROCE.ModificarValoresMultiplesItems 9
        
        Case 10 'Solicitud
                frmPROCE.ModificarValoresMultiplesItems 10
        
        Case 12  'Distribucion
                frmPROCE.ModificarDistribMultiplesItems
        
        Case 13  'Presupuestos
                frmPROCE.ModificarPresupMultiplesItems
                
        Case 14 'Especificaciones
                frmPROCE.ModificarEspecMultiplesItems
        
        Case 16 'A�adir
                frmPROCE.AnyadirMultiplesItemsAGrupo
        
        Case 17 'Copiar
                frmPROCE.CopiarMultiplesItemsEnGrupo
        
        Case 18 'Eliminar
                frmPROCE.EliminarMultiplesItemsDeGrupo
        
        Case 20 'Cambiar de grupo
                frmPROCE.CambiarMultiplesItemsDeGrupo
    
    End Select

End Sub

Private Sub mnuPopUpModifPresup_Click(Index As Integer)

    frmPROCE.Menu_mnuPopUpModifPresup (Index)
    
End Sub

Private Sub mnuPopUpMostrarGrafico_Click(Index As Integer)

        g_ofrmOrigenGraficos.POPUPMostrarGrafico (Index)
        Set g_ofrmOrigenGraficos = Nothing
End Sub

Private Sub mnuPOPUPNewCopyForm_Click(Index As Integer)

    Select Case Index
        Case 0 'se crea nuevo formulario como hasta version 31000
              frmFormularios.AnyaNuevoFormulario
        Case 1
            'se copia el formulario
            frmFormularios.CopiarFormularioSeleccionado
        Case 2 'Marca el campo seleccionado como titulo para mostrarlo luego en el PM
            'He reutilizado un menu PopUp que ya existia porque no me deja crear nuevos ya que se ha llegado al limite maximo de menus
            frmFormularios.MarcarTitulo
        Case 3 'Desmarca el campo seleccionado como titulo
            frmFormularios.DesmarcarTitulo
        Case 4 'Marca el campo seleccionado como Denominaci�n de proceso
            frmFormularios.MarcarDenominacionProceso
        Case 5 'Desmarca el campo seleccionado como Denominaci�n de proceso
            frmFormularios.DesmarcarDenominacionProceso
        Case 6  'Marcar el campo seleccionado como peso
            frmFormularios.MarcarComoPeso
        Case 7  'Desmarcar el campo seleccionado como peso
            frmFormularios.DesmarcarComoPeso
    End Select
End Sub

Private Sub mnuPopUpOfeMenu_Click(Index As Integer)
    Select Case Index
        Case 1:
            'Detalle oferta
            Screen.MousePointer = vbHourglass
            frmOFEBuzon.sdbgOfertas_DblClick
            Screen.MousePointer = vbNormal

        Case 2:
            'Eliminar oferta
            Screen.MousePointer = vbHourglass
            frmOFEBuzon.EliminarOferta
            Screen.MousePointer = vbNormal
            
        Case 3:
            'A�adir oferta
            frmOFEBuzon.IrARecepcionOfertas True
            
        Case 4:
            
        Case 5:
            'Oferta le�da
            Screen.MousePointer = vbHourglass
            frmOFEBuzon.MarcarComoLeida
            Screen.MousePointer = vbNormal

        Case 6:
            'Oferta no le�da
            Screen.MousePointer = vbHourglass
            frmOFEBuzon.MarcarComoNoLeida
            Screen.MousePointer = vbNormal

    End Select
End Sub

Private Sub mnuPopUpPlantillas_Click(Index As Integer)
    Select Case Index
        Case 1
            frmPlantillasProce.ModificarPlantilla
        Case 2
            frmPlantillasProce.EliminarPlantilla
        Case 3
            frmPlantillasProce.DetalleVista
        Case 4
            frmPlantillasProce.ConfigurarVistas
        Case 5
            frmPlantillasProce.EliminarVistas
    End Select
    
End Sub

Private Sub mnuPopUpSolicitudes_Click(Index As Integer)
''' <summary>
''' evento que llama a las diferentes acciones que se pueden realizar con una solicitud: anular, eliminar...
''' </summary>
''' <remarks>
''' Tiempo m�ximo: 0 sec.</remarks>

    Select Case Index
        Case 0:
            'Detalle
            frmSolicitudes.DetalleSolicitud
        Case 1:
            'Detalle de desglose
            frmSolicitudes.DetalleDesgloseSolicitud
        Case 2:
            'Modificar
            frmSolicitudes.ModificarSolicitud
        Case 3:
            'Asignar compradores
            frmSolicitudes.AsignarComprador
        Case 4:
            'Aprobar
            frmSolicitudes.AprobarSolicitud
        Case 5:
            'Rechazar
            frmSolicitudes.RechazarSolicitud
        Case 6:
            'Cerrar
            frmSolicitudes.CerrarSolicitud
        Case 7:
            'Reabrir
            frmSolicitudes.ReabrirSolicitud
        Case 8:
            'Anular
            frmSolicitudes.AnularSolicitud
        Case 9:
            'Eliminar
            frmSolicitudes.EliminarSolicitud
        Case 10
            'Activar monitorizaci�n
            frmSolicitudes.ActivarMonitorizacion
        Case 11
            'Desactivar monitorizaci�n
            frmSolicitudes.DesactivarMonitorizacion
    End Select
End Sub

Private Sub mnuPopUpUsuAnya_Click()
    
    frmUSUARIOS.cmdUsu_Click 0

End Sub


Private Sub mnuPopUpUsu_Click(Index As Integer)
    Select Case Index
        Case 1 'Modificar Usuario
            frmUSUARIOS.cmdUsu_Click 1
        Case 2 'Eliminar Usuario
            frmUSUARIOS.cmdUsu_Click 2
        Case 3 'Detalle de Usuario
            frmUSUARIOS.mnuPopUpUsuDet
    End Select
End Sub


Private Sub mnuPresAnu_Click(Index As Integer)
        Select Case Index
        Case 1
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPresupuestos1
            Screen.MousePointer = vbNormal
        Case 2
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPresupuestos2
            Screen.MousePointer = vbNormal
    End Select
End Sub

Private Sub mnuPresArtMod_Click(Index As Integer)
    frmPRESArt.ModificarPresupuestos (Index)
End Sub

Private Sub mnuPresArtPres_Click(Index As Integer)

    frmPRESArt.CargarPresupuestos (Index)
    
End Sub

Private Sub mnuPRESMAT_Click()
    Screen.MousePointer = vbHourglass
    MostrarFormulario frmPRESArt
    Screen.MousePointer = vbNormal
End Sub

Private Sub mnuPROCE_Click(Index As Integer)
Screen.MousePointer = vbHourglass
Select Case Index
Case 0 'Solicitudes
    MostrarFormulario frmSolicitudes
Case 1
    MostrarFormulario frmPROCE
Case 2
    MostrarFormulario frmSELPROVE
Case 3
    MostrarFormulario frmOFEPet
Case 4

Case 5
    MostrarFormulario frmADJ
Case 6
    MostrarPaginaWeb Me, mnuPROCE.Item(Index).Name, gParametrosGenerales.gsURLPM
End Select
Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Mostrar una pagina web
''' </summary>
''' <param name="frmOrigen">Padre para el formulario q contiene el objeto donde se muestra la pagina</param>
''' <param name="sNombre">Titulo para el formulario q contiene el objeto donde se muestra la pagina</param>
''' <param name="sRuta">pagina web</param>
''' <remarks>Llamada desde: MDI.mnuPROCE_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub MostrarPaginaWeb(ByVal frmOrigen As Form, ByVal sNombre As String, ByVal sRuta As String)
    Dim strSessionId As String
    
    If Len(sRuta) <> 0 Then
        strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
         
        With frmInternet
            .g_sOrigen = frmOrigen.Name
            .g_sNombre = sNombre
            .g_sRuta = sRuta & "?desdeGS=1&SessionId=" & strSessionId
            If MDI.ActiveForm Is Nothing Then
                .WindowState = vbNormal
            Else
                If MDI.ActiveForm.WindowState = vbMaximized Then
                    .WindowState = vbMaximized
                Else
                    .WindowState = vbNormal
                End If
            End If
            .SetFocus
        End With
    End If
End Sub

Private Sub mnuProceEst_Click()

    Screen.MousePointer = vbHourglass
    If mnuProceEst.Checked Then
        mnuProceEst.Checked = False
        Unload frmEST
    Else
        mnuProceEst.Checked = True
        frmEST.lblSaludo.caption = m_sTituloVisor
        If Not frmEST.Visible Then frmEST.Visible = True
        MostrarFormulario frmEST
    End If
    Screen.MousePointer = vbNormal
   
End Sub



Private Sub mnuPROVE_Click(Index As Integer)
    Select Case Index
        Case 1
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPROVE
            Screen.MousePointer = vbNormal
        
        Case 2
            Screen.MousePointer = vbHourglass
            frmAdmProvePortal.g_sOrigen = "MDI"
            MostrarFormulario frmAdmProvePortal
            Screen.MousePointer = vbNormal
            
        Case 3
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPROVEMatPorProve
            Screen.MousePointer = vbNormal
            
        Case 4
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPROVEProvePorMat
            Screen.MousePointer = vbNormal
            
        Case 5
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPROVEEqpPorProve
            Screen.MousePointer = vbNormal
        
        Case 6
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmPROVEProvePorEqp
            Screen.MousePointer = vbNormal
    End Select
End Sub


Private Sub mnuRES_Click(Index As Integer)
    Select Case Index
        Case 1
        
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmREU
            Screen.MousePointer = vbNormal
        
        Case 2
            Screen.MousePointer = vbHourglass
            MostrarFormulario frmRESREU
            Screen.MousePointer = vbNormal
    End Select
End Sub

Private Sub mnuResponsable_Click(Index As Integer)
    Select Case Index
        Case 0
            'Ver detalle
            Screen.MousePointer = vbHourglass
            g_ofrmOrigenResponsable.VerDetalleResponsable
            Screen.MousePointer = vbNormal
        Case 1
            'Sustituir
            Screen.MousePointer = vbHourglass
            g_ofrmOrigenResponsable.SustituirResponsable
            Screen.MousePointer = vbNormal
    End Select
    Set g_ofrmOrigenResponsable = Nothing
End Sub

Private Sub mnuSeguridad_Click(Index As Integer)

    Select Case Index

            Case 0
                    Screen.MousePointer = vbHourglass
                    MostrarFormulario frmUSUARIOS
                    Screen.MousePointer = vbNormal
            Case 1
                    Screen.MousePointer = vbHourglass
                    AbrirMntoPerfiles
                    Screen.MousePointer = vbNormal
            Case 2
                    Screen.MousePointer = vbHourglass
                    MostrarFormulario frmLISPERSegur
                    Screen.MousePointer = vbNormal
            Case 4
                    'Se elimina la opci�n de cambio de contrase�a de administrador
                    Screen.MousePointer = vbHourglass
                    MostrarFormUSUCamb oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, False, oUsuarioSummit, oMensajes, gParametrosGenerales
                    Screen.MousePointer = vbNormal
            Case 5
                    Screen.MousePointer = vbHourglass
                    frmOBJSADMCamb.Show 1
                    Screen.MousePointer = vbNormal

    End Select

End Sub

''' <summary>Abre el mnto. de perfiles que est� en FullstepWeb</summary>
''' <remarks>Llamada desde: mnuSeguridad_Click</remarks>
''' <revision>LTG 12/12/2012</revision>

Private Sub AbrirMntoPerfiles()
    Dim sRuta As String
    Dim sSessionId As String
    
    sSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
    
    sRuta = gParametrosGenerales.gsRutasFullstepWeb
    If Right(sRuta, 1) <> "/" Then sRuta = sRuta & "/"
    sRuta = sRuta & "App_Pages/SG/VisorPerfiles.aspx?sessionId=" & sSessionId & "&DesdeGS=1"
    
    With frmWebChild
        .g_sRuta = sRuta
        .caption = mnuSeguridad.Item(1).caption
        .Show
        .SetFocus
    End With
End Sub

Private Sub mnuSolicitudes_Click(Index As Integer)
    Screen.MousePointer = vbHourglass
    
    Select Case Index
        Case 2 'Cumplimentaci�n
            MostrarFormulario frmSOLConfiguracion
        Case 0 'Formularios
            MostrarFormulario frmFormularios
            
    End Select
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub mnuVisorApertura_Click()
    
    frmEST.mnuVisorApertura
    
End Sub

Private Sub mnuVisorComparativa_Click()
    frmEST.mnuVisorComparativa
End Sub

Private Sub mnuVisorComuniProve_Click()
    frmEST.mnuVisorComuniProve
End Sub

Private Sub mnuVisorPanel_Click()
    frmEST.mnuVisorPanel
End Sub

Private Sub mnuVisorPresup_Click(Index As Integer)
    Select Case Index
        Case 0
            Screen.MousePointer = vbHourglass
            frmSELPresAnuUON.sOrigen = "frmEST1"
            frmSELPresAnuUON.g_iTipoPres = 1
            Screen.MousePointer = vbNormal
            frmSELPresAnuUON.Show 1
            
            
        Case 1
            Screen.MousePointer = vbHourglass
            frmSELPresAnuUON.sOrigen = "frmEST2"
            frmSELPresAnuUON.g_iTipoPres = 2
            Screen.MousePointer = vbNormal
            frmSELPresAnuUON.Show 1
            
        Case 2
            Screen.MousePointer = vbHourglass
            frmSELPresUO.sOrigen = "frmEST3"
            frmSELPresUO.g_iTipoPres = 3
            Screen.MousePointer = vbNormal
            frmSELPresUO.Show 1
            
        Case 3
            Screen.MousePointer = vbHourglass
            frmSELPresUO.sOrigen = "frmEST4"
            frmSELPresUO.g_iTipoPres = 4
            Screen.MousePointer = vbNormal
            frmSELPresUO.Show 1
            
    End Select
    
End Sub

Private Sub mnuVisorRecOfe_Click()
    frmEST.mnuVisorRecOfe
End Sub

Private Sub mnuVisorSelProve_Click()
    frmEST.mnuVisorSelProve
End Sub

Private Sub Timer1_Timer()
Dim pos As POINTAPI

    If Not IsAppActive Then
        g_iContadorBloqueo = g_iContadorBloqueo + 1
    Else
        If GetMouseOver(Me.hWnd) = True Then 'Si el mouse esta dentro del formulario
            'comparar las posiciones anterior y actual
            GetCursorPos pos
            If pos.X = g_CurPos.X And pos.Y = g_CurPos.Y Then
                g_iContadorBloqueo = g_iContadorBloqueo + 1
                'No se ha movido, incrementamos contador
            Else
                'Se ha movido ponemos a 0 el contador y guardamos la posici�n
                g_iContadorBloqueo = 0
                g_CurPos.X = pos.X
                g_CurPos.Y = pos.Y
            End If

        Else
            'Si el mouse esta fuera del formulario
            'Incrementamos contador
            g_iContadorBloqueo = g_iContadorBloqueo + 1
        End If
    End If
    If g_iContadorBloqueo / 12 >= gParametrosGenerales.giTimeOutBloqueoProce Then
        g_iContadorBloqueo = 0
        
        DesbloquearProcesosEnFormularios

    End If

End Sub

Private Sub DesbloquearProcesosEnFormularios()
Dim vElement As Variant
Dim iCount As Integer

iCount = 0
For Each vElement In g_dicBloqueos

    Select Case vElement
    Case "frmPROCE"
        frmPROCE.TimeoutBloqueo
        iCount = iCount + 1
    Case "frmSELPROVE"
        frmSELPROVE.TimeoutBloqueo
        iCount = iCount + 1
    Case "frmOFERec"
        frmOFERec.TimeoutBloqueo
        iCount = iCount + 1
    Case "frmADJ"
        'Si esta validando no desbloquea
        If frmADJ.m_udtOrigBloqueo <> Validando Then
            frmADJ.TimeoutBloqueo
            iCount = iCount + 1
        End If
    Case "frmRESREU"
        If frmRESREU.m_udtOrigBloqueo <> Validando Then
            frmRESREU.TimeoutBloqueo
            iCount = iCount + 1
        End If
        
    End Select
Next
If iCount > 0 Then
    oMensajes.TimeoutBloqueoDeProceso gParametrosGenerales.giTimeOutBloqueoProce
End If
End Sub

Private Function IsAppActive() As Boolean
Dim hFG As Long, pId As Long

    hFG = GetForegroundWindow()
    If hFG <> 0 Then
        GetWindowThreadProcessId hFG, pId
        If pId = GetCurrentProcessId() Then
            IsAppActive = True
        End If
    End If
End Function


Public Sub MostrarFormulario(frm As Form, Optional ByVal NoFoco As Boolean, Optional ByVal NoVisible As Boolean)
    If MDI.ActiveForm Is Nothing Then
        frm.WindowState = vbNormal
    Else
        If MDI.ActiveForm.WindowState = vbMaximized Then
            frm.WindowState = vbMaximized
        Else
            frm.WindowState = vbNormal
        End If
    End If
    
    If NoVisible Then
        frm.Visible = True
    End If
    
    If Not NoFoco And frm.Visible Then
        frm.SetFocus
    End If
End Sub

Private Sub CerrarManualAyuda()
Dim i As Long
Dim oFos As Scripting.FileSystemObject
Dim sFileHelp As String

    Call fEnumWindows

    If colHandle.Count = 0 And glPid <> 0 Then   'Si el manual ya estaba abierto
        Set oFos = New Scripting.FileSystemObject
        If oFos.FileExists(gParametrosGenerales.gsHelpPDF) = True Then
            'Reemplaza el manual de ayuda por el del idioma de la aplicaci�n
            sFileHelp = oFos.GetFileName(gParametrosGenerales.gsHelpPDF)
            If sFileHelp <> "" Then
                sFileHelp = basPublic.gParametrosInstalacion.gIdioma & "_" & Mid(sFileHelp, 5)
                glHandle = FindWindow(0&, "Adobe reader - [" & sFileHelp & "]")
                Call SendMessage(glHandle, WM_CLOSE, 0&, 0&)
            End If
        End If
        Set oFos = Nothing
        
        
    Else  'si el manual se ha abierto desde el GS
        For i = 1 To colHandle.Count
            glHandle = colHandle.Item(i)
            Call SendMessage(glHandle, WM_CLOSE, 0&, 0&)
        Next i
    End If
    
    
End Sub

Private Sub mnuPopUpEstrOrgDepart_Click(Index As Integer)
    Select Case Index
        Case 1
            frmESTRORG.cmdA�adir_Click
        
        Case 3
            frmESTRORG.cmdModif_Click
        
        Case 4
            frmESTRORG.cmdEli_Click
        
        Case 5
            frmESTRORG.CambiarCodigo
                
        Case 7
            frmESTRORG.cmdDet_Click
    End Select
End Sub

''' <summary>
''' Procedimiento que controla la opci�n elegida en el men� mnuPopUpEstrOrgNivel2
''' y ejecuta el m�todo correspondiente a la opci�n elegida
''' </summary>
''' <param name="Index">�ndice de la opci�n seleccionada en el men�</param>
''' <remarks>Llamada desde el evento de cliqueo en el men� contextual mnuPopUpEstrOrgNivel2; Tiempo m�ximo < 1 seg.</remarks>
Private Sub mnuPopUpEstrOrgNiv2_Click(Index As Integer)
    Select Case Index
        Case 1
            frmESTRORG.mnuPopUpAnyadirNivel3
        
        Case 2
            frmESTRORG.mnuPopUpAnyadirDep
        
        Case 3
            frmESTRORG.GestionarCCoste
        
        Case 5
            frmESTRORG.cmdModif_Click
        
        Case 6
            frmESTRORG.cmdEli_Click
               
        Case 7
            frmESTRORG.cmdBajaLog_Click
                
        Case 8
            frmESTRORG.DeshacerBajaLog
                
        Case 9
            frmESTRORG.CambiarCodigo
            
        Case 11
            frmESTRORG.cmdDet_Click
   
    End Select
    
End Sub

''' <summary>
''' Procedimiento que controla la opci�n elegida en el men� mnuPopUpEstrOrgNivel3
''' y ejecuta el m�todo correspondiente a la opci�n elegida
''' </summary>
''' <param name="Index">�ndice de la opci�n seleccionada en el men�</param>
''' <remarks>Llamada desde el evento de cliqueo en el men� contextual mnuPopUpEstrOrgNivel3; Tiempo m�ximo < 1 seg.</remarks>
Private Sub mnuPopUpEstrOrgNiv3_Click(Index As Integer)

        Select Case Index
            Case 1
                frmESTRORG.mnuPopUpAnyadirCC
            
            Case 2
                frmESTRORG.mnuPopUpAnyadirDep
            
            Case 3 'BLP
                frmESTRORG.GestionarCCoste
            
            Case 5
                frmESTRORG.cmdModif_Click
            
            Case 6
                frmESTRORG.cmdEli_Click
                   
            Case 7
                frmESTRORG.cmdBajaLog_Click
                    
            Case 8
                frmESTRORG.DeshacerBajaLog
                    
            Case 9
                frmESTRORG.CambiarCodigo
                
            Case 11
            
                frmESTRORG.cmdDet_Click
        
        End Select
   
End Sub

Private Sub mnuTipoAtributoEsp_Click(Index As Integer)
Select Case Index
Case 0
    g_ofrmOrigenResponsable.MostrarDetalleAtributo
Case 1
    g_ofrmOrigenResponsable.MarcarDesmarcarAtribInterno 1
Case 2
    g_ofrmOrigenResponsable.MarcarDesmarcarAtribInterno 0
Case 3
    g_ofrmOrigenResponsable.MarcarDesmarcarAtribPedido 1
Case 4
    g_ofrmOrigenResponsable.MarcarDesmarcarAtribPedido 0
Case 5
    g_ofrmOrigenResponsable.MarcarDesmarcarSincronizarArticulo 1, 0
Case 6
    g_ofrmOrigenResponsable.MarcarDesmarcarSincronizarArticulo 0, 0
End Select
End Sub

''' <summary>
''' Procedimiento que controla la opci�n elegida en el men� mnuPopUpEstrOrgNivel4
''' y ejecuta el m�todo correspondiente a la opci�n elegida
''' </summary>
''' <param name="Index">�ndice de la opci�n seleccionada en el men�</param>
''' <remarks>Llamada desde el evento de cliqueo en el men� contextual mnuPopUpEstrOrgNivel4; Tiempo m�ximo < 1 seg.</remarks>
Private Sub mnuPopUpEstrOrgNiv4_Click(Index As Integer)

    If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.AccesoFSSM Then
    'Si tiene el producto FSSM activado, tendr� la opcion A�adir Centro de Coste

        Select Case Index
            
            Case 1
                frmESTRORG.GestionarCCoste
                
            Case 3

                frmESTRORG.cmdModif_Click
            
            Case 4
                frmESTRORG.cmdEli_Click
                   
            Case 5
                frmESTRORG.cmdBajaLog_Click
                    
            Case 6
                frmESTRORG.DeshacerBajaLog
                
            Case 7
            
                frmESTRORG.cmdDet_Click
                
            Case 8
            
                frmESTRORG.cmdDet_Click
        
        End Select
        
    End If

End Sub

Private Sub mnuEstrPres5_click(Index As Integer)
Select Case Index
            
            Case 1
                frmPresupuestos5.cmdA�adir_Click
            
            Case 2  'Separador
                
                   
            Case 3
                frmPresupuestos5.cmdModif_Click
                    
            Case 4
                frmPresupuestos5.cmdEli_Click
                
            Case 5, 6
            
                frmPresupuestos5.cmdBajaLog_Click
                
            Case 7 'Separador
                
            Case 8
                frmPresupuestos5.MostarDetalle
        
        End Select
End Sub

Private Sub mnuPopUpFSQAVarCal_Click(Index As Integer)
    Select Case Index
        Case 1
            'marca el campo para que sea de piezas defectuosas
            frmFormularios.marcarPiezasDefectuosas True
        Case 2
            'marca el campo para que sea de Envios correctos
            frmFormularios.MarcarEnviosCorrectos True
        Case 3
            'Desmarcar el campo para que sea de piezas defectuosas
            frmFormularios.marcarPiezasDefectuosas False
        Case 4
            'marca el campo para que sea de Envios correctos
            frmFormularios.MarcarEnviosCorrectos False
        Case 5  'Marcar el campo seleccionado como peso
            frmFormularios.MarcarComoPeso
        Case 6  'Desmarcar el campo seleccionado como peso
            frmFormularios.DesmarcarComoPeso
    End Select
End Sub
Private Sub mnuPopUpFSQAVarCalDesg_Click(Index As Integer)

 Select Case Index
        Case 1
            frmFormularios.g_ofrmDesglose.marcarPiezasDefectuosas True
        Case 2
            frmFormularios.g_ofrmDesglose.MarcarEnviosCorrectos True
        Case 3
             frmFormularios.g_ofrmDesglose.marcarPiezasDefectuosas False
        Case 4
             frmFormularios.g_ofrmDesglose.MarcarEnviosCorrectos False
    End Select
End Sub


Private Sub mnuPOPUPSincroArti_Click(Index As Integer)
    Select Case Index
    Case 1
        frmPROCE.MarcarDesmarcarSincronizarArticulo 1, frmPROCE.sdbgItems.Columns("ID").Value
    Case 2
        frmPROCE.MarcarDesmarcarSincronizarArticulo 0, frmPROCE.sdbgItems.Columns("ID").Value
    End Select
End Sub

