VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.UserControl ctlATRIB 
   ClientHeight    =   7170
   ClientLeft      =   480
   ClientTop       =   3630
   ClientWidth     =   10545
   ScaleHeight     =   7170
   ScaleWidth      =   10545
   Begin TabDlg.SSTab sstabgeneralCtrl 
      Height          =   7050
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10410
      _ExtentX        =   18362
      _ExtentY        =   12435
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DFiltro"
      TabPicture(0)   =   "ctlATRIB.ctx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "frmIntro"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "frmTipo"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraAtributo"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "frmMat"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "frmArticulo"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "frmTipoAtributo"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "fraCosteDtos"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Picture1"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "frmAmbito"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).ControlCount=   9
      TabCaption(1)   =   "DAtributos"
      TabPicture(1)   =   "ctlATRIB.ctx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbgAtributosGrupoCtrl"
      Tab(1).Control(1)=   "sdbddTipo"
      Tab(1).Control(2)=   "picEdit"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "picNavigate"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "sdbddValor"
      Tab(1).ControlCount=   5
      Begin SSDataWidgets_B.SSDBGrid sdbgAtributosGrupoCtrl 
         Height          =   4290
         Left            =   -74925
         TabIndex        =   10
         Top             =   420
         Width           =   9765
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   24
         stylesets.count =   10
         stylesets(0).Name=   "Ponderacion"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "ctlATRIB.ctx":0038
         stylesets(1).Name=   "ConDescr"
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "ctlATRIB.ctx":01A3
         stylesets(1).AlignmentText=   1
         stylesets(1).AlignmentPicture=   1
         stylesets(2).Name=   "Azulito"
         stylesets(2).BackColor=   16777160
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "ctlATRIB.ctx":01EA
         stylesets(3).Name=   "Yellow"
         stylesets(3).BackColor=   8454143
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "ctlATRIB.ctx":0206
         stylesets(4).Name=   "Gris"
         stylesets(4).BackColor=   13619151
         stylesets(4).HasFont=   -1  'True
         BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(4).Picture=   "ctlATRIB.ctx":0222
         stylesets(5).Name=   "Bold"
         stylesets(5).HasFont=   -1  'True
         BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(5).Picture=   "ctlATRIB.ctx":023E
         stylesets(6).Name=   "Proceso"
         stylesets(6).BackColor=   10461087
         stylesets(6).HasFont=   -1  'True
         BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(6).Picture=   "ctlATRIB.ctx":025A
         stylesets(7).Name=   "Normal"
         stylesets(7).HasFont=   -1  'True
         BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(7).Picture=   "ctlATRIB.ctx":0276
         stylesets(8).Name=   "Green"
         stylesets(8).BackColor=   7912636
         stylesets(8).HasFont=   -1  'True
         BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(8).Picture=   "ctlATRIB.ctx":0292
         stylesets(9).Name=   "3puntos"
         stylesets(9).HasFont=   -1  'True
         BeginProperty stylesets(9).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(9).Picture=   "ctlATRIB.ctx":02AE
         UseGroups       =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   3
         HeadStyleSet    =   "Normal"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         ExtraHeight     =   26
         Groups.Count    =   6
         Groups(0).Width =   7541
         Groups(0).Caption=   "DDefinici�n de atributos"
         Groups(0).HasHeadForeColor=   -1  'True
         Groups(0).HasHeadBackColor=   -1  'True
         Groups(0).HeadForeColor=   16777215
         Groups(0).HeadBackColor=   32896
         Groups(0).StyleSet=   "Green"
         Groups(0).Columns.Count=   11
         Groups(0).Columns(0).Width=   688
         Groups(0).Columns(0).Caption=   "Oferta"
         Groups(0).Columns(0).Name=   "OFERTA"
         Groups(0).Columns(0).DataField=   "Column 0"
         Groups(0).Columns(0).DataType=   8
         Groups(0).Columns(0).FieldLen=   256
         Groups(0).Columns(0).Style=   2
         Groups(0).Columns(0).HasHeadForeColor=   -1  'True
         Groups(0).Columns(0).HasHeadBackColor=   -1  'True
         Groups(0).Columns(0).HeadForeColor=   16777215
         Groups(0).Columns(0).HeadBackColor=   32896
         Groups(0).Columns(1).Width=   1138
         Groups(0).Columns(1).Caption=   "dCoste"
         Groups(0).Columns(1).Name=   "COSTE"
         Groups(0).Columns(1).DataField=   "Column 1"
         Groups(0).Columns(1).DataType=   8
         Groups(0).Columns(1).FieldLen=   256
         Groups(0).Columns(1).Locked=   -1  'True
         Groups(0).Columns(1).Style=   2
         Groups(0).Columns(1).HasHeadForeColor=   -1  'True
         Groups(0).Columns(1).HasHeadBackColor=   -1  'True
         Groups(0).Columns(1).HeadForeColor=   16777215
         Groups(0).Columns(1).HeadBackColor=   32896
         Groups(0).Columns(2).Width=   1588
         Groups(0).Columns(2).Caption=   "dDescuento"
         Groups(0).Columns(2).Name=   "DESCUENTO"
         Groups(0).Columns(2).DataField=   "Column 2"
         Groups(0).Columns(2).DataType=   8
         Groups(0).Columns(2).FieldLen=   256
         Groups(0).Columns(2).Locked=   -1  'True
         Groups(0).Columns(2).Style=   2
         Groups(0).Columns(2).HasHeadForeColor=   -1  'True
         Groups(0).Columns(2).HasHeadBackColor=   -1  'True
         Groups(0).Columns(2).HeadForeColor=   16777215
         Groups(0).Columns(2).HeadBackColor=   32896
         Groups(0).Columns(3).Width=   900
         Groups(0).Columns(3).Caption=   "DC�digo"
         Groups(0).Columns(3).Name=   "COD"
         Groups(0).Columns(3).DataField=   "Column 3"
         Groups(0).Columns(3).DataType=   8
         Groups(0).Columns(3).FieldLen=   20
         Groups(0).Columns(3).Locked=   -1  'True
         Groups(0).Columns(3).ButtonsAlways=   -1  'True
         Groups(0).Columns(3).HasHeadForeColor=   -1  'True
         Groups(0).Columns(3).HasHeadBackColor=   -1  'True
         Groups(0).Columns(3).HasForeColor=   -1  'True
         Groups(0).Columns(3).HasBackColor=   -1  'True
         Groups(0).Columns(3).HeadForeColor=   16777215
         Groups(0).Columns(3).HeadBackColor=   32896
         Groups(0).Columns(3).BackColor=   16777160
         Groups(0).Columns(4).Width=   953
         Groups(0).Columns(4).Caption=   "DDenominaci�n"
         Groups(0).Columns(4).Name=   "NOMBRE"
         Groups(0).Columns(4).DataField=   "Column 4"
         Groups(0).Columns(4).DataType=   8
         Groups(0).Columns(4).FieldLen=   50
         Groups(0).Columns(4).ButtonsAlways=   -1  'True
         Groups(0).Columns(4).HasHeadForeColor=   -1  'True
         Groups(0).Columns(4).HasHeadBackColor=   -1  'True
         Groups(0).Columns(4).HasForeColor=   -1  'True
         Groups(0).Columns(4).HasBackColor=   -1  'True
         Groups(0).Columns(4).HeadForeColor=   16777215
         Groups(0).Columns(4).HeadBackColor=   32896
         Groups(0).Columns(4).BackColor=   16777215
         Groups(0).Columns(5).Width=   900
         Groups(0).Columns(5).Caption=   "Descr."
         Groups(0).Columns(5).Name=   "DESCR"
         Groups(0).Columns(5).Alignment=   2
         Groups(0).Columns(5).DataField=   "Column 5"
         Groups(0).Columns(5).DataType=   8
         Groups(0).Columns(5).FieldLen=   256
         Groups(0).Columns(5).Style=   4
         Groups(0).Columns(5).ButtonsAlways=   -1  'True
         Groups(0).Columns(5).HasHeadForeColor=   -1  'True
         Groups(0).Columns(5).HasHeadBackColor=   -1  'True
         Groups(0).Columns(5).HeadForeColor=   16777215
         Groups(0).Columns(5).HeadBackColor=   32896
         Groups(0).Columns(5).StyleSet=   "Bold"
         Groups(0).Columns(6).Width=   1376
         Groups(0).Columns(6).Caption=   "DTipo de dato"
         Groups(0).Columns(6).Name=   "TIPO"
         Groups(0).Columns(6).DataField=   "Column 6"
         Groups(0).Columns(6).DataType=   8
         Groups(0).Columns(6).FieldLen=   256
         Groups(0).Columns(6).Style=   3
         Groups(0).Columns(6).Row.Count=   4
         Groups(0).Columns(6).Col.Count=   2
         Groups(0).Columns(6).Row(0).Col(0)=   "Num�rico"
         Groups(0).Columns(6).Row(1).Col(0)=   "Fecha"
         Groups(0).Columns(6).Row(2).Col(0)=   "S�/No"
         Groups(0).Columns(6).Row(3).Col(0)=   "Texto"
         Groups(0).Columns(6).HasHeadForeColor=   -1  'True
         Groups(0).Columns(6).HasHeadBackColor=   -1  'True
         Groups(0).Columns(6).HasBackColor=   -1  'True
         Groups(0).Columns(6).HeadForeColor=   16777215
         Groups(0).Columns(6).HeadBackColor=   32896
         Groups(0).Columns(6).BackColor=   16777160
         Groups(0).Columns(7).Width=   3678
         Groups(0).Columns(7).Visible=   0   'False
         Groups(0).Columns(7).Caption=   "ID"
         Groups(0).Columns(7).Name=   "ID"
         Groups(0).Columns(7).DataField=   "Column 7"
         Groups(0).Columns(7).DataType=   8
         Groups(0).Columns(7).FieldLen=   256
         Groups(0).Columns(8).Width=   3704
         Groups(0).Columns(8).Visible=   0   'False
         Groups(0).Columns(8).Caption=   "idTIPO"
         Groups(0).Columns(8).Name=   "idTIPO"
         Groups(0).Columns(8).DataField=   "Column 8"
         Groups(0).Columns(8).DataType=   8
         Groups(0).Columns(8).FieldLen=   256
         Groups(0).Columns(9).Width=   3625
         Groups(0).Columns(9).Visible=   0   'False
         Groups(0).Columns(9).Caption=   "GENERICO"
         Groups(0).Columns(9).Name=   "GENERICO"
         Groups(0).Columns(9).DataField=   "Column 9"
         Groups(0).Columns(9).DataType=   8
         Groups(0).Columns(9).FieldLen=   256
         Groups(0).Columns(10).Width=   3545
         Groups(0).Columns(10).Visible=   0   'False
         Groups(0).Columns(10).Caption=   "LISTA_EXTERNA"
         Groups(0).Columns(10).Name=   "LISTA_EXTERNA"
         Groups(0).Columns(10).DataField=   "Column 10"
         Groups(0).Columns(10).DataType=   8
         Groups(0).Columns(10).FieldLen=   256
         Groups(1).Width =   4842
         Groups(1).Caption=   "DIntroducci�n"
         Groups(1).HasHeadForeColor=   -1  'True
         Groups(1).HasHeadBackColor=   -1  'True
         Groups(1).HeadForeColor=   16777215
         Groups(1).HeadBackColor=   32896
         Groups(1).Columns.Count=   5
         Groups(1).Columns(0).Width=   661
         Groups(1).Columns(0).Caption=   "DLibre"
         Groups(1).Columns(0).Name=   "LIBRE"
         Groups(1).Columns(0).DataField=   "Column 11"
         Groups(1).Columns(0).DataType=   8
         Groups(1).Columns(0).FieldLen=   256
         Groups(1).Columns(0).Style=   2
         Groups(1).Columns(0).HasHeadForeColor=   -1  'True
         Groups(1).Columns(0).HasHeadBackColor=   -1  'True
         Groups(1).Columns(0).HasForeColor=   -1  'True
         Groups(1).Columns(0).HeadForeColor=   16777215
         Groups(1).Columns(0).HeadBackColor=   32896
         Groups(1).Columns(1).Width=   979
         Groups(1).Columns(1).Caption=   "DM�nimo"
         Groups(1).Columns(1).Name=   "MINIMO"
         Groups(1).Columns(1).CaptionAlignment=   2
         Groups(1).Columns(1).DataField=   "Column 12"
         Groups(1).Columns(1).DataType=   8
         Groups(1).Columns(1).FieldLen=   256
         Groups(1).Columns(1).HasHeadForeColor=   -1  'True
         Groups(1).Columns(1).HasHeadBackColor=   -1  'True
         Groups(1).Columns(1).HeadForeColor=   16777215
         Groups(1).Columns(1).HeadBackColor=   32896
         Groups(1).Columns(2).Width=   953
         Groups(1).Columns(2).Caption=   "DM�ximo"
         Groups(1).Columns(2).Name=   "MAXIMO"
         Groups(1).Columns(2).CaptionAlignment=   2
         Groups(1).Columns(2).DataField=   "Column 13"
         Groups(1).Columns(2).DataType=   8
         Groups(1).Columns(2).FieldLen=   256
         Groups(1).Columns(2).HasHeadForeColor=   -1  'True
         Groups(1).Columns(2).HasHeadBackColor=   -1  'True
         Groups(1).Columns(2).HeadForeColor=   16777215
         Groups(1).Columns(2).HeadBackColor=   32896
         Groups(1).Columns(3).Width=   1270
         Groups(1).Columns(3).Caption=   "DSelecci�n"
         Groups(1).Columns(3).Name=   "SELECCION"
         Groups(1).Columns(3).DataField=   "Column 14"
         Groups(1).Columns(3).DataType=   8
         Groups(1).Columns(3).FieldLen=   256
         Groups(1).Columns(3).Style=   2
         Groups(1).Columns(3).HasHeadForeColor=   -1  'True
         Groups(1).Columns(3).HasHeadBackColor=   -1  'True
         Groups(1).Columns(3).HeadForeColor=   16777215
         Groups(1).Columns(3).HeadBackColor=   32896
         Groups(1).Columns(4).Width=   979
         Groups(1).Columns(4).Name=   "BOT"
         Groups(1).Columns(4).Alignment=   2
         Groups(1).Columns(4).DataField=   "Column 15"
         Groups(1).Columns(4).DataType=   8
         Groups(1).Columns(4).FieldLen=   256
         Groups(1).Columns(4).Style=   4
         Groups(1).Columns(4).ButtonsAlways=   -1  'True
         Groups(1).Columns(4).HasHeadForeColor=   -1  'True
         Groups(1).Columns(4).HasHeadBackColor=   -1  'True
         Groups(1).Columns(4).HeadForeColor=   16777215
         Groups(1).Columns(4).HeadBackColor=   32896
         Groups(1).Columns(4).StyleSet=   "Bold"
         Groups(2).Width =   1799
         Groups(2).Caption=   "DPreferencia"
         Groups(2).HasHeadForeColor=   -1  'True
         Groups(2).HasHeadBackColor=   -1  'True
         Groups(2).HeadForeColor=   16777215
         Groups(2).HeadBackColor=   32896
         Groups(2).Columns(0).Width=   1799
         Groups(2).Columns(0).Caption=   "DValores bajos"
         Groups(2).Columns(0).Name=   "ASC"
         Groups(2).Columns(0).CaptionAlignment=   2
         Groups(2).Columns(0).DataField=   "Column 16"
         Groups(2).Columns(0).DataType=   8
         Groups(2).Columns(0).FieldLen=   256
         Groups(2).Columns(0).Style=   2
         Groups(2).Columns(0).HasHeadForeColor=   -1  'True
         Groups(2).Columns(0).HasHeadBackColor=   -1  'True
         Groups(2).Columns(0).HasBackColor=   -1  'True
         Groups(2).Columns(0).HeadForeColor=   16777215
         Groups(2).Columns(0).HeadBackColor=   32896
         Groups(2).Columns(0).BackColor=   16777215
         Groups(3).Width =   1667
         Groups(3).Caption=   "DPonderaci�n"
         Groups(3).HasHeadForeColor=   -1  'True
         Groups(3).HasHeadBackColor=   -1  'True
         Groups(3).HeadForeColor=   16777215
         Groups(3).HeadBackColor=   32896
         Groups(3).Columns.Count=   5
         Groups(3).Columns(0).Width=   1667
         Groups(3).Columns(0).Name=   "POND"
         Groups(3).Columns(0).DataField=   "Column 17"
         Groups(3).Columns(0).DataType=   8
         Groups(3).Columns(0).FieldLen=   256
         Groups(3).Columns(0).Style=   4
         Groups(3).Columns(0).ButtonsAlways=   -1  'True
         Groups(3).Columns(0).HasHeadForeColor=   -1  'True
         Groups(3).Columns(0).HasHeadBackColor=   -1  'True
         Groups(3).Columns(0).HasForeColor=   -1  'True
         Groups(3).Columns(0).HasBackColor=   -1  'True
         Groups(3).Columns(0).HeadForeColor=   16777215
         Groups(3).Columns(0).HeadBackColor=   32896
         Groups(3).Columns(1).Width=   794
         Groups(3).Columns(1).Visible=   0   'False
         Groups(3).Columns(1).Caption=   "Baja"
         Groups(3).Columns(1).Name=   "BAJA"
         Groups(3).Columns(1).DataField=   "Column 18"
         Groups(3).Columns(1).DataType=   8
         Groups(3).Columns(1).FieldLen=   256
         Groups(3).Columns(2).Width=   582
         Groups(3).Columns(2).Visible=   0   'False
         Groups(3).Columns(2).Caption=   "ValorPond"
         Groups(3).Columns(2).Name=   "TipoPond"
         Groups(3).Columns(2).DataField=   "Column 19"
         Groups(3).Columns(2).DataType=   8
         Groups(3).Columns(2).FieldLen=   256
         Groups(3).Columns(3).Width=   556
         Groups(3).Columns(3).Visible=   0   'False
         Groups(3).Columns(3).Caption=   "FECACT"
         Groups(3).Columns(3).Name=   "FECACT"
         Groups(3).Columns(3).DataField=   "Column 20"
         Groups(3).Columns(3).DataType=   8
         Groups(3).Columns(3).FieldLen=   256
         Groups(3).Columns(4).Width=   503
         Groups(3).Columns(4).Visible=   0   'False
         Groups(3).Columns(4).Caption=   "HIDENDESCR"
         Groups(3).Columns(4).Name=   "HIDENDESCR"
         Groups(3).Columns(4).DataField=   "Column 21"
         Groups(3).Columns(4).DataType=   8
         Groups(3).Columns(4).FieldLen=   256
         Groups(4).Width =   3200
         Groups(4).Caption=   "DValorpordefecto"
         Groups(4).HasHeadForeColor=   -1  'True
         Groups(4).HasHeadBackColor=   -1  'True
         Groups(4).HeadForeColor=   16777215
         Groups(4).HeadBackColor=   32896
         Groups(4).Columns(0).Width=   3201
         Groups(4).Columns(0).Caption=   "VDEFECTO"
         Groups(4).Columns(0).Name=   "VDEFECTO"
         Groups(4).Columns(0).DataField=   "Column 22"
         Groups(4).Columns(0).DataType=   8
         Groups(4).Columns(0).FieldLen=   800
         Groups(4).Columns(0).HasHeadForeColor=   -1  'True
         Groups(4).Columns(0).HasHeadBackColor=   -1  'True
         Groups(4).Columns(0).HasBackColor=   -1  'True
         Groups(4).Columns(0).HeadForeColor=   32896
         Groups(4).Columns(0).HeadBackColor=   32896
         Groups(4).Columns(0).BackColor=   16777215
         Groups(5).Width =   3200
         Groups(5).Caption=   "UON"
         Groups(5).HasHeadForeColor=   -1  'True
         Groups(5).HasHeadBackColor=   -1  'True
         Groups(5).HeadForeColor=   16777215
         Groups(5).HeadBackColor=   32896
         Groups(5).Columns(0).Width=   3201
         Groups(5).Columns(0).Name=   "UONS"
         Groups(5).Columns(0).CaptionAlignment=   2
         Groups(5).Columns(0).DataField=   "Column 23"
         Groups(5).Columns(0).DataType=   8
         Groups(5).Columns(0).FieldLen=   256
         Groups(5).Columns(0).Style=   4
         Groups(5).Columns(0).ButtonsAlways=   -1  'True
         Groups(5).Columns(0).HasHeadForeColor=   -1  'True
         Groups(5).Columns(0).HasHeadBackColor=   -1  'True
         Groups(5).Columns(0).HasForeColor=   -1  'True
         Groups(5).Columns(0).HasBackColor=   -1  'True
         Groups(5).Columns(0).HeadForeColor=   16777215
         Groups(5).Columns(0).HeadBackColor=   32896
         Groups(5).Columns(0).ForeColor=   16777215
         _ExtentX        =   17224
         _ExtentY        =   7567
         _StockProps     =   79
         BackColor       =   -2147483648
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame frmAmbito 
         Caption         =   "D�mbito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   6960
         TabIndex        =   63
         Top             =   5400
         Width           =   3180
         Begin VB.CheckBox chkAmbitoArticulo 
            Caption         =   "DArticulo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   360
            TabIndex        =   66
            Top             =   960
            Width           =   1725
         End
         Begin VB.CheckBox chkAmbitoMaterial 
            Caption         =   "DMaterial"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   345
            TabIndex        =   65
            Top             =   580
            Width           =   1725
         End
         Begin VB.CheckBox chkAmbitoUon 
            Caption         =   "DUnidad Organizativa"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   360
            TabIndex        =   64
            Top             =   240
            UseMaskColor    =   -1  'True
            Width           =   2565
         End
      End
      Begin VB.PictureBox Picture1 
         Height          =   1335
         Left            =   120
         ScaleHeight     =   1275
         ScaleWidth      =   6555
         TabIndex        =   56
         Top             =   1920
         Width           =   6615
         Begin VB.OptionButton chkUonContenida 
            Caption         =   "DMostrar s�lo atributos relacionados con la unidad organizativa o contenida"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   62
            Top             =   960
            Value           =   -1  'True
            Width           =   6255
         End
         Begin VB.OptionButton chkUonCoincidente 
            Caption         =   "DMostrar s�lo atributos relacionados directamente con la unidad organizativa"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   120
            TabIndex        =   61
            Top             =   600
            Width           =   6255
         End
         Begin VB.TextBox txtUonSeleccionada 
            BackColor       =   &H80000018&
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            MaxLength       =   100
            TabIndex        =   59
            Top             =   120
            Width           =   4215
         End
         Begin VB.CommandButton cmdBuscarUon 
            Height          =   285
            Left            =   6120
            Picture         =   "ctlATRIB.ctx":0600
            Style           =   1  'Graphical
            TabIndex        =   58
            Top             =   120
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarUon 
            Height          =   285
            Left            =   5760
            Picture         =   "ctlATRIB.ctx":0982
            Style           =   1  'Graphical
            TabIndex        =   57
            Top             =   120
            Width           =   315
         End
         Begin VB.Label lblUons 
            Caption         =   "dUn. org.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   60
            Top             =   120
            Width           =   1215
         End
      End
      Begin VB.Frame fraCosteDtos 
         Caption         =   "DCostes/Desc."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1305
         Left            =   8700
         TabIndex        =   52
         Top             =   480
         Width           =   1500
         Begin VB.CheckBox chkOtros 
            Caption         =   "DOtros"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   180
            TabIndex        =   55
            Top             =   990
            UseMaskColor    =   -1  'True
            Width           =   1275
         End
         Begin VB.CheckBox chkDtos 
            Caption         =   "DDescuentos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   180
            TabIndex        =   54
            Top             =   615
            Width           =   1275
         End
         Begin VB.CheckBox chkCostes 
            Caption         =   "DCostes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   180
            TabIndex        =   53
            Top             =   265
            UseMaskColor    =   -1  'True
            Width           =   1275
         End
      End
      Begin VB.Frame frmTipoAtributo 
         Caption         =   "DTipo de Atributo"
         Height          =   1305
         Left            =   6960
         TabIndex        =   47
         Top             =   480
         Width           =   1605
         Begin VB.OptionButton Option8 
            Caption         =   "DEspecificaci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   180
            TabIndex        =   50
            Top             =   1000
            Width           =   1395
         End
         Begin VB.OptionButton Option7 
            Caption         =   "Oferta"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   165
            TabIndex        =   49
            Top             =   650
            Width           =   1395
         End
         Begin VB.OptionButton Option6 
            Caption         =   "DTodos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   180
            TabIndex        =   48
            Top             =   300
            Value           =   -1  'True
            Width           =   1395
         End
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddTipo 
         Height          =   975
         Left            =   -69975
         TabIndex        =   46
         Top             =   2040
         Width           =   1815
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         MaxDropDownItems=   4
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "ctlATRIB.ctx":0EC4
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "NUM"
         Columns(0).Name =   "NUM"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "TIPO"
         Columns(1).Name =   "TIPO"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3201
         _ExtentY        =   1720
         _StockProps     =   77
         BackColor       =   8421376
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame frmArticulo 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   120
         TabIndex        =   41
         Top             =   5640
         Width           =   6615
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
            Height          =   285
            Left            =   1110
            TabIndex        =   42
            Top             =   345
            Width           =   1335
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   6
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "GMN1"
            Columns(2).Name =   "GMN1"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "GMN2"
            Columns(3).Name =   "GMN2"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "GMN3"
            Columns(4).Name =   "GMN3"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "GMN4"
            Columns(5).Name =   "GMN4"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            _ExtentX        =   2355
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
            Height          =   285
            Left            =   2475
            TabIndex        =   43
            Top             =   345
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   6
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "GMN1"
            Columns(2).Name =   "GMN1"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "GMN2"
            Columns(3).Name =   "GMN2"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "GMN3"
            Columns(4).Name =   "GMN3"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "GMN4"
            Columns(5).Name =   "GMN4"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblArtiCod 
            Caption         =   "DArt�culo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   225
            TabIndex        =   44
            Top             =   360
            Width           =   825
         End
      End
      Begin VB.Frame frmMat 
         Caption         =   "DMaterial"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Left            =   120
         TabIndex        =   27
         Top             =   3360
         Width           =   6570
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5955
            Picture         =   "ctlATRIB.ctx":0EE0
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   135
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4CodCtrl 
            Height          =   285
            Left            =   1365
            TabIndex        =   29
            Top             =   480
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4CodCtrl 
            Height          =   285
            Left            =   1365
            TabIndex        =   30
            Top             =   900
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4CodCtrl 
            Height          =   285
            Left            =   1365
            TabIndex        =   31
            Top             =   1320
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4CodCtrl 
            Height          =   285
            Left            =   1365
            TabIndex        =   32
            Top             =   1740
            Width           =   1035
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1826
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4DenCtrl 
            Height          =   285
            Left            =   2475
            TabIndex        =   33
            Top             =   900
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4DenCtrl 
            Height          =   285
            Left            =   2475
            TabIndex        =   34
            Top             =   1320
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4DenCtrl 
            Height          =   285
            Left            =   2475
            TabIndex        =   35
            Top             =   1740
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4DenCtrl 
            Height          =   285
            Left            =   2475
            TabIndex        =   36
            Top             =   480
            Width           =   3825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6747
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblGMN4_4 
            Caption         =   "DGrupo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   40
            Top             =   1800
            Width           =   1560
         End
         Begin VB.Label lblGMN1_4 
            Caption         =   "DCommodity:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   39
            Top             =   540
            Width           =   1560
         End
         Begin VB.Label lblGMN2_4 
            Caption         =   "DFamilia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   38
            Top             =   960
            Width           =   1560
         End
         Begin VB.Label lblGMN3_4 
            Caption         =   "DSubfamilia:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   37
            Top             =   1380
            Width           =   1560
         End
      End
      Begin VB.Frame fraAtributo 
         Caption         =   "DAtributo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1305
         Left            =   150
         TabIndex        =   22
         Top             =   480
         Width           =   6570
         Begin VB.TextBox txtDen 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1365
            TabIndex        =   24
            Top             =   765
            Width           =   4890
         End
         Begin VB.TextBox txtCodCtrl 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1365
            TabIndex        =   23
            Top             =   270
            Width           =   1335
         End
         Begin VB.Label lblDen 
            Caption         =   "DDenominaci�n:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   210
            TabIndex        =   26
            Top             =   825
            Width           =   1095
         End
         Begin VB.Label lblCod 
            Caption         =   "DC�digo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   225
            TabIndex        =   25
            Top             =   300
            Width           =   840
         End
      End
      Begin VB.PictureBox picEdit 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   -74925
         ScaleHeight     =   495
         ScaleWidth      =   9795
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   4860
         Visible         =   0   'False
         Width           =   9795
         Begin VB.CommandButton cmdAnyadir 
            Caption         =   "&A�adir"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   75
            Width           =   1005
         End
         Begin VB.CommandButton cmdModoEdicionCtrl 
            Caption         =   "&Edici�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   8760
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   75
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminar 
            Caption         =   "&Eliminar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1065
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   75
            Width           =   1005
         End
         Begin VB.CommandButton cmdCodigo 
            Caption         =   "&C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2130
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   75
            Width           =   1005
         End
         Begin VB.CommandButton cmdDeshacer 
            Caption         =   "&Deshacer"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4260
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   75
            Width           =   1005
         End
         Begin VB.CommandButton cmdBajaLog 
            Caption         =   "&Baja l�gica"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3195
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   75
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigate 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   0  'None
         FontTransparent =   0   'False
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   -74925
         ScaleHeight     =   495
         ScaleWidth      =   9570
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   4860
         Width           =   9570
         Begin VB.CommandButton cmdModoEdicion2Ctrl 
            Caption         =   "&DEdicion"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   8550
            TabIndex        =   45
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdSeleccionarCtrl 
            Caption         =   "&DSeleccionar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2115
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   60
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdActualizar 
            Caption         =   "&DRestaurar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "&DListado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1065
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.Frame frmTipo 
         Caption         =   "DTipo de datos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2310
         Left            =   6930
         TabIndex        =   4
         Top             =   1860
         Width           =   3240
         Begin VB.OptionButton optTipo 
            Caption         =   "DArchivo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   330
            TabIndex        =   67
            Top             =   2010
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DTodos"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   330
            TabIndex        =   9
            Top             =   300
            Value           =   -1  'True
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DNum�rico"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   330
            TabIndex        =   8
            Top             =   650
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DFecha"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   330
            TabIndex        =   7
            Top             =   1000
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DTexto"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   330
            TabIndex        =   6
            Top             =   1350
            Width           =   2055
         End
         Begin VB.OptionButton optTipo 
            Caption         =   "DS�/No"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   330
            TabIndex        =   5
            Top             =   1680
            Width           =   2055
         End
      End
      Begin VB.Frame frmIntro 
         Caption         =   "Introducci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   6960
         TabIndex        =   1
         Top             =   4230
         Width           =   3180
         Begin VB.CheckBox chkLibre 
            Caption         =   "DLibre"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   345
            TabIndex        =   3
            Top             =   265
            UseMaskColor    =   -1  'True
            Width           =   1845
         End
         Begin VB.CheckBox chkSeleccion 
            Caption         =   "DSelecci�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   345
            TabIndex        =   2
            Top             =   580
            Width           =   1725
         End
      End
      Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
         Height          =   915
         Left            =   -72600
         TabIndex        =   51
         Top             =   120
         Width           =   2025
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "ctlATRIB.ctx":0F4C
         DividerStyle    =   3
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   370
         ExtraHeight     =   265
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Name =   "VALOR"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DESC"
         Columns(1).Name =   "DESC"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3572
         _ExtentY        =   1614
         _StockProps     =   77
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "ctlATRIB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'Valor m�ximo de NVarcha(MAX)
Private Const cLongAtribEsp_Texto As Long = 32471

Private oOrigen As Object ' para guardar el formulario origen
Private haySeleccion As Integer ' 1 si, 2 no, 0 deshabilitado
Private m_oContainer As Form

'Variable de control de flujo de proceso
Public Accion As AccionesSummit
Public g_oOrigen As Form
'Articulo seleccionado
Public g_oArtiSeleccionado As CArticulo
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
Public g_idAtribCod As Variant
Public g_GMN1RespetarCombo As Boolean

Public g_sOrigen As String
Public g_bEdicion As Boolean
Public g_bModError As Boolean
Public g_bAnyaError As Boolean
'3328
Public g_sPedId As String

Public g_sGmn1 As String
Public g_sGmn2 As String
Public g_sGmn3 As String
Public g_sGmn4 As String
Public g_sArtCod As String

Private m_bPrimera As Boolean
Private m_bTodas As Boolean
Private m_oAtributo As CAtributo
Private m_oAtributos As CAtributos

Public g_oAtributoEnEdicion As CAtributo
Private m_oAtributoAnyadir As CAtributo
Private m_oIBAseDatosEnEdicion As IBaseDatos

Private m_bRespetarCombo As Boolean
Private m_bCargarComboDesde As Boolean
Private m_bRespetarCheck As Boolean
Private m_bCargando As Boolean

'Variables para materiales
Private m_oGruposMN1 As CGruposMatNivel1
Private m_oGruposMN2 As CGruposMatNivel2
Private m_oGruposMN3 As CGruposMatNivel3
Private m_oGruposMN4 As CGruposMatNivel4

Private m_oGMN1Seleccionado As CGrupoMatNivel1
Private m_oGMN2Seleccionado As CGrupoMatNivel2
Private m_oGMN3Seleccionado As CGrupoMatNivel3
Private m_oGMN4Seleccionado As CGrupoMatNivel4

Private m_GMN1CargarComboDesde As Boolean
Private m_GMN2CargarComboDesde As Boolean
Public g_GMN2RespetarCombo As Boolean
Private m_GMN3CargarComboDesde As Boolean
Public g_GMN3RespetarCombo As Boolean
Private m_GMN4CargarComboDesde As Boolean
Public g_GMN4RespetarCombo As Boolean
Public Adores As Ador.Recordset

Private m_sMensajes(18) As String  'literales para basmensajes
Private m_sCombo(1 To 5) As String
Private m_sIdioma(1 To 2) As String
Private m_sIdiMenAtributo As String
Private m_sIdiAtributo As String
Private m_sItem As String

Private m_bBajasLog As Boolean
Private m_bModifAtri As Boolean
Private m_bModifCodigo As Boolean
Private m_bError As Boolean
Private m_bNoCambio As Boolean

Private m_sIdiTrue As String
Private m_sIdiFalse As String

Public m_bRespuesta As Boolean
Private m_sFormularios As String
Private m_sProcesos As String
Private m_sPedidos As String
Private m_sApertura As String
Private m_bCambioSeleccion As Boolean

Public bRComprador As Boolean
Public sAmbitoAtribEsp As String
Public iAmbitoAtribEsp As TipoAmbitoProceso
Public g_bSoloSeleccion As Boolean

Private m_bClickEnListaCombo As Boolean
Private m_sMaestroAtributos As String

'seguridad
Private m_bRestrMantAtribUsu As Boolean
Private m_bRestrMantAtribPerf As Boolean

Private m_oUonsSeleccionadas As CUnidadesOrganizativas

''' --------------------------------ATRIBUTOS ----------------------------------

Public Property Get sstabGeneral() As SSTab
    Set sstabGeneral = sstabgeneralCtrl
End Property

Public Property Get sdbgAtributosGrupo() As SSDBGrid
    Set sdbgAtributosGrupo = sdbgAtributosGrupoCtrl
End Property

Public Property Get sdbcGMN1_4Cod() As SSDBCombo
    Set sdbcGMN1_4Cod = sdbcGMN1_4CodCtrl
End Property

Public Property Get sdbcGMN1_4Den() As SSDBCombo
    Set sdbcGMN1_4Den = sdbcGMN1_4DenCtrl
End Property

Public Property Get sdbcGMN2_4Cod() As SSDBCombo
    Set sdbcGMN2_4Cod = sdbcGMN2_4CodCtrl
End Property

Public Property Get sdbcGMN2_4Den() As SSDBCombo
    Set sdbcGMN2_4Den = sdbcGMN2_4DenCtrl
End Property

Public Property Get sdbcGMN3_4Cod() As SSDBCombo
    Set sdbcGMN3_4Cod = sdbcGMN3_4CodCtrl
End Property

Public Property Get sdbcGMN3_4Den() As SSDBCombo
    Set sdbcGMN3_4Den = sdbcGMN3_4DenCtrl
End Property

Public Property Get sdbcGMN4_4Cod() As SSDBCombo
    Set sdbcGMN4_4Cod = sdbcGMN4_4CodCtrl
End Property

Public Property Get sdbcGMN4_4Den() As SSDBCombo
    Set sdbcGMN4_4Den = sdbcGMN4_4DenCtrl
End Property

Public Property Get cmdSeleccionar() As CommandButton
    Set cmdSeleccionar = cmdSeleccionarCtrl
End Property

Public Property Get cmdModoEdicion() As CommandButton
    Set cmdModoEdicion = cmdModoEdicionCtrl
End Property

Public Property Get cmdModoEdicion2() As CommandButton
    Set cmdModoEdicion2 = cmdModoEdicion2Ctrl
End Property

Public Property Get txtCod() As TextBox
    Set txtCod = txtCodCtrl
End Property

Public Property Get optOption1() As OptionButton
    Set optOption1 = optTipo(0)
End Property

Public Property Get optOption2() As OptionButton
    Set optOption2 = optTipo(1)
End Property

Public Property Get optOption3() As OptionButton
    Set optOption3 = optTipo(2)
End Property

Public Property Get optOption4() As OptionButton
    Set optOption4 = optTipo(3)
End Property

Public Property Get optOption5() As OptionButton
    Set optOption5 = optTipo(4)
End Property

Public Property Get Costes() As CheckBox
    Set Costes = chkCostes
End Property

Public Property Get Dtos() As CheckBox
    Set Dtos = chkDtos
End Property

Public Property Get fraTipo() As Frame
    Set fraTipo = frmTipo
End Property

Public Property Get AtributoSeleccionado() As CAtributo
    Set AtributoSeleccionado = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value)
End Property

''' ----------------- FIN ATRIBUTOS ----------------------------------

''' <summary>Establece cual es el formulario contenerdor del UserControl</summary>
''' <remarks>Llamada desde: frmAtrib_Load</remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Public Sub SetContainer(ByRef oForm As Form)
    Set m_oContainer = oForm
End Sub

Private Sub chkCostes_Click()
    If g_sOrigen = "frmCatalogoCostesDescuentos_Costes" Or g_sOrigen = "frmCatalogo_Costes" Then
        chkCostes.Value = vbChecked
    ElseIf g_sOrigen = "frmCatalogoCostesDescuentos_Descuentos" Or g_sOrigen = "frmCatalogo_Descuentos" Then
        chkCostes.Value = vbUnchecked
    End If
End Sub

Private Sub chkDtos_Click()
    If g_sOrigen = "frmCatalogoCostesDescuentos_Descuentos" Or g_sOrigen = "frmCatalogo_Descuentos" Then
        chkDtos.Value = vbChecked
    ElseIf g_sOrigen = "frmCatalogoCostesDescuentos_Costes" Or g_sOrigen = "frmCatalogo_Costes" Then
        chkDtos.Value = vbUnchecked
    End If
End Sub

Private Sub chkLibre_Click()
    If chkSeleccion.Value = vbChecked And chkLibre.Value = vbChecked Then
        chkSeleccion.Value = vbUnchecked
    End If
End Sub

Private Sub chkOtros_Click()
    If g_sOrigen = "frmCatalogoCostesDescuentos_Descuentos" Or g_sOrigen = "frmCatalogo_Descuentos" Or g_sOrigen = "frmCatalogoCostesDescuentos_Costes" Or g_sOrigen = "frmCatalogo_Costes" Then
        chkOtros.Value = vbUnchecked
    End If
End Sub

Private Sub chkSeleccion_Click()
    If chkSeleccion.Value = vbChecked And chkLibre.Value = vbChecked Then
        chkLibre.Value = vbUnchecked
    End If
End Sub

Private Sub cmdActualizar_Click()
''' * Objetivo: Restaurar el contenido de la grid
''' * Objetivo: desde la base de datos
    Screen.MousePointer = vbHourglass
    CargarAtributos
    g_bEdicion = False
    Screen.MousePointer = vbNormal
End Sub

Public Sub cmdAnyadir_Click()
    sdbddTipo.Enabled = True
    cmdEliminar.Enabled = False
    cmdCodigo.Enabled = False
    cmdAnyadir.Enabled = False
    cmdBajaLog.Enabled = False
    cmdDeshacer.Enabled = True

    Accion = ACCAtributosAnya
    sdbgAtributosGrupoCtrl.Scroll 0, sdbgAtributosGrupoCtrl.Rows - sdbgAtributosGrupoCtrl.Row
    
    If sdbgAtributosGrupoCtrl.VisibleRows > 0 Then
        
        If sdbgAtributosGrupoCtrl.VisibleRows >= sdbgAtributosGrupoCtrl.Rows Then
            If sdbgAtributosGrupoCtrl.VisibleRows = sdbgAtributosGrupoCtrl.Rows Then
                sdbgAtributosGrupoCtrl.Row = sdbgAtributosGrupoCtrl.Rows - 1
            Else
                sdbgAtributosGrupoCtrl.Row = sdbgAtributosGrupoCtrl.Rows
    
            End If
        Else
            sdbgAtributosGrupoCtrl.Row = sdbgAtributosGrupoCtrl.Rows - (sdbgAtributosGrupoCtrl.Rows - sdbgAtributosGrupoCtrl.VisibleRows) - 1
        End If
        
    End If
    
    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
End Sub

Private Sub cmdBajaLog_Click()
Dim teserror As TipoErrorSummit

    If sdbgAtributosGrupoCtrl.Rows = 0 Then Exit Sub
    
    If cmdBajaLog.caption = m_sIdioma(2) Then
        teserror = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).DarBajaLogica
    Else
        teserror = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).DeshacerBajaLogica
    End If
    If teserror.NumError <> TESnoerror Then
        TratarError teserror
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    'sdbgAtributosGrupoCtrl.Columns("FECACT").Value = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).FECACT
    sdbgAtributosGrupoCtrl.Columns("BAJA").Value = BooleanToSQLBinary(m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).BajaLogica)
    sdbgAtributosGrupoCtrl.Update
End Sub



''' <summary>
''' Cambiar de codigo del atributo actual
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdCodigo_Click()
    
    Dim teserror As TipoErrorSummit
     
    If sdbgAtributosGrupoCtrl.Rows = 0 Then Exit Sub
    If IsNull(sdbgAtributosGrupoCtrl.Bookmark) Then Exit Sub
    If sdbgAtributosGrupoCtrl.Columns("COD").Value = "" Then Exit Sub
    If sdbgAtributosGrupoCtrl.Columns("ID").Value = "" Then Exit Sub
    
    sdbgAtributosGrupoCtrl.SelBookmarks.Add sdbgAtributosGrupoCtrl.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    
    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
    g_oAtributoEnEdicion.FECACT = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value)).FECACT
    Set m_oIBAseDatosEnEdicion = g_oAtributoEnEdicion
     
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
        Exit Sub
    End If
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
    frmMODCOD.caption = m_sIdiMenAtributo
    frmMODCOD.Left = m_oContainer.Left + 500
    frmMODCOD.Top = m_oContainer.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = 20
    frmMODCOD.txtCodAct.Text = g_oAtributoEnEdicion.Cod
    Set frmMODCOD.fOrigen = m_oContainer
'    If g_sOrigen = "MDI" Then
'        Set frmMODCOD.fOrigen = ctlAtrib
'    Else
'        If g_sOrigen = "frmPROCE" Then
'            Set frmMODCOD.fOrigen = frmPROCE.g_ofrmATRIB
'            Unload frmATRIB
'        Else
'            If g_sOrigen = "PLANTILLAS" Then
'
'            Else
'                Set frmMODCOD.fOrigen = frmESTRMATAtrib.g_ofrmATRIB
'                Unload frmATRIB
'            End If
'        End If
'    End If
    g_bCodigoCancelar = False
    frmMODCOD.Show 1
    DoEvents
    
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If Trim(g_sCodigoNuevo) = "" Then
        oMensajes.NoValido sdbgAtributosGrupoCtrl.Columns("COD").caption
        Set m_oIBAseDatosEnEdicion = Nothing
        Set g_oAtributoEnEdicion = Nothing
        Exit Sub
    End If
        
    If NombreDeAtributoValido(g_sCodigoNuevo) = False Then
        oMensajes.CodigoAtributoNoValido sdbgAtributosGrupoCtrl.Columns("COD").caption
        Set m_oIBAseDatosEnEdicion = Nothing
        Set g_oAtributoEnEdicion = Nothing
        Exit Sub
    End If
    
    If UCase(g_sCodigoNuevo) = UCase(g_oAtributoEnEdicion.Cod) Then
        oMensajes.NoValido sdbgAtributosGrupoCtrl.Columns("COD").caption
        Set m_oIBAseDatosEnEdicion = Nothing
        Set g_oAtributoEnEdicion = Nothing
        Exit Sub
    End If
    
    ''' Cambiar el codigo
    
    Screen.MousePointer = vbHourglass
    
    teserror = m_oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
    
    Screen.MousePointer = vbNormal
    
             
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
    
    ''' Actualizar los datos
    
    m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value)).Cod = g_sCodigoNuevo
    sdbgAtributosGrupoCtrl.Columns("COD").Value = g_sCodigoNuevo
    'sdbgAtributosGrupoCtrl.Columns("FECACT").Value = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value)).FECACT
    
    
    Set m_oIBAseDatosEnEdicion = Nothing
    Set g_oAtributoEnEdicion = Nothing

    sdbgAtributosGrupoCtrl.MoveFirst
    sdbgAtributosGrupoCtrl.Refresh
    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(0)
    sdbgAtributosGrupoCtrl.SelBookmarks.RemoveAll
        
    
End Sub

''' <summary>
''' Deshacer la edicion en el Atributo actual
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub cmdDeshacer_Click()
    
sdbgAtributosGrupoCtrl.CancelUpdate
sdbgAtributosGrupoCtrl.DataChanged = False
    
If Not g_oAtributoEnEdicion Is Nothing Then

    Set m_oIBAseDatosEnEdicion = Nothing
    Set g_oAtributoEnEdicion = Nothing
End If

If g_bEdicion Then
    sdbgAtributosGrupoCtrl.Columns("OFERTA").Locked = False
    sdbgAtributosGrupoCtrl.Columns("COSTE").Locked = False
    sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Locked = False
    sdbgAtributosGrupoCtrl.Columns("NOMBRE").Locked = False
    sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = True
    sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = False
    sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = False
    sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = False
    sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = False
    sdbgAtributosGrupoCtrl.Columns("ASC").Locked = False
    sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = False
End If

cmdAnyadir.Enabled = True
cmdEliminar.Enabled = True
cmdDeshacer.Enabled = False
cmdCodigo.Enabled = True

If sdbgAtributosGrupoCtrl.Rows = 1 Then
    sdbgAtributosGrupoCtrl.MoveFirst
    sdbgAtributosGrupoCtrl.Refresh
    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(0)
    sdbgAtributosGrupoCtrl.SelBookmarks.RemoveAll
End If
End Sub

Private Sub cmdEliminar_Click()
'******************************************************************
'*** Descripci�n: Elimina los atributos seleccionados con opcion  ***
'***                    de eliminar varios a la vez              ***
'******************************************************************
Dim aIdentificadores As Variant
Dim aBookmarks As Variant
Dim aAux As Variant
Dim iIndice As Integer
Dim inum As Integer
Dim i As Integer
Dim irespuesta As Integer
Dim udtTeserror As TipoErrorSummit
Dim vbm As Variant
  
If sdbgAtributosGrupoCtrl.Rows = 0 Then Exit Sub
    
Screen.MousePointer = vbHourglass

If sdbgAtributosGrupoCtrl.SelBookmarks.Count = 0 Then

    vbm = sdbgAtributosGrupoCtrl.GetBookmark(0)
    sdbgAtributosGrupoCtrl.Bookmark = vbm
    sdbgAtributosGrupoCtrl.SelBookmarks.Add sdbgAtributosGrupoCtrl.Bookmark
End If



Select Case sdbgAtributosGrupoCtrl.SelBookmarks.Count

    Case 0
        Screen.MousePointer = vbNormal
        Exit Sub
    Case 1
      '  sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks.Item(0)
        irespuesta = oMensajes.PreguntaEliminar(m_sIdiMenAtributo & " " & sdbgAtributosGrupoCtrl.Columns("COD").Value & " (" & sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value & ")")

        If irespuesta = vbNo Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If

        ReDim aIdentificadores(1)
        aIdentificadores(1) = sdbgAtributosGrupoCtrl.Columns("ID").Value
        If m_oAtributos.ExistenValoresDeAtributos(aIdentificadores) Then
            irespuesta = oMensajes.PreguntaDesasignarMaterialProve(4)
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If

'        sdbgAtributosGrupoCtrl.SelBookmarks.Add sdbgAtributosGrupoCtrl.Bookmark

        Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))

        Set m_oIBAseDatosEnEdicion = g_oAtributoEnEdicion
        udtTeserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos()

        If udtTeserror.NumError <> TESnoerror Then
            TratarError udtTeserror
            Screen.MousePointer = vbNormal
            
            If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
            Exit Sub

        Else
            'sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks.Item(0)
            m_oAtributos.Remove (CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
            If sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.Bookmark) > -1 Then
                sdbgAtributosGrupoCtrl.RemoveItem (sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.Bookmark))
            Else
                sdbgAtributosGrupoCtrl.RemoveItem (0)
            End If
            If IsEmpty(sdbgAtributosGrupoCtrl.GetBookmark(0)) Then
                sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.GetBookmark(-1)
            Else
                sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.GetBookmark(0)
            End If
        End If

        Set m_oIBAseDatosEnEdicion = Nothing
        Set g_oAtributoEnEdicion = Nothing
    
    Case Is > 1
    
    
        irespuesta = oMensajes.PreguntaEliminarMonedas(m_sIdiAtributo)
     
        If irespuesta = vbNo Then
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
            
        ReDim aIdentificadores(sdbgAtributosGrupoCtrl.SelBookmarks.Count)
        ReDim aBookmarks(sdbgAtributosGrupoCtrl.SelBookmarks.Count)
        
        i = 0
       
        While i < sdbgAtributosGrupoCtrl.SelBookmarks.Count
            sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(i)
            aIdentificadores(i + 1) = sdbgAtributosGrupoCtrl.Columns("ID").Value
            aBookmarks(i + 1) = sdbgAtributosGrupoCtrl.SelBookmarks(i)
            i = i + 1
        Wend
        If m_oAtributos.ExistenValoresDeAtributos(aIdentificadores) Then
            irespuesta = oMensajes.PreguntaDesasignarMaterialProve(5)
            If irespuesta = vbNo Then
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        End If
            
        udtTeserror = m_oAtributos.EliminacionMultiple(aIdentificadores)
        If udtTeserror.NumError <> TESnoerror Then
          If udtTeserror.NumError = TESImposibleEliminar Then
              iIndice = 1
              aAux = udtTeserror.Arg1
              inum = UBound(udtTeserror.Arg1, 2)
              
              For i = 0 To inum
                  udtTeserror.Arg1(2, i) = m_oAtributos.Item(sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.Bookmark)).Den
                  sdbgAtributosGrupoCtrl.SelBookmarks.Remove (aAux(2, i) - iIndice)
                  iIndice = iIndice + 1
              Next
                
              oMensajes.ImposibleEliminacionMultiple 391, udtTeserror.Arg1
              If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                  For i = 0 To sdbgAtributosGrupoCtrl.SelBookmarks.Count - 1
                       sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks.Item(i)
                       m_oAtributos.Remove (CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                  Next
                  sdbgAtributosGrupoCtrl.DeleteSelected
              End If
          Else
              TratarError udtTeserror
              Screen.MousePointer = vbNormal
              Exit Sub
          End If
        Else
            For i = 0 To sdbgAtributosGrupoCtrl.SelBookmarks.Count - 1
                 m_oAtributos.Remove (CStr(sdbgAtributosGrupoCtrl.Columns("ID").CellValue(sdbgAtributosGrupoCtrl.SelBookmarks(i))))
            Next
            sdbgAtributosGrupoCtrl.DeleteSelected
            DoEvents
        End If
End Select
    
sdbgAtributosGrupoCtrl.SelBookmarks.RemoveAll


If sdbgAtributosGrupoCtrl.Rows = 0 Then
    cmdEliminar.Enabled = False
Else
    cmdEliminar.Enabled = True
End If

Screen.MousePointer = vbNormal
Accion = ACCAtributosMod

End Sub



Private Sub cmdLimpiarUon_Click()
    m_oContainer.UonsSeleccionadas.clear
    txtUonSeleccionada.Text = ""
End Sub

Private Sub cmdlistado_Click()
    frmLstAtributos.WindowState = vbNormal
    
    'Rellena por defecto con la misma selecci�n de este formulario:
    frmLstAtributos.g_sOrigen = g_sOrigen
    frmLstAtributos.m_bRComprador = bRComprador
    frmLstAtributos.txtCod.Text = txtCodCtrl.Text
    frmLstAtributos.txtDen.Text = txtDen.Text
    frmLstAtributos.chkLibre.Value = chkLibre.Value
    frmLstAtributos.chkSeleccion.Value = chkSeleccion.Value
    frmLstAtributos.optTipo(0).Value = optTipo(0).Value
    frmLstAtributos.optTipo(1).Value = optTipo(1).Value
    frmLstAtributos.optTipo(2).Value = optTipo(2).Value
    frmLstAtributos.optTipo(3).Value = optTipo(3).Value
    frmLstAtributos.optTipo(4).Value = optTipo(4).Value
    frmLstAtributos.optTipo(5).Value = optTipo(5).Value
    frmLstAtributos.chkAmbitoUon = chkAmbitoUon.Value
    frmLstAtributos.chkAmbitoMaterial = chkAmbitoMaterial.Value
    frmLstAtributos.chkAmbitoArticulo = chkAmbitoArticulo.Value
    frmLstAtributos.g_sGmn1 = g_sGmn1
    frmLstAtributos.g_sGmn2 = g_sGmn2
    frmLstAtributos.g_sGmn3 = g_sGmn3
    frmLstAtributos.g_sGmn4 = g_sGmn4
    frmLstAtributos.g_sArtCod = g_sArtCod
    Set frmLstAtributos.UonsSeleccionadas = m_oUonsSeleccionadas
    If Not m_oGMN1Seleccionado Is Nothing Then
        Set frmLstAtributos.m_oGMN1Seleccionado = m_oGMN1Seleccionado
        frmLstAtributos.sdbcGMN1_4Cod.Text = sdbcGMN1_4CodCtrl.Text
        frmLstAtributos.sdbcGMN1_4Cod_Validate False
    End If
    If Not m_oGMN2Seleccionado Is Nothing Then
        Set frmLstAtributos.m_oGMN2Seleccionado = m_oGMN2Seleccionado
        frmLstAtributos.sdbcGMN2_4Cod.Text = sdbcGMN2_4CodCtrl.Text
        frmLstAtributos.sdbcGMN2_4Cod_Validate False
    End If
    If Not m_oGMN3Seleccionado Is Nothing Then
        Set frmLstAtributos.m_oGMN3Seleccionado = m_oGMN3Seleccionado
        frmLstAtributos.sdbcGMN3_4Cod.Text = sdbcGMN3_4CodCtrl.Text
        frmLstAtributos.sdbcGMN3_4Cod_Validate False
    End If
    If Not m_oGMN4Seleccionado Is Nothing Then
        Set frmLstAtributos.m_oGMN4Seleccionado = m_oGMN4Seleccionado
        frmLstAtributos.sdbcGMN4_4Cod.Text = sdbcGMN4_4CodCtrl.Text
        frmLstAtributos.sdbcGMN4_4Cod_Validate False
    End If
    
    frmLstAtributos.sdbcArtiCod.Text = sdbcArtiCod.Text
    frmLstAtributos.sdbcArtiCod_Validate False
    
    frmLstAtributos.Show vbModal

End Sub

''' <summary>Evento click bot�n de intercambir modo edici�n / modo consulta</summary>
''' <remarks>Llamada desde:sistema; Tiempo m�ximo:0,1</remarks>
Private Sub cmdModoEdicionCtrl_Click()
    If Not g_bEdicion Then
    
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = 0
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Style = ssStyleEdit
                
        sdbgAtributosGrupoCtrl.AllowAddNew = True
        If g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS" Or g_sOrigen = "GMN4" Or g_sOrigen = "ART4" Or g_sOrigen = "frmPEDIDOSOE" Or g_sOrigen = "frmPEDIDOSLP" Then
             sdbgAtributosGrupoCtrl.SelectTypeRow = ssSelectionTypeMultiSelectRange
        Else
            sdbgAtributosGrupoCtrl.SelectTypeRow = ssSelectionTypeSingleSelect
        End If
        picNavigate.Visible = False
        picEdit.Visible = True
        If sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "0" Then
            sdbgAtributosGrupoCtrl.Columns("OFERTA").Locked = False
            sdbgAtributosGrupoCtrl.Columns("COSTE").Locked = False
            sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Locked = False
            sdbgAtributosGrupoCtrl.Columns("NOMBRE").Locked = False
            sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = True
            If sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value <> TipoArchivo Then
                sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = False
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = False
                sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = False
                sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = False
                sdbgAtributosGrupoCtrl.Columns("ASC").Locked = False
                sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = False
            End If
            cmdBajaLog.caption = m_sIdioma(2)
        Else
            cmdBajaLog.caption = m_sIdioma(1)
        End If
        g_bEdicion = True
    
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
        If sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "0" Then
            cmdBajaLog.Enabled = True
        Else
            cmdBajaLog.Enabled = False
        End If
        
    Else
        If sdbgAtributosGrupoCtrl.DataChanged = True Then
            sdbgAtributosGrupoCtrl.Update
            If m_bError Then
                Exit Sub
            End If
    
        End If
        
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = 0
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Style = ssStyleEdit

        sdbgAtributosGrupoCtrl.AllowAddNew = False
        If g_sOrigen = "frmPROCE" Or g_sOrigen = "PLANTILLAS" Or g_sOrigen = "GMN4" Or g_sOrigen = "ART4" Or g_sOrigen = "frmPEDIDOSOE" Or g_sOrigen = "frmPEDIDOSLP" Then
             sdbgAtributosGrupoCtrl.SelectTypeRow = ssSelectionTypeMultiSelectRange
        Else
            sdbgAtributosGrupoCtrl.SelectTypeRow = ssSelectionTypeSingleSelect
        End If
    
        picNavigate.Visible = True
        picEdit.Visible = False
        sdbgAtributosGrupoCtrl.Columns("OFERTA").Locked = True
        sdbgAtributosGrupoCtrl.Columns("COSTE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("COD").Locked = True
        sdbgAtributosGrupoCtrl.Columns("NOMBRE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = True
        sdbgAtributosGrupoCtrl.Columns("ASC").Locked = True
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Style = ssStyleEdit
        g_bEdicion = False
        If g_sOrigen <> "" And g_sOrigen <> "MDI" And g_sOrigen <> "POSI" Then
            cmdSeleccionarCtrl.Visible = True
        End If
    End If
    If Not SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value) Then
        If sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(1) Then
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Style = ssStyleEditButton
            DoEvents
        End If
    End If
End Sub

Public Sub cmdModoEdicion2Ctrl_Click()
    cmdModoEdicionCtrl_Click
End Sub

Private Sub cmdSeleccionarCtrl_Click()
    'edu T94
    ' si se ha inicializado oOrigen llamar a su metodo
    
    If haySeleccion = 1 Then
        haySeleccion = 2
        Exit Sub
    End If
    
    Select Case g_sOrigen
    Case "frmCostesDescuentos", "frmCatalogoCamposPed", "frmCatalogoCostesDescuentos_Costes", "frmCatalogoCostesDescuentos_Descuentos", "frmCatalogo_Costes", "frmCatalogo_Descuentos", "frmPEDIDOS", "frmSeguimiento", "frmSeguimientoLinea"
        DevolverAtributoDirecto_1
    Case Else
        DevolverAtributoDirecto
    End Select
End Sub


''' <summary>Devuelve al atributo a la pantalla que la llama</summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdSeleccionarCtrl_clik() (A esta la llaman todas las pantallas que llevan a atributos)  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub DevolverAtributoDirecto_1()
    Dim sBoton As String
    Dim i As Integer
    Dim inum As Integer
    Dim bAddAtrib As Boolean
    Dim iNumAtrib As Integer
    Dim oGrid As SSDBGrid
    Dim inum2 As Integer
    Dim vbm As Variant
    Dim valorText As Variant
    Dim valorNum As Variant
    Dim valorFec As Variant
    Dim valorBool As Variant
    Dim oCtlCostes As Object
    
    Select Case g_sOrigen
        Case "frmCostesDescuentos", "frmPEDIDOS", "frmSeguimiento", "frmSeguimientoLinea"
            inum = 0
            Select Case g_sOrigen
                Case "frmCostesDescuentos"
                    Set oCtlCostes = frmCostesDescuentos.ctlCostesDescuentos
                Case "frmPEDIDOS"
                    Set oCtlCostes = frmPEDIDOS.ctlCostesDescuentos
                Case "frmSeguimiento"
                    Set oCtlCostes = frmSeguimiento.ctlCostesDescuentos
                Case "frmSeguimientoLinea"
                    Set oCtlCostes = frmSeguimiento.ctlCostesDescuentosLinea
            End Select
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                Select Case oCtlCostes.oSstabCD.Tab
                    Case enumTabInicial.Costes
                        Set oGrid = oCtlCostes.oSdbgCostes
                    Case enumTabInicial.Descuentos
                        Set oGrid = oCtlCostes.oSdbgDescuentos
                End Select
                    
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With oGrid
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        'Comprobar que no existe ya
                        bAddAtrib = True
                        iNumAtrib = 0
                        If .Rows > 0 Then
                            Do While iNumAtrib <= .Rows
                                .Bookmark = iNumAtrib
                                If sdbgAtributosGrupoCtrl.Columns("ID").Value = .Columns("ID_ATRIB").Value Then
                                    bAddAtrib = False
                                    Exit Do
                                End If
                                
                                iNumAtrib = iNumAtrib + 1
                            Loop
                        End If
                        
                        If Not oCtlCostes.CostesDescuentos Is Nothing Then
                            If oCtlCostes.CostesDescuentos.Count > 0 Then
                                If Not oCtlCostes.CostesDescuentos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value) Is Nothing Then
                                    oMensajes.CosteDescuentoExiste
                                    bAddAtrib = False
                                End If
                            End If
                        End If
                        
                        If bAddAtrib Then
                            .AddNew
                            .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                            .Columns("NOM").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            If sdbgAtributosGrupoCtrl.Columns("GENERICO").Value Then
                                oCtlCostes.NumGenerico = oCtlCostes.NumGenerico + 1
                                .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value & "G" & CStr(oCtlCostes.NumGenerico)
                            Else
                                .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                            End If
                            .Columns("GENERICO").Value = sdbgAtributosGrupoCtrl.Columns("GENERICO").Value
                            If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value Then
                                .Columns("INTRO").Value = 1
                            Else
                                .Columns("INTRO").Value = 0
                            End If
                            .Columns("APLICFAC").Value = False
                            
                            .Update
                            .Refresh
                        End If
                    End With
                    
                    inum = inum + 1
                Wend
                
                Set oGrid = Nothing
            End If
        Case "frmCatalogoCamposPed"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmCatalogoCamposPed.sdbgCamposPedidoLinea
                        .AddNew
                        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                            sBoton = "..."
                        Else
                            sBoton = ""
                        End If
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value) = .Columns("ATRIB_ID").CellValue(vbm) Then
                                    .CancelUpdate
                                    GoTo SiguienteAtribCatalogoCamposPed
                                End If
                            Next inum2
                        End If
                        '1. A�ADIR AL GRID DE CAMPOS DE PEDIDO DE FRMCATALOGO
                        .Columns("ID").Text = ""
                        .Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                        .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                        .Columns("VALOR").Value = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                        .Columns("VALOR").Text = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
                        .Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                        .Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                        .Columns("INTRO").Value = IIf((sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1), 1, 0)
                        .Columns("TIPO_DATO").Value = sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                        .Columns("ATRIB_ID").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                        .Columns("ORIGEN_VALOR").Value = AtributosLineaCatalogo.MaestroAtributos
                        .Columns("ORIGEN").Value = "dMaestro"
                        valorText = Null
                        valorNum = Null
                        valorFec = Null
                        valorBool = Null
                        Select Case .Columns("TIPO_DATO").Value
                            Case TiposDeAtributos.TipoString
                                valorText = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoNumerico
                                valorNum = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoFecha
                                valorFec = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoBoolean
                                valorBool = .Columns("VALOR").Text
                                If .Columns("VALOR").Text = m_sIdiTrue Then
                                    valorBool = 1
                                Else
                                    valorBool = 0
                                End If
                        End Select
                        .Columns("VERENRECEP").Value = 0
                        frmCatalogoCamposPed.g_oCamposPedidoLinea.Add 0, .Columns("COD").Text, .Columns("DEN").Text _
                                    , , .Columns("VALOR").Value, , , .Columns("INTRO").Value, .Columns("MIN").Value, .Columns("MAX").Value, .Columns("TIPO_DATO").Value, .Columns("ATRIB_ID").Value, AtributosLineaCatalogo.MaestroAtributos _
                                    , valorNum, valorText, valorFec, valorBool, , False
                    
                        .Update
                        .Refresh
                    End With
SiguienteAtribCatalogoCamposPed:
                    inum = inum + 1
                Wend
           End If
           frmCatalogoCamposPed.sdbgCamposPedidoLinea.MoveLast
           frmCatalogoCamposPed.sdbgCamposPedidoLinea.Columns("VALOR").DropDownHwnd = 0
        Case "frmCatalogoCostesDescuentos_Costes"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With sdbgAtributosGrupoCtrl
                        .Bookmark = .SelBookmarks(inum)
                        
                        frmCatalogoCostesDescuentos.g_oCostesA�adir.Add 0, .Columns("COD").Text, .Columns("NOMBRE").Text _
                                    , valor:=.Columns("VDEFECTO").Text, Minimo:=.Columns("MINIMO").Value, Maximo:=.Columns("MAXIMO").Value, _
                                    TipoIntroduccion:=IIf((.Columns("SELECCION").Value = -1), 1, 0), bCoste:=True, Atrib:=.Columns("ID").Value, _
                                    Tipo:=TipoNumerico, valor_num:=.Columns("VDEFECTO").Text, OperacionCosteDescuento:="+", GrupoCosteDescuento:="", varIndice:=inum, _
                                    Descripcion:=.Columns("HIDENDESCR").Value
                    End With
SiguienteAtribCatalogoCostesDescuentos_Costes:
                    inum = inum + 1
                Wend
           End If
           
    Case "frmCatalogoCostesDescuentos_Descuentos"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With sdbgAtributosGrupoCtrl
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        frmCatalogoCostesDescuentos.g_oDescuentosA�adir.Add 0, .Columns("COD").Text, .Columns("NOMBRE").Text, _
                                    valor:=.Columns("VDEFECTO").Text, Minimo:=.Columns("MINIMO").Value, Maximo:=.Columns("MAXIMO").Value, _
                                    TipoIntroduccion:=IIf((.Columns("SELECCION").Value = -1), 1, 0), bDescuento:=True, Atrib:=.Columns("ID").Value, _
                                    Tipo:=TipoNumerico, valor_num:=.Columns("VDEFECTO").Text, OperacionCosteDescuento:="-", GrupoCosteDescuento:="", varIndice:=inum, _
                                    Descripcion:=.Columns("HIDENDESCR").Value
                    End With
SiguienteAtribCatalogoCostesDescuentos_Descuentos:
                    inum = inum + 1
                Wend
           End If
           
    Case "frmCatalogo_Costes"
        If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmCatalogo.sdbgCostesCategoria
                        .AddNew
                        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                            sBoton = "..."
                        Else
                            sBoton = ""
                        End If
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value) = .Columns("ATRIB_ID").CellValue(vbm) Then
                                    .CancelUpdate
                                    GoTo SiguienteAtribCatalogoCategoriaCostes
                                End If
                            Next inum2
                        End If
                        '1. A�ADIR AL GRID DE CAMPOS DE PEDIDO DE FRMCATALOGO
                        .Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                        .Columns("NOMBRE").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                        .Columns("DESCR").Value = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
                        .Columns("OPERACION_HIDDEN").Text = "+"
                        .Columns("VALOR").Text = IIf(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = "", 0, sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value)
                        .Columns("TIPO").Text = ""
                        .Columns("TIPO_HIDDEN").Text = TipoCostesDescuentosLineaCatalogo.Fijos
                        .Columns("GRUPO").Text = ""
                        .Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                        .Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                        .Columns("INTRO").Value = IIf((sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1), 1, 0)
                        .Columns("ATRIB_ID").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                        .Columns("AMBITO").Value = ambitodelcampodepedido.cabecera
                        .Columns("AMBITO_HIDDEN").Value = ambitodelcampodepedido.cabecera
                        valorText = Null
                        valorNum = Null
                        valorFec = Null
                        valorBool = Null
                        Select Case sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                            Case TiposDeAtributos.TipoString
                                valorText = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoNumerico
                                valorNum = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoFecha
                                valorFec = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoBoolean
                                valorBool = .Columns("VALOR").Text
                        End Select
                        frmCatalogo.g_oCostesA�adir.Add 0, .Columns("COD").Text, .Columns("NOMBRE").Text _
                                    , valor:=.Columns("VALOR").Text, Minimo:=.Columns("MIN").Value, Maximo:=.Columns("MAX").Value, TipoIntroduccion:=.Columns("INTRO").Value, bCoste:=True, Atrib:=.Columns("ATRIB_ID").Value, Tipo:=TipoNumerico, valor_num:=.Columns("VALOR").Text, OperacionCosteDescuento:="+", GrupoCosteDescuento:="", varIndice:=inum
                        .Update
                        .Refresh
                    End With
SiguienteAtribCatalogoCategoriaCostes:
                    inum = inum + 1
                Wend
           End If
           'frmCatalogo.sdbgCostesCategoria.MoveLast
           'frmCatalogo.sdbgCostesCategoria.Columns("VALOR").DropDownHwnd = 0
    Case "frmCatalogo_Descuentos"
        If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmCatalogo.sdbgDescuentosCategoria
                        .AddNew
                        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                            sBoton = "..."
                        Else
                            sBoton = ""
                        End If
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value) = .Columns("ATRIB_ID").CellValue(vbm) Then
                                    .CancelUpdate
                                    GoTo SiguienteAtribCatalogoCategoriaDescuentos
                                End If
                            Next inum2
                        End If
                        '1. A�ADIR AL GRID DE CAMPOS DE PEDIDO DE FRMCATALOGO
                        .Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                        .Columns("NOMBRE").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                        .Columns("DESCR").Value = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
                        .Columns("OPERACION_HIDDEN").Text = "-"
                        .Columns("VALOR").Text = IIf(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = "", 0, sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value)
                        .Columns("TIPO").Text = ""
                        .Columns("TIPO_HIDDEN").Text = TipoCostesDescuentosLineaCatalogo.Fijos
                        .Columns("GRUPO").Text = ""
                        .Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                        .Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                        .Columns("INTRO").Value = IIf((sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1), 1, 0)
                        .Columns("ATRIB_ID").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                        .Columns("AMBITO").Value = ambitodelcampodepedido.cabecera
                        .Columns("AMBITO_HIDDEN").Value = ambitodelcampodepedido.cabecera
                        valorText = Null
                        valorNum = Null
                        valorFec = Null
                        valorBool = Null
                        Select Case sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                            Case TiposDeAtributos.TipoString
                                valorText = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoNumerico
                                valorNum = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoFecha
                                valorFec = .Columns("VALOR").Text
                            Case TiposDeAtributos.TipoBoolean
                                valorBool = .Columns("VALOR").Text
                        End Select
                        frmCatalogo.g_oDescuentosA�adir.Add 0, .Columns("COD").Text, .Columns("NOMBRE").Text _
                                    , valor:=.Columns("VALOR").Text, Minimo:=.Columns("MIN").Value, Maximo:=.Columns("MAX").Value, TipoIntroduccion:=.Columns("INTRO").Value, bCoste:=True, Atrib:=.Columns("ATRIB_ID").Value, Tipo:=TipoNumerico, valor_num:=.Columns("VALOR").Text, OperacionCosteDescuento:="-", GrupoCosteDescuento:="", varIndice:=inum
                    End With
SiguienteAtribCatalogoCategoriaDescuentos:
                    inum = inum + 1
                Wend
           End If
           frmCatalogo.sdbgDescuentosCategoria.MoveLast
           frmCatalogo.sdbgDescuentosCategoria.Columns("VALOR").DropDownHwnd = 0
    End Select
    
    Unload m_oContainer
End Sub

''' <summary>Para aligerar DevolverAtributoDirecto</summary>
''' <returns></returns>
''' <remarks>Llamada desde: DevolverAtributoDirecto </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
''' <revision>EPB 21/10/2014</revision>

Private Function RellenarfrmPlantillasProce(ByVal sBoton As String) As Boolean
Dim v As Variant
        
         RellenarfrmPlantillasProce = False
         frmPlantillasProce.sdbgAtribPlant.Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("NOMBRE").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("TIPO").Value = sdbgAtributosGrupoCtrl.Columns("TIPO").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("idtipo").Value = sdbgAtributosGrupoCtrl.Columns("idtipo").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("LIBRE").Value = sdbgAtributosGrupoCtrl.Columns("LIBRE").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("SELECCION").Value = sdbgAtributosGrupoCtrl.Columns("SELECCION").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("BOT").Value = sBoton
         frmPlantillasProce.sdbgAtribPlant.Columns("AMBITO").Value = m_sItem
         frmPlantillasProce.sdbgAtribPlant.Columns("VALORBAJO").Value = sdbgAtributosGrupoCtrl.Columns("ASC").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("TipoPond").Value = sdbgAtributosGrupoCtrl.Columns("TipoPond").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("DESCR").Value = sdbgAtributosGrupoCtrl.Columns("DESCR").Value
         frmPlantillasProce.sdbgAtribPlant.Columns("HIDENDESCR").Value = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
         
         If frmPlantillasProce.sdbgAtribPlant.Rows > 1 Then
             If frmPlantillasProce.sdbgAtribPlant.DataChanged Then
                 v = frmPlantillasProce.sdbgAtribPlant.ActiveCell.Value
                 If (v <> "") Then frmPlantillasProce.sdbgAtribPlant.ActiveCell.Value = v
                 RellenarfrmPlantillasProce = True
             End If
        Else
             frmPlantillasProce.sdbgAtribPlant.Update
        End If
        

End Function

''' <summary>Devuelve al atributo a la pantalla que la llama</summary>
''' <returns></returns>
''' <remarks>Llamada desde: cmdSeleccionarCtrl_clik() (A esta la llaman todas las pantallas que llevan a atributos)  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
''' <revision>LTG 07/09/2011</revision>

Private Sub DevolverAtributoDirecto()
    Dim oAtributoAnyadir As CGrupoMatNivel4
    Dim teserror As TipoErrorSummit
    Dim sArtCod As Variant
    Dim sBoton As String
    Dim i As Integer
    Dim inum As Integer
    Dim v As Variant
    Dim bAnyadirCol As Boolean
    Dim oColumn As SSDataWidgets_B.Column
    Dim oCampos As CFormItems
    Dim oDenominaciones As CMultiidiomas
    Dim oIdioma As CIdioma
    Dim tLibre As TAtributoIntroduccion
    Dim tTipoAtrib As TiposDeAtributos
    Dim oCamposPredef As CCamposPredef
    Dim vMinimo As Variant
    Dim vMaximo As Variant
    Dim vMinFec As Variant
    Dim vMaxFec As Variant
    Dim sValor As String
    Dim bAddAtrib As Boolean
    Dim iNumAtrib As Integer
    Dim oGrid As SSDBGrid
    Dim oCombo As SSDBDropDown
    Dim Cod() As String
    Dim inum2 As Integer
    Dim vbm As Variant
    Dim aBookmarks As Variant
    Dim aIdentificadores As Variant
    Dim vIndice As Variant
    Dim oLista As CValorPond
    Dim oAtributoAnyadirTP As CTipoPedido
    Dim oatrib As CAtributo
    Dim j As Integer
    
    Select Case g_sOrigen
        Case "PLANTILLAS"
            inum = 0
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    frmPlantillasProce.g_sOrigen = "frmATRIB"
                    
                    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    inum = inum + 1
                    If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                        sBoton = "..."
                    Else
                        sBoton = ""
                    End If
                    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                    bAnyadirCol = True
                    If frmPlantillasProce.g_oPlantillaSeleccionada.ATRIBUTOS Is Nothing Then Set frmPlantillasProce.g_oPlantillaSeleccionada.ATRIBUTOS = oFSGSRaiz.Generar_CAtributos
                    
                    For i = 1 To frmPlantillasProce.g_oPlantillaSeleccionada.ATRIBUTOS.Count
                        If g_oAtributoEnEdicion.Id = frmPlantillasProce.g_oPlantillaSeleccionada.ATRIBUTOS.Item(i).Id Then
                            
                            frmPlantillasProce.sdbgAtribPlant.SelBookmarks.RemoveAll
                                                                                
                            For j = 0 To frmPlantillasProce.sdbgAtribPlant.Rows - 1
                                
                                vbm = frmPlantillasProce.sdbgAtribPlant.AddItemBookmark(j)
                                                            
                                If (frmPlantillasProce.sdbgAtribPlant.Columns("ATRIB").CellValue(vbm) = CStr(g_oAtributoEnEdicion.Id)) Then
                                    frmPlantillasProce.sdbgAtribPlant.Bookmark = vbm
                                    frmPlantillasProce.sdbgAtribPlant.SelBookmarks.Add vbm
                                    Exit For
                                End If
                            Next
                            
                            bAnyadirCol = False
                            If RellenarfrmPlantillasProce(sBoton) Then
                                frmPlantillasProce.actualizarYSalirAtrib
                            End If
                           Exit For
                        End If
                    Next
                    If bAnyadirCol Then
                        frmPlantillasProce.sdbgAtribPlant.AddNew
                        'Le indico q el ambito es Item pq as� lo pone siempre RellenarfrmPlantillasProce
                        frmPlantillasProce.g_oPlantillaSeleccionada.ATRIBUTOS.Add g_oAtributoEnEdicion.Id, g_oAtributoEnEdicion.Cod, g_oAtributoEnEdicion.Den, g_oAtributoEnEdicion.Tipo, g_oAtributoEnEdicion.PreferenciaValorBajo, , , , , , , , , g_oAtributoEnEdicion.FECACT, frmPlantillasProce.g_oPlantillaSeleccionada.Id, , , frmPlantillasProce.g_sCodGRP, , g_oAtributoEnEdicion.TipoIntroduccion, g_oAtributoEnEdicion.Minimo, g_oAtributoEnEdicion.Maximo, , , TipoAmbitoProceso.AmbItem, , g_oAtributoEnEdicion.PrecioFormula, g_oAtributoEnEdicion.PrecioAplicarA, g_oAtributoEnEdicion.PrecioDefecto, g_oAtributoEnEdicion.TipoPonderacion
                        RellenarfrmPlantillasProce sBoton
                    End If
                Wend
            End If
        Case "CONFINTEGRACION"
            inum = 0
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    frmCONFIntegracion.g_sOrigen = "frmATRIB"
                    frmCONFIntegracion.sdbgAtribPlant.AddNew
                    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    
                    inum = inum + 1
                    
                    If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                        sBoton = "..."
                    Else
                        sBoton = ""
                    End If
                    
                    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                    
                    If Not frmCONFIntegracion.m_oAtributos Is Nothing Then
                       For i = 1 To frmCONFIntegracion.m_oAtributos.Count
                           If g_oAtributoEnEdicion.Id = FSGSLibrary.StrToDbl0(frmCONFIntegracion.m_oAtributos.Item(i).Atrib) Then
                               frmCONFIntegracion.sdbgAtribPlant.CancelUpdate
                               GoTo SiguienteAtribIntPlant
                           End If
                       Next
                    End If
                        
                    frmCONFIntegracion.m_oAtributos.AddAtrInt g_oAtributoEnEdicion.Id, g_oAtributoEnEdicion.Den, g_oAtributoEnEdicion.Tipo, , , , g_oAtributoEnEdicion.TipoIntroduccion, , , g_oAtributoEnEdicion.Descripcion, g_oAtributoEnEdicion.valorNum, g_oAtributoEnEdicion.valorText, g_oAtributoEnEdicion.valorFec, g_oAtributoEnEdicion.valorBool, , , g_oAtributoEnEdicion.FECACT, ""
                    
                    Dim sBooleanNeg As String
                    Dim sDescripcion As String
                    Dim sAmbito As String
                    
                    sBooleanNeg = "" 'Para que los checks del sdbg de atributos de integracion aparezca no habilitado por defecto
                    sDescripcion = "..."
                    
                    Select Case g_oAtributoEnEdicion.ambito
                        Case 1: sAmbito = "Proceso" ' m_asAtrib(4)
                        Case 2: sAmbito = "Grupo" ' m_asAtrib(5)
                        Case 3: sAmbito = "Item" ' m_asAtrib(6)
                     End Select
                    
                    frmCONFIntegracion.sdbgAtribPlant.Columns("ID").Text = ""
                    frmCONFIntegracion.sdbgAtribPlant.Columns("IDATRIB").Text = g_oAtributoEnEdicion.Id
                    frmCONFIntegracion.sdbgAtribPlant.Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("idtipo").Value
                    frmCONFIntegracion.sdbgAtribPlant.Columns("ADJUDICACION").Value = sBooleanNeg
                    frmCONFIntegracion.sdbgAtribPlant.Columns("PEDIDO").Value = sBooleanNeg
                    frmCONFIntegracion.sdbgAtribPlant.Columns("CODIGO").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                    frmCONFIntegracion.sdbgAtribPlant.Columns("DENOMINACION").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                    If sdbgAtributosGrupoCtrl.Columns("DESCR").Value <> "" Then
                        frmCONFIntegracion.sdbgAtribPlant.Columns("DESCRIPCION").Value = sDescripcion
                        frmCONFIntegracion.sdbgAtribPlant.Columns("HIDDENDESCR").Value = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
                    Else
                        frmCONFIntegracion.sdbgAtribPlant.Columns("DESCRIPCION").Value = ""
                        frmCONFIntegracion.sdbgAtribPlant.Columns("HIDDENDESCR").Value = ""
                    End If
                    frmCONFIntegracion.sdbgAtribPlant.Columns("TIPODEDATO").Value = sdbgAtributosGrupoCtrl.Columns("TIPO").Value
                    frmCONFIntegracion.sdbgAtribPlant.Columns("SELECCION").Text = sdbgAtributosGrupoCtrl.Columns("SELECCION").Value
                    frmCONFIntegracion.sdbgAtribPlant.Columns("LISTAVALORES").Value = sBoton
                    frmCONFIntegracion.sdbgAtribPlant.Columns("AMBITO").Value = sAmbito
                    frmCONFIntegracion.sdbgAtribPlant.Columns("OBLIGATORIO").Value = sBooleanNeg
                    frmCONFIntegracion.sdbgAtribPlant.Columns("VALORPORDEFECTO").Value = ""
                    frmCONFIntegracion.sdbgAtribPlant.Columns("VALIDACION").Value = sBooleanNeg
                    frmCONFIntegracion.sdbgAtribPlant.Columns("RELACION").Value = ""
                    frmCONFIntegracion.sdbgAtribPlant.Columns("FECACT").Value = sdbgAtributosGrupoCtrl.Columns("FECACT").Value
                    frmCONFIntegracion.sdbgAtribPlant.Columns("TIPO_PEDIDO").Value = "-1" 'Le indico que al dar de alta el atributo, se da de alta con todos los tipos de pedido
                    frmCONFIntegracion.sdbgAtribPlant.Columns("TIPO_PEDIDO_BTN").Text = "..."
                    frmCONFIntegracion.sdbgAtribPlant.Update
                    If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                        frmCONFIntegracion.m_oAtributos.Item(CStr(g_oAtributoEnEdicion.Id)).CargarListaDeValoresInt (True)
                         teserror = frmCONFIntegracion.m_oAtributos.Item(CStr(g_oAtributoEnEdicion.Id)).ModificarListaDeValores(0, True)
                    End If
                Wend
            
SiguienteAtribIntPlant:
                inum = inum + 1
           End If
           frmCONFIntegracion.sdbgAtribPlant.MoveLast
           DoEvents
           frmCONFIntegracion.sdbgAtribPlant.Columns("VALORPORDEFECTO").DropDownHwnd = 0
           
        Case "PLANTILLASESP"
            inum = 0
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    frmPlantillasProce.g_sOrigen = "frmATRIB"
                    frmPlantillasProce.sdbgAtributosEsp.AddNew
                    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    
                    inum = inum + 1
                
                    If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                        sBoton = "..."
                    Else
                        sBoton = ""
                    End If
                    
                    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                                        
                    If Not frmPlantillasProce.g_oPlantillaSeleccionada.AtributosEspecificacion Is Nothing Then
                       For i = 1 To frmPlantillasProce.g_oPlantillaSeleccionada.AtributosEspecificacion.Count
                           If g_oAtributoEnEdicion.Id = FSGSLibrary.StrToDbl0(frmPlantillasProce.g_oPlantillaSeleccionada.AtributosEspecificacion.Item(i).Atrib) Then
                               frmPlantillasProce.sdbgAtributosEsp.CancelUpdate
                               GoTo SiguienteAtribEspPlant
                           End If
                       Next
                    End If
                       
                    frmPlantillasProce.g_oPlantillaSeleccionada.AtributosEspecificacion.Add g_oAtributoEnEdicion.Id, g_oAtributoEnEdicion.Cod, g_oAtributoEnEdicion.Den, g_oAtributoEnEdicion.Tipo, g_oAtributoEnEdicion.PreferenciaValorBajo, , , , , , , , , g_oAtributoEnEdicion.FECACT, , , , frmPlantillasProce.g_sCodGRPEsp, g_oAtributoEnEdicion.Id, g_oAtributoEnEdicion.TipoIntroduccion, g_oAtributoEnEdicion.Minimo, g_oAtributoEnEdicion.Maximo, , , iAmbitoAtribEsp, g_oAtributoEnEdicion.Obligatorio, g_oAtributoEnEdicion.PrecioFormula, g_oAtributoEnEdicion.PrecioAplicarA, g_oAtributoEnEdicion.PrecioDefecto, g_oAtributoEnEdicion.TipoPonderacion, , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value, , , g_oAtributoEnEdicion.valorNum, g_oAtributoEnEdicion.valorText, g_oAtributoEnEdicion.valorFec, g_oAtributoEnEdicion.valorBool, , , 0, g_oAtributoEnEdicion.Id
                    frmPlantillasProce.sdbgAtributosEsp.Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("TIPO").Value = sdbgAtributosGrupoCtrl.Columns("TIPO").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("idtipo").Value = sdbgAtributosGrupoCtrl.Columns("idtipo").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("IDATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("DESCR").Value = sdbgAtributosGrupoCtrl.Columns("DESCR").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("AMBITO").Value = sAmbitoAtribEsp
                    frmPlantillasProce.sdbgAtributosEsp.Columns("VALOR").Value = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("VALOR").Text = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
                    frmPlantillasProce.sdbgAtributosEsp.Columns("TIPO_INTRO").Value = sdbgAtributosGrupoCtrl.Columns("SELECCION").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("TIPO_DATOS").Value = sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("VALIDACION").Value = m_sApertura
                    frmPlantillasProce.sdbgAtributosEsp.Columns("HIDENDESCR").Value = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                    frmPlantillasProce.sdbgAtributosEsp.Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                    frmPlantillasProce.sdbgAtributosEsp.Update
                Wend
            
SiguienteAtribEspPlant:
                inum = inum + 1
           End If
           frmPlantillasProce.sdbgAtributosEsp.MoveLast
           DoEvents
           
           frmPlantillasProce.sdbgAtributosEsp.Columns("VALOR").DropDownHwnd = 0
        
        Case "ATRIB_ESP", "ATRIB_ESP_GRUPO"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.AddNew
                    
                    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                         sBoton = "..."
                    Else
                        sBoton = ""
                    End If
                    
                    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                    
                    If g_sOrigen = "ATRIB_ESP" Then
                       If Not frmPROCE.g_oProcesoSeleccionado.AtributosEspecificacion Is Nothing Then
                          For i = 1 To frmPROCE.g_oProcesoSeleccionado.AtributosEspecificacion.Count
                              If g_oAtributoEnEdicion.Id = FSGSLibrary.StrToDbl0(frmPROCE.g_oProcesoSeleccionado.AtributosEspecificacion.Item(i).Atrib) Then
                                  frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.CancelUpdate
                                  GoTo SiguienteAtribEsp
                              End If
                          Next
                       End If
                    Else
                       If Not frmPROCE.g_oGrupoSeleccionado.AtributosEspecificacion Is Nothing Then
                         For i = 1 To frmPROCE.g_oGrupoSeleccionado.AtributosEspecificacion.Count
                             If g_oAtributoEnEdicion.Id = FSGSLibrary.StrToDbl0(frmPROCE.g_oGrupoSeleccionado.AtributosEspecificacion.Item(i).Atrib) Then
                                 frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.CancelUpdate
                                 GoTo SiguienteAtribEsp
                             End If
                         Next
                       End If
                    End If
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("ANYO").Value = frmPROCE.g_oProcesoSeleccionado.Anyo
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("GMN1").Value = frmPROCE.g_oProcesoSeleccionado.GMN1Cod
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("PROCE").Value = frmPROCE.g_oProcesoSeleccionado.Cod
                    If g_sOrigen = "ATRIB_ESP_GRUPO" Then
                       frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("GRUPO").Value = frmPROCE.g_oGrupoSeleccionado.Codigo
                       frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("GRUPOID").Value = frmPROCE.g_oGrupoSeleccionado.Id
                    End If
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("PEDIDO").Value = 0
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("VALOR").Value = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("VALOR").Text = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("TIPO_INTRO").Value = sdbgAtributosGrupoCtrl.Columns("SELECCION").Value
                    If g_sOrigen = "ATRIB_ESP" Then 'Atributo de especificaci�n a nivel de proceso
                       frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("AMBITO").Value = 1
                    Else 'Atributo de especificaci�n a nivel de grupo
                       frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("AMBITO").Value = 2
                    End If
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("TIPO_DATOS").Value = sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("ID_A").Value = sdbgAtributosGrupoCtrl.Columns("FECACT").Value
                    frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Update
        
SiguienteAtribEsp:
                     inum = inum + 1
                Wend
           End If
           frmPROCE.g_sOrigen = ""
        
           frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.MoveLast
           DoEvents
           frmPROCE.ctlEspecificacionesProceso1.GridAtributosEsp.Columns("VALOR").DropDownHwnd = 0
      
        Case "frmPROCE"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                     frmPROCE.g_sOrigen = "frmATRIB"
                     frmPROCE.sdbgAtributos.AddNew
                     sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                     If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                          sBoton = "..."
                     Else
                         sBoton = ""
                     End If
                     
                     Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                     
                     If frmPROCE.sdbcGrupoA.Visible And frmPROCE.sdbcGrupoA.Columns(0).Value <> "************" Then
                        If Not frmPROCE.g_oGrupoSeleccionado.ATRIBUTOS Is Nothing Then
                            For i = 1 To frmPROCE.g_oGrupoSeleccionado.ATRIBUTOS.Count
                                If g_oAtributoEnEdicion.Id = CLng(frmPROCE.g_oGrupoSeleccionado.ATRIBUTOS.Item(i).Id) Then
                                    frmPROCE.sdbgAtributos.CancelUpdate
                                    GoTo Siguiente
                                End If
                            Next
                        End If
                     Else
                        If Not frmPROCE.g_oProcesoSeleccionado.ATRIBUTOS Is Nothing Then
                            For i = 1 To frmPROCE.g_oProcesoSeleccionado.ATRIBUTOS.Count
                                If g_oAtributoEnEdicion.Id = CLng(frmPROCE.g_oProcesoSeleccionado.ATRIBUTOS.Item(i).Id) Then
                                    frmPROCE.sdbgAtributos.CancelUpdate
                                    GoTo Siguiente
                                End If
                            Next
                        End If
                     End If
                    
                     frmPROCE.sdbgAtributos.Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                     frmPROCE.sdbgAtributos.Columns("NOMBRE").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                     frmPROCE.sdbgAtributos.Columns("TIPO").Value = sdbgAtributosGrupoCtrl.Columns("TIPO").Value
                     frmPROCE.sdbgAtributos.Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("idtipo").Value
                     frmPROCE.sdbgAtributos.Columns("IDATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                     frmPROCE.sdbgAtributos.Columns("LIBRE").Value = sdbgAtributosGrupoCtrl.Columns("LIBRE").Value
                     frmPROCE.sdbgAtributos.Columns("SELECCION").Value = sdbgAtributosGrupoCtrl.Columns("SELECCION").Value
                     frmPROCE.sdbgAtributos.Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                     frmPROCE.sdbgAtributos.Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                     frmPROCE.sdbgAtributos.Columns("BOT").Value = sBoton
                     frmPROCE.sdbgAtributos.Columns("AMBITOVALOR").Value = m_sItem
                     frmPROCE.sdbgAtributos.Columns("ASC").Value = sdbgAtributosGrupoCtrl.Columns("ASC").Value
                     frmPROCE.sdbgAtributos.Columns("TIPOPONDERACION").Value = sdbgAtributosGrupoCtrl.Columns("TipoPond").Value
                     frmPROCE.sdbgAtributos.Columns("FECACT").Value = sdbgAtributosGrupoCtrl.Columns("FECACT").Value
                     frmPROCE.sdbgAtributos.Columns("DESCR").Value = sdbgAtributosGrupoCtrl.Columns("DESCR").Value
                     frmPROCE.sdbgAtributos.Columns("HIDENDESCR").Value = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
                     frmPROCE.g_bCambiarDefini = False
                     If frmPROCE.sdbgAtributos.Rows > 1 Then
                       If frmPROCE.sdbgAtributos.DataChanged Then
                           v = frmPROCE.sdbgAtributos.ActiveCell.Value
                           If (v <> "") Then frmPROCE.sdbgAtributos.ActiveCell.Value = v
                       End If
                       frmPROCE.sdbgAtributos.Update
                    Else
                       frmPROCE.sdbgAtributos.Update
                    End If
                    frmPROCE.sdbgAtributos.Refresh
        
Siguiente:
                     inum = inum + 1
                Wend
           End If
           frmPROCE.g_sOrigen = ""
           frmPROCE.sdbgAtributos.MoveLast
        
        Case "frmFormularios", "frmDesglose"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                Set oCampos = oFSGSRaiz.Generar_CFormCampos
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    
                    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                    For Each oIdioma In frmFormularios.m_oIdiomas
                        oDenominaciones.Add oIdioma.Cod, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                    Next
                    vIndice = Null
                    If GridCheckToBoolean(sdbgAtributosGrupoCtrl.Columns("LIBRE").Value) Then
                        tLibre = IntroLibre
                    Else
                        Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                        If sdbgAtributosGrupoCtrl.Columns("LISTA_EXTERNA").Value = "1" Then
                            tLibre = IntroListaExterna
                        Else
                            tLibre = Introselec
                        End If
                        If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text <> "" And sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value = TiposDeAtributos.TipoString Then
                            If g_oAtributoEnEdicion.ListaPonderacion Is Nothing Then
                                g_oAtributoEnEdicion.CargarListaDeValores
                            ElseIf g_oAtributoEnEdicion.ListaPonderacion.Count = 0 Then
                                g_oAtributoEnEdicion.CargarListaDeValores
                            End If
                            
                            For Each oLista In g_oAtributoEnEdicion.ListaPonderacion
                                If oLista.ValorLista = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text Then
                                    vIndice = oLista.indice
                                    Exit For
                                End If
                            Next
                            
                            Set g_oAtributoEnEdicion = Nothing
                        End If
                    End If
                    
                    If (sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value = TiposDeAtributos.TipoString) And (tLibre = IntroLibre) Then
                        tTipoAtrib = TiposDeAtributos.TipoTextoLargo
                    ElseIf (sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value = TiposDeAtributos.TipoString) Then
                        tTipoAtrib = TiposDeAtributos.TipoTextoMedio
                    Else
                        tTipoAtrib = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                    End If
                    
                    If g_sOrigen = "frmFormularios" Then
                        Select Case sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            Case TiposDeAtributos.TipoString
                                oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, vIndice, sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text, , , sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , , , , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            Case TiposDeAtributos.TipoNumerico
                                oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text, , , , sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , , , , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            Case TiposDeAtributos.TipoFecha
                                oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, , , sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text, , sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , , , , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            Case TiposDeAtributos.TipoBoolean
                                oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, , , , sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text, sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , , , , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                         End Select
                    Else  'Desglose
                        Select Case sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            Case TiposDeAtributos.TipoString
                                oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, , sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text, , , sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, True, , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            Case TiposDeAtributos.TipoNumerico
                                oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text, , , , sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, True, , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            Case TiposDeAtributos.TipoFecha
                                oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, , , sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text, , sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, True, , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            Case TiposDeAtributos.TipoBoolean
                                If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text = m_sIdiTrue Then 'Si
                                    oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, , , , 1, sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, True, , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                                Else 'No
                                    oCampos.Add inum, frmFormularios.g_oGrupoSeleccionado, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, , , , 0, sdbgAtributosGrupoCtrl.Columns("MINIMO").Value, sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , , , frmFormularios.g_ofrmDesglose.g_oCampoDesglose, True, , , , , , , , , , , , , , , , , , , sdbgAtributosGrupoCtrl.Columns("COD").Value, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                                End If
                         End Select
                    End If
                    
                    Set oDenominaciones = Nothing
                    
                    inum = inum + 1
                Wend
                
                teserror = oCampos.AnyadirCamposDeGSoAtributos(True)
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
                    
                If g_sOrigen = "frmFormularios" Then
                    frmFormularios.AnyadirCampos oCampos
                    frmFormularios.g_Accion = ACCFormularioCons
                Else
                    frmFormularios.g_ofrmDesglose.AnyadirCampos oCampos
                    frmFormularios.g_ofrmDesglose.g_Accion = ACCDesgloseCons
                End If
                
                Set oCampos = Nothing
            End If
        Case "frmPARTipoSolicit", "frmPARTipoSolicitDesglose"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                Set oCamposPredef = oFSGSRaiz.Generar_CCamposPredef
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    
                    Set oDenominaciones = oFSGSRaiz.Generar_CMultiidiomas
                    For Each oIdioma In frmPARTipoSolicit.m_oIdiomas
                        oDenominaciones.Add oIdioma.Cod, sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                    Next
                    
                    If GridCheckToBoolean(sdbgAtributosGrupoCtrl.Columns("LIBRE").Value) Then
                        tLibre = IntroLibre
                    Else
                        Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                        If g_oAtributoEnEdicion.ListaExterna = 1 Then
                            tLibre = IntroListaExterna
                        Else
                            tLibre = Introselec
                        End If
                        Set g_oAtributoEnEdicion = Nothing
                    End If
                    
                    If sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value = TiposDeAtributos.TipoString Then
                        tTipoAtrib = TiposDeAtributos.TipoTextoMedio
                    Else
                        tTipoAtrib = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                    End If
                    
                    Select Case sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                        Case TiposDeAtributos.TipoNumerico
                            vMinimo = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            vMaximo = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            vMinFec = Null
                            vMaxFec = Null
                        Case TiposDeAtributos.TipoFecha
                            vMinFec = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            vMaxFec = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            vMinimo = Null
                            vMaximo = Null
                        Case Else
                            vMinimo = Null
                            vMaximo = Null
                            vMinFec = Null
                            vMaxFec = Null
                    End Select
                    
                    If g_sOrigen = "frmPARTipoSolicit" Then
                        oCamposPredef.Add inum, frmPARTipoSolicit.g_oTipoSeleccionado.Id, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, vMinimo, vMaximo, vMinFec, vMaxFec, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , False
                    Else 'Es desglose
                        oCamposPredef.Add inum, frmPARTipoSolicit.g_ofrmDesglose.g_oCampoDesglose.TipoSolicitud, oDenominaciones, , TipoCampoPredefinido.Atributo, tTipoAtrib, tLibre, vMinimo, vMaximo, vMinFec, vMaxFec, , sdbgAtributosGrupoCtrl.Columns("ID").Value, , True, , frmPARTipoSolicit.g_ofrmDesglose.g_oCampoDesglose
                    End If
                    
                    Set oDenominaciones = Nothing
                    inum = inum + 1
                Wend
                
                teserror = oCamposPredef.AnyadirCamposDeAtributos
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Exit Sub
                End If
                    
                If g_sOrigen = "frmPARTipoSolicit" Then
                    frmPARTipoSolicit.AnyadirCampos oCamposPredef
                    frmPARTipoSolicit.g_Accion = ACCTipoSolicitudCons
                Else  'Es desglose:
                    frmPARTipoSolicit.g_ofrmDesglose.AnyadirCampos oCamposPredef
                    frmPARTipoSolicit.g_ofrmDesglose.g_Accion = ACCTipoSolicitudCons
                End If
                Set oCamposPredef = Nothing
            
            End If
            
        Case "frmPEDIDOSOE", "frmPEDIDOSLP"
            DevolverAtributoDirecto_frmPedidos
        
        Case "ITEM_ATRIBESP"
            Dim oItem As CItem
            
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                         sBoton = "..."
                    Else
                        sBoton = ""
                    End If
                    
                    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                    
                    If Not frmPROCE.sdbgItems.Columns("AT_" & CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                        oMensajes.DatoDuplicado2 g_oAtributoEnEdicion.Den, ""
                        Exit Sub
                    End If
                    
                    ' a�adir el atributo a la coleccion
                    If frmPROCE.g_oGrupoSeleccionado.Items Is Nothing Then
                        frmPROCE.g_oGrupoSeleccionado.CargarTodosLosItems OrdItemPorOrden
                    End If
                    
                    If frmPROCE.m_sCodGrupo <> "" Then
                    'Para el grupo seleccionado.
                        For Each oItem In frmPROCE.g_oGrupoSeleccionado.Items
                            teserror = oItem.InsertarAtributo(g_oAtributoEnEdicion)
                            If teserror.NumError <> TESnoerror Then
                                TratarError teserror
                                Exit Sub
                            End If
                            oItem.CargarTodosLosAtributosEspecificacion
                        Next
                    Else
                    'Para todos los grupos del proceso.
                        For Each oItem In frmPROCE.g_oProcesoSeleccionado.Items
                            teserror = oItem.InsertarAtributo(g_oAtributoEnEdicion)
                            If teserror.NumError <> TESnoerror Then
                                TratarError teserror
                                Exit Sub
                            End If
                            oItem.CargarTodosLosAtributosEspecificacion
                        Next
                    End If
                    
                    frmPROCE.sdbgItems.Columns.Add frmPROCE.sdbgItems.Columns.Count
                    Set oColumn = frmPROCE.sdbgItems.Columns(frmPROCE.sdbgItems.Columns.Count - 1)
                    oColumn.Name = "AT_" & CStr(g_oAtributoEnEdicion.Id)
                    oColumn.CaptionAlignment = ssColCapAlignCenter
                
                    If g_oAtributoEnEdicion.TipoIntroduccion = IntroLibre Then
                        Select Case g_oAtributoEnEdicion.Tipo
                            Case TipoFecha
                                oColumn.Alignment = ssCaptionAlignmentRight
                                oColumn.Style = ssStyleEditButton
                            Case TipoBoolean
                                oColumn.Style = ssStyleComboBox
                                oColumn.DropDownHwnd = frmPROCE.sdbddValor.hWnd
                            Case TipoString
                                oColumn.Style = ssStyleEditButton
                                oColumn.FieldLen = cLongAtribEsp_Texto
                            Case Else
                                oColumn.Alignment = ssCaptionAlignmentRight
                                oColumn.Style = ssStyleEdit
                        End Select
        
                    Else
                        oColumn.Style = ssStyleComboBox
                        Select Case g_oAtributoEnEdicion.Tipo
                          Case TiposDeAtributos.TipoString
                              oColumn.Alignment = ssCaptionAlignmentLeft
                              oColumn.FieldLen = 800
                          Case TiposDeAtributos.TipoNumerico
                              oColumn.Alignment = ssCaptionAlignmentRight
                          Case TiposDeAtributos.TipoFecha
                              oColumn.Alignment = ssCaptionAlignmentRight
                          Case TiposDeAtributos.TipoBoolean
                        End Select
                
                        oColumn.DropDownHwnd = frmPROCE.sdbddValor.hWnd
                    End If
                    
                    Dim bm0 As Variant
                    For i = 0 To frmPROCE.sdbgItems.Rows - 1
                        bm0 = frmPROCE.sdbgItems.GetBookmark(i)
                        sValor = ""
                                   
                        frmPROCE.sdbgItems.Bookmark = bm0
                        
                        Select Case g_oAtributoEnEdicion.Tipo
                            Case TiposDeAtributos.TipoString
                                frmPROCE.sdbgItems.Columns(oColumn.Name).Value = g_oAtributoEnEdicion.valorText
                            Case TiposDeAtributos.TipoNumerico
                                frmPROCE.sdbgItems.Columns(oColumn.Name).Value = g_oAtributoEnEdicion.valorNum
                            Case TiposDeAtributos.TipoFecha
                                sValor = Format(g_oAtributoEnEdicion.valorFec, "Short date")
                                frmPROCE.sdbgItems.Columns(oColumn.Name).Value = sValor
                            Case TiposDeAtributos.TipoBoolean
                                Select Case g_oAtributoEnEdicion.valorBool
                                    Case 0, False
                                        sValor = m_sIdiFalse
                                    Case 1, True
                                        sValor = m_sIdiTrue
                                    Case Else
                                        sValor = ""
                                End Select
                                frmPROCE.sdbgItems.Columns(oColumn.Name).Value = sValor
                        End Select
                    Next
                    
                    oColumn.Locked = False
                    oColumn.caption = g_oAtributoEnEdicion.Cod
                    oColumn.StyleSet = "Normal"
                    oColumn.Visible = True
                    oColumn.TagVariant = "000"
                    Set oColumn = Nothing
                    
                    frmPROCE.sdbgItems.Update
                    frmPROCE.RedimensionarGridItems
        
                    inum = inum + 1
                Wend
           End If
           frmPROCE.g_sOrigen = ""
        
           DoEvents
            
        Case "frmPROCEBuscarArticulo"
            inum = 0
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmPROCEBuscarArticulo.sdbgAtributos
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        'Comprobar que no existe ya
                        bAddAtrib = True
                        iNumAtrib = 0
                        Do While iNumAtrib <= .Rows
                            .Bookmark = iNumAtrib
                            If sdbgAtributosGrupoCtrl.Columns("ID").Value = .Columns("ID_ATRIB").Value Then
                                .Columns("USAR").Value = "1"
                                bAddAtrib = False
                                Exit Do
                            End If
                            
                            iNumAtrib = iNumAtrib + 1
                        Loop
                        
                        If bAddAtrib Then
                            .AddNew
                            .Columns("USAR").Value = "1"
                            .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                            .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            .Columns("GRUPO").Value = "0"
                            .Columns("INTRO").Value = Abs(CInt(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value))
                            .Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                            .Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            .Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            .Update
                            .Refresh
                        End If
                        
                        Set oatrib = frmPROCEBuscarArticulo.addAtributo(m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                        If Not oatrib Is Nothing Then
                            If oatrib.uons.Count > 0 Then
                                oatrib.AmbitoAtributo = TAmbitoUon
                            Else
                                oatrib.AmbitoAtributo = TAmbitoMaterial
                            End If
                        End If
                        
                    End With
                    
                    inum = inum + 1
                Wend
            End If
            
        Case "frmESTRMATBuscar"
            inum = 0
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmESTRMATBuscar.sdbgAtributos
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        'Comprobar que no existe ya
                        bAddAtrib = True
                        iNumAtrib = 0
                        Do While iNumAtrib <= .Rows
                            .Bookmark = iNumAtrib
                            If sdbgAtributosGrupoCtrl.Columns("ID").Value = .Columns("ID_ATRIB").Value Then
                                .Columns("USAR").Value = "1"
                                bAddAtrib = False
                                Exit Do
                            End If
                            
                            iNumAtrib = iNumAtrib + 1
                        Loop
                        
                        If bAddAtrib Then
                            .AddNew
                            .Columns("USAR").Value = "1"
                            .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                            .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            .Columns("INTRO").Value = Abs(CInt(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value))
                            .Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                            .Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            .Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            .Update
                            .Refresh
                        End If
                        
                        Set oatrib = frmESTRMATBuscar.addAtributo(m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                        If Not oatrib Is Nothing Then
                            If oatrib.uons.Count > 0 Then
                                oatrib.AmbitoAtributo = TAmbitoUon
                            Else
                                oatrib.AmbitoAtributo = TAmbitoMaterial
                            End If
                        End If
                    End With
                    
                    inum = inum + 1
                Wend
            End If
        
        Case "frmLstMAT"
            inum = 0
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmLstMAT.sdbgAtributos
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        'Comprobar que no existe ya
                        bAddAtrib = True
                        iNumAtrib = 0
                        Do While iNumAtrib <= .Rows
                            .Bookmark = iNumAtrib
                            If sdbgAtributosGrupoCtrl.Columns("ID").Value = .Columns("ID_ATRIB").Value Then
                                .Columns("USAR").Value = "1"
                                bAddAtrib = False
                                Exit Do
                            End If
                            
                            iNumAtrib = iNumAtrib + 1
                        Loop
                        
                        If bAddAtrib Then
                            .AddNew
                            .Columns("USAR").Value = "1"
                            .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                            .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            .Columns("INTRO").Value = Abs(CInt(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value))
                            .Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                            .Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            .Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            .Update
                            .Refresh
                        End If
                        
                        Set oatrib = frmLstMAT.addAtributo(m_oAtributos.Item(iNumAtrib))
                        If Not oatrib Is Nothing Then
                            If oatrib.uons.Count > 0 Then
                                oatrib.AmbitoAtributo = TAmbitoUon
                            Else
                                oatrib.AmbitoAtributo = TAmbitoMaterial
                            End If
                        End If
                    End With
                    
                    inum = inum + 1
                Wend
            End If
            
        Case "frmLstART"
            inum = 0
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmLstART.sdbgAtributos
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        'Comprobar que no existe ya
                        bAddAtrib = True
                        iNumAtrib = 0
                        Do While iNumAtrib <= .Rows
                            .Bookmark = iNumAtrib
                            If sdbgAtributosGrupoCtrl.Columns("ID").Value = .Columns("ID_ATRIB").Value Then
                                .Columns("USAR").Value = "1"
                                bAddAtrib = False
                                Exit Do
                            End If
                            
                            iNumAtrib = iNumAtrib + 1
                        Loop
                        
                        If bAddAtrib Then
                            .AddNew
                            .Columns("USAR").Value = "1"
                            .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                            .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            .Columns("INTRO").Value = Abs(CInt(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value))
                            .Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                            .Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            .Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            .Update
                            .Refresh
                        End If
                        
                        Set oatrib = frmLstART.addAtributo(m_oAtributos.Item(iNumAtrib))
                        If Not oatrib Is Nothing Then
                            If oatrib.uons.Count > 0 Then
                                oatrib.AmbitoAtributo = TAmbitoUon
                            Else
                                oatrib.AmbitoAtributo = TAmbitoMaterial
                            End If
                        End If
                    End With
                    
                    inum = inum + 1
                Wend
            End If
            
        Case "frmItemsWizard"
            inum = 0
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmItemsWizard.sdbgAtributos
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        'Comprobar que no existe ya
                        bAddAtrib = True
                        iNumAtrib = 0
                        Do While iNumAtrib <= .Rows
                            .Bookmark = iNumAtrib
                            If sdbgAtributosGrupoCtrl.Columns("ID").Value = .Columns("ID_ATRIB").Value Then
                                .Columns("USAR").Value = "1"
                                bAddAtrib = False
                                Exit Do
                            End If
                            
                            iNumAtrib = iNumAtrib + 1
                        Loop
                        
                        If bAddAtrib Then
                            .AddNew
                            .Columns("USAR").Value = "1"
                            .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                            .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            .Columns("GRUPO").Value = "0"
                            .Columns("INTRO").Value = Abs(CInt(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value))
                            .Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                            .Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            .Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            .Update
                            .Refresh
                        End If
                        
                        Set oatrib = frmItemsWizard.addAtributo(m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                        If Not oatrib Is Nothing Then
                            If oatrib.uons.Count > 0 Then
                                oatrib.AmbitoAtributo = TAmbitoUon
                            Else
                                oatrib.AmbitoAtributo = TAmbitoMaterial
                            End If
                        End If
                        
                    End With
                    
                    inum = inum + 1
                Wend
            End If
            
        Case "frmARTFiltrar"
            inum = 0
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmARTFiltrar.sdbgAtributos
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        'Comprobar que no existe ya
                        bAddAtrib = True
                        iNumAtrib = 0
                        Do While iNumAtrib <= .Rows
                            .Bookmark = iNumAtrib
                            If sdbgAtributosGrupoCtrl.Columns("ID").Value = .Columns("ID_ATRIB").Value Then
                                .Columns("USAR").Value = "1"
                                bAddAtrib = False
                                Exit Do
                            End If
                            
                            iNumAtrib = iNumAtrib + 1
                        Loop
                        
                        If bAddAtrib Then
                            .AddNew
                            .Columns("USAR").Value = "1"
                            .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                            .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                            .Columns("INTRO").Value = Abs(CInt(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value))
                            .Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                            .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                            .Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                            .Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                            .Update
                            .Refresh
                        End If
                        
                        Set oatrib = frmARTFiltrar.addAtributo(m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value)))
                        If Not oatrib Is Nothing Then
                            If oatrib.uons.Count > 0 Then
                                oatrib.AmbitoAtributo = TAmbitoUon
                            Else
                                oatrib.AmbitoAtributo = TAmbitoMaterial
                            End If
                        End If
                    End With
                    
                    inum = inum + 1
                Wend
            End If
            
        Case "frmCatalogo", "frmCatalogo_Recep"
            If g_sOrigen = "frmCatalogo_Recep" Then
                Set oGrid = frmCatalogo.sdbgCamposRecepcion
            Else
                Set oGrid = frmCatalogo.sdbgCamposPedido
            End If
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0

                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With oGrid
                        .AddNew
                        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                            sBoton = "..."
                        Else
                            sBoton = ""
                        End If
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)

                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value) = .Columns("ATRIBID").CellValue(vbm) Then
                                    .CancelUpdate
                                    GoTo SiguienteAtribCatalogo
                                End If
                            Next inum2
                        End If
                        .Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                        .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                        .Columns("OBL").Value = 0
                        .Columns("VAL").Value = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                        .Columns("VAL").Text = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
                        .Columns("AMB").Value = 0
                        .Columns("INT").Value = 0
                        .Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                        .Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                        .Columns("TIPO_INTRODUCCION").Value = IIf((sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1), 1, 0)
                        .Columns("TIPO_DATOS").Value = sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                        .Columns("ATRIBID").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                        .Columns("ID").Value = 0
                        .Columns("LISTA_EXTERNA").Value = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaExterna
                        If g_sOrigen = "frmCatalogo" Then .Columns("VERENRECEP").Value = 0
                        '2. A�ADIR A LA VARIABLE DE CCAMPOS DE FRMCATALOGO
                        frmCatalogo.g_oCampos.Add 0, .Columns("COD").Text, .Columns("DEN").Text, 0, .Columns("VALOR").Value, 0, 0, _
                            .Columns("TIPO_INTRODUCCION").Value, .Columns("MIN").Value, .Columns("MAX").Value, .Columns("TIPO_DATOS").Value, .Columns("ATRIBID").Value, , , , , , _
                            m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaExterna, False
                        .Update
                        .Refresh
                    End With
SiguienteAtribCatalogo:
                    inum = inum + 1
                Wend
           End If
           oGrid.MoveLast
           oGrid.Columns("VAL").DropDownHwnd = 0
        Case "frmCatalogoAtribEspYAdj"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmCatalogoAtribEspYAdj.sdbgCamposEsp
                        .AddNew
                        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                            sBoton = "..."
                        Else
                            sBoton = ""
                        End If
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value) = .Columns("ATRIBID").CellValue(vbm) Then
                                    .CancelUpdate
                                    GoTo SiguienteAtribCatalogoAtribEspYAdj
                                End If
                            Next inum2
                        End If
                        '1. A�ADIR AL GRID DE CAMPOS DE PEDIDO DE FRMCATALOGO
                        .Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                        .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                        .Columns("VAL").Value = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                        .Columns("VAL").Text = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
                        .Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                        .Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                        .Columns("TIPO_INTRODUCCION").Value = IIf((sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1), 1, 0)
                        .Columns("TIPO_DATOS").Value = sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                        .Columns("ATRIBID").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                        .Columns("ORICOD").Value = AtributosLineaCatalogo.MaestroAtributos
                        .Columns("ORI").Value = m_sMaestroAtributos
                        Dim valorText As Variant
                        Dim valorNum As Variant
                        Dim valorFec As Variant
                        Dim valorBool As Variant
                        valorText = Null
                        valorNum = Null
                        valorFec = Null
                        valorBool = Null
                        Select Case .Columns("TIPO_DATOS").Value
                            Case TiposDeAtributos.TipoString
                                valorText = .Columns("VAL").Text
                            Case TiposDeAtributos.TipoNumerico
                                valorNum = .Columns("VAL").Text
                            Case TiposDeAtributos.TipoFecha
                                valorFec = .Columns("VAL").Text
                            Case TiposDeAtributos.TipoBoolean
                                valorBool = .Columns("VAL").Text
                        End Select
                        frmCatalogoAtribEspYAdj.g_oAtributosLinea.Add 0, .Columns("COD").Text, .Columns("DEN").Text _
                                    , .Columns("TIPO_DATOS").Value, , , , , , , , inum, .Columns("VALOR").Value _
                                    , , , , , , , .Columns("TIPO_INTRODUCCION").Value, .Columns("MIN").Value _
                                    , .Columns("MAX").Value, , , , , , , , , , , , , , , , , , , , , valorNum, valorText, valorFec, valorBool, , , _
                                    , .Columns("ATRIBID").Value, , , , , , .Columns("ORICOD").Value
                        .Update
                        .Refresh
                    End With
SiguienteAtribCatalogoAtribEspYAdj:
                    inum = inum + 1
                Wend
           End If
           frmCatalogoAtribEspYAdj.sdbgCamposEsp.MoveLast
           frmCatalogoAtribEspYAdj.sdbgCamposEsp.Columns("VAL").DropDownHwnd = 0
        Case "frmSeguimientoArtBuscar", "frmESTArtBuscar"
            inum = 0
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                Select Case g_sOrigen
                    Case "frmSeguimientoArtBuscar"
                        Set oGrid = frmSeguimiento.sdbgAtrBuscar
                        Set oCombo = frmSeguimiento.sdbddAmbito
                        i = TAtributoAmbito.CabeceraPedidoAtr
                    Case "frmESTArtBuscar"
                        Set oGrid = frmEST.sdbgAtrBuscar
                        Set oCombo = frmEST.sdbddAmbito
                        i = TAtributoAmbito.ProcesoAtr
                End Select
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With oGrid
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        
                        .AddNew
                        .Columns("COD").Value = sdbgAtributosGrupoCtrl.Columns("COD").Value
                        .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                        .Columns("INTRO").Value = Abs(CInt(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value))
                        .Columns("IDTIPO").Value = sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value
                        .Columns("ID_ATRIB").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                        .Columns("MAXIMO").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                        .Columns("MINIMO").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                        .Columns("NUM").Value = inum + 1
                        .Columns("AMBITO_VALOR").Value = i
                         ComboValorAtrBuscar_PositionList oCombo, .Columns("AMBITO_VALOR").Value, oGrid
                        .Columns("AMBITO").Value = oCombo.Columns(1).Value
                        .Columns("LISTAEXTERNA").Value = sdbgAtributosGrupoCtrl.Columns("LISTA_EXTERNA").Value
                        .Update
                        
                    End With
                    inum = inum + 1
                Wend
                Set oGrid = Nothing
            End If
            
        Case "frmCONFINST"
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0

                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    With frmCONFINST.sdbgCamposPedido
                        .AddNew
                        bAddAtrib = True
                        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                            sBoton = "..."
                        Else
                            sBoton = ""
                        End If
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)

                        'Comprobar que no existe ya
                        If .Rows > 0 Then
                            For inum2 = 0 To .Rows - 1
                                vbm = .AddItemBookmark(inum2)
                                If CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value) = .Columns("ATRIBID").CellValue(vbm) Then
                                    .CancelUpdate
                                    .Bookmark = vbm
                                    bAddAtrib = False
                                    GoTo SiguienteAtribDefecto
                                End If
                            Next inum2
                        End If
                        .Columns("COD").Text = sdbgAtributosGrupoCtrl.Columns("COD").Value
                        .Columns("DEN").Value = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
                        .Columns("VAL").Value = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                        .Columns("VAL").Text = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
                        .Columns("MIN").Value = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
                        .Columns("MAX").Value = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
                        .Columns("TIPO_INTRODUCCION").Value = IIf((sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1), 1, 0)
                        .Columns("TIPO_DATOS").Value = sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
                        .Columns("ATRIBID").Value = sdbgAtributosGrupoCtrl.Columns("ID").Value
                        .Columns("ESINTEGRA").Value = 0
                        .Columns("LISTA_EXTERNA").Value = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaExterna
                        
                        Set oatrib = frmCONFINST.g_oAtribsAnyadir.Add(.Columns("ATRIBID").Value, .Columns("COD").Text, .Columns("DEN").Text, .Columns("TIPO_DATOS").Text, , , , , , , , , _
                            .Columns("VAL").Value, , , , , , , .Columns("TIPO_INTRODUCCION").Text, .Columns("MIN").Value, .Columns("MAX").Value, , , , , , , , , , , , , , _
                            , , , , , , , , , , , , , , , , , , , , MaestroAtributos)
                        oatrib.ListaExterna = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaExterna
                        
                        If oatrib.ListaExterna Then
                            .Columns("DEN").Value = .Columns("DEN").Value & " " & g_sGmn1
                        End If
                        
                        If oatrib.Tipo = TipoBoolean Then
                            If oatrib.valor = m_sIdiTrue Then
                                oatrib.valor = 1
                            ElseIf oatrib.valor = m_sIdiFalse Then
                                oatrib.valor = 0
                            End If
                        End If
                        .Update
                        .Refresh
                    End With
SiguienteAtribDefecto:
                    inum = inum + 1
                Wend
           End If
           If bAddAtrib Then frmCONFINST.sdbgCamposPedido.MoveLast
           frmCONFINST.sdbgCamposPedido.Columns("VAL").DropDownHwnd = 0
                            
        Case Else
            'Estructura de materiales
            ReDim aIdentificadores(sdbgAtributosGrupoCtrl.SelBookmarks.Count)
            ReDim aBookmarks(sdbgAtributosGrupoCtrl.SelBookmarks.Count)
        
            If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
                inum = 0
                If frmESTRMATAtrib.g_sOrigen = "GMN4" Then Set frmESTRMATAtrib.dicNuevosAtrib = New Dictionary
                
                While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
                    '3328
                    If frmESTRMATAtrib.g_sOrigen = "TIPPED" Then
                        Set oAtributoAnyadirTP = oFSGSRaiz.Generar_CTipoPedido
                    
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                    
                        'Se guarda atributo junto con su valor por defecto en ART4_ATRIB o en GMN4_ATRIB.
                        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = 4 Then  'Si es tipo boolean
                            If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = m_sIdiTrue Then
                                teserror = oAtributoAnyadirTP.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), g_sPedId, True, sdbgAtributosGrupoCtrl.Columns("idTIPO").Value)
                            ElseIf sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = m_sIdiFalse Then
                                teserror = oAtributoAnyadirTP.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), g_sPedId, False, sdbgAtributosGrupoCtrl.Columns("idTIPO").Value)
                            Else
                                teserror = oAtributoAnyadirTP.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), g_sPedId, "", sdbgAtributosGrupoCtrl.Columns("idTIPO").Value)
                            End If
                        Else 'Resto de tipos
                            teserror = oAtributoAnyadirTP.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), g_sPedId, sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value, sdbgAtributosGrupoCtrl.Columns("idTIPO").Value)
                        End If
                    
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            cmdDeshacer_Click
                            If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                            GoTo SiguienteA
                        End If
                        
                        frmESTRMATAtrib.sdbgAtributos.Update
                    Else
                        Set oAtributoAnyadir = oFSGSRaiz.Generar_CGrupoMatNivel4
                    
                        oAtributoAnyadir.Cod = frmESTRMAT.ssMateriales.ActiveRow.Cells("COD").Value
                        oAtributoAnyadir.GMN3Cod = frmESTRMAT.ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                        oAtributoAnyadir.GMN2Cod = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                        oAtributoAnyadir.GMN1Cod = frmESTRMAT.ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
                        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
                        If frmESTRMATAtrib.g_sOrigen = "GMN4" Then
                            sArtCod = Null
                        Else
                            sArtCod = frmESTRMAT.sdbgArticulos.Columns("COD").Value
                        End If
                    
                        'Se guarda atributo junto con su valor por defecto en ART4_ATRIB o en GMN4_ATRIB.
                        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = 4 Then  'Si es tipo boolean
                            If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = m_sIdiTrue Then
                                teserror = oAtributoAnyadir.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), sArtCod, True, sdbgAtributosGrupoCtrl.Columns("idTIPO").Value, oUsuarioSummit.Cod)
                            ElseIf sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = m_sIdiFalse Then
                                teserror = oAtributoAnyadir.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), sArtCod, False, sdbgAtributosGrupoCtrl.Columns("idTIPO").Value, oUsuarioSummit.Cod)
                            Else
                                teserror = oAtributoAnyadir.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), sArtCod, "", sdbgAtributosGrupoCtrl.Columns("idTIPO").Value, oUsuarioSummit.Cod)
                            End If
                        Else 'Resto de tipos
                            teserror = oAtributoAnyadir.AsignarAtributo(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ID").Value), sArtCod, sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value, sdbgAtributosGrupoCtrl.Columns("idTIPO").Value, oUsuarioSummit.Cod)
                        End If
                    
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            cmdDeshacer_Click
                            If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                            GoTo SiguienteA
                        End If
                        
                        If frmESTRMATAtrib.g_sOrigen = "GMN4" Then frmESTRMATAtrib.dicNuevosAtrib.Add sdbgAtributosGrupo.Columns("ID").Value, sdbgAtributosGrupo.Columns("COD").Value
                        
                        frmESTRMATAtrib.sdbgAtributos.Update
                    End If
SiguienteA:
                    inum = inum + 1
                Wend
            End If
            Unload m_oContainer
            frmESTRMATAtrib.cmdRestaurar_Click
            frmESTRMATAtrib.SetFocus
    End Select

    Unload m_oContainer
End Sub
Private Sub cmdSelMat_Click()

    frmSELMAT.sOrigen = g_sOrigen & "frmATRIB"
    frmSELMAT.bRComprador = bRComprador
    frmSELMAT.Show 1

End Sub

''' <summary>
''' Inicializa el user control
''' </summary>
''' <param name="ParametroEntrada1">Explicaci�npar�metro1</param>
''' <param name="ParametroEntrada2">Explicaci�npar�metro2</param>
''' <returns>Explicaci�nretornodelafunci�n</returns>
''' <remarks>Llamada desde: xxx ; Tiempo m�ximo: 0</remarks>
Public Sub Initialize()

    CargarRecursos
    ConfigurarSeguridad
    ConfigurarNombres
    
    sdbddTipo.Enabled = False
    sdbddTipo.AddItem " "
    sdbgAtributosGrupoCtrl.Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
    sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = True
    
    m_bCargando = False
    If Not g_bEdicion Then
        sdbgAtributosGrupoCtrl.Columns("OFERTA").Locked = True
        sdbgAtributosGrupoCtrl.Columns("COSTE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("COD").Locked = True
        sdbgAtributosGrupoCtrl.Columns("NOMBRE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = True
        sdbgAtributosGrupoCtrl.Columns("ASC").Locked = True
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = True
        sdbddTipo.Enabled = False
        sdbddValor.Enabled = False
        If g_sOrigen <> "MDI" Then
            cmdSeleccionarCtrl.Visible = True
        End If
        
    Else
        sdbgAtributosGrupoCtrl.Columns("OFERTA").Locked = False
        sdbgAtributosGrupoCtrl.Columns("COSTE").Locked = False
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Locked = False
        sdbgAtributosGrupoCtrl.Columns("COD").Locked = False
        sdbgAtributosGrupoCtrl.Columns("NOMBRE").Locked = False
        'sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = False
        sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = False
        sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = False
        sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = False
        sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = False
        sdbgAtributosGrupoCtrl.Columns("ASC").Locked = False
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = False
        sdbddTipo.Enabled = True
        sdbddValor.Enabled = False
    End If
    sdbgAtributosGrupoCtrl.Columns("BOT").Locked = False
    sdbgAtributosGrupoCtrl.Columns("POND").Locked = False
    
    If g_bSoloSeleccion Then
        cmdModoEdicionCtrl.Visible = False
        cmdModoEdicion2Ctrl.Visible = False
    End If
    
    m_bRespetarCheck = False
    m_bPrimera = False
    
    PonerFieldSeparator
    
    Select Case g_sOrigen
    Case "FRMATRIBRELACION"
        Option8.Value = True
    Case "frmCatalogoCostesDescuentos_Costes", "frmCatalogo_Costes"
        chkCostes.Value = vbChecked
    Case "frmCatalogoCostesDescuentos_Descuentos", "frmCatalogo_Descuentos"
        chkDtos.Value = vbChecked
    End Select
    

End Sub

Public Sub PonerFieldSeparator()
    Dim sdg As Control
    Dim Name4 As String
    Dim Name5 As String
        
    For Each sdg In UserControl.Controls
        
        Name4 = LCase(Left(sdg.Name, 4))
        Name5 = LCase(Left(sdg.Name, 5))
        
        If Name4 = "sdbg" Or Name4 = "sdbc" Or Name5 = "sdbdd" Or Name4 = "ssdb" Then
            If sdg.DataMode = ssDataModeAddItem Then
                sdg.FieldSeparator = Chr(m_lSeparador)
            End If
        End If
    Next
End Sub



''' <summary>
''' Evento que salta al producirse un cambio en el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgAtributosGrupoCtrl_Click()
    If Not SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value) Then
        If sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(1) Then
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Style = ssStyleEditButton
            DoEvents
        End If
    End If
    If sdbgAtributosGrupoCtrl.Columns("COSTE").Locked = True Then Exit Sub
    
    If sdbgAtributosGrupoCtrl.col = 1 Or sdbgAtributosGrupoCtrl.col = 2 Then
        If Not (CStr(sdbgAtributosGrupoCtrl.Columns("TIPO").Value) = m_sCombo(2)) _
        And Not (sdbgAtributosGrupoCtrl.IsAddRow) Then
            sdbgAtributosGrupoCtrl.Columns("COSTE").Value = 0
            sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = 0
        ElseIf Not (CStr(sdbgAtributosGrupoCtrl.Columns("TIPO").Value) = m_sCombo(2)) _
        And Not (CStr(sdbgAtributosGrupoCtrl.Columns("TIPO").Value) = "") _
        And (sdbgAtributosGrupoCtrl.IsAddRow) Then
            sdbgAtributosGrupoCtrl.Columns("COSTE").Value = 0
            sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = 0
        ElseIf sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "1" Or sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "-1" Then
            sdbgAtributosGrupoCtrl.Columns("COSTE").Value = 0
            sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = 0
        ElseIf sdbgAtributosGrupoCtrl.col = 1 And sdbgAtributosGrupoCtrl.Columns("COSTE").Value = "0" Then 'Se activa coste
            sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = 0
        ElseIf sdbgAtributosGrupoCtrl.col = 2 And sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = "0" Then 'Se activa descuento
            sdbgAtributosGrupoCtrl.Columns("COSTE").Value = 0
        End If
    End If
    
End Sub

Private Sub UserControl_Initialize()
    If Not oFSGSRaiz Is Nothing Then Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
End Sub

''' <summary>
''' Redimensionar el control
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub UserControl_Resize()
    If UserControl.Width > 500 And UserControl.Height > 2900 Then
    
        sstabgeneralCtrl.Width = UserControl.Width
        sstabgeneralCtrl.Height = UserControl.Height
        sdbgAtributosGrupoCtrl.Width = sstabgeneralCtrl.Width - 215
        sdbgAtributosGrupoCtrl.Height = sstabgeneralCtrl.Height - 1080
        sdbgAtributosGrupoCtrl.Top = 540
        
        If sdbgAtributosGrupoCtrl.Rows > 0 Then
            Dim vbm As Variant
            vbm = sdbgAtributosGrupoCtrl.Bookmark
            sdbgAtributosGrupoCtrl.Bookmark = 0
        End If
        
        If basParametros.gParametrosGenerales.gbUsarPonderacion Then
            sdbgAtributosGrupoCtrl.Groups(3).Visible = True
            sdbgAtributosGrupoCtrl.Groups(0).Width = sdbgAtributosGrupoCtrl.Width * 0.4
            sdbgAtributosGrupoCtrl.Groups(1).Width = sdbgAtributosGrupoCtrl.Width * 0.353
            'sdbgAtributosGrupoCtrl.Groups(2).Width = sdbgAtributosGrupoCtrl.Width * 0.094
            sdbgAtributosGrupoCtrl.Groups(2).Width = sdbgAtributosGrupoCtrl.Width * 0.07
            'sdbgAtributosGrupoCtrl.Groups(3).Width = sdbgAtributosGrupoCtrl.Width * 0.1
            sdbgAtributosGrupoCtrl.Groups(3).Width = sdbgAtributosGrupoCtrl.Width * 0.064
            sdbgAtributosGrupoCtrl.Groups(4).Width = sdbgAtributosGrupoCtrl.Width * 0.08
        Else
            sdbgAtributosGrupoCtrl.Groups(3).Visible = False
            'sdbgAtributosGrupoCtrl.Groups(0).Width = sdbgAtributosGrupoCtrl.Width * 0.48
            'sdbgAtributosGrupoCtrl.Groups(1).Width = sdbgAtributosGrupoCtrl.Width * 0.33
            'sdbgAtributosGrupoCtrl.Groups(2).Width = sdbgAtributosGrupoCtrl.Width * 0.13
            sdbgAtributosGrupoCtrl.Groups(0).Width = sdbgAtributosGrupoCtrl.Width * 0.45
            sdbgAtributosGrupoCtrl.Groups(1).Width = sdbgAtributosGrupoCtrl.Width * 0.32
            sdbgAtributosGrupoCtrl.Groups(2).Width = sdbgAtributosGrupoCtrl.Width * 0.12
            sdbgAtributosGrupoCtrl.Groups(3).Width = 0
            sdbgAtributosGrupoCtrl.Groups(4).Width = sdbgAtributosGrupoCtrl.Width * 0.05
                        
            sdbgAtributosGrupoCtrl.Columns("POND").Width = 0
            sdbgAtributosGrupoCtrl.Columns("POND").Visible = False
            
        End If
        'sdbgAtributosGrupoCtrl.Columns("COD").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.22
        'sdbgAtributosGrupoCtrl.Columns("NOMBRE").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.35
        'sdbgAtributosGrupoCtrl.Columns("DESCR").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.15
        'sdbgAtributosGrupoCtrl.Columns("TIPO").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.28
        
        sdbgAtributosGrupoCtrl.Columns("COD").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.18
        sdbgAtributosGrupoCtrl.Columns("NOMBRE").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.32
        sdbgAtributosGrupoCtrl.Columns("DESCR").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.15
        sdbgAtributosGrupoCtrl.Columns("TIPO").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.2
        sdbgAtributosGrupoCtrl.Columns("OFERTA").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.1
        sdbgAtributosGrupoCtrl.Columns("COSTE").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.1
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Width = sdbgAtributosGrupoCtrl.Groups(0).Width * 0.15
        
        
        sdbgAtributosGrupoCtrl.Columns("LIBRE").Width = sdbgAtributosGrupoCtrl.Groups(1).Width * 0.15
        sdbgAtributosGrupoCtrl.Columns("MINIMO").Width = sdbgAtributosGrupoCtrl.Groups(1).Width * 0.262
        sdbgAtributosGrupoCtrl.Columns("MAXIMO").Width = sdbgAtributosGrupoCtrl.Groups(1).Width * 0.262
        sdbgAtributosGrupoCtrl.Columns("SELECCION").Width = sdbgAtributosGrupoCtrl.Groups(1).Width * 0.22
        sdbgAtributosGrupoCtrl.Columns("BOT").Width = sdbgAtributosGrupoCtrl.Groups(1).Width * 0.1145
        
        sdbgAtributosGrupoCtrl.Columns("ASC").Width = sdbgAtributosGrupoCtrl.Groups(2).Width
        
        sdbgAtributosGrupoCtrl.Columns("BAJA").Width = sdbgAtributosGrupoCtrl.Groups(3).Width

        picEdit.Top = sstabgeneralCtrl.Height - 525
        picNavigate.Top = sstabgeneralCtrl.Height - 525
        picNavigate.Width = sstabgeneralCtrl.Width - 100
        cmdModoEdicion2Ctrl.Left = picNavigate.Width - cmdModoEdicion2Ctrl.Width - 100
        picEdit.Width = sstabgeneralCtrl.Width - 100
        cmdModoEdicionCtrl.Left = picEdit.Width - cmdModoEdicionCtrl.Width - 100
        
        cmdCodigo.Visible = m_bModifCodigo
        cmdBajaLog.Visible = m_bBajasLog
        If Not cmdCodigo.Visible Then
            If cmdBajaLog.Visible Then
                cmdDeshacer.Left = cmdBajaLog.Left
                cmdBajaLog.Left = cmdCodigo.Left
            Else
                cmdDeshacer.Left = 2130
            End If
        End If
        If basOptimizacion.gTipoDeUsuario <> TIpoDeUsuario.Administrador Then
            If m_bBajasLog Then
                If Not m_bPrimera Then m_bPrimera = True
            End If
        End If
        
        If sdbgAtributosGrupoCtrl.Rows > 0 Then
            'Recup�ro el bookmark y si no se ve la fila la pongo la priemra visible
            sdbgAtributosGrupoCtrl.Bookmark = vbm
            If sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.Bookmark) < sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.FirstRow) Or sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.Bookmark) > sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.FirstRow) + sdbgAtributosGrupoCtrl.VisibleRows - 1 Then
                sdbgAtributosGrupoCtrl.FirstRow = sdbgAtributosGrupoCtrl.Bookmark
            End If
        End If
        
        
    End If
    
End Sub

Private Sub UserControl_Terminate()
    If Not m_oIBAseDatosEnEdicion Is Nothing Then
        m_oIBAseDatosEnEdicion.CancelarEdicion
        Set m_oIBAseDatosEnEdicion = Nothing
    End If
    Set g_oAtributoEnEdicion = Nothing
    Set m_oAtributoAnyadir = Nothing
    Set m_oGruposMN1 = Nothing
    Set m_oGruposMN2 = Nothing
    Set m_oGruposMN3 = Nothing
    Set m_oGruposMN4 = Nothing
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
    Set m_oAtributo = Nothing
    Set m_oAtributos = Nothing
    m_bCargando = False
    g_bEdicion = False
    
    Select Case g_sOrigen
        Case "ART4"
            frmESTRMATAtrib.cmdRestaurar_Click
            frmESTRMATAtrib.SetFocus
        Case "frmPEDIDOSLP"
            g_oOrigen.g_sItem_AtributosLinea = ""
    End Select
    Set m_oUonsSeleccionadas = Nothing
End Sub

Private Sub sdbcArtiCod_Click()
    If Not sdbcArtiCod.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiCod_CloseUp()
 
    If sdbcArtiCod.Value = "..." Then
        sdbcArtiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiCod.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    g_sArtCod = sdbcArtiCod.Text
    
    m_bRespetarCombo = False
    
    ArticuloSeleccionado
    
    m_bCargarComboDesde = False
End Sub

Private Sub sdbcArtiCod_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcArtiDen.Text = ""
        m_bRespetarCombo = False
        Set g_oArtiSeleccionado = Nothing
        m_bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiCod_DropDown()
    Dim oArt As CArticulo
    
    
    sdbcArtiCod.RemoveAll

    Screen.MousePointer = vbHourglass
    GMN4Seleccionado
    If m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod.Value, , False
    
    For Each oArt In m_oGMN4Seleccionado.ARTICULOS
        sdbcArtiCod.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den & Chr(m_lSeparador) & oArt.GMN1Cod & Chr(m_lSeparador) & oArt.GMN2Cod & Chr(m_lSeparador) & oArt.GMN3Cod & Chr(m_lSeparador) & oArt.GMN4Cod
    Next
    If m_oGMN4Seleccionado.ARTICULOS.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcArtiCod.AddItem "..."
    End If
    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiCod_InitColumnProps()
    
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArtiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcArtiCod.Rows - 1
            bm = sdbcArtiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcArtiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcArtiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcArtiCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    
    
    If sdbcArtiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (m_oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcArtiDen.Text = m_oGMN4Seleccionado.ARTICULOS.Item(1).Den
        
        sdbcArtiCod.Columns(0).Value = sdbcArtiCod.Text
        sdbcArtiCod.Columns(1).Value = sdbcArtiDen.Text
        
        m_bRespetarCombo = False
        ArticuloSeleccionado
        m_bCargarComboDesde = False
    End If
    If Not m_oGMN4Seleccionado Is Nothing Then
        Set m_oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcArtiDen_Click()
    
    If Not sdbcArtiDen.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiDen_CloseUp()
    
    If sdbcArtiDen.Value = "..." Then
        sdbcArtiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiDen.Value = "" Then Exit Sub
    
    m_bRespetarCombo = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    g_sArtCod = sdbcArtiCod.Text
    
    m_bRespetarCombo = False
    
    ArticuloSeleccionado
    
    m_bCargarComboDesde = False
End Sub

Private Sub sdbcArtiDen_Change()
    
    If Not m_bRespetarCombo Then
    
        m_bRespetarCombo = True
        sdbcArtiCod.Text = ""
        m_bRespetarCombo = False
        Set g_oArtiSeleccionado = Nothing
        m_bCargarComboDesde = True
        
    End If
End Sub

Private Sub sdbcArtiDen_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiDen.RemoveAll
   
    Screen.MousePointer = vbHourglass
    
    GMN4Seleccionado

    If m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If m_bCargarComboDesde Then
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
    Else
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , True
    End If
    
    
    For Each oArt In m_oGMN4Seleccionado.ARTICULOS
        sdbcArtiDen.AddItem oArt.Den & Chr(m_lSeparador) & oArt.Cod & Chr(m_lSeparador) & oArt.GMN1Cod & Chr(m_lSeparador) & oArt.GMN2Cod & Chr(m_lSeparador) & oArt.GMN3Cod & Chr(m_lSeparador) & oArt.GMN4Cod
    Next
    
    If m_oGMN4Seleccionado.ARTICULOS.Count = gParametrosInstalacion.giCargaMaximaCombos Then
        sdbcArtiDen.AddItem "..."
    End If
    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiDen_InitColumnProps()
    
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArtiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcArtiDen.Rows - 1
            bm = sdbcArtiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcArtiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcArtiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcArtiDen_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
    
    
    If sdbcArtiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (m_oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiDen.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcArtiCod.Text = m_oGMN4Seleccionado.ARTICULOS.Item(1).Cod
        
        sdbcArtiDen.Columns(0).Value = sdbcArtiDen.Text
        sdbcArtiDen.Columns(1).Value = sdbcArtiCod.Text
        
        m_bRespetarCombo = False
        ArticuloSeleccionado
        m_bCargarComboDesde = False
    End If
    If Not m_oGMN4Seleccionado Is Nothing Then
       Set m_oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcGMN1_4CodCtrl_CloseUp()
    
    If sdbcGMN1_4CodCtrl.Value = "..." Then
        sdbcGMN1_4CodCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn1 = sdbcGMN1_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN1_4CodCtrl.Value = "" Then Exit Sub
    
    g_GMN1RespetarCombo = True
    sdbcGMN1_4DenCtrl.Text = sdbcGMN1_4CodCtrl.Columns(1).Text
    sdbcGMN1_4CodCtrl.Text = sdbcGMN1_4CodCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn1 = sdbcGMN1_4CodCtrl.Text
    End If
    g_GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_GMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4CodCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4CodCtrl.RemoveAll
    
    If bRComprador Then
        
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_GMN1CargarComboDesde Then
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4CodCtrl), , , , False)
        Else
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
          If m_GMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4CodCtrl), , , False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4CodCtrl.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If m_GMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4CodCtrl.AddItem "..."
    End If

    sdbcGMN1_4CodCtrl.SelStart = 0
    sdbcGMN1_4CodCtrl.SelLength = Len(sdbcGMN1_4CodCtrl.Text)
    sdbcGMN1_4CodCtrl.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4CodCtrl_InitColumnProps()
    
    sdbcGMN1_4CodCtrl.DataFieldList = "Column 0"
    sdbcGMN1_4CodCtrl.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4CodCtrl_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4CodCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4CodCtrl.Rows - 1
            bm = sdbcGMN1_4CodCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4CodCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4CodCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Public Sub sdbcGMN1_4CodCtrl_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4CodCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass

    If bRComprador Then
        Set oIMAsig = oUsuarioSummit.comprador
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4CodCtrl), , True, , False)
    Else
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4CodCtrl.Text, , True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set m_oGMN1Seleccionado = Nothing
        sdbcGMN1_4CodCtrl.Text = ""
        sdbcGMN2_4CodCtrl.Text = ""
    Else
        g_GMN1RespetarCombo = True
        sdbcGMN1_4DenCtrl.Text = oGMN1s.Item(1).Den
        sdbcGMN1_4CodCtrl.Columns(0).Value = sdbcGMN1_4CodCtrl.Text
        sdbcGMN1_4CodCtrl.Columns(1).Value = sdbcGMN1_4DenCtrl.Text
        
        g_GMN1RespetarCombo = False
        GMN1Seleccionado
        m_GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN1_4DenCtrl_InitColumnProps()
    
    sdbcGMN1_4DenCtrl.DataFieldList = "Column 0"
    sdbcGMN1_4DenCtrl.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4DenCtrl_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN1_4DenCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN1_4DenCtrl.Rows - 1
            bm = sdbcGMN1_4DenCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN1_4DenCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN1_4DenCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If


End Sub

Private Sub sdbcGMN1_4DenCtrl_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If sdbcGMN1_4DenCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRComprador Then
         Set oIMAsig = oUsuarioSummit.comprador
         Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4DenCtrl), True, , False)
    Else
         oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4DenCtrl.Text, True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set m_oGMN1Seleccionado = Nothing
        sdbcGMN1_4DenCtrl.Text = ""
    Else
        g_GMN1RespetarCombo = True
        sdbcGMN1_4CodCtrl.Text = oGMN1s.Item(1).Cod
        sdbcGMN1_4DenCtrl.Columns(0).Value = sdbcGMN1_4DenCtrl.Text
        sdbcGMN1_4DenCtrl.Columns(1).Value = sdbcGMN1_4CodCtrl.Text
        g_GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing

End Sub


Private Sub sdbcGMN2_4CodCtrl_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN2_4CodCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN2_4CodCtrl.Rows - 1
            bm = sdbcGMN2_4CodCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN2_4CodCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN2_4CodCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Public Sub sdbcGMN2_4CodCtrl_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4CodCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, Trim(sdbcGMN2_4CodCtrl), , True, , False)
        Else
             m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4CodCtrl.Text, , True, , False
            Set oGMN2s = m_oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4CodCtrl.Text = ""
    Else
        g_GMN2RespetarCombo = True
        sdbcGMN2_4DenCtrl.Text = oGMN2s.Item(1).Den
        sdbcGMN2_4CodCtrl.Columns(0).Value = sdbcGMN2_4CodCtrl.Text
        sdbcGMN2_4CodCtrl.Columns(1).Value = sdbcGMN2_4DenCtrl.Text
        g_GMN2RespetarCombo = False
        GMN2Seleccionado
        m_GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    
End Sub

Private Sub sdbcGMN2_4DenCtrl_InitColumnProps()
    
    sdbcGMN2_4DenCtrl.DataFieldList = "Column 0"
    sdbcGMN2_4DenCtrl.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4DenCtrl_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN2_4DenCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN2_4DenCtrl.Rows - 1
            bm = sdbcGMN2_4DenCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN2_4DenCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN2_4DenCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcGMN2_4DenCtrl_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If sdbcGMN2_4DenCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, , Trim(sdbcGMN2_4DenCtrl), True, , False)
        Else
             m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4DenCtrl.Text, True, , False
            Set oGMN2s = m_oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4DenCtrl.Text = ""
    Else
        g_GMN2RespetarCombo = True
        sdbcGMN2_4CodCtrl.Text = oGMN2s.Item(1).Cod
        sdbcGMN2_4DenCtrl.Columns(0).Value = sdbcGMN2_4DenCtrl.Text
        sdbcGMN2_4DenCtrl.Columns(1).Value = sdbcGMN2_4CodCtrl.Text
        g_GMN2RespetarCombo = False
        GMN2Seleccionado
        m_GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing

End Sub

Private Sub sdbcGMN3_4CodCtrl_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN3_4CodCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN3_4CodCtrl.Rows - 1
            bm = sdbcGMN3_4CodCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN3_4CodCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN3_4CodCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Public Sub sdbcGMN3_4CodCtrl_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4CodCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, sdbcGMN2_4CodCtrl, Trim(sdbcGMN3_4CodCtrl), , True, , False)
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4CodCtrl.Text, , True, , False
            Set oGMN3s = m_oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4CodCtrl.Text = ""
    Else
        g_GMN3RespetarCombo = True
        sdbcGMN3_4DenCtrl.Text = oGMN3s.Item(1).Den
        sdbcGMN3_4CodCtrl.Columns(0).Value = sdbcGMN3_4CodCtrl.Text
        sdbcGMN3_4CodCtrl.Columns(1).Value = sdbcGMN3_4DenCtrl.Text
        g_GMN3RespetarCombo = False
        GMN3Seleccionado
        m_GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN3_4DenCtrl_Change()
    
    If Not g_GMN3RespetarCombo Then

        g_GMN3RespetarCombo = True
        sdbcGMN3_4CodCtrl.Text = ""
        sdbcGMN4_4CodCtrl.Text = ""
        g_GMN3RespetarCombo = False
        Set m_oGMN3Seleccionado = Nothing
        m_GMN3CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN3_4DenCtrl_CloseUp()
  
    If sdbcGMN3_4DenCtrl.Value = "..." Then
        sdbcGMN3_4DenCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn3 = sdbcGMN3_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN3_4DenCtrl.Value = "" Then Exit Sub
    
    g_GMN3RespetarCombo = True
    sdbcGMN3_4CodCtrl.Text = sdbcGMN3_4DenCtrl.Columns(1).Text
    sdbcGMN3_4DenCtrl.Text = sdbcGMN3_4DenCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn3 = sdbcGMN3_4CodCtrl.Text
    End If
    g_GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4DenCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4DenCtrl.RemoveAll
    
    If m_oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN3 = Nothing
        If m_GMN3CargarComboDesde Then
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4CodCtrl), Trim(sdbcGMN2_4CodCtrl), Trim(sdbcGMN3_4CodCtrl), , , , False)
        Else
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4CodCtrl), Trim(sdbcGMN2_4CodCtrl), , , , , False)
        End If
        
    Else
        
           If m_GMN3CargarComboDesde Then
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4CodCtrl), , , False
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = m_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = m_oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4DenCtrl.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_GMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4DenCtrl.AddItem "..."
    End If

    sdbcGMN3_4DenCtrl.SelStart = 0
    sdbcGMN3_4DenCtrl.SelLength = Len(sdbcGMN3_4DenCtrl.Text)
    sdbcGMN3_4DenCtrl.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4DenCtrl_InitColumnProps()
    sdbcGMN3_4DenCtrl.DataFieldList = "Column 0"
    sdbcGMN3_4DenCtrl.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4DenCtrl_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN3_4DenCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN3_4DenCtrl.Rows - 1
            bm = sdbcGMN3_4DenCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN3_4DenCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN3_4DenCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If


End Sub


Private Sub sdbcGMN3_4DenCtrl_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If sdbcGMN3_4DenCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, sdbcGMN2_4CodCtrl, , Trim(sdbcGMN3_4DenCtrl), True, , False)
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4DenCtrl.Text, True, , False
            Set oGMN3s = m_oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4DenCtrl.Text = ""
    Else
        g_GMN3RespetarCombo = True
        sdbcGMN3_4CodCtrl.Text = oGMN3s.Item(1).Cod
        sdbcGMN3_4DenCtrl.Columns(0).Value = sdbcGMN3_4DenCtrl.Text
        sdbcGMN3_4DenCtrl.Columns(1).Value = sdbcGMN3_4CodCtrl.Text
        g_GMN3RespetarCombo = False
        GMN3Seleccionado
        m_GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN4_4CodCtrl_PositionList(ByVal Text As String)

''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN4_4CodCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN4_4CodCtrl.Rows - 1
            bm = sdbcGMN4_4CodCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN4_4CodCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN4_4CodCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Public Sub sdbcGMN4_4CodCtrl_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4CodCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, sdbcGMN2_4CodCtrl, sdbcGMN3_4CodCtrl.Text, Trim(sdbcGMN4_4CodCtrl), , True, , False)
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4CodCtrl.Text, , True, , False
            Set oGMN4s = m_oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4CodCtrl.Text = ""
    Else
        g_GMN4RespetarCombo = True
        sdbcGMN4_4DenCtrl.Text = oGMN4s.Item(1).Den
        sdbcGMN4_4CodCtrl.Columns(0).Value = sdbcGMN4_4CodCtrl.Text
        sdbcGMN4_4CodCtrl.Columns(1).Value = sdbcGMN4_4DenCtrl.Text
        g_GMN4RespetarCombo = False
        GMN4Seleccionado
        m_GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub
Private Sub sdbcGMN4_4DenCtrl_Change()
    
    If Not g_GMN4RespetarCombo Then

        g_GMN4RespetarCombo = True
        sdbcGMN4_4CodCtrl.Text = ""
        g_GMN4RespetarCombo = False
        Set m_oGMN4Seleccionado = Nothing
        m_GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4DenCtrl_CloseUp()

    If sdbcGMN4_4DenCtrl.Value = "..." Then
        sdbcGMN4_4DenCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn4 = sdbcGMN4_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN4_4DenCtrl.Value = "" Then Exit Sub
    
    g_GMN4RespetarCombo = True
    If sdbcGMN4_4CodCtrl.Text <> sdbcGMN4_4DenCtrl.Columns(1).Text Then
        sdbcArtiCod.Text = ""
    End If
    sdbcGMN4_4CodCtrl.Text = sdbcGMN4_4DenCtrl.Columns(1).Text
    sdbcGMN4_4DenCtrl.Text = sdbcGMN4_4DenCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn4 = sdbcGMN4_4CodCtrl.Text
    End If
    g_GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4DenCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4DenCtrl.RemoveAll
    
    If m_oGMN3Seleccionado Is Nothing Then Exit Sub
        
    Screen.MousePointer = vbHourglass
    
    If bRComprador Then

        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN4 = Nothing
        If m_GMN4CargarComboDesde Then
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4CodCtrl), Trim(sdbcGMN2_4CodCtrl), Trim(sdbcGMN3_4CodCtrl), Trim(sdbcGMN4_4CodCtrl), , , , False)
        Else
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4CodCtrl), Trim(sdbcGMN2_4CodCtrl), Trim(sdbcGMN3_4CodCtrl), , , , , False)
        End If
    Else
        
        If m_GMN4CargarComboDesde Then
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4CodCtrl), , , False
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = m_oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = m_oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4DenCtrl.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_GMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4DenCtrl.AddItem "..."
    End If

    sdbcGMN4_4DenCtrl.SelStart = 0
    sdbcGMN4_4DenCtrl.SelLength = Len(sdbcGMN4_4DenCtrl.Text)
    sdbcGMN4_4DenCtrl.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN4_4DenCtrl_InitColumnProps()
    
    sdbcGMN4_4DenCtrl.DataFieldList = "Column 0"
    sdbcGMN4_4DenCtrl.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4DenCtrl_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcGMN4_4DenCtrl.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcGMN4_4DenCtrl.Rows - 1
            bm = sdbcGMN4_4DenCtrl.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcGMN4_4DenCtrl.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcGMN4_4DenCtrl.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcGMN4_4DenCtrl_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If sdbcGMN4_4DenCtrl.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not m_oGMN3Seleccionado Is Nothing Then
    
        Screen.MousePointer = vbHourglass
        If bRComprador Then
            Set oIMAsig = oUsuarioSummit.comprador
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, sdbcGMN2_4CodCtrl, sdbcGMN3_4CodCtrl.Text, , Trim(sdbcGMN4_4DenCtrl), , True, False)
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4DenCtrl.Text, True, , False
            Set oGMN4s = m_oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4DenCtrl.Text = ""
    Else
        g_GMN4RespetarCombo = True
        sdbcGMN4_4CodCtrl.Text = oGMN4s.Item(1).Cod
        sdbcGMN4_4DenCtrl.Columns(0).Value = sdbcGMN4_4DenCtrl.Text
        sdbcGMN4_4DenCtrl.Columns(1).Value = sdbcGMN4_4CodCtrl.Text
        g_GMN4RespetarCombo = False
        GMN4Seleccionado
        m_GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub
Private Sub sdbcGMN1_4CodCtrl_Change()

    If Not g_GMN1RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn1 = ""
        End If
        g_GMN1RespetarCombo = True
        sdbcGMN1_4DenCtrl.Text = ""
        sdbcGMN2_4CodCtrl.Text = ""
        g_GMN1RespetarCombo = False
        Set m_oGMN1Seleccionado = Nothing
        m_GMN1CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN1_4DenCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4DenCtrl.RemoveAll

    If bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN1 = Nothing
        If m_GMN1CargarComboDesde Then
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4DenCtrl), , True, False)
        Else
            Set m_oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set m_oGruposMN1 = Nothing
        Set m_oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
         If m_GMN1CargarComboDesde Then
            m_oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4DenCtrl), True, False
        Else
            m_oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = m_oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4DenCtrl.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_GMN1CargarComboDesde And Not m_oGruposMN1.EOF Then
        sdbcGMN1_4DenCtrl.AddItem "..."
    End If

    sdbcGMN1_4DenCtrl.SelStart = 0
    sdbcGMN1_4DenCtrl.SelLength = Len(sdbcGMN1_4DenCtrl.Text)
    sdbcGMN1_4CodCtrl.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4CodCtrl_Change()
    
    If Not g_GMN2RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn2 = ""
        End If
        g_GMN2RespetarCombo = True
        sdbcGMN2_4DenCtrl.Text = ""
        sdbcGMN3_4CodCtrl.Text = ""
        g_GMN2RespetarCombo = False
        Set m_oGMN2Seleccionado = Nothing
        
        m_GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4CodCtrl_CloseUp()

    If sdbcGMN2_4CodCtrl.Value = "..." Then
        sdbcGMN2_4CodCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn2 = sdbcGMN2_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN2_4CodCtrl.Value = "" Then Exit Sub
    
    g_GMN2RespetarCombo = True
    sdbcGMN2_4DenCtrl.Text = sdbcGMN2_4CodCtrl.Columns(1).Text
    sdbcGMN2_4CodCtrl.Text = sdbcGMN2_4CodCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn2 = sdbcGMN2_4CodCtrl.Text
    End If
    g_GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4CodCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN2_4CodCtrl.RemoveAll

    If m_oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN2 = Nothing
        If m_GMN2CargarComboDesde Then
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, Trim(sdbcGMN2_4CodCtrl), , , , False)
        Else
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4CodCtrl, , , , , False)
        End If
    Else
        
         If m_GMN2CargarComboDesde Then
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4CodCtrl), , , False
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set m_oGruposMN2 = m_oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = m_oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4CodCtrl.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If m_GMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4CodCtrl.AddItem "..."
    End If

    sdbcGMN2_4CodCtrl.SelStart = 0
    sdbcGMN2_4CodCtrl.SelLength = Len(sdbcGMN2_4CodCtrl.Text)
    sdbcGMN2_4CodCtrl.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN2_4DenCtrl_Change()
    
    If Not g_GMN2RespetarCombo Then

        g_GMN2RespetarCombo = True
        sdbcGMN2_4CodCtrl.Text = ""
        sdbcGMN3_4CodCtrl.Text = ""
        g_GMN2RespetarCombo = False
        Set m_oGMN2Seleccionado = Nothing
        m_GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4DenCtrl_CloseUp()
   
    If sdbcGMN2_4DenCtrl.Value = "..." Then
        sdbcGMN2_4DenCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn2 = sdbcGMN2_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN2_4DenCtrl.Value = "" Then Exit Sub
    
    g_GMN2RespetarCombo = True
    sdbcGMN2_4CodCtrl.Text = sdbcGMN2_4DenCtrl.Columns(1).Text
    sdbcGMN2_4DenCtrl.Text = sdbcGMN2_4DenCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn2 = sdbcGMN2_4CodCtrl.Text
    End If
    g_GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    m_GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4DenCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer
    
    sdbcGMN2_4DenCtrl.RemoveAll
    
    If m_oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN2 = Nothing
        If m_GMN2CargarComboDesde Then
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4CodCtrl), Trim(sdbcGMN2_4CodCtrl), , , False)
        Else
            Set m_oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(Trim(sdbcGMN1_4CodCtrl), , , , False)
        End If
    
    Else
        
        If m_GMN2CargarComboDesde Then
            m_oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4CodCtrl), , , False
        Else
            m_oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set m_oGruposMN2 = m_oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = m_oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4DenCtrl.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If m_GMN2CargarComboDesde And Not m_oGruposMN2.EOF Then
        sdbcGMN2_4DenCtrl.AddItem "..."
    End If

    sdbcGMN2_4DenCtrl.SelStart = 0
    sdbcGMN2_4DenCtrl.SelLength = Len(sdbcGMN2_4DenCtrl.Text)
    sdbcGMN2_4DenCtrl.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4CodCtrl_Change()
    
    If Not g_GMN3RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn3 = ""
        End If
        g_GMN3RespetarCombo = True
        sdbcGMN3_4DenCtrl.Text = ""
        sdbcGMN4_4CodCtrl.Text = ""
        g_GMN3RespetarCombo = False
        Set m_oGMN3Seleccionado = Nothing
        m_GMN3CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN3_4CodCtrl_CloseUp()
  
    If sdbcGMN3_4CodCtrl.Value = "..." Then
        sdbcGMN3_4CodCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn3 = sdbcGMN3_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN3_4CodCtrl.Value = "" Then Exit Sub
    
    g_GMN3RespetarCombo = True
    sdbcGMN3_4DenCtrl.Text = sdbcGMN3_4CodCtrl.Columns(1).Text
    sdbcGMN3_4CodCtrl.Text = sdbcGMN3_4CodCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn3 = sdbcGMN3_4CodCtrl.Text
    End If
    g_GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    m_GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4CodCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4CodCtrl.RemoveAll

    If m_oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRComprador Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN3 = Nothing
        If m_GMN3CargarComboDesde Then
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl, Trim(sdbcGMN2_4CodCtrl), Trim(sdbcGMN3_4CodCtrl), , , False)
        Else
            Set m_oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4CodCtrl, sdbcGMN2_4CodCtrl, , , , False)
        End If
    Else
        
         If m_GMN3CargarComboDesde Then
            m_oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4CodCtrl), , , False
        Else
            m_oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN3 = m_oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = m_oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4CodCtrl.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If m_GMN3CargarComboDesde And Not m_oGruposMN3.EOF Then
        sdbcGMN3_4CodCtrl.AddItem "..."
    End If

    sdbcGMN3_4CodCtrl.SelStart = 0
    sdbcGMN3_4CodCtrl.SelLength = Len(sdbcGMN3_4CodCtrl.Text)
    sdbcGMN3_4CodCtrl.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4DenCtrl_Change()
    
    If Not g_GMN1RespetarCombo Then

        g_GMN1RespetarCombo = True
        sdbcGMN1_4CodCtrl.Text = ""
        sdbcGMN2_4CodCtrl.Text = ""
        g_GMN1RespetarCombo = False
        Set m_oGMN1Seleccionado = Nothing
        m_GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4DenCtrl_CloseUp()
    
    
    If sdbcGMN1_4DenCtrl.Value = "..." Then
        sdbcGMN1_4DenCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn1 = sdbcGMN1_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN1_4DenCtrl.Value = "" Then Exit Sub
    
    g_GMN1RespetarCombo = True
    sdbcGMN1_4CodCtrl.Text = sdbcGMN1_4DenCtrl.Columns(1).Text
    sdbcGMN1_4DenCtrl.Text = sdbcGMN1_4DenCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn1 = sdbcGMN1_4CodCtrl.Text
    End If
    g_GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    m_GMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4CodCtrl_Change()
        
    If Not g_GMN4RespetarCombo Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn4 = ""
        End If
        g_GMN4RespetarCombo = True
        sdbcGMN4_4DenCtrl.Text = ""
        g_GMN4RespetarCombo = False
        Set m_oGMN4Seleccionado = Nothing
        m_GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4CodCtrl_CloseUp()

    If sdbcGMN4_4CodCtrl.Value = "..." Then
        sdbcGMN4_4CodCtrl.Text = ""
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn4 = sdbcGMN4_4CodCtrl.Text
        End If
        Exit Sub
    End If
    
    If sdbcGMN4_4CodCtrl.Value = "" Then Exit Sub
    
    g_GMN4RespetarCombo = True
    If sdbcGMN4_4DenCtrl.Text <> sdbcGMN4_4CodCtrl.Columns(1).Text Then
        sdbcArtiCod.Text = ""
        sdbcArtiDen.Text = ""
    End If
    sdbcGMN4_4DenCtrl.Text = sdbcGMN4_4CodCtrl.Columns(1).Text
    sdbcGMN4_4CodCtrl.Text = sdbcGMN4_4CodCtrl.Columns(0).Text
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn4 = sdbcGMN4_4CodCtrl.Text
    End If
    g_GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    m_GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4CodCtrl_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

     sdbcGMN4_4CodCtrl.RemoveAll

    If m_oGMN3Seleccionado Is Nothing Then Exit Sub

    Screen.MousePointer = vbHourglass

    If bRComprador Then

        Set oIMAsig = oUsuarioSummit.comprador
        Set m_oGruposMN4 = Nothing
        If m_GMN4CargarComboDesde Then
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, Trim(sdbcGMN4_4CodCtrl), , , , False)
        Else
            Set m_oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, , , , , False)
        End If
    Else

       If m_GMN4CargarComboDesde Then
            m_oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4CodCtrl), , , False
        Else
            m_oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set m_oGruposMN4 = m_oGMN3Seleccionado.GruposMatNivel4

    End If

    Codigos = m_oGruposMN4.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1

        sdbcGMN4_4CodCtrl.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)

    Next

    If m_GMN4CargarComboDesde And Not m_oGruposMN4.EOF Then
        sdbcGMN4_4CodCtrl.AddItem "..."
    End If

    sdbcGMN4_4CodCtrl.SelStart = 0
    sdbcGMN4_4CodCtrl.SelLength = Len(sdbcGMN3_4CodCtrl.Text)
    sdbcGMN4_4CodCtrl.Refresh

    Screen.MousePointer = vbNormal

End Sub

''' <summary>
''' Carga los recursos necesarios del modulo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: UserControl_Initialize </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ATRIB, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        fraAtributo.caption = Ador(0).Value
        m_sIdiMenAtributo = Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value & ":"
        sdbgAtributosGrupoCtrl.Columns("COD").caption = Ador(0).Value
        m_sMensajes(0) = Ador(0).Value
        
        sdbcGMN1_4CodCtrl.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4CodCtrl.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4CodCtrl.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4CodCtrl.Columns(0).caption = Ador(0).Value
        sdbcGMN1_4DenCtrl.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4DenCtrl.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4DenCtrl.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4DenCtrl.Columns(1).caption = Ador(0).Value
        sdbcArtiCod.Columns(0).caption = Ador(0).Value
        sdbcArtiDen.Columns(1).caption = Ador(0).Value
        
        Ador.MoveNext
        lblDen.caption = Ador(0).Value & ":"
        sdbgAtributosGrupoCtrl.Columns("NOMBRE").caption = Ador(0).Value
        m_sMensajes(1) = Ador(0).Value
        sdbcGMN1_4CodCtrl.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4CodCtrl.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4CodCtrl.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4CodCtrl.Columns(1).caption = Ador(0).Value
        sdbcGMN1_4DenCtrl.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4DenCtrl.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4DenCtrl.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4DenCtrl.Columns(0).caption = Ador(0).Value
        sdbcArtiCod.Columns(1).caption = Ador(0).Value
        sdbcArtiDen.Columns(0).caption = Ador(0).Value
        
        Ador.MoveNext
        frmMat.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        lblArtiCod.caption = Ador(0).Value
        Ador.MoveNext
        frmTipo.caption = Ador(0).Value
        sdbgAtributosGrupoCtrl.Columns("TIPO").caption = Ador(0).Value
        m_sMensajes(2) = Ador(0).Value
        Ador.MoveNext
        optTipo(0).caption = Ador(0).Value
        Ador.MoveNext
        optTipo(1).caption = Ador(0).Value
        m_sCombo(2) = Ador(0).Value
        Ador.MoveNext
        optTipo(2).caption = Ador(0).Value
        m_sCombo(3) = Ador(0).Value
        Ador.MoveNext
        optTipo(3).caption = Ador(0).Value
        m_sCombo(1) = Ador(0).Value
        Ador.MoveNext
        optTipo(4).caption = Ador(0).Value
        m_sCombo(4) = Ador(0).Value
        Ador.MoveNext
        frmIntro.caption = Ador(0).Value
        sdbgAtributosGrupoCtrl.Groups(1).caption = Ador(0).Value
        Ador.MoveNext
        chkLibre.caption = Ador(0).Value
        sdbgAtributosGrupoCtrl.Columns("LIBRE").caption = Ador(0).Value
        Ador.MoveNext
        chkSeleccion.caption = Ador(0).Value
        sdbgAtributosGrupoCtrl.Columns("SELECCION").caption = Ador(0).Value
        Ador.MoveNext
        sstabgeneralCtrl.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabgeneralCtrl.TabCaption(1) = Ador(0).Value '
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Groups(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Groups(2).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Groups(3).caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Columns("MINIMO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Columns("MAXIMO").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Columns("ASC").caption = Ador(0).Value
        Ador.MoveNext
        cmdActualizar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdModoEdicion2Ctrl.caption = Ador(0).Value
        Ador.MoveNext
        cmdModoEdicionCtrl.caption = Ador(0).Value
        Ador.MoveNext
        cmdAnyadir.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCodigo.caption = Ador(0).Value
        Ador.MoveNext
        cmdBajaLog.caption = Ador(0).Value
        m_sIdioma(2) = Ador(0).Value
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdioma(1) = Ador(0).Value
        Ador.MoveNext
        m_sIdiAtributo = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(3) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(4) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(5) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(6) = Ador(0).Value
        Ador.MoveNext
        cmdSeleccionarCtrl.caption = Ador(0).Value
        Ador.MoveNext
        m_sItem = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(7) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(8) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(9) = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Columns("DESCR").caption = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(10) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(11) = Ador(0).Value
        
        
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value
        Ador.MoveNext

        sdbgAtributosGrupoCtrl.Columns("OFERTA").caption = Ador(0).Value
        Ador.MoveNext

        sdbgAtributosGrupoCtrl.Groups(4).caption = Ador(0).Value
        sdbgAtributosGrupoCtrl.Columns("VDEFECTO").caption = Ador(0).Value
        Ador.MoveNext
        
        m_sMensajes(12) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(13) = Ador(0).Value
        
        Ador.MoveNext
        frmTipoAtributo.caption = Ador(0).Value
        
        Ador.MoveNext
        Option6.caption = Ador(0).Value
            
        Ador.MoveNext
        Option7.caption = Ador(0).Value
            
        Ador.MoveNext
        Option8.caption = Ador(0).Value
        
        Ador.MoveNext
        m_sMensajes(14) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(15) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(16) = Ador(0).Value
        Ador.MoveNext
        m_sMensajes(17) = Ador(0).Value

        Ador.MoveNext
        m_sFormularios = Ador(0).Value
        
        Ador.MoveNext
        m_sProcesos = Ador(0).Value
        
        Ador.MoveNext
        m_sPedidos = Ador(0).Value
        
        Ador.MoveNext
        m_sApertura = Ador(0).Value
        
        Ador.MoveNext
        fraCosteDtos.caption = Ador(0).Value
        Ador.MoveNext
        chkCostes.caption = Ador(0).Value
        Ador.MoveNext
        chkDtos.caption = Ador(0).Value
        Ador.MoveNext
        chkOtros.caption = Ador(0).Value
        
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Columns("COSTE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        m_sMaestroAtributos = Ador(0).Value '76 Maestro Atrib.
        Ador.MoveNext
        chkUonCoincidente.caption = Ador(0).Value
        Ador.MoveNext
        chkUonContenida.caption = Ador(0).Value
        Ador.MoveNext
        frmAmbito.caption = Ador(0).Value
        Ador.MoveNext
        chkAmbitoUon.caption = Ador(0).Value
        Ador.MoveNext
        chkAmbitoMaterial.caption = Ador(0).Value
        Ador.MoveNext
        chkAmbitoArticulo.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributosGrupoCtrl.Groups(5).caption = Ador(0).Value
        sdbgAtributosGrupo.Columns("UONS").caption = ""
        lblUons.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        m_sCombo(5) = Ador(0).Value 'Adjunto
        optTipo(5).caption = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 03/05/2013</revision>

Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBBajasLog)) Is Nothing) Then
        m_bBajasLog = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBModificar)) Is Nothing) Then
        m_bModifAtri = True
    End If
    m_bModifCodigo = (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBModificarCodigo)) Is Nothing))
    
    If g_sOrigen = "ART4" Or g_sOrigen = "GMN4" Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestAsigAtrib)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            bRComprador = True
        End If
    ElseIf g_sOrigen = "frmPROCE" Then
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUsoAtrib)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            bRComprador = True
        End If
    Else
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.ATRIBRestMatComp)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
            bRComprador = True
        End If
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtribRestrMantAtribUonUsu)) Is Nothing) Then
        m_bRestrMantAtribUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtribRestrMantAtribUonPer)) Is Nothing) Then
        m_bRestrMantAtribPerf = True
    End If


    
    If g_sOrigen = "frmPROCE" Then
        m_bBajasLog = False
    End If
    If m_bModifAtri Then
        cmdModoEdicion2Ctrl.Visible = Not g_bSoloSeleccion
    Else
        cmdModoEdicion2Ctrl.Visible = False
    End If
End Sub
Private Sub GMN1Seleccionado()
    
    sdbcGMN2_4CodCtrl.Text = ""
    sdbcGMN3_4CodCtrl.Text = ""
    sdbcGMN4_4CodCtrl.Text = ""
    sdbcGMN3_4DenCtrl.Text = ""
    sdbcGMN4_4DenCtrl.Text = ""
    sdbcGMN2_4DenCtrl.Text = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn2 = ""
        g_sGmn3 = ""
        g_sGmn4 = ""
        g_sArtCod = ""
    End If
    
    If sdbcGMN1_4CodCtrl.Value = "" Then Exit Sub
    
    Set m_oGMN1Seleccionado = Nothing
    Set m_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
    
    m_oGMN1Seleccionado.Cod = sdbcGMN1_4CodCtrl
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4CodCtrl.RemoveAll
    sdbcGMN4_4CodCtrl.RemoveAll
    sdbcGMN3_4DenCtrl.RemoveAll
    sdbcGMN4_4DenCtrl.RemoveAll
    sdbcGMN3_4CodCtrl.Text = ""
    sdbcGMN4_4CodCtrl.Text = ""
    sdbcGMN3_4DenCtrl.Text = ""
    sdbcGMN4_4DenCtrl.Text = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn3 = ""
        g_sGmn4 = ""
        g_sArtCod = ""
    End If
    
    If sdbcGMN2_4CodCtrl.Value = "" Then Exit Sub
    
    Set m_oGMN2Seleccionado = Nothing
    Set m_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    m_oGMN2Seleccionado.Cod = sdbcGMN2_4CodCtrl.Text
    m_oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4CodCtrl.Text
    
End Sub
Private Sub GMN3Seleccionado()

    Set m_oGMN3Seleccionado = Nothing
    Set m_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    Set m_oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4CodCtrl.RemoveAll
    sdbcGMN4_4DenCtrl.RemoveAll
    sdbcGMN4_4CodCtrl.Text = ""
    sdbcGMN4_4DenCtrl.Text = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
    If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
        g_sGmn4 = ""
        g_sArtCod = ""
    End If
    
    If sdbcGMN3_4CodCtrl.Value = "" Then Exit Sub
    
    m_oGMN3Seleccionado.Cod = sdbcGMN3_4CodCtrl.Text
    m_oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4CodCtrl.Text
    m_oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4CodCtrl.Text
    
End Sub

Private Sub GMN4Seleccionado()
      
    g_sArtCod = ""
        
    Set m_oGMN4Seleccionado = Nothing
    Set m_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    m_oGMN4Seleccionado.Cod = sdbcGMN4_4CodCtrl.Text
    m_oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4CodCtrl.Text
    m_oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4CodCtrl.Text
    m_oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4CodCtrl.Text
    
End Sub
Public Sub PonerMatSeleccionadoEnCombos()

    Set m_oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado

    If Not m_oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4CodCtrl.Text = m_oGMN1Seleccionado.Cod
        sdbcGMN1_4CodCtrl_Validate False
    Else
        sdbcGMN1_4CodCtrl.Value = ""

    End If

    Set m_oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado

    If Not m_oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4CodCtrl.Text = m_oGMN2Seleccionado.Cod
        sdbcGMN2_4CodCtrl_Validate False
    Else
        sdbcGMN2_4CodCtrl.Value = ""

    End If

    Set m_oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado

    If Not m_oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4CodCtrl.Text = m_oGMN3Seleccionado.Cod
        sdbcGMN3_4CodCtrl_Validate False
    Else
        sdbcGMN3_4CodCtrl.Value = ""

    End If

    Set m_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado

    If Not m_oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4CodCtrl.Text = m_oGMN4Seleccionado.Cod
        sdbcGMN4_4CodCtrl_Validate False
    Else
        sdbcGMN4_4CodCtrl.Value = ""

    End If
    sdbcArtiCod.Text = ""

End Sub
Private Sub ArticuloSeleccionado()
    
    g_sArtCod = ""
    Set g_oArtiSeleccionado = Nothing
    Set g_oArtiSeleccionado = oFSGSRaiz.Generar_CArticulo
    
End Sub

''' <summary>
''' Cargar el grid de atributos
''' </summary>
''' <param name="iCol">Columna en cuya cabecera se cliquo</param>
''' <remarks>Llamada desde: sdbgAtributosGrupoCtrl_HeadClick    sstabgeneralCtrl_Click  cmdActualizar_Click; Tiempo m�ximo: 0</remarks>
Private Sub CargarAtributos(Optional ByVal iCol As Variant)
Dim vTipo As Variant
Dim vValor As Variant
Dim bLibre As Variant
Dim bSeleccion As Variant
Dim indi As Integer
Dim Texto As String
Dim sPuntos As String

Dim vTipoAtributo As Variant
Dim bOferta As Variant

Dim bCoste As Boolean
Dim bDescuento As Boolean
Dim bNoCosteDescuento As Boolean
Dim sEqp As String
Dim sComprador As String
m_bCargando = False
If optTipo(0).Value Then
    vTipo = Null
ElseIf optTipo(1).Value Then
    vTipo = 2
ElseIf optTipo(2).Value Then
    vTipo = 3
ElseIf optTipo(3).Value Then
    vTipo = 1
ElseIf optTipo(4).Value Then
    vTipo = 4
ElseIf optTipo(5).Value Then
    vTipo = TiposDeAtributos.TipoArchivo
End If

'Opciones de atributo
If Option6.Value = True Then
    vTipoAtributo = 1
Else
    If Option7.Value = True Then
        vTipoAtributo = 2
    Else
        If Option8.Value = True Then
            vTipoAtributo = 3
        End If
    End If
End If

Dim bfiltraruon, bfiltrarmat, bfiltrarart As Boolean
bfiltraruon = chkAmbitoUon
bfiltrarmat = chkAmbitoMaterial
bfiltrarart = chkAmbitoArticulo


If chkLibre.Value = vbChecked Then
    vValor = 0
Else
    If chkSeleccion.Value = vbChecked Then
        vValor = 1
    Else
        vValor = Null
    End If
End If

bCoste = (chkCostes.Value = vbChecked)
bDescuento = (chkDtos.Value = vbChecked)
bNoCosteDescuento = (chkOtros.Value = vbChecked)
Dim iPerfil As Integer
If oUsuarioSummit.Perfil Is Nothing Then
    iPerfil = 0
Else
    iPerfil = oUsuarioSummit.Perfil.Id
End If
Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
'If Not IsMissing(iCol) Then
    If g_sOrigen = "ART4" Or g_sOrigen = "GMN4" Then
        If bRComprador Then
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, True, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, m_bBajasLog, iCol, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4, g_sArtCod, bRComprador, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod, , , bCoste, bDescuento, bNoCosteDescuento, , m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, iPerfil, chkUonContenida.Value
        Else
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, True, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, m_bBajasLog, iCol, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4, g_sArtCod, , , , , , bCoste, bDescuento, bNoCosteDescuento, , m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, iPerfil, chkUonContenida.Value
        End If
    '3328
    ElseIf g_sOrigen = "TIPPED" Then
        If bRComprador Then
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, True, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, m_bBajasLog, iCol, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4, g_sArtCod, bRComprador, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod, , , bCoste, bDescuento, bNoCosteDescuento, g_sPedId
        Else
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, True, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, m_bBajasLog, iCol, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4, g_sArtCod, , , , , , bCoste, bDescuento, bNoCosteDescuento, g_sPedId
        End If
    ElseIf g_sOrigen = "POSI" Then
        If bRComprador Then
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, True, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4, g_sArtCod, vTipo, vValor, m_bBajasLog, iCol, , , , , , bRComprador, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod, , , bCoste, bDescuento, bNoCosteDescuento
        Else
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, True, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4, g_sArtCod, vTipo, vValor, m_bBajasLog, iCol
        End If
    ElseIf UCase(g_sOrigen) = "FRMSEGUIMIENTOARTBUSCAR" Or UCase(g_sOrigen) = "FRMESTARTBUSCAR" Or UCase(g_sOrigen) = "FRMFORMULARIOS" Then
        If bRComprador Then
            sEqp = oUsuarioSummit.comprador.codEqp
            sComprador = oUsuarioSummit.comprador.Cod
        Else
            sEqp = ""
            sComprador = ""
        End If
        m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, False, g_sGmn1, g_sGmn2, g_sGmn3, g_sGmn4, g_sArtCod, vTipo, vValor, m_bBajasLog, iCol, _
                                             , , , , , bRComprador, sEqp, sComprador, , , , , , , m_bRestrMantAtribUsu, _
                                             m_bRestrMantAtribPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, iPerfil
    ElseIf g_sOrigen = "PLANTILLAS" Then
        If bRComprador Then
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, False, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, False, iCol, , , , , , bRComprador, basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , , bCoste, bDescuento, bNoCosteDescuento
        Else
            m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, False, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, False, iCol, , , , , , , , , , , bCoste, bDescuento, bNoCosteDescuento
        End If
    ElseIf bRComprador Then
        m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, False, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, m_bBajasLog, iCol, , , , , , bRComprador, oUsuarioSummit.comprador.codEqp, oUsuarioSummit.comprador.Cod, vTipoAtributo, , bCoste, bDescuento, bNoCosteDescuento
    Else
        m_oAtributos.CargarTodosLosAtributos g_sOrigen, txtCodCtrl.Text, txtDen.Text, False, sdbcGMN1_4CodCtrl.Text, sdbcGMN2_4CodCtrl.Text, _
                                        sdbcGMN3_4CodCtrl.Text, sdbcGMN4_4CodCtrl.Text, sdbcArtiCod.Text, vTipo, vValor, m_bBajasLog, iCol, _
                                        , , , , , , , , vTipoAtributo, , bCoste, bDescuento, bNoCosteDescuento, , m_bRestrMantAtribUsu, _
                                        m_bRestrMantAtribPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, _
                                        iPerfil, chkUonContenida.Value, UonsSeleccionadas, bfiltraruon, bfiltrarmat, bfiltrarart
    End If

'Carga la grid
    
    sdbgAtributosGrupoCtrl.RemoveAll
    sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = 0
    
    m_bTodas = False
    
    indi = 1
    
    If m_oAtributos.Count > 0 Then
       
      For Each m_oAtributo In m_oAtributos
        bLibre = 0
        bSeleccion = 0
        If m_oAtributo.TipoIntroduccion = 0 Then
            bLibre = -1
        Else
            If m_oAtributo.TipoIntroduccion = 1 Then
                bSeleccion = -1
            End If
        End If
        
        bOferta = 0
        If m_oAtributo.Oferta Then
            bOferta = -1
        End If

        Select Case m_oAtributo.Tipo
        
            Case 1: Texto = m_sCombo(1)
            Case 2: Texto = m_sCombo(2)
            Case 3: Texto = m_sCombo(3)
            Case 4: Texto = m_sCombo(4)
            Case 8: Texto = m_sCombo(5)
        End Select
        If indi >= m_oAtributos.Count Then
            m_bTodas = True
        End If
        
        If Not IsNull(m_oAtributo.Descripcion) Then
            sPuntos = "..."
        Else
            sPuntos = ""
        End If
        m_bCargando = True
        Select Case m_oAtributo.Tipo
            Case 1
                sdbgAtributosGrupoCtrl.AddItem bOferta & Chr(m_lSeparador) & IIf(m_oAtributo.Coste, -1, 0) & Chr(m_lSeparador) & IIf(m_oAtributo.Descuento, -1, 0) & Chr(m_lSeparador) & m_oAtributo.Cod & Chr(m_lSeparador) & m_oAtributo.Den & Chr(m_lSeparador) & Chr(m_lSeparador) & Texto & Chr(m_lSeparador) & m_oAtributo.Id & Chr(m_lSeparador) & m_oAtributo.Tipo & Chr(m_lSeparador) & m_oAtributo.Generico & Chr(m_lSeparador) & m_oAtributo.ListaExterna & Chr(m_lSeparador) & bLibre & Chr(m_lSeparador) & m_oAtributo.Minimo & Chr(m_lSeparador) & m_oAtributo.Maximo & Chr(m_lSeparador) & bSeleccion & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.PreferenciaValorBajo) & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.BajaLogica) & Chr(m_lSeparador) & m_oAtributo.TipoPonderacion & Chr(m_lSeparador) & m_oAtributo.FECACT & Chr(m_lSeparador) & m_oAtributo.Descripcion & Chr(m_lSeparador) & m_oAtributo.valorText
            Case 2
                sdbgAtributosGrupoCtrl.AddItem bOferta & Chr(m_lSeparador) & IIf(m_oAtributo.Coste, -1, 0) & Chr(m_lSeparador) & IIf(m_oAtributo.Descuento, -1, 0) & Chr(m_lSeparador) & m_oAtributo.Cod & Chr(m_lSeparador) & m_oAtributo.Den & Chr(m_lSeparador) & Chr(m_lSeparador) & Texto & Chr(m_lSeparador) & m_oAtributo.Id & Chr(m_lSeparador) & m_oAtributo.Tipo & Chr(m_lSeparador) & m_oAtributo.Generico & Chr(m_lSeparador) & m_oAtributo.ListaExterna & Chr(m_lSeparador) & bLibre & Chr(m_lSeparador) & m_oAtributo.Minimo & Chr(m_lSeparador) & m_oAtributo.Maximo & Chr(m_lSeparador) & bSeleccion & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.PreferenciaValorBajo) & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.BajaLogica) & Chr(m_lSeparador) & m_oAtributo.TipoPonderacion & Chr(m_lSeparador) & m_oAtributo.FECACT & Chr(m_lSeparador) & m_oAtributo.Descripcion & Chr(m_lSeparador) & m_oAtributo.valorNum
            Case 3
                sdbgAtributosGrupoCtrl.AddItem bOferta & Chr(m_lSeparador) & IIf(m_oAtributo.Coste, -1, 0) & Chr(m_lSeparador) & IIf(m_oAtributo.Descuento, -1, 0) & Chr(m_lSeparador) & m_oAtributo.Cod & Chr(m_lSeparador) & m_oAtributo.Den & Chr(m_lSeparador) & Chr(m_lSeparador) & Texto & Chr(m_lSeparador) & m_oAtributo.Id & Chr(m_lSeparador) & m_oAtributo.Tipo & Chr(m_lSeparador) & m_oAtributo.Generico & Chr(m_lSeparador) & m_oAtributo.ListaExterna & Chr(m_lSeparador) & bLibre & Chr(m_lSeparador) & m_oAtributo.Minimo & Chr(m_lSeparador) & m_oAtributo.Maximo & Chr(m_lSeparador) & bSeleccion & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.PreferenciaValorBajo) & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.BajaLogica) & Chr(m_lSeparador) & m_oAtributo.TipoPonderacion & Chr(m_lSeparador) & m_oAtributo.FECACT & Chr(m_lSeparador) & m_oAtributo.Descripcion & Chr(m_lSeparador) & m_oAtributo.valorFec
            Case 4
                If IsNull(m_oAtributo.valorBool) Then
                        sdbgAtributosGrupoCtrl.AddItem bOferta & Chr(m_lSeparador) & IIf(m_oAtributo.Coste, -1, 0) & Chr(m_lSeparador) & IIf(m_oAtributo.Descuento, -1, 0) & Chr(m_lSeparador) & m_oAtributo.Cod & Chr(m_lSeparador) & m_oAtributo.Den & Chr(m_lSeparador) & Chr(m_lSeparador) & Texto & Chr(m_lSeparador) & m_oAtributo.Id & Chr(m_lSeparador) & m_oAtributo.Tipo & Chr(m_lSeparador) & m_oAtributo.Generico & Chr(m_lSeparador) & m_oAtributo.ListaExterna & Chr(m_lSeparador) & bLibre & Chr(m_lSeparador) & m_oAtributo.Minimo & Chr(m_lSeparador) & m_oAtributo.Maximo & Chr(m_lSeparador) & bSeleccion & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.PreferenciaValorBajo) & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.BajaLogica) & Chr(m_lSeparador) & m_oAtributo.TipoPonderacion & Chr(m_lSeparador) & m_oAtributo.FECACT & Chr(m_lSeparador) & m_oAtributo.Descripcion & Chr(m_lSeparador)
                Else
                    If StrToDbl0(m_oAtributo.valorBool) = 0 Then
                        sdbgAtributosGrupoCtrl.AddItem bOferta & Chr(m_lSeparador) & IIf(m_oAtributo.Coste, -1, 0) & Chr(m_lSeparador) & IIf(m_oAtributo.Descuento, -1, 0) & Chr(m_lSeparador) & m_oAtributo.Cod & Chr(m_lSeparador) & m_oAtributo.Den & Chr(m_lSeparador) & Chr(m_lSeparador) & Texto & Chr(m_lSeparador) & m_oAtributo.Id & Chr(m_lSeparador) & m_oAtributo.Tipo & Chr(m_lSeparador) & m_oAtributo.Generico & Chr(m_lSeparador) & m_oAtributo.ListaExterna & Chr(m_lSeparador) & bLibre & Chr(m_lSeparador) & m_oAtributo.Minimo & Chr(m_lSeparador) & m_oAtributo.Maximo & Chr(m_lSeparador) & bSeleccion & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.PreferenciaValorBajo) & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.BajaLogica) & Chr(m_lSeparador) & m_oAtributo.TipoPonderacion & Chr(m_lSeparador) & m_oAtributo.FECACT & Chr(m_lSeparador) & m_oAtributo.Descripcion & Chr(m_lSeparador) & m_sIdiFalse
                    Else
                        sdbgAtributosGrupoCtrl.AddItem bOferta & Chr(m_lSeparador) & IIf(m_oAtributo.Coste, -1, 0) & Chr(m_lSeparador) & IIf(m_oAtributo.Descuento, -1, 0) & Chr(m_lSeparador) & m_oAtributo.Cod & Chr(m_lSeparador) & m_oAtributo.Den & Chr(m_lSeparador) & Chr(m_lSeparador) & Texto & Chr(m_lSeparador) & m_oAtributo.Id & Chr(m_lSeparador) & m_oAtributo.Tipo & Chr(m_lSeparador) & m_oAtributo.Generico & Chr(m_lSeparador) & m_oAtributo.ListaExterna & Chr(m_lSeparador) & bLibre & Chr(m_lSeparador) & m_oAtributo.Minimo & Chr(m_lSeparador) & m_oAtributo.Maximo & Chr(m_lSeparador) & bSeleccion & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.PreferenciaValorBajo) & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.BajaLogica) & Chr(m_lSeparador) & m_oAtributo.TipoPonderacion & Chr(m_lSeparador) & m_oAtributo.FECACT & Chr(m_lSeparador) & m_oAtributo.Descripcion & Chr(m_lSeparador) & m_sIdiTrue
                    End If
                End If
            Case 8
                sdbgAtributosGrupoCtrl.AddItem bOferta & Chr(m_lSeparador) & IIf(m_oAtributo.Coste, -1, 0) & Chr(m_lSeparador) & IIf(m_oAtributo.Descuento, -1, 0) & Chr(m_lSeparador) & m_oAtributo.Cod & Chr(m_lSeparador) & m_oAtributo.Den & Chr(m_lSeparador) & Chr(m_lSeparador) & Texto & Chr(m_lSeparador) & m_oAtributo.Id & Chr(m_lSeparador) & m_oAtributo.Tipo & Chr(m_lSeparador) & m_oAtributo.Generico & Chr(m_lSeparador) & m_oAtributo.ListaExterna & Chr(m_lSeparador) & bLibre & Chr(m_lSeparador) & m_oAtributo.Minimo & Chr(m_lSeparador) & m_oAtributo.Maximo & Chr(m_lSeparador) & bSeleccion & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.PreferenciaValorBajo) & Chr(m_lSeparador) & Chr(m_lSeparador) & BooleanToSQLBinary(m_oAtributo.BajaLogica) & Chr(m_lSeparador) & m_oAtributo.TipoPonderacion & Chr(m_lSeparador) & m_oAtributo.FECACT & Chr(m_lSeparador) & m_oAtributo.Descripcion & Chr(m_lSeparador) & m_oAtributo.valorText
        End Select
        indi = indi + 1
      Next
        m_bCargando = False
      If sdbgAtributosGrupoCtrl.Rows = 0 Then
         sdbgAtributosGrupoCtrl.AddItem ""
      End If

      Accion = ACCAtributosCon
      
      
    End If
    
End Sub

Private Sub sdbddTipo_CloseUp()

If sdbgAtributosGrupoCtrl.Columns("TIPO").Value = "" Then Exit Sub

If Not sdbgAtributosGrupoCtrl.IsAddRow Then
    m_bNoCambio = False
    sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(sdbddTipo.Columns(0).Value)
    Select Case sdbddTipo.Columns(0).Value
        Case 2
            If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> 2 Then
                ComprobarPeticionesDeOfertas 2, False
            End If
        Case 1, 3, 4
            If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> sdbddTipo.Columns(0).Value Then
                If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = 2 Then
                    ComprobarPeticionesDeOfertas sdbddTipo.Columns(0).Value, True
                Else
                    ComprobarPeticionesDeOfertas sdbddTipo.Columns(0).Value, False
                End If
            End If
        Case 5  'Archivo
            
    End Select
    If m_bNoCambio Then
        Exit Sub
    End If
    sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = sdbddTipo.Columns(0).Value
    BorrarPondeYvaloresLista
    sdbgAtributosGrupoCtrl.Update
End If

End Sub

Private Sub sdbddTipo_DropDown()
Dim i As Integer

If sdbgAtributosGrupoCtrl.IsAddRow Then

    If sdbgAtributosGrupoCtrl.Columns("TIPO").Locked Then
        sdbddTipo.Enabled = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    sdbddTipo.RemoveAll
    
    If g_sOrigen = "frmCostesDescuentos" Then
            'Si el formulario se abre desde costes/descuentos s�lo permitir crear atributos num�ricos
            sdbddTipo.AddItem 2 & Chr(m_lSeparador) & m_sCombo(2)
    Else
    
        For i = 1 To 5
            sdbddTipo.AddItem i & Chr(m_lSeparador) & m_sCombo(i)
        Next
    
    End If
    Screen.MousePointer = vbNormal

End If
End Sub
Private Sub sdbddTipo_InitColumnProps()
    sdbddTipo.DataFieldList = "Column 1"
    sdbddTipo.DataFieldToDisplay = "Column 1"
End Sub
Private Sub sdbddTipo_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbddTipo.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddTipo.Rows - 1
            bm = sdbddTipo.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddTipo.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbgAtributosGrupoCtrl.Columns("TIPO").Value = Mid(sdbddTipo.Columns(1).CellText(bm), 1, Len(Text))
                sdbddTipo.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbddTipo_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
    
        ''' Comprobar la existencia en la lista
        Select Case UCase(sdbgAtributosGrupoCtrl.Columns("TIPO").Value)
        
            Case UCase(m_sCombo(1)):
                    RtnPassed = True
                    sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoString
            Case UCase(m_sCombo(2)):
                    RtnPassed = True
                    sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoNumerico
            Case UCase(m_sCombo(3)):
                    RtnPassed = True
                    sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoFecha
            Case UCase(m_sCombo(4)):
                    RtnPassed = True
                    sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoBoolean
            Case UCase(m_sCombo(5)):
                    RtnPassed = True
                    sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoArchivo
            Case Else: sdbgAtributosGrupoCtrl.Columns("TIPO").Text = ""
        
        End Select
    
    End If

End Sub



Private Sub sdbgAtributosGrupoCtrl_AfterDelete(RtnDispErrMsg As Integer)
    RtnDispErrMsg = 0
    If (sdbgAtributosGrupoCtrl.Rows = 0) Then
        Exit Sub
    End If
    If IsEmpty(sdbgAtributosGrupoCtrl.GetBookmark(0)) Then
        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.GetBookmark(-1)
    Else
        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.GetBookmark(0)
    End If
    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
End Sub

Private Sub sdbgAtributosGrupoCtrl_AfterInsert(RtnDispErrMsg As Integer)
''' * Objetivo: Si no hay error, volver a la
''' * Objetivo: situacion normal
    
    RtnDispErrMsg = 0
    If m_bCargando Then Exit Sub
    
    If g_bAnyaError = False Then
        cmdAnyadir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
        cmdCodigo.Enabled = True
    End If
    
    If IsEmpty(sdbgAtributosGrupoCtrl.GetBookmark(0)) Then
        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.GetBookmark(-1)
    Else
        sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.GetBookmark(0)
    End If
    'sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.RowBookmark(sdbgAtributosGrupoCtrl.Row)
End Sub

Private Sub sdbgAtributosGrupoCtrl_AfterUpdate(RtnDispErrMsg As Integer)
''' * Objetivo: Actualizar la fila en edicion
''' * Recibe: Buffer con los datos y bookmark de la fila
    
RtnDispErrMsg = 0
If g_bAnyaError = False And g_bModError = False Then
    cmdAnyadir.Visible = True
    cmdEliminar.Visible = True
End If

If sdbgAtributosGrupoCtrl.col = 2 Then
    sdbddTipo_ValidateList sdbgAtributosGrupoCtrl.Columns("TIPO").Text, False
    sdbddTipo.Enabled = True
End If

If Not g_oAtributoEnEdicion Is Nothing Then
    sdbgAtributosGrupoCtrl.Columns("FECACT").Value = g_oAtributoEnEdicion.FECACT
End If

Set g_oAtributoEnEdicion = Nothing

End Sub

Private Sub sdbgAtributosGrupoCtrl_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = 0
End Sub

Private Sub sdbgAtributosGrupoCtrl_BeforeInsert(Cancel As Integer)
    If bRComprador Then 'Solo se hace seleccionar los 4 niveles al que tiene restriccion
        If (sdbcGMN1_4CodCtrl.Text = "" Or sdbcGMN2_4CodCtrl.Text = "" Or sdbcGMN3_4CodCtrl.Text = "" Or sdbcGMN4_4CodCtrl.Text = "") And g_bEdicion Then
            oMensajes.MensajeOKOnly 891
            Cancel = True
        End If
    End If
End Sub


''' <summary>
''' Guarda los cambios producidos en el grid
''' </summary>
''' <param name="Cancel">Cancela los cambios</param>
''' <returns></returns>
''' <remarks>Llamada desde: Al cambiar de linea en el grid  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbgAtributosGrupoCtrl_BeforeUpdate(Cancel As Integer)
Dim teserror As TipoErrorSummit
Dim SFormularios As String
Dim SProcesos As String
Dim SPedidos As String
Dim sCadena As String
Dim bFechaValida As Boolean

Cancel = False
m_bError = False
If Not sdbgAtributosGrupoCtrl.IsAddRow Then
    If g_oAtributoEnEdicion Is Nothing Then
        DoEvents
        Exit Sub
    End If
    
End If
cmdDeshacer.Enabled = True

If Trim(sdbgAtributosGrupoCtrl.Columns("COD").Value) = "" Then
    oMensajes.NoValido m_sMensajes(0)
    Cancel = True
    GoTo Salir
End If

If NombreDeAtributoValido(Trim(sdbgAtributosGrupoCtrl.Columns("COD").Value)) = False Then
    oMensajes.CodigoAtributoNoValido m_sMensajes(0)
    Cancel = True
    GoTo Salir
End If

If Trim(sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value) = "" Then
    oMensajes.NoValido m_sMensajes(1)
    Cancel = True
    GoTo Salir
End If
    
If Trim(sdbgAtributosGrupoCtrl.Columns("TIPO").Value) = "" Then
    oMensajes.NoValido m_sMensajes(2)
    Cancel = True
    GoTo Salir
End If

If sdbgAtributosGrupoCtrl.Columns("MINIMO").Text <> "" Then
    If sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0" Then
        oMensajes.NoValida sdbgAtributosGrupoCtrl.Columns("MINIMO").caption
        If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
        Cancel = True
        GoTo Salir
    End If
    Select Case sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
        Case TiposDeAtributos.TipoFecha
            bFechaValida = False
            
            If IsDate(sdbgAtributosGrupoCtrl.Columns("MINIMO").Text) Then
                If Not FechaConHora(CDate(sdbgAtributosGrupoCtrl.Columns("MINIMO").Text)) Then
                    bFechaValida = True
                End If
            End If
            
            If Not bFechaValida Then
               oMensajes.NoValida sdbgAtributosGrupoCtrl.Columns("MINIMO").caption & ",  " & m_sMensajes(4)
               If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
               Cancel = True
               GoTo Salir
            End If
        Case TiposDeAtributos.TipoNumerico
            If Not IsNumeric(sdbgAtributosGrupoCtrl.Columns("MINIMO").Text) Then
                oMensajes.NoValida sdbgAtributosGrupoCtrl.Columns("MINIMO").caption & ",  " & m_sMensajes(3)
                If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                Cancel = True
                GoTo Salir
            End If
        Case Else
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Text = ""
    End Select
End If
If sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text <> "" Then
    If sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0" Then
        oMensajes.NoValida sdbgAtributosGrupoCtrl.Columns("MAXIMO").caption
        If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
        Cancel = True
        GoTo Salir
    End If
    Select Case sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
        Case TiposDeAtributos.TipoFecha
            bFechaValida = False
            
            If IsDate(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text) Then
                If Not FechaConHora(CDate(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text)) Then
                    bFechaValida = True
                End If
            End If
            
            If Not bFechaValida Then
               oMensajes.NoValida sdbgAtributosGrupoCtrl.Columns("MAXIMO").caption & ",  " & m_sMensajes(4)
               If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
               Cancel = True
               GoTo Salir
            End If
        Case TiposDeAtributos.TipoNumerico
            If Not IsNumeric(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text) Then
                oMensajes.NoValida sdbgAtributosGrupoCtrl.Columns("MAXIMO").caption & ",  " & m_sMensajes(3)
                If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                Cancel = True
                GoTo Salir
            End If
        Case Else
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text = ""
    End Select
End If

If sdbgAtributosGrupoCtrl.Columns("MINIMO").Text <> "" And sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text <> "" Then
    If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoFecha Then
        If CDate(sdbgAtributosGrupoCtrl.Columns("MINIMO").Text) > CDate(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text) Then
            MsgBox m_sMensajes(6), vbInformation, "FULLSTEP"
            If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
            Cancel = True
            GoTo Salir
        End If
    End If
    If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoNumerico Then
        If StrToDbl0(sdbgAtributosGrupoCtrl.Columns("MINIMO").Text) > StrToDbl0(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text) Then
            MsgBox m_sMensajes(6), vbInformation, "FULLSTEP"
            If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
            Cancel = True
            GoTo Salir
        End If
    End If
End If


If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text <> "" Then

    Select Case UCase(sdbgAtributosGrupoCtrl.Columns("TIPO").Text)

        Case UCase(m_sCombo(2)) 'Numero
                    If (Not IsNumeric(sdbgAtributosGrupoCtrl.Columns("VDefecto").Value)) Then
                        MsgBox m_sMensajes(14), vbInformation, "FULLSTEP"
                        Cancel = True
                        GoTo Salir

                    Else
                        If sdbgAtributosGrupoCtrl.Columns("MINIMO").Text <> "" And sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text <> "" Then
                            If StrToDbl0(sdbgAtributosGrupoCtrl.Columns("MINIMO").Value) > StrToDbl0(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value) Or StrToDbl0(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value) < StrToDbl0(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value) Then
                                MsgBox m_sMensajes(15), vbInformation, "FULLSTEP"
                                Cancel = True
                                GoTo Salir
                            End If
                        End If
                    
                    End If



        Case UCase(m_sCombo(3)) 'Fecha
                        If (Not IsDate(sdbgAtributosGrupoCtrl.Columns("VDefecto").Text) And sdbgAtributosGrupoCtrl.Columns("VDefecto").Value <> "") Then
                            MsgBox m_sMensajes(16), vbInformation, "FULLSTEP"
                            Cancel = True
                            GoTo Salir

                        Else
                            If sdbgAtributosGrupoCtrl.Columns("MINIMO").Text <> "" And sdbgAtributosGrupoCtrl.Columns("MAXIMO").Text <> "" Then
                                If CDate(sdbgAtributosGrupoCtrl.Columns("MINIMO").Value) > CDate(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value) Or CDate(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value) < CDate(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value) Then
                                    MsgBox m_sMensajes(15), vbInformation, "FULLSTEP"
                                    Cancel = True
                                    GoTo Salir

                                End If
                            End If
                    End If


    End Select

End If

'TAREA 3299:Atributos por uon
'Un atributo de UO no puede ser de oferta
If sdbgAtributosGrupoCtrl.Columns("OFERTA").Value = -1 Then
    Dim mensaje As Boolean
    If sdbgAtributosGrupoCtrl.IsAddRow And m_oUonsSeleccionadas.Count > 0 Then
        mensaje = True
    ElseIf Not AtributoSeleccionado Is Nothing Then
        If AtributoSeleccionado.uons.Count Then
            mensaje = True
        End If
    End If
    If mensaje Then
        'mensaje
        oMensajes.atributoOfertaNoUon
        Cancel = True
        GoTo Salir
    End If
End If

If sdbgAtributosGrupoCtrl.IsAddRow Then
    Set m_oAtributoAnyadir = oFSGSRaiz.Generar_CAtributo
    m_oAtributoAnyadir.Id = sdbgAtributosGrupoCtrl.Columns("ID").Value

    m_oAtributoAnyadir.PreferenciaValorBajo = SQLBinaryToBoolean(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("ASC").Value))
    m_oAtributoAnyadir.Cod = sdbgAtributosGrupoCtrl.Columns("COD").Value
    m_oAtributoAnyadir.Den = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
    
    If sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value = "" Or IsNull(sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value) Then
        m_oAtributoAnyadir.Descripcion = Null
    Else
        m_oAtributoAnyadir.Descripcion = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
    End If

    Select Case UCase(sdbgAtributosGrupoCtrl.Columns("TIPO").Text)
    
        Case UCase(m_sCombo(1)): m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoString
                               sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoString
                               m_oAtributoAnyadir.valorText = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
        Case UCase(m_sCombo(2)): m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoNumerico
                               sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoNumerico
                               If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = "" Then
                                    m_oAtributoAnyadir.valorNum = Null
                               Else
                                    m_oAtributoAnyadir.valorNum = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                               End If
        Case UCase(m_sCombo(3)): m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoFecha
                               sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoFecha
                               If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = "" Then
                                    m_oAtributoAnyadir.valorFec = Null
                               Else
                                m_oAtributoAnyadir.valorFec = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                                End If
        Case UCase(m_sCombo(4)): m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoBoolean
                               sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoBoolean
                               m_oAtributoAnyadir.valorBool = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
        Case UCase(m_sCombo(5)): m_oAtributoAnyadir.Tipo = TiposDeAtributos.TipoArchivo
                               sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoArchivo
    End Select
    If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = "4" Then
      sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
      sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
      sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0"
      sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "0"
    End If

    If SQLBinaryToBoolean(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value)) Then
        m_oAtributoAnyadir.TipoIntroduccion = Introselec
    Else
        m_oAtributoAnyadir.TipoIntroduccion = IntroLibre
    End If
    m_oAtributoAnyadir.Maximo = StrToNull(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value)
    m_oAtributoAnyadir.Minimo = StrToNull(sdbgAtributosGrupoCtrl.Columns("MINIMO").Value)

    m_oAtributoAnyadir.TipoPonderacion = SinPonderacion
    m_oAtributoAnyadir.GMN1 = sdbcGMN1_4CodCtrl.Text
    m_oAtributoAnyadir.GMN2 = sdbcGMN2_4CodCtrl.Text
    m_oAtributoAnyadir.GMN3 = sdbcGMN3_4CodCtrl.Text
    m_oAtributoAnyadir.GMN4 = sdbcGMN4_4CodCtrl.Text
    
    
    If SQLBinaryToBoolean(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("OFERTA").Value)) Then
        m_oAtributoAnyadir.Oferta = 1
    Else
        m_oAtributoAnyadir.Oferta = 0
    End If
    
    m_oAtributoAnyadir.Coste = SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("COSTE").Value)
    m_oAtributoAnyadir.Descuento = SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value)
        
    Set m_oIBAseDatosEnEdicion = m_oAtributoAnyadir
           
    
    teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
    
            
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        g_bAnyaError = True
        cmdDeshacer_Click
        If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
        GoTo Salir
    Else
        'actualizamos si las hubiera las tablas relativas a los atributos de uon (recolocar)
        'siempre que el atributo no sea de oferta
        If m_oAtributoAnyadir.Oferta = 0 And m_oUonsSeleccionadas.Count > 0 Then
            Set m_oAtributoAnyadir.uons = m_oUonsSeleccionadas
            m_oAtributoAnyadir.updateUons
        End If
        basSeguridad.RegistrarAccion AccionesSummit.ACCAtributosAnya, "Id:" & m_oAtributoAnyadir.Id
    End If
    
    m_oAtributos.Add m_oAtributoAnyadir.Id, m_oAtributoAnyadir.Cod, m_oAtributoAnyadir.Den, m_oAtributoAnyadir.Tipo, m_oAtributoAnyadir.PreferenciaValorBajo, , , , , , , m_oAtributoAnyadir.Id, m_oAtributoAnyadir.valor, m_oAtributoAnyadir.FECACT, , , , , , m_oAtributoAnyadir.TipoIntroduccion, m_oAtributoAnyadir.Minimo, m_oAtributoAnyadir.Maximo, m_oAtributoAnyadir.BajaLogica, , , , , , , m_oAtributoAnyadir.TipoPonderacion, m_oAtributoAnyadir.ListaPonderacion, m_oAtributoAnyadir.Formula, m_oAtributoAnyadir.Numero, m_oAtributoAnyadir.ValorPondSi, m_oAtributoAnyadir.ValorPondNo
    sdbgAtributosGrupoCtrl.Columns("ID").Value = m_oAtributoAnyadir.Id
    sdbgAtributosGrupoCtrl.Columns("FECACT").Value = m_oAtributoAnyadir.FECACT
    sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = m_oAtributoAnyadir.TipoPonderacion
    sdbgAtributosGrupoCtrl.Columns("BAJA").Value = m_oAtributoAnyadir.BajaLogica
    sdbgAtributosGrupoCtrl.Columns("GENERICO").Value = m_oAtributoAnyadir.Generico
    If m_oAtributoAnyadir.TipoIntroduccion = Introselec Then
        sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1"
        
        If m_oAtributoAnyadir.ListaPonderacion Is Nothing Then
            CargarListaValores
            GoTo Salir
        ElseIf m_oAtributoAnyadir.ListaPonderacion.Count = 0 And Not m_bClickEnListaCombo Then
            CargarListaValores

            GoTo Salir
        End If
    Else
        sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "0"
    End If
    g_bAnyaError = False
    
    sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = 0
    
Else

    g_oAtributoEnEdicion.Cod = sdbgAtributosGrupoCtrl.Columns("COD").Value
    
    
    Dim iAtrib As Integer
    
    For iAtrib = 1 To m_oAtributos.Count
        If m_oAtributos.Item(iAtrib).Cod = sdbgAtributosGrupoCtrl.Columns("COD").Value Then
            Exit For
        End If
    Next
    Dim iTipoIntro
    
    If SQLBinaryToBoolean(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value)) Then
        iTipoIntro = Introselec
        
        If g_oAtributoEnEdicion.ListaPonderacion.Count = 0 And Not m_bClickEnListaCombo And Not g_oAtributoEnEdicion.ListaExterna = 1 Then
            If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
            'oMensajes.NoValido m_sMensajes(17)
            MsgBox m_sMensajes(17), vbInformation, "FULLSTEP"
            Cancel = True

            CargarListaValores

            GoTo Salir
        End If
    Else
        iTipoIntro = IntroLibre
    End If
    
    
    If (m_oAtributos.Item(iAtrib).Maximo <> sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value _
         Or m_oAtributos.Item(iAtrib).Minimo <> sdbgAtributosGrupoCtrl.Columns("MINIMO").Value _
         Or m_oAtributos.Item(iAtrib).TipoIntroduccion <> iTipoIntro) Then

            If g_oAtributoEnEdicion.ComprobarAtributoEnFormsPM(SFormularios) Then
            
                m_bRespuesta = False
                frmAtribMsg.g_sOrigen = g_sOrigen & "frmATRIB"
                frmAtribMsg.m_itipoMensaje = 1
                frmAtribMsg.txtMsg.Text = m_sFormularios & vbCrLf & SFormularios
                frmAtribMsg.Show 1
                                
                If m_bRespuesta = False Then
                    
                    If m_oAtributos.Item(iAtrib).TipoIntroduccion <> iTipoIntro And iTipoIntro = Introselec Then
                        sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = 0
                        sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = -1
                        sdbgAtributosGrupoCtrl.Columns("BOT").Value = " "
                    End If
                
                    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                    Cancel = True
                    m_bCambioSeleccion = False
                    GoTo Salir
                End If
                
            End If
            
            If g_oAtributoEnEdicion.ComprobarAtributoEnProcesosYPedidos(SProcesos, SPedidos) Then
                m_bRespuesta = False
                frmAtribMsg.g_sOrigen = g_sOrigen & "frmATRIB"
                frmAtribMsg.m_itipoMensaje = 2
                If SProcesos <> "" Then
                    sCadena = m_sProcesos & vbCrLf
                    sCadena = sCadena & SProcesos
                End If
                
                frmAtribMsg.txtMsg.Text = sCadena
                frmAtribMsg.Show 1
                
                If m_bRespuesta = False Then
                
                    If m_oAtributos.Item(iAtrib).TipoIntroduccion <> iTipoIntro And iTipoIntro = Introselec Then
                        sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = 0
                        sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = -1
                        sdbgAtributosGrupoCtrl.Columns("BOT").Value = " "
                    End If
                
                    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                    Cancel = True
                    m_bCambioSeleccion = False
                    GoTo Salir
                End If
                
            End If
            
    Else
        'En caso de que solo se haya modificado la denominacion del campo
        If (m_oAtributos.Item(iAtrib).Den <> sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value) Then
            If g_oAtributoEnEdicion.ComprobarAtributoEnFormsPM(SFormularios) Then
            
                m_bRespuesta = False
                frmAtribMsg.g_sOrigen = g_sOrigen
                frmAtribMsg.m_itipoMensaje = 1
                frmAtribMsg.txtMsg.Text = m_sFormularios & vbCrLf & SFormularios
                frmAtribMsg.Show 1
                                
                If m_bRespuesta = False Then
                    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                    Cancel = True
                    m_bCambioSeleccion = False
                    GoTo Salir
                End If
                
            End If
        
        End If

    End If
    
    'Comprobamos si se ha cambiado alguno de los siguientes valores
    'tipo de introduccion,maximo,minimo,valor por defecto
    
    g_oAtributoEnEdicion.Den = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
    If sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value = "" Or IsNull(sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value) Then
        g_oAtributoEnEdicion.Descripcion = Null
    Else
        g_oAtributoEnEdicion.Descripcion = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
    End If
    
    g_oAtributoEnEdicion.PreferenciaValorBajo = SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("ASC").Value)
    If SQLBinaryToBoolean(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("SELECCION").Value)) Then
        g_oAtributoEnEdicion.TipoIntroduccion = Introselec
    Else
        g_oAtributoEnEdicion.TipoIntroduccion = IntroLibre
    End If
    
    Select Case UCase(sdbgAtributosGrupoCtrl.Columns("TIPO").Text)
      
          Case UCase(m_sCombo(1)): g_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoString
                                 sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoString
                                 If g_oAtributoEnEdicion.TipoIntroduccion = Introselec Then
                                    g_oAtributoEnEdicion.valorNum = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                                 End If
                                 g_oAtributoEnEdicion.valorText = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
          Case UCase(m_sCombo(2)): g_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoNumerico
                                 sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoNumerico
                                 g_oAtributoEnEdicion.valorNum = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
          Case UCase(m_sCombo(3)): g_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoFecha
                                 sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoFecha
                                 If g_oAtributoEnEdicion.TipoIntroduccion = Introselec Then
                                    g_oAtributoEnEdicion.valorNum = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
                                    
                                 End If
                                 g_oAtributoEnEdicion.valorFec = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text
          Case UCase(m_sCombo(4)): g_oAtributoEnEdicion.Tipo = TiposDeAtributos.TipoBoolean
                                 sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = TiposDeAtributos.TipoBoolean
                                 g_oAtributoEnEdicion.valorBool = sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value
      
    End Select
    
    If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = "4" Then
      sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
      sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
      sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0"
      sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "0"
    End If
    
    If SQLBinaryToBoolean(StrToDbl0(sdbgAtributosGrupoCtrl.Columns("OFERTA").Value)) Then
        g_oAtributoEnEdicion.Oferta = 1
    Else
        g_oAtributoEnEdicion.Oferta = 0
    End If


    g_oAtributoEnEdicion.Maximo = StrToNull(sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value)
    g_oAtributoEnEdicion.Minimo = StrToNull(sdbgAtributosGrupoCtrl.Columns("MINIMO").Value)
    
  Screen.MousePointer = vbHourglass
  g_oAtributoEnEdicion.TipoPonderacion = sdbgAtributosGrupoCtrl.Columns("TipoPond").Value
  g_oAtributoEnEdicion.BajaLogica = SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("BAJA").Value)
  
  g_oAtributoEnEdicion.Coste = SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("COSTE").Value)
  g_oAtributoEnEdicion.Descuento = SQLBinaryToBoolean(sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value)
  
  Set m_oIBAseDatosEnEdicion = g_oAtributoEnEdicion
  teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
  Screen.MousePointer = vbNormal
            
  If teserror.NumError <> TESnoerror Then
    If teserror.NumError = TESDatoNoValido Then
       oMensajes.IntervaloMinimoMaximoNulo
       If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
        g_bModError = True
        sdbgAtributosGrupoCtrl.DataChanged = False
        m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).Maximo = sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value
        m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).Minimo = sdbgAtributosGrupoCtrl.Columns("MINIMO").Value
        m_bError = True
    
     Else
        TratarError teserror
        If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
        g_bModError = True
        sdbgAtributosGrupoCtrl.DataChanged = False
        m_bError = True
    End If
  Else
        
    ''' Registro de acciones
     sdbgAtributosGrupoCtrl.Columns("FECACT").Value = g_oAtributoEnEdicion.FECACT
     basSeguridad.RegistrarAccion ACCAtributosMod, "Id:" & g_oAtributoEnEdicion.Id
     cmdDeshacer.Enabled = False
     sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = 0
     
  End If
            
  Set m_oIBAseDatosEnEdicion = Nothing
    
End If

'Set g_oAtributoEnEdicion = Nothing
Set m_oAtributoAnyadir = Nothing
Exit Sub

Salir:

    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
    m_bError = True
End Sub

Private Function FechaConHora(ByVal dtFecha As String) As Boolean
    Dim strFecha As String
    
    FechaConHora = False
    
    strFecha = CStr(dtFecha)
    If InStr(1, strFecha, ":") > 0 Then FechaConHora = True
    If InStr(1, strFecha, " ") > 0 Then FechaConHora = True
End Function

''' <summary>
''' Responde al click en un boton mostrando el mantenimiento corerespondiente
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAtributosGrupoCtrl_BtnClick()

If sdbgAtributosGrupoCtrl.col < 0 Then Exit Sub

  
If Left(sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name, 4) = "POND" Then
    'Si es Archivo no se puede ponderar
    If sdbgAtributosGrupoCtrl.Columns("IDTIPO").Value = TiposDeAtributos.TipoArchivo Then Exit Sub
    'acabo con la edicion del atributo
    If sdbgAtributosGrupoCtrl.DataChanged Then
        sdbgAtributosGrupoCtrl.Update
        If m_bError Then
            Exit Sub
        End If
    End If
    If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> "" Then
        frmPonderacion.g_iTipo = sdbgAtributosGrupoCtrl.Columns("idTIPO").Value
        frmPonderacion.g_sQuien = sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value
        CargarPonderacion
        If sdbgAtributosGrupoCtrl.DataChanged Then  'devuelve modicaciones
            sdbgAtributosGrupoCtrl.Update
            If m_bError Then
                Exit Sub
            End If
            
        End If
    End If

ElseIf Left(sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name, 3) = "BOT" Then
    
    If sdbgAtributosGrupoCtrl.Columns("BOT").Value = "..." Then
        'acabo con la edicion del atributo
        If sdbgAtributosGrupoCtrl.DataChanged = True Then
            m_bClickEnListaCombo = True
            sdbgAtributosGrupoCtrl.Update
            m_bClickEnListaCombo = False
            If m_bError Then
                Exit Sub
            End If
        End If
    
        CargarListaValores
        'acabo con la edicion del atributo cuando me devuelve modificaciones.
        If sdbgAtributosGrupoCtrl.DataChanged Then
            sdbgAtributosGrupoCtrl.Update
            If m_bError Then
                Exit Sub
            End If
        End If
    End If

ElseIf Left(sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name, 3) = "DES" Then
    
        'acabo con la edicion del atributo
        If sdbgAtributosGrupoCtrl.DataChanged = True Then
            sdbgAtributosGrupoCtrl.Update
            If m_bError Then
                Exit Sub
            End If
        End If
    
        If Trim(sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value) = "" And (cmdBajaLog.caption <> m_sIdioma(2) Or g_bEdicion = False) Then
            oMensajes.MensajeOKOnly 644
            Exit Sub
        Else
            If cmdBajaLog.caption = m_sIdioma(2) Then
                frmATRIBDescr.g_bEdicion = g_bEdicion
            Else
                frmATRIBDescr.g_bEdicion = False
            End If
            frmATRIBDescr.g_sOrigen = g_sOrigen & "frmATRIB"
            frmATRIBDescr.txtDescr.Text = sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value
            frmATRIBDescr.Show 1
    
    
            'acabo con la edicion del atributo cuando me devuelve modificaciones.
            If sdbgAtributosGrupoCtrl.DataChanged Then
                If g_oAtributoEnEdicion Is Nothing Then
                    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
                End If
    
                sdbgAtributosGrupoCtrl.Update
                If m_bError Then
                    Exit Sub
                End If
            End If
        End If
        
ElseIf Left(sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name, 4) = "VDEF" Then
                    
        If cmdBajaLog.caption = m_sIdioma(2) Then
            frmATRIBDescr.g_bEdicion = g_bEdicion
        Else
            frmATRIBDescr.g_bEdicion = False
        End If
        frmATRIBDescr.g_sOrigen = g_sOrigen & "frmATRIB2"
        frmATRIBDescr.txtDescr.Text = sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Value
        frmATRIBDescr.Show 1
        
        'frmATRIBDescr.txtDescr.Text = sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Value
        
    
        If sdbgAtributosGrupoCtrl.DataChanged Then
            If g_oAtributoEnEdicion Is Nothing Then
                Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
            End If
    
            sdbgAtributosGrupoCtrl.Update
            If m_bError Then
                Exit Sub
            End If
        End If
Else
    If sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name = "UONS" Then
    'acabo con la edicion del atributo
    If sdbgAtributosGrupoCtrl.DataChanged Then
        sdbgAtributosGrupoCtrl.Update
        If m_bError Then
            Exit Sub
        End If
    End If
        'si es un alta a�n no podemos asignar unidades organizativas o incumplir�amos la integridad referencial
        If Not sdbgAtributosGrupoCtrl.IsAddRow Then
            abrirFormUons (False)
        End If
    End If
End If



End Sub

''' <summary>
''' Evento que salta al producirse un cambio en el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0</remarks>
Private Sub sdbgAtributosGrupoCtrl_Change()
Dim teserror As TipoErrorSummit

If sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "1" Then
    cmdDeshacer_Click
    Exit Sub
End If
If (sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name = "LIBRE") Then
    If sdbgAtributosGrupoCtrl.Columns("idTipo").Value = "4" Or sdbgAtributosGrupoCtrl.Columns("idTipo").Value = TipoArchivo Then
        cmdDeshacer_Click
        Exit Sub
    End If
End If
If (sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name = "SELECCION") Then
    If sdbgAtributosGrupoCtrl.Columns("idTipo").Value = "4" Or sdbgAtributosGrupoCtrl.Columns("idTipo").Value = TipoArchivo Then
        cmdDeshacer_Click
        Exit Sub
    End If
End If

If cmdDeshacer.Enabled = False Then
    cmdAnyadir.Enabled = False
    cmdEliminar.Enabled = False
    cmdDeshacer.Enabled = True
    cmdCodigo.Enabled = False
    cmdBajaLog.Enabled = False
End If

Select Case sdbgAtributosGrupoCtrl.Columns("TIPO").Value
    Case m_sCombo(1)
        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> "1" Then
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = False
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = -1
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = False
            sdbgAtributosGrupoCtrl.Columns("LIBRE").CellStyleSet "Normal"
            sdbgAtributosGrupoCtrl.Columns("SELECCION").CellStyleSet "Normal"
        End If
        
        sdbgAtributosGrupoCtrl.Columns("COSTE").Value = 0
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = 0
        
    Case m_sCombo(2)
        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> "2" Then
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = False
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = False
            sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Normal"
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Normal"
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = False
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = False
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "-1"
            sdbgAtributosGrupoCtrl.Columns("LIBRE").CellStyleSet "Normal"
            sdbgAtributosGrupoCtrl.Columns("SELECCION").CellStyleSet "Normal"
        End If
    Case m_sCombo(3)
        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> "3" Then
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = False
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = False
            sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Normal"
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Normal"
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = False
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = False
            sdbgAtributosGrupoCtrl.Columns("LIBRE").CellStyleSet "Normal"
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = False
            sdbgAtributosGrupoCtrl.Columns("SELECCION").CellStyleSet "Normal"
        End If
        
        sdbgAtributosGrupoCtrl.Columns("COSTE").Value = 0
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = 0
        
    Case m_sCombo(4)
        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> "4" Then
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0"
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "0"
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = True
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("LIBRE").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("SELECCION").CellStyleSet "Gris"
        End If
        
        sdbgAtributosGrupoCtrl.Columns("COSTE").Value = 0
        sdbgAtributosGrupoCtrl.Columns("DESCUENTO").Value = 0
    Case m_sCombo(5)    'Archivo
        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value <> TiposDeAtributos.TipoArchivo Then
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0"
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "0"
            sdbgAtributosGrupoCtrl.Columns("POND").Value = ""
            sdbgAtributosGrupoCtrl.Columns("ASC").Value = ""
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = True
            sdbgAtributosGrupoCtrl.Columns("POND").Locked = True
            sdbgAtributosGrupoCtrl.Columns("ASC").Locked = True
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("LIBRE").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("SELECCION").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("POND").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("ASC").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Style = ssStyleEdit
        End If
End Select

If Not m_bRespetarCheck Then
    
    If sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name = "LIBRE" Then
        If sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0" And sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "0" Then
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1"
            sdbgAtributosGrupoCtrl.Columns("BOT").Value = "..."
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            m_bCambioSeleccion = True
            sdbgAtributosGrupoCtrl_BtnClick
            
            'sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = sdbddValor.hwnd
            
        End If
        If sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "-1" Then
            If Not sdbgAtributosGrupoCtrl.IsAddRow Then
                BorrarPondeYvaloresLista
            End If
            If sdbgAtributosGrupoCtrl.col < 0 Then Exit Sub

            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            m_bRespetarCheck = False
        
            If sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(2) Or sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(3) Then
                sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = False
                sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Normal", sdbgAtributosGrupoCtrl.Row
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = False
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Normal", sdbgAtributosGrupoCtrl.Row
            Else
                sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
                sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Gris", sdbgAtributosGrupoCtrl.Row
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Gris", sdbgAtributosGrupoCtrl.Row
            End If
            
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = 0
            
            If sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(1) Then
                sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Style = ssStyleEditButton
            End If
            
        End If
    End If
    
    If sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name = "SELECCION" Then
        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
            m_bRespetarCheck = True
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = 0
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = ""
            sdbgAtributosGrupoCtrl.Columns("BOT").Value = "..."
            m_bRespetarCheck = False
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Gris", sdbgAtributosGrupoCtrl.Row
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Gris", sdbgAtributosGrupoCtrl.Row
            sdbgAtributosGrupoCtrl.col = 11
            If sdbgAtributosGrupoCtrl.IsAddRow Then
                m_bCambioSeleccion = True
                sdbgAtributosGrupoCtrl_BtnClick
                Exit Sub
            End If
        Else
            If Not sdbgAtributosGrupoCtrl.IsAddRow Then
                m_bRespetarCheck = True
                BorrarPondeYvaloresLista
                m_bRespetarCheck = False
            End If
            If Not cmdDeshacer.Enabled Then
                sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0"
                Exit Sub
            End If
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "-1"
        End If
    End If
    
    If sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name = "OFERTA" Then
        If sdbgAtributosGrupoCtrl.Columns("OFERTA").Value <> 1 And sdbgAtributosGrupoCtrl.Columns("OFERTA").Value <> -1 Then
            'ATrib Especificacion
                If g_bEdicion Then
                    sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = False
                Else
                    sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = True
                End If
                    sdbgAtributosGrupoCtrl.Columns("VDEFECTO").CellStyleSet "Normal", sdbgAtributosGrupoCtrl.Row
        Else
            'ATrib Oferta
            
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").CellStyleSet "Gris", sdbgAtributosGrupoCtrl.Row
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text = ""
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = ""
            
        End If
    End If

End If
 If Not sdbgAtributosGrupoCtrl.IsAddRow Then
    Set g_oAtributoEnEdicion = Nothing
    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
    Set m_oIBAseDatosEnEdicion = g_oAtributoEnEdicion
        
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
        
    If teserror.NumError = TESInfModificada Then
            
        TratarError teserror
        sdbgAtributosGrupoCtrl.DataChanged = False
        sdbgAtributosGrupoCtrl.Columns("OFERTA").Value = g_oAtributoEnEdicion.Oferta
        sdbgAtributosGrupoCtrl.Columns("COD").Value = g_oAtributoEnEdicion.Cod
        sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value = g_oAtributoEnEdicion.Den
        Select Case g_oAtributoEnEdicion.Tipo
            Case TiposDeAtributos.TipoString
                sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(1)
            Case TiposDeAtributos.TipoNumerico
                sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(2)
            Case TiposDeAtributos.TipoFecha
                sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(3)
            Case TiposDeAtributos.TipoBoolean
                sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(4)
        End Select

        sdbgAtributosGrupoCtrl.Columns("ID").Value = g_oAtributoEnEdicion.Id
        sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = g_oAtributoEnEdicion.Tipo
        If g_oAtributoEnEdicion.TipoIntroduccion = IntroLibre Then
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = -1
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = 0
        Else
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = 0
        End If
        sdbgAtributosGrupoCtrl.Columns("MINIMO").Value = NullToStr(g_oAtributoEnEdicion.Minimo)
        sdbgAtributosGrupoCtrl.Columns("MAXIMO").Value = NullToStr(g_oAtributoEnEdicion.Maximo)
        
        sdbgAtributosGrupoCtrl.Columns("ASC").Value = g_oAtributoEnEdicion.PreferenciaValorBajo
        sdbgAtributosGrupoCtrl.Columns("BAJA").Value = BooleanToSQLBinary(g_oAtributoEnEdicion.BajaLogica)
        sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = g_oAtributoEnEdicion.TipoPonderacion

        teserror.NumError = TESnoerror
            
    End If
        
    If teserror.NumError <> TESnoerror Then

        TratarError teserror
        If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus

    Else
        If sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name = "BOT" Then
            If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = "-1" Then
                m_bCambioSeleccion = True
                sdbgAtributosGrupoCtrl_BtnClick
            End If
        End If
        Accion = ACCAtributosMod

    End If
           
End If

End Sub


Private Sub sdbgAtributosGrupoCtrl_HeadClick(ByVal ColIndex As Integer)
Dim sHeadCaption As String

    ''' * Objetivo: Ordenar el grid segun la columna

    If ColIndex > 10 Then Exit Sub
    
    If g_bEdicion Then Exit Sub

    Screen.MousePointer = vbHourglass

    sHeadCaption = sdbgAtributosGrupoCtrl.Columns(ColIndex).caption
'    sdbgAtributos.Columns(ColIndex).Caption = m_sIdiOrdenando
    
    
    CargarAtributos ColIndex
    
    sdbgAtributosGrupoCtrl.Columns(ColIndex).caption = sHeadCaption

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbgAtributosGrupoCtrl_InitColumnProps()
    sdbgAtributosGrupoCtrl.Columns("COD").StyleSet = "BLUE"
End Sub

Private Sub sdbgAtributosGrupoCtrl_KeyDown(KeyCode As Integer, Shift As Integer)
''' * Objetivo: Capturar la tecla Supr para
''' * Objetivo: poder eliminar desde el teclado

    If KeyCode = vbKeyDelete Then

        Exit Sub

    End If
End Sub

Private Sub sdbgAtributosGrupoCtrl_KeyPress(KeyAscii As Integer)
     If KeyAscii = vbKeyEscape Then
                                    
        If sdbgAtributosGrupoCtrl.DataChanged = False Then
                
            sdbgAtributosGrupoCtrl.CancelUpdate
            sdbgAtributosGrupoCtrl.DataChanged = False
                
            If Not g_oAtributoEnEdicion Is Nothing Then
                m_oIBAseDatosEnEdicion.CancelarEdicion
                Set m_oIBAseDatosEnEdicion = Nothing
                Set g_oAtributoEnEdicion = Nothing
            End If
               
            If Not sdbgAtributosGrupoCtrl.IsAddRow Then
                cmdAnyadir.Enabled = True
                cmdEliminar.Enabled = True
                cmdDeshacer.Enabled = False
                cmdCodigo.Enabled = True
            Else
                 cmdAnyadir.Enabled = False
                 cmdEliminar.Enabled = False
                 cmdDeshacer.Enabled = False
                 cmdCodigo.Enabled = False
            End If
                   
        End If
          
    End If

    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        If Left(sdbgAtributosGrupoCtrl.Columns(sdbgAtributosGrupoCtrl.col).Name, 4) <> "VDEF" Then
            sdbgAtributosGrupoCtrl_BtnClick
        End If
    End If
End Sub

''' <summary>
''' Evento al moverse de columna o de fila en la grid
''' </summary>
''' <param name="lastRow">Fila desde la que se mueve</param>
''' <param name="LastCol">Columna desde la que se mueve</param>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgAtributosGrupoCtrl_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim i As Integer
With sdbgAtributosGrupoCtrl
    If .IsAddRow Then
        .Columns("TIPO").Locked = False
        .Columns("TIPO").DropDownHwnd = sdbddTipo.hWnd
    Else
        .Columns("TIPO").Locked = True
        .Columns("TIPO").DropDownHwnd = 0
    End If
    
    If .Columns("BAJA").Value = "1" Or .Columns("BAJA").Value = "-1" Then
        If Not IsNull(LastRow) Then
            If CStr(LastRow) <> CStr(.Bookmark) Then
                For i = 0 To 20
                    If .Columns(i).Name <> "DESCR" And .Columns(i).Name <> "BOT" Then
                        .Columns(i).CellStyleSet "Gris", .Row
                    End If
                    .Columns(i).Locked = True
                Next i
                cmdBajaLog.caption = m_sIdioma(1)
                cmdCodigo.Enabled = False
            End If
        Else
            For i = 0 To 20
                If .Columns(i).Name <> "DESCR" And .Columns(i).Name <> "BOT" Then
                    .Columns(i).CellStyleSet "Gris", .Row
                End If
                .Columns(i).Locked = True
            Next i
            cmdBajaLog.caption = m_sIdioma(1)
            cmdCodigo.Enabled = False
        End If
    Else
        cmdBajaLog.caption = m_sIdioma(2)
        cmdCodigo.Enabled = (.Columns("ID").Value <> "")
        If g_bEdicion Then
            If Not IsNull(LastRow) Then
                If CStr(LastRow) <> CStr(.Bookmark) Then
                    For i = 0 To 18
                        .Columns(i).Locked = False
                    Next i
                End If
            Else
                For i = 0 To 18
                    .Columns(i).Locked = False
                Next i
            End If

            If Not .IsAddRow Then
                .Columns("TIPO").Locked = True
                If (.Columns("idTIPO").Value = 2 Or .Columns("idTIPO").Value = 3) And .Columns("LIBRE").Value = -1 Then
                    .Columns("MINIMO").Locked = False
                    .Columns("MAXIMO").Locked = False
                    If .Columns("idTIPO").Value = "2" Then
                        .Columns("MINIMO").Alignment = ssCaptionAlignmentRight
                        .Columns("MAXIMO").Alignment = ssCaptionAlignmentRight
                    End If
                Else
                    .Columns("MINIMO").Locked = True
                    .Columns("MAXIMO").Locked = True
                End If
                If Not IsNull(LastRow) Then
                    If CStr(LastRow) <> CStr(.Bookmark) Then
                        .Columns("COD").Locked = True
                        cmdEliminar.Enabled = True
                        cmdAnyadir.Enabled = True
                        cmdBajaLog.Enabled = True
                    End If
                Else
                    .Columns("COD").Locked = True
                    cmdEliminar.Enabled = True
                    cmdAnyadir.Enabled = True
                    cmdBajaLog.Enabled = True
                End If
                .Columns("LIBRE").Locked = (.Columns("LISTA_EXTERNA").Value = "1")
                .Columns("SELECCION").Locked = (.Columns("LISTA_EXTERNA").Value = "1")
            Else
                .Columns("TIPO").Locked = False
                If .Columns("COD").Locked = True Then
                    .col = 1
                    cmdDeshacer.Enabled = False
                    .Columns("COD").Locked = False
                    cmdCodigo.Enabled = False
                    cmdEliminar.Enabled = False
                    cmdAnyadir.Enabled = False
                    cmdBajaLog.Enabled = False
                End If
                Select Case .Columns("TIPO").Value
                    Case m_sCombo(1)
                
                        If .Columns("idTIPO").Value = "1" Then
                            .Columns("MINIMO").Value = ""
                            .Columns("MAXIMO").Value = ""
                            .Columns("MINIMO").Locked = True
                            .Columns("MAXIMO").Locked = True
                            .Columns("MINIMO").CellStyleSet "Gris", .Row
                            .Columns("MAXIMO").CellStyleSet "Gris", .Row
                            .Columns("LIBRE").Locked = False
                            .Columns("LIBRE").Value = -1
                            .Columns("SELECCION").Locked = False
                            .Columns("LIBRE").CellStyleSet "Normal", .Row
                            .Columns("SELECCION").CellStyleSet "Normal", .Row
                        End If
                    Case m_sCombo(2)
                        If .Columns("idTIPO").Value = "2" Then
                            .Columns("MINIMO").Locked = False
                            .Columns("MAXIMO").Locked = False
                            .Columns("MINIMO").CellStyleSet "Normal", .Row
                            .Columns("MAXIMO").CellStyleSet "Normal", .Row
                            .Columns("LIBRE").Locked = False
                            .Columns("SELECCION").Locked = False
                            .Columns("LIBRE").Value = "-1"
                            .Columns("LIBRE").CellStyleSet "Normal", .Row
                            .Columns("SELECCION").CellStyleSet "Normal", .Row
                        End If
                    Case m_sCombo(3)
                        If .Columns("idTIPO").Value = "3" Then
                            .Columns("MINIMO").Locked = False
                            .Columns("MAXIMO").Locked = False
                            .Columns("MINIMO").CellStyleSet "Normal", .Row
                            .Columns("MAXIMO").CellStyleSet "Normal", .Row
                            .Columns("LIBRE").Locked = False
                            .Columns("SELECCION").Locked = False
                            .Columns("LIBRE").CellStyleSet "Normal", .Row
                            .Columns("SELECCION").CellStyleSet "Normal", .Row
                        End If
                    Case m_sCombo(4)
                        If .Columns("idTIPO").Value = "4" Then
                            .Columns("MINIMO").Value = ""
                            .Columns("MAXIMO").Value = ""
                            .Columns("LIBRE").Value = "0"
                            .Columns("SELECCION").Value = "0"
                            .Columns("MINIMO").Locked = True
                            .Columns("MAXIMO").Locked = True
                            .Columns("LIBRE").Locked = True
                            .Columns("SELECCION").Locked = True
                            .Columns("MINIMO").CellStyleSet "Gris", .Row
                            .Columns("MAXIMO").CellStyleSet "Gris", .Row
                            .Columns("LIBRE").CellStyleSet "Gris", .Row
                            .Columns("SELECCION").CellStyleSet "Gris", .Row
                        End If
                    Case m_sCombo(5)
                        If .Columns("idTIPO").Value = TiposDeAtributos.TipoArchivo Then
                            .Columns("MINIMO").Value = ""
                            .Columns("MAXIMO").Value = ""
                            .Columns("LIBRE").Value = "0"
                            .Columns("SELECCION").Value = "0"
                            .Columns("MINIMO").Locked = True
                            .Columns("MAXIMO").Locked = True
                            .Columns("LIBRE").Locked = True
                            .Columns("SELECCION").Locked = True
                            .Columns("ASC").Locked = True
                            .Columns("POND").Locked = True
                            .Columns("VDEFECTO").Locked = True
                            .Columns("MINIMO").CellStyleSet "Gris", .Row
                            .Columns("MAXIMO").CellStyleSet "Gris", .Row
                            .Columns("LIBRE").CellStyleSet "Gris", .Row
                            .Columns("SELECCION").CellStyleSet "Gris", .Row
                            .Columns("POND").CellStyleSet "Gris", .Row
                            .Columns("ASC").CellStyleSet "Gris", .Row
                            .Columns("VDEFECTO").CellStyleSet "Gris", .Row
                        End If
                End Select
                
            End If
            'Para la columna del valor por defecto
        End If
    End If
                
    If Not .Columns("TIPO").Locked Then
        sdbddTipo.Enabled = True
    Else
        sdbddTipo.Enabled = False
    End If
    If .Columns("idTIPO").Value = 4 Then
        .Columns("MINIMO").Locked = True
        .Columns("MAXIMO").Locked = True
        .Columns("LIBRE").Locked = True
        .Columns("SELECCION").Locked = True
        .Columns("MINIMO").CellStyleSet "Gris", .Row
        .Columns("MAXIMO").CellStyleSet "Gris", .Row
        .Columns("LIBRE").CellStyleSet "Gris", .Row
        .Columns("SELECCION").CellStyleSet "Gris", .Row
        .Columns("LIBRE").Value = "0"
    End If
    
    
    
            If .Columns("OFERTA").Value <> 1 And .Columns("OFERTA").Value <> -1 Then
                                    
                Select Case .Columns("SELECCION").Value
                    Case -1:  'Seleccion
                        sdbddValor.RemoveAll
                        sdbddValor.AddItem ""
                        .Columns("VDEFECTO").DropDownHwnd = sdbddValor.hWnd
                        sdbddValor.Enabled = True
                        m_bClickEnListaCombo = True
                        sdbddValor_DropDown
                        m_bClickEnListaCombo = False
                    Case Else 'Libre
                        If .Columns("idTIPO").Value = TipoBoolean Then
                            sdbddValor.RemoveAll
                            sdbddValor.AddItem ""
                            .Columns("VDEFECTO").DropDownHwnd = sdbddValor.hWnd
                            sdbddValor.Enabled = True
                        Else
                            .Columns("VDEFECTO").DropDownHwnd = 0
                            sdbddValor.Enabled = False
                            If .Columns("TIPO").Value = m_sCombo(1) Then
                                .Columns("VDEFECTO").Style = ssStyleEditButton
                                DoEvents
                            End If
                        End If
                End Select
            Else
                .Columns("VDEFECTO").DropDownHwnd = 0
                sdbddValor.Enabled = False
                .Columns("VDEFECTO").Locked = True
            End If
    If .Columns("OFERTA").Value <> 1 And .Columns("OFERTA").Value <> -1 And .Columns("IDTIPO").Value <> TiposDeAtributos.TipoArchivo Then
        If g_bEdicion Then
            .Columns("VDEFECTO").Locked = False
        Else
            .Columns("VDEFECTO").Locked = True
        End If
        .Columns("VDEFECTO").CellStyleSet "Normal", .Row
    Else
        .Columns("VDEFECTO").Locked = True
        .Columns("VDEFECTO").CellStyleSet "Gris", .Row
    End If
End With
End Sub


''' <summary>
''' Evento producido en el campo del valor del atributo al hacer click para expandir la lista de valores del atributo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click en la lista de valores de un atributo </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>

Private Sub sdbddValor_DropDown()

Dim oLista As CValorPond
Dim iIndice As Integer


    If sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked Then
        sdbddValor.DroppedDown = False
        Exit Sub
    Else
        If m_oAtributos Is Nothing Then
            Exit Sub
        End If
    End If
    
    
iIndice = 1

sdbddValor.RemoveAll

    Select Case sdbgAtributosGrupoCtrl.Columns("idtipo").Value
    
    Case 1, 2, 3
        Set frmLista.g_oAtributo = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value)
            
            If frmLista.g_oAtributo Is Nothing Then
                If Not m_bClickEnListaCombo Then
                    MsgBox m_sMensajes(17), vbInformation, "FULLSTEP"
                End If
            Else
                m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).CargarListaDeValores
                For Each oLista In m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaPonderacion
                    sdbddValor.AddItem iIndice & Chr(m_lSeparador) & oLista.ValorLista
                    iIndice = iIndice + 1
                Next
            End If
        
    Case 4
        sdbddValor.AddItem "" & Chr(m_lSeparador) & ""
        sdbddValor.AddItem 1 & Chr(m_lSeparador) & m_sIdiTrue
        sdbddValor.AddItem 0 & Chr(m_lSeparador) & m_sIdiFalse
        
        
    End Select


End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
''' * Objetivo: Validar la seleccion (nulo o existente)
''' Si es nulo, correcto
Dim bExiste As Boolean
Dim oLista As CValoresPond
Dim oElem As CValorPond
Dim oatrib As CAtributo

    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        
        Set oatrib = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value)
        If oatrib Is Nothing Then Exit Sub

        If oatrib.TipoIntroduccion = Introselec Then
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If oatrib.Tipo = TipoBoolean Then
                If UCase(m_sIdiTrue) = UCase(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text) Then
                    bExiste = True
                End If
                If UCase(m_sIdiFalse) = UCase(sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text) Then
                    bExiste = True
                End If

            End If
        End If
        If Not bExiste Then
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Text = ""
            oMensajes.NoValido sdbgAtributosGrupoCtrl.Columns("VDEFECTO").caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
End Sub



Private Sub sdbgAtributosGrupoCtrl_RowLoaded(ByVal Bookmark As Variant)
Dim i As Integer

If m_bTodas Then
    If sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "1" Or sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "-1" Then
        For i = 0 To 18
            sdbgAtributosGrupoCtrl.Columns(i).Locked = True
            If sdbgAtributosGrupoCtrl.Columns(i).Name <> "DESCR" And sdbgAtributosGrupoCtrl.Columns(i).Name <> "BOT" Then
                sdbgAtributosGrupoCtrl.Columns(i).CellStyleSet "Gris"
            End If
        Next i
        cmdBajaLog.caption = m_sIdioma(1)
        cmdCodigo.Enabled = False
        If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1 Then
           sdbgAtributosGrupoCtrl.Columns("BOT").Value = "..."
        End If
    Else
        cmdBajaLog.caption = m_sIdioma(2)
        cmdCodigo.Enabled = True
        If sdbgAtributosGrupoCtrl.Columns("OFERTA").Value = -1 Or sdbgAtributosGrupoCtrl.Columns("OFERTA").Value = -1 Then
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").CellStyleSet "Gris"
        End If
        If sdbgAtributosGrupoCtrl.Columns("idTIPO").Value = 4 Or sdbgAtributosGrupoCtrl.Columns("idTipo").Value = TipoArchivo Then
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = True
            sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = True
            sdbgAtributosGrupoCtrl.Columns("LIBRE").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("SELECCION").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = "0"
        End If
        If sdbgAtributosGrupoCtrl.Columns("idTipo").Value = TipoArchivo Then
            sdbgAtributosGrupoCtrl.Columns("VDEFECTO").CellStyleSet "Gris"
        End If
        If sdbgAtributosGrupoCtrl.Columns("LIBRE").Value = -1 Then
            If sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(2) Or sdbgAtributosGrupoCtrl.Columns("TIPO").Value = m_sCombo(3) Then
                If g_bEdicion = True Then
                    sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = False
                    sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = False
                Else
                    sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
                    sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
                End If
                sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Normal"
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Normal"
            Else
                sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
                sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Gris"
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
                sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Gris"
            End If
        Else
            If sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = -1 Then
               sdbgAtributosGrupoCtrl.Columns("BOT").Value = "..."
            End If
            sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MINIMO").CellStyleSet "Gris"
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
            sdbgAtributosGrupoCtrl.Columns("MAXIMO").CellStyleSet "Gris"
        End If
    End If
    If sdbgAtributosGrupoCtrl.Columns("HIDENDESCR").Value <> "" Then
        sdbgAtributosGrupoCtrl.Columns("DESCR").Value = "..."
    Else
        sdbgAtributosGrupoCtrl.Columns("DESCR").Value = ""
    End If
End If

If sdbgAtributosGrupoCtrl.Columns("TipoPond").Value <> "0" Then
    sdbgAtributosGrupoCtrl.Columns("POND").CellStyleSet "Ponderacion"
Else
    sdbgAtributosGrupoCtrl.Columns("POND").CellStyleSet ""
End If
If sdbgAtributosGrupoCtrl.Columns("ID").Value <> "" Then
    If m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ambitoAtributoUon Then
        sdbgAtributosGrupoCtrl.Columns("UONS").Value = "..."
    End If
End If
End Sub


Private Sub sdbgAtributosGrupoCtrl_Scroll(Cancel As Integer)
    sdbgAtributosGrupoCtrl.Columns("VDEFECTO").DropDownHwnd = 0
End Sub

Private Sub sstabgeneralCtrl_Click(PreviousTab As Integer)
    If PreviousTab = 0 And sstabgeneralCtrl.Tab = 1 Then
    'DES_PRT_GS_181
'        If bRComprador Then
'        'El usuario tiene la restricci�n de
'        '"Restringir las acciones al material del comprador"
'        'en la secci�n de atributos.
'            If sdbcGMN1_4CodCtrl.Text = "" And g_sOrigen <> "POSI" Then
'            'Se le obliga a seleccionar algo en filtro de materiales.
'                sstabgeneralCtrl.Tab = 0
'                Exit Sub
'            End If
'        End If
        Screen.MousePointer = vbHourglass
        
        m_bError = False
        CargarAtributos
        Screen.MousePointer = vbNormal
        If m_bError Then
          sstabgeneralCtrl.Tab = 0
          Exit Sub
        End If
        sdbgAtributosGrupoCtrl.Columns("COD").Locked = True

        g_bEdicion = False
    End If
  
    If sstabgeneralCtrl.Tab = 0 And Not g_bEdicion Then
        If g_sOrigen <> "ART4" And g_sOrigen <> "GMN4" Then
            g_sGmn1 = sdbcGMN1_4CodCtrl.Text
            g_sGmn2 = sdbcGMN2_4CodCtrl.Text
            g_sGmn3 = sdbcGMN3_4CodCtrl.Text
            g_sGmn4 = sdbcGMN4_4CodCtrl.Text
            g_sArtCod = sdbcArtiCod.Text
        End If
        sdbgAtributosGrupoCtrl.AllowAddNew = False
        picNavigate.Visible = False
        picEdit.Visible = False
        g_bEdicion = False
    End If
  
  If sstabgeneralCtrl.Tab = 1 Then
        sdbgAtributosGrupoCtrl.Columns("OFERTA").Locked = True
        sdbgAtributosGrupoCtrl.Columns("COD").Locked = True
        sdbgAtributosGrupoCtrl.Columns("NOMBRE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("TIPO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("LIBRE").Locked = True
        sdbgAtributosGrupoCtrl.Columns("MINIMO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("MAXIMO").Locked = True
        sdbgAtributosGrupoCtrl.Columns("SELECCION").Locked = True
        sdbgAtributosGrupoCtrl.Columns("ASC").Locked = True
        g_bEdicion = False
        picNavigate.Visible = True
        
        If m_bModifAtri Then
            cmdModoEdicion2Ctrl.Visible = Not g_bSoloSeleccion
        Else
            cmdModoEdicion2Ctrl.Visible = False
        End If
  End If
End Sub

''' <summary>
''' Mostrar la pantalla con los valores del combo
''' </summary>
''' <remarks>Llamada desde: sdbgAtributosGrupoCtrl_BeforeUpdate         sdbgAtributosGrupoCtrl_BtnClick; Tiempo m�ximo: 0,2</remarks>
Public Sub CargarListaValores()
Dim oLista As CValorPond
Dim SFormularios As String
Dim SProcesos As String
Dim SPedidos As String
Dim sCadena As String

    If cmdBajaLog.caption = m_sIdioma(2) Then
        If m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaExterna Then
            frmLista.g_bEdicion = False
        Else
            frmLista.g_bEdicion = g_bEdicion
        End If
    Else
        frmLista.g_bEdicion = False
    End If
    
    If g_bEdicion Then
    
            If Not m_bCambioSeleccion Then

            Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))

            If g_oAtributoEnEdicion.ComprobarAtributoEnFormsPM(SFormularios) Then
                m_bRespuesta = False
                frmAtribMsg.g_sOrigen = g_sOrigen & "frmATRIB"
                frmAtribMsg.m_itipoMensaje = 1
                frmAtribMsg.txtMsg.Text = m_sFormularios & vbCrLf & SFormularios
                frmAtribMsg.Show 1

                If m_bRespuesta = False Then
                    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                    m_bCambioSeleccion = False
                    Exit Sub
                End If

            End If

            If g_oAtributoEnEdicion.ComprobarAtributoEnProcesosYPedidos(SProcesos, SPedidos) Then
                m_bRespuesta = False
                frmAtribMsg.g_sOrigen = g_sOrigen & "frmATRIB"
                frmAtribMsg.m_itipoMensaje = 2
                If SProcesos <> "" Then
                    sCadena = m_sProcesos & vbCrLf
                    sCadena = sCadena & SProcesos
                End If

                'If sPedidos <> "" Then
                '    sCadena = sCadena & m_sPedidos & vbCrLf
                '    sCadena = sCadena & sPedidos
                'End If


                frmAtribMsg.txtMsg.Text = sCadena
                frmAtribMsg.Show 1

                If m_bRespuesta = False Then
                    m_bCambioSeleccion = False
                    If m_oContainer.Visible Then sdbgAtributosGrupoCtrl.SetFocus
                    Exit Sub
                End If

            Else
                m_bCambioSeleccion = False
            End If
            
                
            End If
    End If
    
    Set frmLista.g_oAtributo = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value)
    frmLista.sdbgValores.RemoveAll
        
    m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).CargarListaDeValores
    frmLista.g_iIndiCole = 1
    Set frmLista.g_oPondIntermedia = Nothing
    Set frmLista.g_oPondIntermedia = oFSGSRaiz.Generar_CValoresPond
    For Each oLista In m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaPonderacion
        frmLista.sdbgValores.AddItem oLista.ValorLista & Chr(m_lSeparador) & frmLista.g_iIndiCole & Chr(m_lSeparador) & oLista.IDAtributo & Chr(m_lSeparador) & oLista.ValorPond & Chr(m_lSeparador) & oLista.FechaActualizacion
        frmLista.g_oPondIntermedia.Add oLista.IDAtributo, oLista.AnyoProce, oLista.GMN1Proce, , , , oLista.ValorLista, , , , oLista.FechaActualizacion, frmLista.g_iIndiCole
        frmLista.g_iIndiCole = frmLista.g_iIndiCole + 1
    Next
    frmLista.optListaExterna.Value = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).ListaExterna
    
    frmLista.g_sOrigen = g_sOrigen & "frmATRIB"
    frmLista.Top = sdbgAtributosGrupoCtrl.Top + sdbgAtributosGrupoCtrl.ActiveCell.Top + 200
    frmLista.Show 1
    
    m_bCambioSeleccion = False
End Sub
''' <summary>
''' Carga la pantalla de Ponderaci�n
''' </summary>
''' <remarks>Llamada desde: frmatrib/sdbgAtributosGrupo_BtnClick ; Tiempo m�ximo: 0</remarks>
Public Sub CargarPonderacion()

    m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).CargarPonderacion
    Set frmPonderacion.g_oAtributo = m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value)
        
    frmPonderacion.g_sOrigen = g_sOrigen & "frmATRIB"
    frmPonderacion.g_bError = False
    frmPonderacion.g_bAtribNoModif = Not m_bModifAtri
    If sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "1" Or sdbgAtributosGrupoCtrl.Columns("BAJA").Value = "-1" Then
        frmPonderacion.g_bEdicion = False
        frmPonderacion.cmdModif1.Visible = False
    Else
        frmPonderacion.g_bEdicion = g_bEdicion
    End If
    frmPonderacion.WindowState = vbNormal
    If Not frmPonderacion.g_bError Then
        frmPonderacion.Show 1
    Else
        Unload frmPonderacion
    End If
       
End Sub
Public Sub EliminarAtributoSeleccionado()
Dim irespuesta As Integer
Dim teserror As TipoErrorSummit


    Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
    Set m_oIBAseDatosEnEdicion = g_oAtributoEnEdicion
    
    irespuesta = oMensajes.PreguntaEliminar(m_sIdiMenAtributo & " " & sdbgAtributosGrupoCtrl.Columns("COD").Value & " - " & sdbgAtributosGrupoCtrl.Columns("NOMBRE").Value)

    If irespuesta = vbYes Then
        teserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos
        
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
            Exit Sub
        Else
            basSeguridad.RegistrarAccion AccionesSummit.ACCAtributosEli, "Id:" & CStr(g_oAtributoEnEdicion.Id)
        End If
        
        sdbgAtributosGrupoCtrl.SelBookmarks.Add sdbgAtributosGrupoCtrl.Bookmark
        m_oAtributos.Remove (CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
        If sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.Bookmark) > -1 Then
            sdbgAtributosGrupoCtrl.RemoveItem (sdbgAtributosGrupoCtrl.AddItemRowIndex(sdbgAtributosGrupoCtrl.Bookmark))
        Else
            sdbgAtributosGrupoCtrl.RemoveItem (0)
        End If
    End If
    
    If sdbgAtributosGrupoCtrl.Rows > 0 Then
        If IsEmpty(sdbgAtributosGrupoCtrl.RowBookmark(sdbgAtributosGrupoCtrl.Row)) Then
            sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.RowBookmark(sdbgAtributosGrupoCtrl.Row - 1)
        Else
            sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.RowBookmark(sdbgAtributosGrupoCtrl.Row)
        End If
    End If
    sdbgAtributosGrupoCtrl.SelBookmarks.RemoveAll
    
    If sdbgAtributosGrupoCtrl.Rows = 0 Then
        cmdEliminar.Enabled = False
    Else
        cmdEliminar.Enabled = True
    End If
    
End Sub

Private Sub BorrarPondeYvaloresLista()
    Dim irespuesta As Integer
    
    m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).CargarListaDeValores
    If m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value)).ListaPonderacion.Count > 0 Then

            ''''''''''''PREGUNTA ELIMINAR LISTA DE VALORES
        irespuesta = oMensajes.PreguntaEliminarListaValores
        
        If irespuesta = vbNo Then
           m_bRespetarCheck = False
           cmdDeshacer_Click
           Exit Sub
        End If
    End If

sdbgAtributosGrupoCtrl.Columns("BOT").Value = ""
sdbgAtributosGrupoCtrl.Columns("SELECCION").Value = 0

''''''''''''PREGUNTA DE CAMBIO DE PONDERACION
 
If sdbgAtributosGrupoCtrl.Columns("TipoPond").Value <> "4" And sdbgAtributosGrupoCtrl.Columns("TipoPond").Value <> "0" Then
    If sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "1" And ((sdbgAtributosGrupoCtrl.Columns("idTipo").Value <> "2" And sdbgAtributosGrupoCtrl.Columns("idTipo").Value <> "3") Or (StrToDbl0(sdbgAtributosGrupoCtrl.Columns("idTipo").Value) <> m_oAtributos.Item(sdbgAtributosGrupoCtrl.Columns("ID").Value).Tipo)) Then
        irespuesta = oMensajes.PreguntaCambiarPonderacion
        If irespuesta = vbNo Then
           m_bRespetarCheck = False
           cmdDeshacer_Click
           Exit Sub
        End If
        sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "0"
    End If
    If sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "2" Then
        irespuesta = oMensajes.PreguntaCambiarPonderacion
        If irespuesta = vbNo Then
           m_bRespetarCheck = False
           cmdDeshacer_Click
           Exit Sub
        End If
        sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "0"
    End If
    If sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "3" And sdbgAtributosGrupoCtrl.Columns("idTipo").Value <> "2" Then
        irespuesta = oMensajes.PreguntaCambiarPonderacion
        If irespuesta = vbNo Then
           m_bRespetarCheck = False
           cmdDeshacer_Click
           Exit Sub
        End If
        sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "0"

    End If
    If sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "5" And sdbgAtributosGrupoCtrl.Columns("idTipo").Value <> "4" Then
        irespuesta = oMensajes.PreguntaCambiarPonderacion
        If irespuesta = vbNo Then
           m_bRespetarCheck = False
           cmdDeshacer_Click
           Exit Sub
        End If
        sdbgAtributosGrupoCtrl.Columns("TipoPond").Value = "0"
    End If
End If

End Sub
Private Sub ComprobarPeticionesDeOfertas(iNuevoTipo As Integer, bComprobarAplic As Boolean)
Dim iExisten As Integer
Dim iResp As Integer

    Set Adores = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value)).BuscarEnProcesoPeticionesYOfertas
    While Not Adores.EOF
        If Adores("PETICIONES").Value > 0 Then
            If Adores("TIPOV").Value = 1 Then
                oMensajes.ImposibleCambiarTipoDatoAtributo (m_sMensajes(10) & " " & Adores("ANYO").Value & "-" & Adores("GMN1").Value & "-" & Adores("PROCE").Value & ", " & m_sMensajes(7))
                m_bRespetarCheck = False
                cmdDeshacer_Click
                m_bNoCambio = True
                Exit Sub
            Else
                oMensajes.ImposibleCambiarTipoDatoAtributo (m_sMensajes(11) & " " & Adores("ANYO").Value & "/" & Adores("GMN1").Value & "/" & Adores("PROCE").Value)
                m_bRespetarCheck = False
                cmdDeshacer_Click
                m_bNoCambio = True
                Exit Sub
            End If
        ElseIf Adores("OFERTAS").Value > 0 Then
            oMensajes.ImposibleCambiarTipoDatoAtributo (m_sMensajes(10) & " " & Adores("ANYO").Value & "-" & Adores("GMN1").Value & "-" & Adores("PROCE").Value & ", " & m_sMensajes(9))
            m_bRespetarCheck = False
            cmdDeshacer_Click
            m_bNoCambio = True
            Exit Sub

        Else
            If Not IsNull(Adores("POND").Value) Then
                If Adores("POND").Value <> 0 And Adores("POND").Value <> 4 Then
                    If Adores("POND").Value = 1 And iNuevoTipo <> "2" And iNuevoTipo <> "3" Then
                        oMensajes.ImposibleCambiarTipoDatoAtributo (m_sMensajes(10) & " " & Adores("ANYO").Value & "-" & Adores("GMN1").Value & "-" & Adores("PROCE").Value & ", " & m_sMensajes(8))
                        m_bRespetarCheck = False
                        cmdDeshacer_Click
                        m_bNoCambio = True
                        Exit Sub
                    Else
                       If Adores("POND").Value = 2 Then
                            oMensajes.ImposibleCambiarTipoDatoAtributo (m_sMensajes(10) & " " & Adores("ANYO").Value & "-" & Adores("GMN1").Value & "-" & Adores("PROCE").Value & ", " & m_sMensajes(8))
                            m_bRespetarCheck = False
                            cmdDeshacer_Click
                            m_bNoCambio = True
                            Exit Sub
                       Else
                            If Adores("POND").Value = 3 And iNuevoTipo <> "2" Then
                                oMensajes.ImposibleCambiarTipoDatoAtributo (m_sMensajes(10) & " " & Adores("ANYO").Value & "-" & Adores("GMN1").Value & "-" & Adores("PROCE").Value & ", " & m_sMensajes(8))
                                m_bRespetarCheck = False
                                cmdDeshacer_Click
                                m_bNoCambio = True
                                Exit Sub
                            Else
                                If Adores("POND").Value = 5 And iNuevoTipo <> "4" Then
                                    oMensajes.ImposibleCambiarTipoDatoAtributo (m_sMensajes(10) & " " & Adores("ANYO").Value & "-" & Adores("GMN1").Value & "-" & Adores("PROCE").Value & ", " & m_sMensajes(8))
                                    m_bRespetarCheck = False
                                    cmdDeshacer_Click
                                    m_bNoCambio = True
                                    Exit Sub
                                End If
                            End If
                       End If
                    End If
                End If
            End If
        End If
        Adores.MoveNext
    Wend
    Adores.Close
    Set Adores = Nothing
    If bComprobarAplic Then
        iExisten = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value)).BuscarEnProcesoYPlantillaAtribsAplicables
        If iExisten > 0 Then
            iResp = oMensajes.PreguntaCambioTipoAtributo(iExisten)
            If iResp = vbNo Then
                m_bRespetarCheck = False
                cmdDeshacer_Click
                m_bNoCambio = True
                Exit Sub
            End If
        End If
    End If
End Sub

Private Sub ConfigurarNombres()
    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"
End Sub

Private Sub cmdBuscarUon_Click()
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim ouons1 As CUnidadesOrgNivel1
    Dim ouons2 As CUnidadesOrgNivel2
    Dim ouons3 As CUnidadesOrgNivel3
    Dim iPerfil As Integer
    If oUsuarioSummit.Perfil Is Nothing Then
        iPerfil = 0
    Else
        iPerfil = oUsuarioSummit.Perfil.Id
    End If
    Set ouons1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set ouons2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set ouons3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    
    ouons1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, iPerfil, , , False
    ouons2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, iPerfil, , , False
    ouons3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, iPerfil, , False
    
    Dim frm As frmSELUO
    Set frm = New frmSELUO
    
    frm.multiselect = True
    frm.GuardarUon0 = False
    frm.cargarArbol ouons1, ouons2, ouons3
    
    If m_oUonsSeleccionadas.Count > 0 Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    
    If chkUonCoincidente.Value = True Then
        frm.CheckChildren = False
    Else
        frm.CheckChildren = True
    End If
    frm.Show vbModal
    If frm.Aceptar Then
        txtUonSeleccionada.Text = frm.UonsSeleccionadas.titulo
        Set m_oUonsSeleccionadas = frm.UonsSeleccionadas
    End If
    Set ouons1 = Nothing
    Set ouons2 = Nothing
    Set ouons3 = Nothing
    Set frm = Nothing
End Sub

Private Function abrirFormUons(ByVal isAlta As Boolean)
    
    Dim oUons As CUnidadesOrgNivel3
    Set oUons = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    Dim oAtributo As CAtributo
    
    If isAlta Then
        Set oAtributo = g_oAtributoEnEdicion
    Else
        Set oAtributo = Me.AtributoSeleccionado
    End If
    If Not oAtributo Is Nothing Then
        If oAtributo.ambitoAtributoUon Then
            Dim frm As frmSELUO
            Set frm = New frmSELUO
            
            oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, oUsuarioSummit.Perfil.Id
            oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, oUsuarioSummit.Perfil.Id
            oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantAtribUsu, m_bRestrMantAtribPerf, oUsuarioSummit.Perfil.Id
              
            frm.multiselect = True
            'No guardamos la uon de nivel 0, en su caso seleccionamos todas las uon de nivel 1
            frm.GuardarUon0 = False
            frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
            frm.subtitulo = oAtributo.CodDen
            
            If Not isAlta Then
                Set frm.UonsSeleccionadas = oAtributo.uons
            Else
                frm.enableCancelar = False
            End If
            
            frm.Show vbModal
            'Una vez cerrado el form obtenemos las uons seleccionadas y las actualizamos
        
            If frm.Aceptar Then
                'Antes de reasignar uons debemos comprobar que ninguna de las eliminadas tiene atributos con valor
                'o en caso contrario deberemos advertir que �stos se borrar�n
                Dim oUonsNuevas As CUnidadesOrganizativas
                Set oUonsNuevas = frm.UonsSeleccionadas
                Dim oUonsABorrar As CUnidadesOrganizativas
                Set oUonsABorrar = oFSGSRaiz.Generar_CUnidadesOrganizativas
                Dim oUon As IUOn
                Dim borrar As Boolean
                For Each oUon In oAtributo.uons
                    If oUon.tieneAtributoValorado(oAtributo.Id) Then
                        If Not oUonsNuevas.CONTIENE(oUon) Then
                            oUonsABorrar.Add oUon
                            borrar = True
                        End If
                    End If
                Next
                If borrar = True Then
                    If oMensajes.DesvincularUonsAtrib(oUonsABorrar.toString, oAtributo.CodDen) = vbYes Then
                        oAtributo.eliminarAtributoArticuloEnUons oUonsABorrar
                    Else
                        frm.Aceptar = False
                    End If
                End If
                
                
                Set oUon = Nothing
                Set oUonsNuevas = Nothing
            End If
            'si no es un alta actualizamos las uons, en caso contrio lo haremos cuando ya se haya creado el atributo en BD
            If Not isAlta And frm.Aceptar Then
                Set oAtributo.uons = frm.UonsSeleccionadas
                oAtributo.updateUons
            End If
        End If
    End If
    Set frm = Nothing
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    Set oAtributo = Nothing

End Function

Public Property Get UonsSeleccionadas() As CUnidadesOrganizativas
    Set UonsSeleccionadas = m_oUonsSeleccionadas
End Property

Public Property Set UonsSeleccionadas(colUonsSeleccionadas As CUnidadesOrganizativas)
    Set m_oUonsSeleccionadas = colUonsSeleccionadas
End Property

Private Sub DevolverAtributoDirecto_frmPedidos()
    Dim inum As Integer
    Dim m_Col As Object
    Dim oGrid As SSDBGrid
    
    inum = 0
                 
    Select Case g_sOrigen
        Case "frmPEDIDOSOE"
            Set m_Col = g_oOrigen.g_oOrdenEntrega.ATRIBUTOS
            Set oGrid = g_oOrigen.sdbgAtributos
        Case "frmPEDIDOSLP"
            Set m_Col = g_oOrigen.g_oOrdenEntrega.AtributosLineas
            Set oGrid = g_oOrigen.sdbgAtributosLinea
    End Select
    If sdbgAtributosGrupoCtrl.SelBookmarks.Count > 0 Then
        While inum < sdbgAtributosGrupoCtrl.SelBookmarks.Count
            sdbgAtributosGrupoCtrl.Bookmark = sdbgAtributosGrupoCtrl.SelBookmarks(inum)
            inum = inum + 1
            
            Set g_oAtributoEnEdicion = m_oAtributos.Item(CStr(sdbgAtributosGrupoCtrl.Columns("ID").Value))
            If g_oAtributoEnEdicion.TipoIntroduccion = Introselec Then
                If g_oAtributoEnEdicion.ListaPonderacion Is Nothing Then
                    g_oAtributoEnEdicion.CargarListaDeValores
                ElseIf g_oAtributoEnEdicion.ListaPonderacion.Count = 0 Then
                    g_oAtributoEnEdicion.CargarListaDeValores
                    If g_oAtributoEnEdicion.ListaPonderacion.Count = 0 Then
                        g_oAtributoEnEdicion.TipoIntroduccion = IntroLibre
                    End If
                End If
            End If
            If m_Col Is Nothing Then
                Set m_Col = oFSGSRaiz.Generar_CAtributosOfertados
            End If
            If m_Col.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                Select Case g_sOrigen
                    Case "frmPEDIDOSOE"
                        m_Col.Add g_oAtributoEnEdicion.Id, , , , Null, Null, Null, , Null, , , g_oAtributoEnEdicion
                    Case "frmPEDIDOSLP"
                        m_Col.Add g_oAtributoEnEdicion.Id, g_oAtributoEnEdicion.Cod, g_oAtributoEnEdicion.Den, g_oAtributoEnEdicion.Tipo, , , , , , , , , , , , , , , , g_oAtributoEnEdicion.TipoIntroduccion, g_oAtributoEnEdicion.Minimo, g_oAtributoEnEdicion.Maximo, , g_oAtributoEnEdicion.interno, , , , , , , g_oAtributoEnEdicion.ListaPonderacion, , , , , , , , , , g_oAtributoEnEdicion.Validacion_ERP, , , , , , True
                        g_oOrigen.g_oLineaSeleccionada.ATRIBUTOS.Add g_oAtributoEnEdicion.Id, , , , Null, Null, Null, , Null, , , g_oAtributoEnEdicion
                End Select
                oGrid.AddNew
                oGrid.Columns("COD").Text = g_oAtributoEnEdicion.Cod
                oGrid.Columns("DEN").Value = g_oAtributoEnEdicion.Den
                
                Select Case g_oAtributoEnEdicion.Tipo
                    Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                        oGrid.Columns("VALOR").Alignment = ssCaptionAlignmentLeft
                        If g_oAtributoEnEdicion.TipoIntroduccion = IntroLibre Then
                            oGrid.Columns("VALOR").Style = ssStyleEdit
                            oGrid.Columns("VALOR").FieldLen = cLongAtribEsp_Texto
                        Else
                            oGrid.Columns("VALOR").FieldLen = 800
                        End If
                                                                        
                        If g_sOrigen = "frmPEDIDOSOE" Then
                            If Not g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)).valorText
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorText
                            Else
                                oGrid.Columns("VALOR").Value = g_oAtributoEnEdicion.valorText
                            End If
                        Else
                            If Not g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)).valorText
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorText
                            Else
                                oGrid.Columns("VALOR").Value = g_oAtributoEnEdicion.valorText
                            End If
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        oGrid.Columns("VALOR").Alignment = ssCaptionAlignmentRight
                        If g_oAtributoEnEdicion.TipoIntroduccion = IntroLibre Then
                            oGrid.Columns("VALOR").Style = ssStyleEdit
                        End If
                                                                    
                        If g_sOrigen = "frmPEDIDOSOE" Then
                            If Not g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)).valorNum
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorNum
                            Else
                                oGrid.Columns("VALOR").Value = g_oAtributoEnEdicion.valorNum
                            End If
                        Else
                            If Not g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)).valorNum
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorNum
                            Else
                                oGrid.Columns("VALOR").Value = g_oAtributoEnEdicion.valorNum
                            End If
                        End If
                        
                    Case TiposDeAtributos.TipoFecha
                        oGrid.Columns("VALOR").Alignment = ssCaptionAlignmentRight
                        If g_oAtributoEnEdicion.TipoIntroduccion = IntroLibre Then
                            oGrid.Columns("VALOR").Style = ssStyleEdit
                        End If
                        
                        If g_sOrigen = "frmPEDIDOSOE" Then
                            If Not g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)).valorFec
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorFec
                            Else
                                oGrid.Columns("VALOR").Value = g_oAtributoEnEdicion.valorFec
                            End If
                        Else
                            If Not g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)).valorFec
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                oGrid.Columns("VALOR").Value = g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorFec
                            Else
                                oGrid.Columns("VALOR").Value = g_oAtributoEnEdicion.valorFec
                            End If
                        End If
                        
                    Case TiposDeAtributos.TipoBoolean
                        oGrid.Columns("VALOR").Style = ssStyleEdit
                                                
                        If g_sOrigen = "frmPEDIDOSOE" Then
                            If Not g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                If g_oOrigen.g_oAtributosProce.Item(CStr(g_oAtributoEnEdicion.Id)).valorBool Then
                                   oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                   oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                If g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor Then
                                    oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                    oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                If g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorBool Then
                                    oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                    oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                            Else
                                If g_oAtributoEnEdicion.valorBool Then
                                   oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                   oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                            End If
                        Else
                            If Not g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)) Is Nothing Then
                                If g_oOrigen.g_oAtributosLinea.Item(CStr(g_oOrigen.g_sItem_AtributosLinea & g_oAtributoEnEdicion.Id)).valorBool Then
                                   oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                   oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                                
                            ElseIf Not g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                If g_oOrigen.g_oAtributosDefecto.Item(CStr(g_oAtributoEnEdicion.Id)).valor Then
                                   oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                   oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                            ElseIf Not g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)) Is Nothing Then
                                If g_oOrigen.g_oAtributosIntegracion.Item(CStr(g_oAtributoEnEdicion.Id)).valorBool Then
                                    oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                    oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                            Else
                                If g_oAtributoEnEdicion.valorBool Then
                                   oGrid.Columns("VALOR").Value = m_sIdiTrue
                                Else
                                   oGrid.Columns("VALOR").Value = m_sIdiFalse
                                End If
                            End If
                        End If
                End Select
                            
                oGrid.Columns("ID").Value = g_oAtributoEnEdicion.Id
                Select Case g_sOrigen
                    Case "frmPEDIDOSOE"
                        g_oOrigen.FuerzaPosicion
                End Select
                oGrid.Columns("INTERNO").Value = g_oAtributoEnEdicion.interno
                g_oAtributoEnEdicion.pedido = True
                oGrid.Update
            End If
        Wend
    End If
End Sub
