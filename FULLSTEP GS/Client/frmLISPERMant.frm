VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmLISPERMant 
   Caption         =   "Listados personalizados"
   ClientHeight    =   4440
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5910
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLISPERMant.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4440
   ScaleWidth      =   5910
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   0
      ScaleHeight     =   495
      ScaleWidth      =   5910
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3945
      Width           =   5910
      Begin VB.CommandButton cmdListado 
         Caption         =   "&Listado"
         Height          =   345
         Left            =   1200
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdRestaurar 
         Caption         =   "&Restaurar"
         Height          =   345
         Left            =   60
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "&Eliminar"
         Height          =   345
         Left            =   1200
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdA�adir 
         Caption         =   "&A�adir"
         Height          =   345
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.CommandButton cmdModoEdicion 
         Caption         =   "&Edici�n"
         Height          =   345
         Left            =   4860
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   120
         Width           =   1005
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer"
         Enabled         =   0   'False
         Height          =   345
         Left            =   2340
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   1005
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgListados 
      Height          =   3705
      Left            =   45
      TabIndex        =   0
      Top             =   60
      Width           =   9990
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   9
      AllowUpdate     =   0   'False
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3969
      Columns(1).Caption=   "Archivo ""rpt"""
      Columns(1).Name =   "RPT"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   5741
      Columns(2).Caption=   "Denominaci�n"
      Columns(2).Name =   "DEN"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "FILTRO"
      Columns(3).Name =   "FILTRO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   873
      Columns(4).Caption=   "GS"
      Columns(4).Name =   "GS"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Style=   2
      Columns(5).Width=   873
      Columns(5).Caption=   "EP"
      Columns(5).Name =   "EP"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(6).Width=   873
      Columns(6).Caption=   "PM"
      Columns(6).Name =   "PM"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   873
      Columns(7).Caption=   "QA"
      Columns(7).Name =   "QA"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "FILTROID"
      Columns(8).Name =   "FILTROID"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   2
      Columns(8).FieldLen=   256
      _ExtentX        =   17621
      _ExtentY        =   6535
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddFiltros 
      Height          =   1065
      Left            =   120
      TabIndex        =   8
      Top             =   3960
      Width           =   3735
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      HeadLines       =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   5292
      Columns(0).Caption=   "DEN"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   2
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "GS"
      Columns(2).Name =   "GS"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "EP"
      Columns(3).Name =   "EP"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "PM"
      Columns(4).Name =   "PM"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "QA"
      Columns(5).Name =   "QA"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   6588
      _ExtentY        =   1879
      _StockProps     =   77
   End
End
Attribute VB_Name = "frmLISPERMant"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

''' Variables de control
Private bModoEdicion As Boolean
Private bModif  As Boolean
Private bAnyadir As Boolean

''' Control de errores

Private bModError As Boolean
Private bAnyaError As Boolean
Private bValError As Boolean

'Multilenguaje
Private sIdiTitulos(1 To 2) As String
Private sIdiConsulta As String
Private sIdiEdicion As String
Private sIdiRPT As String
Private sIdiDenominacion As String

Private intMax As Integer

Private Function actualizarYSalir() As Boolean
    ' Evita el bug de la grid de Infragistics
    ' moviendonos a una fila adyacente y
    ' regresando luego a la actual en caso de
    ' que no haya error. (jf)
    If sdbgListados.DataChanged = True Then
        bValError = False
        bAnyaError = False
        bModError = False
        If sdbgListados.Row = 0 Then
            sdbgListados.MoveNext
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgListados.MovePrevious
            End If
        Else
            sdbgListados.MovePrevious
            DoEvents
            If bValError Or bAnyaError Or bModError Then
                actualizarYSalir = True
                Exit Function
            Else
                sdbgListados.MoveNext
            End If
        End If
    Else
        actualizarYSalir = False
    End If
End Function

Private Sub cmdA�adir_Click()
    ''' * Objetivo: Situarnos en la fila de adicion
    
    
    sdbgListados.Scroll 0, sdbgListados.Rows - sdbgListados.Row
    
    If sdbgListados.VisibleRows > 0 Then
        
        If sdbgListados.VisibleRows > sdbgListados.Rows Then
            sdbgListados.Row = sdbgListados.Rows
        Else
            sdbgListados.Row = sdbgListados.Rows - (sdbgListados.Rows - sdbgListados.VisibleRows) - 1
        End If
        
    End If
    
    bAnyadir = True
    If Me.Visible Then sdbgListados.SetFocus
End Sub

Private Sub cmdDeshacer_Click()
    ''' * Objetivo: Deshacer la edicion en el pais actual
    
    sdbgListados.CancelUpdate
    sdbgListados.DataChanged = False

    cmdA�adir.Enabled = True
    cmdEliminar.Enabled = True
    cmdDeshacer.Enabled = False

    bAnyadir = False
End Sub

Private Sub cmdEliminar_Click()
    ''' * Objetivo: Eliminar el listado actual
    
    If sdbgListados.Rows = 0 Then Exit Sub
    
    sdbgListados.SelBookmarks.Add sdbgListados.Bookmark
    sdbgListados.DeleteSelected
    sdbgListados.SelBookmarks.RemoveAll
End Sub

Private Sub cmdlistado_Click()
    Dim ofrmLstPARAsis As frmLstLISPER
    
    Set ofrmLstPARAsis = New frmLstLISPER
    ofrmLstPARAsis.WindowState = vbNormal
    ofrmLstPARAsis.Show 1
End Sub

Private Sub cmdModoEdicion_Click()
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
        
        sdbgListados.AllowAddNew = True
        sdbgListados.AllowUpdate = True
        sdbgListados.AllowDelete = True
        
        Me.caption = sIdiTitulos(1)
        
        cmdModoEdicion.caption = sIdiConsulta
        
        cmdRestaurar.Visible = False
        cmdListado.Visible = False
        cmdA�adir.Visible = True
        cmdEliminar.Visible = True
        cmdDeshacer.Visible = True
            
        bModoEdicion = True
        
    Else
                
        If sdbgListados.DataChanged = True Then
        
            v = sdbgListados.ActiveCell.Value
            If Me.Visible Then sdbgListados.SetFocus
            If (v <> "") Then
                sdbgListados.ActiveCell.Value = v
            End If
                        
            If (actualizarYSalir() = True) Then
                Exit Sub
            End If
        End If
        
        sdbgListados.AllowAddNew = False
        sdbgListados.AllowUpdate = False
        sdbgListados.AllowDelete = False
                
        cmdA�adir.Visible = False
        cmdEliminar.Visible = False
        cmdDeshacer.Visible = False
        cmdRestaurar.Visible = True
        cmdListado.Visible = True
        
        Me.caption = sIdiTitulos(2)
        
        cmdModoEdicion.caption = sIdiEdicion
        
        cmdRestaurar_Click
        
        bModoEdicion = False
        
    End If
    sdbgListados.SetFocus
    
End Sub

Private Sub cmdRestaurar_Click()
    'Restaura los datos
    CargarListados
End Sub

Private Sub Form_Load()
    Me.Width = 9000
    Me.Height = 4845
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sdbgListados.Columns("FILTRO").DropDownHwnd = sdbddFiltros.hWnd
    
    If gParametrosGenerales.gbAccesoFSGS = TipoAccesoFSGS.SinAcceso Then
        sdbgListados.Columns(4).Visible = False
    End If
    If gParametrosGenerales.gbPedidosAprov = False Then
        sdbgListados.Columns(5).Visible = False
    End If
    If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.SinAcceso Then
        sdbgListados.Columns(6).Visible = False
    End If
    If gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso Then
        sdbgListados.Columns(7).Visible = False
    End If
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    bModoEdicion = False
    
    'Carga todos los listados personalizados
    CargarListados
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Arrange()
Dim iTamanoNombre As Integer
Dim iTamanoDescripcion As Integer
Dim iTamanoFiltro As Integer
Dim iContColumn As Integer

iTamanoNombre = 25
iTamanoDescripcion = 30
iTamanoFiltro = 20
iContColumn = 3

    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 4630 Then   'de tama�o de la ventana
            Me.Width = 4830        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 1805 Then  'cuando no se maximiza ni
            Me.Height = 1905       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
 

    If Height >= 900 Then sdbgListados.Height = Height - 1000
    If Width >= 250 Then sdbgListados.Width = Width - 200
    
    If gParametrosGenerales.gbAccesoFSGS <> TipoAccesoFSGS.SinAcceso Then
        iContColumn = iContColumn + 1
        sdbgListados.Columns(iContColumn).Width = sdbgListados.Width * 5 / 100
    Else
        iTamanoNombre = iTamanoNombre + 2
        iTamanoDescripcion = iTamanoDescripcion + 3
    End If
    If gParametrosGenerales.gbPedidosAprov = True Then
        iContColumn = iContColumn + 1
        sdbgListados.Columns(iContColumn).Width = sdbgListados.Width * 5 / 100
    Else
        iTamanoNombre = iTamanoNombre + 2
        iTamanoDescripcion = iTamanoDescripcion + 3
    End If
    If gParametrosGenerales.gsAccesoFSWS <> TipoAccesoFSWS.SinAcceso Then
        iContColumn = iContColumn + 1
        sdbgListados.Columns(iContColumn).Width = sdbgListados.Width * 5 / 100
    Else
        iTamanoNombre = iTamanoNombre + 2
        iTamanoDescripcion = iTamanoDescripcion + 3
    End If
    If gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
        iContColumn = iContColumn + 1
        sdbgListados.Columns(iContColumn).Width = sdbgListados.Width * 5 / 100
    Else
        iTamanoNombre = iTamanoNombre + 2
        iTamanoDescripcion = iTamanoDescripcion + 3
    End If
    
    sdbgListados.Columns(1).Width = sdbgListados.Width * iTamanoNombre / 100
    sdbgListados.Columns(2).Width = sdbgListados.Width * iTamanoDescripcion / 100
    sdbgListados.Columns(3).Width = sdbgListados.Width * iTamanoFiltro / 100
    
        
    cmdModoEdicion.Left = sdbgListados.Left + sdbgListados.Width - cmdModoEdicion.Width
    
End Sub


Private Sub CargarRecursos()

Dim Ador As Ador.Recordset
Dim sIdi As String

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LISPER_MANT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        sIdiTitulos(1) = Ador(0).Value & " "
        Ador.MoveNext
        sIdiTitulos(2) = Ador(0).Value & " "
        Me.caption = Ador(0).Value
        Ador.MoveNext
        
        'Columnas del grid
        sdbgListados.Columns(1).caption = Ador(0).Value
        sIdiRPT = Ador(0).Value
        Ador.MoveNext
        sdbgListados.Columns(2).caption = Ador(0).Value
        sIdiDenominacion = Ador(0).Value
        Ador.MoveNext
        
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        
        cmdEliminar.caption = Ador(0).Value
        Ador.MoveNext
        
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiEdicion = Ador(0).Value
        cmdModoEdicion.caption = Ador(0).Value
        Ador.MoveNext
        
        sIdiConsulta = Ador(0).Value
        Ador.MoveNext
        
        sdbgListados.Columns(3).caption = Ador(0).Value
        
        Ador.Close
    
    End If

   Set Ador = Nothing

End Sub

Private Sub CargarListados()
    'Carga todos los listados personalizados
    Dim Ador As Ador.Recordset
    Dim sListaProductos As String
    Dim sListaFiltros As String
    sdbgListados.RemoveAll
    
    Screen.MousePointer = vbHourglass
    Set Ador = oGestorListadosPers.DevolverListadosPersonalizados
    
    sdbddFiltros.RemoveAll
    
        
    sListaFiltros = "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
    sdbddFiltros.AddItem sListaFiltros

    If Not Ador Is Nothing Then
        Ador.MoveFirst
        
        While Not Ador.EOF
             sListaProductos = Ador("COD") & Chr(m_lSeparador) & Ador("RPT") & Chr(m_lSeparador) & Ador("DEN")
             
            If Not IsNull(Ador("FILTRO")) Then
            
                sListaProductos = sListaProductos & Chr(m_lSeparador) & Ador("FILTRODEN")
                sListaFiltros = Ador("FILTRODEN") & Chr(m_lSeparador) & Ador("FILTRO") & Chr(m_lSeparador) & Ador("FILTROGS") & Chr(m_lSeparador) & Ador("FILTROEP") & Chr(m_lSeparador) & Ador("FILTROPM") & Chr(m_lSeparador) & Ador("FILTROQA")
                
            Else
                sListaProductos = sListaProductos & Chr(m_lSeparador) & ""
                sListaFiltros = "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                
            End If
             
             
            sListaProductos = sListaProductos & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("GS"))
            sListaProductos = sListaProductos & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("EP"))
            sListaProductos = sListaProductos & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("PM"))
            sListaProductos = sListaProductos & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("QA"))
            
            If Not IsNull(Ador("FILTRO")) Then
            
                sListaProductos = sListaProductos & Chr(m_lSeparador) & Ador("FILTRO")
                
            Else
                
                sListaProductos = sListaProductos & Chr(m_lSeparador) & ""
                
            End If
            
            sdbgListados.AddItem sListaProductos
            sdbddFiltros.AddItem sListaFiltros
            
            intMax = Ador("COD")
                        
            Ador.MoveNext
        Wend
        
        Ador.Close
        Set Ador = Nothing
    End If

    Screen.MousePointer = vbDefault
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If (actualizarYSalir() = True) Then
        Cancel = True
        Exit Sub
    End If
    Me.Visible = False
End Sub

Private Sub sdbddFiltros_CloseUp()

    Dim bActualizar As Boolean
    bActualizar = True
    
    If sdbddFiltros.Columns("DEN").Value = "" Then
        bActualizar = True
    Else
    
    
        If sdbddFiltros.Columns("GS").Value = "" And (sdbgListados.Columns("GS").Value = "True" Or sdbgListados.Columns("GS").Value = "-1") Then
                bActualizar = False
        End If
    
        If sdbddFiltros.Columns("EP").Value = "" And (sdbgListados.Columns("EP").Value = "True" Or sdbgListados.Columns("EP").Value = "-1") Then
                bActualizar = False
        End If
    
        If sdbddFiltros.Columns("PM").Value = "" And (sdbgListados.Columns("PM").Value = "True" Or sdbgListados.Columns("PM").Value = "-1") Then
                bActualizar = False
        End If
    
        If sdbddFiltros.Columns("QA").Value = "" And (sdbgListados.Columns("QA").Value = "True" Or sdbgListados.Columns("QA").Value = "-1") Then
                bActualizar = False
        End If
        
    End If
    
    If bActualizar Then
        sdbgListados.Columns("FILTROID").Value = sdbddFiltros.Columns("ID").Value
    Else
    
        'MsgBox "Error q me falta por poner"
        oMensajes.ListadoPerFiltros sdbddFiltros.Columns("GS").Value, sdbddFiltros.Columns("EP").Value, sdbddFiltros.Columns("PM").Value, sdbddFiltros.Columns("QA").Value
        sdbgListados.CancelUpdate
        
    End If
    
End Sub

Private Sub sdbddFiltros_DropDown()

    Dim ADORs As Ador.Recordset
           
    Screen.MousePointer = vbHourglass
        
    sdbddFiltros.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    Set ADORs = oGestorListadosPers.DevolverFiltros
                    
    sdbddFiltros.AddItem "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & ""
                
    If ADORs Is Nothing Then
        Screen.MousePointer = vbNormal
        sdbddFiltros.AddItem ""
        sdbgListados.ActiveCell.SelStart = 0
        sdbgListados.ActiveCell.SelLength = Len(sdbgListados.ActiveCell.Text)
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbddFiltros.AddItem ADORs("DEN").Value & Chr(m_lSeparador) & ADORs("ID").Value & Chr(m_lSeparador) & ADORs("GS").Value & Chr(m_lSeparador) & ADORs("EP").Value & Chr(m_lSeparador) & ADORs("PM").Value & Chr(m_lSeparador) & ADORs("QA").Value
        ADORs.MoveNext
    Wend
    
    If sdbddFiltros.Rows = 0 Then
        sdbddFiltros.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing
    
    sdbgListados.ActiveCell.SelStart = 0
    sdbgListados.ActiveCell.SelLength = Len(sdbgListados.ActiveCell.Text)
        
    Screen.MousePointer = vbNormal



End Sub


Private Sub sdbddFiltros_InitColumnProps()

    sdbddFiltros.DataFieldList = "Column 1"
    sdbddFiltros.DataFieldToDisplay = "Column 0"
    

End Sub

Private Sub sdbddFiltros_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddFiltros.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddFiltros.Rows - 1
            bm = sdbddFiltros.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddFiltros.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddFiltros.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbgListados_AfterDelete(RtnDispErrMsg As Integer)
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    
    If Me.Visible Then sdbgListados.SetFocus
    sdbgListados.Bookmark = sdbgListados.RowBookmark(sdbgListados.Row)

End Sub

Private Sub sdbgListados_AfterInsert(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bAnyaError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If

    bAnyadir = False
End Sub

Private Sub sdbgListados_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bAnyaError = False And bModError = False Then
        cmdA�adir.Enabled = True
        cmdEliminar.Enabled = True
        cmdDeshacer.Enabled = False
    End If

End Sub

Private Sub sdbgListados_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
        
    Dim irespuesta As Integer
    Dim teserror As TipoErrorSummit
    
    DispPromptMsg = 0
    
    If sdbgListados.IsAddRow Then
        Cancel = True
        Exit Sub
    End If
    
    irespuesta = oMensajes.PreguntaEliminarListPers(sdbgListados.Columns(1).Value)
    
    If irespuesta = vbNo Then
        Cancel = True
    
    Else
        ''' Eliminamos de la base de datos
    
        teserror = oGestorListadosPers.EliminarListado(sdbgListados.Columns(0).Value)
            
        If teserror.NumError <> TESnoerror Then
            TratarError teserror
            sdbgListados.Refresh
            Cancel = True
            If Me.Visible Then sdbgListados.SetFocus
        End If
        
    End If

End Sub

Private Sub sdbgListados_BeforeInsert(Cancel As Integer)
    
    If Not bModoEdicion Then Exit Sub
        
    bAnyadir = True
    bAnyaError = False
End Sub

''' <summary>
''' Evento producido al hacer cambios sobre el grid de listados
''' </summary>
''' <param name="Cancel">Cancelacion de la actualizacion </param>
''' <returns></returns>
''' <remarks>Llamada desde: Al hacer click  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub sdbgListados_BeforeUpdate(Cancel As Integer)
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    Dim i As Long
    Dim Ador As Ador.Recordset
    
    ''' * Objetivo: Validar los datos y si todo es correcto guardar los cambios en BD
    
    bValError = False
    Cancel = False
    
    'Comprueba que se rellene el fichero
    If Trim(sdbgListados.Columns(1).Value) = "" Then
        oMensajes.NoValido sIdiRPT
        Cancel = True
        GoTo Salir
    End If
    
    'Comprueba que el fichero introducido tenga extensi�n .rpt o .xls
    If Len(sdbgListados.Columns(1).Value) < 4 Then
        oMensajes.ArchivoRpt
        Cancel = True
        GoTo Salir
    ElseIf Mid(LCase(sdbgListados.Columns(1).Value), Len(sdbgListados.Columns(1).Value) - 3) <> ".rpt" Then 'Que tenga extensi�n .rpt
        If Mid(LCase(sdbgListados.Columns(1).Value), Len(sdbgListados.Columns(1).Value) - 3) = ".xls" Then 'Que tenga extensi�n .xls
        Else
            oMensajes.ArchivoRpt
            Cancel = True
            GoTo Salir
        End If
    End If
    
    'Comprueba que se rellene la denominaci�n del informe
    If Trim(sdbgListados.Columns(2).Value) = "" Then
        oMensajes.NoValida sIdiDenominacion
        Cancel = True
        GoTo Salir
    End If
    
    'Comprueba que no se introduzca un .rpt ya existente
    Set Ador = oGestorListadosPers.DevolverListadosPersonalizados
    If Not Ador Is Nothing Then
        Ador.MoveFirst
        While Not Ador.EOF
            If Ador("RPT") = sdbgListados.Columns(1).Value And _
            CStr(Ador("COD")) <> sdbgListados.Columns(0).Value Then
                oMensajes.NoValido sIdiRPT
                Cancel = True
                Ador.Close
                Set Ador = Nothing
                GoTo Salir
            End If
            'Comprueba que no se introduzca una denominaci�n ya existente
            If Ador("DEN") = sdbgListados.Columns(2).Value And _
            CStr(Ador("COD")) <> sdbgListados.Columns(0).Value Then
                oMensajes.NoValido sIdiDenominacion
                Cancel = True
                Ador.Close
                Set Ador = Nothing
                GoTo Salir
            End If
            Ador.MoveNext
        Wend
        Ador.Close
        Set Ador = Nothing
    End If
    
    
    If bAnyadir And sdbgListados.IsAddRow Then   'Inserci�n
        bAnyaError = False
       
        ''' Anyadir a la base de datos
        'teserror = oGestorListadosPers.A�adirListado(sdbgListados.Columns(1).Value, sdbgListados.Columns(2).Value, IIf(sdbgListados.Columns(4).Value = "", False, sdbgListados.Columns(4).Value), IIf(sdbgListados.Columns(5).Value = "", False, sdbgListados.Columns(5).Value), IIf(sdbgListados.Columns(6).Value = "", False, sdbgListados.Columns(6).Value), IIf(sdbgListados.Columns(7).Value = "", False, sdbgListados.Columns(7).Value), IIf(sdbgListados.Columns(3).Value = "", -1, sdbddFiltros.Columns("ID").Value))
        teserror = oGestorListadosPers.A�adirListado(sdbgListados.Columns(1).Value, sdbgListados.Columns(2).Value, IIf(sdbgListados.Columns(4).Value = "", False, sdbgListados.Columns(4).Value), IIf(sdbgListados.Columns(5).Value = "", False, sdbgListados.Columns(5).Value), IIf(sdbgListados.Columns(6).Value = "", False, sdbgListados.Columns(6).Value), IIf(sdbgListados.Columns(7).Value = "", False, sdbgListados.Columns(7).Value), IIf(sdbgListados.Columns("FILTROID").Value = "", -1, sdbgListados.Columns("FILTROID").Value))
        
        If teserror.NumError <> TESnoerror Then
            
            v = sdbgListados.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgListados.SetFocus
            bAnyaError = True
            sdbgListados.ActiveCell.Value = v
            Exit Sub
        Else
            bAnyadir = False
            intMax = intMax + 1
            sdbgListados.Columns(0).Value = intMax
        End If
        
        
    Else  'Modificaci�n
        bModError = False
            
        'teserror = oGestorListadosPers.ModificarListado(sdbgListados.Columns(0).Value, sdbgListados.Columns(1).Value, sdbgListados.Columns(2).Value, IIf(sdbgListados.Columns(4).Value = "", False, sdbgListados.Columns(4).Value), IIf(sdbgListados.Columns(5).Value = "", False, sdbgListados.Columns(5).Value), IIf(sdbgListados.Columns(6).Value = "", False, sdbgListados.Columns(6).Value), IIf(sdbgListados.Columns(7).Value = "", False, sdbgListados.Columns(7).Value), IIf(sdbgListados.Columns(3).Value = "", -1, sdbddFiltros.Columns("ID").Value))
        teserror = oGestorListadosPers.ModificarListado(sdbgListados.Columns(0).Value, sdbgListados.Columns(1).Value, sdbgListados.Columns(2).Value, IIf(sdbgListados.Columns(4).Value = "", False, sdbgListados.Columns(4).Value), IIf(sdbgListados.Columns(5).Value = "", False, sdbgListados.Columns(5).Value), IIf(sdbgListados.Columns(6).Value = "", False, sdbgListados.Columns(6).Value), IIf(sdbgListados.Columns(7).Value = "", False, sdbgListados.Columns(7).Value), IIf(sdbgListados.Columns("FILTROID").Value = "", -1, sdbgListados.Columns("FILTROID").Value))
        If teserror.NumError <> TESnoerror Then
            v = sdbgListados.ActiveCell.Value
            TratarError teserror
            If Me.Visible Then sdbgListados.SetFocus
            bModError = True
            sdbgListados.ActiveCell.Value = v
            Exit Sub
        End If
    End If
    
    
Salir:
        
    bValError = Cancel
    If Me.Visible Then sdbgListados.SetFocus

End Sub

Private Sub sdbgListados_Change()
    
    If cmdDeshacer.Enabled = False Then
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        cmdDeshacer.Enabled = True
    End If
    
    Dim i As Long
    Dim vbm As Variant
    
    
    
If sdbgListados.Columns("FILTRO").Value <> "" Then
    
    Select Case sdbgListados.Col
                                    
            Case 4 'GS
            
                If sdbgListados.Columns("GS").Value = -1 Or sdbgListados.Columns("GS").Value = 1 Then
                
                For i = 0 To sdbddFiltros.Rows - 1
                    vbm = sdbddFiltros.AddItemBookmark(i)
                    If sdbddFiltros.Columns("ID").CellValue(vbm) = sdbgListados.Columns("FILTROID").Value Then
                      
                         If sdbddFiltros.Columns("GS").CellValue(vbm) <> "" Then
    
                         Else
                            'MsgBox "Este check no se puede seleccionar"
                            oMensajes.ListadoPerFiltros sdbddFiltros.Columns("GS").CellValue(vbm), sdbddFiltros.Columns("EP").CellValue(vbm), sdbddFiltros.Columns("PM").CellValue(vbm), sdbddFiltros.Columns("QA").CellValue(vbm)
                            sdbgListados.Columns("GS").Value = 0
                            'sdbgListados.CancelUpdate
                            'Cancel = True
                            'sdbgListados.DataChanged = False
                         End If
                            
                        Exit For
                    End If
                Next
                
                End If
                    
                                   
            Case 5 'EP
            
            
                If sdbgListados.Columns("EP").Value = -1 Or sdbgListados.Columns("EP").Value = 1 Then
                
                For i = 0 To sdbddFiltros.Rows - 1
                    vbm = sdbddFiltros.AddItemBookmark(i)
                    If sdbddFiltros.Columns("ID").CellValue(vbm) = sdbgListados.Columns("FILTROID").Value Then
                      
                         If sdbddFiltros.Columns("EP").CellValue(vbm) <> "" Then
                         
                         Else
                            'MsgBox "Este check no se puede seleccionar"
                            oMensajes.ListadoPerFiltros sdbddFiltros.Columns("GS").CellValue(vbm), sdbddFiltros.Columns("EP").CellValue(vbm), sdbddFiltros.Columns("PM").CellValue(vbm), sdbddFiltros.Columns("QA").CellValue(vbm)
                            'Cancel = True
                            sdbgListados.Columns("EP").Value = 0
                            'sdbgListados.CancelUpdate
                            'sdbgListados.DataChanged = False
                         End If
                            
                        Exit For
                    End If
                Next
                    
                End If
                   
            
            Case 6 'PM
            
                If sdbgListados.Columns("PM").Value = -1 Or sdbgListados.Columns("PM").Value = 1 Then
                
                For i = 0 To sdbddFiltros.Rows - 1
                    vbm = sdbddFiltros.AddItemBookmark(i)
                    If sdbddFiltros.Columns("ID").CellValue(vbm) = sdbgListados.Columns("FILTROID").Value Then
                      
                         If sdbddFiltros.Columns("PM").CellValue(vbm) <> "" Then
                         
                         Else
                            'MsgBox "Este check no se puede seleccionar"
                            oMensajes.ListadoPerFiltros sdbddFiltros.Columns("GS").CellValue(vbm), sdbddFiltros.Columns("EP").CellValue(vbm), sdbddFiltros.Columns("PM").CellValue(vbm), sdbddFiltros.Columns("QA").CellValue(vbm)
                            sdbgListados.Columns("PM").Value = 0
                            'sdbgListados.CancelUpdate
                            'sdbgListados.DataChanged = False
                            
                         End If
                            
                        Exit For
                    End If
                Next
                End If
            
            Case 7 'QA
            
                If sdbgListados.Columns("QA").Value = -1 Or sdbgListados.Columns("QA").Value = 1 Then
                
                For i = 0 To sdbddFiltros.Rows - 1
                    vbm = sdbddFiltros.AddItemBookmark(i)
                    If sdbddFiltros.Columns("ID").CellValue(vbm) = sdbgListados.Columns("FILTROID").Value Then
                      
                         If sdbddFiltros.Columns("QA").CellValue(vbm) <> "" Then
                         
                         Else
                            'MsgBox "Este check no se puede seleccionar"
                            oMensajes.ListadoPerFiltros sdbddFiltros.Columns("GS").CellValue(vbm), sdbddFiltros.Columns("EP").CellValue(vbm), sdbddFiltros.Columns("PM").CellValue(vbm), sdbddFiltros.Columns("QA").CellValue(vbm)
                            sdbgListados.Columns("QA").Value = 0
                            'sdbgListados.CancelUpdate
                            'sdbgListados.DataChanged = False
                            
                         End If
                            
                        Exit For
                    End If
                Next
                End If
            
            
            
            
    End Select

End If
    

End Sub


Private Sub sdbgListados_Click()

Select Case sdbgListados.Col

       Case 3 'Filtro
                sdbddFiltros.DroppedDown = True
                                                                    
End Select

End Sub

Private Sub sdbgListados_HeadClick(ByVal ColIndex As Integer)
    'Carga todos los listados personalizados ordenados seg�n la columna
    Dim Ador As Ador.Recordset
    
    sdbgListados.RemoveAll
    
    Screen.MousePointer = vbHourglass
            
    Set Ador = oGestorListadosPers.DevolverListadosPersonalizados(, , ColIndex)
    
    If Not Ador Is Nothing Then
        Ador.MoveFirst
                            
        While Not Ador.EOF
            sdbgListados.AddItem Ador("COD") & Chr(m_lSeparador) & Ador("RPT") & Chr(m_lSeparador) & Ador("DEN") & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("GS")) & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("EP")) & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("PM")) & Chr(m_lSeparador) & SQLBinaryToBoolean(Ador("QA"))
            Ador.MoveNext
        Wend
        
        Ador.Close
        Set Ador = Nothing
    End If

    Screen.MousePointer = vbDefault
End Sub

Private Sub sdbgListados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
        ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If Not sdbgListados.IsAddRow Then
        If sdbgListados.DataChanged = False Then
            cmdA�adir.Enabled = True
            cmdEliminar.Enabled = True
            cmdDeshacer.Enabled = False
        End If
    Else
        If sdbgListados.DataChanged = True Then cmdDeshacer.Enabled = True
        If Not IsNull(LastRow) Then
            If val(LastRow) <> val(sdbgListados.Row) Then
                sdbgListados.Col = 1
                cmdDeshacer.Enabled = False
            End If
        End If
        cmdA�adir.Enabled = False
        cmdEliminar.Enabled = False
        
    End If

End Sub












