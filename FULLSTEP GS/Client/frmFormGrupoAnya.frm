VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFormGrupoAnya 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DAnyadir grupo"
   ClientHeight    =   1230
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6585
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFormGrupoAnya.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1230
   ScaleWidth      =   6585
   StartUpPosition =   1  'CenterOwner
   Begin SSDataWidgets_B.SSDBGrid sdbgGrupo 
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   6355
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   3
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   -2147483630
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   2514
      Columns(0).Caption=   "IDI"
      Columns(0).Name =   "IDI"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   14671839
      Columns(1).Width=   8123
      Columns(1).Caption=   "DEN"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "COD_IDI"
      Columns(2).Name =   "COD_IDI"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   11210
      _ExtentY        =   1085
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   1980
      TabIndex        =   2
      Top             =   840
      Width           =   975
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3180
      TabIndex        =   3
      Top             =   840
      Width           =   975
   End
   Begin VB.TextBox txtDen 
      Height          =   285
      Left            =   1400
      TabIndex        =   0
      Top             =   200
      Width           =   5070
   End
   Begin VB.Label lblDen 
      BackColor       =   &H00808000&
      Caption         =   "DDenominaci�n:"
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   4
      Top             =   280
      Width           =   1200
   End
End
Attribute VB_Name = "frmFormGrupoAnya"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_sIdiForm As String
Private m_sIdiDen As String
Private m_sIdiCaption As String
Private m_sIdiCaptionModif As String

Public g_oIdiomas As CIdiomas

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FORM_GR_ANYA, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        m_sIdiForm = Ador(0).Value   '1 Formulario
        Ador.MoveNext
        lblDen.caption = Ador(0).Value & ":"   '2 Denominaci�n
        m_sIdiDen = Ador(0).Value
        sdbgGrupo.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value  '3 &Aceptar
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value '4 &Cancelar
        Ador.MoveNext
        m_sIdiCaption = Ador(0).Value ' 5 A�adir grupo
        Ador.MoveNext
        sdbgGrupo.Columns("IDI").caption = Ador(0).Value  '6 Idioma
        Ador.MoveNext
        m_sIdiCaptionModif = Ador(0).Value ' 5 Modificar grupo
        
        Ador.Close
    End If
    
    Set Ador = Nothing

End Sub

Private Sub cmdAceptar_Click()
    Dim udtTeserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim oIdioma As CIdioma
    Dim oMultiidi As CMultiidiomas
    Dim oGrupo As CFormGrupo
    Dim i As Integer
    Dim vbm As Variant
    
    If txtDen.Visible = True Then
        If Trim(txtDen.Text) = "" Then
            oMensajes.NoValido m_sIdiDen
            If Me.Visible Then txtDen.SetFocus
            Exit Sub
        End If
    Else
        'Tiene que introducir la denominaci�n en todos los idiomas:
        sdbgGrupo.MoveFirst
        For i = 0 To sdbgGrupo.Rows - 1
            vbm = sdbgGrupo.AddItemBookmark(i)
            If sdbgGrupo.Columns("COD_IDI").CellValue(vbm) = gParametrosInstalacion.gIdioma Then
                If sdbgGrupo.Columns("DEN").CellValue(vbm) = "" Then
                    oMensajes.NoValido sdbgGrupo.Columns("IDI").CellValue(vbm)
                    Exit Sub
                End If
                Exit For
            End If
        Next i
    End If
    
    Screen.MousePointer = vbHourglass
    
    'Carga las denominaciones del grupo de datos generales en todos los idiomas
    Set oMultiidi = oFSGSRaiz.Generar_CMultiidiomas
    For Each oIdioma In g_oIdiomas
        If frmFormularios.g_oFormSeleccionado.Multiidioma = True Then
            For i = 0 To sdbgGrupo.Rows - 1
                vbm = sdbgGrupo.AddItemBookmark(i)
                If sdbgGrupo.Columns("COD_IDI").CellValue(vbm) = oIdioma.Cod Then
                    oMultiidi.Add oIdioma.Cod, Trim(sdbgGrupo.Columns("DEN").CellValue(vbm))
                    Exit For
                End If
            Next i
        Else
            oMultiidi.Add oIdioma.Cod, Trim(txtDen.Text)
        End If
    Next
            
    Select Case frmFormularios.g_Accion
     
        Case ACCFormGrupoModif
            'Modifica las denominaciones de los grupos:
            Set frmFormularios.g_oGrupoSeleccionado.Denominaciones = oMultiidi

            Set oIBaseDatos = frmFormularios.g_oGrupoSeleccionado
            udtTeserror = oIBaseDatos.FinalizarEdicionModificando
            If udtTeserror.NumError = TESnoerror Then
                'Cambia la denominaci�n en el tab de formularios:
                frmFormularios.AnyaModifGrupo

                RegistrarAccion ACCFormGrupoModif, "Id:" & frmFormularios.g_oGrupoSeleccionado.ID
                Unload Me
            Else
                Screen.MousePointer = vbNormal
                TratarError udtTeserror
                Exit Sub
            End If
        
        Case ACCFormGrupoAnyadir
            'A�ade el grupo:
            Set oGrupo = oFSGSRaiz.Generar_CFormGrupo
            Set oGrupo.Formulario = frmFormularios.g_oFormSeleccionado
            Set oGrupo.Denominaciones = oMultiidi
            
            Set oIBaseDatos = oGrupo
            udtTeserror = oIBaseDatos.AnyadirABaseDatos
                
            If udtTeserror.NumError = TESnoerror Then
                frmFormularios.g_oFormSeleccionado.Grupos.Add oGrupo.ID, oGrupo.Denominaciones, oGrupo.FECACT, frmFormularios.g_oFormSeleccionado, , , oGrupo.Orden
                frmFormularios.AnyaModifGrupo oGrupo.ID
                
                RegistrarAccion ACCFormGrupoAnyadir, "Id:" & oGrupo.ID
            Else
                Screen.MousePointer = vbNormal
                TratarError udtTeserror
                Exit Sub
            End If
            Set oGrupo = Nothing
            
    End Select
    
    Set oIBaseDatos = Nothing
    Set oMultiidi = Nothing
    
    Screen.MousePointer = vbNormal
    
    frmFormularios.g_Accion = ACCFormularioCons
    
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()

    CargarRecursos
    
    PonerFieldSeparator Me
    
    If frmFormularios.g_oFormSeleccionado.Multiidioma = True Then
        'Es multiidioma,muestra la grid:
        sdbgGrupo.Visible = True
        lblDen.Visible = False
        txtDen.Visible = False
        CargarGridIdiomas
        
        'Ajusta la pantalla para que no haya que hacer scroll
        sdbgGrupo.Height = (sdbgGrupo.Rows + 1.2) * sdbgGrupo.RowHeight
        cmdAceptar.Top = sdbgGrupo.Top + sdbgGrupo.Height + 85
        cmdCancelar.Top = cmdAceptar.Top
        Me.Height = cmdAceptar.Top + cmdAceptar.Height + 550
        
    Else
        'Se muestra solo el textbox
        sdbgGrupo.Visible = False
        lblDen.Visible = True
        txtDen.Visible = True
        
        If frmFormularios.g_Accion = ACCFormGrupoModif Then
            txtDen.Text = NullToStr(frmFormularios.g_oGrupoSeleccionado.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        End If
    End If
    
    If frmFormularios.g_Accion = ACCFormGrupoModif Then
        Me.caption = m_sIdiForm & " " & frmFormularios.g_oFormSeleccionado.Den & " " & m_sIdiCaptionModif
    Else
        Me.caption = m_sIdiForm & " " & frmFormularios.g_oFormSeleccionado.Den & " " & m_sIdiCaption
    End If
End Sub

Private Sub CargarGridIdiomas()
Dim oIdioma As CIdioma

    'Carga las denominaciones del grupo de datos generales en todos los idiomas:
    For Each oIdioma In g_oIdiomas
        If frmFormularios.g_Accion = ACCFormGrupoModif Then
            sdbgGrupo.AddItem oIdioma.Den & Chr(m_lSeparador) & NullToStr(frmFormularios.g_oGrupoSeleccionado.Denominaciones.Item(oIdioma.Cod).Den) & Chr(m_lSeparador) & oIdioma.Cod
        Else
            sdbgGrupo.AddItem oIdioma.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oIdioma.Cod
        End If
    Next
    sdbgGrupo.MoveFirst

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oIdiomas = Nothing
End Sub
