VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTraspasoPedERP 
   Caption         =   "Traspaso de pedido al ERP"
   ClientHeight    =   6960
   ClientLeft      =   43260
   ClientTop       =   450
   ClientWidth     =   13680
   Icon            =   "frmTraspasoPedERP.frx":0000
   LinkTopic       =   "frmTraspasoPedERP"
   LockControls    =   -1  'True
   ScaleHeight     =   6960
   ScaleWidth      =   13680
   StartUpPosition =   3  'Windows Default
   Begin SSDataWidgets_B.SSDBDropDown sdbddValorItem 
      Height          =   795
      Left            =   8760
      TabIndex        =   14
      Top             =   4710
      Width           =   2805
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ORDEN"
      Columns(0).Name =   "ORDEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "VALOR"
      Columns(1).Name =   "VALOR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4948
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValorProce 
      Height          =   795
      Left            =   8580
      TabIndex        =   13
      Top             =   2550
      Width           =   2805
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ORDEN"
      Columns(0).Name =   "ORDEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "VALOR"
      Columns(1).Name =   "VALOR"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   4948
      _ExtentY        =   1402
      _StockProps     =   77
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.PictureBox picProce 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   240
      ScaleHeight     =   375
      ScaleWidth      =   1785
      TabIndex        =   11
      Top             =   1230
      Width           =   1785
      Begin VB.Label lblAtribProce 
         Appearance      =   0  'Flat
         Caption         =   "Atributos de proceso:"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   30
         TabIndex        =   12
         Top             =   60
         Width           =   1695
      End
   End
   Begin VB.PictureBox picItem 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   270
      ScaleHeight     =   405
      ScaleWidth      =   1575
      TabIndex        =   9
      Top             =   3690
      Width           =   1575
      Begin VB.Label lblAtribItem 
         Caption         =   "Atributos de �tem:"
         Height          =   405
         Left            =   0
         TabIndex        =   10
         Top             =   120
         Width           =   1545
      End
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   510
      Left            =   0
      ScaleHeight     =   510
      ScaleWidth      =   13680
      TabIndex        =   3
      Top             =   6444
      Width           =   13680
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   375
         Left            =   5460
         TabIndex        =   5
         Top             =   60
         Width           =   1275
      End
      Begin VB.CommandButton cmdSiguiente 
         Caption         =   "Siguiente"
         Height          =   375
         Left            =   4050
         TabIndex        =   4
         Top             =   60
         Width           =   1275
      End
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAtribProce 
      Height          =   2010
      Left            =   300
      TabIndex        =   7
      Top             =   1680
      Width           =   11805
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   5
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTraspasoPedERP.frx":014A
      stylesets(1).Name=   "NormalInterno"
      stylesets(1).BackColor=   16777152
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTraspasoPedERP.frx":0166
      stylesets(1).AlignmentPicture=   1
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   2487
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasHeadForeColor=   -1  'True
      Columns(0).HasHeadBackColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).HeadBackColor=   -2147483633
      Columns(0).BackColor=   16777152
      Columns(1).Width=   6535
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   200
      Columns(1).Locked=   -1  'True
      Columns(1).HasHeadForeColor=   -1  'True
      Columns(1).HasHeadBackColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).HeadBackColor=   -2147483633
      Columns(1).BackColor=   16777152
      Columns(2).Width=   6218
      Columns(2).Caption=   "Valor"
      Columns(2).Name =   "VALOR"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   0
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   800
      Columns(2).HasHeadForeColor=   -1  'True
      Columns(2).HasHeadBackColor=   -1  'True
      Columns(2).HeadBackColor=   -2147483633
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "ID_A"
      Columns(3).Name =   "ID"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "INTERNO"
      Columns(4).Name =   "INTERNO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   11
      Columns(4).FieldLen=   256
      _ExtentX        =   20823
      _ExtentY        =   3545
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgAtribItem 
      Height          =   2010
      Left            =   300
      TabIndex        =   8
      Top             =   4110
      Width           =   11805
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   5
      stylesets.count =   2
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTraspasoPedERP.frx":03B3
      stylesets(1).Name=   "NormalInterno"
      stylesets(1).BackColor=   16777152
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTraspasoPedERP.frx":03CF
      stylesets(1).AlignmentPicture=   1
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Caption=   "Art�culo"
      Columns(0).Name =   "ARTICULO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Locked=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777160
      Columns(1).Width=   3200
      Columns(1).Caption=   "Precio U.P."
      Columns(1).Name =   "PRECIOUP"
      Columns(1).Alignment=   1
      Columns(1).CaptionAlignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   11662320
      Columns(2).Width=   3200
      Columns(2).Caption=   "Cantidad"
      Columns(2).Name =   "CANTIDAD"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   11662320
      Columns(3).Width=   3200
      Columns(3).Caption=   "Unidad"
      Columns(3).Name =   "UNIDAD"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   11662320
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "INDICE"
      Columns(4).Name =   "INDICE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   20823
      _ExtentY        =   3545
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TabStrip sstabAtrib 
      Height          =   5535
      Left            =   120
      TabIndex        =   6
      Top             =   810
      Width           =   12195
      _ExtentX        =   21511
      _ExtentY        =   9763
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInfo2 
      Caption         =   "Los atributos marcados con (*) son de obligada cumplimentaci�n"
      Height          =   345
      Left            =   7260
      TabIndex        =   2
      Top             =   210
      Width           =   4875
   End
   Begin VB.Label lblERP 
      Caption         =   "(Nombre del ERP)"
      Height          =   345
      Left            =   5970
      TabIndex        =   1
      Top             =   210
      Width           =   1305
   End
   Begin VB.Label lblInfo1 
      Caption         =   "Los siguientes atributos son necesarios para la integracion del pedido en el ERP"
      Height          =   345
      Left            =   180
      TabIndex        =   0
      Top             =   210
      Width           =   5715
   End
End
Attribute VB_Name = "frmTraspasoPedERP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables publicas
Public g_sDatoERp As String
Public g_oOrdenEntrega As COrdenEntrega
Public g_oLineaSeleccionada As CLineaPedido

Public g_oOrigen As Form

'Variables privadas
Private m_oAtributosProce As CAtributos
Private m_oAtributosItem As CAtributos
Private m_oProves As CProveedores
Private m_oERPsInt As CERPsInt

'variables Textos
Private m_sIdiFalse As String
Private m_sIdiTrue As String

Private m_bEstaComprobado As Boolean
Private m_bValError As Boolean 'vble para no sacar 2 veces el mismo mensaje
'Variables textos
Private m_sLayOut As String
Private m_bCargarLayout As Boolean

Private sIdiMayor As String
Private sIdiMenor As String
Private sIdiEntre As String
Private m_sMoneda As String

Private m_bRReceptorUsu As Boolean

Private Sub cmdCancelar_Click()

 Unload Me

End Sub

'''<summary>Realiza las validaciones previas a la emisi�n: comprobar que los atributos obligatorios est�n rellenados</summary>
'''<remarks>Llamada desde:  FSGSClient.frmTraspasoERP.cmdSiguiente_click Tiempo m�ximo: ? </remarks>
Private Sub cmdSiguiente_Click()

Dim bObligatorios As Boolean

  cmdSiguiente.Enabled = False
  cmdCancelar.Enabled = False
  Screen.MousePointer = vbHourglass
  
  If sdbgAtribProce.DataChanged Then sdbgAtribProce.Update
  If sdbgAtribItem.DataChanged Then sdbgAtribItem.Update

If ComprobarValorAtributosObligatorios() Then

    'Aqui hay que llamar a al funcion nuestra de validacion ERP para los atributos de integracion
    'si la funcion devuelve false saldriamos sin emitir
     If ValidacionERP() Then
        g_oOrigen.ComprobarCostesDescuentos
        'Si la validacion ERP ha ido bien entonces pasamos a emitir el pedido desde aqui
        If gParametrosGenerales.gbPedidoDirectoEnvioEmail = True Then
            g_oOrigen.g_oOrdenesTemporales.CargarNotificacionesPedido
            Set frmPedidosEmision1.g_oOrigen = g_oOrigen
            Screen.MousePointer = vbNormal
            frmPedidosEmision1.Show 1
            'Para que al terminar de emitir se vuelva a la pantalla de emision de pedidos
            cmdSiguiente.Enabled = True
            cmdCancelar.Enabled = True
            Unload Me
        Else
            'Emitir pedidos
            Dim sUsuario As Variant
            Dim teserror As TipoErrorSummit
            
            sUsuario = g_oOrigen.DevolverAprovisionador
            g_oOrigen.g_oOrdenesTemporales.CargarCentrosDistribucion
            teserror = g_oOrigen.g_oOrdenesTemporales.EmitirPedidoConLineasTemporales(sUsuario, g_oOrigen.m_oTipoPedido.Id, g_oOrigen.g_bSolicitudAbono)
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                cmdSiguiente.Enabled = True
                cmdCancelar.Enabled = True
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
                
            'Paso las ordenes al form de resumen
            Set frmPedidosEmision2.oPedido = oFSGSRaiz.Generar_CPedido
            frmPedidosEmision2.oPedido.Anyo = Year(Date)
            frmPedidosEmision2.oPedido.Estado = 1
            frmPedidosEmision2.oPedido.Fecha = g_oOrigen.g_oOrdenesTemporales.Item(1).Fecha
            frmPedidosEmision2.oPedido.Id = g_oOrigen.g_oOrdenesTemporales.Item(1).idPedido
            frmPedidosEmision2.oPedido.Numero = g_oOrigen.g_oOrdenesTemporales.Item(1).NumPedido
            frmPedidosEmision2.oPedido.Persona = sUsuario
            frmPedidosEmision2.oPedido.Tipo = Directo
            Set frmPedidosEmision2.oPedido.OrdenesEntrega = g_oOrigen.g_oOrdenesTemporales
            
            'Limpiamos Datos:
            g_oOrigen.sdbgMiPedido.RemoveAll
            g_oOrigen.sstabPedidos.Tab = 1
            g_oOrigen.sstabPedidos.Tab = 0
            g_oOrigen.sdbgPedidos.RemoveAll
            g_oOrigen.sdbcGMN1_4Cod = ""
            g_oOrigen.sdbcProceCod = ""
            g_oOrigen.sdbcProceDen = ""
            g_oOrigen.LimpiarMiPedido
            Screen.MousePointer = vbNormal
            frmPedidosEmision2.Show 1
                
            g_oOrigen.VaciarOrdenesTemporales
            cmdSiguiente.Enabled = True
            cmdCancelar.Enabled = True
                
            Unload Me
        End If
    Else
        cmdSiguiente.Enabled = True
        cmdCancelar.Enabled = True
        Screen.MousePointer = vbNormal
    End If
Else
    cmdSiguiente.Enabled = True
    cmdCancelar.Enabled = True
    Screen.MousePointer = vbNormal
End If
End Sub

''' <summary>
''' Carga la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()

    Dim sTemp As String
    
    Me.Height = 7220
    Me.Width = 11175
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    ' Para que quede centrada en la MDI
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    ' Cargar los captions dependiendo del idioma
    CargarRecursos
    
    ConfigurarSeguridad
    
    PonerFieldSeparator Me
    
    Set m_oERPsInt = Nothing
    Set m_oERPsInt = oFSGSRaiz.Generar_CERPsInt
    m_oERPsInt.CargarTodosLosERPs
    
    lblERP.caption = "(" & m_oERPsInt.Item(CStr(g_sDatoERp)).Den & ")"
    
     'Tengo que guardar la estructura original de la grid para los atributos
    sTemp = basUtilidades.DevolverPathFichTemp
    m_sLayOut = sTemp & "LayMiAtribItem"
    sdbgAtribItem.SaveLayout m_sLayOut, ssSaveLayoutAll
    
    inicializarTabsProveedor
    
    sdbgAtribProce.AllowUpdate = True
    sdbgAtribItem.AllowUpdate = True

End Sub

''' <summary>
''' Redimensiona los campos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Resize()

    If Me.Height < 1500 Then Exit Sub
    If Me.Width < 1500 Then Exit Sub
    If Me.Height < 10875 Then
     
     Me.Width = 12510
     sstabAtrib.Height = Me.Height - 2000
     sstabAtrib.Width = Me.Width - 350
    
        sdbgAtribProce.Height = (sstabAtrib.Height / 3)
        sdbgAtribProce.Width = sstabAtrib.Width - 300
        
        sdbgAtribItem.Height = (sstabAtrib.Height / 3)
        sdbgAtribItem.Width = sstabAtrib.Width - 300
    Else
     sstabAtrib.Height = Me.Height - 2000
     sstabAtrib.Width = Me.Width - 350
    
        sdbgAtribProce.Height = sstabAtrib.Height / 3 + 800
        sdbgAtribProce.Width = sstabAtrib.Width - 300
        
        sdbgAtribItem.Height = sstabAtrib.Height / 3 + 800 ''2800
        sdbgAtribItem.Width = sstabAtrib.Width - 300
    End If
    
    'Ancho de las columnas del grid
    sdbgAtribProce.Columns("COD").Width = sdbgAtribProce.Width * 0.2
    sdbgAtribProce.Columns("DEN").Width = sdbgAtribProce.Width * 0.3
    sdbgAtribProce.Columns("VALOR").Width = sdbgAtribProce.Width * 0.45
    sdbddValorProce.Columns("VALOR").Width = (sdbgAtribProce.Columns("VALOR").Width - 300)
    'El ancho de las columnas del gri de atribs de item ya veremos a ver..-.
    sdbgAtribItem.Columns("ARTICULO").Width = sdbgAtribItem.Width * 0.2
    
    If Me.Height < 10875 Then
        sdbgAtribProce.Top = sstabAtrib.Top + 800
        sdbgAtribItem.Top = sdbgAtribProce.Top + sdbgAtribProce.Height + 500
    Else
        sdbgAtribProce.Top = sstabAtrib.Top + 800
        sdbgAtribItem.Top = sdbgAtribProce.Top + sdbgAtribProce.Height + 1000
    End If
'    lblAtribItem.Top = sdbgAtribItem.Top - 300
'    lblAtribItem.Left = sdbgAtribItem.Left
    
    picItem.Top = sdbgAtribItem.Top - 400
    picItem.Left = sdbgAtribItem.Left
    
'    lblAtribProce.Top = sdbgAtribProce.Top - 300
'    lblAtribProce.Left = sdbgAtribProce.Left
    
    picProce.Top = sdbgAtribProce.Top - 400
    picProce.Left = sdbgAtribProce.Left
    
    lblInfo1.Top = sstabAtrib.Top - 500
    lblInfo1.Left = sstabAtrib.Left
    lblERP.Top = lblInfo1.Top
    lblERP.Left = lblInfo1.Left + lblInfo1.Width
    lblInfo2.Top = lblInfo1.Top
    lblInfo2.Left = lblInfo1.Left + lblInfo1.Width + lblERP.Width
       

    'lblNotificados.Width = sstabPlantillas.Width - 200
    
    cmdSiguiente.Left = sstabAtrib.Width / 2 - cmdSiguiente.Width - 200
    cmdCancelar.Left = cmdSiguiente.Left + cmdSiguiente.Width + 50
 
End Sub

''' <summary>
''' Cargar las variables de pantalla multiidioma
''' </summary>
''' <remarks>Llamada desde: form_load; Tiempo m�ximo:0 </remarks>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_TRASPASO_PEDERP, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
       lblInfo1.caption = Ador(0).Value
       Ador.MoveNext
       lblInfo2.caption = Ador(0).Value
       Ador.MoveNext
       lblAtribProce.caption = Ador(0).Value
       Ador.MoveNext
       lblAtribItem.caption = Ador(0).Value
       Ador.MoveNext
       cmdSiguiente.caption = Ador(0).Value
       Ador.MoveNext
       cmdCancelar.caption = Ador(0).Value
       Ador.MoveNext
       sdbgAtribProce.Columns(0).caption = Ador(0).Value 'Codigo
       Ador.MoveNext
       sdbgAtribProce.Columns(1).caption = Ador(0).Value 'Denominacion
       Ador.MoveNext
       sdbgAtribProce.Columns(0).caption = Ador(0).Value 'Valor
       Ador.MoveNext
       sdbgAtribItem.Columns(0).caption = Ador(0).Value 'Articulo
       Ador.MoveNext
       sdbgAtribItem.Columns(1).caption = Ador(0).Value 'Precio UP
       Ador.MoveNext
       sdbgAtribItem.Columns(2).caption = Ador(0).Value 'Cantidad
       Ador.MoveNext
       sdbgAtribItem.Columns(3).caption = Ador(0).Value 'Unidad
       Ador.MoveNext
       m_sIdiTrue = Ador(0).Value 'Si
       Ador.MoveNext
       m_sIdiFalse = Ador(0).Value 'No
        Ador.MoveNext
        sIdiMayor = Ador(0).Value
        Ador.MoveNext
        sIdiMenor = Ador(0).Value
        Ador.MoveNext
        sIdiEntre = Ador(0).Value
        Ador.MoveNext
        m_sMoneda = Ador(0).Value
        Ador.MoveNext
        Me.caption = Ador(0).Value
        
       Ador.Close
    End If

    Set Ador = Nothing

End Sub

''' <summary>
''' Carga el grid de atributos de integracion de proceso los atributos marcados para integracion de pedidos (para el ERP :g_sDatoERp )
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde: Load del form  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub CargarGridAtribIntProceso(ByVal iCodErp As Long)
Dim oatrib As CAtributo
Dim oAtribOrden As CAtributo
Dim oLista As CValorPond
Dim sTipo As String
Dim sObl As String
Dim i As Integer
Dim bInsertarAtribInt As Boolean

    sdbgAtribProce.RemoveAll
    
    Set m_oAtributosProce = Nothing
    Set m_oAtributosProce = oFSGSRaiz.Generar_CAtributos
        
    m_oAtributosProce.CargarAtributosIntegracionItemProceso iCodErp, TipoAmbitoProceso.AmbProceso

    Set g_oOrdenEntrega = g_oOrigen.g_oOrdenesTemporales.Item(CStr(sstabAtrib.selectedItem.caption))
    
    If g_oOrdenEntrega.AtributosIntegracion Is Nothing Then
        Set g_oOrdenEntrega.AtributosIntegracion = oFSGSRaiz.Generar_CAtributos
    End If
    
    If m_oAtributosProce.Count = 0 Then Exit Sub
    
    
'   LimpiarGridProceso
    ConfigurarGridProceso
   
    'Explicacion
    For Each oatrib In m_oAtributosProce
        If Not g_oOrdenEntrega.AtributosIntegracion Is Nothing Then
            For i = 1 To g_oOrdenEntrega.ATRIBUTOS.Count
                If oatrib.Atrib = g_oOrdenEntrega.ATRIBUTOS.Item(i).idAtribProce Then
                'Indicamos que este atributo de proceso esta tambien como atributo de integracion
                    g_oOrdenEntrega.ATRIBUTOS.Item(i).AtribIntEnAtributosProceso = True
                    'Revisamos los valores introducidos para los atributos de proceso, si esos atributos de proceso son
                    'tambien de integracion, pondremos esos valores a los atributos de integracion.
                    'Si son de tipo lista se comprobara que el valor del atributo de proceso esta en la lista de valores del atributo de integracion
                    If oatrib.TipoIntroduccion = Introselec Then
                        oatrib.CargarListaDeValoresInt
                        For Each oLista In oatrib.ListaPonderacion
                            Select Case oatrib.Tipo
                                Case 1:
                                    If g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorText = oLista.ValorLista Then
                                        oatrib.ValorText = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorText
                                        Exit For
                                    End If
                                Case 2:
                                    If g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorNum = oLista.ValorLista Then
                                        oatrib.ValorNum = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorNum
                                        Exit For
                                    End If
                                Case 3:
                                    If g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorFec = oLista.ValorLista Then
                                        oatrib.ValorFec = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorFec
                                        Exit For
                                    End If
                                Case 4:
                                    If g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorBool = oLista.ValorLista Then
                                        oatrib.ValorBool = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorBool
                                        Exit For
                                    End If
                            End Select
                        Next
                    Else
                        Select Case oatrib.Tipo
                            Case 1:
                                If Not IsNull(g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorText) Then
                                    oatrib.ValorText = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorText
                                End If
                            Case 2:
                                 If Not IsNull(g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorNum) Then
                                     oatrib.ValorNum = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorNum
                                 End If
                            Case 3:
                                 If Not IsNull(g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorFec) Then
                                     oatrib.ValorFec = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorFec
                                 End If
                            Case 4:
                                If Not IsNull(g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorBool) Then
                                    oatrib.ValorBool = g_oOrdenEntrega.ATRIBUTOS.Item(i).ValorBool
                                End If
                        End Select
                        Exit For
                    End If
                End If
            Next
            
            bInsertarAtribInt = True
            For i = 1 To g_oOrdenEntrega.AtributosIntegracion.Count
                If oatrib.Atrib = g_oOrdenEntrega.AtributosIntegracion.Item(i).Atrib Then
                    Set oAtribOrden = g_oOrdenEntrega.AtributosIntegracion.Item(i)
                    bInsertarAtribInt = False
                    Exit For
                End If
            Next
        End If
    
        If bInsertarAtribInt Then
        
            'Si no lo ha encontrado lo a�adimos
            If oatrib.TipoIntroduccion = Introselec Then
                oatrib.CargarListaDeValoresInt
            End If
            Select Case oatrib.Tipo
                Case 1: g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , oatrib.ValorText, , , oatrib.Validacion_ERP, , , oatrib.Id, oatrib.ListaPonderacion
                Case 2: g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, oatrib.ValorNum, , , , oatrib.Validacion_ERP, , , oatrib.Id, oatrib.ListaPonderacion
                Case 3: g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , oatrib.ValorFec, , oatrib.Validacion_ERP, , , oatrib.Id, oatrib.ListaPonderacion
                Case 4:
                    If oatrib.ValorBool Then
                        g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , 1, oatrib.Validacion_ERP, , , oatrib.Id
                    Else
                        g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , 0, oatrib.Validacion_ERP, , , oatrib.Id
                    End If
            End Select

            If oatrib.Obligatorio Then
                sObl = "(*)"
            Else
                sObl = ""
            End If
        
            Select Case oatrib.Tipo
                Case 1: sdbgAtribProce.AddItem oatrib.Cod & " " & sObl & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.ValorText & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & ""
                Case 2: sdbgAtribProce.AddItem oatrib.Cod & " " & sObl & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.ValorNum & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & ""
                Case 3: sdbgAtribProce.AddItem oatrib.Cod & " " & sObl & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & oatrib.ValorFec & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & ""
                Case 4:
                    If IsNull(oatrib.ValorBool) Then
                        sdbgAtribProce.AddItem oatrib.Cod & " " & sObl & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & ""
                    Else
                        If oatrib.ValorBool Then
                            sdbgAtribProce.AddItem oatrib.Cod & " " & sObl & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & m_sIdiTrue & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & ""
                        Else
                            sdbgAtribProce.AddItem oatrib.Cod & " " & sObl & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & m_sIdiFalse & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & ""
                        End If
                    End If
            End Select
    
        Else
        
            If oAtribOrden.Obligatorio Then
                sObl = "(*)"
            Else
                sObl = ""
            End If

            Select Case oAtribOrden.Tipo
                Case 1:
                    If oAtribOrden.GuardadoEnEmision = False Then 'Si no se ha guardado un valor en esta pantalla previamente pondremos el valor que tenia en frmPedidos
                        oAtribOrden.ValorText = oatrib.ValorText
                    End If
                    
                    sdbgAtribProce.AddItem oAtribOrden.Cod & " " & sObl & Chr(m_lSeparador) & oAtribOrden.Den & Chr(m_lSeparador) & oAtribOrden.ValorText & Chr(m_lSeparador) & oAtribOrden.Atrib & Chr(m_lSeparador) & ""
                Case 2:
                    If oAtribOrden.GuardadoEnEmision = False Then
                        oAtribOrden.ValorNum = oatrib.ValorNum
                    End If
                        
                    sdbgAtribProce.AddItem oAtribOrden.Cod & " " & sObl & Chr(m_lSeparador) & oAtribOrden.Den & Chr(m_lSeparador) & oAtribOrden.ValorNum & Chr(m_lSeparador) & oAtribOrden.Atrib & Chr(m_lSeparador) & ""
                Case 3:
                    If oAtribOrden.GuardadoEnEmision = False Then
                        oAtribOrden.ValorFec = oatrib.ValorFec
                    End If
                    
                    sdbgAtribProce.AddItem oAtribOrden.Cod & " " & sObl & Chr(m_lSeparador) & oAtribOrden.Den & Chr(m_lSeparador) & oAtribOrden.ValorFec & Chr(m_lSeparador) & oAtribOrden.Atrib & Chr(m_lSeparador) & ""
                Case 4:
                    If oAtribOrden.GuardadoEnEmision = False Then
                        oAtribOrden.ValorBool = oatrib.ValorBool
                    End If
                    
                    If IsNull(oAtribOrden.ValorBool) Then
                        sdbgAtribProce.AddItem oAtribOrden.Cod & " " & sObl & Chr(m_lSeparador) & oAtribOrden.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & oAtribOrden.Atrib & Chr(m_lSeparador) & ""
                    Else
                        If oAtribOrden.ValorBool Then
                            sdbgAtribProce.AddItem oAtribOrden.Cod & " " & sObl & Chr(m_lSeparador) & oAtribOrden.Den & Chr(m_lSeparador) & m_sIdiTrue & Chr(m_lSeparador) & oAtribOrden.Atrib & Chr(m_lSeparador) & ""
                        Else
                            sdbgAtribProce.AddItem oAtribOrden.Cod & " " & sObl & Chr(m_lSeparador) & oAtribOrden.Den & Chr(m_lSeparador) & m_sIdiFalse & Chr(m_lSeparador) & oAtribOrden.Atrib & Chr(m_lSeparador) & ""
                        End If
                    End If
                
            End Select
      
        End If
    Next
    
    sdbgAtribProce.MoveFirst

End Sub

''' <summary>
''' Carga el grid de atributos de integracion de ambito item, los atributos marcados para integracion de pedidos (para el ERP :g_sDatoERp )
''' </summary>
''' <remarks>Llamada desde: Load del form  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Private Sub CargarGridAtribIntItem(ByVal iCodErp As Integer)

Dim oatrib As CAtributo
Dim oatribItem As CAtributo
Dim sTipo As String
Dim sObl As String
Dim oOrden As COrdenEntrega
Dim oLinea As CLineaPedido
Dim sLinea As String
Dim sIndiceLinea As String
Dim i As Integer
Dim oValor As Variant

    sdbgAtribItem.RemoveAll
    
    Set m_oAtributosItem = Nothing
    Set m_oAtributosItem = oFSGSRaiz.Generar_CAtributos
    
    m_oAtributosItem.CargarAtributosIntegracionItemProceso iCodErp, TipoAmbitoProceso.AmbItem

    
    If m_oAtributosItem.Count = 0 Then Exit Sub
    
    'cargo la configuracion incial del grid par que no haya columnas duplicadas
    LimpiarGridMiPedido
    ConfigurarGridMiPedido
    
    Set g_oOrdenEntrega = g_oOrigen.g_oOrdenesTemporales.Item(CStr(sstabAtrib.selectedItem.caption))
    sdbgAtribItem.RemoveAll
    
    'Metemos la lista de atributos de integracion de las lineas en la propiedad AtributosLineasIntegracion
    If g_oOrdenEntrega.atributoslineasintegracion Is Nothing Then
        Set g_oOrdenEntrega.atributoslineasintegracion = oFSGSRaiz.Generar_CAtributos
        For Each oatrib In m_oAtributosItem
            g_oOrdenEntrega.atributoslineasintegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , , oatrib.Validacion_ERP, , , oatrib.Id
        Next
    End If
    
    'Comprobaremos si los atributos de linea son tambien atributos de integracion
    Dim oAtribLinea As CAtributo
    If Not g_oOrdenEntrega.AtributosLineas Is Nothing And Not g_oOrdenEntrega.atributoslineasintegracion Is Nothing Then
        For Each oAtribLinea In g_oOrdenEntrega.AtributosLineas
            For Each oatrib In g_oOrdenEntrega.atributoslineasintegracion
                If oAtribLinea.Id = oatrib.Atrib Then
                    oAtribLinea.AtribIntEnAtributosProceso = True
                End If
            Next
        Next
    End If
    
    For Each oLinea In g_oOrdenEntrega.LineasPedido
        If oLinea.AtributosIntegracion Is Nothing Then
            Set oLinea.AtributosIntegracion = oFSGSRaiz.Generar_CAtributos
        End If
            
        'Obtenemos el indice de la misma forma que se hace en frmpedidos para almacenar la linea en la coleccion de ClineasPedido para luego porder
        'acceder a la linea seleccionada y almacenar asi el valor de los atributos de integracion
        sIndiceLinea = oLinea.GMN1Proce & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oLinea.GMN1Proce))
        sIndiceLinea = oLinea.Anyo & sIndiceLinea & oLinea.ProceCod & oLinea.Item
                        
        sLinea = oLinea.ArtCod_Interno & "-" & oLinea.ArtDen & Chr(m_lSeparador) & oLinea.PrecioUC & Chr(m_lSeparador) & oLinea.CantidadPedida & Chr(m_lSeparador) & oLinea.UnidadPedido & Chr(m_lSeparador) & sIndiceLinea
            
        Dim bAtribEncontrado As Boolean
        For Each oatrib In m_oAtributosItem
            bAtribEncontrado = False
            If Not oLinea.AtributosIntegracion Is Nothing And Not oLinea.ATRIBUTOS Is Nothing Then
                        
                For i = 1 To oLinea.AtributosIntegracion.Count
                    If oatrib.Atrib = oLinea.AtributosIntegracion.Item(i).Atrib Then
                        Set oatribItem = oLinea.AtributosIntegracion.Item(i)
                        bAtribEncontrado = True
                        Exit For
                    End If
                Next
                
                If Not oLinea.ATRIBUTOS.Item(CStr(oatrib.Atrib)) Is Nothing Then 'en pantalla emisi�n
                    oValor = MostrarAtributo(oLinea.ATRIBUTOS.Item(CStr(oatrib.Atrib)), oatrib.Tipo, m_sIdiFalse, m_sIdiTrue)
                ElseIf Not bAtribEncontrado Then 'no en pantalla emisi�n, no en colecci�n AtributosIntegracion, lo q diga bbdd
                    Select Case oatrib.Tipo
                    Case 1:
                        oValor = oatrib.ValorText
                    Case 2:
                        oValor = oatrib.ValorNum
                    Case 3:
                        oValor = oatrib.ValorFec
                    Case 4:
                        Select Case oatrib.ValorBool
                        Case 0, False
                            oValor = m_sIdiFalse
                        Case 1, True
                            oValor = m_sIdiTrue
                        Case Else
                            oValor = ""
                        End Select
                    End Select
                Else 'no en pantalla emisi�n, en colecci�n AtributosIntegracion, lo q diga AtributosIntegracion
                    Select Case oatrib.Tipo
                    Case 1:
                        oValor = oatribItem.ValorText
                    Case 2:
                        oValor = oatribItem.ValorNum
                    Case 3:
                        oValor = oatribItem.ValorFec
                    Case 4:
                        oValor = oatribItem.ValorBool
                    End Select
                End If
                
                If bAtribEncontrado = False Then
                    'Si no lo ha encontrado lo a�adimos
                    Select Case oatrib.Tipo
                        Case 1: oLinea.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , oValor, , , oatrib.Validacion_ERP, , , oatrib.Id
                        Case 2: oLinea.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, oValor, , , , oatrib.Validacion_ERP, , , oatrib.Id
                        Case 3: oLinea.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , oValor, , oatrib.Validacion_ERP, , , oatrib.Id
                        Case 4:
                            If NullToStr(oValor) = "" Then
                                oLinea.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , oValor, oatrib.Validacion_ERP, , , oatrib.Id
                            Else
                                If oValor = m_sIdiTrue Then
                                    oLinea.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , 1, oatrib.Validacion_ERP, , , oatrib.Id
                                ElseIf oValor = m_sIdiFalse Then
                                    oLinea.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , 0, oatrib.Validacion_ERP, , , oatrib.Id
                                Else
                                    oLinea.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , oatrib.pedido, oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , oValor, oatrib.Validacion_ERP, , , oatrib.Id
                                End If
                            End If
                    End Select

                    sLinea = sLinea & Chr(m_lSeparador) & NullToStr(oValor)
                    
                Else
                                                                
                    Select Case oatribItem.Tipo
                        Case 1:
                            If oatribItem.GuardadoEnEmision = False Then 'Si no se ha guardado un valor en esta pantalla previamente pondremos el valor que tenia en frmPedidos
                                oatribItem.ValorText = oValor
                            End If
                            sLinea = sLinea & Chr(m_lSeparador) & oatribItem.ValorText
                        Case 2:
                            If oatribItem.GuardadoEnEmision = False Then
                                oatribItem.ValorNum = oValor
                            End If
                            sLinea = sLinea & Chr(m_lSeparador) & oatribItem.ValorNum
                        Case 3
                            If oatribItem.GuardadoEnEmision = False Then
                                oatribItem.ValorFec = oValor
                            End If
                            
                            sLinea = sLinea & Chr(m_lSeparador) & oatribItem.ValorFec
                        Case 4:
                            If oatribItem.GuardadoEnEmision = False Then
                                If NullToStr(oValor) = m_sIdiTrue Then
                                    oatribItem.ValorBool = 1
                                ElseIf NullToStr(oValor) = m_sIdiTrue Then
                                    oatribItem.ValorBool = 0
                                Else
                                    oatribItem.ValorBool = oValor
                                End If
                            End If
                            
                            
                            If NullToStr(oatribItem.ValorBool) = "" Then
                                sLinea = sLinea & Chr(m_lSeparador) & ""
                            Else
                                If oatribItem.ValorBool Then
                                    sLinea = sLinea & Chr(m_lSeparador) & m_sIdiTrue
                                Else
                                    sLinea = sLinea & Chr(m_lSeparador) & m_sIdiFalse
                                End If
                            End If
                    End Select
                End If
            End If
        Next
           
        sdbgAtribItem.AddItem sLinea
        DoEvents
            
    Next
    
    sdbgAtribItem.SplitterPos = 1
    
    sdbgAtribItem.MoveFirst
    
End Sub
    
''' <summary>
''' A�ade un tab de proveedor para cada uno
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub inicializarTabsProveedor()

    Dim oProve As CProveedor
    Dim INumProve As Integer
    Dim j As Integer
    Dim oOrden As COrdenEntrega
    Dim oLinea As CLineaPedido
        
    
    Set m_oProves = Nothing
    Set m_oProves = oFSGSRaiz.generar_CProveedores
    
    For Each oOrden In g_oOrigen.g_oOrdenesTemporales
        For Each oLinea In oOrden.LineasPedido
            If m_oProves.Item(CStr(oLinea.ProveCod)) Is Nothing Then
                m_oProves.Add oLinea.ProveCod, oLinea.ProveDen
                m_oProves.CargarDatosProveedor CStr(oLinea.ProveCod)
            End If
        Next
    Next
    
    'a�ade un tab de proveedor para cada uno
    j = sstabAtrib.Tabs.Count
    While j > 0
        sstabAtrib.Tabs.Remove j
        j = sstabAtrib.Tabs.Count
    Wend
    INumProve = 0
    For Each oProve In m_oProves
        INumProve = INumProve + 1
        sstabAtrib.Tabs.Add INumProve, "A" & oProve.Cod, oProve.Cod
        sstabAtrib.Tabs(INumProve).caption = oProve.Cod
    Next
    
    If (INumProve > 0) Then
        sstabAtrib.Tabs(1).Selected = True
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)

 Dim FOSFile As Scripting.FileSystemObject
        
    Set m_oAtributosProce = Nothing
    Set m_oAtributosItem = Nothing
    Set m_oProves = Nothing
    Set m_oERPsInt = Nothing
    
 On Error Resume Next
    Set FOSFile = New Scripting.FileSystemObject
    
    FOSFile.DeleteFile m_sLayOut, True 'Borro el layout de la grid
    
    Set FOSFile = Nothing
    
    Me.Visible = False
    


End Sub

''' <summary>
''' Carga el combo de atributos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValorItem_DropDown()

    Dim oLista As CValorPond
    Dim iIndice As Integer
    Dim oatrib As CAtributo
    Dim lIdAtrib As Long
    
    iIndice = 1
    
    sdbddValorItem.RemoveAll
    
    lIdAtrib = Right(sdbgAtribItem.Columns(sdbgAtribItem.col).Name, Len(sdbgAtribItem.Columns(sdbgAtribItem.col).Name) - 3)
    Set oatrib = m_oAtributosItem.Item(CStr(lIdAtrib))

    Select Case oatrib.Tipo
    
    Case 1, 2, 3
            
            oatrib.CargarListaDeValoresInt
            For Each oLista In oatrib.ListaPonderacion
                sdbddValorItem.AddItem iIndice & Chr(m_lSeparador) & oLista.ValorLista
                iIndice = iIndice + 1
            Next
        
    Case 4
        sdbddValorItem.AddItem "" & Chr(m_lSeparador) & ""
        sdbddValorItem.AddItem 1 & Chr(m_lSeparador) & m_sIdiTrue
        sdbddValorItem.AddItem 0 & Chr(m_lSeparador) & m_sIdiFalse
    End Select
    
    If (sdbgAtribItem.Columns(sdbgAtribItem.col).Width > 800) Then
        sdbddValorItem.Columns("VALOR").Width = (sdbgAtribItem.Columns(sdbgAtribItem.col).Width - 300)
    Else
        sdbddValorItem.Columns("VALOR").Width = 500
    End If
    
    Set oatrib = Nothing
    Set oLista = Nothing

End Sub

Private Sub sdbddValorItem_InitColumnProps()

 sdbddValorItem.DataFieldList = "Column 0"
 sdbddValorItem.DataFieldToDisplay = "Column 1"

End Sub

''' <summary>
''' Carga el combo de atributos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbddValorProce_DropDown()

    Dim oLista As CValorPond
    Dim iIndice As Integer
    Dim oatrib As CAtributo
    
    iIndice = 1
    
    sdbddValorProce.RemoveAll

    Set oatrib = m_oAtributosProce.Item(CStr(sdbgAtribProce.Columns("ID").Value))

    Select Case oatrib.Tipo
        Case 1, 2, 3
                oatrib.CargarListaDeValoresInt
                For Each oLista In oatrib.ListaPonderacion
                    sdbddValorProce.AddItem iIndice & Chr(m_lSeparador) & oLista.ValorLista
                    iIndice = iIndice + 1
                Next
        Case 4
            sdbddValorProce.AddItem "" & Chr(m_lSeparador) & ""
            sdbddValorProce.AddItem 1 & Chr(m_lSeparador) & m_sIdiTrue
            sdbddValorProce.AddItem 0 & Chr(m_lSeparador) & m_sIdiFalse
    End Select
        
    Set oatrib = Nothing
    Set oLista = Nothing

End Sub

Private Sub sdbddValorProce_InitColumnProps()

 sdbddValorProce.DataFieldList = "Column 0"
 sdbddValorProce.DataFieldToDisplay = "Column 1"

End Sub




Private Sub sdbgAtribItem_AfterUpdate(RtnDispErrMsg As Integer)

Dim oatrib As CAtributo

If Not g_oLineaSeleccionada.AtributosIntegracion Is Nothing Then
    For Each oatrib In g_oLineaSeleccionada.AtributosIntegracion
        Select Case oatrib.Tipo
            Case TiposDeAtributos.TipoNumerico
                If Not IsNull(oatrib.ValorNum) Then
                    sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = oatrib.ValorNum
                Else
                    sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = ""
                End If
            Case TiposDeAtributos.TipoString
                If Not IsNull(oatrib.ValorText) Then
                    sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = oatrib.ValorText
                Else
                    sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = ""
                End If
            Case TiposDeAtributos.TipoFecha
                If Not IsNull(oatrib.ValorFec) Then
                    sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = oatrib.ValorFec
                Else
                    sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = ""
                End If
            Case TiposDeAtributos.TipoBoolean
                If Not NullToStr(oatrib.ValorBool) = "" Then
                    If oatrib.ValorBool Then
                        sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = m_sIdiTrue
                    Else
                        sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = m_sIdiFalse
                    End If
                Else
                    sdbgAtribItem.Columns("AT_" & CStr(oatrib.Atrib)).Text = ""
                End If
        End Select
    Next
End If

End Sub

Private Sub sdbgAtribItem_BeforeRowColChange(Cancel As Integer)

If g_oOrdenEntrega Is Nothing Then Exit Sub
   

 If sdbgAtribItem.col = -1 Then Exit Sub
 If sdbgAtribItem.Rows = 0 Then Exit Sub
 If sdbgAtribItem.DataChanged = False Then Exit Sub
    
 If Not ComprobarValorAtributoItem(sdbgAtribItem.col) Then
    Cancel = True
    'm_bValError = True
    sdbgAtribItem.Columns(sdbgAtribItem.col).Text = ""
    sdbgAtribItem.SetFocus
    Exit Sub
 End If

''Prueba para setear al linea en curso seleccionada
'    Set g_oLineaSeleccionada = DameLineaPedido(g_oOrdenEntrega.LineasPedido, sCod, CStr(sdbgAtribItem.Columns("INDICE").Value))

End Sub

''' <summary>
''' Carga la variable "Lineaseleccionada" tras cada cambio en el grid
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAtribItem_Change()
    Dim sCod As String

    sCod = g_oOrdenEntrega.ProveCod
    
    Set g_oLineaSeleccionada = DameLineaPedido(g_oOrdenEntrega.LineasPedido, sCod, CStr(sdbgAtribItem.Columns("INDICE").Value))
 
End Sub

Private Sub sdbgAtribItem_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim oatrib As CAtributo
Dim lIdAtrib As Long

With sdbgAtribItem

 
 If NullToStr(LastRow) <> NullToStr(sdbgAtribItem.Bookmark) Or sdbgAtribItem.col > 3 Then

        If Left(.Columns(.col).Name, 3) = "AT_" Then
            lIdAtrib = Right(.Columns(.col).Name, Len(.Columns(.col).Name) - 3)
            Set oatrib = m_oAtributosItem.Item(CStr(lIdAtrib))
            
            If Not oatrib Is Nothing Then
                If oatrib.TipoIntroduccion = Introselec Then
                    sdbddValorItem.RemoveAll
                    sdbddValorItem.AddItem ""
                    If oatrib.Tipo = TipoNumerico Then
                        sdbddValorItem.Columns(1).Alignment = ssCaptionAlignmentRight
                    Else
                        sdbddValorItem.Columns(1).Alignment = ssCaptionAlignmentLeft
                    End If
                    .Columns(.col).DropDownHwnd = sdbddValorItem.hwnd
                    sdbddValorItem.Enabled = True
                    sdbddValorItem.DroppedDown = True
                    sdbddValorItem_DropDown
                    sdbddValorItem.DroppedDown = True
                End If
                If oatrib.TipoIntroduccion <> Introselec Then
                    If oatrib.Tipo = TipoBoolean Then
                        sdbddValorItem.RemoveAll
                        sdbddValorItem.AddItem ""
                        .Columns(.col).DropDownHwnd = sdbddValorItem.hwnd
                        sdbddValorItem.Enabled = True
                        sdbddValorItem.DroppedDown = True
                        sdbddValorItem_DropDown
                        sdbddValorItem.DroppedDown = True
                    Else
                        .Columns(.col).DropDownHwnd = 0
                        sdbddValorItem.Enabled = False
                    End If
                End If
            End If
        End If
    
    End If
    
    End With

End Sub

Private Sub sdbgAtribProce_BeforeRowColChange(Cancel As Integer)

    If sdbgAtribProce.col = -1 Then Exit Sub
    ''If sdbgMiPedido.Row = -1 Then Exit Sub ''Para evitar errores l�neas "ocultas"
    If sdbgAtribProce.Rows = 0 Then Exit Sub
    If sdbgAtribProce.DataChanged = False Then Exit Sub
    
    If Not ComprobarValorAtributoProceso(sdbgAtribProce.col) Then
        Cancel = True
        'm_bValError = True
        sdbgAtribProce.Columns(sdbgAtribProce.col).Text = ""
        sdbgAtribProce.SetFocus
        Exit Sub
    End If

End Sub
Private Sub sdbgAtribProce_ColResize(ByVal ColIndex As Integer, Cancel As Integer)
    EvitarPerdidaDatosGrid Cancel
End Sub

Private Sub sdbgAtribProce_Scroll(Cancel As Integer)
    EvitarPerdidaDatosGrid Cancel
End Sub

Private Sub EvitarPerdidaDatosGrid(ByRef pCancelar As Integer)
    Dim oatrib As CAtributo

    Set oatrib = m_oAtributosProce.Item(CStr(sdbgAtribProce.Columns("ID").Value))
    'Si es un combo lo que se ha cambiado bloquea el scroll y el resize. Con esto se evita que se pierdan los datos de la columna
    If (oatrib.TipoIntroduccion <> Introselec And oatrib.Tipo = TipoBoolean) Or (oatrib.TipoIntroduccion = Introselec) Then
        pCancelar = True
    End If
End Sub


''' <summary>
''' Carga los posibles valores de la columna "valor" del grid. Quita el estilo combo de dicha columna si no es combo la nueva celda.
''' </summary>
''' <param name="lastRow">Fila desde la que se mueve</param>
''' <param name="LastCol">Columna desde la que se mueve</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub sdbgAtribProce_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
Dim oatrib As CAtributo

With sdbgAtribProce
 
    If sdbgAtribProce.col = 2 Then
            Set oatrib = m_oAtributosProce.Item(CStr(sdbgAtribProce.Columns("ID").Value))
            
            If Not oatrib Is Nothing And .col > -1 Then
                If oatrib.TipoIntroduccion = Introselec Then
                    sdbddValorProce.RemoveAll
                    sdbddValorProce.AddItem ""
                    If oatrib.Tipo = TipoNumerico Then
                        sdbddValorProce.Columns(1).Alignment = ssCaptionAlignmentRight
                    Else
                        sdbddValorProce.Columns(1).Alignment = ssCaptionAlignmentLeft
                    End If
                    .Columns(.col).DropDownHwnd = sdbddValorProce.hwnd
                    sdbddValorProce.Enabled = True
                End If
                If oatrib.TipoIntroduccion <> Introselec Then
                    If oatrib.Tipo = TipoBoolean Then
                        sdbddValorProce.RemoveAll
                        sdbddValorProce.AddItem ""
                        .Columns(.col).DropDownHwnd = sdbddValorProce.hwnd
                        sdbddValorProce.Enabled = True
                    Else
                        .Columns(.col).DropDownHwnd = 0
                        sdbddValorProce.Enabled = False
                    End If
                End If
            End If
        
    End If
        
    
End With


End Sub

Private Sub sstabAtrib_Click()

CargarGridAtribIntProceso (g_sDatoERp)
'sdbgAtribProce.AllowUpdate = True
sdbgAtribProce.Update
CargarGridAtribIntItem (g_sDatoERp)

End Sub

''' <summary>
''' Configura las columnas que va a tener el grid GridAtribItem, y que en el se a�adiran atributos de integracion
''' </summary>
''' <remarks>Llamada desde: cargarGridAtribIntItem; Tiempo m�ximo: 0,1</remarks>

Private Sub ConfigurarGridMiPedido()
    Dim oAtribs As CAtributos
    Dim oatrib  As CAtributo
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim i As Integer
    Dim oColumn As SSDataWidgets_B.Column
    Dim oParamSM As CParametroSM
    Dim iPosDatosSM As Integer

    Set oAtribs = m_oAtributosItem
    
    i = sdbgAtribItem.Columns.Count
    
    'Metemos los atributos de integracion.
    If Not oAtribs Is Nothing Then
        For Each oatrib In oAtribs
                DoEvents
                sdbgAtribItem.Columns.Add i
                DoEvents
                Set oColumn = sdbgAtribItem.Columns(i)
                oColumn.Name = "AT_" & CStr(oatrib.Atrib)
                oColumn.CaptionAlignment = ssColCapAlignCenter
                Select Case oatrib.Tipo
                Case TiposDeAtributos.TipoString
                    oColumn.Alignment = ssCaptionAlignmentLeft
                    If oatrib.TipoIntroduccion = IntroLibre Then
                        oColumn.Style = ssStyleEdit
                    End If
                    oColumn.FieldLen = 800
                    
                Case TiposDeAtributos.TipoNumerico
                    oColumn.Alignment = ssCaptionAlignmentRight
                    oColumn.NumberFormat = FormateoNumericoComp(2)
                    If oatrib.TipoIntroduccion = IntroLibre Then
                        oColumn.Style = ssStyleEdit
                    End If
                    
                Case TiposDeAtributos.TipoFecha
                    oColumn.Alignment = ssCaptionAlignmentRight
                    If oatrib.TipoIntroduccion = IntroLibre Then
                        oColumn.Style = ssStyleEdit
                    End If
                    
                Case TiposDeAtributos.TipoBoolean
                    oColumn.Style = ssStyleEdit
                    
                End Select
                
                If oatrib.Obligatorio Then
                   oColumn.caption = oatrib.Cod & "(*)"
                Else
                   oColumn.caption = oatrib.Cod
                End If
                'oColumn.StyleSet = "Normal"
                oColumn.Visible = True
                oColumn.Locked = False
                Set oColumn = Nothing
                i = i + 1
            
        Next
    End If
       
    m_bCargarLayout = True
    
    sdbgAtribItem.AllowUpdate = True
    
    'RedimensionarGridItems
    
End Sub

''' <summary>
''' Establece el estilo de una columna
''' </summary>
''' <remarks>Llamada desde: CargarGridAtribIntProceso ; Tiempo m�ximo: 0,2</remarks>
Private Sub ConfigurarGridProceso()
    Dim oAtribs As CAtributos
    Dim oColumn As SSDataWidgets_B.Column
    
    Set oAtribs = m_oAtributosProce
    
    If Not oAtribs Is Nothing Then
    
        Set oColumn = sdbgAtribProce.Columns("VALOR")
        oColumn.Style = ssStyleEdit
        sdbgAtribProce.Columns("VALOR").DropDownHwnd = 0
    End If
    
    sdbgAtribProce.AllowUpdate = True
    
End Sub

Private Sub LimpiarGridMiPedido()
Dim i As Integer
Dim iCols As Integer

sdbgAtribItem.RemoveAll
If m_bCargarLayout Then
    sdbgAtribItem.LoadLayout m_sLayOut
    sdbgAtribItem.FieldSeparator = Chr(m_lSeparador)
    m_bCargarLayout = False
    'enlazamos las dropdown
'    sdbddDestinos.AddItem ""
'    sdbddUnidades.AddItem ""
'    sdbgMiPedido.Columns("DEST").DropDownHwnd = sdbddDestinos.hwnd
'    sdbgMiPedido.Columns("UP").DropDownHwnd = sdbddUnidades.hwnd
'    sdbddCentros.AddItem ""
'    sdbddAlmacenes.AddItem ""
'    sdbgMiPedido.Columns("CENTRO").DropDownHwnd = sdbddCentros.hwnd
'    sdbgMiPedido.Columns("ALMACEN").DropDownHwnd = sdbddAlmacenes.hwnd
End If

End Sub
Private Sub LimpiarGridProceso()
Dim i As Integer
Dim iCols As Integer

sdbgAtribProce.RemoveAll
'If m_bCargarLayout Then
'    sdbgAtribItem.LoadLayout m_sLayOut
'    sdbgAtribItem.FieldSeparator = Chr(m_lSeparador)
'    m_bCargarLayout = False
    'enlazamos las dropdown
'    sdbddDestinos.AddItem ""
'    sdbddUnidades.AddItem ""
'    sdbgMiPedido.Columns("DEST").DropDownHwnd = sdbddDestinos.hwnd
'    sdbgMiPedido.Columns("UP").DropDownHwnd = sdbddUnidades.hwnd
'    sdbddCentros.AddItem ""
'    sdbddAlmacenes.AddItem ""
'    sdbgMiPedido.Columns("CENTRO").DropDownHwnd = sdbddCentros.hwnd
'    sdbgMiPedido.Columns("ALMACEN").DropDownHwnd = sdbddAlmacenes.hwnd
'End If
sdbddValorProce.AddItem ""
sdbgAtribProce.Columns("VALOR").DropDownHwnd = sdbddValorProce.hwnd

End Sub

'''<summary>Funci�n que comprueba y actualiza el valor del atributo de integracion de Proceso (o a�ade el atributo a la colecci�n en caso de que no exista)</summary>
'''<param name="LastCol">Identificador de la columna de la grid</param>
'''<returns>TRUE si todo es correcto </returns>
'''<remarks>Llamada desde: FSGSClient.frmTraspasoPedERP.sdbgAtribProce_BeforeRowColChange Tiempo m�ximo: 0,1</remarks>
Private Function ComprobarValorAtributoProceso(ByVal LastCol As Integer) As Boolean
Dim lIdAtrib As Long
Dim i As Integer
Dim oatrib As CAtributo
Dim bSalir As Boolean
Dim bEncontrado As Boolean
Dim oElem As CValorPond
Dim sError As String
Dim vValorM As Variant

If Not m_bEstaComprobado Then
m_bEstaComprobado = True

If LastCol > 1 Then
    If sdbgAtribProce.Columns(LastCol).Name = "VALOR" Then
    
'        lIdAtrib = Right(sdbgMiPedido.Columns(LastCol).Name, Len(sdbgMiPedido.Columns(LastCol).Name) - 3)
'        Set oatrib = g_oOrdenEntrega.AtributosLineas.Item(CStr(lIdAtrib))
        Set oatrib = m_oAtributosProce.Item(CStr(sdbgAtribProce.Columns("ID").Value))
            
        If sdbgAtribProce.Columns(LastCol).Text <> "" Then
            bSalir = False
            Select Case oatrib.Tipo
            Case TiposDeAtributos.TipoNumerico
                If Not IsNumeric(sdbgAtribProce.Columns(LastCol).Text) Then
                    sError = "TIPO2"
                    bSalir = True
                End If
                
            Case TiposDeAtributos.TipoFecha
                If Not IsDate(sdbgAtribProce.Columns(LastCol).Text) Then
                    sError = "TIPO3"
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoBoolean
                If UCase(sdbgAtribProce.Columns(LastCol).Text) <> UCase(m_sIdiTrue) And UCase(sdbgAtribProce.Columns(LastCol).Text) <> UCase(m_sIdiFalse) Then
                    sError = "TIPO4"
                    bSalir = True
                End If
            End Select
            If bSalir Then
                basMensajes.AtributoValorNoValido sError
                m_bEstaComprobado = False
                ComprobarValorAtributoProceso = False
                Exit Function
            End If
            
            bEncontrado = False
            If oatrib.TipoIntroduccion = Introselec Then
                For Each oElem In oatrib.ListaPonderacion
                    Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoString
                        If oElem.ValorLista = sdbgAtribProce.Columns(LastCol).Text Then
                            bEncontrado = True
                            Exit For
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        If CDec(oElem.ValorLista) = CDec(sdbgAtribProce.Columns(LastCol).Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If CDate(oElem.ValorLista) = CDate(sdbgAtribProce.Columns(LastCol).Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    End Select
                Next
                If Not bEncontrado Then
                    basMensajes.AtributoValorNoValido "NO_LISTA"
                    m_bEstaComprobado = False
                    ComprobarValorAtributoProceso = False
                    Exit Function
                End If
            End If
            bSalir = False
            sError = ""
            If oatrib.TipoIntroduccion = IntroLibre Then
                Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoNumerico
                        vValorM = CDec(sdbgAtribProce.Columns(LastCol).Text)
                        If IsNumeric(oatrib.Maximo) And Not IsEmpty(oatrib.Maximo) Then
                            sError = FormateoNumerico(oatrib.Maximo)
                            If CDec(oatrib.Maximo) < CDec(vValorM) Then
                                bSalir = True
                            End If
                        End If
                        If IsNumeric(oatrib.Minimo) And Not IsEmpty(oatrib.Minimo) Then
                            If sError = "" Then
                                sError = sIdiMayor & " " & FormateoNumerico(oatrib.Minimo)
                            Else
                                sError = sIdiEntre & "  " & FormateoNumerico(oatrib.Minimo) & " - " & sError
                            End If
                            If CDec(oatrib.Minimo) > CDec(vValorM) Then
                                bSalir = True
                            End If
                        Else
                            If sError <> "" Then
                                sError = sIdiMenor & " " & sError
                            End If
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If IsDate(oatrib.Maximo) Then
                            sError = oatrib.Maximo
                            If CDate(oatrib.Maximo) < CDate(sdbgAtribProce.Columns(LastCol).Text) Then
                                bSalir = True
                            End If
                        End If
                        If IsDate(oatrib.Minimo) Then
                            If sError = "" Then
                                sError = sIdiMayor & " " & oatrib.Minimo
                            Else
                                sError = sIdiEntre & "  " & oatrib.Minimo & " - " & sError
                            End If
                            If CDate(oatrib.Minimo) > CDate(sdbgAtribProce.Columns(LastCol).Text) Then
                                bSalir = True
                            End If
                        Else
                            If sError <> "" Then
                                sError = sIdiMenor & " " & sError
                            End If
                        End If
                End Select
                If bSalir Then
                    basMensajes.AtributoValorNoValido sError
                    sdbgAtribProce.Columns(LastCol).Text = ""
                    m_bEstaComprobado = False
                    ComprobarValorAtributoProceso = False
                    Exit Function
                End If
            End If
            'Aqui miramos si existe el atributo, Si no existe crea uno nuevo si no cambia sus valores para que este actualizado el grid con respecto al objeto
            If g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)) Is Nothing Then
                With oatrib
                    Select Case .Tipo
                    Case TiposDeAtributos.TipoNumerico
                        g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbProceso, oatrib.Obligatorio, oatrib.Descripcion, sdbgAtribProce.Columns(LastCol).Text, , , , oatrib.Validacion_ERP, , , oatrib.Id, , True
                    Case TiposDeAtributos.TipoFecha
                        g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbProceso, oatrib.Obligatorio, oatrib.Descripcion, , , sdbgAtribProce.Columns(LastCol).Text, , oatrib.Validacion_ERP, , , oatrib.Id, , True
                    Case TiposDeAtributos.TipoBoolean
                        If UCase(sdbgAtribProce.Columns(LastCol).Text) = UCase(m_sIdiTrue) Then
                            g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbProceso, oatrib.Obligatorio, oatrib.Descripcion, , , , True, oatrib.Validacion_ERP, , , oatrib.Id, , True
                        Else
                            g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbProceso, oatrib.Obligatorio, oatrib.Descripcion, , , , False, oatrib.Validacion_ERP, , , oatrib.Id, , True
                        End If
                    Case TiposDeAtributos.TipoString
                        g_oOrdenEntrega.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbProceso, oatrib.Obligatorio, oatrib.Descripcion, , sdbgAtribProce.Columns(LastCol).Text, , , oatrib.Validacion_ERP, , , oatrib.Id, , True
                    End Select
                    If .TipoIntroduccion = Introselec Then
                       g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).CargarListaDeValoresInt
                    End If
                End With
            Else
                Select Case oatrib.Tipo
                Case TiposDeAtributos.TipoNumerico
                    g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorNum = sdbgAtribProce.Columns(LastCol).Text
                    
                Case TiposDeAtributos.TipoFecha
                    g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorFec = CDate(sdbgAtribProce.Columns(LastCol).Text)
    
                Case TiposDeAtributos.TipoBoolean
                    If UCase(sdbgAtribProce.Columns(LastCol).Text) = UCase(m_sIdiTrue) Then
                        g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorBool = True
                    Else
                        g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorBool = False
                    End If
                
                Case TiposDeAtributos.TipoString
                    g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorText = sdbgAtribProce.Columns(LastCol).Text
                End Select
                'Con GuardadoEnEmision a True le decimos que en esta pantalla se le ha cambiado el valor al atributo
                'Asi cuando hay mas de un proveedor en esta pantalla y cambiemos de pesta�a, al volver mostrara el valor que
                'se guardo en su momento, no el que tiene el atributo por defecto o el que tenia ya cargado en frmPedidos
                g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).GuardadoEnEmision = True
            End If
        Else
            If Not g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)) Is Nothing Then
                g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorText = Null
                g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorBool = Null
                g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorFec = Null
                g_oOrdenEntrega.AtributosIntegracion.Item(CStr(oatrib.Atrib)).ValorNum = Null
            End If
        End If 'Si hab�a valor
    End If
    
End If
End If
m_bEstaComprobado = False
ComprobarValorAtributoProceso = True
End Function

'''<summary>Funci�n que comprueba y actualiza el valor del atributo (o a�ade el atributo a la colecci�n en caso de que no exista)</summary>
'''<param name="LastCol">Identificador de la columna de la grid</param>
'''<returns>TRUE si todo es correcto </returns>
'''<remarks>Llamada desde: FSGSClient.frmPedidos.sdbgAtribItem_BeforeRowColChange Tiempo m�ximo: 0,1</remarks>
Private Function ComprobarValorAtributoItem(ByVal LastCol As Integer) As Boolean
Dim lIdAtrib As Long
Dim i As Integer
Dim oatrib As CAtributo
Dim bSalir As Boolean
Dim bEncontrado As Boolean
Dim oElem As CValorPond
Dim sError As String
Dim vValorM As Variant

If Not m_bEstaComprobado Then
m_bEstaComprobado = True

If LastCol > 4 Then
    If Left(sdbgAtribItem.Columns(LastCol).Name, 3) = "AT_" Then
    
        lIdAtrib = Right(sdbgAtribItem.Columns(LastCol).Name, Len(sdbgAtribItem.Columns(LastCol).Name) - 3)
        Set oatrib = m_oAtributosItem.Item(CStr(lIdAtrib))
            
        If sdbgAtribItem.Columns(LastCol).Text <> "" Then
            bSalir = False
            Select Case oatrib.Tipo
            Case TiposDeAtributos.TipoNumerico
                If Not IsNumeric(sdbgAtribItem.Columns(LastCol).Text) Then
                    sError = "TIPO2"
                    bSalir = True
                End If
                
            Case TiposDeAtributos.TipoFecha
                If Not IsDate(sdbgAtribItem.Columns(LastCol).Text) Then
                    sError = "TIPO3"
                    bSalir = True
                End If
            Case TiposDeAtributos.TipoBoolean
                If UCase(sdbgAtribItem.Columns(LastCol).Text) <> UCase(m_sIdiTrue) And UCase(sdbgAtribItem.Columns(LastCol).Text) <> UCase(m_sIdiFalse) Then
                    sError = "TIPO4"
                    bSalir = True
                End If
            End Select
            If bSalir Then
                basMensajes.AtributoValorNoValido sError
                m_bEstaComprobado = False
                ComprobarValorAtributoItem = False
                Exit Function
            End If
            
            bEncontrado = False
            If oatrib.TipoIntroduccion = Introselec Then
                For Each oElem In oatrib.ListaPonderacion
                    Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoString
                        If oElem.ValorLista = sdbgAtribItem.Columns(LastCol).Text Then
                            bEncontrado = True
                            Exit For
                        End If
                    Case TiposDeAtributos.TipoNumerico
                        If CDec(oElem.ValorLista) = CDec(sdbgAtribItem.Columns(LastCol).Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    Case TiposDeAtributos.TipoFecha
                        If CDate(oElem.ValorLista) = CDate(sdbgAtribItem.Columns(LastCol).Text) Then
                            bEncontrado = True
                            Exit For
                        End If
                    End Select
                Next
                If Not bEncontrado Then
                    basMensajes.AtributoValorNoValido "NO_LISTA"
                    m_bEstaComprobado = False
                    ComprobarValorAtributoItem = False
                    Exit Function
                End If
            End If
            bSalir = False
            sError = ""
            If oatrib.TipoIntroduccion = IntroLibre Then
                Select Case oatrib.Tipo
                    Case TiposDeAtributos.TipoNumerico
                        vValorM = CDec(sdbgAtribItem.Columns(LastCol).Text)
'                        If IsNumeric(oatrib.Maximo) Then
'                            sError = FormateoNumerico(oatrib.Maximo)
'                            If CDec(oatrib.Maximo) < CDec(vValorM) Then
'                                bSalir = True
'                            End If
'                        End If
'                        If IsNumeric(oatrib.Minimo) Then
'                            If sError = "" Then
'                                sError = sIdiMayor & " " & FormateoNumerico(oatrib.Minimo)
'                            Else
'                                sError = sIdiEntre & "  " & FormateoNumerico(oatrib.Minimo) & " - " & sError
'                            End If
'                            If CDec(oatrib.Minimo) > CDec(vValorM) Then
'                                bSalir = True
'                            End If
'                        Else
'                            If sError <> "" Then
'                                sError = sIdiMenor & " " & sError
'                            End If
'                        End If
                    Case TiposDeAtributos.TipoFecha
                        If IsDate(oatrib.Maximo) Then
                            sError = oatrib.Maximo
                            If CDate(oatrib.Maximo) < CDate(sdbgAtribItem.Columns(LastCol).Text) Then
                                bSalir = True
                            End If
                        End If
'                        If IsDate(oatrib.Minimo) Then
'                            If sError = "" Then
'                                sError = sIdiMayor & " " & oatrib.Minimo
'                            Else
'                                sError = sIdiEntre & "  " & oatrib.Minimo & " - " & sError
'                            End If
'                            If CDate(oatrib.Minimo) > CDate(sdbgAtribItem.Columns(LastCol).Text) Then
'                                bSalir = True
'                            End If
'                        Else
'                            If sError <> "" Then
'                                sError = sIdiMenor & " " & sError
'                            End If
'                        End If
                End Select
                If bSalir Then
                    basMensajes.AtributoValorNoValido sError
                    sdbgAtribItem.Columns(LastCol).Text = ""
                    m_bEstaComprobado = False
                    ComprobarValorAtributoItem = False
                    Exit Function
                End If
            End If
            If g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)) Is Nothing Then
                With oatrib
                    Select Case .Tipo
                    Case TiposDeAtributos.TipoNumerico
                        g_oLineaSeleccionada.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, sdbgAtribItem.Columns(LastCol).Text, , , , oatrib.Validacion_ERP, , , oatrib.Id, , True
                    Case TiposDeAtributos.TipoFecha
                        g_oLineaSeleccionada.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , sdbgAtribItem.Columns(LastCol).Text, , oatrib.Validacion_ERP, , , oatrib.Id, , True
                    Case TiposDeAtributos.TipoBoolean
                        If UCase(sdbgAtribItem.Columns(LastCol).Text) = UCase(m_sIdiTrue) Then
                            g_oLineaSeleccionada.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , True, oatrib.Validacion_ERP, , , oatrib.Id, , True
                        Else
                            g_oLineaSeleccionada.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , , , False, oatrib.Validacion_ERP, , , oatrib.Id, , True
                        End If
                    Case TiposDeAtributos.TipoString
                        g_oLineaSeleccionada.AtributosIntegracion.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, oatrib.Cod, , , oatrib.TipoIntroduccion, AmbItem, oatrib.Obligatorio, oatrib.Descripcion, , sdbgAtribItem.Columns(LastCol).Text, , , oatrib.Validacion_ERP, , , oatrib.Id, , True
                    End Select
                    'cargar la lista de valores del atributo para que a la hora de emitir almacene sus valores
                    If .TipoIntroduccion = Introselec Then
                        g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).CargarListaDeValoresInt
                    End If
                End With
            Else
                Select Case oatrib.Tipo
                Case TiposDeAtributos.TipoNumerico
                    g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorNum = sdbgAtribItem.Columns(LastCol).Text
                    
                Case TiposDeAtributos.TipoFecha
                    g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorFec = CDate(sdbgAtribItem.Columns(LastCol).Text)
    
                Case TiposDeAtributos.TipoBoolean
                    If UCase(sdbgAtribItem.Columns(LastCol).Text) = UCase(m_sIdiTrue) Then
                        g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorBool = True
                    Else
                        g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorBool = False
                    End If
                
                Case TiposDeAtributos.TipoString
                    g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorText = sdbgAtribItem.Columns(LastCol).Text
                End Select
                'Con GuardadoEnEmision a True le decimos que en esta pantalla se le ha cambiado el valor al atributo
                'Asi cuando hay mas de un proveedor en esta pantalla y cambiemos de pesta�a, al volver mostrara el valor que
                'se guardo en su momento, no el que tiene el atributo por defecto o el que tenia ya cargado en frmPedidos
                g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(oatrib.Atrib)).GuardadoEnEmision = True
            End If
        Else
            If Not g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)) Is Nothing Then
                g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorText = Null
                g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorBool = Null
                g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorFec = Null
                g_oLineaSeleccionada.AtributosIntegracion.Item(CStr(lIdAtrib)).ValorNum = Null
            End If
        End If 'Si hab�a valor
    End If
    
End If
End If
m_bEstaComprobado = False
ComprobarValorAtributoItem = True

End Function

Private Function DameLineaPedido(ByVal Coleccion As CLineasPedido, ByVal sCod As String, ByVal sIndice As String) As CLineaPedido
    If Not Coleccion.Item(sIndice) Is Nothing Then
        Set DameLineaPedido = Coleccion.Item(sIndice)
    ElseIf Not Coleccion.Item(sCod) Is Nothing Then
        Set DameLineaPedido = Coleccion.Item(sCod)
    Else
        Set DameLineaPedido = Nothing
    End If
End Function

'''<summary>Funci�n que comprueba que todos los atributos de integracion marcados como obligatorios tiene valor</summary>
'''<returns>TRUE si todo es correcto </returns>
'''<remarks>Llamada desde: FSGSClient.frmTraspasoPedERP.CmdSiguiente_click Tiempo m�ximo: 0,1</remarks>
Private Function ComprobarValorAtributosObligatorios() As Boolean
Dim lIdAtrib As Long
Dim i As Integer
Dim oatrib As CAtributo
Dim oAtribOrden As CAtributo
Dim bSalir As Boolean


Dim oElem As CValorPond
Dim sError As String
Dim vValorM As Variant
Dim oOrden As COrdenEntrega
Dim oLinea As CLineaPedido

Dim bEncontrado As Boolean

'If Not m_bEstaComprobado Then
'm_bEstaComprobado = True


sError = "OBLIGATORIO"

'Primero comprobamos los atributos de proceso y luego los atributos de item
'Miramos por cada orden "es decir por cada pestanya de traspasoPed ERP"
For Each oOrden In g_oOrigen.g_oOrdenesTemporales
  
  If Not m_oAtributosProce Is Nothing Then
     If Not oOrden.AtributosIntegracion Is Nothing Then
     
         For Each oatrib In m_oAtributosProce
              If oatrib.Obligatorio Then
                 If Not oOrden.AtributosIntegracion.Item(CStr(oatrib.Atrib)) Is Nothing Then
                    'Commprobar que tiene valor
                    Set oAtribOrden = oOrden.AtributosIntegracion.Item(CStr(oatrib.Atrib))
                    bEncontrado = False
                    With oatrib
                        Select Case .Tipo
                            Case TiposDeAtributos.TipoNumerico
                                'Comprobar si tiene valor numerico
                                 If Not IsNumeric(oAtribOrden.ValorNum) Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                            Case TiposDeAtributos.TipoFecha
                                'Comprobar si tiene valor Fecha
                                If Not IsDate(oAtribOrden.ValorFec) Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                            Case TiposDeAtributos.TipoBoolean
                                'Comprobar sitiene valor bolleano
                                 If IsNull(oAtribOrden.ValorBool) Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                            Case TiposDeAtributos.TipoString
                                'Comprobar si tiene valor stri
                                If IsNull(oAtribOrden.ValorText) Or (oAtribOrden.ValorText = "") Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                        End Select
                    End With
                 Else
                     'Error por que no forma parte de la colecion de la orden y deberia estar
                        basMensajes.FaltaAtributoObligatorio sError, oOrden.ProveCod
                        ComprobarValorAtributosObligatorios = False
                        Exit Function
                 End If
                 
                 
              End If
         Next
          If bEncontrado Then
                basMensajes.FaltaAtributoObligatorio sError, oOrden.ProveCod
                ComprobarValorAtributosObligatorios = False
                Exit Function
          End If
     Else
       ''Error por que hay atributos de integracion de proceso y la orden de entrega no tiene atributos de integracion
       basMensajes.FaltaAtributoObligatorio sError, oOrden.ProveCod
       ComprobarValorAtributosObligatorios = False
       Exit Function
    End If
  End If
  
  '''''Atributos de proceso comprobados
  
  'Comprobamos los atributos de item para ello hay que mirar cada una de la lineas CPedido de la orden de entraga
  If Not m_oAtributosItem Is Nothing Then
     For Each oLinea In oOrden.LineasPedido
         If Not oLinea.AtributosIntegracion Is Nothing Then
            For Each oatrib In m_oAtributosItem
                If oatrib.Obligatorio Then
                   If Not oLinea.AtributosIntegracion.Item(CStr(oatrib.Atrib)) Is Nothing Then
                      'Commprobar que tiene valor
                    Set oAtribOrden = oLinea.AtributosIntegracion.Item(CStr(oatrib.Atrib))
                    bEncontrado = False
                    With oatrib
                        Select Case .Tipo
                            Case TiposDeAtributos.TipoNumerico
                                'Comprobar si tiene valor numerico
                                 If Not IsNumeric(oAtribOrden.ValorNum) Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                            Case TiposDeAtributos.TipoFecha
                                'Comprobar si tiene valor Fecha
                                If Not IsDate(oAtribOrden.ValorFec) Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                            Case TiposDeAtributos.TipoBoolean
                                'Comprobar sitiene valor bolleano
                                 If IsNull(oAtribOrden.ValorBool) Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                            Case TiposDeAtributos.TipoString
                                'Comprobar si tiene valor stri
                                If IsNull(oAtribOrden.ValorText) Or (oAtribOrden.ValorText = "") Then
                                    bEncontrado = True
                                    Exit For
                                 End If
                        End Select
                    End With
                   
                   Else
                   'Error por que el atributo no esta en la collecion de atributos de intagracion de la linea
                                basMensajes.FaltaAtributoObligatorio sError, oOrden.ProveCod
                                ComprobarValorAtributosObligatorios = False
                                Exit Function
                   End If
                   
                End If
            Next
            'Error el atributo existe en la collecion de la linea pero no tiene valor
             If bEncontrado Then
                basMensajes.FaltaAtributoObligatorio sError, oOrden.ProveCod
                ComprobarValorAtributosObligatorios = False
                Exit Function
             End If
         End If
     
     Next
  End If

Next
 'Se ha comprobado que todos los atributos obligatorios tienen un valor
 ComprobarValorAtributosObligatorios = True
 
End Function

''' <summary>
''' Validaciones de cada ERP en la emisi�n de pedidos
''' </summary>
''' <returns>True/false paso la validacion</returns>
''' <remarks>Llamada desde: cmdSiguiente_Click ; Tiempo m�ximo: 0,2</remarks>
Private Function ValidacionERP() As Boolean

    Dim j As Integer
    Dim dImporte As Double
    Dim oOrden As COrdenEntrega
    Dim oLinea As CLineaPedido
    Dim oatrib As CAtributoOfertado
    Dim oatribInt As CAtributo
    
    
    ''Arrays con los datos del pedido.
    ''Se pasar�n a la mapper para las validaciones de cada ERP en la emisi�n de pedidos (si est�n definidas)
    'Datos nivel item
    Dim arrItemAnyo() As Integer, arrItemProce() As Long
    Dim arrItemGmn1() As String, arrItemProve() As String, arrItemAlm() As String
    Dim arrItemId() As Long
    Dim arrItemIndice() As Variant
    Dim arrItemCentro() As String
    Dim arrItemPrecio() As Double, arrItemCantidad() As Double
    Dim arrItemPres() As String, arrItemPedAbierto() As String
    Dim arrItemInicio() As Variant, arrItemFin() As Variant
    Dim arrItemUnidadPedido() As String, arrItemDestino() As String
    Dim arrItemFecEntrega() As Variant
    Dim iNumElemIt As Integer
    'Pres5_Niv1 l�nea
    Dim arrItemPres5Item() As String, arrItemPres5Cod() As String, arrItemPres5CentroSM() As String
    Dim iNumElemItPres5 As Integer
    Dim bImputacionLinea As Boolean
    'Datos nivel cabecera
    Dim arrProceEmpresa() As Integer
    Dim arrProceCodERP() As String, arrProceOrgCompras() As String, arrProceViaPag() As String
    Dim arrProceTipoPedido() As String
    Dim iNumElemPrCodErp As Integer
    'Atributos cabecera
    Dim arrProceAtribOrden() As String
    Dim arrProceAtribId() As Long
    Dim arrProceAtribValor() As String
    Dim iNumElemPrAtr As Integer
    'Atributos l�nea
    Dim arrItemAtribItem() As String
    Dim arrItemAtribId() As Long
    Dim arrItemAtribValor() As String
    Dim iNumElemItAtr As Integer
    'Pres4_Niv1 l�nea
    Dim arrItemPres4Item() As String
    Dim arrItemPres4Cod() As String
    Dim arrItemPres4Porcen() As Double
    Dim iNumElemItPres4 As Integer
    Dim iContPres4PorItem As Integer
    Dim TextosGS As cTextosGS
    Dim Modulo As Integer
    Dim bSeguir As Boolean
    Dim oImp As Cimputacion
    
    
    ReDim arrItemAnyo(0 To 0) As Integer
    ReDim arrItemProce(0 To 0) As Long
    ReDim arrItemGmn1(0 To 0) As String
    ReDim arrItemId(0 To 0) As Long
    ReDim arrItemIndice(0 To 0) As Variant
    ReDim arrItemProve(0 To 0) As String
    ReDim arrItemAlm(0 To 0) As String
    ReDim arrItemCentro(0 To 0) As String
    ReDim arrItemPrecio(0 To 0) As Double
    ReDim arrItemCantidad(0 To 0) As Double
    ReDim arrItemPres(0 To 0) As String
    ReDim arrItemPedAbierto(0 To 0) As String
    ReDim arrItemInicio(0 To 0) As Variant
    ReDim arrItemFin(0 To 0) As Variant
    ReDim arrItemUnidadPedido(0 To 0) As String
    ReDim arrItemDestino(0 To 0) As String
    ReDim arrItemPres5Item(0 To 0) As String
    ReDim arrItemPres5Cod(0 To 0) As String
    ReDim arrItemPres5CentroSM(0 To 0) As String
    ReDim arrItemFecEntrega(0 To 0) As Variant
    iNumElemItPres5 = 0
    iNumElemIt = 0
    '
    ReDim arrProceEmpresa(0 To 0) As Integer
    ReDim arrProceCodERP(0 To 0) As String
    ReDim arrProceOrgCompras(0 To 0) As String
    ReDim arrProceViaPag(0 To 0) As String
    ReDim arrProceTipoPedido(0 To 0) As String
    iNumElemPrCodErp = 0
    '
    ReDim arrProceAtribOrden(0 To 0) As String
    ReDim arrProceAtribId(0 To 0) As Long
    ReDim arrProceAtribValor(0 To 0) As String
    iNumElemPrAtr = 0
    '
    ReDim arrItemAtribItem(0 To 0) As String
    ReDim arrItemAtribId(0 To 0) As Long
    ReDim arrItemAtribValor(0 To 0) As String
    iNumElemItAtr = 0
    '
    ReDim arrItemPres4Item(0 To 0) As String
    ReDim arrItemPres4Cod(0 To 0) As String
    ReDim arrItemPres4Porcen(0 To 0) As Double
    iNumElemItPres4 = 0
    '
    
    For Each oOrden In g_oOrigen.g_oOrdenesTemporales
        'Atributos de integracion que se han a�adido en traspasoPedERP
        If Not oOrden.AtributosIntegracion Is Nothing Then
            For Each oatribInt In oOrden.AtributosIntegracion
                ReDim Preserve arrProceAtribOrden(0 To iNumElemPrAtr) As String
                ReDim Preserve arrProceAtribId(0 To iNumElemPrAtr) As Long
                ReDim Preserve arrProceAtribValor(0 To iNumElemPrAtr) As String
                arrProceAtribOrden(iNumElemPrAtr) = oOrden.ProveCod
                arrProceAtribId(iNumElemPrAtr) = CLng(oatribInt.Atrib)
                Select Case oatribInt.Tipo
                    Case TipoString
                        arrProceAtribValor(iNumElemPrAtr) = NullToStr(UCase(oatribInt.ValorText))
                    Case TipoNumerico
                        arrProceAtribValor(iNumElemPrAtr) = NullToStr(oatribInt.ValorNum)
                    Case TipoFecha
                        arrProceAtribValor(iNumElemPrAtr) = NullToStr(oatribInt.ValorFec)
                    Case TipoBoolean
                        arrProceAtribValor(iNumElemPrAtr) = NullToStr(oatribInt.ValorBool)
                End Select
                iNumElemPrAtr = iNumElemPrAtr + 1
            Next
        End If
        
        ReDim Preserve arrProceEmpresa(0 To iNumElemPrCodErp) As Integer
        ReDim Preserve arrProceCodERP(0 To iNumElemPrCodErp) As String
        ReDim Preserve arrProceOrgCompras(0 To iNumElemPrCodErp) As String
        ReDim Preserve arrProceViaPag(0 To iNumElemPrCodErp) As String
        ReDim Preserve arrProceTipoPedido(0 To iNumElemPrCodErp) As String
        arrProceEmpresa(iNumElemPrCodErp) = oOrden.Empresa
        arrProceCodERP(iNumElemPrCodErp) = NullToStr(oOrden.ProveERP)
        arrProceOrgCompras(iNumElemPrCodErp) = oOrden.OrgCompras
        arrProceViaPag(iNumElemPrCodErp) = NullToStr(oOrden.ViaPagoCod)
        arrProceTipoPedido(iNumElemPrCodErp) = NullToStr(oOrden.TipoDePedido.Cod)
        '
        iNumElemPrCodErp = iNumElemPrCodErp + 1
        
        For Each oLinea In oOrden.LineasPedido
          'Atributos de integracion de las lineas de pedido a�adidos en frmTraspasoPedERP
            If Not oLinea.AtributosIntegracion Is Nothing Then
                For Each oatribInt In oLinea.AtributosIntegracion
                    ReDim Preserve arrItemAtribItem(0 To iNumElemItAtr) As String
                    ReDim Preserve arrItemAtribId(0 To iNumElemItAtr) As Long
                    ReDim Preserve arrItemAtribValor(0 To iNumElemItAtr) As String
                    arrItemAtribItem(iNumElemItAtr) = oLinea.Anyo & oLinea.GMN1Proce & oLinea.ProceCod & oLinea.Item & oLinea.indice
                    arrItemAtribId(iNumElemItAtr) = CLng(oatribInt.Atrib)
                    Select Case oatribInt.Tipo
                        Case TipoString
                            arrItemAtribValor(iNumElemItAtr) = NullToStr(UCase(oatribInt.ValorText))
                        Case TipoNumerico
                            arrItemAtribValor(iNumElemItAtr) = NullToStr(oatribInt.ValorNum)
                        Case TipoFecha
                            arrItemAtribValor(iNumElemItAtr) = NullToStr(oatribInt.ValorFec)
                        Case TipoBoolean
                            If NullToStr(oatribInt.ValorBool) = "" Then
                                arrItemAtribValor(iNumElemItAtr) = NullToStr(oatribInt.ValorBool)
                            ElseIf oatribInt.ValorBool Then
                                arrItemAtribValor(iNumElemItAtr) = "True"
                            Else
                                arrItemAtribValor(iNumElemItAtr) = "False"
                            End If
                    End Select
                    iNumElemItAtr = iNumElemItAtr + 1
                Next
            End If
    
            ReDim Preserve arrItemAnyo(0 To iNumElemIt) As Integer
            ReDim Preserve arrItemProce(0 To iNumElemIt) As Long
            ReDim Preserve arrItemGmn1(0 To iNumElemIt) As String
            ReDim Preserve arrItemId(0 To iNumElemIt) As Long
            ReDim Preserve arrItemIndice(0 To iNumElemIt) As Variant
            ReDim Preserve arrItemProve(0 To iNumElemIt) As String
            ReDim Preserve arrItemAlm(0 To iNumElemIt) As String
            ReDim Preserve arrItemCentro(0 To iNumElemIt) As String
            ReDim Preserve arrItemPrecio(0 To iNumElemIt) As Double
            ReDim Preserve arrItemCantidad(0 To iNumElemIt) As Double
            ReDim Preserve arrItemPres(0 To iNumElemIt) As String
            ReDim Preserve arrItemPedAbierto(0 To iNumElemIt) As String
            ReDim Preserve arrItemInicio(0 To iNumElemIt) As Variant
            ReDim Preserve arrItemFin(0 To iNumElemIt) As Variant
            ReDim Preserve arrItemUnidadPedido(0 To iNumElemIt) As String
            ReDim Preserve arrItemDestino(0 To iNumElemIt) As String
            ReDim Preserve arrItemFecEntrega(0 To iNumElemIt) As Variant
            '
            arrItemAnyo(iNumElemIt) = oLinea.Anyo
            arrItemProce(iNumElemIt) = oLinea.ProceCod
            arrItemGmn1(iNumElemIt) = oLinea.GMN1Proce
            arrItemId(iNumElemIt) = oLinea.Item
            arrItemIndice(iNumElemIt) = oLinea.indice
            arrItemProve(iNumElemIt) = oOrden.ProveCod
            arrItemAlm(iNumElemIt) = NullToStr(IIf(IsMissing(oLinea.Almacen), Null, oLinea.Almacen))
            arrItemCentro(iNumElemIt) = NullToStr(IIf(IsMissing(oLinea.Centro), Null, oLinea.Centro))
            arrItemPrecio(iNumElemIt) = oLinea.PrecioUP
            arrItemCantidad(iNumElemIt) = oLinea.CantidadPedida
            arrItemPres(iNumElemIt) = DameArrItemPres(oLinea)
            arrItemPedAbierto(iNumElemIt) = IIf(oLinea.PedidoAbierto, "True", "False")
            arrItemUnidadPedido(iNumElemIt) = oLinea.UnidadPedido
            arrItemDestino(iNumElemIt) = oLinea.CodDestino
            arrItemFecEntrega(iNumElemIt) = oLinea.FechaEntrega
            
            'Recoge las partidas presupuestarias (PRES5) a nivel de l�nea:
            If Not oLinea.Imputaciones Is Nothing Then
                For Each oImp In oLinea.Imputaciones
                    ReDim Preserve arrItemPres5Item(0 To iNumElemItPres5) As String
                    ReDim Preserve arrItemPres5Cod(0 To iNumElemItPres5) As String
                    ReDim Preserve arrItemPres5CentroSM(0 To iNumElemItPres5) As String
                    arrItemPres5Item(iNumElemItPres5) = oLinea.Anyo & oLinea.GMN1Proce & oLinea.ProceCod & oLinea.Item & oLinea.indice
                    arrItemPres5CentroSM(iNumElemItPres5) = oImp.CodCenCoste
                    If Not IsEmpty(oImp.PRES5) And Not IsNull(oImp.PRES5) And oImp.PRES5 <> "" Then
                        arrItemPres5Cod(iNumElemItPres5) = oImp.PRES5
                    End If
                    If Not IsEmpty(oImp.Pres1) And Not IsNull(oImp.Pres1) And oImp.Pres1 <> "" Then
                        arrItemPres5Cod(iNumElemItPres5) = arrItemPres5Cod(iNumElemItPres5) & "#" & oImp.Pres1
                    End If
                    If Not IsEmpty(oImp.Pres2) And Not IsNull(oImp.Pres2) And oImp.Pres2 <> "" Then
                        arrItemPres5Cod(iNumElemItPres5) = arrItemPres5Cod(iNumElemItPres5) & "#" & oImp.Pres2
                    End If
                    If Not IsEmpty(oImp.Pres3) And Not IsNull(oImp.Pres3) And oImp.Pres3 <> "" Then
                        arrItemPres5Cod(iNumElemItPres5) = arrItemPres5Cod(iNumElemItPres5) & "#" & oImp.Pres3
                    End If
                    If Not IsEmpty(oImp.Pres4) And Not IsNull(oImp.Pres4) And oImp.Pres4 <> "" Then
                        arrItemPres5Cod(iNumElemItPres5) = arrItemPres5Cod(iNumElemItPres5) & "#" & oImp.Pres4
                    End If
                    iNumElemItPres5 = iNumElemItPres5 + 1
                Next
            End If
            
            iNumElemIt = iNumElemIt + 1
            
            'Recoge los presupuestos de tipo 4 (nivel 1):
            If Not oLinea.presupuestos4 Is Nothing Then
                If oLinea.presupuestos4.Count <> 0 Then
                Dim oPres41 As Object
                For Each oPres41 In oLinea.presupuestos4
                    If oPres41.IDNivel = 1 Then
                        ReDim Preserve arrItemPres4Item(0 To iNumElemItPres4) As String
                        ReDim Preserve arrItemPres4Cod(0 To iNumElemItPres4) As String
                        ReDim Preserve arrItemPres4Porcen(0 To iNumElemItPres4) As Double
                        arrItemPres4Item(iNumElemItPres4) = oLinea.Anyo & oLinea.GMN1Proce & oLinea.ProceCod & oLinea.Item & oLinea.indice
                        arrItemPres4Cod(iNumElemItPres4) = oPres41.CodPRES1
                        arrItemPres4Porcen(iNumElemItPres4) = oPres41.Porcen
                        iNumElemItPres4 = iNumElemItPres4 + 1
                    End If
                Next
                
                End If
            End If
            
        Next

    Next
    
    ''Llamada a la funci�n ValidacionesERP para las validaciones de integraci�n.
    If ValidacionesERP(arrProceEmpresa(), arrProceOrgCompras(), arrItemAnyo(), arrItemProce(), arrItemGmn1(), _
    arrItemId(), arrItemIndice(), arrItemProve(), arrItemAlm(), arrItemCentro(), arrItemPrecio(), arrItemPres(), arrItemPedAbierto(), _
    arrItemInicio(), arrItemFin(), arrProceCodERP(), arrProceAtribOrden(), arrProceAtribId(), arrProceAtribValor(), _
    arrItemAtribItem(), arrItemAtribId(), arrItemAtribValor(), arrItemUnidadPedido(), arrItemDestino(), arrItemPres4Item(), arrItemPres4Cod(), arrItemPres4Porcen(), _
    arrProceViaPag(), arrItemCantidad(), arrProceTipoPedido(), arrItemPres5Item(), arrItemPres5Cod(), arrItemPres5CentroSM(), arrItemFecEntrega()) = False Then
        ''Sale sin emitir el pedido
        ValidacionERP = False
        Exit Function
    Else
        ValidacionERP = True
    End If
    
End Function

''' <summary>
''' Ver si pasa todas las validaciones mapper y para cada atributo si valida ok � ko
''' </summary>
''' <param name="sMapper">contiene el nombre de la mapper contra la que valida</param>
''' <param name="bParam2">para cada atributo si valida ok � ko</param>
''' <returns>Si pasa todas las validaciones mapper</returns>
''' <remarks>Llamada desde: ValidacionesERP ; Tiempo m�ximo: 0,2</remarks>
Private Function fBolComprobarAtributosERP(ByVal sMapper As String, ByRef bParam2() As Boolean) As Boolean

    fBolComprobarAtributosERP = True
    
    Dim oAtribs As CAtributos
    Dim oatrib  As CAtributo
    Dim oOrden As COrdenEntrega
    Dim oLinea As CLineaPedido
    Dim oMapper As Object
    Dim sInstalacion As String
    Dim strParam1, strParam2 As String
    Dim strMensaje, strTextoGrid As String, strTextoCentro As String

    Dim iParam2 As Long
    iParam2 = 0
    
    On Error Resume Next
    Set oMapper = CreateObject(sMapper & ".clsValidacion")
    On Error GoTo 0
    If Not oMapper Is Nothing Then
        For Each oOrden In g_oOrigen.g_oOrdenesTemporales
            For Each oLinea In oOrden.LineasPedido
                If Not oLinea.AtributosIntegracion Is Nothing Then
                    For Each oatrib In oLinea.AtributosIntegracion
                        bParam2(iParam2) = False ''Inicializa para cada atributo a false
                        If oatrib.Validacion_ERP = True Then
                            ''Validaciones Externas:
                            Select Case oatrib.Tipo
                            Case TipoString
                                strTextoGrid = NullToStr(oatrib.ValorText)
                            Case TipoNumerico
                                strTextoGrid = NullToStr(oatrib.ValorNum)
                            Case TipoFecha
                                strTextoGrid = NullToStr(oatrib.ValorFec)
                            Case TipoBoolean
                                strTextoGrid = NullToStr(oatrib.ValorBool)
'                            Case Else
'                                strTextoGrid = NullToStr(oatrib.ValorPond)
                            End Select
                            strTextoCentro = oLinea.Centro
                
                            If strTextoGrid <> "" Then
                                sInstalacion = oMapper.DevolverInstalacion()
                                Select Case sInstalacion
                                    Case "GESTAMP"
                                        strParam1 = strTextoCentro
                                    Case Else
                                        strParam1 = ""
                                End Select
                                strParam2 = ""
                                strMensaje = oMapper.fStrValidarAtributo(gInstancia, oatrib.Atrib, strTextoGrid, gParametrosInstalacion.gIdioma, strParam1, strParam2)
                                If strMensaje <> "" Then
                                    fBolComprobarAtributosERP = False
                                    basMensajes.MensajeOKOnly strMensaje, Exclamation
                                    Exit Function
                                End If
                                If strParam2 = "X" Then  'Gestamp: Es una orden de inversi�n
                                    bParam2(iParam2) = True
                                End If
                            End If
                        End If
                        iParam2 = iParam2 + 1
                    Next
                End If
            Next
        Next
    End If
    
    Set oAtribs = Nothing
    Set oatrib = Nothing
    Set oOrden = Nothing
    Set oLinea = Nothing
    Set oMapper = Nothing
    
End Function

'''<summary>Funci�n que realiza las validaciones espec�ficas de cada ERP (mediante llamada a la mapper correspondiente) antes de emitir el pedido</summary>
'''<param name="arrProceEmpresa">Array con la empresa para la que se emite el pedido</param>
'''<param name="arrProceOrgCompras">Array con la organizaci�n de compras para la que se emite el pedido</param>
'''<param name="arrItemAnyo">Array con el a�o del proceso (por linea de pedido)</param>
'''<param name="arrItemProce">Array con el c�digo del proceso (por linea de pedido)</param>
'''<param name="arrItemGmn1">Array con el c�digo del GMN1 del proceso (por linea de pedido)</param>
'''<param name="arrItemId">Array con el ID del item dentro del proceso (por linea de pedido)</param>
'''<param name="arrItemIndice"></param>
'''<param name="arrItemProve">Array con el proveedor asignado (por l�nea pedido)</param>
'''<param name="arrItemAlm">Array con el almac�n seleccionado (por l�nea pedido)</param>
'''<param name="arrItemCentro">Array con el centro seleccionado (por l�nea pedido)</param>
'''<param name="arrItemPrecio">Array con el precio de la l�nea (por l�nea pedido)</param>
'''<param name="arrItemPres">Array que indica si la l�nea tiene presupuestos asignados (true) o no (false) (por l�nea pedido)</param>
'''<param name="arrItemPedAbierto">Array que indica si es un pedido abierto (true) o no (false) (por l�nea pedido)</param>
'''<param name="arrItemInicio"></param>
'''<param name="arrItemFin"></param>
'''<param name="arrProceCodERP">Array con el c�digo del proveedor en el ERP</param>
'''<param name="arrProceAtribOrden>Array con el c�digo GS del proveedor para el que se emite la orden (por atributo de cabecera)</param>
'''<param name="arrProceAtribId">Array con los ID de los atributos (por atributo de cabecera)</param>
'''<param name="arrProceAtribValor">Array con los valores de los atributos (por atributo de cabecera)</param>
'''<param name="arrItemAtribItem">Array con lo oLinea.Anyo & oLinea.GMN1Proce & oLinea.ProceCod & oLinea.Item & oLinea.indice(por atributo de l�nea)</param>
'''<param name="arrItemAtribId">Array con los ID de los atributos (por atributo de l�nea)</param>
'''<param name="arrItemAtribValor">Array con los valores de los atributos (por atributo de l�nea)</param>
'''<param name="arrItemUnidadPedido">Array con el c�digo de la unidad de pedido (por l�nea pedido)</param>
'''<param name="arrItemCodDestino">Array con el c�digo del destino (por l�nea pedido)</param>
'''<param name="arrItemPres4Item">Array con lo oLinea.Anyo & oLinea.GMN1Proce & oLinea.ProceCod & oLinea.Item & oLinea.indice(por presupuesto de tipo 4, nivel 1)</param>
'''<param name="arrItemPres4Cod">Array con el c�digo del presupesto de tipo 4 y nivel 1</param>
'''<param name="arrItemPres4Pres">Array con el porcentaje asignado del presupesto de tipo 4 y nivel 1</param>
'''<param name="arrProceViaPag">Array con las v�as de pago del pedido</param>
'''<param name="arrItemCantidad">Array con las cantidades de cada l�nea de pedido</param>
'''<param name="arrProceTipoPedido">Array con el c�digo del tipo de pedido (por cada orden)</param>
'''<param name="arrItemPres5Item">Array con el ID del item para cada partida presupuestaria a nivel de item</param>
'''<param name="arrItemPres5Cod">Array con el c�digo de la partida presupuestaria de cada item (PRES0#PRES1#PRES2#PRES3#PRES4)</param>
'''<param name="arrItemPres5CentroSM">Array con el c�digo del CentroSM relacionado con cada partida</param>
'''<returns></returns>
'''<remarks>Llamada desde: FSGSClient.frmPedido.cmdEmitir_Click() Tiempo m�ximo: 0,4</remarks>
Private Function ValidacionesERP(ByRef arrProceEmpresa As Variant, ByRef arrProceOrgCompras As Variant, _
    ByRef arrItemAnyo() As Integer, ByRef arrItemProce() As Long, ByRef arrItemGmn1() As String, _
    ByRef arrItemId() As Long, ByRef arrItemIndice() As Variant, ByRef arrItemProve() As String, _
    ByRef arrItemAlm() As String, ByRef arrItemCentro() As String, ByRef arrItemPrecio() As Double, ByRef arrItemPres() As String, _
    ByRef arrItemPedAbierto() As String, ByRef arrItemInicio As Variant, ByRef arrItemFin As Variant, _
    ByRef arrProceCodERP() As String, ByRef arrProceAtribOrden() As String, ByRef arrProceAtribId() As Long, _
    ByRef arrProceAtribValor() As String, ByRef arrItemAtribItem() As String, ByRef arrItemAtribId() As Long, _
    ByRef arrItemAtribValor() As String, ByRef arrItemUnidadPedido() As String, ByRef arrItemCodDestino() As String, ByRef arrItemPres4Item() As String, ByRef arrItemPres4Cod() As String, ByRef arrItemPres4Porcen() As Double, ByRef arrProceViaPag() As String, _
    ByRef arrItemCantidad() As Double, ByRef arrProceTipoPedido() As String, ByRef arrItemPres5Item() As String, ByRef arrItemPres5Cod() As String, ByRef arrItemPres5CentroSM() As String, ByRef arrItemFecEntrega() As Variant) As Boolean
Dim Maper As Object
Dim sMapper As String
Dim iNumError As Integer
Dim auxERP As fsgsserver.CERPInt
Dim TextosGS As cTextosGS
Dim Modulo As Integer
Dim bParam2() As Boolean ''En Gestamp: Indica si la orden es de inversi�n o no
ReDim Preserve bParam2(0 To UBound(arrItemAtribValor)) As Boolean ''1 por cada atributo
  
Dim i As Integer
For i = 0 To UBound(arrItemAtribValor)
    bParam2(i) = False
Next

If gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) = True Then

    ValidacionesERP = False
    ''En primer lugar obtiene el nombre de la mapper:
    Set auxERP = oFSGSRaiz.Generar_CERPInt
    sMapper = auxERP.ObtenerMapper(arrProceOrgCompras(0), arrProceEmpresa(0))
    '--------------------------------------------------------------------------
    'Valida los atributos externos contra el ERP:
    If fBolComprobarAtributosERP(sMapper, bParam2()) = False Then
        Exit Function
    End If
    '-------------------------------------------------------------------------
    ''Continua con el resto de validaciones:
    If g_oOrigen.g_oOrdenesTemporales.Count = 0 Then Exit Function
    '
    On Error Resume Next
    Set Maper = CreateObject(sMapper & ".clsInPedidosDirectos")
    On Error GoTo 0

    If Not Maper Is Nothing Then
        On Error GoTo NoFnTratarLinea
        Dim sError As String
        If Not Maper.TratarLinea(basPublic.gInstancia, iNumError, sError _
        , arrItemAnyo, arrItemProce, arrItemGmn1, arrItemId, arrItemIndice, arrItemProve _
        , arrItemAlm, arrItemCentro, arrItemPrecio, arrItemPres, arrItemPedAbierto, arrItemInicio, arrItemFin _
        , arrProceEmpresa, arrProceOrgCompras, arrProceCodERP _
        , arrProceAtribOrden, arrProceAtribId, arrProceAtribValor _
        , arrItemAtribItem, arrItemAtribId, arrItemAtribValor, bParam2, arrItemUnidadPedido, arrItemCodDestino _
        , arrItemPres4Item, arrItemPres4Cod, arrItemPres4Porcen, arrProceViaPag, arrItemCantidad, arrProceTipoPedido _
        , arrItemPres5Item, arrItemPres5Cod, arrItemPres5CentroSM, arrItemFecEntrega) Then
        
            If iNumError = -1000 Then
                'bbdd mal
                Exit Function
            End If
            
            Set TextosGS = oFSGSRaiz.Generar_CTextosGS
                    
            Modulo = MapperModuloMensaje.PedidoDirecto
            
            Call basMensajes.MapperTextosGS(TextosGS.MensajeError(Modulo, iNumError, basPublic.gParametrosInstalacion.gIdioma, sError))
            
            Exit Function
        Else
           ValidacionesERP = True
        End If
    
                
NoFnTratarLinea:
    End If
    
    ValidacionesERP = True
Else ''Si no est� en sentido salida, no hay que validar
    ValidacionesERP = True
End If

Set Maper = Nothing
Set auxERP = Nothing
Set TextosGS = Nothing

End Function

''' <summary>Carga las variables de seguridad seg�n el usuario</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
Private Sub ConfigurarSeguridad()
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIRRestReceptorUsu)) Is Nothing) Then
        m_bRReceptorUsu = True
    End If
End Sub
''' <summary>Indica si se ha seleccionado alg�n presupuesto de tipo 4 en el pedido</summary>
''' <param name="oLinea"> Objeto de la l�nea de pedido </param>
''' <returns>Cadena con los valores "true" o "false" en funci�n de si existe o no presupuesto seleccionado en la l�nea</returns>
''' <remarks>Llamada desde:ValidacionERP</remarks>
Private Function DameArrItemPres(ByVal oLinea As CLineaPedido) As String
    DameArrItemPres = "False"
    
    On Error GoTo Error:

    If (oLinea.presupuestos4.Count > 0) Then
        DameArrItemPres = "True"
    End If
    
    Exit Function

Error:
    'Si tiene por parametrizacion solo 3 niveles y los tiene vacios llegara a la pregunta 4 que incluye el count
    'de algo nothing, luego erroneo pero ha ido bien. Piya false con raz�n
End Function

