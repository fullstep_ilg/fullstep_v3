Attribute VB_Name = "basPedidos"
Option Explicit

Private Function EliminarMailsDuplicados(ByVal sMails1 As String, ByVal sMails2 As String) As String
Dim ListaMail() As String
Dim iMail As Integer

sMails1 = Trim(sMails1)
sMails2 = Trim(sMails2)
If sMails2 <> "" And Right(sMails2, 1) <> ";" Then
    sMails2 = sMails2 & ";"
End If
If sMails1 <> "" And Right(sMails1, 1) = ";" Then
    sMails1 = Left(sMails1, Len(sMails1) - 1)
End If

ListaMail = Split(sMails1, ";")

For iMail = 0 To UBound(ListaMail)
    'Ponemos el ; para que reemplace todo el mail bien
    If ListaMail(iMail) <> "" And Right(ListaMail(iMail), 1) <> ";" Then
        ListaMail(iMail) = ListaMail(iMail) & ";"
    End If
    sMails2 = Replace(sMails2, ListaMail(iMail), "")
Next
If sMails1 <> "" And Right(sMails1, 1) <> ";" Then
    sMails1 = sMails1 & ";"
End If
EliminarMailsDuplicados = sMails2 & sMails1

End Function

'oOrdenes puede ser CordenEntrega o COrdenesEntrega en funcion de donde venga
Public Function RellenaPedNotif_CCO(ByVal sMails As String, ByVal oOrdenes As COrdenesEntrega, Optional ByVal oOrdenUnica As COrdenEntrega = Nothing) As String
Dim sListaMail As String
Dim vlbComprador As Boolean
Dim vlbPeticionario As Boolean
Dim oOrden As COrdenEntrega
Dim oLinea As CLineaPedido
If gParametrosInstalacion.gbPedNotifMailCCActivado Then
    'Sacamos la lista de mails de la configuraci�n del usuario en caso de que est� configurado
    If gParametrosInstalacion.gsPedNotifMailCC <> "" Then
        sListaMail = gParametrosInstalacion.gsPedNotifMailCC
    End If
    If gParametrosInstalacion.giPedNotifMailCCPeti = 1 Then
        vlbPeticionario = True
    Else
        vlbPeticionario = False
    End If
    If gParametrosInstalacion.giPedNotifMailCCComp = 1 Then
        vlbComprador = True
    Else
        vlbComprador = False
    End If
Else
    'Si no est� marcada la activaci�n a nivel de usuario sacamos la lista de mails de la configuraci�n general
    'en caso de que est� configurado
    If gParametrosGenerales.gsPedNotifMailCC <> "" Then
        sListaMail = gParametrosGenerales.gsPedNotifMailCC
    End If
    If gParametrosGenerales.giPedNotifMailCCPeti = 1 Then
        vlbPeticionario = True
    Else
        vlbPeticionario = False
    End If
    If gParametrosGenerales.giPedNotifMailCCComp = 1 Then
        vlbComprador = True
    Else
        vlbComprador = False
    End If
End If

'En caso de que est� configurado la lista de copias y no se haya puesto el ; al final, se lo ponemos ahora
If sListaMail <> "" Then
    If sListaMail <> "" And Right(sListaMail, 1) <> ";" Then
        sListaMail = sListaMail & ";"
    End If
    If sMails <> "" And Right(sMails, 1) <> ";" Then
        sMails = sMails & ";"
    End If
    'Miramos que en la lista no se repitan los mails que ya estan en CC
    sListaMail = EliminarMailsDuplicados(sListaMail, sMails)
End If

'Si se ha configurado que se a�adan los peticionarios
If vlbPeticionario Then
    If oOrdenes Is Nothing Then
        'oOrdenes = cOrdenEntrega
        For Each oLinea In oOrdenUnica.LineasPedido
            If oLinea.Solicitud & "" <> "" Then
                sListaMail = EliminarMailsDuplicados(oLinea.SacarMailPeticionarios(NullToDbl0(oLinea.Solicitud)), sListaMail)
            End If
        Next
    Else
        'oOrdenes = cOrdenesEntrega
        For Each oOrden In oOrdenes
            For Each oLinea In oOrden.LineasPedido
                If oLinea.Solicitud & "" <> "" Then
                    sListaMail = EliminarMailsDuplicados(oLinea.SacarMailPeticionarios(NullToDbl0(oLinea.Solicitud)), sListaMail)
                End If
            Next
        Next
    End If
End If
If vlbComprador Then
    If oOrdenes Is Nothing Then
        'oOrdenes = cOrdenEntrega
        For Each oLinea In oOrdenUnica.LineasPedido
            sListaMail = EliminarMailsDuplicados(oLinea.SacarMailComprador(NullToDbl0(oLinea.Anyo), NullToStr(oLinea.GMN1Proce), NullToDbl0(oLinea.ProceCod)), sListaMail)
        Next
    Else
        'oOrdenes = cOrdenesEntrega
        For Each oOrden In oOrdenes
            For Each oLinea In oOrden.LineasPedido
                sListaMail = EliminarMailsDuplicados(oLinea.SacarMailComprador(NullToDbl0(oLinea.Anyo), NullToStr(oLinea.GMN1Proce), NullToDbl0(oLinea.ProceCod)), sListaMail)
            Next
        Next
    End If
End If
If Right(sListaMail, 1) = ";" Then
    'Quitamos el separador final Incidencia 53794
    sListaMail = Left(sListaMail, Len(sListaMail) - 1)
End If
RellenaPedNotif_CCO = sListaMail
End Function


Public Sub pgSacar_Cod_Den_ProveERP(sId As String, sCod As String, sDen As String, lEmpresa As Long)
Dim oProvesERPs As CProveERPs
Dim oProvesERP As CProveERP
Dim sNIFProveFact As String
Dim oProveFacts As CProveedores
Dim oProveFact As CProveedor

Set oProvesERPs = oFSGSRaiz.Generar_CProveERPs
oProvesERPs.CargarProveedoresERP lEmpresa, , sId, , , False
For Each oProvesERP In oProvesERPs
    sNIFProveFact = oProvesERP.NIF
Next
Set oProveFacts = oProvesERPs.ProveedoresVinculados(sNIFProveFact)
For Each oProveFact In oProveFacts
    sCod = oProveFact.Cod
    sDen = oProveFact.Den
Next
Set oProvesERPs = Nothing
Set oProvesERP = Nothing
Set oProveFact = Nothing
Set oProveFacts = Nothing
End Sub


''' <summary>Carga una l�nea de pedido en la orden</summary>
''' <param name="oOrden">Orden de entrega</param>
''' <param name="adoFields">Datos de la l�nea</param>
''' <param name="sCentro">Centro</param>
''' <param name="sAlmacen">Almacen</param>
''' <param name="vIndice">Indice</param>
''' <param name="oPlanesEntrega">Planes de entrega</param>
''' <param name="sCodErp">Cod. ERP</param>
''' <param name="Id l�nea">Indice</param>
''' <param name="bPedidoExistente">Indica si la l�nea se est� a�adiendo a un pedido ya existente</param>
''' <returns>L�nea de pedido introducida</returns>
''' <remarks>Llamada desde: ProcesoSeleccionado</remarks>
''' <revision>LTG 11/04/2012</revision>
Public Function CargarLineaEnOrden(ByRef oOrden As COrdenEntrega, ByRef adoFields As ADODB.Fields, Optional ByVal iAnyo As Integer, Optional ByVal sGMN1 As String, Optional ByVal lProceCod As Long, _
        Optional ByVal sCentro As String, Optional ByVal sAlmacen As String, Optional ByVal vIndice As Variant, Optional oPlanesEntrega As CPlanesEntrega, Optional ByVal sCodErp As String, _
        Optional ByVal lIdLinea As Variant, Optional ByVal bPedidoExistente As Boolean = False, Optional ByVal Usuario As String = "", Optional ByVal iTipoPedido As Integer = 0) As CLineaPedido
    Dim oLinea As CLineaPedido
    Dim sCodExt As String
    Dim bHom As Boolean
    Dim dblCantPed As Double
    Dim sCodDestino As String
    Dim iNumDecimales As Integer
    Dim oatrib As CAtributoOfertado
    Dim oOrdenes As COrdenesEntrega
    Dim oAtributosItem As CAtributos
    Dim oAtributosItemAnt As CAtributos

    If IsNull(adoFields("COD_EXT").Value) Then
        sCodExt = ""
    Else
        sCodExt = adoFields("COD_EXT").Value
    End If
    
    If IsNull(adoFields("HOM").Value) Then
        bHom = 0
    Else
        bHom = (adoFields("HOM").Value = 1)
    End If
    
    If IsNull(adoFields("CANT_PED").Value) Then
        dblCantPed = 0
    Else
        dblCantPed = adoFields("CANT_PED").Value
    End If
    
    If sCodDestino = "" Then sCodDestino = NullToStr(adoFields("DEST").Value)
        
    Set oLinea = oOrden.LineasPedido.Add(lIdLinea, , , , iAnyo, adoFields("GMN1").Value _
                                               , adoFields("GMN2").Value, adoFields("GMN3").Value _
                                               , adoFields("GMN4").Value, lProceCod, adoFields("ID").Value _
                                               , adoFields("PROVE").Value, NullToStr(adoFields("ART").Value) _
                                               , NullToStr(sCodExt), NullToStr(adoFields("ARTICULO").Value), bHom _
                                               , NullToStr(adoFields("UNI").Value), NullToStr(adoFields("UNI").Value) _
                                               , , CDbl(adoFields("PRECIO_MONCEN")), 0, adoFields("CANT_ADJ").Value _
                                               , NullToDbl0(adoFields("CANT_ADJ").Value), dblCantPed _
                                               , sCodDestino, adoFields("FECINI").Value, , , , , , , , , , , , , , IIf(IsMissing(vIndice), Null, vIndice), , _
                                               , CDbl(adoFields("PRECIO_MONCEN").Value), NullToStr(adoFields("DEN").Value) _
                                               , , adoFields("SOLICIT").Value, , , , , , , , , , sCentro, sAlmacen, , sGMN1)
                                
    'Numeraci�n de l�neas
    If bPedidoExistente Then
        NumerarNuevaLineaPedido oOrden, oLinea
    Else
        NumerarLineasDePedido oOrden, oLinea, True
    End If
                            
    oLinea.MonedaProceso = adoFields("MONEDAPROCESO").Value
    oLinea.MonedaOferta = adoFields("MONEDA").Value
    oLinea.CambioMonOferta = adoFields("CAMBIO").Value
    If IsMissing(vIndice) And gParametrosGenerales.gbOblPedidosDistCompra Then oLinea.Porcen = NullToDbl0(adoFields("PORCEN").Value)
    'A los art�culos no codificados no aplica el concepto, almacen, recepcion.
    If adoFields("ART").Value <> "" Then
        oLinea.Concepto = adoFields("CONCEPTO").Value
        oLinea.Almacenable = adoFields("ALMACENAR").Value
        oLinea.Recepcion = adoFields("RECEPCIONAR").Value
    End If
    oLinea.FechaFin = adoFields("FECFIN").Value
    oLinea.FechaIni = adoFields("FECINI").Value
    oLinea.ProveCodAdjudicado = adoFields("PROVE").Value
    oLinea.ProveDenAdjudicado = NullToStr(adoFields("DEN").Value)
    oLinea.grupoCod = adoFields("GRUPO_COD").Value
    oLinea.GrupoID = adoFields("GRUPO_ID").Value
    oLinea.UsarEscalados = SQLBinaryToBoolean(adoFields("ESCALADOS").Value)
    oLinea.NumEscAdjudicados = adoFields("NUMESCADJ").Value
    If NullToDbl0(oLinea.NumEscAdjudicados) > 1 Then oLinea.CargarEscAdjParaPedido
    If oLinea.UsarEscalados And oLinea.NumEscAdjudicados = 1 Then oLinea.IdEscalado = adoFields("ESC").Value
    oLinea.DesvioRecep = 0
    oLinea.IdLineaSolicit = adoFields("LINEA_SOLICIT").Value
    oLinea.IdCampoSolicit = adoFields("CAMPO_SOLICIT").Value
    oLinea.tipoRecepcion = NullToDbl0(adoFields("TIPORECEPCION").Value)
    
     'Inicializamos valores
    If CDec(oLinea.FC) = 0 Then oLinea.FC = 1
    If Not oLinea.UsarEscalados Or (oLinea.UsarEscalados And oLinea.NumEscAdjudicados = 1) Then
        If CDec(oLinea.PrecioUC) <> 0 And CDec(oLinea.PrecioUP) = 0 Then
            oLinea.PrecioUP = CDec(oLinea.PrecioUC * oLinea.FC)
        End If
        
        If oLinea.CantidadPedida = 0 Then
            If Not IsNull(oLinea.CantidadAdj) Then
                If oLinea.CantidadAdj < 0 Then
                    If oLinea.CantidadAdj > oLinea.CantidadPedidaTotal Then
                        oLinea.CantidadPedida = 0
                    Else
                        oLinea.CantidadPedida = oLinea.CantidadAdj - oLinea.CantidadPedidaTotal
                    End If
                Else
                    If CDec(oLinea.CantidadAdj - oLinea.CantidadPedidaTotal) < 0 Then
                        oLinea.CantidadPedida = 0
                    Else
                        oLinea.CantidadPedida = oLinea.CantidadAdj - oLinea.CantidadPedidaTotal
                    End If
                End If
            Else
                oLinea.CantidadPedida = 0
            End If
        End If
    End If
    
    'Planes de entrega
    If Not oPlanesEntrega Is Nothing Then AplicarPlanConfigurado oOrden, oLinea, True, oPlanesEntrega, oLinea.CantidadPedida, oLinea.RecepAutom
    
    If Not IsNull(oLinea.Item) Then
        oLinea.CargarPresupuestosDelItem
        oLinea.CargarAtributosProceso Usuario, IIf(sCodErp = "", 0, sCodErp), iTipoPedido
    End If
    
    '3276
    'Carga de los atributos de integracion de LINEA
    If gParametrosIntegracion.gaExportar(EntidadIntegracion.PED_directo) Then
        If sCodErp <> "" Then
            Set oOrdenes = oFSGSRaiz.Generar_COrdenesEntrega
            oOrdenes.AddOrdenEntrega oOrden
            
            CargarAtribIntegracionItem oOrdenes, oAtributosItem, oAtributosItemAnt, sCodErp, , Usuario
        End If
    End If
    
    If Not IsNull(oLinea.Item) Then oLinea.CargarCostesDescuentosProceso
    
    'Tratamos el numero maximo de decimales de la cantidad del pedido
    iNumDecimales = ValidarNumeroMaximoDecimales(oLinea.UnidadCompra, oLinea.CantidadPedida)
    If iNumDecimales <> -1 Then
         oLinea.CantidadPedida = Round(oLinea.CantidadPedida, iNumDecimales)
    End If
    
    'En AtributosLineas mantengo los atributos distintos de las lineas
    If Not oLinea.ATRIBUTOS Is Nothing Then
        If oOrden.AtributosLineas Is Nothing Then Set oOrden.AtributosLineas = oFSGSRaiz.Generar_CAtributos
        For Each oatrib In oLinea.ATRIBUTOS
            If oOrden.AtributosLineas.Item(CStr(oatrib.idAtribProce)) Is Nothing Then
                oOrden.AtributosLineas.Add oatrib.idAtribProce, oatrib.objeto.Cod, oatrib.objeto.Den, oatrib.objeto.Tipo, , , , , , , , , , , , , , , , oatrib.objeto.TipoIntroduccion, oatrib.objeto.Minimo, oatrib.objeto.Maximo, , oatrib.objeto.interno, , , , , , , oatrib.objeto.ListaPonderacion, , , , , , , , , , oatrib.Validacion_ERP, , , , , , oatrib.objeto.pedido
            End If
        Next
    End If
        
    Set CargarLineaEnOrden = oLinea
    Set oLinea = Nothing
End Function

''' <summary>Carga los datos de partida y centro de coste para una l�nea de pedido</summary>
''' <param name="oLinea">L�nea de pedido</param>
''' <param name="oProceso">Proceso</param>
''' <param name="oParamSM">Par�metros SM</param>
''' <param name="bImputaPedido">Si hay imputaciones en el pedido</param>
''' <param name="bCabecera">Imputaciones a nivel de cabecera</param>
''' <param name="lIdEmpresa">Id empresa</param>
''' <param name="sCenCoste">Centro de coste</param>
''' <param name="sContrato">Contrato</param>
''' <remarks>Llamada desde: AgregarLineaAOrden</remarks>
''' <returns>Booleano indicando si se han cargado los datos de imputaciones</returns>

Public Function CargarDatosImputaciones(ByRef oLinea As CLineaPedido, ByRef oProceso As cProceso, ByVal lIdEmpresa As Long, _
        Optional ByRef bCabecera As Boolean, Optional ByRef sCenCoste As String, Optional ByRef sContrato As String, Optional ByRef oParamSM As CParametroSM) As Boolean
    Dim oGrupo As CGrupo
    Dim lID As Long
    Dim oParam As CParametroSM
    Dim bImputaPedido  As Boolean
    Dim bCab As Boolean
    Dim sCC As String
    Dim sContr As String
    
    CargarDatosImputaciones = False
    
    For Each oParam In g_oParametrosSM
        If oParam.ImpPedido <> TipoImputacionEmiPed.NoImputa Then
            bImputaPedido = True
            Exit For
        End If
    Next
    Set oParam = Nothing
    
    If Not IsMissing(bCabecera) Then
        bCab = bCabecera
    Else
        bCab = False
    End If
    'SM CARGA DE LOS DATOS DE PARTIDA Y CENTROCOSTE
    If gParametrosGenerales.gsAccesoFSSM = AccesoFSSM Then
        If Not g_oParametrosSM Is Nothing And bImputaPedido Then
            If oLinea.Solicitud <> 0 Then
                'obtener datos
                If Not oProceso.Grupos Is Nothing Then
                    For Each oGrupo In oProceso.Grupos
                        lID = oLinea.Item
                        If Not oGrupo.Items.Item(lID) Is Nothing Then
                            If Not oGrupo.Items.Item(lID).Activo Is Nothing Then
                                Set oLinea.Activo = oGrupo.Items.Item(lID).Activo
                            End If
                            If Not oGrupo.Items.Item(lID).Almacen Is Nothing Then
                                oLinea.Almacen = oGrupo.Items.Item(lID).Almacen.Id
                            End If
                            If Not oGrupo.Items.Item(lID).Imputaciones Is Nothing Then
                                Set oLinea.Imputaciones = oGrupo.Items.Item(lID).Imputaciones
                                If Not bCab Then
                                    If Not g_oParametrosSM Is Nothing Then
                                        For Each oParam In g_oParametrosSM
                                            If oParam.ImpPedido <> 0 Then
                                                If Not oLinea.Imputaciones Is Nothing Then
                                                    If Not oLinea.Imputaciones.Item(oParam.PRES5) Is Nothing Then
                                                        If oLinea.Imputaciones.Item(oParam.PRES5).Empresa = lIdEmpresa Then
                                                            If oLinea.Imputaciones.Item(oParam.PRES5).CC4 <> "" Then
                                                                sCC = oLinea.Imputaciones.Item(oParam.PRES5).CC4
                                                            Else
                                                                If oLinea.Imputaciones.Item(oParam.PRES5).CC3 <> "" Then
                                                                    sCC = oLinea.Imputaciones.Item(oParam.PRES5).CC3
                                                                Else
                                                                    If oLinea.Imputaciones.Item(oParam.PRES5).CC2 <> "" Then
                                                                        sCC = oLinea.Imputaciones.Item(oParam.PRES5).CC2
                                                                    Else
                                                                        sCC = oLinea.Imputaciones.Item(oParam.PRES5).CC1
                                                                    End If
                                                                End If
                                                            End If
                                                                                                                                                                                                           
                                                            sContr = oLinea.Imputaciones.Item(oParam.PRES5).Pres1
                                                            If oLinea.Imputaciones.Item(oParam.PRES5).Pres2 <> "" Then
                                                                sContr = sContr & " - " & oLinea.Imputaciones.Item(oParam.PRES5).Pres2
                                                                If oLinea.Imputaciones.Item(oParam.PRES5).Pres3 <> "" Then
                                                                    sContr = sContr & " - " & oLinea.Imputaciones.Item(oParam.PRES5).Pres3
                                                                    If oLinea.Imputaciones.Item(oParam.PRES5).Pres4 <> "" Then
                                                                        sContr = sContr & " - " & oLinea.Imputaciones.Item(oParam.PRES5).Pres4
                                                                    End If
                                                                End If
                                                            End If
                                                             
                                                            bCab = True
                                                             
                                                            Exit For
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        Next
                                         
                                        CargarDatosImputaciones = True
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        End If
    End If
    
    If Not IsMissing(bCabecera) Then bCabecera = bCab
    If Not IsMissing(sCenCoste) Then sCenCoste = sCC
    If Not IsMissing(sContrato) Then sContrato = sContr
    If Not IsMissing(oParamSM) Then Set oParamSM = oParam
    Set oGrupo = Nothing
    Set oParam = Nothing
End Function

''' <summary>
''' Asigna n�mero de l�nea cada vez que �stas van siendo a�adidas
''' </summary>
''' <param name="Orden">Orden de entrega</param>
''' <param name="Linea">L�nea de pedido</param>
''' <remarks>Llamada desde: sdbgPedidos_Change/CargarLineaEnOrden</remarks>
Public Sub NumerarLineasDePedido(ByVal Orden As COrdenEntrega, ByVal Linea As CLineaPedido, ByVal HaSidoEditada As Boolean)
    Dim i As Integer
    Dim NumLinMayor As Integer
    
    NumLinMayor = BuscarNUMSuperior(Orden)
    
    'Si s�lo hab�a una l�nea (el Count es 2, la que hab�a y la nueva a la que hay que asignar la numeraci�n)
    If Orden.LineasPedido.Count = 2 Then
        Linea.Num = (2 * Orden.LineasPedido.Item(1).Num)
    Else
        'N�Lineas <= 99:          Numeraci�n de 10 en 10
        '-----------------------------------------------
        If Orden.LineasPedido.Count > 0 And Orden.LineasPedido.Count <= 99 Then
            If HaSidoEditada Then
                If Orden.LineasPedido.Count > 1 Then
                    If (NumLinMayor + 10) > 999 And Orden.LineasPedido.Count < 999 Then
                        'Aun hay huecos para rellenar. Preparar el NUM de l�nea para que cuando se compruebe que es 1000, se deje el campo vacio
                        Linea.Num = 1000
                    Else
                        Linea.Num = NumLinMayor + 10
                    End If
                Else
                    'Entra aqui si ha sido editada. Primera l�nea
                    Linea.Num = 10
                End If
            Else
                Linea.Num = Orden.LineasPedido.Count * 10
            End If
        Else
        'N�Lineas > 99 y <= 200 : Numeraci�n de 5 en 5
        '---------------------------------------------
            If Orden.LineasPedido.Count > 99 And Orden.LineasPedido.Count <= 200 Then
            
                If HaSidoEditada Then
                    If (NumLinMayor + 5) > 999 And Orden.LineasPedido.Count < 999 Then
                        Linea.Num = 1000
                    Else
                        Linea.Num = NumLinMayor + 5
                    End If
                Else
                    'Cuando llegue a la l�nea 100, se reordenan las numeradas hasta ese momento
                    If Orden.LineasPedido.Count = 100 Then
                        For i = 1 To Orden.LineasPedido.Count
                            '1-5-10-15...., no 5-10-15...
                            If i = 1 Then
                                Orden.LineasPedido.Item(i).Num = i
                            Else
                                If i = 2 Then
                                    Orden.LineasPedido.Item(i).Num = Orden.LineasPedido.Item(i - 1).Num + 4
                                Else
                                    Orden.LineasPedido.Item(i).Num = Orden.LineasPedido.Item(i - 1).Num + 5
                                End If
                            End If
                        Next
                    Else
                        Linea.Num = Orden.LineasPedido.Count * 5
                    End If
                End If
                    
            Else
            'N�Lineas > 200 y <= 499: Numeraci�n de 2 en 2
            '---------------------------------------------
                If Orden.LineasPedido.Count > 200 And Orden.LineasPedido.Count <= 499 Then
                    If HaSidoEditada Then
                        If (NumLinMayor + 2) > 999 And Orden.LineasPedido.Count < 999 Then
                            Linea.Num = 1000
                        Else
                            Linea.Num = NumLinMayor + 2
                        End If
                    Else
                        'Cuando llegue a la l�nea 210, se reordenan las numeradas hasta ese momento
                        If Orden.LineasPedido.Count = 210 Then
                            For i = 1 To Orden.LineasPedido.Count
                                If i = 1 Then Orden.LineasPedido.Item(i).Num = i Else Orden.LineasPedido.Item(i).Num = Orden.LineasPedido.Item(i - 1).Num + 2
                            Next
                        Else
                            'Se coge la numeraci�n de la l�nea anterior y se le suma 2
                            Linea.Num = Orden.LineasPedido.Item(Orden.LineasPedido.Count - 1).Num + 2
                        End If
                    End If
                
                Else
                'N�Lineas > 499 y <= 999: Numeraci�n de 1 en 1 (como hasta ahora)
                '----------------------------------------------------------------
                    If Orden.LineasPedido.Count > 499 And Orden.LineasPedido.Count <= 999 Then
                        If HaSidoEditada Then
                            If (NumLinMayor + 1) > 999 And Orden.LineasPedido.Count < 999 Then
                                Linea.Num = 1000
                            Else
                                Linea.Num = NumLinMayor + 1
                            End If
                        Else
                            'Cuando llegue a la l�nea 500, se reordenan las numeradas hasta ese momento
                            If Orden.LineasPedido.Count = 500 Then
                                For i = 1 To Orden.LineasPedido.Count
                                    Orden.LineasPedido.Item(i).Num = i
                                Next
                            Else
                                Linea.Num = Orden.LineasPedido.Count
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
End Sub

''' <summary>
''' Asigna n�mero de l�nea cada vez que �stas van siendo a�adidas
''' </summary>
''' <param name="Orden">Orden de entrega</param>
''' <param name="Linea">L�nea de pedido</param>
''' <remarks>Llamada desde: sdbgPedidos_Change/CargarLineaEnOrden</remarks>
Public Sub NumerarNuevaLineaPedido(ByVal Orden As COrdenEntrega, ByVal Linea As CLineaPedido)
    Dim oLinea As CLineaPedido
    Dim iUltNum As Integer
    Dim iDif As Integer
    
    iUltNum = Orden.DameUltimoNum_BajaLog()
    iDif = 10
    
    'Si s�lo hab�a una l�nea (el Count es 2, la que hab�a y la nueva a la que hay que asignar la numeraci�n)
    If (Orden.LineasPedido.Count = 2) And (iUltNum = 0) Then
        Linea.Num = (2 * Orden.LineasPedido.Item(1).Num)
    Else
        For Each oLinea In Orden.LineasPedido
            If oLinea.Num > iUltNum Then
                iDif = oLinea.Num - iUltNum
                iUltNum = oLinea.Num
            End If
        Next
    
        If (iUltNum + iDif) <= 9999 Then
            Linea.Num = iUltNum + iDif
        Else
            Linea.Num = 10000
        End If
    End If
End Sub

''' <summary>
''' Localiza cual es el n�mero de l�nea mayor entre todas las l�neas a�adidas
''' </summary>
''' <param name="Orden">Orden de entrega</param>
''' <remarks>Llamada desde: NumerarLineasDePedido </remarks>
Private Function BuscarNUMSuperior(ByVal Orden As COrdenEntrega) As Integer
    Dim oLinea As CLineaPedido
    Dim NumTemporal As Integer

    NumTemporal = Orden.LineasPedido.Item(1).Num

    For Each oLinea In Orden.LineasPedido
        If oLinea.Num > NumTemporal Then NumTemporal = oLinea.Num
    Next

    BuscarNUMSuperior = NumTemporal
End Function

''' <summary>
''' Se acaba de chequear "Aplicar a todas las lineas" -> aplica los planes a todas las lineas del pedido.
''' Se acaba de deschequear "Aplicar a todas las lineas" -> actualiza la orden con este cambio
''' </summary>
''' <param name="ATodos">"Aplicar a todas las lineas" si/no</param>
''' <param name="oPlanesConfigurado">Planes configurados</param>
''' <param name="dCantLineaConfig">Cantidad pedida de la linea de pedido a la q pertenecen los planes</param>
''' <param name="RecepAutom">"Recepcion automatica" si/no</param>
''' <param name="HaCambiadoCantidadPedida">Si hay cambio en la cantidad pedida en esta misma pantalla tras configurar planes, estos planes deben ser recalculados</param>
''' <param name="bPubPortal">Indica la publicaci�n en el portal de los planes de entrega</param>
''' <param name="dImporteLineaConfig">Importe pedido de la linea de pedido a la q pertenecen los planes</param>
''' <param name="HaCambiadoImportePedido">Si hay cambio en el importe pedido en esta misma pantalla tras configurar planes, estos planes deben ser recalculados</param>
''' <remarks>Llamada desde: frmPlanesEntrega/chkAplicarTodos_Click; Tiempo m�ximo: 0,5sg</remarks>
Public Sub AplicarPlanConfigurado(ByVal oOrden As COrdenEntrega, ByVal oLinea As CLineaPedido, ByVal ATodos As Boolean, ByVal oPlanesConfigurado As CPlanesEntrega, ByVal dCantLineaConfig As Double, _
        Optional ByVal RecepAutom As Boolean, Optional ByVal HaCambiadoCantidadPedida As Boolean = False, Optional ByVal bPubPortal As Boolean = False, _
        Optional ByVal dImporteLineaConfig As Double, Optional ByVal RecepcionPorImporte As Boolean = False, Optional ByVal HaCambiadoImportePedido As Boolean = False)
    Dim oLineaPedido As CLineaPedido
    Dim oPlanConfigurado As CPlanEntrega
    Dim dCantPlanConfig As Double
    Dim dImportePlanConfig As Double
    Dim dPorcentajeAConfig As Double

    Screen.MousePointer = vbHourglass

    If Not HaCambiadoCantidadPedida Then oOrden.AplicarTodosPlanEntrega = ATodos
    If Not HaCambiadoImportePedido Then oOrden.AplicarTodosPlanEntrega = ATodos
        
    dCantPlanConfig = 0
    dImportePlanConfig = 0
    
    If RecepcionPorImporte Then
        For Each oPlanConfigurado In oPlanesConfigurado
            dImportePlanConfig = dImportePlanConfig + oPlanConfigurado.importe
        Next
        If dImporteLineaConfig = 0 Then
            dPorcentajeAConfig = 1
        Else
            dPorcentajeAConfig = dImportePlanConfig / dImporteLineaConfig
        End If
    Else
        For Each oPlanConfigurado In oPlanesConfigurado
            dCantPlanConfig = dCantPlanConfig + oPlanConfigurado.Cantidad
        Next
        If dCantLineaConfig = 0 Then
            dPorcentajeAConfig = 1
        Else
            dPorcentajeAConfig = dCantPlanConfig / dCantLineaConfig
        End If
    End If

    If ATodos Then
        For Each oLineaPedido In oOrden.LineasPedido
            If ((RecepcionPorImporte = False And (Not HaCambiadoCantidadPedida)) Or (RecepcionPorImporte = True And (Not HaCambiadoImportePedido))) Then
                oLineaPedido.RecepAutom = RecepAutom
                oLineaPedido.PlanesPubPortal = bPubPortal
                oLineaPedido.FechaEntrega = Null
                oLineaPedido.Obligat = False
            End If
        
            If RecepcionPorImporte Then
                If Not (oLineaPedido.Item = oLinea.Item) Or HaCambiadoImportePedido Then
                    oLineaPedido.HayPlanes = (oPlanesConfigurado.Count > 0)
                    Set oLineaPedido.Planes = oPlanesConfigurado.RecalcularCantidadesPlan(oLineaPedido, dImportePlanConfig, dPorcentajeAConfig, RecepcionPorImporte)
                End If
            Else
                If Not (oLineaPedido.Item = oLinea.Item) Or HaCambiadoCantidadPedida Then
                    oLineaPedido.HayPlanes = (oPlanesConfigurado.Count > 0)
                    Set oLineaPedido.Planes = oPlanesConfigurado.RecalcularCantidadesPlan(oLineaPedido, dCantPlanConfig, dPorcentajeAConfig)
                End If
            End If
            
        Next
    Else
        oLinea.RecepAutom = RecepAutom
        If RecepcionPorImporte Then
            Set oLinea.Planes = oPlanesConfigurado.RecalcularCantidadesPlan(oLinea, dImportePlanConfig, dPorcentajeAConfig, RecepcionPorImporte)
        Else
            Set oLinea.Planes = oPlanesConfigurado.RecalcularCantidadesPlan(oLinea, dCantPlanConfig, dPorcentajeAConfig)
        End If
    End If
    
    Screen.MousePointer = vbNormal
End Sub

''' <summary>
''' Carga los de atributos de integracion de LINEA marcados para integracion de pedidos en la ORDEN (para el ERP)
''' </summary>
''' <remarks>Llamada desde: Load del form  </remarks>
''' <remarks>Tiempo m�ximo: 0 </remarks>
Public Sub CargarAtribIntegracionItem(ByVal oOrdenes As COrdenesEntrega, ByRef oAtributosItem As CAtributos, ByRef oAtributosItemAnt As CAtributos, ByVal iCodErp As Integer, Optional ByVal bLineaAnyadida As Boolean _
    , Optional ByVal Usuario As String = "")
    Dim oatrib As CAtributo
    Dim oAtribLin As CAtributo
    Dim oLinea As CLineaPedido
    Dim i As Integer
    Dim bInsertarAtribInt As Boolean
    Dim bAnyadir As Boolean
    Dim bCargar As Boolean
    Dim oOrden As COrdenEntrega
    
    bCargar = True
    If Not oAtributosItem Is Nothing Then
        If oAtributosItem.Count > 0 And (IsMissing(bLineaAnyadida) = True Or bLineaAnyadida = False) Then bCargar = False
    End If
    
    If bCargar Then
        If oAtributosItemAnt Is Nothing Then Set oAtributosItemAnt = oFSGSRaiz.Generar_CAtributos
        Set oAtributosItem = oFSGSRaiz.Generar_CAtributos
        For Each oOrden In oOrdenes
            If Not oOrden.TipoDePedido Is Nothing Then
                oAtributosItem.CargarAtributosIntegracionItemProceso iCodErp, TipoAmbitoProceso.AmbItem, oOrden.TipoDePedido.Id, Usuario
            Else
                oAtributosItem.CargarAtributosIntegracionItemProceso iCodErp, TipoAmbitoProceso.AmbItem, , Usuario
            End If
            Exit For
        Next
    End If
    
    If oAtributosItem Is Nothing Then
        Exit Sub
    Else
        If oAtributosItem.Count = 0 Then Exit Sub
    End If
    
    For Each oOrden In oOrdenes
        If oOrden.AtributosLineas Is Nothing Then Set oOrden.AtributosLineas = oFSGSRaiz.Generar_CAtributos
               
        'Recorro los ATRIBUTOS DE INTEGRACION recuperados de BBDD
        For Each oatrib In oAtributosItem
            bAnyadir = True
            
            'Recorro todos los atributos de la orden
            For i = 1 To oOrden.AtributosLineas.Count
                'Si ya existe el atributo en la orden...
                If oatrib.Atrib = oOrden.AtributosLineas.Item(i).Id Then
                    'Ya no se a�ade este atributo de integracion, puesto que ya existe en la LINEA
                    bAnyadir = False
                    oOrden.AtributosLineas.Item(i).Obligatorio = oatrib.Obligatorio
                    oOrden.AtributosLineas.Item(i).OrdenarIntegracion = True
                                        
                    If Not oOrden.LineasPedido Is Nothing Then
                        If oOrden.LineasPedido.Count > 1 Then Exit For
                    End If
                    
                    'Si el atributo de integracion es obligatorio, se marca como obligatorio el de la orden
                    If oatrib.Obligatorio = True And oOrden.AtributosLineas.Item(i).Obligatorio = False Then
                        oOrden.AtributosLineas.Item(i).Obligatorio = True
                        If oAtributosItemAnt.Item(CStr(oatrib.Atrib)) Is Nothing Then oAtributosItemAnt.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, , , , , , False, , Null, Null, Null, Null
                        Exit For
                    End If
                    
                    If oAtributosItemAnt.Item(CStr(oatrib.Atrib)) Is Nothing Then oAtributosItemAnt.AddAtrInt oatrib.Atrib, oatrib.Den, oatrib.Tipo, , , , , , True, , Null, Null, Null, Null
                    Exit For
                End If
            Next
            
            If oatrib.TipoIntroduccion = Introselec Then oatrib.CargarListaDeValoresInt
            
            oatrib.pedido = True
            
            If bAnyadir Then
                Set oAtribLin = oOrden.AtributosLineas.Add(oatrib.Atrib, oatrib.Cod, oatrib.Den, oatrib.Tipo, , , , , , , , , , , , , , , , oatrib.TipoIntroduccion, , , , , , oatrib.Obligatorio, , , , , oatrib.ListaPonderacion, , , , , , , , , , oatrib.Validacion_ERP, , , , , , oatrib.pedido)
                oAtribLin.OrdenarIntegracion = True
                oAtribLin.ListaExterna = oatrib.ListaExterna
            End If
            
            If Not oOrden.LineasPedido Is Nothing Then
                For Each oLinea In oOrden.LineasPedido
                    bInsertarAtribInt = True
                
                    If Not oLinea.ATRIBUTOS Is Nothing Then
                        'Recorro todos los atributos de la orden
                        For i = 1 To oLinea.ATRIBUTOS.Count
                            If oatrib.Atrib = oLinea.ATRIBUTOS.Item(i).idAtribProce Then
                                'Si el atributo de integracion es obligatorio, se marca como obligatorio el de la orden
                                If oatrib.Obligatorio = True And oLinea.ATRIBUTOS.Item(i).Obligatorio = False Then
                                    oLinea.ATRIBUTOS.Item(i).Obligatorio = True
                                End If
                                'Ya no se a�ade este atributo de integracion, puesto que ya existe en la orden
                                bInsertarAtribInt = False
                                
                                oLinea.ATRIBUTOS.Item(i).OrdenarIntegracion = True
                            End If
                        Next
                    End If
                    
                    'Si NO se a encontrado el atributo en la orden, se a�ade el atributo de integracion de cabecera
                    If bInsertarAtribInt = True Then
                        If oatrib.valorBool = 1 Then
                            oatrib.valorBool = True
                        ElseIf oatrib.valorBool = 0 Then
                            oatrib.valorBool = False
                        End If
                        
                        If oLinea.ATRIBUTOS Is Nothing Then Set oLinea.ATRIBUTOS = oFSGSRaiz.Generar_CAtributosOfertados
                        oLinea.ATRIBUTOS.Add oatrib.Atrib, , , , oatrib.valorBool, oatrib.valorFec, oatrib.valorNum, , oatrib.valorText, , , oatrib, , oatrib.Validacion_ERP, oatrib.Obligatorio, True
                    End If
                'FOR por cada LINEA de pedido
                Next
            End If
        'FOR por cada atributo de integracion leido de BBDD
        Next
    'FOR por cada ORDEN
    Next
End Sub
