VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFormularioSimulacion 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   5595
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   11130
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5595
   ScaleWidth      =   11130
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCalcular 
      Caption         =   "DRecalcular"
      Height          =   315
      Left            =   60
      TabIndex        =   1
      Top             =   5200
      Width           =   1500
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCalculados 
      Height          =   5025
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   10995
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   9
      stylesets.count =   3
      stylesets(0).Name=   "Gris"
      stylesets(0).BackColor=   12632256
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFormularioSimulacion.frx":0000
      stylesets(1).Name=   "Amarillo"
      stylesets(1).BackColor=   13172735
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFormularioSimulacion.frx":001C
      stylesets(2).Name=   "NoHabilitado"
      stylesets(2).BackColor=   14671839
      stylesets(2).Picture=   "frmFormularioSimulacion.frx":0038
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID_GRUPO"
      Columns(1).Name =   "ID_GRUPO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1376
      Columns(2).Caption=   "ID_CALCULO"
      Columns(2).Name =   "ID_CALCULO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(2).HasBackColor=   -1  'True
      Columns(2).BackColor=   16777215
      Columns(2).StyleSet=   "Gris"
      Columns(3).Width=   3043
      Columns(3).Caption=   "GRUPO"
      Columns(3).Name =   "GRUPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      Columns(3).HasBackColor=   -1  'True
      Columns(3).BackColor=   16777215
      Columns(3).StyleSet=   "Gris"
      Columns(4).Width=   3200
      Columns(4).Caption=   "NOMBRE"
      Columns(4).Name =   "NOMBRE"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).HasBackColor=   -1  'True
      Columns(4).BackColor=   16777215
      Columns(4).StyleSet=   "Gris"
      Columns(5).Width=   3200
      Columns(5).Caption=   "VALOR_ENTRADA"
      Columns(5).Name =   "VALOR_ENTRADA"
      Columns(5).Alignment=   1
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).NumberFormat=   "standard"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "VALOR_CALCULADO"
      Columns(6).Name =   "VALOR_CALCULADO"
      Columns(6).Alignment=   1
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).NumberFormat=   "standard"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).HasBackColor=   -1  'True
      Columns(6).BackColor=   16777215
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "TIPO"
      Columns(7).Name =   "TIPO"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "ORIGEN"
      Columns(8).Name =   "ORIGEN"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      _ExtentX        =   19394
      _ExtentY        =   8864
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFormularioSimulacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public g_bModif As Boolean
Public g_oFormulario As CFormulario

Private m_sIdiForm As String
Private m_sIdiSimulacion As String
Private m_sIdiErrorEvaluacion(2) As String
Private m_sIdiCampo As String

Private Sub cmdCalcular_Click()
    RecalcularImportesFormulario
End Sub

Private Sub Form_Load()
    Me.Height = 6015
    Me.Width = 11250
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    If g_bModif = False Then
        cmdCalcular.Visible = False
        sdbgCalculados.Columns("VALOR_ENTRADA").Locked = True
    End If
    
    Me.caption = m_sIdiForm & g_oFormulario.Den & ". " & m_sIdiSimulacion
    CargarCamposCalculados
End Sub

Private Sub Form_Resize()
    If Me.Width < 1000 Then Exit Sub
    If Me.Height < 1300 Then Exit Sub

    sdbgCalculados.Width = Me.Width - 250
        
    sdbgCalculados.Columns("ID_CALCULO").Width = sdbgCalculados.Width / 20
    sdbgCalculados.Columns("GRUPO").Width = sdbgCalculados.Width / 5
    sdbgCalculados.Columns("NOMBRE").Width = sdbgCalculados.Width / 2.6
    sdbgCalculados.Columns("VALOR_ENTRADA").Width = sdbgCalculados.Width / 6
    sdbgCalculados.Columns("VALOR_CALCULADO").Width = sdbgCalculados.Width / 6

    If g_bModif = True Then
        sdbgCalculados.Height = Me.Height - 940
    Else
        sdbgCalculados.Height = Me.Height - 600
    End If
    cmdCalcular.Top = sdbgCalculados.Top + sdbgCalculados.Height + 50
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    g_bModif = False
    Set g_oFormulario = Nothing
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SIMULACION, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
    
        m_sIdiForm = Ador(0).Value  '1 Formulario:
        Ador.MoveNext
        m_sIdiSimulacion = Ador(0).Value  '2 Simulaci�n
        Ador.MoveNext
        sdbgCalculados.Columns("ID_CALCULO").caption = Ador(0).Value '3 Id.
        Ador.MoveNext
        sdbgCalculados.Columns("GRUPO").caption = Ador(0).Value '4 Grupo
        Ador.MoveNext
        sdbgCalculados.Columns("NOMBRE").caption = Ador(0).Value '5 Nombre
        Ador.MoveNext
        sdbgCalculados.Columns("VALOR_ENTRADA").caption = Ador(0).Value '6 Valor de entrada
        Ador.MoveNext
        sdbgCalculados.Columns("VALOR_CALCULADO").caption = Ador(0).Value '7 Valor calculado
        Ador.MoveNext
        cmdCalcular.caption = Ador(0).Value '8 &Recalcular
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '9 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '10 Error al realizar el c�lculo:Valores incorrectos.
        Ador.MoveNext
        m_sIdiCampo = Ador(0).Value   '11 Valor calculado para el campo
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub CargarCamposCalculados()
Dim Ador As Ador.Recordset
Dim sCadena As String

    sdbgCalculados.RemoveAll
    
    Set Ador = g_oFormulario.CargarCamposCalculados
     
    If Not Ador Is Nothing Then
        If Ador.RecordCount > 0 Then Ador.MoveFirst
        
        While Not Ador.EOF
            sCadena = Ador.Fields("ID").Value & Chr(m_lSeparador) & Ador.Fields("GRUPO").Value & Chr(m_lSeparador) & Ador.Fields("ID_CALCULO").Value
            sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("DENGRUPO").Value & Chr(m_lSeparador) & Ador.Fields("DEN_" & gParametrosInstalacion.gIdioma).Value
            
            If Ador.Fields("TIPO").Value = TipoCampoPredefinido.Calculado Then  'Es un campo calculado:
                sCadena = sCadena & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & Ador.Fields("VALOR_NUM").Value
            Else  'es un campo normal
                sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("VALOR_NUM").Value & Chr(m_lSeparador) & ""
            End If
            
            sCadena = sCadena & Chr(m_lSeparador) & Ador.Fields("TIPO").Value & Chr(m_lSeparador) & Ador.Fields("ORIGEN_CALC_DESGLOSE").Value
            
            sdbgCalculados.AddItem sCadena
            
            Ador.MoveNext
        Wend

        Ador.Close
    End If
 
    Set Ador = Nothing
End Sub

Private Sub sdbgCalculados_BeforeUpdate(Cancel As Integer)
    If sdbgCalculados.Columns("VALOR_ENTRADA").Value <> "" Then
        If Not IsNumeric(sdbgCalculados.Columns("VALOR_ENTRADA").Value) Then
            oMensajes.NoValido sdbgCalculados.Columns("VALOR_ENTRADA").caption
            If Me.Visible Then sdbgCalculados.SetFocus
            sdbgCalculados.col = 5
            Cancel = True
            Exit Sub
        End If
    End If
End Sub

Private Sub sdbgCalculados_BtnClick()
Dim oCampo As CFormItem

    'LLama al formulario de las l�neas de desglose:
    frmDesgloseValores.g_sOrigen = "frmFormularioSimulacion"
    
    Set oCampo = oFSGSRaiz.Generar_CFormCampo
    oCampo.Id = sdbgCalculados.Columns("ORIGEN").Value
    Set oCampo.Grupo = g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value))
    Set frmDesgloseValores.g_oCampoDesglose = oCampo
    
    Set frmDesgloseValores.g_oCampoFormula = g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").Value)).Campos.Item(CStr(sdbgCalculados.Columns("ID").Value))
    
    frmDesgloseValores.Show vbModal
    
    Set oCampo = Nothing
End Sub

Private Sub sdbgCalculados_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgCalculados.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
        sdbgCalculados.Columns("VALOR_ENTRADA").Locked = True
    Else
        sdbgCalculados.Columns("VALOR_ENTRADA").Locked = False
    End If
    
    If sdbgCalculados.Columns("ORIGEN").Value = "" Then
        sdbgCalculados.Columns("VALOR_ENTRADA").Style = ssStyleEdit
    Else
        sdbgCalculados.Columns("VALOR_ENTRADA").Style = ssStyleEditButton
    End If
End Sub

Private Sub RecalcularImportesFormulario()
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim i As Integer
Dim vbm As Variant
Dim sVariables() As String
Dim dValues() As Double
Dim teserror As TipoErrorSummit

    'Actualizo para recalcular pq si pulsa el boton recalcular sin cambiar de la fila no recalcula
    sdbgCalculados.Update
    
    'Recalcula las f�rmulas:
    Set iEq = New USPExpression
    
    'Almacena las variables en un array:
    ReDim sVariables(sdbgCalculados.Rows - 1)
    ReDim dValues(sdbgCalculados.Rows - 1)
    
    For i = 0 To sdbgCalculados.Rows - 1
        vbm = sdbgCalculados.AddItemBookmark(i)
        sVariables(i) = sdbgCalculados.Columns("ID_CALCULO").CellValue(vbm)
        If sdbgCalculados.Columns("TIPO").CellValue(vbm) = TipoCampoPredefinido.Calculado Then
            If sdbgCalculados.Columns("ORIGEN").CellValue(vbm) <> "" Then
                dValues(i) = StrToDbl0(sdbgCalculados.Columns("VALOR_CALCULADO").CellValue(vbm))
            Else
                dValues(i) = 0
            End If
        Else
            dValues(i) = StrToDbl0(sdbgCalculados.Columns("VALOR_ENTRADA").CellValue(vbm))
        End If
        
        'Guarda los valores en la colecci�n:
        If IsNull(g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).OrigenCalcDesglose) Then
            g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).valorNum = dValues(i)
        End If
    Next i
    
    'Va recalculando cada f�rmula
    For i = 0 To sdbgCalculados.Rows - 1
        vbm = sdbgCalculados.AddItemBookmark(i)
        
        If sdbgCalculados.Columns("TIPO").CellValue(vbm) = TipoCampoPredefinido.Calculado Then
            If IsNull(g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).OrigenCalcDesglose) Then
                  
                'Se calculan las f�rmulas que no sean de desglose:
                lIndex = iEq.Parse(g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).Formula, sVariables, lErrCode)
                
                If lErrCode = USPEX_NO_ERROR Then
                    On Error GoTo EvaluationError
                    dValues(i) = iEq.Evaluate(dValues)
                     
                    'Almacena el valor calculado en la colecci�n:
                    g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).valorNum = dValues(i)
                   
                Else
                     oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                     Exit Sub
                End If
            End If
        End If
    Next i
    
    'Ahora almacena los resultados en BD:
    teserror = g_oFormulario.ModificarImportesCalculados()
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        
    Else
        'Actualiza la grid con los campos calculados
        For i = 0 To sdbgCalculados.Rows - 1
            vbm = sdbgCalculados.AddItemBookmark(i)
            If sdbgCalculados.Columns("TIPO").CellValue(vbm) = TipoCampoPredefinido.Calculado Then
                If IsNull(g_oFormulario.Grupos.Item(CStr(sdbgCalculados.Columns("ID_GRUPO").CellValue(vbm))).Campos.Item(CStr(sdbgCalculados.Columns("ID").CellValue(vbm))).OrigenCalcDesglose) Then
                    sdbgCalculados.Bookmark = vbm
                    sdbgCalculados.Columns("VALOR_CALCULADO").Value = dValues(i)
                    sdbgCalculados.Update
                End If
            End If
        Next i
    
    End If
    
    Set iEq = Nothing
    
    Exit Sub
    
EvaluationError:
    oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(2))
End Sub

Private Sub sdbgCalculados_RowLoaded(ByVal Bookmark As Variant)
    'Dependiendo de la columna que sea,le pondr� el estilo:
    If sdbgCalculados.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
        sdbgCalculados.Columns("VALOR_ENTRADA").CellStyleSet "NoHabilitado"
        sdbgCalculados.Columns("VALOR_CALCULADO").CellStyleSet "Amarillo"
    Else
        sdbgCalculados.Columns("VALOR_ENTRADA").CellStyleSet ""
        sdbgCalculados.Columns("VALOR_CALCULADO").CellStyleSet "NoHabilitado"
    End If
    
End Sub

