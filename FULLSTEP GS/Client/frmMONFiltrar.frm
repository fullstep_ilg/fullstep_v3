VERSION 5.00
Begin VB.Form frmMONFiltrar 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Monedas (Filtro)"
   ClientHeight    =   2520
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5400
   Icon            =   "frmMONFiltrar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2520
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1590
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2100
      Width           =   1005
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2760
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   2100
      Width           =   1005
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   75
      TabIndex        =   8
      Top             =   1065
      Width           =   5235
      Begin VB.CheckBox chkIgualDen 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   3060
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   420
         Width           =   2055
      End
      Begin VB.TextBox txtDEN 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   240
         MaxLength       =   100
         TabIndex        =   2
         Top             =   360
         Width           =   2700
      End
      Begin VB.OptionButton optDEN 
         BackColor       =   &H00808000&
         Caption         =   "Por denominaci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   -60
         Width           =   2280
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00808000&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   75
      TabIndex        =   6
      Top             =   45
      Width           =   5235
      Begin VB.CheckBox chkIgualCod 
         BackColor       =   &H00808000&
         Caption         =   "Coincidencia &total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   195
         Left            =   1140
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   420
         Width           =   2280
      End
      Begin VB.TextBox txtCOD 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   3
         TabIndex        =   0
         Top             =   360
         Width           =   795
      End
      Begin VB.OptionButton optCOD 
         BackColor       =   &H00808000&
         Caption         =   "Por c�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   0
         Value           =   -1  'True
         Width           =   2280
      End
   End
End
Attribute VB_Name = "frmMONFiltrar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
''' *** Formulario: frmMONFiltrar
''' *** Creacion: 28/12/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

Option Explicit
Private Sub Form_Load()

    ''' * Objetivo: Situar el formulario respecto al
    ''' * Objetivo: principal de monedas
    
    On Error Resume Next
    
    Me.Left = frmMON.Left + 500
    Me.Top = frmMON.Top + 1000
    CargarRecursos
    txtCod.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodMON
    
End Sub
Private Sub optCOD_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    If optCOD.Value = True Then
        optDEN.Value = False
    End If
        
    txtCod.SetFocus
   
End Sub
Private Sub chkIgualDen_Click()
    
    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    txtDen.SetFocus

End Sub
Private Sub chkIgualCod_Click()

    ''' * Objetivo: Seleccionar filtrar por codigo
    
    txtCod.SetFocus
    
End Sub
Private Sub optDEN_Click()

    ''' * Objetivo: Seleccionar filtrar por denominacion
    
    If optDEN.Value = True Then
        optCOD.Value = False
    End If
    
    txtDen.SetFocus
    
End Sub

Private Sub cmdAceptar_Click()

    ''' * Objetivo: Aplicar el filtro y descargar
    ''' * Objetivo: el formulario
    
    Screen.MousePointer = vbHourglass
    
    Set frmMON.oMonedas = Nothing
    Set frmMON.oMonedas = oFSGSRaiz.generar_cmonedas
   
        
'    frmMON.Caption = "Monedas (Consulta)"
    frmMON.ponerCaption "", 3, False
 
    If optCOD.Value = True And txtCod <> "" Then
        
        frmMON.sCriterioCodListado = txtCod
        frmMON.sCriterioDenListado = ""
        frmMON.bCriterioCoincidenciaTotal = chkIgualCod.Value
        
        If Not chkIgualCod.Value = vbChecked Then
        
            frmMON.oMonedas.CargarTodasLasMonedas Trim(txtCod), , , , , , frmMON.chkIncluirEqNoAct, True, gParametrosIntegracion.gaExportar(EntidadIntegracion.mon)
'            frmMON.Caption = "Monedas (Consulta Codigo = " & txtCOD & "*)"
            frmMON.ponerCaption txtCod, 1, True
                        
        Else
        
            frmMON.oMonedas.CargarTodasLasMonedas Trim(txtCod), , True, , , , frmMON.chkIncluirEqNoAct, True, gParametrosIntegracion.gaExportar(EntidadIntegracion.mon)
'            frmMON.Caption = "Monedas (Consulta Codigo = " & txtCOD & ")"
            frmMON.ponerCaption txtCod, 1, False
            
            
        End If
        
    Else
    
        If optDEN.Value = True And txtDen <> "" Then
        
            frmMON.sCriterioCodListado = ""
            frmMON.sCriterioDenListado = txtDen
            frmMON.bCriterioCoincidenciaTotal = chkIgualDen.Value
            
            If Not chkIgualDen.Value = vbChecked Then
                
                frmMON.oMonedas.CargarTodasLasMonedas , txtDen, , True, , , frmMON.chkIncluirEqNoAct, True, gParametrosIntegracion.gaExportar(EntidadIntegracion.mon)
'                frmMON.Caption = "Monedas (Consulta Denominacion = " & txtDEN & "*)"
                frmMON.ponerCaption txtDen, 2, True
                
            Else
                
                frmMON.oMonedas.CargarTodasLasMonedas , txtDen, True, True, , , frmMON.chkIncluirEqNoAct, True, gParametrosIntegracion.gaExportar(EntidadIntegracion.mon)
'                frmMON.Caption = "Monedas (Consulta Denominacion = " & txtDEN & ")"
                frmMON.ponerCaption txtDen, 2, False
                
            End If
        
        Else
        
            frmMON.sCriterioCodListado = ""
            frmMON.sCriterioDenListado = ""
            frmMON.bCriterioCoincidenciaTotal = False
        
            frmMON.oMonedas.CargarTodasLasMonedas , , True, , , , frmMON.chkIncluirEqNoAct, True, gParametrosIntegracion.gaExportar(EntidadIntegracion.mon)
            
        End If
        
    End If
        
            
    frmMON.sdbgMonedas.ReBind
    
    MDI.MostrarFormulario frmMON, True
    
    frmMON.sdbgMonedas.MoveFirst
    
    Screen.MousePointer = vbNormal
    
    Unload Me

    frmMON.SetFocus
    
End Sub
Private Sub cmdCancelar_Click()

    ''' * Objetivo: Cerrar el formulario
    
    MDI.MostrarFormulario frmMON, True
    
    Unload Me
    
    frmMON.SetFocus
    
End Sub
Private Sub Form_Activate()

    ''' * Objetivo: Iniciar el formulario
    
    txtCod.SetFocus
    
End Sub
Private Sub txtCOD_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por codigo

    optCOD.Value = True
    optDEN.Value = False
    
End Sub
Private Sub txtDEN_GotFocus()

    ''' * Objetivo: Seleccionar filtrar por denominacion

    optCOD.Value = False
    optDEN.Value = True
    
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_MONFILTRAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        
    Caption = Ador(0).Value
    Ador.MoveNext
    optCOD.Caption = Ador(0).Value
    Ador.MoveNext
    chkIgualCod.Caption = Ador(0).Value
    chkIgualDen.Caption = Ador(0).Value
    Ador.MoveNext
    optDEN.Caption = Ador(0).Value
    Ador.MoveNext
    cmdAceptar.Caption = Ador(0).Value
    Ador.MoveNext
    cmdCancelar.Caption = Ador(0).Value
    
    Ador.Close
    
    End If

    Set Ador = Nothing



End Sub


