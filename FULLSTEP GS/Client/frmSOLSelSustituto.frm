VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSOLSelSustituto 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DSeleccione peticionario"
   ClientHeight    =   7890
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5550
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSOLSelSustituto.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7890
   ScaleWidth      =   5550
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdBuscarCodigo 
      Height          =   295
      Left            =   4000
      Picture         =   "frmSOLSelSustituto.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   260
      Width           =   335
   End
   Begin VB.TextBox tbBuscarCodigo 
      Height          =   285
      Left            =   2000
      TabIndex        =   8
      Top             =   260
      Width           =   1980
   End
   Begin VB.OptionButton optTipoSustTodas 
      BackColor       =   &H00808000&
      Caption         =   "DSustituir rol en todas las solicitudes"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   100
      TabIndex        =   7
      Top             =   5720
      Value           =   -1  'True
      Width           =   3500
   End
   Begin VB.OptionButton optTipoSustEnCurso 
      BackColor       =   &H00808000&
      Caption         =   "DSustituir rol en solicitudes en curso"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   100
      TabIndex        =   6
      Top             =   5360
      Width           =   3500
   End
   Begin VB.OptionButton optTipoSustFinalizadas 
      BackColor       =   &H00808000&
      Caption         =   "DSustituir rol en solicitudes finalizadas"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   100
      TabIndex        =   5
      Top             =   5000
      Width           =   3500
   End
   Begin VB.PictureBox picNavigate 
      Align           =   2  'Align Bottom
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      ScaleHeight     =   420
      ScaleWidth      =   5550
      TabIndex        =   2
      Top             =   7470
      Width           =   5550
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   315
         Left            =   2955
         TabIndex        =   4
         Top             =   30
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   315
         Left            =   1755
         TabIndex        =   3
         Top             =   30
         Width           =   1005
      End
   End
   Begin VB.PictureBox picEstrOrg 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   4095
      Left            =   45
      ScaleHeight     =   4095
      ScaleWidth      =   5565
      TabIndex        =   0
      Top             =   700
      Width           =   5565
      Begin MSComctlLib.TreeView tvwEstrOrg 
         Height          =   3900
         Left            =   15
         TabIndex        =   1
         Top             =   100
         Width           =   5400
         _ExtentX        =   9525
         _ExtentY        =   6879
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   120
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":0D03
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":10CB
            Key             =   "Persona"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":11D5
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":1529
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":187D
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":1BD1
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":1F65
            Key             =   "PerUsu"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":206F
            Key             =   "PersonaBaja"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSOLSelSustituto.frx":20FA
            Key             =   "PerUsuBaja"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblBusqueda 
      BackColor       =   &H00808000&
      Caption         =   "B�squeda por c�digo :"
      ForeColor       =   &H8000000E&
      Height          =   285
      Left            =   100
      TabIndex        =   10
      Top             =   260
      Width           =   1665
   End
End
Attribute VB_Name = "frmSOLSelSustituto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables para interactuar con otros forms
Public bRUO As Boolean
Public bRDep As Boolean
Public sSustituto As String
Public iTipoSustitucion As TipoSustitucion
Public lRol As Long

'Variables privadas
Private m_sTituloSolConf As String

Private Sub cmdAceptar_Click()
Dim nodx As MSComctlLib.node
Dim scod1 As String
Dim iTipoSustitucion As TipoSustitucion

Dim teserror As TipoErrorSummit

Dim oPer As CPersona
Dim oRolSustituir As CPMRol

    Set nodx = tvwEstrOrg.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    scod1 = DevolverCod(nodx)
    Select Case nodx.Image
        Case "PerUsu"
            frmFlujosRoles.sdbgRoles.Columns("SUSTITUCION").Value = "..."
            frmFlujosRoles.sdbgRoles.Columns("SUSTITUTO").Value = scod1
            
            If optTipoSustFinalizadas.Value = True Then
                iTipoSustitucion = 1
            ElseIf optTipoSustEnCurso.Value = True Then
                iTipoSustitucion = 2
            ElseIf optTipoSustTodas.Value = True Then
                iTipoSustitucion = 3
            End If
            frmFlujosRoles.sdbgRoles.Columns("TIPO_SUSTITUCION").Value = iTipoSustitucion
            
            Set oRolSustituir = oFSGSRaiz.Generar_CPMRol
            oRolSustituir.Id = lRol
            teserror = oRolSustituir.Sustituir_Personas(scod1, iTipoSustitucion)
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
            End If
            frmFlujosRoles.g_vFecAct = oRolSustituir.FecAct
        Case Else
            oMensajes.PersonaNoValida
            Screen.MousePointer = vbNormal
            If Me.Visible Then tvwEstrOrg.SetFocus
            Exit Sub
    End Select
    
    Screen.MousePointer = vbNormal
    
    Unload Me
    
    Set oPer = Nothing
    Exit Sub
End Sub

Private Sub cmdBuscarCodigo_Click()
    Dim blnEncontrado As Boolean
    Dim oNodo As node
    
    blnEncontrado = False
    
    For Each oNodo In tvwEstrOrg.Nodes
        If UCase(Mid(oNodo.Tag, 5)) = UCase(tbBuscarCodigo.Text) Then
            blnEncontrado = True
            Exit For
        End If
    Next
    
    If blnEncontrado Then
        Set tvwEstrOrg.selectedItem = oNodo
        oNodo.EnsureVisible
    Else
        Set tvwEstrOrg.selectedItem = tvwEstrOrg.Nodes(1)
    End If
    tvwEstrOrg.SetFocus
    
    Set oNodo = Nothing
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset


' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_SOL_SELEC_SUSTITUTO, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then

        m_sTituloSolConf = Ador(0).Value  '1 Seleccione peticionario
        Ador.MoveNext
        optTipoSustFinalizadas.caption = Ador(0).Value
        Ador.MoveNext
        optTipoSustEnCurso.caption = Ador(0).Value
        Ador.MoveNext
        optTipoSustTodas.caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        lblBusqueda.caption = Ador(0).Value & " :" '7 B�squeda por c�digo :.
        
        Ador.Close
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()
Dim nodx As node

    Me.Width = 5640
    Me.Height = 7000
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    
    CargarRecursos
    
    GenerarEstructuraOrg False
    
    On Error GoTo Error
    If frmFlujosRoles.sdbgRoles.Columns("SUSTITUTO").Value <> "" Then
        Set nodx = tvwEstrOrg.Nodes.Item("PERS" & CStr(frmFlujosRoles.sdbgRoles.Columns("SUSTITUTO").Value))
        nodx.Selected = True
        nodx.EnsureVisible
        tvwEstrOrg.Enabled = True
    End If
    
Continuar:
    Select Case iTipoSustitucion
        Case TipoSustitucion.Finalizadas
            optTipoSustFinalizadas.Value = True
        Case TipoSustitucion.EnCurso
            optTipoSustEnCurso.Value = True
        Case TipoSustitucion.Todas
            optTipoSustTodas.Value = True
        Case Else
            optTipoSustTodas.Value = True
    End Select
    
    Me.caption = m_sTituloSolConf
    Exit Sub
    
Error:
    If err.Number = 35601 Then
        GoTo Continuar
    End If
End Sub

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Departamentos asociados
Dim oDepsAsocN0 As CDepAsociados
Dim oDepsAsocN1 As CDepAsociados
Dim oDepsAsocN2 As CDepAsociados
Dim oDepsAsocN3 As CDepAsociados
Dim oDepAsoc As CDepAsociado

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3

' Personas
Dim oPersN0 As CPersonas
Dim oPersN1 As CPersonas
Dim oPersN2 As CPersonas
Dim oPersN3 As CPersonas
Dim oPer As CPersona

' Otras
Dim nodx As node


Dim varCodPersona As Variant



    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
    Set oDepsAsocN0 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN1 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN2 = oFSGSRaiz.generar_CDepAsociados
    Set oDepsAsocN3 = oFSGSRaiz.generar_CDepAsociados
    
    Set oPersN0 = oFSGSRaiz.Generar_CPersonas
    Set oPersN1 = oFSGSRaiz.Generar_CPersonas
    Set oPersN2 = oFSGSRaiz.Generar_CPersonas
    Set oPersN3 = oFSGSRaiz.Generar_CPersonas
         
    If (oUsuarioSummit.Tipo = TIpoDeUsuario.Persona Or oUsuarioSummit.Tipo = TIpoDeUsuario.comprador) And _
       (bRUO Or bRDep) Then
        
        varCodPersona = oUsuarioSummit.Persona.Cod
        oDepsAsocN0.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 0, , , , bOrdenadoPorDen, False
        
        ' Cargamos las personas  de esos departamentos.
        For Each oDepAsoc In oDepsAsocN0
             oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
        Next

        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , False, , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 2, , , , bOrdenadoPorDen, False
                oDepsAsocN3.CargarTodosLosDepAsociados basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, basOptimizacion.gCodDepUsuario, bRUO, bRDep, 3, , , , bOrdenadoPorDen, False
                ' Cargamos las personas  de esos departamentos.
        
                'Personas
                
                For Each oDepAsoc In oDepsAsocN1
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN2
                   oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                Next
                
                For Each oDepAsoc In oDepsAsocN3
                    oDepAsoc.CargarTodasLasPersonas , , , , , , True, , , False, , True
                    
                Next
        End Select
        
        
    Else
        
        oDepsAsocN0.CargarTodosLosDepAsociados , , , , , , 0, , , , bOrdenadoPorDen, False
        oPersN0.CargarTodasLasPersonas , , , , 0, , True, , , False, , True
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
        
        Case 1
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
        Case 2
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen, False
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, False
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
        Case 3
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
                oDepsAsocN1.CargarTodosLosDepAsociados , , , , , , 1, , , , bOrdenadoPorDen, False
                oDepsAsocN2.CargarTodosLosDepAsociados , , , , , , 2, , , , bOrdenadoPorDen, True
                oDepsAsocN3.CargarTodosLosDepAsociados , , , , , , 3, , , , bOrdenadoPorDen, False
                oPersN1.CargarTodasLasPersonas , , , , 1, , True, , , False, , True
                oPersN2.CargarTodasLasPersonas , , , , 2, , True, , , False, , True
                oPersN3.CargarTodasLasPersonas , , , , 3, , True, , , False, , True
                
        End Select
        
    End If
    
   '************************************************************
    'Generamos la estructura arborea
    tvwEstrOrg.Nodes.clear

    ' Unidades organizativas
    Set nodx = tvwEstrOrg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    ' Departamentos
    
    If (basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador) _
        And (bRUO Or bRDep) Then

        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))

            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER0" & CStr(oPer.Cod)
            Next
        Next

        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER1" & CStr(oPer.Cod)
            Next
        Next

        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER2" & CStr(oPer.Cod)
            Next
        Next

        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            For Each oPer In oDepAsoc.Personas
                If Not IsNull(oPer.Usuario) Then
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
                Else
                    Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
                End If
                nodx.Tag = "PER3" & CStr(oPer.Cod)
            Next
        Next
    
    Else
    
        'Departamentos
        For Each oDepAsoc In oDepsAsocN0
            scod1 = oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON0", tvwChild, "DEPA" & scod1, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP0" & CStr(oDepAsoc.Cod)
        
        Next
                
        For Each oDepAsoc In oDepsAsocN1
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON1" & scod1, tvwChild, "DEPA" & scod2, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP1" & CStr(oDepAsoc.Cod)
            
        Next
                
        For Each oDepAsoc In oDepsAsocN2
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
                
            Set nodx = tvwEstrOrg.Nodes.Add("UON2" & scod2, tvwChild, "DEPA" & scod3, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP2" & CStr(oDepAsoc.Cod)
            
        Next
        
        For Each oDepAsoc In oDepsAsocN3
            scod1 = oDepAsoc.CodUON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oDepAsoc.CodUON1))
            scod2 = scod1 & oDepAsoc.CodUON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oDepAsoc.CodUON2))
            scod3 = scod2 & oDepAsoc.CodUON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oDepAsoc.CodUON3))
            scod4 = scod3 & oDepAsoc.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oDepAsoc.Cod))
            
            Set nodx = tvwEstrOrg.Nodes.Add("UON3" & scod3, tvwChild, "DEPA" & scod4, CStr(oDepAsoc.Cod) & " - " & oDepAsoc.Den, "Departamento")
            nodx.Tag = "DEP3" & CStr(oDepAsoc.Cod)
            
        Next
                
        'Personas
        
        For Each oPer In oPersN0
            scod1 = oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod1, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER0" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN1
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod2, tvwChild, "PERS" & oPer.Cod, oPer.Cod & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER1" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN2
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod3, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER2" & CStr(oPer.Cod)
        Next
        
        For Each oPer In oPersN3
            scod1 = oPer.UON1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oPer.UON1))
            scod2 = scod1 & oPer.UON2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oPer.UON2))
            scod3 = scod2 & oPer.UON3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oPer.UON3))
            scod4 = scod3 & oPer.CodDep & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodDEP - Len(oPer.CodDep))
            
            If Not IsNull(oPer.Usuario) Then
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "PerUsu")
            Else
                Set nodx = tvwEstrOrg.Nodes.Add("DEPA" & scod4, tvwChild, "PERS" & CStr(oPer.Cod), CStr(oPer.Cod) & " - " & oPer.Apellidos & " " & oPer.nombre, "Persona")
            End If
            nodx.Tag = "PER3" & CStr(oPer.Cod)
        Next
    End If
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    Set oDepsAsocN0 = Nothing
    Set oDepsAsocN1 = Nothing
    Set oDepsAsocN2 = Nothing
    Set oDepsAsocN3 = Nothing
    Set oDepAsoc = Nothing
    
    Set oPersN0 = Nothing
    Set oPersN1 = Nothing
    Set oPersN2 = Nothing
    Set oPersN3 = Nothing
    Set oPer = Nothing
    
End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant
    
    If node Is Nothing Then
        Exit Function
    End If
    
    If Len(node.Tag) < 4 Then
        DevolverCod = node.Tag
        Exit Function
    End If
    DevolverCod = Right(node.Tag, Len(node.Tag) - 4)

End Function

