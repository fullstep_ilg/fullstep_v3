VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmESTRMAT 
   Caption         =   "Materiales"
   ClientHeight    =   5535
   ClientLeft      =   1290
   ClientTop       =   2625
   ClientWidth     =   10215
   Icon            =   "frmESTRMAT.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5535
   ScaleWidth      =   10215
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   7440
      Top             =   4800
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":014A
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":059E
            Key             =   "GMN3A"
            Object.Tag             =   "GMN3A"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0668
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0718
            Key             =   "GMN1A"
            Object.Tag             =   "GMN1A"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":07D9
            Key             =   "GMN2"
            Object.Tag             =   "GMN2"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0889
            Key             =   "GMN2A"
            Object.Tag             =   "GMN2A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0949
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0CDC
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0D9A
            Key             =   "GMN4"
            Object.Tag             =   "GMN4"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0E4A
            Key             =   "GMN4A"
            Object.Tag             =   "GMN4A"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0F0A
            Key             =   "GMN1"
            Object.Tag             =   "GMN1"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRMAT.frx":0FBA
            Key             =   "GMN3"
            Object.Tag             =   "GMN3"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab SSTabEstrMat 
      Height          =   5430
      Left            =   120
      TabIndex        =   10
      Top             =   60
      Width           =   10050
      _ExtentX        =   17727
      _ExtentY        =   9578
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabMaxWidth     =   3528
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Estructura"
      TabPicture(0)   =   "frmESTRMAT.frx":1073
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "ssMateriales"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "tvwEstrMat"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "chkVerIdiomas"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "picEdicion"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "picNavigate"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Art�culos"
      TabPicture(1)   =   "frmESTRMAT.frx":108F
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Picture1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Picture2"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         Height          =   410
         Left            =   120
         ScaleHeight     =   405
         ScaleWidth      =   6720
         TabIndex        =   12
         Top             =   4860
         Width           =   6720
         Begin VB.CommandButton cmdA�adir 
            Caption         =   "&A�adir"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   28
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   5400
            TabIndex        =   4
            Top             =   60
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "&Buscar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4320
            TabIndex        =   3
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3240
            TabIndex        =   2
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdCodigo 
            Caption         =   "&C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2160
            TabIndex        =   0
            Top             =   60
            Width           =   1005
         End
         Begin VB.CommandButton cmdEli 
            Caption         =   "&Eliminar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1080
            TabIndex        =   1
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEdicion 
         BorderStyle     =   0  'None
         Height          =   410
         Left            =   120
         ScaleHeight     =   405
         ScaleWidth      =   1455
         TabIndex        =   21
         Top             =   4860
         Visible         =   0   'False
         Width           =   1455
         Begin VB.CommandButton cmdDeshacer 
            Caption         =   "&Deshacer"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   22
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.CheckBox chkVerIdiomas 
         Caption         =   "DVer en todos los idiomas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   3015
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   -74880
         ScaleHeight     =   375
         ScaleWidth      =   9855
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   4920
         Width           =   9855
         Begin VB.CommandButton cmdReubicar 
            Caption         =   "Reubicar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   4560
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdFiltrarArticulo 
            Caption         =   "&Filtrar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   15
            Width           =   1005
         End
         Begin VB.CommandButton cmdListadoArticulo 
            Caption         =   "&Listado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2280
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   15
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurarArticulo 
            Caption         =   "&Restaurar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1140
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   15
            Width           =   1005
         End
         Begin VB.CommandButton cmdA�adirArticulo 
            Caption         =   "&A�adir"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   0
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   15
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdModoEdicion 
            Caption         =   "&Edici�n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   8740
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   15
            Width           =   1005
         End
         Begin VB.CommandButton cmdCodigoArticulo 
            Caption         =   "&C�digo"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3420
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdDeshacerArticulo 
            Caption         =   "&Deshacer"
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2280
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   15
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdEliminarArticulo 
            Caption         =   "&Eliminar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1140
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   15
            Visible         =   0   'False
            Width           =   1005
         End
      End
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4365
         Left            =   -74880
         ScaleHeight     =   4365
         ScaleWidth      =   9885
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   480
         Width           =   9885
         Begin SSDataWidgets_B.SSDBDropDown ssdbddConcepto 
            Height          =   1575
            Left            =   360
            TabIndex        =   24
            Top             =   1470
            Width           =   1095
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            MaxDropDownItems=   10
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   1588
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   50
            _ExtentX        =   1931
            _ExtentY        =   2778
            _StockProps     =   77
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown ssdbddRecepcion 
            Height          =   1575
            Left            =   3780
            TabIndex        =   27
            Top             =   990
            Width           =   1515
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            MaxDropDownItems=   10
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   2461
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   50
            _ExtentX        =   2672
            _ExtentY        =   2778
            _StockProps     =   77
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown ssdbddAlmacen 
            Height          =   1575
            Left            =   1770
            TabIndex        =   26
            Top             =   1200
            Width           =   1515
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            MaxDropDownItems=   10
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns(0).Width=   2461
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   50
            _ExtentX        =   2672
            _ExtentY        =   2778
            _StockProps     =   77
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddUnidades 
            Height          =   1575
            Left            =   5550
            TabIndex        =   13
            Top             =   1110
            Width           =   3855
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            MaxDropDownItems=   10
            _Version        =   196617
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1085
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   3
            Columns(1).Width=   4868
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   50
            _ExtentX        =   6800
            _ExtentY        =   2778
            _StockProps     =   77
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgArticulos 
            Height          =   4335
            Left            =   120
            TabIndex        =   25
            Top             =   0
            Width           =   9420
            _Version        =   196617
            DataMode        =   1
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GroupHeaders    =   0   'False
            stylesets.count =   9
            stylesets(0).Name=   "Atributos"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmESTRMAT.frx":10AB
            stylesets(1).Name=   "IntOK"
            stylesets(1).ForeColor=   16777215
            stylesets(1).BackColor=   -2147483633
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmESTRMAT.frx":1164
            stylesets(2).Name=   "Normal"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmESTRMAT.frx":1180
            stylesets(3).Name=   "ActiveRow"
            stylesets(3).ForeColor=   16777215
            stylesets(3).BackColor=   -2147483647
            stylesets(3).HasFont=   -1  'True
            BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(3).Picture=   "frmESTRMAT.frx":119C
            stylesets(3).AlignmentText=   0
            stylesets(4).Name=   "IntHeader"
            stylesets(4).HasFont=   -1  'True
            BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(4).Picture=   "frmESTRMAT.frx":11B8
            stylesets(4).AlignmentText=   0
            stylesets(4).AlignmentPicture=   0
            stylesets(5).Name=   "Adjudica"
            stylesets(5).HasFont=   -1  'True
            BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(5).Picture=   "frmESTRMAT.frx":1310
            stylesets(6).Name=   "IntKO"
            stylesets(6).ForeColor=   16777215
            stylesets(6).BackColor=   255
            stylesets(6).HasFont=   -1  'True
            BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(6).Picture=   "frmESTRMAT.frx":1685
            stylesets(7).Name=   "styEspAdjSi"
            stylesets(7).HasFont=   -1  'True
            BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(7).Picture=   "frmESTRMAT.frx":16A1
            stylesets(8).Name=   "HayImpuestos"
            stylesets(8).HasFont=   -1  'True
            BeginProperty stylesets(8).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(8).Picture=   "frmESTRMAT.frx":16BD
            stylesets(8).AlignmentText=   2
            stylesets(8).AlignmentPicture=   2
            AllowRowSizing  =   0   'False
            AllowGroupMoving=   0   'False
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            HeadStyleSet    =   "Normal"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterPos     =   2
            SplitterVisible =   -1  'True
            Columns.Count   =   17
            Columns(0).Width=   688
            Columns(0).Name =   "INT"
            Columns(0).Alignment=   2
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(0).HeadStyleSet=   "IntHeader"
            Columns(0).StyleSet=   "IntOK"
            Columns(1).Width=   2064
            Columns(1).Caption=   "C�digo"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   3
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777152
            Columns(1).HeadStyleSet=   "Normal"
            Columns(2).Width=   2646
            Columns(2).Caption=   "Denominaci�n"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   200
            Columns(2).HeadStyleSet=   "Normal"
            Columns(3).Width=   714
            Columns(3).Caption=   "Unidad def."
            Columns(3).Name =   "UNI"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(3).HeadStyleSet=   "Normal"
            Columns(4).Width=   873
            Columns(4).Caption=   "Consumo anual"
            Columns(4).Name =   "CANT"
            Columns(4).Alignment=   1
            Columns(4).CaptionAlignment=   0
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).NumberFormat=   "Standard"
            Columns(4).FieldLen=   256
            Columns(4).HeadStyleSet=   "Normal"
            Columns(5).Width=   794
            Columns(5).Caption=   "Gen"
            Columns(5).Name =   "GENERICO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(5).Style=   2
            Columns(6).Width=   1508
            Columns(6).Caption=   "Concepto"
            Columns(6).Name =   "CONCEP"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(6).HeadStyleSet=   "Normal"
            Columns(7).Width=   1508
            Columns(7).Caption=   "Almac�n"
            Columns(7).Name =   "ALMAC"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   1508
            Columns(8).Caption=   "Recepci�n"
            Columns(8).Name =   "RECEP"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Caption=   "TIPORECEPCION"
            Columns(9).Name =   "TIPORECEPCION"
            Columns(9).Alignment=   2
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   2
            Columns(9).FieldLen=   256
            Columns(9).Style=   2
            Columns(10).Width=   3200
            Columns(10).Caption=   "DUnidades Organizativas"
            Columns(10).Name=   "UON_DEN"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(10).Style=   1
            Columns(11).Width=   3200
            Columns(11).Caption=   "dCentral"
            Columns(11).Name=   "ART"
            Columns(11).Alignment=   2
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(11).Locked=   -1  'True
            Columns(12).Width=   3200
            Columns(12).Caption=   "DArticuloAgregado"
            Columns(12).Name=   "AGREGADO"
            Columns(12).Alignment=   2
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            Columns(12).Style=   4
            Columns(12).ButtonsAlways=   -1  'True
            Columns(12).HasBackColor=   -1  'True
            Columns(12).BackColor=   -2147483633
            Columns(13).Width=   688
            Columns(13).Caption=   "Adj"
            Columns(13).Name=   "P"
            Columns(13).DataField=   "Column 13"
            Columns(13).DataType=   8
            Columns(13).FieldLen=   256
            Columns(13).Style=   4
            Columns(13).ButtonsAlways=   -1  'True
            Columns(13).HeadStyleSet=   "Normal"
            Columns(14).Width=   1058
            Columns(14).Caption=   "Espec."
            Columns(14).Name=   "ESPEC"
            Columns(14).DataField=   "Column 14"
            Columns(14).DataType=   8
            Columns(14).FieldLen=   256
            Columns(14).Style=   4
            Columns(14).ButtonsAlways=   -1  'True
            Columns(14).HeadStyleSet=   "Normal"
            Columns(15).Width=   1402
            Columns(15).Caption=   "Atributos"
            Columns(15).Name=   "ATRIBUTOS"
            Columns(15).DataField=   "Column 15"
            Columns(15).DataType=   8
            Columns(15).FieldLen=   256
            Columns(15).Style=   4
            Columns(15).ButtonsAlways=   -1  'True
            Columns(15).HeadStyleSet=   "Normal"
            Columns(16).Width=   1826
            Columns(16).Caption=   "Impuestos"
            Columns(16).Name=   "IMPUESTO"
            Columns(16).DataField=   "Column 16"
            Columns(16).DataType=   8
            Columns(16).FieldLen=   256
            Columns(16).Style=   4
            Columns(16).ButtonsAlways=   -1  'True
            _ExtentX        =   16616
            _ExtentY        =   7646
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin MSComctlLib.TreeView tvwEstrMat 
         Height          =   192
         Left            =   144
         TabIndex        =   19
         Top             =   616
         Width           =   1332
         _ExtentX        =   2328
         _ExtentY        =   344
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         Scroll          =   0   'False
         ImageList       =   "ImageList1"
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin UltraGrid.SSUltraGrid ssMateriales 
         Height          =   4212
         Left            =   120
         TabIndex        =   20
         Top             =   580
         Width           =   9812
         _ExtentX        =   17304
         _ExtentY        =   7408
         _Version        =   131072
         GridFlags       =   17040384
         Images          =   "frmESTRMAT.frx":1A32
         LayoutFlags     =   72351764
         BorderStyle     =   6
         RowConnectorStyle=   2
         BorderStyleCaption=   5
         InterBandSpacing=   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Small Fonts"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Override        =   "frmESTRMAT.frx":1FE5
         Appearance      =   "frmESTRMAT.frx":2087
         Caption         =   "ssMateriales"
         Layouts         =   "frmESTRMAT.frx":20C3
      End
   End
End
Attribute VB_Name = "frmESTRMAT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variable que contendra los articulos
Public m_bDescargarFrm As Boolean
Public g_oArticulos As CArticulos
Private m_bActivado As Boolean
Private m_oArticuloEnEdicion As CArticulo
Private m_oArticulosAModificar As CArticulos
Private m_oUnidades As CUnidades
' Variables de restricciones
Private m_bModifEstr As Boolean
Private m_bModifArti As Boolean
Private m_bModifArtiGen As Boolean
Private m_bModifCodigo As Boolean
Private m_bRProve As Boolean
Private m_bRAtributo As Boolean
Public g_bRComprador As Boolean
Public g_bRCompResp As Boolean
Private m_bRestrMantUsu As Boolean
Private m_bRestrMantPerf As Boolean
'Variables para la grid
Private m_bModoEdicion As Boolean
Private m_lIndiceArticulos As Long
Private m_bAnyaError As Boolean
Private m_bModError As Boolean
Private m_bValError As Boolean
Private m_oIBAseDatosEnEdicion As IBaseDatos
Private m_bRespetar As Boolean
' Variables para interactuar con otros forms
Public g_oGMN1Seleccionado As CGrupoMatNivel1
Public g_oGMN2Seleccionado As CGrupoMatNivel2
Public g_oGMN3Seleccionado As CGrupoMatNivel3
Public g_oGMN4Seleccionado As CGrupoMatNivel4
Public oIBaseDatos As IBaseDatos
Private m_oGruposMat4 As CGruposMatNivel4
Private m_arrIdiomas()  As Variant
Private m_arrIdiomasDen()  As Variant
' Variable de control de flujo
Public g_bCodigoCancelar As Boolean
Public g_sCodigoNuevo As String
Public Accion As accionessummit
Private AccionArti As accionessummit
Private m_CargarComboDesde As Boolean
Public m_DesactivarTabClick As Boolean
'Variable para el orden den listado
Private m_sOrdenListado As String
Private m_sOrderAscDesc As String
'Variables de idiomas
Private m_sIdiTitulos(1 To 6) As String
Private m_sIdiModos(2) As String
Private m_sIdiCod As String
Private m_sIdiMat As String
Private m_sIdiCompProvPlan As String
Private m_sIdiCompProv As String
Private m_sIdiCompPlan As String
Private m_sIdiProvPlan As String
Private m_sIdiComp As String
Private m_sIdiProv As String
Private m_sIdiPlan As String
Private m_sIdiArt As String
Private m_sIdiDen As String
Private m_sIdiUniPDef As String
Private m_sIdiCant As String
Private m_sIdiAdd As String
Private m_sIdiModif As String
Private m_sIdiDetalle As String
Private m_sIdiOrden As String
Private m_sIdiEspec As String
Private m_sIdiArticulo As String
Private sIdiSincronizado As String
Private sIdiNoSincronizado As String
Private sTipoRecepcion As String
Private sRecepcionCantidad As String
Private sRecepcionImporte As String
Private m_sVariasUnidades As String
Private m_sCantidadModificable As String
'Variable que tiene el Asunto para la Notificaci�n de Alta de Material de GS
Private sNotificarAltamat As String
Public g_sCodBusqueda As String
Private m_sTextConcepAlmacRecep() As String
Private m_sIdiConcepto As String
Private m_sIdiAlmacenable As String
Private m_sIdiRecepcionable As String
Private m_aBookmarks As Collection
Private m_sTextImpuestos As String
Private m_bConsultaImpuesto As Boolean
Private m_bModifImpuesto As Boolean
Private m_bHaCambiadoUon As Boolean
Private m_oGMNSeleccionado As Object
Private m_oUonsIntegradas As CUnidadesOrganizativas
Private m_sMsgError As String

Public Property Get ArticuloSeleccionado() As CArticulo
    If sdbgArticulos.IsAddRow Then
        If g_oArticulos.Count > 0 Then
            Set ArticuloSeleccionado = g_oArticulos.Item(CStr(g_oArticulos.Count - 1))
        Else
            Set ArticuloSeleccionado = oFSGSRaiz.Generar_CArticulo
        End If
    ElseIf Not IsNull(sdbgArticulos.Bookmark) Then
        Set ArticuloSeleccionado = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark))
    Else
        Set ArticuloSeleccionado = oFSGSRaiz.Generar_CArticulo
    End If
End Property

Public Property Get ArticuloEnEdicion() As CArticulo
    
    If g_oArticulos.Count > 0 Then
        Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(g_oArticulos.Count - 1))
    Else
        Set m_oArticuloEnEdicion = oFSGSRaiz.Generar_CArticulo
    End If
    Set ArticuloEnEdicion = m_oArticuloEnEdicion
End Property
Public Sub CambiarCodigo()
    Dim teserror As TipoErrorSummit
    If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then
        Exit Sub
    End If
    Screen.MousePointer = vbHourglass
    
    Select Case ssMateriales.ActiveRow.Band.Index
    
    Case 0  'GMN1
    
        Set oIBaseDatos = Nothing
        Set g_oGMN1Seleccionado = Nothing
        Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
        ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
        g_oGMN1Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
        
        g_oGMN1Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
        
        
        Set oIBaseDatos = g_oGMN1Seleccionado
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then ssMateriales.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = gParametrosGenerales.gsDEN_GMN1 & " (C�digo)"
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodGMN1
        frmMODCOD.txtCodAct.Text = g_oGMN1Seleccionado.Cod
        Set frmMODCOD.fOrigen = frmESTRMAT
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
        
        If Not ValidarCodigo(g_sCodigoNuevo, g_oGMN1Seleccionado.Cod) Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click
                    
    Case 1  'GMN2
        
        Set g_oGMN2Seleccionado = Nothing
        Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
        g_oGMN2Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
        g_oGMN2Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
         ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
        g_oGMN2Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
   
        Set oIBaseDatos = g_oGMN2Seleccionado
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then ssMateriales.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = gParametrosGenerales.gsDEN_GMN2 & " (C�digo)"
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodGMN2
        frmMODCOD.txtCodAct.Text = g_oGMN2Seleccionado.Cod
        Set frmMODCOD.fOrigen = frmESTRMAT
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If Not ValidarCodigo(g_sCodigoNuevo, g_oGMN2Seleccionado.Cod) Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click
                                            
    Case 2  'GMN3
        
        Set g_oGMN3Seleccionado = Nothing
        Set g_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
        g_oGMN3Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
        g_oGMN3Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
        g_oGMN3Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
        ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
        g_oGMN3Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
      
        Set oIBaseDatos = g_oGMN3Seleccionado
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then ssMateriales.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = gParametrosGenerales.gsDEN_GMN3 & " (C�digo)"
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodGMN3
        frmMODCOD.txtCodAct.Text = g_oGMN3Seleccionado.Cod
        Set frmMODCOD.fOrigen = frmESTRMAT
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If Not ValidarCodigo(g_sCodigoNuevo, g_oGMN3Seleccionado.Cod) Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
    
        cmdRestaurar_Click
                            
    Case 3 ''GMN4
    
        Set g_oGMN4Seleccionado = Nothing
        Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
        g_oGMN4Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
        g_oGMN4Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
        g_oGMN4Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
        g_oGMN4Seleccionado.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
        
        ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
        g_oGMN4Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
      
        Set oIBaseDatos = g_oGMN4Seleccionado
        
        teserror = oIBaseDatos.IniciarEdicion
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            If Me.Visible Then ssMateriales.SetFocus
            Exit Sub
        End If
        
        oIBaseDatos.CancelarEdicion
                
        frmMODCOD.caption = gParametrosGenerales.gsDEN_GMN4 & " (C�digo)"
        frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodGMN4
        frmMODCOD.txtCodAct.Text = g_oGMN4Seleccionado.Cod
        Set frmMODCOD.fOrigen = frmESTRMAT
        g_bCodigoCancelar = False
        Screen.MousePointer = vbNormal
        frmMODCOD.Show 1
            
        DoEvents
        
        If g_bCodigoCancelar = True Then Exit Sub
            
        If Not ValidarCodigo(g_sCodigoNuevo, g_oGMN4Seleccionado.Cod) Then
            Exit Sub
        End If
        
        Screen.MousePointer = vbHourglass
        teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            basErrores.TratarError teserror
            Exit Sub
        End If

        ''' Actualizar datos
        cmdRestaurar_Click
        
    End Select
    
    Screen.MousePointer = vbNormal

End Sub

''' <summary>Comprueba si el c�digo nuevo de un GMN es correcto</summary>
''' <param name="sCodigoActual">c�digo actual</param>
''' <param name="sCodigoNuevo">c�digo nuevo</param>
''' <returns>Booleano indicando si el c�digo es correcto</returns>
''' <remarks>Llamada desde: CambiarCodigo; Tiempo m�ximo</remarks>

Private Function ValidarCodigo(ByVal sCodigoNuevo As String, Optional ByVal sCodigoActual As Variant) As Boolean
    Dim bOk As Boolean
    
    bOk = True
    
    If (sCodigoNuevo = "") Then
        bOk = False
    End If
    If bOk And Not IsMissing(sCodigoActual) Then
        If UCase(sCodigoNuevo) = UCase(sCodigoActual) Then
            bOk = False
        End If
    End If
    If bOk And gParametrosGenerales.gbValidaGMN Then
        If Not ComprobarCaracteresValidos(sCodigoNuevo) Then
            bOk = False
        End If
    End If
    
    If Not bOk Then oMensajes.NoValido m_sIdiCod
    
    ValidarCodigo = bOk
End Function

Private Sub chkVerIdiomas_Click()
    LockWindowUpdate Me.hWnd
    
    If chkVerIdiomas.Value = vbChecked Then  'Multiidioma
        ssMateriales.Layout.Load App.Path & "\LayoutMaterialesMultiIdioma.ugd", ssPersistenceTypeFile, ssPropCatAll, True
    Else
        ssMateriales.Layout.Load App.Path & "\LayoutMaterialesUndioma.ugd", ssPersistenceTypeFile, ssPropCatAll, True
    End If
    
    ssMateriales.Refresh ssRefreshDisplay
    InicializarGrid chkVerIdiomas.Value
    ssMateriales.CollapseAll
    
    LockWindowUpdate 0&
End Sub

Public Sub cmdA�adir_Click()
    Dim oRow As SSRow
    Dim scod2 As String
    Dim scod3 As String
    'comprobar si ya estamos a�adiendo, para evitar el casque
    
    If Not ssMateriales.ActiveRow Is Nothing Then
        If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then Exit Sub
        
        Select Case ssMateriales.ActiveRow.Band.Index
            Case 0
                Accion = accionessummit.ACCGMN2Anya
            Case 1
                Accion = accionessummit.ACCGMN3Anya
            Case 2
                Accion = accionessummit.ACCGMN4Anya
        End Select
    Else
        'Est� situado en el nodo Raiz de materiales:
        Accion = accionessummit.ACCGMN1Anya
    End If
    
    LockWindowUpdate Me.hWnd
    
    'si hay sincronizaci�n y s�lo estamos viendo el idioma de la aplicaci�n se cambiar� el layout para ver todos.
    'Cuando terminemos de a�adir el material se volver� a ver solo un idioma:
    If gParametrosGenerales.gbSincronizacionMat Then
        If chkVerIdiomas.Value = vbUnchecked Then
            Select Case Accion
                Case accionessummit.ACCGMN3Anya
                    scod2 = ssMateriales.ActiveRow.Cells("COD").Value
                    
                Case accionessummit.ACCGMN4Anya
                    scod2 = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                    scod3 = ssMateriales.ActiveRow.Cells("COD").Value
            End Select
            
            ssMateriales.Layout.Load App.Path & "\LayoutMaterialesMultiIdioma.ugd", ssPersistenceTypeFile, ssPropCatAll, True
            ssMateriales.Refresh ssRefreshDisplay
            InicializarGrid (True)
            ssMateriales.CollapseAll
            If Accion <> accionessummit.ACCGMN1Anya Then
                ssMateriales.ActiveRow.Expanded = True
            End If
            
            'Hay que hacerlo as�,porque cuando se cambia de layout las ramas hijas pierden el foco,y aunque se guarde previamente en un
            'objeto ssrow este tambi�n pierde la referencia.Por lo tanto para a�adir filas de nivel 3 y 4 se har� as�:
            Select Case Accion
                Case accionessummit.ACCGMN3Anya
                    Set oRow = ssMateriales.ActiveRow.GetChild(ssChildRowFirst)
                    If oRow.Cells("COD").Value <> scod2 Then
                        Do While oRow.HasNextSibling
                            Set oRow = oRow.GetSibling(ssSiblingRowNext)
                            If oRow.Cells("COD").Value = scod2 Then Exit Do
                        Loop
                    End If
                    ssMateriales.ActiveRow = oRow
                    
                Case accionessummit.ACCGMN4Anya
                    Set oRow = ssMateriales.ActiveRow.GetChild(ssChildRowFirst)
                    If oRow.Cells("COD").Value <> scod2 Then
                        Do While oRow.HasNextSibling
                            Set oRow = oRow.GetSibling(ssSiblingRowNext)
                            If oRow.Cells("COD").Value = scod2 Then Exit Do
                        Loop
                    End If
                    Set oRow = oRow.GetChild(ssChildRowFirst)
                    If oRow.Cells("COD").Value <> scod3 Then
                        Do While oRow.HasNextSibling
                            Set oRow = oRow.GetSibling(ssSiblingRowNext)
                            If oRow.Cells("COD").Value = scod3 Then Exit Do
                        Loop
                    End If
                    ssMateriales.ActiveRow = oRow
            End Select
        End If
    End If
    
    'a�ade una nueva fila a la grid:
    If Not ssMateriales.ActiveRow Is Nothing Then
        ssMateriales.Bands(ssMateriales.ActiveRow.Band.Index + 1).AddNew
    Else
        ssMateriales.Bands(0).AddNew
    End If
    
    'Pone el foco en el campo c�digo de la nueva fila:
    ssMateriales.ActiveRow.Cells("COD").Column.Activation = ssActivationAllowEdit
    ssMateriales.Selected.Cells.clear
    Set ssMateriales.ActiveCell = ssMateriales.ActiveRow.Cells("COD")
    If Me.Visible Then ssMateriales.SetFocus
    ssMateriales.PerformAction ssKeyActionEnterEditMode
    ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value = 0
    ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value = False
    
    picNavigate.Visible = False
    picEdicion.Visible = True
    
    LockWindowUpdate 0&
End Sub

Private Sub cmdBuscar_Click()
    If Not ssMateriales.ActiveRow Is Nothing Then
        frmESTRMATBuscar.sGMN1Cod = vbNullString
        frmESTRMATBuscar.sGMN2Cod = vbNullString
        frmESTRMATBuscar.sGMN3Cod = vbNullString
        frmESTRMATBuscar.sGMN4Cod = vbNullString
        
        Select Case ssMateriales.ActiveRow.Band.Index
            Case 0  'GMN1
                frmESTRMATBuscar.sGMN1Cod = ssMateriales.ActiveRow.Cells("COD")
            Case 1  'GMN2
                frmESTRMATBuscar.sGMN1Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                frmESTRMATBuscar.sGMN2Cod = ssMateriales.ActiveRow.Cells("COD")
            Case 2  'GMN3
                frmESTRMATBuscar.sGMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                frmESTRMATBuscar.sGMN2Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                frmESTRMATBuscar.sGMN3Cod = ssMateriales.ActiveRow.Cells("COD")
            Case 3  'GMN4
                frmESTRMATBuscar.sGMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
                frmESTRMATBuscar.sGMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                frmESTRMATBuscar.sGMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                frmESTRMATBuscar.sGMN4Cod = ssMateriales.ActiveRow.Cells("COD")
        End Select
                
        If gParametrosGenerales.gbSincronizacionMat Then
            frmESTRMATBuscar.sGMNDen = ssMateriales.ActiveRow.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
        Else
            frmESTRMATBuscar.sGMNDen = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
        End If
    End If
    
    frmESTRMATBuscar.Show 1
End Sub

Private Sub cmdCodigo_Click()
    CambiarCodigo
End Sub

Private Sub cmdDeshacer_Click()
    SendKeys "{ESC}", True
    
    If Accion = accionessummit.ACCGMN1Anya Or Accion = accionessummit.ACCGMN2Anya Or Accion = accionessummit.ACCGMN3Anya Or Accion = accionessummit.ACCGMN4Anya Then
        ssMateriales.ActiveRow.Delete
        
        If chkVerIdiomas.Value = vbUnchecked And gParametrosGenerales.gbSincronizacionMat = True Then
            LockWindowUpdate Me.hWnd
            
            ssMateriales.Layout.Load App.Path & "\LayoutMaterialesUndioma.ugd", ssPersistenceTypeFile, ssPropCatAll, True
            ssMateriales.Refresh ssRefreshDisplay
            InicializarGrid False
            ssMateriales.CollapseAll
            If Not ssMateriales.ActiveRow Is Nothing Then
                ssMateriales.ActiveRow.Expanded = True
            End If
            
            LockWindowUpdate 0&
        End If
    End If
    
    picNavigate.Visible = True
    picEdicion.Visible = False
    Accion = ACCGMCon
End Sub

Public Sub cmdEli_Click()
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    Dim oICompProveAsig As ICompProveAsignados
    Dim bHayProve As Boolean
    Dim bHayComp As Boolean
    Dim Plantillas As CPlantillas
    Dim i As Integer
    Dim ListaGmn As String
    
    If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then Exit Sub
    If Not ssMateriales.ActiveRow Is Nothing Then
        
    If gParametrosGenerales.gbSincronizacionMat = True Then
        ReDim arrDenIdiomas(UBound(m_arrIdiomas))
        For i = 1 To UBound(arrDenIdiomas)
            arrDenIdiomas(i) = ssMateriales.ActiveRow.Cells("DEN_" & m_arrIdiomas(i)).Value
        Next i
    End If
    
    Select Case ssMateriales.ActiveRow.Band.Index
        Case 0  'GMN1
            Accion = ACCGMN1Eli
            
            Set g_oGMN1Seleccionado = Nothing
            Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
            If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then
                Exit Sub
            Else
                g_oGMN1Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            End If
            ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
            g_oGMN1Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
            If gParametrosGenerales.gbSincronizacionMat Then
                g_oGMN1Seleccionado.CodIdiomas = m_arrIdiomas
                g_oGMN1Seleccionado.DenIdiomas = arrDenIdiomas
            End If
            g_oGMN1Seleccionado.tipoRecepcion = ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value
            g_oGMN1Seleccionado.CantidadPedidoModificable = SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
            
            Set oIBaseDatos = g_oGMN1Seleccionado
            
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.IniciarEdicion
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiMat & " " & CStr(g_oGMN1Seleccionado.Cod) & " (" & g_oGMN1Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    'Mirar si existen compradores asignados
                    Set oICompProveAsig = g_oGMN1Seleccionado
                    If (oICompProveAsig.DevolverCompradoresAsignados.Count) <> 0 Then bHayComp = True
                    
                    'Mirar si existen proveedores asignados
                    oICompProveAsig.BuscarProveedoresDesde 1, , , , , , , , , , , , , , False
                    If oICompProveAsig.Proveedores.Count <> 0 Then bHayProve = True
                    
                    Set oICompProveAsig = Nothing
                    
                    'Mirar si hay plantilla
                    Set Plantillas = oFSGSRaiz.Generar_CPlantillas
                    If Plantillas.ExisteRestriccionMaterial(g_oGMN1Seleccionado.Cod, ListaGmn) Then
                        Call oMensajes.MaterialPlantillaNoEliminando(m_sIdiPlan, ListaGmn)
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    If bHayComp And bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiCompProv)
                    ElseIf bHayComp Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiComp)
                    ElseIf bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiProv)
                    End If
                    
                    If irespuesta = vbNo Then
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.FinalizarEdicionEliminando
                    Screen.MousePointer = vbNormal
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        EliminarGMDeEstructura
                        RegistrarAccion ACCGMN1Eli, "Cod:" & CStr(g_oGMN1Seleccionado.Cod)
                        Set oIBaseDatos = Nothing
                        Set g_oGMN1Seleccionado = Nothing
                    End If
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set g_oGMN1Seleccionado = Nothing
                End If
            
            Else
                'Ha habido un error al iniciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN1Seleccionado = Nothing
                Exit Sub
            End If
        
        Case 1 'GMN2
            Accion = ACCGMN2Eli
            
            Set g_oGMN2Seleccionado = Nothing
            Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
            g_oGMN2Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            g_oGMN2Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
            ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
            g_oGMN2Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
            If gParametrosGenerales.gbSincronizacionMat Then
                g_oGMN2Seleccionado.CodIdiomas = m_arrIdiomas
                g_oGMN2Seleccionado.DenIdiomas = arrDenIdiomas
            End If
            g_oGMN2Seleccionado.tipoRecepcion = ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value
            g_oGMN2Seleccionado.CantidadPedidoModificable = SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
             
            Set oIBaseDatos = g_oGMN2Seleccionado
            
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.IniciarEdicion
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiMat & " " & CStr(g_oGMN2Seleccionado.Cod) & " (" & g_oGMN2Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    'Mirar si existen compradores asignados
                    Set oICompProveAsig = g_oGMN2Seleccionado
                    If (oICompProveAsig.DevolverCompradoresAsignados.Count) <> 0 Then bHayComp = True
                    
                    'Mirar si existen proveedores asignados
                    oICompProveAsig.BuscarProveedoresDesde 1, , , , , , , , , , , , , , False
                    If oICompProveAsig.Proveedores.Count <> 0 Then bHayProve = True
                    
                    Set oICompProveAsig = Nothing
                    
                    'Mirar si hay plantilla
                    Set Plantillas = oFSGSRaiz.Generar_CPlantillas
                    If Plantillas.ExisteRestriccionMaterial(g_oGMN2Seleccionado.GMN1Cod, ListaGmn, g_oGMN2Seleccionado.Cod) Then
                        Call oMensajes.MaterialPlantillaNoEliminando(m_sIdiPlan, ListaGmn)
                        
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    If bHayComp And bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiCompProv)
                    ElseIf bHayComp Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiComp)
                    ElseIf bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiProv)
                    End If
                    
                    If irespuesta = vbNo Then
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.FinalizarEdicionEliminando
                    Screen.MousePointer = vbNormal
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        EliminarGMDeEstructura
                        RegistrarAccion ACCGMN2Eli, "CodGMN1:" & CStr(g_oGMN2Seleccionado.GMN1Cod) & "CodGMN2:" & CStr(g_oGMN2Seleccionado.Cod)
                        Set oIBaseDatos = Nothing
                        Set g_oGMN1Seleccionado = Nothing
                    End If
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set g_oGMN1Seleccionado = Nothing
                End If
            Else
                'Ha habido un error al iniciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN2Seleccionado = Nothing
                Exit Sub
            End If
            
        Case 2  'GMN3
             Accion = ACCGMN3Eli
            
            Set g_oGMN3Seleccionado = Nothing
            Set g_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
            g_oGMN3Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            g_oGMN3Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
            g_oGMN3Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
            ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
            g_oGMN3Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
            If gParametrosGenerales.gbSincronizacionMat Then
                g_oGMN3Seleccionado.CodIdiomas = m_arrIdiomas
                g_oGMN3Seleccionado.DenIdiomas = arrDenIdiomas
            End If
            g_oGMN3Seleccionado.tipoRecepcion = ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value
            g_oGMN3Seleccionado.CantidadPedidoModificable = SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
            
            Set oIBaseDatos = g_oGMN3Seleccionado
            
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.IniciarEdicion
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiMat & " " & CStr(g_oGMN3Seleccionado.Cod) & " (" & g_oGMN3Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    'Mirar si existen compradores asignados
                    Set oICompProveAsig = g_oGMN3Seleccionado
                    If (oICompProveAsig.DevolverCompradoresAsignados.Count) <> 0 Then bHayComp = True
                    
                    'Mirar si existen proveedores asignados
                    oICompProveAsig.BuscarProveedoresDesde 1, , , , , , , , , , , , , , False
                    If oICompProveAsig.Proveedores.Count <> 0 Then bHayProve = True
                    
                    Set oICompProveAsig = Nothing
                    
                    'Mirar si hay plantilla
                    Set Plantillas = oFSGSRaiz.Generar_CPlantillas
                    If Plantillas.ExisteRestriccionMaterial(g_oGMN3Seleccionado.GMN1Cod, ListaGmn, g_oGMN3Seleccionado.GMN2Cod, g_oGMN3Seleccionado.Cod) Then
                        Call oMensajes.MaterialPlantillaNoEliminando(m_sIdiPlan, ListaGmn)
                        
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    If bHayComp And bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiCompProv)
                    ElseIf bHayComp Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiComp)
                    ElseIf bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiProv)
                    End If
                    
                    If irespuesta = vbNo Then
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.FinalizarEdicionEliminando
                    Screen.MousePointer = vbNormal
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        EliminarGMDeEstructura
                       RegistrarAccion ACCGMN3Eli, "CodGMN1:" & CStr(g_oGMN3Seleccionado.GMN1Cod) & "CodGMN2:" & CStr(g_oGMN3Seleccionado.GMN2Cod) & "CodGMN3:" & CStr(g_oGMN3Seleccionado.Cod)
                        Set oIBaseDatos = Nothing
                        Set g_oGMN3Seleccionado = Nothing
                    End If
                
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set g_oGMN3Seleccionado = Nothing
                End If
                
            Else
                'Ha habido un error al inciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN3Seleccionado = Nothing
                Exit Sub
            End If
            
        Case 3   'GMN4
            Accion = ACCGMN4Eli
            Set g_oGMN4Seleccionado = Nothing
            Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
            g_oGMN4Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            g_oGMN4Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
            ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
            g_oGMN4Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
            If gParametrosGenerales.gbSincronizacionMat = True Then
                g_oGMN4Seleccionado.CodIdiomas = m_arrIdiomas
                g_oGMN4Seleccionado.DenIdiomas = arrDenIdiomas
            End If
            g_oGMN4Seleccionado.tipoRecepcion = ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value
            g_oGMN4Seleccionado.CantidadPedidoModificable = SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
            
            Set oIBaseDatos = g_oGMN4Seleccionado
            
            Screen.MousePointer = vbHourglass
            teserror = oIBaseDatos.IniciarEdicion
            Screen.MousePointer = vbNormal
            
            If teserror.NumError = TESnoerror Then
                irespuesta = oMensajes.PreguntaEliminar(m_sIdiMat & " " & CStr(g_oGMN4Seleccionado.Cod) & " (" & g_oGMN4Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    'Mirar si hay plantilla
                    '05/03/2008 cuando se vaya a eliminar un material, se comprueba si est� en plantillas, si es as�
                    'no dejar� eliminar el material, antes nos obligar� a quitarlo de las plantillas mediante un
                    'mensaje informativo que nos diga cuales son las plantillas en las que est� el material que
                    'queremos eliminar
                    Set Plantillas = oFSGSRaiz.Generar_CPlantillas
                    If Plantillas.ExisteRestriccionMaterial(g_oGMN4Seleccionado.GMN1Cod, ListaGmn, g_oGMN4Seleccionado.GMN2Cod, g_oGMN4Seleccionado.GMN3Cod, g_oGMN4Seleccionado.Cod) Then
                        Call oMensajes.MaterialPlantillaNoEliminando(m_sIdiPlan, ListaGmn)
                        
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    'Mirar si existen compradores asignados
                    Set oICompProveAsig = g_oGMN4Seleccionado
                    If (oICompProveAsig.DevolverCompradoresAsignados.Count) <> 0 Then bHayComp = True
                    
                    'Mirar si existen proveedores asignados
                    oICompProveAsig.BuscarProveedoresDesde 1, , , , , , , , , , , , , , False
                    If oICompProveAsig.Proveedores.Count <> 0 Then bHayProve = True
                    
                    Set oICompProveAsig = Nothing
                    
                    If bHayComp And bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiCompProv)
                    ElseIf bHayComp Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiComp)
                    ElseIf bHayProve Then
                        irespuesta = oMensajes.PreguntaContinuarEliminando(m_sIdiProv)
                    End If
                    
                    If irespuesta = vbNo Then
                        oIBaseDatos.CancelarEdicion
                        Exit Sub
                    End If
                    
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.FinalizarEdicionEliminando
                    Screen.MousePointer = vbNormal
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        EliminarGMDeEstructura
                        RegistrarAccion ACCGMN4Eli, "CodGMN1:" & CStr(g_oGMN4Seleccionado.GMN1Cod) & "CodGMN2:" & CStr(g_oGMN4Seleccionado.GMN2Cod) & "CodGMN3:" & CStr(g_oGMN4Seleccionado.GMN3Cod) & "Cod:" & CStr(g_oGMN4Seleccionado.Cod)
                        Set oIBaseDatos = Nothing
                        Set g_oGMN4Seleccionado = Nothing
                    End If
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set g_oGMN4Seleccionado = Nothing
                End If
            Else
                ' Ha habido un error al inciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN4Seleccionado = Nothing
                Exit Sub
            End If
        End Select
    
    End If
    Accion = ACCGMCon
End Sub

Private Sub cmdEliminarArticulo_Click()
    
    ''' * Objetivo: Eliminar articulos
    Dim vItems As Variant
    Dim irespuesta As Integer
    Dim bhayArticuloConPresup As Boolean
    Dim i As Integer
    Dim oArticulo As CArticulo
    Dim teserror As TipoErrorSummit

        
    If sdbgArticulos.Rows = 0 Then Exit Sub
    If IsNull(sdbgArticulos.Bookmark) Then Exit Sub
    'sdbgArticulos.SelBookmarks.RemoveAll ' Elimino pq de momento solo se elimina uno y al poder seleccionar varios da casque
    sdbgArticulos.SelBookmarks.Add sdbgArticulos.Bookmark

    Select Case sdbgArticulos.SelBookmarks.Count
        Case 0
            Screen.MousePointer = vbNormal
            Exit Sub
        Case 1
             irespuesta = oMensajes.PreguntaEliminar(m_sIdiArt & " " & sdbgArticulos.Columns("COD").Value & " (" & sdbgArticulos.Columns("DEN").Value & ")")
        Case Is > 1
             irespuesta = oMensajes.PreguntaEliminarMultiplesArticulos
    End Select
        
    
    If irespuesta = vbNo Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    i = 0
'    'compruebo si existen presupuestos para ese art�culo y unidad:
    While i < sdbgArticulos.SelBookmarks.Count
        Set oArticulo = g_oArticulos.Item(CStr(sdbgArticulos.SelBookmarks(i)))
        If Not oArticulo Is Nothing Then
            If oArticulo.ExistenPresupuestos = True Then
                bhayArticuloConPresup = True
            End If
        End If
        Set oArticulo = Nothing
        i = i + 1
        
    Wend
    
    If bhayArticuloConPresup Then
        If sdbgArticulos.SelBookmarks.Count = 1 Then
            irespuesta = oMensajes.PreguntaEliminarArticuloConPresup
        Else
            irespuesta = oMensajes.PreguntaEliminarMultiplesArticulosConPresup
        End If
        If irespuesta = vbNo Then
            Exit Sub
        End If
    
    End If
                                
    ReDim aIdentificadores(sdbgArticulos.SelBookmarks.Count, 1)
    i = 0
    While i < sdbgArticulos.SelBookmarks.Count
        aIdentificadores(i + 1, 1) = g_oArticulos.Item(CStr(sdbgArticulos.SelBookmarks(i))).Cod
        i = i + 1
    Wend
    
    Screen.MousePointer = vbHourglass
    teserror = g_oArticulos.EliminarMultiplesArticulosDeBaseDatos(aIdentificadores, basOptimizacion.gvarCodUsuario)
    
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        
        vItems = teserror.Arg1
        oMensajes.ImposibleEliminacionMultipleArticulos vItems
            
        sdbgArticulos.SelBookmarks.RemoveAll
        
        CargarGridArticulos
        sdbgArticulos.ReBind
        

    Else
        basSeguridad.RegistrarAccion accionessummit.ACCArtEli, "Cod:" & sdbgArticulos.Columns("COD").Text
        sdbgArticulos.SelBookmarks.RemoveAll
        CargarGridArticulos
        sdbgArticulos.ReBind
   
    End If
           

End Sub

Private Sub cmdFiltrarArticulo_Click()
    
    ''' * Objetivo: Activar el formulario de filtrar.
        
    frmARTFiltrar.sGMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
    frmARTFiltrar.sGMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
    frmARTFiltrar.sGMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
    frmARTFiltrar.sGMN4Cod = ssMateriales.ActiveRow.Cells("COD").Value
    frmARTFiltrar.WindowState = vbNormal
    frmARTFiltrar.Show 1

End Sub

Private Sub cmdListadoArticulo_Click()

    ''' * Objetivo: Obtener un listado
    
    frmLstART.optEst.Value = True
    frmLstART.PonerMatSeleccionado ("frmESTRMATART")
    frmLstART.CargarAtributos
    frmLstART.optOrdEst.Value = True
    frmLstART.chkArtDet.Value = vbChecked
        
    frmLstART.Show 1
    
End Sub

Private Sub cmdRestaurar_Click()
    
    CargarGridMateriales MDI.mnuPopUpEstrMatOrdPorCod.Checked
    ssMateriales.CollapseAll
    
    ConfigurarInterfazSeguridad -1
    
    
End Sub


Private Sub cmdReubicar_Click()

    If sdbgArticulos.Rows = 0 Then Exit Sub
    If IsNull(sdbgArticulos.Bookmark) Then Exit Sub
    
    'If IsNull(sdbgArticulos.Bookmark) Then
    '    sdbgArticulos.Bookmark = 0
    'End If
    
    ''' Resaltar el Articulo actual
    sdbgArticulos.SelBookmarks.Add sdbgArticulos.Bookmark

    frmSELMAT.sOrigen = "frmESTRMAT"
    frmSELMAT.bRComprador = g_bRComprador
    frmSELMAT.Show 1
    
End Sub

Private Sub Form_Activate()
    On Error GoTo Error
    
    If m_bDescargarFrm Then
        Unload Me
        Exit Sub
    End If
    
    If Not m_bActivado Then m_bActivado = True

    Exit Sub
Error:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmESTRMAT", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    Dim sCaption As String
    Dim iIdi As Integer
    Dim oIdioma As CIdioma
    Dim oIdiomas As CIdiomas
    
    On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    ReDim m_sTextConcepAlmacRecep(2, 2)
        
    Me.Height = 6000
    Me.Width = 10350
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
        
    CargarRecursos
    
    PonerFieldSeparator Me
    
    sCaption = Me.caption
    Me.caption = m_sIdiTitulos(5)
    tvwEstrMat.Nodes.Add , , "Raiz", m_sIdiTitulos(3), "Raiz"
    
    SSTabEstrMat.TabVisible(1) = False
    
    If FSEPConf Then sdbgArticulos.Columns("P").Visible = False
    
    ConfigurarSeguridad

    'Carga la ultragrid de materiales:
    CargarGridMateriales (True)
    ssMateriales.Layout.Load App.Path & "\LayoutMaterialesUndioma.ugd", ssPersistenceTypeFile, ssPropCatAll
    ssMateriales.CollapseAll
    
    If Not gParametrosGenerales.gbSincronizacionMat Then   'si no hay sincronizaci�n de materiales/actividades no se ver�n los idiomas.
        chkVerIdiomas.Visible = False
    Else
        Set oIdiomas = oGestorParametros.DevolverIdiomas(False, True, False)
        iIdi = 1
        For Each oIdioma In oIdiomas
            ReDim Preserve m_arrIdiomas(iIdi)
            m_arrIdiomas(iIdi) = oIdioma.Cod
            ReDim Preserve m_arrIdiomasDen(iIdi)
            m_arrIdiomasDen(iIdi) = oIdioma.Den
            iIdi = iIdi + 1
        Next
    End If
    ' edu 20071010 tarea 436 ocultar columna Generico cuando el par�metro vale falso
    
    On Error Resume Next
    If Not gParametrosGenerales.gbArticulosGenericos Then
        sdbgArticulos.Columns("GEN").Visible = False
    Else
        sdbgArticulos.Columns("GEN").Visible = True
    End If
    On Error GoTo 0
    
    If Not m_bRAtributo Then
        frmESTRMAT.Width = frmESTRMAT.Width - 500
        SSTabEstrMat.Width = SSTabEstrMat.Width - 500
        Picture1.Width = Picture1.Width - 500
        sdbgArticulos.Width = sdbgArticulos.Width - 500
        cmdModoEdicion.Left = cmdModoEdicion.Left - 500
    End If
    
    Arrange
        
    Me.Show
    DoEvents
    
    cmdCodigo.Enabled = False
    cmdA�adirArticulo.Enabled = False
    cmdEliminarArticulo.Enabled = False
    cmdListadoArticulo.Enabled = False
    cmdRestaurarArticulo.Enabled = False
    cmdModoEdicion.Enabled = False
    cmdA�adir.Enabled = True
    cmdEli.Enabled = True
    cmdListado.Enabled = True
    cmdRestaurar.Enabled = True

    'Cargamos las unidades
    Set m_oUnidades = oFSGSRaiz.Generar_CUnidades
    Screen.MousePointer = vbHourglass
    
    m_oUnidades.CargarTodasLasUnidades , , , , False
    CargarGridConUnidades
    
    AccionArti = ACCArtCon

    Me.caption = sCaption
    
    sdbgArticulos.Columns("UNI").DropDownHwnd = sdbddUnidades.hWnd
    sdbgArticulos.Columns("CONCEP").DropDownHwnd = ssdbddConcepto.hWnd
    sdbgArticulos.Columns("ALMAC").DropDownHwnd = ssdbddAlmacen.hWnd
    sdbgArticulos.Columns("RECEP").DropDownHwnd = ssdbddRecepcion.hWnd
    sdbgArticulos.Columns("TIPORECEPCION").Style = ssStyleCheckBox
    CargarGridConConcepto
    CargarGridConAlmacen
    CargarGridConRecepcion
    
    If (Not m_bConsultaImpuesto) Then
        Me.sdbgArticulos.Columns("IMPUESTO").Visible = False
    End If
    
    'mostrar/ocultar columnas referidas a articulos centrales en funci�n del par�metro general
    If gParametrosGenerales.gbArticulosCentrales Then
        Me.sdbgArticulos.Columns("ART").Visible = True
        Me.sdbgArticulos.Columns("AGREGADO").Visible = True
    Else
        Me.sdbgArticulos.Columns("ART").Visible = False
        Me.sdbgArticulos.Columns("AGREGADO").Visible = False
    End If
    Screen.MousePointer = vbNormal
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmESTRMAT", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Configuracion de la pantalla en funcion de las acciones de seguridad</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <revision>LTG 11/12/2012</revision>

Private Sub ConfigurarSeguridad()
    m_bRAtributo = True
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATModificar)) Is Nothing) Then
        m_bModifEstr = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificar)) Is Nothing) Then
        m_bModifArti = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATRestMatComp)) Is Nothing) And basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
        g_bRComprador = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestResponsable)) Is Nothing) Then
        g_bRCompResp = True
    End If
    
    If (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATAtributoModif)) Is Nothing) Then
        m_bRAtributo = False
    End If
    
    If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiAdjCon)) Is Nothing Then
        m_bRProve = True
        sdbgArticulos.Columns("P").Visible = False
    End If

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiModificarGen)) Is Nothing) Then
        m_bModifArtiGen = True
    End If
    m_bModifCodigo = (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATModificarCodigo)) Is Nothing))
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATModificarImpuesto)) Is Nothing) Then
        m_bModifImpuesto = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATConsultaImpuesto)) Is Nothing) Then
        m_bConsultaImpuesto = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonUsu)) Is Nothing) Then
        m_bRestrMantUsu = True
    End If
    
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.MATArtiRestrMantUonPerf)) Is Nothing) Then
        m_bRestrMantPerf = True
    End If
    
    
    ''' Aplicar condiciones de integraci�n para ARTICULOS
    ''' 3016 - Articulos centrales y FSGE: Nuevo expediente Pruebas 32100.1 / 2015 / 71
    ''' Siempre vamos a poder modificar art�culos en general independientemente de la integraci�n
    ''' El control lo vamos a hacer a nivel de art�culo (en funci�n del sentido de integraci�n de los ERP's relacionados
    ''' con las unidades organizativas del �rticulo
    ''' Eliminada g_bModifArtiIntegr
    
    
    ''' Aplicar condiciones de integraci�n para MATERIALES
    ''' Si el sentido es solo de entrada, solo se permitir� la consulta de la estructura
    ''' Si el sentido es de entrada/salida o salida:
    '''                                   se permite el mantenimiento de materiales
    If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Materiales) Then
        m_bModifEstr = False
    End If
    
    
    If (m_bModifArti Or m_bModifArtiGen) Then
        cmdModoEdicion.Visible = True
    Else
        cmdModoEdicion.Visible = False
    End If
    
    If Not m_bModifCodigo Then
        cmdCodigo.Visible = False
        cmdRestaurar.Left = cmdCodigo.Left
        cmdBuscar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 100
        cmdListado.Left = cmdBuscar.Left + cmdBuscar.Width + 100
    End If
    
    If m_bModifEstr Then
        cmdA�adir.Visible = True
        cmdEli.Visible = True
    Else
        cmdA�adir.Visible = False
        cmdEli.Visible = False
        If Not oUsuarioSummit.EsAdmin Then
            cmdRestaurar.Left = ssMateriales.Left
        Else
            cmdCodigo.Left = ssMateriales.Left
            cmdRestaurar.Left = cmdCodigo.Left + cmdCodigo.Width + 100
        End If
        cmdBuscar.Left = cmdRestaurar.Left + cmdRestaurar.Width + 100
        cmdListado.Left = cmdBuscar.Left + cmdBuscar.Width + 100
        cmdRestaurarArticulo.Left = sdbgArticulos.Left
        cmdFiltrarArticulo.Left = cmdRestaurarArticulo.Width + 200
    End If
    
    cmdListado.Visible = True
End Sub


''' <summary>
''' Unload del formulario
''' </summary>
''' <param name="Cancel">Indica si detiene la descarga del formulario</param>
''' <returns></returns>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
Private Sub Form_Unload(Cancel As Integer)
    Dim v As Variant
    
    On Error GoTo Error
    
    If m_bDescargarFrm Then
         m_bDescargarFrm = False
         oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If
    
    If sdbgArticulos.DataChanged And SSTabEstrMat.TabVisible(1) Then
        v = sdbgArticulos.ActiveCell.Value
        SSTabEstrMat.Tab = 1
        If Me.Visible Then sdbgArticulos.SetFocus
        sdbgArticulos.ActiveCell.Value = v
        
        sdbgArticulos.Update
            
        m_bValError = False
        m_bAnyaError = False
        m_bModError = False
        
        If m_bValError Or m_bAnyaError Or m_bModError Then Cancel = True
    End If
    
    Unload frmESTRMATAtrib
    
    Set ssMateriales.DataSource = Nothing

    Erase m_arrIdiomas
    Erase m_arrIdiomasDen
    Erase m_sTextConcepAlmacRecep
    
    Set g_oArticulos = Nothing
    Set m_oArticuloEnEdicion = Nothing
    Set m_oIBAseDatosEnEdicion = Nothing
    Set g_oGMN1Seleccionado = Nothing
    Set g_oGMN2Seleccionado = Nothing
    Set g_oGMN3Seleccionado = Nothing
    Set g_oGMN4Seleccionado = Nothing
    Set m_oUnidades = Nothing
    Set m_oGruposMat4 = Nothing
    Set m_aBookmarks = Nothing
    Set m_oUonsIntegradas = Nothing
    
    
    Accion = ACCArtAdjCon
    m_bModoEdicion = False
    Me.Visible = False
    
    Exit Sub
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmESTRMAT", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub


''' <summary>
''' Segun el caso que sea. Si la celda de la grid dispone de boton envia el foco a otro formulario
''' </summary>
''' <remarks>Tiempo m�ximo:0,3seg.</remarks>
Private Sub sdbgArticulos_BtnClick()

Dim teserror As TipoErrorSummit
Dim vBookmark As Variant
Dim iRow As Long
Dim lAdjProceso As Long
Dim isAlta As Boolean
 If sdbgArticulos.col = -1 Then Exit Sub
    
    Select Case sdbgArticulos.Columns(sdbgArticulos.col).Name

        Case "P"   'Adjudicaciones anteriores
                iRow = sdbgArticulos.Row

                If m_bModoEdicion Then

                    m_bModError = False
                    m_bAnyaError = False
                    m_bValError = False

                    sdbgArticulos.Update
                    DoEvents

                    If m_bModError Or m_bAnyaError Or m_bValError Then
                        Exit Sub
                    End If

                End If


                vBookmark = sdbgArticulos.RowBookmark(iRow)

                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If

                sdbgArticulos.Bookmark = vBookmark

                sdbgArticulos.SelBookmarks.Add vBookmark

                'Comprobamos haber si existen adjudicaciones de summit

                Set frmESTRMATProve.oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt

                Set frmESTRMATProve.oArticulo = Nothing
                Set frmESTRMATProve.oArticulo = g_oArticulos.Item(CStr(sdbgArticulos.Bookmark))

                If frmESTRMATProve.oArticulo Is Nothing Then Exit Sub

                lAdjProceso = frmESTRMATProve.oAdjudicaciones.CargarTodasLasAdjsDeArt(OrdAdjPorFecFin, frmESTRMATProve.oArticulo, False, True)
                frmESTRMATProve.bModoAdjAnteriores = False
                'Si no hay adjudicaciones desde el programa dejo introducir
                If lAdjProceso = 0 Then
'                'No hay adjudicaciones nuevas, luego se podran introducir las antiguas
'                    frmESTRMATProve.oAdjudicaciones.CargarTodasLasAdjsDeArt OrdAdjPorFecFin, g_oArticulos.Item(CStr(sdbgArticulos.Bookmark)), True, True
                    frmESTRMATProve.bModoAdjAnteriores = True
                    frmESTRMATProve.bModoEdicion = m_bModoEdicion
'                Else
'                'Ya hay adjudicaciones de summit.La grid sera de modo lectura.
'                    frmESTRMATProve.bModoAdjAnteriores = False
                End If

                frmESTRMATProve.Show 1
'                If g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).EspAdj = 1 Then
'                        sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
'                    Else
'                        sdbgArticulos.Columns("ESPEC").CellStyleSet ""
'                    End If
                sdbgArticulos.Refresh
                If Me.Visible Then sdbgArticulos.SetFocus
                sdbgArticulos.col = 6
                

        Case "ESPEC" 'Especificaciones
                iRow = sdbgArticulos.Row

                If m_bModoEdicion Then

                    m_bModError = False
                    m_bAnyaError = False
                    m_bValError = False

                    sdbgArticulos.Update
                    DoEvents

                    If m_bModError Or m_bAnyaError Or m_bValError Then
                        Exit Sub
                    End If

                End If

                vBookmark = sdbgArticulos.RowBookmark(iRow)

                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If

                sdbgArticulos.Bookmark = vBookmark

                sdbgArticulos.SelBookmarks.Add vBookmark

                'Si no tiene permisos para modificar los art�culos:
                If m_bModifArti = False Then
                    frmArticuloEsp.txtArticuloEsp.Locked = True
                    frmArticuloEsp.cmdA�adirEsp.Enabled = False
                    frmArticuloEsp.cmdEliminarEsp.Enabled = False
                    frmArticuloEsp.cmdModificarEsp.Enabled = False
                    frmArticuloEsp.cmdSalvarEsp.Enabled = True
                    frmArticuloEsp.cmdAbrirEsp.Enabled = True
                    frmArticuloEsp.cmdRestaurar.Visible = False
                End If
                
                Set frmArticuloEsp.g_oArticulo = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark))
                If frmArticuloEsp.g_oArticulo Is Nothing Then Exit Sub
                ''' Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n
                frmArticuloEsp.g_oArticulo.Usuario = basOptimizacion.gvarCodUsuario
                
                Set frmArticuloEsp.g_oIBaseDatos = frmArticuloEsp.g_oArticulo
                teserror = frmArticuloEsp.g_oIBaseDatos.IniciarEdicion
                If teserror.NumError <> TESnoerror Then
                    basErrores.TratarError teserror
                    Set frmArticuloEsp.g_oIBaseDatos = Nothing
                    Set frmArticuloEsp.g_oArticulo = Nothing
                    Exit Sub
                End If
                frmArticuloEsp.g_oIBaseDatos.CancelarEdicion

                frmArticuloEsp.g_sOrigen = "frmESTRMAT"
                frmArticuloEsp.g_oArticulo.CargarTodasLasEspecificaciones
                If Not sdbgArticulos.IsAddRow Then
                    
                        'Configurar parametros del commondialog
                        frmArticuloEsp.cmmdEsp.FLAGS = cdlOFNHideReadOnly
                        'frmArticuloEsp.cmmdEsp.CancelError = False
                        frmArticuloEsp.caption = m_sIdiEspec & " " & sdbgArticulos.Columns("COD").Value & " " & sdbgArticulos.Columns("DEN").Value
    
                        frmArticuloEsp.g_bRespetarCombo = True
                        frmArticuloEsp.txtArticuloEsp.Text = NullToStr(frmArticuloEsp.g_oArticulo.esp)
                        frmArticuloEsp.g_bRespetarCombo = False
                        frmArticuloEsp.AnyadirEspsALista
                        frmArticuloEsp.Show 1
                        If g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).EspAdj = 1 Then
                            sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
                        Else
                            sdbgArticulos.Columns("ESPEC").CellStyleSet ""
                        End If
                        sdbgArticulos.Refresh
                    
                End If
                If Me.Visible Then sdbgArticulos.SetFocus
                sdbgArticulos.col = 7
                CargarGridArticulos
         Case "ATRIBUTOS" 'Atributos
                iRow = sdbgArticulos.Row
                If m_bModoEdicion Then
                    
                    m_bModError = False
                    m_bAnyaError = False
                    
                    ' Primero acabamos la edicion del articulo
                    sdbgArticulos.Update
                    ' Si se ha producido un error no continuamos
                    If m_bValError Or m_bModError Or m_bAnyaError Then
                        Exit Sub
                    End If
                End If
                
                vBookmark = sdbgArticulos.RowBookmark(iRow)

                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If

                sdbgArticulos.Bookmark = vBookmark

                frmESTRMATAtrib.g_sOrigen = "ART4"
                frmESTRMATAtrib.g_bModoEdicion = m_bRAtributo
                Set frmESTRMATAtrib.g_oAtributos = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).DevolverTodosLosAtributosDelArticulo
                frmESTRMATAtrib.g_vCodArt = sdbgArticulos.Columns("COD").Value
                Set frmESTRMATAtrib.Articulo = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark))
                frmESTRMATAtrib.Show
        Case "IMPUESTO"
                iRow = sdbgArticulos.Row
                If m_bModoEdicion Then
                    
                    m_bModError = False
                    m_bAnyaError = False
                    
                    ' Primero acabamos la edicion del articulo
                    sdbgArticulos.Update
                    ' Si se ha producido un error no continuamos
                    If m_bValError Or m_bModError Or m_bAnyaError Then
                        Exit Sub
                    End If
                End If
                
                vBookmark = sdbgArticulos.RowBookmark(iRow)

                If IsNull(vBookmark) Or IsEmpty(vBookmark) Then
                    vBookmark = 0
                End If

                sdbgArticulos.Bookmark = vBookmark
                
                frmImpuestos.ArticuloCod = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).Cod
                
                frmImpuestos.GMN1Cod = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN1Cod
                frmImpuestos.GMN2Cod = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN2Cod
                frmImpuestos.GMN3Cod = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN3Cod
                frmImpuestos.GMN4Cod = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN4Cod
                
                frmImpuestos.PermiteModif = m_bModifImpuesto
                
                frmImpuestos.caption = m_sTextImpuestos & " " & g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN1Cod & " - "
                frmImpuestos.caption = frmImpuestos.caption & g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN2Cod & " - "
                frmImpuestos.caption = frmImpuestos.caption & g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN3Cod & " - "
                frmImpuestos.caption = frmImpuestos.caption & g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).GMN4Cod & " - "
                frmImpuestos.caption = frmImpuestos.caption & g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).Cod & " - "
                frmImpuestos.caption = frmImpuestos.caption & g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).Den
                frmImpuestos.g_sOrigen = "frmESTRMAT"
                frmImpuestos.Show vbModal
                
                If frmImpuestos.ArticuloCod = "0" Then
                    g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).ConImpuestos = False
                Else
                    g_oGMN4Seleccionado.ARTICULOS.Item(CStr(sdbgArticulos.Bookmark)).ConImpuestos = True
                End If
                frmImpuestos.ArticuloCod = ""
                
                RefrescarLineasImpuestos sdbgArticulos.Bookmark
            Case "UON_DEN"
                
                isAlta = sdbgArticulos.IsAddRow
                If m_bModoEdicion Then
                    
                    m_bModError = False
                    m_bAnyaError = False
                    
                    ' Primero acabamos la edicion del articulo
                    sdbgArticulos.Update
                    ' Si se ha producido un error no continuamos
                    If m_bValError Or m_bModError Or m_bAnyaError Then
                        Exit Sub
                    End If
                End If
                'Si estamos en modo edici�n
                Me.cargarFormUons (isAlta)
                
                
            Case "AGREGADO"
                isAlta = sdbgArticulos.IsAddRow
                If m_bModoEdicion Then
                    
                    m_bModError = False
                    m_bAnyaError = False
                    
                    ' Primero acabamos la edicion del articulo
                    sdbgArticulos.Update
                    ' Si se ha producido un error no continuamos
                    If m_bValError Or m_bModError Or m_bAnyaError Then
                        Exit Sub
                    End If
                End If
                Dim oArticulo As CArticulo
                If isAlta Then
                    Set oArticulo = Me.ArticuloEnEdicion
                Else
                    Set oArticulo = Me.ArticuloSeleccionado
                End If
                If Not isAlta Then
                    If (oArticulo.AltaIntegracion And m_bModoEdicion) Then
                        oMensajes.ImposibleAnyadirArticuloIntegraCentral
                        Exit Sub
                    End If

                End If
                
                If oArticulo.isCentral Then
                    frmArtCentralUnifConfirm.ModoEdicion = m_bModoEdicion
                    frmArtCentralUnifConfirm.enableCancel = False
                    Set frmArtCentralUnifConfirm.ArticuloCentral = ArticuloSeleccionado
                    Set frmArtCentralUnifConfirm.GMNSeleccionada = g_oGMN4Seleccionado
                    frmArtCentralUnifConfirm.Show
                    
                Else
                    If m_bModoEdicion Then
                        frmArtCentralUnificar.ModoEdicion = m_bModoEdicion
                        frmArtCentralUnificar.Usuario = oUsuarioSummit
                        frmArtCentralUnificar.Denominacion = oArticulo.Den
                        Set frmArtCentralUnificar.ArticuloCentral = oArticulo
                        Set frmArtCentralUnificar.GMNSeleccionada = g_oGMN4Seleccionado
                        frmArtCentralUnificar.Show
                    End If
                End If
                Set oArticulo = Nothing
                
    End Select

End Sub

''' <summary>
''' Configura la linea de la grid con el estilo
''' </summary>
''' <param name="Bookmark">del evento</param>
''' <remarks>Llamada desde: Evento; Tiempo m�ximo: 0,1</remarks>
Private Sub sdbgArticulos_RowLoaded(ByVal Bookmark As Variant)
    
Dim oArticulo As CArticulo
    
    Set oArticulo = Nothing
    If Not IsNull(Bookmark) And Not IsEmpty(Bookmark) Then
        Set oArticulo = g_oArticulos.Item(CStr(Bookmark))
        If oArticulo.ConAdjsAnteriores Then
            sdbgArticulos.Columns("P").CellStyleSet "Adjudica"
        Else
            sdbgArticulos.Columns("P").CellStyleSet ""
        End If
        If oArticulo.EspAdj <> 0 Then
            sdbgArticulos.Columns("ESPEC").CellStyleSet "styEspAdjSi"
        Else
            sdbgArticulos.Columns("ESPEC").CellStyleSet ""
        End If
        If oArticulo.ConAtributos Then
            sdbgArticulos.Columns("ATRIBUTOS").CellStyleSet "Atributos"
        Else
            sdbgArticulos.Columns("ATRIBUTOS").CellStyleSet ""
        End If
        
        '''Impuesto
        If oArticulo.ConImpuestos Then
            sdbgArticulos.Columns("IMPUESTO").CellStyleSet "HayImpuestos"
        Else
            sdbgArticulos.Columns("IMPUESTO").CellStyleSet ""
        End If
        If oArticulo.isCentral Then
            sdbgArticulos.Columns("AGREGADO").CellStyleSet "Adjudica" 'Im�gen de visto bueno
        Else
            sdbgArticulos.Columns("AGREGADO").CellStyleSet ""
        End If
    End If
        ''' INTEGRACION
    If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4) Then
        If sdbgArticulos.Columns("INT").Value = "" Or sdbgArticulos.Columns("INT").Value = CStr(EstadoIntegracion.recibidocorrecto) Then
            sdbgArticulos.Columns("INT").Value = "       " & sIdiSincronizado 'Los espacios son para que no se vea el texto cuando se selecciona la fila de la grid
            sdbgArticulos.Columns("INT").CellStyleSet "IntOK", Bookmark
        Else
            'If sdbgArticulos.Columns("INT").Value = CStr(EstadoIntegracion.Enviado) Or sdbgArticulos.Columns("INT").Value = CStr(EstadoIntegracion.PendienteDeReintentar) Or sdbgArticulos.Columns("INT").Value = CStr(EstadoIntegracion.RecibidoRechazado) Or sdbgArticulos.Columns("INT").Value = CStr(EstadoIntegracion.PendienteDeTratar) Then
                sdbgArticulos.Columns("INT").Value = "       " & sIdiNoSincronizado
                sdbgArticulos.Columns("INT").CellStyleSet "IntKO", Bookmark
            'End If
        End If
    End If

End Sub

Private Sub ssdbddConcepto_CloseUp()
    
    Dim teserror As TipoErrorSummit
    Dim oArt As CArticulo
    Dim bk As Variant
    
    Screen.MousePointer = vbHourglass
    If Not m_aBookmarks Is Nothing Then
        If m_aBookmarks.Count > 0 Then
            m_bRespetar = True
            teserror = g_oArticulos.ActualizarConcepAlmacRecep(Concepto, TraducirTextoCelda(0, ssdbddConcepto.Columns("DEN").Value), m_aBookmarks)
            If teserror.Arg2 Then
                Screen.MousePointer = vbNormal
                oMensajes.ImposibleModificarMultiplesArticulos teserror.Arg1
                Screen.MousePointer = vbHourglass
                Set m_oArticulosAModificar = Nothing
    
                Screen.MousePointer = vbNormal
                'Vuelvo a seleccionar las filas modificadas
    
                If m_aBookmarks.Count > 0 Then
                    For Each bk In m_aBookmarks
                        sdbgArticulos.SelBookmarks.Add bk
                    Next
                End If
            Else
                For Each oArt In m_oArticulosAModificar
                    RegistrarAccion accionessummit.ACCArtMod, "Art�culo: " & oArt.Cod & " Concepto: " & oArt.Concepto
                Next
            End If
            sdbgArticulos.Update
            sdbgArticulos.ReBind
            If m_aBookmarks.Count > 0 Then
                For Each bk In m_aBookmarks
                    sdbgArticulos.SelBookmarks.Add bk
                Next
            End If
            Set m_aBookmarks = Nothing
            m_bRespetar = False
        End If
    End If
    Screen.MousePointer = vbNormal
    
End Sub

''' <summary>
''' Modificaci�n de una celda del grid
''' </summary>
''' <param name="Cell">celda modificada</param>
''' <remarks>Llamada desde: Sistema; Tiempo m�ximo:0,1</remarks>
Private Sub ssMateriales_AfterCellUpdate(ByVal Cell As UltraGrid.SSCell)
    Dim teserror As TipoErrorSummit
    Dim i As Integer
    Dim arrDenIdiomas() As Variant
    Dim bModifTipoRecep As Boolean
    Dim bModifCantModificable As Boolean
    
    On Error GoTo Error
    
    If m_bDescargarFrm Then Exit Sub
    
    If ssMateriales.ActiveRow Is Nothing Then Exit Sub
    If Accion = ACCGMN1Anya Or Accion = ACCGMN2Anya Or Accion = ACCGMN3Anya Or Accion = ACCGMN4Anya Then Exit Sub
    If Not Cell.DataChanged Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    Select Case Cell.Column.Band.Index
        Case 0    'GMN1
            Accion = ACCGMN1Mod
            
            Set g_oGMN1Seleccionado = Nothing
            Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
            g_oGMN1Seleccionado.Cod = Cell.Row.Cells("COD").Value
                        
            Set oIBaseDatos = g_oGMN1Seleccionado
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN1Seleccionado = Nothing
                Exit Sub
            Else
                If gParametrosGenerales.gbSincronizacionMat Then
                    g_oGMN1Seleccionado.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = Cell.Row.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    g_oGMN1Seleccionado.DenIdiomas = arrDenIdiomas
                Else
                    g_oGMN1Seleccionado.Den = Cell.Row.Cells("DEN_SPA").Value
                End If
                g_oGMN1Seleccionado.tipoRecepcion = Abs(Cell.Row.Cells("TIPORECEPCION").Value)
                g_oGMN1Seleccionado.CantidadPedidoModificable = Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value
                g_oGMN1Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
                teserror = oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN1Mod, "Cod:" & g_oGMN1Seleccionado.Cod
                    
                    bModifTipoRecep = (Cell.Column.BaseColumnName = "TIPORECEPCION")
                    bModifCantModificable = (Cell.Column.BaseColumnName = "CANTIDAD_MODIFICABLE")
                    If bModifTipoRecep And g_oGMN1Seleccionado.tipoRecepcion = 1 And Not bModifCantModificable Then
                        bModifCantModificable = True
                        g_oGMN1Seleccionado.CantidadPedidoModificable = False
                        Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value = 0
                    End If
                    If bModifTipoRecep Or bModifCantModificable Then
                        ActualizarGMNsHijas 1, bModifTipoRecep, bModifCantModificable, g_oGMN1Seleccionado.tipoRecepcion, g_oGMN1Seleccionado.CantidadPedidoModificable, Cell.Row.Cells("COD").Value
                    End If
                    
                    ssMateriales.Update
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set g_oGMN1Seleccionado = Nothing
                    Exit Sub
                End If
            End If

        Case 1  'GMN2
            Accion = ACCGMN2Mod

            Set g_oGMN2Seleccionado = Nothing
            Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
            g_oGMN2Seleccionado.Cod = Cell.Row.Cells("COD").Value
            g_oGMN2Seleccionado.GMN1Cod = Cell.Row.GetParent.Cells("COD").Value
            
            Set oIBaseDatos = g_oGMN2Seleccionado
            teserror = oIBaseDatos.IniciarEdicion

            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN2Seleccionado = Nothing
                Exit Sub
            Else
                If gParametrosGenerales.gbSincronizacionMat Then
                    g_oGMN2Seleccionado.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = Cell.Row.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    g_oGMN2Seleccionado.DenIdiomas = arrDenIdiomas
                Else
                    g_oGMN2Seleccionado.Den = Cell.Row.Cells("DEN_SPA").Value
                End If
                g_oGMN2Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
                g_oGMN2Seleccionado.tipoRecepcion = Abs(Cell.Row.Cells("TIPORECEPCION").Value)
                g_oGMN2Seleccionado.CantidadPedidoModificable = Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value
                teserror = frmESTRMAT.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN2Mod, "Cod:" & g_oGMN2Seleccionado.Cod
                    
                    bModifTipoRecep = (Cell.Column.BaseColumnName = "TIPORECEPCION")
                    bModifCantModificable = (Cell.Column.BaseColumnName = "CANTIDAD_MODIFICABLE")
                    If bModifTipoRecep And g_oGMN2Seleccionado.tipoRecepcion = 1 And Not bModifCantModificable Then
                        bModifCantModificable = True
                        g_oGMN2Seleccionado.CantidadPedidoModificable = False
                        Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value = 0
                    End If
                    If bModifTipoRecep Or bModifCantModificable Then
                        ActualizarGMNsHijas 2, bModifTipoRecep, bModifCantModificable, g_oGMN2Seleccionado.tipoRecepcion, g_oGMN2Seleccionado.CantidadPedidoModificable, Cell.Row.Cells("GMN1").Value, _
                                            Cell.Row.Cells("COD").Value
                    End If
                    
                    ssMateriales.Update
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set g_oGMN2Seleccionado = Nothing
                    Exit Sub
                End If
            End If

        Case 2 'GMN3
            Accion = ACCGMN3Mod

            Set g_oGMN3Seleccionado = Nothing
            Set g_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
            g_oGMN3Seleccionado.Cod = Cell.Row.Cells("COD").Value
            g_oGMN3Seleccionado.GMN1Cod = Cell.Row.GetParent.GetParent.Cells("COD").Value
            g_oGMN3Seleccionado.GMN2Cod = Cell.Row.GetParent.Cells("COD").Value
                        
            Set oIBaseDatos = g_oGMN3Seleccionado
            teserror = oIBaseDatos.IniciarEdicion

            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN3Seleccionado = Nothing
                Exit Sub
            Else
                If gParametrosGenerales.gbSincronizacionMat Then
                    g_oGMN3Seleccionado.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = Cell.Row.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    g_oGMN3Seleccionado.DenIdiomas = arrDenIdiomas
                Else
                    g_oGMN3Seleccionado.Den = Cell.Row.Cells("DEN_SPA").Value
                End If
            
                g_oGMN3Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
                g_oGMN3Seleccionado.tipoRecepcion = Abs(Cell.Row.Cells("TIPORECEPCION").Value)
                g_oGMN3Seleccionado.CantidadPedidoModificable = Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value
                teserror = frmESTRMAT.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN3Mod, "Cod:" & g_oGMN3Seleccionado.Cod
                    
                    bModifTipoRecep = (Cell.Column.BaseColumnName = "TIPORECEPCION")
                    bModifCantModificable = (Cell.Column.BaseColumnName = "CANTIDAD_MODIFICABLE")
                    If bModifTipoRecep And g_oGMN3Seleccionado.tipoRecepcion = 1 And Not bModifCantModificable Then
                        bModifCantModificable = True
                        g_oGMN3Seleccionado.CantidadPedidoModificable = False
                        Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value = 0
                    End If
                    If bModifTipoRecep Or bModifCantModificable Then
                        ActualizarGMNsHijas 3, bModifTipoRecep, bModifCantModificable, g_oGMN3Seleccionado.tipoRecepcion, g_oGMN3Seleccionado.CantidadPedidoModificable, Cell.Row.Cells("GMN1").Value, _
                                            Cell.Row.Cells("GMN2").Value, Cell.Row.Cells("COD").Value
                    End If
                    
                    ssMateriales.Update
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set g_oGMN3Seleccionado = Nothing
                    Exit Sub
                End If
            End If

        Case 3  'GMN4
            Accion = ACCGMN4Mod

            Set g_oGMN4Seleccionado = Nothing
            Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
            g_oGMN4Seleccionado.Cod = Cell.Row.Cells("COD").Value
            g_oGMN4Seleccionado.GMN1Cod = Cell.Row.GetParent.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN2Cod = Cell.Row.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN3Cod = Cell.Row.GetParent.Cells("COD").Value
                        
            Set oIBaseDatos = g_oGMN4Seleccionado
            teserror = oIBaseDatos.IniciarEdicion

            If teserror.NumError <> TESnoerror Then
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN4Seleccionado = Nothing
                Exit Sub
            Else
                If gParametrosGenerales.gbSincronizacionMat Then
                    g_oGMN4Seleccionado.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = Cell.Row.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    g_oGMN4Seleccionado.DenIdiomas = arrDenIdiomas
                Else
                    g_oGMN4Seleccionado.Den = Cell.Row.Cells("DEN_SPA").Value
                End If
            
                g_oGMN4Seleccionado.Usuario = basOptimizacion.gvarCodUsuario
                g_oGMN4Seleccionado.tipoRecepcion = Abs(Cell.Row.Cells("TIPORECEPCION").Value)
                g_oGMN4Seleccionado.CantidadPedidoModificable = Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value
                teserror = frmESTRMAT.oIBaseDatos.FinalizarEdicionModificando
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN4Mod, "Cod:" & Trim(g_oGMN4Seleccionado.Cod)
                    
                    If g_oGMN4Seleccionado.tipoRecepcion = 1 Then
                        g_oGMN4Seleccionado.CantidadPedidoModificable = False
                        Cell.Row.Cells("CANTIDAD_MODIFICABLE").Value = 0
                    End If
                    
                    ssMateriales.Update
                Else
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set g_oGMN4Seleccionado = Nothing
                    Exit Sub
                End If
            End If
    End Select
    
    Accion = ACCGMCon
    picNavigate.Visible = True
    picEdicion.Visible = False
    Screen.MousePointer = vbNormal
    Exit Sub
Error:
    Screen.MousePointer = vbNormal
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmESTRMat", "ssMateriales_AfterCellUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

''' <summary>Actualiza en el grid el valor de los checks para los campos modificados en sus filas hijas</summary>
''' <param name="button">que boton del raton hemos pulsado</param>
''' <param name="shift">accion del shift</param>
''' <param name="X">posicion X</param>
''' <param name="Y">posicion Y</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>

Private Sub ActualizarGMNsHijas(ByVal iNivel As Integer, ByVal bTipoRecep As Boolean, ByVal bCantModif As Boolean, ByVal iTipoRecepChecked As Integer, ByVal bCantModifChecked As Boolean, _
        ByVal sGMN1 As String, Optional ByVal sGMN2 As String, Optional ByVal sGMN3 As String)
    Dim rsGMN1 As ADODB.Recordset
    Dim rsGMN2 As ADODB.Recordset
    Dim rsGMN3 As ADODB.Recordset
    Dim rsGMN4 As ADODB.Recordset
    Dim oRow As SSRow
    
    On Error GoTo Error
        
    Set oRow = ssMateriales.ActiveRow
    
    Set rsGMN1 = ssMateriales.DataSource
    Select Case iNivel
        Case 1
            rsGMN1.MoveFirst
            rsGMN1.Find "COD='" & sGMN1 & "'", , adSearchForward
            Set rsGMN2 = rsGMN1("GMN2").Value
            
            If rsGMN2.RecordCount > 0 Then
                rsGMN2.MoveFirst
                While Not rsGMN2.EOF
                    If bTipoRecep Then rsGMN2("TIPORECEPCION") = iTipoRecepChecked
                    If bCantModif Then rsGMN2("CANTIDAD_MODIFICABLE") = IIf(bCantModifChecked, 1, 0)
                    'ActualizarGMNsHijas 2, bTipoRecep, bCantModif, iTipoRecepChecked, iCantModifChecked, sGMN1, rsGMN2("COD")
                    Set rsGMN3 = rsGMN2("GMN3").Value
                        
                    If rsGMN3.RecordCount > 0 Then
                        rsGMN3.MoveFirst
                        While Not rsGMN3.EOF
                            If bTipoRecep Then rsGMN3("TIPORECEPCION") = iTipoRecepChecked
                            If bCantModif Then rsGMN3("CANTIDAD_MODIFICABLE") = IIf(bCantModifChecked, 1, 0)
                            'ActualizarGMNsHijas 3, bTipoRecep, bCantModif, iTipoRecepChecked, iCantModifChecked, sGMN1, sGMN2, rsGMN3("COD")
                            Set rsGMN4 = rsGMN3("GMN4").Value
                                    
                            If rsGMN4.RecordCount > 0 Then
                                rsGMN4.MoveFirst
                                While Not rsGMN4.EOF
                                    If bTipoRecep Then rsGMN4("TIPORECEPCION") = iTipoRecepChecked
                                    If bCantModif Then rsGMN4("CANTIDAD_MODIFICABLE") = IIf(bCantModifChecked, 1, 0)
                                
                                    rsGMN4.MoveNext
                                Wend
                            End If
                            
                            rsGMN3.MoveNext
                        Wend
                    End If
                    
                    rsGMN2.MoveNext
                Wend
            End If
        Case 2
            rsGMN1.Find "COD='" & sGMN1 & "'", , adSearchForward
            Set rsGMN2 = rsGMN1("GMN2").Value
            rsGMN2.Find "COD='" & sGMN2 & "'", , adSearchForward
            Set rsGMN3 = rsGMN2("GMN3").Value
                        
            If rsGMN3.RecordCount > 0 Then
                rsGMN3.MoveFirst
                While Not rsGMN3.EOF
                    If bTipoRecep Then rsGMN3("TIPORECEPCION") = iTipoRecepChecked
                    If bCantModif Then rsGMN3("CANTIDAD_MODIFICABLE") = IIf(bCantModifChecked, 1, 0)
                    'ActualizarGMNsHijas 3, bTipoRecep, bCantModif, iTipoRecepChecked, iCantModifChecked, sGMN1, sGMN2, rsGMN3("COD")
                    Set rsGMN4 = rsGMN3("GMN4").Value
                                    
                    If rsGMN4.RecordCount > 0 Then
                        rsGMN4.MoveFirst
                        While Not rsGMN4.EOF
                            If bTipoRecep Then rsGMN4("TIPORECEPCION") = iTipoRecepChecked
                            If bCantModif Then rsGMN4("CANTIDAD_MODIFICABLE") = IIf(bCantModifChecked, 1, 0)
                        
                            rsGMN4.MoveNext
                        Wend
                    End If
                    
                    rsGMN3.MoveNext
                Wend
            End If
        Case 3
            rsGMN1.Find "COD='" & sGMN1 & "'", , adSearchForward
            Set rsGMN2 = rsGMN1("GMN2").Value
            rsGMN2.Find "COD='" & sGMN2 & "'", , adSearchForward
            Set rsGMN3 = rsGMN2("GMN3").Value
            rsGMN3.Find "COD='" & sGMN3 & "'", , adSearchForward
            Set rsGMN4 = rsGMN3("GMN4").Value
                                    
            If rsGMN4.RecordCount > 0 Then
                rsGMN4.MoveFirst
                While Not rsGMN4.EOF
                    If bTipoRecep Then rsGMN4("TIPORECEPCION") = iTipoRecepChecked
                    If bCantModif Then rsGMN4("CANTIDAD_MODIFICABLE") = IIf(bCantModifChecked, 1, 0)
                
                    rsGMN4.MoveNext
                Wend
            End If
    End Select
    
Salir:
    Set oRow = Nothing
    Set rsGMN1 = Nothing
    Set rsGMN2 = Nothing
    Set rsGMN3 = Nothing
    Set rsGMN4 = Nothing
    Exit Sub
Error:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmESTRMat", "ActualizarGMNsHijas", err, Erl, Me, m_bActivado, m_sMsgError)
       Resume Salir
    End If
End Sub

Private Sub ssMateriales_AfterRowActivate()
    If ssMateriales.ActiveRow Is Nothing Then Exit Sub
        
    ConfigurarInterfazSeguridad ssMateriales.ActiveRow.Band.Index

    If (ssMateriales.ActiveRow.Band.Index + 1) = gParametrosGenerales.giNEM Then
        If Accion = ACCGMN1Anya Or Accion = ACCGMN2Anya Or Accion = ACCGMN3Anya Or Accion = ACCGMN4Anya Then
            SSTabEstrMat.TabVisible(1) = False
        Else
            SSTabEstrMat.TabVisible(1) = True
        End If
    Else
        SSTabEstrMat.TabVisible(1) = False
    End If

    If ssMateriales.ActiveRowScrollRegion.FirstRow.Bookmark > 1 Then
        tvwEstrMat.Visible = False
    Else
        tvwEstrMat.Visible = True
    End If
    
End Sub

Private Sub ssMateriales_AfterRowCollapsed(ByVal Row As UltraGrid.SSRow)
    If ssMateriales.ActiveRow Is Nothing Then Exit Sub
    
    If Row.Band.Index <> ssMateriales.ActiveRow.Band.Index Then Exit Sub
    
    ConfigurarInterfazSeguridad Row.Band.Index
End Sub

Private Sub ssMateriales_BeforeCellUpdate(ByVal Cell As UltraGrid.SSCell, NewValue As Variant, ByVal Cancel As UltraGrid.SSReturnBoolean)
    
    If Not Cell.DataChanged Then Exit Sub
    
    If Cell.Column.DataField = "TIPORECEPCION" Then
        If Cell.Row.Band.key = "GMN1" Or Cell.Row.Band.key = "GMN2" Then
            If Not oMensajes.ConfirmarActualizacion Then
                Cancel = True
                Exit Sub
            End If
        End If
    End If
    
    If Cell.Column.DataField = "CANTIDAD_MODIFICABLE" Then
        If Cell.Row.Cells("TIPORECEPCION").Value = 1 Then Cancel = True
    End If
    
    If Me.ActiveControl.Name = "cmdDeshacer" Then
        If Accion = ACCGMN1Anya Or Accion = ACCGMN2Anya Or Accion = ACCGMN3Anya Or Accion = ACCGMN4Anya Then
        Else
            Cancel = True
            picNavigate.Visible = True
            picEdicion.Visible = False
            Accion = ACCGMCon
        End If
        Exit Sub
    End If
    
    If Not (Accion = ACCGMN1Anya Or Accion = ACCGMN2Anya Or Accion = ACCGMN3Anya Or Accion = ACCGMN4Anya) Then
        If IsNull(NewValue) Or NewValue = "" Then
            If Cell.Column.key = "COD" Then
                oMensajes.NoValido m_sIdiCod
            Else
                If Mid(Cell.Column.key, 1, 3) = "DEN" Then
                    oMensajes.NoValido m_sIdiDen & " " & Cell.Column.Header.caption
                End If
            End If
            
            Cancel = True
            If Me.Visible Then ssMateriales.SetFocus
            Screen.MousePointer = vbNormal
            Exit Sub
        End If
    End If
End Sub

Private Sub ssMateriales_BeforeRowDeactivate(ByVal Cancel As UltraGrid.SSReturnBoolean)
    'Antes de que se mueva a otra fila hago las comprobaciones
    'Valida los datos antes de insertar un nuevo nivel de material.Se deben rellenar todos los campos:
    Dim oCelda As SSCell
    Dim oGMN1 As CGrupoMatNivel1
    Dim oGMN2 As CGrupoMatNivel2
    Dim oGMN3 As CGrupoMatNivel3
    Dim oGMN4 As CGrupoMatNivel4
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit
    Dim i As Integer
        
    On Error GoTo Error
    
    If m_bDescargarFrm Then Exit Sub
    If Not ssMateriales.ActiveRow.DataChanged Then Exit Sub
        
    If Accion = ACCGMN1Anya Or Accion = ACCGMN2Anya Or Accion = ACCGMN3Anya Or Accion = ACCGMN4Anya Then
        For Each oCelda In ssMateriales.ActiveRow.Cells
            If oCelda.Column.key = "COD" Then
                If Not ValidarCodigo(oCelda.GetText(ssMaskModeIncludeBoth)) Then
                    Screen.MousePointer = vbNormal
                    Cancel = True
                    If Me.Visible Then ssMateriales.SetFocus
                    Exit Sub
                End If
            Else
                If Mid(oCelda.Column.key, 1, 3) = "DEN" Then
                    If oCelda.GetText(ssMaskModeIncludeBoth) = "" Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido m_sIdiDen & " " & oCelda.Column.Header.caption
                        Cancel = True
                        If Me.Visible Then ssMateriales.SetFocus
                        Exit Sub
                    End If
                End If
            End If
        Next
        
        'A�ade un nuevo nivel de material:
        Screen.MousePointer = vbHourglass

        Select Case Accion
            Case ACCGMN1Anya
                Set oGMN1 = oFSGSRaiz.generar_CGrupoMatNivel1
                oGMN1.Cod = ssMateriales.ActiveRow.Cells("COD")
                If gParametrosGenerales.gbSincronizacionMat = True Then
                    oGMN1.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = ssMateriales.ActiveRow.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    oGMN1.DenIdiomas = arrDenIdiomas
                Else
                    oGMN1.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                End If

                 ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
                oGMN1.Usuario = basOptimizacion.gvarCodUsuario
                oGMN1.tipoRecepcion = Abs(ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value)
                oGMN1.CantidadPedidoModificable = ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value
                Set oIBaseDatos = oGMN1

                teserror = oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN1Anya, "Cod:" & oGMN1.Cod
                Else
                    TratarError teserror
                    Cancel = True
                    If Me.Visible Then ssMateriales.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oGMN1 = Nothing
                Set oIBaseDatos = Nothing

            Case ACCGMN2Anya
                Set oGMN2 = oFSGSRaiz.generar_CGrupoMatNivel2
                ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
                oGMN2.Usuario = basOptimizacion.gvarCodUsuario

                oGMN2.Cod = ssMateriales.ActiveRow.Cells("COD")
                oGMN2.GMN1Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                If gParametrosGenerales.gbSincronizacionMat = True Then
                    oGMN2.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = ssMateriales.ActiveRow.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    oGMN2.DenIdiomas = arrDenIdiomas
                Else
                    oGMN2.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                End If
                oGMN2.tipoRecepcion = Abs(ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value)
                oGMN2.CantidadPedidoModificable = ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value
                Set oIBaseDatos = oGMN2

                teserror = oIBaseDatos.AnyadirABaseDatos()
                
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN2Anya, "Cod:" & oGMN2.Cod

                    If basParametros.gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                        'Notificar a Usuarios de QA:
                        teserror = NotificarAltaMat(oGMN2.Cod, oGMN2.Den, oGMN2.CodIdiomas, oGMN2.DenIdiomas, oGMN2.GMN1Cod)
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            'Exit Sub
                        End If
                    End If
                Else
                    TratarError teserror
                    Cancel = True
                    If Me.Visible Then ssMateriales.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oGMN2 = Nothing
                Set oIBaseDatos = Nothing

            Case ACCGMN3Anya
                Set oGMN3 = oFSGSRaiz.generar_CGrupoMatNivel3

                oGMN3.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                oGMN3.GMN2Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                oGMN3.Cod = ssMateriales.ActiveRow.Cells("COD")
                If gParametrosGenerales.gbSincronizacionMat = True Then
                    oGMN3.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = ssMateriales.ActiveRow.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    oGMN3.DenIdiomas = arrDenIdiomas
                Else
                    oGMN3.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                End If
                oGMN3.tipoRecepcion = Abs(ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value)
                oGMN3.CantidadPedidoModificable = ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value
                Set oIBaseDatos = oGMN3
                ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
                oGMN3.Usuario = basOptimizacion.gvarCodUsuario

                teserror = oIBaseDatos.AnyadirABaseDatos()
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN3Anya, "Cod:" & oGMN3.Cod

                    If basParametros.gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                        'Notificar a Usuarios de QA:
                        teserror = NotificarAltaMat(oGMN3.Cod, oGMN3.Den, oGMN3.CodIdiomas, oGMN3.DenIdiomas, oGMN3.GMN1Cod, oGMN3.GMN2Cod)
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            'Exit Sub
                        End If
                    End If
                Else
                    TratarError teserror
                    Cancel = True
                    If Me.Visible Then ssMateriales.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oGMN3 = Nothing
                Set oIBaseDatos = Nothing

            Case ACCGMN4Anya
                Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4

                oGMN4.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD")
                oGMN4.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD")
                oGMN4.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD")
                oGMN4.Cod = ssMateriales.ActiveRow.Cells("COD")
                If gParametrosGenerales.gbSincronizacionMat = True Then
                    oGMN4.CodIdiomas = m_arrIdiomas
                    ReDim arrDenIdiomas(UBound(m_arrIdiomas))
                    For i = 1 To UBound(arrDenIdiomas)
                        arrDenIdiomas(i) = ssMateriales.ActiveRow.Cells("DEN_" & m_arrIdiomas(i)).Value
                    Next i
                    oGMN4.DenIdiomas = arrDenIdiomas
                Else
                    oGMN4.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                End If

                ' Usuario que realiza la modificaci�n, para el log de cambios e INTEGRACION
                oGMN4.Usuario = basOptimizacion.gvarCodUsuario
                oGMN4.tipoRecepcion = Abs(ssMateriales.ActiveRow.Cells("TIPORECEPCION"))
                oGMN4.CantidadPedidoModificable = ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value
                Set oIBaseDatos = oGMN4

                teserror = oIBaseDatos.AnyadirABaseDatos
                If teserror.NumError = TESnoerror Then
                    RegistrarAccion ACCGMN4Anya, "Cod:" & oGMN4.Cod

                    If basParametros.gParametrosGenerales.gsAccesoFSQA <> TipoAccesoFSQA.SinAcceso Then
                        'Notificar a Usuarios de QA:
                        teserror = NotificarAltaMat(oGMN4.Cod, oGMN4.Den, oGMN4.CodIdiomas, oGMN4.DenIdiomas, oGMN4.GMN1Cod, oGMN4.GMN2Cod, oGMN4.GMN3Cod)
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            'Exit Sub
                        End If
                    End If
                Else
                    TratarError teserror
                    Cancel = True
                    If Me.Visible Then ssMateriales.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If

                Set oGMN4 = Nothing
                Set oIBaseDatos = Nothing

        End Select

        Accion = ACCGMCon
        ssMateriales.ActiveRow.Cells("COD").Column.Activation = ssActivationActivateNoEdit
        picNavigate.Visible = True
        picEdicion.Visible = False
    
        If chkVerIdiomas.Value = vbUnchecked And gParametrosGenerales.gbSincronizacionMat Then
            LockWindowUpdate Me.hWnd
            
            ssMateriales.Layout.Load App.Path & "\LayoutMaterialesUndioma.ugd", ssPersistenceTypeFile, ssPropCatAll, True
            ssMateriales.Refresh ssRefreshDisplay
            InicializarGrid False
            ssMateriales.CollapseAll
            If Not ssMateriales.ActiveRow Is Nothing Then
                ssMateriales.ActiveRow.Expanded = True
            End If
            
            LockWindowUpdate 0&
        End If
        
        Screen.MousePointer = vbNormal
    End If
    
    Exit Sub
Error:
    Screen.MousePointer = vbNormal
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmESTRMat", "ssMateriales_BeforeRowDeactivate", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

Private Sub ssMateriales_BeforeRowRegionScroll(ByVal NewState As UltraGrid.SSRowScrollRegion, ByVal OldState As UltraGrid.SSRowScrollRegion, ByVal Cancel As UltraGrid.SSReturnBoolean)
    If NewState.FirstRow.Bookmark > 1 Then
        tvwEstrMat.Visible = False
    Else
        tvwEstrMat.Visible = True
    End If
End Sub

Private Sub ssMateriales_BeforeRowsDeleted(ByVal Rows As UltraGrid.SSSelectedRows, ByVal DisplayPromptMsg As UltraGrid.SSReturnBoolean, ByVal Cancel As UltraGrid.SSReturnBoolean)
    'para que no salte el mensaje de confirmaci�n de borrado de las filas
    DisplayPromptMsg = False
End Sub

Private Sub ssMateriales_CellChange(ByVal Cell As UltraGrid.SSCell)
    picNavigate.Visible = False
    picEdicion.Visible = True
    
    If Cell.Column.BaseColumnName = "TIPORECEPCION" Or Cell.Column.BaseColumnName = "CANTIDAD_MODIFICABLE" Then ssMateriales.Update
End Sub

Private Sub ssMateriales_DblClick()
    If ssMateriales.ActiveRow Is Nothing Then Exit Sub
    
    If ssMateriales.ActiveRow.Expanded = True Then
        ssMateriales.ActiveRow.CollapseAll
    Else
        ssMateriales.ActiveRow.Expanded = True
    End If
End Sub

Private Sub ssMateriales_Error(ByVal ErrorInfo As UltraGrid.SSError)
    If ErrorInfo.Code = 3617 Then
        ErrorInfo.DisplayErrorDialog = False
        oMensajes.FaltaValor
    End If
End Sub

Private Sub ssMateriales_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    InicializarGrid chkVerIdiomas.Value
End Sub

Private Sub ssMateriales_InitializeRow(ByVal Context As UltraGrid.Constants_Context, ByVal Row As UltraGrid.SSRow, ByVal ReInitialize As Boolean)
On Error GoTo err_ssMateriales_InitializeRow
    'Dim chk As CheckBox
    
    'Si tiene restricci�n de comprador:
    If g_bRComprador = True Then
        If Row.Cells("ASIG").Value = 1 Then
            Row.Cells("COD").Appearance.Picture = "GMN" & Row.Band.Index + 1 & "A"
        Else
            Row.Cells("COD").Appearance.Picture = "GMN" & Row.Band.Index + 1
        End If
    End If

    Exit Sub
    
err_ssMateriales_InitializeRow:
    Select Case err.Number
        Case -2147217887
            Resume Next
    End Select
End Sub

Private Sub ssMateriales_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        'Esto es para evitar que despu�s de realizar las acciones del men� que sea en el evento ssMateriales_MouseUp, se muestre el
        'men� popup por defecto de edici�n: Edit, Copy, Paste ...
        'La idea es deshabilitar el control porque cuando est� deshabilitado no se muestra dicho men� y llamar al ssMateriales_MouseUp para,
        ' una vez en ese evento, volver a habilitarlo ah�.
        ssMateriales.Enabled = False
        ssMateriales_MouseUp Button, Shift, X, Y
    End If
End Sub

''' <summary>
''' Mostrar un menu PopUp con las acciones posibles sobre el material seleccionado
''' </summary>
''' <param name="button">que boton del raton hemos pulsado</param>
''' <param name="shift">accion del shift</param>
''' <param name="X">posicion X</param>
''' <param name="Y">posicion Y</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0</remarks>
Private Sub ssMateriales_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)

    If Button = 2 Then
        ssMateriales.Enabled = True
        
        If ssMateriales.ActiveRow Is Nothing Then Exit Sub
        
        If cmdA�adir.Visible = False And cmdA�adirArticulo.Visible = False And (ssMateriales.ActiveRow.Band.Index <> 3 Or Not m_bRAtributo) And (Not m_bConsultaImpuesto) Then Exit Sub
        
        Select Case ssMateriales.ActiveRow.Band.Index
            Case 0  'GMN1
                    If gParametrosGenerales.giNEM = 1 Then
                        PopupMenu MDI.mnuPopUpEstrMatGMN4
                    Else
                        PopupMenu MDI.mnuPopUpEstrMatGMN1
                    End If
            
            Case 1  'GMN2
                    If gParametrosGenerales.giNEM = 2 Then
                        PopupMenu MDI.mnuPopUpEstrMatGMN4
                    Else
                        PopupMenu MDI.mnuPopUpEstrMatGMN2
                    End If
                        
            Case 2  'GMN3
                    If gParametrosGenerales.giNEM = 3 Then
                        PopupMenu MDI.mnuPopUpEstrMatGMN4
                    Else
                        PopupMenu MDI.mnuPopUpEstrMatGMN3
                    End If
                        
            Case 3  'GMN4
                    PopupMenu MDI.mnuPopUpEstrMatGMN4
                         
        End Select
    
    End If
End Sub

Public Sub SSTabEstrMat_Click(PreviousTab As Integer)
    
    Arrange
    If m_DesactivarTabClick = True Then Exit Sub

    If PreviousTab = 1 Then
    
        If m_bModoEdicion Then
            m_DesactivarTabClick = True
            SSTabEstrMat.Tab = 1
            m_DesactivarTabClick = False
            Exit Sub
        End If
     
        caption = m_sIdiTitulos(3)
           
        cmdA�adirArticulo.Enabled = False
        cmdEliminarArticulo.Enabled = False
        cmdListadoArticulo.Enabled = False
        cmdRestaurarArticulo.Enabled = False
        cmdModoEdicion.Enabled = False
        cmdA�adir.Enabled = True
        cmdEli.Enabled = True
        cmdListado.Enabled = True
        cmdRestaurar.Enabled = True
        
        sdbgArticulos.Visible = False
        If Me.Visible Then ssMateriales.SetFocus
   
    Else
        
        cmdA�adirArticulo.Enabled = True
        cmdEliminarArticulo.Enabled = True
        cmdListadoArticulo.Enabled = True
        cmdRestaurarArticulo.Enabled = True
        cmdModoEdicion.Enabled = True
        cmdA�adir.Enabled = False
        cmdEli.Enabled = False
        cmdListado.Enabled = False
        cmdRestaurar.Enabled = False
        
        caption = m_sIdiTitulos(4)
    
        CargarGridArticulos
                        
        sdbgArticulos.Visible = True
        sdbgArticulos.ReBind
        
        If sdbgArticulos.Rows = 0 Then
            sdbgArticulos.Columns("COD").Locked = False
        Else
            sdbgArticulos.Columns("COD").Locked = True
        End If
        
        If m_bModoEdicion Then
            If m_bModifArti Then
                sdbgArticulos.Columns("DEN").Locked = False
                sdbgArticulos.Columns("UNI").Locked = False
                sdbgArticulos.Columns("CONCEP").Locked = False
                sdbgArticulos.Columns("ALMAC").Locked = False
                sdbgArticulos.Columns("RECEP").Locked = False
                sdbgArticulos.Columns("CANT").Locked = False
                sdbgArticulos.Columns("TIPORECEPCION").Locked = False
                sdbgArticulos.Columns("TIPORECEPCION").Value = -1
            End If
            If m_bModifArtiGen Then
                sdbgArticulos.Columns("GENERICO").Locked = False
            End If
        Else
            sdbgArticulos.Columns("DEN").Locked = True
            sdbgArticulos.Columns("UNI").Locked = True
            sdbgArticulos.Columns("CONCEP").Locked = True
            sdbgArticulos.Columns("ALMAC").Locked = True
            sdbgArticulos.Columns("RECEP").Locked = True
            sdbgArticulos.Columns("CANT").Locked = True
            sdbgArticulos.Columns("GENERICO").Locked = True
            sdbgArticulos.Columns("TIPORECEPCION").Locked = True
        End If
        
        If sdbgArticulos.Rows <> 0 Then
            cmdListadoArticulo.Visible = True
            sdbgArticulos.Bookmark = 0
        Else
            cmdListadoArticulo.Visible = False
        End If
        
    End If

End Sub
''' <summary>
''' Muestra/oculta los men�s correspondientes al nodo seleccionado en funci�n de los permisos del usuario conectado
''' </summary>
''' <param name="indice">-1 ->raiz   0-> GMN1   1-> GMN2   2-> GMN3   3-> GMN4 </param>
''' <remarks>Llamada desde: Ordenar     frmESTRMATBuscar/cmdSeleccionar_Click; Tiempo m�ximo: 0</remarks>
Public Sub ConfigurarInterfazSeguridad(ByVal indice As Integer)
    
    Select Case indice
        Case -1  'Raiz
            SSTabEstrMat.TabVisible(1) = False

            cmdCodigo.Enabled = False
            If m_bModifEstr Then
                If Not g_bRComprador Then
                    cmdA�adir.Enabled = True
                    MDI.mnuPopUpEstrMatAnyaGMN1 = True
                Else
                    cmdA�adir.Enabled = False
                    MDI.mnuPopUpEstrMatAnyaGMN1 = False
                End If
            Else
                cmdA�adir.Enabled = False
                MDI.mnuPopUpEstrMatAnyaGMN1.Visible = False
                MDI.mnuPopUpEstrMatSep.Visible = False
            End If

            cmdEli.Enabled = False
                
                
        Case 0  'GMN1
            
            If ssMateriales.ActiveRow Is Nothing Then Exit Sub
            cmdCodigo.Enabled = True
            If m_bModifEstr Then
                If Not g_bRComprador Then
                        
                    cmdA�adir.Enabled = True
                    MDI.mnuPopUpEstrMatAnyaGMN2.Enabled = True
                    If ssMateriales.ActiveRow.HasChild = True Then
                        MDI.mnuPopUpEstrMatEliGMN1.Enabled = False
                        cmdEli.Enabled = False
                    Else
                        MDI.mnuPopUpEstrMatEliGMN1.Enabled = True
                        cmdEli.Enabled = True
                    End If
                
                Else
                    'Un comprador no puede eliminar el nivel mas alto de su asignacion
                    ' Si esta posicionado en un material que no le ha sido asignado,
                    ' no debe poder modificar nada a ese nivel.
                    If ssMateriales.ActiveRow.Cells("COD").Appearance.Picture <> "GMN1A" Then
                        cmdEli.Enabled = False
                        cmdA�adir.Enabled = False
                        MDI.mnuPopUpEstrMatAnyaGMN2.Enabled = False
                        MDI.mnuPopUpEstrMatEliGMN1.Enabled = False
                    Else
                        ' Lo tiene asignado
                        cmdA�adir.Enabled = True
                        cmdEli.Enabled = False
                        MDI.mnuPopUpEstrMatAnyaGMN2.Enabled = True
                        MDI.mnuPopUpEstrMatEliGMN1.Enabled = False
                    End If
                    
                End If
                        
            Else
                cmdEli.Enabled = False
                cmdA�adir.Enabled = False
                MDI.mnuPopUpEstrMatAnyaGMN2.Visible = False
                MDI.mnuPopUpEstrMatEliGMN1.Visible = False
                MDI.mnuPopUpEstrMatNiv1Sep1.Visible = False
                MDI.mnuPopUpEstrMatNiv1Sep2.Visible = False
                
            End If
            
            If Not m_bModifCodigo Then
                MDI.mnuPopUpEstrMatNiv1CamCod.Visible = False
            Else
                If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Materiales) Then
                    MDI.mnuPopUpEstrMatNiv1CamCod.Visible = False
                End If
            End If
            If gParametrosGenerales.giNEM > 1 Then
                SSTabEstrMat.TabVisible(1) = False
            End If
            
        Case 1  'GMN2
            If ssMateriales.ActiveRow Is Nothing Then Exit Sub
            cmdCodigo.Enabled = True
            If m_bModifEstr Then
                If Not g_bRComprador Then
                    cmdA�adir.Enabled = True
                    MDI.mnuPopUpEstrMatAnyaGMN3.Enabled = True
                    If ssMateriales.ActiveRow.HasChild = True Then
                        MDI.mnuPopUpEstrMatEliGMN2.Enabled = False
                        cmdEli.Enabled = False
                    Else
                        MDI.mnuPopUpEstrMatEliGMN2.Enabled = True
                        cmdEli.Enabled = True
                    End If
                
                Else
                    ' Si esta posicionado en un material que no le ha sido asignado,
                    ' no debe poder modificar nada a ese nivel.
                    If ssMateriales.ActiveRow.Cells("COD").Appearance.Picture = "GMN2" And ssMateriales.Bands(ssMateriales.ActiveRow.GetParent.Band.Index).Columns("COD").CellAppearance.Picture = "GMN1" Then
                        cmdEli.Enabled = False
                        cmdA�adir.Enabled = False
                        MDI.mnuPopUpEstrMatAnyaGMN3.Enabled = False
                        MDI.mnuPopUpEstrMatEliGMN2.Enabled = False
                    Else
                        cmdA�adir.Enabled = True
                        MDI.mnuPopUpEstrMatAnyaGMN3.Enabled = True
                        If ssMateriales.ActiveRow.HasChild = True Then
                            MDI.mnuPopUpEstrMatEliGMN2.Enabled = False
                            cmdEli.Enabled = False
                        Else
                            MDI.mnuPopUpEstrMatEliGMN2.Enabled = True
                            cmdEli.Enabled = True
                        End If
                    End If
                    
                End If
                        
            Else
                cmdEli.Enabled = False
                cmdA�adir.Enabled = False
                MDI.mnuPopUpEstrMatAnyaGMN3.Visible = False
                MDI.mnuPopUpEstrMatEliGMN2.Visible = False
                MDI.mnuPopUpEstrMatNiv2Sep1.Visible = False
                MDI.mnuPopUpEstrMatNiv2Sep2.Visible = False
            End If
        
            If Not m_bModifCodigo Then
                MDI.mnuPopUpEstrMatNiv2CamCod.Visible = False
            Else
                If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Materiales) Then
                    MDI.mnuPopUpEstrMatNiv2CamCod.Visible = False
                End If
            End If
        
            
            If gParametrosGenerales.giNEM > 2 Then
                SSTabEstrMat.TabVisible(1) = False
            End If
            
        Case 2  'GMN3
            If ssMateriales.ActiveRow Is Nothing Then Exit Sub
            cmdCodigo.Enabled = True
            If m_bModifEstr Then
                If Not g_bRComprador Then
                    cmdA�adir.Enabled = True
                    MDI.mnuPopUpEstrMatAnyaGMN3.Enabled = True
                    If ssMateriales.ActiveRow.HasChild = True Then
                        MDI.mnuPopUpEstrMatEliGMN3.Enabled = False
                        cmdEli.Enabled = False
                    Else
                        MDI.mnuPopUpEstrMatEliGMN3.Enabled = True
                        cmdEli.Enabled = True
                    End If
                
                Else
                    ' Si esta posicionado en un material que no le ha sido asignado,
                    ' no debe poder modificar nada a ese nivel.
                    If ssMateriales.ActiveRow.Cells("COD").Appearance.Picture = "GMN3" And ssMateriales.Bands(ssMateriales.ActiveRow.GetParent.Band.Index).Columns("COD").CellAppearance.Picture = "GMN2" And ssMateriales.Bands(ssMateriales.ActiveRow.GetParent.GetParent.Band.Index).Columns("COD").CellAppearance.Picture = "GMN1" Then
                        cmdEli.Enabled = False
                        cmdA�adir.Enabled = False
                        MDI.mnuPopUpEstrMatAnyaGMN4.Enabled = False
                        MDI.mnuPopUpEstrMatEliGMN3.Enabled = False
                    Else
                        cmdA�adir.Enabled = True
                        MDI.mnuPopUpEstrMatAnyaGMN4.Enabled = True
                        If ssMateriales.ActiveRow.HasChild = True Then
                            MDI.mnuPopUpEstrMatEliGMN3.Enabled = False
                            cmdEli.Enabled = False
                        Else
                            MDI.mnuPopUpEstrMatEliGMN3.Enabled = True
                            cmdEli.Enabled = True
                        End If
                    End If
                    
                End If
                        
            Else
                cmdEli.Enabled = False
                cmdA�adir.Enabled = False
                MDI.mnuPopUpEstrMatAnyaGMN4.Visible = False
                MDI.mnuPopUpEstrMatEliGMN3.Visible = False
                MDI.mnuPopUpEstrMatNiv3Sep1.Visible = False
                MDI.mnuPopUpEstrMatNiv3Sep2.Visible = False
                
            End If
            
            If Not m_bModifCodigo Then
                MDI.mnuPopUpEstrMatNiv3CamCod.Visible = False
            Else
                If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Materiales) Then
                    MDI.mnuPopUpEstrMatNiv3CamCod.Visible = False
                End If
            End If
        
        
            If gParametrosGenerales.giNEM > 3 Then
                SSTabEstrMat.TabVisible(1) = False
            End If
            
        Case 3   'GMN4
            If ssMateriales.ActiveRow Is Nothing Then Exit Sub
            
            cmdCodigo.Enabled = True
            cmdA�adir.Enabled = False
            If m_bModifEstr Then
                If Not g_bRComprador Then
                    If Me.ssMateriales.ActiveRow.HasChild = True Then
                        MDI.mnuPopUpGMN4EstrMat(0).Enabled = False 'Eli
                        cmdEli.Enabled = False
                    Else
                        MDI.mnuPopUpGMN4EstrMat(0).Enabled = True 'Eli
                        cmdEli.Enabled = True
                    End If
                
                Else
                    ' Si esta posicionado en un material que no le ha sido asignado,
                    ' no debe poder modificar nada a ese nivel.
                    If ssMateriales.ActiveRow.Cells("COD").Appearance.Picture = "GMN4" And ssMateriales.Bands(ssMateriales.ActiveRow.GetParent.Band.Index).Columns("COD").CellAppearance.Picture = "GMN3" And ssMateriales.Bands(ssMateriales.ActiveRow.GetParent.GetParent.Band.Index).Columns("COD").CellAppearance.Picture = "GMN2" And ssMateriales.Bands(ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Band.Index).Columns("COD").CellAppearance.Picture = "GMN1" Then
                        cmdEli.Enabled = False
                        MDI.mnuPopUpGMN4EstrMat(0).Enabled = False 'Eli
                    Else
                        If ssMateriales.ActiveRow.HasChild = True Then
                            MDI.mnuPopUpGMN4EstrMat(0).Enabled = False 'Eli
                            cmdEli.Enabled = False
                        Else
                            MDI.mnuPopUpGMN4EstrMat(0).Enabled = True 'Eli
                            cmdEli.Enabled = True
                        End If
                    End If
                    
                End If
                        
            Else
                cmdEli.Enabled = False
                MDI.mnuPopUpGMN4EstrMat(0).Visible = False 'Eli
                MDI.mnuPopUpGMN4EstrMat(2).Visible = False 'Sep1
            End If
            
            If Not m_bModifCodigo Then
                MDI.mnuPopUpGMN4EstrMat(1).Visible = False 'CamCod
            Else
                If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Materiales) Then
                    MDI.mnuPopUpGMN4EstrMat(1).Visible = False 'CamCod
                End If
            End If
            
    End Select
    
End Sub

Public Sub mnuPopUpEstrMatAtrGMN1()
    If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then Exit Sub
    
    frmESTRMATAtrib.g_sOrigen = "GMN4"
    Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4

    g_oGMN4Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
    If gParametrosGenerales.gbSincronizacionMat = True Then
        g_oGMN4Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
    Else
        g_oGMN4Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
    End If
    g_oGMN4Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
    g_oGMN4Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
    g_oGMN4Seleccionado.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
    frmESTRMATAtrib.g_vCodArt = ""
    Set frmESTRMATAtrib.g_oAtributos = g_oGMN4Seleccionado.DevolverTodosLosAtributosDelMaterial(False)
    frmESTRMATAtrib.g_bModoEdicion = m_bRAtributo
    frmESTRMATAtrib.Show
    'frmESTRMATAtrib.cmdRestaurar_Click
    'frmESTRMATAtrib.ZOrder 0
    
End Sub
Public Sub mnuPopUpEstrMatDet()
    Dim teserror As TipoErrorSummit
    Dim EstIntegracion As Variant
    Dim sRecepcion As String

    If ssMateriales.ActiveRow Is Nothing Then Exit Sub
    If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If (ssMateriales.ActiveRow.Cells("TIPORECEPCION").Value = 0) Then
        sRecepcion = sRecepcionCantidad
    Else
        sRecepcion = sRecepcionImporte
    End If
    
    Select Case ssMateriales.ActiveRow.Band.Index
        Case 0  'GMN1
            Accion = ACCGMN1Det
                        
            Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
            If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then
                Exit Sub
            Else
                g_oGMN1Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            End If
            
            Set oIBaseDatos = g_oGMN1Seleccionado
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Materiales) Then EstIntegracion = g_oGMN1Seleccionado.DevolverEstadoIntegracion
                
                Screen.MousePointer = vbNormal
                MostrarFormESTRMATDetalle oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sRecepcion, basParametros.gParametrosIntegracion, basParametros.gLongitudesDeCodigos, _
                    m_sIdiDetalle & ": " & LCase(gParametrosGenerales.gsDEN_GMN1), ssMateriales.ActiveRow.Cells("COD").Value, g_oGMN1Seleccionado.Den, EstIntegracion, Accion, _
                    SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN1Seleccionado = Nothing
                Exit Sub
            End If
                       
            Set g_oGMN1Seleccionado = Nothing
        Case 1   'GMN2
            Accion = ACCGMN2Det
                        
            Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
            g_oGMN2Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            g_oGMN2Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
           
            Set oIBaseDatos = g_oGMN2Seleccionado
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Materiales) Then EstIntegracion = g_oGMN2Seleccionado.DevolverEstadoIntegracion
                
                Screen.MousePointer = vbNormal
                MostrarFormESTRMATDetalle oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sRecepcion, basParametros.gParametrosIntegracion, basParametros.gLongitudesDeCodigos, _
                    m_sIdiDetalle & ": " & LCase(gParametrosGenerales.gsDEN_GMN2), ssMateriales.ActiveRow.Cells("COD").Value, g_oGMN2Seleccionado.Den, EstIntegracion, Accion, _
                    SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN2Seleccionado = Nothing
                Exit Sub
            End If
                    
            Set g_oGMN2Seleccionado = Nothing
        Case 2  'GMN3
             Accion = ACCGMN3Det
                        
            Set g_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
            g_oGMN3Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            g_oGMN3Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
            g_oGMN3Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                    
            Set oIBaseDatos = g_oGMN3Seleccionado
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Materiales) Then EstIntegracion = g_oGMN3Seleccionado.DevolverEstadoIntegracion
                
                Screen.MousePointer = vbNormal
                MostrarFormESTRMATDetalle oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sRecepcion, basParametros.gParametrosIntegracion, basParametros.gLongitudesDeCodigos, _
                    m_sIdiDetalle & ": " & LCase(gParametrosGenerales.gsDEN_GMN3), ssMateriales.ActiveRow.Cells("COD").Value, g_oGMN3Seleccionado.Den, EstIntegracion, Accion, _
                    SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN3Seleccionado = Nothing
                Exit Sub
            End If
                    
            Set g_oGMN3Seleccionado = Nothing
        Case 3   'GMN4
            Accion = ACCGMN4Det
                        
            Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
            g_oGMN4Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            g_oGMN4Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
           
            Set oIBaseDatos = g_oGMN4Seleccionado
            If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.Materiales) Then EstIntegracion = g_oGMN4Seleccionado.DevolverEstadoIntegracion
                
                Screen.MousePointer = vbNormal
                MostrarFormESTRMATDetalle oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, sRecepcion, basParametros.gParametrosIntegracion, basParametros.gLongitudesDeCodigos, _
                    m_sIdiDetalle & ": " & LCase(gParametrosGenerales.gsDEN_GMN4), ssMateriales.ActiveRow.Cells("COD").Value, g_oGMN4Seleccionado.Den, EstIntegracion, Accion, _
                    SQLBinaryToBoolean(ssMateriales.ActiveRow.Cells("CANTIDAD_MODIFICABLE").Value)
            Else
                Screen.MousePointer = vbNormal
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set g_oGMN4Seleccionado = Nothing
                Exit Sub
            End If
                        
            Set g_oGMN4Seleccionado = Nothing
    End Select
    
    Set oIBaseDatos = Nothing
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub CargarGridArticulos()

    Set g_oArticulos = Nothing
    Dim perf As Integer
    If oUsuarioSummit.Perfil Is Nothing Then
        perf = 0
    Else
        perf = oUsuarioSummit.Perfil.Id
    End If
    
    Screen.MousePointer = vbHourglass
 
    Select Case gParametrosGenerales.giNEM
    
    Case 1
    
    Case 2
    
    Case 3
    
    Case 4
            Set g_oGMN4Seleccionado = Nothing
            Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
            
            g_oGMN4Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
            g_oGMN4Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
            g_oGMN4Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_" & oUsuarioSummit.idioma).Value
            g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , , True, True, _
                                                basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , _
                                            True, , , , , g_sCodBusqueda, sUON1:=basOptimizacion.gUON1Usuario, _
                                            sUON2:=basOptimizacion.gUON2Usuario, sUON3:=basOptimizacion.gUON3Usuario, bRUsuUON:=m_bRestrMantUsu, _
                                            bRPerfUON:=m_bRestrMantPerf, lIdPerf:=perf, bMostrarUon:=True, bImpuestos:=m_bConsultaImpuesto
            g_sCodBusqueda = ""
            Set g_oArticulos = Nothing
            Set g_oArticulos = g_oGMN4Seleccionado.ARTICULOS
                
    End Select
    
    
    Screen.MousePointer = vbNormal
   
    Accion = ACCArtCon
    DoEvents
    
End Sub

Private Sub CargarGridConUnidades()

    ''' * Objetivo: Cargar combo con la coleccion de monedas
    
    Dim oUni As CUnidad
    
    sdbddUnidades.RemoveAll
    
    For Each oUni In m_oUnidades
        sdbddUnidades.AddItem oUni.Cod & Chr(m_lSeparador) & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next
    
    If sdbddUnidades.Rows = 0 Then
        sdbddUnidades.AddItem ""
    End If
    
End Sub
Private Function EliminarGMDeEstructura()
Dim FilaSiguiente As SSRow

    'elimina una fila de la grid y se posiciona en la correspondiente, configurando su seguridad
    If ssMateriales.ActiveRow.HasParent = True Then
        Set FilaSiguiente = ssMateriales.ActiveRow.GetParent
    ElseIf ssMateriales.ActiveRow.HasChild = True Then
        Set FilaSiguiente = ssMateriales.ActiveRow.GetChild(ssChildRowFirst)
    ElseIf ssMateriales.ActiveRow.HasNextSibling = True Then
        'se situa en la siguiente del mismo nivel
        Set FilaSiguiente = ssMateriales.ActiveRow.GetSibling(ssSiblingRowNext)
    ElseIf ssMateriales.ActiveRow.HasPrevSibling = True Then
        Set FilaSiguiente = ssMateriales.ActiveRow.GetSibling(ssSiblingRowPrevious)
    End If
    
    ssMateriales.ActiveRow.Delete
    
    If Not FilaSiguiente Is Nothing Then
        ssMateriales.ActiveRow = FilaSiguiente
        ssMateriales_AfterRowActivate
    End If
    
    If Me.Visible Then ssMateriales.SetFocus


End Function

Public Sub Ordenar(ByVal bOrdPorDen As Boolean)
    
    CargarGridMateriales Not bOrdPorDen
    ssMateriales.CollapseAll
    
    ConfigurarInterfazSeguridad -1
    
End Sub

Private Sub cmdA�adirArticulo_Click()

    ''' * Objetivo: Situarnos en la fila de adicion
    
    While g_oArticulos.Count > sdbgArticulos.Rows
        sdbgArticulos.Scroll 0, sdbgArticulos.Rows - sdbgArticulos.Row
    Wend
    sdbgArticulos.Scroll 0, sdbgArticulos.Rows - sdbgArticulos.Row
    
    If sdbgArticulos.VisibleRows > 0 Then
        
        If sdbgArticulos.VisibleRows >= sdbgArticulos.Rows Then
            If sdbgArticulos.Rows > 0 Then
                sdbgArticulos.Row = sdbgArticulos.Rows - 1
            Else
                sdbgArticulos.Row = 0
            End If
        Else
            sdbgArticulos.Row = sdbgArticulos.Rows - (sdbgArticulos.Rows - sdbgArticulos.VisibleRows) - 1
        End If
        
    End If
    
    If Me.Visible Then sdbgArticulos.SetFocus
    
End Sub

''' <summary>
''' Cambio de codigo de un articulo
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde; Tiempo m�ximo</remarks>
Private Sub cmdCodigoArticulo_Click()

    ''' * Objetivo: Cambiar de codigo el Articulo actual
    
    Dim teserror As TipoErrorSummit
    Dim iResp As Integer
    Dim sCodigoNuevo As String
    
    If sdbgArticulos.Rows = 0 Then Exit Sub
    
    If IsNull(sdbgArticulos.Bookmark) Then
        sdbgArticulos.Bookmark = 0
    End If
    
    ''' Resaltar el Articulo actual
    sdbgArticulos.SelBookmarks.Add sdbgArticulos.Bookmark
    
    ''' Iniciar y cancelar la edicion, para comprobar que sigue existiendo
    
    Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(sdbgArticulos.Bookmark))
    Set m_oIBAseDatosEnEdicion = m_oArticuloEnEdicion
    
    Screen.MousePointer = vbHourglass
    teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
        
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        If Me.Visible Then sdbgArticulos.SetFocus
        Exit Sub
    End If
    
    m_oIBAseDatosEnEdicion.CancelarEdicion
    
    ''' Activacion del formulario de cambio de codigo
    ''' para conocer el nuevo codigo
    
    frmMODCOD.caption = "Art�culos (C�digo)"
    frmMODCOD.Left = frmESTRMAT.Left + 500
    frmMODCOD.Top = frmESTRMAT.Top + 1000
    
    frmMODCOD.txtCodNue.MaxLength = gLongitudesDeCodigos.giLongCodART
    frmMODCOD.txtCodAct.Text = m_oArticuloEnEdicion.Cod
    Set frmMODCOD.fOrigen = frmESTRMAT
    g_bCodigoCancelar = False
    Screen.MousePointer = vbNormal
    frmMODCOD.Show 1
    DoEvents
    If g_bCodigoCancelar = True Then Exit Sub
    
    ''' Comprobar validez del codigo
    
    If g_sCodigoNuevo = "" Then
        oMensajes.NoValido m_sIdiCod
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oArticuloEnEdicion = Nothing
        Exit Sub
    End If
        
    If UCase(g_sCodigoNuevo) = UCase(m_oArticuloEnEdicion.Cod) Then
        oMensajes.NoValido m_sIdiCod
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oArticuloEnEdicion = Nothing
        Exit Sub
    End If
                  
                                        
    If m_oArticuloEnEdicion.ComprobarNuevoCodigoEnBaseDatos(g_sCodigoNuevo) Then
        oMensajes.DatoDuplicado 2
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oArticuloEnEdicion = Nothing
        Exit Sub
    End If
                  
    sCodigoNuevo = m_oArticuloEnEdicion.ComprobarCambioCodigoPendiente
    If sCodigoNuevo <> "" Then
        oMensajes.CambioCodigoPendiente sCodigoNuevo
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oArticuloEnEdicion = Nothing
        Exit Sub
    End If
    
    ''' Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n
    m_oArticuloEnEdicion.Usuario = basOptimizacion.gvarCodUsuario
    
    ''' Cambiar el codigo
    ''' Antes de cambiar pregunta lo siguiente:
    ''' El cambio de c�digo de art�culo es una operaci�n que requiere
    ''' una gran cantidad de recursos disponibles.  Su cambio de c�digo
    ''' se realizar� en un proceso batch y estar� disponible al d�a
    ''' siguiente de su petici�n. �Desea continuar?"
    
    iResp = oMensajes.PreguntarCambiarCodigo
    If iResp = vbYes Then
        teserror = m_oIBAseDatosEnEdicion.CambiarCodigo(g_sCodigoNuevo)
    End If
    
    If teserror.NumError <> TESnoerror Then
        basErrores.TratarError teserror
        Exit Sub
    End If
             
    ''' Actualizar los datos
    
    sdbgArticulos.MoveFirst
    sdbgArticulos.Refresh
    sdbgArticulos.Bookmark = sdbgArticulos.SelBookmarks(0)
    sdbgArticulos.SelBookmarks.RemoveAll
    sdbgArticulos.Refresh
        
    Set m_oIBAseDatosEnEdicion = Nothing
    Set m_oArticuloEnEdicion = Nothing
    
End Sub

Private Sub cmdDeshacerArticulo_Click()

    ''' * Objetivo: Deshacer la edicion en el Articulo actual
    
    sdbgArticulos.CancelUpdate
    sdbgArticulos.DataChanged = False
    DoEvents
        
    If Not m_oArticuloEnEdicion Is Nothing Then
        Set m_oIBAseDatosEnEdicion = m_oArticuloEnEdicion
        
        m_oIBAseDatosEnEdicion.CancelarEdicion
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oArticuloEnEdicion = Nothing
    End If
    
    If Not sdbgArticulos.IsAddRow Then
        cmdA�adirArticulo.Enabled = True
        cmdEliminarArticulo.Enabled = True
        cmdDeshacerArticulo.Enabled = False
        cmdCodigoArticulo.Enabled = True
    Else
        cmdDeshacerArticulo.Enabled = False
    End If
    
    m_CargarComboDesde = False
    
    Accion = ACCArtCon
    
End Sub

Private Sub cmdlistado_Click()
    
    ''' * Objetivo: Obtener un listado, pasar a selecci�n de par�metros listado
    
    If m_sOrdenListado = "DEN" Then
        frmLstMAT.optOrdDen.Value = True
    End If
    
    If ssMateriales.ActiveRow Is Nothing Then
        tvwEstrMat.Nodes.Item("Raiz").Selected = True
        frmLstMAT.opTodos.Value = True
    
    Else
        Screen.MousePointer = vbHourglass
        Select Case ssMateriales.ActiveRow.Band.Index
                
            Case 0  'GMN1
                        Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        g_oGMN1Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                        End If
                        Set g_oGMN2Seleccionado = Nothing
                        Set g_oGMN3Seleccionado = Nothing
                        Set g_oGMN4Seleccionado = Nothing
                        frmLstMAT.opPais.Value = True

            Case 1  'GMN2
                        Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        g_oGMN1Seleccionado.Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.GetParent.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.GetParent.Cells("DEN_SPA").Value
                        End If
                        
                        Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                        g_oGMN2Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN2Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN2Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                        End If
                        g_oGMN2Seleccionado.GMN1Cod = ssMateriales.ActiveRow.Cells("COD").Value
                        
                        Set g_oGMN3Seleccionado = Nothing
                        Set g_oGMN4Seleccionado = Nothing
                        frmLstMAT.opPais.Value = True
                        
            Case 2   'GMN3
                        Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        g_oGMN1Seleccionado.Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.GetParent.GetParent.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.GetParent.GetParent.Cells("DEN_SPA").Value
                        End If
                        
                        Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                        g_oGMN2Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                        g_oGMN2Seleccionado.Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN2Seleccionado.Den = ssMateriales.ActiveRow.GetParent.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN2Seleccionado.Den = ssMateriales.ActiveRow.GetParent.Cells("DEN_SPA").Value
                        End If
                        
                        Set g_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
                        g_oGMN3Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN3Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN3Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                        End If
                        g_oGMN3Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                        g_oGMN3Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                        
                        Set g_oGMN4Seleccionado = Nothing
                        frmLstMAT.opPais.Value = True
            
            Case 3   'GMN4
                        Set g_oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
                        g_oGMN1Seleccionado.Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN1Seleccionado.Den = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("DEN_SPA").Value
                        End If
                        
                        Set g_oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
                        g_oGMN2Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
                        g_oGMN2Seleccionado.Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN2Seleccionado.Den = ssMateriales.ActiveRow.GetParent.GetParent.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN2Seleccionado.Den = ssMateriales.ActiveRow.GetParent.GetParent.Cells("DEN_SPA").Value
                        End If
                        
                        Set g_oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
                        g_oGMN3Seleccionado.Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN3Seleccionado.Den = ssMateriales.ActiveRow.GetParent.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN3Seleccionado.Den = ssMateriales.ActiveRow.GetParent.Cells("DEN_SPA").Value
                        End If
                        g_oGMN3Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
                        g_oGMN3Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                        
                        Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
                        g_oGMN4Seleccionado.Cod = ssMateriales.ActiveRow.Cells("COD").Value
                        If gParametrosGenerales.gbSincronizacionMat = True Then
                            g_oGMN3Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Value
                        Else
                            g_oGMN3Seleccionado.Den = ssMateriales.ActiveRow.Cells("DEN_SPA").Value
                        End If
                        g_oGMN4Seleccionado.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
                        g_oGMN4Seleccionado.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
                        g_oGMN4Seleccionado.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
                        frmLstMAT.opPais.Value = True
                        
        End Select
    End If
    
    Screen.MousePointer = vbNormal
    
    If SSTabEstrMat.TabVisible(1) = True Then
        frmLstMAT.chkIncluirItems.Value = vbChecked
    End If
    frmLstMAT.PonerMatSeleccionado ("frmESTRMAT")
    frmLstMAT.CargarAtributos
    
    frmLstMAT.Show 1
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdModoEdicion_Click()
    
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not m_bModoEdicion Then
                
        'cmdRestaurar_Click
        
        If m_bModifArti Then
            sdbgArticulos.AllowAddNew = True
            sdbgArticulos.AllowDelete = True
            'sdbgArticulos.Columns("COD").Locked = False
            sdbgArticulos.Columns("DEN").Locked = False
            sdbgArticulos.Columns("UNI").Locked = False
            sdbgArticulos.Columns("CANT").Locked = False
            sdbgArticulos.Columns("CONCEP").Locked = False
            sdbgArticulos.Columns("ALMAC").Locked = False
            sdbgArticulos.Columns("RECEP").Locked = False
            sdbgArticulos.Columns("TIPORECEPCION").Locked = False
        End If
        
        If m_bModifArtiGen Then
            sdbgArticulos.Columns("GENERICO").Locked = False
        End If
        
'        Caption = "Art�culos (Edici�n)"
        caption = m_sIdiTitulos(6)
        
'        cmdModoEdicion.Caption = "&Consulta"
        cmdModoEdicion.caption = m_sIdiModos(2)
        
        cmdFiltrarArticulo.Visible = False
        cmdRestaurarArticulo.Visible = False
        cmdListadoArticulo.Visible = False
        cmdA�adirArticulo.Visible = True
        cmdEliminarArticulo.Visible = True
        cmdDeshacerArticulo.Visible = True
        cmdCodigoArticulo.Visible = m_bModifCodigo

        If gParametrosGenerales.gbMultiMaterial Then
            If Not cmdCodigoArticulo.Visible Then
            'Cuando es comprador no esta visible el
                cmdReubicar.Left = cmdCodigoArticulo.Left
            Else
                cmdReubicar.Left = cmdCodigoArticulo.Left + 1140
            End If
            
            cmdReubicar.Visible = True
        End If

        m_bModoEdicion = True
        
        AccionArti = ACCArtCon
        
    Else
                
        If (sdbgArticulos.DataChanged Or m_bValError Or m_bAnyaError Or m_bModError) Then
            v = sdbgArticulos.ActiveCell.Value
            If Me.Visible Then sdbgArticulos.SetFocus
            If (v <> "") Then
                sdbgArticulos.ActiveCell.Value = v
            End If
            
            m_bValError = False
            m_bAnyaError = False
            m_bModError = False
            If Not m_bHaCambiadoUon Then
                sdbgArticulos.Update
            End If
            
            If m_bValError Or m_bAnyaError Or m_bModError Then
                Exit Sub
            End If
            
        End If
        m_bHaCambiadoUon = False
        sdbgArticulos.AllowAddNew = False
        sdbgArticulos.AllowDelete = False
        sdbgArticulos.Columns("COD").Locked = True
        sdbgArticulos.Columns("DEN").Locked = True
        sdbgArticulos.Columns("UNI").Locked = True
        sdbgArticulos.Columns("CONCEP").Locked = True
        sdbgArticulos.Columns("ALMAC").Locked = True
        sdbgArticulos.Columns("RECEP").Locked = True
        sdbgArticulos.Columns("CANT").Locked = True
        sdbgArticulos.Columns("GENERICO").Locked = True
        sdbgArticulos.Columns("TIPORECEPCION").Locked = True
                
        cmdA�adirArticulo.Visible = False
        cmdEliminarArticulo.Visible = False
        cmdDeshacerArticulo.Visible = False
        cmdCodigoArticulo.Visible = False
        cmdFiltrarArticulo.Visible = True
        cmdRestaurarArticulo.Visible = True
        'marta
         If sdbgArticulos.Rows <> 0 Then
            cmdListadoArticulo.Visible = True
         End If
'        Caption = "Art�culos (Consulta)"
         caption = m_sIdiTitulos(4)
        
        cmdReubicar.Visible = False
        
'        cmdModoEdicion.Caption = "&Edici�n"
        cmdModoEdicion.caption = m_sIdiModos(1)
        
        cmdRestaurarArticulo_Click
        
        m_bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgArticulos.SetFocus
End Sub

''' <summary>Coloca los controles en el formulario en funci�n del tama�o de este</summary>
''' <remarks>Llamada desde: Form_Resize; Tiempo m�ximo: 0</remarks>
Private Sub Arrange()
    Dim i As Integer
    
    On Error Resume Next

    If Width >= 250 Then SSTabEstrMat.Width = Width - 250
    If Height >= 550 Then SSTabEstrMat.Height = Height - 550
    
    If Height >= 1600 Then
        ssMateriales.Height = Me.Height - 1600
        Picture1.Height = Height - 1600
    End If
    
    If Width >= 525 Then
        ssMateriales.Width = Me.Width - 525
        Picture1.Width = Width - 525
    End If
    
    picNavigate.Top = ssMateriales.Top + ssMateriales.Height
    picNavigate.Left = ssMateriales.Left
    Picture2.Top = Picture1.Top + Picture1.Height + 100
    Picture2.Width = Picture1.Width
    picEdicion.Top = picNavigate.Top
    
    sdbgArticulos.Width = Picture1.Width - 200
    sdbgArticulos.Height = Picture1.Height
    
    cmdModoEdicion.Left = Picture2.Left + Picture2.Width - cmdModoEdicion.Width - 125
    
    If FSEPConf Then
        With sdbgArticulos
            .Width = Picture1.Width
            .ScrollBars = ssScrollBarsBoth
            .Columns("COD").Width = .Width * 10 / 100 - 50
            .Columns("DEN").Width = (.Width * 20 / 100) - 350
            .Columns("CANT").Width = .Width * 11 / 100 - 1230
            .Columns("ESPEC").Width = (.Width * 14 / 100 - 250) / 2.8
            .Columns("ATRIBUTOS").Width = (.Width * 14 / 100 - 100) / 2.8
            .Columns("CONCEP").Width = (.Width * 3 / 100) + 675
            .Columns("ALMAC").Width = (.Width * 3 / 100) + 750
            .Columns("RECEP").Width = (.Width * 3 / 100) + 675
                        
            If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4) Then
                .Columns("UNI").Width = .Width * 4 / 100 + 50
                .Columns("INT").Visible = True
                .Columns("INT").Width = .Width * 5 / 100
            Else
                .Columns("UNI").Width = .Width * 6.5 / 100 + 300
                .Columns("INT").Visible = False
            End If
            .Columns("IMPUESTO").Width = (.Width * 13 / 100 - 20) / 2.8
        End With
    Else
        With sdbgArticulos
            .Columns("COD").Width = .Width * 10 / 100 - 50
            .Columns("DEN").Width = (.Width * 20 / 100) * 1.1 - 50
            .Columns("CANT").Width = .Width * 10 / 100 - 1230
            .Columns("ESPEC").Width = (.Width * 11 / 100 - 50) / 2.8
            .Columns("ATRIBUTOS").Width = (.Width * 12 / 100 - 30) / 2.8
            .Columns("CONCEP").Width = (.Width * 3 / 100) + 650
            .Columns("ALMAC").Width = (.Width * 3 / 100) + 750
            .Columns("RECEP").Width = (.Width * 3 / 100) + 650
            .ScrollBars = ssScrollBarsAutomatic
            If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4) Then
                .Columns("P").Width = (.Width * 8 / 100 - 50) / 2.8
                .Columns("UNI").Width = .Width * 5 / 100 + 50
                .Columns("INT").Visible = True
                .Columns("INT").Width = .Width * 2.5 / 100 - 30
            Else
                .Columns("P").Width = (.Width * 12 / 100 - 50) / 2.8
                .Columns("UNI").Width = .Width * 6 / 100 + 50
                .Columns("INT").Visible = False
            End If
            .Columns("IMPUESTO").Width = (.Width * 13.3 / 100 - 20) / 2.8
        End With
    End If
    If Not sdbgArticulos.Columns("IMPUESTO").Visible Then sdbgArticulos.Columns("DEN").Width = sdbgArticulos.Columns("DEN").Width + sdbgArticulos.Columns("IMPUESTO").Width
    
    sdbgArticulos.Columns("UNI").Width = sdbgArticulos.Width * 8 / 100 + 50
        
    'columnas de la ultragrid:
    With ssMateriales
        If .Bands.Count > 0 Then
            If gParametrosGenerales.gbSincronizacionMat Then
                If chkVerIdiomas.Value = vbChecked Then
                    Dim codWidth, NUMCOLS As Integer
                    codWidth = .Bands(0).Columns("COD").Width 'ancho de la banda m�s ancha
                    NUMCOLS = UBound(m_arrIdiomas) + 2 'uno por cada idioma m�s la columna tiporecepcion
                    
                    For i = 1 To UBound(m_arrIdiomas)
                        If .Width > 2200 Then
                            .Bands(0).Columns("DEN_" & m_arrIdiomas(i)).Width = (.Width - codWidth) / NUMCOLS
                            .Bands(1).Columns("DEN_" & m_arrIdiomas(i)).Width = (.Width - codWidth) / NUMCOLS
                            .Bands(2).Columns("DEN_" & m_arrIdiomas(i)).Width = (.Width - codWidth) / NUMCOLS
                            .Bands(3).Columns("DEN_" & m_arrIdiomas(i)).Width = (.Width - codWidth) / NUMCOLS
                        End If
                    Next i
                    .Bands(0).Columns("TIPORECEPCION").Width = (.Width - codWidth) / NUMCOLS
                    .Bands(0).Columns("CANTIDAD_MODIFICABLE").Width = (.Width - codWidth) / NUMCOLS
                Else
                    If .Width > 3000 Then
                        .Bands(0).Columns("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Width = .Width - 3000
                        .Bands(1).Columns("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Width = .Width - 3000
                        .Bands(2).Columns("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Width = .Width - 3000
                        .Bands(3).Columns("DEN_" & basPublic.gParametrosInstalacion.gIdiomaPortal).Width = .Width - 3000
                    End If
                End If
            
            Else
                If .Width > 3000 Then
                    .Bands(0).Columns("DEN_SPA").Width = .Width - 3000
                    .Bands(1).Columns("DEN_SPA").Width = .Width - 3000
                    .Bands(2).Columns("DEN_SPA").Width = .Width - 3000
                    .Bands(3).Columns("DEN_SPA").Width = .Width - 3000
                End If
            End If
        End If
        .Refresh ssRefreshDisplay
    End With
End Sub

Private Sub cmdRestaurarArticulo_Click()

    ''' * Objetivo: Restaurar el contenido de las grid
    ''' * Objetivo: desde la base de datos
    
'    Me.Caption = "Art�culos (Consulta)"
    Me.caption = m_sIdiTitulos(4)
    
    CargarGridArticulos
    
    sdbgArticulos.ReBind
    If Me.Visible Then sdbgArticulos.SetFocus
    
End Sub

Private Sub Form_Resize()

    ''' * Objetivo: Adecuar los controles
    
    Arrange
    
End Sub

Private Sub sdbddUnidades_DropDown()

    If sdbgArticulos.Columns("UNI").Locked Then
        sdbddUnidades.DroppedDown = False
        Exit Sub
    End If
    
    ''' * Objetivo: Abrir el combo de monedas de la forma adecuada
    Screen.MousePointer = vbHourglass
    
    If m_CargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidadesDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbgArticulos.ActiveCell.Value), , , False
    Else
        m_oUnidades.CargarTodasLasUnidades , , , , False
    End If
        
    CargarGridConUnidades
    
    sdbgArticulos.ActiveCell.SelStart = 0
    sdbgArticulos.ActiveCell.SelLength = Len(sdbgArticulos.ActiveCell.Text)
     
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbddUnidades_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    sdbddUnidades.DataFieldList = "Column 0"
    sdbddUnidades.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbddUnidades_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddUnidades.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddUnidades.Rows - 1
            bm = sdbddUnidades.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddUnidades.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddUnidades.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Private Sub sdbddUnidades_ValidateList(Text As String, RtnPassed As Integer)

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        ''' Comprobar la existencia en la lista
        
        If Not m_oUnidades.Item(Text) Is Nothing Then
            RtnPassed = True

        Else
            sdbgArticulos.Columns("UNI").Value = ""
        End If
    
    End If
    
End Sub
Private Sub sdbgArticulos_AfterDelete(RtnDispErrMsg As Integer)
    
    ''' * Objetivo: Posicionar el bookmark en la fila
    ''' * Objetivo: actual despues de eliminar
    RtnDispErrMsg = 0
    If sdbgArticulos.Rows > 0 Then
        If IsEmpty(sdbgArticulos.RowBookmark(sdbgArticulos.Row)) Then
            sdbgArticulos.Bookmark = sdbgArticulos.RowBookmark(sdbgArticulos.Row - 1)
        Else
            sdbgArticulos.Bookmark = sdbgArticulos.RowBookmark(sdbgArticulos.Row)
        End If
    End If
    If Me.Visible Then sdbgArticulos.SetFocus
    
End Sub
Private Sub sdbgArticulos_AfterInsert(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    If m_bAnyaError = False Then
        cmdA�adirArticulo.Enabled = True
        cmdEliminarArticulo.Enabled = True
        cmdDeshacerArticulo.Enabled = False
        cmdCodigoArticulo.Enabled = True
    End If
    
    'Si el registro es el primero, se le pone como seleccionado
    If sdbgArticulos.Rows = 1 Then sdbgArticulos.Bookmark = 0
    
End Sub
Private Sub sdbgArticulos_AfterUpdate(RtnDispErrMsg As Integer)

    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    RtnDispErrMsg = 0
    If m_bAnyaError = False And m_bModError = False Then
        cmdA�adirArticulo.Enabled = True
        cmdEliminarArticulo.Enabled = True
        cmdDeshacerArticulo.Enabled = False
        cmdCodigoArticulo.Enabled = True
    End If
    
End Sub
Private Sub sdbgArticulos_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

    ''' * Objetivo: Confirmacion antes de eliminar
'    Dim irespuesta As Integer
'    Dim oArticulo As CArticulo
'
    DispPromptMsg = 0
'
'    If sdbgArticulos.IsAddRow Or Accion = ACCArtMod Then
'        Cancel = True
'        Exit Sub
'    End If
'
'    irespuesta = omensajes.PreguntaEliminar(m_sIdiArt & " " & sdbgArticulos.Columns("COD").Value & " (" & sdbgArticulos.Columns("DEN").Value & ")")
'
'    If irespuesta = vbNo Then Cancel = True
'
'    'compruebo si existen presupuestos para ese art�culo y unidad:
'    Set oArticulo = g_oArticulos.Item(CStr(sdbgArticulos.Bookmark))
'    If Not oArticulo Is Nothing Then
'        If oArticulo.ExistenPresupuestos = True Then
'            irespuesta = omensajes.PreguntaEliminarArticuloConPresup
'            If irespuesta = vbNo Then
'                Cancel = True
'                Exit Sub
'            End If
'        End If
'    End If
'    Set oArticulo = Nothing
    
End Sub
Private Sub sdbgArticulos_BeforeUpdate(Cancel As Integer)

    ''' * Objetivo: Validar los datos
    Dim irespuesta As Integer
    
    If m_bRespetar Then Exit Sub
    
    m_bValError = False
    Cancel = False
    
    If Trim(sdbgArticulos.Columns("COD").Value) = "" Then
        oMensajes.NoValido m_sIdiCod
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgArticulos.Columns("DEN").Value) = "" Then
        oMensajes.NoValida m_sIdiDen
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgArticulos.Columns("UNI").Value) = "" Then
        oMensajes.NoValida m_sIdiUniPDef
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgArticulos.Columns("CONCEP").Value) = "" Then
        oMensajes.NoValida m_sIdiConcepto
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgArticulos.Columns("ALMAC").Value) = "" Then
        oMensajes.NoValida m_sIdiAlmacenable
        Cancel = True
        GoTo Salir
    End If
    
    If Trim(sdbgArticulos.Columns("RECEP").Value) = "" Then
        oMensajes.NoValida m_sIdiRecepcionable
        Cancel = True
        GoTo Salir
    End If

    If Trim(sdbgArticulos.Columns("CANT").Value) <> "" Then
        If Not IsNumeric(sdbgArticulos.Columns("CANT").Value) Then
            oMensajes.NoValida m_sIdiCant
            Cancel = True
            GoTo Salir
        End If
    End If
    
    
    
    If sdbgArticulos.Row = -1 Then
        Cancel = True
        GoTo Salir
    End If
    
    'si se modifica la unidad compruebo si existen presupuestos para ese art�culo y unidad
    If Not m_oArticuloEnEdicion Is Nothing Then
        If sdbgArticulos.Columns("UNI").Value <> m_oArticuloEnEdicion.CodigoUnidad Then
            If m_oArticuloEnEdicion.ExistenPresupuestos = True Then
                irespuesta = oMensajes.ExistenPresupuestosParaUnidad
                If irespuesta = vbNo Then
                    Cancel = True
                    GoTo Salir
                End If
            End If
        End If
    End If
    
Salir:
        
    m_bValError = Cancel
    If Me.Visible Then sdbgArticulos.SetFocus
    
End Sub

''' <summary>
''' Evento que se produce al haber cambios en el grid
''' </summary>
''' <returns></returns>
''' <remarks>Llamada desde Evento que se produce al haber cambios en el grid; Tiempo m�ximo</remarks>
Private Sub sdbgArticulos_Change()
    Dim oArticulo As CArticulo
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    Dim bk As Variant
    Dim oArt As CArticulo
    Dim bGen As Boolean
    Dim bValidado As Boolean
    'Selecci�n m�ltiple de articulos
    If Not m_aBookmarks Is Nothing Then
        If sdbgArticulos.Columns(sdbgArticulos.col).Name = "GENERICO" Then
            If m_aBookmarks.Count > 0 Then
                Screen.MousePointer = vbHourglass
                bGen = sdbgArticulos.Columns("GENERICO").Value
                For Each bk In m_aBookmarks
                    Set oArticulo = g_oGMN4Seleccionado.ARTICULOS.Item(CStr(bk))
                    If bGen Then
                        bValidado = True
                    Else
                        bValidado = validarErpsEntrada(oArticulo)
                    End If
                    If bValidado And oArticulo.Generico <> bGen Then
                        teserror = g_oArticulos.ActualizarGenerico(bGen, oArticulo)
                        If teserror.Arg2 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.ImposibleModificarMultiplesArticulos teserror.Arg1
                            Set m_oArticulosAModificar = Nothing
                        Else
                            RegistrarAccion accionessummit.ACCArtMod, "Art�culo: " & oArticulo.Cod & " Concepto: " & oArticulo.Concepto
                        End If
                    Else
                        sdbgArticulos.CancelUpdate
                    End If
                Next
                sdbgArticulos.Update
                sdbgArticulos.ReBind
                
                Set m_aBookmarks = Nothing
                
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            If sdbgArticulos.Columns(sdbgArticulos.col).Name = "TIPORECEPCION" Then
                If m_aBookmarks.Count > 0 Then
                    Screen.MousePointer = vbHourglass
                    teserror = g_oArticulos.actualizarRecepcion(sdbgArticulos.Columns(sdbgArticulos.col).Value, m_aBookmarks)
                    If teserror.Arg2 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.ImposibleModificarMultiplesArticulos teserror.Arg1
                        Set m_oArticulosAModificar = Nothing
                        'Vuelvo a seleccionar las filas modificadas
            
                        If m_aBookmarks.Count > 0 Then
                            For Each bk In m_aBookmarks
                                sdbgArticulos.SelBookmarks.Add bk
                            Next
                        End If
                    Else
                        For Each oArt In m_oArticulosAModificar
                            RegistrarAccion accionessummit.ACCArtMod, "Art�culo: " & oArt.Cod & " Concepto: " & oArt.Concepto
                        Next
                    End If
                    sdbgArticulos.Update
                    sdbgArticulos.ReBind
                    If m_aBookmarks.Count > 0 Then
                        For Each bk In m_aBookmarks
                            sdbgArticulos.SelBookmarks.Add bk
                        Next
                    End If
                    Set m_aBookmarks = Nothing
                    
                    Screen.MousePointer = vbNormal
                    sdbgArticulos.Refresh
                    Exit Sub
                End If
            End If
        End If
    End If

    'Selecci�n simple de art�culos
    If cmdDeshacerArticulo.Enabled = False Then
    
        cmdA�adirArticulo.Enabled = False
        cmdEliminarArticulo.Enabled = False
        cmdDeshacerArticulo.Enabled = True
        cmdCodigoArticulo.Enabled = False
    
    End If
        
    If sdbgArticulos.col = 2 Then
        If Len(sdbgArticulos.Columns("DEN").Value) + 1 > basParametros.gLongitudesDeCodigos.giLongCodDENART Then
            sdbgArticulos.Columns("DEN").Value = Left(sdbgArticulos.Columns("DEN").Value, basParametros.gLongitudesDeCodigos.giLongCodDENART)
        End If
    End If
        
    If Accion = ACCArtCon And Not sdbgArticulos.IsAddRow Then
    
        Set m_oArticuloEnEdicion = Nothing
        Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(sdbgArticulos.Bookmark))
    
        Set m_oIBAseDatosEnEdicion = m_oArticuloEnEdicion
        
        Screen.MousePointer = vbHourglass
        teserror = m_oIBAseDatosEnEdicion.IniciarEdicion
        
        If teserror.NumError = TESInfModificada Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            sdbgArticulos.DataChanged = False
            sdbgArticulos.Columns("COD").Value = m_oArticuloEnEdicion.Cod
            sdbgArticulos.Columns("DEN").Value = m_oArticuloEnEdicion.Den
            sdbgArticulos.Columns("CANT").Value = m_oArticuloEnEdicion.Cantidad
            sdbgArticulos.Columns("UNI").Value = m_oArticuloEnEdicion.CodigoUnidad
            sdbgArticulos.Columns("CONCEP").Value = m_oArticuloEnEdicion.Concepto
            sdbgArticulos.Columns("ALMAC").Value = m_oArticuloEnEdicion.Almacenable
            sdbgArticulos.Columns("RECEP").Value = m_oArticuloEnEdicion.Recepcionable
            sdbgArticulos.Columns("TIPORECEPCION").Value = m_oArticuloEnEdicion.isRecepcionImporte
            
            teserror.NumError = TESnoerror
        End If
        
        If teserror.NumError <> TESnoerror Then
            Screen.MousePointer = vbNormal
            TratarError teserror
            If Me.Visible Then sdbgArticulos.SetFocus
        Else
            Accion = ACCArtMod
        End If
                    
    End If

    If sdbgArticulos.col = 3 Then
        m_CargarComboDesde = True
    End If
      
    If sdbgArticulos.Columns("UNI").Text = "" And sdbgArticulos.IsAddRow Then
        sdbgArticulos.Columns("UNI").Text = gParametrosGenerales.gsUNIDEF
        sdbddUnidades_ValidateList sdbgArticulos.Columns("UNI").Text, True
        cmdDeshacerArticulo.Enabled = True
    End If
    
    If sdbgArticulos.Columns("CONCEP").Text = "" And sdbgArticulos.IsAddRow Then
        sdbgArticulos.Columns("CONCEP").Text = m_sTextConcepAlmacRecep(0, 2)
        cmdDeshacerArticulo.Enabled = True
    End If
    
    If sdbgArticulos.Columns("ALMAC").Text = "" And sdbgArticulos.IsAddRow Then
        sdbgArticulos.Columns("ALMAC").Text = m_sTextConcepAlmacRecep(1, 2)
        cmdDeshacerArticulo.Enabled = True
    End If
    
    If sdbgArticulos.Columns("RECEP").Text = "" And sdbgArticulos.IsAddRow Then
        sdbgArticulos.Columns("RECEP").Text = m_sTextConcepAlmacRecep(2, 2)
        cmdDeshacerArticulo.Enabled = True
    End If

    'Comprobamos si se ha cambiado el check de GENERICO
    Set m_oArticuloEnEdicion = Nothing
    
    If Not (IsNull(sdbgArticulos.Bookmark) Or sdbgArticulos.IsAddRow) Then
        Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(sdbgArticulos.Bookmark))

        If sdbgArticulos.Columns(sdbgArticulos.col).Name = "GENERICO" Then
            If GridCheckToBoolean(sdbgArticulos.Columns("GENERICO").Value) <> m_oArticuloEnEdicion.Generico Then
                'If sdbgArticulos.Columns("GENERICO").Value = 0 And gParametrosGenerales.gbArtGenericosUonErp = True Then
                If sdbgArticulos.Columns("GENERICO").Value = 0 Then
                    If Not validarErpsEntrada(m_oArticuloEnEdicion) Then
                        sdbgArticulos.CancelUpdate
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    End If
                End If
                teserror = m_oArticuloEnEdicion.ActualizarGenerico(GridCheckToBoolean(sdbgArticulos.Columns("GENERICO").Value))
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    If Me.Visible Then sdbgArticulos.SetFocus
                Else
                    RegistrarAccion accionessummit.ACCArtMod, "Art�culo: " & m_oArticuloEnEdicion.Cod & " Generico: " & CStr(sdbgArticulos.Columns("GENERICO").Value)
                End If
                Accion = ACCArtCon
                'Antes de hacer sdbgArticulos.Update de GENERICO comprobamos si
                'la denominaci�n, cantidad o unidad se ha modificado.
                'Si se ha modificado m_bRespetar es false y
                'se llama a FINALIZAREDICIONMODIFICANDO
                If sdbgArticulos.Columns("DEN").Value = m_oArticuloEnEdicion.Den And sdbgArticulos.Columns("CANT").Value = m_oArticuloEnEdicion.Cantidad _
                    And sdbgArticulos.Columns("UNI").Value = m_oArticuloEnEdicion.CodigoUnidad And sdbgArticulos.Columns("CONCEP").Value = m_oArticuloEnEdicion.Concepto _
                    And sdbgArticulos.Columns("ALMAC").Value = m_oArticuloEnEdicion.Almacenable And sdbgArticulos.Columns("RECEP").Value = m_oArticuloEnEdicion.Recepcionable Then
                        m_bRespetar = True
                End If
                
                sdbgArticulos.Update
                m_bRespetar = False
            End If
        End If
        Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(sdbgArticulos.Bookmark))
        
        If sdbgArticulos.Columns(sdbgArticulos.col).Name = "TIPORECEPCION" Then
            If GridCheckToBoolean(sdbgArticulos.Columns("TIPORECEPCION").Value) <> m_oArticuloEnEdicion.isRecepcionImporte() Then
                teserror = m_oArticuloEnEdicion.actualizarRecepcion(GridCheckToBoolean(sdbgArticulos.Columns("TIPORECEPCION").Value))
                If teserror.NumError <> TESnoerror Then
                    Screen.MousePointer = vbNormal
                    TratarError teserror
                    If Me.Visible Then sdbgArticulos.SetFocus
                Else
                    RegistrarAccion accionessummit.ACCArtMod, "Art�culo: " & m_oArticuloEnEdicion.Cod & " Generico: " & CStr(sdbgArticulos.Columns("GENERICO").Value)
                End If
                Accion = ACCArtCon
                'Antes de hacer sdbgArticulos.Update de GENERICO comprobamos si
                'la denominaci�n, cantidad o unidad se ha modificado.
                'Si se ha modificado m_bRespetar es false y
                'se llama a FINALIZAREDICIONMODIFICANDO
                If sdbgArticulos.Columns("DEN").Value = m_oArticuloEnEdicion.Den And sdbgArticulos.Columns("CANT").Value = m_oArticuloEnEdicion.Cantidad _
                    And sdbgArticulos.Columns("UNI").Value = m_oArticuloEnEdicion.CodigoUnidad And sdbgArticulos.Columns("CONCEP").Value = m_oArticuloEnEdicion.Concepto _
                    And sdbgArticulos.Columns("ALMAC").Value = m_oArticuloEnEdicion.Almacenable And sdbgArticulos.Columns("RECEP").Value = m_oArticuloEnEdicion.Recepcionable Then
                   
                        m_bRespetar = True
                End If
                sdbgArticulos.Update
                m_bRespetar = False
            End If
        End If
    End If
 
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbgArticulos_HeadClick(ByVal ColIndex As Integer)

    ''' * Objetivo: Ordenar el grid segun la columna
    On Error GoTo err
    
    Dim sCodigo As String
    Dim lCodigo As Integer
    Dim sDenominacion As String
    Dim lDenominacion As Integer
    Dim sHeadCaption As String
    If m_bModoEdicion Then Exit Sub
    
   
    Screen.MousePointer = vbHourglass
    
    sHeadCaption = sdbgArticulos.Columns(ColIndex).caption
'    sdbgArticulos.Columns(ColIndex).Caption = "Ordenando...."
    sdbgArticulos.Columns(ColIndex).caption = m_sIdiOrden
    m_sOrdenListado = sdbgArticulos.Columns(ColIndex).Name
    ''' Volvemos a cargar los articulos, ordenados segun la columna
    ''' en cuyo encabezado se ha hecho 'clic'.
    Select Case gParametrosGenerales.giNEM
                
        Case 1
                    
                        
        Case 2
                    
        Case 3
                    
        Case 4
                
'                If Caption <> "Art�culos (Consulta)" Then
                 If caption <> m_sIdiTitulos(4) Then
                
'                    If InStr(1, Caption, "Codigo", vbTextCompare) <> 0 Then
                     If InStr(1, caption, m_sIdiTitulos(1), vbTextCompare) <> 0 Then
                    
                        ''' Datos filtrados por codigo
                        
                        If InStr(1, caption, "*", vbTextCompare) <> 0 Then
                            
                            lCodigo = Len(caption) - Len(m_sIdiTitulos(1))
                            sCodigo = Mid(caption, Len(m_sIdiTitulos(1)) + 1, lCodigo - 1)
'                            sCodigo = Right(Caption, Len(Caption) - 28)
 '                           sCodigo = Left(sCodigo, Len(sCodigo) - 2)
                            
                            Select Case ColIndex
                            Case 0 'Por estado de integraci�n
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), True, True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 1 'Por codigo
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 2  'Por den
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , , True, , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 4  ' Por cantidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , , , True, , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 3 ' Por unidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , , , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case Else
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , , , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, orderColumn:=m_sOrdenListado, orderascdesc:=m_sOrderAscDesc, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                                cambiarOrdenacionAscDesc
                            End Select
                            
                        Else
                        
'                           sCodigo = Right(Caption, Len(Caption) - 28)
'                           sCodigo = Left(sCodigo, Len(sCodigo) - 1)
                            lCodigo = Len(caption) - Len(m_sIdiTitulos(1))
                            sCodigo = Mid(caption, Len(m_sIdiTitulos(1)) + 1, lCodigo)
                            
                            Select Case ColIndex
                            Case 0 'Por estado integraci�n
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , True, , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), True, True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 1 'Por codigo
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , True, , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 2  'Por den
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , True, True, , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 4  ' Por cantidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , True, , True, , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 3 ' Por unidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , True, , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case Else
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sCodigo), , True, , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, orderColumn:=m_sOrdenListado, orderascdesc:=m_sOrderAscDesc, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                                cambiarOrdenacionAscDesc
                            End Select
                            
                        End If
                        
                    Else
                        
                        ''' Datos filtrados por denominacion
                        
                        lDenominacion = Len(caption) - Len(m_sIdiTitulos(2))
                        sDenominacion = Mid(caption, Len(m_sIdiTitulos(2)) + 1, lDenominacion)
'                        sDenominacion = Right(Caption, Len(Caption) - 34)
                        
                        If InStr(1, sDenominacion, "*", vbTextCompare) <> 0 Then
                        
                            
'                            sDenominacion = Left(sDenominacion, Len(sDenominacion) - 2)
                            sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                            
                            Select Case ColIndex
                            Case 0 'Por estado integraci�n
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), True, True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 1 'Por codigo
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 2  'Por den
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), , True, , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 4  ' Por cantidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), , , True, , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 3 ' Por unidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), , , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case Else
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), , , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, orderColumn:=m_sOrdenListado, orderascdesc:=m_sOrderAscDesc, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                                cambiarOrdenacionAscDesc
                            End Select
                                        
                        Else
                        
'                            sDenominacion = Left(sDenominacion, Len(sDenominacion) - 1)
                            sDenominacion = Left(sDenominacion, Len(sDenominacion))
                            
                            Select Case ColIndex
                            Case 0 'Por estado de integraci�n
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), True, , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), True, True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 1 'Por codigo
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), True, , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 2  'Por den
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), True, True, , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 4  ' Por cantidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), True, , True, , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case 3 ' Por unidad
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), True, , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                            Case Else
                                g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , Trim(sDenominacion), True, , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, orderColumn:=m_sOrdenListado, orderascdesc:=m_sOrderAscDesc, bImpuestos:=m_bConsultaImpuesto, bMostrarUon:=True
                                cambiarOrdenacionAscDesc
                            End Select
                        
                        End If
                            
                    End If
                        
                
                Else
                    
                    ''' Datos no filtrados
                    
                    Select Case ColIndex
                        Case 0 'Por estado de integraci�n
                            g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), True, True, bMostrarUon:=True
                        Case 1 'Por codigo
                            g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bMostrarUon:=True
                        Case 2  'Por den
                            g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , True, , , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bMostrarUon:=True
                        Case 4  ' Por cantidad
                            g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , True, , True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bMostrarUon:=True
                        Case 3 ' Por unidad
                            g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, bMostrarUon:=True
                        Case 11, 12 ' Aquellas columnas con nombre igual al campo de la consulta
                            g_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , , , True, True, True, basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4), , True, orderColumn:=m_sOrdenListado, orderascdesc:=m_sOrderAscDesc, bMostrarUon:=True
                            cambiarOrdenacionAscDesc
                        End Select
                
                End If
        Select Case ColIndex
        Case 1, 0
            m_sOrdenListado = "COD"
        Case 2
            m_sOrdenListado = "DEN"
        Case 3
            m_sOrdenListado = "CAN"
        Case 4
            m_sOrdenListado = "UNI"
        End Select
        
        Set g_oArticulos = Nothing
        Set g_oArticulos = g_oGMN4Seleccionado.ARTICULOS
        
    End Select
        
    Screen.MousePointer = vbNormal
    
    sdbgArticulos.ReBind
    sdbgArticulos.Columns(ColIndex).caption = sHeadCaption
    DoEvents
    Exit Sub
err:
    MsgBox "Imposible ordenar listado por ese concepto"
End Sub

Private Sub sdbgArticulos_InitColumnProps()
    
    sdbgArticulos.Columns("COD").FieldLen = basParametros.gLongitudesDeCodigos.giLongCodART
    sdbgArticulos.Columns("UNI").FieldLen = basParametros.gLongitudesDeCodigos.giLongCodUNI
    sdbgArticulos.Columns("DEN").FieldLen = basParametros.gLongitudesDeCodigos.giLongCodDENART
    
End Sub

Private Sub sdbgArticulos_KeyPress(KeyAscii As Integer)

    ''' * Objetivo: Restaurar la situacion normal
    ''' * Objetivo: si no hay cambios pendientes
    If KeyAscii = vbKeyReturn Or KeyAscii = vbKeySpace Then
        sdbgArticulos_BtnClick
    End If
    
    
    If KeyAscii = vbKeyEscape Then
                                
        If sdbgArticulos.DataChanged = False Then
            
            sdbgArticulos.CancelUpdate
            sdbgArticulos.DataChanged = False
            
            If Not m_oArticuloEnEdicion Is Nothing Then
                m_oIBAseDatosEnEdicion.CancelarEdicion
                Set m_oIBAseDatosEnEdicion = Nothing
                Set m_oArticuloEnEdicion = Nothing
            End If
           
            m_CargarComboDesde = False
            
            cmdA�adirArticulo.Enabled = True
            cmdEliminarArticulo.Enabled = True
            cmdDeshacerArticulo.Enabled = False
            cmdCodigoArticulo.Enabled = True

            Accion = ACCArtCon
            
        Else
        
            If sdbgArticulos.IsAddRow Then
           
                cmdA�adirArticulo.Enabled = True
                cmdEliminarArticulo.Enabled = True
                cmdDeshacerArticulo.Enabled = False
                cmdCodigoArticulo.Enabled = True
           
                Accion = ACCArtCon
            
            End If
            
        End If
        
    End If
    
End Sub

''' <summary>
''' Evento que se produce al cambiar de columna o de fila de la grid
''' </summary>
''' <returns></returns>
''' <param name="LastRow">Linea</param>
''' <param name="LastCol">Columna</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>
Private Sub sdbgArticulos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

    Dim bk As Variant
    Dim sCol As String
    ''' * Objetivo: Configurar la fila segun
    ''' * Objetivo: sea o no la fila de adicion
    
    If sdbgArticulos.col = -1 Then
        Set m_aBookmarks = New Collection
        Set m_oArticulosAModificar = oFSGSRaiz.Generar_CArticulos
        For Each bk In sdbgArticulos.SelBookmarks
            m_aBookmarks.Add bk
        Next
    Else
        If sdbgArticulos.Columns(sdbgArticulos.col).Name = "CONCEP" Or sdbgArticulos.Columns(sdbgArticulos.col).Name = "ALMAC" Or sdbgArticulos.Columns(sdbgArticulos.col).Name = "RECEP" Or sdbgArticulos.Columns(sdbgArticulos.col).Name = "GENERICO" Or sdbgArticulos.Columns(sdbgArticulos.col).Name = "TIPORECEPCION" Then
                If sdbgArticulos.SelBookmarks.Count = 0 And Not m_aBookmarks Is Nothing Then
                    For Each bk In m_aBookmarks
                        sdbgArticulos.SelBookmarks.Add bk
                    Next
                End If
        Else
            Set m_aBookmarks = Nothing
        End If
        
        If Not sdbgArticulos.IsAddRow Then
            sCol = sdbgArticulos.Columns(sdbgArticulos.col).Name
            'Edicion
            
            If sdbgArticulos.DataChanged = False Then
                cmdDeshacerArticulo.Enabled = False
            End If
            
            'Validaciones erp entrada
            If sCol = "UON_DEN" Or sCol = "ATRIBUTOS" Or sCol = "ESP" Then
                sdbgArticulos.Columns(sdbgArticulos.col).Locked = False
            ElseIf sCol = "GENERICO" Or sCol = "TIPORECEPCION" Then
                sdbgArticulos.Columns(sdbgArticulos.col).Locked = Not m_bModoEdicion
            Else
                If Me.tieneArticuloUonsEntrada(Me.ArticuloSeleccionado) And Me.ArticuloSeleccionado.AltaIntegracion Then
                    sdbgArticulos.Columns(sdbgArticulos.col).Locked = True
                    Me.cmdCodigoArticulo.Enabled = False
                    Me.cmdEliminarArticulo.Enabled = False
                    Me.cmdReubicar.Enabled = False
                Else
                    sdbgArticulos.Columns(sdbgArticulos.col).Locked = Not m_bModoEdicion
                    Me.cmdCodigoArticulo.Enabled = True
                    Me.cmdEliminarArticulo.Enabled = True
                    Me.cmdReubicar.Enabled = True
                End If
            End If
            sdbgArticulos.Columns("COD").Locked = True
        Else
            'alta
            sdbgArticulos.Columns(sdbgArticulos.col).Locked = False
            If sdbgArticulos.DataChanged = True Then cmdDeshacerArticulo.Enabled = True
            sdbgArticulos.Columns("COD").Locked = False
            'sugerir
            If Not IsNull(LastRow) Then
                If val(LastRow) <> val(sdbgArticulos.Row) Then
                    If gParametrosInstalacion.gbSugerirCodArticulos Then
                        If sdbgArticulos.Columns("COD").Value = "" Then
                            If g_oArticulos Is Nothing Then
                                Set g_oArticulos = oFSGSRaiz.Generar_CArticulos
                            End If
                            sdbgArticulos.Columns("COD").Value = g_oArticulos.SugerirCodigoDeArticulo(g_oGMN4Seleccionado.GMN1Cod, g_oGMN4Seleccionado.GMN2Cod, g_oGMN4Seleccionado.GMN3Cod, g_oGMN4Seleccionado.Cod)
                            'Ponemos los botones en posici�n
                            If cmdDeshacerArticulo.Enabled = False Then
                                cmdA�adirArticulo.Enabled = False
                                cmdEliminarArticulo.Enabled = False
                                cmdDeshacerArticulo.Enabled = True
                                cmdCodigoArticulo.Enabled = False
                            End If
                            sdbgArticulos.col = 2
                        End If
                    Else
                        cmdDeshacerArticulo.Enabled = False
                        sdbgArticulos.col = 1
                    End If
                End If
            Else
                If gParametrosInstalacion.gbSugerirCodArticulos Then
                    If sdbgArticulos.Columns("COD").Value = "" Then
                        If g_oArticulos Is Nothing Then
                            Set g_oArticulos = oFSGSRaiz.Generar_CArticulos
                        End If
                        sdbgArticulos.Columns("COD").Value = g_oArticulos.SugerirCodigoDeArticulo(g_oGMN4Seleccionado.GMN1Cod, g_oGMN4Seleccionado.GMN2Cod, g_oGMN4Seleccionado.GMN3Cod, g_oGMN4Seleccionado.Cod)
                        'Ponemos los botones en posici�n
                        If cmdDeshacerArticulo.Enabled = False Then
                            cmdA�adirArticulo.Enabled = False
                            cmdEliminarArticulo.Enabled = False
                            cmdDeshacerArticulo.Enabled = True
                            cmdCodigoArticulo.Enabled = False
                        End If
                        sdbgArticulos.col = 2
                    End If
                End If
            End If
            cmdA�adirArticulo.Enabled = False
            cmdEliminarArticulo.Enabled = False
            cmdCodigoArticulo.Enabled = False
        End If
    End If

    If sdbgArticulos.col = 3 Then
        m_CargarComboDesde = False
    End If

End Sub

''' * Objetivo: Hemos anyadido una fila al grid,
''' * Objetivo: ahora hay que a�adirla a la
''' * Objetivo: coleccion
''' * Recibe: Buffer con los datos a a�adir y bookmark de la fila
Private Sub sdbgArticulos_UnboundAddData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, NewRowBookmark As Variant)
    Dim teserror As TipoErrorSummit
    Dim v As Variant
    
    m_bAnyaError = False
        
    ''' A�adir a la colecci�n
    g_oArticulos.Add ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.Cells("COD").Value, "", "", 0, "", g_oArticulos.Count
    
    ''' A�adir a la base de datos
    Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(g_oArticulos.Count - 1))
    
    m_oArticuloEnEdicion.Cod = EliminarEspacioYTab("" & sdbgArticulos.Columns("COD").Value)
    m_oArticuloEnEdicion.Den = sdbgArticulos.Columns("DEN").Value
    m_oArticuloEnEdicion.Cantidad = StrToDblOrNull(sdbgArticulos.Columns("CANT").Value)
    m_oArticuloEnEdicion.CodigoUnidad = sdbgArticulos.Columns("UNI").Value
    m_oArticuloEnEdicion.Concepto = TraducirTextoCelda(0, sdbgArticulos.Columns("CONCEP").Value)  '''Error de tipos
    m_oArticuloEnEdicion.Almacenable = TraducirTextoCelda(1, sdbgArticulos.Columns("ALMAC").Value)
    m_oArticuloEnEdicion.Recepcionable = TraducirTextoCelda(2, sdbgArticulos.Columns("RECEP").Value)
    m_oArticuloEnEdicion.tipoRecepcion = GridCheckToBoolean(sdbgArticulos.Columns("TIPORECEPCION").Value)
        ''' pasamos el usuario conectado para el LOG e Integraci�n
    m_oArticuloEnEdicion.Usuario = basOptimizacion.gvarCodUsuario
    m_oArticuloEnEdicion.Generico = GridCheckToBoolean(sdbgArticulos.Columns("GENERICO").Value)
    m_oArticuloEnEdicion.AltaIntegracion = False
    
    If m_oArticuloEnEdicion.uons.Count > 1 Then
        m_oArticuloEnEdicion.NombreUON = m_sVariasUnidades
    Else
        m_oArticuloEnEdicion.NombreUON = sdbgArticulos.Columns("UON_DEN").Text
    End If
    Set m_oIBAseDatosEnEdicion = m_oArticuloEnEdicion
    
    Screen.MousePointer = vbHourglass
    teserror = m_oIBAseDatosEnEdicion.AnyadirABaseDatos
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        v = sdbgArticulos.ActiveCell.Value
        g_oArticulos.Remove (CStr(g_oArticulos.Count - 1))
        TratarError teserror
        If Me.Visible Then sdbgArticulos.SetFocus
        m_bAnyaError = True
        RowBuf.RowCount = 0
        sdbgArticulos.ActiveCell.Value = v
    Else
        basSeguridad.RegistrarAccion accionessummit.ACCArtAnya, "Cod:" & m_oArticuloEnEdicion.Cod
        
        Accion = ACCArtCon
    End If
    
    Set m_oIBAseDatosEnEdicion = Nothing
End Sub
Private Sub sdbgArticulos_UnboundDeleteRow(Bookmark As Variant)

    ''' * Objetivo: Hemos eliminado una fila al grid,
    ''' * Objetivo: ahora hay que eliminarla de la
    ''' * Objetivo: coleccion
    ''' * Recibe: Bookmark de la fila
    
'    Dim teserror As TipoErrorSummit
'
'    Dim IndFor As Long, IndArt As Long
'
'    ''' Eliminamos de la base de datos
'
'    Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(Bookmark))
'
'    ''' pasamos el usuario conectado para el LOG e Integraci�n
'    m_oArticuloEnEdicion.Usuario = basOptimizacion.gvarCodUsuario
'
'    Set m_oIBAseDatosEnEdicion = m_oArticuloEnEdicion
'
'    Screen.MousePointer = vbHourglass
'    teserror = m_oIBAseDatosEnEdicion.EliminarDeBaseDatos
'
'    If teserror.NumError <> TESnoerror Then
'        Screen.MousePointer = vbNormal
'        TratarError teserror
'        sdbgArticulos.Refresh
'        sdbgArticulos.SetFocus
'
'    Else
'
'        ''' Registro de acciones
'
'        basSeguridad.RegistrarAccion accionessummit.ACCArtEli, "Cod:" & m_oArticuloEnEdicion.Cod
'
'        ''' Eliminar de la coleccion
'
'        IndArt = val(Bookmark)
'
'        For IndFor = IndArt To g_oArticulos.Count - 2
'
'            g_oArticulos.Remove (CStr(IndFor))
'            Set m_oArticuloEnEdicion = g_oArticulos.Item(CStr(IndFor + 1))
'            m_oArticuloEnEdicion.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
'            m_oArticuloEnEdicion.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
'            m_oArticuloEnEdicion.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
'            m_oArticuloEnEdicion.GMN4Cod = ssMateriales.ActiveRow.Cells("COD").Value
'
'            ''' INTEGRACION
'            If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4) Then
'                g_oArticulos.Add ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.Cells("COD").Value, m_oArticuloEnEdicion.Cod, m_oArticuloEnEdicion.Den, m_oArticuloEnEdicion.Cantidad, m_oArticuloEnEdicion.CodigoUnidad, IndFor, , , , , , m_oArticuloEnEdicion.FECACT, m_oArticuloEnEdicion.EstadoIntegracion
'            Else
'                g_oArticulos.Add ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.GetParent.Cells("COD").Value, ssMateriales.ActiveRow.Cells("COD").Value, m_oArticuloEnEdicion.Cod, m_oArticuloEnEdicion.Den, m_oArticuloEnEdicion.Cantidad, m_oArticuloEnEdicion.CodigoUnidad, IndFor, , , , , , m_oArticuloEnEdicion.FECACT
'            End If
'
'            Set m_oArticuloEnEdicion = Nothing
'
'        Next IndFor
'
'        g_oArticulos.Remove (CStr(IndFor))
'
'        Accion = ACCArtCon
'
'    End If
'
'    Set m_oIBAseDatosEnEdicion = Nothing
'    Set m_oArticuloEnEdicion = Nothing
'    Screen.MousePointer = vbNormal
'
End Sub
Private Sub sdbgArticulos_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Long
    Dim i As Long
    Dim j As Long
    
    Dim oArticulo As CArticulo
    
    Dim iNumArticulos As Long

    If g_oArticulos Is Nothing Then
        RowBuf.RowCount = 0
        Exit Sub
    End If

    iNumArticulos = g_oArticulos.Count

    If IsNull(StartLocation) Then
    
        If ReadPriorRows Then
            m_lIndiceArticulos = iNumArticulos - 1
        Else
            m_lIndiceArticulos = 0
        End If
        
    Else
        
        m_lIndiceArticulos = StartLocation
    
        If ReadPriorRows Then
            m_lIndiceArticulos = m_lIndiceArticulos - 1
        Else
            m_lIndiceArticulos = m_lIndiceArticulos + 1
        End If
        
    End If
    
    
    For i = 0 To RowBuf.RowCount - 1
        
        If m_lIndiceArticulos < 0 Or m_lIndiceArticulos > iNumArticulos - 1 Then Exit For
    
        Set oArticulo = Nothing
        Set oArticulo = g_oArticulos.Item(CStr(m_lIndiceArticulos))
    
        For j = 0 To 11
      
            Select Case j
                    
                Case 0:
                        RowBuf.Value(i, 1) = oArticulo.Cod
                        
                Case 1:
                        RowBuf.Value(i, 2) = oArticulo.Den
                
                Case 2:
                        RowBuf.Value(i, 3) = oArticulo.CodigoUnidad
                
                Case 3:
                        RowBuf.Value(i, 4) = oArticulo.Cantidad
                        
                Case 4:
                        RowBuf.Value(i, 5) = BooleanToSQLBinary(oArticulo.Generico)
                        
                Case 5:       '''  INTEGRACION
                        If basParametros.gParametrosIntegracion.gaExportar(EntidadIntegracion.art4) Then
                            RowBuf.Value(i, 0) = NullToStr(oArticulo.EstadoIntegracion)
                        End If
                        
                Case 6:
                        RowBuf.Value(i, 6) = m_sTextConcepAlmacRecep(0, oArticulo.Concepto)
                        
                Case 7:
                        RowBuf.Value(i, 7) = m_sTextConcepAlmacRecep(1, oArticulo.Almacenable)
                Case 8:
                        RowBuf.Value(i, 8) = m_sTextConcepAlmacRecep(2, oArticulo.Recepcionable)
                Case 9:
                        RowBuf.Value(i, 9) = BooleanToSQLBinary(oArticulo.isRecepcionImporte)
                Case 10:
                        If oArticulo.NombreUON <> "Multiples*" Then
                            RowBuf.Value(i, 10) = oArticulo.NombreUON
                        Else
                            RowBuf.Value(i, 10) = m_sVariasUnidades
                        End If
                Case 11:
                        RowBuf.Value(i, 11) = oArticulo.CodArticuloCentral
                
                    
            End Select
            
        Next j
    
        RowBuf.Bookmark(i) = m_lIndiceArticulos
    
        If ReadPriorRows Then
            m_lIndiceArticulos = m_lIndiceArticulos - 1
        Else
            m_lIndiceArticulos = m_lIndiceArticulos + 1
        End If
            r = r + 1
        Next i
    
    RowBuf.RowCount = r
    Set oArticulo = Nothing
End Sub

''' <summary>
''' Evento que se produce al hacer update de la grid
''' </summary>
''' <returns></returns>
''' <param name="RowBuf">Buffer con los datos de la linea</param>
''' <param name="WriteLocation">Tipo de la escritura</param>
''' <remarks>Llamada desde Evento ; Tiempo m�ximo</remarks>

Private Sub sdbgArticulos_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)

    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    
    Dim v As Variant
    
    If m_bRespetar Then Exit Sub
    If m_oArticuloEnEdicion Is Nothing Then Exit Sub
    
    m_bModError = False
    
    ''' Modificamos en la base de datos
    m_oArticuloEnEdicion.Cod = sdbgArticulos.Columns("COD").Value
    m_oArticuloEnEdicion.Den = sdbgArticulos.Columns("DEN").Value
    m_oArticuloEnEdicion.Cantidad = StrToDblOrNull(sdbgArticulos.Columns("CANT").Value)
    m_oArticuloEnEdicion.CodigoUnidad = sdbgArticulos.Columns("UNI").Value
    m_oArticuloEnEdicion.Concepto = TraducirTextoCelda(0, sdbgArticulos.Columns("CONCEP").Value)  '''Error de tipos
    m_oArticuloEnEdicion.Almacenable = TraducirTextoCelda(1, sdbgArticulos.Columns("ALMAC").Value)
    m_oArticuloEnEdicion.Recepcionable = TraducirTextoCelda(2, sdbgArticulos.Columns("RECEP").Value)
    ''' pasamos el usuario conectado para el LOG e Integraci�n
    m_oArticuloEnEdicion.Usuario = basOptimizacion.gvarCodUsuario
    m_oArticuloEnEdicion.tipoRecepcion = Abs(sdbgArticulos.Columns("TIPORECEPCION").Value)
    
    Screen.MousePointer = vbHourglass
    Set m_oIBAseDatosEnEdicion = m_oArticuloEnEdicion
    teserror = m_oIBAseDatosEnEdicion.FinalizarEdicionModificando
    Screen.MousePointer = vbNormal
       
    If teserror.NumError <> TESnoerror Then
    
        v = sdbgArticulos.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgArticulos.SetFocus
        m_bModError = True
        RowBuf.RowCount = 0
        sdbgArticulos.ActiveCell.Value = v
        
    Else
    
        
        ''' Registro de acciones
        
        basSeguridad.RegistrarAccion accionessummit.ACCArtAnya, "Cod:" & m_oArticuloEnEdicion.Cod
        
        Accion = ACCArtCon
        
        Set m_oIBAseDatosEnEdicion = Nothing
        Set m_oArticuloEnEdicion = Nothing
        
    End If
        
End Sub


Public Sub ponerCaption(Texto, indice, comodin)
    
    caption = m_sIdiTitulos(indice) & Texto & IIf(comodin, "*", "")

End Sub
''' <summary>
''' Procedimiento que carga los textos que aparecer�n en el formulario en el idioma que corresponda
''' </summary>
''' <remarks>Llamada desde: form_load ; Tiempo m�ximo: 0</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRMAT, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        m_sIdiTitulos(1) = Ador(0).Value & " "
        Ador.MoveNext
        m_sIdiTitulos(2) = Ador(0).Value & " "
        Ador.MoveNext
        m_sIdiTitulos(3) = Ador(0).Value
        caption = m_sIdiTitulos(3)
        Ador.MoveNext
        m_sIdiTitulos(4) = Ador(0).Value
        Ador.MoveNext
        m_sIdiTitulos(5) = Ador(0).Value
        Ador.MoveNext
        m_sIdiTitulos(6) = Ador(0).Value
        Ador.MoveNext
        cmdA�adir.caption = Ador(0).Value
        Ador.MoveNext
        cmdA�adirArticulo.caption = Ador(0).Value
        Ador.MoveNext
        cmdBuscar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCodigoArticulo.caption = Ador(0).Value
        Ador.MoveNext
        cmdDeshacerArticulo.caption = Ador(0).Value
        cmdDeshacer.caption = Ador(0).Value
        Ador.MoveNext
        cmdEli.caption = Ador(0).Value
        Ador.MoveNext
        cmdEliminarArticulo.caption = Ador(0).Value
        Ador.MoveNext
        cmdFiltrarArticulo.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdListadoArticulo.caption = Ador(0).Value
        Ador.MoveNext
        'cmdModif.Caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiModos(1) = Ador(0).Value
        cmdModoEdicion.caption = m_sIdiModos(1)
        Ador.MoveNext
        m_sIdiModos(2) = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurarArticulo.caption = Ador(0).Value
        Ador.MoveNext
        SSTabEstrMat.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTabEstrMat.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("UNI").caption = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("CANT").caption = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("P").caption = Ador(0).Value
        Ador.MoveNext
        sdbddUnidades.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbddUnidades.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiCod = Ador(0).Value
        cmdCodigo.caption = "&" & Ador(0).Value
        Ador.MoveNext
        m_sIdiMat = Ador(0).Value
        Ador.MoveNext
        m_sIdiCompProv = Ador(0).Value
        Ador.MoveNext
        m_sIdiComp = Ador(0).Value
        Ador.MoveNext
        m_sIdiProv = Ador(0).Value
        Ador.MoveNext
        m_sIdiArt = Ador(0).Value
        Ador.MoveNext
        m_sIdiDen = Ador(0).Value
        Ador.MoveNext
        m_sIdiUniPDef = Ador(0).Value
        Ador.MoveNext
        m_sIdiCant = Ador(0).Value
        Ador.MoveNext
        m_sIdiAdd = Ador(0).Value
        Ador.MoveNext
        m_sIdiModif = Ador(0).Value
        Ador.MoveNext
        m_sIdiDetalle = Ador(0).Value
        Ador.MoveNext
        m_sIdiOrden = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("ESPEC").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiEspec = Ador(0).Value
        Ador.MoveNext
        m_sIdiArticulo = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("ATRIBUTOS").caption = Ador(0).Value
        Ador.MoveNext
        sIdiSincronizado = Ador(0).Value
        Ador.MoveNext
        sIdiNoSincronizado = Ador(0).Value
        Ador.MoveNext
        chkVerIdiomas.caption = Ador(0).Value
        Ador.MoveNext
        sNotificarAltamat = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("GENERICO").caption = Ador(0).Value
        Ador.MoveNext
        cmdReubicar.caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiCompProvPlan = Ador(0).Value
        Ador.MoveNext
        m_sIdiCompPlan = Ador(0).Value
        Ador.MoveNext
        m_sIdiProvPlan = Ador(0).Value
        Ador.MoveNext
        m_sIdiPlan = Ador(0).Value
        Ador.MoveNext
        m_sTextConcepAlmacRecep(0, 0) = Ador(0).Value 'Gasto
        Ador.MoveNext
        m_sTextConcepAlmacRecep(0, 1) = Ador(0).Value 'Inversi�n
        Ador.MoveNext
        m_sTextConcepAlmacRecep(0, 2) = Ador(0).Value 'Gas./Inv.
        Ador.MoveNext
        m_sTextConcepAlmacRecep(1, 1) = Ador(0).Value 'Obligatorio almacenar
        Ador.MoveNext
        m_sTextConcepAlmacRecep(1, 0) = Ador(0).Value 'No almacenable
        Ador.MoveNext
        m_sTextConcepAlmacRecep(1, 2) = Ador(0).Value 'Opcional almacenar
        m_sTextConcepAlmacRecep(2, 2) = Ador(0).Value 'Opcional recepcionar
        Ador.MoveNext
        m_sTextConcepAlmacRecep(2, 1) = Ador(0).Value 'Obligatoria recepci�n
        Ador.MoveNext
        m_sTextConcepAlmacRecep(2, 0) = Ador(0).Value 'No recepcionar
        Ador.MoveNext
        m_sIdiConcepto = Ador(0).Value
        sdbgArticulos.Columns("CONCEP").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiAlmacenable = Ador(0).Value
        sdbgArticulos.Columns("ALMAC").caption = Ador(0).Value
        Ador.MoveNext
        m_sIdiRecepcionable = Ador(0).Value
        sdbgArticulos.Columns("RECEP").caption = Ador(0).Value
        Ador.MoveNext
        m_sTextImpuestos = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("IMPUESTO").caption = Ador(0).Value
        Ador.MoveNext
        sTipoRecepcion = Ador(0).Value
        sdbgArticulos.Columns("TIPORECEPCION").caption = Ador(0).Value
        Ador.MoveNext
        chkVerIdiomas.caption = Ador(0).Value
        Ador.MoveNext
        sRecepcionCantidad = Ador(0).Value
        Ador.MoveNext
        sRecepcionImporte = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns("UON_DEN").caption = Ador(0).Value 'Unidades Org.
        Ador.MoveNext
        m_sVariasUnidades = Ador(0).Value '(M�ltiples unidades organizativas)
        Ador.MoveNext
        m_sCantidadModificable = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
    
    'pargen lit
    Dim oLiterales As CLiterales
    Set oLiterales = oGestorParametros.DevolverLiterales(48, 50, basPublic.gParametrosInstalacion.gIdioma)
    sdbgArticulos.Columns("AGREGADO").caption = oLiterales.Item(1).Den 'Art de planta o agregado
    sdbgArticulos.Columns("ART").caption = oLiterales.Item(3).Den 'central
    Set oLiterales = Nothing
End Sub

Private Sub CargarGridMateriales(ByVal bOrdPorCod As Boolean)
    Screen.MousePointer = vbHourglass
    
    Set m_oGruposMat4 = Nothing
    Set m_oGruposMat4 = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If g_bRComprador Then
        Set ssMateriales.DataSource = m_oGruposMat4.DevolverEstructuraDeMateriales(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, bOrdPorCod)
    Else
        Set ssMateriales.DataSource = m_oGruposMat4.DevolverEstructuraDeMateriales(, , bOrdPorCod)
    End If
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub tvwEstrMat_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        'Raiz
        PopupMenu MDI.mnuPopUpEstrMat
    End If
End Sub

Private Sub tvwEstrMat_NodeClick(ByVal node As MSComctlLib.node)
    On Error Resume Next
    
    Set ssMateriales.ActiveRow = Nothing
    
    ConfigurarInterfazSeguridad -1
End Sub

Private Sub InicializarGrid(ByVal bMultiIdi As Boolean)
    'Inicializa el layout de la grid
    Dim i As Integer
    Dim iPos As Integer
    Dim j As Integer
    
    With ssMateriales
        .Font.Name = "Tahoma"
        .Font.Size = 8
        .Override.ActiveRowAppearance.Backcolor = vbHighlight
        
        'Columna Tipo de recepci�n
        .Bands.Item(1).Override.AllowUpdate = ssAllowUpdateYes
        .Bands(0).Columns("TIPORECEPCION").Header.caption = sTipoRecepcion
        .Bands(0).Columns("TIPORECEPCION").Header.Appearance.TextAlign = ssAlignCenter
        .Bands(0).Columns("TIPORECEPCION").Style = UltraGrid.ssStyleCheckBox
        .Bands(1).Columns("TIPORECEPCION").Style = UltraGrid.ssStyleCheckBox
        .Bands(2).Columns("TIPORECEPCION").Style = UltraGrid.ssStyleCheckBox
        .Bands(3).Columns("TIPORECEPCION").Style = UltraGrid.ssStyleCheckBox
        .Bands(1).Columns("TIPORECEPCION").Activation = ssActivationAllowEdit
        
        'Columna cantidad modificable
        .Bands.Item(1).Override.AllowUpdate = ssAllowUpdateYes
        .Bands(0).Columns("CANTIDAD_MODIFICABLE").Header.caption = m_sCantidadModificable
        .Bands(0).Columns("CANTIDAD_MODIFICABLE").Header.Appearance.TextAlign = ssAlignCenter
        .Bands(0).Columns("CANTIDAD_MODIFICABLE").Style = UltraGrid.ssStyleCheckBox
        .Bands(1).Columns("CANTIDAD_MODIFICABLE").Style = UltraGrid.ssStyleCheckBox
        .Bands(2).Columns("CANTIDAD_MODIFICABLE").Style = UltraGrid.ssStyleCheckBox
        .Bands(3).Columns("CANTIDAD_MODIFICABLE").Style = UltraGrid.ssStyleCheckBox
        .Bands(1).Columns("CANTIDAD_MODIFICABLE").Activation = ssActivationAllowEdit
        
        'Next i
        If gParametrosGenerales.gbSincronizacionMat Then
            If bMultiIdi = False Then
                For i = 1 To UBound(m_arrIdiomas)
                    'Con un �nico idioma, el layout carga las bandas con grupo, mientras que en modo multiidioma no se asigna.
                    'Los idiomas posteriores a los incluidos en 'LayoutMaterialesUnIdioma.ugd' no tienen el grupo asociado, por lo que se asigna el grupo de uno ya existente
                    'Un GMN por cada banda
                     If .Bands(0).Columns("DEN_" & m_arrIdiomas(i)).Group Is Nothing Then Set .Bands(0).Columns("DEN_" & m_arrIdiomas(i)).Group = .Bands(0).Columns("DEN_SPA").Group
                     If .Bands(1).Columns("DEN_" & m_arrIdiomas(i)).Group Is Nothing Then Set .Bands(1).Columns("DEN_" & m_arrIdiomas(i)).Group = .Bands(1).Columns("DEN_SPA").Group
                     If .Bands(2).Columns("DEN_" & m_arrIdiomas(i)).Group Is Nothing Then Set .Bands(2).Columns("DEN_" & m_arrIdiomas(i)).Group = .Bands(2).Columns("DEN_SPA").Group
                     If .Bands(3).Columns("DEN_" & m_arrIdiomas(i)).Group Is Nothing Then Set .Bands(3).Columns("DEN_" & m_arrIdiomas(i)).Group = .Bands(3).Columns("DEN_SPA").Group
                
                     .Bands(0).Columns("DEN_" & m_arrIdiomas(i)).Header.caption = " "
                     
                     If m_arrIdiomas(i) <> basPublic.gParametrosInstalacion.gIdiomaPortal Then
                        .Bands(0).Columns("DEN_" & m_arrIdiomas(i)).Hidden = True
                        .Bands(1).Columns("DEN_" & m_arrIdiomas(i)).Hidden = True
                        .Bands(2).Columns("DEN_" & m_arrIdiomas(i)).Hidden = True
                        .Bands(3).Columns("DEN_" & m_arrIdiomas(i)).Hidden = True
                     Else
                        If .Width > 3000 Then
                            .Bands(0).Columns("DEN_" & m_arrIdiomas(i)).Width = .Width - 3000
                            .Bands(1).Columns("DEN_" & m_arrIdiomas(i)).Width = .Width - 3000
                            .Bands(2).Columns("DEN_" & m_arrIdiomas(i)).Width = .Width - 3000
                            .Bands(3).Columns("DEN_" & m_arrIdiomas(i)).Width = .Width - 3000
                        End If
                     End If
                Next i
                
                'Ajusta el tama�o de las columnas de c�digo para que quede lo m�s parecido posible a un treview:
                .Bands(0).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN1 * 250) / 2
                .Bands(1).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN2 * 250) / 2
                .Bands(2).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN3 * 250) / 2
                .Bands(3).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN4 * 250) / 2
            Else
                'si hay sincronizaci�n de materiales habr� varias columnas de idiomas:
                'Caption y posici�n de las diferentes columnas de idiomas:
                iPos = 2
                .Bands(0).Columns("COD").Width = 2000
                
                For j = 1 To UBound(m_arrIdiomas)
                    If Not .Bands(0).Columns("DEN_" & m_arrIdiomas(j)) Is Nothing Then
                        .Bands(0).Columns("DEN_" & m_arrIdiomas(j)).Header.caption = m_arrIdiomasDen(j)
                        Dim iNumeroCampos As Integer
                        iNumeroCampos = UBound(m_arrIdiomas) + 2 'el n�mero de campos a mostrar es el n�mero de idiomas m�s la columna tiporecepcion m�s la de cantidad modificable
                        .Bands(0).Columns("DEN_" & m_arrIdiomas(j)).Width = (.Width - 2300) / iNumeroCampos
                        .Bands(1).Columns("DEN_" & m_arrIdiomas(j)).Width = (.Width - 2300) / iNumeroCampos
                        .Bands(2).Columns("DEN_" & m_arrIdiomas(j)).Width = (.Width - 2300) / iNumeroCampos
                        .Bands(3).Columns("DEN_" & m_arrIdiomas(j)).Width = (.Width - 2300) / iNumeroCampos
                    End If
                    .Bands(0).Columns("TIPORECEPCION").Width = (.Width - 2300) / iNumeroCampos
                    .Bands(0).Columns("CANTIDAD_MODIFICABLE").Width = (.Width - 2300) / iNumeroCampos
                    
                    'El idioma de la aplicaci�n ser� la 1� columna, las dem�s en orden alfab�tico:
                    If m_arrIdiomas(j) = gParametrosInstalacion.gIdiomaPortal Then
                        For i = 0 To 3
                            If Not .Bands(i).Columns("DEN_" & m_arrIdiomas(j)) Is Nothing Then
                                .Bands(i).Columns("DEN_" & m_arrIdiomas(j)).Header.VisiblePosition = 1
                            End If
                        Next i
                    Else
                        For i = 0 To 3
                            If Not .Bands(i).Columns("DEN_" & m_arrIdiomas(j)) Is Nothing Then
                                .Bands(i).Columns("DEN_" & m_arrIdiomas(j)).Header.VisiblePosition = iPos
                            End If
                        Next i
                        iPos = iPos + 1
                    End If
                Next
            End If
        Else
            'Si no hay sincronizaci�n s�lo mostrar� una denominaci�n:
            .Bands(0).Columns("DEN_SPA").Header.caption = " "
            If .Width > 3000 Then
                .Bands(0).Columns("DEN_SPA").Width = .Width - 3000
                .Bands(1).Columns("DEN_SPA").Width = .Width - 3000
                .Bands(2).Columns("DEN_SPA").Width = .Width - 3000
                .Bands(3).Columns("DEN_SPA").Width = .Width - 3000
            End If
            
             'Ajusta el tama�o de las columnas de c�digo para que quede lo m�s parecido posible a un treview:
            .Bands(0).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN1 * 250) / 2
            .Bands(1).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN2 * 250) / 2
            .Bands(2).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN3 * 250) / 2
            .Bands(3).Columns("COD").Width = 350 + (gLongitudesDeCodigos.giLongCodGMN4 * 250) / 2
        End If
        
        'si no tiene permiso de modificaci�n la grid no se podr� modificar
        If Not m_bModifEstr Then
            For i = 0 To 3
                .Bands(i).Override.AllowUpdate = ssAllowUpdateNo
            Next i
        Else
            For i = 0 To 3
                .Bands(i).Columns("COD").Activation = ssActivationActivateNoEdit
            Next i
        End If
        
        If g_bRComprador Then
            For i = 0 To 3
                .Bands(i).Columns("ASIG").Hidden = True
            Next i
        End If
        
        'Que no se pueda seleccionar las columnas del �rbol de materiales
        .Override.SelectTypeCol = ssSelectTypeNone
        .Override.AllowGroupMoving = ssAllowGroupMovingNotAllowed
        .Override.AllowColMoving = ssAllowColMovingNotAllowed
    End With
End Sub
''' <summary>
''' Notificar Alta Material
''' </summary>
''' <param name="GMNCod">GMN a ultimo nivel</param>
''' <param name="GMNDen">Den GMN a ultimo nivel</param>
''' <param name="GMNCodIdiomas">GMN idioma a ultimo nivel</param>
''' <param name="GMNDenIdiomas">Den GMN idioma a ultimo nivel</param>
''' <param name="GMN1">GMN1 de material, de haber</param>
''' <param name="GMN2">GMN2 de material, de haber</param>
''' <param name="GMN3">GMN3 de material, de haber</param>
''' <returns>error de haberlo</returns>
''' <remarks>Llamada desde: ssMateriales_BeforeRowDeactivate ; Tiempo m�ximo: 0,2</remarks>
Private Function NotificarAltaMat(ByVal GMNCod As String, ByVal GMNDen As String, ByRef GMNCodIdiomas As Variant, ByRef GMNDenIdiomas As Variant, Optional ByVal GMN1 As String = "", Optional ByVal GMN2 As String = "", Optional ByVal GMN3 As String = "") As TipoErrorSummit
    Dim teserror As TipoErrorSummit
    Dim rsUsu As ADODB.Recordset
    Dim Ador As Ador.Recordset
    Dim oUsuarios As CUsuarios
            
    Dim sAsunto As String
    Dim sSubjectFrom As String
    Dim sCuerpo As String
    
    Dim arCodIdiomas() As Variant 'array que contendr� los distintos codigos de Idioma de los usuarios
    Dim arTos() As Variant 'emails de los usuarios de cada idioma
    Dim arHTMLs() As Variant 'Booleanos que indicaran si el email tiene que ser HTML o Texto
    Dim arSubject() As Variant
    Dim arIdiSubject() As Variant
    Dim arSubjectFrom() As Variant
    Dim i, j As Integer 'Indice para estos arrays
    Dim bEncontrado As Boolean
    Dim bSesionIniciada As Boolean
    
    'Obtenemos los emails de los notificados:
    Set oUsuarios = oFSGSRaiz.Generar_CUsuarios
    'Pero solo si su padre tiene puesto que se notifique:
    If GMN1 <> "" Then
        If GMN2 <> "" Then
            If GMN3 <> "" Then
                teserror = oUsuarios.getEmailsUsuariosGestMatQA(rsUsu, GMN3, GMN1, GMN2)
            Else
                teserror = oUsuarios.getEmailsUsuariosGestMatQA(rsUsu, GMN2, GMN1)
            End If
        Else
            teserror = oUsuarios.getEmailsUsuariosGestMatQA(rsUsu, GMN1)
        End If
    End If
    
    If teserror.NumError = TESnoerror And Not rsUsu Is Nothing Then
        If Not rsUsu.EOF Then
            ReDim arCodIdiomas(0)
            ReDim arHTMLs(0)
            ReDim arTos(0)
            
            ReDim arIdiSubject(0)
            ReDim arSubject(0)
            ReDim arSubjectFrom(0)
                        
            arCodIdiomas(0) = NullToStr(rsUsu("IDIOMA").Value)
            arHTMLs(0) = SQLBinaryToBoolean(rsUsu("TIPOEMAIL").Value)
            arTos(0) = NullToStr(rsUsu("EMAIL").Value)
            
            arIdiSubject(0) = basPublic.gParametrosInstalacion.gIdioma
            arSubject(0) = sNotificarAltamat
            arSubjectFrom(0) = gSubjectMultiIdiomaSMTP
            
            rsUsu.MoveNext
            While Not rsUsu.EOF
                bEncontrado = False
                For i = LBound(arCodIdiomas) To UBound(arCodIdiomas)
                    If arCodIdiomas(i) = NullToStr(rsUsu("IDIOMA").Value) And arHTMLs(i) = SQLBinaryToBoolean(rsUsu("TIPOEMAIL").Value) Then
                        bEncontrado = True
                        Exit For
                    End If
                Next
                If bEncontrado Then
                    arTos(i) = arTos(i) & ";" & NullToStr(rsUsu("EMAIL").Value)
                Else
                    ReDim Preserve arCodIdiomas(UBound(arCodIdiomas) + 1)
                    ReDim Preserve arHTMLs(UBound(arHTMLs) + 1)
                    ReDim Preserve arTos(UBound(arTos) + 1)
                    
                    arCodIdiomas(UBound(arCodIdiomas)) = NullToStr(rsUsu("IDIOMA").Value)
                    arHTMLs(UBound(arHTMLs)) = SQLBinaryToBoolean(rsUsu("TIPOEMAIL").Value)
                    arTos(UBound(arTos)) = NullToStr(rsUsu("EMAIL").Value)
                End If
                
                bEncontrado = False
                For i = LBound(arIdiSubject) To UBound(arIdiSubject)
                    If arIdiSubject(i) = NullToStr(rsUsu("IDIOMA").Value) Then
                        bEncontrado = True
                        Exit For
                    End If
                Next
                If Not bEncontrado Then
                    ReDim Preserve arIdiSubject(UBound(arIdiSubject) + 1)
                    ReDim Preserve arSubject(UBound(arSubject) + 1)
                    ReDim Preserve arSubjectFrom(UBound(arSubjectFrom) + 1)
                    
                    arIdiSubject(UBound(arIdiSubject)) = NullToStr(rsUsu("IDIOMA").Value)
                    
                    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESTRMAT, NullToStr(rsUsu("IDIOMA").Value), 51)
                    arSubject(UBound(arSubject)) = Ador(0).Value
                    Ador.Close
                    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(OTROS, NullToStr(rsUsu("IDIOMA").Value), 201)
                    arSubjectFrom(UBound(arSubjectFrom)) = Ador(0).Value
                    Ador.Close
                    Set Ador = Nothing
                End If
                
                rsUsu.MoveNext
            Wend
            
            If Not bSesionIniciada Or oIdsMail Is Nothing Then
                Set oIdsMail = IniciarSesionMail
                bSesionIniciada = True
            End If
            
            For i = LBound(arCodIdiomas) To UBound(arCodIdiomas)
                sCuerpo = GenerarMensajeNotificarAltaMat(GMNCod, GMNDen, GMNCodIdiomas, GMNDenIdiomas, GMN1, GMN2, GMN3, arCodIdiomas(i), arHTMLs(i))
                
                sAsunto = sNotificarAltamat
                sSubjectFrom = gSubjectMultiIdiomaSMTP
                For j = LBound(arIdiSubject) To UBound(arIdiSubject)
                    If arCodIdiomas(i) = arIdiSubject(j) Then
                        sAsunto = arSubject(j)
                        sSubjectFrom = arSubjectFrom(j)
                        Exit For
                    End If
                Next
                
               'Y lo mandamos
               If sCuerpo <> "" Then
                   teserror = ComponerMensaje(arTos(i), sAsunto, sCuerpo, , , arHTMLs(i), , , sSubjectFrom)
               Else
                   Call ComponerMensaje(arTos(i), sAsunto, sCuerpo, , , arHTMLs(i), , , sSubjectFrom)
                    
                   teserror.NumError = TESNotifUsuMatQA
                   teserror.Arg1 = arTos(i)
                   basErrores.TratarError teserror
                   teserror.NumError = TESnoerror
               End If
            Next
            
            If bSesionIniciada Then
                FinalizarSesionMail
            End If
        End If
    End If
    
    NotificarAltaMat = teserror
End Function

Private Function GenerarMensajeNotificarAltaMat(ByVal GMNCod As String, ByVal GMNDen As String, ByRef GMNCodIdiomas As Variant, ByRef GMNDenIdiomas As Variant, Optional ByVal GMN1 As String = "", Optional ByVal GMN2 As String = "", Optional ByVal GMN3 As String = "", Optional ByVal sIdi As String = "SPA", Optional ByVal bHTML As Boolean = False) As String
'**************************************************************************
'*** Descripci�n: Genera el cuerpo del e-mail en texto plano a partir   ***
'***              de la plantilla contenida en el archivo               ***
'***              SPA_FSQA_NotifAltaMat.txt o                           ***
'***              ENG_FSQA_NotifAltaMat.txt o                           ***
'***              GER_FSQA_NotifAltaMat.txt o                           ***
'***              dependi�ndo del idioma.                               ***
'***                                                                    ***
'*** Valor que devuelve: Un string con el cuerpo del mensaje del e-mail ***
'**************************************************************************
    Dim oFos As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim ostream As Scripting.TextStream
    Dim sCuerpoMensaje As String
    Dim sMaterial As String
    Dim i As Integer
    Dim sFileName As String
    
    Set oFos = New FileSystemObject
    
    'Coge la plantilla de par�metros de la instalaci�n
    If bHTML Then
        sFileName = basPublic.gParametrosInstalacion.gsPathSolicitudes & "\" & sIdi & "_FSQA_NotifAltaMat.html"
    Else
        sFileName = basPublic.gParametrosInstalacion.gsPathSolicitudes & "\" & sIdi & "_FSQA_NotifAltaMat.txt"
    End If
    
    If Not oFos.FileExists(sFileName) Then
        Screen.MousePointer = vbNormal
        oMensajes.PlantillaNoEncontrada sFileName
        Set oFos = Nothing
        GenerarMensajeNotificarAltaMat = ""
        Exit Function
    End If
    
    Set oFile = oFos.GetFile(sFileName)
    Set ostream = oFile.OpenAsTextStream(ForReading, TristateUseDefault)
    
    sCuerpoMensaje = ostream.ReadAll
    
    If GMN1 <> "" Then
        sMaterial = sMaterial & GMN1 & " - "
        If GMN2 <> "" Then
            sMaterial = sMaterial & GMN2 & " - "
            If GMN3 <> "" Then
                sMaterial = sMaterial & GMN3 & " - "
            End If
        End If
    End If
    sMaterial = sMaterial & GMNCod & " - "
    If gParametrosGenerales.gbSincronizacionMat = True Then
        For i = 1 To UBound(GMNCodIdiomas)
            If GMNCodIdiomas(i) = sIdi Then
                sMaterial = sMaterial & GMNDenIdiomas(i)
            End If
        Next i
    Else
        sMaterial = sMaterial & GMNDen
    End If
    
    sCuerpoMensaje = Replace(sCuerpoMensaje, "@MATERIAL", sMaterial)
    
    Set oFile = Nothing
    Set oFos = Nothing
    Set ostream = Nothing

    GenerarMensajeNotificarAltaMat = sCuerpoMensaje
End Function


Public Sub Reubicar()


    Dim teserror As TipoErrorSummit
    Dim oArticulo As CArticulo
    Set oArticulo = g_oArticulos.Item(CStr(sdbgArticulos.SelBookmarks.Item(0)))
    ''' Pasamos el usuario que realiza la modificaci�n (LOG e Integraci�n
    oArticulo.Usuario = basOptimizacion.gvarCodUsuario

    Dim sCod As String
    Dim sCodAnterior As String
        
    Dim l As Long
    
    Dim vArtCod As Variant

    If IsEmpty(vArtCod) Then
        ReDim vArtCod(sdbgArticulos.SelBookmarks.Count)
        For l = 0 To sdbgArticulos.SelBookmarks.Count - 1
            vArtCod(l + 1) = sdbgArticulos.Columns("COD").CellValue(sdbgArticulos.SelBookmarks.Item(l))
        Next
    End If

    
    Screen.MousePointer = vbHourglass
    
    teserror = oArticulo.Reubicar(vArtCod, frmSELMAT.oGMN1Seleccionado.Cod, frmSELMAT.oGMN2Seleccionado.Cod, frmSELMAT.oGMN3Seleccionado.Cod, frmSELMAT.oGMN4Seleccionado.Cod)
    
    Screen.MousePointer = vbNormal
    
    If teserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        basErrores.TratarError teserror
        Accion = ACCArtCon
        vArtCod = Empty
        
    Else
        CargarGridArticulos
        sdbgArticulos.ReBind
    
        sdbgArticulos.MoveFirst
        sdbgArticulos.Refresh
        sdbgArticulos.Bookmark = sdbgArticulos.SelBookmarks(0)
        sdbgArticulos.SelBookmarks.RemoveAll
        sdbgArticulos.Refresh

        sCod = frmSELMAT.oGMN1Seleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(frmSELMAT.oGMN1Seleccionado.Cod))
        sCod = sCod & frmSELMAT.oGMN2Seleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(frmSELMAT.oGMN2Seleccionado.Cod))
        sCod = sCod & frmSELMAT.oGMN3Seleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(frmSELMAT.oGMN3Seleccionado.Cod))
        sCod = sCod & frmSELMAT.oGMN4Seleccionado.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(frmSELMAT.oGMN4Seleccionado.Cod))
        
        sCodAnterior = oArticulo.GMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(oArticulo.GMN1Cod))
        sCodAnterior = sCodAnterior & oArticulo.GMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(oArticulo.GMN2Cod))
        sCodAnterior = sCodAnterior & oArticulo.GMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(oArticulo.GMN3Cod))
        sCodAnterior = sCodAnterior & oArticulo.GMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(oArticulo.GMN4Cod))
        
        For l = 1 To UBound(vArtCod)
            basSeguridad.RegistrarAccion accionessummit.ACCArtReubicar, "Articulo reubicado:" & CStr(vArtCod(l)) & " Desde: " & sCodAnterior & " Hasta: " & sCod
        Next
        
        vArtCod = Empty
    End If
    
    Set oArticulo = Nothing
    Unload frmSELMAT

End Sub

'Private Sub ssdbddConcepto_CloseUp()
    'sdbgArticulos.Columns("CONCEP").Value = ssdbddConcepto.Columns(0).Value
'End Sub


Private Sub ssdbddConcepto_DropDown()
    
    If sdbgArticulos.Columns("CONCEP").Locked Then
        ssdbddConcepto.DroppedDown = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    sdbgArticulos.ActiveCell.SelStart = 0
    sdbgArticulos.ActiveCell.SelLength = Len(sdbgArticulos.ActiveCell.Text)

    Screen.MousePointer = vbNormal

End Sub

Private Sub ssdbddConcepto_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    ssdbddConcepto.DataFieldList = "Column 0"
    ssdbddConcepto.DataFieldToDisplay = "Column 0"
        
End Sub

Private Sub ssdbddConcepto_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    ssdbddConcepto.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ssdbddConcepto.Rows - 1
            bm = ssdbddConcepto.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ssdbddConcepto.Columns(0).CellText(bm), 1, Len(Text))) Then
                ssdbddConcepto.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub ssdbddConcepto_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Integer
Dim bPasa As Boolean

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
            
        For i = 0 To 2
            If UCase(sdbgArticulos.Columns("CONCEP").Text) = UCase(m_sTextConcepAlmacRecep(0, i)) Then
                bPasa = True
                Exit For
            End If
        Next
        If bPasa = False Then
            RtnPassed = False
            sdbgArticulos.Columns("CONCEP").Text = m_sTextConcepAlmacRecep(0, g_oArticulos.Item(CStr(sdbgArticulos.Bookmark)).Concepto)
        End If
    End If
    
End Sub

Private Sub ssdbddAlmacen_CloseUp()

    Dim teserror As TipoErrorSummit
    Dim oArt As CArticulo
    Dim bk As Variant
    
    If m_aBookmarks Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass

    If m_aBookmarks.Count > 0 Then
        m_bRespetar = True
        teserror = g_oArticulos.ActualizarConcepAlmacRecep(Almacenar, TraducirTextoCelda(1, ssdbddAlmacen.Columns(0).Value), m_aBookmarks)
        If teserror.Arg2 Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesArticulos teserror.Arg1
            Screen.MousePointer = vbHourglass

            Set m_oArticulosAModificar = Nothing

            Screen.MousePointer = vbNormal
            'Vuelvo a seleccionar las filas modificadas
            sdbgArticulos.ReBind
            If m_aBookmarks.Count > 0 Then
                For Each bk In m_aBookmarks
                    sdbgArticulos.SelBookmarks.Add bk
                Next
            End If
        Else
            For Each oArt In m_oArticulosAModificar
                RegistrarAccion accionessummit.ACCArtMod, "Art�culo: " & oArt.Cod & " Almacenable: " & oArt.Almacenable
            Next
        End If
        
        sdbgArticulos.Update
        sdbgArticulos.ReBind
        If m_aBookmarks.Count > 0 Then
            For Each bk In m_aBookmarks
                sdbgArticulos.SelBookmarks.Add bk
            Next
        End If
        Set m_aBookmarks = Nothing
        m_bRespetar = False
        
    End If
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub ssdbddAlmacen_DropDown()

    If sdbgArticulos.Columns("ALMAC").Locked Then
        ssdbddAlmacen.DroppedDown = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

    sdbgArticulos.ActiveCell.SelStart = 0
    sdbgArticulos.ActiveCell.SelLength = Len(sdbgArticulos.ActiveCell.Text)

    Screen.MousePointer = vbNormal

End Sub

Private Sub ssdbddAlmacen_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    ssdbddAlmacen.DataFieldList = "Column 0"
    ssdbddAlmacen.DataFieldToDisplay = "Column 0"
        
End Sub

Private Sub ssdbddAlmacen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    ssdbddAlmacen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ssdbddAlmacen.Rows - 1
            bm = ssdbddAlmacen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ssdbddAlmacen.Columns(0).CellText(bm), 1, Len(Text))) Then
                ssdbddAlmacen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub ssdbddAlmacen_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Integer
Dim bPasa As Boolean

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        
        For i = 0 To 2
            If UCase(sdbgArticulos.Columns("ALMAC").Text) = UCase(m_sTextConcepAlmacRecep(1, i)) Then
                bPasa = True
                Exit For
            End If
        Next
        If bPasa = False Then
            RtnPassed = False
            sdbgArticulos.Columns("ALMAC").Text = m_sTextConcepAlmacRecep(1, g_oArticulos.Item(CStr(sdbgArticulos.Bookmark)).Almacenable)
        End If
    End If
    
End Sub

Private Sub ssdbddRecepcion_CloseUp()

    Dim teserror As TipoErrorSummit
    Dim oArt As CArticulo
    Dim bk As Variant
    
    If m_aBookmarks Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If m_aBookmarks.Count > 0 Then
        m_bRespetar = True
        teserror = g_oArticulos.ActualizarConcepAlmacRecep(Recepcionar, TraducirTextoCelda(2, ssdbddRecepcion.Columns("DEN").Value), m_aBookmarks)
        If teserror.Arg2 Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesArticulos teserror.Arg1
            Screen.MousePointer = vbHourglass

            Set m_oArticulosAModificar = Nothing

            Screen.MousePointer = vbNormal
            'Vuelvo a seleccionar las filas modificadas
            sdbgArticulos.ReBind
            If m_aBookmarks.Count > 0 Then
                For Each bk In m_aBookmarks
                    sdbgArticulos.SelBookmarks.Add bk
                Next
            End If
        Else
            For Each oArt In m_oArticulosAModificar
                RegistrarAccion accionessummit.ACCArtMod, "Art�culo: " & oArt.Cod & " Recepcionable: " & oArt.Recepcionable
            Next
        End If
        
        sdbgArticulos.Update
        sdbgArticulos.ReBind
        If m_aBookmarks.Count > 0 Then
            For Each bk In m_aBookmarks
                sdbgArticulos.SelBookmarks.Add bk
            Next
        End If
        Set m_aBookmarks = Nothing
        m_bRespetar = False
        
        
    End If
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub ssdbddRecepcion_DropDown()

    If sdbgArticulos.Columns("RECEP").Locked Then
        ssdbddRecepcion.DroppedDown = False
        Exit Sub
    End If

    Screen.MousePointer = vbHourglass

   

    sdbgArticulos.ActiveCell.SelStart = 0
    sdbgArticulos.ActiveCell.SelLength = Len(sdbgArticulos.ActiveCell.Text)

    Screen.MousePointer = vbNormal

End Sub

Private Sub ssdbddRecepcion_InitColumnProps()
    
    ''' * Objetivo: Definir que columna es la de busqueda y seleccion,
    ''' * Objetivo: y cual llevaremos a la grid
    
    ssdbddRecepcion.DataFieldList = "Column 0"
    ssdbddRecepcion.DataFieldToDisplay = "Column 0"
        
End Sub

Private Sub ssdbddRecepcion_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    ssdbddRecepcion.MoveFirst
    
    If Text <> "" Then
        For i = 0 To ssdbddRecepcion.Rows - 1
            bm = ssdbddRecepcion.GetBookmark(i)
            If UCase(Text) = UCase(Mid(ssdbddRecepcion.Columns(0).CellText(bm), 1, Len(Text))) Then
                ssdbddRecepcion.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub ssdbddRecepcion_ValidateList(Text As String, RtnPassed As Integer)
Dim i As Integer
Dim bPasa As Boolean

    ''' * Objetivo: Validar la seleccion (nulo o existente)
    
    ''' Si es nulo, correcto
    
    If Text = "" Then
        RtnPassed = True

    Else
    
        
        For i = 0 To 2
            If UCase(sdbgArticulos.Columns("RECEP").Text) = UCase(m_sTextConcepAlmacRecep(2, i)) Then
                bPasa = True
                Exit For
            End If
        Next
        If bPasa = False Then
            RtnPassed = False
            sdbgArticulos.Columns("RECEP").Text = m_sTextConcepAlmacRecep(2, g_oArticulos.Item(CStr(sdbgArticulos.Bookmark)).Recepcionable)
        End If
    End If
    
End Sub

Private Sub CargarGridConConcepto()

    Dim i As Integer
    
    ssdbddConcepto.RemoveAll
    
    For i = 0 To 2
        ssdbddConcepto.AddItem m_sTextConcepAlmacRecep(0, i)
    Next
    
End Sub

Private Sub CargarGridConAlmacen()

    Dim i As Integer
    
    ssdbddAlmacen.RemoveAll
    
    For i = 0 To 2
        ssdbddAlmacen.AddItem m_sTextConcepAlmacRecep(1, i)
    Next
    
End Sub

Private Sub CargarGridConRecepcion()

    Dim i As Integer
    
    ssdbddRecepcion.RemoveAll
    
    For i = 0 To 2
        ssdbddRecepcion.AddItem m_sTextConcepAlmacRecep(2, i)
    Next
        
End Sub

Private Function TraducirTextoCelda(ByVal Tipo As Integer, sTexto As String) As Integer
Dim i As Integer

For i = 0 To 2
    If UCase(m_sTextConcepAlmacRecep(Tipo, i)) = UCase(sTexto) Then
        TraducirTextoCelda = i
        Exit Function
    End If
Next
End Function

''' <summary>
''' Carga la pantalla para asociar impuestos al material
''' </summary>
''' <remarks>Llamada desde: MDI/mnuPopUpGMN4EstrMat MDI/mnuPopUpEstrMatImpuestoGMN1  MDI/mnuPopUpEstrMatImpuestoGMN2   MDI/mnuPopUpEstrMatImpuestoGMN3 ; Tiempo m�ximo: 0</remarks>
Public Sub Impuesto()
    If ssMateriales.ActiveRow.Cells("COD").GetText = "" Then Exit Sub
    Dim sIdiUSU As String
        
    If gParametrosGenerales.gbSincronizacionMat = False Then  'si no hay sincronizaci�n de materiales/actividades no se ver�n los idiomas.
        sIdiUSU = basPublic.gParametrosInstalacion.gIdioma
    Else
        sIdiUSU = oUsuarioSummit.idioma
    End If
    
    Select Case ssMateriales.ActiveRow.Band.Index
    Case 0  'GMN1
        frmImpuestos.GMN1Cod = ssMateriales.ActiveRow.Cells("COD").Value
        
        frmImpuestos.PermiteModif = m_bModifImpuesto
        
        frmImpuestos.caption = m_sTextImpuestos & " " & ssMateriales.ActiveRow.Cells("COD").Value & " - " & ssMateriales.ActiveRow.Cells("DEN_" & sIdiUSU).Value
        frmImpuestos.g_sOrigen = "frmESTRMAT"
        frmImpuestos.Show vbModal
    Case 1  'GMN2
        frmImpuestos.GMN2Cod = ssMateriales.ActiveRow.Cells("COD").Value
        frmImpuestos.GMN1Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
        
        frmImpuestos.PermiteModif = m_bModifImpuesto
        
        frmImpuestos.caption = m_sTextImpuestos & " " & ssMateriales.ActiveRow.GetParent.Cells("COD").Value & " - "
        frmImpuestos.caption = frmImpuestos.caption & ssMateriales.ActiveRow.Cells("COD").Value & " - " & ssMateriales.ActiveRow.Cells("DEN_" & sIdiUSU).Value
        frmImpuestos.g_sOrigen = "frmESTRMAT"
        frmImpuestos.Show vbModal
    Case 2  'GMN3
        frmImpuestos.GMN3Cod = ssMateriales.ActiveRow.Cells("COD").Value
        frmImpuestos.GMN2Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
        frmImpuestos.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
        
        frmImpuestos.PermiteModif = m_bModifImpuesto
        
        frmImpuestos.caption = m_sTextImpuestos & " " & ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value & " - "
        frmImpuestos.caption = frmImpuestos.caption & ssMateriales.ActiveRow.GetParent.Cells("COD").Value & " - "
        frmImpuestos.caption = frmImpuestos.caption & ssMateriales.ActiveRow.Cells("COD").Value & " - " & ssMateriales.ActiveRow.Cells("DEN_" & sIdiUSU).Value
        frmImpuestos.g_sOrigen = "frmESTRMAT"
        frmImpuestos.Show vbModal
    Case 3  'GMN4
        frmImpuestos.GMN4Cod = ssMateriales.ActiveRow.Cells("COD").Value
        frmImpuestos.GMN3Cod = ssMateriales.ActiveRow.GetParent.Cells("COD").Value
        frmImpuestos.GMN2Cod = ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value
        frmImpuestos.GMN1Cod = ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value
        
        frmImpuestos.PermiteModif = m_bModifImpuesto
        
        frmImpuestos.caption = m_sTextImpuestos & " " & ssMateriales.ActiveRow.GetParent.GetParent.GetParent.Cells("COD").Value & " - "
        frmImpuestos.caption = frmImpuestos.caption & ssMateriales.ActiveRow.GetParent.GetParent.Cells("COD").Value & " - "
        frmImpuestos.caption = frmImpuestos.caption & ssMateriales.ActiveRow.GetParent.Cells("COD").Value & " - "
        frmImpuestos.caption = frmImpuestos.caption & ssMateriales.ActiveRow.Cells("COD").Value & " - " & ssMateriales.ActiveRow.Cells("DEN_" & sIdiUSU).Value
        frmImpuestos.g_sOrigen = "frmESTRMAT"
        frmImpuestos.Show vbModal
    End Select
End Sub

''' <summary>
''' Configura las lineas de la grid para q se vea el "tick"
''' </summary>
''' <param name="bm">Fila en q cliqueo</param>
''' <remarks>Llamada desde: sdbgArticulos_BtnClick; Tiempo m�ximo: 0</remarks>
Private Sub RefrescarLineasImpuestos(ByVal bm As Variant)
    Dim oArticulo As CArticulo
    Dim i As Integer
    
    Screen.MousePointer = vbHourglass
    LockWindowUpdate Me.hWnd

    Me.sdbgArticulos.MoveFirst
    For i = 0 To sdbgArticulos.Rows - 1

        Set oArticulo = g_oArticulos.Item(CStr(i))
        
        If oArticulo.ConImpuestos Then
            sdbgArticulos.Columns("IMPUESTO").CellStyleSet "HayImpuestos"
        Else
            sdbgArticulos.Columns("IMPUESTO").CellStyleSet ""
        End If
        
        Me.sdbgArticulos.MoveNext
    Next
    
    sdbgArticulos.Bookmark = bm
    
    Me.sdbgArticulos.col = Me.sdbgArticulos.Columns("IMPUESTO").Position
    Me.sdbgArticulos.Refresh

    LockWindowUpdate 0&
    Screen.MousePointer = vbNormal
End Sub


Private Sub cambiarOrdenacionAscDesc()
    If m_sOrderAscDesc = "" Then
        m_sOrderAscDesc = "DESC"
    Else
        m_sOrderAscDesc = ""
    End If
End Sub


Public Sub cargarFormUons(ByVal isAlta As Boolean)
    Dim oUons As CUnidadesOrganizativas
    Set oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
    '''Cargar unidades organizativas en funci�n de las restricciones
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    Dim oArticulo As CArticulo
    Dim oUonsNuevas As CUnidadesOrganizativas
    Dim oUonsABorrar As CUnidadesOrganizativas
    
    Dim frm As frmSELUO
    Set frm = New frmSELUO
    
    frm.multiselect = True
    frm.EnableCheck = m_bModifArti
    frm.GuardarUon0 = False
    
    
    oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id
    oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id
    oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrMantUsu, m_bRestrMantPerf, oUsuarioSummit.Perfil.Id
    
    frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
    'subtitulo del formulario
    If isAlta Then
        Set oArticulo = Me.ArticuloEnEdicion
    Else
        Set oArticulo = Me.ArticuloSeleccionado
    End If
    frm.subtitulo = oArticulo.CodDen
    'bloqueo de uons integradas sentido
    Dim oUnidadesIntegradas As CUnidadesOrganizativas
    Set oUnidadesIntegradas = oFSGSRaiz.Generar_CUnidadesOrganizativas
    oUnidadesIntegradas.cargarUonsIntegradasArticulo
    
    'If oArticulo.Generico And gParametrosGenerales.gbArtGenericosUonErp Then
    If oArticulo.Generico And Not oArticulo.AltaIntegracion Then
        frm.marcarErpUons oUnidadesIntegradas, False
    Else
        frm.marcarErpUons oUnidadesIntegradas, True
    End If
    
    Set frm.UonsSeleccionadas = oArticulo.uons
    If oArticulo.isCentral Then
        'si es central evitamos poder hacer modificaciones, todas se hacen desde los agregados
        frm.EnableCheck = False
    End If
    
    'DoEvents
    
    frm.Show vbModal
    'Una vez cerrado el form obtenemos las uons seleccionadas y las actualizamos

    If frm.Aceptar Then
        'Antes de reasignar uons debemos comprobar que ninguna de las eliminadas tiene atributos con valor
        'o en caso contrario deberemos advertir que �stos se borrar�n
        
        Set oUonsNuevas = frm.UonsSeleccionadas
        Set oUonsABorrar = oFSGSRaiz.Generar_CUnidadesOrganizativas
        Dim oUON As IUon
        Dim borrar As Boolean
        For Each oUON In oArticulo.uons
            If oUON.tieneAtributoValoradoARTICULO(oArticulo.Cod) Then
                If Not oUonsNuevas.ContieneIgual(oUON) Then
                    oUonsABorrar.Add oUON
                    borrar = True
                End If
            End If
        Next
        'si hay algun atributo a borrar...
        If borrar = True Then
            If oMensajes.DesvincularUonsArticulo(oUonsABorrar.toString, oArticulo.CodDen) = vbYes Then
                oArticulo.eliminarAtributoArticuloEnUons oUonsABorrar
            Else
                frm.Aceptar = False
            End If
        End If
        
        If frm.Aceptar Then
            Dim oUonsAntes As CUnidadesOrganizativas
            Set oUonsAntes = oArticulo.uons.clonar

            If frm.UonsSeleccionadas.Count > 1 Then
                sdbgArticulos.Columns("UON_DEN").Text = m_sVariasUnidades
                oArticulo.NombreUON = m_sVariasUnidades
            Else
                sdbgArticulos.Columns("UON_DEN").Text = frm.UonsSeleccionadas.titulo
                oArticulo.NombreUON = frm.UonsSeleccionadas.titulo
            End If
                        sdbgArticulos.Update
            Set oArticulo.uons = frm.UonsSeleccionadas
            oArticulo.actualizaruons
            'Integracion
            oArticulo.Usuario = oUsuarioSummit.Cod
            oArticulo.IntegracionDeUonsArticulo oUonsAntes
            m_bHaCambiadoUon = True
            
            'Llamada a FSIS
            oArticulo.LlamarFSIS
        End If
        Set oUonsNuevas = Nothing
        Set oUonsABorrar = Nothing
        
        
    End If
    

    Set frm = Nothing
    Set oUnidadesIntegradas = Nothing
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
End Sub


Public Function validarErpsEntrada(oArticulo As CArticulo) As Boolean
    
    '3016 Articulos centrales
    'Si desmarcamos un art�culo como gen�rico, entonces habr� que validar que
    'las unidades organizativas no tengan ERP con integraci�n de art�culos en sentido de solo ENTRADA.
    'Mostrar mensaje de advertencia
    validarErpsEntrada = True
    Dim oUON As IUon
    Dim oUonArt As IUon
    Dim sUonsErp As String
    Dim oUonsErp As CUnidadesOrganizativas
    Set oUonsErp = oFSGSRaiz.Generar_CUnidadesOrganizativas
    If oArticulo.TieneUonsErpEntrada Then
        For Each oUON In oArticulo.UonsIntegradas
            If oUON.SentidoIntegracion = Entrada Then
                For Each oUonArt In oArticulo.uons
                    If oUonArt.incluye(oUON) Or oUonArt.perteneceA(oUON) Then
                            If Not oUonsErp.existe(oUonArt.key) Then
                                oUonsErp.Add oUonArt
                                sUonsErp = sUonsErp & oUonArt.titulo & vbCrLf
                            End If
                    End If
                Next
            End If
        Next
        '''mostrar mensaje de advertencia
        If sUonsErp <> "" Then
            If oMensajes.DesvincularUons(sUonsErp) = vbYes Then
                '''mostra advertencia de borrado de atributos uon
                Dim borrar As Boolean
                Dim oUonsABorrarAtrib As CUnidadesOrganizativas
                Set oUonsABorrarAtrib = oFSGSRaiz.Generar_CUnidadesOrganizativas
                For Each oUON In oUonsErp
                    If oUON.tieneAtributoValoradoARTICULO(oArticulo.Cod) Then
                        oUonsABorrarAtrib.Add oUON
                        borrar = True
                    End If
                Next
                'si hay algun atributo a borrar...
                If borrar = True Then
                    If oMensajes.DesvincularUonsArticulo(oUonsABorrarAtrib.toString, oArticulo.CodDen) = vbYes Then
                        oArticulo.eliminarAtributoArticuloEnUons oUonsABorrarAtrib
                    Else
                        sdbgArticulos.Columns("GENERICO").Value = 1
                        validarErpsEntrada = False
                        Exit Function
                    End If
                End If
                Dim oUonsAntes As CUnidadesOrganizativas
                Set oUonsAntes = oArticulo.uons.clonar
                For Each oUON In oUonsErp
                    oArticulo.deleteUon oUON
                Next
                'actualizamos las uons
                oArticulo.Usuario = oUsuarioSummit.Cod
                oArticulo.actualizaruons
                oArticulo.IntegracionDeUonsArticulo oUonsAntes
                sdbgArticulos.Columns("UON_DEN").Text = IIf(oArticulo.uons.Count > 1, m_sVariasUnidades, oArticulo.uons.titulo)
                oArticulo.NombreUON = sdbgArticulos.Columns("UON_DEN").Text
                'hayCambioEnUons = True
                Set oUonsAntes = Nothing
            Else
                sdbgArticulos.Columns("GENERICO").Value = 1
                validarErpsEntrada = False
            End If
        Else
            validarErpsEntrada = True
        End If
    End If
    Set oUonsErp = Nothing
End Function

Public Function ArticulosSeleccionados() As CArticulos
    Dim vb As Variant
    Set ArticulosSeleccionados = oFSGSRaiz.Generar_CArticulos
    For Each vb In sdbgArticulos.Bookmark
        ArticulosSeleccionados.addArticulo (g_oGMN4Seleccionado.ARTICULOS.Item(CStr(vb)))
    Next
End Function


'---------------------------------------------------------------------------------------
' Procedure : UonsIntegradas
' Author    : jim
' Date      : 14/04/2015
' Purpose   : Obtiene la colecci�n de uons con erp
'---------------------------------------------------------------------------------------
'
Public Function UonsIntegradas() As CUnidadesOrganizativas
    If m_oUonsIntegradas Is Nothing Then
        Set m_oUonsIntegradas = oFSGSRaiz.Generar_CUnidadesOrganizativas
        m_oUonsIntegradas.cargarUonsIntegradasArticulo
    End If
    Set UonsIntegradas = m_oUonsIntegradas
End Function

'---------------------------------------------------------------------------------------
' Procedure : tieneArticuloUonsEntrada
' Author    : jim
' Date      : 14/04/2015
' Purpose   : Comprueba si el art�culo pasado como par�metro pertenece a alguna Un.Org.
' con integraci�n de entrada y en tal caso devuelve true.
'---------------------------------------------------------------------------------------
'
Public Function tieneArticuloUonsEntrada(ByRef oArticulo As CArticulo) As Boolean
    Dim oUON As IUon
    Dim oUonInt As IUon
    For Each oUonInt In UonsIntegradas
        If oUonInt.SentidoIntegracion = Entrada Then
            For Each oUON In oArticulo.uons
                If oUON.incluye(oUonInt) Or oUonInt.incluye(oUON) Then
                    tieneArticuloUonsEntrada = True
                    Exit For
                End If
            Next
        End If
    Next
End Function

