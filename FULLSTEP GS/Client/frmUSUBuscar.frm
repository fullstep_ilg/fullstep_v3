VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmUSUBuscar 
   BackColor       =   &H00808000&
   Caption         =   "Buscar usuario"
   ClientHeight    =   5730
   ClientLeft      =   -135
   ClientTop       =   2025
   ClientWidth     =   13035
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUSUBuscar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5730
   ScaleWidth      =   13035
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture2 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2445
      Left            =   0
      ScaleHeight     =   2445
      ScaleWidth      =   13005
      TabIndex        =   12
      Top             =   30
      Width           =   13005
      Begin VB.CommandButton cmdCargarPer 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10320
         Picture         =   "frmUSUBuscar.frx":014A
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Cargar"
         Top             =   240
         Width           =   315
      End
      Begin VB.TextBox txtCodUsuPer 
         Height          =   285
         Left            =   1650
         MaxLength       =   50
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   210
         Width           =   2235
      End
      Begin VB.TextBox txtApePer 
         Height          =   285
         Left            =   4800
         MaxLength       =   150
         TabIndex        =   1
         Top             =   225
         Width           =   3495
      End
      Begin VB.TextBox txtNombrePer 
         Height          =   285
         Left            =   4800
         MaxLength       =   50
         TabIndex        =   3
         Top             =   690
         Width           =   3495
      End
      Begin VB.TextBox txtCodPer 
         Height          =   285
         Left            =   1650
         MaxLength       =   50
         TabIndex        =   2
         Top             =   690
         Width           =   2235
      End
      Begin VB.Frame fraBloq 
         BackColor       =   &H00808000&
         Caption         =   "DBloqueos"
         ForeColor       =   &H8000000E&
         Height          =   1125
         Left            =   8460
         TabIndex        =   14
         Top             =   120
         Width           =   1700
         Begin VB.OptionButton Option1 
            BackColor       =   &H00808000&
            Caption         =   "Bloqueados"
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Width           =   1500
         End
         Begin VB.OptionButton Option2 
            BackColor       =   &H00808000&
            Caption         =   "Sin bloquear"
            ForeColor       =   &H80000014&
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   495
            Width           =   1500
         End
         Begin VB.OptionButton Option3 
            BackColor       =   &H00808000&
            Caption         =   "Todos"
            ForeColor       =   &H80000014&
            Height          =   255
            Left            =   120
            TabIndex        =   8
            Top             =   735
            Width           =   1500
         End
      End
      Begin VB.Frame fraAccesos 
         BackColor       =   &H00808000&
         Caption         =   "DAcceso a los m�dulos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   1065
         Left            =   100
         TabIndex        =   13
         Top             =   1200
         Width           =   12820
         Begin VB.CheckBox chkFSSM 
            BackColor       =   &H00808000&
            Caption         =   "DCon acceso a FSSM"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   7740
            TabIndex        =   25
            Top             =   330
            Width           =   2190
         End
         Begin VB.CheckBox chkFSIM 
            BackColor       =   &H00808000&
            Caption         =   "DCon acceso a FSIM"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   7740
            TabIndex        =   24
            Top             =   660
            Width           =   2400
         End
         Begin VB.CheckBox chkCONTRATOS 
            BackColor       =   &H00808000&
            Caption         =   "DCon acceso a FSCM"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   5180
            TabIndex        =   23
            Top             =   330
            Width           =   2220
         End
         Begin VB.CheckBox chkFSINT 
            BackColor       =   &H00808000&
            Caption         =   "DCon acceso a FSIS"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   10280
            TabIndex        =   22
            Top             =   330
            Width           =   2310
         End
         Begin VB.CheckBox chkFSQA 
            BackColor       =   &H00808000&
            Caption         =   "DCon acceso a FSQA"
            ForeColor       =   &H00FFFFFF&
            Height          =   195
            Left            =   2660
            TabIndex        =   21
            Top             =   660
            Width           =   2310
         End
         Begin VB.CheckBox chkFSWS 
            BackColor       =   &H00808000&
            Caption         =   "DCon acceso a FSPM"
            ForeColor       =   &H80000014&
            Height          =   195
            Left            =   2660
            TabIndex        =   20
            Top             =   330
            Width           =   2280
         End
         Begin VB.CheckBox chkProcurement 
            BackColor       =   &H00808000&
            Caption         =   "Con acceso a FSEP"
            ForeColor       =   &H80000014&
            Height          =   195
            Left            =   5180
            TabIndex        =   19
            Top             =   660
            Width           =   2220
         End
         Begin VB.CheckBox chkFSGS 
            BackColor       =   &H00808000&
            Caption         =   "DCon acceso a FSGS"
            ForeColor       =   &H80000014&
            Height          =   195
            Left            =   120
            TabIndex        =   4
            Top             =   330
            Width           =   1800
         End
         Begin VB.CheckBox chkSoloComp 
            BackColor       =   &H00808000&
            Caption         =   "Comprador"
            ForeColor       =   &H80000014&
            Height          =   195
            Left            =   300
            TabIndex        =   5
            Top             =   660
            Width           =   1650
         End
      End
      Begin VB.Label lblUsuCod2 
         BackColor       =   &H00808000&
         Caption         =   "C�digo de usuario:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   120
         TabIndex        =   18
         Top             =   270
         Width           =   1920
      End
      Begin VB.Label lblApe2 
         BackColor       =   &H00808000&
         Caption         =   "Apellidos:"
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   4020
         TabIndex        =   17
         Top             =   270
         Width           =   1200
      End
      Begin VB.Label lblNom2 
         BackColor       =   &H00808000&
         Caption         =   "Nombre:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   4020
         TabIndex        =   16
         Top             =   750
         Width           =   1200
      End
      Begin VB.Label lblPerCod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo de persona:"
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Left            =   120
         TabIndex        =   15
         Top             =   750
         Width           =   1980
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cerrar"
      Height          =   315
      Left            =   6810
      TabIndex        =   11
      Top             =   5250
      Width           =   1305
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Seleccionar"
      Default         =   -1  'True
      Height          =   315
      Left            =   5370
      TabIndex        =   10
      Top             =   5250
      Width           =   1305
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgPersonas 
      Height          =   2625
      Left            =   105
      TabIndex        =   9
      Top             =   2490
      Width           =   12825
      ScrollBars      =   3
      _Version        =   196617
      DataMode        =   2
      Col.Count       =   17
      stylesets.count =   2
      stylesets(0).Name=   "Usu"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmUSUBuscar.frx":01D5
      stylesets(1).Name=   "ActiveRowBlue"
      stylesets(1).ForeColor=   16777215
      stylesets(1).BackColor=   8388608
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmUSUBuscar.frx":01F1
      UseGroups       =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeRow   =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   185
      Groups.Count    =   12
      Groups(0).Width =   1323
      Groups(0).Caption=   "Usuarios"
      Groups(0).HasHeadForeColor=   -1  'True
      Groups(0).HasHeadBackColor=   -1  'True
      Groups(0).HeadForeColor=   16777215
      Groups(0).HeadBackColor=   8421504
      Groups(0).Columns(0).Width=   1323
      Groups(0).Columns(0).Caption=   "C�digo"
      Groups(0).Columns(0).Name=   "USU"
      Groups(0).Columns(0).AllowSizing=   0   'False
      Groups(0).Columns(0).DataField=   "Column 0"
      Groups(0).Columns(0).DataType=   8
      Groups(0).Columns(0).FieldLen=   256
      Groups(0).Columns(0).HasBackColor=   -1  'True
      Groups(0).Columns(0).BackColor=   16449500
      Groups(1).Width =   4128
      Groups(1).Caption=   "Unidades organizativas"
      Groups(1).HasHeadForeColor=   -1  'True
      Groups(1).HasHeadBackColor=   -1  'True
      Groups(1).HeadForeColor=   16777215
      Groups(1).HeadBackColor=   8421504
      Groups(1).Columns.Count=   4
      Groups(1).Columns(0).Width=   979
      Groups(1).Columns(0).Caption=   "DAcceso"
      Groups(1).Columns(0).Name=   "UON1"
      Groups(1).Columns(0).DataField=   "Column 1"
      Groups(1).Columns(0).DataType=   8
      Groups(1).Columns(0).FieldLen=   256
      Groups(1).Columns(0).HasBackColor=   -1  'True
      Groups(1).Columns(0).BackColor=   16449500
      Groups(1).Columns(1).Width=   979
      Groups(1).Columns(1).Caption=   "UON2"
      Groups(1).Columns(1).Name=   "UON2"
      Groups(1).Columns(1).DataField=   "Column 2"
      Groups(1).Columns(1).DataType=   8
      Groups(1).Columns(1).FieldLen=   256
      Groups(1).Columns(1).HasBackColor=   -1  'True
      Groups(1).Columns(1).BackColor=   16449500
      Groups(1).Columns(2).Width=   1005
      Groups(1).Columns(2).Caption=   "UON3"
      Groups(1).Columns(2).Name=   "UON3"
      Groups(1).Columns(2).DataField=   "Column 3"
      Groups(1).Columns(2).DataType=   8
      Groups(1).Columns(2).FieldLen=   256
      Groups(1).Columns(2).HasBackColor=   -1  'True
      Groups(1).Columns(2).BackColor=   16449500
      Groups(1).Columns(3).Width=   1164
      Groups(1).Columns(3).Caption=   "Dep."
      Groups(1).Columns(3).Name=   "DEP"
      Groups(1).Columns(3).DataField=   "Column 4"
      Groups(1).Columns(3).DataType=   8
      Groups(1).Columns(3).FieldLen=   256
      Groups(1).Columns(3).HasBackColor=   -1  'True
      Groups(1).Columns(3).BackColor=   16449500
      Groups(2).Width =   5847
      Groups(2).Caption=   "Personas"
      Groups(2).HasHeadForeColor=   -1  'True
      Groups(2).HasHeadBackColor=   -1  'True
      Groups(2).HeadForeColor=   16777215
      Groups(2).HeadBackColor=   8421504
      Groups(2).Columns.Count=   3
      Groups(2).Columns(0).Width=   1561
      Groups(2).Columns(0).Caption=   "C�digo"
      Groups(2).Columns(0).Name=   "CODPER"
      Groups(2).Columns(0).DataField=   "Column 5"
      Groups(2).Columns(0).DataType=   8
      Groups(2).Columns(0).FieldLen=   256
      Groups(2).Columns(1).Width=   2381
      Groups(2).Columns(1).Caption=   "Apellidos"
      Groups(2).Columns(1).Name=   "APE"
      Groups(2).Columns(1).DataField=   "Column 6"
      Groups(2).Columns(1).DataType=   8
      Groups(2).Columns(1).FieldLen=   256
      Groups(2).Columns(2).Width=   1905
      Groups(2).Columns(2).Caption=   "Nombre"
      Groups(2).Columns(2).Name=   "NOM"
      Groups(2).Columns(2).DataField=   "Column 7"
      Groups(2).Columns(2).DataType=   8
      Groups(2).Columns(2).FieldLen=   256
      Groups(3).Width =   2884
      Groups(3).Caption=   "Comprador"
      Groups(3).CaptionAlignment=   0
      Groups(3).HasHeadForeColor=   -1  'True
      Groups(3).HasHeadBackColor=   -1  'True
      Groups(3).HeadForeColor=   16777215
      Groups(3).HeadBackColor=   8421504
      Groups(3).Columns(0).Width=   2884
      Groups(3).Columns(0).Caption=   "Equipo"
      Groups(3).Columns(0).Name=   "Equipo"
      Groups(3).Columns(0).DataField=   "Column 8"
      Groups(3).Columns(0).DataType=   8
      Groups(3).Columns(0).FieldLen=   256
      Groups(4).Width =   1561
      Groups(4).Caption=   "E-Procurement"
      Groups(4).HasHeadForeColor=   -1  'True
      Groups(4).HasHeadBackColor=   -1  'True
      Groups(4).HeadForeColor=   16777215
      Groups(4).HeadBackColor=   8421504
      Groups(4).Columns(0).Width=   1561
      Groups(4).Columns(0).Caption=   "Acceso"
      Groups(4).Columns(0).Name=   "FSEP-Acceso"
      Groups(4).Columns(0).Alignment=   2
      Groups(4).Columns(0).DataField=   "Column 9"
      Groups(4).Columns(0).DataType=   8
      Groups(4).Columns(0).FieldLen=   256
      Groups(4).Columns(0).Style=   2
      Groups(5).Width =   1720
      Groups(5).Caption=   "FSGS"
      Groups(5).HasHeadForeColor=   -1  'True
      Groups(5).HasHeadBackColor=   -1  'True
      Groups(5).HeadForeColor=   16777215
      Groups(5).HeadBackColor=   8421504
      Groups(5).Columns(0).Width=   1720
      Groups(5).Columns(0).Caption=   "FSGS_ACCESO"
      Groups(5).Columns(0).Name=   "FSGS_ACCESO"
      Groups(5).Columns(0).Alignment=   2
      Groups(5).Columns(0).DataField=   "Column 10"
      Groups(5).Columns(0).DataType=   8
      Groups(5).Columns(0).FieldLen=   256
      Groups(5).Columns(0).Style=   2
      Groups(6).Width =   1508
      Groups(6).Caption=   "FSWS"
      Groups(6).HasHeadForeColor=   -1  'True
      Groups(6).HasHeadBackColor=   -1  'True
      Groups(6).HeadForeColor=   16777215
      Groups(6).HeadBackColor=   8421504
      Groups(6).Columns(0).Width=   1508
      Groups(6).Columns(0).Caption=   "FSWS_ACCESO"
      Groups(6).Columns(0).Name=   "FSWS_ACCESO"
      Groups(6).Columns(0).Alignment=   2
      Groups(6).Columns(0).DataField=   "Column 11"
      Groups(6).Columns(0).DataType=   8
      Groups(6).Columns(0).FieldLen=   256
      Groups(6).Columns(0).Style=   2
      Groups(7).Width =   1588
      Groups(7).Caption=   "FSQA"
      Groups(7).HasHeadForeColor=   -1  'True
      Groups(7).HasHeadBackColor=   -1  'True
      Groups(7).HeadForeColor=   16777215
      Groups(7).HeadBackColor=   8421504
      Groups(7).Columns(0).Width=   1588
      Groups(7).Columns(0).Caption=   "FSQA_ACCESO"
      Groups(7).Columns(0).Name=   "FSQA_ACCESO"
      Groups(7).Columns(0).Alignment=   2
      Groups(7).Columns(0).DataField=   "Column 12"
      Groups(7).Columns(0).DataType=   8
      Groups(7).Columns(0).FieldLen=   256
      Groups(7).Columns(0).Style=   2
      Groups(8).Width =   1588
      Groups(8).Caption=   "FSINT"
      Groups(8).HasHeadForeColor=   -1  'True
      Groups(8).HasHeadBackColor=   -1  'True
      Groups(8).HeadForeColor=   16777215
      Groups(8).HeadBackColor=   8421504
      Groups(8).Columns(0).Width=   1588
      Groups(8).Columns(0).Caption=   "FSINT_ACCESO"
      Groups(8).Columns(0).Name=   "FSINT_ACCESO"
      Groups(8).Columns(0).Alignment=   2
      Groups(8).Columns(0).DataField=   "Column 13"
      Groups(8).Columns(0).DataType=   8
      Groups(8).Columns(0).FieldLen=   256
      Groups(8).Columns(0).Style=   2
      Groups(9).Width =   1931
      Groups(9).Caption=   "FSCM"
      Groups(9).HasHeadForeColor=   -1  'True
      Groups(9).HasHeadBackColor=   -1  'True
      Groups(9).HeadForeColor=   16777215
      Groups(9).HeadBackColor=   8421504
      Groups(9).Columns(0).Width=   1931
      Groups(9).Columns(0).Caption=   "FSCM_ACCESO"
      Groups(9).Columns(0).Name=   "FSCM_ACCESO"
      Groups(9).Columns(0).Alignment=   2
      Groups(9).Columns(0).DataField=   "Column 14"
      Groups(9).Columns(0).DataType=   8
      Groups(9).Columns(0).FieldLen=   256
      Groups(9).Columns(0).Style=   2
      Groups(10).Width=   1614
      Groups(10).Caption=   "FSIM"
      Groups(10).HasHeadForeColor=   -1  'True
      Groups(10).HasHeadBackColor=   -1  'True
      Groups(10).HeadForeColor=   16777215
      Groups(10).HeadBackColor=   8421504
      Groups(10).Columns(0).Width=   1614
      Groups(10).Columns(0).Caption=   "FSIM_ACCESO"
      Groups(10).Columns(0).Name=   "FSIM_ACCESO"
      Groups(10).Columns(0).Alignment=   2
      Groups(10).Columns(0).DataField=   "Column 15"
      Groups(10).Columns(0).DataType=   8
      Groups(10).Columns(0).FieldLen=   256
      Groups(10).Columns(0).Style=   2
      Groups(11).Width=   1614
      Groups(11).Caption=   "FSSM"
      Groups(11).HasHeadForeColor=   -1  'True
      Groups(11).HasHeadBackColor=   -1  'True
      Groups(11).HeadForeColor=   16777215
      Groups(11).HeadBackColor=   8421504
      Groups(11).Columns(0).Width=   1614
      Groups(11).Columns(0).Caption=   "FSSM_ACCESO"
      Groups(11).Columns(0).Name=   "FSSM_ACCESO"
      Groups(11).Columns(0).Alignment=   2
      Groups(11).Columns(0).DataField=   "Column 16"
      Groups(11).Columns(0).DataType=   8
      Groups(11).Columns(0).FieldLen=   256
      Groups(11).Columns(0).Style=   2
      TabNavigation   =   1
      _ExtentX        =   22622
      _ExtentY        =   4630
      _StockProps     =   79
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmUSUBuscar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnSep As Integer = 2540

Private oUsuarios As Variant
Public g_sOrigen As String

Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private Sub chkFSGS_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    If chkFSGS.Value = vbUnchecked Then chkSoloComp.Value = vbUnchecked
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "chkFSGS_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkSoloComp_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    If chkSoloComp.Value = vbChecked Then chkFSGS.Value = vbChecked
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "chkSoloComp_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAceptar_Click()
    Dim nodx As MSComctlLib.node
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub

    If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm:
    
    Select Case g_sOrigen
        Case "frmUSUARIOS"
            If sdbgPersonas.Rows = 0 Then Exit Sub
            Set nodx = frmUSUARIOS.tvwUsu.Nodes.Item("Cod" & sdbgPersonas.Columns("USU").Value)
            
            nodx.Selected = True
            nodx.EnsureVisible
                
            Me.Hide
            frmUSUARIOS.tvwUsu_NodeClick nodx
            Unload Me
            Exit Sub
            
        Case "frmPROCE"
            If sdbgPersonas.Rows = 0 Then Exit Sub
            frmPROCE.sdbcUsuSobre.Text = sdbgPersonas.Columns("USU").Value
            frmPROCE.sdbcUsuSobre_Validate False
            
            Unload Me
        Case "frmDatoAmbito"
            If sdbgPersonas.Rows = 0 Then Exit Sub
            frmDatoAmbitoProce.sdbcUsuSobre.Text = sdbgPersonas.Columns("USU").Value
            frmDatoAmbitoProce.sdbcUsuSobre_Validate False
            
            Unload Me
    End Select
  
    Exit Sub

ERROR_Frm:
    
    oMensajes.UsuarioNuevo
    If Not oFSGSRaiz.fg_bProgramando Then Unload Me
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "cmdAceptar_Click", err, Erl, Me)
      GoTo ERROR_Frm
      Exit Sub
   End If
End Sub

Private Sub cmdCancelar_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    Unload Me
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
    
End Sub
''' <summary>Cargar la grid de personas con los datos</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo < 1</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub cmdCargarPer_Click()
Dim i As Long
Dim Cuenta As TipoEstadoCuentasUsuario

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    If Option2.Value = True Then Cuenta = Activas
    If Option1.Value = True Then Cuenta = Bloqueadas
    If Option3.Value = True Then Cuenta = TodasCuentas
    
    Screen.MousePointer = vbHourglass
    sdbgPersonas.RemoveAll
  
    oUsuarios = basPublic.oGestorSeguridad.BuscarTodosLosUsuariosDeTipoPersona(txtCodUsuPer, txtApePer, txtNombrePer, txtCodPer, , Cuenta, chkSoloComp, chkProcurement.Value, , chkFSWS.Value, chkFSGS.Value, chkFSQA.Value, chkFSINT.Value, chkCONTRATOS.Value, chkFSIM.Value, chkFSSM.Value)
  
    If IsEmpty(oUsuarios) Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    For i = 0 To UBound(oUsuarios)
        sdbgPersonas.AddItem oUsuarios(i).usu & Chr(m_lSeparador) & oUsuarios(i).UON1 & Chr(m_lSeparador) & oUsuarios(i).UON2 & Chr(m_lSeparador) & _
            oUsuarios(i).UON3 & Chr(m_lSeparador) & oUsuarios(i).Dep & Chr(m_lSeparador) & oUsuarios(i).Per & Chr(m_lSeparador) & oUsuarios(i).Ape & _
            Chr(m_lSeparador) & oUsuarios(i).Nom & Chr(m_lSeparador) & oUsuarios(i).Eqp & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSEP)) & _
            Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSGS)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSWS)) & _
            Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSQA)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSINT)) & _
            Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSCONTRATOS)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSIM)) & _
            Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSSM))
    Next
    
    Screen.MousePointer = vbNormal
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "cmdCargarPer_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If

If Not m_bActivado Then m_bActivado = True
If Me.Visible Then txtCodUsuPer.SetFocus
Exit Sub

ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>procedimiento de carga inicial del formulario</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1 sec</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub Form_Load()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    CargarRecursos
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    PonerFieldSeparator Me
    
    If FSEPConf Then
        chkFSGS.Visible = False
        chkSoloComp.Visible = False
        
        sdbgPersonas.Groups(3).Visible = False
        sdbgPersonas.Groups("FSGS").Visible = False
    Else
        'Si no tiene acceso al m�dulo del EP no muestra su frame:
        If Not gParametrosGenerales.gbPedidosAprov Then
            chkProcurement.Visible = False
            sdbgPersonas.Groups(4).Visible = False
        End If
        
        'Si no tiene acceso al m�dulo del WS no muestra su frame
        If gParametrosGenerales.gsAccesoFSWS = TipoAccesoFSWS.SinAcceso Then
            chkFSWS.Visible = False
            sdbgPersonas.Groups(6).Visible = False  'FSPM
        End If
        
        'Si no tiene acceso al m�dulo del QA no muestra su frame
        If gParametrosGenerales.gsAccesoFSQA = TipoAccesoFSQA.SinAcceso Then
            chkFSQA.Visible = False
            sdbgPersonas.Groups(7).Visible = False  'FSQA
        End If
        
        'Si no tiene acceso a Contratos, no muestra su frame
        If gParametrosGenerales.gsAccesoContrato = TipoAccesoContrato.SinAcceso Then
            chkCONTRATOS.Visible = False
            sdbgPersonas.Groups(9).Visible = False  'CONTRATOS
        End If
        
        'Si no tiene acceso a FSIM, no muestra su frame
        If gParametrosGenerales.gsAccesoFSIM = TIPOACCESOFSIM.SinAcceso Then
            chkFSIM.Visible = False
            sdbgPersonas.Groups(10).Visible = False  'CONTRATOS
        End If

        'Si no tiene acceso al visor de integraci�n no muestra su frame
        If Not gParametrosGenerales.gbIntegracion Then
            chkFSINT.Visible = False
            sdbgPersonas.Groups(8).Visible = False  'FSINT
        End If
        
        'Si no tiene acceso a FSSM no muestra su frame
        If gParametrosGenerales.gsAccesoFSSM = TipoAccesoFSSM.SinAcceso Then
            chkFSSM.Visible = False
            sdbgPersonas.Groups("FSSM").Visible = False
        End If
    End If
       
    If gParametrosGenerales.giLOGPREBLOQ = 0 Then
        fraBloq.Visible = False
    Else
        If gParametrosGenerales.giWinSecurity = UsuariosLocales Then
            fraBloq.Visible = False
        Else
            fraBloq.Visible = True
        End If
    End If
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>procedimiento de ajuste del tama�o</summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1 sec</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub Form_Resize()
    Dim colChecksVis As Collection
    Dim oChk As CheckBox
    Dim lTop As Long
    Dim lLeft As Long
    Dim k As Integer

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    On Error Resume Next
    If Me.Height < 4860 Then Me.Height = 4860
    If Me.Width < 13150 Then Me.Width = 13150
    
    'Posici�n de los checks de acceso seg�n su visibilidad
    Set colChecksVis = New Collection
    If chkFSGS.Visible Then colChecksVis.Add chkFSGS
    If chkSoloComp.Visible Then colChecksVis.Add chkSoloComp
    If chkProcurement.Visible Then colChecksVis.Add chkProcurement
    If chkFSQA.Visible Then colChecksVis.Add chkFSQA
    If chkCONTRATOS.Visible Then colChecksVis.Add chkCONTRATOS
    If chkFSWS.Visible Then colChecksVis.Add chkFSWS
    If chkFSSM.Visible Then colChecksVis.Add chkFSSM
    If chkFSIM.Visible Then colChecksVis.Add chkFSIM
    If chkFSINT.Visible Then colChecksVis.Add chkFSINT
    
    k = 0
    lTop = chkFSGS.Top
    lLeft = chkFSGS.Left
    For Each oChk In colChecksVis
        'Establecer Top seg�n la fila
        If k Mod 2 > 0 Then
            '2� fila
            oChk.Top = lTop + oChk.Height + 115
        Else
            '1� fila
            oChk.Top = lTop
        End If
        
        'Establecer Left seg�n la columna
        oChk.Left = lLeft + ((k \ 2) * cnSep)
        fraAccesos.Width = oChk.Left + cnSep
        
        k = k + 1
    Next
    Set oChk = Nothing
    Set colChecksVis = Nothing
    
    chkSoloComp.Left = chkSoloComp.Left + 180
    
    Picture2.Height = fraAccesos.Top + fraAccesos.Height + 115
        
    sdbgPersonas.Top = Picture2.Height + 115
    sdbgPersonas.Height = Me.Height - Picture2.Height - 1185
    sdbgPersonas.Width = Me.Width - 300
    
    cmdAceptar.Top = sdbgPersonas.Top + sdbgPersonas.Height + 80
    cmdCancelar.Top = cmdAceptar.Top
    cmdAceptar.Left = (Me.Width / 2) - cmdAceptar.Width
    cmdCancelar.Left = cmdAceptar.Left + 1440
    
    If FSEPConf Then
        sdbgPersonas.Groups(0).Width = sdbgPersonas.Width * 12 / 100
        sdbgPersonas.Groups(1).Width = sdbgPersonas.Width * 23 / 100
        sdbgPersonas.Groups(2).Width = sdbgPersonas.Width * 29 / 100
        sdbgPersonas.Groups(4).Width = sdbgPersonas.Width * 6 / 100 'FSEP
        sdbgPersonas.Groups(6).Width = sdbgPersonas.Width * 6 / 100 'FSPM
        sdbgPersonas.Groups(7).Width = sdbgPersonas.Width * 6 / 100  'FSQA
        sdbgPersonas.Groups(8).Width = sdbgPersonas.Width * 6 / 100  'FSINT
        sdbgPersonas.Groups(9).Width = sdbgPersonas.Width * 6 / 100  'CONTRATOS
        sdbgPersonas.Groups(10).Width = sdbgPersonas.Width * 6 / 100  'FSIM
        sdbgPersonas.Groups("FSSM").Width = sdbgPersonas.Width * 6 / 100  'FSIM
    Else
        Dim gruposVisibles As Integer
        gruposVisibles = 0
        Dim i As Integer
        For i = 0 To sdbgPersonas.Groups.Count - 1
            If sdbgPersonas.Groups(i).Visible Then
                gruposVisibles = gruposVisibles + 1
            End If
        Next
        
        'Hasta ahora, los tres primeros grupos del grid sdbgPersonas siempre est�n visibles.
        'Si eso cambia, habr� que cambiar c�mo est� hecho este c�lculo:
        sdbgPersonas.Groups(0).Width = sdbgPersonas.Width * 6 / 100
        sdbgPersonas.Groups(1).Width = sdbgPersonas.Width * 16 / 100
        sdbgPersonas.Groups(2).Width = sdbgPersonas.Width * 20 / 100
        If sdbgPersonas.Groups(3).Visible Then sdbgPersonas.Groups(3).Width = sdbgPersonas.Width * CSng((((51 / (gruposVisibles - 3)) + 2) / 100))
        For i = 4 To gruposVisibles - 1
            If sdbgPersonas.Groups(i).Visible Then
                sdbgPersonas.Groups(i).Width = sdbgPersonas.Width * CSng((51 / 100) / (gruposVisibles - 3))
            End If
        Next
    End If
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "Form_Resize", err, Erl)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
        Exit Sub
   End If
oFSGSRaiz.pg_sFrmCargado Me.Name, False
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgPersonas_DblClick()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    cmdAceptar_Click
    Exit Sub
ERROR:
    If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "sdbgPersonas_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
    End If
End Sub

''' Revisado por: blp. Fecha:23/10/2012
''' <summary>Al pulsar en la cabecera de las columnas se ordena por ese criterio el listado</summary>
''' <remarks>Llamada desde: evento HeadClick; Tiempo m�ximo: 0,1 sec</remarks>
Private Sub sdbgPersonas_HeadClick(ByVal ColIndex As Integer)
Dim Cuenta As TipoEstadoCuentasUsuario
Dim i As Integer
Dim iOrden As Integer

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    If sdbgPersonas.Rows = 0 Then Exit Sub

 Screen.MousePointer = vbHourglass
 
    If Option2.Value = True Then Cuenta = Activas
    If Option1.Value = True Then Cuenta = Bloqueadas
    If Option3.Value = True Then Cuenta = TodasCuentas
    
 sdbgPersonas.RemoveAll
    
    Select Case ColIndex
        Case 0 'C�digo de usuario
                iOrden = 0
        Case 1 'UON1
                iOrden = TipoOrdenacionPersonas.OrdPorUO
        Case 2 'UON2
                iOrden = TipoOrdenacionPersonas.OrdPorUO
        Case 3 'UON3
                iOrden = TipoOrdenacionPersonas.OrdPorUO
        Case 4 'dep
                iOrden = TipoOrdenacionPersonas.OrdPorDep
        Case 5 'Per
                iOrden = TipoOrdenacionPersonas.OrdPorCod
        Case 6 'Ape
                iOrden = TipoOrdenacionPersonas.OrdPorApe
        Case 7 'Nom
                iOrden = TipoOrdenacionPersonas.OrdPorNom
        Case 8 'Eqp
                iOrden = TipoOrdenacionPersonas.OrdPorEqp
        Case 9 'FSEP
                iOrden = TipoOrdenacionPersonas.OrdPorFSEP
        Case 10 'FSGS
                iOrden = TipoOrdenacionPersonas.OrdPorFSGS
        Case 11 'FSWS
                iOrden = TipoOrdenacionPersonas.OrdPorFSWS
        Case 12 'FSQA
                iOrden = TipoOrdenacionPersonas.OrdPorFSQA
        Case 13 'FSINT
                iOrden = TipoOrdenacionPersonas.OrdPorFSINT
        Case 14 'Contratos
                iOrden = TipoOrdenacionPersonas.OrdPorContratos
        Case 15 'FSIM
                iOrden = TipoOrdenacionPersonas.OrdPorFSIM
    End Select
    
    oUsuarios = basPublic.oGestorSeguridad.BuscarTodosLosUsuariosDeTipoPersona(txtCodUsuPer, txtApePer, txtNombrePer, txtCodPer, iOrden, Cuenta, chkSoloComp, chkProcurement, , chkFSWS.Value, chkFSGS.Value, chkFSQA.Value, chkFSINT.Value, chkCONTRATOS.Value, chkFSIM.Value)
    
    If IsEmpty(oUsuarios) Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    For i = 0 To UBound(oUsuarios)
         sdbgPersonas.AddItem oUsuarios(i).usu & Chr(m_lSeparador) & oUsuarios(i).UON1 & Chr(m_lSeparador) & oUsuarios(i).UON2 & Chr(m_lSeparador) & oUsuarios(i).UON3 & Chr(m_lSeparador) & oUsuarios(i).Dep & Chr(m_lSeparador) & oUsuarios(i).Per & Chr(m_lSeparador) & oUsuarios(i).Ape & Chr(m_lSeparador) & oUsuarios(i).Nom & Chr(m_lSeparador) & oUsuarios(i).Eqp & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSEP)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSGS)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSWS)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSQA)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSINT) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSCONTRATOS)) & Chr(m_lSeparador) & CStr(BooleanToSQLBinary(oUsuarios(i).FSIM)))
    Next
  
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "sdbgPersonas_HeadClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>Carga de los literales multiidioma</summary>
''' <remarks>Llamada desde:frmUSUBuscar.Form_Load; Tiempo m�ximo:0,1</remarks>
''' <revision>NGO 27/12/2011</revision>
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_USUBUSCAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        Me.caption = Ador(0).Value
        Ador.MoveNext
        Option3.caption = Ador(0).Value
        Ador.MoveNext
        lblUsuCod2.caption = Ador(0).Value
        Ador.MoveNext
        lblApe2.caption = Ador(0).Value '9
        Ador.MoveNext
        lblNom2.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Groups("Usuarios").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns("CODPER").caption = Ador(0).Value
        sdbgPersonas.Columns("USU").caption = Ador(0).Value
        sdbgPersonas.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns("APE").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns("NOMBRE").caption = Ador(0).Value '16
        Ador.MoveNext
        lblPerCod.caption = Ador(0).Value '20
        Ador.MoveNext
        sdbgPersonas.Groups("Unidades Organizativas").caption = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '29
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Columns("DEP").caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Groups("Personas").caption = Ador(0).Value
        Ador.MoveNext
        cmdCargarPer.ToolTipText = Ador(0).Value '34
        Ador.MoveNext
        sdbgPersonas.Groups("Funci�n compradora").caption = Ador(0).Value '40
        Ador.MoveNext
        sdbgPersonas.Columns("Equipo").caption = Ador(0).Value
        Ador.MoveNext
        chkSoloComp.caption = Ador(0).Value
        Ador.MoveNext
        sdbgPersonas.Groups(3).caption = Ador(0).Value
        Ador.MoveNext
        Option1.caption = Ador(0).Value
        Ador.MoveNext
        Option2.caption = Ador(0).Value
        Ador.MoveNext
        Ador.MoveNext
        sdbgPersonas.Columns("FSEP-Acceso").caption = Ador(0).Value
        sdbgPersonas.Columns("FSGS_ACCESO").caption = Ador(0).Value
        sdbgPersonas.Columns("FSWS_ACCESO").caption = Ador(0).Value
        sdbgPersonas.Columns("FSQA_ACCESO").caption = Ador(0).Value
        sdbgPersonas.Columns("FSINT_ACCESO").caption = Ador(0).Value
        sdbgPersonas.Columns("FSCM_ACCESO").caption = Ador(0).Value '24 Contratos
        sdbgPersonas.Columns("FSIM_ACCESO").caption = Ador(0).Value '24 Contratos
        sdbgPersonas.Columns("FSSM_ACCESO").caption = Ador(0).Value
        Ador.MoveNext
        chkFSWS.caption = Ador(0).Value  '25 Con acceso a FSWS
        Ador.MoveNext
        chkProcurement.caption = Ador(0).Value  '26 Con acceso a FSEP
        Ador.MoveNext
        chkFSGS.caption = Ador(0).Value  '27 Con acceso a FSGS
        Ador.MoveNext
        chkFSQA.caption = Ador(0).Value '28 Con acceso a FSQA
        Ador.MoveNext
        chkFSINT.caption = Ador(0).Value '29 Con acceso al visor de integraci�n
        Ador.MoveNext
        chkCONTRATOS.caption = Ador(0).Value '30 Con acceso a Contratos
        Ador.MoveNext
        sdbgPersonas.Groups("CONTRATOS").caption = UCase(Ador(0).Value) '31 CONTRATOS
        Ador.MoveNext
        chkFSIM.caption = Ador(0).Value '32 Con acceso a FSIM
        Ador.MoveNext
        chkFSSM.caption = Ador(0).Value '33 Con acceso a FSSM
        Ador.MoveNext
        fraAccesos.caption = Ador(0).Value '34 Acceso a los m�dulos
        Ador.MoveNext
        fraBloq.caption = Ador(0).Value '35 Bloqueos
        Ador.Close
    End If
    
    sdbgPersonas.Columns("UON1").caption = basParametros.gParametrosGenerales.gsABR_UON1
    sdbgPersonas.Columns("UON2").caption = basParametros.gParametrosGenerales.gsABR_UON2
    sdbgPersonas.Columns("UON3").caption = basParametros.gParametrosGenerales.gsABR_UON3
    
    sdbgPersonas.Groups("FSWS").caption = "FSPM"
    sdbgPersonas.Groups("FSGS").caption = "FSGS"
    sdbgPersonas.Groups("FSQA").caption = "FSQA"
    sdbgPersonas.Groups(4).caption = "FSEP"
    sdbgPersonas.Groups("FSINT").caption = "FSINT"
    sdbgPersonas.Groups("FSIM").caption = "FSIM"
    sdbgPersonas.Groups("FSSM").caption = "FSSM"
    
    Set Ador = Nothing
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmUSUBuscar", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
