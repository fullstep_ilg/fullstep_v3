VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmPRESConCopiar 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Copiar en otro a�o"
   ClientHeight    =   2505
   ClientLeft      =   870
   ClientTop       =   630
   ClientWidth     =   3240
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2505
   ScaleWidth      =   3240
   Begin VB.CheckBox chkImp 
      BackColor       =   &H00808000&
      Caption         =   "Copiar importes y objetivos"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   255
      TabIndex        =   6
      Top             =   1680
      Value           =   1  'Checked
      Width           =   2940
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   1680
      TabIndex        =   3
      Top             =   2100
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   540
      TabIndex        =   2
      Top             =   2100
      Width           =   1005
   End
   Begin VB.OptionButton optTodos 
      BackColor       =   &H00808000&
      Caption         =   "Copiar toda la rama"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   255
      TabIndex        =   1
      Top             =   840
      Value           =   -1  'True
      Width           =   3000
   End
   Begin VB.OptionButton optSeleccionado 
      BackColor       =   &H00808000&
      Caption         =   "Copiar solo la seleccionada"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   255
      TabIndex        =   0
      Top             =   1200
      Width           =   2940
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
      Height          =   285
      Left            =   1215
      TabIndex        =   4
      Top             =   120
      Width           =   960
      ScrollBars      =   2
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns(0).Width=   1693
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   1693
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      X1              =   270
      X2              =   2925
      Y1              =   1560
      Y2              =   1560
   End
   Begin VB.Label lblA�o 
      BackColor       =   &H00808000&
      Caption         =   "En a�o:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   330
      TabIndex        =   5
      Top             =   180
      Width           =   825
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      X1              =   270
      X2              =   2925
      Y1              =   660
      Y2              =   660
   End
End
Attribute VB_Name = "frmPRESConCopiar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmdAceptar_Click()
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer

    If optSeleccionado.Visible = False Then
                
        irespuesta = oMensajes.PreguntaCopiarPresupuestos
        If irespuesta = vbYes Then
            
            Screen.MousePointer = vbHourglass
            teserror = frmPresupuestos2.oPresupuestos.CopiarTodasLasPartidasEnAnyo(frmPresupuestos2.sdbcAnyo, sdbcAnyo, (chkImp.Value = vbChecked), frmPresupuestos2.m_sUON1, frmPresupuestos2.m_sUON2, frmPresupuestos2.m_sUON3)
        
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        Else
            Screen.MousePointer = vbNormal
            Unload Me
        End If
        
    Else
        
        If Not frmPresupuestos2.tvwEstrPres.selectedItem Is Nothing Then
    
            Screen.MousePointer = vbHourglass
            
            Select Case Left(frmPresupuestos2.tvwEstrPres.selectedItem.Tag, 5)
            
                Case "Raiz "
                        
        
                Case "PRES1"
                    
                        teserror = frmPresupuestos2.oPres1Seleccionado.CopiarEnAnyo(sdbcAnyo, (optSeleccionado.Value = True), (chkImp.Value = vbChecked))
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                
                Case "PRES2"
                        
                        teserror = frmPresupuestos2.oPres2Seleccionado.CopiarEnAnyo(sdbcAnyo, (optSeleccionado.Value = True), (chkImp.Value = vbChecked))
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                
                Case "PRES3"
                        
                        teserror = frmPresupuestos2.oPres3Seleccionado.CopiarEnAnyo(sdbcAnyo, (optSeleccionado.Value = True), (chkImp.Value = vbChecked))
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
                        
                Case "PRES4"
                        
                        teserror = frmPresupuestos2.oPres4Seleccionado.CopiarEnAnyo(sdbcAnyo, (optSeleccionado.Value = True), (chkImp.Value = vbChecked))
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Screen.MousePointer = vbNormal
                            Exit Sub
                        End If
            End Select
                
        End If
        

    End If
    
    Screen.MousePointer = vbNormal
    
    Unload Me
    
End Sub

Private Sub cmdCancelar_Click()
    
    Unload Me
    
End Sub
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset
Dim i As Integer

' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESCON_COPIAR, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        chkImp.caption = Ador(0).Value '
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        frmPRESConCopiar.caption = Ador(0).Value
        Ador.MoveNext
        lblA�o.caption = Ador(0).Value
        Ador.MoveNext
        optSeleccionado.caption = Ador(0).Value
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub


Private Sub Form_Load()
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2

    CargarAnyos
    
End Sub
Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    CargarRecursos
    
    iAnyoActual = Int(frmPresupuestos2.sdbcAnyo) + 1
        
    For iInd = iAnyoActual - 5 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 5
    
End Sub


