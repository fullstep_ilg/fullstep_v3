VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPROVEEqpPorProve 
   Caption         =   "Equipos por proveedor"
   ClientHeight    =   5520
   ClientLeft      =   2100
   ClientTop       =   2835
   ClientWidth     =   6840
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROVEEqpPorProve.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   ScaleHeight     =   5520
   ScaleWidth      =   6840
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   5400
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   18
      ImageHeight     =   18
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPROVEEqpPorProve.frx":014A
            Key             =   "Equipo"
            Object.Tag             =   "Equipo"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab stabGeneral 
      Height          =   5340
      Left            =   45
      TabIndex        =   16
      Top             =   90
      Width           =   6720
      _ExtentX        =   11853
      _ExtentY        =   9419
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Filtro"
      TabPicture(0)   =   "frmPROVEEqpPorProve.frx":01CF
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblGMN4_4"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblGMN1_4"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblGMN2_4"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblGMN3_4"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "sdbcGMN4_4Den"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "sdbcGMN3_4Den"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "sdbcGMN2_4Den"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "sdbcGMN1_4Den"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "sdbcGMN4_4Cod"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "sdbcGMN3_4Cod"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "sdbcGMN2_4Cod"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "sdbcGMN1_4Cod"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "cmdSelMat"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "Proveedor"
      TabPicture(1)   =   "frmPROVEEqpPorProve.frx":01EB
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "picEdit"
      Tab(1).Control(1)=   "picNavigate"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "lstEqpMod"
      Tab(1).Control(3)=   "lstEqp"
      Tab(1).Control(4)=   "fraselProve"
      Tab(1).ControlCount=   5
      Begin VB.CommandButton cmdSelMat 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6165
         Picture         =   "frmPROVEEqpPorProve.frx":0207
         Style           =   1  'Graphical
         TabIndex        =   0
         Top             =   1260
         Width           =   345
      End
      Begin VB.Frame fraselProve 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   750
         Left            =   -74865
         TabIndex        =   26
         Top             =   405
         Width           =   6450
         Begin VB.CommandButton cmdBuscar 
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5985
            Picture         =   "frmPROVEEqpPorProve.frx":0273
            Style           =   1  'Graphical
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   270
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
            Height          =   285
            Left            =   2160
            TabIndex        =   10
            Top             =   270
            Width           =   3795
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominación"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 1"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2117
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 0"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6694
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
            Height          =   285
            Left            =   945
            TabIndex        =   9
            Top             =   270
            Width           =   1215
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2540
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Denominación"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   2143
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblProveCod 
            Caption         =   "Código:"
            Height          =   195
            Left            =   150
            TabIndex        =   27
            Top             =   300
            Width           =   720
         End
      End
      Begin MSComctlLib.ListView lstEqp 
         Height          =   3615
         Left            =   -74865
         TabIndex        =   12
         Top             =   1215
         Width           =   6450
         _ExtentX        =   11377
         _ExtentY        =   6376
         View            =   2
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "COD"
            Object.Tag             =   "COD"
            Text            =   "Cod"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "DEN"
            Object.Tag             =   "DEN"
            Text            =   "Denominación"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lstEqpMod 
         Height          =   3615
         Left            =   -74865
         TabIndex        =   25
         Top             =   1215
         Width           =   6450
         _ExtentX        =   11377
         _ExtentY        =   6376
         View            =   2
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         _Version        =   393217
         Icons           =   "ImageList1"
         SmallIcons      =   "ImageList1"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "COD"
            Object.Tag             =   "COD"
            Text            =   "Cod"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "DEN"
            Object.Tag             =   "DEN"
            Text            =   "Denominación"
            Object.Width           =   2540
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Cod 
         Height          =   285
         Left            =   1905
         TabIndex        =   1
         Top             =   1680
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1164
         Columns(0).Caption=   "Código"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3889
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Cod 
         Height          =   285
         Left            =   1905
         TabIndex        =   3
         Top             =   2145
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Cod 
         Height          =   285
         Left            =   1905
         TabIndex        =   5
         Top             =   2595
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Cod 
         Height          =   285
         Left            =   1905
         TabIndex        =   7
         Top             =   3060
         Width           =   1065
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   900
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Denominación"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 2"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1879
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN1_4Den 
         Height          =   285
         Left            =   2985
         TabIndex        =   2
         Top             =   1680
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4657
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1535
         Columns(1).Caption=   "Código"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN2_4Den 
         Height          =   285
         Left            =   2985
         TabIndex        =   4
         Top             =   2145
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4657
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1535
         Columns(1).Caption=   "Código"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN3_4Den 
         Height          =   285
         Left            =   2985
         TabIndex        =   6
         Top             =   2595
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4657
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1535
         Columns(1).Caption=   "Código"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcGMN4_4Den 
         Height          =   285
         Left            =   2985
         TabIndex        =   8
         Top             =   3060
         Width           =   3525
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   4657
         Columns(0).Caption=   "Denominación"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1535
         Columns(1).Caption=   "Código"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6218
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.PictureBox picNavigate 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -74865
         ScaleHeight     =   375
         ScaleWidth      =   6450
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   4920
         Width           =   6450
         Begin VB.CommandButton cmdListado 
            Caption         =   "&Listado"
            Height          =   345
            Left            =   2220
            TabIndex        =   15
            Top             =   0
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdModificar 
            Caption         =   "&Modificar"
            Height          =   345
            Left            =   30
            TabIndex        =   13
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "&Restaurar"
            Height          =   345
            Left            =   1125
            TabIndex        =   14
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.PictureBox picEdit 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -74985
         ScaleHeight     =   345
         ScaleWidth      =   6435
         TabIndex        =   18
         Top             =   4920
         Visible         =   0   'False
         Width           =   6435
         Begin VB.CommandButton cmdCancelar 
            Caption         =   "Cancelar"
            Height          =   345
            Left            =   3420
            TabIndex        =   20
            Top             =   0
            Width           =   1005
         End
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Height          =   345
            Left            =   2205
            TabIndex        =   19
            Top             =   0
            Width           =   1005
         End
      End
      Begin VB.Label lblGMN3_4 
         Caption         =   "Subfamilia:"
         Height          =   225
         Left            =   270
         TabIndex        =   24
         Top             =   2625
         Width           =   1635
      End
      Begin VB.Label lblGMN2_4 
         Caption         =   "Familia:"
         Height          =   225
         Left            =   270
         TabIndex        =   23
         Top             =   2175
         Width           =   1635
      End
      Begin VB.Label lblGMN1_4 
         Caption         =   "Comodity:"
         Height          =   225
         Left            =   270
         TabIndex        =   22
         Top             =   1710
         Width           =   1635
      End
      Begin VB.Label lblGMN4_4 
         Caption         =   "Grupo:"
         Height          =   225
         Left            =   270
         TabIndex        =   21
         Top             =   3090
         Width           =   1635
      End
   End
End
Attribute VB_Name = "frmPROVEEqpPorProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables del resize
Private iAltura As Integer
Private iAnchura As Integer

' Variables de seguridad
Private bREqp As Boolean
Private bRMat As Boolean
Private bModif As Boolean

'Variables para materiales
Private oGruposMN1 As CGruposMatNivel1
Private oGruposMN2 As CGruposMatNivel2
Private oGruposMN3 As CGruposMatNivel3
Private oGruposMN4 As CGruposMatNivel4

'Variables para contener los Paises, provincias y monedas
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4

Private oProveSeleccionado As CProveedor
Private oProves As CProveedores
Private oEqps As CEquipos
Private oEqpsSeleccionados As CEquipos
Private oEqpsDesSeleccionados As CEquipos
Private oEqpSeleccionado As CEquipo
Private oIEquiposAsignados As IEquiposAsigAProveedor
Private Accion As AccionesSummit

'''Combos
Private GMN1RespetarCombo As Boolean
Private GMN1CargarComboDesde As Boolean
Private GMN2RespetarCombo As Boolean
Private GMN2CargarComboDesde As Boolean
Private GMN3RespetarCombo As Boolean
Private GMN3CargarComboDesde As Boolean
Private GMN4RespetarCombo As Boolean
Private GMN4CargarComboDesde As Boolean

Private RespetarComboProve As Boolean
Private Sub Arrange()

    If Me.Width >= 200 Then stabGeneral.Width = Me.Width - 200
    If Me.Height >= 550 Then stabGeneral.Height = Me.Height - 550
    If stabGeneral.Width >= 250 Then
        lstEqp.Width = stabGeneral.Width - 250
        lstEqpMod.Width = stabGeneral.Width - 250
    End If
    If stabGeneral.Height >= 1700 Then
        lstEqp.Height = stabGeneral.Height - 1700
        lstEqpMod.Height = stabGeneral.Height - 1700
    End If
    picNavigate.Top = lstEqp.Top + lstEqp.Height + 75
    picEdit.Top = lstEqp.Top + lstEqp.Height + 75
    picEdit.Left = (lstEqp.Width / 2) - (picEdit.Width / 2)

End Sub

Private Sub sdbcGMN1_4Cod_CloseUp()
    
    If sdbcGMN1_4Cod.Value = "..." Then
        sdbcGMN1_4Cod.Text = ""
        Exit Sub
    End If
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Den.Text = sdbcGMN1_4Cod.Columns(1).Text
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Cod.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN1_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Cod.RemoveAll
    
    If bRMat Then
        
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , , False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
        
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , , False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, , False
        End If
        
    End If

    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Cod.AddItem "..."
    End If

    sdbcGMN1_4Cod.SelStart = 0
    sdbcGMN1_4Cod.SelLength = Len(sdbcGMN1_4Cod.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Cod_InitColumnProps()
    
    sdbcGMN1_4Cod.DataFieldList = "Column 0"
    sdbcGMN1_4Cod.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN1_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Cod, Text
End Sub

Private Sub sdbcGMN1_4Cod_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If stabGeneral.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
    
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), , True, , False)
    Else
        
        oGMN1s.CargarTodosLosGruposMat sdbcGMN1_4Cod.Text, , True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = oGMN1s.Item(1).Den
        
        sdbcGMN1_4Cod.Columns(0).Value = sdbcGMN1_4Cod.Text
        sdbcGMN1_4Cod.Columns(1).Value = sdbcGMN1_4Den.Text
        
        GMN1RespetarCombo = False
        GMN1Seleccionado
        GMN1CargarComboDesde = False
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN1_4Den_Click()
     If Not sdbcGMN1_4Den.DroppedDown Then
        sdbcGMN1_4Cod = ""
        sdbcGMN1_4Den = ""
    End If
End Sub

Private Sub sdbcGMN1_4Den_InitColumnProps()
    
    sdbcGMN1_4Den.DataFieldList = "Column 0"
    sdbcGMN1_4Den.DataFieldToDisplay = "Column 0"
    
End Sub



Private Sub sdbcGMN1_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN1_4Den, Text
End Sub

Private Sub sdbcGMN1_4Den_Validate(Cancel As Boolean)

    Dim oGMN1s As CGruposMatNivel1
    Dim oIMAsig As IMaterialAsignado
    
    Dim bExiste As Boolean
    
    Set oGMN1s = oFSGSRaiz.Generar_CGruposMatNivel1
    
    If stabGeneral.Tab = 1 Then Exit Sub
    
    If sdbcGMN1_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    Screen.MousePointer = vbHourglass
    If bRMat Then
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGMN1s = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, , False)
    Else
       
        oGMN1s.CargarTodosLosGruposMat , sdbcGMN1_4Den.Text, True, , False
    End If
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oGMN1s.Count = 0)
    
    If Not bExiste Then
        Set oGMN1Seleccionado = Nothing
        sdbcGMN1_4Den.Text = ""
    Else
        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = oGMN1s.Item(1).Cod
        
        sdbcGMN1_4Den.Columns(0).Value = sdbcGMN1_4Den.Text
        sdbcGMN1_4Den.Columns(1).Value = sdbcGMN1_4Cod.Text
                    
        GMN1RespetarCombo = False
        GMN1Seleccionado
    End If
    
    Set oGMN1s = Nothing

End Sub

Private Sub sdbcGMN2_4Cod_Click()
     
     If Not sdbcGMN2_4Cod.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Cod, Text
End Sub

Private Sub sdbcGMN2_4Cod_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If stabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN2_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , True, , False)
        Else
          
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN2_4Cod.Text, , True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Cod.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = oGMN2s.Item(1).Den
        
        sdbcGMN2_4Cod.Columns(0).Value = sdbcGMN2_4Cod.Text
        sdbcGMN2_4Cod.Columns(1).Value = sdbcGMN2_4Den.Text
        
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing
    
End Sub

Private Sub sdbcGMN2_4Den_Click()
     
     If Not sdbcGMN2_4Den.DroppedDown Then
        sdbcGMN2_4Cod = ""
        sdbcGMN2_4Den = ""
    End If
End Sub

Private Sub sdbcGMN2_4Den_InitColumnProps()
    
    sdbcGMN2_4Den.DataFieldList = "Column 0"
    sdbcGMN2_4Den.DataFieldToDisplay = "Column 0"
    
End Sub

Private Sub sdbcGMN2_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN2_4Den, Text
End Sub

Private Sub sdbcGMN2_4Den_Validate(Cancel As Boolean)

    Dim oGMN2s As CGruposMatNivel2
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN2s = oFSGSRaiz.Generar_CGruposMatNivel2
    
    If stabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN2_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN1Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN2s = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, , Trim(sdbcGMN2_4Den), True, , False)
        Else
          
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN2_4Den.Text, True, , False
            Set oGMN2s = oGMN1Seleccionado.GruposMatNivel2
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN2s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN2_4Den.Text = ""
    Else
        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = oGMN2s.Item(1).Cod
        
        sdbcGMN2_4Den.Columns(0).Value = sdbcGMN2_4Den.Text
        sdbcGMN2_4Den.Columns(1).Value = sdbcGMN2_4Cod.Text
                    
        GMN2RespetarCombo = False
        GMN2Seleccionado
        GMN2CargarComboDesde = False
    End If
    
    Set oGMN2s = Nothing

End Sub

Private Sub sdbcGMN3_4Cod_Click()
    
     If Not sdbcGMN3_4Cod.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
    
End Sub

Private Sub sdbcGMN3_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Cod, Text
End Sub

Private Sub sdbcGMN3_4Cod_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If stabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN3_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, Trim(sdbcGMN3_4Cod), , True, , False)
        Else
           
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN3_4Cod.Text, , True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Cod.Text = ""
   Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = oGMN3s.Item(1).Den
        
        sdbcGMN3_4Cod.Columns(0).Value = sdbcGMN3_4Cod.Text
        sdbcGMN3_4Cod.Columns(1).Value = sdbcGMN3_4Den.Text
        
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN3_4den_Change()
    
    If Not GMN3RespetarCombo Then

        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Den_Click()
     
     If Not sdbcGMN3_4Den.DroppedDown Then
        sdbcGMN3_4Cod = ""
        sdbcGMN3_4Den = ""
    End If
End Sub

Private Sub sdbcGMN3_4Den_CloseUp()
  
    If sdbcGMN3_4Den.Value = "..." Then
        sdbcGMN3_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Den.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Den.Columns(1).Text
    sdbcGMN3_4Den.Text = sdbcGMN3_4Den.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Den.RemoveAll
    
    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , , , False)
        End If
        
    
    Else
        
        
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Den.AddItem "..."
    End If

    sdbcGMN3_4Den.SelStart = 0
    sdbcGMN3_4Den.SelLength = Len(sdbcGMN3_4Den.Text)
    sdbcGMN3_4Den.Refresh
     
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN3_4Den_InitColumnProps()
    sdbcGMN3_4Den.DataFieldList = "Column 0"
    sdbcGMN3_4Den.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcGMN3_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN3_4Den, Text
End Sub


Private Sub sdbcGMN3_4Den_Validate(Cancel As Boolean)

    Dim oGMN3s As CGruposMatNivel3
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN3s = oFSGSRaiz.Generar_CGruposMatNivel3
    
    If stabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN3_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN2Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN3s = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, , Trim(sdbcGMN3_4Den), True, , False)
        Else
            
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN3_4Den.Text, True, , False
            Set oGMN3s = oGMN2Seleccionado.GruposMatNivel3
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN3s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN3_4Den.Text = ""
   Else
        GMN3RespetarCombo = True
        sdbcGMN3_4Cod.Text = oGMN3s.Item(1).Cod
        
        sdbcGMN3_4Den.Columns(0).Value = sdbcGMN3_4Den.Text
        sdbcGMN3_4Den.Columns(1).Value = sdbcGMN3_4Cod.Text
                    
        GMN3RespetarCombo = False
        GMN3Seleccionado
        GMN3CargarComboDesde = False
    End If
    
    Set oGMN3s = Nothing

End Sub

Private Sub sdbcGMN4_4Cod_Click()
     
     If Not sdbcGMN4_4Cod.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Cod_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Cod, Text
End Sub

Private Sub sdbcGMN4_4Cod_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If stabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN4_4Cod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
           
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, Trim(sdbcGMN4_4Cod), , True, , False)
        Else
           
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN4_4Cod.Text, , True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Cod.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = oGMN4s.Item(1).Den
        
        sdbcGMN4_4Cod.Columns(0).Value = sdbcGMN4_4Cod.Text
        sdbcGMN4_4Cod.Columns(1).Value = sdbcGMN4_4Den.Text
        
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub

Private Sub sdbcGMN4_4Den_Click()
     If Not sdbcGMN4_4Den.DroppedDown Then
        sdbcGMN4_4Cod = ""
        sdbcGMN4_4Den = ""
    End If
End Sub

Private Sub sdbcGMN4_4Den_InitColumnProps()
    
    sdbcGMN4_4Den.DataFieldList = "Column 0"
    sdbcGMN4_4Den.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcGMN4_4Den_PositionList(ByVal Text As String)
PositionList sdbcGMN4_4Den, Text
End Sub
Private Sub sdbcGMN1_4Cod_Change()

    If Not GMN1RespetarCombo Then
    
        GMN1RespetarCombo = True
        sdbcGMN1_4Den.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub
Private Sub sdbcGMN1_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    
    sdbcGMN1_4Den.RemoveAll
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN1 = Nothing
        If GMN1CargarComboDesde Then
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), , True, False)
        Else
            Set oGruposMN1 = oIMAsig.DevolverGruposMN1Visibles(, , , True, False)
        End If
    Else
        Set oGruposMN1 = Nothing
        Set oGruposMN1 = oFSGSRaiz.Generar_CGruposMatNivel1
       
        If GMN1CargarComboDesde Then
            oGruposMN1.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcGMN1_4Den), True, False
        Else
            oGruposMN1.CargarTodosLosGruposMat , , 1, True, False
        End If
    End If
        
    
    Codigos = oGruposMN1.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN1_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN1CargarComboDesde And Not oGruposMN1.EOF Then
        sdbcGMN1_4Den.AddItem "..."
    End If

    sdbcGMN1_4Den.SelStart = 0
    sdbcGMN1_4Den.SelLength = Len(sdbcGMN1_4Den.Text)
    sdbcGMN1_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN2_4Cod_Change()
    
    If Not GMN2RespetarCombo Then
    
        GMN2RespetarCombo = True
        sdbcGMN2_4Den.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcGMN2_4Cod_CloseUp()

    If sdbcGMN2_4Cod.Value = "..." Then
        sdbcGMN2_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Cod.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Den.Text = sdbcGMN2_4Cod.Columns(1).Text
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Cod.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Cod.RemoveAll

    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), , , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2Visibles(sdbcGMN1_4Cod, , , , , False)
        End If
    Else
        
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Cod.AddItem "..."
    End If

    sdbcGMN2_4Cod.SelStart = 0
    sdbcGMN2_4Cod.SelLength = Len(sdbcGMN2_4Cod.Text)
    sdbcGMN2_4Cod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN2_4Den_Change()
    
    If Not GMN2RespetarCombo Then

        GMN2RespetarCombo = True
        sdbcGMN2_4Cod.Text = ""
        sdbcGMN3_4Cod.Text = ""
        GMN2RespetarCombo = False
        Set oGMN2Seleccionado = Nothing
        GMN2CargarComboDesde = True
        
    End If

    
End Sub
Private Sub sdbcGMN2_4Den_CloseUp()
   
    If sdbcGMN2_4Den.Value = "..." Then
        sdbcGMN2_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN2_4Den.Value = "" Then Exit Sub
    
    GMN2RespetarCombo = True
    sdbcGMN2_4Cod.Text = sdbcGMN2_4Den.Columns(1).Text
    sdbcGMN2_4Den.Text = sdbcGMN2_4Den.Columns(0).Text
    GMN2RespetarCombo = False
    
    GMN2Seleccionado
    
    GMN2CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN2_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN2_4Den.RemoveAll
    
    If oGMN1Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN2 = Nothing
        If GMN2CargarComboDesde Then
            Set oGruposMN2 = oIMAsig.DevolverGruposMN2VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), , , False)
        Else
            Set oGruposMN2 = oIMAsig.DevolverGruposMN1Visibles(, , , , False)
        End If
    
    Else
        
        
        If GMN2CargarComboDesde Then
            oGMN1Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN2_4Cod), , , False
        Else
            oGMN1Seleccionado.CargarTodosLosGruposMat , , , , False
        End If
            
        Set oGruposMN2 = oGMN1Seleccionado.GruposMatNivel2
        
    End If

    
    Codigos = oGruposMN2.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN2_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN2CargarComboDesde And Not oGruposMN2.EOF Then
        sdbcGMN2_4Den.AddItem "..."
    End If

    sdbcGMN2_4Den.SelStart = 0
    sdbcGMN2_4Den.SelLength = Len(sdbcGMN2_4Den.Text)
    sdbcGMN2_4Den.Refresh

    Screen.MousePointer = vbNormal
End Sub



Private Sub sdbcGMN3_4Cod_Change()
    
    If Not GMN3RespetarCombo Then
    
        GMN3RespetarCombo = True
        sdbcGMN3_4Den.Text = ""
        sdbcGMN4_4Cod.Text = ""
        GMN3RespetarCombo = False
        Set oGMN3Seleccionado = Nothing
        GMN3CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN3_4Cod_CloseUp()
  
    If sdbcGMN3_4Cod.Value = "..." Then
        sdbcGMN3_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN3_4Cod.Value = "" Then Exit Sub
    
    GMN3RespetarCombo = True
    sdbcGMN3_4Den.Text = sdbcGMN3_4Cod.Columns(1).Text
    sdbcGMN3_4Cod.Text = sdbcGMN3_4Cod.Columns(0).Text
    GMN3RespetarCombo = False
    
    GMN3Seleccionado
    
    GMN3CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN3_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN3_4Cod.RemoveAll

    If oGMN2Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
       
        Set oGruposMN3 = Nothing
        If GMN3CargarComboDesde Then
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3VisiblesDesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , False)
        Else
            Set oGruposMN3 = oIMAsig.DevolverGruposMN3Visibles(sdbcGMN1_4Cod, sdbcGMN2_4Cod, , , , False)
        End If
    Else
        
        
        If GMN3CargarComboDesde Then
            oGMN2Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN3_4Cod), , , False
        Else
            oGMN2Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN3 = oGMN2Seleccionado.GruposMatNivel3
        
    End If

    
    Codigos = oGruposMN3.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN3_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN3CargarComboDesde And Not oGruposMN3.EOF Then
        sdbcGMN3_4Cod.AddItem "..."
    End If

    sdbcGMN3_4Cod.SelStart = 0
    sdbcGMN3_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN3_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcGMN1_4Den_Change()
    
    If Not GMN1RespetarCombo Then

        GMN1RespetarCombo = True
        sdbcGMN1_4Cod.Text = ""
        sdbcGMN2_4Cod.Text = ""
        GMN1RespetarCombo = False
        Set oGMN1Seleccionado = Nothing
        GMN1CargarComboDesde = True
        
    End If

End Sub

Private Sub sdbcGMN1_4Den_CloseUp()
    
    
    If sdbcGMN1_4Den.Value = "..." Then
        sdbcGMN1_4Den.Text = ""
        Exit Sub
    End If
        
    If sdbcGMN1_4Den.Value = "" Then Exit Sub
    
    GMN1RespetarCombo = True
    sdbcGMN1_4Cod.Text = sdbcGMN1_4Den.Columns(1).Text
    sdbcGMN1_4Den.Text = sdbcGMN1_4Den.Columns(0).Text
    GMN1RespetarCombo = False
    
    GMN1Seleccionado
    
    GMN1CargarComboDesde = False
        
End Sub


Private Sub sdbcGMN4_4Cod_Change()
    
    If Not GMN4RespetarCombo Then
    
        GMN4RespetarCombo = True
        sdbcGMN4_4Den.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub



Private Sub sdbcGMN4_4Cod_CloseUp()

    If sdbcGMN4_4Cod.Value = "..." Then
        sdbcGMN4_4Cod.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Cod.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Den.Text = sdbcGMN4_4Cod.Columns(1).Text
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Cod.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Cod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Cod.RemoveAll

    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, Trim(sdbcGMN3_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(sdbcGMN1_4Cod.Text, sdbcGMN2_4Cod.Text, sdbcGMN3_4Cod.Text, , , , , False)
        End If
    Else
        
        
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Cod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Cod.AddItem "..."
    End If

    sdbcGMN4_4Cod.SelStart = 0
    sdbcGMN4_4Cod.SelLength = Len(sdbcGMN3_4Cod.Text)
    sdbcGMN4_4Cod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcGMN4_4Den_Change()
    
    If Not GMN4RespetarCombo Then

        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = ""
        GMN4RespetarCombo = False
        Set oGMN4Seleccionado = Nothing
        GMN4CargarComboDesde = True
        
    End If
    
End Sub

Private Sub sdbcGMN4_4Den_CloseUp()

    If sdbcGMN4_4Den.Value = "..." Then
        sdbcGMN4_4Den.Text = ""
        Exit Sub
    End If
    
    If sdbcGMN4_4Den.Value = "" Then Exit Sub
    
    GMN4RespetarCombo = True
    sdbcGMN4_4Cod.Text = sdbcGMN4_4Den.Columns(1).Text
    sdbcGMN4_4Den.Text = sdbcGMN4_4Den.Columns(0).Text
    GMN4RespetarCombo = False
    
    GMN4Seleccionado
    
    GMN4CargarComboDesde = False
    
End Sub

Private Sub sdbcGMN4_4Den_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim oIMAsig As IMaterialAsignado
    Dim i As Integer

    sdbcGMN4_4Den.RemoveAll
    
    If oGMN3Seleccionado Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    If bRMat Then
                
        Set oIMAsig = oUsuarioSummit.comprador
        
        Set oGruposMN4 = Nothing
        If GMN4CargarComboDesde Then
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , False)
        Else
            Set oGruposMN4 = oIMAsig.DevolverGruposMN4Visibles(Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), , , , , False)
        End If
    Else
        
     
        If GMN4CargarComboDesde Then
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcGMN4_4Cod), , , False
        Else
            oGMN3Seleccionado.CargarTodosLosGruposMat , , , False
        End If
        Set oGruposMN4 = oGMN3Seleccionado.GruposMatNivel4
        
    End If

    
    Codigos = oGruposMN4.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
   
        sdbcGMN4_4Den.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
   
    Next

    If GMN4CargarComboDesde And Not oGruposMN4.EOF Then
        sdbcGMN4_4Den.AddItem "..."
    End If

    sdbcGMN4_4Den.SelStart = 0
    sdbcGMN4_4Den.SelLength = Len(sdbcGMN4_4Den.Text)
    sdbcGMN4_4Den.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdAceptar_Click()

Dim teserror As TipoErrorSummit

Screen.MousePointer = vbHourglass

teserror.NumError = TESnoerror

  Set oIEquiposAsignados.Equipos = oEqpsSeleccionados
  teserror = oIEquiposAsignados.AsignarEquipos
    
  If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Screen.MousePointer = vbNormal
    Exit Sub
  End If
  
  teserror.NumError = TESnoerror

  Set oIEquiposAsignados.Equipos = Nothing
  Set oIEquiposAsignados.Equipos = oEqpsDesSeleccionados
  teserror = oIEquiposAsignados.DesAsignarEquipos
    
  If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Screen.MousePointer = vbNormal
    Exit Sub
  End If
  
  RegistrarAccion AccionesSummit.ACCEqpPorProveMod, "Cod:" & Trim(sdbcProveCod.Text)
  
  Accion = ACCEqpPorProveCon
  fraselProve.Enabled = True
  lstEqpMod.Visible = False
  lstEqp.Visible = True
  picNavigate.Visible = True
  picEdit.Visible = False
  
  cmdRestaurar_Click
  
End Sub

Private Sub cmdCancelar_Click()
     
  fraselProve.Enabled = True
  lstEqpMod.Visible = False
  lstEqp.Visible = True
  picNavigate.Visible = True
  picEdit.Visible = False
  Accion = ACCEqpPorProveCon
  Set oEqpsSeleccionados = Nothing
  Set oEqpsDesSeleccionados = Nothing
  
End Sub

Private Sub cmdlistado_Click()
    
    If sdbcProveCod.Text = "" And sdbcProveCod.Text = "" Then
        frmLstPROVEEqpPorProve.optTodos.Value = True
    Else
        If Not sdbcProveCod.Text = "" And Not sdbcProveCod.Text = "" Then
            frmLstPROVEEqpPorProve.optProve = True
            frmLstPROVEEqpPorProve.RespetarComboProve = True
            frmLstPROVEEqpPorProve.sdbcProveCod.Text = sdbcProveCod.Text
            frmLstPROVEEqpPorProve.sdbcProveDen.Text = sdbcProveDen.Text
            frmLstPROVEEqpPorProve.RespetarComboProve = False
        End If
    End If
    If frmPROVEEqpPorProve.sdbcGMN1_4Cod <> "" Then
        frmLstPROVEEqpPorProve.GMN1RespetarCombo = True
        frmLstPROVEEqpPorProve.sdbcGMN1_4Cod = sdbcGMN1_4Cod
        frmLstPROVEEqpPorProve.sdbcGMN1_4Den = sdbcGMN1_4Den
        frmLstPROVEEqpPorProve.GMN1RespetarCombo = False
        frmLstPROVEEqpPorProve.GMN1Seleccionado
    End If
    If frmPROVEEqpPorProve.sdbcGMN2_4Cod <> "" Then
        frmLstPROVEEqpPorProve.GMN2RespetarCombo = True
        frmLstPROVEEqpPorProve.sdbcGMN2_4Cod = sdbcGMN2_4Cod
        frmLstPROVEEqpPorProve.sdbcGMN2_4Den = sdbcGMN2_4Den
        frmLstPROVEEqpPorProve.GMN2RespetarCombo = False
        frmLstPROVEEqpPorProve.GMN2Seleccionado
    End If
    If frmPROVEEqpPorProve.sdbcGMN3_4Cod <> "" Then
        frmLstPROVEEqpPorProve.GMN3RespetarCombo = True
        frmLstPROVEEqpPorProve.sdbcGMN3_4Cod = sdbcGMN3_4Cod
        frmLstPROVEEqpPorProve.sdbcGMN3_4Den = sdbcGMN3_4Den
        frmLstPROVEEqpPorProve.GMN3RespetarCombo = False
        frmLstPROVEEqpPorProve.GMN3Seleccionado
    End If
    If frmPROVEEqpPorProve.sdbcGMN4_4Cod <> "" Then
        frmLstPROVEEqpPorProve.GMN4RespetarCombo = True
        frmLstPROVEEqpPorProve.sdbcGMN4_4Cod = sdbcGMN4_4Cod
        frmLstPROVEEqpPorProve.sdbcGMN4_4Den = sdbcGMN4_4Den
        frmLstPROVEEqpPorProve.GMN4RespetarCombo = False
        frmLstPROVEEqpPorProve.GMN4Seleccionado
    End If
    
    frmLstPROVEEqpPorProve.stabGeneral.Tab = 1
    frmLstPROVEEqpPorProve.Show vbModal
End Sub

Private Sub cmdModificar_Click()

    Screen.MousePointer = vbHourglass
    
    Accion = ACCEqpPorProveMod
    CargarEquiposAsignables
    fraselProve.Enabled = False
    picNavigate.Visible = False
    picEdit.Visible = True
    lstEqp.Visible = False
    lstEqpMod.Visible = True
    
    Set oEqpsDesSeleccionados = oFSGSRaiz.Generar_CEquipos
    Set oEqpsSeleccionados = oFSGSRaiz.Generar_CEquipos
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdRestaurar_Click()
    
    ProveedorSeleccionado
    
End Sub


Private Sub cmdBuscar_Click()
    
    frmPROVEBuscar.sOrigen = "EqpPorProve"
    frmPROVEBuscar.CodGMN1 = Trim(sdbcGMN1_4Cod)
    frmPROVEBuscar.CodGMN2 = Trim(sdbcGMN2_4Cod)
    frmPROVEBuscar.CodGMN3 = Trim(sdbcGMN3_4Cod)
    frmPROVEBuscar.codGMN4 = Trim(sdbcGMN4_4Cod)
    If Not oEqpSeleccionado Is Nothing Then
        frmPROVEBuscar.codEqp = oEqpSeleccionado.Cod
    End If
    
    frmPROVEBuscar.Hide
    frmPROVEBuscar.Show 1
    If Me.Visible Then lstEqp.SetFocus

End Sub

Private Sub cmdSelMat_Click()
     
    frmSELMAT.sOrigen = "frmPROVEEqpPorProve"
    frmSELMAT.bRComprador = bRMat
    frmSELMAT.Hide
    frmSELMAT.Show 1
    
End Sub

Private Sub Form_Activate()
    
    Unload frmSELMAT
    Unload frmPROVEBuscar
    
End Sub
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROVE_EQPPORPROVE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        stabGeneral.TabCaption(0) = Ador(0).Value '1
        Ador.MoveNext
        stabGeneral.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(0).caption = Ador(0).Value '3 Código
        sdbcGMN1_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(1).caption = Ador(0).Value
        sdbcProveCod.Columns(0).caption = Ador(0).Value
        sdbcProveDen.Columns(1).caption = Ador(0).Value
        lstEqp.ColumnHeaders(0).Text = Ador(0).Value
        lblProveCod.caption = Ador(0).Value & ":"
        Ador.MoveNext
        sdbcGMN1_4Cod.Columns(1).caption = Ador(0).Value '4 Denominación
        sdbcGMN1_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN2_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN2_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN3_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN3_4Den.Columns(0).caption = Ador(0).Value
        sdbcGMN4_4Cod.Columns(1).caption = Ador(0).Value
        sdbcGMN4_4Den.Columns(0).caption = Ador(0).Value
        sdbcProveCod.Columns(1).caption = Ador(0).Value
        sdbcProveDen.Columns(0).caption = Ador(0).Value
        lstEqp.ColumnHeaders(1).Text = Ador(0).Value
        Ador.MoveNext
        cmdAceptar.caption = Ador(0).Value '5
        Ador.MoveNext
        cmdCancelar.caption = Ador(0).Value
        Ador.MoveNext
        cmdListado.caption = Ador(0).Value
        Ador.MoveNext
        cmdModificar.caption = Ador(0).Value
        Ador.MoveNext
        cmdRestaurar.caption = Ador(0).Value
        Ador.MoveNext
        frmPROVEEqpPorProve.caption = Ador(0).Value
        
        Ador.Close
        
    End If
    
    Set Ador = Nothing
    
End Sub

Private Sub Form_Load()

iAltura = 5925
iAnchura = 6960

Me.Height = 5925
Me.Width = 6960
If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
    Me.Top = 0
    Me.Left = 0
End If

CargarRecursos

    PonerFieldSeparator Me

Accion = ACCEqpPorProveCon

cmdModificar.Enabled = False
cmdRestaurar.Enabled = False

ConfigurarNombres

ConfigurarSeguridad

Set oProves = oFSGSRaiz.generar_CProveedores


Set oEqps = oFSGSRaiz.Generar_CEquipos


        
End Sub


Private Sub Form_Resize()

    Arrange
    
End Sub

Private Sub sdbcGMN4_4Den_Validate(Cancel As Boolean)

    Dim oGMN4s As CGruposMatNivel4
    Dim oIMAsig As IMaterialAsignado
    Dim bExiste As Boolean
    
    
    Set oGMN4s = oFSGSRaiz.Generar_CGruposMatNivel4
    
    If stabGeneral.Tab = 1 Then Exit Sub
    If sdbcGMN4_4Den.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN3Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        If bRMat Then
            Set oIMAsig = oUsuarioSummit.comprador
          
            Set oGMN4s = oIMAsig.DevolverGruposMN4Visiblesdesde(gParametrosInstalacion.giCargaMaximaCombos, sdbcGMN1_4Cod, sdbcGMN2_4Cod, sdbcGMN3_4Cod.Text, , Trim(sdbcGMN4_4Den), , True, False)
        Else
           
            oGMN3Seleccionado.CargarTodosLosGruposMatDesde gParametrosInstalacion.giCargaMaximaCombos, , sdbcGMN4_4Den.Text, True, , False
            Set oGMN4s = oGMN3Seleccionado.GruposMatNivel4
        End If
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4s.Count = 0)
        
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcGMN4_4Den.Text = ""
    Else
        GMN4RespetarCombo = True
        sdbcGMN4_4Cod.Text = oGMN4s.Item(1).Cod
        
        sdbcGMN4_4Den.Columns(0).Value = sdbcGMN4_4Den.Text
        sdbcGMN4_4Den.Columns(1).Value = sdbcGMN4_4Cod.Text
                    
        GMN4RespetarCombo = False
        GMN4Seleccionado
        GMN4CargarComboDesde = False
    End If
    
    Set oGMN4s = Nothing

End Sub

Private Sub sdbcProveCod_Click()
     
     If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveCod.Text = "" Then Exit Sub
    
    If sdbcProveCod.Text = sdbcProveCod.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    
    If sdbcProveCod.Text = sdbcProveDen.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proveedor
    Screen.MousePointer = vbHourglass
    oProves.BuscarTodosLosProveedoresDesde 1, , Trim(sdbcProveCod.Text), , , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveDen.Text = oProves.Item(1).Den
        
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        
        RespetarComboProve = False
        ProveedorSeleccionado
    End If

End Sub

Private Sub sdbcProveDen_Click()
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod = ""
        sdbcProveDen = ""
    End If
End Sub

Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub

Private Sub sdbcProveDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcProveDen.Text = "" Then Exit Sub
    
    If sdbcProveDen.Text = sdbcProveDen.Columns(0).Text Then
        RespetarComboProve = True
        sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    
    If sdbcProveDen.Text = sdbcProveCod.Columns(1).Text Then
        RespetarComboProve = True
        sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
        RespetarComboProve = False
        ProveedorSeleccionado
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el proveedor
    Screen.MousePointer = vbHourglass
    oProves.BuscarTodosLosProveedoresDesde 1, , , Trim(sdbcProveDen.Text), , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , True, , , , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oProves.Count = 0)
    If bExiste Then bExiste = (UCase(oProves.Item(1).Den) = UCase(sdbcProveDen.Text))
    If Not bExiste Then
        sdbcProveDen.Text = ""
    Else
        RespetarComboProve = True
        sdbcProveCod.Text = oProves.Item(1).Cod
        
        sdbcProveDen.Columns(0).Value = sdbcProveDen.Text
        sdbcProveDen.Columns(1).Value = sdbcProveCod.Text
        
        RespetarComboProve = False
        ProveedorSeleccionado
    End If

End Sub

Private Sub stabGeneral_Click(PreviousTab As Integer)
Dim oICompAsignado As ICompProveAsignados
Dim iNivelAsig As Integer

If stabGeneral.Tab = 1 Then
    
    If bRMat Then
            If oGMN4Seleccionado Is Nothing And oGMN3Seleccionado Is Nothing And oGMN2Seleccionado Is Nothing And oGMN1Seleccionado Is Nothing Then
                stabGeneral.Tab = 0
                Exit Sub
            End If
            
            If Not oGMN4Seleccionado Is Nothing Then Exit Sub
            
            Screen.MousePointer = vbHourglass
            
            If Not oGMN3Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN3Seleccionado
                
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig = 0 Or iNivelAsig > 3 Then
                    stabGeneral.Tab = 0
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            End If
                
            If Not oGMN2Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN2Seleccionado
              
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig = 0 Or iNivelAsig > 2 Then
                    stabGeneral.Tab = 0
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                Exit Sub
            End If
            
            If Not oGMN1Seleccionado Is Nothing Then
                Set oICompAsignado = oGMN1Seleccionado
               
                iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                If iNivelAsig < 1 Or iNivelAsig > 2 Then
                    stabGeneral.Tab = 0
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                Exit Sub
            End If
            
        End If
Else
            
        If Accion = AccionesSummit.ACCEqpPorProveMod Then
            stabGeneral.Tab = 1
        Else
            lstEqp.ListItems.clear
            sdbcProveCod.Text = ""
            sdbcProveDen.Text = ""
            Set oProveSeleccionado = Nothing
        End If
        
        
End If

Screen.MousePointer = vbNormal
End Sub
Private Sub Form_Unload(Cancel As Integer)

'Unload frmSELMaterial

Set oEqps = Nothing
Set oEqpsSeleccionados = Nothing
Set oProves = Nothing
Set oProveSeleccionado = Nothing
Set oGMN1Seleccionado = Nothing
Set oGMN2Seleccionado = Nothing
Set oGMN3Seleccionado = Nothing
Set oGMN4Seleccionado = Nothing
Set oGruposMN1 = Nothing
Set oGruposMN2 = Nothing
Set oGruposMN3 = Nothing
Set oGruposMN4 = Nothing
Me.Visible = False

End Sub





Private Sub ConfigurarSeguridad()

If basOptimizacion.gTipoDeUsuario <> Administrador Then

    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.EQPPorPROVEModificar)) Is Nothing) Then
        bModif = True
        cmdModificar.Visible = True
    Else
        cmdModificar.Visible = False
        cmdListado.Left = cmdRestaurar.Left
        'cmdBuscar.Left = cmdRestaurar.Left
        cmdRestaurar.Left = cmdModificar.Left
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.EQPPorPROVERestEquipo)) Is Nothing) Then
        bREqp = True
    End If
    
    If basOptimizacion.gTipoDeUsuario = comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.EQPPorPROVERestMatComp)) Is Nothing) Then
        bRMat = True
    End If
    
Else
    
    bModif = True
    cmdModificar.Visible = True
    
End If

cmdListado.Visible = True

End Sub




Private Sub lstEqpMod_ItemCheck(ByVal Item As MSComctlLib.listItem)
If Item.Checked Then
    
    If oEqps.Item(Mid(Item.key, 4, Len(Item.key) - 3)) Is Nothing Then
        oEqpsSeleccionados.Add Mid(Item.key, 4, Len(Item.key) - 3), Item.Tag
    End If
    oEqpsDesSeleccionados.Remove Mid(Item.key, 4, Len(Item.key) - 3)
Else
    If Not oEqps.Item(Mid(Item.key, 4, Len(Item.key) - 3)) Is Nothing Then
        oEqpsDesSeleccionados.Add Mid(Item.key, 4, Len(Item.key) - 3), Item.Tag
    End If
    oEqpsSeleccionados.Remove Mid(Item.key, 4, Len(Item.key) - 3)
End If

End Sub

Private Sub sdbcProveCod_Change()

    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveDen.Text = ""
        Set oProveSeleccionado = Nothing
        lstEqp.ListItems.clear
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        RespetarComboProve = False
               
    End If
    
End Sub

Private Sub sdbcProveDen_Change()
    
    If Not RespetarComboProve Then
    
        RespetarComboProve = True
        sdbcProveCod.Text = ""
        RespetarComboProve = False
        
        Set oProveSeleccionado = Nothing
        lstEqp.ListItems.clear
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
        
    End If
    
End Sub



Private Sub sdbcProveCod_CloseUp()
    
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
     
    If sdbcProveCod.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
            
End Sub

Private Sub sdbcProveCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbcProveCod.RemoveAll

    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod.Text), Trim(sdbcProveDen), , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , , , , , False

    Codigos = oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If

    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveCod_InitColumnProps()
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcProveDen_CloseUp()

    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    RespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    RespetarComboProve = False
    
    DoEvents
    
    ProveedorSeleccionado
    
End Sub

Private Sub sdbcProveDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer

    Screen.MousePointer = vbHourglass
    sdbcProveDen.RemoveAll
    
    oProves.BuscarTodosLosProveedoresDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcProveCod), Trim(sdbcProveDen), , , , Trim(sdbcGMN1_4Cod), Trim(sdbcGMN2_4Cod), Trim(sdbcGMN3_4Cod), Trim(sdbcGMN4_4Cod), , , , True, , , , False

    Codigos = oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
       
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
       
    Next

    If Not oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If
    
    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProveDen_InitColumnProps()
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
End Sub
Private Sub ProveedorSeleccionado()
    
    Screen.MousePointer = vbHourglass
    
    Set oProveSeleccionado = Nothing
    Set oProveSeleccionado = oProves.Item(sdbcProveCod.Text)
    
    If Not oProveSeleccionado Is Nothing Then
     
        Set oIEquiposAsignados = oProveSeleccionado
        CargarEquiposDeCompra
        cmdModificar.Enabled = True
        cmdRestaurar.Enabled = True
    Else
        cmdModificar.Enabled = False
        cmdRestaurar.Enabled = False
    End If
    
    Screen.MousePointer = vbNormal
End Sub


Private Sub CargarEquiposDeCompra()
Dim oeqp As CEquipo
Dim Item As MSComctlLib.listItem

lstEqp.ListItems.clear

    Set oEqps = oIEquiposAsignados.DevolverEquiposAsignados

If bREqp Then
    For Each oeqp In oEqps
        If oeqp.Cod = basOptimizacion.gCodEqpUsuario Then
            Set Item = lstEqp.ListItems.Add(, "Cod" & CStr(oeqp.Cod), CStr(oeqp.Cod) & " " & CStr(oeqp.Den), "Equipo", "Equipo")
            Item.Tag = oeqp.Den
        End If
    Next
Else
    For Each oeqp In oEqps
        Set Item = lstEqp.ListItems.Add(, "Cod" & CStr(oeqp.Cod), CStr(oeqp.Cod) & " " & CStr(oeqp.Den), "Equipo", "Equipo")
        Item.Tag = oeqp.Den
    Next
End If

End Sub

Private Sub CargarEquiposAsignables()
Dim oeqp As CEquipo
Dim oEqpsAsig As CEquipos
Dim iNumEqps As Integer

Screen.MousePointer = vbHourglass

lstEqpMod.ListItems.clear

Set oEqpsAsig = oFSGSRaiz.Generar_CEquipos

  
If bREqp Then
    oEqpsAsig.Add basOptimizacion.gCodEqpUsuario, oUsuarioSummit.comprador.DenEqp
Else
    oEqpsAsig.CargarTodosLosEquipos , , , , False
End If

    For Each oeqp In oEqpsAsig
        lstEqpMod.ListItems.Add , "Cod" & CStr(oeqp.Cod), CStr(oeqp.Cod) & " " & CStr(oeqp.Den), "Equipo", "Equipo"
        lstEqpMod.Tag = CStr(oeqp.Cod)
    Next

Set oEqpsAsig = Nothing

    For iNumEqps = 1 To lstEqpMod.ListItems.Count
    
    If Not (oEqps.Item(Mid(lstEqpMod.ListItems(iNumEqps).key, 4, Len(lstEqpMod.ListItems(iNumEqps).key) - 3)) Is Nothing) Then
        lstEqpMod.ListItems.Item(iNumEqps).Checked = True
    End If
    
    Next
    
Screen.MousePointer = vbNormal
    
End Sub
Public Sub CargarProveedorConBusqueda()

    Set oProves = Nothing
    Set oProves = frmPROVEBuscar.oProveEncontrados
   
    Set frmPROVEBuscar.oProveEncontrados = Nothing
    sdbcProveCod = oProves.Item(1).Cod
    sdbcProveCod_Validate False
    ProveedorSeleccionado
    
End Sub
Private Sub GMN1Seleccionado()
    
    sdbcGMN2_4Cod.RemoveAll
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN2_4Den.RemoveAll
    sdbcGMN2_4Cod.Text = ""
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    sdbcGMN2_4Den.Text = ""
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN1Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel1
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    oGMN1Seleccionado.Cod = sdbcGMN1_4Cod
    
    stabGeneral.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
    
    
End Sub

Private Sub GMN2Seleccionado()
    
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN3_4Cod.RemoveAll
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN3_4Den.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN3_4Cod.Text = ""
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN3_4Den.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    
    Set oGMN2Seleccionado = Nothing
    Set oGMN2Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel2
    oGMN2Seleccionado.Cod = sdbcGMN2_4Cod
    oGMN2Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    
    stabGeneral.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
End Sub
Private Sub GMN3Seleccionado()
    

    Set oGMN3Seleccionado = Nothing
    Set oGMN3Seleccionado = oFSGSRaiz.generar_CGrupoMatNivel3
    Set oGMN4Seleccionado = Nothing
    
    sdbcGMN4_4Cod.RemoveAll
    sdbcGMN4_4Den.RemoveAll
    sdbcGMN4_4Cod.Text = ""
    sdbcGMN4_4Den.Text = ""
    
    oGMN3Seleccionado.Cod = sdbcGMN3_4Cod
    oGMN3Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN3Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    
    stabGeneral.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
End Sub
Private Sub GMN4Seleccionado()
    

    Set oGMN4Seleccionado = Nothing
    Set oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGMN4Seleccionado.Cod = sdbcGMN4_4Cod
    oGMN4Seleccionado.GMN1Cod = sdbcGMN1_4Cod
    oGMN4Seleccionado.GMN2Cod = sdbcGMN2_4Cod
    oGMN4Seleccionado.GMN3Cod = sdbcGMN3_4Cod
    
    stabGeneral.Enabled = True
    If bModif Then
        cmdModificar.Enabled = True
    End If
    cmdRestaurar.Enabled = True
End Sub
Private Sub ConfigurarNombres()

    lblGMN1_4.caption = gParametrosGenerales.gsDEN_GMN1 & ":"
    lblGMN2_4.caption = gParametrosGenerales.gsDEN_GMN2 & ":"
    lblGMN3_4.caption = gParametrosGenerales.gsDEN_GMN3 & ":"
    lblGMN4_4.caption = gParametrosGenerales.gsDEN_GMN4 & ":"

End Sub

Public Sub PonerMatSeleccionadoEnCombos()
    
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sdbcGMN1_4Cod.Text = oGMN1Seleccionado.Cod
        sdbcGMN1_4Cod_Validate False
    Else
        sdbcGMN1_4Cod.Text = ""
        
    End If
    
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    
    If Not oGMN2Seleccionado Is Nothing Then
        sdbcGMN2_4Cod.Text = oGMN2Seleccionado.Cod
        sdbcGMN2_4Cod_Validate False
    Else
        sdbcGMN2_4Cod.Text = ""
        
    End If
    
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    
    If Not oGMN3Seleccionado Is Nothing Then
        sdbcGMN3_4Cod.Text = oGMN3Seleccionado.Cod
        sdbcGMN3_4Cod_Validate False
    Else
        sdbcGMN3_4Cod.Text = ""
        
    End If
    
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN4Seleccionado Is Nothing Then
        sdbcGMN4_4Cod.Text = oGMN4Seleccionado.Cod
        sdbcGMN4_4Cod_Validate False
    Else
        sdbcGMN4_4Cod.Text = ""
        
    End If
    
           
End Sub

