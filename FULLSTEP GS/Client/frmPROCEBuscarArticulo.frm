VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPROCEBuscarArticulo 
   BackColor       =   &H00808000&
   Caption         =   "Buscar Art�culo"
   ClientHeight    =   9750
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8940
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPROCEBuscarArticulo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9750
   ScaleWidth      =   8940
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picDatosPedido 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   855
      Left            =   0
      ScaleHeight     =   855
      ScaleWidth      =   8895
      TabIndex        =   50
      Top             =   0
      Width           =   8895
      Begin VB.Label lblPedido 
         BackStyle       =   0  'Transparent
         Caption         =   "DPedido"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   135
         Width           =   1395
      End
      Begin VB.Label lblDPedido 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1200
         TabIndex        =   57
         Top             =   120
         Width           =   2010
      End
      Begin VB.Label lblEmpresa 
         BackStyle       =   0  'Transparent
         Caption         =   "DEmpresa"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   4800
         TabIndex        =   56
         Top             =   120
         Width           =   1395
      End
      Begin VB.Label lblDEmpresa 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   5880
         TabIndex        =   55
         Top             =   120
         Width           =   2850
      End
      Begin VB.Label lblProve 
         BackStyle       =   0  'Transparent
         Caption         =   "DProveedor"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   120
         TabIndex        =   54
         Top             =   495
         Width           =   1395
      End
      Begin VB.Label lblDProve 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   1200
         TabIndex        =   53
         Top             =   480
         Width           =   3450
      End
      Begin VB.Label lblOrgCompra 
         BackStyle       =   0  'Transparent
         Caption         =   "DOrgCompra"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   4800
         TabIndex        =   52
         Top             =   495
         Width           =   1395
      End
      Begin VB.Label lblDOrgCompra 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   5880
         TabIndex        =   51
         Top             =   480
         Width           =   2850
      End
   End
   Begin TabDlg.SSTab sstabArticulos 
      Height          =   8655
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   15266
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BackColor       =   8421376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Datos de busqueda"
      TabPicture(0)   =   "frmPROCEBuscarArticulo.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraDatosBusqueda"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Art�culos"
      TabPicture(1)   =   "frmPROCEBuscarArticulo.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "sdbgArticulos"
      Tab(1).Control(1)=   "cmdCerrar"
      Tab(1).Control(2)=   "cmdSeleccionar"
      Tab(1).ControlCount=   3
      Begin VB.Frame fraDatosBusqueda 
         Height          =   6615
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   8415
         Begin VB.ListBox lstMultiUon 
            BackColor       =   &H80000018&
            Height          =   255
            ItemData        =   "frmPROCEBuscarArticulo.frx":0182
            Left            =   1680
            List            =   "frmPROCEBuscarArticulo.frx":0189
            TabIndex        =   48
            Top             =   600
            Visible         =   0   'False
            Width           =   5760
         End
         Begin VB.CommandButton cmdSelUon 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7890
            Picture         =   "frmPROCEBuscarArticulo.frx":019A
            Style           =   1  'Graphical
            TabIndex        =   45
            TabStop         =   0   'False
            Top             =   600
            Width           =   345
         End
         Begin VB.CommandButton cmdBorrarUon 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7515
            Picture         =   "frmPROCEBuscarArticulo.frx":0206
            Style           =   1  'Graphical
            TabIndex        =   44
            TabStop         =   0   'False
            Top             =   600
            Width           =   345
         End
         Begin VB.CheckBox chkBuscarCentrales 
            Caption         =   "Buscar s�lo art�culos centrales"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   4200
            TabIndex        =   43
            Top             =   1680
            Width           =   4170
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddOper 
            Height          =   915
            Left            =   1560
            TabIndex        =   42
            Top             =   3000
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROCEBuscarArticulo.frx":02AB
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
         End
         Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
            Height          =   915
            Left            =   3720
            TabIndex        =   41
            Top             =   3000
            Width           =   2025
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ColumnHeaders   =   0   'False
            stylesets.count =   1
            stylesets(0).Name=   "Normal"
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROCEBuscarArticulo.frx":02C7
            DividerStyle    =   3
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   370
            ExtraHeight     =   53
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Name =   "VALOR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESC"
            Columns(1).Name =   "DESC"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3572
            _ExtentY        =   1614
            _StockProps     =   77
         End
         Begin VB.CommandButton cmdBuscaAtrib 
            Height          =   285
            Left            =   7920
            Picture         =   "frmPROCEBuscarArticulo.frx":02E3
            Style           =   1  'Graphical
            TabIndex        =   32
            TabStop         =   0   'False
            Top             =   2160
            Width           =   315
         End
         Begin VB.Frame fraConcepto 
            BorderStyle     =   0  'None
            Caption         =   "DConcepto"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   240
            TabIndex        =   24
            Top             =   5160
            Width           =   2115
            Begin VB.CheckBox chkConcepto 
               Caption         =   "DGasto/Inversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   27
               Top             =   990
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "DInversi�n"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   26
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkConcepto 
               Caption         =   "DGasto"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   25
               Top             =   390
               Width           =   1935
            End
            Begin VB.Label lblConcepto 
               Caption         =   "DConcepto"
               Height          =   255
               Left            =   120
               TabIndex        =   28
               Top             =   0
               Width           =   975
            End
         End
         Begin VB.Frame fraAlmacen 
            BorderStyle     =   0  'None
            Caption         =   "DAlmacenamiento"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   2880
            TabIndex        =   20
            Top             =   5160
            Width           =   2115
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "DOpcional"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   23
               Top             =   990
               Width           =   1875
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "DObligatorio"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   22
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkAlmacen 
               Caption         =   "DNo almacenar"
               ForeColor       =   &H00000000&
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   21
               Top             =   390
               Width           =   1905
            End
            Begin VB.Label lblAlmacenamiento 
               Caption         =   "DAlmacenamiento"
               Height          =   375
               Left            =   120
               TabIndex        =   29
               Top             =   0
               Width           =   1575
            End
         End
         Begin VB.Frame fraRecepcion 
            BorderStyle     =   0  'None
            Caption         =   "DRecepci�n"
            ForeColor       =   &H00000000&
            Height          =   1245
            Left            =   5760
            TabIndex        =   16
            Top             =   5160
            Width           =   2115
            Begin VB.CheckBox chkRecep 
               Caption         =   "DOpcional"
               ForeColor       =   &H00000000&
               Height          =   285
               Index           =   2
               Left            =   120
               TabIndex        =   19
               Top             =   990
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "DObligatorio"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   18
               Top             =   690
               Width           =   1935
            End
            Begin VB.CheckBox chkRecep 
               Caption         =   "DNo recepcionar"
               ForeColor       =   &H00000000&
               Height          =   255
               Index           =   1
               Left            =   120
               TabIndex        =   17
               Top             =   390
               Width           =   1935
            End
            Begin VB.Label lblRecepcion 
               Caption         =   "DRecepci�n"
               Height          =   255
               Left            =   120
               TabIndex        =   30
               Top             =   0
               Width           =   1215
            End
         End
         Begin VB.CommandButton cmdBorrar 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7520
            Picture         =   "frmPROCEBuscarArticulo.frx":0370
            Style           =   1  'Graphical
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   240
            Width           =   345
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   7890
            Picture         =   "frmPROCEBuscarArticulo.frx":0415
            Style           =   1  'Graphical
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   240
            Width           =   345
         End
         Begin VB.TextBox txtDen 
            Height          =   315
            Left            =   4320
            TabIndex        =   9
            Top             =   1080
            Width           =   3630
         End
         Begin VB.TextBox txtCod 
            Height          =   315
            Left            =   1440
            TabIndex        =   8
            Top             =   1095
            Width           =   1605
         End
         Begin VB.CommandButton cmdAyuda 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   7.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   8010
            Picture         =   "frmPROCEBuscarArticulo.frx":0481
            Style           =   1  'Graphical
            TabIndex        =   7
            Top             =   1140
            Width           =   225
         End
         Begin VB.CheckBox chkBuscarGenericos 
            Caption         =   "Buscar s�lo art�culos genericos"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   1680
            Width           =   4170
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgAtributos 
            Height          =   2535
            Left            =   120
            TabIndex        =   33
            Top             =   2520
            Width           =   8145
            ScrollBars      =   2
            _Version        =   196617
            DataMode        =   2
            RecordSelectors =   0   'False
            Col.Count       =   13
            stylesets.count =   3
            stylesets(0).Name=   "Yellow"
            stylesets(0).BackColor=   11862015
            stylesets(0).HasFont=   -1  'True
            BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(0).Picture=   "frmPROCEBuscarArticulo.frx":06B2
            stylesets(1).Name=   "Normal"
            stylesets(1).ForeColor=   0
            stylesets(1).BackColor=   16777215
            stylesets(1).HasFont=   -1  'True
            BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(1).Picture=   "frmPROCEBuscarArticulo.frx":06CE
            stylesets(2).Name=   "Header"
            stylesets(2).HasFont=   -1  'True
            BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets(2).Picture=   "frmPROCEBuscarArticulo.frx":06EA
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   0
            HeadStyleSet    =   "Header"
            StyleSet        =   "Normal"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   13
            Columns(0).Width=   979
            Columns(0).Name =   "USAR"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Style=   2
            Columns(1).Width=   3201
            Columns(1).Caption=   "COD"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(2).Width=   4419
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Locked=   -1  'True
            Columns(3).Width=   1058
            Columns(3).Caption=   "OPER"
            Columns(3).Name =   "OPER"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3889
            Columns(4).Caption=   "VALOR"
            Columns(4).Name =   "VALOR"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "GRUPO"
            Columns(5).Name =   "GRUPO"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "INTRO"
            Columns(6).Name =   "INTRO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "IDTIPO"
            Columns(7).Name =   "IDTIPO"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "ID_ATRIB"
            Columns(8).Name =   "ID_ATRIB"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "MAXIMO"
            Columns(9).Name =   "MAXIMO"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "MINIMO"
            Columns(10).Name=   "MINIMO"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            Columns(11).Width=   3200
            Columns(11).Visible=   0   'False
            Columns(11).Caption=   "VALOR_ATRIB"
            Columns(11).Name=   "VALOR_ATRIB"
            Columns(11).DataField=   "Column 11"
            Columns(11).DataType=   8
            Columns(11).FieldLen=   256
            Columns(12).Width=   3200
            Columns(12).Visible=   0   'False
            Columns(12).Caption=   "PROCE"
            Columns(12).Name=   "PROCE"
            Columns(12).DataField=   "Column 12"
            Columns(12).DataType=   8
            Columns(12).FieldLen=   256
            _ExtentX        =   14367
            _ExtentY        =   4471
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblUon 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1680
            TabIndex        =   47
            Top             =   600
            Width           =   5760
         End
         Begin VB.Label lblUniOrg 
            BackStyle       =   0  'Transparent
            Caption         =   "DUnidad organizativa"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   46
            Top             =   645
            Width           =   1530
         End
         Begin VB.Label lblBusqAtrib 
            Caption         =   "B�squeda por atributos"
            Height          =   255
            Left            =   120
            TabIndex        =   31
            Top             =   2280
            Width           =   1935
         End
         Begin VB.Label lblMat 
            BackStyle       =   0  'Transparent
            Caption         =   "Material:"
            ForeColor       =   &H00000000&
            Height          =   240
            Left            =   120
            TabIndex        =   15
            Top             =   285
            Width           =   1050
         End
         Begin VB.Label lblMaterial 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Height          =   315
            Left            =   1680
            TabIndex        =   14
            Top             =   240
            Width           =   5760
         End
         Begin VB.Label lblDen 
            Caption         =   "Denominaci�n:"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   3120
            TabIndex        =   11
            Top             =   1155
            Width           =   1470
         End
         Begin VB.Label lblCod 
            Caption         =   "C�digo:"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   120
            TabIndex        =   10
            Top             =   1155
            Width           =   885
         End
      End
      Begin VB.CommandButton cmdSeleccionar 
         Caption         =   "&Seleccionar"
         Height          =   315
         Left            =   -71970
         TabIndex        =   4
         Top             =   5640
         Width           =   1125
      End
      Begin VB.CommandButton cmdCerrar 
         Cancel          =   -1  'True
         Caption         =   "&Cerrar"
         Height          =   315
         Left            =   -70575
         TabIndex        =   3
         Top             =   5640
         Width           =   1125
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgProveedores 
         Height          =   2805
         Left            =   -74880
         TabIndex        =   1
         Top             =   480
         Width           =   7320
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   6
         stylesets.count =   2
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROCEBuscarArticulo.frx":0706
         stylesets(1).Name=   "Selected"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPROCEBuscarArticulo.frx":0722
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   1773
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3969
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1111
         Columns(2).Caption=   "CAL1"
         Columns(2).Name =   "CAL1"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1111
         Columns(3).Caption=   "CAL2"
         Columns(3).Name =   "CAL2"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1111
         Columns(4).Caption=   "CAL3"
         Columns(4).Name =   "CAL3"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3201
         Columns(5).Caption=   "C�d.web"
         Columns(5).Name =   "CodWeb"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   12912
         _ExtentY        =   4948
         _StockProps     =   79
         BackColor       =   12632256
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgArticulos 
         Height          =   4935
         Left            =   -74880
         TabIndex        =   2
         Top             =   480
         Width           =   8415
         ScrollBars      =   3
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   13
         stylesets.count =   3
         stylesets(0).Name=   "Normal"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16777215
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROCEBuscarArticulo.frx":073E
         stylesets(1).Name=   "ActiveRowBlue"
         stylesets(1).ForeColor=   16777215
         stylesets(1).BackColor=   8388608
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPROCEBuscarArticulo.frx":075A
         stylesets(1).AlignmentPicture=   0
         stylesets(2).Name=   "adjudica"
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmPROCEBuscarArticulo.frx":0776
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeRow   =   0
         StyleSet        =   "Normal"
         BackColorOdd    =   16777215
         RowHeight       =   423
         ActiveRowStyleSet=   "ActiveRowBlue"
         Columns.Count   =   13
         Columns(0).Width=   1085
         Columns(0).Caption=   "GMNA"
         Columns(0).Name =   "GMNA"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).HasForeColor=   -1  'True
         Columns(1).Width=   953
         Columns(1).Caption=   "FAM"
         Columns(1).Name =   "FAM"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).HasForeColor=   -1  'True
         Columns(2).Width=   1138
         Columns(2).Caption=   "SFA"
         Columns(2).Name =   "SFA"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasForeColor=   -1  'True
         Columns(3).Width=   1244
         Columns(3).Caption=   "SFA4"
         Columns(3).Name =   "SFA4"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).HasForeColor=   -1  'True
         Columns(4).Width=   2064
         Columns(4).Caption=   "ARTICULO"
         Columns(4).Name =   "ARTICULO"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HasForeColor=   -1  'True
         Columns(5).Width=   2805
         Columns(5).Caption=   "DENOMINACION"
         Columns(5).Name =   "DENOMINACION"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).HasForeColor=   -1  'True
         Columns(6).Width=   1640
         Columns(6).Caption=   "CANTIDAD"
         Columns(6).Name =   "CANT"
         Columns(6).Alignment=   1
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).NumberFormat=   "Standard"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "GENERICO"
         Columns(7).Name =   "GENERICO"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "UNI"
         Columns(8).Name =   "UNI"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   2434
         Columns(9).Caption=   "UON_DEN"
         Columns(9).Name =   "UON_DEN"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).Style=   1
         Columns(10).Width=   1799
         Columns(10).Caption=   "CENTRAL"
         Columns(10).Name=   "CENTRAL"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   1826
         Columns(11).Caption=   "AGREGADO"
         Columns(11).Name=   "AGREGADO"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         Columns(11).Style=   4
         Columns(11).ButtonsAlways=   -1  'True
         Columns(11).StyleSet=   "adjudica"
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "TIPORECEP"
         Columns(12).Name=   "TIPORECEP"
         Columns(12).DataField=   "Column 12"
         Columns(12).DataType=   8
         Columns(12).FieldLen=   256
         _ExtentX        =   14843
         _ExtentY        =   8705
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraDatosArticulo 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   3135
      Left            =   120
      TabIndex        =   34
      Top             =   960
      Width           =   7935
      Begin VB.TextBox txtDACod 
         Height          =   315
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   375
         Width           =   1605
      End
      Begin VB.TextBox txtDADen 
         Height          =   315
         Left            =   3930
         Locked          =   -1  'True
         TabIndex        =   35
         Top             =   360
         Width           =   3870
      End
      Begin SSDataWidgets_B.SSDBGrid ssdbArticulosAgregados 
         Height          =   2055
         Left            =   0
         TabIndex        =   49
         Top             =   960
         Width           =   7665
         _Version        =   196617
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   3
         stylesets.count =   4
         stylesets(0).Name=   "Gris"
         stylesets(0).BackColor=   -2147483633
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmPROCEBuscarArticulo.frx":0AF8
         stylesets(1).Name=   "Yellow"
         stylesets(1).BackColor=   11862015
         stylesets(1).HasFont=   -1  'True
         BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(1).Picture=   "frmPROCEBuscarArticulo.frx":0B14
         stylesets(2).Name=   "Normal"
         stylesets(2).ForeColor=   0
         stylesets(2).BackColor=   16777215
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "frmPROCEBuscarArticulo.frx":0B30
         stylesets(3).Name=   "Header"
         stylesets(3).HasFont=   -1  'True
         BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(3).Picture=   "frmPROCEBuscarArticulo.frx":0B4C
         AllowDelete     =   -1  'True
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         HeadStyleSet    =   "Header"
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "UON"
         Columns(0).Name =   "UON"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   3200
         Columns(1).Caption=   "COD"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "UONKEY"
         Columns(2).Name =   "UONKEY"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   13520
         _ExtentY        =   3625
         _StockProps     =   79
         BackColor       =   -2147483633
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblDACod 
         BackColor       =   &H00808000&
         Caption         =   "C�digo:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   0
         TabIndex        =   40
         Top             =   435
         Width           =   885
      End
      Begin VB.Label lblDADen 
         BackColor       =   &H00808000&
         Caption         =   "Denominaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   2760
         TabIndex        =   39
         Top             =   435
         Width           =   1470
      End
      Begin VB.Label lblDAMaterial 
         BackColor       =   &H80000018&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   960
         TabIndex        =   38
         Top             =   0
         Width           =   6840
      End
      Begin VB.Label lblDAMat 
         BackStyle       =   0  'Transparent
         Caption         =   "Material:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   37
         Top             =   45
         Width           =   1050
      End
   End
   Begin VB.Label lblTDPedido 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   240
      TabIndex        =   62
      Top             =   10080
      Width           =   2010
   End
   Begin VB.Label lblTDProve 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   240
      TabIndex        =   61
      Top             =   10560
      Width           =   3450
   End
   Begin VB.Label lblTDEmpresa 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5520
      TabIndex        =   60
      Top             =   10080
      Width           =   2850
   End
   Begin VB.Label lblTDOrgCompra 
      BackColor       =   &H80000018&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   5520
      TabIndex        =   59
      Top             =   10560
      Width           =   2850
   End
End
Attribute VB_Name = "frmPROCEBuscarArticulo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const cnEspacio As Long = 120

Public sOrigen As String
Public g_oGMN4Seleccionado As CGrupoMatNivel4
Public g_bRAperMatComp As Boolean
Public g_bRCompResp As Boolean
Public g_bRMat As Boolean
Public g_bSeleccionar As Boolean
Public g_bCerrar As Boolean
Public g_oArticuloSeleccionado As CArticulo
Public oArticulos As CArticulos
Public bModoEdicion As Boolean
Public g_bPermProcMultimaterial As Boolean
Public g_oAtributos As CAtributos  'Aqui se guardan todos los atributos de especificaci�n del art�culo seleccionado.
Public g_oProcesoSeleccionado As CProceso
Public g_oGrupoSeleccionado As CGrupo
Public g_oItemSeleccionado As CItem
Public g_bParaPedido As Boolean
Public g_oOrdenEntrega As COrdenEntrega

Private msGMN1Cod As String
Private msGMN2Cod As String
Private msGMN3Cod As String
Private msGMN4Cod As String
Private msGMN4Den As String
Private msCodArticulo As String
Private msDenArticulo As String
Private msMsgMinMax As String
Private msTrue As String
Private msFalse As String
Private arOper As Variant

Private m_oUonsSeleccionadas As CUnidadesOrganizativas

Private m_oArticulo As CArticulo
'grid de articulos agregados
Private m_oArticuloSeleccionado As CArticulo
Private m_oArticulosAgregados As CArticulos
Private m_oAtributos As CAtributos
Private m_oUons As CUnidadesOrganizativas
Private m_oUonsDistribuidas As CUnidadesOrganizativas
Private m_oArticulos As CArticulos
'Separadores
Private Const lSeparador = 127

'Seguridad
Private m_bRestrDistrUsu As Boolean
Private m_bRestrDistrPerf As Boolean
Private m_bRUsuUON As Boolean
Private m_bRUsuPerfUON As Boolean

Private m_sVariasUnidades As String
Private oAtribs As CAtributos
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Public Sub ConfigurarSeguridad()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_bParaPedido Then
        m_bRUsuUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestUO)) Is Nothing)
        m_bRUsuPerfUON = (Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDSegRestPerfUO)) Is Nothing)
    Else
        'Restriccion a la distribuci�n de la compra
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestUOPers)) Is Nothing) Then
            m_bRestrDistrUsu = True
        End If
        
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APERestDisCompraUONPerf)) Is Nothing) Then
            m_bRestrDistrPerf = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Property Get CodArticulo() As String
    CodArticulo = msCodArticulo
End Property

Public Property Let CodArticulo(ByVal sNewValue As String)
    msCodArticulo = sNewValue
End Property

'Carga el art�culo que hemos seleccionado
Private Sub cargarArticulo()
    Dim oArticulos As CArticulos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        oArticulos.DevolverArticulosDesde 1, , , , , , msCodArticulo
        Set m_oArticulo = oArticulos.Item(1)
        Set oArticulos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cargarArticulo", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

'Devuelve el articulo seleccionado en el grid de articulos agregados
Public Property Get ArticuloSeleccionado() As CArticulo
    Dim oArticulos As CArticulos
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
    oArticulos.DevolverArticulosDesde 1, , , , , , Me.ssdbArticulosAgregados.Columns("COD").Value
    Set ArticuloSeleccionado = oArticulos.Item(1)
    Set oArticulos = Nothing
End Property

Public Property Let ArticuloSeleccionado(ByRef oArticulo As CArticulo)
    Set m_oArticuloSeleccionado = oArticulo
End Property

Public Property Get DenArticulo() As String
    DenArticulo = msDenArticulo
End Property

Public Property Let DenArticulo(ByVal sNewValue As String)
    msDenArticulo = sNewValue
End Property

Public Property Get GMN1Cod() As String
    GMN1Cod = msGMN1Cod
End Property

Public Property Let GMN1Cod(ByVal sNewValue As String)
    msGMN1Cod = sNewValue
    If Not g_oGMN4Seleccionado Is Nothing Then
        g_oGMN4Seleccionado.GMN1Cod = sNewValue
    End If
End Property

Public Property Get GMN2Cod() As String
    GMN2Cod = msGMN2Cod
End Property

Public Property Let GMN2Cod(ByVal sNewValue As String)
    msGMN2Cod = sNewValue
    If Not g_oGMN4Seleccionado Is Nothing Then
        g_oGMN4Seleccionado.GMN2Cod = sNewValue
    End If
End Property

Public Property Get GMN3Cod() As String
    GMN3Cod = msGMN3Cod
End Property

Public Property Let GMN3Cod(ByVal sNewValue As String)
    msGMN3Cod = sNewValue
    If Not g_oGMN4Seleccionado Is Nothing Then
        g_oGMN4Seleccionado.GMN3Cod = sNewValue
    End If
End Property

Public Property Get GMN4Cod() As String
    GMN4Cod = msGMN4Cod
End Property

Public Property Let GMN4Cod(ByVal sNewValue As String)
    msGMN4Cod = sNewValue
    If Not g_oGMN4Seleccionado Is Nothing Then
        g_oGMN4Seleccionado.Cod = sNewValue
    End If
End Property

Public Property Get GMN4Den() As String
    GMN4Den = msGMN4Den
End Property

Public Property Let GMN4Den(ByVal sNewValue As String)
    msGMN4Den = sNewValue
    If Not g_oGMN4Seleccionado Is Nothing Then
        g_oGMN4Seleccionado.Den = sNewValue
    End If
End Property



Private Sub cmdAyuda_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
    
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdAyuda_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBorrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblMaterial.caption = ""
    If sdbgArticulos.Rows > 0 Then
        sdbgArticulos.RemoveAll
    End If

    Set g_oGMN4Seleccionado = Nothing
    msGMN1Cod = vbNullString
    msGMN2Cod = vbNullString
    msGMN3Cod = vbNullString
    msGMN4Cod = vbNullString
    msGMN4Den = vbNullString
    
    VaciarAtributosMaterial
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdBorrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Carga el grid de art�culos</summary>
''' <remarks>Llamada desde: Form_Activate, sstabArticulos_Click </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 19/12/2011</revision

Private Sub CargarArticulos()
    Dim oArt As CArticulo
    Dim bCoincidencia As Boolean
    Dim bCodFinalizanPor As Boolean
    Dim bDenFinalizanPor As Boolean
    Dim bCoincidenciaCod As Boolean
    Dim bCoincidenciaDen As Boolean
    Dim sCod As String
    Dim sDen As String
    Dim arConcepto(2) As Boolean
    Dim arAlmacen(2) As Boolean
    Dim arRecep(2) As Boolean
    Dim i As Integer
    Dim iNumAtrib As Integer
    Dim oAtributos As CAtributos
    Dim oArts As CArticulos
    Dim oItem As CItem
    Dim bCargar As Boolean
    Dim o As CAtributo
     
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bCargar = True
        
    If txtCod.Text <> "" Then
        If InStr(txtCod.Text, "*") > 0 Or InStr(txtCod.Text, "*") = Len(txtCod.Text) Then
            bCoincidencia = False
            bCoincidenciaCod = False
            If InStr(txtCod.Text, "*") > 1 Then 'div*
                sCod = Left(txtCod.Text, Len(txtCod.Text) - 1)
                bCodFinalizanPor = False
            Else '*div � *div*
                sCod = Mid(txtCod.Text, 2)
                bCodFinalizanPor = True
                If InStr(sCod, "*") > 1 Then '*div*
                    sCod = Replace(sCod, "*", "%", , , vbTextCompare)
                End If
            End If
        Else
            'Si el proceso se ha creado a partir de una plantilla y esta tiene restricci�n de material,
            'comprobar que el c�digo del art�culo pertenece a los materiales de la plantilla
            If sOrigen = "COD" Then
                If Not frmPROCE.m_oPlantillaSeleccionada Is Nothing Then
                    Set oArts = oFSGSRaiz.Generar_CArticulos
                    oArts.DevolverArticulosDesde 1, , , , , , txtCod.Text, , True
                    If oArts.Count > 0 Then
                        Set oItem = oFSGSRaiz.Generar_CItem
                        oItem.GMN1Cod = oArts.Item(1).GMN1Cod
                        oItem.GMN2Cod = oArts.Item(1).GMN2Cod
                        oItem.GMN3Cod = oArts.Item(1).GMN3Cod
                        oItem.GMN4Cod = oArts.Item(1).GMN4Cod
                        
                        If Not frmPROCE.m_oPlantillaSeleccionada.AdmiteMaterial(oItem) Then
                            bCargar = False
                            oMensajes.ArticuloNoEnMaterialPlantilla
                        End If
                    End If
                End If
            End If
            Set oArts = Nothing
            Set oItem = Nothing
            
            If bCargar Then
                sCod = txtCod.Text
                bCoincidencia = True
                bCoincidenciaCod = True
                bCodFinalizanPor = False
            End If
        End If
    End If
    If txtDen.Text <> "" Then
        If InStr(txtDen.Text, "*") > 0 Or InStr(txtDen.Text, "*") = Len(txtDen.Text) Then
            bCoincidencia = False
            bCoincidenciaDen = False
            If InStr(txtDen.Text, "*") > 1 Then 'div*
                sDen = Left(txtDen.Text, Len(txtDen.Text) - 1)
                bDenFinalizanPor = False
            Else '*div � *div*
                sDen = Mid(txtDen.Text, 2)
                bDenFinalizanPor = True
                If InStr(sDen, "*") > 1 Then '*div*
                    sDen = Replace(sDen, "*", "%", , , vbTextCompare)
                End If
            End If
        Else
            sDen = txtDen.Text
            bCoincidencia = True
            bCoincidenciaDen = True
            bDenFinalizanPor = False
        End If
    End If
    
    If bCargar Then
        'Preparar los checks de los conceptos en arrays
        For i = 0 To 2
            If chkConcepto(i).Value = vbChecked Then arConcepto(i) = True
            If chkAlmacen(i).Value = vbChecked Then arAlmacen(i) = True
            If chkRecep(i).Value = vbChecked Then arRecep(i) = True
        Next
        
        'Atributos
        If sdbgAtributos.Rows > 0 Then
            Set oAtributos = oFSGSRaiz.Generar_CAtributos
            
            With sdbgAtributos
                iNumAtrib = 0
                Do While iNumAtrib < .Rows
                    .Bookmark = iNumAtrib
                    If .Columns("USAR").Value Then
                        If .Columns("VALOR").Value <> "" Then
                            Select Case .Columns("IDTIPO").Value
                                Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_text:=Replace(.Columns("VALOR").Value, "*", "%"))
                                Case TiposDeAtributos.TipoNumerico
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_num:=.Columns("VALOR").Value, Formula:=.Columns("OPER").Value)
                                Case TiposDeAtributos.TipoFecha
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_fec:=.Columns("VALOR").Value)
                                Case TiposDeAtributos.TipoBoolean
                                    Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value, valor_bool:=.Columns("VALOR_ATRIB").Value)
                            End Select
                        Else
                            Set o = oAtributos.Add(.Columns("ID_ATRIB").Value, .Columns("COD").Value, .Columns("DEN").Value, .Columns("IDTIPO").Value)
                        End If
                        o.AmbitoAtributo = oAtribs.Item(CStr(.Columns("ID_ATRIB").Value)).AmbitoAtributo
                    End If
                    iNumAtrib = iNumAtrib + 1
                Loop
                
                .Bookmark = 0
            End With
        End If
        
        Dim ouons1 As CUnidadesOrgNivel1
        Dim ouons2 As CUnidadesOrgNivel2
        Dim ouons3 As CUnidadesOrgNivel3
        
        Set ouons1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
        Set ouons2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
        Set ouons3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
        
        '''obtenemos las unidades sobre las que hay que filtrar
        Dim oUonsRestringidas As CUnidadesOrganizativas
        
        '''caso 1: Hemos checkado alguna de las uons del buscador de uons
        If m_oUonsSeleccionadas.Count > 0 Then
            Set oUonsRestringidas = m_oUonsSeleccionadas
        Else
            Set oUonsRestringidas = oFSGSRaiz.Generar_CUnidadesOrganizativas
            
            If g_bParaPedido Then
                Dim lIdEmpresa As Long
                Dim sOrgCompras As String
                Dim lIdPerfil As Long
                
                If Not g_oOrdenEntrega Is Nothing Then lIdEmpresa = g_oOrdenEntrega.Empresa
                If gParametrosGenerales.gbUsarOrgCompras Then sOrgCompras = g_oOrdenEntrega.OrgCompras
                If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
                oUonsRestringidas.cargarUonsEmpresa lIdEmpresa, sOrgCompras, IIf(m_bRUsuUON, basOptimizacion.gUON1Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON2Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON3Usuario, ""), _
                    IIf(m_bRUsuPerfUON, lIdPerfil, 0)
            Else
                '''comprobar si la distribuci�n ya est� lista
                '''En caso de no estarlo restringimos a las unidades organizativas disponibles en la apertura de proceso
                ''' si no est� distribuido
                If Not g_oProcesoSeleccionado Is Nothing Then
                    Select Case g_oProcesoSeleccionado.DefDistribUON
                        Case TipoDefinicionDatoProceso.EnProceso
                            oUonsRestringidas.cargarTodasLasUnidadesDistribuidasNivelProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
                        Case TipoDefinicionDatoProceso.EnGrupo
                            If g_oGrupoSeleccionado.HayDistribucionUON Then
                                oUonsRestringidas.cargarTodasLasUnidadesDistribuidasNivelGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
                            Else
                                oUonsRestringidas.cargarTodasLasUnidadesUsuPerf oUsuarioSummit, m_bRestrDistrUsu, m_bRestrDistrPerf, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
                            End If
                        Case TipoDefinicionDatoProceso.EnItem
                            oUonsRestringidas.cargarTodasLasUnidadesDistribuidasNivelItem g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oItemSeleccionado.Id
                    End Select
                End If
            End If
        End If
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        If g_bRAperMatComp Then
            If lblMaterial.caption <> "" Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, g_oGMN4Seleccionado.GMN1Cod, g_oGMN4Seleccionado.GMN2Cod, g_oGMN4Seleccionado.GMN3Cod, g_oGMN4Seleccionado.Cod, , sCod, sDen, bCoincidencia, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , bCodFinalizanPor, bDenFinalizanPor, , True, bCoincidenciaCod, bCoincidenciaDen, , chkBuscarGenericos.Value, arConcepto, arAlmacen, arRecep, oAtributos, Me.chkBuscarCentrales, oUonsRestringidas, True, True, True
            Else
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, "", "", "", "", , sCod, sDen, bCoincidencia, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, , bCodFinalizanPor, bDenFinalizanPor, , True, bCoincidenciaCod, bCoincidenciaDen, , chkBuscarGenericos.Value, arConcepto, arAlmacen, arRecep, oAtributos, Me.chkBuscarCentrales, oUonsRestringidas, True, True, True
            End If
        Else
            If lblMaterial.caption <> "" Then
                oArticulos.CargarTodosLosArticulos sCod, sDen, bCoincidencia, False, False, , g_oGMN4Seleccionado.GMN1Cod, g_oGMN4Seleccionado.GMN2Cod, g_oGMN4Seleccionado.GMN3Cod, g_oGMN4Seleccionado.Cod, chkBuscarGenericos.Value, bCodFinalizanPor, bDenFinalizanPor, True, bCoincidenciaCod, bCoincidenciaDen, arConcepto, arAlmacen, arRecep, oAtributos, Me.chkBuscarCentrales, True, oUonsRestringidas, , , , , , , , , True, , True
            Else
                oArticulos.CargarTodosLosArticulos sCod, sDen, bCoincidencia, False, False, , , , , , chkBuscarGenericos.Value, bCodFinalizanPor, bDenFinalizanPor, True, bCoincidenciaCod, bCoincidenciaDen, arConcepto, arAlmacen, arRecep, oAtributos, Me.chkBuscarCentrales, True, oUonsRestringidas, , , , , , , , , True, , True
           End If
        End If
              
        sdbgArticulos.RemoveAll
        If oArticulos.Count > 0 Then
            For Each oArt In oArticulos
                sdbgArticulos.AddItem oArt.GMN1Cod & Chr(m_lSeparador) & oArt.GMN2Cod & Chr(m_lSeparador) & oArt.GMN3Cod & Chr(m_lSeparador) & oArt.GMN4Cod & Chr(m_lSeparador) & oArt.Cod & Chr(m_lSeparador) & oArt.Den & Chr(m_lSeparador) & oArt.Cantidad & Chr(m_lSeparador) & oArt.Generico & Chr(m_lSeparador) & oArt.CodigoUnidad & Chr(m_lSeparador) & oArt.NombreUON & Chr(m_lSeparador) & oArt.CodArticuloCentral & Chr(m_lSeparador) & IIf(oArt.isCentral, 1, 0) & Chr(m_lSeparador) & oArt.tipoRecepcion
            Next
        Else
            oMensajes.NoExistenArticulos
        End If
    End If

    If sdbgArticulos.Rows > 0 Then
        Set g_oArticuloSeleccionado = oFSGSRaiz.Generar_CArticulo
        g_oArticuloSeleccionado.Cod = sdbgArticulos.Columns("ARTICULO").Value
        g_oArticuloSeleccionado.Den = sdbgArticulos.Columns("DENOMINACION").Value
        g_oArticuloSeleccionado.GMN1Cod = sdbgArticulos.Columns("GMNA").Value
        g_oArticuloSeleccionado.GMN2Cod = sdbgArticulos.Columns("FAM").Value
        g_oArticuloSeleccionado.GMN3Cod = sdbgArticulos.Columns("SFA").Value
        g_oArticuloSeleccionado.GMN4Cod = sdbgArticulos.Columns("SFA4").Value
        g_oArticuloSeleccionado.Generico = GridCheckToBoolean(sdbgArticulos.Columns("GENERICO").Value)
        g_oArticuloSeleccionado.Cantidad = sdbgArticulos.Columns("CANT").Value
        g_oArticuloSeleccionado.CodigoUnidad = sdbgArticulos.Columns("UNI").Value
        g_oArticuloSeleccionado.tipoRecepcion = sdbgArticulos.Columns("TIPORECEP").Value
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "CargarArticulos", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

''' <summary>Indica si hay alg�n filtro por atributos</summary>
''' <remarks>Llamada desde: CargarArticulos </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Function HayFiltroAtributos() As Boolean
    Dim iNumAtrib As Integer
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    HayFiltroAtributos = False
    
    With sdbgAtributos
        If .Rows > 0 Then
            iNumAtrib = 0
            Do While iNumAtrib < .Rows
                .Bookmark = iNumAtrib
                If .Columns("USAR").Value Then
                    HayFiltroAtributos = True
                    Exit Do
                End If
                iNumAtrib = iNumAtrib + 1
            Loop
        End If
    End With
    
    If Not HayFiltroAtributos Then
        For i = 0 To 2
            If chkConcepto(i).Value = vbChecked Then
                HayFiltroAtributos = True
                Exit For
            End If
            If chkAlmacen(i).Value = vbChecked Then
                HayFiltroAtributos = True
                Exit For
            End If
            If chkRecep(i).Value = vbChecked Then
                HayFiltroAtributos = True
                Exit For
            End If
        Next
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "HayFiltroAtributos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>Carga el grid de atributos</summary>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 28/06/2012</revision>

Private Sub CargarAtributos()
Dim oatrib As CAtributo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
    
    'Cargar atributos de grupo
    If Not g_oGrupoSeleccionado Is Nothing Then
        If Not g_oGrupoSeleccionado.AtributosDefinicionEspecItem Is Nothing Then
            For Each oatrib In g_oGrupoSeleccionado.AtributosDefinicionEspecItem
                sdbgAtributos.AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Atrib & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0"
            Next
            
            Set oatrib = Nothing
        End If
    End If
    
    'Cargar atributos de las ramas de material del proceso
    CargarAtributosMaterialesProceso
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "CargarAtributos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Carga el grid de atributos con los atributos de las ramas de material del proceso</summary>
''' <remarks>Llamada desde: CargarAtributos </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 28/06/2012</revision>

Private Sub CargarAtributosMaterialesProceso()
    Dim oMat As CGrupoMatNivel4
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oProcesoSeleccionado Is Nothing Then
        For Each oMat In g_oProcesoSeleccionado.MaterialProce
            CargarAtributosMaterial oMat.GMN1Cod, oMat.GMN2Cod, oMat.GMN3Cod, oMat.Cod, True
        Next
        Set oMat = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "CargarAtributosMaterialesProceso", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Elimina del grid de atributos los atributos de material</summary>
''' <remarks>Llamada desde: cmdSelMat_Click, cmdBorrar_Click </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub VaciarAtributosMaterial()
'    Dim iRow As Long
'
'    With sdbgAtributos
'        iRow = .Rows
'        Do While iRow >= 0
'            .Bookmark = iRow
'            'Se elimina si no es un atributo de grupo o de materiales del proceso
'            If .Columns("GRUPO").Value = 0 And .Columns("PROCE").Value = 0 Then
'                .RemoveItem .AddItemRowIndex(.Bookmark)
'            End If
'
'            iRow = iRow - 1
'        Loop
'    End With
    'Jim : Borro todos los atributos , para evitar posibles conflictos con los a�adidos por uon, por material o individualmente.
'Si se puede esto ya se arreglar� m�s adelante con tiempo.
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgAtributos.RemoveAll
    Set oAtribs = Nothing
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "VaciarAtributosMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Carga el grid de atributos con los atributos de material</summary>
''' <param name="sGMN1Cod">Cod. GMN1</param>
''' <param name="sGMN2Cod">Cod. GMN2</param>
''' <param name="sGMN3Cod">Cod. GMN3</param>
''' <param name="sGMN4Cod">Cod. GMN4</param>
''' <param name="bProce">Indica si se trata de un material de proceso</param>
''' <remarks>Llamada desde: Form_Load </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>
''' <revision>LTG 24/11/2011</revision>

Private Sub CargarAtributosMaterial(ByVal sGMN1Cod As String, ByVal sGMN2Cod As String, ByVal sGMN3Cod As String, ByVal sGMN4Cod As String, ByVal bProce As Boolean)
    Dim oatrib As CAtributo
    Dim oAtribGrupo As CAtributo
    Dim oGMN4 As CGrupoMatNivel4
    
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
    oGMN4.GMN1Cod = sGMN1Cod
    oGMN4.GMN2Cod = sGMN2Cod
    oGMN4.GMN3Cod = sGMN3Cod
    oGMN4.Cod = sGMN4Cod
    
    Dim oUON As IUon
    
    If Not m_oUonsSeleccionadas Is Nothing Then
        If m_oUonsSeleccionadas.Count <> 0 Then
            Set oAtribs = oGMN4.DevolverAtribMatYArtMat(oUons:=m_oUonsSeleccionadas)
        Else
            Set oAtribs = oGMN4.DevolverAtribMatYArtMat
        End If
    Else
        Set oAtribs = oGMN4.DevolverAtribMatYArtMat
    End If
    
    Set oUON = Nothing
    If Not oAtribs Is Nothing Then
        If oAtribs.Count > 0 Then
            For Each oatrib In oAtribs
                'A�adir si no existe ya entre los atributos que se han a�adido ya al grid
                If Not ExisteAtributoEnGrid(oatrib.Id) Then
                    sdbgAtributos.AddItem "0" & Chr(m_lSeparador) & oatrib.Cod & Chr(m_lSeparador) & oatrib.Den & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & oatrib.TipoIntroduccion & Chr(m_lSeparador) & oatrib.Tipo & Chr(m_lSeparador) & oatrib.Id & Chr(m_lSeparador) & oatrib.Maximo & Chr(m_lSeparador) & oatrib.Minimo & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & IIf(bProce, "1", "0")
                End If
            Next
        End If
    End If
    
    Set oAtribGrupo = Nothing
    Set oGMN4 = Nothing
    Set oatrib = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "CargarAtributosMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Indica si ya existe un atributo en el grid</summary>
''' <param name="lIdAtrib">Id del atributo a buscar</param>
''' <returns>Booleano indicando si existe el atributo o no</returns>
''' <remarks>Llamada desde: CargarAtributosMaterial </remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Function ExisteAtributoEnGrid(ByVal lIdAtrib As Long) As Boolean
    Dim vbm As Variant
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ExisteAtributoEnGrid = False
    
    With sdbgAtributos
        If .Rows > 0 Then
            For i = 0 To .Rows - 1
                vbm = .AddItemBookmark(i)
                If .Columns("ID_ATRIB").CellValue(vbm) = lIdAtrib Then
                    ExisteAtributoEnGrid = True
                    Exit For
                End If
            Next
        End If
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "ExisteAtributoEnGrid", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub cmdBorrarUon_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.lblUon.caption = ""
    m_oUonsSeleccionadas.clear
    VaciarAtributosMaterial
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdBorrarUon_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscaAtrib_Click()
    Dim ofrmATRIB As frmAtribMod
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set ofrmATRIB = New frmAtribMod
    
    ofrmATRIB.g_sOrigen = "frmPROCEBuscarArticulo"
    ofrmATRIB.g_sGmn1 = msGMN1Cod
        ofrmATRIB.g_sGmn2 = msGMN2Cod
        ofrmATRIB.g_sGmn3 = msGMN3Cod
        ofrmATRIB.g_sGmn4 = msGMN4Cod
        
        ofrmATRIB.sstabGeneral.Tab = 0
        ofrmATRIB.g_GMN1RespetarCombo = True
         
        ofrmATRIB.sdbcGMN1_4Cod.Text = msGMN1Cod
             
        If g_oGMN4Seleccionado Is Nothing Then
            ofrmATRIB.sdbcGMN1_4Cod_Validate False
            ofrmATRIB.sdbcGMN2_4Cod_Validate False
            ofrmATRIB.sdbcGMN3_4Cod_Validate False
            ofrmATRIB.sdbcGMN4_4Cod_Validate False
        Else
            If g_oGMN4Seleccionado.GMN1Den = "" Then
                ofrmATRIB.sdbcGMN1_4Cod_Validate False
            Else
                ofrmATRIB.sdbcGMN1_4Den.Text = g_oGMN4Seleccionado.GMN1Den
            End If
            ofrmATRIB.g_GMN1RespetarCombo = False
            ofrmATRIB.g_GMN2RespetarCombo = True
            ofrmATRIB.sdbcGMN2_4Cod.Text = msGMN2Cod
            If g_oGMN4Seleccionado.GMN2Den = "" Then
                ofrmATRIB.sdbcGMN2_4Cod_Validate False
            Else
                ofrmATRIB.sdbcGMN2_4Den.Text = g_oGMN4Seleccionado.GMN2Den
            End If
            ofrmATRIB.g_GMN2RespetarCombo = False
            ofrmATRIB.g_GMN3RespetarCombo = True
            ofrmATRIB.sdbcGMN3_4Cod.Text = msGMN3Cod
            If g_oGMN4Seleccionado.GMN3Den = "" Then
                ofrmATRIB.sdbcGMN3_4Cod_Validate False
            Else
                ofrmATRIB.sdbcGMN3_4Den.Text = g_oGMN4Seleccionado.GMN3Den
            End If
            ofrmATRIB.g_GMN3RespetarCombo = False
            ofrmATRIB.g_GMN4RespetarCombo = True
            ofrmATRIB.sdbcGMN4_4Cod.Text = msGMN4Cod
            If g_oGMN4Seleccionado.Den = "" Then
                ofrmATRIB.sdbcGMN4_4Cod_Validate False
            Else
                ofrmATRIB.sdbcGMN4_4Den.Text = g_oGMN4Seleccionado.Den
            End If
            ofrmATRIB.g_GMN4RespetarCombo = False
        End If
    
    ofrmATRIB.cmdSeleccionar.Visible = True
   
    ofrmATRIB.sdbgAtributosGrupo.SelectTypeRow = ssSelectionTypeMultiSelectRange
    ofrmATRIB.g_bSoloSeleccion = True
    
    ofrmATRIB.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdBuscaAtrib_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdCerrar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    g_bCerrar = True
    Me.Hide
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdCerrar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSeleccionar_Click()
    'a�adir al sdbgitems el articulo seleccionado
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oArticuloSeleccionado Is Nothing Then Exit Sub
    g_bSeleccionar = True
    Me.Hide
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdSeleccionar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSelMat_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SeleccionarMaterial "frmPROCEBuscarArticulo", g_oProcesoSeleccionado, g_bRAperMatComp, g_bPermProcMultimaterial, g_bRCompResp

    VaciarAtributosMaterial
    CargarAtributosMaterial msGMN1Cod, msGMN2Cod, msGMN3Cod, msGMN4Cod, False
    
    sdbgArticulos.RemoveAll
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdSelMat_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdSelUon_Click()
    Dim uons1 As CUnidadesOrgNivel1
    Dim uons2 As CUnidadesOrgNivel2
    Dim uons3 As CUnidadesOrgNivel3
    Dim oUON As IUon
    
    Dim frm As frmSELUO
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set frm = New frmSELUO
    frm.multiselect = True
    If m_oUonsSeleccionadas.Count > 0 Then
        Set frm.UonsSeleccionadas = m_oUonsSeleccionadas
    End If
    Set uons1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set uons2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set uons3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
    
    '''cargamos las unidades organizativas que se pueden usar
    If g_bParaPedido Then
        Dim lIdEmpresa As Long
        Dim sOrgCompras As String
        Dim lIdPerfil As Long
        
        If Not g_oOrdenEntrega Is Nothing Then lIdEmpresa = g_oOrdenEntrega.Empresa
        If gParametrosGenerales.gbUsarOrgCompras Then sOrgCompras = g_oOrdenEntrega.OrgCompras
        If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
                    
        uons1.CargarTodasLasUnidadesOrganizativasEmpresa lIdEmpresa, sOrgCompras, IIf(m_bRUsuUON, basOptimizacion.gUON1Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON2Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON3Usuario, ""), _
                    IIf(m_bRUsuPerfUON, lIdPerfil, 0)
        uons2.CargarTodasLasUnidadesOrganizativasEmpresa lIdEmpresa, sOrgCompras, IIf(m_bRUsuUON, basOptimizacion.gUON1Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON2Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON3Usuario, ""), _
                    IIf(m_bRUsuPerfUON, lIdPerfil, 0)
        uons3.CargarTodasLasUnidadesOrganizativasEmpresa lIdEmpresa, sOrgCompras, IIf(m_bRUsuUON, basOptimizacion.gUON1Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON2Usuario, ""), IIf(m_bRUsuUON, basOptimizacion.gUON3Usuario, ""), _
                    IIf(m_bRUsuPerfUON, lIdPerfil, 0)
    Else
        Select Case g_oProcesoSeleccionado.DefDistribUON
            Case TipoDefinicionDatoProceso.EnProceso
                uons1.cargarTodasLasUnidadesDeDistribucionProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
                uons2.cargarTodasLasUnidadesDeDistribucionProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
                uons3.cargarTodasLasUnidadesDeDistribucionProceso g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod
            Case TipoDefinicionDatoProceso.EnGrupo
                'g_oGrupoSeleccionado.HayDistribucion no est� bien mantenida, s�lo es correcta cuando cargamos un proceso existente
                'as� que cargamos las unidades del grupo y si no hay ninguna es que no hab�a distribuci�n y tomamos s�lo las uon restringidas
                uons1.cargarTodasLasUnidadesDeDistribucionGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
                uons2.cargarTodasLasUnidadesDeDistribucionGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
                uons3.cargarTodasLasUnidadesDeDistribucionGrupo g_oProcesoSeleccionado.Anyo, g_oProcesoSeleccionado.GMN1Cod, g_oProcesoSeleccionado.Cod, g_oGrupoSeleccionado.Id
                If uons1.Count = 0 And uons2.Count = 0 And uons3.Count = 0 Then
                    cargarUonsRestringidas uons1, uons2, uons3
                End If
            Case TipoDefinicionDatoProceso.EnItem
                'si no hay distribuci�n seleccionamos las uon con restricci�n de usuario
                cargarUonsRestringidas uons1, uons2, uons3
        End Select
    End If
    
    frm.cargarArbol uons1, uons2, uons3
    frm.Show vbModal
    Set m_oUonsSeleccionadas = frm.UonsSeleccionadas
    If frm.Aceptar And frm.UonsSeleccionadas.Count > 0 Then
        Me.lblUon.caption = frm.UonsSeleccionadas.titulo
        Me.lstMultiUon.clear
        For Each oUON In m_oUonsSeleccionadas
            Me.lstMultiUon.AddItem oUON.titulo
        Next
        VaciarAtributosMaterial
        CargarAtributosMaterial msGMN1Cod, msGMN2Cod, msGMN3Cod, msGMN4Cod, False
    End If

    Set uons1 = Nothing
    Set uons2 = Nothing
    Set uons3 = Nothing
    Set frm = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cmdSelUon_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()
    Dim sMaterial As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
    If g_oGMN4Seleccionado Is Nothing Then ObjetoMaterial 'Si no esta cargado g_oGMN4Seleccionado casca la busqueda
    If msGMN1Cod & msGMN2Cod & msGMN3Cod & msGMN4Cod & msGMN4Den <> "" Then
        sMaterial = msGMN1Cod & "-" & msGMN2Cod & "-" & msGMN3Cod & "-" & msGMN4Cod & "-" & msGMN4Den
    End If
    
    If bModoEdicion Then
        lblMaterial.caption = sMaterial
        
        If sOrigen = "COD" Or sOrigen = "DEN" Then
            If (frmPROCE.sdbgItems.Columns("COD").Value <> "") And (frmPROCE.sdbgItems.Columns("DEN").Value <> "") Then
                CargarArticulos
                sOrigen = ""
            End If
        End If
    Else
        lblDAMaterial.caption = sMaterial
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ObjetoMaterial()

Set g_oGMN4Seleccionado = oFSGSRaiz.Generar_CGrupoMatNivel4
g_oGMN4Seleccionado.GMN1Cod = msGMN1Cod
g_oGMN4Seleccionado.GMN2Cod = msGMN2Cod
g_oGMN4Seleccionado.GMN3Cod = msGMN3Cod
g_oGMN4Seleccionado.Cod = msGMN4Cod
g_oGMN4Seleccionado.Den = msGMN4Den

End Sub

Private Sub Form_Initialize()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oArticulosAgregados = oFSGSRaiz.Generar_CArticulos
    Set m_oUonsSeleccionadas = oFSGSRaiz.Generar_CUnidadesOrganizativas
    Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
    Set m_oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
    Set m_oArticulos = oFSGSRaiz.Generar_CArticulos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "Form_Initialize", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()
    Dim oArticulos As CArticulos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oAtribs = oFSGSRaiz.Generar_CAtributos
    m_bActivado = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    ConfigurarSeguridad
    CargarRecursos
    PonerFieldSeparator Me
    If bModoEdicion Then
        If g_bParaPedido Then MostrarDatosPedido
        
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        If m_oUonsSeleccionadas.Count > 0 Then
            Me.lblUon.caption = m_oUonsSeleccionadas.titulo
        End If
        If sOrigen = "COD" Then
            txtCod.Text = msCodArticulo
        Else
            txtDen.Text = msDenArticulo
        End If
        Me.ssdbArticulosAgregados.Visible = False
        CargarAtributos
    Else
        txtDACod.Text = msCodArticulo
        txtDADen.Text = msDenArticulo
        Me.ssdbArticulosAgregados.Visible = True
        Me.ssdbArticulosAgregados.Top = txtDACod.Top + txtDACod.Height + 20
        'inicializar colecci�n de uons y atributos
        Set m_oUons = Nothing
        Set m_oAtributos = Nothing
        Set m_oUons = oFSGSRaiz.Generar_CUnidadesOrganizativas
        Set m_oAtributos = oFSGSRaiz.Generar_CAtributos
        Set m_oArticulos = Nothing
        Set m_oArticulos = oFSGSRaiz.Generar_CArticulos
        
        cargarArticulo
        montarColumnasGridAtributosUon
        cargarGridArticulosAgregados
        cargarValoresGridArticulosAgregados
    End If
    
    If sOrigen = "COD" Or sOrigen = "DEN" Then Set frmPROCE.ArticuloSeleccionado = Nothing
    
    CargarComboOperandos
    cargarArticulo
    
    Me.chkBuscarCentrales.Visible = gParametrosGenerales.gbArticulosCentrales
    
    If Not m_oArticulo Is Nothing Then
        If Not m_oArticulo.isCentral Then
            Me.ssdbArticulosAgregados.Columns("COD").Visible = False
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Muestra los datos del pedido</summary>
''' <remarks>Llamada desde: Form_Load</remarks>

Private Sub MostrarDatosPedido()
    Dim oEmpresa As CEmpresa
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oOrdenEntrega Is Nothing Then
        lblDPedido.caption = g_oOrdenEntrega.Anyo & "/" & g_oOrdenEntrega.NumPedido & "/" & g_oOrdenEntrega.Numero
        lblDProve.caption = g_oOrdenEntrega.ProveCod & " - " & g_oOrdenEntrega.ProveDen
        lblDOrgCompra.caption = g_oOrdenEntrega.OrgComprasDen
                
        lblDPedido.ToolTipText = lblDPedido.caption
        lblDProve.ToolTipText = lblDProve.caption
        lblDOrgCompra.ToolTipText = lblDOrgCompra.caption
        
        Set oEmpresa = oFSGSRaiz.Generar_CEmpresa
        oEmpresa.Id = g_oOrdenEntrega.Empresa
        oEmpresa.cargarDatosEmpresa
        lblDEmpresa.caption = oEmpresa.NIF & " - " & g_oOrdenEntrega.EmpresaDen
        lblDEmpresa.ToolTipText = lblDEmpresa.caption
        Set oEmpresa = Nothing
        
        fraAlmacen.Enabled = False
        fraConcepto.Enabled = False
        fraRecepcion.Enabled = False
        Select Case g_oOrdenEntrega.TipoDePedido.CodConcep
            Case TipoConcepto.Ambos
                'chkConcepto(2).Value = vbChecked
                fraConcepto.Enabled = True
            Case TipoConcepto.Gasto
                chkConcepto(0).Value = vbChecked
            Case TipoConcepto.Inversion
                chkConcepto(1).Value = vbChecked
        End Select
        Select Case g_oOrdenEntrega.TipoDePedido.CodAlmac
            Case TipoArtAlmacenable.OpcionalAlmacenar
                'chkAlmacen(2).Value = vbChecked
                fraAlmacen.Enabled = True
            Case TipoArtAlmacenable.NoAlmacenable
                chkAlmacen(1).Value = vbChecked
            Case TipoArtAlmacenable.ObligatorioAlmacenar
                chkAlmacen(0).Value = vbChecked
        End Select
        Select Case g_oOrdenEntrega.TipoDePedido.CodRecep
            Case TipoArtRecepcionable.OpcionalRececpionar
                'chkRecep(2).Value = vbChecked
                fraRecepcion.Enabled = True
            Case TipoArtRecepcionable.NoRececpionar
                chkRecep(1).Value = vbChecked
            Case TipoArtRecepcionable.ObligatorioRececpionar
                chkRecep(0).Value = vbChecked
        End Select
        
        'Tooltips labels transparentes
        lblTDPedido.ToolTipText = lblDPedido.ToolTipText
        lblTDProve.ToolTipText = lblDProve.ToolTipText
        lblTDEmpresa.ToolTipText = lblDEmpresa.ToolTipText
        lblTDOrgCompra.ToolTipText = lblDOrgCompra.ToolTipText
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "MostrarDatosPedido", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Terminate()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oArticuloSeleccionado = Nothing
    Set m_oArticulosAgregados = Nothing
    Set m_oArticulo = Nothing
    Set m_oUonsSeleccionadas = Nothing
    Set m_oUonsDistribuidas = Nothing
    Set m_oArticulos = Nothing
    Set oAtribs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "Form_Terminate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oArticuloSeleccionado = Nothing
    oFSGSRaiz.pg_sFrmCargado Me.Name, False
    msGMN1Cod = vbNullString
    msGMN2Cod = vbNullString
    msGMN3Cod = vbNullString
    msGMN4Cod = vbNullString
    msGMN4Den = vbNullString
    msCodArticulo = vbNullString
    msDenArticulo = vbNullString
    msMsgMinMax = vbNullString
    msTrue = vbNullString
    msFalse = vbNullString
    g_bSeleccionar = False
    g_bCerrar = False
    Erase arOper
    Set oAtribs = Nothing
    m_oUonsSeleccionadas.clear
    g_bParaPedido = False
    
sOrigen = ""
g_bRAperMatComp = False
g_bRCompResp = False
g_bRMat = False
bModoEdicion = False
g_bPermProcMultimaterial = False
Set g_oGMN4Seleccionado = Nothing
Set g_oArticuloSeleccionado = Nothing
Set oArticulos = Nothing
Set g_oAtributos = Nothing
Set g_oProcesoSeleccionado = Nothing
Set g_oGrupoSeleccionado = Nothing
Set g_oItemSeleccionado = Nothing
Set g_oOrdenEntrega = Nothing
Set m_oArticulo = Nothing
Set m_oArticuloSeleccionado = Nothing
Set m_oArticulosAgregados = Nothing
Set m_oAtributos = Nothing
Set m_oUons = Nothing
Set m_oUonsDistribuidas = Nothing
Set m_oArticulos = Nothing

'Seguridad
m_bRestrDistrUsu = False
m_bRestrDistrPerf = False
m_bRUsuUON = False
m_bRUsuPerfUON = False

m_sVariasUnidades = ""
m_bActivado = False
m_bDescargarFrm = False
m_sMsgError = ""
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset


'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_PROCEBUSCARARTICULO, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
        lblMat.caption = Ador(0).Value
        lblDAMat.caption = Ador(0).Value
        Ador.MoveNext
        lblCod.caption = Ador(0).Value
        lblDACod.caption = Ador(0).Value
        Ador.MoveNext
        lblDen.caption = Ador(0).Value
        lblDADen.caption = Ador(0).Value
        Ador.MoveNext
        chkBuscarGenericos.caption = Ador(0).Value
        Ador.MoveNext
        
        sdbgArticulos.Columns(0).caption = gParametrosGenerales.gsABR_GMN1
        sdbgArticulos.Columns(1).caption = gParametrosGenerales.gsABR_GMN2
        sdbgArticulos.Columns(2).caption = gParametrosGenerales.gsABR_GMN3
        sdbgArticulos.Columns(3).caption = gParametrosGenerales.gsABR_GMN4
        Ador.MoveNext
        Ador.MoveNext
        Ador.MoveNext
        sdbgArticulos.Columns(4).caption = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns(5).caption = Ador(0).Value
        Ador.MoveNext
        sdbgArticulos.Columns(6).caption = Ador(0).Value
        Ador.MoveNext
        cmdSeleccionar.caption = Ador(0).Value
        Ador.MoveNext
        cmdCerrar.caption = Ador(0).Value
        Ador.MoveNext
        If bModoEdicion Then
            Me.caption = Ador(0).Value
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
        Else
            Ador.MoveNext
            Ador.MoveNext
            Ador.MoveNext
            Me.caption = Ador(0).Value
        End If
        Ador.MoveNext
        chkConcepto(0).caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(1).caption = Ador(0).Value
        Ador.MoveNext
        chkConcepto(2).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(0).caption = Ador(0).Value
        chkRecep(0).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(1).caption = Ador(0).Value
        Ador.MoveNext
        chkAlmacen(2).caption = Ador(0).Value
        chkRecep(2).caption = Ador(0).Value
        Ador.MoveNext
        chkRecep(1).caption = Ador(0).Value
        Ador.MoveNext
        lblConcepto.caption = Ador(0).Value
        Ador.MoveNext
        lblAlmacenamiento.caption = Ador(0).Value
        Ador.MoveNext
        lblRecepcion.caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("DEN").caption = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("VALOR").caption = Ador(0).Value
        Ador.MoveNext
        msTrue = Ador(0).Value
        Ador.MoveNext
        msFalse = Ador(0).Value
        Ador.MoveNext
        lblBusqAtrib.caption = Ador(0).Value & ":"
        Ador.MoveNext
        msMsgMinMax = Ador(0).Value
        Ador.MoveNext
        sdbgAtributos.Columns("OPER").caption = Ador(0).Value
        Ador.MoveNext
        sstabArticulos.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        sstabArticulos.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        Me.ssdbArticulosAgregados.Columns("UON").caption = Ador(0).Value
        Me.sdbgArticulos.Columns("UON_DEN").caption = Ador(0).Value
        Me.lblUniOrg.caption = Ador(0).Value
        Ador.MoveNext
        Me.ssdbArticulosAgregados.Columns("COD").caption = Ador(0).Value
        Ador.MoveNext
        m_sVariasUnidades = Ador(0).Value
        Ador.MoveNext
        lblPedido.caption = Ador(0).Value
        Ador.MoveNext
        lblProve.caption = Ador(0).Value
        Ador.MoveNext
        lblEmpresa.caption = Ador(0).Value
        Ador.MoveNext
        lblOrgCompra.caption = Ador(0).Value
        Ador.MoveNext
        If g_bParaPedido Then Me.caption = Ador(0).Value
        
        Ador.Close
    End If

    Set Ador = Nothing
    'pargen lit
    Dim oLiterales As CLiterales
    Set oLiterales = oGestorParametros.DevolverLiterales(48, 52, basPublic.gParametrosInstalacion.gIdioma)
    Me.sdbgArticulos.Columns("CENTRAL").caption = oLiterales.Item(3).Den
    Me.sdbgArticulos.Columns("AGREGADO").caption = oLiterales.Item(1).Den
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

Public Sub PonerMatSeleccionado()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set g_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        
    If Not g_oGMN4Seleccionado Is Nothing Then
        lblMaterial.caption = g_oGMN4Seleccionado.GMN1Cod & " - " & g_oGMN4Seleccionado.GMN2Cod & " - " & g_oGMN4Seleccionado.GMN3Cod & "-" & g_oGMN4Seleccionado.Cod & " - " & g_oGMN4Seleccionado.Den
        txtCod.Text = ""
        txtDen.Text = ""
        
        msGMN1Cod = g_oGMN4Seleccionado.GMN1Cod
        msGMN2Cod = g_oGMN4Seleccionado.GMN2Cod
        msGMN3Cod = g_oGMN4Seleccionado.GMN3Cod
        msGMN4Cod = g_oGMN4Seleccionado.Cod
        msGMN4Den = g_oGMN4Seleccionado.Den
    Else
        Exit Sub
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "PonerMatSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub Form_Resize()

'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Arrange
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
End Sub

''' <summary>Coloca los controles en el formulario en funci�n del tama�o de este</summary>
''' <remarks>Llamada desde: Form_Resize</remarks>

Private Sub Arrange()
On Error Resume Next
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.WindowState <> vbMinimized Then
        sstabArticulos.Visible = bModoEdicion
        fraDatosArticulo.Visible = Not bModoEdicion
        picDatosPedido.Visible = g_bParaPedido
        lblOrgCompra.Visible = gParametrosGenerales.gbUsarOrgCompras
        lblDOrgCompra.Visible = gParametrosGenerales.gbUsarOrgCompras
                
        lblTDPedido.Visible = g_bParaPedido
        lblTDProve.Visible = g_bParaPedido
        lblTDEmpresa.Visible = g_bParaPedido
        lblTDOrgCompra.Visible = g_bParaPedido
        If g_bParaPedido Then
            'Si picDatosPedido no est� habilitado no funcionan los tooltips de los controles que contiene
            'Se ponen unos labels transparentes por encima de estos controles y en la misma posici�n con sus tooltips para poder ofrecer esta funcionalidad
            lblTDPedido.Left = lblDPedido.Left
            lblTDPedido.Top = lblDPedido.Top
            lblTDProve.Left = lblDProve.Left
            lblTDProve.Top = lblDProve.Top
            lblTDEmpresa.Left = lblDEmpresa.Left
            lblTDEmpresa.Top = lblDEmpresa.Top
            lblTDOrgCompra.Left = lblDOrgCompra.Left
            lblTDOrgCompra.Top = lblDOrgCompra.Top
            
            lblTDPedido.BackStyle = BackStyleConstants.cc2BackstyleTransparent
            lblTDProve.BackStyle = BackStyleConstants.cc2BackstyleTransparent
            lblTDEmpresa.BackStyle = BackStyleConstants.cc2BackstyleTransparent
            lblTDOrgCompra.BackStyle = BackStyleConstants.cc2BackstyleTransparent
                    
            lblTDPedido.ZOrder 0
            lblTDProve.ZOrder 0
            lblTDEmpresa.ZOrder 0
            lblTDOrgCompra.ZOrder 0
        End If
        
        If Not bModoEdicion Then
            fraDatosArticulo.Top = cnEspacio
            Me.ssdbArticulosAgregados.Visible = True
            Me.Width = fraDatosArticulo.Width + (2 * cnEspacio)
            Me.Height = fraDatosArticulo.Height + (2 * cnEspacio)
        Else
            If Me.Height < 6900 Then Me.Height = 6900
            If Me.Width < 9090 Then Me.Width = 9090
            
            If g_bParaPedido Then
                sstabArticulos.Top = picDatosPedido.Top + picDatosPedido.Height + cnEspacio
                sstabArticulos.Height = Me.ScaleHeight - picDatosPedido.Height - (3 * cnEspacio)
            Else
                sstabArticulos.Top = cnEspacio
                sstabArticulos.Height = Me.ScaleHeight - (2 * cnEspacio)
            End If
            sstabArticulos.Width = Me.ScaleWidth - (2 * cnEspacio)
                                    
            fraDatosBusqueda.Width = sstabArticulos.Width - (2 * cnEspacio)
            fraDatosBusqueda.Height = sstabArticulos.Height - (4 * cnEspacio)
            sdbgAtributos.Width = fraDatosBusqueda.Width - (2 * cnEspacio)
            sdbgAtributos.Height = fraDatosBusqueda.Height - sdbgAtributos.Top - fraConcepto.Height - (2 * cnEspacio)
            fraConcepto.Top = fraDatosBusqueda.Height - fraConcepto.Height - cnEspacio
            fraAlmacen.Top = fraConcepto.Top
            fraRecepcion.Top = fraConcepto.Top
                    
            sdbgAtributos.Columns("USAR").Width = 600
            sdbgAtributos.Columns("COD").Width = (sdbgAtributos.Width - 1200) * 0.22
            sdbgAtributos.Columns("DEN").Width = (sdbgAtributos.Width - 1200) * 0.42
            sdbgAtributos.Columns("OPER").Width = 600
            sdbgAtributos.Columns("VALOR").Width = (sdbgAtributos.Width - 1200) * 0.33
                    
            sdbgArticulos.Width = sstabArticulos.Width - (2 * cnEspacio)
            sdbgArticulos.Columns(0).Width = sdbgArticulos.Width * 7 / 100
            sdbgArticulos.Columns(1).Width = sdbgArticulos.Width * 7 / 100
            sdbgArticulos.Columns(2).Width = sdbgArticulos.Width * 7 / 100
            sdbgArticulos.Columns(3).Width = sdbgArticulos.Width * 7 / 100
            sdbgArticulos.Columns(4).Width = sdbgArticulos.Width * 15 / 100
            sdbgArticulos.Columns(5).Width = sdbgArticulos.Width * 42 / 100
            sdbgArticulos.Columns(6).Width = sdbgArticulos.Width * 12 / 100 - 210
            sdbgArticulos.Height = sstabArticulos.Height - sdbgArticulos.Top - cmdCerrar.Height - (2 * cnEspacio)
            cmdSeleccionar.Top = sdbgArticulos.Top + sdbgArticulos.Height + cnEspacio
            cmdCerrar.Top = cmdSeleccionar.Top
            cmdSeleccionar.Left = sdbgArticulos.Width / 2 - 1000
            cmdCerrar.Left = cmdSeleccionar.Left + cmdSeleccionar.Width + 300
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 
End Sub

''' <summary>Carga el combo de operandos</summary>
''' <remarks>Llamada desde: Form_Load</remarks>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Private Sub CargarComboOperandos()
    Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    arOper = Array("=", ">", ">=", "<", "<=")
    
    sdbddOper.RemoveAll
    
    sdbddOper.AddItem ""
    For i = 0 To UBound(arOper)
        sdbddOper.AddItem arOper(i) & Chr(m_lSeparador) & arOper(i)
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "CargarComboOperandos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub sdbddOper_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddOper.DataFieldList = "Column 0"
    sdbddOper.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbddOper_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddOper_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
   
    sdbddOper.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddOper.Rows - 1
            bm = sdbddOper.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddOper.Columns(0).CellText(bm), 1, Len(Text))
                sdbddOper.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbddOper_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddOper_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        
        ''' Comprobar la existencia en la lista
        For i = 0 To UBound(arOper)
            If arOper(i) = sdbgAtributos.Columns(sdbgAtributos.col).Text Then
                bExiste = True
                Exit For
            End If
        Next
        
        If Not bExiste Then
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).Text
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbddOper_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_DropDown()
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer
    Dim oLista As CValoresPond
    Dim oElem As CValorPond

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.RemoveAll
    
    sdbddValor.AddItem ""

    iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

    Set oatrib = oFSGSRaiz.Generar_CAtributo
    Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)

    If Not oatrib Is Nothing Then
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            oatrib.CargarListaDeValores
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                sdbddValor.AddItem oElem.ValorLista & Chr(m_lSeparador) & oElem.ValorLista
            Next
            Set oLista = Nothing
        Else
            'Campo de tipo boolean:
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                sdbddValor.AddItem msTrue & Chr(m_lSeparador) & msTrue
                sdbddValor.AddItem msFalse & Chr(m_lSeparador) & msFalse
            End If
        End If
    
        Set oatrib = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbddValor_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbddValor_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
   
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbgAtributos.Columns(sdbgAtributos.col).Value = Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbddValor_PositionList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbddValor_ValidateList(Text As String, RtnPassed As Integer)
    Dim bExiste As Boolean
    Dim oLista As CValoresPond
    Dim oElem As CValorPond
    Dim oatrib As CAtributo
    Dim iIdAtrib As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Text = "" Then
        RtnPassed = True
    Else
        bExiste = False
        ''' Comprobar la existencia en la lista
        If sdbgAtributos.Columns("INTRO").Value = "1" Then
            iIdAtrib = val(sdbgAtributos.Columns("ID_ATRIB").Value)

            Set oatrib = oFSGSRaiz.Generar_CAtributo
            Set oatrib = oatrib.SeleccionarDefinicionDeAtributo(iIdAtrib)
            
            If oatrib Is Nothing Then Exit Sub
            
            oatrib.CargarListaDeValores
            
            Set oLista = oatrib.ListaPonderacion
            For Each oElem In oLista
                If UCase(oElem.ValorLista) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    bExiste = True
                    Exit For
                End If
            Next
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TiposDeAtributos.TipoBoolean Then
                If UCase(msTrue) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "1"
                    bExiste = True
                End If
                If UCase(msFalse) = UCase(sdbgAtributos.Columns(sdbgAtributos.col).Text) Then
                    sdbgAtributos.Columns("VALOR_ATRIB").Text = "0"
                    bExiste = True
                End If
                
            End If
        End If
        If Not bExiste Then
            sdbgAtributos.Columns(sdbgAtributos.col).Text = ""
            oMensajes.NoValido sdbgAtributos.Columns(sdbgAtributos.col).caption
            RtnPassed = False
            Exit Sub
        End If
        RtnPassed = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbddValor_ValidateList", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbgArticulos_Click()
    Dim oArticulo As CArticulo
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgArticulos.Rows = 0 Then Exit Sub
    
    Set g_oArticuloSeleccionado = oFSGSRaiz.Generar_CArticulo
    g_oArticuloSeleccionado.Cod = sdbgArticulos.Columns("ARTICULO").Value
    g_oArticuloSeleccionado.Den = sdbgArticulos.Columns("DENOMINACION").Value
    g_oArticuloSeleccionado.GMN1Cod = sdbgArticulos.Columns("GMNA").Value
    g_oArticuloSeleccionado.GMN2Cod = sdbgArticulos.Columns("FAM").Value
    g_oArticuloSeleccionado.GMN3Cod = sdbgArticulos.Columns("SFA").Value
    g_oArticuloSeleccionado.GMN4Cod = sdbgArticulos.Columns("SFA4").Value
    g_oArticuloSeleccionado.Generico = GridCheckToBoolean(sdbgArticulos.Columns("GENERICO").Value)
    g_oArticuloSeleccionado.Cantidad = sdbgArticulos.Columns("CANT").Value
    g_oArticuloSeleccionado.CodigoUnidad = sdbgArticulos.Columns("UNI").Value
    g_oArticuloSeleccionado.tipoRecepcion = sdbgArticulos.Columns("TIPORECEP").Value
    If sdbgArticulos.col <> -1 Then
        Select Case sdbgArticulos.Columns(sdbgArticulos.col).Name
            Case "UON_DEN"
                Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
                Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
                Dim oUnidadesOrgN3 As CUnidadesOrgNivel3
                Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
                Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
                Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
                
                oUnidadesOrgN1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.Perfil.Id
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.Perfil.Id
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.Perfil.Id
                
                Dim frm As frmSELUO
                Set frm = New frmSELUO
                frm.multiselect = True
                frm.EnableCheck = False
                frm.cargarArbol oUnidadesOrgN1, oUnidadesOrgN2, oUnidadesOrgN3
                
                
                Set oArticulo = oArticulos.Item(sdbgArticulos.Columns("ARTICULO").Text)
                
                frm.subtitulo = oArticulo.CodDen
                                    
                
                Set frm.UonsSeleccionadas = oArticulo.uons
                
                frm.Show vbModal
            
                Set frm = Nothing
            Case "AGREGADO"
                
                Set oArticulo = oArticulos.Item(sdbgArticulos.Columns("ARTICULO").Text)
                If oArticulo.isCentral Then
                    frmArtCentralUnifConfirm.ModoEdicion = False
                    frmArtCentralUnifConfirm.enableCancel = False
                    Set frmArtCentralUnifConfirm.ArticuloCentral = oArticulo
                    frmArtCentralUnifConfirm.Show vbModal
                End If
        End Select
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbgArticulos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgArticulos_DblClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    cmdSeleccionar_Click
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbgArticulos_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgArticulos_RowLoaded(ByVal Bookmark As Variant)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
         If sdbgArticulos.Columns("AGREGADO").Value = "1" Then
            sdbgArticulos.Columns("AGREGADO").CellStyleSet "Adjudica"
            sdbgArticulos.Columns("AGREGADO").Text = ""
        Else
            sdbgArticulos.Columns("AGREGADO").CellStyleSet ""
            sdbgArticulos.Columns("AGREGADO").Text = ""
        End If
        If sdbgArticulos.Columns("UON_DEN").Value = "Multiples*" Then
            sdbgArticulos.Columns("UON_DEN").Value = m_sVariasUnidades
        End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbgArticulos_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbgatributos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With sdbgAtributos
        Select Case .Columns(ColIndex).Name
            Case "VALOR"
                'Comprobar que el valor introducido se corresponde con el tipo de atributo
        If .Columns("VALOR").Text <> "" Then
            Select Case UCase(.Columns("IDTIPO").Text)
                Case TiposDeAtributos.TipoString
                Case TiposDeAtributos.TipoNumerico
                    If (Not IsNumeric(.Columns("VALOR").Value)) Then
                        MsgBox "El valor del campo Valor debe ser num�rico", vbInformation, "FULLSTEP"
                        Cancel = True
                    Else
                        If .Columns("OPER").Text <> "" And .Columns("OPER").Text = "=" Then
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                            End If
                        End If
                    End If
                    
                    If .Columns("OPER").Text = "" Then
                        .Columns("OPER").Text = "="
                        If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                            Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                        End If
                    End If
                Case TiposDeAtributos.TipoFecha
                    If (Not IsDate(.Columns("VALOR").Text) And .Columns("VALOR").Value <> "") Then
                        MsgBox "El valor del campo Valor debe ser una fecha.", vbInformation, "FULLSTEP"
                        Cancel = True
                    Else
                        If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                            If CDate(.Columns("MINIMO").Value) > CDate(.Columns("VALOR").Value) Or CDate(.Columns("MAXIMO").Value) < CDate(.Columns("VALOR").Value) Then
                                MsgBox Replace(Replace(msMsgMinMax, "@Valor1", .Columns("MINIMO").Value), "@Valor2", .Columns("MAXIMO").Value), vbInformation, "FULLSTEP"
                                Cancel = True
                            End If
                        End If
                    End If
                Case TiposDeAtributos.TipoBoolean
            End Select
        End If
            Case "OPER"
                'Comprobar si se est� intentando dejar en blanco la col. del operando teniendo un valor introducido
                If .Columns("OPER").Text = vbNullString And .Columns("VALOR").Text <> vbNullString Then
                    Cancel = True
                ElseIf .Columns("OPER").Text = "=" And .Columns("VALOR").Text <> vbNullString Then
                    Select Case UCase(.Columns("IDTIPO").Text)
                        Case TiposDeAtributos.TipoNumerico
                            If .Columns("MINIMO").Text <> "" And .Columns("MAXIMO").Text <> "" Then
                                Cancel = Not ComprobarValoresMaxMin(StrToDbl0(.Columns("VALOR").Value), StrToDbl0(.Columns("MINIMO").Value), StrToDbl0(.Columns("MAXIMO").Value))
                            End If
                    End Select
                End If
        End Select
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbgatributos_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba que un valor est� entre un m�nimo y un m�ximo</summary>
''' <remarks>Llamada desde: sdbgatributos_BeforeColUpdate</remarks>
''' <param name="dblValor">Valor</param>
''' <param name="dblMin">M�nimo</param>
''' <param name="dblMax">M�ximo</param>
''' <remarks>Tiempo m�ximo: < 1seg </remarks>

Public Function ComprobarValoresMaxMin(ByVal dblValor As Double, ByVal dblMin As Double, ByVal dblMax As Double) As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ComprobarValoresMaxMin = True
    
    If dblMin > dblValor Or dblMax < dblValor Then
        MsgBox Replace(Replace(msMsgMinMax, "@Valor1", dblMin), "@Valor2", dblMax), vbInformation, "FULLSTEP"
        ComprobarValoresMaxMin = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "ComprobarValoresMaxMin", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Function
   End If
End Function

Private Sub sdbgAtributos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    Dim bCargarCombo As Boolean
    
    'Combo de operandos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgAtributos.Columns("IDTIPO").Value = TipoNumerico Then
        sdbgAtributos.Columns("OPER").DropDownHwnd = sdbddOper.hWnd
        sdbgAtributos.Columns("OPER").Locked = False
    Else
        sdbgAtributos.Columns("OPER").DropDownHwnd = 0
        sdbgAtributos.Columns("OPER").Locked = True
    End If
    
    'Combo de valores
    bCargarCombo = False
    If sdbgAtributos.col = sdbgAtributos.Columns("VALOR").Position Then
        If sdbgAtributos.Columns("INTRO").Value Then
            bCargarCombo = True
        Else
            If sdbgAtributos.Columns("IDTIPO").Value = TipoBoolean Then
                bCargarCombo = True
            End If
        End If
    End If
    
    If bCargarCombo Then
        sdbddValor.RemoveAll
        sdbgAtributos.Columns("VALOR").DropDownHwnd = sdbddValor.hWnd
        sdbddValor.Enabled = True
        sdbddValor_DropDown
    Else
        sdbgAtributos.Columns("VALOR").DropDownHwnd = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sdbgAtributos_RowColChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub ssdbArticulosAgregados_RowLoaded(ByVal Bookmark As Variant)
Dim oUON As IUon
    Dim oatrib As CAtributo
    Dim oArticulo As CArticulo
    'celdas editables o no
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_oArticulo Is Nothing Then
        If m_oArticulo.isCentral Then
            'tomamos el art�culo agregado y la uon que se corresponden con la fila que estamos cargando
            Set oArticulo = m_oArticulo.articulosAgregados.Item(ssdbArticulosAgregados.Columns("COD").Value)
            If m_oUons.Count > 0 Then
                Set oUON = oArticulo.uons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)
                For Each oatrib In oArticulo.getAtributosUon
                    If Not oatrib.uons.CONTIENE(oUON) Then
                        ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Gris", ssdbArticulosAgregados.Row
                    Else
                        ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Normal", ssdbArticulosAgregados.Row
                    End If
                Next
            End If
        Else
            'uon de la fila
            Set oUON = m_oArticulo.uons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)
            'para cada atributo(columna atributo)
            For Each oatrib In m_oArticulo.getAtributosUon
                If Not ssdbArticulosAgregados.Columns(CStr(oatrib.Id)) Is Nothing Then
                    'si las uons del atributo contienen a la uon del art�culo puden tener valor , si no "no aplica"
                    If Not oatrib.uons.CONTIENE(oUON) Then
                        ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Gris", ssdbArticulosAgregados.Row
                    Else
                        ssdbArticulosAgregados.Columns(CStr(oatrib.Id)).CellStyleSet "Normal", ssdbArticulosAgregados.Row
                    End If
                End If
            Next
           
        End If
    End If
 
    Set oUON = Nothing
    Set oatrib = Nothing
    Set oArticulo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "ssdbArticulosAgregados_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sstabArticulos_Click(PreviousTab As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If PreviousTab = 0 And sstabArticulos.Tab = 1 Then
        If lblMaterial.caption = "" And txtCod.Text = "" And txtDen.Text = "" And Not HayFiltroAtributos And lblUon.caption = "" And Me.chkBuscarCentrales.Value = 0 And Me.chkBuscarGenericos.Value = 0 Then
            sstabArticulos.Tab = 0
            'No se ha seleccionado ningun criterio de b�squeda
            oMensajes.MensajeOKOnly oMensajes.CargarTexto(FRM_PROCEBUSCARARTICULO, 14), TipoIconoMensaje.Information
            Exit Sub
        Else
            Screen.MousePointer = vbHourglass
            CargarArticulos
            Screen.MousePointer = vbNormal
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "sstabArticulos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgArticulos.RemoveAll
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "txtCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbgArticulos.RemoveAll
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "txtDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Abre la pantalla de selecci�n de material</summary>
''' <param name="Origen">Desde qu� formulario se llama a la pantalla</param>
''' <param name="proceso">Proceso</param>
''' <param name="comprador">Comprador</param>
''' <param name="multimaterial">Multimaterial</param>
''' <param name="responsable">Responsable</param>
''' <remarks>Llamada desde: cmdSelMat_Click</remarks>

Public Sub SeleccionarMaterial(ByVal Origen As String, ByVal proceso As CProceso, Optional ByVal comprador As Boolean, Optional ByVal multimaterial As Boolean = False, Optional ByVal responsable As Boolean)
    Dim restriccion As Boolean
    Dim oPlantillas As CPlantillas
    Dim oPlantillaSeleccionada As CPlantilla

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    restriccion = False
    
    If Not proceso Is Nothing Then
        If Not IsEmpty(proceso.Plantilla) Then
            If Not IsNull(proceso.Plantilla) Then
                Set oPlantillas = oFSGSRaiz.Generar_CPlantillas
                oPlantillas.CargarTodasLasPlantillas proceso.Plantilla, False
                Set oPlantillaSeleccionada = oPlantillas.Item(CStr(proceso.Plantilla))
                If oPlantillaSeleccionada.ExisteRestriccionMaterial Then
                    restriccion = True
                End If
            End If
        End If
    End If
    
    ' si la plantilla tiene plantilla con restriccion de material abrir nuevo formulario de restriccion de material

    If restriccion Then
        Set frmSELMATPlantilla.oPlantillaSeleccionada = oPlantillaSeleccionada
        frmSELMATPlantilla.sOrigen = Origen
        If Not IsEmpty(comprador) Then
            frmSELMATPlantilla.bRComprador = comprador
        End If
        frmSELMATPlantilla.bRestringirSoloMaterialesComprador = g_bRMat
        frmSELMATPlantilla.Show 1
    Else
        frmSELMAT.bPermProcMultiMaterial = multimaterial
        frmSELMAT.sOrigen = Origen
        If Not IsEmpty(comprador) Then
            frmSELMAT.bRComprador = comprador
        End If
        Set frmSELMAT.oProcesoSeleccionado = proceso
        frmSELMAT.bRCompResponsable = responsable
        frmSELMAT.g_bParaPedido = g_bParaPedido
        frmSELMAT.Show 1
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "SeleccionarMaterial", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

'edu pm98
'si llegan argumentos hacer abstraccion del nombre del formulario
'una vez depurada esta funcion debe sustituir a PonerMatSeleccionadoEnCombos

Public Sub PonerMatSeleccionadoNew(Optional oGMN4Seleccionado As CGrupoMatNivel4)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If IsMissing(oGMN4Seleccionado) Or IsEmpty(oGMN4Seleccionado) Then
        Set g_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    Else
        Set g_oGMN4Seleccionado = oGMN4Seleccionado
    End If
    
    If Not g_oGMN4Seleccionado Is Nothing Then
        lblMaterial.caption = g_oGMN4Seleccionado.GMN1Cod & " - " & g_oGMN4Seleccionado.GMN2Cod & " - " & g_oGMN4Seleccionado.GMN3Cod & "-" & g_oGMN4Seleccionado.Cod & " - " & g_oGMN4Seleccionado.Den
        txtCod.Text = ""
        txtDen.Text = ""
        
        msGMN1Cod = g_oGMN4Seleccionado.GMN1Cod
        msGMN2Cod = g_oGMN4Seleccionado.GMN2Cod
        msGMN3Cod = g_oGMN4Seleccionado.GMN3Cod
        msGMN4Cod = g_oGMN4Seleccionado.Cod
        msGMN4Den = g_oGMN4Seleccionado.Den
    Else
        Exit Sub
    End If
    'Me.Show 1
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "PonerMatSeleccionadoNew", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Public Sub cargarGridArticulosAgregados()
    
    Dim oAtributo As CAtributo
    Dim oArticuloAgr As CArticulo
    Dim sLinea As String
    Dim oUON As IUon
    Dim oUonDistribuida As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oUonsDistribuidas = g_oProcesoSeleccionado.getUonsDistribuidas(g_oItemSeleccionado.Id)
    
    If Not m_oArticulo Is Nothing Then
        If m_oArticulo.isCentral Then
            Set m_oArticulosAgregados = Nothing
            Set m_oArticulosAgregados = m_oArticulo.articulosAgregados
            
            ssdbArticulosAgregados.RemoveAll
            'caso 1: El art�culo est� por debajo de la uon distribuida
            For Each oArticuloAgr In m_oArticulosAgregados
                For Each oUON In oArticuloAgr.uons
                    If m_oUonsDistribuidas.CONTIENE(oUON) Then
                        m_oArticulos.addArticulo oArticuloAgr
                        If Not m_oUons.existe(oUON.key) Then
                            m_oUons.Add oUON, oUON.key
                            sLinea = oUON.titulo & Chr(lSeparador) 'Codigo de unidadorganizativa del articulo
                            sLinea = sLinea & oArticuloAgr.Cod & Chr(lSeparador) ' codigo de art�culo de planta
                            sLinea = sLinea & oUON.key ' codificacion de uon
                            ssdbArticulosAgregados.AddItem sLinea
                        End If
                    End If
                Next
            Next
            'caso 2: el art�culo est� por encima de la uon distribuida
            
            For Each oArticuloAgr In m_oArticulosAgregados
                For Each oUonDistribuida In m_oUonsDistribuidas
                    For Each oUON In oArticuloAgr.uons
                        If oUON.incluye(oUonDistribuida) And Not oUonDistribuida.equals(oUON) Then
                            If Not m_oUons.existe(oUON.key) Then
                                m_oArticulos.addArticulo oArticuloAgr
                                m_oUons.Add oUON
                                sLinea = oUON.titulo & Chr(lSeparador) 'Codigo de unidadorganizativa del articulo
                                sLinea = sLinea & oArticuloAgr.Cod & Chr(lSeparador) ' codigo de art�culo de planta
                                sLinea = sLinea & oUON.key ' codificacion de uon
                                ssdbArticulosAgregados.AddItem sLinea
                            End If
                        End If
                    Next
                Next
            Next
        Else
            'caso 1: el art�culo est� por debajo de la uon distribuida
            For Each oUON In m_oArticulo.uons
                If m_oUonsDistribuidas.CONTIENE(oUON) Then
                    If Not m_oUons.existe(oUON.key) Then
                        m_oUons.Add oUON
                        sLinea = oUON.titulo & Chr(lSeparador)
                        sLinea = sLinea & "" & Chr(lSeparador) 'La columna de codigo de planta se deja vac�a y luego se oculta
                        sLinea = sLinea & oUON.key ' codificacion de uon
                        ssdbArticulosAgregados.AddItem sLinea
                    End If
                End If
            Next
            'caso 2: el art�culo est� por debajo de la uon distribuida
            
            For Each oUonDistribuida In m_oUonsDistribuidas
                For Each oUON In m_oArticulo.uons
                    If oUON.incluye(oUonDistribuida) And Not oUON.equals(oUonDistribuida) Then 'no igual para evitar repetidos en caso de que coincidan
                        If Not m_oUons.existe(oUON.key) Then
                            m_oUons.Add oUON
                            sLinea = oUON.titulo & Chr(lSeparador)
                            sLinea = sLinea & "" & Chr(lSeparador) 'La columna de codigo de planta se deja vac�a y luego se oculta
                            sLinea = sLinea & oUON.key ' codificacion de uon
                            ssdbArticulosAgregados.AddItem sLinea
                        End If
                    End If
                Next
            Next
            
        End If
    End If
    
    Set oUON = Nothing
    Set oUonDistribuida = Nothing
    Set oAtributo = Nothing
    Set oArticuloAgr = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cargarGridArticulosAgregados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function cargarUonsRestringidas(ByRef uons1 As CUnidadesOrgNivel1, ByRef uons2 As CUnidadesOrgNivel2, ByRef uons3 As CUnidadesOrgNivel3)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    uons1.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.Perfil.Id, , , False
    uons2.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.Perfil.Id, , , False
    uons3.CargarTodasLasUnidadesOrgUsuPerf basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRestrDistrUsu, m_bRestrDistrPerf, oUsuarioSummit.Perfil.Id, , False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cargarUonsRestringidas", err, Erl, , m_bActivado)
      Exit Function
   End If
        
End Function

Private Sub montarColumnasGridAtributosUon()
    Dim icolumns As Integer
    Dim oArticulo As CArticulo
    Dim oColumn As SSDataWidgets_B.Column
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    icolumns = ssdbArticulosAgregados.Columns.Count
    Dim oAtributo As CAtributo
    If Not m_oArticulo Is Nothing Then
        If m_oArticulo.isCentral Then
            For Each oArticulo In m_oArticulo.articulosAgregados
                For Each oAtributo In oArticulo.getAtributosUon
                    If ssdbArticulosAgregados.Columns(CStr(oAtributo.Id)) Is Nothing Then
                        ssdbArticulosAgregados.Columns.Add icolumns
                        Set oColumn = ssdbArticulosAgregados.Columns(icolumns)
                        oColumn.Name = oAtributo.Id
                        oColumn.caption = oAtributo.Den
                        oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                        oColumn.Alignment = ssCaptionAlignmentLeft
                        oColumn.Style = ssStyleEdit
                        oColumn.Locked = True
                        oColumn.FieldLen = 100
                        icolumns = icolumns + 1
                        m_oAtributos.addAtributo oAtributo
                        Set oColumn = Nothing
                    End If
                Next
            Next
        Else
            For Each oAtributo In m_oArticulo.getAtributosUon
                If Not m_oAtributos.existe(oAtributo.Id) Then
                    ssdbArticulosAgregados.Columns.Add icolumns
                    Set oColumn = ssdbArticulosAgregados.Columns(icolumns)
                    oColumn.Name = oAtributo.Id
                    oColumn.caption = oAtributo.Den
                    oColumn.CaptionAlignment = ssColCapAlignLeftJustify
                    oColumn.Alignment = ssCaptionAlignmentLeft
                    oColumn.Style = ssStyleEdit
                    oColumn.Locked = True
                    oColumn.FieldLen = 100
                    m_oAtributos.addAtributo oAtributo
                    icolumns = icolumns + 1
                    Set oColumn = Nothing
                End If
            Next
        End If
    End If
    Set oAtributo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "montarColumnasGridAtributosUon", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub cargarValoresGridArticulosAgregados()
    Dim oAtributo As CAtributo
    Dim oAtributos As CAtributos
    Dim oArticulo As CArticulo
    Dim i, j As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    LockWindowUpdate Me.hWnd
    If Not m_oArticulo Is Nothing Then
        If m_oArticulo.isCentral Then
            Me.ssdbArticulosAgregados.MoveFirst
            For i = 0 To ssdbArticulosAgregados.Rows - 1
                Set oArticulo = m_oArticulo.articulosAgregados.Item(ssdbArticulosAgregados.Columns("COD").Value)
                Set oAtributos = oArticulo.AtributosUonValorados
                For j = 3 To ssdbArticulosAgregados.Cols - 1
                    If oAtributos.existe(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value) Then
                        Set oAtributo = oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value)
                        If oAtributo.Tipo = TipoBoolean Then
                            If oAtributo.getValor = 1 Then
                                ssdbArticulosAgregados.Columns(j).Value = msTrue
                            Else
                                ssdbArticulosAgregados.Columns(j).Value = msFalse
                            End If
                        Else
                            ssdbArticulosAgregados.Columns(j).Value = oAtributo.getValor
                        End If
                    Else
                        'si no existe le damos el valor por defecto (guardado en atributosuon)
                        Set oAtributo = m_oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name)
                        If oAtributo.uons.CONTIENE(m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)) Then
                            If oAtributo.Tipo = TipoBoolean Then
                                If oAtributo.getValor = 1 Then
                                    ssdbArticulosAgregados.Columns(j).Text = msTrue
                                Else
                                    ssdbArticulosAgregados.Columns(j).Text = msFalse
                                End If
                            Else
                                ssdbArticulosAgregados.Columns(j).Text = NullToStr(oAtributo.getValor)
                            End If
                        End If
                        Set oAtributo = Nothing
                    End If
                    
                Next
                
                ssdbArticulosAgregados.MoveNext
            Next
        Else
            Me.ssdbArticulosAgregados.MoveFirst
            Set oAtributos = m_oArticulo.AtributosUonValorados
            For i = 0 To ssdbArticulosAgregados.Rows - 1
                
                For j = 3 To ssdbArticulosAgregados.Cols - 1
                    If oAtributos.existe(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value) Then
                        Set oAtributo = oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name & " " & ssdbArticulosAgregados.Columns("UONKEY").Value)
                        If oAtributo.Tipo = TipoBoolean Then
                            If oAtributo.getValor = 1 Then
                                ssdbArticulosAgregados.Columns(j).Value = msTrue
                            Else
                                ssdbArticulosAgregados.Columns(j).Value = msFalse
                            End If
                        Else
                            ssdbArticulosAgregados.Columns(j).Value = oAtributo.getValor
                        End If
                    Else
                        'si no existe le damos el valor por defecto (guardado en atributosuon)
                        Set oAtributo = m_oAtributos.Item(ssdbArticulosAgregados.Columns(j).Name)
                        If oAtributo.uons.CONTIENE(m_oUons.Item(ssdbArticulosAgregados.Columns("UONKEY").Value)) Then
                            If oAtributo.Tipo = TipoBoolean Then
                                If oAtributo.getValor = 1 Then
                                    ssdbArticulosAgregados.Columns(j).Text = msTrue
                                Else
                                    ssdbArticulosAgregados.Columns(j).Text = msFalse
                                End If
                            Else
                                ssdbArticulosAgregados.Columns(j).Text = NullToStr(oAtributo.getValor)
                            End If
                        End If
                        Set oAtributo = Nothing
                    End If
                    
                Next
                
                ssdbArticulosAgregados.MoveNext
            Next
        End If
    End If
    ssdbArticulosAgregados.MoveFirst
    LockWindowUpdate 0&
    Set oAtributo = Nothing
    Set oAtributos = Nothing
    Set oArticulo = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "cargarValoresGridArticulosAgregados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub


Private Sub lblUon_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_oUonsSeleccionadas.Count > 1 Then
        lstMultiUon.Height = m_oUonsSeleccionadas.Count * (lblUon.Height - 30)
        lstMultiUon.Visible = True
        lstMultiUon.ZOrder 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "lblUon_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub lstMultiUon_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
        Dim lWnd As Long
    
    'El c�digo siguiente imita el MouseLeave
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lWnd = GetCapture

    If lWnd <> lstMultiUon.hWnd Then
        'Dentro de lstMultiMaterial
        SetCapture lstMultiUon.hWnd
    Else
'        If (X < 0) Or (X > Me.ScaleX(lstMultiMaterial.Width, vbPixels, vbTwips)) Or _
'            (Y < 0) Or (Y > Me.ScaleY(lstMultiMaterial.Height, vbPixels, vbTwips)) Then
        If (X < 0) Or (X > lstMultiUon.Width) Or _
            (Y < 0) Or (Y > lstMultiUon.Height) Then
            'Fuera de lstMultiMaterial
            ReleaseCapture
            lstMultiUon.Visible = False
            lstMultiUon.ZOrder 1
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "lstMultiUon_MouseMove", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Public Function addAtributo(ByRef oAtributo As CAtributo) As CAtributo
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not oAtribs.existe(oAtributo.Id) Then
        Set addAtributo = oAtribs.addAtributo(oAtributo)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmPROCEBuscarArticulo", "addAtributo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

