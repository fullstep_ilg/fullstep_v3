VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstSolicitud 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DListado de solicitudes"
   ClientHeight    =   6720
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8835
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstSolicitud.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6720
   ScaleWidth      =   8835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdObtener 
      Caption         =   "&Obtener"
      Height          =   435
      Left            =   7410
      TabIndex        =   33
      Top             =   6270
      Width           =   1350
   End
   Begin TabDlg.SSTab stabSolicitud 
      Height          =   6165
      Left            =   0
      TabIndex        =   34
      Top             =   0
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   10874
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "DSelecci�n"
      TabPicture(0)   =   "frmLstSolicitud.frx":0CB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Timer1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraSeleccion"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "DOpciones"
      TabPicture(1)   =   "frmLstSolicitud.frx":0CCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraOpciones"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraSeleccion 
         Height          =   5670
         Left            =   120
         TabIndex        =   36
         Top             =   420
         Width           =   8560
         Begin VB.PictureBox picNumSolicitErp 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   90
            ScaleHeight     =   285
            ScaleWidth      =   6975
            TabIndex        =   70
            Top             =   3600
            Width           =   6975
            Begin VB.TextBox txtNumSolicErp 
               Height          =   285
               Left            =   1350
               TabIndex        =   72
               Top             =   0
               Width           =   5235
            End
            Begin VB.CommandButton cmdAyuda 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   7.5
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   6750
               Picture         =   "frmLstSolicitud.frx":0CEA
               Style           =   1  'Graphical
               TabIndex        =   71
               Top             =   0
               Width           =   225
            End
            Begin VB.Label lblNumSolicitErp 
               AutoSize        =   -1  'True
               Caption         =   "DN� solicit. ERP"
               Height          =   195
               Left            =   30
               TabIndex        =   73
               Top             =   30
               Width           =   1110
            End
         End
         Begin VB.ListBox lstCentroCoste 
            Height          =   300
            IntegralHeight  =   0   'False
            Left            =   1575
            TabIndex        =   69
            Top             =   2955
            Visible         =   0   'False
            Width           =   4935
         End
         Begin VB.ListBox lstContrato 
            Height          =   270
            IntegralHeight  =   0   'False
            Left            =   1725
            TabIndex        =   68
            Top             =   3375
            Visible         =   0   'False
            Width           =   4950
         End
         Begin VB.PictureBox picDebajoDePartidas 
            BorderStyle     =   0  'None
            Height          =   1590
            Left            =   30
            ScaleHeight     =   1590
            ScaleWidth      =   8370
            TabIndex        =   62
            Top             =   3990
            Width           =   8370
            Begin VB.CommandButton cmdCalFecHasta 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   5550
               Picture         =   "frmLstSolicitud.frx":0F1B
               Style           =   1  'Graphical
               TabIndex        =   24
               TabStop         =   0   'False
               Top             =   30
               Width           =   315
            End
            Begin VB.CommandButton cmdCalFecDesde 
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2565
               Picture         =   "frmLstSolicitud.frx":14A5
               Style           =   1  'Graphical
               TabIndex        =   22
               TabStop         =   0   'False
               Top             =   30
               Width           =   315
            End
            Begin VB.TextBox txtFecHasta 
               Height          =   285
               Left            =   4380
               TabIndex        =   23
               Top             =   30
               Width           =   1110
            End
            Begin VB.TextBox txtFecDesde 
               Height          =   285
               Left            =   1410
               TabIndex        =   21
               Top             =   30
               Width           =   1110
            End
            Begin VB.CheckBox chkEstado 
               Caption         =   "DPendientes"
               Height          =   255
               Index           =   0
               Left            =   90
               TabIndex        =   29
               Top             =   1035
               Value           =   1  'Checked
               Width           =   3540
            End
            Begin VB.CheckBox chkEstado 
               Caption         =   "DAprobadas"
               Height          =   255
               Index           =   1
               Left            =   90
               TabIndex        =   31
               Top             =   1275
               Width           =   3540
            End
            Begin VB.CheckBox chkEstado 
               Caption         =   "DAnuladas"
               Height          =   255
               Index           =   3
               Left            =   3810
               TabIndex        =   30
               Top             =   1035
               Width           =   3540
            End
            Begin VB.CheckBox chkEstado 
               Caption         =   "DRechazadas"
               Height          =   255
               Index           =   2
               Left            =   3810
               TabIndex        =   28
               Top             =   795
               Width           =   3540
            End
            Begin VB.CheckBox chkEstado 
               Caption         =   "DCerradas"
               Height          =   255
               Index           =   4
               Left            =   3810
               TabIndex        =   32
               Top             =   1275
               Width           =   3540
            End
            Begin VB.CheckBox chkEstado 
               Caption         =   "DEn Curso"
               Height          =   255
               Index           =   5
               Left            =   90
               TabIndex        =   27
               Top             =   795
               Width           =   3540
            End
            Begin VB.TextBox txtImporteHasta 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   4380
               TabIndex        =   26
               Top             =   375
               Width           =   1485
            End
            Begin VB.TextBox txtImporteDesde 
               Alignment       =   1  'Right Justify
               Height          =   285
               Left            =   1410
               TabIndex        =   25
               Top             =   390
               Width           =   1485
            End
            Begin VB.Label lblMon 
               AutoSize        =   -1  'True
               Caption         =   "MONCEN"
               Height          =   195
               Left            =   5940
               TabIndex        =   67
               Top             =   420
               Width           =   645
            End
            Begin VB.Label lblFecHasta 
               AutoSize        =   -1  'True
               Caption         =   "Hasta:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   3090
               TabIndex        =   66
               Top             =   45
               Width           =   480
            End
            Begin VB.Label lblFecDesde 
               AutoSize        =   -1  'True
               Caption         =   "Desde:"
               ForeColor       =   &H00000000&
               Height          =   195
               Left            =   90
               TabIndex        =   65
               Top             =   45
               Width           =   510
            End
            Begin VB.Label lblImporteDesde 
               AutoSize        =   -1  'True
               Caption         =   "DImporte desde:"
               Height          =   195
               Left            =   90
               TabIndex        =   64
               Top             =   420
               Width           =   1215
            End
            Begin VB.Label lblImporteHasta 
               AutoSize        =   -1  'True
               Caption         =   "DImporte hasta:"
               Height          =   195
               Left            =   3090
               TabIndex        =   63
               Top             =   420
               Width           =   1185
            End
         End
         Begin VB.CommandButton cmdBuscarContrato 
            Height          =   285
            Index           =   0
            Left            =   6780
            Picture         =   "frmLstSolicitud.frx":1A2F
            Style           =   1  'Graphical
            TabIndex        =   20
            Top             =   3240
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarContrato 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   0
            Left            =   6420
            Picture         =   "frmLstSolicitud.frx":1DB1
            Style           =   1  'Graphical
            TabIndex        =   19
            Top             =   3240
            Width           =   315
         End
         Begin VB.TextBox txtContrato 
            Height          =   285
            Index           =   0
            Left            =   1440
            TabIndex        =   18
            Top             =   3240
            Width           =   4950
         End
         Begin VB.CommandButton cmdBuscarCentroCoste 
            Height          =   285
            Left            =   6780
            Picture         =   "frmLstSolicitud.frx":1E56
            Style           =   1  'Graphical
            TabIndex        =   17
            Top             =   2880
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarCentroCoste 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6420
            Picture         =   "frmLstSolicitud.frx":21D8
            Style           =   1  'Graphical
            TabIndex        =   16
            Top             =   2880
            Width           =   315
         End
         Begin VB.TextBox txtCentroCoste 
            Height          =   285
            Left            =   1440
            TabIndex        =   15
            Top             =   2880
            Width           =   4950
         End
         Begin VB.TextBox txtDenArticulo 
            Height          =   285
            Left            =   3570
            TabIndex        =   14
            Top             =   2520
            Width           =   3540
         End
         Begin VB.CommandButton cmdBuscarMateriales 
            Height          =   285
            Left            =   6780
            Picture         =   "frmLstSolicitud.frx":227D
            Style           =   1  'Graphical
            TabIndex        =   12
            Top             =   2160
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarMateriales 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6420
            Picture         =   "frmLstSolicitud.frx":25FF
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   2160
            Width           =   315
         End
         Begin VB.TextBox txtMateriales 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   10
            Top             =   2160
            Width           =   4950
         End
         Begin VB.CommandButton cmdBuscarUO 
            Height          =   285
            Left            =   3825
            Picture         =   "frmLstSolicitud.frx":26A4
            Style           =   1  'Graphical
            TabIndex        =   7
            Top             =   1440
            Width           =   315
         End
         Begin VB.CommandButton cmdLimpiarUO 
            Height          =   285
            Left            =   3465
            Picture         =   "frmLstSolicitud.frx":2A26
            Style           =   1  'Graphical
            TabIndex        =   6
            Top             =   1440
            Width           =   315
         End
         Begin VB.TextBox txtUO 
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1440
            Locked          =   -1  'True
            TabIndex        =   5
            Top             =   1425
            Width           =   1995
         End
         Begin VB.TextBox txtId 
            Height          =   285
            Left            =   1440
            TabIndex        =   1
            Top             =   645
            Width           =   1300
         End
         Begin VB.TextBox txtDescr 
            Height          =   285
            Left            =   4005
            TabIndex        =   2
            Top             =   645
            Width           =   4455
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcPeticionario 
            Height          =   285
            Left            =   1440
            TabIndex        =   3
            Top             =   1050
            Width           =   2715
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4895
            Columns(1).Caption=   "PET"
            Columns(1).Name =   "PET"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4789
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcTipoSolicit 
            Height          =   285
            Left            =   1440
            TabIndex        =   0
            Top             =   240
            Width           =   7065
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   2249
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   9419
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "ID"
            Columns(2).Name =   "ID"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   12462
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEmpresa 
            Height          =   285
            Left            =   1440
            TabIndex        =   9
            Top             =   1800
            Width           =   5670
            DataFieldList   =   "Column 1"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   4
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "ID"
            Columns(0).Name =   "ID"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2593
            Columns(1).Caption=   "NIF"
            Columns(1).Name =   "NIF"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   9022
            Columns(2).Caption=   "DEN"
            Columns(2).Name =   "DEN"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "NIF_DEN"
            Columns(3).Name =   "NIF_DEN"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            _ExtentX        =   10001
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArticulo 
            Height          =   285
            Left            =   1440
            TabIndex        =   13
            Top             =   2520
            Width           =   2055
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4630
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3625
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B.SSDBGrid sdbgPartidas 
            Height          =   855
            Left            =   3390
            TabIndex        =   59
            Top             =   2505
            Visible         =   0   'False
            Width           =   5775
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            Col.Count       =   11
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   0
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            ExtraHeight     =   265
            Columns.Count   =   11
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).Locked=   -1  'True
            Columns(1).Width=   2381
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).Locked=   -1  'True
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   12632256
            Columns(2).Width=   6694
            Columns(2).Caption=   "VALOR"
            Columns(2).Name =   "VALOR"
            Columns(2).AllowSizing=   0   'False
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(2).Style=   1
            Columns(2).ButtonsAlways=   -1  'True
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "NIVEL"
            Columns(3).Name =   "NIVEL"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "PRES5"
            Columns(4).Name =   "PRES5NIV1"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            Columns(5).Width=   3200
            Columns(5).Visible=   0   'False
            Columns(5).Caption=   "PRES5UON"
            Columns(5).Name =   "PRES5UON"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "VALIDADO"
            Columns(6).Name =   "VALIDADO"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            Columns(7).Width=   3200
            Columns(7).Visible=   0   'False
            Columns(7).Caption=   "PRES5NIV2"
            Columns(7).Name =   "PRES5NIV2"
            Columns(7).DataField=   "Column 7"
            Columns(7).DataType=   8
            Columns(7).FieldLen=   256
            Columns(8).Width=   3200
            Columns(8).Visible=   0   'False
            Columns(8).Caption=   "PRES5NIV3"
            Columns(8).Name =   "PRES5NIV3"
            Columns(8).DataField=   "Column 8"
            Columns(8).DataType=   8
            Columns(8).FieldLen=   256
            Columns(9).Width=   3200
            Columns(9).Visible=   0   'False
            Columns(9).Caption=   "PRES5NIV4"
            Columns(9).Name =   "PRES5NIV4"
            Columns(9).DataField=   "Column 9"
            Columns(9).DataType=   8
            Columns(9).FieldLen=   256
            Columns(10).Width=   3200
            Columns(10).Visible=   0   'False
            Columns(10).Caption=   "TAG"
            Columns(10).Name=   "TAG"
            Columns(10).DataField=   "Column 10"
            Columns(10).DataType=   8
            Columns(10).FieldLen=   256
            _ExtentX        =   10186
            _ExtentY        =   1508
            _StockProps     =   79
            BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcComprador 
            Height          =   285
            Left            =   5640
            TabIndex        =   4
            Top             =   1050
            Width           =   2850
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            HeadLines       =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   11192
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5027
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcDepartamento 
            Height          =   285
            Left            =   5640
            TabIndex        =   8
            Top             =   1425
            Width           =   2850
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2619
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7197
            Columns(1).Caption=   "DEN"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5027
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblComprador 
            AutoSize        =   -1  'True
            Caption         =   "DComprador:"
            Height          =   195
            Left            =   4320
            TabIndex        =   61
            Top             =   1080
            Width           =   960
         End
         Begin VB.Label lblDepartamento 
            AutoSize        =   -1  'True
            Caption         =   "DDepartamento:"
            Height          =   195
            Left            =   4320
            TabIndex        =   60
            Top             =   1485
            Width           =   1200
         End
         Begin VB.Label lblContrato 
            AutoSize        =   -1  'True
            Caption         =   "DContrato:"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   58
            Top             =   3285
            Width           =   810
         End
         Begin VB.Label lblCentroCoste 
            AutoSize        =   -1  'True
            Caption         =   "DCentro coste:"
            Height          =   195
            Left            =   120
            TabIndex        =   57
            Top             =   2925
            Width           =   1095
         End
         Begin VB.Label lblArticulo 
            AutoSize        =   -1  'True
            Caption         =   "DArticulo:"
            Height          =   195
            Left            =   120
            TabIndex        =   56
            Top             =   2565
            Width           =   705
         End
         Begin VB.Label lblEmpresa 
            AutoSize        =   -1  'True
            Caption         =   "DEmpresa:"
            Height          =   195
            Left            =   120
            TabIndex        =   55
            Top             =   1845
            Width           =   780
         End
         Begin VB.Label lblMateriales 
            AutoSize        =   -1  'True
            Caption         =   "DMateriales:"
            Height          =   195
            Left            =   120
            TabIndex        =   54
            Top             =   2205
            Width           =   900
         End
         Begin VB.Label lblUO 
            Caption         =   "UO: "
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   120
            TabIndex        =   51
            Top             =   1455
            Width           =   480
         End
         Begin VB.Label lblTipoSolicit 
            Caption         =   "Dtipo de solicitud:"
            Height          =   195
            Left            =   120
            TabIndex        =   50
            Top             =   270
            Width           =   1350
         End
         Begin VB.Label lblIdentificador 
            AutoSize        =   -1  'True
            Caption         =   "DIdentificador: "
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   120
            TabIndex        =   39
            Top             =   675
            Width           =   1125
         End
         Begin VB.Label lblPeticionario 
            AutoSize        =   -1  'True
            Caption         =   "DPeticionario : "
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   120
            TabIndex        =   38
            Top             =   1080
            Width           =   1080
         End
         Begin VB.Label lblDescripcion 
            AutoSize        =   -1  'True
            Caption         =   "DDescripci�n: "
            ForeColor       =   &H00000000&
            Height          =   195
            Left            =   2925
            TabIndex        =   37
            Top             =   675
            Width           =   1020
         End
      End
      Begin VB.Frame fraOpciones 
         Height          =   2895
         Left            =   -74880
         TabIndex        =   35
         Top             =   420
         Width           =   8535
         Begin VB.CheckBox chkDetalle 
            Caption         =   "DListado detallado"
            Height          =   375
            Left            =   120
            TabIndex        =   49
            Top             =   240
            Width           =   2535
         End
         Begin VB.Frame fraOrden 
            Caption         =   "DOrden"
            Height          =   2175
            Left            =   2880
            TabIndex        =   40
            Top             =   240
            Width           =   5535
            Begin VB.OptionButton optOrden 
               Caption         =   "DDepartamento"
               Height          =   255
               Index           =   9
               Left            =   3960
               TabIndex        =   53
               Top             =   720
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DPeticionario"
               Height          =   255
               Index           =   8
               Left            =   3960
               TabIndex        =   52
               Top             =   240
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DAlta"
               Height          =   255
               Index           =   0
               Left            =   120
               TabIndex        =   48
               Top             =   240
               Value           =   -1  'True
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DNecesidad"
               Height          =   255
               Index           =   1
               Left            =   2040
               TabIndex        =   47
               Top             =   240
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DIdentificador"
               Height          =   255
               Index           =   2
               Left            =   120
               TabIndex        =   46
               Top             =   720
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DDescripci�n breve"
               Height          =   255
               Index           =   3
               Left            =   2040
               TabIndex        =   45
               Top             =   720
               Width           =   2055
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DImporte"
               Height          =   255
               Index           =   4
               Left            =   2040
               TabIndex        =   44
               Top             =   1200
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DPeticionario"
               Height          =   255
               Index           =   5
               Left            =   120
               TabIndex        =   43
               Top             =   1680
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DComprador"
               Height          =   255
               Index           =   6
               Left            =   2040
               TabIndex        =   42
               Top             =   1680
               Width           =   1455
            End
            Begin VB.OptionButton optOrden 
               Caption         =   "DEstado"
               Height          =   255
               Index           =   7
               Left            =   120
               TabIndex        =   41
               Top             =   1200
               Width           =   1455
            End
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   4335
         Top             =   195
      End
   End
End
Attribute VB_Name = "frmLstSolicitud"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables p�blicas
Public g_sOrigen As String
Public g_sOrigen2 As String
'Para el filtro de UO
Public sUON1 As String
Public sUON2 As String
Public sUON3 As String

Public g_oCenCos As CCentroCoste
Public g_oPartida As CContratopres

'filtro de materiales
Private m_sGMN1Cod As String
Private m_sGMN2Cod As String
Private m_sGMN3Cod As String
Private m_sGMN4Cod As String

'Idiomas para el report
Private m_txtComprador As String
Private m_txtPet As String
Private m_txtImporte As String
Private m_txtFecNec As String
Private m_txtFecAlta As String
Private m_txtDen As String
Private m_txtId As String
Private m_txtSeleccion As String
Private m_txtDe As String
Private m_txtPag As String
Private m_txtTitulo As String
Private m_txtEstado As String
Private m_txtUO As String
Private m_txtDepartamento As String
Private m_txtCargo As String
Private m_txtProv As String
Private m_txtDest As String
Private m_txtDescr As String
Private m_txtAprobador As String
Private m_txtFecAprob As String
Private m_txtSi As String
Private m_txtNo As String
Private m_sEspera(3) As String
Private m_stxtTipo As String

Private m_stxtMateriales As String
Private m_stxtArticulo As String
Private m_stxtImporteDesde As String
Private m_stxtImporteHasta As String
Private m_stxtCentroCoste As String
Private m_stxtEmpresa As String
Private m_stxtComprador As String
Private m_stxtNumSolERP As String

Private m_sSubFicAdjuntos(1 To 5) As String
Private m_sSubProcesos(1 To 9) As String
Private m_sSubCatalogos(1 To 14) As String
Private m_sSubPedidos(1 To 10) As String
Private m_sIdiEst(1 To 10) As String
Private m_sIdiEstPedido(1 To 10) As String

'Variables de idiomas
Private m_sFecDesde As String
Private m_sFecHasta As String
Private m_sPendiente As String
Private m_sAprobada As String
Private m_sRechazada As String
Private m_sEnCursoRechazada As String
Private m_sAnulada As String
Private m_sCerrada As String
Private m_sEnCurso(2) As String

'Seguridad
Private m_bRuo As Boolean
Private m_bREquipo As Boolean
Private m_bRAsig As Boolean
Private m_bRDep As Boolean
Private m_bVerEnCurso As Boolean
Private m_bRestrMatComp As Boolean
Private m_bRPerfUON As Boolean

Private m_bRespetarCombo As Boolean

'Para el filtro por materiales/articulos
Private m_bEditandoArticulo As Boolean
Private m_bEditandoArticuloConDropDown As Boolean
Private m_bEditandoArticuloDen As Boolean
Private m_oGMN4Seleccionado As CGrupoMatNivel4

Private m_sIncrementoAltura As Single

'Para el filtro de centros de centro coste
Private m_sUON1_CC As String
Private m_sUON2_CC As String
Private m_sUON3_CC As String
Private m_sUON4_CC As String
Private m_sUON_CC As String
Private m_bCentroCosteValidado As Boolean
Private m_oCentrosCoste As CCentrosCoste

'Para el filtro de partidas
Private m_asPres5Niv0() As String
Private m_aiNivelImp() As Integer
Private m_asPres5UON() As String
Private m_aPres5() As TipoConfigVisorSolPP
Private m_bPres5Validado As Boolean
Private m_oContratosPres As CContratosPres

'Control de Errores
Public m_bDescargarFrm As Boolean
Private m_bActivado As Boolean
Private m_sMsgError As String


Private Sub cmdAyuda_Click()
    MostrarFormPROCEAyuda oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma
End Sub

Private Sub chkEstado_Click(Index As Integer)
    Dim i As Integer
    Dim bCancelar As Boolean
    
    'cuando se deschequea se comprueba si existe alg�n check m�s marcado.En caso contrario
    'no permite quitar el check
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkEstado(Index).Value = vbUnchecked Then
        bCancelar = True
        For i = 0 To chkEstado.Count - 1
            If chkEstado(i).Value = vbChecked Then
                bCancelar = False
                Exit For
            End If
        Next i
        If bCancelar = True Then
            chkEstado(Index).Value = vbChecked
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "chkEstado_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBuscarCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCenCoste.g_sOrigen = "frmLstSolicitud"
    frmSelCenCoste.g_bSaltarComprobacionArbol = True
    frmSelCenCoste.Show vbModal
    If Not g_oCenCos Is Nothing Then
        If Len(g_oCenCos.Cod) > 0 Then
            If Len(g_oCenCos.CodConcat) > 0 Then
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den & " (" & g_oCenCos.CodConcat & ")"
            Else
                txtCentroCoste.Text = g_oCenCos.Cod & " - " & g_oCenCos.Den
            End If
            txtCentroCoste.Tag = g_oCenCos.Cod
            m_bCentroCosteValidado = True
            
            m_sUON1_CC = g_oCenCos.UON1
            m_sUON2_CC = g_oCenCos.UON2
            m_sUON3_CC = g_oCenCos.UON3
            m_sUON4_CC = g_oCenCos.UON4
            
            m_sUON_CC = FormatearUON(g_oCenCos)
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdBuscarCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Function FormatearPres5o(ByVal sPres5Niv0 As String, ByVal Partida As CContratopres) As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    FormatearPres5o = FormatearPres5s(sPres5Niv0, Partida.Pres1, Partida.Pres2, Partida.Pres3, Partida.Pres4)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "FormatearPres5o", err, Erl, , m_bActivado, m_sMsgError)
      Exit Function
   End If

End Function

Private Function FormatearPres5s(ByVal sPres5Niv0 As String, ByVal sPres5Niv1 As String, ByVal sPres5Niv2 As String, ByVal sPres5Niv3 As String, ByVal sPres5Niv4 As String) As String
    Dim sPRES5 As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sPRES5 = sPres5Niv0
    If sPres5Niv1 <> "" Then
        sPRES5 = sPRES5 & "@" & sPres5Niv1
        If sPres5Niv2 <> "" Then
            sPRES5 = sPRES5 & "|" & sPres5Niv2
            If sPres5Niv3 <> "" Then
                sPRES5 = sPRES5 & "|" & sPres5Niv3
                If sPres5Niv4 <> "" Then
                    sPRES5 = sPRES5 & "|" & sPres5Niv4
                End If
            End If
        End If
    End If
    FormatearPres5s = sPRES5
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "FormatearPres5s", err, Erl, , m_bActivado, m_sMsgError)
      Exit Function
   End If

End Function

Private Function FormatearUON(ByVal CentroCoste As CCentroCoste)
    Dim sUON As String
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If CentroCoste.UON1 <> "" Then
        sUON = sUON & CentroCoste.UON1
        If CentroCoste.UON2 <> "" Then
            sUON = sUON & "#" & CentroCoste.UON2
            If CentroCoste.UON3 <> "" Then
                sUON = sUON & "#" & CentroCoste.UON3
                If CentroCoste.UON4 <> "" Then
                    sUON = sUON & "#" & CentroCoste.UON4
                End If
            End If
        End If
    End If
    FormatearUON = sUON
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Function
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "FormatearUON", err, Erl, , m_bActivado, m_sMsgError)
      Exit Function
   End If

End Function

Private Sub cmdBuscarContrato_Click(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSelCContables.g_sOrigen = "frmLstSolicitud"
    frmSelCContables.g_sPres0 = m_asPres5Niv0(Index)
    frmSelCContables.g_iNivel = m_aiNivelImp(Index)
    frmSelCContables.Show vbModal
    If Not g_oPartida Is Nothing Then
        If Len(g_oPartida.Cod) > 0 Then
                        
            txtContrato(Index).Text = g_oPartida.Cod & " - " & g_oPartida.Den
            txtContrato(Index).Tag = g_oPartida.Cod
            m_asPres5UON(Index) = FormatearPres5o(m_asPres5Niv0(Index), g_oPartida)
                        
            m_aPres5(Index).sPres5_Nivel0 = m_asPres5Niv0(Index)
            m_aPres5(Index).sPres5_Nivel1 = g_oPartida.Pres1
            m_aPres5(Index).sPres5_Nivel2 = g_oPartida.Pres2
            m_aPres5(Index).sPres5_Nivel3 = g_oPartida.Pres3
            m_aPres5(Index).sPres5_Nivel4 = g_oPartida.Pres4
                        
            m_bPres5Validado = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdBuscarContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBuscarMateriales_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELMAT.sOrigen = Me.Name
    frmSELMAT.bRComprador = m_bRestrMatComp
    frmSELMAT.m_PYME = oUsuarioSummit.pyme
    frmSELMAT.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdBuscarMateriales_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdBuscarUO_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSELUO.sOrigen = "frmLstSolicitud"
    frmSELUO.bRUO = m_bRuo
    frmSELUO.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdBuscarUO_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtCentroCoste.Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdLimpiarCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarContrato_Click(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtContrato(Index).Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdLimpiarContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarMateriales_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtMateriales.Text = ""
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    Set m_oGMN4Seleccionado = Nothing
    sdbcArticulo.Value = ""
    txtDenArticulo.Text = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdLimpiarMateriales_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub cmdLimpiarUO_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtUO.Text = "" Then Exit Sub
    
    txtUO.Text = ""
    
    sUON1 = ""
    sUON2 = ""
    sUON3 = ""
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdLimpiarUO_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub

Private Sub cmdObtener_Click()
    'Hace las comprobaciones necesarias:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtId.Text <> "" Then
        If Not IsNumeric(txtId.Text) Then
            oMensajes.NoValido m_txtId
            If Me.Visible Then txtId.SetFocus
            Exit Sub
        End If
    End If
    
    If txtFecDesde.Text <> "" Then
        If Not IsDate(txtFecDesde.Text) Then
            oMensajes.NoValido m_sFecDesde
            If Me.Visible Then txtFecDesde.SetFocus
            Exit Sub
        End If
    End If
    
    If txtFecHasta.Text <> "" Then
        If Not IsDate(txtFecHasta.Text) Then
            oMensajes.NoValido m_sFecHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    If txtFecDesde.Text <> "" And txtFecHasta.Text <> "" Then
        If CDate(txtFecDesde) > CDate(txtFecHasta) Then
            oMensajes.FechaDesdeMayorFechaHasta
            If Me.Visible Then txtFecHasta.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtImporteDesde.Text) <> "" Then
        If Not IsNumeric(txtImporteDesde.Text) Then
            oMensajes.NoValido m_stxtImporteDesde
            If Me.Visible Then txtImporteDesde.SetFocus
            Exit Sub
        End If
    End If
    
    If Trim(txtImporteHasta.Text) <> "" Then
        If Not IsNumeric(txtImporteHasta.Text) Then
            oMensajes.NoValido m_stxtImporteDesde
            If Me.Visible Then txtImporteHasta.SetFocus
            Exit Sub
        End If
    End If
    
    
    
    'Dependiendo de si se selecciona o no le check de listado detallado se obtiene
    'un informe u otro
    If chkDetalle.Value = vbChecked Then
        ObtenerListadoDetalle
    Else
        ObtenerListado
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "cmdObtener_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub PonerDenominaciondeUO(ByVal pUON1 As String, ByVal pUON2 As String, ByVal pUON3 As String)
    Dim oUnidades1 As CUnidadesOrgNivel1
    Dim oUnidades2 As CUnidadesOrgNivel2
    Dim oUnidades3 As CUnidadesOrgNivel3

    Dim strCadena As String
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If pUON3 <> "" Then
        Set oUnidades3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
        strCadena = oUnidades3.DevolverDenominacion(pUON1, pUON2, pUON3)
        If strCadena = "" Then
            txtUO.Text = ""
        Else
            txtUO.Text = pUON1 & " - " & pUON2 & " - " & pUON3 & " - " & strCadena
        End If
        Set oUnidades3 = Nothing
        
    ElseIf pUON2 <> "" Then
        Set oUnidades2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
        strCadena = oUnidades2.DevolverDenominacion(pUON1, pUON2)
        If strCadena = "" Then
            txtUO.Text = ""
        Else
            txtUO.Text = pUON1 & " - " & pUON2 & " - " & strCadena
        End If
        Set oUnidades2 = Nothing
        
    ElseIf pUON1 <> "" Then
        Set oUnidades1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
        strCadena = oUnidades1.DevolverDenominacion(pUON1)
        If strCadena = "" Then
            txtUO.Text = ""
        Else
            txtUO.Text = pUON1 & " - " & strCadena
        End If
        Set oUnidades1 = Nothing
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "PonerDenominaciondeUO", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
'fin jpa Tarea 540 -----------

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
   If Not m_bActivado Then
        m_bActivado = True
   End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub Form_Load()
    Dim iEstado As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bDescargarFrm = False
    m_bActivado = False
    
    Set m_oCentrosCoste = oFSGSRaiz.Generar_CCentrosCoste
    Set m_oContratosPres = oFSGSRaiz.Generar_CContratosPres
    
    lblMon.caption = gParametrosGenerales.gsMONCEN
    
    sdbgPartidas.RowHeight = 285
    
    CargarRecursos
    
    PonerFieldSeparator Me
    
    ConfigurarSeguridad
    
    ReDim m_asPres5Niv0(0)
    ReDim m_aiNivelImp(0)
    ReDim m_asPres5UON(0)
    ReDim m_aPres5(0)
    
    CargarPartidas
    
    Select Case g_sOrigen
        Case "frmSolicitudes"
            txtId.Text = frmSolicitudes.txtId.Text
            txtDescr.Text = frmSolicitudes.txtDescr.Text
            txtFecDesde.Text = frmSolicitudes.txtFecDesde.Text
            txtFecHasta.Text = frmSolicitudes.txtFecHasta.Text
            
            sdbcPeticionario.Text = frmSolicitudes.sdbcPeticionario.Text
            sdbcPeticionario.Columns(0).Value = frmSolicitudes.sdbcPeticionario.Columns(0).Value
            sdbcPeticionario.Columns(1).Value = frmSolicitudes.sdbcPeticionario.Columns(1).Value
        
            chkEstado(5).Value = 1
            chkEstado(0).Value = frmSolicitudes.chkEstado(0).Value
            chkEstado(1).Value = frmSolicitudes.chkEstado(1).Value
            chkEstado(2).Value = frmSolicitudes.chkEstado(2).Value
            chkEstado(3).Value = frmSolicitudes.chkEstado(3).Value
            chkEstado(4).Value = frmSolicitudes.chkEstado(4).Value
            chkEstado(5).Value = frmSolicitudes.chkEstado(5).Value
            
            sdbcTipoSolicit.Text = frmSolicitudes.sdbcTipoSolicit.Text
            sdbcTipoSolicit.Columns("COD").Value = frmSolicitudes.sdbcTipoSolicit.Columns("COD").Value
            sdbcTipoSolicit.Columns("DEN").Value = frmSolicitudes.sdbcTipoSolicit.Columns("DEN").Value
            
            If m_bRuo Then
                sUON1 = basOptimizacion.gUON1Usuario
                sUON2 = basOptimizacion.gUON2Usuario
                sUON3 = basOptimizacion.gUON3Usuario
                cmdLimpiarUO.Visible = False
                cmdBuscarUO.Visible = False
                txtUO.Width = 2715
            Else
                sUON1 = frmSolicitudes.sUON1
                sUON2 = frmSolicitudes.sUON2
                sUON3 = frmSolicitudes.sUON3
                cmdLimpiarUO.Visible = True
                cmdBuscarUO.Visible = True
                txtUO.Width = 1995
            End If
            PonerDenominaciondeUO sUON1, sUON2, sUON3
            chkDetalle.Value = vbUnchecked
            
            txtMateriales.Text = frmSolicitudes.txtMateriales.Text
            
            sdbcArticulo.Text = frmSolicitudes.sdbcArticulo.Text
            sdbcArticulo.Columns("COD").Value = frmSolicitudes.sdbcArticulo.Columns("COD").Value
            sdbcArticulo.Columns("DEN").Value = frmSolicitudes.sdbcArticulo.Columns("DEN").Value
            
            txtDenArticulo.Text = frmSolicitudes.txtDenArticulo.Text
            txtImporteDesde.Text = frmSolicitudes.txtImporteDesde.Text
            txtImporteHasta.Text = frmSolicitudes.txtImporteHasta.Text
            
            sdbcEmpresa.Text = frmSolicitudes.sdbcEmpresa.Text
            sdbcEmpresa.Columns("ID").Value = frmSolicitudes.sdbcEmpresa.Columns("ID").Value
            sdbcEmpresa.Columns("NIF").Value = frmSolicitudes.sdbcEmpresa.Columns("NIF").Value
            sdbcEmpresa.Columns("DEN").Value = frmSolicitudes.sdbcEmpresa.Columns("DEN").Value
            sdbcEmpresa.Columns("NIF_DEN").Value = frmSolicitudes.sdbcEmpresa.Columns("NIF_DEN").Value
            
            sdbcDepartamento.Text = frmSolicitudes.sdbcDepartamento.Text
            sdbcDepartamento.Columns("COD").Value = frmSolicitudes.sdbcDepartamento.Columns("COD").Value
            sdbcDepartamento.Columns("DEN").Value = frmSolicitudes.sdbcDepartamento.Columns("DEN").Value
            
            sdbcComprador.Text = frmSolicitudes.sdbcComprador.Text
            sdbcComprador.Columns("COD").Value = frmSolicitudes.sdbcComprador.Columns("COD").Value
            sdbcComprador.Columns("DEN").Value = frmSolicitudes.sdbcComprador.Columns("DEN").Value
            

            If Not g_oParametrosSM Is Nothing Then
                txtCentroCoste.Text = frmSolicitudes.txtCentroCoste.Text
                txtCentroCoste.Backcolor = frmSolicitudes.txtCentroCoste.Backcolor
                m_sUON_CC = frmSolicitudes.sUON_CC
            
                If g_oParametrosSM.Count > 1 Then
                    Dim vbm As Variant
                    Dim Row As Integer
                    Row = sdbgPartidas.Rows - 1
                    While Row >= 0
                        vbm = sdbgPartidas.AddItemBookmark(Row)
                        sdbgPartidas.Bookmark = vbm
                        frmSolicitudes.sdbgPartidas.Bookmark = vbm
                        sdbgPartidas.Columns("COD").Value = frmSolicitudes.sdbgPartidas.Columns("COD").Value
                        sdbgPartidas.Columns("VALOR").Value = frmSolicitudes.sdbgPartidas.Columns("VALOR").Value
                        sdbgPartidas.Columns("DEN").Value = frmSolicitudes.sdbgPartidas.Columns("DEN").Value
                        sdbgPartidas.Columns("PRES5UON").Value = frmSolicitudes.sdbgPartidas.Columns("PRES5UON").Value
                        sdbgPartidas.Columns("PRES5NIV1").Value = frmSolicitudes.sdbgPartidas.Columns("PRES5NIV1").Value
                        sdbgPartidas.Columns("PRES5NIV2").Value = frmSolicitudes.sdbgPartidas.Columns("PRES5NIV2").Value
                        sdbgPartidas.Columns("PRES5NIV3").Value = frmSolicitudes.sdbgPartidas.Columns("PRES5NIV3").Value
                        sdbgPartidas.Columns("PRES5NIV4").Value = frmSolicitudes.sdbgPartidas.Columns("PRES5NIV4").Value
                        sdbgPartidas.Columns("TAG").Value = frmSolicitudes.sdbgPartidas.Columns("TAG").Value
                        Row = Row - 1
                    Wend
                    sdbgPartidas.Update
                Else
                    txtContrato(0).Text = frmSolicitudes.txtContrato(0).Text
                    txtContrato(0).Tag = frmSolicitudes.txtContrato(0).Tag
                    txtContrato(0).Backcolor = frmSolicitudes.txtContrato(0).Backcolor
                End If
            End If
                        
        Case "frmSolicitudDetalle"
            'si se le llama desde el formulario de detalle de solicitud muestra la solicitud seleccionada
            Select Case g_sOrigen2
                Case "frmSolicitudes"   's�lo si el formulario origen es el de solicitudes
                    txtId.Text = frmSolicitudes.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
                    iEstado = frmSolicitudes.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Estado
                    
                Case "frmPROCE"
                    txtId.Text = frmPROCE.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
                    iEstado = frmPROCE.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Estado
                    
                Case "frmCatalogo"
                    txtId.Text = frmCatalogo.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
                    iEstado = frmCatalogo.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Estado
                    
                Case "frmPedidos"
                    txtId.Text = frmPEDIDOS.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
                    iEstado = frmPEDIDOS.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Estado
                    
                Case "frmSeguimiento"
                    txtId.Text = frmSeguimiento.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Id
                    iEstado = frmSeguimiento.g_ofrmDetalleSolic.g_oSolicitudSeleccionada.Estado
            End Select
            
            Select Case iEstado
                Case EstadoSolicitud.Anulada
                    chkEstado(3).Value = vbChecked
                Case EstadoSolicitud.Aprobada
                    chkEstado(1).Value = vbChecked
                Case EstadoSolicitud.Cerrada
                    chkEstado(4).Value = vbChecked
                Case EstadoSolicitud.Rechazada
                    chkEstado(2).Value = vbChecked
                Case EstadoSolicitud.Pendiente
                    chkEstado(0).Value = vbChecked
                Case Else
                    chkEstado(5).Value = vbChecked
            End Select
            
            chkDetalle.Value = vbChecked
    End Select
    picNumSolicitErp.Visible = HayIntegracionSolicitudes
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecDesde_Click()
    AbrirFormCalendar Me, txtFecDesde
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

''' <summary>
''' Obtener un listado de solicitudes simple.
''' </summary>
''' <remarks>Llamada desde:cmdObtener.btn_Click</remarks>
''' <remarks>Tiempo m�ximo: 3 seg </remarks>

Private Sub ObtenerListado()
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim CriterioOrdenacion As TipoOrdenacionSolicitudes
    Dim sTipoSolic As String
    Dim sPet As String
    Dim vEstado() As Variant
    Dim i As Integer
    Dim sestado As String
    Dim adoresDG As Ador.Recordset
    Dim sMateriales As String
    Dim sCodArticulo As String
    Dim lEmpresa As Long
    Dim sDepartamento As String
    Dim sComprador As String
    Dim sPartidas As String
    Dim vbm As Variant
    Dim Row As Integer
    Dim lIdPerfil As Long
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not crs_Connected Then Exit Sub
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptSolicitud.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    'Obtiene el tipo de solicitud
    If sdbcTipoSolicit.Text <> "" Then
        sTipoSolic = sdbcTipoSolicit.Columns("COD").Value
    End If
    
    'Obtiene el peticionario
    If sdbcPeticionario.Text <> "" Then
        sPet = sdbcPeticionario.Columns(0).Value
    End If
    
    If m_sGMN1Cod <> "" Then
        sMateriales = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod)) & m_sGMN1Cod
        If m_sGMN2Cod <> "" Then
            sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod)) & m_sGMN2Cod
            If m_sGMN3Cod <> "" Then
                sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod)) & m_sGMN3Cod
                If m_sGMN4Cod <> "" Then
                    sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod)) & m_sGMN4Cod
                End If
            End If
        End If
    End If
            
    If sdbcArticulo.Text <> "" Then
        sCodArticulo = sdbcArticulo.Columns(0).Value
    End If
    
    If sdbcEmpresa.Text <> "" Then
        lEmpresa = sdbcEmpresa.Columns(0).Value
    End If
    
    If sdbcDepartamento.Text <> "" Then
        sDepartamento = sdbcDepartamento.Columns(0).Value
    End If
    
    If sdbcComprador.Text <> "" Then
        sComprador = sdbcComprador.Columns(0).Value
    End If
    
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 1 Then
            Row = sdbgPartidas.Rows - 1
            While Row >= 0
                vbm = sdbgPartidas.AddItemBookmark(Row)
                If Len(sdbgPartidas.Columns("VALOR").CellValue(vbm)) > 0 Then
                    If Len(sPartidas) = 0 Then
                        sPartidas = sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    Else
                        sPartidas = sPartidas & ";" & sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    End If
                End If
                Row = Row - 1
            Wend
        Else
            sPartidas = m_asPres5UON(0)
        End If
    End If
    
    'Obtiene los estados
    i = 1
    If Trim(txtId.Text) = "" Then
        sestado = m_txtEstado & ":"
        'S�lo busca por los estados cuando no se ha introducido el identificador
        If chkEstado(0).Value = vbChecked Then  'Pendiente:cuando se ha aprobado desde el WS (si hab�a workflow) o si no hab�a cuando se ha enviado
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Pendiente
            i = i + 1
            sestado = sestado & m_sPendiente
        End If
        If chkEstado(1).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Aprobada
            sestado = sestado & "," & m_sAprobada
            i = i + 1
        End If
        If chkEstado(2).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Rechazada
            sestado = sestado & "," & m_sRechazada
            i = i + 1
        End If
        If chkEstado(3).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Anulada
            sestado = sestado & "," & m_sAnulada
            i = i + 1
        End If
        If chkEstado(4).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Cerrada
            sestado = sestado & "," & m_sCerrada
            i = i + 1
        End If
        If chkEstado(5).Value = vbChecked Then  'En curso del PM:
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenPendiente
            i = i + 1
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenRechazada
            sestado = sestado & "," & m_sEnCurso(1)
        End If
    Else
        sestado = ""
        ReDim Preserve vEstado(i)
        vEstado(i) = ""
    End If
    sestado = sestado & "  ; "
        
    SelectionText = sestado
    
    If Trim(txtId.Text) <> "" Then SelectionText = SelectionText & m_txtId & ":" & txtId.Text & "  ; "
    If Trim(txtDescr.Text) <> "" Then SelectionText = SelectionText & m_txtDen & ":" & txtDescr.Text & "  ; "
    If Trim(sdbcTipoSolicit.Text) <> "" Then SelectionText = SelectionText & m_stxtTipo & ":" & sdbcTipoSolicit & "  ; "
    If Trim(sdbcPeticionario.Text) <> "" Then SelectionText = SelectionText & m_txtPet & ":" & sPet & "  ; "
    If Trim(sMateriales) <> "" Then SelectionText = SelectionText & m_stxtMateriales & txtMateriales.Text & "  ; "
    If Trim(sdbcArticulo.Text) <> "" Then
        SelectionText = SelectionText & m_stxtArticulo & sdbcArticulo.Text & "  ; "
    ElseIf Trim(txtDenArticulo.Text) <> "" Then
        SelectionText = SelectionText & m_stxtArticulo & txtDenArticulo.Text & "  ; "
    End If
    If Trim(txtImporteDesde.Text) <> "" Then SelectionText = SelectionText & m_stxtImporteDesde & txtImporteDesde.Text & "  ; "
    If Trim(txtImporteHasta.Text) <> "" Then SelectionText = SelectionText & m_stxtImporteHasta & txtImporteHasta.Text & "  ; "
    If Trim(sdbcEmpresa.Text) <> "" Then SelectionText = SelectionText & m_stxtEmpresa & sdbcEmpresa.Text & "  ; "
    If Trim(sdbcDepartamento.Text) <> "" Then SelectionText = SelectionText & m_txtDepartamento & ":" & sdbcDepartamento.Text & "  ; "
    If Trim(sdbcComprador.Text) <> "" Then SelectionText = SelectionText & m_stxtComprador & sdbcComprador.Text & "  ; "
    If Trim(txtCentroCoste.Text) <> "" And txtCentroCoste.Backcolor <> &H8080FF Then SelectionText = SelectionText & m_stxtCentroCoste & txtCentroCoste.Text & "  ; "
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 1 Then
            Row = sdbgPartidas.Rows - 1
            While Row >= 0
                vbm = sdbgPartidas.AddItemBookmark(Row)
                If Len(sdbgPartidas.Columns("VALOR").CellValue(vbm)) > 0 Then
                    SelectionText = SelectionText & sdbgPartidas.Columns("DEN").CellValue(vbm) & ":" & sdbgPartidas.Columns("VALOR").CellValue(vbm) & "  ; "
                End If
                Row = Row - 1
            Wend
        Else
            If Trim(txtContrato(0).Text) <> "" And txtContrato(0).Backcolor <> &H8080FF Then
                SelectionText = SelectionText & lblContrato(0).caption & txtContrato(0).Text & "  ; "
            End If
        End If
    End If
    If Trim(txtFecDesde.Text) <> "" Then SelectionText = SelectionText & m_sFecDesde & ":" & txtFecDesde.Text & "  ; "
    If Trim(txtFecHasta.Text) <> "" Then SelectionText = SelectionText & m_sFecHasta & ":" & txtFecHasta.Text & "  ; "
    
    SelectionText = Mid(SelectionText, 1, Len(SelectionText) - 4)
    
    Dim bcoincidnumsolerp As Boolean
    Dim strNumsolicitud As String
    bcoincidnumsolerp = True
    strNumsolicitud = Me.txtNumSolicErp.Text
    If InStr(1, Me.txtNumSolicErp.Text, "*") > 0 Then
        strNumsolicitud = Replace(Me.txtNumSolicErp.Text, "*", "%")
        bcoincidnumsolerp = False
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & m_txtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_txtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtId")).Text = """" & m_txtId & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = """" & m_txtDen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecAlta")).Text = """" & m_txtFecAlta & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecNec")).Text = """" & m_txtFecNec & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtImporte")).Text = """" & m_txtImporte & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPet")).Text = """" & m_txtPet & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComprador")).Text = """" & m_txtComprador & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEstado")).Text = """" & m_txtEstado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPediente")).Text = """" & m_sPendiente & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAprobada")).Text = """" & m_sAprobada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRechazada")).Text = """" & m_sRechazada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAnulada")).Text = """" & m_sAnulada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCerrada")).Text = """" & m_sCerrada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUO")).Text = """" & m_txtUO & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipo")).Text = """" & m_stxtTipo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEnCurso")).Text = """" & m_sEnCurso(2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEnCursoRechazada")).Text = """" & m_sEnCursoRechazada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDepartamento")).Text = """" & m_txtDepartamento & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNumSolERP")).Text = """" & m_stxtNumSolERP & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "mostrarNumSolErp")).Text = """" & BooleanToSQLBinary(HayIntegracionSolicitudes) & """"
    
    'orden del report
    If optOrden(0).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorfecalta
    ElseIf optOrden(1).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorFecNec
    ElseIf optOrden(2).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorId
    ElseIf optOrden(3).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorDescr
    ElseIf optOrden(4).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorImporte
    ElseIf optOrden(5).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorPet
    ElseIf optOrden(6).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorComp
    ElseIf optOrden(7).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorEstado
    ElseIf optOrden(8).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorUO
    ElseIf optOrden(9).Value = True Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorDepartamento
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    If m_bRuo Then
        Set adoresDG = oGestorInformes.ListadoSolicitudes(False, txtId.Text, txtDescr.Text, txtFecDesde.Text, txtFecHasta.Text, sTipoSolic, sPet, vEstado, CriterioOrdenacion, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, sUON1, sUON2, sUON3, m_bRDep, basOptimizacion.gCodDepUsuario, sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), m_sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, strNumsolicitud, bcoincidnumsolerp, m_bRPerfUON, lIdPerfil, m_bRuo, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario)
    Else
        Set adoresDG = oGestorInformes.ListadoSolicitudes(False, txtId.Text, txtDescr.Text, txtFecDesde.Text, txtFecHasta.Text, sTipoSolic, sPet, vEstado, CriterioOrdenacion, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, sUON1, sUON2, sUON3, m_bRDep, basOptimizacion.gCodDepUsuario, sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), m_sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, strNumsolicitud, bcoincidnumsolerp, m_bRPerfUON, lIdPerfil)
    End If
        
    If Not adoresDG Is Nothing Then
        oReport.Database.SetDataSource adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = m_txtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = m_sEspera(1) & " " & pv.caption      'Generando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = m_sEspera(2)  'Seleccionando registros ...
    frmESPERA.lblDetalle = m_sEspera(3)  'Visualizando listado ...
    frmESPERA.Show
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "ObtenerListado", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LST_SOLICITUD, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then
    
        Me.caption = Ador(0).Value       '1 Listado de solicitudes de compra
        m_txtTitulo = Ador(0).Value
        
        Ador.MoveNext
        lblIdentificador.caption = Ador(0).Value & ":"  'Identificador
        m_txtId = Ador(0).Value
        Ador.MoveNext
        lblDescripcion.caption = Ador(0).Value   'descripci�n
        Ador.MoveNext
        lblPeticionario.caption = Ador(0).Value & ":"  'peticionario
        optOrden(5).caption = Ador(0).Value
        m_txtPet = Ador(0).Value
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value & ":"    'desde
        m_sFecDesde = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value & ":"  'hasta
        m_sFecHasta = Ador(0).Value
        
        Ador.MoveNext
        chkEstado(0).caption = Ador(0).Value   'pendientes
        Ador.MoveNext
        chkEstado(1).caption = Ador(0).Value   'aprobadas
        Ador.MoveNext
        chkEstado(2).caption = Ador(0).Value   'rechazadas
        Ador.MoveNext
        chkEstado(3).caption = Ador(0).Value   'anuladas
        Ador.MoveNext
        chkEstado(4).caption = Ador(0).Value   'cerradas
        
        Ador.MoveNext
        optOrden(0).caption = Ador(0).Value    'alta
        m_txtFecAlta = Ador(0).Value
        Ador.MoveNext
        optOrden(1).caption = Ador(0).Value   'necesidad
        m_txtFecNec = Ador(0).Value
        Ador.MoveNext
        optOrden(2).caption = Ador(0).Value   'identificador
        Ador.MoveNext
        optOrden(3).caption = Ador(0).Value 'descripci�n breve
        m_txtDen = Ador(0).Value
        Ador.MoveNext
        optOrden(4).caption = Ador(0).Value  'importe
        m_txtImporte = Ador(0).Value
        Ador.MoveNext
        optOrden(7).caption = Ador(0).Value  'estado
        m_txtEstado = Ador(0).Value
        Ador.MoveNext
        optOrden(6).caption = Ador(0).Value  'comprador
        m_txtComprador = Ador(0).Value
        
        Ador.MoveNext
        m_sPendiente = Ador(0).Value   'Pendiente
        Ador.MoveNext
        m_sAprobada = Ador(0).Value    'Aprobada
        Ador.MoveNext
        m_sRechazada = Ador(0).Value   'Rechazada
        Ador.MoveNext
        m_sAnulada = Ador(0).Value     'Anulada
        Ador.MoveNext
        m_sCerrada = Ador(0).Value     'Cerrada
        
        Ador.MoveNext
        m_txtSeleccion = Ador(0).Value  'Selecci�n
        Ador.MoveNext
        m_txtPag = Ador(0).Value  'P�g
        Ador.MoveNext
        m_txtDe = Ador(0).Value   'De
        Ador.MoveNext
        m_txtUO = Ador(0).Value   'UO
        
        Ador.MoveNext
        stabSolicitud.TabCaption(0) = Ador(0).Value     'Selecci�n
        Ador.MoveNext
        stabSolicitud.TabCaption(1) = Ador(0).Value   'Opciones
        Ador.MoveNext
        fraOrden.caption = Ador(0).Value    'Orden
        Ador.MoveNext
        chkDetalle.caption = Ador(0).Value 'Listado detallado
        
        Ador.MoveNext
        m_txtAprobador = Ador(0).Value  'Aprobador
        Ador.MoveNext
        m_txtFecAprob = Ador(0).Value   'Fecha aprobaci�n
        Ador.MoveNext
        m_txtCargo = Ador(0).Value      'Con cargo a
        Ador.MoveNext
        m_txtDescr = Ador(0).Value      'Descripci�n
        Ador.MoveNext
        m_txtProv = Ador(0).Value       'Proveedores
        Ador.MoveNext
        m_txtDest = Ador(0).Value       'Destino
        
        Ador.MoveNext
        m_sSubFicAdjuntos(1) = Ador(0).Value 'Ficheros adjuntos
        Ador.MoveNext
        m_sSubProcesos(1) = Ador(0).Value  'Procesos
        Ador.MoveNext
        m_sSubCatalogos(1) = Ador(0).Value  'Publicaciones
        Ador.MoveNext
        m_sSubPedidos(1) = Ador(0).Value   'Pedidos de cat�logo
        Ador.MoveNext
        m_sSubPedidos(2) = Ador(0).Value   'Pedidos directos
        
        Ador.MoveNext
        m_sSubProcesos(2) = Ador(0).Value   'A�o
        Ador.MoveNext
        m_sSubProcesos(3) = Ador(0).Value   'Mat.
        Ador.MoveNext
        m_sSubProcesos(4) = Ador(0).Value   'Cod
        Ador.MoveNext
        m_sSubProcesos(5) = Ador(0).Value   'Descripci�n
        Ador.MoveNext
        m_sSubProcesos(6) = Ador(0).Value   'Estado
        Ador.MoveNext
        m_sSubProcesos(7) = Ador(0).Value   'Resp.
        Ador.MoveNext
        m_sSubProcesos(8) = Ador(0).Value   'Dir
        Ador.MoveNext
        m_sSubProcesos(9) = Ador(0).Value   'Fec.pres.
        
        Ador.MoveNext
        m_sSubFicAdjuntos(2) = Ador(0).Value  'Fichero
        Ador.MoveNext
        m_sSubFicAdjuntos(3) = Ador(0).Value  'Usuario
        Ador.MoveNext
        m_sSubFicAdjuntos(4) = Ador(0).Value  'Comentario
        Ador.MoveNext
        m_sSubFicAdjuntos(5) = Ador(0).Value  'Fecha
         
        Ador.MoveNext
        m_sSubCatalogos(2) = Ador(0).Value   'Pub.
        Ador.MoveNext
        m_sSubCatalogos(3) = Ador(0).Value   'Despublicaci�n
        Ador.MoveNext
        m_sSubCatalogos(4) = Ador(0).Value   'Proceso
        Ador.MoveNext
        m_sSubCatalogos(5) = Ador(0).Value   'Art�culo
        Ador.MoveNext
        m_sSubCatalogos(6) = Ador(0).Value   'Proveedor
        Ador.MoveNext
        m_sSubCatalogos(7) = Ador(0).Value   'Destino
        Ador.MoveNext
        m_sSubCatalogos(8) = Ador(0).Value   'Cant
        Ador.MoveNext
        m_sSubCatalogos(9) = Ador(0).Value   'U.base
        Ador.MoveNext
        m_sSubCatalogos(10) = Ador(0).Value  'Precio
        Ador.MoveNext
        m_sSubCatalogos(11) = Ador(0).Value  'UP
        Ador.MoveNext
        m_sSubCatalogos(12) = Ador(0).Value  'FC
        Ador.MoveNext
        m_sSubCatalogos(13) = Ador(0).Value  'Otras
        Ador.MoveNext
        m_sSubCatalogos(14) = Ador(0).Value  'Cant. m�n. ped.
        
        Ador.MoveNext
        m_sIdiEst(1) = Ador(0).Value  '67  Pendiente de validar apertura
        Ador.MoveNext
        m_sIdiEst(2) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(3) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(4) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(5) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(6) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(7) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(8) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(9) = Ador(0).Value
        Ador.MoveNext
        m_sIdiEst(10) = Ador(0).Value  '77 Anulado
        
        Ador.MoveNext
        m_sIdiEstPedido(1) = Ador(0).Value '78 Pendiente de aprobaci�n
        Ador.MoveNext
        m_sIdiEstPedido(2) = Ador(0).Value '79 Denegado parcial por aprobador
        Ador.MoveNext
        m_sIdiEstPedido(3) = Ador(0).Value '80  Emitido al proveedor
        Ador.MoveNext
        m_sIdiEstPedido(4) = Ador(0).Value '81  Aceptado por proveedor
        Ador.MoveNext
        m_sIdiEstPedido(5) = Ador(0).Value '82  En camino
        Ador.MoveNext
        m_sIdiEstPedido(6) = Ador(0).Value '83  En recepci�n
        
        Ador.MoveNext
        m_sSubPedidos(3) = Ador(0).Value  '84 a�o
        Ador.MoveNext
        m_sSubPedidos(4) = Ador(0).Value   '85 Pedido
        Ador.MoveNext
        m_sSubPedidos(5) = Ador(0).Value   '86 Orden
        Ador.MoveNext
        m_sSubPedidos(6) = Ador(0).Value  '87 Proveedor
        Ador.MoveNext
        m_sSubPedidos(7) = Ador(0).Value  '88 Fec.Emisi�n
        Ador.MoveNext
        m_sSubPedidos(8) = Ador(0).Value  '89 N�m.externo
        Ador.MoveNext
        m_sSubPedidos(9) = Ador(0).Value  '90 Estado actual
        Ador.MoveNext
        m_sSubPedidos(10) = Ador(0).Value   '91 Fec.cambio est.
        
        Ador.MoveNext
        m_sIdiEstPedido(7) = Ador(0).Value '92 Recibido y cerrado
        Ador.MoveNext
        m_sIdiEstPedido(8) = Ador(0).Value '93 Anulado
        Ador.MoveNext
        m_sIdiEstPedido(9) = Ador(0).Value '94  Rechazado por proveedor
        Ador.MoveNext
        m_sIdiEstPedido(10) = Ador(0).Value '95  Denegado total por aprobador
        
        Ador.MoveNext
        m_txtSi = Ador(0).Value  'Si
        Ador.MoveNext
        m_txtNo = Ador(0).Value  'No
        
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        
        Ador.MoveNext
        m_sEspera(1) = Ador(0).Value
        Ador.MoveNext
        m_sEspera(2) = Ador(0).Value
        Ador.MoveNext
        m_sEspera(3) = Ador(0).Value
        
        Ador.MoveNext
        lblTipoSolicit.caption = Ador(0).Value & ":"  '102 Tipo de solicitud
        m_stxtTipo = Ador(0).Value
        Ador.MoveNext
        chkEstado(5).caption = Ador(0).Value  '103 En curso de aprobaci�n
        m_sEnCurso(1) = Ador(0).Value
        Ador.MoveNext
        sdbcTipoSolicit.Columns("COD").caption = Ador(0).Value  '104 C�digo
        Ador.MoveNext
        sdbcTipoSolicit.Columns("DEN").caption = Ador(0).Value  '105 Denominaci�n
        Ador.MoveNext
        m_sEnCurso(2) = Ador(0).Value  'En curso
        Ador.MoveNext
        lblUO.caption = Ador(0).Value  'UO
        optOrden(8).caption = Ador(0).Value  'UO
        Ador.MoveNext
        m_txtDepartamento = Ador(0).Value
        optOrden(9).caption = Ador(0).Value
        lblDepartamento.caption = Ador(0).Value & ":"
        Ador.MoveNext
        lblMateriales.caption = Ador(0).Value
        m_stxtMateriales = Ador(0).Value
        Ador.MoveNext
        lblArticulo.caption = Ador(0).Value
        m_stxtArticulo = Ador(0).Value
        Ador.MoveNext
        lblImporteDesde.caption = Ador(0).Value
        m_stxtImporteDesde = Ador(0).Value
        If Right(m_stxtImporteDesde, 1) = ":" Then m_stxtImporteDesde = Left(m_stxtImporteDesde, Len(m_stxtImporteDesde) - 1)
        Ador.MoveNext
        lblImporteHasta.caption = Ador(0).Value
        m_stxtImporteHasta = Ador(0).Value
        If Right(m_stxtImporteHasta, 1) = ":" Then m_stxtImporteHasta = Left(m_stxtImporteHasta, Len(m_stxtImporteHasta) - 1)
        Ador.MoveNext
        lblCentroCoste.caption = Ador(0).Value
        m_stxtCentroCoste = Ador(0).Value
        Ador.MoveNext
        lblEmpresa.caption = Ador(0).Value
        m_stxtEmpresa = Ador(0).Value
        Ador.MoveNext
        lblComprador.caption = Ador(0).Value
        m_stxtComprador = Ador(0).Value
        Ador.MoveNext
        m_stxtNumSolERP = Ador(0).Value
        Me.lblNumSolicitErp.caption = Ador(0).Value
        Ador.MoveNext
        m_sEnCursoRechazada = Ador(0).Value
        
        Ador.Close
    End If
    
    Set Ador = Nothing
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
        m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    Set m_oGMN4Seleccionado = Nothing
    Set m_oCentrosCoste = Nothing
    Set m_oContratosPres = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub lstCentroCoste_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtCentroCoste.Text = lstCentroCoste.List(lstCentroCoste.ListIndex)
    lstCentroCoste.Visible = False
    m_bCentroCosteValidado = True
    txtCentroCoste.Backcolor = &HC0FFC0
    
    Dim selectedItem As CCentroCoste
    Set selectedItem = m_oCentrosCoste.Item(lstCentroCoste.ListIndex + 1)
    If Not selectedItem Is Nothing Then
        m_sUON1_CC = selectedItem.UON1
        m_sUON2_CC = selectedItem.UON2
        m_sUON3_CC = selectedItem.UON3
        m_sUON4_CC = selectedItem.UON4
        m_sUON_CC = FormatearUON(selectedItem)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "lstCentroCoste_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub lstContrato_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    txtContrato(0).Text = lstContrato.List(lstContrato.ListIndex)
    lstContrato.Visible = False
    m_bPres5Validado = True
    txtContrato(0).Backcolor = &HC0FFC0

    Dim selectedItem As CContratopres
    Set selectedItem = m_oContratosPres.Item(lstContrato.ListIndex + 1)
    If Not selectedItem Is Nothing Then
        txtContrato(0).Tag = selectedItem.Cod
        m_asPres5UON(0) = FormatearPres5o(m_asPres5Niv0(0), selectedItem)
        m_aPres5(0).sPres5_Nivel0 = m_asPres5Niv0(0)
        m_aPres5(0).sPres5_Nivel1 = selectedItem.Pres1
        m_aPres5(0).sPres5_Nivel2 = selectedItem.Pres2
        m_aPres5(0).sPres5_Nivel3 = selectedItem.Pres3
        m_aPres5(0).sPres5_Nivel4 = selectedItem.Pres4
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "lstContrato_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticulo = True
    m_bEditandoArticuloConDropDown = False
    If Not m_bEditandoArticuloDen Then
        txtDenArticulo.Text = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sdbcArticulo.Value) <> 0 Then
        txtDenArticulo.Text = sdbcArticulo.Columns(1).Text
    End If
    m_bEditandoArticuloConDropDown = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcArticulo_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticulo = False
    m_bEditandoArticuloConDropDown = True
    sdbcArticulo.RemoveAll
    
    Dim oArticulos As CArticulos
    Set oArticulos = oFSGSRaiz.Generar_CArticulos
        
    If m_bRestrMatComp Then
        If Not m_oGMN4Seleccionado Is Nothing Or sdbcArticulo.Text <> "" Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, False, Trim(sdbcArticulo), , , , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario, True
        End If
    Else
        If m_oGMN4Seleccionado Is Nothing Then
            oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, , False, Trim(sdbcArticulo.Text), , , , , , , , , , , , , oUsuarioSummit.pyme
        Else
            m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcArticulo.Text), , , , , , False
            Set oArticulos = m_oGMN4Seleccionado.ARTICULOS
        End If
    End If

    Dim oArt As CArticulo
    For Each oArt In oArticulos
        sdbcArticulo.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
    Next

    sdbcArticulo.SelStart = 0
    sdbcArticulo.SelLength = Len(sdbcArticulo.Text)
    sdbcArticulo.Refresh
    
    Set oArticulos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcArticulo_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcArticulo_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bEditandoArticulo And Len(sdbcArticulo.Text) <> 0 Then
        Dim oArticulos As CArticulos
        Set oArticulos = oFSGSRaiz.Generar_CArticulos
        If m_bRestrMatComp Then
            If Not m_oGMN4Seleccionado Is Nothing Or sdbcArticulo.Text <> "" Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod, True, Trim(sdbcArticulo), , True, , basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario
            End If
        Else
            If m_oGMN4Seleccionado Is Nothing Then
                oArticulos.DevolverArticulosDesde gParametrosInstalacion.giCargaMaximaCombos, m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, , True, Trim(sdbcArticulo.Text), , True, , , , , , , , , , , oUsuarioSummit.pyme
            Else
                m_oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcArticulo.Text), , True, , , , True
                Set oArticulos = m_oGMN4Seleccionado.ARTICULOS
            End If
        End If
        If oArticulos.Count = 0 Then
            oMensajes.NoValido 21
            sdbcArticulo.Text = ""
            txtDenArticulo.Text = ""
        Else
            sdbcArticulo.Columns(0).Value = oArticulos.Item(1).Cod
            sdbcArticulo.Columns(1).Value = oArticulos.Item(1).Den
            txtDenArticulo.Text = oArticulos.Item(1).Den
        End If
        Set oArticulos = Nothing
    End If
    m_bEditandoArticulo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcArticulo_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_DropDown()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcComprador.RemoveAll
    
    Dim oCompradores As CCompradores
    Set oCompradores = oFSGSRaiz.generar_CCompradores
    
    If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
        oCompradores.CargarTodosLosCompradoresPorUON , , , sUON1, sUON2, sUON3, sdbcDepartamento.Value, True, "NOM"
    Else
        If m_bRuo Then
            oCompradores.CargarTodosLosCompradoresPorUON , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, sdbcDepartamento.Value, True, "NOM"
        Else
            oCompradores.CargarTodosLosCompradoresPorUON , , , , , , sdbcDepartamento.Value, True, "NOM", , oUsuarioSummit.pyme
        End If
    End If

    sdbcComprador.AddItem vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oComprador As CComprador
    For Each oComprador In oCompradores
        sdbcComprador.AddItem oComprador.Cod & Chr(m_lSeparador) & oComprador.Cod & " - " & oComprador.nombre & " " & oComprador.Apel
    Next

    sdbcComprador.SelStart = 0
    sdbcComprador.SelLength = Len(sdbcComprador.Text)
    sdbcComprador.Refresh
    
    Set oCompradores = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcComprador_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcComprador_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcComprador.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcComprador_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDepartamento_DropDown()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDepartamento.RemoveAll

    Dim oDepartamentos As CDepartamentos
    Set oDepartamentos = oFSGSRaiz.Generar_CDepartamentos
        
    If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
        oDepartamentos.CargarTodosLosDepartamentosPorUON , , sUON1, sUON2, sUON3, , True
    Else
        If m_bRuo Then
            oDepartamentos.CargarTodosLosDepartamentosPorUON , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , True
        Else
            oDepartamentos.CargarTodosLosDepartamentos , , , True, , , oUsuarioSummit.pyme
        End If
    End If
        
    sdbcDepartamento.AddItem vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oDepartamento As CDepartamento
    For Each oDepartamento In oDepartamentos
        sdbcDepartamento.AddItem oDepartamento.Cod & Chr(m_lSeparador) & oDepartamento.Den
    Next
    
    sdbcDepartamento.SelStart = 0
    sdbcDepartamento.SelLength = Len(sdbcDepartamento.Text)
    sdbcDepartamento.Refresh

    Screen.MousePointer = vbNormal
    Set oDepartamentos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcDepartamento_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcDepartamento_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcDepartamento.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcDepartamento_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEmpresa_DropDown()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass

    sdbcEmpresa.RemoveAll
        
    Dim oEmpresas As CEmpresas
    Set oEmpresas = oFSGSRaiz.Generar_CEmpresas
    If m_bRuo Then
        oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
    Else
        oEmpresas.CargarTodasLasEmpresasDesde , , , , True, , , , , , , , oUsuarioSummit.pyme
    End If
    
    sdbcEmpresa.AddItem 0 & Chr(m_lSeparador) & vbNullString & Chr(m_lSeparador) & vbNullString & Chr(m_lSeparador) & vbNullString
    Dim oEmpresa As CEmpresa
    For Each oEmpresa In oEmpresas
        sdbcEmpresa.AddItem oEmpresa.Id & Chr(m_lSeparador) & oEmpresa.NIF & Chr(m_lSeparador) & oEmpresa.Den & Chr(m_lSeparador) & oEmpresa.NIF & " - " & oEmpresa.Den
    Next
    
    sdbcEmpresa.SelStart = 0
    sdbcEmpresa.SelLength = Len(sdbcEmpresa.Text)
    sdbcEmpresa.Refresh

    Screen.MousePointer = vbNormal
    Set oEmpresas = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcEmpresa_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcEmpresa_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         sdbcEmpresa.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcEmpresa_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPeticionario_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPeticionario.DroppedDown Then
        sdbcPeticionario = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcPeticionario_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPeticionario_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     If sdbcPeticionario.Value = "..." Or sdbcPeticionario = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    
    
    m_bRespetarCombo = True
    sdbcPeticionario.Text = sdbcPeticionario.Columns(1).Text
    m_bRespetarCombo = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcPeticionario_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub


Private Sub sdbcPeticionario_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
    Dim oPersona As CPersona
    Dim oPersonas As CPersonas
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcPeticionario.RemoveAll
    
    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
    Else
        oPersonas.CargarPeticionariosSolCompra (True)
    End If

    For Each oPersona In oPersonas
        sdbcPeticionario.AddItem oPersona.Cod & Chr(m_lSeparador) & oPersona.Cod & " - " & NullToStr(oPersona.nombre) & " " & oPersona.Apellidos
    Next
        
    Set oPersonas = Nothing
    
    sdbcPeticionario.SelStart = 0
    sdbcPeticionario.SelLength = Len(sdbcPeticionario.Text)
    sdbcPeticionario.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcPeticionario_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPeticionario_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPeticionario.DataFieldList = "Column 1"
    sdbcPeticionario.DataFieldToDisplay = "Column 1"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcPeticionario_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcPeticionario_PositionList(ByVal Text As String)
    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcPeticionario.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcPeticionario.Rows - 1
            bm = sdbcPeticionario.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPeticionario.Columns(1).CellText(bm), 1, Len(Text))) Then
                sdbcPeticionario.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub


Private Sub ConfigurarSeguridad()
    'Si no es el usuario administrador
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
       Exit Sub
    End If
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

        'Restricci�n de equipo
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestEquipo)) Is Nothing) Then
            If basOptimizacion.gTipoDeUsuario = TIpoDeUsuario.comprador Then
                'S�lo le afectar� la restricci�n si el usuario es un comprador.
                m_bREquipo = True
            End If
        End If
        
        'Restricci�n de asignaci�n
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestAsig)) Is Nothing) Then
            m_bRAsig = True
        End If
        
        'Restricci�n de unidad organizativa
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestUO)) Is Nothing) Then
            m_bRuo = True
        End If
        
        'Restricci�n de departamento
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrDpto)) Is Nothing) Then
            m_bRDep = True
        End If
        
        'Ver pedidos en curso
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICEnCurso)) Is Nothing) Then
            m_bVerEnCurso = True
        End If
        
        'Restriccion material usuario comprador
        If oUsuarioSummit.Tipo = TIpoDeUsuario.comprador And Not oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing Then
            m_bRestrMatComp = True
        End If
                
        m_bRPerfUON = (Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestPerfUON)) Is Nothing))
            
    'Ver pedidos en curso
    If m_bVerEnCurso = False Then
        chkEstado(5).Visible = False
        chkEstado(4).Top = chkEstado(3).Top
        chkEstado(3).Top = chkEstado(2).Top
        chkEstado(2).Top = chkEstado(1).Top
        chkEstado(1).Top = chkEstado(0).Top
        chkEstado(0).Top = chkEstado(5).Top
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "ConfigurarSeguridad", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Obtener un listado de solicitudes detallado, con detalle de procesos abiertos, pedidos, desglose de campos, adjuntos, etc.
''' </summary>
''' <remarks>Llamada desde:cmdObtener.btn_Click</remarks>
''' <remarks>Tiempo m�ximo: 3 seg </remarks>

Private Sub ObtenerListadoDetalle()
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim SelectionText As String
    Dim pv As Preview
    Dim oReport As CRAXDRT.Report
    Dim CriterioOrdenacion As TipoOrdenacionSolicitudes
    Dim sTipoSolic As String
    Dim sPet As String
    Dim vEstado() As Variant
    Dim i As Integer
    Dim sestado As String
    Dim SubListado As CRAXDRT.Report
    Dim Table As CRAXDRT.DatabaseTable
    Dim adoresDG As Ador.Recordset
    Dim sMateriales As String
    Dim sCodArticulo As String
    Dim lEmpresa As Long
    Dim sDepartamento As String
    Dim sComprador As String
    Dim sPartidas As String
    Dim bcoincidnumsolerp As Boolean
    Dim strNumsolicitud As String
    Dim vbm As Variant
    Dim Row As Integer
    Dim lIdPerfil As Long
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not crs_Connected Then Exit Sub
    
   If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            oMensajes.RutaDeRPTNoValida
           Set oReport = Nothing
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptSolicitudDetalle.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        oMensajes.RutaDeRPTNoValida
        Set oReport = Nothing
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    Screen.MousePointer = vbHourglass
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    'Obtiene el tipo de solicitud
    If sdbcTipoSolicit.Text <> "" Then sTipoSolic = sdbcTipoSolicit.Columns("COD").Value
    
    'Obtiene el peticionario
    If sdbcPeticionario.Text <> "" Then sPet = sdbcPeticionario.Columns(0).Value
    
    If m_sGMN1Cod <> "" Then
        sMateriales = Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod)) & m_sGMN1Cod
        If m_sGMN2Cod <> "" Then
            sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod)) & m_sGMN2Cod
            If m_sGMN3Cod <> "" Then
                sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod)) & m_sGMN3Cod
                If m_sGMN4Cod <> "" Then
                    sMateriales = sMateriales & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod)) & m_sGMN4Cod
                End If
            End If
        End If
    End If
            
    If sdbcArticulo.Text <> "" Then sCodArticulo = sdbcArticulo.Columns(0).Value
    If sdbcEmpresa.Text <> "" Then lEmpresa = sdbcEmpresa.Columns(0).Value
    If sdbcDepartamento.Text <> "" Then sDepartamento = sdbcDepartamento.Columns(0).Value
    If sdbcComprador.Text <> "" Then sComprador = sdbcComprador.Columns(0).Value
    
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 1 Then
            Row = sdbgPartidas.Rows - 1
            While Row >= 0
                vbm = sdbgPartidas.AddItemBookmark(Row)
                If Len(sdbgPartidas.Columns("VALOR").CellValue(vbm)) > 0 Then
                    If Len(sPartidas) = 0 Then
                        sPartidas = sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    Else
                        sPartidas = sPartidas & ";" & sdbgPartidas.Columns("PRES5UON").CellValue(vbm)
                    End If
                End If
                Row = Row - 1
            Wend
        Else
            sPartidas = m_asPres5UON(0)
        End If
    End If
    
    'Obtiene los estados
    i = 1
    If Trim(txtId.Text) = "" Then
        sestado = m_txtEstado & ":"
        'S�lo busca por los estados cuando no se ha introducido el identificador
        If chkEstado(0).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Pendiente
            sestado = sestado & m_sPendiente
            i = i + 1
        End If
        If chkEstado(1).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Aprobada
            sestado = sestado & "," & m_sAprobada
            i = i + 1
        End If
        If chkEstado(2).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Rechazada
            sestado = sestado & "," & m_sRechazada
            i = i + 1
        End If
        If chkEstado(3).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Anulada
            sestado = sestado & "," & m_sAnulada
            i = i + 1
        End If
        If chkEstado(4).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.Cerrada
            i = i + 1
            sestado = sestado & "," & m_sCerrada
        End If
        If chkEstado(5).Value = vbChecked Then
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenPendiente
            i = i + 1
            ReDim Preserve vEstado(i)
            vEstado(i) = EstadoSolicitud.GenRechazada
            sestado = sestado & "," & m_sEnCurso(1)
        End If
    Else
        sestado = ""
        ReDim Preserve vEstado(i)
        vEstado(i) = ""
    End If
    sestado = sestado & "  ; "
    
    SelectionText = sestado
    
    If Trim(txtId.Text) <> "" Then SelectionText = SelectionText & m_txtId & ":" & txtId.Text & "  ; "
    If Trim(txtDescr.Text) <> "" Then SelectionText = SelectionText & m_txtDen & ":" & txtDescr.Text & "  ; "
    If Trim(sdbcTipoSolicit.Text) <> "" Then SelectionText = SelectionText & m_stxtTipo & ":" & sdbcTipoSolicit & "  ; "
    If Trim(sdbcPeticionario.Text) <> "" Then SelectionText = SelectionText & m_txtPet & ":" & sPet & "  ; "
    If Trim(sMateriales) <> "" Then SelectionText = SelectionText & m_stxtMateriales & txtMateriales.Text & "  ; "
    If Trim(sdbcArticulo.Text) <> "" Then
        SelectionText = SelectionText & m_stxtArticulo & sdbcArticulo.Text & "  ; "
    ElseIf Trim(txtDenArticulo.Text) <> "" Then
        SelectionText = SelectionText & m_stxtArticulo & txtDenArticulo.Text & "  ; "
    End If
    If Trim(txtImporteDesde.Text) <> "" Then SelectionText = SelectionText & m_stxtImporteDesde & txtImporteDesde.Text & "  ; "
    If Trim(txtImporteHasta.Text) <> "" Then SelectionText = SelectionText & m_stxtImporteHasta & txtImporteHasta.Text & "  ; "
    If Trim(sdbcEmpresa.Text) <> "" Then SelectionText = SelectionText & m_stxtEmpresa & sdbcEmpresa.Text & "  ; "
    If Trim(sdbcDepartamento.Text) <> "" Then SelectionText = SelectionText & m_txtDepartamento & ":" & sdbcDepartamento.Text & "  ; "
    If Trim(sdbcComprador.Text) <> "" Then SelectionText = SelectionText & m_stxtComprador & sdbcComprador.Text & "  ; "
    If Trim(txtCentroCoste.Text) <> "" And txtCentroCoste.Backcolor <> &H8080FF Then SelectionText = SelectionText & m_stxtCentroCoste & txtCentroCoste.Text & "  ; "
    
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 1 Then
            Row = sdbgPartidas.Rows - 1
            While Row >= 0
                vbm = sdbgPartidas.AddItemBookmark(Row)
                If Len(sdbgPartidas.Columns("VALOR").CellValue(vbm)) > 0 Then
                    SelectionText = SelectionText & sdbgPartidas.Columns("DEN").CellValue(vbm) & ":" & sdbgPartidas.Columns("VALOR").CellValue(vbm) & "  ; "
                End If
                Row = Row - 1
            Wend
        Else
            If Trim(txtContrato(0).Text) <> "" And txtContrato(0).Backcolor <> &H8080FF Then
                SelectionText = SelectionText & lblContrato(0).caption & txtContrato(0).Text & "  ; "
            End If
        End If
    End If
    
    If Trim(txtFecDesde.Text) <> "" Then SelectionText = SelectionText & m_sFecDesde & ":" & txtFecDesde.Text & "  ; "
    If Trim(txtFecHasta.Text) <> "" Then SelectionText = SelectionText & m_sFecHasta & ":" & txtFecHasta.Text & "  ; "
    SelectionText = Mid(SelectionText, 1, Len(SelectionText) - 4)
    
    bcoincidnumsolerp = True
    strNumsolicitud = Me.txtNumSolicErp.Text
    If InStr(1, Me.txtNumSolicErp.Text, "*") > 0 Then
        strNumsolicitud = Replace(Me.txtNumSolicErp.Text, "*", "%")
        bcoincidnumsolerp = False
    End If
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & SelectionText & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTitulo")).Text = """" & m_txtTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPag")).Text = """" & m_txtPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDe")).Text = """" & m_txtDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtSeleccion")).Text = """" & m_txtSeleccion & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtId")).Text = """" & m_txtId & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDen")).Text = """" & m_txtDen & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecAlta")).Text = """" & m_txtFecAlta & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFecNec")).Text = """" & m_txtFecNec & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtImporte")).Text = """" & m_txtImporte & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPet")).Text = """" & m_txtPet & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtComprador")).Text = """" & m_txtComprador & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEstado")).Text = """" & m_txtEstado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPediente")).Text = """" & m_sPendiente & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAprobada")).Text = """" & m_sAprobada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtRechazada")).Text = """" & m_sRechazada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAnulada")).Text = """" & m_sAnulada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCerrada")).Text = """" & m_sCerrada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtUO")).Text = """" & m_txtUO & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtCargo")).Text = """" & m_txtCargo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtProv")).Text = """" & m_txtProv & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDestino")).Text = """" & m_txtDest & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDescr")).Text = """" & m_txtDescr & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtTipo")).Text = """" & m_stxtTipo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEnCurso")).Text = """" & m_sEnCurso(2) & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtEnCursoRechazada")).Text = """" & m_sEnCursoRechazada & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDepartamento")).Text = """" & m_txtDepartamento & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNumSolERP")).Text = """" & m_stxtNumSolERP & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "mostrarNumSolErp")).Text = """" & BooleanToSQLBinary(Me.HayIntegracionSolicitudes) & """"
    
    'orden del report
    If optOrden(0).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorfecalta
    ElseIf optOrden(1).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorFecNec
    ElseIf optOrden(2).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorId
    ElseIf optOrden(3).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorDescr
    ElseIf optOrden(4).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorImporte
    ElseIf optOrden(5).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorPet
    ElseIf optOrden(6).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorComp
    ElseIf optOrden(7).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorEstado
    ElseIf optOrden(8).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorUO
    ElseIf optOrden(9).Value Then
        CriterioOrdenacion = TipoOrdenacionSolicitudes.OrdSolicPorDepartamento
    End If
    
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
        
    If m_bRuo Then
        Set adoresDG = oGestorInformes.ListadoSolicitudes(True, txtId.Text, txtDescr.Text, txtFecDesde.Text, txtFecHasta.Text, sTipoSolic, sPet, vEstado, CriterioOrdenacion, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, sUON1, sUON2, sUON3, m_bRDep, basOptimizacion.gCodDepUsuario, sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), m_sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, strNumsolicitud, bcoincidnumsolerp, m_bRPerfUON, lIdPerfil, m_bRuo, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario)
    Else
        Set adoresDG = oGestorInformes.ListadoSolicitudes(True, txtId.Text, txtDescr.Text, txtFecDesde.Text, txtFecHasta.Text, sTipoSolic, sPet, vEstado, CriterioOrdenacion, m_bRAsig, m_bREquipo, basOptimizacion.gCodCompradorUsuario, basOptimizacion.gCodEqpUsuario, sUON1, sUON2, sUON3, m_bRDep, basOptimizacion.gCodDepUsuario, sMateriales, sCodArticulo, txtDenArticulo.Text, NullToDbl0(StrToDblOrNull(txtImporteDesde.Text)), NullToDbl0(StrToDblOrNull(txtImporteHasta.Text)), m_sUON_CC, lEmpresa, sDepartamento, sComprador, sPartidas, strNumsolicitud, bcoincidnumsolerp, m_bRPerfUON, lIdPerfil)
    End If
    
    If Not adoresDG Is Nothing Then
        oReport.Database.SetDataSource adoresDG
    Else
        oMensajes.ImposibleMostrarInforme
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    'subReport de Ficheros adjuntos
    Set SubListado = oReport.OpenSubreport("rptAdjuntos")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        Table.Location = Table.Name
    Next
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTit")).Text = """" & m_sSubFicAdjuntos(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFichero")).Text = """" & m_sSubFicAdjuntos(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUsu")).Text = """" & m_sSubFicAdjuntos(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCom")).Text = """" & m_sSubFicAdjuntos(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecha")).Text = """" & m_sSubFicAdjuntos(5) & """"
    Set SubListado = Nothing
        
    'subReport de Proceso
    Set SubListado = oReport.OpenSubreport("rptProcesos")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        Table.Location = Table.Name
    Next
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTit")).Text = """" & m_sSubProcesos(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAnyo")).Text = """" & m_sSubProcesos(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtGmn1")).Text = """" & m_sSubProcesos(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProce")).Text = """" & m_sSubProcesos(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDescr")).Text = """" & m_sSubProcesos(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEstado")).Text = """" & m_sSubProcesos(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtResp")).Text = """" & m_sSubProcesos(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDir")).Text = """" & m_sSubProcesos(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecPres")).Text = """" & m_sSubProcesos(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst1")).Text = """" & m_sIdiEst(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst2")).Text = """" & m_sIdiEst(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst3")).Text = """" & m_sIdiEst(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst4")).Text = """" & m_sIdiEst(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst5")).Text = """" & m_sIdiEst(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst6")).Text = """" & m_sIdiEst(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst7")).Text = """" & m_sIdiEst(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst8")).Text = """" & m_sIdiEst(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst9")).Text = """" & m_sIdiEst(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst10")).Text = """" & m_sIdiEst(10) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_txtSi & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_txtNo & """"
    Set SubListado = Nothing
        
    'subReport de publicaciones
    Set SubListado = oReport.OpenSubreport("rptPublicaciones")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        Table.Location = Table.Name
    Next
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTit")).Text = """" & m_sSubCatalogos(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPub")).Text = """" & m_sSubCatalogos(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDespub")).Text = """" & m_sSubCatalogos(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProceso")).Text = """" & m_sSubCatalogos(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtArt")).Text = """" & m_sSubCatalogos(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProveedor")).Text = """" & m_sSubCatalogos(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtDest")).Text = """" & m_sSubCatalogos(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCant")).Text = """" & m_sSubCatalogos(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtUbase")).Text = """" & m_sSubCatalogos(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPrecio")).Text = """" & m_sSubCatalogos(10) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "UP")).Text = """" & m_sSubCatalogos(11) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFC")).Text = """" & m_sSubCatalogos(12) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtCantMin")).Text = """" & m_sSubCatalogos(14) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtSi")).Text = """" & m_txtSi & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNo")).Text = """" & m_txtNo & """"
    Set SubListado = Nothing
    
    'subReport de pedidos cat�logo
    Set SubListado = oReport.OpenSubreport("rptPedidosCat")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        Table.Location = Table.Name
    Next
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTit")).Text = """" & m_sSubPedidos(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAnyo")).Text = """" & m_sSubPedidos(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPedido")).Text = """" & m_sSubPedidos(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOrden")).Text = """" & m_sSubPedidos(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProv")).Text = """" & m_sSubPedidos(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecEmi")).Text = """" & m_sSubPedidos(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNum")).Text = """" & m_sSubPedidos(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEstado")).Text = """" & m_sSubPedidos(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecCambio")).Text = """" & m_sSubPedidos(10) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst1")).Text = """" & m_sIdiEstPedido(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst2")).Text = """" & m_sIdiEstPedido(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst3")).Text = """" & m_sIdiEstPedido(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst4")).Text = """" & m_sIdiEstPedido(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst5")).Text = """" & m_sIdiEstPedido(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst6")).Text = """" & m_sIdiEstPedido(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst7")).Text = """" & m_sIdiEstPedido(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst8")).Text = """" & m_sIdiEstPedido(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst9")).Text = """" & m_sIdiEstPedido(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst10")).Text = """" & m_sIdiEstPedido(10) & """"
    Set SubListado = Nothing

    'subReport de pedidos directos
    Set SubListado = oReport.OpenSubreport("rptPedidosDir")
    For Each Table In SubListado.Database.Tables
        Table.SetLogOnInfo crs_Server, crs_Database, crs_User, crs_Password
        Table.Location = Table.Name
    Next
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtTit")).Text = """" & m_sSubPedidos(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtAnyo")).Text = """" & m_sSubPedidos(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtPedido")).Text = """" & m_sSubPedidos(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtOrden")).Text = """" & m_sSubPedidos(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtProv")).Text = """" & m_sSubPedidos(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecEmi")).Text = """" & m_sSubPedidos(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtNum")).Text = """" & m_sSubPedidos(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEstado")).Text = """" & m_sSubPedidos(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtFecCambio")).Text = """" & m_sSubPedidos(10) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst1")).Text = """" & m_sIdiEstPedido(1) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst2")).Text = """" & m_sIdiEstPedido(2) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst3")).Text = """" & m_sIdiEstPedido(3) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst4")).Text = """" & m_sIdiEstPedido(4) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst5")).Text = """" & m_sIdiEstPedido(5) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst6")).Text = """" & m_sIdiEstPedido(6) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst7")).Text = """" & m_sIdiEstPedido(7) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst8")).Text = """" & m_sIdiEstPedido(8) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst9")).Text = """" & m_sIdiEstPedido(9) & """"
    SubListado.FormulaFields(crs_FormulaIndex(SubListado, "txtEst10")).Text = """" & m_sIdiEstPedido(10) & """"
    Set SubListado = Nothing
        
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = m_txtTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.lblGeneral.caption = m_sEspera(1) & " " & pv.caption      'Generando
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = m_sEspera(2)  'Seleccionando registros ...
    frmESPERA.lblDetalle = m_sEspera(3)  'Visualizando listado ...
    frmESPERA.Show
    
    Timer1.Enabled = True
    
    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me
    
    Screen.MousePointer = vbNormal
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "ObtenerListadoDetalle", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPeticionario_Validate(Cancel As Boolean)
    Dim oPersonas As CPersonas
    Dim bExiste As Boolean
    Dim sbuscar As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
  
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPeticionario.Text = "" Then
        sdbcPeticionario.Columns(0).Value = ""
        Exit Sub
    End If
    
    If sdbcPeticionario.Text = sdbcPeticionario.Columns(0).Text Then Exit Sub

    Set oPersonas = oFSGSRaiz.Generar_CPersonas
    
    If sdbcPeticionario.Columns(0).Value = "" Then
        sdbcPeticionario.Text = ""
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el peticionario
    
    Screen.MousePointer = vbHourglass
    
    If sdbcPeticionario.Columns(0).Text = "" Then
        sbuscar = "NULL"
    Else
        sbuscar = sdbcPeticionario.Columns(0).Text
    End If
    
    If m_bRuo Then
        oPersonas.CargarPeticionariosSolCompra True, sbuscar, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario
    Else
        oPersonas.CargarPeticionariosSolCompra True, sbuscar
    End If
    
    bExiste = Not (oPersonas.Count = 0)
    
    If Not bExiste Then
        sdbcPeticionario.Text = ""
    Else
        m_bRespetarCombo = True
        sdbcPeticionario.Text = oPersonas.Item(1).Cod & " - " & NullToStr(oPersonas.Item(1).nombre) & " " & NullToStr(oPersonas.Item(1).Apellidos)
        sdbcPeticionario.Columns(0).Value = oPersonas.Item(1).Cod
        sdbcPeticionario.Columns(1).Value = oPersonas.Item(1).Cod & "-" & oPersonas.Item(1).nombre & " " & NullToStr(oPersonas.Item(1).Apellidos)
        
        m_bRespetarCombo = False
        
    End If
    
    Set oPersonas = Nothing

    Screen.MousePointer = vbNormal
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcPeticionario_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcTipoSolicit_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    If Not sdbcTipoSolicit.DroppedDown Then
        sdbcTipoSolicit = ""
    End If

    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcTipoSolicit_Click", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbcTipoSolicit_CloseUp()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then
        Exit Sub
    End If

    If sdbcTipoSolicit.Value = "..." Or sdbcTipoSolicit.Value = "" Then
        sdbcTipoSolicit.Text = ""
        Exit Sub
    End If
    
    m_bRespetarCombo = True
    sdbcTipoSolicit.Text = sdbcTipoSolicit.Columns(1).Text
    m_bRespetarCombo = False

    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcTipoSolicit_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoSolicit_DropDown()
    'Cargar la combo con todos los usuarios con acceso al EP
    Dim oSolic As CSolicitud
    Dim oSolicitudes As CSolicitudes
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
  
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcTipoSolicit.RemoveAll
    
    Dim lIdPerfil As Long
    If Not oUsuarioSummit.Perfil Is Nothing Then lIdPerfil = oUsuarioSummit.Perfil.Id
    
    Set oSolicitudes = oFSGSRaiz.Generar_CSolicitudes
    If m_bRuo Then
        oSolicitudes.CargarTodasSolicitudesDeCompras , , , , basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, m_bRPerfUON, lIdPerfil
    Else
        oSolicitudes.CargarTodasSolicitudesDeCompras , , , , , , , m_bRPerfUON, lIdPerfil
    End If

    For Each oSolic In oSolicitudes
        sdbcTipoSolicit.AddItem oSolic.Codigo & Chr(m_lSeparador) & NullToStr(oSolic.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & Chr(m_lSeparador) & oSolic.Id
    Next
        
    Set oSolicitudes = Nothing
    
    sdbcTipoSolicit.SelStart = 0
    sdbcTipoSolicit.SelLength = Len(sdbcTipoSolicit.Text)
    sdbcTipoSolicit.Refresh
    
    Screen.MousePointer = vbNormal
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcTipoSolicit_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcTipoSolicit_InitColumnProps()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub

    sdbcTipoSolicit.DataFieldList = "Column 1"
    sdbcTipoSolicit.DataFieldToDisplay = "Column 1"

    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbcTipoSolicit_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

''' * Objetivo: Posicionarse en el combo segun la seleccion
Private Sub sdbcTipoSolicit_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcTipoSolicit.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcTipoSolicit.Rows - 1
            bm = sdbcTipoSolicit.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcTipoSolicit.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcTipoSolicit.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgPartidas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbgPartidas.Columns(ColIndex).Name = "VALOR" And sdbgPartidas.Columns("VALIDADO").Value = 0 Then
        sdbgPartidas.Columns("PRES5NIV1").Value = ""
        sdbgPartidas.Columns("PRES5NIV2").Value = ""
        sdbgPartidas.Columns("PRES5NIV3").Value = ""
        sdbgPartidas.Columns("PRES5NIV4").Value = ""
        sdbgPartidas.Columns("TAG").Value = ""
        sdbgPartidas.Columns("PRES5UON").Value = ""
        
        If sdbgPartidas.Columns(ColIndex).Value <> "" Then
            Dim pos As Integer
            pos = InStr(1, sdbgPartidas.Columns("VALOR").Value, " - ", vbTextCompare)
            If pos > 0 Then
                sdbgPartidas.Columns("TAG").Value = Trim(Mid(sdbgPartidas.Columns("VALOR").Value, 1, pos))
            Else
                sdbgPartidas.Columns("TAG").Value = ""
            End If
        
            Dim sValue As String
            If Len(sdbgPartidas.Columns("TAG").Value) > 0 Then
                sValue = CStr(sdbgPartidas.Columns("TAG").Value)
            Else
                sValue = sdbgPartidas.Columns(ColIndex).Value
            End If
                        
            If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
                m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, sUON1, sUON2, sUON3, , sValue
            Else
                If m_bRuo Then
                    m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , sValue
                Else
                    If oUsuarioSummit.pyme <> 0 Then
                        m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, , , , sValue
                    Else
                        m_oContratosPres.CargarTodosLosContratos sdbgPartidas.Columns("COD").Value, sdbgPartidas.Columns("NIVEL").Value, IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, , , , , sValue
                    End If
                End If
            End If
                    
            If m_oContratosPres.Count > 0 Then
'                sdbgPartidas.Columns(ColIndex).Style = ssStyleComboBox
'                sdbgPartidas.Columns(ColIndex).RemoveAll
'                Dim sItemContrato As String
'                Dim i As Integer
'                For i = 1 To m_oContratosPres.Count
'                    sItemContrato = m_oContratosPres.Item(i).cod & " - " & m_oContratosPres.Item(i).Den
'                    sdbgPartidas.Columns(ColIndex).AddItem sItemContrato
'                Next
                
                'Se coge la primera partida
                sdbgPartidas.Columns("VALOR").Value = m_oContratosPres.Item(1).Cod & " - " & m_oContratosPres.Item(1).Den
                sdbgPartidas.Columns("TAG").Value = m_oContratosPres.Item(1).Cod
                sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5o(sdbgPartidas.Columns("COD").Value, m_oContratosPres.Item(1))
                sdbgPartidas.Columns("VALIDADO").Value = 1
                sdbgPartidas.Columns("PRES5NIV1").Value = m_oContratosPres.Item(1).Pres1
                sdbgPartidas.Columns("PRES5NIV2").Value = m_oContratosPres.Item(1).Pres2
                sdbgPartidas.Columns("PRES5NIV3").Value = m_oContratosPres.Item(1).Pres3
                sdbgPartidas.Columns("PRES5NIV4").Value = m_oContratosPres.Item(1).Pres4
            Else
                sdbgPartidas.Columns("VALOR").Value = sValue
            End If
        End If
    End If

    Exit Sub
ERROR:
    If err.Number <> 0 Then
       m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbgPartidas_BeforeColUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
       Exit Sub
    End If
End Sub

Private Sub sdbgPartidas_BtnClick()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    frmSelCContables.g_sOrigen = "frmLstSolicitud"
    frmSelCContables.g_sPres0 = sdbgPartidas.Columns("COD").Value
    frmSelCContables.g_iNivel = sdbgPartidas.Columns("NIVEL").Value
    frmSelCContables.Show vbModal
    If Not g_oPartida Is Nothing Then
        If Len(g_oPartida.Cod) > 0 Then
                        
            sdbgPartidas.Columns("VALOR").Value = g_oPartida.Cod & " - " & g_oPartida.Den
            sdbgPartidas.Columns("TAG").Value = g_oPartida.Cod
            sdbgPartidas.Columns("PRES5UON").Value = FormatearPres5o(sdbgPartidas.Columns("COD").Value, g_oPartida)
            sdbgPartidas.Columns("VALIDADO").Value = 2
            sdbgPartidas.Columns("PRES5NIV1").Value = g_oPartida.Pres1
            sdbgPartidas.Columns("PRES5NIV2").Value = g_oPartida.Pres2
            sdbgPartidas.Columns("PRES5NIV3").Value = g_oPartida.Pres3
            sdbgPartidas.Columns("PRES5NIV4").Value = g_oPartida.Pres4
            
            sdbgPartidas.Update
        End If
    End If

    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbgPartidas_BtnClick", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgPartidas_Change()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
  
    sdbgPartidas.Columns("VALIDADO").Value = 0
    sdbgPartidas.Columns("VALOR").CellStyleSet ""

    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbgPartidas_Change", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgPartidas_KeyPress(KeyAscii As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
   
    If KeyAscii = vbKeyReturn Then
        If sdbgPartidas.Row + 1 = sdbgPartidas.Rows Then
            sdbgPartidas.MoveFirst
        Else
            sdbgPartidas.MoveNext
        End If
    End If

    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbgPartidas_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub sdbgPartidas_RowLoaded(ByVal Bookmark As Variant)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub

    If sdbgPartidas.Columns("VALOR").CellValue(Bookmark) = "" Then
        sdbgPartidas.Columns("VALOR").CellStyleSet ""
    Else
        If sdbgPartidas.Columns("VALIDADO").CellValue(Bookmark) = 1 Then
            sdbgPartidas.Columns("VALOR").CellStyleSet "Verde"
        ElseIf sdbgPartidas.Columns("VALIDADO").CellValue(Bookmark) = 0 Then
            sdbgPartidas.Columns("VALOR").CellStyleSet "Rojo"
        End If
    End If

    Exit Sub
ERROR:
    If err.Number <> 0 Then
        m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "sdbgPartidas_RowLoaded", err, Erl, Me, m_bActivado, m_sMsgError)
        Exit Sub
    End If
End Sub

Private Sub Timer1_Timer()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
    
    If frmESPERA.ProgressBar2.Value < 90 Then
        frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 10
    End If
    If frmESPERA.ProgressBar1.Value < 10 Then
        frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
    End If

    Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "Timer1_Timer", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub txtCentroCoste_Change()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
    If m_bDescargarFrm Then Exit Sub
  
    m_bCentroCosteValidado = False
    lstCentroCoste.Visible = False
    txtCentroCoste.Backcolor = &H80000005
    
    m_sUON1_CC = ""
    m_sUON2_CC = ""
    m_sUON3_CC = ""
    m_sUON4_CC = ""
    m_sUON_CC = ""
    
    Dim pos As Integer
    pos = InStr(1, txtCentroCoste.Text, " - ", vbTextCompare)
    If pos > 0 Then
        txtCentroCoste.Tag = Trim(Mid(txtCentroCoste.Text, 1, pos))
    Else
        txtCentroCoste.Tag = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtCentroCoste_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtCentroCoste_KeyPress(KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        txtCentroCoste_Validate True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtCentroCoste_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtCentroCoste_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bCentroCosteValidado And txtCentroCoste.Text <> "" Then
        Dim sCod As String
        If Len(txtCentroCoste.Tag) > 0 Then
            sCod = CStr(txtCentroCoste.Tag)
        Else
            sCod = txtCentroCoste.Text
        End If
        
        If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
            If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
                CargarCentroCostePorUON sCod, sUON1, sUON2, sUON3
            Else
                CargarCentroCostePorUON sCod, sUON1, sUON2, sUON3, oUsuarioSummit.Cod
            End If
        Else
            If oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador Then
                CargarCentroCostePorCentroSM sCod
            Else
                If m_bRuo Then
                   CargarCentroCostePorUON sCod, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, oUsuarioSummit.Cod
                Else
                    If oUsuarioSummit.pyme <> 0 Then
                        CargarCentroCostePorUON sCod, basOptimizacion.gUON1Usuario, , , oUsuarioSummit.Cod
                    Else
                        CargarCentroCostePorCentroSM sCod, oUsuarioSummit.Cod
                    End If
                End If
            End If
        End If
             
        If m_oCentrosCoste.Count > 0 Then
            If m_oCentrosCoste.Count > 1 Then
                lstCentroCoste.clear
                Dim sItemCentro As String
                Dim i As Integer
                For i = 1 To m_oCentrosCoste.Count
                    sItemCentro = m_oCentrosCoste.Item(i).CodConcat
                    If m_oCentrosCoste.Item(1).UON1 <> "" Then
                        sItemCentro = sItemCentro & " (" & m_oCentrosCoste.Item(1).UON1
                        If m_oCentrosCoste.Item(1).UON2 <> "" Then
                            sItemCentro = sItemCentro & "-" & m_oCentrosCoste.Item(1).UON2
                            If m_oCentrosCoste.Item(1).UON3 <> "" Then
                                sItemCentro = sItemCentro & "-" & m_oCentrosCoste.Item(1).UON3
                            End If
                        End If
                        sItemCentro = sItemCentro & ")"
                    End If
                    lstCentroCoste.AddItem sItemCentro
                Next
                lstCentroCoste.Visible = True
                If Me.Visible Then lstCentroCoste.SetFocus
            ElseIf m_oCentrosCoste.Count = 1 Then
                txtCentroCoste.Text = m_oCentrosCoste.Item(1).CodConcat
                If m_oCentrosCoste.Item(1).UON1 <> "" Then
                    txtCentroCoste.Text = txtCentroCoste.Text & " (" & m_oCentrosCoste.Item(1).UON1
                    If m_oCentrosCoste.Item(1).UON2 <> "" Then
                        txtCentroCoste.Text = txtCentroCoste.Text & "-" & m_oCentrosCoste.Item(1).UON2
                        If m_oCentrosCoste.Item(1).UON3 <> "" Then
                            txtCentroCoste.Text = txtCentroCoste.Text & "-" & m_oCentrosCoste.Item(1).UON3
                        End If
                    End If
                    txtCentroCoste.Text = txtCentroCoste.Text & ")"
                End If
                txtCentroCoste.Tag = m_oCentrosCoste.Item(1).COD_SM
                txtCentroCoste.Backcolor = &HC0FFC0
    
                m_sUON1_CC = m_oCentrosCoste.Item(1).UON1
                m_sUON2_CC = m_oCentrosCoste.Item(1).UON2
                m_sUON3_CC = m_oCentrosCoste.Item(1).UON3
                m_sUON4_CC = m_oCentrosCoste.Item(1).UON4
                
                m_sUON_CC = FormatearUON(m_oCentrosCoste.Item(1))
            End If
        Else
            txtCentroCoste.Tag = ""
            txtCentroCoste.Backcolor = &H8080FF
        End If
        m_bCentroCosteValidado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtCentroCoste_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarCentroCostePorCentroSM(ByVal sCod As String, Optional ByVal sUsu As String)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sCod) > 0 Then
        '(Ver txtCentro_Validate de frmSelActivo)
        If Len(sUsu) > 0 Then
            m_oCentrosCoste.CargarCentrosDeCoste sCod, , , , , , sUsu
            If m_oCentrosCoste.Count = 0 Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , sUsu
                If m_oCentrosCoste.Count = 0 Then
                    m_oCentrosCoste.CargarCentrosDeCoste , , sCod, , , , sUsu
                    If m_oCentrosCoste.Count = 0 Then
                        m_oCentrosCoste.CargarCentrosDeCoste , , , sCod, , , sUsu
                        If m_oCentrosCoste.Count = 0 Then
                            m_oCentrosCoste.CargarCentrosDeCoste , , , , sCod, , sUsu
                            If m_oCentrosCoste.Count = 0 Then
                                m_oCentrosCoste.CargarCentrosDeCoste , , , , , sCod, sUsu
                            End If
                        End If
                    End If
                End If
            End If
        Else
            m_oCentrosCoste.CargarCentrosDeCoste sCod
            If m_oCentrosCoste.Count = 0 Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod
                If m_oCentrosCoste.Count = 0 Then
                    m_oCentrosCoste.CargarCentrosDeCoste , , sCod
                    If m_oCentrosCoste.Count = 0 Then
                        m_oCentrosCoste.CargarCentrosDeCoste , , , sCod
                        If m_oCentrosCoste.Count = 0 Then
                            m_oCentrosCoste.CargarCentrosDeCoste , , , , sCod
                            If m_oCentrosCoste.Count = 0 Then
                                m_oCentrosCoste.CargarCentrosDeCoste , , , , , sCod
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "CargarCentroCostePorCentroSM", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarCentroCostePorUON(ByVal sCod As String, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal sUsu As String)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Len(sCod) > 0 Then
        If Len(sUsu) > 0 Then
            If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sUON3, sCod, , sUsu
            ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sCod, , , sUsu
            ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sCod, , , , sUsu
            ElseIf sUON1 = "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod, , , , , sUsu
            End If
        Else
            If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sUON3, sCod
            ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sUON2, sCod
            ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sUON1, sCod
            ElseIf sUON1 = "" And sUON2 = "" And sUON3 = "" Then
                m_oCentrosCoste.CargarCentrosDeCoste , sCod
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "CargarCentroCostePorUON", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub


Private Sub txtContrato_Change(Index As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bPres5Validado = False
    lstContrato.Visible = False
    txtContrato(Index).Backcolor = &H80000005
    
    m_asPres5UON(Index) = ""
    m_aPres5(Index).sPres5_Nivel0 = ""
    m_aPres5(Index).sPres5_Nivel1 = ""
    m_aPres5(Index).sPres5_Nivel2 = ""
    m_aPres5(Index).sPres5_Nivel3 = ""
    m_aPres5(Index).sPres5_Nivel4 = ""
    
    Dim pos As Integer
    pos = InStr(1, txtContrato(Index).Text, " - ", vbTextCompare)
    If pos > 0 Then
        txtContrato(Index).Tag = Trim(Mid(txtContrato(Index).Text, 1, pos))
    Else
        txtContrato(Index).Tag = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtContrato_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtContrato_KeyPress(Index As Integer, KeyAscii As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyReturn Then
        txtContrato_Validate Index, True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtContrato_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtContrato_Validate(Index As Integer, Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPres5Validado And txtContrato(Index).Text <> "" Then
        Dim sCod As String
        If Len(txtContrato(Index).Tag) > 0 Then
            sCod = CStr(txtContrato(Index).Tag)
        Else
            sCod = txtContrato(Index).Text
        End If
        
        If sUON1 <> "" Or sUON2 <> "" Or sUON3 <> "" Then
            m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, sUON1, sUON2, sUON3, , sCod
        Else
            If m_bRuo Then
                m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, , sCod
            Else
                If oUsuarioSummit.pyme <> 0 Then
                    m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, basOptimizacion.gUON1Usuario, , , , sCod
                Else
                    m_oContratosPres.CargarTodosLosContratos m_asPres5Niv0(Index), m_aiNivelImp(Index), IIf(oUsuarioSummit.Tipo = TIpoDeUsuario.Administrador, "", oUsuarioSummit.Cod), gParametrosInstalacion.gIdioma, , , , , sCod
                End If
            End If
        End If
        
        If m_oContratosPres.Count > 0 Then
            If m_oContratosPres.Count > 1 Then
                lstContrato.clear
                Dim sItemContrato As String
                Dim i As Integer
                For i = 1 To m_oContratosPres.Count
                    sItemContrato = m_oContratosPres.Item(i).Cod & " - " & m_oContratosPres.Item(i).Den
                    lstContrato.AddItem sItemContrato
                Next
                lstContrato.Left = txtContrato(Index).Left
                lstContrato.Top = txtContrato(Index).Top + txtContrato(Index).Height
                lstContrato.Height = 570
                lstContrato.Visible = True
                If Me.Visible Then lstContrato.SetFocus
            ElseIf m_oContratosPres.Count = 1 Then
                txtContrato(Index).Text = m_oContratosPres.Item(1).Cod & " - " & m_oContratosPres.Item(1).Den
                txtContrato(Index).Tag = m_oContratosPres.Item(1).Cod
                txtContrato(Index).Backcolor = &HC0FFC0
                
                m_asPres5UON(Index) = FormatearPres5o(m_asPres5Niv0(Index), m_oContratosPres.Item(1))
                m_aPres5(Index).sPres5_Nivel0 = m_asPres5Niv0(Index)
                m_aPres5(Index).sPres5_Nivel1 = m_oContratosPres.Item(1).Pres1
                m_aPres5(Index).sPres5_Nivel2 = m_oContratosPres.Item(1).Pres2
                m_aPres5(Index).sPres5_Nivel3 = m_oContratosPres.Item(1).Pres3
                m_aPres5(Index).sPres5_Nivel4 = m_oContratosPres.Item(1).Pres4
            End If
        Else
            txtContrato(Index).Tag = ""
            txtContrato(Index).Backcolor = &H8080FF
        End If
        m_bPres5Validado = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtContrato_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtDenArticulo_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bEditandoArticuloDen = True
    If Not m_bEditandoArticulo And Not m_bEditandoArticuloConDropDown Then
        sdbcArticulo.Value = ""
    End If
    m_bEditandoArticuloDen = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtDenArticulo_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtFecDesde_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecDesde <> "" Then
        If Not IsDate(txtFecDesde) Then
            oMensajes.NoValida " " & m_sFecDesde & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtFecDesde_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub txtFecHasta_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If txtFecHasta <> "" Then
        If Not IsDate(txtFecHasta) Then
            oMensajes.NoValida " " & m_sFecHasta & " "
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtFecHasta_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub
Public Sub MostrarUOSeleccionada()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sUON1 = frmSELUO.sUON1 And sUON2 = frmSELUO.sUON2 And sUON3 = frmSELUO.sUON3 Then Exit Sub
        
    If frmSELUO.sUON3 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sUON3 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = frmSELUO.sUON3
        
    ElseIf frmSELUO.sUON2 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sUON2 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = frmSELUO.sUON2
        sUON3 = ""
        
    ElseIf frmSELUO.sUON1 <> "" Then
        txtUO.Text = frmSELUO.sUON1 & " - " & frmSELUO.sDen
        sUON1 = frmSELUO.sUON1
        sUON2 = ""
        sUON3 = ""
        
    Else
        txtUO.Text = ""
        txtUO.Refresh
        sUON1 = ""
        sUON2 = ""
        sUON3 = ""
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "MostrarUOSeleccionada", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

   
End Sub

Private Sub txtMateriales_KeyDown(KeyCode As Integer, Shift As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
         cmdLimpiarMateriales_Click
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "txtMateriales_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Public Sub PonerMatSeleccionado()
    Dim oGMN1Seleccionado As CGrupoMatNivel1
    Dim oGMN2Seleccionado As CGrupoMatNivel2
    Dim oGMN3Seleccionado As CGrupoMatNivel3
    Dim oGMN4Seleccionado As CGrupoMatNivel4
    Dim sNuevoCod1 As String
    Dim sNuevoCod2 As String
    Dim sNuevoCod3 As String
    Dim sNuevoCod4 As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
    Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
    Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
    Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    Set m_oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
    
    If Not oGMN1Seleccionado Is Nothing Then
        sNuevoCod1 = oGMN1Seleccionado.Cod
    End If
    If Not oGMN2Seleccionado Is Nothing Then
        sNuevoCod2 = oGMN2Seleccionado.Cod
    End If
    If Not oGMN3Seleccionado Is Nothing Then
        sNuevoCod3 = oGMN3Seleccionado.Cod
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        sNuevoCod4 = oGMN4Seleccionado.Cod
    End If
    If m_sGMN1Cod = sNuevoCod1 And m_sGMN2Cod = sNuevoCod2 And m_sGMN3Cod = sNuevoCod3 And m_sGMN4Cod = sNuevoCod4 Then
        Exit Sub
    End If
    
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
        
    If Not oGMN1Seleccionado Is Nothing Then
        m_sGMN1Cod = oGMN1Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & oGMN1Seleccionado.Den
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        m_sGMN2Cod = oGMN2Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & oGMN2Seleccionado.Den
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        m_sGMN3Cod = oGMN3Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & oGMN3Seleccionado.Den
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        m_sGMN4Cod = oGMN4Seleccionado.Cod
        txtMateriales.Text = m_sGMN1Cod & " - " & m_sGMN2Cod & " - " & m_sGMN3Cod & " - " & m_sGMN4Cod & " - " & oGMN4Seleccionado.Den
    End If
    
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "PonerMatSeleccionado", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub CargarPartidas()
   
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not g_oParametrosSM Is Nothing Then
        If g_oParametrosSM.Count > 0 Then
            If g_oParametrosSM.Count > 1 Then
                Dim pres As CParametroSM
                For Each pres In g_oParametrosSM
                    sdbgPartidas.AddItem pres.PRES5 & Chr(m_lSeparador) & _
                                        pres.NomNivelImputacion & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        pres.impnivel & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        0 & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        "" & Chr(m_lSeparador) & _
                                        ""
                Next
                ConfiguracionMasDe1Partida
            Else
                lblContrato(0).caption = g_oParametrosSM.Item(1).NomNivelImputacion & ":"
                m_asPres5Niv0(0) = g_oParametrosSM.Item(1).PRES5
                m_aiNivelImp(0) = g_oParametrosSM.Item(1).impnivel
            End If
                    
            Me.picNumSolicitErp.Top = Me.txtContrato(0).Top + txtContrato(0).Height + 60
    
        End If
    Else
        lblContrato(0).Visible = False
        txtContrato(0).Visible = False
        cmdBuscarContrato(0).Visible = False
        cmdLimpiarContrato(0).Visible = False
        
        lblCentroCoste.Visible = False
        txtCentroCoste.Visible = False
        cmdLimpiarCentroCoste.Visible = False
        cmdBuscarCentroCoste.Visible = False
        lstCentroCoste.Visible = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "CargarPartidas", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub ConfiguracionMasDe1Partida()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblContrato(0).Visible = False
    txtContrato(0).Visible = False
    cmdBuscarContrato(0).Visible = False
    cmdLimpiarContrato(0).Visible = False
    sdbgPartidas.Visible = True
    
    sdbgPartidas.Top = lblContrato(0).Top
    sdbgPartidas.Left = lblContrato(0).Left
    sdbgPartidas.Width = (cmdBuscarContrato(0).Left - lblContrato(0).Left) + cmdBuscarContrato(0).Width
    
    Dim cota As Integer
    cota = 4
    If g_oParametrosSM.Count > 2 And g_oParametrosSM.Count <= cota Then
        sdbgPartidas.Height = sdbgPartidas.Rows * (sdbgPartidas.RowHeight + 10)
        m_sIncrementoAltura = sdbgPartidas.Rows * (sdbgPartidas.RowHeight + 10) + 250
        sdbgPartidas.Columns(1).Width = sdbgPartidas.Width * 0.19
        sdbgPartidas.Columns(2).Width = sdbgPartidas.Width * 0.75
    Else
        sdbgPartidas.ScrollBars = ssScrollBarsVertical
        sdbgPartidas.Height = cota * (sdbgPartidas.RowHeight + 10)
        m_sIncrementoAltura = cota * (sdbgPartidas.RowHeight + 10) + 250
        sdbgPartidas.Columns(1).Width = sdbgPartidas.Width * 0.19
        sdbgPartidas.Columns(2).Width = sdbgPartidas.Width * 0.71
    End If
              
    Me.Height = Me.Height + m_sIncrementoAltura
    stabSolicitud.Height = stabSolicitud.Height + m_sIncrementoAltura
    fraSeleccion.Height = fraSeleccion.Height + m_sIncrementoAltura
    picDebajoDePartidas.Top = picDebajoDePartidas.Top + m_sIncrementoAltura
    cmdObtener.Top = cmdObtener.Top + m_sIncrementoAltura
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
   Exit Sub
ERROR:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Formulario", "frmLstSolicitud", "ConfiguracionMasDe1Partida", err, Erl, , m_bActivado, m_sMsgError)
      Exit Sub
   End If

    
End Sub


Public Property Get HayIntegracionSolicitudes() As Boolean
    Dim oInstancias As CInstancias
    Set oInstancias = oFSGSRaiz.Generar_CInstancias
    HayIntegracionSolicitudes = oInstancias.HayIntegracionActiva
    Set oInstancias = Nothing
End Property

