Attribute VB_Name = "basParametros"
' ************ ConstantesGlobales del sistema **********
     
'Indica cuanto tiempo se mantiene una ventana de edicion abierta
Public Const giMinutosDeRetardoEnVentanasDeEdicion = 4
'Indica el tamanyo de buffer de lectura de ficheros
Public Const giChunkSize As Integer = 16384
'Indica el m�ximo n�mero de c�digo de proceso
Public Const giMaxProceCod As Long = 2147483647

'Indica el m�ximo n�mero de columnas en una hoja excel
Public giMaxNumColExcel As Integer

Public gParametrosGenerales As ParametrosGenerales
Public gLongitudesDeCodigos As LongitudesDeCodigos
Public gParametrosIntegracion As ParametrosIntegracion
Public g_oParametrosSM As CParametrosSM
Public garSimbolos As Variant

'**************************************************************************************************************
'  18/11/2003 PARAMETROS DE LA INSTALACION PASAN DEL REGISTRO A BASE DE DATOS
'  epb
'**************************************************************************************************************
'Los par�metros que se vayan rellenando por ah� solo se guardan al final, al salir
Public g_GuardarParametrosIns As Boolean



'Configuraci�n si admin publica obligatoria

Public Sub ConfigurarAdminPublicaObligatoria()
        
        gParametrosGenerales.gbAdminPublica = True
        gParametrosGenerales.gCPProcesoAdminPub = True
        gParametrosGenerales.gbSolicitudesCompras = False
        gParametrosGenerales.gCPDestino = EnProceso
        gParametrosGenerales.gCPPago = EnProceso
        gParametrosGenerales.gCPProveActual = NoDefinido
        gParametrosGenerales.gCPSolicitud = NoDefinido
        gParametrosGenerales.gCPSubasta = False
        gParametrosGenerales.gCPSubastaEspera = 0
        gParametrosGenerales.gCPSubastaProve = False
        gParametrosGenerales.gCPSubastaPujas = False
        gParametrosGenerales.gCPSubastaBajMinPuja = False
        gParametrosGenerales.gCPAdjunOfe = False
        gParametrosGenerales.gCPAlternativasPrec = False
        gParametrosGenerales.gCPSolCantMax = False
        gParametrosGenerales.gCPCambiarMonOferta = False
        oGestorParametros.GuardarParametrosGenerales gParametrosGenerales, , True

        gParametrosInstalacion.gCPProcesoAdminPub = True
        gParametrosInstalacion.gCPDestino = EnProceso
        gParametrosInstalacion.gCPPago = EnProceso
        gParametrosInstalacion.gCPProveActual = NoDefinido
        gParametrosInstalacion.gCPSolicitud = NoDefinido
        gParametrosInstalacion.gCPSubasta = False
        gParametrosGenerales.gCPSubastaEspera = 0
        gParametrosGenerales.gCPSubastaProve = False
        gParametrosGenerales.gCPSubastaPujas = False
        gParametrosGenerales.gCPSubastaBajMinPuja = False
        gParametrosInstalacion.gCPAdjunOfe = False
        gParametrosInstalacion.gCPAlternativasPrec = False
        gParametrosInstalacion.gCPSolCantMax = False
        gParametrosInstalacion.gCPCambiarMonOferta = False
        oGestorParametros.GuardarParametrosInstalacion gParametrosInstalacion, oUsuarioSummit.Cod
End Sub


