VERSION 5.00
Begin VB.Form frmEsperaAccion 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   945
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   5175
   ClipControls    =   0   'False
   ControlBox      =   0   'False
      BackColor       = &H8000000F&
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   945
   ScaleWidth      =   5175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Label Label1 
      Caption         =   "Aplicando propiedades de usuario en FullStep GS+"
      Height          =   195
      Left            =   720
      TabIndex        =   0
      Top             =   360
      Width           =   4080
      BackColor       = &H8000000F&
   End
End
Attribute VB_Name = "frmEsperaAccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next

    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_ESPERA_ACCION, basPublic.gParametrosInstalacion.gIdioma)

    If Not Ador Is Nothing Then

        Label1.Caption = Ador(0).Value
        Ador.Close

    End If

    Set Ador = Nothing

End Sub


Private Sub Form_Load()
    CargarRecursos
End Sub
