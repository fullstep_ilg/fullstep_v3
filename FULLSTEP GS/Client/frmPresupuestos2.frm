VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmPresupuestos2 
   Caption         =   "DPresupuestos2"
   ClientHeight    =   5985
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9135
   Icon            =   "frmPresupuestos2.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5985
   ScaleWidth      =   9135
   Begin TabDlg.SSTab SSTabPresupuestos 
      Height          =   5865
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9085
      _ExtentX        =   16034
      _ExtentY        =   10345
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   448
      TabCaption(0)   =   "DUnidad organizativa"
      TabPicture(0)   =   "frmPresupuestos2.frx":014A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "picNavigate1"
      Tab(0).Control(1)=   "tvwestrorg"
      Tab(0).Control(2)=   "lblEstrorg1"
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "DPresupuestos"
      TabPicture(1)   =   "frmPresupuestos2.frx":0166
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "lblAnyo"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "lblEstrorg2"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "sdbgPresupuestos"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "sdbcAnyo"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "tvwEstrPres"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "picSepar"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "picNavigate2"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "picImportes"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "cmdExpandir"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "chkBajaLog"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).ControlCount=   10
      Begin VB.CheckBox chkBajaLog 
         Caption         =   "Ver bajas l�gicas"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   440
         Width           =   3015
      End
      Begin VB.CommandButton cmdExpandir 
         Height          =   435
         Left            =   6345
         MouseIcon       =   "frmPresupuestos2.frx":0182
         Picture         =   "frmPresupuestos2.frx":02CC
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   4905
         Width           =   645
      End
      Begin VB.PictureBox picImportes 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   420
         Left            =   90
         ScaleHeight     =   420
         ScaleWidth      =   8790
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   4860
         Visible         =   0   'False
         Width           =   8785
         Begin VB.CommandButton cmdDeshacerPresupuesto 
            Caption         =   "D&Deshacer"
            Enabled         =   0   'False
            Height          =   345
            Left            =   0
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   60
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.CommandButton cmdModoEdicion 
            Caption         =   "D&Edici�n"
            Height          =   345
            Left            =   5895
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   60
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigate1 
         BorderStyle     =   0  'None
         Height          =   390
         Left            =   -74850
         ScaleHeight     =   390
         ScaleWidth      =   2445
         TabIndex        =   20
         Top             =   5280
         Width           =   2445
         Begin VB.CommandButton cmdBuscarUO 
            Caption         =   "D&Buscar"
            Height          =   345
            Left            =   1140
            TabIndex        =   22
            Top             =   30
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurarUO 
            Caption         =   "D&Restaurar"
            Height          =   345
            Left            =   60
            TabIndex        =   21
            Top             =   30
            Width           =   1005
         End
      End
      Begin VB.PictureBox picNavigate2 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ClipControls    =   0   'False
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   45
         ScaleHeight     =   435
         ScaleWidth      =   8865
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   5400
         Width           =   8860
         Begin VB.CommandButton cmdBajaLog 
            Caption         =   "Baja l�gica"
            Height          =   345
            Left            =   3600
            TabIndex        =   29
            Top             =   45
            Width           =   1705
         End
         Begin VB.CommandButton cmdA�adir 
            Caption         =   "D&A�adir"
            Height          =   345
            Left            =   0
            TabIndex        =   19
            Top             =   45
            Width           =   1005
         End
         Begin VB.CommandButton cmdListado 
            Caption         =   "D&Listado"
            Height          =   345
            Left            =   7810
            TabIndex        =   18
            Top             =   45
            Width           =   1005
         End
         Begin VB.CommandButton cmdBuscar 
            Caption         =   "D&Buscar"
            Height          =   345
            Left            =   6640
            TabIndex        =   17
            Top             =   45
            Width           =   1005
         End
         Begin VB.CommandButton cmdRestaurar 
            Caption         =   "D&Restaurar"
            Height          =   345
            Left            =   5470
            TabIndex        =   16
            Top             =   45
            Width           =   1005
         End
         Begin VB.CommandButton cmdModif 
            Caption         =   "D&Modificar"
            Height          =   345
            Left            =   1215
            TabIndex        =   15
            Top             =   45
            Width           =   1005
         End
         Begin VB.CommandButton cmdEli 
            Caption         =   "D&Eliminar"
            Height          =   345
            Left            =   2430
            TabIndex        =   14
            Top             =   45
            Width           =   1005
         End
      End
      Begin VB.PictureBox picSepar 
         Height          =   435
         Left            =   90
         ScaleHeight     =   375
         ScaleWidth      =   6195
         TabIndex        =   2
         Top             =   4905
         Width           =   6255
         Begin VB.TextBox txtPartida 
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   0
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   45
            Width           =   2775
         End
         Begin VB.TextBox txtObj 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5520
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   45
            Width           =   675
         End
         Begin VB.TextBox txtPres 
            Alignment       =   1  'Right Justify
            BackColor       =   &H80000018&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3360
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   45
            Width           =   1695
         End
         Begin VB.Label lblObj 
            Caption         =   "Obj.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   5100
            TabIndex        =   7
            Top             =   105
            Width           =   435
         End
         Begin VB.Label lblPres 
            Caption         =   "Pres.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00400000&
            Height          =   225
            Left            =   2835
            TabIndex        =   6
            Top             =   90
            Width           =   540
         End
      End
      Begin MSComctlLib.TreeView tvwEstrPres 
         Height          =   4185
         Left            =   120
         TabIndex        =   12
         Top             =   1055
         Width           =   8740
         _ExtentX        =   15425
         _ExtentY        =   7382
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList1"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.TreeView tvwestrorg 
         Height          =   4575
         Left            =   -74880
         TabIndex        =   23
         Top             =   675
         Width           =   8800
         _ExtentX        =   15531
         _ExtentY        =   8070
         _Version        =   393217
         HideSelection   =   0   'False
         LabelEdit       =   1
         Style           =   7
         HotTracking     =   -1  'True
         ImageList       =   "ImageList2"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcAnyo 
         Height          =   285
         Left            =   7945
         TabIndex        =   24
         Top             =   740
         Width           =   885
         ScrollBars      =   2
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         AllowInput      =   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   1693
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1561
         _ExtentY        =   503
         _StockProps     =   93
         Text            =   "2002"
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBGrid sdbgPresupuestos 
         Height          =   2220
         Left            =   90
         TabIndex        =   1
         Top             =   3150
         Visible         =   0   'False
         Width           =   8755
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   1
         GroupHeaders    =   0   'False
         AllowUpdate     =   0   'False
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         BalloonHelp     =   0   'False
         RowNavigation   =   1
         CellNavigation  =   1
         MaxSelectedRows =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   1455
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "COD"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   3
         Columns(0).Locked=   -1  'True
         Columns(0).HasBackColor=   -1  'True
         Columns(0).BackColor=   16777152
         Columns(1).Width=   3651
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   50
         Columns(1).Locked=   -1  'True
         Columns(1).HasBackColor=   -1  'True
         Columns(1).BackColor=   16777152
         Columns(2).Width=   4207
         Columns(2).Caption=   "Presupuesto"
         Columns(2).Name =   "PRES"
         Columns(2).Alignment=   1
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "Standard"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1958
         Columns(3).Caption=   "Objetivo %"
         Columns(3).Name =   "OBJ"
         Columns(3).Alignment=   2
         Columns(3).CaptionAlignment=   2
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "0.0#\%"
         Columns(3).FieldLen=   256
         _ExtentX        =   15443
         _ExtentY        =   3916
         _StockProps     =   79
         Caption         =   "Desglose"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblEstrorg2 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "DUnidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   120
         TabIndex        =   27
         Top             =   740
         Width           =   8725
      End
      Begin VB.Label lblEstrorg1 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "DUnidad organizativa"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   -74880
         TabIndex        =   26
         Top             =   360
         Width           =   8800
      End
      Begin VB.Label lblAnyo 
         BackColor       =   &H00FFC0C0&
         Caption         =   "DA�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   225
         Left            =   7360
         TabIndex        =   25
         Top             =   785
         Width           =   570
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":0416
            Key             =   "Raiz"
            Object.Tag             =   "Raiz"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":0EE0
            Key             =   "PRES1"
            Object.Tag             =   "PRES1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":1332
            Key             =   "PRES2"
            Object.Tag             =   "PRES2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":1784
            Key             =   "PRES3"
            Object.Tag             =   "PRES3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":1BD6
            Key             =   "PRES4"
            Object.Tag             =   "PRES4"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":2028
            Key             =   "PRESBAJALOGICA"
            Object.Tag             =   "PRESBAJALOGICA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":2A3A
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":2E02
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":3156
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":34AA
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":383E
            Key             =   "UON1A"
            Object.Tag             =   "UON1A"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":3B90
            Key             =   "UON2A"
            Object.Tag             =   "UON2A"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":3EE2
            Key             =   "UON3A"
            Object.Tag             =   "UON3A"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPresupuestos2.frx":3FA4
            Key             =   "UON0D"
            Object.Tag             =   "UON0D"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPresupuestos2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variable de control de flujo
Public Accion As accionessummit

'Variables para el resize

''' Control de errores

Private bModError As Boolean
Private bValError As Boolean

' Indice para la grid
Private p As Long

'Variables de seguridad
Private bModif As Boolean
Private bModifCod As Boolean
Private bModoEdicion As Boolean

'Presupuestos seleccionados
Public oPres1Seleccionado As CPresConNivel1
Public oPres2Seleccionado As CPresconNivel2
Public oPres3Seleccionado As CPresConNivel3
Public oPres4Seleccionado As CPresconNivel4
Public m_sUON1 As String
Public m_sUON2 As String
Public m_sUON3 As String

Public oPresupuestos As CPresContablesNivel1 'Contendra toda la estructura de presupuestos

Private oPresupuestosN1 As CPresContablesNivel1 'Contendra los presupuestos de 1er nivel.(Lo necesitamos para la grid, ya que los niveles inferiores se cargan desde el material superior, pero el primero no tiene nivel superior)

Private oPresupuestoEnEdicionN1 As CPresConNivel1
Private oPresupuestoEnEdicionN2 As CPresconNivel2
Private oPresupuestoEnEdicionN3 As CPresConNivel3
Private oPresupuestoEnEdicionN4 As CPresconNivel4

'Para gestionar el almacenamiento de la estructura de proyectos
Public oIBaseDatos As IBaseDatos
'Para gestionar el almacenamiento de la grid
Private oIBAseDatosEnEdicion As IBaseDatos
'Variable para mostrar la suma de presupuestos en la caption de la grid
Private dAcumulado As Double
'Variable que contendra el importe del presupuesto selecionado en el arbol
Private dPresGeneralSel As Double
Private bNoContinuar As Boolean
'Variables para el cambio de c�digo
Public g_sCodigoNuevo As String
Public g_bCodigoCancelar As Boolean
Public sOrigen As String
Private bOrdenDen As Boolean ' orden listado

'Multilenguaje
Private sIdiAnyadir As String
Private sIdiModificar As String
'Private sIdiPresupPor As String
Private sIdiCodigoP As String
Private sIdiCodigo As String
Private sIdiSuma As String
Private sIdiPresupuesto As String
Private sIdiObjetivo As String
Private sIdiDetalle As String

'Multilenguaje II
Private sIdiPresupuestos As String
Private sIdiConsultaPar As String
Private sIdiEdicionPar As String
Private sIdiMEEdicion As String
Private sIdiMEConsulta As String
Private sIdiSumaDesglose As String

Public sOrdenListadoDen As String
Public bRUO As Boolean
Public iUOBase As Integer
Private m_bLoad As Boolean
Private m_bCargarEstructuraOrganizativa As Boolean
Private m_sCaptionBajaLogica As String
Private m_sCaptionDeshacerBaja As String

Private Sub CargarRecursos()
    Dim adoresAdo As Ador.Recordset

    On Error Resume Next
    
    'FRM_PRESUPUESTOS1 = 260
    'Puesto que los textos son los mismos que los usados para la
    'ventana frmPresupuestos1, tomo los textos de la base de datos
    'de idiomas de ese mismo m�dulo.
    Set adoresAdo = oGestorIdiomas.DevolverTextosDelModulo(FRM_PRESUPUESTOS1, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not adoresAdo Is Nothing Then
        sIdiPresupuestos = adoresAdo(0).Value '1
        adoresAdo.MoveNext
        sIdiConsultaPar = adoresAdo(0).Value '2
        adoresAdo.MoveNext
        sIdiEdicionPar = adoresAdo(0).Value '3
        adoresAdo.MoveNext
        lblEstrorg1.caption = adoresAdo(0).Value '4
        lblEstrorg2.caption = adoresAdo(0).Value '4
        SSTabPresupuestos.TabCaption(0) = adoresAdo(0).Value '4
        adoresAdo.MoveNext
        cmdRestaurarUO.caption = adoresAdo(0).Value '5
        cmdRestaurar.caption = adoresAdo(0).Value '5
        adoresAdo.MoveNext
        cmdBuscarUO.caption = adoresAdo(0).Value '6
        cmdBuscar.caption = adoresAdo(0).Value '6
        adoresAdo.MoveNext
        lblAnyo.caption = adoresAdo(0).Value '7
        adoresAdo.MoveNext
        lblPres.caption = adoresAdo(0).Value '8
        adoresAdo.MoveNext
        lblObj.caption = adoresAdo(0).Value '9
        adoresAdo.MoveNext
        cmdA�adir.caption = adoresAdo(0).Value '10
        adoresAdo.MoveNext
        cmdModif.caption = adoresAdo(0).Value '11
        adoresAdo.MoveNext
        cmdEli.caption = adoresAdo(0).Value '12
        adoresAdo.MoveNext
        cmdListado.caption = adoresAdo(0).Value '13
        adoresAdo.MoveNext
        sdbgPresupuestos.Columns(0).caption = adoresAdo(0).Value '14
        sIdiCodigo = adoresAdo(0).Value
        adoresAdo.MoveNext
        sdbgPresupuestos.Columns(1).caption = adoresAdo(0).Value '15
        adoresAdo.MoveNext
        sdbgPresupuestos.Columns(2).caption = adoresAdo(0).Value '16
        adoresAdo.MoveNext
        sdbgPresupuestos.Columns(3).caption = adoresAdo(0).Value '17
        adoresAdo.MoveNext
        cmdDeshacerPresupuesto.caption = adoresAdo(0).Value '18
        adoresAdo.MoveNext
        sIdiMEEdicion = adoresAdo(0).Value '19
        adoresAdo.MoveNext
        sIdiMEConsulta = adoresAdo(0).Value '20
        adoresAdo.MoveNext
        sIdiSumaDesglose = adoresAdo(0).Value '21
        adoresAdo.MoveNext
        sIdiAnyadir = adoresAdo(0).Value '22
        adoresAdo.MoveNext
        sIdiModificar = adoresAdo(0).Value '23
        adoresAdo.MoveNext
        adoresAdo.MoveNext
        sIdiDetalle = adoresAdo(0).Value '25
        adoresAdo.MoveNext
        cmdBajaLog.caption = adoresAdo(0).Value '26
        m_sCaptionBajaLogica = adoresAdo(0).Value
        adoresAdo.MoveNext
        chkBajaLog.caption = adoresAdo(0).Value '27
        adoresAdo.MoveNext
        m_sCaptionDeshacerBaja = adoresAdo(0).Value '28
        adoresAdo.Close
    End If
    Set adoresAdo = Nothing
    
    If gParametrosGenerales.gsPlurPres2 <> "" Then
        Me.caption = sIdiPresupuestos & " - " & PonerPrimeraLetraEnMayuscula(gParametrosGenerales.gsPlurPres2)
    Else
        Me.caption = ""
    End If
    
    cmdModoEdicion.caption = sIdiMEEdicion
    SSTabPresupuestos.TabCaption(1) = PonerPrimeraLetraEnMayuscula(gParametrosGenerales.gsPlurPres2)
End Sub

Private Sub chkBajaLog_Click()
    GenerarArbolPresupuestos
    txtObj.Text = ""
    txtPres.Text = ""
    txtPartida.Text = ""
    tvwEstrPres.Nodes.Item("Raiz ").Selected = True
    cmdA�adir.Enabled = True
    cmdModif.Enabled = True
    cmdEli.Enabled = True
    cmdBajaLog.Enabled = True
    cmdBuscar.Enabled = True
    cmdListado.Enabled = True
    cmdBajaLog.caption = m_sCaptionBajaLogica
End Sub

Public Sub cmdA�adir_Click()
    Dim nodx As MSComctlLib.node


    Set nodx = tvwEstrPres.selectedItem
    Screen.MousePointer = vbHourglass
    If Not nodx Is Nothing Then
        Select Case Left(nodx.Tag, 5)
            Case "Raiz "
                Accion = accionessummit.ACCPresConNivel1Anya
                frmPRESConDetalle.caption = sIdiAnyadir & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo)
                Screen.MousePointer = vbNormal
                frmPRESConDetalle.Show 1
                
            Case "PRES1"
                Accion = accionessummit.ACCPresConNivel2Anya
                frmPRESConDetalle.caption = sIdiAnyadir & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo) & " - " & DevolverCod(nodx)
                Screen.MousePointer = vbNormal
                frmPRESConDetalle.Show 1

            Case "PRES2"
                Accion = accionessummit.ACCPresConNivel3Anya
                frmPRESConDetalle.caption = sIdiAnyadir & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                Screen.MousePointer = vbNormal
                frmPRESConDetalle.Show 1

            Case "PRES3"
                Accion = accionessummit.ACCPresConNivel4Anya
                frmPRESConDetalle.caption = sIdiAnyadir & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                Screen.MousePointer = vbNormal
                frmPRESConDetalle.Show 1
        End Select
    End If
    Screen.MousePointer = vbNormal
End Sub

Private Sub cmdBajaLog_Click()
    
    BajaLogica

End Sub

Private Sub cmdBuscar_Click()
    frmPRESConBuscar.sOrigen = Me.Name
    frmPRESConBuscar.Show 1
    If Me.Visible Then tvwEstrPres.SetFocus
End Sub

Private Sub cmdDeshacerPresupuesto_Click()

    ''' * Objetivo: Deshacer la edicion en la Destino actual
    
    sdbgPresupuestos.CancelUpdate
    sdbgPresupuestos.DataChanged = False
    
    Screen.MousePointer = vbHourglass

    If Not oPresupuestoEnEdicionN1 Is Nothing Or Not oPresupuestoEnEdicionN2 Is Nothing Or Not oPresupuestoEnEdicionN3 Is Nothing Or Not oPresupuestoEnEdicionN4 Is Nothing Then
        oIBAseDatosEnEdicion.CancelarEdicion
        Set oIBAseDatosEnEdicion = Nothing
        Set oPresupuestoEnEdicionN1 = Nothing
        Set oPresupuestoEnEdicionN2 = Nothing
        Set oPresupuestoEnEdicionN3 = Nothing
        Set oPresupuestoEnEdicionN4 = Nothing
    End If
    
    cmdDeshacerPresupuesto.Enabled = False
    
    Accion = ACCPresConCon
        
    Screen.MousePointer = vbNormal

End Sub

Private Sub cmdBuscarUO_Click()
    frmESTRORGBuscarUO.g_sOrigen = "frmPresupuestos2"
    frmESTRORGBuscarUO.Show
End Sub

Public Sub cmdEli_Click()
Dim nodx As MSComctlLib.node
Dim teserror As TipoErrorSummit
Dim irespuesta As Integer
Set nodx = tvwEstrPres.selectedItem


If Not nodx Is Nothing Then
        
    Screen.MousePointer = vbHourglass
    
    Select Case Left(nodx.Tag, 5)
        
    Case "PRES1"
             
            Accion = ACCPresConNivel1Eli
            
            Set oPres1Seleccionado = Nothing
            Set oPres1Seleccionado = oFSGSRaiz.generar_CPresConNivel1
            oPres1Seleccionado.Anyo = sdbcAnyo
            oPres1Seleccionado.Cod = DevolverCod(nodx)
            oPres1Seleccionado.UON1 = m_sUON1
            oPres1Seleccionado.UON2 = m_sUON2
            oPres1Seleccionado.UON3 = m_sUON3
            Set oIBaseDatos = oPres1Seleccionado
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                
                Screen.MousePointer = vbNormal

                irespuesta = oMensajes.PreguntaEliminar(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres1Seleccionado.Cod) & " (" & oPres1Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    oPres1Seleccionado.EliminarSolicitudesFavoritasPRES
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        RegistrarAccion accionessummit.ACCPresConNivel1Eli, "Cod:" & CStr(oPres1Seleccionado.Cod)
                        EliminarPARDeEstructura
                        Set oIBaseDatos = Nothing
                    End If
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set oPres1Seleccionado = Nothing
                End If
            
            Else
                'Ha habido un error al iniciar la edicion
                
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres1Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            
            End If
    
    Case "PRES2"
                
            Accion = ACCPresConNivel2Eli
            
            Set oPres2Seleccionado = Nothing
            Set oPres2Seleccionado = oFSGSRaiz.generar_CPresConNivel2
            oPres2Seleccionado.Anyo = sdbcAnyo
            oPres2Seleccionado.Cod = DevolverCod(nodx)
            oPres2Seleccionado.CodPRES1 = DevolverCod(nodx.Parent)
            oPres2Seleccionado.UON1 = m_sUON1
            oPres2Seleccionado.UON2 = m_sUON2
            oPres2Seleccionado.UON3 = m_sUON3
            
            Set oIBaseDatos = oPres2Seleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                
                Screen.MousePointer = vbNormal
                irespuesta = oMensajes.PreguntaEliminar(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres2Seleccionado.Cod) & " (" & oPres2Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    oPres2Seleccionado.EliminarSolicitudesFavoritasPRES
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        RegistrarAccion ACCPresConNivel2Eli, "CodPRES1:" & CStr(oPres2Seleccionado.CodPRES1) & "CodPRES2:" & CStr(oPres2Seleccionado.Cod)
                        EliminarPARDeEstructura
                        Set oIBaseDatos = Nothing
                        Set oPres2Seleccionado = Nothing
                    End If
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set oPres2Seleccionado = Nothing
                End If
                    
            Else
                'Ha habido un error al iniciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres2Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        
        Case "PRES3"
        
             Accion = ACCPresConNivel3Eli
            
            Set oPres3Seleccionado = Nothing
            Set oPres3Seleccionado = oFSGSRaiz.generar_CPresConNivel3
            oPres3Seleccionado.Cod = DevolverCod(nodx)
            oPres3Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent)
            oPres3Seleccionado.CodPRES2 = DevolverCod(nodx.Parent)
            oPres3Seleccionado.Anyo = sdbcAnyo
            oPres3Seleccionado.UON1 = m_sUON1
            oPres3Seleccionado.UON2 = m_sUON2
            oPres3Seleccionado.UON3 = m_sUON3
          
            Set oIBaseDatos = oPres3Seleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                
                Screen.MousePointer = vbNormal
                irespuesta = oMensajes.PreguntaEliminar(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres3Seleccionado.Cod) & " (" & oPres3Seleccionado.Den & ")")
                If irespuesta = vbYes Then
                    oPres3Seleccionado.EliminarSolicitudesFavoritasPRES
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        RegistrarAccion ACCPresConNivel3Eli, "CodPRES1:" & CStr(oPres3Seleccionado.CodPRES1) & "CodPRES2:" & CStr(oPres3Seleccionado.CodPRES2) & "CodPRES3:" & CStr(oPres3Seleccionado.Cod)
                        EliminarPARDeEstructura
                        Set oIBaseDatos = Nothing
                    End If
                
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set oPres3Seleccionado = Nothing
                End If
                
            Else
                'Ha habido un error al inciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres3Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        
        Case "PRES4"
             
            Accion = ACCPresConNivel4Eli
            
            Set oPres4Seleccionado = Nothing
            Set oPres4Seleccionado = oFSGSRaiz.generar_CPresConNivel4
            oPres4Seleccionado.Anyo = sdbcAnyo
            oPres4Seleccionado.Cod = DevolverCod(nodx)
            oPres4Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent.Parent)
            oPres4Seleccionado.CodPRES2 = DevolverCod(nodx.Parent.Parent)
            oPres4Seleccionado.CodPRES3 = DevolverCod(nodx.Parent)
            oPres4Seleccionado.UON1 = m_sUON1
            oPres4Seleccionado.UON2 = m_sUON2
            oPres4Seleccionado.UON3 = m_sUON3
         
            Set oIBaseDatos = oPres4Seleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError = TESnoerror Then
                
                Screen.MousePointer = vbNormal
                irespuesta = oMensajes.PreguntaEliminar(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres4Seleccionado.Cod) & " (" & oPres4Seleccionado.Den & ")")
                
                If irespuesta = vbYes Then
                    oPres4Seleccionado.EliminarSolicitudesFavoritasPRES
                    teserror = oIBaseDatos.EliminarDeBaseDatos
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        Exit Sub
                    Else
                        Screen.MousePointer = vbHourglass
                        RegistrarAccion ACCPresConNivel4Eli, "CodPRES1:" & CStr(oPres4Seleccionado.CodPRES1) & "CodPRES2:" & CStr(oPres4Seleccionado.CodPRES2) & "CodPRES3:" & CStr(oPres4Seleccionado.CodPRES3) & "Cod:" & CStr(oPres4Seleccionado.Cod)
                        EliminarPARDeEstructura
                        Set oIBaseDatos = Nothing
                        Screen.MousePointer = vbNormal
                    End If
                
                Else
                    'No desea continuar
                    oIBaseDatos.CancelarEdicion
                    Set oIBaseDatos = Nothing
                    Set oPres4Seleccionado = Nothing
                End If
                    
            Else
                ' Ha habido un error al inciar la edicion
                TratarError teserror
                Set oIBaseDatos = Nothing
                Set oPres4Seleccionado = Nothing
                Screen.MousePointer = vbNormal
                Exit Sub
            End If
        
    End Select
    
    Screen.MousePointer = vbNormal

End If
Accion = ACCPresConCon

End Sub

Private Sub cmdExpandir_Click()
    Dim nodx As MSComctlLib.node
    
    dAcumulado = 0
    
    Set nodx = tvwEstrPres.selectedItem
    
    If nodx Is Nothing Then Exit Sub
    
    
    Screen.MousePointer = vbHourglass
        
        If sdbgPresupuestos.Visible = False Then
        
            Select Case Left(nodx.Tag, 5)
            
                Case "Raiz "
                    Set oPresupuestosN1 = Nothing
                    Set oPresupuestosN1 = oFSGSRaiz.Generar_CPresContablesNivel1
                    
                    dAcumulado = oPresupuestosN1.CargarTodosLosPresupuestos(sdbcAnyo, , , , , , , , True, m_sUON1, m_sUON2, m_sUON3)
                    MostrarImportes
                Case "PRES1"
                    Set oPres1Seleccionado = Nothing
                    Set oPres1Seleccionado = oFSGSRaiz.generar_CPresConNivel1
                
                    oPres1Seleccionado.Cod = DevolverCod(nodx)
                    If m_sUON1 <> "" Then oPres1Seleccionado.UON1 = m_sUON1
                    If m_sUON2 <> "" Then oPres1Seleccionado.UON2 = m_sUON2
                    If m_sUON3 <> "" Then oPres1Seleccionado.UON3 = m_sUON3
                    dAcumulado = oPres1Seleccionado.CargarTodosLosPresupuestos(sdbcAnyo, , , , , , , , True)
                    MostrarImportes
                Case "PRES2"
                    Set oPres2Seleccionado = Nothing
                    Set oPres2Seleccionado = oFSGSRaiz.generar_CPresConNivel2
                    
                    oPres2Seleccionado.CodPRES1 = DevolverCod(nodx.Parent)
                    oPres2Seleccionado.Cod = DevolverCod(nodx)
                    If m_sUON1 <> "" Then oPres2Seleccionado.UON1 = m_sUON1
                    If m_sUON2 <> "" Then oPres2Seleccionado.UON2 = m_sUON2
                    If m_sUON3 <> "" Then oPres2Seleccionado.UON3 = m_sUON3
                    dAcumulado = oPres2Seleccionado.CargarTodosLosPresupuestos(sdbcAnyo, , , , , , , , True)
                    MostrarImportes
                Case "PRES3"
                    Set oPres3Seleccionado = Nothing
                    Set oPres3Seleccionado = oFSGSRaiz.generar_CPresConNivel3
                    
                    oPres3Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent)
                    oPres3Seleccionado.CodPRES2 = DevolverCod(nodx.Parent)
                    oPres3Seleccionado.Cod = DevolverCod(nodx)
                    If m_sUON1 <> "" Then oPres3Seleccionado.UON1 = m_sUON1
                    If m_sUON2 <> "" Then oPres3Seleccionado.UON2 = m_sUON2
                    If m_sUON3 <> "" Then oPres3Seleccionado.UON3 = m_sUON3
                    dAcumulado = oPres3Seleccionado.CargarTodosLosPresupuestos(sdbcAnyo, , , , , , , , True)
                    MostrarImportes
            End Select
        
        Else
        
            'OcultarImportes
        
        End If
    
    Screen.MousePointer = vbNormal
            
End Sub

Private Sub cmdlistado_Click()
    Dim nodx As MSComctlLib.node

    Dim sDesc As String


    frmLstPRESPorParCon.m_bRuo = bRUO
    If bRUO Then
        frmLstPRESPorParCon.m_sUON1 = basOptimizacion.gUON1Usuario
        frmLstPRESPorParCon.m_sUON2 = basOptimizacion.gUON2Usuario
        frmLstPRESPorParCon.m_sUON3 = basOptimizacion.gUON3Usuario
    End If
    frmLstPRESPorParCon.m_sUON1Sel = m_sUON1
    frmLstPRESPorParCon.m_sUON2Sel = m_sUON2
    frmLstPRESPorParCon.m_sUON3Sel = m_sUON3
    frmLstPRESPorParCon.m_bVerBajaLog = chkBajaLog.Value
    sDesc = DevolverDescripcionUOSel
    frmLstPRESPorParCon.m_sUODescrip = sDesc
    frmLstPRESPorParCon.MostrarUOSeleccionada
    frmLstPRESPorParCon.WindowState = vbNormal
    
    frmLstPRESPorParCon.sdbcAnyo.Text = frmPresupuestos2.sdbcAnyo.Text

    If bOrdenDen Then
        frmLstPRESPorParCon.opOrdDen = True
    Else
        frmLstPRESPorParCon.opOrdCod = True
    End If

    Set nodx = tvwEstrPres.selectedItem

    If nodx Is Nothing Then Exit Sub

    Screen.MousePointer = vbHourglass

    Select Case Left(nodx.Tag, 5)

        Case "Raiz "
                    frmLstPRESPorParCon.CargarNivelesDeDesglose (1)
                    frmLstPRESPorParCon.lblParCon = ""
                    frmLstPRESPorParCon.cmbOblPC.Text = gParametrosGenerales.giNEPP

        Case "PRES1"

                    frmLstPRESPorParCon.lblParCon = DevolverCod(nodx) & DevolverDen(nodx)
                    frmLstPRESPorParCon.sParCon1 = DevolverCod(nodx)
                    frmLstPRESPorParCon.sParCon2 = ""
                    frmLstPRESPorParCon.sParCon3 = ""
                    frmLstPRESPorParCon.sParCon4 = ""
                    frmLstPRESPorParCon.CargarNivelesDeDesglose (1)
                    frmLstPRESPorParCon.cmbOblPC.Text = 1

        Case "PRES2"
                    frmLstPRESPorParCon.sParCon2 = DevolverCod(nodx)
                    frmLstPRESPorParCon.sParCon1 = DevolverCod(nodx.Parent)
                    frmLstPRESPorParCon.sParCon3 = ""
                    frmLstPRESPorParCon.sParCon4 = ""
                    frmLstPRESPorParCon.lblParCon = frmLstPRESPorParCon.sParCon1 & " - " & frmLstPRESPorParCon.sParCon2 & DevolverDen(nodx)
                    frmLstPRESPorParCon.CargarNivelesDeDesglose (2)
                    frmLstPRESPorParCon.cmbOblPC.Text = 2
        Case "PRES3"
                    frmLstPRESPorParCon.sParCon1 = DevolverCod(nodx.Parent.Parent)
                    frmLstPRESPorParCon.sParCon2 = DevolverCod(nodx.Parent)
                    frmLstPRESPorParCon.sParCon3 = DevolverCod(nodx)
                    frmLstPRESPorParCon.sParCon4 = ""
                    frmLstPRESPorParCon.lblParCon = frmLstPRESPorParCon.sParCon1 & " - " & frmLstPRESPorParCon.sParCon2 & " - " & frmLstPRESPorParCon.sParCon3 & DevolverDen(nodx)
                    frmLstPRESPorParCon.CargarNivelesDeDesglose (3)
                    frmLstPRESPorParCon.cmbOblPC.Text = 3
        Case "PRES4"
                    frmLstPRESPorParCon.sParCon1 = DevolverCod(nodx.Parent.Parent.Parent)
                    frmLstPRESPorParCon.sParCon2 = DevolverCod(nodx.Parent.Parent)
                    frmLstPRESPorParCon.sParCon3 = DevolverCod(nodx.Parent)
                    frmLstPRESPorParCon.sParCon4 = DevolverCod(nodx)
                    frmLstPRESPorParCon.lblParCon = frmLstPRESPorParCon.sParCon1 & " - " & frmLstPRESPorParCon.sParCon2 & " - " & frmLstPRESPorParCon.sParCon3 & " - " & frmLstPRESPorParCon.sParCon4 & DevolverDen(nodx)
                    frmLstPRESPorParCon.CargarNivelesDeDesglose (4)
                    frmLstPRESPorParCon.cmbOblPC.Text = 4
        End Select
    
    Screen.MousePointer = vbNormal
    frmLstPRESPorParCon.Show vbModal

End Sub

Public Sub cmdModif_Click()
    
    Dim nodx As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
            
        Screen.MousePointer = vbHourglass
    
        Select Case Left(nodx.Tag, 5)
            
        Case "PRES1"
                Accion = ACCPresConNivel1Mod
                
                Set oPres1Seleccionado = Nothing
                Set oPres1Seleccionado = oFSGSRaiz.generar_CPresConNivel1
                
                oPres1Seleccionado.Cod = DevolverCod(nodx)
                oPres1Seleccionado.Anyo = sdbcAnyo.Text
                If m_sUON1 <> "" Then oPres1Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres1Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres1Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres1Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError = TESnoerror Then
                
                    frmPRESConDetalle.caption = sIdiModificar & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo) & " - " & DevolverCod(nodx)
                    frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                    frmPRESConDetalle.txtDen = oPres1Seleccionado.Den
                    
                    If IsNull(oPres1Seleccionado.importe) Then
                        frmPRESConDetalle.txtImp = ""
                    Else
                        frmPRESConDetalle.txtImp = Format(oPres1Seleccionado.importe, "Standard")
                    End If
                    
                    If IsNull(oPres1Seleccionado.Objetivo) Then
                        frmPRESConDetalle.txtObj = ""
                    Else
                        frmPRESConDetalle.txtObj = Format(oPres1Seleccionado.Objetivo, "0.0#")
                    End If
                    Screen.MousePointer = vbNormal
                    frmPRESConDetalle.Show 1
                    
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres1Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
        
        Case "PRES2"
                 Accion = ACCPresConNivel2Mod
                
                Set oPres2Seleccionado = Nothing
                Set oPres2Seleccionado = oFSGSRaiz.generar_CPresConNivel2
                oPres2Seleccionado.Anyo = sdbcAnyo.Text
                oPres2Seleccionado.Cod = DevolverCod(nodx)
                oPres2Seleccionado.CodPRES1 = DevolverCod(nodx.Parent)
                If m_sUON1 <> "" Then oPres2Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres2Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres2Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres2Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError = TESnoerror Then
                
                    frmPRESConDetalle.caption = sIdiModificar & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                    frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                    frmPRESConDetalle.txtDen = oPres2Seleccionado.Den
                    If Not IsNull(oPres2Seleccionado.importe) Then
                        frmPRESConDetalle.txtImp = Format(oPres2Seleccionado.importe, "Standard")
                    Else
                        frmPRESConDetalle.txtImp = ""
                    End If
                    If Not IsNull(oPres2Seleccionado.Objetivo) Then
                        frmPRESConDetalle.txtObj = Format(oPres2Seleccionado.Objetivo, "0.0#")
                    Else
                        frmPRESConDetalle.txtObj = ""
                    End If
                    Screen.MousePointer = vbNormal
                    frmPRESConDetalle.Show 1
                    
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres2Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            
            Case "PRES3"
                Accion = ACCPresConNivel3Mod
                
                Set oPres3Seleccionado = Nothing
                Set oPres3Seleccionado = oFSGSRaiz.generar_CPresConNivel3
                oPres3Seleccionado.Cod = DevolverCod(nodx)
                oPres3Seleccionado.Anyo = sdbcAnyo.Text
                oPres3Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent)
                oPres3Seleccionado.CodPRES2 = DevolverCod(nodx.Parent)
                If m_sUON1 <> "" Then oPres3Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres3Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres3Seleccionado.UON3 = m_sUON3
            
                Set oIBaseDatos = oPres3Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError = TESnoerror Then
                
                    frmPRESConDetalle.caption = sIdiModificar & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                    frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                    frmPRESConDetalle.txtDen = oPres3Seleccionado.Den
                    If Not IsNull(oPres3Seleccionado.importe) Then
                        frmPRESConDetalle.txtImp = Format(oPres3Seleccionado.importe, "Standard")
                    Else
                        frmPRESConDetalle.txtImp = ""
                    End If
                    If Not IsNull(oPres3Seleccionado.Objetivo) Then
                        frmPRESConDetalle.txtObj = Format(oPres3Seleccionado.Objetivo, "0.0#")
                    Else
                        frmPRESConDetalle.txtObj = ""
                    End If
                    Screen.MousePointer = vbNormal
                    frmPRESConDetalle.Show 1
                    
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres3Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
            
            Case "PRES4"
                Accion = accionessummit.ACCPresConNivel4MOd
                
                Set oPres4Seleccionado = Nothing
                Set oPres4Seleccionado = oFSGSRaiz.generar_CPresConNivel4
                oPres4Seleccionado.Cod = DevolverCod(nodx)
                oPres4Seleccionado.Anyo = sdbcAnyo.Text
                oPres4Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent.Parent)
                oPres4Seleccionado.CodPRES2 = DevolverCod(nodx.Parent.Parent)
                oPres4Seleccionado.CodPRES3 = DevolverCod(nodx.Parent)
                If m_sUON1 <> "" Then oPres4Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres4Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres4Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres4Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError = TESnoerror Then
                
                    frmPRESConDetalle.caption = sIdiModificar & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & CStr(sdbcAnyo) & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                    frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                    frmPRESConDetalle.txtDen = oPres4Seleccionado.Den
                    If Not IsNull(oPres4Seleccionado.importe) Then
                        frmPRESConDetalle.txtImp = Format(oPres4Seleccionado.importe, "Standard")
                    Else
                        frmPRESConDetalle.txtImp = ""
                    End If
                    If Not IsNull(oPres4Seleccionado.Objetivo) Then
                        frmPRESConDetalle.txtObj = Format(oPres4Seleccionado.Objetivo, "0.0#")
                    Else
                        frmPRESConDetalle.txtObj = ""
                    End If
                    Screen.MousePointer = vbNormal
                    frmPRESConDetalle.Show 1
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres4Seleccionado = Nothing
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
        End Select
        Screen.MousePointer = vbNormal
    End If
End Sub

Private Sub cmdModoEdicion_Click()
        
    ''' * Objetivo: Cambiar entre modo de edicion y
    ''' * Objetivo: modo de consulta
    
    Dim v As Variant
    
    If Not bModoEdicion Then
        
        sdbgPresupuestos.AllowUpdate = True
                
        cmdModoEdicion.caption = sIdiMEConsulta
        
        cmdDeshacerPresupuesto.Visible = True
            
        bModoEdicion = True
        
        Accion = ACCPresConCon
    
    Else
                
        If sdbgPresupuestos.DataChanged = True Then
        
            v = sdbgPresupuestos.ActiveCell.Value
            If Me.Visible Then sdbgPresupuestos.SetFocus
            sdbgPresupuestos.ActiveCell.Value = v
            
            bValError = bModError = False
            
            sdbgPresupuestos.Update
            
            If bValError Or bModError Then
                Exit Sub
            End If
            
        End If
        
        sdbgPresupuestos.AllowUpdate = False
                
        cmdDeshacerPresupuesto.Visible = False
                
        cmdModoEdicion.caption = sIdiMEEdicion
        
        cmdRestaurar_Click
        
        bModoEdicion = False
        
    End If
    
    If Me.Visible Then sdbgPresupuestos.SetFocus

End Sub

Private Sub cmdRestaurar_Click()
    
    Screen.MousePointer = vbHourglass
    GenerarEstructuraPresupuestos False
    Screen.MousePointer = vbNormal
    
End Sub

Public Sub BajaLogica()

    Dim nodx As MSComctlLib.node
    Dim nodSiguiente As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    Dim irespuesta As Integer
    
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    
    Set nodx = tvwEstrPres.selectedItem

    If nodx Is Nothing Then
        Exit Sub
    
    Else
        Screen.MousePointer = vbHourglass
        
        Select Case Left(nodx.Tag, 5)
            
            Case "PRES1"
                    
                'Seleccionamos el presupuesto
                Set oPres1Seleccionado = Nothing
                Set oPres1Seleccionado = oFSGSRaiz.generar_CPresConNivel1
                oPres1Seleccionado.Anyo = sdbcAnyo
                oPres1Seleccionado.Cod = DevolverCod(nodx)
                oPres1Seleccionado.UON1 = m_sUON1
                oPres1Seleccionado.UON2 = m_sUON2
                oPres1Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres1Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
            
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres1Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres1Seleccionado.Cod) & " (" & oPres1Seleccionado.Den & ")")
                Else
                    irespuesta = oMensajes.DarBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres1Seleccionado.Cod) & " (" & oPres1Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres1Seleccionado.BajaAltaLogica(Not oPres1Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            oPresupuestos.Item(scod1).BajaLog = False
                            nodx.Image = "PRES1"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            oPresupuestos.Item(scod1).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
                Set oPres1Seleccionado = Nothing
        
        Case "PRES2"
                    
                'Seleccionamos el presupuesto
                Set oPres2Seleccionado = Nothing
                Set oPres2Seleccionado = oFSGSRaiz.generar_CPresConNivel2
                oPres2Seleccionado.Anyo = sdbcAnyo
                oPres2Seleccionado.Cod = DevolverCod(nodx)
                oPres2Seleccionado.CodPRES1 = DevolverCod(nodx.Parent)
                oPres2Seleccionado.UON1 = m_sUON1
                oPres2Seleccionado.UON2 = m_sUON2
                oPres2Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres2Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
            
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres2Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres2Seleccionado.Cod) & " (" & oPres2Seleccionado.Den & ")")
                Else
                    irespuesta = oMensajes.DarBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres2Seleccionado.Cod) & " (" & oPres2Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres2Seleccionado.BajaAltaLogica(Not oPres2Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                        scod2 = DevolverCod(nodx)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).BajaLog = False
                            nodx.Image = "PRES2"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
                Set oPres2Seleccionado = Nothing
                
        Case "PRES3"
                    
                'Seleccionamos el presupuesto
                Set oPres3Seleccionado = Nothing
                Set oPres3Seleccionado = oFSGSRaiz.generar_CPresConNivel3
                oPres3Seleccionado.Anyo = sdbcAnyo
                oPres3Seleccionado.Cod = DevolverCod(nodx)
                oPres3Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent)
                oPres3Seleccionado.CodPRES2 = DevolverCod(nodx.Parent)
                oPres3Seleccionado.UON1 = m_sUON1
                oPres3Seleccionado.UON2 = m_sUON2
                oPres3Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres3Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres3Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres3Seleccionado.Cod) & " (" & oPres3Seleccionado.Den & ")")
                Else
                    irespuesta = oMensajes.DarBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres3Seleccionado.Cod) & " (" & oPres3Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres3Seleccionado.BajaAltaLogica(Not oPres3Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
                        scod3 = DevolverCod(nodx)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).BajaLog = False
                            nodx.Image = "PRES3"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
                Set oPres3Seleccionado = Nothing
        
        Case "PRES4"
                    
                'Seleccionamos el presupuesto
                Set oPres4Seleccionado = Nothing
                Set oPres4Seleccionado = oFSGSRaiz.generar_CPresConNivel4
                oPres4Seleccionado.Anyo = sdbcAnyo
                oPres4Seleccionado.Cod = DevolverCod(nodx)
                oPres4Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent.Parent)
                oPres4Seleccionado.CodPRES2 = DevolverCod(nodx.Parent.Parent)
                oPres4Seleccionado.CodPRES3 = DevolverCod(nodx.Parent)
                oPres4Seleccionado.UON1 = m_sUON1
                oPres4Seleccionado.UON2 = m_sUON2
                oPres4Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres4Seleccionado
                
                teserror = oIBaseDatos.IniciarEdicion
                
                If teserror.NumError <> TESnoerror Then
                    'Ha habido un error al iniciar la edicion
                    basErrores.TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres4Seleccionado = Nothing
                    If Me.Visible Then tvwEstrPres.SetFocus
                    Screen.MousePointer = vbNormal
                    Exit Sub
                End If
                
                'Si el presupuesto est� dado de baja preguntamos "Deshacer baja???"
                'sino preguntamos "Dar de baja???"
                Screen.MousePointer = vbNormal
                If (nodx.Image = "PRESBAJALOGICA") Then
                    irespuesta = oMensajes.DeshacerBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres4Seleccionado.Cod) & " (" & oPres4Seleccionado.Den & ")")
                Else
                    irespuesta = oMensajes.DarBajaLogicaPres(basParametros.gParametrosGenerales.gsSingPres2 & ": " & CStr(oPres4Seleccionado.Cod) & " (" & oPres4Seleccionado.Den & ")")
                End If
                Screen.MousePointer = vbHourglass
                If irespuesta = vbYes Then
                    teserror = oPres4Seleccionado.BajaAltaLogica(Not oPres4Seleccionado.BajaLog)
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        If Me.Visible Then tvwEstrPres.SetFocus
                        Screen.MousePointer = vbNormal
                        Exit Sub
                    Else
                        scod1 = DevolverCod(nodx.Parent.Parent.Parent)
                        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                        scod2 = DevolverCod(nodx.Parent.Parent)
                        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
                        scod3 = DevolverCod(nodx.Parent)
                        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
                        scod4 = DevolverCod(nodx)
                        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
                        If (nodx.Image = "PRESBAJALOGICA") Then
                            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).BajaLog = False
                            nodx.Image = "PRES4"
                            nodx.BackColor = &H80000009 'color blanco
                            cmdBajaLog.caption = m_sCaptionBajaLogica
                        Else
                            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).BajaLog = True
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                            cmdBajaLog.caption = m_sCaptionDeshacerBaja
                        End If
                    End If
                End If
                oIBaseDatos.CancelarEdicion
                Set oIBaseDatos = Nothing
                Set oPres4Seleccionado = Nothing
        
        End Select
    End If

    'Si el check VER BAJAS LOGICAS no esta checked
    'y hemos dado de baja el presupuesto
    'entonces lo eliminamos del arbol, es decir, lo borramos de la pantalla
    If Not chkBajaLog.Value = vbChecked And nodx.Image = "PRESBAJALOGICA" Then
        ' Me posiciono en el siguiente
        If Not nodx.Previous Is Nothing Then
            Set nodSiguiente = nodx.Previous
        Else
            If Not nodx.Next Is Nothing Then
                Set nodSiguiente = nodx.Next
                Else
                    Set nodSiguiente = nodx.Parent
            End If
        End If
    
        
        'Lo elimino del arbol
        tvwEstrPres.Nodes.Remove (frmPresupuestos2.tvwEstrPres.selectedItem.Index)
        tvwEstrPres_NodeClick nodSiguiente
    
    Else
        tvwEstrPres_NodeClick nodx
    End If
    
    Screen.MousePointer = vbNormal
    
    If Me.Visible Then tvwEstrPres.SetFocus
    
    Set oIBaseDatos = Nothing
    Set oPres1Seleccionado = Nothing

End Sub

Public Sub CambiarCodigo()
    Dim teserror As TipoErrorSummit
    Dim nodx As node
    
    
        
    Set nodx = tvwEstrPres.selectedItem
    
    Select Case Left(nodx.Tag, 5)
        Case "PRES1"
            Set oIBaseDatos = Nothing
            Set oPres1Seleccionado = Nothing
            Set oPres1Seleccionado = oFSGSRaiz.generar_CPresConNivel1
            oPres1Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
            oPres1Seleccionado.Cod = DevolverCod(nodx)
            oPres1Seleccionado.UON1 = m_sUON1
            oPres1Seleccionado.UON2 = m_sUON2
            oPres1Seleccionado.UON3 = m_sUON3
            
            Set oIBaseDatos = oPres1Seleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                If Me.Visible Then tvwEstrPres.SetFocus
                Exit Sub
            End If
                                
            frmMODCOD.caption = basParametros.gParametrosGenerales.gsPlurPres2 & " " & sIdiCodigoP
            frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON1
            frmMODCOD.txtCodAct.Text = oPres1Seleccionado.Cod
            Set frmMODCOD.fOrigen = frmPresupuestos2
            g_bCodigoCancelar = False
            frmMODCOD.Show 1
                
            DoEvents
            
            If g_bCodigoCancelar = True Then Exit Sub
                
            If g_sCodigoNuevo = "" Then
                oMensajes.NoValido sIdiCodigo
                Exit Sub
            End If
            
            If UCase(g_sCodigoNuevo) = UCase(oPres1Seleccionado.Cod) Then
                oMensajes.NoValido sIdiCodigo
                Exit Sub
            End If
            
            
            If oPres1Seleccionado.ExistePresEnOtroAnyo Then
                If oMensajes.PreguntaModificarCodigoEnTodosLosAnyos = vbYes Then
                    Screen.MousePointer = vbHourglass
                    teserror = oPres1Seleccionado.CambioDeCodigoGeneral(g_sCodigoNuevo)
                    Screen.MousePointer = vbNormal
                Else
                    Screen.MousePointer = vbHourglass
                    teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
                    Screen.MousePointer = vbNormal
                End If
            Else
                Screen.MousePointer = vbHourglass
                teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
                Screen.MousePointer = vbNormal
            End If
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
    
            ''' Actualizar datos
            cmdRestaurar_Click
                        
        Case "PRES2"
            Set oPres2Seleccionado = Nothing
            Set oPres2Seleccionado = oFSGSRaiz.generar_CPresConNivel2
            oPres2Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
            oPres2Seleccionado.Cod = DevolverCod(nodx)
            oPres2Seleccionado.CodPRES1 = DevolverCod(nodx.Parent)
            oPres2Seleccionado.UON1 = m_sUON1
            oPres2Seleccionado.UON2 = m_sUON2
            oPres2Seleccionado.UON3 = m_sUON3
            
            Set oIBaseDatos = oPres2Seleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                If Me.Visible Then tvwEstrPres.SetFocus
                Exit Sub
            End If
            
                    
            frmMODCOD.caption = basParametros.gParametrosGenerales.gsSingPres2 & sIdiCodigoP
            frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON2
            frmMODCOD.txtCodAct.Text = oPres2Seleccionado.Cod
            Set frmMODCOD.fOrigen = frmPresupuestos2
            g_bCodigoCancelar = False
            frmMODCOD.Show 1
                
            DoEvents
            
            If g_bCodigoCancelar = True Then Exit Sub
                
            If g_sCodigoNuevo = "" Then
                oMensajes.NoValido sIdiCodigo
                Exit Sub
            End If
            
            If UCase(g_sCodigoNuevo) = UCase(oPres2Seleccionado.Cod) Then
                oMensajes.NoValido sIdiCodigo
                Exit Sub
            End If
            
            Screen.MousePointer = vbHourglass
            
            teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
            
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
    
            ''' Actualizar datos
        
            cmdRestaurar_Click
                                                
        Case "PRES3"
            Set oPres3Seleccionado = Nothing
            Set oPres3Seleccionado = oFSGSRaiz.generar_CPresConNivel3
            oPres3Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
            oPres3Seleccionado.Cod = DevolverCod(nodx)
            oPres3Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent)
            oPres3Seleccionado.CodPRES2 = DevolverCod(nodx.Parent)
            oPres3Seleccionado.UON1 = m_sUON1
            oPres3Seleccionado.UON2 = m_sUON2
            oPres3Seleccionado.UON3 = m_sUON3
          
            Set oIBaseDatos = oPres3Seleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                If Me.Visible Then tvwEstrPres.SetFocus
                Exit Sub
            End If
            
                    
            frmMODCOD.caption = basParametros.gParametrosGenerales.gsSingPres2 & " " & sIdiCodigoP
            frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON3
            frmMODCOD.txtCodAct.Text = oPres3Seleccionado.Cod
            Set frmMODCOD.fOrigen = frmPresupuestos2
            g_bCodigoCancelar = False
            frmMODCOD.Show 1
                
            DoEvents
            
            If g_bCodigoCancelar = True Then Exit Sub
                
            If g_sCodigoNuevo = "" Then
                oMensajes.NoValido sIdiCodigo
                Exit Sub
            End If
            
            If UCase(g_sCodigoNuevo) = UCase(oPres3Seleccionado.Cod) Then
                oMensajes.NoValido sIdiCodigo
                Exit Sub
            End If
            
            Screen.MousePointer = vbHourglass
            
            teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
            
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
    
            ''' Actualizar datos
        
            cmdRestaurar_Click
                                
        Case "PRES4"
            Set oPres4Seleccionado = Nothing
            Set oPres4Seleccionado = oFSGSRaiz.generar_CPresConNivel4
            oPres4Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
            oPres4Seleccionado.Cod = DevolverCod(nodx)
            oPres4Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent.Parent)
            oPres4Seleccionado.CodPRES2 = DevolverCod(nodx.Parent.Parent)
            oPres4Seleccionado.CodPRES3 = DevolverCod(nodx.Parent)
            oPres4Seleccionado.UON1 = m_sUON1
            oPres4Seleccionado.UON2 = m_sUON2
            oPres4Seleccionado.UON3 = m_sUON3
           
            Set oIBaseDatos = oPres4Seleccionado
            
            teserror = oIBaseDatos.IniciarEdicion
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                If Me.Visible Then tvwEstrPres.SetFocus
                Exit Sub
            End If
            
                    
            frmMODCOD.caption = basParametros.gParametrosGenerales.gsSingPres2 & " " & sIdiCodigoP
            frmMODCOD.txtCodNue.MaxLength = basParametros.gLongitudesDeCodigos.giLongCodPRESCON4
            frmMODCOD.txtCodAct.Text = oPres4Seleccionado.Cod
            Set frmMODCOD.fOrigen = frmPresupuestos2
            g_bCodigoCancelar = False
            frmMODCOD.Show 1
                
            DoEvents
            
            If g_bCodigoCancelar = True Then Exit Sub
                
            If g_sCodigoNuevo = "" Then
                oMensajes.NoValido sIdiCodigoP
                Exit Sub
            End If
            
            If UCase(g_sCodigoNuevo) = UCase(oPres4Seleccionado.Cod) Then
                oMensajes.NoValido sIdiCodigoP
                Exit Sub
            End If
            
            Screen.MousePointer = vbHourglass
            
            teserror = oIBaseDatos.CambiarCodigo(g_sCodigoNuevo)
            
            Screen.MousePointer = vbNormal
            
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                Exit Sub
            End If
    
            ''' Actualizar datos
        
            cmdRestaurar_Click
    End Select

End Sub
Private Sub Arrange()

    If Me.WindowState = 0 Then     'Limitamos la reducci�n
        If Me.Width <= 7250 Then   'de tama�o de la ventana
            Me.Width = 7350        'para que no se superpongan
            Exit Sub               'unos controles sobre
        End If                     'otros. S�lo lo hacemos
        If Me.Height <= 4500 Then  'cuando no se maximiza ni
            Me.Height = 4600       'minimiza la ventana,
            Exit Sub               'WindowState=0, pues cuando esto
        End If                     'ocurre no se pueden cambiar las
    End If                         'propiedades Form.Height y Form.Width
        
    If Me.WindowState <> 1 Then
        If m_bLoad Then Exit Sub
        SSTabPresupuestos.Height = Me.Height - 500
        SSTabPresupuestos.Width = Me.Width - 200
        Select Case SSTabPresupuestos.Tab
            Case 0
                lblEstrorg1.Width = SSTabPresupuestos.Left - lblEstrorg1.Left + SSTabPresupuestos.Width - 100
                tvwestrorg.Width = SSTabPresupuestos.Left - tvwestrorg.Left + SSTabPresupuestos.Width - 100
                tvwestrorg.Height = SSTabPresupuestos.Top - tvwestrorg.Top + SSTabPresupuestos.Height - 500
                picNavigate1.Top = tvwestrorg.Top + tvwestrorg.Height + 50
            Case 1
                If sdbgPresupuestos.Visible Then
                    picNavigate2.Visible = False
                    picImportes.Visible = True
                    lblEstrorg2.Width = SSTabPresupuestos.Left - lblEstrorg2.Left + SSTabPresupuestos.Width - 100
                    sdbcAnyo.Left = lblEstrorg2.Left + lblEstrorg2.Width - sdbcAnyo.Width
                    lblAnyo.Left = lblEstrorg2.Left + lblEstrorg2.Width - sdbcAnyo.Width - lblAnyo.Width
                    tvwEstrPres.Width = SSTabPresupuestos.Left - tvwEstrPres.Left + SSTabPresupuestos.Width - 100
                    tvwEstrPres.Height = SSTabPresupuestos.Top - tvwEstrPres.Top + SSTabPresupuestos.Height - 3000
                    picSepar.Left = tvwEstrPres.Left
                    picSepar.Top = tvwEstrPres.Top + tvwEstrPres.Height + 50
                    cmdExpandir.Top = picSepar.Top
                    cmdExpandir.Left = picSepar.Left + picSepar.Width + 65
                    sdbgPresupuestos.Top = picSepar.Top + picSepar.Height + 50
                    sdbgPresupuestos.Height = SSTabPresupuestos.Top - sdbgPresupuestos.Top + SSTabPresupuestos.Height - 600
                    sdbgPresupuestos.Left = tvwEstrPres.Left
                    sdbgPresupuestos.Width = tvwEstrPres.Width
                    sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 10 / 100
                    sdbgPresupuestos.Columns(1).Width = sdbgPresupuestos.Width * 35 / 100
                    sdbgPresupuestos.Columns(2).Width = sdbgPresupuestos.Width * 30 / 100
                    sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 25 / 100 - 580
                    picImportes.Left = sdbgPresupuestos.Left
                    picImportes.Top = sdbgPresupuestos.Top + sdbgPresupuestos.Height + 25
                Else
                    picImportes.Visible = False
                    picNavigate2.Visible = True
                    lblEstrorg2.Width = SSTabPresupuestos.Left - lblEstrorg2.Left + SSTabPresupuestos.Width - 100
                    sdbcAnyo.Left = lblEstrorg2.Left + lblEstrorg2.Width - sdbcAnyo.Width
                    lblAnyo.Left = lblEstrorg2.Left + lblEstrorg2.Width - sdbcAnyo.Width - lblAnyo.Width
                    tvwEstrPres.Width = SSTabPresupuestos.Left - tvwEstrPres.Left + SSTabPresupuestos.Width - 100
                    tvwEstrPres.Height = SSTabPresupuestos.Top - tvwEstrPres.Top + SSTabPresupuestos.Height - 1000
                    picSepar.Left = tvwEstrPres.Left
                    picSepar.Top = tvwEstrPres.Top + tvwEstrPres.Height + 50
                    cmdExpandir.Top = picSepar.Top
                    cmdExpandir.Left = picSepar.Left + picSepar.Width + 65
                    picNavigate2.Left = picSepar.Left
                    picNavigate2.Top = picSepar.Top + picSepar.Height + 25
                End If
        End Select
    End If
End Sub

Private Sub cmdRestaurarUO_Click()
    Screen.MousePointer = vbHourglass
    CargarEstructuraOrg False
    Screen.MousePointer = vbNormal
End Sub

Private Sub Form_Load()
        
    m_bLoad = True
    Me.Width = 9250
    Me.Height = 6390
    m_bLoad = False
    CargarRecursos
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    bRUO = False
    ConfigurarSeguridad
    
    iUOBase = 0
    If bRUO Then
        If (CStr(basOptimizacion.gUON3Usuario) <> "") Then
            iUOBase = 3
        ElseIf (CStr(basOptimizacion.gUON2Usuario) <> "") Then
            iUOBase = 2
        ElseIf (CStr(basOptimizacion.gUON1Usuario) <> "") Then
            iUOBase = 1
        End If
    End If
    
    bNoContinuar = True
        
    If m_bCargarEstructuraOrganizativa Then CargarEstructuraOrg False
    CargarAnyos
    Accion = accionessummit.ACCPresConCon
End Sub

Private Sub GenerarEstructuraPresupuestos(ByVal bOrdenadoPorDen As Boolean, Optional ByVal bBuscarEnOtrosAnyos As Boolean = False)
    Dim nodo As MSComctlLib.node
    
    tvwEstrPres.Nodes.clear
    
    Set nodo = tvwEstrPres.Nodes.Add(, , "Raiz ", PonerPrimeraLetraEnMayuscula(gParametrosGenerales.gsPlurPres2), "Raiz")
    nodo.Tag = "Raiz "
    
    nodo.Expanded = True
        
    Set oPresupuestos = oFSGSRaiz.Generar_CPresContablesNivel1
 
    If SSTabPresupuestos.TabVisible(0) = True Then
        Set nodo = tvwestrorg.selectedItem
        If nodo Is Nothing Then Exit Sub
        Select Case Left(nodo.Tag, 4)
            Case "UON3"
                oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, DevolverCod(nodo.Parent.Parent), DevolverCod(nodo.Parent), DevolverCod(nodo), bBuscarEnOtrosAnyos
            Case "UON2"
                oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, DevolverCod(nodo.Parent), DevolverCod(nodo), , bBuscarEnOtrosAnyos
            Case "UON1"
                oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, DevolverCod(nodo), , , bBuscarEnOtrosAnyos
            Case "UON0"
                oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, , , , bBuscarEnOtrosAnyos
        End Select
    Else
        If basOptimizacion.gUON3Usuario <> "" Then
            oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bBuscarEnOtrosAnyos
        ElseIf basOptimizacion.gUON2Usuario <> "" Then
            oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, , bBuscarEnOtrosAnyos
        ElseIf basOptimizacion.gUON1Usuario <> "" Then
            oPresupuestos.GenerarEstructuraPresupuestos sdbcAnyo, sdbcAnyo, bOrdenadoPorDen, basOptimizacion.gUON1Usuario, , , bBuscarEnOtrosAnyos
        End If
    End If
    
    'Ponemos en la combo el a�o cuyos presupuestos aparecen en pantalla y
    'lo a�adimos a la combo si no se encuentra ya en ella.
    If Not oPresupuestos.Item(1) Is Nothing Then
        sdbcAnyo.Text = oPresupuestos.Item(1).Anyo
        If (oPresupuestos.Item(1).Anyo < Year(Date) - 10) Or (oPresupuestos.Item(1).Anyo > Year(Date) + 10) Then
            If Not ExisteAnyoEnCombo(CStr(oPresupuestos.Item(1).Anyo)) Then
                sdbcAnyo.AddItem sdbcAnyo.Text
            End If
        End If
    End If
    
    'Generamos el arbol de presupuestos que se ve en el formulario
    GenerarArbolPresupuestos
            
    Exit Sub
    
Error:
    Set nodo = Nothing
    Resume Next
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde Form_Load</remarks>
''' <revision>LTG 03/05/2013</revision>

Private Sub ConfigurarSeguridad()
    Dim i As Byte

    bModif = False
    bModifCod = False
    bRUO = False
    m_sUON1 = ""
    m_sUON2 = ""
    m_sUON3 = ""

    If basParametros.gParametrosIntegracion.gaSoloImportar(EntidadIntegracion.Presupuestos2) Then
        
        ''' La integraci�n de presupuestos est� activada en sentido ERP->Fullstep
        ''' y no se pueden modificar los presupuestos1.
        bModif = False
        bModifCod = False
    Else
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorParConModificar)) Is Nothing) Then
            bModif = True
        End If
        bModifCod = Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PRESPorParModificarCodigo)) Is Nothing)
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.presporparconRestUO)) Is Nothing) Then
            bRUO = True
            basOptimizacion.gUON1Usuario = oUsuarioSummit.Persona.UON1
            basOptimizacion.gUON2Usuario = oUsuarioSummit.Persona.UON2
            basOptimizacion.gUON3Usuario = oUsuarioSummit.Persona.UON3
            m_sUON1 = basOptimizacion.gUON1Usuario
            m_sUON2 = basOptimizacion.gUON2Usuario
            m_sUON3 = basOptimizacion.gUON3Usuario
        End If
    End If
    
    If Not bModif Then
        cmdModoEdicion.Visible = False
        cmdA�adir.Visible = False
        cmdModif.Visible = False
        cmdEli.Visible = False
        cmdBajaLog.Visible = False
        cmdRestaurar.Left = cmdA�adir.Left
        cmdBuscar.Left = cmdModif.Left
        cmdListado.Left = cmdEli.Left
        For i = 1 To 4
            MDI.mnuPopUpEstrPresConN0.Item(i).Visible = False
        Next i
        For i = 1 To 8
            MDI.mnuPopUpEstrPresConN1.Item(i).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(i).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(i).Visible = False
        Next i
        For i = 10 To 12
            MDI.mnuPopUpEstrPresConN1.Item(i).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(i).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(i).Visible = False
        Next i
        For i = 1 To 6
            MDI.mnuPopUpEstrPresConN4.Item(i).Visible = False
        Next i
        For i = 8 To 10
            MDI.mnuPopUpEstrPresConN4.Item(i).Visible = False
        Next i
    Else
        MDI.mnuPopUpEstrPresConN0.Item(1).Visible = True
        MDI.mnuPopUpEstrPresConN0.Item(2).Visible = True
    End If
    
    m_bCargarEstructuraOrganizativa = True
    If bRUO Then
        If Not oUsuarioSummit.ExisteUODebajo Then
            SSTabPresupuestos.TabVisible(1) = True
            SSTabPresupuestos.TabVisible(0) = False
            m_bCargarEstructuraOrganizativa = False
            MDI.mnuPopUpEstrPresConN0.Item(3).Visible = False
            If m_sUON3 <> "" Then
                lblEstrorg2.caption = m_sUON3
            ElseIf m_sUON2 <> "" Then
                lblEstrorg2.caption = m_sUON2
            ElseIf m_sUON1 <> "" Then
                lblEstrorg2.caption = m_sUON1
            End If
            lblEstrorg2.caption = lblEstrorg2.caption & " - " & DevolverDescripcionUOSel
        Else
            SSTabPresupuestos.TabVisible(1) = False
        End If
    Else
        SSTabPresupuestos.TabVisible(1) = False
    End If

    cmdListado.Visible = True

End Sub

Public Function DevolverCod(ByVal node As MSComctlLib.node) As Variant

    If node Is Nothing Then Exit Function
    
    Select Case Left(node.Tag, 5)
        Case "PRES1", "PRES2", "PRES3", "PRES4"
            DevolverCod = Right(node.Tag, Len(node.Tag) - 5)
    End Select
    
    Select Case Left(node.Tag, 4)
        Case "UON1", "UON2", "UON3"
            DevolverCod = Right(node.Tag, Len(node.Tag) - 4)
    End Select
End Function
Private Function DevolverDen(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function

Select Case Left(node.Tag, 5)

Case "PRES1"
        
        DevolverDen = Right(node.Text, (Len(node.Text) - (Len(node.Tag) - 5)))
    
Case "PRES2"
    
        DevolverDen = Right(node.Text, (Len(node.Text) - (Len(node.Tag) - 5)))
    
Case "PRES3"
    
        DevolverDen = Right(node.Text, (Len(node.Text) - (Len(node.Tag) - 5)))

Case "PRES4"
    
        DevolverDen = Right(node.Text, (Len(node.Text) - (Len(node.Tag) - 5)))

End Select

End Function

Public Function DevolverAnyo(ByVal node As MSComctlLib.node) As Variant

If node Is Nothing Then Exit Function

If Left(node.Tag, 5) = "-ANYO-" Then
        
        DevolverAnyo = Int(val(Right(node.Tag, Len(node.Tag) - 5)))
Else
        DevolverAnyo = 0
End If

End Function

Private Sub MostrarImportes()
    sdbgPresupuestos.Visible = True
    sdbgPresupuestos.ReBind
    picNavigate2.Visible = False
    picImportes.Visible = True
    sdbgPresupuestos.caption = " " & sIdiSumaDesglose & " " & DblToStr(dAcumulado)
    DoEvents
    Arrange
End Sub

Private Sub OcultarImportes()
    sdbgPresupuestos.Visible = False
    picNavigate2.Visible = True
    picImportes.Visible = False
    bNoContinuar = True
    Arrange
End Sub

Private Sub Form_Resize()
    Arrange
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Set oPresupuestos = Nothing
    Me.Visible = False
    
End Sub

Private Sub sdbcAnyo_Click()
    
    txtPartida = ""
    txtPres = ""
    txtObj = ""

    cmdRestaurar_Click
    
End Sub

Private Sub sdbgPresupuestos_AfterUpdate(RtnDispErrMsg As Integer)
    ''' * Objetivo: Si no hay error, volver a la
    ''' * Objetivo: situacion normal
    
    If bModError = False Then
        cmdDeshacerPresupuesto.Enabled = False
    End If
    
End Sub

Private Sub sdbgPresupuestos_BeforeUpdate(Cancel As Integer)
    
    ''' * Objetivo: Validar los datos
Dim irespuesta As Integer

    bValError = False
    Cancel = False
    
    If Trim(sdbgPresupuestos.Columns(2).Value) <> "" Then
        If Not IsNumeric(sdbgPresupuestos.Columns(2).Value) Then
            oMensajes.NoValido sIdiPresupuesto
            Cancel = True
            bValError = True
            If Me.Visible Then sdbgPresupuestos.SetFocus
            Exit Sub
        End If
    End If
        
    If Trim(sdbgPresupuestos.Columns(3).Value) <> "" Then
        If Not IsNumeric(sdbgPresupuestos.Columns(3).Value) Then
            oMensajes.NoValido sIdiObjetivo
            Cancel = True
            bValError = True
            If Me.Visible Then sdbgPresupuestos.SetFocus
            Exit Sub
        End If
    End If
    
    
    If bNoContinuar And dPresGeneralSel <> 0 Then
        If dAcumulado + NullToDbl0(StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)) > dPresGeneralSel Then
            irespuesta = oMensajes.PreguntaPresImporteSuperior
            If irespuesta = vbNo Then
                Cancel = True
                bValError = Cancel
                If Me.Visible Then sdbgPresupuestos.SetFocus
            Else
                bNoContinuar = False
            End If
        End If
    End If
    
        

End Sub

Private Sub sdbgPresupuestos_Change()
    
    ''' * Objetivo: Iniciar la edicion, si no hay errores
    ''' * Objetivo: Atencion si han cambiado los datos
    
    Dim teserror As TipoErrorSummit
    
    If cmdDeshacerPresupuesto.Enabled = False Then

        cmdDeshacerPresupuesto.Enabled = True

    End If
    
    
    If Accion = ACCPresConCon And Not sdbgPresupuestos.IsAddRow Then
                
        Set oPresupuestoEnEdicionN1 = Nothing
        Set oPresupuestoEnEdicionN2 = Nothing
        Set oPresupuestoEnEdicionN3 = Nothing
        Set oPresupuestoEnEdicionN4 = Nothing
        
        If Not oPres3Seleccionado Is Nothing Then
            
            'Edicion de presupuesto de nivel  4
            
            Set oPresupuestoEnEdicionN4 = oPres3Seleccionado.PresContablesNivel4.Item(CStr(sdbgPresupuestos.Bookmark))
           
            Set oIBAseDatosEnEdicion = oPresupuestoEnEdicionN4
            
            teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
            If teserror.NumError = TESInfModificada Then
                TratarError teserror
                sdbgPresupuestos.DataChanged = False
                sdbgPresupuestos.Columns(0).Value = oPresupuestoEnEdicionN4.Cod
                sdbgPresupuestos.Columns(1).Value = oPresupuestoEnEdicionN4.Den
                sdbgPresupuestos.Columns(2).Value = oPresupuestoEnEdicionN4.importe
                sdbgPresupuestos.Columns(3).Value = oPresupuestoEnEdicionN4.Objetivo
                teserror.NumError = TESnoerror
            End If
                    
            If teserror.NumError <> TESnoerror Then
                TratarError teserror
                If Me.Visible Then sdbgPresupuestos.SetFocus
            Else
                Accion = ACCPresConNivel4MOd
            End If
        
        Else
            
            If Not oPres2Seleccionado Is Nothing Then
                
                'Edicion de presupuesto de nivel  3
                
                Set oPresupuestoEnEdicionN3 = oPres2Seleccionado.PresContablesNivel3.Item(CStr(sdbgPresupuestos.Bookmark))
               
                Set oIBAseDatosEnEdicion = oPresupuestoEnEdicionN3
                    
                teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
                If teserror.NumError = TESInfModificada Then
                    TratarError teserror
                    sdbgPresupuestos.DataChanged = False
                    sdbgPresupuestos.Columns(0).Value = oPresupuestoEnEdicionN3.Cod
                    sdbgPresupuestos.Columns(1).Value = oPresupuestoEnEdicionN3.Den
                    sdbgPresupuestos.Columns(2).Value = oPresupuestoEnEdicionN3.importe
                    sdbgPresupuestos.Columns(3).Value = oPresupuestoEnEdicionN3.Objetivo
                    teserror.NumError = TESnoerror
                End If
                    
                If teserror.NumError <> TESnoerror Then
                    TratarError teserror
                    If Me.Visible Then sdbgPresupuestos.SetFocus
                Else
                    Accion = ACCPresConNivel3Mod
                End If
            Else
                If Not oPres1Seleccionado Is Nothing Then
                    
                    'Edicion de presupuesto de nivel  2
                    
                    Set oPresupuestoEnEdicionN2 = oPres1Seleccionado.PresContablesNivel2.Item(CStr(sdbgPresupuestos.Bookmark))
                   
                    Set oIBAseDatosEnEdicion = oPresupuestoEnEdicionN2
                
                    teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
                    If teserror.NumError = TESInfModificada Then
                        TratarError teserror
                        sdbgPresupuestos.DataChanged = False
                        sdbgPresupuestos.Columns(0).Value = oPresupuestoEnEdicionN2.Cod
                        sdbgPresupuestos.Columns(1).Value = oPresupuestoEnEdicionN2.Den
                        sdbgPresupuestos.Columns(2).Value = oPresupuestoEnEdicionN2.importe
                        sdbgPresupuestos.Columns(3).Value = oPresupuestoEnEdicionN2.Objetivo
                        teserror.NumError = TESnoerror
                    End If
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        If Me.Visible Then sdbgPresupuestos.SetFocus
                    Else
                        Accion = ACCPresConNivel2Mod
                    End If
                Else
                    
                    'Edicion de presupuesto de nivel  1
                    
                    Set oPresupuestoEnEdicionN1 = oPresupuestosN1.Item(CStr(sdbgPresupuestos.Bookmark))
                  
                    Set oIBAseDatosEnEdicion = oPresupuestoEnEdicionN1
                
                    teserror = oIBAseDatosEnEdicion.IniciarEdicion
        
                    If teserror.NumError = TESInfModificada Then
                        TratarError teserror
                        sdbgPresupuestos.DataChanged = False
                        sdbgPresupuestos.Columns(0).Value = oPresupuestoEnEdicionN1.Cod
                        sdbgPresupuestos.Columns(1).Value = oPresupuestoEnEdicionN1.Den
                        sdbgPresupuestos.Columns(2).Value = oPresupuestoEnEdicionN1.importe
                        sdbgPresupuestos.Columns(3).Value = oPresupuestoEnEdicionN1.Objetivo
                        teserror.NumError = TESnoerror
                    End If
                    
                    If teserror.NumError <> TESnoerror Then
                        TratarError teserror
                        If Me.Visible Then sdbgPresupuestos.SetFocus
                    Else
                        Accion = ACCPresConNivel1Mod
                    End If
                    
                End If
            End If
        End If
     End If
     
        
        
End Sub

Private Sub sdbgPresupuestos_InitColumnProps()
    
    sdbgPresupuestos.Columns(0).Width = sdbgPresupuestos.Width * 10 / 100
    sdbgPresupuestos.Columns(1).Width = sdbgPresupuestos.Width * 40 / 100
    sdbgPresupuestos.Columns(2).Width = sdbgPresupuestos.Width * 30 / 100
    sdbgPresupuestos.Columns(3).Width = sdbgPresupuestos.Width * 20 / 100 - 550
    
End Sub

Private Sub sdbgPresupuestos_UnboundReadData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, StartLocation As Variant, ByVal ReadPriorRows As Boolean)

    ''' * Objetivo: La grid pide datos, darle datos
    ''' * Objetivo: siguiendo proceso estandar
    ''' * Recibe: Buffer donde situar los datos, fila de comienzo
    ''' * Recibe: y si la lectura es hacia atras o normal (hacia adelante)
    
    Dim r As Integer
    Dim i As Integer
    Dim j As Integer
    
    Dim opres1 As CPresConNivel1
    Dim oPRES2 As CPresconNivel2
    Dim oPRES3 As CPresConNivel3
    Dim oPRES4 As CPresconNivel4
    
    Dim iNumPres As Integer

    If oPres1Seleccionado Is Nothing And _
    oPres2Seleccionado Is Nothing And _
    oPres3Seleccionado Is Nothing And _
    oPresupuestosN1 Is Nothing Then
    
        RowBuf.RowCount = 0
        Exit Sub
    End If
    
    
    If Not oPres3Seleccionado Is Nothing Then
        iNumPres = oPres3Seleccionado.PresContablesNivel4.Count
    Else
        If Not oPres2Seleccionado Is Nothing Then
            iNumPres = oPres2Seleccionado.PresContablesNivel3.Count
        Else
            If Not oPres1Seleccionado Is Nothing Then
                iNumPres = oPres1Seleccionado.PresContablesNivel2.Count
            Else
                'Presupuestos de 1er nivel
                iNumPres = oPresupuestosN1.Count
            End If
        End If
    End If
    
    If IsNull(StartLocation) Then       'If the grid is empty then
        If ReadPriorRows Then               'If moving backwards through grid then
            p = iNumPres - 1                             'pointer = # of last grid row
        Else                                        'else
            p = 0                                       'pointer = # of first grid row
        End If
    Else                                        'If the grid already has data in it then
        p = StartLocation                       'pointer = location just before or after the row where data will be added
                
        If ReadPriorRows Then               'If moving backwards through grid then
            p = p - 1                               'move pointer back one row
        Else                                        'else
            p = p + 1                               'move pointer ahead one row
        End If
    End If
                
                'The pointer (p) now points to the row of the grid where you will start adding data.
                
    If Not oPres3Seleccionado Is Nothing Then
        For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
            If p < 0 Or p > iNumPres - 1 Then Exit For           'If the pointer is outside the grid then stop this
            
            Set oPRES4 = oPres3Seleccionado.PresContablesNivel4.Item(CStr(p))
                For j = 0 To 3
                    Select Case j
                            Case 0:
                                        RowBuf.Value(i, 0) = oPRES4.Cod
                            Case 1:
                                        RowBuf.Value(i, 1) = oPRES4.Den
                            Case 2:
                                        RowBuf.Value(i, 2) = oPRES4.importe
                            Case 3:
                                        RowBuf.Value(i, 3) = oPRES4.Objetivo
                                        
                    End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
                Next j
                    
                    RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
                
                    If ReadPriorRows Then                               'move the pointer forward or backward, depending
                        p = p - 1                                           'on which way it's supposed to move
                    Else
                        p = p + 1
                    End If
                        r = r + 1
        Next i
    Else
        If Not oPres2Seleccionado Is Nothing Then
            For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
                If p < 0 Or p > iNumPres - 1 Then Exit For           'If the pointer is outside the grid then stop this
                Set oPRES3 = oPres2Seleccionado.PresContablesNivel3.Item(CStr(p))
                    For j = 0 To 3
                        Select Case j
                                Case 0:
                                        RowBuf.Value(i, 0) = oPRES3.Cod
                                Case 1:
                                        RowBuf.Value(i, 1) = oPRES3.Den
                                Case 2:
                                        RowBuf.Value(i, 2) = oPRES3.importe
                                Case 3:
                                        RowBuf.Value(i, 3) = oPRES3.Objetivo
                        End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
                    Next j
                    
                    RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
                
                    If ReadPriorRows Then                               'move the pointer forward or backward, depending
                        p = p - 1                                           'on which way it's supposed to move
                    Else
                        p = p + 1
                    End If
                        r = r + 1
            Next i
        Else
            If Not oPres1Seleccionado Is Nothing Then
                For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
                If p < 0 Or p > iNumPres - 1 Then Exit For           'If the pointer is outside the grid then stop this
                    Set oPRES2 = oPres1Seleccionado.PresContablesNivel2.Item(CStr(p))
                        For j = 0 To 3
                            Select Case j
                                    Case 0:
                                            RowBuf.Value(i, 0) = oPRES2.Cod
                                    Case 1:
                                            RowBuf.Value(i, 1) = oPRES2.Den
                                    Case 2:
                                            RowBuf.Value(i, 2) = oPRES2.importe
                                    Case 3:
                                            RowBuf.Value(i, 3) = oPRES2.Objetivo
                            End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
                        Next j
                    RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
                
                    If ReadPriorRows Then                               'move the pointer forward or backward, depending
                        p = p - 1                                           'on which way it's supposed to move
                    Else
                        p = p + 1
                    End If
                        r = r + 1
                Next i
            Else
                For i = 0 To RowBuf.RowCount - 1                    'For each row in the row buffer
                If p < 0 Or p > iNumPres - 1 Then Exit For           'If the pointer is outside the grid then stop this
                    Set opres1 = oPresupuestosN1.Item(CStr(p))
                    For j = 0 To 3
                        Select Case j
                                Case 0:
                                        RowBuf.Value(i, 0) = opres1.Cod
                                Case 1:
                                        RowBuf.Value(i, 1) = opres1.Den
                                Case 2:
                                        RowBuf.Value(i, 2) = opres1.importe
                                Case 3:
                                        RowBuf.Value(i, 3) = opres1.Objetivo
                        End Select           'Set the value of each column in the row buffer to the corresponding value in the arrray
                    Next j
                    
                    RowBuf.Bookmark(i) = p                              'set the value of the bookmark for the current row in the rowbuffer
                
                    If ReadPriorRows Then                               'move the pointer forward or backward, depending
                        p = p - 1                                           'on which way it's supposed to move
                    Else
                        p = p + 1
                    End If
                        r = r + 1
                Next i
            End If
        End If
    End If
                
    RowBuf.RowCount = r                                         'set the size of the row buffer to the number of rows read
            
    Set opres1 = Nothing
    Set oPRES2 = Nothing
    Set oPRES3 = Nothing
    Set oPRES4 = Nothing
            
        
End Sub

Private Sub sdbgPresupuestos_UnboundWriteData(ByVal RowBuf As SSDataWidgets_B.ssRowBuffer, WriteLocation As Variant)
        
    ''' * Objetivo: Actualizar la fila en edicion
    ''' * Recibe: Buffer con los datos y bookmark de la fila
    
    Dim teserror As TipoErrorSummit
    Dim nodx As MSComctlLib.node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim v As Variant
    Dim dImpAnterior As Double
    
    bModError = False
    
    Set nodx = tvwEstrPres.selectedItem
    
    ''' Modificamos en la base de datos
    
    If Not oPres3Seleccionado Is Nothing Then
        
        oPresupuestoEnEdicionN4.Den = sdbgPresupuestos.Columns(1).Value
        dImpAnterior = NullToDbl0(oPresupuestoEnEdicionN4.importe)
        oPresupuestoEnEdicionN4.importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
        oPresupuestoEnEdicionN4.Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)
        
        'Tambien debo modificarlo en la coleccion de apoyo a la estructura
        scod1 = frmPresupuestos2.DevolverCod(nodx.Parent.Parent)
        scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
        scod2 = frmPresupuestos2.DevolverCod(nodx.Parent)
        scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
        scod3 = frmPresupuestos2.DevolverCod(nodx)
        scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
        scod4 = sdbgPresupuestos.Columns(0).Text
        scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
            
        oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Den = sdbgPresupuestos.Columns(1).Value
        oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
        oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)

    Else
        If Not oPres2Seleccionado Is Nothing Then
            
            oPresupuestoEnEdicionN3.Den = sdbgPresupuestos.Columns(1).Value
            dImpAnterior = NullToDbl0(oPresupuestoEnEdicionN3.importe)
            oPresupuestoEnEdicionN3.importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
            oPresupuestoEnEdicionN3.Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)
            
            'Tambien debo modificarlo en la coleccion de apoyo a la estructura
            scod1 = frmPresupuestos2.DevolverCod(nodx.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = frmPresupuestos2.DevolverCod(nodx)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = sdbgPresupuestos.Columns(0).Text
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            
            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Den = sdbgPresupuestos.Columns(1).Value
            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)
            
        Else
            If Not oPres1Seleccionado Is Nothing Then
                
                oPresupuestoEnEdicionN2.Den = sdbgPresupuestos.Columns(1).Value
                dImpAnterior = NullToDbl0(oPresupuestoEnEdicionN2.importe)
                oPresupuestoEnEdicionN2.importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
                oPresupuestoEnEdicionN2.Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)
                'Tambien debo modificarlo en la coleccion de apoyo a la estructura
                scod1 = frmPresupuestos2.DevolverCod(nodx)
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                scod2 = sdbgPresupuestos.Columns(0).Text
                scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
                
                oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Den = sdbgPresupuestos.Columns(1).Value
                oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
                oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)

            Else
                
                oPresupuestoEnEdicionN1.Den = sdbgPresupuestos.Columns(1).Value
                dImpAnterior = NullToDbl0(oPresupuestoEnEdicionN1.importe)
                oPresupuestoEnEdicionN1.importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
                oPresupuestoEnEdicionN1.Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)
                
                'Tambien debo modificarlo en la coleccion de apoyo a la estructura
                scod1 = sdbgPresupuestos.Columns(0).Text
                scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                oPresupuestos.Item(scod1).Den = sdbgPresupuestos.Columns(1).Value
                oPresupuestos.Item(scod1).importe = StrToDblOrNull(sdbgPresupuestos.Columns(2).Value)
                oPresupuestos.Item(scod1).Objetivo = StrToDblOrNull(sdbgPresupuestos.Columns(3).Value)

            End If
        End If
    End If
    
    teserror = oIBAseDatosEnEdicion.FinalizarEdicionModificando
       
    If teserror.NumError <> TESnoerror Then
    
        v = sdbgPresupuestos.ActiveCell.Value
        TratarError teserror
        If Me.Visible Then sdbgPresupuestos.SetFocus
        bModError = True
        RowBuf.RowCount = 0
        sdbgPresupuestos.ActiveCell.Value = v
        
    Else
        
        ''' Registro de acciones
        ''' Actualizacion del acumulado
        If Not oPres3Seleccionado Is Nothing Then
            RegistrarAccion ACCPresConNivel4MOd, "CodPRES1:" & oPres3Seleccionado.CodPRES1 & "CodPRES2:" & oPres3Seleccionado.CodPRES2 & "CodPRES3:" & oPres3Seleccionado.Cod & "Cod:" & sdbgPresupuestos.Columns(0).Value
        Else
            If Not oPres2Seleccionado Is Nothing Then
                RegistrarAccion ACCPresConNivel3Mod, "CodPRES1:" & oPres2Seleccionado.CodPRES1 & "CodPRES2:" & oPres2Seleccionado.Cod & "Cod:" & sdbgPresupuestos.Columns(0).Value
            Else
                If Not oPres1Seleccionado Is Nothing Then
                    RegistrarAccion ACCPresConNivel2Mod, "CodPRES1:" & oPres1Seleccionado.Cod & "Cod:" & sdbgPresupuestos.Columns(0).Value
                Else
                     RegistrarAccion ACCPresConNivel1Mod, "Cod:" & sdbgPresupuestos.Columns(0).Value
                End If
            End If
        End If
    
        Accion = ACCPresConCon
        
        ' Actualizamos el acumulado
        dAcumulado = dAcumulado - dImpAnterior + NullToDbl0(StrToDblOrNull(sdbgPresupuestos.Columns(2).Value))
        sdbgPresupuestos.caption = " " & sIdiSuma & " " & DblToStr(dAcumulado)
        
        Set oIBAseDatosEnEdicion = Nothing
        Set oPresupuestoEnEdicionN1 = Nothing
        
    End If

End Sub

Private Sub SSTabPresupuestos_Click(PreviousTab As Integer)
    Arrange
    If SSTabPresupuestos.Tab = 1 Then
        GenerarEstructuraPresupuestos False, True
        txtObj.Text = ""
        txtPres.Text = ""
        txtPartida.Text = ""
        tvwEstrPres.Nodes.Item("Raiz ").Selected = True
    Else
        CargarEstructuraOrg False
        SeleccionarNodoActual
    End If
End Sub

Private Sub tvwestrorg_NodeClick(ByVal node As MSComctlLib.node)
    lblEstrorg1.caption = node.Text
    lblEstrorg2.caption = lblEstrorg1.caption
    
    If Not bRUO And SSTabPresupuestos.TabVisible(1) = False Then
        SSTabPresupuestos.TabVisible(1) = True
    End If
    
    Select Case iUOBase
        Case 0
            SSTabPresupuestos.TabVisible(1) = True
        Case 1
            Select Case Left(node.Tag, 4)
                Case "UON1", "UON2", "UON3"
                    SSTabPresupuestos.TabVisible(1) = True
                Case Else
                    SSTabPresupuestos.TabVisible(1) = False
            End Select
        Case 2
            Select Case Left(node.Tag, 4)
                Case "UON2", "UON3"
                    SSTabPresupuestos.TabVisible(1) = True
                Case Else
                    SSTabPresupuestos.TabVisible(1) = False
            End Select
        Case 3
            Select Case Left(node.Tag, 4)
                Case "UON3"
                    SSTabPresupuestos.TabVisible(1) = True
                Case Else
                    SSTabPresupuestos.TabVisible(1) = False
            End Select
    End Select
    
    Select Case Left(node.Tag, 4)
        Case "UON1"
            m_sUON1 = Right(node.Tag, Len(node.Tag) - 4)
            m_sUON2 = ""
            m_sUON3 = ""
        Case "UON2"
            m_sUON1 = Right(node.Parent.Tag, Len(node.Parent.Tag) - 4)
            m_sUON2 = Right(node.Tag, Len(node.Tag) - 4)
            m_sUON3 = ""
        Case "UON3"
            m_sUON1 = Right(node.Parent.Parent.Tag, Len(node.Parent.Parent.Tag) - 4)
            m_sUON2 = Right(node.Parent.Tag, Len(node.Parent.Tag) - 4)
            m_sUON3 = Right(node.Tag, Len(node.Tag) - 4)
        Case "UON0"
            m_sUON1 = ""
            m_sUON2 = ""
            m_sUON3 = ""
    End Select
End Sub


Private Sub tvwEstrPres_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim nodx As MSComctlLib.node
    
    
    If Button = 2 Then
        Set nodx = tvwEstrPres.selectedItem
        
        If Not nodx Is Nothing Then
        
            Select Case Left(nodx.Tag, 5)
                Case "Raiz "
                    PopupMenu MDI.mnuPopUpEstrPresConN0Cab
                
                Case "PRES1"
                    If gParametrosGenerales.giNEPP <= 1 Then
                        MDI.mnuPopUpEstrPresConN1(1).Visible = False
                    End If
                    PopupMenu MDI.mnuPopUpEstrPresConN1Cab
                                    
                Case "PRES2"
                    If gParametrosGenerales.giNEPP <= 2 Then
                        MDI.mnuPopUpEstrPresConN2(1).Visible = False
                    End If
                    PopupMenu MDI.mnuPopUpEstrPresConN2Cab
                            
                Case "PRES3"
                    If gParametrosGenerales.giNEPP <= 3 Then
                        MDI.mnuPopUpEstrPresConN3(1).Visible = False
                    End If
                    PopupMenu MDI.mnuPopUpEstrPresConN3Cab
                
                Case "PRES4"
                    PopupMenu MDI.mnuPopUpEstrPresConN4Cab
            End Select
        
        End If
    End If
End Sub

Public Sub tvwEstrPres_NodeClick(ByVal node As MSComctlLib.node)
    Dim nodoSiguiente As MSComctlLib.node
    Set oPres1Seleccionado = Nothing
    Set oPres2Seleccionado = Nothing
    Set oPres3Seleccionado = Nothing
    OcultarImportes

    ConfigurarInterfazSeguridad node
    
    If (node.Image = "PRESBAJALOGICA") Then 'Si es baja l�gica
        
        cmdA�adir.Enabled = False
        cmdModif.Enabled = False
        cmdEli.Enabled = True
        cmdBajaLog.Enabled = True
        cmdBajaLog.caption = m_sCaptionDeshacerBaja
        MDI.mnuPopUpEstrPresConN1.Item(4).Enabled = True
        MDI.mnuPopUpEstrPresConN1.Item(6).Enabled = True
        MDI.mnuPopUpEstrPresConN2.Item(4).Enabled = True
        MDI.mnuPopUpEstrPresConN2.Item(6).Enabled = True
        MDI.mnuPopUpEstrPresConN3.Item(4).Enabled = True
        MDI.mnuPopUpEstrPresConN3.Item(6).Enabled = True
        MDI.mnuPopUpEstrPresConN4.Item(2).Enabled = True
        MDI.mnuPopUpEstrPresConN4.Item(4).Enabled = True
        
        Select Case Left(node.Tag, 5)
            Case "Raiz "
                cmdEli.Enabled = False
            Case "PRES1"
                If gParametrosGenerales.giNEPP = 1 Then
                    MDI.mnuPopUpEstrPresConN1.Item(2).Enabled = True
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN1.Item(4).Enabled = False
                        MDI.mnuPopUpEstrPresConN1.Item(6).Enabled = False
                        cmdEli.Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
                If gParametrosGenerales.giNEPP > 1 Then
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN1.Item(4).Enabled = False
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN1.Item(6).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
            
            Case "PRES2"
                If gParametrosGenerales.giNEPP = 2 Then
                    MDI.mnuPopUpEstrPresConN2.Item(2).Enabled = True
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN2.Item(4).Enabled = False
                        MDI.mnuPopUpEstrPresConN2.Item(6).Enabled = False
                        cmdEli.Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
                If gParametrosGenerales.giNEPP > 2 Then
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN2.Item(4).Enabled = False
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN2.Item(6).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
        
            Case "PRES3"
                If gParametrosGenerales.giNEPP = 3 Then
                    MDI.mnuPopUpEstrPresConN3.Item(2).Enabled = True
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN3.Item(4).Enabled = False
                        MDI.mnuPopUpEstrPresConN3.Item(6).Enabled = False
                        cmdEli.Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
                If gParametrosGenerales.giNEPP > 3 Then
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN3.Item(4).Enabled = False
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN3.Item(6).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
                
            Case "PRES4"
                If gParametrosGenerales.giNEPP = 4 Then
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN4.Item(2).Enabled = False
                        MDI.mnuPopUpEstrPresConN4.Item(4).Enabled = False
                        cmdEli.Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
                If gParametrosGenerales.giNEPP > 4 Then
                    If (node.Children > 0) Or (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN4.Item(2).Enabled = False
                        cmdEli.Enabled = False
                    End If
                    If (node.Parent.Image = "PRESBAJALOGICA") Then
                        MDI.mnuPopUpEstrPresConN4.Item(4).Enabled = False
                        cmdBajaLog.Enabled = False
                    End If
                End If
        End Select
        
    Else                                    'si no es baja l�gica
    
        cmdA�adir.Enabled = True
        cmdModif.Enabled = True
        cmdEli.Enabled = True
        cmdBajaLog.Enabled = True
        cmdBajaLog.caption = m_sCaptionBajaLogica
        MDI.mnuPopUpEstrPresConN1.Item(4).Enabled = True
        MDI.mnuPopUpEstrPresConN1.Item(5).Enabled = True
        MDI.mnuPopUpEstrPresConN2.Item(4).Enabled = True
        MDI.mnuPopUpEstrPresConN2.Item(5).Enabled = True
        MDI.mnuPopUpEstrPresConN3.Item(4).Enabled = True
        MDI.mnuPopUpEstrPresConN3.Item(5).Enabled = True
        MDI.mnuPopUpEstrPresConN4.Item(2).Enabled = True
        MDI.mnuPopUpEstrPresConN4.Item(3).Enabled = True

        Select Case Left(node.Tag, 5)
            Case "Raiz "
                cmdEli.Enabled = False
                cmdBajaLog.Enabled = False
            Case "PRES1"
                If gParametrosGenerales.giNEPP = 1 Then
                    cmdA�adir.Enabled = False
                End If
                If gParametrosGenerales.giNEPP > 1 Then
                     If (node.Children > 0) Then
                        MDI.mnuPopUpEstrPresConN1.Item(4).Enabled = False
                        cmdEli.Enabled = False
                        Set nodoSiguiente = node.Child
                        Do While Not nodoSiguiente Is Nothing
                            If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                                MDI.mnuPopUpEstrPresConN1.Item(5).Enabled = False
                                cmdBajaLog.Enabled = False
                                Exit Do
                            End If
                            Set nodoSiguiente = nodoSiguiente.Next
                        Loop
                    End If
               End If
            
            Case "PRES2"
                If gParametrosGenerales.giNEPP = 2 Then
                    cmdA�adir.Enabled = False
                End If
                If gParametrosGenerales.giNEPP > 2 Then
                    If (node.Children > 0) Then
                        MDI.mnuPopUpEstrPresConN2.Item(4).Enabled = False
                        cmdEli.Enabled = False
                        Set nodoSiguiente = node.Child
                        Do While Not nodoSiguiente Is Nothing
                            If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                                MDI.mnuPopUpEstrPresConN2.Item(5).Enabled = False
                                cmdBajaLog.Enabled = False
                                Exit Do
                            End If
                            Set nodoSiguiente = nodoSiguiente.Next
                        Loop
                    End If
                End If

            
            Case "PRES3"
                If gParametrosGenerales.giNEPP = 3 Then
                    cmdA�adir.Enabled = False
                End If
                If gParametrosGenerales.giNEPP > 3 Then
                    If (node.Children > 0) Then
                        MDI.mnuPopUpEstrPresConN3.Item(4).Enabled = False
                        cmdEli.Enabled = False
                        Set nodoSiguiente = node.Child
                        Do While Not nodoSiguiente Is Nothing
                            If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                                MDI.mnuPopUpEstrPresConN3.Item(5).Enabled = False
                                cmdBajaLog.Enabled = False
                                Exit Do
                            End If
                            Set nodoSiguiente = nodoSiguiente.Next
                        Loop
                    End If
                End If
                
            Case "PRES4"
                If gParametrosGenerales.giNEPP = 4 Then
                    cmdA�adir.Enabled = False
                End If
                If gParametrosGenerales.giNEPP > 4 Then
                    If (node.Children > 0) Then
                        MDI.mnuPopUpEstrPresConN4.Item(2).Enabled = False
                        cmdEli.Enabled = False
                        Set nodoSiguiente = node.Child
                        Do While Not nodoSiguiente Is Nothing
                            If nodoSiguiente.Image <> "PRESBAJALOGICA" Then
                                MDI.mnuPopUpEstrPresConN4.Item(3).Enabled = False
                                cmdBajaLog.Enabled = False
                                Exit Do
                            End If
                            Set nodoSiguiente = nodoSiguiente.Next
                        Loop
                    End If
                End If
        End Select
    End If
    
    MostrarDatosBarraInf
End Sub
Private Sub CargarAnyos()
    
Dim iAnyoActual As Integer
Dim iInd As Integer

    iAnyoActual = Year(Date)
        
    For iInd = iAnyoActual - 10 To iAnyoActual + 10
        
        sdbcAnyo.AddItem iInd
        
    Next

    sdbcAnyo.Text = iAnyoActual
    sdbcAnyo.ListAutoPosition = True
    sdbcAnyo.Scroll 1, 7
    
End Sub

Public Sub Ordenar(ByVal bOrdPorDen As Boolean)
    tvwEstrPres.Nodes.clear
    GenerarEstructuraPresupuestos bOrdPorDen
    bOrdenDen = bOrdPorDen
End Sub

Public Sub MostrarDatosBarraInf()
    Dim nodx As MSComctlLib.node
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim vImporte As Variant
    Dim vObjetivo As Variant
    
    

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
        
        Select Case Left(nodx.Tag, 5)
            Case "Raiz "
                txtPartida = ""
                txtPres = ""
                txtObj = ""
                dPresGeneralSel = 0
            Case "PRES1"
                scod1 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx) & " (" & oPresupuestos.Item(scod1).Den & ")"
                txtPres = DblToStr(oPresupuestos.Item(scod1).importe)
                dPresGeneralSel = NullToDbl0(oPresupuestos.Item(scod1).importe)
                txtObj = Format(DblToStr(oPresupuestos.Item(scod1).Objetivo), "0.0#\%")
            Case "PRES2"
                scod1 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent)))
                scod2 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx.Parent) & " (" & oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).importe
                vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).Objetivo
                txtPres = DblToStr(vImporte)
                dPresGeneralSel = NullToDbl0(vImporte)
                txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
            Case "PRES3"
                scod1 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent)))
                scod3 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx.Parent.Parent) & " (" & oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).importe
                vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).Objetivo
                txtPres = DblToStr(vImporte)
                dPresGeneralSel = NullToDbl0(vImporte)
                txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
            Case "PRES4"
                scod1 = DevolverCod(nodx.Parent.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(DevolverCod(nodx.Parent.Parent.Parent)))
                scod2 = DevolverCod(nodx.Parent.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(DevolverCod(nodx.Parent.Parent)))
                scod3 = DevolverCod(nodx.Parent) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(DevolverCod(nodx.Parent)))
                scod4 = DevolverCod(nodx) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(DevolverCod(nodx)))
                txtPartida = DevolverCod(nodx.Parent.Parent.Parent) & " (" & oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Den & ")"
                vImporte = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).importe
                vObjetivo = oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Item(scod1 & scod2 & scod3 & scod4).Objetivo
                txtPres = DblToStr(vImporte)
                dPresGeneralSel = NullToDbl0(vImporte)
                txtObj = Format(DblToStr(vObjetivo), "0.0#\%")
        End Select
    
    End If

End Sub

Private Function EliminarPARDeEstructura()
Dim nod As node
Dim nodSiguiente As MSComctlLib.node
Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

Set nod = tvwEstrPres.selectedItem
Select Case Left(nod.Tag, 5)
    
    Case "PRES1"
            
            scod1 = DevolverCod(nod)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
                    
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oPresupuestos.Remove (scod1)
    
    Case "PRES2"
            
            scod1 = DevolverCod(nod.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = DevolverCod(nod)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oPresupuestos.Item(scod1).PresContablesNivel2.Remove (scod1 & scod2)
            
    Case "PRES3"
            
            scod1 = DevolverCod(nod.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = DevolverCod(nod.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = DevolverCod(nod)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Remove (scod1 & scod2 & scod3)
            
    Case "PRES4"
            
            scod1 = DevolverCod(nod.Parent.Parent.Parent)
            scod1 = scod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(scod1))
            scod2 = DevolverCod(nod.Parent.Parent)
            scod2 = scod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(scod2))
            scod3 = DevolverCod(nod.Parent)
            scod3 = scod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(scod3))
            scod4 = DevolverCod(nod)
            scod4 = scod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(scod4))
            
            'Tengo que eliminarlo de la coleccion que mantiene la inf de la estructura
            oPresupuestos.Item(scod1).PresContablesNivel2.Item(scod1 & scod2).PresContablesNivel3.Item(scod1 & scod2 & scod3).PresContablesNivel4.Remove (scod1 & scod2 & scod3 & scod4)
            
End Select

    
    ' Me posiciono en el siguiente
    If Not nod.Previous Is Nothing Then
        Set nodSiguiente = nod.Previous
    Else
        If Not nod.Next Is Nothing Then
            Set nodSiguiente = nod.Next
            Else
                Set nodSiguiente = nod.Parent
        End If
    End If

    
    'Lo elimino del arbol
    tvwEstrPres.Nodes.Remove (frmPresupuestos2.tvwEstrPres.selectedItem.Index)
    tvwEstrPres_NodeClick nodSiguiente

    nodSiguiente.Selected = True
    If Me.Visible Then tvwEstrPres.SetFocus

End Function

Public Sub mnuPopUpEstrPresConDet()
    Dim nodx As MSComctlLib.node
    Dim teserror As TipoErrorSummit
    

    Set nodx = tvwEstrPres.selectedItem
    
    If Not nodx Is Nothing Then
            
        Select Case Left(nodx.Tag, 5)
            Case "PRES1"
                 Accion = ACCPresConnivel1Det
                 
                 Set oPres1Seleccionado = Nothing
                 Set oPres1Seleccionado = oFSGSRaiz.generar_CPresConNivel1
                 oPres1Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
                 oPres1Seleccionado.Cod = DevolverCod(nodx)
                 oPres1Seleccionado.UON1 = m_sUON1
                 oPres1Seleccionado.UON2 = m_sUON2
                 oPres1Seleccionado.UON3 = m_sUON3
                
                 Set oIBaseDatos = oPres1Seleccionado
                 
                 If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                     frmPRESConDetalle.caption = sIdiDetalle & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & DevolverCod(nodx)
                     frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                     frmPRESConDetalle.txtDen = oPres1Seleccionado.Den
                     If IsNull(oPres1Seleccionado.importe) Then
                         frmPRESConDetalle.txtImp = ""
                     Else
                         frmPRESConDetalle.txtImp = Format(oPres1Seleccionado.importe, "Standard")
                     End If
                     
                     If IsNull(oPres1Seleccionado.Objetivo) Then
                         frmPRESConDetalle.txtObj = ""
                     Else
                         frmPRESConDetalle.txtObj = Format(oPres1Seleccionado.Objetivo, "0.0#")
                     End If
                     
                     frmPRESConDetalle.Show 1
                 Else
                     TratarError teserror
                     Set oIBaseDatos = Nothing
                     Set oPres1Seleccionado = Nothing
                     Exit Sub
                 End If
            
            Case "PRES2"
                 Accion = ACCPresConnivel2Det
                 
                 Set oPres2Seleccionado = Nothing
                 Set oPres2Seleccionado = oFSGSRaiz.generar_CPresConNivel2
                 oPres2Seleccionado.Cod = DevolverCod(nodx)
                 oPres2Seleccionado.CodPRES1 = DevolverCod(nodx.Parent)
                 oPres2Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
                 oPres2Seleccionado.UON1 = m_sUON1
                 oPres2Seleccionado.UON2 = m_sUON2
                 oPres2Seleccionado.UON3 = m_sUON3
                 
                
                 Set oIBaseDatos = oPres2Seleccionado
                 
                 If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                 
                     frmPRESConDetalle.caption = sIdiDetalle & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                     frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                     frmPRESConDetalle.txtDen = oPres2Seleccionado.Den
                     If IsNull(oPres2Seleccionado.importe) Then
                         frmPRESConDetalle.txtImp = ""
                     Else
                         frmPRESConDetalle.txtImp = Format(oPres2Seleccionado.importe, "Standard")
                     End If
                     
                     If IsNull(oPres2Seleccionado.Objetivo) Then
                         frmPRESConDetalle.txtObj = ""
                     Else
                         frmPRESConDetalle.txtObj = Format(oPres2Seleccionado.Objetivo, "0.0#")
                     End If
                     frmPRESConDetalle.Show 1
                 Else
                     TratarError teserror
                     Set oIBaseDatos = Nothing
                     Set oPres2Seleccionado = Nothing
                     Exit Sub
                 End If
                
            Case "PRES3"
                 Accion = ACCPresConnivel3Det
                 
                 Set oPres3Seleccionado = Nothing
                 Set oPres3Seleccionado = oFSGSRaiz.generar_CPresConNivel3
                 oPres3Seleccionado.Cod = DevolverCod(nodx)
                 oPres3Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent)
                 oPres3Seleccionado.CodPRES2 = DevolverCod(nodx.Parent)
                 oPres3Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
                 oPres3Seleccionado.UON1 = m_sUON1
                 oPres3Seleccionado.UON2 = m_sUON2
                 oPres3Seleccionado.UON3 = m_sUON3
                
                
                 Set oIBaseDatos = oPres3Seleccionado
                 
                 If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                 
                     frmPRESConDetalle.caption = sIdiDetalle & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                     frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                     frmPRESConDetalle.txtDen = oPres3Seleccionado.Den
                     
                     If IsNull(oPres3Seleccionado.importe) Then
                         frmPRESConDetalle.txtImp = ""
                     Else
                         frmPRESConDetalle.txtImp = Format(oPres3Seleccionado.importe, "Standard")
                     End If
                     
                     If IsNull(oPres3Seleccionado.Objetivo) Then
                         frmPRESConDetalle.txtObj = ""
                     Else
                         frmPRESConDetalle.txtObj = Format(oPres3Seleccionado.Objetivo, "0.0#")
                     End If
                     
                     frmPRESConDetalle.Show 1
                 
                 Else
                     TratarError teserror
                     Set oIBaseDatos = Nothing
                     Set oPres3Seleccionado = Nothing
                     Exit Sub
                 End If
                
            Case "PRES4"
                 Accion = ACCPresConnivel4Det
                 
                 Set oPres4Seleccionado = Nothing
                 Set oPres4Seleccionado = oFSGSRaiz.generar_CPresConNivel4
                 oPres4Seleccionado.Anyo = CInt(val(sdbcAnyo.Text))
                 oPres4Seleccionado.Cod = DevolverCod(nodx)
                 oPres4Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent.Parent)
                 oPres4Seleccionado.CodPRES2 = DevolverCod(nodx.Parent.Parent)
                 oPres4Seleccionado.CodPRES3 = DevolverCod(nodx.Parent)
                 oPres4Seleccionado.UON1 = m_sUON1
                 oPres4Seleccionado.UON2 = m_sUON2
                 oPres4Seleccionado.UON3 = m_sUON3
                
                 Set oIBaseDatos = oPres4Seleccionado
                 
                 If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                     frmPRESConDetalle.caption = sIdiDetalle & ": " & basParametros.gParametrosGenerales.gsSingPres2 & " - " & DevolverCod(nodx.Parent.Parent.Parent) & " - " & DevolverCod(nodx.Parent.Parent) & " - " & DevolverCod(nodx.Parent) & " - " & DevolverCod(nodx)
                     frmPRESConDetalle.txtCOD = DevolverCod(nodx)
                     frmPRESConDetalle.txtDen = oPres4Seleccionado.Den
                     If IsNull(oPres4Seleccionado.importe) Then
                         frmPRESConDetalle.txtImp = ""
                     Else
                         frmPRESConDetalle.txtImp = Format(oPres4Seleccionado.importe, "Standard")
                     End If
                     
                     If IsNull(oPres4Seleccionado.Objetivo) Then
                         frmPRESConDetalle.txtObj = ""
                     Else
                         frmPRESConDetalle.txtObj = Format(oPres4Seleccionado.Objetivo, "0.0#")
                     End If
                     
                     frmPRESConDetalle.Show 1
                 Else
                     TratarError teserror
                     Set oIBaseDatos = Nothing
                     Set oPres4Seleccionado = Nothing
                     Exit Sub
                 End If
        End Select
    End If
End Sub

Public Sub mnuPopUpEstrPresConCopiar(ByVal bCopiaEnAnyo As Boolean)
    Dim nodx As MSComctlLib.node
    Dim teserror As TipoErrorSummit
        
        
    
    Set nodx = tvwEstrPres.selectedItem
        
    If Not nodx Is Nothing Then
        frmPRESAnuCopiarUON.m_bCheckRama = True
        Select Case Left(nodx.Tag, 5)
            Case "PRES1"
                Set oPres1Seleccionado = Nothing
                Set oPres1Seleccionado = oFSGSRaiz.generar_CPresConNivel1
                oPres1Seleccionado.Anyo = sdbcAnyo
                oPres1Seleccionado.Cod = DevolverCod(nodx)
                If m_sUON1 <> "" Then oPres1Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres1Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres1Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres1Seleccionado
                
                If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                    If bCopiaEnAnyo Then
                        frmPRESConCopiar.Show 1
                    Else
                        frmPRESAnuCopiarUON.bRUO = bRUO
                        frmPRESAnuCopiarUON.sOrigen = Me.Name
                        frmPRESAnuCopiarUON.sInfoPRES = nodx.Text
                        frmPRESAnuCopiarUON.Show 1
                    End If
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres1Seleccionado = Nothing
                    Exit Sub
                End If
                
            Case "PRES2"
                Set oPres2Seleccionado = Nothing
                Set oPres2Seleccionado = oFSGSRaiz.generar_CPresConNivel2
                oPres2Seleccionado.Cod = DevolverCod(nodx)
                oPres2Seleccionado.CodPRES1 = DevolverCod(nodx.Parent)
                oPres2Seleccionado.Anyo = sdbcAnyo
                If m_sUON1 <> "" Then oPres2Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres2Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres2Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres2Seleccionado
                
                If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                    If bCopiaEnAnyo Then
                        frmPRESConCopiar.Show 1
                    Else
                        frmPRESAnuCopiarUON.bRUO = bRUO
                        frmPRESAnuCopiarUON.sOrigen = Me.Name
                        frmPRESAnuCopiarUON.sInfoPRES = nodx.Text
                        frmPRESAnuCopiarUON.Show 1
                    End If
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres2Seleccionado = Nothing
                    Exit Sub
                End If
                
            Case "PRES3"
                Accion = ACCPresConnivel3Det
                Set oPres3Seleccionado = Nothing
                Set oPres3Seleccionado = oFSGSRaiz.generar_CPresConNivel3
                oPres3Seleccionado.Cod = DevolverCod(nodx)
                oPres3Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent)
                oPres3Seleccionado.CodPRES2 = DevolverCod(nodx.Parent)
                oPres3Seleccionado.Anyo = sdbcAnyo
                If m_sUON1 <> "" Then oPres3Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres3Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres3Seleccionado.UON3 = m_sUON3
                
                Set oIBaseDatos = oPres3Seleccionado
                
                If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                    If bCopiaEnAnyo Then
                        frmPRESConCopiar.Show 1
                    Else
                        frmPRESAnuCopiarUON.bRUO = bRUO
                        frmPRESAnuCopiarUON.sOrigen = Me.Name
                        frmPRESAnuCopiarUON.sInfoPRES = nodx.Text
                        frmPRESAnuCopiarUON.Show 1
                    End If
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres3Seleccionado = Nothing
                    Exit Sub
                End If
            Case "PRES4"
                Set oPres4Seleccionado = Nothing
                Set oPres4Seleccionado = oFSGSRaiz.generar_CPresConNivel4
                oPres4Seleccionado.Anyo = val(sdbcAnyo)
                oPres4Seleccionado.Cod = DevolverCod(nodx)
                oPres4Seleccionado.CodPRES1 = DevolverCod(nodx.Parent.Parent.Parent)
                oPres4Seleccionado.CodPRES2 = DevolverCod(nodx.Parent.Parent)
                oPres4Seleccionado.CodPRES3 = DevolverCod(nodx.Parent)
                If m_sUON1 <> "" Then oPres4Seleccionado.UON1 = m_sUON1
                If m_sUON2 <> "" Then oPres4Seleccionado.UON2 = m_sUON2
                If m_sUON3 <> "" Then oPres4Seleccionado.UON3 = m_sUON3
                              
                Set oIBaseDatos = oPres4Seleccionado
                
                If oIBaseDatos.ComprobarExistenciaEnBaseDatos Then
                    frmPRESAnuCopiarUON.m_bCheckRama = False
                    If bCopiaEnAnyo Then
                        frmPRESConCopiar.optSeleccionado.Value = True
                        frmPRESConCopiar.optTodos.Enabled = False
                        frmPRESConCopiar.Show 1
                    Else
                        frmPRESAnuCopiarUON.bRUO = bRUO
                        frmPRESAnuCopiarUON.sOrigen = Me.Name
                        frmPRESAnuCopiarUON.sInfoPRES = nodx.Text
                        frmPRESAnuCopiarUON.Show 1
                    End If
                Else
                    TratarError teserror
                    Set oIBaseDatos = Nothing
                    Set oPres4Seleccionado = Nothing
                    Exit Sub
                End If
        End Select
    End If
End Sub

   
Public Sub mnuPopUpEstrPresConCopiarEnAnyo()
    
    frmPRESConCopiar.Height = frmPRESConCopiar.Height - 1100
    frmPRESConCopiar.cmdAceptar.Top = frmPRESConCopiar.cmdAceptar.Top - 1000
    frmPRESConCopiar.cmdCancelar.Top = frmPRESConCopiar.cmdCancelar.Top - 1000
    frmPRESConCopiar.chkImp.Top = frmPRESConCopiar.chkImp.Top - 900
    frmPRESConCopiar.optSeleccionado.Visible = False
    frmPRESConCopiar.optTodos.Visible = False
    frmPRESConCopiar.Show 1
End Sub

Private Sub CargarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String

    ' Unidades organizativas
    Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
    Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
    Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

    Dim oUON1 As CUnidadOrgNivel1
    Dim oUON2 As CUnidadOrgNivel2
    Dim oUON3 As CUnidadOrgNivel3

    ' Otras
    Dim nodx As node
    Dim bPresEnRaiz As Boolean
    
    
    tvwestrorg.Nodes.clear
    
    sOrdenListadoDen = bOrdenadoPorDen

    Set oUnidadesOrgN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
     
         
    If (oUsuarioSummit.Tipo = TipoDeUsuario.Persona Or basOptimizacion.gTipoDeUsuario = comprador) And bRUO Then
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                bPresEnRaiz = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, 2)
            Case 2
                bPresEnRaiz = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, 2)
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, 2
            Case 3
                bPresEnRaiz = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres(basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, 2)
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, 2
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPres basOptimizacion.gUON1Usuario, basOptimizacion.gUON2Usuario, basOptimizacion.gUON3Usuario, bRUO, , , False, bOrdenadoPorDen, False, 2
        End Select
               
    Else
        
        'Cargamos toda la estrucutura organizativa
        Select Case gParametrosGenerales.giNEO
            Case 1
                bPresEnRaiz = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres(, , , , , , , bOrdenadoPorDen, False, 2)
            Case 2
                bPresEnRaiz = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres(, , , , , , , bOrdenadoPorDen, False, 2)
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres , , , , , , , bOrdenadoPorDen, False, 2
            Case 3
                bPresEnRaiz = oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1ParaPres(, , , , , , , bOrdenadoPorDen, False, 2)
                oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2ParaPres , , , , , , , bOrdenadoPorDen, False, 2
                oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3ParaPres , , , , , , , bOrdenadoPorDen, False, 2
        End Select
                
    End If
    
        
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gParametrosGenerales.gsDEN_UON0, "UON0")
    If bPresEnRaiz Then
        nodx.Image = "UON0D"
    End If
    nodx.Tag = "UON0"
    nodx.Expanded = True
    
    For Each oUON1 In oUnidadesOrgN1
        scod1 = oUON1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, "UON1")
        If oUON1.ConPresup Then
            nodx.Image = "UON1A"
        End If
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, "UON2")
        If oUON2.ConPresup Then
            nodx.Image = "UON2A"
        End If
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, "UON3")
        If oUON3.ConPresup Then
            nodx.Image = "UON3A"
        End If
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
    Next
    
    Set nodx = Nothing
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
        
End Sub

Public Sub mnuPopUpEstrPresConCopiarEnUON()
    'Procedimiento llamado s�lo cuando quiero una copia total, desde la
    'ra�z del �rbol de presupuestos.
    Dim nodx As MSComctlLib.node
    
    Set nodx = tvwEstrPres.selectedItem
    frmPRESAnuCopiarUON.bRUO = bRUO
    frmPRESAnuCopiarUON.sOrigen = Me.Name
    frmPRESAnuCopiarUON.sInfoPRES = nodx.Text
    frmPRESAnuCopiarUON.m_bCheckRama = False
    frmPRESAnuCopiarUON.Show 1
End Sub

Private Function DevolverDescripcionUOSel() As String
'************************************************************************************
'*** Descripci�n: Devuelve un string con la descripci�n de la unidad organizativa ***
'***              seleccionada, los c�digos de la UO seleccionada est�n en las    ***
'***              variables de m�dulo: m_sUON1, m_sUON2 y m_sUON3.                ***
'***                                                                              ***
'*** Par�metros:  ------                                                          ***
'***                                                                              ***
'***                                                                              ***
'*** Valor que devuelve: Una cadena de caracteres con la descripci�n de la        ***
'***                     unidad organizativa seleccionada.                        ***
'************************************************************************************
    Dim sDescrip As String
    Dim oUOrganizativasN1 As CUnidadesOrgNivel1
    Dim oUOrganizativasN2 As CUnidadesOrgNivel2
    Dim oUOrganizativasN3 As CUnidadesOrgNivel3
    
    sDescrip = ""
    If m_sUON3 <> "" Then
        Set oUOrganizativasN3 = oFSGSRaiz.generar_CUnidadesOrgNivel3
        sDescrip = oUOrganizativasN3.DevolverDenominacion(m_sUON1, m_sUON2, m_sUON3)
        Set oUOrganizativasN3 = Nothing
    ElseIf m_sUON2 <> "" Then
        Set oUOrganizativasN2 = oFSGSRaiz.generar_CUnidadesOrgNivel2
        sDescrip = oUOrganizativasN2.DevolverDenominacion(m_sUON1, m_sUON2)
        Set oUOrganizativasN2 = Nothing
    ElseIf m_sUON1 <> "" Then
        Set oUOrganizativasN1 = oFSGSRaiz.generar_CUnidadesOrgNivel1
        sDescrip = oUOrganizativasN1.DevolverDenominacion(m_sUON1)
        Set oUOrganizativasN1 = Nothing
    Else
        sDescrip = gParametrosGenerales.gsDEN_UON0
    End If
    DevolverDescripcionUOSel = sDescrip
End Function

Private Sub SeleccionarNodoActual()
'****************************************************************************************
'*** Descripci�n: En el Tree View de la estructura organizativa (estrorg) selecciona  ***
'***              el nodo actual de trabajo, que viene dado por las variables de      ***
'***              m�dulo m_sUON1, m_sUON2, m_sUON3. Este procedimiento es invocado    ***
'***              cu�ndo cambiamos de la pesta�a de presupuestos a la pesta�a de      ***
'***              estructura de la organizaci�n, tras generar la estructura           ***
'***              organizativa.                                                       ***
'***                                                                                  ***
'*** Par�metros:  -------------                                                       ***
'***                                                                                  ***
'*** Valor que devuelve: ------ (en la pantalla, en la pesta�a de estructura          ***
'***                             organizativa aparecer� seleccionada la U.O. actual)  ***
'****************************************************************************************
    Dim lLongitudCodUON1 As Long
    Dim lLongitudCodUON2 As Long
    Dim lLongitudCodUON3 As Long
    
    lLongitudCodUON1 = basParametros.gLongitudesDeCodigos.giLongCodUON1
    lLongitudCodUON2 = basParametros.gLongitudesDeCodigos.giLongCodUON2
    lLongitudCodUON3 = basParametros.gLongitudesDeCodigos.giLongCodUON3
    If m_sUON3 <> "" Then
        tvwestrorg.Nodes.Item("UON3" & m_sUON1 & CompletarKey(Len(m_sUON1), lLongitudCodUON1) & m_sUON2 & CompletarKey(Len(m_sUON2), lLongitudCodUON2) & m_sUON3 & CompletarKey(Len(m_sUON3), lLongitudCodUON3)).Selected = True
    ElseIf m_sUON2 <> "" Then
        tvwestrorg.Nodes.Item("UON2" & m_sUON1 & CompletarKey(Len(m_sUON1), lLongitudCodUON1) & m_sUON2 & CompletarKey(Len(m_sUON2), lLongitudCodUON2)).Selected = True
    ElseIf m_sUON1 <> "" Then
        tvwestrorg.Nodes.Item("UON1" & m_sUON1 & CompletarKey(Len(m_sUON1), lLongitudCodUON1)).Selected = True
    Else 'seleccionar la raiz
        tvwestrorg.Nodes.Item("UON0").Selected = True
    End If
End Sub

Private Function CompletarKey(lDato1 As Long, lDato2 As Long) As String
'****************************************************************************************
'*** Descripci�n: Para acceder a un nodo de una Tree View puede hacerse por el        ***
'***              �ndice (propiedad Index) o por la clave (propiedad Key); en         ***
'***              este segundo caso la clave un nodo de la estructura                 ***
'***              organizativa ser� UONX (siendo X el nivel de UO al que              ***
'***              pertenece el nodo) + CodUON1 [+ UON2 + CodUON2 [+ UON3 + CodUON3]]  ***
'***              En realidad cada c�digo de UO si no llega a la longitud de c�digo   ***
'***              establecida en basParametros.gLongitudesDeCodigos.giLongCodUONX,    ***
'***              se rellena con caracteres blancos [Chr(32)] hasta completar dicha   ***
'***              longitud. Esta funci�n devuelve un String con el n�mero de          ***
'***              car�cteres blancos necesarios para completar la longitud de c�digo. ***
'***                                                                                  ***
'*** Par�metros:  lDato1, lDato2 ::> ambos de tipo Long, siendo lDato1 el n�mero de   ***
'***                                 car�cteres del c�digo actual y lDato2 la         ***
'***                                 longitud de c�digo contenida en                  ***
'***                                 basParametros.gLongitudesDeCodigos.giLongCodUONX ***
'***                                                                                  ***
'*** Valor que devuelve: Un string que contiene el n�mero de caracteres blancos:      ***
'***                     0,1,2,... necesarios para completar la propiedad Key de un   ***
'***                     nodo.                                                        ***
'****************************************************************************************
    Dim lCont As Long
    Dim lDiferencia As Long
    Dim sSalida As String
    
    lDiferencia = lDato2 - lDato1
    sSalida = ""
    For lCont = 1 To lDiferencia
        sSalida = sSalida & Chr(32)
    Next lCont
    CompletarKey = sSalida
End Function

Private Function ExisteAnyoEnCombo(ByVal sAnyo As String) As Boolean
'***********************************************************************************
'*** Descripci�n: Comprueba la existencia en la combo de a�os (sdbcAnyo) del a�o ***
'***              que le es pasado en el argumento de tipo string.               ***
'***                                                                             ***
'*** Par�metros : sAnyo ::>> Es de tipo string y especifica el a�o cuya          ***
'***                         existencia en la combo se est� comprobando.         ***
'***                                                                             ***
'*** Valor que devuelve: TRUE: Si el a�o existe en la combo.                     ***
'***                     FALSE: Si el a�o no existe en la combo.                 ***
'***********************************************************************************
    Dim i As Integer
    
    sdbcAnyo.MoveFirst
    For i = 1 To sdbcAnyo.Rows
        If sdbcAnyo.Columns.Item(0).Value = sAnyo Then
            ExisteAnyoEnCombo = True
            Exit Function
        End If
        sdbcAnyo.MoveNext
    Next i
    ExisteAnyoEnCombo = False
End Function

Private Function PonerPrimeraLetraEnMayuscula(sTexto As String) As String
'********************************************************************************************
'*** Descripci�n: Dado un texto (probablemente todo en min�sculas) la funci�n pone la     ***
'***              primera letra del texto en may�scula, es decir, devuelve el texto       ***
'***              original pero con la primera letra en may�scula.                        ***
'***                                                                                      ***
'*** Par�metros : sTexto ::> De tipo string, contendr� el texto original.                 ***
'***                                                                                      ***
'*** Valor que devuelve: Un texto, que ser� el texto original, el pasado como argumento   ***
'***                     de la funci�n, pero cuya primera letra estar� en may�scula.      ***
'********************************************************************************************
    Dim sPrimeraLetra As String
    Dim sResto As String
    
    If sTexto = "" Then Exit Function
    sPrimeraLetra = Left$(sTexto, 1)
    sResto = Right$(sTexto, Len(sTexto) - 1)
    PonerPrimeraLetraEnMayuscula = UCase(sPrimeraLetra) & sResto
End Function

Private Sub GenerarArbolPresupuestos()
    Dim scod1 As String
    Dim scod2 As String
    Dim scod3 As String
    Dim scod4 As String
    Dim opres1 As CPresConNivel1
    Dim oPRES2 As CPresconNivel2
    Dim oPRES3 As CPresConNivel3
    Dim oPRES4 As CPresconNivel4
    Dim nodx As MSComctlLib.node
    'Dim i As Integer
    
    
    
    tvwEstrPres.Nodes.clear
    
    Set nodx = tvwEstrPres.Nodes.Add(, , "Raiz ", PonerPrimeraLetraEnMayuscula(gParametrosGenerales.gsPlurPres2), "Raiz")
    nodx.Tag = "Raiz "
    
    nodx.Expanded = True
        
    Select Case gParametrosGenerales.giNEPP
        
        Case 1
                
            For Each opres1 In oPresupuestos
                If Not ((Not chkBajaLog.Value = vbChecked) And opres1.BajaLog) Then
                    scod1 = opres1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(opres1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, opres1.Cod & " - " & opres1.Den, "PRES1")
                    nodx.Tag = "PRES1" & opres1.Cod
                    If opres1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
            Next
        
        Case 2
            
            For Each opres1 In oPresupuestos
                If Not ((Not chkBajaLog.Value = vbChecked) And opres1.BajaLog) Then
                    scod1 = opres1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(opres1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, opres1.Cod & " - " & opres1.Den, "PRES1")
                    nodx.Tag = "PRES1" & opres1.Cod
                    If opres1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                
                For Each oPRES2 In opres1.PresContablesNivel2
                    If Not ((Not chkBajaLog.Value = vbChecked) And oPRES2.BajaLog) Then
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod
                        If oPRES2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                    End If
                    End If
                Next
            Next
        
        Case 3
            
            For Each opres1 In oPresupuestos
                If Not ((Not chkBajaLog.Value = vbChecked) And opres1.BajaLog) Then
                    scod1 = opres1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(opres1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, opres1.Cod & " - " & opres1.Den, "PRES1")
                    nodx.Tag = "PRES1" & opres1.Cod
                    If opres1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                
                For Each oPRES2 In opres1.PresContablesNivel2
                    If Not ((Not chkBajaLog.Value = vbChecked) And oPRES2.BajaLog) Then
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod
                        If oPRES2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                    End If
                       
                        For Each oPRES3 In oPRES2.PresContablesNivel3
                            If Not ((Not chkBajaLog.Value = vbChecked) And oPRES3.BajaLog) Then
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod
                                If oPRES3.BajaLog Then
                                    nodx.Image = "PRESBAJALOGICA"
                                    nodx.BackColor = &H8000000F 'color gris
                                End If
                            End If
                        Next
                Next
        Next
            
        Case 4
            
            For Each opres1 In oPresupuestos
                If Not ((Not chkBajaLog.Value = vbChecked) And opres1.BajaLog) Then
                    scod1 = opres1.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON1 - Len(opres1.Cod))
                    Set nodx = tvwEstrPres.Nodes.Add("Raiz ", tvwChild, "PRES1" & scod1, opres1.Cod & " - " & opres1.Den, "PRES1")
                    nodx.Tag = "PRES1" & opres1.Cod
                    If opres1.BajaLog Then
                        nodx.Image = "PRESBAJALOGICA"
                        nodx.BackColor = &H8000000F 'color gris
                    End If
                End If
                
                For Each oPRES2 In opres1.PresContablesNivel2
                    If Not ((Not chkBajaLog.Value = vbChecked) And oPRES2.BajaLog) Then
                        scod2 = oPRES2.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON2 - Len(oPRES2.Cod))
                        Set nodx = tvwEstrPres.Nodes.Add("PRES1" & scod1, tvwChild, "PRES2" & scod1 & scod2, oPRES2.Cod & " - " & oPRES2.Den, "PRES2")
                        nodx.Tag = "PRES2" & oPRES2.Cod
                        If oPRES2.BajaLog Then
                            nodx.Image = "PRESBAJALOGICA"
                            nodx.BackColor = &H8000000F 'color gris
                        End If
                    End If
                        
                        For Each oPRES3 In oPRES2.PresContablesNivel3
                            If Not ((Not chkBajaLog.Value = vbChecked) And oPRES3.BajaLog) Then
                                scod3 = oPRES3.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON3 - Len(oPRES3.Cod))
                                Set nodx = tvwEstrPres.Nodes.Add("PRES2" & scod1 & scod2, tvwChild, "PRES3" & scod1 & scod2 & scod3, oPRES3.Cod & " - " & oPRES3.Den, "PRES3")
                                nodx.Tag = "PRES3" & oPRES3.Cod
                                If oPRES3.BajaLog Then
                                    nodx.Image = "PRESBAJALOGICA"
                                    nodx.BackColor = &H8000000F 'color gris
                                End If
                            End If
                            
                            For Each oPRES4 In oPRES3.PresContablesNivel4
                                If Not ((Not chkBajaLog.Value = vbChecked) And oPRES4.BajaLog) Then
                                    scod4 = oPRES4.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPRESCON4 - Len(oPRES4.Cod))
                                    Set nodx = tvwEstrPres.Nodes.Add("PRES3" & scod1 & scod2 & scod3, tvwChild, "PRES4" & scod1 & scod2 & scod3 & scod4, oPRES4.Cod & " - " & oPRES4.Den, "PRES4")
                                    nodx.Tag = "PRES4" & oPRES4.Cod
                                    If oPRES4.BajaLog Then
                                        nodx.Image = "PRESBAJALOGICA"
                                        nodx.BackColor = &H8000000F 'color gris
                                    End If
                                End If
                            Next
                        Next
                Next
        Next
            
    End Select

    
    Set opres1 = Nothing
        
    Exit Sub
    
Error:
    Set nodx = Nothing
    Resume Next
End Sub

''' <summary>Configura la seguridad del formulario</summary>
''' <remarks>Llamada desde Form_Load</remarks>
''' <revision>LTG 03/05/2013</revision>

Private Sub ConfigurarInterfazSeguridad(ByVal nodo As MSComctlLib.node)
    If (nodo.Image = "PRESBAJALOGICA") Then 'Si es baja l�gica
        If bModif Then 'Variable de Seguridad
            MDI.mnuPopUpEstrPresConN1.Item(1).Visible = False
            MDI.mnuPopUpEstrPresConN1.Item(2).Visible = False
            MDI.mnuPopUpEstrPresConN1.Item(3).Visible = False
            MDI.mnuPopUpEstrPresConN1.Item(4).Visible = True     'Eliminar
            MDI.mnuPopUpEstrPresConN1.Item(5).Visible = False    'Baja l�gica
            MDI.mnuPopUpEstrPresConN1.Item(6).Visible = True     'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN1.Item(7).Visible = True     '---------
            MDI.mnuPopUpEstrPresConN1.Item(8).Visible = False
            MDI.mnuPopUpEstrPresConN1.Item(9).Visible = True     'Detalle
            MDI.mnuPopUpEstrPresConN1.Item(10).Visible = False
            MDI.mnuPopUpEstrPresConN1.Item(11).Visible = False
            MDI.mnuPopUpEstrPresConN1.Item(12).Visible = False
            
            MDI.mnuPopUpEstrPresConN2.Item(1).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(2).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(3).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(4).Visible = True     'Eliminar
            MDI.mnuPopUpEstrPresConN2.Item(5).Visible = False    'Baja l�gica
            MDI.mnuPopUpEstrPresConN2.Item(6).Visible = True     'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN2.Item(7).Visible = True     '---------
            MDI.mnuPopUpEstrPresConN2.Item(8).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(9).Visible = True     'Detalle
            MDI.mnuPopUpEstrPresConN2.Item(10).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(11).Visible = False
            MDI.mnuPopUpEstrPresConN2.Item(12).Visible = False
            
            MDI.mnuPopUpEstrPresConN3.Item(1).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(2).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(3).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(4).Visible = True     'Eliminar
            MDI.mnuPopUpEstrPresConN3.Item(5).Visible = False    'Baja l�gica
            MDI.mnuPopUpEstrPresConN3.Item(6).Visible = True     'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN3.Item(7).Visible = True     '---------
            MDI.mnuPopUpEstrPresConN3.Item(8).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(9).Visible = True     'Detalle
            MDI.mnuPopUpEstrPresConN3.Item(10).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(11).Visible = False
            MDI.mnuPopUpEstrPresConN3.Item(12).Visible = False
            
            MDI.mnuPopUpEstrPresConN4.Item(1).Visible = False
            MDI.mnuPopUpEstrPresConN4.Item(2).Visible = True     'Eliminar
            MDI.mnuPopUpEstrPresConN4.Item(3).Visible = False     'Baja l�gica
            MDI.mnuPopUpEstrPresConN4.Item(4).Visible = True     'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN4.Item(5).Visible = True     '---------
            MDI.mnuPopUpEstrPresConN4.Item(6).Visible = False
            MDI.mnuPopUpEstrPresConN4.Item(7).Visible = True     'Detalle
            MDI.mnuPopUpEstrPresConN4.Item(8).Visible = False
            MDI.mnuPopUpEstrPresConN4.Item(9).Visible = False
            MDI.mnuPopUpEstrPresConN4.Item(10).Visible = False
        End If
    Else                                    'si no es baja l�gica
        If bModif Then
            MDI.mnuPopUpEstrPresConN1.Item(1).Visible = True
            MDI.mnuPopUpEstrPresConN1.Item(2).Visible = True
            MDI.mnuPopUpEstrPresConN1.Item(3).Visible = True
            MDI.mnuPopUpEstrPresConN1.Item(4).Visible = True
            MDI.mnuPopUpEstrPresConN1.Item(5).Visible = True     'Baja l�gica
            MDI.mnuPopUpEstrPresConN1.Item(6).Visible = False    'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN1.Item(7).Visible = False    '---------
            MDI.mnuPopUpEstrPresConN1.Item(10).Visible = True
            MDI.mnuPopUpEstrPresConN1.Item(11).Visible = True
            MDI.mnuPopUpEstrPresConN1.Item(12).Visible = True
            
            MDI.mnuPopUpEstrPresConN2.Item(1).Visible = True
            MDI.mnuPopUpEstrPresConN2.Item(2).Visible = True
            MDI.mnuPopUpEstrPresConN2.Item(3).Visible = True
            MDI.mnuPopUpEstrPresConN2.Item(4).Visible = True
            MDI.mnuPopUpEstrPresConN2.Item(5).Visible = True     'Baja l�gica
            MDI.mnuPopUpEstrPresConN2.Item(6).Visible = False    'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN2.Item(7).Visible = False    '---------
            MDI.mnuPopUpEstrPresConN2.Item(10).Visible = True
            MDI.mnuPopUpEstrPresConN2.Item(11).Visible = True
            MDI.mnuPopUpEstrPresConN2.Item(12).Visible = True
            
            MDI.mnuPopUpEstrPresConN3.Item(1).Visible = True
            MDI.mnuPopUpEstrPresConN3.Item(2).Visible = True
            MDI.mnuPopUpEstrPresConN3.Item(3).Visible = True
            MDI.mnuPopUpEstrPresConN3.Item(4).Visible = True
            MDI.mnuPopUpEstrPresConN3.Item(5).Visible = True     'Baja l�gica
            MDI.mnuPopUpEstrPresConN3.Item(6).Visible = False    'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN3.Item(7).Visible = False    '---------
            MDI.mnuPopUpEstrPresConN3.Item(10).Visible = True
            MDI.mnuPopUpEstrPresConN3.Item(11).Visible = True
            MDI.mnuPopUpEstrPresConN3.Item(12).Visible = True
            
            MDI.mnuPopUpEstrPresConN4.Item(1).Visible = True
            MDI.mnuPopUpEstrPresConN4.Item(2).Visible = True
            MDI.mnuPopUpEstrPresConN4.Item(3).Visible = True     'Baja l�gica
            MDI.mnuPopUpEstrPresConN4.Item(4).Visible = False    'Deshacer baja l�gica
            MDI.mnuPopUpEstrPresConN4.Item(5).Visible = False    '---------
            MDI.mnuPopUpEstrPresConN4.Item(8).Visible = True
            MDI.mnuPopUpEstrPresConN4.Item(9).Visible = True
            MDI.mnuPopUpEstrPresConN4.Item(10).Visible = True
            
            cmdBajaLog.Enabled = True
        End If
        
        MDI.mnuPopUpEstrPresConN1.Item(8).Visible = bModifCod
        MDI.mnuPopUpEstrPresConN2.Item(8).Visible = bModifCod
        MDI.mnuPopUpEstrPresConN3.Item(8).Visible = bModifCod
        MDI.mnuPopUpEstrPresConN4.Item(6).Visible = bModifCod
        
        If bRUO Then
            If Not oUsuarioSummit.ExisteUODebajo Then
                MDI.mnuPopUpEstrPresConN1.Item(12).Visible = False
                MDI.mnuPopUpEstrPresConN2.Item(12).Visible = False
                MDI.mnuPopUpEstrPresConN3.Item(12).Visible = False
                MDI.mnuPopUpEstrPresConN4.Item(10).Visible = False
            End If
        End If
    
    End If

End Sub

