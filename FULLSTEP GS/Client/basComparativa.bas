Attribute VB_Name = "basComparativa"
Option Explicit

Private m_sExportExcel(1 To 15) As String

''' <summary>Importaci�n de la Excel de Objetivos</summary>
''' <param name="frmOrigen">formulario de origen</param>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: mnuImportar ; Tiempo m�ximo:0 </remarks>
''' <revisi�n>JVS 09/06/2011</revisi�n>

Public Sub ImportarExcelObjetivos(ByVal frmOrigen As Form)
    Dim sFileName As String
    Dim sIdiDialogoExcel As String
    Dim sIdiFiltroExcel As String
    Dim sPorDefecto As String
    Dim m_oGrupoSeleccionado As CGrupo
    Dim sConnect As String
    Dim oExcelAdoConn As ADODB.Connection
    Dim oExcelAdoRS As ADODB.Recordset
    Dim Resultadoxls()
    Dim i As Integer
    Dim lNumFilas, j As Long
    Dim indGrupos As Long
    Dim vbm As Variant
    Dim dFecha As Date
    Dim bCerradoNadaQHacer As Boolean
    Dim teserror As TipoErrorSummit
    Dim bEsDeTodos  As Boolean
    Dim iLineaInicial As Integer
    
    On Error GoTo Error
    
    sPorDefecto = "Fichero"
    
    If m_sExportExcel(1) = "" Then CargarRecursos
    
    sFileName = frmOrigen.MostrarCommonDialog(sIdiDialogoExcel, sIdiFiltroExcel, sPorDefecto & ".xls")
    If sFileName = "" Then Exit Sub
    
    Set oExcelAdoConn = New ADODB.Connection
             
             
    If UCase(Mid(sFileName, Len(sFileName) - 3)) = UCase(".xls") Then
        sConnect = "Provider=MSDASQL.1;" _
             & "Extended Properties=""DBQ=" & sFileName & ";" _
             & "Driver={Microsoft Excel Driver (*.xls)};" _
             & "FIL=excel 8.0;" _
             & "ReadOnly=0;" _
             & "UID=admin;"""
    Else
    
        sConnect = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFileName & ";" _
            & "Extended Properties=""Excel 12.0 Macro;HDR=YES"";"
    End If
             
    oExcelAdoConn.Open sConnect
    
    On Error GoTo ErrorNoEsDeTodos
    
    Set oExcelAdoRS = New ADODB.Recordset
    
    bEsDeTodos = True
    
    'CALIDAD: El * se queda pq no se sabe de antemano q tiene la excel
    oExcelAdoRS.Open "SELECT * FROM [" & m_sExportExcel(15) & "$]", oExcelAdoConn
        
    On Error GoTo Error
    
    If bEsDeTodos Then
        If oExcelAdoRS.EOF Then
            oExcelAdoRS.Close
            Exit Sub
        End If
        
        Resultadoxls = oExcelAdoRS.GetRows()
        oExcelAdoRS.MoveFirst
        
        iLineaInicial = 1
    End If
    
    dFecha = Now
    bCerradoNadaQHacer = False
        
    For indGrupos = 1 To frmOrigen.m_oProcesoSeleccionado.Grupos.Count
        If bEsDeTodos Then
        Else
            If frmOrigen.m_oProcesoSeleccionado.Grupos.Item(indGrupos).Cerrado Then
                bCerradoNadaQHacer = True
            Else
                'CALIDAD: El * se queda pq no se sabe de antemano q tiene la excel
                oExcelAdoRS.Open "SELECT * FROM [" & frmOrigen.m_oProcesoSeleccionado.Grupos.Item(indGrupos).Codigo & "-" & m_sExportExcel(4) & "$]", oExcelAdoConn
            End If
        End If
    
        If Not bCerradoNadaQHacer Then
            Dim sValor As String
            Dim sId, sObjetivoAnt, sObjetivoNue As String
            
            If Not oExcelAdoRS.EOF Then
                If Not bEsDeTodos Then
                    Resultadoxls = oExcelAdoRS.GetRows()
                    
                    iLineaInicial = 1
                End If
                
                If Not bEsDeTodos Then
                    lNumFilas = UBound(Resultadoxls, 2) + 1
                Else
                    lNumFilas = frmOrigen.m_oProcesoSeleccionado.Grupos.Item(indGrupos).Items.Count + iLineaInicial
                End If
                For j = iLineaInicial To lNumFilas - 1
                    sId = ""
                    sValor = ""
                    
                    sValor = NullToStr(Resultadoxls(10, j))
                    If sValor <> "" Then 'Columna del ID
                        sId = sValor
                    End If
                    sValor = NullToStr(Resultadoxls(4, j))
                    If sId <> "" Then  'Columna del Objetivo
                    
                        If frmOrigen.m_oProcesoSeleccionado.Grupos.Item(indGrupos).Items.Item(CStr(sId)).Cerrado = False Then
                    
                            frmOrigen.m_oProcesoSeleccionado.Grupos.Item(indGrupos).Items.Item(CStr(sId)).Objetivo = sValor
                            frmOrigen.m_oProcesoSeleccionado.Grupos.Item(indGrupos).Items.Item(CStr(sId)).FechaObjetivo = dFecha
                            teserror = frmOrigen.m_oProcesoSeleccionado.Grupos.Item(indGrupos).Items.Item(CStr(sId)).ModificarObjetivo
                            If teserror.NumError <> TESnoerror And teserror.NumError <> 25 Then
                                basErrores.TratarError teserror
                                Exit Sub
                            Else
                               basSeguridad.RegistrarAccion accionessummit.ACCOfeCompFijObjetivos, "Anyo:" & CStr(frmOrigen.m_oProcesoSeleccionado.Anyo) & " Gmn1:" & CStr(frmOrigen.m_oProcesoSeleccionado.GMN1Cod) & " Proce:" & CStr(frmOrigen.m_oProcesoSeleccionado.Cod) & " Item:" & CStr(sId)
                            End If
                        End If
                    End If
                Next j
                
                If Not bEsDeTodos Then
                    oExcelAdoRS.Close
                Else
                    iLineaInicial = lNumFilas
                End If
            End If
        End If
    Next indGrupos
    
    If bEsDeTodos Then oExcelAdoRS.Close
    
    'Actualiza la apariencia de las grids despues de importar
    If frmOrigen.sstabComparativa.selectedItem.Tag = "ALL" Then
        'Pesta�a de All
        With frmOrigen.m_oProcesoSeleccionado.Grupos
            i = frmOrigen.sdbgAll.Rows - 1
            While i >= 0
                vbm = frmOrigen.sdbgAll.AddItemBookmark(i)
                'Id del �tem que tratamos
                sId = .Item(frmOrigen.sdbgAll.Columns("GRUPO").CellValue(vbm)).Items.Item(frmOrigen.sdbgAll.Columns("ID").CellValue(vbm)).Id
                sObjetivoAnt = frmOrigen.sdbgAll.Columns("OBJ").CellValue(vbm)
                'Refresca la grid
                sObjetivoNue = NullToStr(.Item(frmOrigen.sdbgAll.Columns("GRUPO").CellValue(vbm)).Items.Item(CStr(sId)).Objetivo)
                If NullToDbl0(sObjetivoNue) <> NullToDbl0(sObjetivoAnt) Then
                    frmOrigen.sdbgAll.Bookmark = vbm
                    frmOrigen.sdbgAll.Columns("OBJ").Value = sObjetivoNue
                End If
                i = i - 1
            Wend
    
        End With
        
        frmOrigen.sdbgAll.Refresh
        frmOrigen.sdbgAll.MoveFirst
                
     Else
        'Es un grupo
        Set m_oGrupoSeleccionado = frmOrigen.m_oProcesoSeleccionado.Grupos.Item(frmOrigen.sstabComparativa.selectedItem.Tag)
        If Not m_oGrupoSeleccionado Is Nothing Then
            i = frmOrigen.sdbgAdj.Rows - 1
            While i >= 0
                vbm = frmOrigen.sdbgAdj.AddItemBookmark(i)
                'Id del �tem que tratamos
                sId = m_oGrupoSeleccionado.Items.Item(frmOrigen.sdbgAdj.Columns("ID").CellValue(vbm)).Id
                sObjetivoAnt = frmOrigen.sdbgAdj.Columns("OBJ").CellValue(vbm)
                'Refresca la grid
                sObjetivoNue = NullToStr(frmOrigen.m_oGrupoSeleccionado.Items.Item(CStr(sId)).Objetivo)
                If NullToDbl0(sObjetivoNue) <> NullToDbl0(sObjetivoAnt) Then
                    frmOrigen.sdbgAdj.Bookmark = vbm
                    frmOrigen.sdbgAdj.Columns("OBJ").Value = sObjetivoNue
                End If
                i = i - 1
            Wend
                
            frmOrigen.sdbgAdj.Refresh
            frmOrigen.sdbgAdj.MoveFirst
        End If
    End If
    
    Exit Sub
ErrorNoEsDeTodos:
    If err.Number = -2147217900 Then
        bEsDeTodos = False
        Resume Next
    End If
Error:
    If err.Number <> cdlCancel Then
        oMensajes.MensajeOKOnly 754
        If oExcelAdoConn.State <> adStateClosed Then
            oExcelAdoConn.Close
        End If
    End If
End Sub

''' <summary>Carga de recursos del m�dulo del Excel de Objetivos</summary>
''' <returns>Nada</returns>
''' <remarks>Llamada desde: ImportarExcelObjetivos ; ExportarExcelObjetivos Tiempo m�ximo:0 </remarks>
''' <revisi�n>JVS 09/06/2011</revisi�n>
Private Sub CargarRecursos()
    Dim ador As ador.Recordset

    On Error Resume Next
    
    Set ador = oGestorIdiomas.DevolverTextosDelModulo(EXCEL_OBJETIVOS, gParametrosInstalacion.gIdioma)
    
    If Not ador Is Nothing Then
        m_sExportExcel(1) = ador(0).Value  '1 Exportando a excel
        ador.MoveNext
        m_sExportExcel(2) = ador(0).Value  '2 Iniciando transferencia
        ador.MoveNext
        m_sExportExcel(3) = ador(0).Value  '3 Transfiriendo objetivos del grupo
        ador.MoveNext
        m_sExportExcel(4) = ador(0).Value  '4 Objetivos
        ador.MoveNext
        m_sExportExcel(5) = ador(0).Value  '5 Datos del �tem
        ador.MoveNext
        m_sExportExcel(6) = ador(0).Value  '6 Adjudicado%
        ador.MoveNext
        m_sExportExcel(7) = ador(0).Value  '7 Proveedores
        ador.MoveNext
        m_sExportExcel(8) = ador(0).Value  '8 Pres.unitario
        ador.MoveNext
        m_sExportExcel(9) = ador(0).Value  '9 Objetivo
        ador.MoveNext
        m_sExportExcel(10) = ador(0).Value  '10 Cantidad
        ador.MoveNext
        m_sExportExcel(11) = ador(0).Value  '11 Precio
        ador.MoveNext
        m_sExportExcel(12) = ador(0).Value  '12 Ahorro imp.
        ador.MoveNext
        m_sExportExcel(13) = ador(0).Value  '13 Ahorro%
        ador.MoveNext
        m_sExportExcel(14) = ador(0).Value  '14 Importe adj.
        ador.MoveNext
        m_sExportExcel(15) = ador(0).Value  '15 Todos los items
        
        ador.Close
    End If
    
    Set ador = Nothing
End Sub

''' <summary>Comprueba que el valor de un atributo es correcto con respecto a su tipo</summary>
''' <param name="oProceso">Proceso al que pertenecen los atributos</param>
''' <param name="lIdAtrib">Id. atributo</param>
''' <param name="vValor">Valor</param>
''' <param name="sSi">Literal para Si</param>
''' <param name="sNo">Literal para No</param>
''' <param name="sMayor">Literal para Mayor</param>
''' <param name="sMenor">Literal para Menor</param>
''' <param name="sEntre">Literal para Entre</param>
''' <param name="Ambito">Ambito del atributo</param>
''' <remarks>Llamada desde: frmADJ, frmRESREU, ctlADJEscalado, frmADJItem</remarks>
''' <returns>Booleano indicando si el valor es v�lido</returns>
''' <revision>LTG  22/05/2012</revision>

Public Function ComprobarValorAtributo(ByVal oProceso As cProceso, ByVal lIdAtrib As Long, ByVal vValor As Variant, ByVal sSi As String, _
        ByVal sNo As String, ByVal sMayor As String, ByVal sMenor As String, ByVal sEntre As String, Optional ByVal ambito As Variant) As Boolean
    Dim oatrib As CAtributo
    Dim bSalir As Boolean
    Dim bEncontrado As Boolean
    Dim oElem As CValorPond
    Dim sError As String
    Dim oAtribs As CAtributos
    
    If Not IsMissing(ambito) Then
        Select Case ambito
            Case TipoAmbitoProceso.AmbProceso
                Set oAtribs = oProceso.ATRIBUTOS
            Case TipoAmbitoProceso.AmbGrupo
                Set oAtribs = oProceso.AtributosGrupo
            Case TipoAmbitoProceso.AmbItem
                Set oAtribs = oProceso.AtributosItem
        End Select
        
        Set oatrib = oAtribs.Item(CStr(lIdAtrib))
    Else
        Set oAtribs = oProceso.ATRIBUTOS
        If Not oAtribs Is Nothing Then
            If Not oAtribs.Item(CStr(lIdAtrib)) Is Nothing Then
                Set oatrib = oAtribs.Item(CStr(lIdAtrib))
            End If
        End If
    
        If oatrib Is Nothing Then
            Set oAtribs = Nothing
            Set oAtribs = oProceso.AtributosGrupo
            If Not oAtribs Is Nothing Then
                If Not oAtribs.Item(CStr(lIdAtrib)) Is Nothing Then
                    Set oatrib = oAtribs.Item(CStr(lIdAtrib))
                End If
            End If
        End If
        
        If oatrib Is Nothing Then
            Set oAtribs = Nothing
            Set oAtribs = oProceso.AtributosItem
            If Not oAtribs Is Nothing Then
                If Not oAtribs.Item(CStr(lIdAtrib)) Is Nothing Then
                    Set oatrib = oAtribs.Item(CStr(lIdAtrib))
                End If
            End If
        End If
    End If
    
    If vValor <> "" Then
        bSalir = False
        Select Case oatrib.Tipo
            Case TipoNumerico
                If Not IsNumeric(vValor) Then
                    sError = "TIPO2"
                    bSalir = True
                End If
            Case TipoFecha
                If Not IsDate(vValor) Then
                    sError = "TIPO3"
                    bSalir = True
                End If
            Case TipoBoolean
                If UCase(vValor) <> UCase(sSi) And UCase(vValor) <> UCase(sNo) And vValor <> "" Then
                    sError = "TIPO4"
                    bSalir = True
                End If
        End Select
        If bSalir Then
            oMensajes.AtributoValorNoValido sError
            ComprobarValorAtributo = False
            Exit Function
        End If
        
        bEncontrado = False
        If oatrib.TipoIntroduccion = Introselec Then
            For Each oElem In oatrib.ListaPonderacion
                Select Case oatrib.Tipo
                    Case TipoString
                        If oElem.ValorLista = vValor Then
                            bEncontrado = True
                            Exit For
                        End If
                    Case TipoNumerico
                        If CDbl(oElem.ValorLista) = CDbl(vValor) Then
                            bEncontrado = True
                            Exit For
                        End If
                    Case TipoFecha
                        If CDate(oElem.ValorLista) = CDate(vValor) Then
                            bEncontrado = True
                            Exit For
                        End If
                End Select
            Next
            If Not bEncontrado Then
                oMensajes.AtributoValorNoValido "NO_LISTA"
                ComprobarValorAtributo = False
                Exit Function
            End If
        End If
        
        bSalir = False
        sError = ""
        If oatrib.TipoIntroduccion = IntroLibre Then
            Select Case oatrib.Tipo
                Case TipoNumerico
                    If IsNumeric(oatrib.Maximo) Then
                        sError = FormateoNumerico(oatrib.Maximo)
                        If CDbl(oatrib.Maximo) < CDbl(vValor) Then
                            bSalir = True
                        End If
                    End If
                    If IsNumeric(oatrib.Minimo) Then
                        If sError = "" Then
                            sError = sMayor & " " & FormateoNumerico(oatrib.Minimo)
                        Else
                            sError = sEntre & "  " & FormateoNumerico(oatrib.Minimo) & " - " & sError
                        End If
                        If CDbl(oatrib.Minimo) > CDbl(vValor) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sMenor & " " & sError
                        End If
                    End If
                Case TipoFecha
                    If IsDate(oatrib.Maximo) Then
                        sError = oatrib.Maximo
                        If CDate(oatrib.Maximo) < CDate(vValor) Then
                            bSalir = True
                        End If
                    End If
                    If IsDate(oatrib.Minimo) Then
                        If sError = "" Then
                            sError = sMayor & " " & oatrib.Minimo
                        Else
                            sError = sEntre & "  " & oatrib.Minimo & " - " & sError
                        End If
                        If CDate(oatrib.Minimo) > CDate(vValor) Then
                            bSalir = True
                        End If
                    Else
                        If sError <> "" Then
                            sError = sMenor & " " & sError
                        End If
                    End If
            End Select
            If bSalir Then
                oMensajes.AtributoValorNoValido sError
                ComprobarValorAtributo = False
                Exit Function
            End If
        End If
               
    End If 'Si hab�a valor

    ComprobarValorAtributo = True
End Function

''' <summary>Indica si un proveedor tiene ofertas</summary>
''' <param name="oOfertas">Ofertas</param>
''' <param name="sCod">C�digo del proveedor</param>
''' <param name="bEscalados">Indica si hay escalados</param>
''' <param name="oEscalados">Escalados</param>
''' <returns>Booleano indicando si es un proveedor sin ofertas</returns>
''' <remarks>Llamada desde: OcultarProvesSinOfe; Tiempo m�ximo: 0</remarks>
''' <revision>21/03/2012</revision>

Public Function ProveSinOfes(ByVal oOfertas As COfertas, ByVal sCod As String, ByVal bEscalados As Boolean, Optional ByVal oEscalados As CEscalados) As Boolean
    Dim oEsc As CEscalado
    Dim bHayOfes As Boolean
    Dim i As Integer
    
    bHayOfes = True
    
    If Not oOfertas Is Nothing Then
        If oOfertas.Item(sCod) Is Nothing Then
            bHayOfes = False
        Else
            bHayOfes = False
            
            If bEscalados Then
                If Not oOfertas.Item(sCod).Lineas Is Nothing Then
                    For i = 1 To oOfertas.Item(sCod).Lineas.Count
                        For Each oEsc In oEscalados
                            If Not oOfertas.Item(sCod).Lineas.Item(i).Escalados.Item(CStr(oEsc.Id)) Is Nothing Then
                                If Not IsNull(oOfertas.Item(sCod).Lineas.Item(i).Escalados.Item(CStr(oEsc.Id)).PrecioOferta) Then
                                    bHayOfes = True
                                    Exit For
                                End If
                            End If
                        Next
                        
                        If bHayOfes Then Exit For
                    Next
                End If
            End If
        End If
    Else
        bHayOfes = False
    End If
    
    ProveSinOfes = Not bHayOfes
    
    Set oEsc = Nothing
End Function

''' <summary>Indica si una oferta tiene precios para todos los items de un proceso</summary>
''' <param name="oProceso">Proceso</param>
''' <param name="sProveCod">Cod. proveedor</param>
''' <returns>Booleano indicando si la oferta tiene todos los precios</returns>
''' <remarks>Llamada desde: frmADJ.CalcularOptimaProceso,frmADJ.CalcularOptimaGrupo</remarks>
''' <revision>26/06/2012</revision>

Public Function OfeCompletaProceso(ByVal oProceso As cProceso, ByVal sProveCod As String) As Boolean
    Dim oGrupo As CGrupo
    Dim oOferta As COferta
    
    OfeCompletaProceso = True
    
    For Each oGrupo In oProceso.Grupos
        Set oOferta = oGrupo.UltimasOfertas.Item(sProveCod)
        If Not oOferta Is Nothing Then
            If oOferta.TienePrecios Then
                If Not OfeCompleta(oGrupo, oOferta) Then
                    OfeCompletaProceso = False
                    Exit For
                End If
            Else
                OfeCompletaProceso = False
                Exit For
            End If
        Else
            OfeCompletaProceso = False
            Exit For
        End If
    Next
End Function

''' <summary>Indica si una oferta tiene precios para todos los items</summary>
''' <param name="oGrupo">grupo para el que se hace la oferta</param>
''' <param name="oOferta">Oferta</param>
''' <param name="bEscalados">Indica si se usa escalados</param>
''' <returns>Booleano indicando si la oferta tiene todos los precios</returns>
''' <remarks>Llamada desde: frmADJ.CalcularOptimaProceso,frmADJ.CalcularOptimaGrupo</remarks>
''' <revision>26/06/2012</revision>

Public Function OfeCompleta(ByVal oGrupo As CGrupo, ByVal oOferta As COferta) As Boolean
    Dim oLinea As CPrecioItem
    Dim oEsc As CEscalado
    Dim bHayPrecioEsc As Boolean
    
    OfeCompleta = True
    
    If Not oOferta Is Nothing Then
        If Not oOferta.Lineas Is Nothing Then
            For Each oLinea In oOferta.Lineas
                If oGrupo.UsarEscalados = 1 Then
                    bHayPrecioEsc = False
                    For Each oEsc In oGrupo.Escalados
                        If Not oLinea.Escalados.Item(CStr(oEsc.Id)) Is Nothing Then
                            If Not IsNull(oLinea.Escalados.Item(CStr(oEsc.Id)).PrecioOferta) Then
                                bHayPrecioEsc = True
                                Exit For
                            End If
                        End If
                    Next
                    
                    If Not bHayPrecioEsc Then
                        OfeCompleta = False
                        Exit For
                    End If
                Else
                    If IsNull(oLinea.PrecioOferta) Then
                        OfeCompleta = False
                        Exit For
                    End If
                End If
            Next
            Set oLinea = Nothing
        Else
            OfeCompleta = False
        End If
    Else
        OfeCompleta = False
    End If
End Function

''' <summary>Calcula el precio v�lido e importe seg�n las f�rmulas a aplicar
''' Se va recorriendo cada grupo y calculado el precio v�lido de las �ltimas ofertas
''' para el grupo aplic�ndole las f�rmulas correspondientes.</summary>
''' <param name="oProcesoSeleccionado">Proceso seleccionado</param>
''' <param name="oAtribsFormulas"></param>
''' <param name="oProvesAsig">proveedores asignados</param>
''' <param name="dblImporteAux">Importe auxiliar</param>
''' <remarks>Llamada desde: CalcularOptimaPrecio; Tiempo m�ximo: 0,1</remarks>
''' <revision>LTG 18/11/2011</revision>

Public Sub CalcularPrecValido(ByRef oProcesoSeleccionado As cProceso, ByRef oAtribsFormulas As CAtributos, ByRef oProvesAsig As CProveedores, ByRef dblImporteAux As Double)
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oProve As CProveedor
    Dim oPrecItem As CPrecioItem
    Dim dPrecioAplicFormula As Variant
    Dim bAplicTotalItem As Boolean
    Dim dImporteIt As Variant
    Dim oEsc As CEscalado
   
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For Each oGrupo In oProcesoSeleccionado.Grupos
        bAplicTotalItem = ComprobarAplicarAtribsItem(oAtribsFormulas, 0, oGrupo.Codigo)
        'Ahora empieza el c�lculo del precio unitario
        For Each oProve In oProvesAsig
            'Para cada item:
            For Each oItem In oGrupo.Items
                If oItem.Confirmado Then
                    dPrecioAplicFormula = 0
                                            
                    If Not oGrupo.UltimasOfertas.Item(oProve.Cod) Is Nothing Then
                        Set oPrecItem = oGrupo.UltimasOfertas.Item(oProve.Cod).Lineas.Item(CStr(oItem.Id))
                        
                        If Not oPrecItem Is Nothing Then
                            If oGrupo.UsarEscalados Then
                                If Not oPrecItem.Escalados Is Nothing Then
                                    For Each oEsc In oPrecItem.Escalados
                                        dPrecioAplicFormula = oEsc.Precio
                                        
                                        If Not IsNull(dPrecioAplicFormula) Then
                                            dPrecioAplicFormula = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, dPrecioAplicFormula, TipoAplicarAPrecio.UnitarioItem, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, oItem.Id, oEsc.Id)
                                            
                                            'Guarda el precio v�lido obtenido en la colecci�n, en moneda de la oferta
                                            oEsc.PrecioOferta = dPrecioAplicFormula
                                        Else
                                            oEsc.PrecioOferta = Null
                                        End If
                                    Next
                                    
                                    Set oEsc = Nothing
                                End If
                            Else
                                'Comprueba si se va a usar el precio 1,2 o 3
                                Select Case oPrecItem.Usar
                                  Case 1
                                        dPrecioAplicFormula = oPrecItem.Precio
                                  Case 2
                                        dPrecioAplicFormula = oPrecItem.Precio2
                                  Case 3
                                        dPrecioAplicFormula = oPrecItem.Precio3
                                End Select
                                If Not IsNull(dPrecioAplicFormula) Then
                                    dPrecioAplicFormula = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, dPrecioAplicFormula, TipoAplicarAPrecio.UnitarioItem, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, oItem.Id)
                                    
                                    'Guarda el precio v�lido obtenido en la colecci�n, en moneda de la oferta
                                    oPrecItem.PrecioOferta = dPrecioAplicFormula
                                    'Calculamos importe de �tem con la cantidad total del item para que sean todos los proves igual
                                    dImporteIt = dPrecioAplicFormula * oItem.Cantidad

                                    'Aplicamos atributos a importe de �tem
                                    If bAplicTotalItem Then
                                        dImporteIt = AplicarAtributos(oProcesoSeleccionado, oAtribsFormulas, dImporteIt, TotalItem, oProve.Cod, dblImporteAux, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE, oGrupo.Codigo, oItem.Id)
                                    End If
                                    oPrecItem.importe = dImporteIt
                                Else
                                    oPrecItem.importe = Null
                                End If
                            End If
                        End If
                    End If
                End If
            Next 'oItem
        Next oProve
    Next  'oGrupo
    
    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      oFSGSRaiz.TratarError "M�dulo", "basComparativa", "CalcularPrecValido", err, Erl
      Exit Sub
   End If
End Sub

''' <summary></summary>
''' <param name="sGrupo"></param>
''' <param name="lItem"></param>
''' <param name="oProvesAsig">proveedores asignados</param>
''' <param name="dblImporteAux">Importe auxiliar</param>
''' <remarks>Llamada desde: frmADJ, frmRESREU</remarks>
''' <revision>LTG 18/11/2011</revision>

Public Function EstaItemEnPedidos(ByVal sGrupo As String, ByVal lItem As Long, ByRef oProcesoSeleccionado As cProceso, ByRef oProvesAsig As CProveedores, ByRef iInd As Integer, _
        ByRef vEnPedidos As Variant, ByRef oAdjuds As CAdjudicaciones) As Boolean
    'Devuelve:  el porcentaje eliminado
    Dim oProv As CProveedor
    Dim oGrupo As CGrupo
    Dim sCod As String
    Dim oAdjs As CAdjudicaciones
    Dim oAdj As CAdjudicacion
    
    'Elimina las adjudicaciones que no pertenecen a pedidos ni a cat�logo
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    EstaItemEnPedidos = False
    Set oAdjs = oFSGSRaiz.Generar_CAdjudicaciones
    Set oGrupo = oProcesoSeleccionado.Grupos.Item(sGrupo)
    For Each oProv In oProvesAsig
        sCod = oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
        sCod = CStr(lItem) & sCod
        If Not oGrupo.Adjudicaciones.Item(sCod) Is Nothing Then
            'Si est� en pedidos o catalogo no se puede cambiar la adjudicaci�n
            If Not IsNull(oGrupo.Adjudicaciones.Item(sCod).pedido) Then
                ReDim Preserve vEnPedidos(3, iInd)
                vEnPedidos(0, iInd) = Trim(NullToStr(oGrupo.Items.Item(CStr(lItem)).ArticuloCod) & "  " & NullToStr(oGrupo.Items.Item(CStr(lItem)).Descr))
                vEnPedidos(1, iInd) = oGrupo.Adjudicaciones.Item(sCod).pedido
                vEnPedidos(2, iInd) = Trim(oProv.Cod & "  " & oProv.Den)
                vEnPedidos(3, iInd) = 174 'El �tem esta incluido en los pedidos
                iInd = iInd + 1
                EstaItemEnPedidos = True
            ElseIf oGrupo.Adjudicaciones.Item(sCod).Catalogo = 1 Then
                ReDim Preserve vEnPedidos(3, iInd)
                vEnPedidos(0, iInd) = Trim(NullToStr(oGrupo.Items.Item(CStr(lItem)).ArticuloCod) & "  " & NullToStr(oGrupo.Items.Item(CStr(lItem)).Descr))
                vEnPedidos(1, iInd) = oGrupo.Adjudicaciones.Item(sCod).pedido
                vEnPedidos(2, iInd) = Trim(oProv.Cod & "  " & oProv.Den)
                vEnPedidos(3, iInd) = 175 'El �tem esta incluido en los pedidos
                iInd = iInd + 1
                EstaItemEnPedidos = True
            End If
            If oAdjs.Item(sCod) Is Nothing Then
                With oGrupo.Adjudicaciones.Item(sCod)
                    oAdjs.Add oProv.Cod, .NumOfe, .Id, .Precio, .Porcentaje, , , , , , .Catalogo, .pedido
                    oAdjs.Item(sCod).importe = .importe
                    oAdjs.Item(sCod).ImporteAdj = .ImporteAdj
                    oAdjs.Item(sCod).ImporteAdjTot = .ImporteAdjTot
                End With
            End If
        End If
    Next
    
    If EstaItemEnPedidos Then
        For Each oAdj In oAdjs
            sCod = oAdj.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdj.ProveCod))
            sCod = CStr(lItem) & sCod
            If oAdjuds.Item(sCod) Is Nothing Then
                With oAdj
                    oAdjuds.Add .ProveCod, .NumOfe, .Id, .Precio, .Porcentaje, , , , , , .Catalogo, .pedido
                    oAdjuds.Item(sCod).importe = .importe
                    oAdjuds.Item(sCod).ImporteAdj = .ImporteAdj
                    oAdjuds.Item(sCod).ImporteAdjTot = .ImporteAdjTot
                End With
            End If
        Next
    End If
    Set oGrupo = Nothing
    Set oAdjs = Nothing
    Set oAdj = Nothing

    '----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
Error:
   If err.Number <> 0 Then
      oFSGSRaiz.TratarError "M�dulo", "basComparativa", "EstaItemEnPedidos", err, Erl
      Exit Function
   End If
End Function

''' <summary>Usando la informaci�n de ultimas ofertas de los proveedores se calcula la adjudicaci�n optima y la aplica en pantalla.</summary>
''' <param name="m_oProcesoSeleccionado">proceso seleccionado</param>
''' <param name="m_oAtribsFormulas"></param>
''' <param name="m_oProvesAsig">proveedores asignados</param>
''' <param name="m_dblImporteAux">importe auxiliar</param>
''' <param name="m_iInd"></param>
''' <param name="m_vEnPedidos"></param>
''' <param name="m_oAdjuds">adjudicaciones</param>
''' <param name="sMensaje">mensaje a mostrar</param>
''' <param name="sCantSinAdj"></param>
''' <remarks>Llamada desde: frmADJ.CalcularOptimaPrecio, frmRESREU.CalcularOptimaPrecio</remarks>
''' <revision>LTG 18/11/2011</revision>

Public Sub OptimaPrecio(ByRef m_oProcesoSeleccionado As cProceso, ByRef m_oAtribsFormulas As CAtributos, ByRef m_oProvesAsig As CProveedores, ByRef m_dblImporteAux As Double, ByRef m_iInd As Integer, _
        ByRef m_vEnPedidos As Variant, ByRef m_oAdjuds As CAdjudicaciones, ByRef sMensaje As String, ByVal sCantSinAdj As String, ByRef sItemEscEmpatados As Variant)
    Dim oGrupo As CGrupo
    Dim oItem As CItem
    Dim oProv As CProveedor
    Dim oOferta As COferta
    Dim sProv() As String
    Dim bMejor As Boolean
    Dim j As Integer
    Dim sCod As String
    Dim dporcentaje As Double
    Dim iProv As Integer
    Dim dCantAdj As Double
    Dim dResto As Double
    Dim dadj As Double
    Dim dCantMax As Double
    Dim dCapacidad As Variant
    Dim dUltimoCantAdj As Double
    Dim sProv2() As String
    Dim i As Integer
    Dim lIdEscalado As Long
    Dim dblImpOfeEsc() As Double
    Dim lEsc() As Long
    Dim dblImporteEsc As Double
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ReDim sItemEscEmpatados(1)
    
    'Recalcula el precio v�lido para calcular la �ptima---Si no se hace no funciona bien,algunos precios v�lidos estar�an a null
    CalcularPrecValido m_oProcesoSeleccionado, m_oAtribsFormulas, m_oProvesAsig, m_dblImporteAux
    
    sMensaje = ""
    m_iInd = 0
    Erase m_vEnPedidos
    'Calcula la �ptima seg�n el precio
    For Each oGrupo In m_oProcesoSeleccionado.Grupos
        If m_oProcesoSeleccionado.Estado = ParcialmenteCerrado Then
            Set m_oAdjuds = EliminarAdjudicacionesPC(oGrupo)
        Else
            Set m_oAdjuds = Nothing
            Set m_oAdjuds = oFSGSRaiz.Generar_CAdjudicaciones
        End If
        For Each oItem In oGrupo.Items
            '''
            If oItem.Cerrado Then
                GoTo Siguiente
'                i = 1
'                ReDim sProv(1)
'                ReDim dblImpOfeEsc(1)
'                ReDim lEsc(1)
'                bMejor = False
'
'                For Each oProv In m_oProvesAsig
'                    If oGrupo.UsarEscalados Then
'                        If Not oGrupo.Escalados Is Nothing Then
'                            For Each oEsc In oGrupo.Escalados
'                                scod = KeyEscalado(oProv.Cod, oItem.Id, oEsc.Id)
'                                Set oAdj = oGrupo.Adjudicaciones.Item(scod)
'
'                                If oAdj Is Nothing Then
'                                    'No ha sido adjudicado a este prove.
'                                Else
'                                    bMejor = True
'                                    m_oAdjuds.Add oAdj.ProveCod, oAdj.NumOfe, oAdj.Id, oAdj.Precio, oAdj.Porcentaje, , , , , , , , , oAdj.Escalado, vCambio:=oAdj.Cambio
'                                End If
'                            Next
'                        End If
'                    Else
'                        scod = oProv.Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oProv.Cod))
'                        scod = CStr(oItem.Id) & scod
'
'                        Set oAdj = oGrupo.Adjudicaciones.Item(scod)
'
'                        If oAdj Is Nothing Then
'                            'No ha sido adjudicado a este prove.
'                        Else
'                            bMejor = True
'                            m_oAdjuds.Add oAdj.ProveCod, oAdj.NumOfe, oAdj.Id, oAdj.Precio, oAdj.Porcentaje, vCambio:=oAdj.Cambio
'                        End If
'                    End If
'                Next
            ElseIf oItem.Confirmado Then
                If m_oProcesoSeleccionado.Estado < conadjudicaciones And m_oProcesoSeleccionado.Pedidos > PPSinPedidos Then
                    If EstaItemEnPedidos(oGrupo.Codigo, oItem.Id, m_oProcesoSeleccionado, m_oProvesAsig, m_iInd, m_vEnPedidos, m_oAdjuds) Then GoTo Siguiente
                End If
                ReDim sProv(1)
                ReDim dblImpOfeEsc(1)
                ReDim lEsc(1)
                bMejor = True
                
                'Es el precio de la oferta
                For Each oProv In m_oProvesAsig
                    Set oOferta = oGrupo.UltimasOfertas.Item(oProv.Cod)
                    If Not oOferta Is Nothing Then
                        If Not oOferta.Lineas Is Nothing Then
                            If oGrupo.UsarEscalados Then
                                If Not oOferta.Lineas.Item(CStr(oItem.Id)).Escalados Is Nothing Then
                                    If oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Count > 0 Then
                                        dblImporteEsc = ImporteOptimoItemEsc(oProv.Cod, oItem, oOferta, lIdEscalado, m_oProcesoSeleccionado, m_oAtribsFormulas, m_dblImporteAux)
                                        
                                        If lIdEscalado > 0 Then
                                            If sProv(1) = "" Then
                                                ReDim sProv(1)
                                                ReDim dblImpOfeEsc(1)
                                                ReDim lEsc(1)
                                                sProv(1) = oProv.Cod
                                                lEsc(1) = lIdEscalado
                                                dblImpOfeEsc(1) = dblImporteEsc
                                            Else
                                                If (NullToDbl0(dblImporteEsc) / oOferta.Cambio) < (NullToDbl0(dblImpOfeEsc(1)) / oGrupo.UltimasOfertas.Item(sProv(1)).Cambio) And sProv(1) <> oProv.Cod Then
                                                    ReDim sProv(1)
                                                    ReDim dblImpOfeEsc(1)
                                                    ReDim lEsc(1)
                                                    sProv(1) = oProv.Cod
                                                    lEsc(1) = lIdEscalado
                                                    dblImpOfeEsc(1) = dblImporteEsc
                                                    bMejor = True
                                                ElseIf (FormateoNumerico(NullToDbl0(dblImporteEsc) / oOferta.Cambio)) = (FormateoNumerico(NullToDbl0(dblImpOfeEsc(1)) / oGrupo.UltimasOfertas.Item(sProv(1)).Cambio)) Then
                                                    'Si hay proveedores empatados se omitir� y se adjudica al primero de ellos o al de cantidad/rango m�s alto
                                                    If sItemEscEmpatados(1) = "" Then
                                                        sItemEscEmpatados(1) = oItem.ArticuloCod & " " & oItem.Descr
                                                    Else
                                                        ReDim Preserve sItemEscEmpatados(UBound(sItemEscEmpatados) + 1)
                                                        sItemEscEmpatados(UBound(sItemEscEmpatados)) = oItem.Id & " " & oItem.Descr
                                                    End If
                                                    
                                                    If oGrupo.Escalados.Item(CStr(lIdEscalado)).Inicial > oGrupo.Escalados.Item(CStr(lEsc(1))).Inicial Then
                                                        ReDim sProv(1)
                                                        ReDim lEsc(1)
                                                        sProv(1) = oProv.Cod
                                                        lEsc(1) = lIdEscalado
                                                        bMejor = True
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                                If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).importe) Then
                                    If sProv(1) = "" Then
                                        ReDim sProv(1)
                                        sProv(1) = oProv.Cod
                                    Else
                                        If Not oOferta.Lineas.Item(CStr(oItem.Id)) Is Nothing Then
                                            If (NullToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).importe) / oOferta.Cambio) < (NullToDbl0(oGrupo.UltimasOfertas.Item(sProv(1)).Lineas.Item(CStr(oItem.Id)).importe) / oGrupo.UltimasOfertas.Item(sProv(1)).Cambio) And sProv(1) <> oProv.Cod Then
                                                ReDim sProv(1)
                                                sProv(1) = oProv.Cod
                                                bMejor = True
                                            ElseIf (FormateoNumerico(NullToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).importe) / oOferta.Cambio)) = (FormateoNumerico(NullToDbl0(oGrupo.UltimasOfertas.Item(sProv(1)).Lineas.Item(CStr(oItem.Id)).importe) / oGrupo.UltimasOfertas.Item(sProv(1)).Cambio)) Then
                                                j = UBound(sProv) + 1
                                                ReDim Preserve sProv(j)
                                                sProv(j) = oProv.Cod
                                                bMejor = False
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                    Set oOferta = Nothing
                Next
                                
                'Ahora adjudica el item a los proveedores correspondientes
                If bMejor Then
                    'Adjudica todo lo que se pueda al proveedor
                    Set oOferta = oGrupo.UltimasOfertas.Item(sProv(1))
                    If Not oOferta Is Nothing Then
                        If oGrupo.UsarEscalados Then
                            If Not oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(lEsc(1))) Is Nothing Then
                                If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(lEsc(1))).PrecioOferta) Then
                                    sCod = sProv(1) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(1)))
                                    sCod = CStr(oItem.Id) & sCod & CStr(lEsc(1))
                                    
                                    m_oAdjuds.Add sProv(1), oOferta.Num, oItem.Id, oOferta.Lineas.Item(CStr(oItem.Id)).Escalados.Item(CStr(lEsc(1))).PrecioOferta, 100, , , , , , , , , lEsc(1), vCambio:=oOferta.Cambio
                                End If
                            End If
                        Else
                            If oOferta.Lineas.Item(CStr(oItem.Id)).CantidadMaxima = 0 _
                            Or IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).CantidadMaxima) Then
                                'No tiene cantidad m�xima.Se adjudica todo al proveedor
                                dporcentaje = 100
                                
                            Else   'Tiene cantidad m�xima
                                If oOferta.Lineas.Item(CStr(oItem.Id)).CantidadMaxima >= oItem.Cantidad Then
                                    dporcentaje = 100
                                Else
                                    dporcentaje = (oOferta.Lineas.Item(CStr(oItem.Id)).CantidadMaxima / oItem.Cantidad) * 100
                                    'Saca un mensaje indicando que no se ha podido adjudicar toda la cantidad del item
                                    sMensaje = sMensaje & oItem.ArticuloCod & " " & oItem.Descr & "   " & sCantSinAdj & (oItem.Cantidad - oOferta.Lineas.Item(CStr(oItem.Id)).CantidadMaxima) & vbCrLf
                                End If
                            End If
                            
                            If Not IsNull(oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta) Then
                                sCod = CStr(oItem.Id) & sProv(1) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(1)))
                                If m_oAdjuds.Item(sCod) Is Nothing Then
                                    If dporcentaje <> 0 Then
                                        m_oAdjuds.Add sProv(1), oOferta.Num, oItem.Id, oOferta.Lineas.Item(CStr(oItem.Id)).PrecioOferta, dporcentaje, vCambio:=oOferta.Cambio
                                    End If
                                Else
                                    If dporcentaje <> 0 Then
                                        m_oAdjuds.Item(sCod).Porcentaje = dporcentaje
                                    Else
                                        m_oAdjuds.Remove (sCod)
                                    End If
                                End If
                            End If
                        End If
                    End If
                    Set oOferta = Nothing
                    ReDim sProv(1)
    
                Else
                    'Adjudica todo el proceso en cantidades proporcionales a los proveedores empatados
                    j = UBound(sProv)
                    dResto = NullToDbl0(oItem.Cantidad)
                    dCantAdj = ParteEntera(NullToDbl0(oItem.Cantidad) / j)
                    dCapacidad = 0
                    If m_oProcesoSeleccionado.SolicitarCantMax = False Then
                        dUltimoCantAdj = NullToDbl0(oItem.Cantidad) - ((j - 1) * dCantAdj)
                    End If
                    
                    While dResto > 0
                        For iProv = 1 To j
                            If dResto <= 0 Then Exit For
                            Set oOferta = oGrupo.UltimasOfertas.Item(sProv(iProv))
                            If Not oOferta Is Nothing Then
                                dporcentaje = 0  'Porcentaje a adjudicar
                                sCod = CStr(oItem.Id) & sProv(iProv) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(iProv)))
                                'Cantidad que ya est� adjudicada para ese item y proveedor:
                                dadj = 0
                                If Not m_oAdjuds.Item(sCod) Is Nothing Then
                                    dadj = (oItem.Cantidad * m_oAdjuds.Item(sCod).Porcentaje) / 100
                                End If
                                
                                If m_oProcesoSeleccionado.SolicitarCantMax = False Then
                                    'Si no hay cantidad m�xima
                                    dCapacidad = Null
                                    If iProv = j Then
                                        dporcentaje = (dUltimoCantAdj / oItem.Cantidad) * 100
                                        dResto = dResto - dUltimoCantAdj
                                    Else
                                        dporcentaje = (dCantAdj / oItem.Cantidad) * 100
                                        dResto = dResto - dCantAdj
                                    End If
                                Else
                                    'Hay cantidad m�xima:
                                    'Cantidad m�xima del proveedor
                                    dCantMax = NullToDbl0(oOferta.Lineas.Item(CStr(oItem.Id)).CantidadMaxima)
                                    If dCantMax = 0 Then
                                        dporcentaje = (dCantAdj / oItem.Cantidad) * 100
                                        dResto = dResto - dCantAdj
                                    Else
                                        If dCantMax > dadj Then
                                            If dCantMax - dadj >= dCantAdj Then
                                                dporcentaje = (dCantAdj / oItem.Cantidad) * 100
                                                dResto = dResto - dCantAdj
                                            Else
                                                dResto = dResto - dCantMax - dadj
                                                dporcentaje = (dCantMax / oItem.Cantidad) * 100
                                            End If
                                        End If
                                    End If
                                End If
                                
                                'Lo a�ade a la colecci�n
                                If dporcentaje > 0 Then
                                    If m_oAdjuds.Item(sCod) Is Nothing Then
                                        m_oAdjuds.Add sProv(iProv), oGrupo.UltimasOfertas.Item(sProv(iProv)).Num, oItem.Id, oGrupo.UltimasOfertas.Item(sProv(iProv)).Lineas.Item(CStr(oItem.Id)).PrecioOferta, dporcentaje, vCambio:=oOferta.Cambio
                                    Else
                                        m_oAdjuds.Item(sCod).Porcentaje = m_oAdjuds.Item(sCod).Porcentaje + dporcentaje
                                    End If
                                End If
                            End If
                            Set oOferta = Nothing
                        Next
                        
                        'Si no se ha adjudicado toda la cantidad se distribuye otra vez en partes proporcionales
                        If dResto > 0 And Not IsNull(dCapacidad) Then
                            j = 0
                            dCapacidad = 0
                            For iProv = 1 To UBound(sProv)
                                sCod = CStr(oItem.Id) & sProv(iProv) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(sProv(iProv)))
                                dCantMax = NullToDbl0(oGrupo.UltimasOfertas.Item(sProv(iProv)).Lineas.Item(CStr(oItem.Id)).CantidadMaxima)
                                If Not m_oAdjuds.Item(sCod) Is Nothing Then
                                    dadj = (oItem.Cantidad * m_oAdjuds.Item(sCod).Porcentaje) / 100
                                Else
                                    dadj = 0
                                End If
                                If Not IsNull(dCantMax) And dCantMax <> 0 Then
                                    'Si tiene cantidad m�xima
                                    If dCantMax > dadj Then
                                        j = j + 1
                                        ReDim Preserve sProv2(j)
                                        sProv2(j) = sProv(iProv)
                                        dCapacidad = dCapacidad + dCantMax - dadj
                                    End If
                                Else
                                    'No tiene cantidad m�xima
                                    j = j + 1
                                    ReDim Preserve sProv2(j)
                                    sProv2(j) = sProv(iProv)
                                    dCapacidad = Null
                                End If
                            Next
                            
                            If j > 0 Then
                                ReDim sProv(UBound(sProv2))
                                For i = 1 To UBound(sProv2)
                                    sProv(i) = sProv2(i)
                                Next i
                                ReDim sProv2(0)
                                dCantAdj = ParteEntera(dResto / j)
                                If dCantAdj = 0 Then
                                    dCantAdj = 1
                                End If
                            Else
                                dCapacidad = Null
                                'Saca un mensaje indicando que no se ha podido adjudicar toda la cantidad del item
                                sMensaje = sMensaje & oItem.ArticuloCod & "  " & oItem.Descr & "   " & sCantSinAdj & dResto & vbCrLf
                                dResto = 0
                            End If
                        End If
                    Wend
                    
                    ReDim sProv(1)
                End If
            End If
Siguiente:
        Next
        Set oGrupo.Adjudicaciones = Nothing
        Set oGrupo.Adjudicaciones = m_oAdjuds
    Next
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      oFSGSRaiz.TratarError "M�dulo", "basComparativa", "OptimaPrecio", err, Erl
      Exit Sub
   End If
End Sub

''' <summary>Se eliminan las adjs solo de items abiertos para el calculo de las optimas</summary>
''' <remarks>Llamada desde:frmADJ frmRESREU CalcularOptimaItemGrupo</remarks>
Public Function EliminarAdjudicacionesPC(ByRef oGrupo As CGrupo) As CAdjudicaciones
Dim oAdjuds  As CAdjudicaciones
Dim oAdj As CAdjudicacion
Dim sCod As String

    Set oAdjuds = oGrupo.Adjudicaciones
    For Each oAdj In oAdjuds
        If Not oGrupo.Items.Item(CStr(oAdj.Id)).Cerrado Then
            If oGrupo.UsarEscalados Then
                sCod = oAdj.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdj.ProveCod))
                sCod = CStr(oAdj.Id) & sCod & CStr(oAdj.Escalado)
            Else
                sCod = CStr(oAdj.Id) & oAdj.ProveCod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(oAdj.ProveCod))
            End If
            oAdjuds.Remove sCod
        End If
    Next
    Set EliminarAdjudicacionesPC = oAdjuds
End Function
