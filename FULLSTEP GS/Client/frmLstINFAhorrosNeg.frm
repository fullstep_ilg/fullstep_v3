VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmLstINFAhorrosNeg 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Ahorros Negociados (Opciones)"
   ClientHeight    =   3120
   ClientLeft      =   270
   ClientTop       =   1530
   ClientWidth     =   8160
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLstINFAhorrosNeg.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MouseIcon       =   "frmLstINFAhorrosNeg.frx":0CB2
   ScaleHeight     =   3120
   ScaleWidth      =   8160
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   0
      ScaleHeight     =   375
      ScaleWidth      =   8160
      TabIndex        =   11
      Top             =   2745
      Width           =   8160
      Begin VB.CommandButton cmdObtener 
         Caption         =   "Obtener"
         Height          =   375
         Left            =   6705
         TabIndex        =   10
         Top             =   0
         Width           =   1410
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2715
      Left            =   15
      TabIndex        =   12
      Top             =   0
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   4789
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Selecci�n"
      TabPicture(0)   =   "frmLstINFAhorrosNeg.frx":1964
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame2"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Timer1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Opciones"
      TabPicture(1)   =   "frmLstINFAhorrosNeg.frx":1980
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).Control(1)=   "Frame3"
      Tab(1).ControlCount=   2
      Begin VB.Frame Frame3 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   750
         Left            =   -74820
         TabIndex        =   19
         Top             =   435
         Width           =   7875
         Begin VB.OptionButton optReu 
            Caption         =   "Adj. en reuni�n"
            Height          =   195
            Left            =   135
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   315
            Value           =   -1  'True
            Width           =   2325
         End
         Begin VB.OptionButton optTodos 
            Caption         =   "Todos"
            Height          =   195
            Left            =   5940
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   315
            Width           =   1665
         End
         Begin VB.OptionButton optDir 
            Caption         =   "Adj. directa"
            Height          =   195
            Left            =   3135
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   315
            Width           =   2130
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1350
         Left            =   -74820
         TabIndex        =   20
         Top             =   1200
         Width           =   7875
         Begin VB.OptionButton opDatGraf 
            Caption         =   "Ver datos y gr�fico"
            Height          =   195
            Left            =   4995
            TabIndex        =   32
            Top             =   870
            Width           =   2775
         End
         Begin VB.OptionButton opVerGraf 
            Caption         =   "Ver solo gr�fico"
            Height          =   195
            Left            =   2520
            TabIndex        =   31
            Top             =   870
            Width           =   2775
         End
         Begin VB.OptionButton opVerDat 
            Caption         =   "Ver solo datos"
            Height          =   195
            Left            =   45
            TabIndex        =   30
            Top             =   870
            Value           =   -1  'True
            Width           =   2775
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcNivel 
            Height          =   285
            Left            =   3525
            TabIndex        =   9
            Top             =   315
            Width           =   1740
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "NIVEL"
            Columns(0).Name =   "NIVEL"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Visible=   0   'False
            Columns(1).Caption=   "VALOR"
            Columns(1).Name =   "VALOR"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3069
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblNivel 
            Alignment       =   1  'Right Justify
            Caption         =   "Desglosar el listado hasta el nivel:"
            Height          =   240
            Left            =   105
            TabIndex        =   29
            Top             =   330
            Width           =   3255
         End
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   2000
         Left            =   3780
         Top             =   75
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2070
         Left            =   180
         TabIndex        =   13
         Top             =   420
         Width           =   7800
         Begin VB.CommandButton cmdBorrar 
            Height          =   315
            Left            =   4890
            Picture         =   "frmLstINFAhorrosNeg.frx":199C
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   1145
            Width           =   345
         End
         Begin VB.TextBox txtEstMat 
            BackColor       =   &H80000018&
            Height          =   285
            Left            =   1185
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   1160
            Width           =   3615
         End
         Begin VB.CommandButton cmdSelMat 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   5300
            Picture         =   "frmLstINFAhorrosNeg.frx":1A41
            Style           =   1  'Graphical
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   1145
            Width           =   345
         End
         Begin VB.TextBox txtFecHasta 
            Height          =   285
            Left            =   4035
            TabIndex        =   2
            Top             =   300
            Width           =   1125
         End
         Begin VB.CommandButton cmdCalFecApeHasta 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5340
            Picture         =   "frmLstINFAhorrosNeg.frx":1AAD
            Style           =   1  'Graphical
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   300
            Width           =   315
         End
         Begin VB.TextBox txtFecDesde 
            Height          =   285
            Left            =   1185
            TabIndex        =   0
            Top             =   300
            Width           =   1140
         End
         Begin VB.CommandButton cmdCalFecApeDesde 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3075
            Picture         =   "frmLstINFAhorrosNeg.frx":2037
            Style           =   1  'Graphical
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   300
            Width           =   315
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcMon 
            Height          =   285
            Left            =   1185
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   735
            Width           =   1140
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1164
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3651
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   1879
            Columns(2).Caption=   "Equivalencia"
            Columns(2).Name =   "EQUIV"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   2011
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpDen 
            Height          =   285
            Left            =   2115
            TabIndex        =   5
            Top             =   1590
            Width           =   2685
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   6165
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "DEN"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   1640
            Columns(1).Caption=   "Cod"
            Columns(1).Name =   "COD"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4736
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcEqpCod 
            Height          =   285
            Left            =   1185
            TabIndex        =   4
            Top             =   1590
            Width           =   825
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            GroupHeaders    =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1296
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "COD"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5345
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "DEN"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1455
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 3"
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcFecReu 
            Height          =   285
            Left            =   1185
            TabIndex        =   1
            Top             =   300
            Width           =   1845
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Fecha"
            Columns(0).Name =   "FECHA"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Fecha"
            Columns(1).Name =   "FECHACORTA"
            Columns(1).DataField=   "Column 2"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   5556
            Columns(2).Caption=   "Referencia"
            Columns(2).Name =   "REF"
            Columns(2).DataField=   "Column 1"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   3254
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiDen 
            Height          =   285
            Left            =   3120
            TabIndex        =   35
            Top             =   1590
            Width           =   4005
            DataFieldList   =   "Column 0"
            _Version        =   196617
            DataMode        =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   5900
            Columns(0).Caption=   "Denominaci�n"
            Columns(0).Name =   "Denominaci�n"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2408
            Columns(1).Caption=   "C�d."
            Columns(1).Name =   "C�d."
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   7064
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin SSDataWidgets_B.SSDBCombo sdbcArtiCod 
            Height          =   285
            Left            =   1200
            TabIndex        =   34
            Top             =   1590
            Width           =   1890
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            ListAutoValidate=   0   'False
            ListAutoPosition=   0   'False
            _Version        =   196617
            DataMode        =   2
            Cols            =   2
            TagVariant      =   ""
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   2408
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5900
            Columns(1).Caption=   "Denominaci�n"
            Columns(1).Name =   "Denominaci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   3334
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblArticulo 
            Caption         =   "Art�culo:"
            Height          =   210
            Left            =   45
            TabIndex        =   36
            Top             =   1665
            Width           =   795
         End
         Begin VB.Label lblEqp 
            BackColor       =   &H80000018&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Label1"
            Height          =   285
            Left            =   1185
            TabIndex        =   18
            Top             =   1590
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.Label lblEstMat 
            Caption         =   "Material:"
            Height          =   240
            Left            =   60
            TabIndex        =   28
            Top             =   1185
            Width           =   1095
         End
         Begin VB.Label lblEqpCod 
            Caption         =   "Equipo"
            Height          =   195
            Left            =   60
            TabIndex        =   27
            Top             =   1635
            Width           =   1095
         End
         Begin VB.Label lblMon 
            Caption         =   "Moneda:"
            Height          =   240
            Left            =   60
            TabIndex        =   26
            Top             =   750
            Width           =   1095
         End
         Begin VB.Label lblFecDesde 
            Caption         =   "Desde:"
            Height          =   255
            Left            =   60
            TabIndex        =   25
            Top             =   315
            Width           =   1095
         End
         Begin VB.Label lblFecHasta 
            Caption         =   "Hasta:"
            Height          =   225
            Left            =   3225
            TabIndex        =   24
            Top             =   330
            Width           =   720
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1875
         Left            =   -74895
         TabIndex        =   21
         Top             =   465
         Width           =   5895
         Begin VB.OptionButton opOrdDen 
            Caption         =   "Denominaci�n"
            Height          =   195
            Left            =   360
            TabIndex        =   23
            Top             =   1065
            Width           =   1515
         End
         Begin VB.OptionButton opOrdCod 
            Caption         =   "C�digo"
            Height          =   195
            Left            =   360
            TabIndex        =   22
            Top             =   720
            Value           =   -1  'True
            Width           =   1515
         End
      End
   End
End
Attribute VB_Name = "frmLstINFAhorrosNeg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Variables de restricciones
Public bRMat As Boolean      ' material asociado
Public bREqpAsig As Boolean         ' equipo comprador
Public bRUO As Boolean
Private oICompAsignado As ICompProveAsignados

'Vbles. para la selecci�n de material
Public sGMN1Cod As String
Public sGMN2Cod As String
Public sGMN3Cod As String
Public sGMN4Cod As String

'Fechas y desglose
Private dFechaDesde As Variant
Private dFechaHasta As Variant
Private iNivel As Integer

' Origen llamada a formulario
Public sOrigen As String

Public sFiltro As String

'Variables para func. combos
Private bRespetarCombo As Boolean
Private bCargarComboDesde As Boolean
Private bEqpRespetarCombo As Boolean
Private bEqpCargarComboDesde As Boolean
Private bMonRespetarCombo As Boolean
Private bMonCargarComboDesde As Boolean

Private oGestorReuniones As CGestorReuniones
Private oReuniones As CReuniones
Private oMonedas As CMonedas
Private dequivalencia As Double
Private sMonedaOrig As String
Private sMoneda As String
Private oEqpSeleccionado As CEquipo
Private oEqps As CEquipos
Private oGMN1Seleccionado As CGrupoMatNivel1
Private oGMN2Seleccionado As CGrupoMatNivel2
Private oGMN3Seleccionado As CGrupoMatNivel3
Private oGMN4Seleccionado As CGrupoMatNivel4

'Vbles. para la selecci�n de concepto3
Private sConcep3_1 As String
Private sConcep3_2 As String
Private sConcep3_3 As String
Private sConcep3_4 As String
'Vbles. para la selecci�n de concepto4
Private sConcep4_1 As String
Private sConcep4_2 As String
Private sConcep4_3 As String
Private sConcep4_4 As String
'Vbles. para la selecci�n de PRESUPUESTOS
Public m_sUON1 As String
Public m_sUON2 As String
Public m_sUON3 As String

'Multiidioma
Private sIdiNoValido(5) As String
Private sIdiTitulosFrm(12) As String
Private sIdiNiveles(9) As String
Private sIdiFecDesde(2) As String
Private sIdiTitulosRpt(14) As String
Private sIdiCriSeleccion As String
Private sIdiFechas As String
Private sIdiFecReu As String
Private sIdiFechaDesde As String
Private sIdiCriEquipo As String
Private sIdiCriSoloReu As String
Private sIdiCriSoloAdj As String
Private sIdiCriReuYAdj As String
Private sIdiCriMoneda As String
Private sIdiCriMaterial As String
Private sIdiGenerando(4) As String
Private sIdiSeleccionando As String
Private sIdiVisualizando As String
Private srIdiGenFecha As String
Private srIdiGenPresupuesto As String
Private srIdiGenAdjudicado As String
Private srIdiGenAhorro As String
Private srIdiGenProceso As String
Private srIdiGenArticulo As String
Private srIdiGenDenomArt As String
Private srIdiGenPresupArt As String
Private srIdiGenAdjudicadoArt As String
Private srIdiGenAhorroArt As String
Private srIdiMatFecha As String
Private srIdiMatNumProc As String
Private srIdiMatPresupuesto As String
Private srIdiMatAdjudicado As String
Private srIdiMatAhorro As String
Private srIdiMatProceso As String
Private srIdiMatArticulo As String
Private srIdiMatDenomArt As String
Private srIdiEqpFecha As String
Private srIdiEqpPresupuesto As String
Private srIdiEqpAdjudicado As String
Private srIdiEqpAhorro As String
Private srIdiResFecha As String
Private srIdiResNumProc As String
Private srIdiResPresupuesto As String
Private srIdiResAdjudicado As String
Private srIdiResAhorro As String
Private srIdiResProceso As String
Private srIdiResArticulo As String
Private srIdiResDenomArt As String
Private srIdiPag As String
Private srIdiDe As String
Private srIdiLAdjudicado As String
Private srIdiLPresupuestado As String
Private srIdiLPositivo As String
Private srIdiLNegativo As String
Private srIdiEquipo As String
Private srIdiComprador As String
Private sIdiRaiz As String
Private sproveedor As String
Private sFechaInicio As String
Private sFechaFin As String
 
Private Sub ConfigurarSeguridad(sOrigen As String)

bREqpAsig = False
bRMat = False

Select Case sOrigen
    
    
    Case "A4B1C2D1", "frmInfAhorroNegMatReu", "A4B1C2D2", "frmInfAhorroNegMatDesde"
        'Material
        If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then

            If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegMatRestMat)) Is Nothing) Then
                bRMat = True
            End If
        End If
    Case "A4B1C3D1", "frmInfAhorroNegEqpResReu", "A4B1C3D2", "frmInfAhorroNegEqpResDesde"
        'Equipos responsables
        If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegEqpResRestComp)) Is Nothing) Then
            bREqpAsig = True
        End If
        End If
    Case "A4B1C4D1", "frmInfAhorroNegEqpReu", "A4B1C4D2", "frmInfAhorroNegEqpDesde"
        'Equipos negociadores
        If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
            If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegEqpRestComp)) Is Nothing) Then
                bREqpAsig = True
            End If
        End If
    Case "A4B1C5D1", "frmInfAhorroNegConcep3Reu", "A4B1C5D2", "frmInfAhorroNegConcep3Desde"
        'uo
        If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon3RestUO)) Is Nothing) Then
                bRUO = True
            End If
        End If
    Case "A4B1C6D1", "frmInfAhorroNegConcep4Reu", "A4B1C6D2", "frmInfAhorroNegConcep4Desde"
        'uo
        If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
            If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.InfNegCon4RestUO)) Is Nothing) Then
                bRUO = True
            End If
        End If
    
    End Select

End Sub

Private Sub cmdBorrar_Click()
    txtEstMat.Text = ""
    sConcep3_1 = ""
    sConcep3_2 = ""
    sConcep3_3 = ""
    sConcep3_4 = ""
    sConcep4_1 = ""
    sConcep4_2 = ""
    sConcep4_3 = ""
    sConcep4_4 = ""
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
    sdbcArtiCod.Text = ""
    sdbcArtiDen.Text = ""
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set oReuniones = Nothing
    Set oGestorReuniones = Nothing
    Set oEqpSeleccionado = Nothing
    Set oMonedas = Nothing
    Set oEqps = Nothing
    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing

End Sub

Private Sub sdbcFecReu_Change()
 
    If Not bRespetarCombo Then
        bCargarComboDesde = True
    End If
    
End Sub

Private Sub sdbcFecReu_CloseUp()
    
    If sdbcFecReu.Value = "" Then
        Exit Sub
    End If
    
    sdbcFecReu.Text = sdbcFecReu.Columns(1).Text
    bCargarComboDesde = False
    
End Sub

Private Sub sdbcFecReu_DropDown()

Dim oReu As CReunion

    
    sdbcFecReu.RemoveAll
        
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
        If IsDate(sdbcFecReu) Then
            Set oReuniones = oGestorReuniones.DevolverReuniones(sdbcFecReu, , , True)
        Else
            Set oReuniones = oGestorReuniones.DevolverReuniones(, , , True)
        End If
    Else
        Set oReuniones = oGestorReuniones.DevolverReuniones(, , , True)
    End If
    
    For Each oReu In oReuniones
        
        sdbcFecReu.AddItem oReu.Fecha & Chr(m_lSeparador) & Format(oReu.Fecha, "short date") & " " & Format(oReu.Fecha, "short time") & Chr(m_lSeparador) & oReu.Referencia
    Next
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcFecReu_InitColumnProps()
    
    sdbcFecReu.DataFieldList = "Column 0"
    sdbcFecReu.DataFieldToDisplay = "Column 1"
    
End Sub

Private Sub sdbcFecReu_PositionList(ByVal Text As String)
 ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcFecReu.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcFecReu.Rows - 1
            bm = sdbcFecReu.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcFecReu.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcFecReu.Bookmark = bm
                Exit For
            End If
        Next i
    End If
    
End Sub

Private Sub sdbcFecReu_Validate(Cancel As Boolean)

    If Trim(sdbcFecReu.Text = "") Then Exit Sub
    
    If Not IsDate(sdbcFecReu) Then
        oMensajes.NoValido sIdiNoValido(1)
        Exit Sub
    End If
    
    If sdbcFecReu.Text = sdbcFecReu.Columns(0).Text Then
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    Set oReuniones = oGestorReuniones.DevolverReuniones(sdbcFecReu.Columns(0).Text, sdbcFecReu.Columns(0).Text, , True)
    
    If oReuniones Is Nothing Then
        sdbcFecReu.Text = ""
        oMensajes.NoValido sIdiNoValido(2)
    End If
    
    Screen.MousePointer = vbNormal
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeDesde_Click()
  
    Set frmCalendar.frmDestination = Me
    If Right(sOrigen, 2) = "D1" Or Right(sOrigen, 3) = "Reu" Then
        Set frmCalendar.ctrDestination = sdbcFecReu
    Else
        Set frmCalendar.ctrDestination = txtFecDesde
    End If
    frmCalendar.addtotop = 900 + 360
    frmCalendar.addtoleft = 180
    
    If txtFecDesde <> "" And (Right(sOrigen, 2) = "D2" Or Right(sOrigen, 3) <> "Reu") Then
        frmCalendar.Calendar.Value = txtFecDesde
    Else
        If sdbcFecReu <> "" And (Right(sOrigen, 2) = "D1" Or Right(sOrigen, 3) = "Reu") Then
            frmCalendar.Calendar.Value = sdbcFecReu
        Else
            frmCalendar.Calendar.Value = Date
        End If
    End If
    
    frmCalendar.Show 1
    
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecApeHasta_Click()
    AbrirFormCalendar Me, txtFecHasta
End Sub

Private Sub Form_Load()

    ConfigurarSeguridad (sOrigen)

    Me.Height = 3525
    Me.Width = 8280
    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2
    Frame3.Top = 435
    Frame4.Top = 1200
        
    
    CargarRecursos
        
    PonerFieldSeparator Me
    
    sIdiRaiz = gParametrosGenerales.gsDEN_UON0
    Set oMonedas = oFSGSRaiz.Generar_CMonedas
    sdbcMon.Text = basParametros.gParametrosGenerales.gsMONCEN
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , , , True
    If oMonedas.Count = 0 Then
        oMensajes.NoValido sIdiNoValido(3)
    Else
        dequivalencia = oMonedas.Item(1).Equiv
        sMonedaOrig = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    End If
    
    sGMN1Cod = ""
    sGMN2Cod = ""
    sGMN3Cod = ""
    sGMN4Cod = ""
 
    
    If bREqpAsig Then
        sdbcEqpCod.Visible = False
        sdbcEqpDen.Visible = False
        lblEqp.Visible = True
        lblEqp.caption = basOptimizacion.gCodEqpUsuario & " " & oUsuarioSummit.comprador.DenEqp
        sdbcEqpCod.Text = basOptimizacion.gCodEqpUsuario
        Set oEqpSeleccionado = Nothing
        Set oEqpSeleccionado = oFSGSRaiz.generar_CEquipo
        oEqpSeleccionado.Cod = basOptimizacion.gCodEqpUsuario
   End If
       
   Select Case sOrigen
        
       Case "A4B1C1D1", "frmInfAhorroNegReu"   ' General en reunion
            Me.caption = sIdiTitulosFrm(1)
            Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
            lblEqpCod.Visible = False
            lblEstMat.Visible = False
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblFecDesde.caption = sIdiFecDesde(2)
            sdbcFecReu.Enabled = True: sdbcFecReu.Visible = True
            txtFecDesde.Enabled = False: txtFecDesde.Visible = False
            lblFecHasta.Visible = False
            txtFecHasta.Enabled = False: txtFecHasta.Visible = False
            cmdCalFecApeHasta.Enabled = False: cmdCalFecApeHasta.Visible = False
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            cmdSelMat.Visible = False
            cmdBorrar.Visible = False
            txtEstMat.Visible = False
            Frame4.Top = 725
            Frame3.Visible = False
            optDir.Visible = False
            optTodos.Visible = False
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = sIdiNiveles(2)
            
        Case "A4B1C1D2", "frmInfAhorroNegDesde"   ' General desde/hasta            Me.Caption = "Listado de Ahorros Negociados - General - En reuni�n (opciones)"
            Me.caption = sIdiTitulosFrm(2)
            lblEqpCod.Visible = False
            lblEstMat.Visible = False
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblFecDesde.caption = sIdiFecDesde(1)
            sdbcFecReu.Enabled = False: sdbcFecReu.Visible = False
            cmdCalFecApeDesde.Left = Me.txtFecDesde.Left + Me.txtFecDesde.Width + 180
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            cmdSelMat.Visible = False
            cmdBorrar.Visible = False
            txtEstMat.Visible = False
            'Desgloses
            Frame3.Visible = True
            sdbcNivel.AddItem sIdiNiveles(1) & Chr(m_lSeparador) & 0
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = sIdiNiveles(1)
            
       Case "A4B1C2D1", "frmInfAhorroNegMatReu"   ' Por material en reunion
            Me.caption = sIdiTitulosFrm(3)
            'sstab1.TabVisible(1) = True
            Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblEqpCod.Visible = False
            lblFecDesde.caption = sIdiFecDesde(2)
            sdbcFecReu.Enabled = True: sdbcFecReu.Visible = True
            txtFecDesde.Enabled = False: txtFecDesde.Visible = False
            lblFecHasta.Visible = False
            txtFecHasta.Enabled = False: txtFecHasta.Visible = False
            cmdCalFecApeHasta.Enabled = False: cmdCalFecApeHasta.Visible = False
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            Frame4.Top = 725
            Frame3.Visible = False
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(6) & Chr(m_lSeparador) & 3
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = sIdiNiveles(6)
            
        Case "A4B1C2D2", "frmInfAhorroNegMatDesde"   ' Por material desde/hasta            Me.Caption = "Listado de Ahorros Negociados - General - En reuni�n (opciones)"
            Me.caption = sIdiTitulosFrm(4)
            lblEqpCod.Visible = False
            lblFecDesde.caption = sIdiFecDesde(1)
            sdbcFecReu.Enabled = False: sdbcFecReu.Visible = False
            cmdCalFecApeDesde.Left = Me.txtFecDesde.Left + Me.txtFecDesde.Width + 180
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            Frame3.Visible = True
            If sOrigen = "A4B1C2D2" Then
                lblArticulo.Visible = True
                sdbcArtiCod.Visible = True
                sdbcArtiDen.Visible = True
                sdbcNivel.AddItem sIdiNiveles(1) & Chr(m_lSeparador) & 0 'fecha
                sdbcNivel.AddItem sIdiNiveles(6) & Chr(m_lSeparador) & 3 'material
                sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4 'proceso
                sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
                sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6 'item
                sdbcNivel.Text = sIdiNiveles(1)
            Else
                Select Case frmInfAhorroNegMatDesde.sdbcFiltro.Text
                Case frmInfAhorroNegMatDesde.sArticulo
                    lblArticulo.Visible = True
                    sdbcArtiCod.Visible = True
                    sdbcArtiDen.Visible = True
                    sdbcNivel.AddItem sIdiNiveles(8) & Chr(m_lSeparador) & 0 'articulo
                    sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 1 'proceso
                    sdbcNivel.AddItem sIdiNiveles(9) & Chr(m_lSeparador) & 2 'proveedor
                    sdbcNivel.Text = sIdiNiveles(8)
                Case frmInfAhorroNegMatDesde.sFechaFiltro
                    lblArticulo.Visible = False
                    sdbcArtiCod.Visible = False
                    sdbcArtiDen.Visible = False
                    sdbcNivel.AddItem sIdiNiveles(1) & Chr(m_lSeparador) & 0 'fecha
                    sdbcNivel.AddItem sIdiNiveles(6) & Chr(m_lSeparador) & 3 'material
                    sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4 'proceso
                    sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
                    sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6 'item
                    sdbcNivel.Text = sIdiNiveles(1)
                Case frmInfAhorroNegMatDesde.sMaterial
                    lblArticulo.Visible = False
                    sdbcArtiCod.Visible = False
                    sdbcArtiDen.Visible = False
                    sdbcNivel.AddItem sIdiNiveles(6) & Chr(m_lSeparador) & 3 'material
                    sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4 'proceso
                    sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
                    sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6 'item
                    sdbcNivel.Text = sIdiNiveles(6)
                End Select
            End If
        Case "A4B1C3D1", "frmInfAhorroNegEqpResReu"   ' Por Eqp responsables en reunion
            Me.caption = sIdiTitulosFrm(5)
            cmdSelMat.Visible = False
            cmdBorrar.Visible = False
            txtEstMat.Visible = False
            lblEstMat.Visible = False
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
            lblFecDesde.caption = sIdiFecDesde(2)
            sdbcFecReu.Enabled = True: sdbcFecReu.Visible = True
            txtFecDesde.Enabled = False: txtFecDesde.Visible = False
            lblFecHasta.Visible = False
            txtFecHasta.Enabled = False: txtFecHasta.Visible = False
            cmdCalFecApeHasta.Enabled = False: cmdCalFecApeHasta.Visible = False
            Frame4.Top = 725
            Frame3.Visible = False
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(4) & Chr(m_lSeparador) & 1
            sdbcNivel.AddItem sIdiNiveles(5) & Chr(m_lSeparador) & 2
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = sIdiNiveles(4)
            sdbcEqpCod.Top = txtEstMat.Top
            sdbcEqpDen.Top = txtEstMat.Top
            lblEqpCod.Top = txtEstMat.Top
            
        Case "A4B1C3D2", "frmInfAhorroNegEqpResDesde"   ' Por Eqp responsables desde/hasta            Me.Caption = "Listado de Ahorros Negociados - General - En reuni�n (opciones)"
            Me.caption = sIdiTitulosFrm(6)
            lblFecDesde.caption = sIdiFecDesde(1)
            sdbcFecReu.Enabled = False: sdbcFecReu.Visible = False
            cmdCalFecApeDesde.Left = Me.txtFecDesde.Left + Me.txtFecDesde.Width + 180
            cmdSelMat.Visible = False
            cmdBorrar.Visible = False
            txtEstMat.Visible = False
            lblEstMat.Visible = False
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            Frame3.Visible = True
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(1) & Chr(m_lSeparador) & 0
            sdbcNivel.AddItem sIdiNiveles(4) & Chr(m_lSeparador) & 1
            sdbcNivel.AddItem sIdiNiveles(5) & Chr(m_lSeparador) & 2
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = sIdiNiveles(1)
            sdbcEqpCod.Top = txtEstMat.Top
            sdbcEqpDen.Top = txtEstMat.Top
            lblEqpCod.Top = txtEstMat.Top
            
        Case "A4B1C4D1", "frmInfAhorroNegEqpReu"   ' Por Eqp negociadores en reunion
            Me.caption = sIdiTitulosFrm(7)
            Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
            lblEstMat.Visible = False
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblFecDesde.caption = sIdiFecDesde(2)
            sdbcFecReu.Enabled = True: sdbcFecReu.Visible = True
            txtFecDesde.Enabled = False: txtFecDesde.Visible = False
            lblFecHasta.Visible = False
            txtFecHasta.Enabled = False: txtFecHasta.Visible = False
            cmdCalFecApeHasta.Enabled = False: cmdCalFecApeHasta.Visible = False
            Frame4.Top = 725
            Frame3.Visible = False
            cmdSelMat.Visible = False
            cmdBorrar.Visible = False
            txtEstMat.Visible = False
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(4) & Chr(m_lSeparador) & 1
            sdbcNivel.AddItem sIdiNiveles(5) & Chr(m_lSeparador) & 2
            sdbcNivel.Text = sIdiNiveles(4)
            sdbcEqpCod.Top = txtEstMat.Top
            sdbcEqpDen.Top = txtEstMat.Top
            lblEqpCod.Top = txtEstMat.Top

        Case "A4B1C4D2", "frmInfAhorroNegEqpDesde"   ' Por Eqp negociadores desde/hasta            Me.Caption = "Listado de Ahorros Negociados - General - En reuni�n (opciones)"
            Me.caption = sIdiTitulosFrm(8)
            lblFecDesde.caption = sIdiFecDesde(1)
            lblEstMat.Visible = False
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            sdbcFecReu.Enabled = False: sdbcFecReu.Visible = False
            cmdCalFecApeDesde.Left = Me.txtFecDesde.Left + Me.txtFecDesde.Width + 180
            cmdSelMat.Visible = False
            cmdBorrar.Visible = False
            txtEstMat.Visible = False
            Frame3.Visible = True
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(1) & Chr(m_lSeparador) & 0
            sdbcNivel.AddItem sIdiNiveles(4) & Chr(m_lSeparador) & 1
            sdbcNivel.AddItem sIdiNiveles(5) & Chr(m_lSeparador) & 2
            sdbcNivel.Text = sIdiNiveles(1)
            sdbcEqpCod.Top = txtEstMat.Top
            sdbcEqpDen.Top = txtEstMat.Top
            lblEqpCod.Top = txtEstMat.Top
            
        Case "A4B1C5D2", "frmInfAhorroNegConcep3Desde" 'por concepto 3 desde
            Me.caption = sIdiTitulosFrm(9)
            lblEqpCod.Visible = False
            lblEstMat.caption = gParametrosGenerales.gsSingPres3 & ":"
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblFecDesde.caption = sIdiFecDesde(1)
            sdbcFecReu.Enabled = False: sdbcFecReu.Visible = False
            cmdCalFecApeDesde.Left = Me.txtFecDesde.Left + Me.txtFecDesde.Width + 180
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            Frame3.Visible = True
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(1) & Chr(m_lSeparador) & 0
            sdbcNivel.AddItem gParametrosGenerales.gsSingPres3 & Chr(m_lSeparador) & 3
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = sIdiNiveles(1)
            
        Case "A4B1C5D1", "frmInfAhorroNegConcep3Reu"  'por concepto 3 en reuni�n
            Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
            Me.caption = sIdiTitulosFrm(10)
            lblEqpCod.Visible = False
            Me.lblEstMat.caption = gParametrosGenerales.gsSingPres3 & ":"
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblFecDesde.caption = sIdiFecDesde(2)
            sdbcFecReu.Enabled = True: sdbcFecReu.Visible = True
            txtFecDesde.Enabled = False: txtFecDesde.Visible = False
            lblFecHasta.Visible = False
            txtFecHasta.Enabled = False: txtFecHasta.Visible = False
            cmdCalFecApeHasta.Enabled = False: cmdCalFecApeHasta.Visible = False
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            Frame4.Top = 725
            Frame3.Visible = False
            'Desgloses
            sdbcNivel.AddItem gParametrosGenerales.gsSingPres3 & Chr(m_lSeparador) & 3
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = gParametrosGenerales.gsSingPres3
            
        Case "A4B1C6D2", "frmInfAhorroNegConcep4Desde" 'por concepto 4 desde
            Me.caption = sIdiTitulosFrm(11)
            lblEqpCod.Visible = False
            lblEstMat.caption = gParametrosGenerales.gsSingPres4 & ":"
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblFecDesde.caption = sIdiFecDesde(1)
            sdbcFecReu.Enabled = False: sdbcFecReu.Visible = False
            cmdCalFecApeDesde.Left = Me.txtFecDesde.Left + Me.txtFecDesde.Width + 180
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            Frame3.Visible = True
            'Desgloses
            sdbcNivel.AddItem sIdiNiveles(1) & Chr(m_lSeparador) & 0
            sdbcNivel.AddItem gParametrosGenerales.gsSingPres4 & Chr(m_lSeparador) & 3
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = sIdiNiveles(1)
            
        Case "A4B1C6D1", "frmInfAhorroNegConcep4Reu" 'por concepto 4 en reuni�n
            Set oGestorReuniones = oFSGSRaiz.Generar_CGestorReuniones
            Me.caption = sIdiTitulosFrm(12)
            lblEqpCod.Visible = False
            Me.lblEstMat.caption = gParametrosGenerales.gsSingPres4 & ":"
            lblArticulo.Visible = False
            sdbcArtiCod.Visible = False
            sdbcArtiDen.Visible = False
            lblFecDesde.caption = sIdiFecDesde(2)
            sdbcFecReu.Enabled = True: sdbcFecReu.Visible = True
            txtFecDesde.Enabled = False: txtFecDesde.Visible = False
            lblFecHasta.Visible = False
            txtFecHasta.Enabled = False: txtFecHasta.Visible = False
            cmdCalFecApeHasta.Enabled = False: cmdCalFecApeHasta.Visible = False
            sdbcEqpCod.Visible = False
            sdbcEqpDen.Visible = False
            Frame4.Top = 725
            Frame3.Visible = False
            'Desgloses
            sdbcNivel.AddItem gParametrosGenerales.gsSingPres4 & Chr(m_lSeparador) & 3
            sdbcNivel.AddItem sIdiNiveles(2) & Chr(m_lSeparador) & 4
            sdbcNivel.AddItem sIdiNiveles(7) & Chr(m_lSeparador) & 5 'grupo
            sdbcNivel.AddItem sIdiNiveles(3) & Chr(m_lSeparador) & 6
            sdbcNivel.Text = gParametrosGenerales.gsSingPres4
        
    End Select


End Sub

Private Sub sdbcMon_Change()
    
    If Not bMonRespetarCombo Then
    
        bMonRespetarCombo = True
        dequivalencia = 0
        sMoneda = ""
        bMonRespetarCombo = False
        
        bMonCargarComboDesde = True
        
    End If
    
End Sub
Private Sub sdbcMon_CloseUp()
    
    bMonRespetarCombo = True
    sdbcMon.Text = sdbcMon.Columns(0).Text
    dequivalencia = sdbcMon.Columns(2).Value
    sMoneda = sdbcMon.Columns(1).Text
    bMonRespetarCombo = False
    bMonCargarComboDesde = False
        
End Sub

Private Sub sdbcMon_DropDown()
    Dim oMon As CMoneda
    
    sdbcMon.RemoveAll
    
    Screen.MousePointer = vbHourglass
    If bMonCargarComboDesde Then
        oMonedas.CargarTodasLasMonedasDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcMon.Text), , , True
    Else
        oMonedas.CargarTodasLasMonedas , , , , , False, True
    End If
    
    For Each oMon In oMonedas
        sdbcMon.AddItem oMon.Cod & Chr(m_lSeparador) & oMon.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oMon.Equiv
    Next
    
    If bMonCargarComboDesde And Not oMonedas.EOF Then
        sdbcMon.AddItem "..."
    End If

    sdbcMon.SelStart = 0
    sdbcMon.SelLength = Len(sdbcMon.Text)
    sdbcMon.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcMon_InitColumnProps()

    sdbcMon.DataFieldList = "Column 0"
    sdbcMon.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcMon_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcMon.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcMon.Rows - 1
            bm = sdbcMon.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcMon.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcMon.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcMon_Validate(Cancel As Boolean)

    Dim bExiste As Boolean
    
    If sdbcMon.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el Moneda
    
    Screen.MousePointer = vbHourglass
    oMonedas.CargarTodasLasMonedas sdbcMon.Text, , True, , False, , True
    
    bExiste = Not (oMonedas.Count = 0)
    
    If Not bExiste Then
        sdbcMon.Text = ""
        dequivalencia = 0
        sMoneda = ""
    Else
        bMonRespetarCombo = True
        dequivalencia = oMonedas.Item(1).Equiv
        sdbcMon.Text = oMonedas.Item(1).Cod
        sMoneda = oMonedas.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        bMonRespetarCombo = False
        bMonCargarComboDesde = False
        
    End If
    Screen.MousePointer = vbNormal
End Sub
Private Sub cmdSelMat_Click()
    If sOrigen = "A4B1C5D2" Or sOrigen = "A4B1C5D1" Or sOrigen = "frmInfAhorroNegConcep3Reu" Or sOrigen = "frmInfAhorroNegConcep3Desde" Then
        frmSELPresUO.sOrigen = "frmLstINFAhorroNeg" & sOrigen
        frmSELPresUO.g_iTipoPres = 3
        frmSELPresUO.bRUO = bRUO
        frmSELPresUO.bMostrarBajas = True
        frmSELPresUO.Show 1
            
    ElseIf sOrigen = "A4B1C6D2" Or sOrigen = "A4B1C6D1" Or sOrigen = "frmInfAhorroNegConcep4Reu" Or sOrigen = "frmInfAhorroNegConcep4Desde" Then
        frmSELPresUO.sOrigen = "frmLstINFAhorroNeg" & sOrigen
        frmSELPresUO.g_iTipoPres = 4
        frmSELPresUO.bRUO = bRUO
        frmSELPresUO.bMostrarBajas = True
        frmSELPresUO.Show 1
    ElseIf sOrigen = "frmInfAhorroNegMatDesde" Then
        frmSELMAT.sOrigen = "frmLstINFAhorroNegDesde"
        frmSELMAT.bRComprador = bRMat
        frmSELMAT.Show 1
    ElseIf sOrigen = "A4B1C2D1" Then
        frmSELMAT.sOrigen = "frmLstINFAhorroNegA4B1C2D1"
        frmSELMAT.bRComprador = bRMat
        frmSELMAT.Show 1
    
    ElseIf sOrigen = "A4B1C2D2" Then
        frmSELMAT.sOrigen = "frmLstINFAhorroNegA4B1C2D2"
        frmSELMAT.bRComprador = bRMat
        frmSELMAT.Show 1
       
    Else 'Materiales
        frmSELMAT.sOrigen = "frmLstINFAhorroNeg"
        frmSELMAT.bRComprador = bRMat
        frmSELMAT.Show 1
    End If
    
End Sub

Public Sub PonerMatSeleccionado(Optional sOrigen As String)

    Set oGMN1Seleccionado = Nothing
    Set oGMN2Seleccionado = Nothing
    Set oGMN3Seleccionado = Nothing
    Set oGMN4Seleccionado = Nothing

    If sOrigen = "frmInfAhorroNegMatDesde" Then
        Set oGMN1Seleccionado = frmInfAhorroNegMatDesde.oGMN1Seleccionado
        Set oGMN2Seleccionado = frmInfAhorroNegMatDesde.oGMN2Seleccionado
        Set oGMN3Seleccionado = frmInfAhorroNegMatDesde.oGMN3Seleccionado
        Set oGMN4Seleccionado = frmInfAhorroNegMatDesde.oGMN4Seleccionado
    Else
        If sOrigen = "frmInfAhorroNegMatReu" Then
            Set oGMN1Seleccionado = frmInfAhorroNegMatReu.oGMN1Seleccionado
            Set oGMN2Seleccionado = frmInfAhorroNegMatReu.oGMN2Seleccionado
            Set oGMN3Seleccionado = frmInfAhorroNegMatReu.oGMN3Seleccionado
            Set oGMN4Seleccionado = frmInfAhorroNegMatReu.oGMN4Seleccionado
        Else
            Set oGMN1Seleccionado = frmSELMAT.oGMN1Seleccionado
            Set oGMN2Seleccionado = frmSELMAT.oGMN2Seleccionado
            Set oGMN3Seleccionado = frmSELMAT.oGMN3Seleccionado
            Set oGMN4Seleccionado = frmSELMAT.oGMN4Seleccionado
        End If
    End If
    
    If Not oGMN1Seleccionado Is Nothing Then
        sGMN1Cod = oGMN1Seleccionado.Cod
        txtEstMat = sGMN1Cod
    Else
        sGMN1Cod = ""
    End If
        
    If Not oGMN2Seleccionado Is Nothing Then
        sGMN2Cod = oGMN2Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN2Cod
    Else
        sGMN2Cod = ""
    End If
        
    If Not oGMN3Seleccionado Is Nothing Then
        sGMN3Cod = oGMN3Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN3Cod
    Else
        sGMN3Cod = ""
    End If
        
    If Not oGMN4Seleccionado Is Nothing Then
        sGMN4Cod = oGMN4Seleccionado.Cod
        txtEstMat = txtEstMat & " - " & sGMN4Cod
    Else
        sGMN4Cod = ""
    End If
       
    
End Sub
Private Sub sdbcEqpCod_Change()
     
    If Not bEqpRespetarCombo Then
    
        bEqpRespetarCombo = True
        sdbcEqpDen.Text = ""
        bEqpRespetarCombo = False
        bEqpCargarComboDesde = True
        Set oEqpSeleccionado = Nothing
        
    End If
    
End Sub
Private Sub sdbcEqpCod_CloseUp()
    
    If sdbcEqpCod.Value = "..." Then
        sdbcEqpCod.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpCod.Value = "" Then Exit Sub
            
    bEqpRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpCod.Columns(1).Text
    sdbcEqpCod.Text = sdbcEqpCod.Columns(0).Text
    bEqpRespetarCombo = False

    Screen.MousePointer = vbHourglass
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(0).Text)
        
    bEqpCargarComboDesde = False
    Screen.MousePointer = vbNormal

End Sub
Private Sub sdbcEqpCod_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpCod.RemoveAll
    
    If bEqpCargarComboDesde Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcEqpCod.Text), , False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, False, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If bEqpCargarComboDesde And Not oEqps.EOF Then
        sdbcEqpCod.AddItem "..."
    End If

    sdbcEqpCod.SelStart = 0
    sdbcEqpCod.SelLength = Len(sdbcEqpCod.Text)
    sdbcEqpCod.Refresh

    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpCod_InitColumnProps()

    sdbcEqpCod.DataFieldList = "Column 0"
    sdbcEqpCod.DataFieldToDisplay = "Column 0"
    
End Sub
Private Sub sdbcEqpCod_Click()
    
    If Not sdbcEqpCod.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpCod_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpCod.Rows - 1
            bm = sdbcEqpCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub
Public Sub sdbcEqpCod_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos sdbcEqpCod.Text, , True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpCod.Text = ""
        
    Else
        bEqpRespetarCombo = True
        sdbcEqpDen.Text = oEquipos.Item(1).Den
        bEqpRespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        bEqpCargarComboDesde = False
        
    End If
    
    Set oEquipos = Nothing
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub sdbcEqpDen_Change()
      
    If Not bEqpRespetarCombo Then
    
        bEqpRespetarCombo = True
        sdbcEqpCod.Text = ""
        bEqpRespetarCombo = False
        
        bEqpCargarComboDesde = True
        Set oEqpSeleccionado = Nothing
                
    End If
          
End Sub

Private Sub sdbcEqpDen_Click()
    If Not sdbcEqpDen.DroppedDown Then
        sdbcEqpCod = ""
        sdbcEqpDen = ""
    End If
End Sub

Private Sub sdbcEqpDen_CloseUp()
    
    If sdbcEqpDen.Value = "..." Then
        sdbcEqpDen.Text = ""
        Exit Sub
    End If
    
    If sdbcEqpDen.Value = "" Then Exit Sub
       
    bEqpRespetarCombo = True
    sdbcEqpDen.Text = sdbcEqpDen.Columns(0).Text
    sdbcEqpCod.Text = sdbcEqpDen.Columns(1).Text
    bEqpRespetarCombo = False
    
    Screen.MousePointer = vbHourglass
    Set oEqpSeleccionado = oEqps.Item(sdbcEqpCod.Columns(1).Text)
        
    bEqpCargarComboDesde = False
    
    Screen.MousePointer = vbNormal
End Sub
Private Sub sdbcEqpDen_DropDown()

    Dim Codigos As TipoDatosCombo
    Dim i As Integer
        
    Screen.MousePointer = vbHourglass
    Set oEqps = Nothing
    Set oEqps = oFSGSRaiz.Generar_CEquipos
    
    sdbcEqpDen.RemoveAll
    
    If bEqpCargarComboDesde Then
        oEqps.CargarTodosLosEquiposDesde gParametrosInstalacion.giCargaMaximaCombos, , Trim(sdbcEqpDen.Text), False, False
    Else
        oEqps.CargarTodosLosEquipos , , False, True, False
    End If
    
    Codigos = oEqps.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcEqpDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If bEqpCargarComboDesde And Not oEqps.EOF Then
        sdbcEqpDen.AddItem "..."
    End If

    sdbcEqpDen.SelStart = 0
    sdbcEqpDen.SelLength = Len(sdbcEqpDen.Text)
    sdbcEqpDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcEqpDen_InitColumnProps()
    sdbcEqpDen.DataFieldList = "Column 0"
    sdbcEqpDen.DataFieldToDisplay = "Column 0"
End Sub


Private Sub sdbcEqpDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcEqpDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcEqpDen.Rows - 1
            bm = sdbcEqpDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcEqpDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcEqpDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub

Private Sub sdbcEqpDen_Validate(Cancel As Boolean)

    Dim oEquipos As CEquipos
    Dim bExiste As Boolean
    
    Set oEquipos = oFSGSRaiz.Generar_CEquipos
    
    If sdbcEqpDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el equipo
    
    Screen.MousePointer = vbHourglass
    oEquipos.CargarTodosLosEquipos , sdbcEqpDen.Text, True, , False
    
    bExiste = Not (oEquipos.Count = 0)
    
    If Not bExiste Then
        sdbcEqpDen.Text = ""
    
    Else
        bEqpRespetarCombo = True
        sdbcEqpCod.Text = oEquipos.Item(1).Cod
        bEqpRespetarCombo = False
        Set oEqpSeleccionado = oEquipos.Item(1)
        bEqpCargarComboDesde = False
        
    End If
    
    Set oEquipos = Nothing
    
    Screen.MousePointer = vbNormal
    
End Sub

Private Sub Timer1_Timer()

   If frmESPERA.ProgressBar2.Value < 90 Then
      frmESPERA.ProgressBar2.Value = frmESPERA.ProgressBar2.Value + 20
   End If
   If frmESPERA.ProgressBar1.Value < 10 Then
       frmESPERA.ProgressBar1.Value = frmESPERA.ProgressBar1.Value + 1
   End If

End Sub

Private Sub cmdObtener_Click()
Dim iNivelAsig As Integer

    If (Right(sOrigen, 2) = "D1" Or Right(sOrigen, 3) = "Reu") Then
        'En reuni�n
        If sdbcFecReu = "" Then
            oMensajes.NoValido sIdiNoValido(1)
            If Me.Visible Then sdbcFecReu.SetFocus
            Exit Sub
        End If
    
        If Not IsDate(sdbcFecReu) Then
            oMensajes.NoValido sIdiNoValido(1)
            If Me.Visible Then sdbcFecReu.SetFocus
            Exit Sub
        End If
        
        dFechaDesde = sdbcFecReu
        dFechaHasta = sdbcFecReu
        
    Else
        If sOrigen = "frmInfAhorroNegMatDesde" Then
            If frmInfAhorroNegMatDesde.sdbcFiltro.Text = frmInfAhorroNegMatDesde.sFechaFiltro Then
            
                'Desde/Hasta
                If txtFecDesde = "" Then
                    oMensajes.NoValido sIdiNoValido(1)
                    If Me.Visible Then txtFecDesde.SetFocus
                    Exit Sub
                End If
                If Not IsDate(txtFecDesde) Then
                    oMensajes.NoValido sIdiNoValido(1)
                    If Me.Visible Then txtFecDesde.SetFocus
                    Exit Sub
                End If
                dFechaDesde = txtFecDesde
                
                If txtFecHasta <> "" Then
                    If Not IsDate(txtFecHasta) Then
                        oMensajes.NoValido sIdiNoValido(1)
                        If Me.Visible Then txtFecHasta.SetFocus
                        Exit Sub
                    End If
                    If CDate(txtFecDesde) > CDate(txtFecHasta) Then
                        oMensajes.FechaDesdeMayorFechaHasta
                        If Me.Visible Then txtFecHasta.SetFocus
                        Exit Sub
                    End If
                    dFechaHasta = txtFecHasta
                Else
                    dFechaHasta = Empty
                End If
            Else
                If IsDate(txtFecDesde) Then
                    dFechaDesde = txtFecDesde
                End If
                If IsDate(txtFecHasta) Then
                    dFechaHasta = txtFecHasta
                End If
            End If
        Else
            'Si viene de Listados
            
            'Desde/Hasta
            If txtFecDesde = "" Then
                oMensajes.NoValido sIdiNoValido(1)
                If Me.Visible Then txtFecDesde.SetFocus
                Exit Sub
            End If
            If Not IsDate(txtFecDesde) Then
                oMensajes.NoValido sIdiNoValido(1)
                If Me.Visible Then txtFecDesde.SetFocus
                Exit Sub
            End If
            dFechaDesde = txtFecDesde
            
            If txtFecHasta <> "" Then
                If Not IsDate(txtFecHasta) Then
                    oMensajes.NoValido sIdiNoValido(1)
                    If Me.Visible Then txtFecHasta.SetFocus
                    Exit Sub
                End If
                If CDate(txtFecDesde) > CDate(txtFecHasta) Then
                    oMensajes.FechaDesdeMayorFechaHasta
                    If Me.Visible Then txtFecHasta.SetFocus
                    Exit Sub
                End If
                dFechaHasta = txtFecHasta
            Else
                dFechaHasta = Empty
            End If
        End If
    End If
    
    If bRMat Then
    
        Select Case gParametrosGenerales.giNEM
    
            Case 1
                    If sGMN1Cod = "" Then
                        oMensajes.NoValido sIdiNoValido(4)
                        Exit Sub
                    End If
            Case 2
                    Screen.MousePointer = vbHourglass
                    If sGMN2Cod = "" And sGMN1Cod <> "" Then
                        Set oICompAsignado = oGMN1Seleccionado
                     
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig < 1 Or iNivelAsig > 2 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    End If
            Case 3
                    Screen.MousePointer = vbHourglass
                    If sGMN3Cod = "" And sGMN2Cod <> "" Then
                        Set oICompAsignado = oGMN2Seleccionado
                       
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig = 0 Or iNivelAsig > 2 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    Else
                        If sGMN2Cod = "" And sGMN1Cod <> "" Then
                            Set oICompAsignado = oGMN1Seleccionado
                          
                            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                                Screen.MousePointer = vbNormal
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        End If
                    End If
            Case 4
                
                Screen.MousePointer = vbHourglass
                If sGMN4Cod = "" And sGMN3Cod <> "" Then
                  
                        Set oICompAsignado = oGMN3Seleccionado
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        'Si el material que tiene asignado el comprador es de nivel 4
                        ' no cargamos nada
                        If iNivelAsig = 0 Or iNivelAsig > 3 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                Else
                    If sGMN3Cod = "" And sGMN2Cod <> "" Then
                        Set oICompAsignado = oGMN2Seleccionado
                  
                        iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                        If iNivelAsig = 0 Or iNivelAsig > 2 Then
                            Screen.MousePointer = vbNormal
                            oMensajes.InfAhorroMatNoValido
                            Exit Sub
                        End If
                    Else
                        If sGMN2Cod = "" And sGMN1Cod <> "" Then
                            Set oICompAsignado = oGMN1Seleccionado
                           
                            iNivelAsig = oICompAsignado.NivelAsignadoComprador(basOptimizacion.gCodEqpUsuario, basOptimizacion.gCodCompradorUsuario)
                            If iNivelAsig < 1 Or iNivelAsig > 2 Then
                                Screen.MousePointer = vbNormal
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        Else
                            If sGMN4Cod = "" Then
                                Screen.MousePointer = vbNormal
                                oMensajes.InfAhorroMatNoValido
                                Exit Sub
                            End If
                        End If
                    End If
                End If
         Screen.MousePointer = vbNormal
       
        End Select
        
    End If

    If sdbcMon = "" Then
        oMensajes.NoValido sIdiNoValido(5)
        If Me.Visible Then sdbcMon.SetFocus
        Exit Sub
    End If

    iNivel = sdbcNivel.Columns("VALOR").Value
    
    If crs_Connected = False Then
        Exit Sub
    End If
    
    Select Case sOrigen
        
        Case "A4B1C1D1", "frmInfAhorroNegReu", "A4B1C1D2", "frmInfAhorroNegDesde"
            ObtenerListadoAhorrosNegGeneral
        Case "A4B1C2D1", "frmInfAhorroNegMatReu", "A4B1C2D2", "frmInfAhorroNegMatDesde"
            If sOrigen = "frmInfAhorroNegMatDesde" Then
                Select Case frmInfAhorroNegMatDesde.sdbcFiltro.Text
                Case frmInfAhorroNegMatDesde.sFechaFiltro
                    ObtenerListadoAhorrosNegMat
                Case frmInfAhorroNegMatDesde.sArticulo
                    ObtenerListadoAhorrosNegMatArticulo
                Case frmInfAhorroNegMatDesde.sMaterial
                    ObtenerListadoAhorrosNegMatMaterial
                End Select
            Else
                If sdbcArtiCod.Text <> "" Then
                    ObtenerListadoAhorrosNegMatArticulo
                Else
                    If txtEstMat.Text <> "" Then
                        ObtenerListadoAhorrosNegMatMaterial
                    Else
                        ObtenerListadoAhorrosNegMat
                    End If
                End If
                
            End If
        Case "A4B1C3D1", "frmInfAhorroNegEqpResReu", "A4B1C3D2", "frmInfAhorroNegEqpResDesde"
            ObtenerListadoAhorrosNegEqpRes
        Case "A4B1C4D1", "frmInfAhorroNegEqpReu", "A4B1C4D2", "frmInfAhorroNegEqpDesde"
            ObtenerListadoAhorrosNegEqpNeg
        Case "A4B1C5D1", "frmInfAhorroNegConcep3Reu", "A4B1C5D2", "frmInfAhorroNegConcep3Desde"
            ObtenerListadoAhorrosNegConcep3
        Case "A4B1C6D1", "frmInfAhorroNegConcep4Reu", "A4B1C6D2", "frmInfAhorroNegConcep4Desde"
            ObtenerListadoAhorrosNegConcep4
            
    End Select

End Sub

Private Sub ObtenerListadoAhorrosNegGeneral()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sTitulo As String
    Dim iGraf As Integer
    Dim ReportFile As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim FormulaSel As String
    Dim FormulaMon As String
    
    Set oCRInformes = GenerarCRInformes
    
    If sOrigen = "A4B1C1D1" Or sOrigen = "frmInfAhorroNegReu" Then
        sTitulo = sIdiTitulosRpt(1)
    Else
        sTitulo = sIdiTitulosRpt(2)
    End If
     
    If opVerDat Then
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If

    Screen.MousePointer = vbHourglass
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
           Screen.MousePointer = vbNormal
           oMensajes.RutaDeRPTNoValida
           Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    ReportFile = "rptAhorroNegGen.rpt"
    RepPath = gParametrosInstalacion.gsRPTPATH & "\" & ReportFile
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
    
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        
        End If
    Else
        sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
    
    End If
    
    If optReu Then
        sSeleccion = sSeleccion & sIdiCriSoloReu & ". "
    Else
        If optDir Then
            sSeleccion = sSeleccion & sIdiCriSoloAdj & ". "
        Else
            sSeleccion = sSeleccion & sIdiCriReuYAdj & ". "
        End If
    End If
       
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    
    FormulaMon = ""
    
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
                
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    

    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECHA")).Text = """" & srIdiGenFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiGenPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiGenAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiGenAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiGenProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtARTICULO")).Text = """" & srIdiGenArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPART")).Text = """" & srIdiGenPresupArt & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADOART")).Text = """" & srIdiGenAdjudicadoArt & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORROART")).Text = """" & srIdiGenAhorroArt & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & sIdiNiveles(7) & """"
    
    If IsEmpty(dFechaHasta) Then
        oCRInformes.ListadoAhorroNeg oReport, dFechaDesde, , iNivel, dequivalencia, optReu, optDir, iGraf
    Else
        oCRInformes.ListadoAhorroNeg oReport, dFechaDesde, dFechaHasta, iNivel, dequivalencia, optReu, optDir, iGraf
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(1)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
End Sub

Private Sub ObtenerListadoAhorrosNegMat()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim iGraf As Integer
    Dim iNivelMat As Integer
    Dim sTitulo As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim sMaterial As String
    Dim FormulaSel As String
    Dim FormulaMon As String
   
    Set oCRInformes = GenerarCRInformes
   
    sTitulo = sIdiTitulosRpt(4)

    If opVerDat Then
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If
    
    iNivelMat = 0
    If sGMN1Cod <> "" Then iNivelMat = 1
    If sGMN2Cod <> "" Then iNivelMat = 2
    If sGMN3Cod <> "" Then iNivelMat = 3
    If sGMN4Cod <> "" Then iNivelMat = 4
    
    Screen.MousePointer = vbHourglass
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    If sOrigen = "frmInfAhorroNegMatDesde" Then
        Select Case frmInfAhorroNegMatDesde.sdbcFiltro.Text
        Case frmInfAhorroNegMatDesde.sFechaFiltro
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMat.rpt"
        Case frmInfAhorroNegMatDesde.sMaterial
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMatMaterial.rpt"
        Case frmInfAhorroNegMatDesde.sArticulo
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMatArticulo.rpt"
        End Select
    Else
        If sdbcArtiCod.Text <> "" Then
            RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMatArticulo.rpt"
        Else
            If txtEstMat.Text <> "" Then
                RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMatMaterial.rpt"
            Else
                RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMat.rpt"
            End If
        End If
    End If
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
      
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        End If
    Else
        
        sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
    End If
        
    sMaterial = ""
    If sGMN4Cod <> "" Then
            sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & sGMN4Cod
    Else
        If sGMN3Cod <> "" Then
                sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod
       Else
            If sGMN2Cod <> "" Then
                    sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod & " - " & sGMN2Cod
            Else
                If sGMN1Cod <> "" Then
                        sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod
                End If
            End If
        End If
    End If
    
    If optReu Then
        sSeleccion = sSeleccion & sIdiCriSoloReu & ". "
    Else
        If optDir Then
            sSeleccion = sSeleccion & sIdiCriSoloAdj & ". "
        Else
            sSeleccion = sSeleccion & sIdiCriReuYAdj & ". "
        End If
    End If
          
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaMon = ""
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SELMATERIAL")).Text = """" & sMaterial & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN1_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN2_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN2 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN3_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN3 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN4_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN4 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECHA")).Text = """" & srIdiMatFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNUMPROC")).Text = """" & srIdiMatNumProc & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiMatPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiMatAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiMatAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiMatProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtARTICULO")).Text = """" & srIdiMatArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & sIdiNiveles(7) & """"
    
    If IsEmpty(dFechaHasta) Then
        oCRInformes.ListadoAhorroNegMat oGestorInformes, oReport, dFechaDesde, , sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat
    Else
        oCRInformes.ListadoAhorroNegMat oGestorInformes, oReport, dFechaDesde, dFechaHasta, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
End Sub

Private Sub ObtenerListadoAhorrosNegMatArticulo()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim iGraf As Integer
    Dim iNivelMat As Integer
    Dim sTitulo As String
    Dim sCodArticulo As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim FormulaSel As String
    Dim FormulaMon As String
    
    Set oCRInformes = GenerarCRInformes
    
    sTitulo = sIdiTitulosRpt(13)
    
    If opVerDat Then
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If
    
    iNivelMat = 0
    
    Screen.MousePointer = vbHourglass
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMatArticulo.rpt"
    
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
      
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        End If
    Else
        If Not IsEmpty(dFechaDesde) Then
            sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
        End If
    End If
    
    If sdbcArtiCod.Value <> "" Then
        sCodArticulo = sdbcArtiCod.Text
    End If
    If optReu Then
        sSeleccion = sSeleccion & sIdiCriSoloReu & ". "
    Else
        If optDir Then
            sSeleccion = sSeleccion & sIdiCriSoloAdj & ". "
        Else
            sSeleccion = sSeleccion & sIdiCriReuYAdj & ". "
        End If
    End If
     
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaMon = ""
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNUMPROC")).Text = """" & srIdiMatNumProc & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiMatPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiMatAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiMatAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiMatProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtARTICULO")).Text = """" & srIdiMatArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROVEEDOR")).Text = """" & sproveedor & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECINI")).Text = """" & sFechaInicio & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECFIN")).Text = """" & sFechaFin & """"
        
    If IsEmpty(dFechaHasta) Then
        If Not IsEmpty(dFechaDesde) Then
            oCRInformes.ListadoAhorroNegMatArticulo oGestorInformes, oReport, dFechaDesde, , sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat, sCodArticulo
        Else
            oCRInformes.ListadoAhorroNegMatArticulo oGestorInformes, oReport, , , sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat, sCodArticulo
        End If
    Else
        oCRInformes.ListadoAhorroNegMatArticulo oGestorInformes, oReport, dFechaDesde, dFechaHasta, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat, sCodArticulo
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
     pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
        
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
End Sub
Private Sub ObtenerListadoAhorrosNegMatMaterial()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim iGraf As Integer
    Dim iNivelMat As Integer
    Dim sTitulo As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim sMaterial As String
    Dim FormulaSel As String
    Dim FormulaMon As String
    Dim sCodArticulo As String
    
    Set oCRInformes = GenerarCRInformes
    
    If sOrigen = "A4B1C2D1" Or sOrigen = "frmInfAhorroNegMatReu" Then
        sTitulo = sIdiTitulosRpt(3)
    Else
        sTitulo = sIdiTitulosRpt(4)
    End If

    If opVerDat Then
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If
    
    iNivelMat = 0
    If sGMN1Cod <> "" Then iNivelMat = 1
    If sGMN2Cod <> "" Then iNivelMat = 2
    If sGMN3Cod <> "" Then iNivelMat = 3
    If sGMN4Cod <> "" Then iNivelMat = 4
    
    Screen.MousePointer = vbHourglass
    
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegMatMaterial.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
      
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        
        End If
    Else
        sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
    End If
    
    sMaterial = ""
    If sGMN4Cod <> "" Then
            sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod & " - " & sGMN4Cod
    Else
        If sGMN3Cod <> "" Then
                sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod & " - " & sGMN2Cod & " - " & sGMN3Cod
       Else
            If sGMN2Cod <> "" Then
                    sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod & " - " & sGMN2Cod
            Else
                If sGMN1Cod <> "" Then
                        sMaterial = " " & sIdiCriMaterial & " " & sGMN1Cod
                End If
            End If
        End If
    End If

    If sdbcArtiCod.Value <> "" Then sCodArticulo = sdbcArtiCod.Text
        
    If optReu Then
        sSeleccion = sSeleccion & sIdiCriSoloReu & ". "
    Else
        If optDir Then
            sSeleccion = sSeleccion & sIdiCriSoloAdj & ". "
        Else
            sSeleccion = sSeleccion & sIdiCriReuYAdj & ". "
        End If
    End If
      
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaMon = ""
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SELMATERIAL")).Text = """" & sMaterial & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN1_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN1 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN2_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN2 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN3_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN3 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN4_NOM")).Text = """" & gParametrosGenerales.gsDEN_GMN4 & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECHA")).Text = """" & srIdiMatFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNUMPROC")).Text = """" & srIdiMatNumProc & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiMatPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiMatAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiMatAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiMatProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtARTICULO")).Text = """" & srIdiMatArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & sIdiNiveles(7) & """"
    
    If IsEmpty(dFechaHasta) Then
        If IsEmpty(dFechaDesde) Then
            oCRInformes.ListadoAhorroNegMat oGestorInformes, oReport, , , sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat
        Else
            oCRInformes.ListadoAhorroNegMat oGestorInformes, oReport, dFechaDesde, , sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat
        End If
    Else
        If IsEmpty(dFechaDesde) Then
            oCRInformes.ListadoAhorroNegMat oGestorInformes, oReport, , dFechaHasta, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat ', sCodArticulo
        Else
            oCRInformes.ListadoAhorroNegMat oGestorInformes, oReport, dFechaDesde, dFechaHasta, sGMN1Cod, sGMN2Cod, sGMN3Cod, sGMN4Cod, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelMat ', sCodArticulo
        End If
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    

End Sub

Private Sub ObtenerListadoAhorrosNegEqpRes()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sGrafico As String
    Dim sTitulo As String
    Dim iGraf As Integer
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim FormulaSel As String
    Dim FormulaMon As String

    Set oCRInformes = GenerarCRInformes

    Select Case sOrigen
    Case "A4B1C3D1", "frmInfAhorroNegEqpResReu"
        sTitulo = sIdiTitulosRpt(5)
    Case "A4B1C3D2", "frmInfAhorroNegEqpResDesde"
        sTitulo = sIdiTitulosRpt(6)
    End Select

    If opVerDat Then
        sGrafico = ""
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If
    
    Screen.MousePointer = vbHourglass
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegEqpRes.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
      
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        
        End If
    Else
        sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
    
    End If

    If bREqpAsig Then
    Else
        If Trim(sdbcEqpCod) <> "" Then
                If sSeleccion = "" Then
                    sSeleccion = sIdiCriEquipo & " " & sdbcEqpCod & ". "
                Else
                    sSeleccion = sSeleccion & sIdiCriEquipo & " " & sdbcEqpCod & ". "
                End If
        End If
    End If
     
      
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaMon = ""
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
                
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)

    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"

    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECHA")).Text = """" & srIdiResFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNUMPROC")).Text = """" & srIdiResNumProc & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiResPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiResAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiResAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "EQP_NOM")).Text = """" & srIdiEquipo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "COM_NOM")).Text = """" & srIdiComprador & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiResProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtARTICULO")).Text = """" & srIdiResArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & sIdiNiveles(7) & """"
    
    If IsEmpty(dFechaHasta) Then
        oCRInformes.ListadoAhorroNegEqpRes oReport, dFechaDesde, , sdbcEqpCod, iNivel, dequivalencia, optReu, optDir, bREqpAsig, iGraf
    Else
        oCRInformes.ListadoAhorroNegEqpRes oReport, dFechaDesde, dFechaHasta, sdbcEqpCod, iNivel, dequivalencia, optReu, optDir, bREqpAsig, iGraf
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(3)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal

End Sub

Private Sub ObtenerListadoAhorrosNegEqpNeg()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim sGrafico As String
    Dim iGraf As Integer
    Dim sTitulo As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim FormulaSel As String
    Dim FormulaMon As String

    Set oCRInformes = GenerarCRInformes

    Select Case sOrigen
    Case "A4B1C4D1", "frmInfAhorroNegEqpReu"
        sTitulo = sIdiTitulosRpt(7)
    Case "A4B1C4D2", "frmInfAhorroNegEqpDesde"
        sTitulo = sIdiTitulosRpt(8)
    End Select
    
    If opVerDat Then
        sGrafico = ""
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If
    Screen.MousePointer = vbHourglass
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegEqp.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
      
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        
        End If
    Else
        sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
    
    End If
    
    If optReu Then
        sSeleccion = sSeleccion & sIdiCriSoloReu & ". "
    Else
        If optDir Then
            sSeleccion = sSeleccion & sIdiCriSoloAdj & ". "
        Else
            sSeleccion = sSeleccion & sIdiCriReuYAdj & ". "
        End If
    End If
    
    If bREqpAsig Then
    Else
        If Trim(sdbcEqpCod) <> "" Then
                If sSeleccion = "" Then
                    sSeleccion = sIdiCriEquipo & " " & sdbcEqpCod & ". "
                Else
                    sSeleccion = sSeleccion & sIdiCriEquipo & " " & sdbcEqpCod & ". "
                End If
        End If
    End If
       
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaMon = ""
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
                
    'SE HABRE EL REPORT
    
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"

    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECHA")).Text = """" & srIdiEqpFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiEqpPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiEqpAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiEqpAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "EQP_NOM")).Text = """" & srIdiEquipo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "COM_NOM")).Text = """" & srIdiComprador & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    
    If IsEmpty(dFechaHasta) Then
        oCRInformes.ListadoAhorroNegEqpNeg oReport, dFechaDesde, , sdbcEqpCod, iNivel, dequivalencia, optReu, optDir, bREqpAsig, iGraf
    Else
        oCRInformes.ListadoAhorroNegEqpNeg oReport, dFechaDesde, dFechaHasta, sdbcEqpCod, iNivel, dequivalencia, optReu, optDir, bREqpAsig, iGraf
    End If
        
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(4)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    
End Sub

Private Sub CargarRecursos()

Dim Ador As Ador.Recordset
Dim sCaption1 As String
Dim sDesde As String
Dim sReunion As String
Dim sDesdeRPT As String
Dim sReunionRPT As String

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_LSTINFAHORROS_NEG, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        caption = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(0) = Ador(0).Value
        Ador.MoveNext
        SSTab1.TabCaption(1) = Ador(0).Value
        Ador.MoveNext
        lblFecDesde.caption = Ador(0).Value
        Ador.MoveNext
        lblFecHasta.caption = Ador(0).Value '5
        Ador.MoveNext
        lblMon.caption = Ador(0).Value
        Ador.MoveNext
        lblEstMat.caption = Ador(0).Value
        Ador.MoveNext
        lblEqpCod.caption = Ador(0).Value
        Ador.MoveNext
        optReu.caption = Ador(0).Value
        Ador.MoveNext
        optDir.caption = Ador(0).Value '10
        Ador.MoveNext
        optTodos.caption = Ador(0).Value
        Ador.MoveNext
        lblNivel.caption = Ador(0).Value
        Ador.MoveNext
        opVerDat.caption = Ador(0).Value
        Ador.MoveNext
        opVerGraf.caption = Ador(0).Value
        Ador.MoveNext
        opDatGraf.caption = Ador(0).Value '15
        Ador.MoveNext
        cmdObtener.caption = Ador(0).Value
        Ador.MoveNext
        sdbcFecReu.Columns(1).caption = Ador(0).Value
        sIdiNoValido(1) = Ador(0).Value
        Ador.MoveNext
        sIdiNoValido(2) = Ador(0).Value
        Ador.MoveNext
        sIdiNoValido(3) = Ador(0).Value
        Ador.MoveNext
        sIdiNoValido(4) = Ador(0).Value '20
        Ador.MoveNext
        sIdiNoValido(5) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosFrm(1) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosFrm(2) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosFrm(3) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosFrm(4) = Ador(0).Value '25
        Ador.MoveNext
        sIdiTitulosFrm(5) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosFrm(6) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosFrm(7) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosFrm(8) = Ador(0).Value
        Ador.MoveNext
        sIdiNiveles(1) = Ador(0).Value '30
        Ador.MoveNext
        sIdiNiveles(2) = Ador(0).Value
        Ador.MoveNext
        sIdiNiveles(3) = Ador(0).Value
        Ador.MoveNext
        sIdiNiveles(4) = Ador(0).Value
        Ador.MoveNext
        sIdiNiveles(5) = Ador(0).Value
        srIdiComprador = Ador(0).Value
        Ador.MoveNext
        sIdiNiveles(6) = Ador(0).Value '35
        Ador.MoveNext
        sIdiFecDesde(1) = Ador(0).Value
        Ador.MoveNext
        sIdiFecDesde(2) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(1) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(2) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(3) = Ador(0).Value '40
        Ador.MoveNext
        sIdiTitulosRpt(4) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(5) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(6) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(7) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(8) = Ador(0).Value '45
        Ador.MoveNext
        'Selecci�n:
        sIdiCriSeleccion = Ador(0).Value
        Ador.MoveNext
        'Fechas:
        sIdiFechas = Ador(0).Value
        Ador.MoveNext
        'Fecha de la reuni�n:
        sIdiFecReu = Ador(0).Value
        Ador.MoveNext
        'Fechas desde:
        sIdiFechaDesde = Ador(0).Value
        Ador.MoveNext
        'Equipo:
        sIdiCriEquipo = Ador(0).Value '50
        Ador.MoveNext
        'Solo procesos de reuni�n
        sIdiCriSoloReu = Ador(0).Value
        Ador.MoveNext
        'Solo procesos de adjudicaci�n directa
        sIdiCriSoloAdj = Ador(0).Value
        Ador.MoveNext
        'Procesos de reuni�n y de adjudicaci�n directa
        sIdiCriReuYAdj = Ador(0).Value
        Ador.MoveNext
        'Moneda:
        sIdiCriMoneda = Ador(0).Value
        Ador.MoveNext
        'Material:
        sIdiCriMaterial = Ador(0).Value
        Ador.MoveNext
        
        sIdiGenerando(1) = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando(2) = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando(3) = Ador(0).Value
        Ador.MoveNext
        sIdiGenerando(4) = Ador(0).Value
        Ador.MoveNext
        
        sIdiSeleccionando = Ador(0).Value
        Ador.MoveNext
        sIdiVisualizando = Ador(0).Value
        Ador.MoveNext

        srIdiGenFecha = Ador(0).Value
        Ador.MoveNext
        srIdiGenPresupuesto = Ador(0).Value
        Ador.MoveNext
        srIdiGenAdjudicado = Ador(0).Value
        Ador.MoveNext
        srIdiGenAhorro = Ador(0).Value
        Ador.MoveNext
        srIdiGenProceso = Ador(0).Value
        Ador.MoveNext
        srIdiGenArticulo = Ador(0).Value
        lblArticulo.caption = Ador(0).Value
        sIdiNiveles(8) = Ador(0).Value
        Ador.MoveNext
        srIdiGenDenomArt = Ador(0).Value
        Ador.MoveNext
        srIdiGenPresupArt = Ador(0).Value
        Ador.MoveNext
        srIdiGenAdjudicadoArt = Ador(0).Value
        Ador.MoveNext
        srIdiGenAhorroArt = Ador(0).Value
        Ador.MoveNext
        
        srIdiMatFecha = Ador(0).Value
        Ador.MoveNext
        srIdiMatNumProc = Ador(0).Value
        Ador.MoveNext
        srIdiMatPresupuesto = Ador(0).Value
        Ador.MoveNext
        srIdiMatAdjudicado = Ador(0).Value
        Ador.MoveNext
        srIdiMatAhorro = Ador(0).Value
        Ador.MoveNext
        srIdiMatProceso = Ador(0).Value
        Ador.MoveNext
        srIdiMatArticulo = Ador(0).Value
        Ador.MoveNext
        srIdiMatDenomArt = Ador(0).Value
        Ador.MoveNext
        
        srIdiEqpFecha = Ador(0).Value
        Ador.MoveNext
        srIdiEqpPresupuesto = Ador(0).Value
        Ador.MoveNext
        srIdiEqpAdjudicado = Ador(0).Value
        Ador.MoveNext
        srIdiEqpAhorro = Ador(0).Value
        Ador.MoveNext
        
        srIdiResFecha = Ador(0).Value
        Ador.MoveNext
        srIdiResNumProc = Ador(0).Value
        Ador.MoveNext
        srIdiResPresupuesto = Ador(0).Value
        Ador.MoveNext
        srIdiResAdjudicado = Ador(0).Value
        Ador.MoveNext
        srIdiResAhorro = Ador(0).Value
        Ador.MoveNext
        srIdiResProceso = Ador(0).Value
        Ador.MoveNext
        srIdiResArticulo = Ador(0).Value
        Ador.MoveNext
        srIdiResDenomArt = Ador(0).Value
        Ador.MoveNext
        srIdiPag = Ador(0).Value
        Ador.MoveNext
        srIdiDe = Ador(0).Value
        Ador.MoveNext
        srIdiLAdjudicado = Ador(0).Value
        Ador.MoveNext
        srIdiLPresupuestado = Ador(0).Value
        Ador.MoveNext
        srIdiLPositivo = Ador(0).Value
        Ador.MoveNext
        srIdiLNegativo = Ador(0).Value
        
        Ador.MoveNext
        sCaption1 = Ador(0).Value
        Ador.MoveNext
        sReunion = Ador(0).Value
        Ador.MoveNext
        sDesde = Ador(0).Value
        Ador.MoveNext
        sReunionRPT = Ador(0).Value
        Ador.MoveNext
        sDesdeRPT = Ador(0).Value
        
        sIdiTitulosFrm(9) = sCaption1 & " - " & gParametrosGenerales.gsSingPres3 & " - " & sDesde
        sIdiTitulosFrm(10) = sCaption1 & " - " & gParametrosGenerales.gsSingPres3 & " - " & sReunion
        sIdiTitulosFrm(11) = sCaption1 & " - " & gParametrosGenerales.gsSingPres4 & " - " & sDesde
        sIdiTitulosFrm(12) = sCaption1 & " - " & gParametrosGenerales.gsSingPres4 & " - " & sReunion
        
        sIdiTitulosRpt(9) = sCaption1 & " - " & gParametrosGenerales.gsSingPres3 & " - " & sDesdeRPT
        sIdiTitulosRpt(10) = sCaption1 & " - " & gParametrosGenerales.gsSingPres3 & " - " & sReunionRPT
        sIdiTitulosRpt(11) = sCaption1 & " - " & gParametrosGenerales.gsSingPres4 & " - " & sDesdeRPT
        sIdiTitulosRpt(12) = sCaption1 & " - " & gParametrosGenerales.gsSingPres4 & " - " & sReunionRPT
        
        Ador.MoveNext
        sdbcFecReu.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(1).caption = Ador(0).Value
        Me.sdbcEqpCod.Columns(1).caption = Ador(0).Value
        Me.sdbcEqpDen.Columns(0).caption = Ador(0).Value
        Ador.MoveNext
        sdbcMon.Columns(2).caption = Ador(0).Value
        Ador.MoveNext
        Me.sdbcEqpCod.Columns(0).caption = Ador(0).Value
        Me.sdbcEqpDen.Columns(1).caption = Ador(0).Value
        Ador.MoveNext
        sIdiNiveles(7) = Ador(0).Value
        Ador.MoveNext
        srIdiEquipo = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(13) = Ador(0).Value
        Ador.MoveNext
        sIdiTitulosRpt(14) = Ador(0).Value
        Ador.MoveNext
        sproveedor = Ador(0).Value
        sIdiNiveles(9) = Ador(0).Value
        Ador.MoveNext
        sFechaInicio = Ador(0).Value
        Ador.MoveNext
        sFechaFin = Ador(0).Value
        
        Ador.Close
    
    End If

   Set Ador = Nothing

End Sub

Public Sub MostrarPresSeleccionado3()
    
    sConcep3_1 = frmSELPresUO.g_sPRES1
    sConcep3_2 = frmSELPresUO.g_sPRES2
    sConcep3_3 = frmSELPresUO.g_sPRES3
    sConcep3_4 = frmSELPresUO.g_sPRES4
    m_sUON1 = frmSELPresUO.g_sUON1
    m_sUON2 = frmSELPresUO.g_sUON2
    m_sUON3 = frmSELPresUO.g_sUON3

    txtEstMat.Text = ""
    If m_sUON1 <> "" Then
        txtEstMat.Text = "(" & m_sUON1
        If m_sUON2 <> "" Then
            txtEstMat.Text = txtEstMat.Text & " - " & m_sUON2
            If m_sUON3 <> "" Then
                txtEstMat.Text = txtEstMat.Text & " - " & m_sUON3 & ") "
            Else
                txtEstMat.Text = txtEstMat.Text & ")"
            End If
        Else
            txtEstMat.Text = txtEstMat.Text & ")"
        End If
    End If
    
    If sConcep3_4 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3 & " - " & sConcep3_4 & " " & frmSELPresUO.g_sDenPres
    ElseIf sConcep3_3 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3 & " " & frmSELPresUO.g_sDenPres
    ElseIf sConcep3_2 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep3_1 & " - " & sConcep3_2 & " " & frmSELPresUO.g_sDenPres
    ElseIf sConcep3_1 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep3_1 & " " & frmSELPresUO.g_sDenPres
    End If
End Sub

Public Sub MostrarPresSeleccionado4()
    sConcep4_1 = frmSELPresUO.g_sPRES1
    sConcep4_2 = frmSELPresUO.g_sPRES2
    sConcep4_3 = frmSELPresUO.g_sPRES3
    sConcep4_4 = frmSELPresUO.g_sPRES4
    m_sUON1 = frmSELPresUO.g_sUON1
    m_sUON2 = frmSELPresUO.g_sUON2
    m_sUON3 = frmSELPresUO.g_sUON3

    txtEstMat.Text = ""
    If m_sUON1 <> "" Then
        txtEstMat.Text = "(" & m_sUON1
        If m_sUON2 <> "" Then
            txtEstMat.Text = txtEstMat.Text & " - " & m_sUON2
            If m_sUON3 <> "" Then
                txtEstMat.Text = txtEstMat.Text & " - " & m_sUON3 & ") "
            Else
                txtEstMat.Text = txtEstMat.Text & ")"
            End If
        Else
            txtEstMat.Text = txtEstMat.Text & ")"
        End If
    End If
    
    If sConcep4_4 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3 & " - " & sConcep4_4 & " " & frmSELPresUO.g_sDenPres
    ElseIf sConcep4_3 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3 & " " & frmSELPresUO.g_sDenPres
    ElseIf sConcep4_2 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep4_1 & " - " & sConcep4_2 & " " & frmSELPresUO.g_sDenPres
    ElseIf sConcep4_1 <> "" Then
        txtEstMat.Text = txtEstMat.Text & sConcep4_1 & " " & frmSELPresUO.g_sDenPres
    End If
    
End Sub



Private Sub ObtenerListadoAhorrosNegConcep3()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim iGraf As Integer
    Dim iNivelPres As Integer
    Dim sTitulo As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim sPresupuesto As String
    Dim FormulaSel As String
    Dim FormulaMon As String

    Set oCRInformes = GenerarCRInformes
    
    If sOrigen = "A4B1C5D2" Or sOrigen = "frmInfAhorroNegConcep3Desde" Then
        sTitulo = sIdiTitulosRpt(9)
    Else
        sTitulo = sIdiTitulosRpt(10)
    End If

    If opVerDat Then
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If
    
    iNivelPres = 0
    If sConcep3_1 <> "" Then iNivelPres = 1
    If sConcep3_2 <> "" Then iNivelPres = 2
    If sConcep3_3 <> "" Then iNivelPres = 3
    If sConcep3_4 <> "" Then iNivelPres = 4
    
    Screen.MousePointer = vbHourglass
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegPres3.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
      
      
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        
        End If
    Else
        sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
    
    End If
    
    
    sPresupuesto = ""
    If sConcep3_4 <> "" Then
            sPresupuesto = " " & gParametrosGenerales.gsSingPres3 & ": " & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3 & " - " & sConcep3_4
    Else
        If sConcep3_3 <> "" Then
                sPresupuesto = " " & gParametrosGenerales.gsSingPres3 & " " & sConcep3_1 & " - " & sConcep3_2 & " - " & sConcep3_3
       Else
            If sConcep3_2 <> "" Then
                    sPresupuesto = " " & gParametrosGenerales.gsSingPres3 & " " & sConcep3_1 & " - " & sConcep3_2
            Else
                If sConcep3_1 <> "" Then
                        sPresupuesto = " " & gParametrosGenerales.gsSingPres3 & " " & sConcep3_1
                End If
            End If
        End If
    End If

    If optReu Then
        sSeleccion = sSeleccion & sIdiCriSoloReu & ". "
    Else
        If optDir Then
            sSeleccion = sSeleccion & sIdiCriSoloAdj & ". "
        Else
            sSeleccion = sSeleccion & sIdiCriReuYAdj & ". "
        End If
    End If
      
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaMon = ""
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
    
    'SE ABRE EL REPORT
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SELMATERIAL")).Text = """" & sPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"
        
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN1_NOM")).Text = """" & gParametrosGenerales.gsSingPres3 & """"
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECHA")).Text = """" & srIdiMatFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNUMPROC")).Text = """" & srIdiMatNumProc & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiMatPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiMatAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiMatAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiMatProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtARTICULO")).Text = """" & srIdiMatArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & sIdiNiveles(7) & """"
    
    If IsEmpty(dFechaHasta) Then
        oCRInformes.ListadoAhorroNegPres3 oReport, dFechaDesde, , sConcep3_1, sConcep3_2, sConcep3_3, sConcep3_4, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelPres, m_sUON1, m_sUON2, m_sUON3
    Else
        oCRInformes.ListadoAhorroNegPres3 oReport, dFechaDesde, dFechaHasta, sConcep3_1, sConcep3_2, sConcep3_3, sConcep3_4, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelPres, m_sUON1, m_sUON2, m_sUON3
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    

End Sub


Private Sub ObtenerListadoAhorrosNegConcep4()
    Dim oReport As Object
    Dim oCRInformes As CRInformes
    Dim pv As Preview
    Dim iGraf As Integer
    Dim iNivelPres As Integer
    Dim sTitulo As String
    Dim RepPath As String
    Dim oFos As FileSystemObject
    Dim sSeleccion As String
    Dim sPresupuesto As String
    Dim FormulaSel As String
    Dim FormulaMon As String

    Set oCRInformes = GenerarCRInformes
    
    If sOrigen = "frmInfAhorroNegConcep4Desde" Or sOrigen = "A4B1C6D2" Then
        sTitulo = sIdiTitulosRpt(11)
    Else
        sTitulo = sIdiTitulosRpt(12)
    End If

    If opVerDat Then
        iGraf = 0
    Else
        If opVerGraf Then
            iGraf = 1
        Else
            iGraf = 2
        End If
    End If
    
    iNivelPres = 0
    If sConcep4_1 <> "" Then iNivelPres = 1
    If sConcep4_2 <> "" Then iNivelPres = 2
    If sConcep4_3 <> "" Then iNivelPres = 3
    If sConcep4_4 <> "" Then iNivelPres = 4
    
    Screen.MousePointer = vbHourglass
    
    
    If crs_Connected = False Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    If gParametrosInstalacion.gsRPTPATH = "" Then
        If gParametrosGenerales.gsRPTPATH = "" Then
            Screen.MousePointer = vbNormal
            oMensajes.RutaDeRPTNoValida
            Exit Sub
        Else
            gParametrosInstalacion.gsRPTPATH = gParametrosGenerales.gsRPTPATH
            g_GuardarParametrosIns = True
        End If
    End If
    
    RepPath = gParametrosInstalacion.gsRPTPATH & "\rptAhorroNegPres4.rpt"
    Set oFos = New FileSystemObject
    If Not oFos.FileExists(RepPath) Then
        Screen.MousePointer = vbNormal
        oMensajes.RutaDeRPTNoValida
        Set oFos = Nothing
        Exit Sub
    End If
    Set oFos = Nothing
      
      
    If Not IsEmpty(dFechaHasta) Then
        If dFechaDesde <> dFechaHasta Then
            sSeleccion = sIdiFechas & " " & CStr(dFechaDesde) & " - " & CStr(dFechaHasta) & ". "
        Else
            sSeleccion = sIdiFecReu & " " & Format(dFechaDesde, "short date") & " " & Format(dFechaDesde, "short time") & ". "
        
        End If
    Else
        sSeleccion = sIdiFechaDesde & " " & CStr(dFechaDesde) & ". "
    
    End If
    
    
    sPresupuesto = ""
    If sConcep4_4 <> "" Then
            sPresupuesto = " " & gParametrosGenerales.gsSingPres4 & ": " & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3 & " - " & sConcep4_4
    Else
        If sConcep4_3 <> "" Then
                sPresupuesto = " " & gParametrosGenerales.gsSingPres4 & " " & sConcep4_1 & " - " & sConcep4_2 & " - " & sConcep4_3
       Else
            If sConcep4_2 <> "" Then
                    sPresupuesto = " " & gParametrosGenerales.gsSingPres4 & " " & sConcep4_1 & " - " & sConcep4_2
            Else
                If sConcep4_1 <> "" Then
                        sPresupuesto = " " & gParametrosGenerales.gsSingPres4 & " " & sConcep4_1
                End If
            End If
        End If
    End If


    If optReu Then
        sSeleccion = sSeleccion & sIdiCriSoloReu & ". "
    Else
        If optDir Then
            sSeleccion = sSeleccion & sIdiCriSoloAdj & ". "
        Else
            sSeleccion = sSeleccion & sIdiCriReuYAdj & ". "
        End If
    End If
  
    'Selecci�n que se lista, formula 1 - @SEL
    FormulaSel = ""
    If sSeleccion <> "" Then
        FormulaSel = sIdiCriSeleccion & " " & Trim(sSeleccion)
    End If
    FormulaMon = ""
    If sMonedaOrig = sMoneda Then
        FormulaMon = sIdiCriMoneda & " " & sMoneda
    Else
        FormulaMon = sIdiCriMoneda & " " & sMoneda & " = " & CStr(dequivalencia) & " " & sMonedaOrig
    End If
    
    'SE ABRE EL REPORT
    Set oReport = crs_crapp.OpenReport(RepPath, crOpenReportByTempCopy)
    
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SEL")).Text = """" & FormulaSel & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "SELMATERIAL")).Text = """" & sPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "MONEDA")).Text = """" & FormulaMon & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "TITULO")).Text = """" & sTitulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "GMN1_NOM")).Text = """" & gParametrosGenerales.gsSingPres4 & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtFECHA")).Text = """" & srIdiMatFecha & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtNUMPROC")).Text = """" & srIdiMatNumProc & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPRESUPUESTO")).Text = """" & srIdiMatPresupuesto & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtADJUDICADO")).Text = """" & srIdiMatAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtAHORRO")).Text = """" & srIdiMatAhorro & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPROCESO")).Text = """" & srIdiMatProceso & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtARTICULO")).Text = """" & srIdiMatArticulo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtPAG")).Text = """" & srIdiPag & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtDE")).Text = """" & srIdiDe & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLADJUDICADO")).Text = """" & srIdiLAdjudicado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPRESUPUESTADO")).Text = """" & srIdiLPresupuestado & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLPOSITIVO")).Text = """" & srIdiLPositivo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtLNEGATIVO")).Text = """" & srIdiLNegativo & """"
    oReport.FormulaFields(crs_FormulaIndex(oReport, "txtGrupo")).Text = """" & sIdiNiveles(7) & """"
    
    If IsEmpty(dFechaHasta) Then
        oCRInformes.ListadoAhorroNegPres4 oReport, dFechaDesde, , sConcep4_1, sConcep4_2, sConcep4_3, sConcep4_4, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelPres, m_sUON1, m_sUON2, m_sUON3
    Else
        oCRInformes.ListadoAhorroNegPres4 oReport, dFechaDesde, dFechaHasta, sConcep4_1, sConcep4_2, sConcep4_3, sConcep4_4, iNivel, dequivalencia, optReu, optDir, iGraf, iNivelPres, m_sUON1, m_sUON2, m_sUON3
    End If
    
    If oReport Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
        
    frmESPERA.lblGeneral.caption = sIdiGenerando(2)
    frmESPERA.Top = MDI.Height / 2 - frmESPERA.Height / 2
    frmESPERA.Left = MDI.Width / 2 - frmESPERA.Width / 2
    frmESPERA.Frame2.caption = ""
    frmESPERA.Frame3.caption = ""
    frmESPERA.lblContacto = sIdiSeleccionando
    frmESPERA.lblDetalle = sIdiVisualizando
    Me.Hide
    Set pv = New Preview
    pv.Hide
    pv.caption = sTitulo
    Set pv.g_oReport = oReport
    pv.crViewer.ReportSource = oReport
    
    frmESPERA.Show
    
    Timer1.Enabled = True

    pv.crViewer.ViewReport
    pv.crViewer.Visible = True
    DoEvents
    pv.Show
    
    
    Timer1.Enabled = False
    Unload frmESPERA
    Unload Me

    Screen.MousePointer = vbNormal
    

End Sub

Private Sub sdbcArtiCod_Change()
    
    If Not bRespetarCombo Then
    
        bRespetarCombo = True
        sdbcArtiDen.Text = ""
        bRespetarCombo = False
        bCargarComboDesde = True
        
    End If
End Sub


Private Sub sdbcArtiCod_Click()
    If Not sdbcArtiCod.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiCod_CloseUp()
    If sdbcArtiCod.Value = "..." Then
        sdbcArtiCod.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiCod.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcArtiDen.Text = sdbcArtiCod.Columns(1).Text
    sdbcArtiCod.Text = sdbcArtiCod.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiCod_DropDown()
    Dim oArt As CArticulo
    
    sdbcArtiCod.RemoveAll

    Screen.MousePointer = vbHourglass
    'GMN4Seleccionado
    If oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos
    For Each oArt In oGMN4Seleccionado.ARTICULOS
        sdbcArtiCod.AddItem oArt.Cod & Chr(m_lSeparador) & oArt.Den
    Next

    sdbcArtiCod.SelStart = 0
    sdbcArtiCod.SelLength = Len(sdbcArtiCod.Text)
    sdbcArtiCod.Refresh
    
    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiCod_InitColumnProps()
    sdbcArtiCod.DataFieldList = "Column 0"
    sdbcArtiCod.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcArtiCod_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArtiCod.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcArtiCod.Rows - 1
            bm = sdbcArtiCod.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcArtiCod.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcArtiCod.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Public Sub sdbcArtiCod_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcArtiCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, sdbcArtiCod, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4Seleccionado.ARTICULOS.Count = 0)
            
    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiCod.Text = ""
    Else
        bRespetarCombo = True
        sdbcArtiDen.Text = oGMN4Seleccionado.ARTICULOS.Item(1).Den
        
        sdbcArtiCod.Columns(0).Value = sdbcArtiCod.Text
        sdbcArtiCod.Columns(1).Value = sdbcArtiDen.Text
        
        bRespetarCombo = False
        bCargarComboDesde = False
    End If
    If Not oGMN4Seleccionado Is Nothing Then
        Set oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub

Private Sub sdbcArtiDen_Click()
    
    If Not sdbcArtiDen.DroppedDown Then
        sdbcArtiCod = ""
        sdbcArtiDen = ""
    End If
End Sub

Private Sub sdbcArtiDen_CloseUp()
    
    If sdbcArtiDen.Value = "..." Then
        sdbcArtiDen.Text = ""
        Exit Sub
    End If
    
    If sdbcArtiDen.Value = "" Then Exit Sub
    
    bRespetarCombo = True
    sdbcArtiCod.Text = sdbcArtiDen.Columns(1).Text
    sdbcArtiDen.Text = sdbcArtiDen.Columns(0).Text
    bRespetarCombo = False
    
    bCargarComboDesde = False
End Sub

Private Sub sdbcArtiDen_Change()
    
    If Not bRespetarCombo Then
        bRespetarCombo = True
        bRespetarCombo = False
        bCargarComboDesde = True
    End If
End Sub

Private Sub sdbcArtiDen_DropDown()

    Dim oArt As CArticulo
    
    sdbcArtiDen.RemoveAll
   
    Screen.MousePointer = vbHourglass
    'GMN4Seleccionado

    If oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbNormal
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    If bCargarComboDesde Then
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
    Else
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , , , True
    End If
    
    
    For Each oArt In oGMN4Seleccionado.ARTICULOS
   
        sdbcArtiDen.AddItem oArt.Den & Chr(m_lSeparador) & oArt.Cod
   
    Next

    sdbcArtiDen.SelStart = 0
    sdbcArtiDen.SelLength = Len(sdbcArtiDen.Text)
    sdbcArtiDen.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcArtiDen_InitColumnProps()
    sdbcArtiDen.DataFieldList = "Column 0"
    sdbcArtiDen.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbcArtiDen_PositionList(ByVal Text As String)
''' * Objetivo: Posicionarse en el combo segun la seleccion
    
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbcArtiDen.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbcArtiDen.Rows - 1
            bm = sdbcArtiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcArtiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcArtiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbcArtiDen_Validate(Cancel As Boolean)
    Dim bExiste As Boolean
    
    If sdbcArtiDen.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el grupo
    
    If Not oGMN4Seleccionado Is Nothing Then
        Screen.MousePointer = vbHourglass
        oGMN4Seleccionado.CargarTodosLosArticulos gParametrosInstalacion.giCargaMaximaCombos, , sdbcArtiDen, , True
        Screen.MousePointer = vbNormal
        
        bExiste = Not (oGMN4Seleccionado.ARTICULOS.Count = 0)

    Else
        bExiste = False
    End If
    
    If Not bExiste Then
        sdbcArtiDen.Text = ""
    Else
        bRespetarCombo = True
        sdbcArtiCod.Text = oGMN4Seleccionado.ARTICULOS.Item(1).Cod
        
        sdbcArtiDen.Columns(0).Value = sdbcArtiDen.Text
        sdbcArtiDen.Columns(1).Value = sdbcArtiCod.Text
        
        bRespetarCombo = False
        bCargarComboDesde = False
    End If
    If Not oGMN4Seleccionado Is Nothing Then
       Set oGMN4Seleccionado.ARTICULOS = Nothing
    End If
End Sub
