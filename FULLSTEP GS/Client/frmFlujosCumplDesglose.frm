VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmFlujosCumplDesglose 
   Caption         =   "DDesglose"
   ClientHeight    =   5550
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10470
   Icon            =   "frmFlujosCumplDesglose.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5550
   ScaleWidth      =   10470
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkVincularPermitirMover 
      Caption         =   "DPermitir mover l�neas de desglose a solicitudes de este mismo tipo"
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   0
      Width           =   9375
   End
   Begin VB.CommandButton cmdSubirCampo 
      Height          =   312
      Left            =   60
      Picture         =   "frmFlujosCumplDesglose.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   300
      Width           =   375
   End
   Begin VB.CommandButton cmdBajarCampo 
      Height          =   312
      Left            =   60
      Picture         =   "frmFlujosCumplDesglose.frx":0D0C
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   660
      Width           =   375
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgCampos 
      Height          =   5175
      Left            =   480
      TabIndex        =   2
      Top             =   300
      Width           =   9435
      _Version        =   196617
      DataMode        =   2
      GroupHeaders    =   0   'False
      Col.Count       =   11
      stylesets.count =   8
      stylesets(0).Name=   "Bloqueo"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmFlujosCumplDesglose.frx":0D66
      stylesets(0).AlignmentPicture=   4
      stylesets(1).Name=   "No"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmFlujosCumplDesglose.frx":0EE7
      stylesets(1).AlignmentPicture=   2
      stylesets(2).Name=   "Calculado"
      stylesets(2).BackColor=   16766421
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmFlujosCumplDesglose.frx":0F03
      stylesets(2).AlignmentPicture=   1
      stylesets(3).Name=   "S�"
      stylesets(3).HasFont=   -1  'True
      BeginProperty stylesets(3).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(3).Picture=   "frmFlujosCumplDesglose.frx":0F6A
      stylesets(3).AlignmentPicture=   2
      stylesets(4).Name=   "Gris"
      stylesets(4).BackColor=   -2147483633
      stylesets(4).HasFont=   -1  'True
      BeginProperty stylesets(4).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(4).Picture=   "frmFlujosCumplDesglose.frx":0F86
      stylesets(5).Name=   "Normal"
      stylesets(5).HasFont=   -1  'True
      BeginProperty stylesets(5).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(5).Picture=   "frmFlujosCumplDesglose.frx":0FA2
      stylesets(6).Name=   "ActiveRow"
      stylesets(6).ForeColor=   16777215
      stylesets(6).BackColor=   8388608
      stylesets(6).HasFont=   -1  'True
      BeginProperty stylesets(6).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(6).Picture=   "frmFlujosCumplDesglose.frx":0FBE
      stylesets(7).Name=   "Amarillo"
      stylesets(7).BackColor=   12648447
      stylesets(7).HasFont=   -1  'True
      BeginProperty stylesets(7).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Small Fonts"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(7).Picture=   "frmFlujosCumplDesglose.frx":0FDA
      DividerType     =   2
      BevelColorHighlight=   16777215
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   1
      HeadStyleSet    =   "Normal"
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   370
      ExtraHeight     =   106
      Columns.Count   =   11
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "ID"
      Columns(0).Name =   "ID"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   3
      Columns(0).FieldLen=   256
      Columns(1).Width=   1931
      Columns(1).Name =   "ATRIBUTO"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777160
      Columns(2).Width=   8731
      Columns(2).Caption=   "DDato"
      Columns(2).Name =   "DATO"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "TIPO"
      Columns(3).Name =   "TIPO"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   2
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "CAMPO_GS"
      Columns(4).Name =   "CAMPO_GS"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   2
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "SUBTIPO"
      Columns(5).Name =   "SUBTIPO"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   2
      Columns(5).FieldLen=   256
      Columns(6).Width=   1376
      Columns(6).Caption=   "Dvis."
      Columns(6).Name =   "VISIBLE"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   11
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(7).Width=   1376
      Columns(7).Caption=   "DEsc."
      Columns(7).Name =   "ESCRITURA"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   11
      Columns(7).FieldLen=   256
      Columns(7).Style=   2
      Columns(8).Width=   529
      Columns(8).Name =   "BLOQUEO"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Style=   4
      Columns(8).ButtonsAlways=   -1  'True
      Columns(8).HeadStyleSet=   "Bloqueo"
      Columns(8).StyleSet=   "Bloqueo"
      Columns(9).Width=   1376
      Columns(9).Caption=   "DObl."
      Columns(9).Name =   "OBLIGATORIO"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   11
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "INTRO"
      Columns(10).Name=   "INTRO"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   11
      Columns(10).FieldLen=   256
      _ExtentX        =   16642
      _ExtentY        =   9128
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmFlujosCumplDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables Publicas
Public mlBloque As Long
Public mlRol As Long
Public mlGrupo As Long
Public mlCampoPadre As Long
Public msBloque As String
Public msRol As String
Public msGrupo As String
Public msCampoPadre As String
Public m_bModifFlujo As Boolean
Public m_udtTipoBloque As TipoBloque
Public mlFormulario As Long
Public mbVinculado As Boolean

'Variables Privadas
Private msCaptionBloque As String
Private msCaptionRol As String
Private msCaptionGrupo As String
Private msCaptionCampo As String
Private msTextoSolicitudVinculada As String

Private msDato As String
Private msVisible As String
Private msEscritura As String
Private msObligatorio As String
Private m_bCambiaOrden As Boolean

Private m_bErrorCumplimentaciones As Boolean
Private moCumplimentaciones As CPMConfCumplimentaciones

Private m_bFormLoad As Boolean

''' <summary>
''' Cambia la configuraci�n de cumplimentaci�n del desglose actual. La propiedad q indica si se permite mover o no lineas vinculadas
''' </summary>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo:0,1</remarks>
Private Sub chkVincularPermitirMover_Click()
    Dim oCumplimentacion As CPMConfCumplimentacion
    Dim teserror As TipoErrorSummit
    
    If m_bFormLoad Then Exit Sub
    
    Set oCumplimentacion = moCumplimentaciones.Item(CStr(mlCampoPadre))
    
    If Not oCumplimentacion Is Nothing Then
        oCumplimentacion.PermitirMover = (Me.chkVincularPermitirMover.Value = 1)
    
        teserror = oCumplimentacion.ModificarMoverCumplimentacion
        If teserror.NumError <> TESnoerror Then
            basErrores.TratarError teserror
        Else
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
        End If
        Set oCumplimentacion = Nothing
    End If
End Sub

Private Sub cmdBajarCampo_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim bOrdenoMat As Boolean, bOrdenoPais As Boolean
Dim bOrdenoArt As Boolean, bOrdenoProvi As Boolean
Dim bOrdenoCampoMat As Boolean, bOrdenoCampoPais As Boolean
Dim bOrdenoDen As Boolean
Dim bNoGrabarOrden As Boolean
Dim vbm As Variant
Dim Campo_Gs As Variant

    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    
    If sdbgCampos.AddItemRowIndex(sdbgCampos.SelBookmarks.Item(0)) = sdbgCampos.Rows - 1 Then Exit Sub
        
    m_bCambiaOrden = True
    i = 0
    ReDim arrValoresMat(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresArt(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresDen(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresAux(sdbgCampos.Columns.Count - 1)
    
    'Ordenacion del Material // Provincia
    If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue((sdbgCampos.GetBookmark(1))) = TipoCampoGS.provincia) Or _
           (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue((sdbgCampos.GetBookmark(1))) = TipoCampoGS.CodArticulo) Or sdbgCampos.Columns("CAMPO_GS").CellValue((sdbgCampos.GetBookmark(1))) = TipoCampoGS.NuevoCodArticulo)) Then
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.DenArticulo) Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 3 Then Exit Sub
                bOrdenoMat = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoPais = True
            End If
                
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).Value
            Next i
        
    'Ordenacion del articulo // Provincia
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Then
            vbm = sdbgCampos.GetBookmark(-1)
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.provincia Or sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) <> TipoCampoGS.DenArticulo Then
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
                bOrdenoProvi = True
            Else
                If val(sdbgCampos.Row) = sdbgCampos.Rows - 2 Then Exit Sub
                bOrdenoArt = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
            Next i
        
    'Ordenacion de la denominacion
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) Then
        If sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.NuevoCodArticulo Then
            If val(sdbgCampos.Row) = sdbgCampos.Rows - 1 Then Exit Sub
            bOrdenoDen = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresDen(i) = sdbgCampos.Columns(i).Value
            Next i
        End If
    Else
        vbm = sdbgCampos.GetBookmark(1)
        'Por si debajo del campo a bajar, esta el Material // Provincia
        If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.provincia) Or _
           (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.CodArticulo) Or (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) = TipoCampoGS.NuevoCodArticulo))) Then
            
            If (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(3)) = TipoCampoGS.DenArticulo) Then
                bOrdenoCampoMat = True
            Else
                bOrdenoCampoPais = True
            End If
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(3))
            Next i
        End If
    End If
    
    
    If bOrdenoMat Or bOrdenoPais Then
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        'Comprobaciones para el campo "Desglose Vinculado"
        If bOrdenoMat Then
            sdbgCampos.MoveNext
            sdbgCampos.MoveNext
            sdbgCampos.MoveNext
            Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        Else
            sdbgCampos.MoveNext
            sdbgCampos.MoveNext
            Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        If Campo_Gs = idsficticios.DesgloseVinculado Then
            GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), -1
            bNoGrabarOrden = True
        End If
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdenoMat Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoMat Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'pongo la 3� fila donde antes estaba el pa�s.
            Next i
        End If
        
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll

    ElseIf bOrdenoArt Or bOrdenoProvi Then
        'Comprobaciones para el campo "Desglose Vinculado"
        If bOrdenoArt Then
            sdbgCampos.MoveNext
            sdbgCampos.MoveNext
            Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
            sdbgCampos.MovePrevious
            sdbgCampos.MovePrevious
        Else
            sdbgCampos.MoveNext
            Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
            sdbgCampos.MovePrevious
        End If
        'Ordeno conjuntamente los campos de material y art�culo y de pa�s y provincia
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)  'donde est� la provincia pongo el pa�s
        Next i
        If Campo_Gs = idsficticios.DesgloseVinculado Then
            GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), -1
            bNoGrabarOrden = True
        End If
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If bOrdenoArt Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
            sdbgCampos.MovePrevious
        End If
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If bOrdenoArt Then
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresAux(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)  'donde estaba el pa�s pongo el contenido de la 3� fila
            Next i
        End If
        
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll
    
    ElseIf bOrdenoCampoMat Or bOrdenoCampoPais Then
        Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
        
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If bOrdenoCampoMat Then
            sdbgCampos.MoveNext
        End If
        If Campo_Gs = idsficticios.DesgloseVinculado Then
            GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), 0
            bNoGrabarOrden = True
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        If bOrdenoCampoMat Then
            sdbgCampos.MovePrevious
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        End If
      
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        If bOrdenoCampoMat Then
            sdbgCampos.MoveNext
        End If
        sdbgCampos.SelBookmarks.RemoveAll
        
    ElseIf bOrdenoDen = True Then
        sdbgCampos.MoveNext
        Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        If Campo_Gs = idsficticios.DesgloseVinculado Then
            GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), -2
            bNoGrabarOrden = True
        End If
        
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresDen(i)
        Next i
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = idsficticios.DesgloseVinculado) Then
        'Campo de desglose Vinculado
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
        
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), 0
        
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll
    Else  'Ordeno normal,de 1 en 1
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
        
        sdbgCampos.MoveNext
        Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        'Si es el campo "Desglose Vinculado" no se modificara el orden como normalmente
        If Campo_Gs = idsficticios.DesgloseVinculado Then
            bNoGrabarOrden = True
            GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), 0
        End If
        
        sdbgCampos.MovePrevious
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.MoveNext
        sdbgCampos.SelBookmarks.RemoveAll
    End If
    sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
    sdbgCampos.Update
End Sub

''Graba un 1 al campo que esta antes del campo "Desglose Vinculado" para al abrir la pantalla saber en que posicion crear ese campo
Sub GrabarPrevioDesgloseVinculado(idx As String, iOrden As Integer)
Dim oCumplimentacion As CPMConfCumplimentacion
Dim teserror As TipoErrorSummit

Set oCumplimentacion = moCumplimentaciones.Item(idx)

teserror = oCumplimentacion.GrabarPrevioDesgloseVinculado(iOrden, mlCampoPadre)

If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    Set oCumplimentacion = Nothing
    Exit Sub
End If
Set oCumplimentacion = Nothing
'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
frmFlujos.HayCambios

End Sub

Private Sub cmdSubirCampo_Click()
Dim i As Integer
Dim arrValoresMat() As Variant
Dim arrValoresArt() As Variant
Dim arrValoresDen() As Variant
Dim arrValoresAux() As Variant
Dim Campo_Gs As Variant
Dim bOrdenoMat As Boolean, bOrdenoPais As Boolean
Dim bOrdenoArt As Boolean, bOrdenoProv As Boolean
Dim bOrdenoDen As Boolean
Dim bOrdenoCampoMat As Boolean, bOrdenoCampoProv As Boolean
Dim bNoGrabarOrden As Boolean
Dim iOrdenPrevio As Integer
Dim vbm As Variant

    If sdbgCampos.SelBookmarks.Count = 0 Then Exit Sub
    
    If sdbgCampos.AddItemRowIndex(sdbgCampos.SelBookmarks.Item(0)) = 0 Then Exit Sub
    
    m_bCambiaOrden = True
    ReDim arrValoresMat(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresArt(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresDen(sdbgCampos.Columns.Count - 1)
    ReDim arrValoresAux(sdbgCampos.Columns.Count - 1)
    i = 0
    '''Ordenacion del Material // Pais
    If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material) Or (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais) Then
        vbm = sdbgCampos.GetBookmark(1)
        If (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Pais And sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Or _
           (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.material And ((sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo) Or (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo))) Then
           
            If (sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia) Or (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(2)) <> TipoCampoGS.DenArticulo) Then
                bOrdenoPais = True
                For i = 0 To sdbgCampos.Columns.Count - 1
                   arrValoresMat(i) = sdbgCampos.Columns(i).Value
                   arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(vbm)
                
                Next i
            Else
                bOrdenoMat = True
                For i = 0 To sdbgCampos.Columns.Count - 1
                    arrValoresMat(i) = sdbgCampos.Columns(i).Value
                    arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(vbm)
                    arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(2))
                Next i
            End If
           
        Else
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresAux(i) = sdbgCampos.Columns(i).Value
            Next i
           
        End If
    'Ordenacion del c�digo del art�culo // Provincia
    ElseIf sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.CodArticulo Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NuevoCodArticulo Or _
     sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia Then
        If val(sdbgCampos.Row) = 1 Then Exit Sub
        vbm = sdbgCampos.GetBookmark(-1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.material And sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(1)) = TipoCampoGS.DenArticulo Then
            bOrdenoArt = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
                arrValoresDen(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(1))
            Next i
        Else
            bOrdenoProv = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(vbm)
                arrValoresArt(i) = sdbgCampos.Columns(i).Value
            Next i
        End If

    'Ordenacion de la denominacion
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.DenArticulo) And (sdbgCampos.Columns("CAMPO_GS").CellValue(sdbgCampos.GetBookmark(-1)) = TipoCampoGS.NuevoCodArticulo) Then
        If val(sdbgCampos.Row) = 2 Then Exit Sub
        
            bOrdenoDen = True
            For i = 0 To sdbgCampos.Columns.Count - 1
                arrValoresMat(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-2))
                arrValoresArt(i) = sdbgCampos.Columns(i).CellValue(sdbgCampos.GetBookmark(-1))
                arrValoresDen(i) = sdbgCampos.Columns(i).Value
            Next i
        
    Else
        vbm = sdbgCampos.GetBookmark(-1)
        If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.provincia Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.CodArticulo Or _
         sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.NuevoCodArticulo Then
           If sdbgCampos.Columns("CAMPO_GS").CellValue(vbm) = TipoCampoGS.DenArticulo Then
               bOrdenoCampoMat = True
           Else
               bOrdenoCampoProv = True
           End If
        End If
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
         
    End If

    If bOrdenoMat Or bOrdenoArt Or bOrdenoDen Or bOrdenoPais Or bOrdenoProv Then
        sdbgCampos.MovePrevious
        If bOrdenoArt Or bOrdenoDen Or bOrdenoProv Then
            iOrdenPrevio = 1
            sdbgCampos.MovePrevious
        End If
        
        If bOrdenoDen Then
            iOrdenPrevio = 2
            sdbgCampos.MovePrevious
        End If
        
        Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        If Campo_Gs = idsficticios.DesgloseVinculado Then
            GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), iOrdenPrevio
            bNoGrabarOrden = True
        End If
        
        sdbgCampos.MoveNext
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        
        If Not bOrdenoPais And Not bOrdenoProv Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        End If
        
        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        
        sdbgCampos.SelBookmarks.RemoveAll
        
        If Not bOrdenoArt And Not bOrdenoDen And Not bOrdenoPais And Not bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        If Not bOrdenoDen And Not bOrdenoProv Then
            sdbgCampos.MovePrevious
        End If
        sdbgCampos.MovePrevious

    ElseIf bOrdenoCampoMat Or bOrdenoCampoProv Then
        Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
        
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If Not bOrdenoCampoProv Then
            sdbgCampos.MovePrevious
        End If

        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        If bOrdenoCampoProv Then
            If Campo_Gs = idsficticios.DesgloseVinculado Then
                GrabarPrevioDesgloseVinculado CStr(arrValoresMat(0)), -1
                bNoGrabarOrden = True
            End If
        Else
            If Campo_Gs = idsficticios.DesgloseVinculado Then
                GrabarPrevioDesgloseVinculado CStr(arrValoresMat(0)), -1
                bNoGrabarOrden = True
            End If
        End If

        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresArt(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i

        sdbgCampos.MoveNext
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresDen(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresArt(i)
        Next i
        
        If Not bOrdenoCampoProv Then
            sdbgCampos.MoveNext
            For i = 0 To sdbgCampos.Columns.Count - 1
                sdbgCampos.Columns(i).Value = arrValoresDen(i)
            Next i
        End If

        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MovePrevious
        sdbgCampos.MovePrevious
        If Not bOrdenoCampoProv Then
            sdbgCampos.MovePrevious
        End If
    ElseIf (sdbgCampos.Columns("CAMPO_GS").Value = idsficticios.DesgloseVinculado) Then
        'Campo de desglose Vinculado
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresAux(i) = sdbgCampos.Columns(i).Value
        Next i
        
        sdbgCampos.MovePrevious
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        sdbgCampos.MoveNext
        
        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), -1
        
        
        sdbgCampos.MovePrevious
        sdbgCampos.SelBookmarks.RemoveAll
    Else  'Ordeno normal,de 1 en 1
        
        sdbgCampos.MovePrevious
        
        Campo_Gs = sdbgCampos.Columns("CAMPO_GS").Value
        For i = 0 To sdbgCampos.Columns.Count - 1
            arrValoresMat(i) = sdbgCampos.Columns(i).Value
            sdbgCampos.Columns(i).Value = arrValoresAux(i)
        Next i
        If Campo_Gs = idsficticios.DesgloseVinculado Then
            GrabarPrevioDesgloseVinculado CStr(sdbgCampos.Columns("ID").Value), 0
            bNoGrabarOrden = True
        End If
        
        sdbgCampos.MoveNext

        For i = 0 To sdbgCampos.Columns.Count - 1
            sdbgCampos.Columns(i).Value = arrValoresMat(i)
        Next i
        sdbgCampos.SelBookmarks.RemoveAll
        sdbgCampos.MovePrevious
   End If

    sdbgCampos.SelBookmarks.Add sdbgCampos.Bookmark
    sdbgCampos.Update

End Sub

Private Sub Form_Load()

    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    Me.caption = msCaptionBloque & " " & msBloque & " - " & msCaptionRol & "  " & msRol & " - " & msCaptionGrupo & " " & msGrupo & " - " & msCaptionCampo & " " & msCampoPadre
    ConfigurarSeguridad '?
    PonerFieldSeparator Me
    CargarCumplimentacion
End Sub

Private Sub CargarRecursos()
Dim Ador As Ador.Recordset

    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_FLUJOSCUMPLDESGLOSE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
            
        msCaptionBloque = Ador(0).Value '1
        Ador.MoveNext
        msCaptionRol = Ador(0).Value
        Ador.MoveNext
        msCaptionGrupo = Ador(0).Value
        Ador.MoveNext
        msCaptionCampo = Ador(0).Value
        Ador.MoveNext
        cmdSubirCampo.ToolTipText = Ador(0).Value '5
        Ador.MoveNext
        cmdBajarCampo.ToolTipText = Ador(0).Value
        Ador.MoveNext
        msDato = Ador(0).Value
        Ador.MoveNext
        msVisible = Ador(0).Value
        Ador.MoveNext
        msEscritura = Ador(0).Value
        Ador.MoveNext
        msObligatorio = Ador(0).Value '10
        Ador.MoveNext
        chkVincularPermitirMover.caption = Ador(0).Value 'Permitir mover l�neas de desglose a solicitudes de este mismo tipo
        Ador.MoveNext
        msTextoSolicitudVinculada = Ador(0).Value
        Ador.Close
    End If
End Sub
Private Sub ConfigurarSeguridad()
    If Not m_bModifFlujo Then
        cmdSubirCampo.Enabled = False
        cmdBajarCampo.Enabled = False
        sdbgCampos.AllowUpdate = False
    End If
    Me.chkVincularPermitirMover.Visible = Not (m_udtTipoBloque = TipoBloque.Peticionario) And gParametrosGenerales.gbAccesoFSGA
End Sub
''' Cargar la configuraci�n de cumplimentaci�n para un rol, bloque y campo desglose
Private Sub CargarCumplimentacion()
    Dim oBloque As CBloque
    Dim oCumplimentacion As CPMConfCumplimentacion
    Dim bCrearCampoVinculado As Boolean
    
    sdbgCampos.RemoveAll
    If mlBloque > 0 Then
        Set oBloque = oFSGSRaiz.Generar_CBloque
        oBloque.Id = mlBloque

        If mlRol > 0 And mlCampoPadre > 0 Then
            oBloque.CargarCumplimentacion mlRol, mlGrupo, mlCampoPadre
            Set moCumplimentaciones = oBloque.Cumplimentaciones
            If Not moCumplimentaciones Is Nothing Then
                'Si no se ha cambiado el orden del campo de "solicitud vinculada" se pondra en la primera posicion
                If Not moCumplimentaciones.Vinculado_Ordenado And mbVinculado = True Then
                    sdbgCampos.AddItem "0" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & msTextoSolicitudVinculada & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & idsficticios.DesgloseVinculado & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0"
                End If
            
                For Each oCumplimentacion In moCumplimentaciones
                    If oCumplimentacion.Campo.Previo_A_Vinculado And mbVinculado = True Then
                        bCrearCampoVinculado = True
                    End If
                    If Not (mlCampoPadre = oCumplimentacion.IdCampo) Then
                        sdbgCampos.AddItem oCumplimentacion.IdCampo & Chr(m_lSeparador) & oCumplimentacion.CodAtrib & Chr(m_lSeparador) & oCumplimentacion.Campo.Denominaciones.Item(basPublic.gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oCumplimentacion.Campo.TipoPredef & Chr(m_lSeparador) & oCumplimentacion.Campo.CampoGS & Chr(m_lSeparador) & oCumplimentacion.Campo.Tipo & Chr(m_lSeparador) & IIf(oCumplimentacion.Visible, 1, 0) & Chr(m_lSeparador) & IIf(oCumplimentacion.Escritura, 1, 0) & Chr(m_lSeparador) & IIf(oCumplimentacion.EscrituraFormula, "...", "") & Chr(m_lSeparador) & IIf(oCumplimentacion.Obligatorio, 1, 0) & Chr(m_lSeparador) & oCumplimentacion.Intro
                        If bCrearCampoVinculado Then
                            sdbgCampos.AddItem "0" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & msTextoSolicitudVinculada & Chr(m_lSeparador) & "1" & Chr(m_lSeparador) & idsficticios.DesgloseVinculado & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "" & Chr(m_lSeparador) & "0" & Chr(m_lSeparador) & "0"
                            bCrearCampoVinculado = False
                        End If
                    Else
                        m_bFormLoad = True
                        Me.chkVincularPermitirMover.Value = BooleanToSQLBinary(oCumplimentacion.PermitirMover)
                        m_bFormLoad = False
                    End If
                Next
            End If
        End If
    End If
    
End Sub
Private Sub Form_Resize()

    If Me.Width < 8220 Or Me.Height < 5880 Then Exit Sub
    sdbgCampos.Width = Me.Width - 830
    sdbgCampos.Height = Me.Height - 615
    sdbgCampos.Columns("DATO").Width = sdbgCampos.Width - sdbgCampos.Columns("ATRIBUTO").Width - sdbgCampos.Columns("VISIBLE").Width - sdbgCampos.Columns("ESCRITURA").Width - sdbgCampos.Columns("BLOQUEO").Width - sdbgCampos.Columns("OBLIGATORIO").Width - 580
    
    Me.chkVincularPermitirMover.Width = sdbgCampos.Width
    sdbgCampos.Height = sdbgCampos.Height - Me.chkVincularPermitirMover.Height
    
    If Me.chkVincularPermitirMover.Visible Then
        sdbgCampos.Height = sdbgCampos.Height - Me.chkVincularPermitirMover.Height
    Else
        Me.sdbgCampos.Top = Me.chkVincularPermitirMover.Top
        
        sdbgCampos.Height = sdbgCampos.Height + Me.chkVincularPermitirMover.Height + 10
    End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
    'Si han modificado el orden de los campos lo guardo
    If m_bCambiaOrden Then
        If Not GrabarOrdenTodos Then Exit Sub
    End If

    m_bCambiaOrden = False
    mlBloque = 0
    mlRol = 0
    mlGrupo = 0
    mlCampoPadre = 0
    
    Set moCumplimentaciones = Nothing
End Sub
Private Sub sdbgCampos_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
    Dim oCumplimentacion As CPMConfCumplimentacion
    Dim oIBaseDatos As IBaseDatos
    Dim teserror As TipoErrorSummit

    If sdbgCampos.Columns("ID").Value <> "" And sdbgCampos.Columns("ID").Value <> "0" Then
        Set oCumplimentacion = moCumplimentaciones.Item(CStr(sdbgCampos.Columns("ID").Value))
        If Not oCumplimentacion Is Nothing Then
            oCumplimentacion.Visible = CBool(sdbgCampos.Columns("VISIBLE").Value)
            oCumplimentacion.Escritura = CBool(sdbgCampos.Columns("ESCRITURA").Value)
            oCumplimentacion.Obligatorio = CBool(sdbgCampos.Columns("OBLIGATORIO").Value)
            Set oIBaseDatos = oCumplimentacion
            teserror = oIBaseDatos.FinalizarEdicionModificando
            If teserror.NumError <> TESnoerror Then
                basErrores.TratarError teserror
                sdbgCampos.CancelUpdate
                If Me.Visible Then sdbgCampos.SetFocus
                Set oCumplimentacion = Nothing
                Set oIBaseDatos = Nothing
                m_bErrorCumplimentaciones = True
                Exit Sub
            End If
            'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
            frmFlujos.HayCambios
            Set oCumplimentacion = Nothing
            Set oIBaseDatos = Nothing
            m_bErrorCumplimentaciones = False
        End If
    End If
End Sub
Private Sub sdbgCampos_BtnClick()
 If sdbgCampos.col < 0 Then Exit Sub

    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
        Case "ATRIBUTO"
            frmFlujosCumplValorLista.mlBloque = mlBloque
            frmFlujosCumplValorLista.mlRol = mlRol
            frmFlujosCumplValorLista.mlCampo = sdbgCampos.Columns("ID").Value
            frmFlujosCumplValorLista.mlSubTipo = sdbgCampos.Columns("SUBTIPO").Value

            frmFlujosCumplValorLista.Show vbModal
        Case "BLOQUEO"
            If sdbgCampos.Columns("ESCRITURA").Value = True Then
                frmFlujosCondicionBloqueo.m_lBloque = mlBloque
                frmFlujosCondicionBloqueo.m_lRol = mlRol
                frmFlujosCondicionBloqueo.m_lCampo = sdbgCampos.Columns("ID").Value
                frmFlujosCondicionBloqueo.m_lIdFormulario = mlFormulario
                frmFlujosCondicionBloqueo.m_lCampoPadre = mlCampoPadre
                frmFlujosCondicionBloqueo.Show vbModal
                CargarCumplimentacion
            End If
    End Select
End Sub
Private Sub sdbgCampos_Change()
    If sdbgCampos.col < 0 Then Exit Sub
                    
    Select Case sdbgCampos.Columns(sdbgCampos.col).Name
        Case "VISIBLE"
            If Not sdbgCampos.Columns("VISIBLE").Value Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo Then
                        sdbgCampos.Columns("VISIBLE").Value = True
                        Exit Sub
                End If
                                                    
                sdbgCampos.Columns("ESCRITURA").Value = False
                sdbgCampos.Columns("OBLIGATORIO").Value = False
            End If
            'Si intenta chekear el campo de desglose vinculado lo impedimos
            If sdbgCampos.Columns("VISIBLE").Value And sdbgCampos.Columns("CAMPO_GS").Value = idsficticios.DesgloseVinculado Then
                sdbgCampos.Columns("VISIBLE").Value = False
            End If
        Case "ESCRITURA"
            If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.PrecioUnitarioAdj Or _
                sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.ProveedorAdj Or _
                                sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.NumSolicitERP Or _
                                sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.CantidadAdj _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaAdj _
                                Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaPreadj _
                                Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                sdbgCampos.Columns("ESCRITURA").Value = False
                Exit Sub
            End If
            
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado Then
                sdbgCampos.Columns("ESCRITURA").Value = False
                Exit Sub
            End If
            
            If Not sdbgCampos.Columns("ESCRITURA").Value Then
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo _
                    Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaAdj Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaPreadj Then
                        sdbgCampos.Columns("ESCRITURA").Value = True
                        Exit Sub
                End If
                
                sdbgCampos.Columns("OBLIGATORIO").Value = False
            Else
                If Not sdbgCampos.Columns("VISIBLE").Value Then
                    sdbgCampos.Columns("ESCRITURA").Value = False
                    Exit Sub
                End If
            End If
            
        Case "OBLIGATORIO"
            If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Calculado _
            Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoEnlace Then
                sdbgCampos.Columns("OBLIGATORIO").Value = False
                Exit Sub
            End If
            
            If Not sdbgCampos.Columns("OBLIGATORIO").Value Then
                'Los campos de certificado "Certificado" tiene que tener obligatorio siempre checkeado:
                If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoCertificado.Certificado Or _
                   sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoNoConformidad.Motivo _
                    Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaAdj Or sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoSC.TotalLineaPreadj Then
                        sdbgCampos.Columns("OBLIGATORIO").Value = True
                        Exit Sub
                End If
            Else
                If Not sdbgCampos.Columns("VISIBLE").Value Or Not sdbgCampos.Columns("ESCRITURA").Value Then
                    sdbgCampos.Columns("OBLIGATORIO").Value = False
                    Exit Sub
                End If
            End If
    End Select
    
    sdbgCampos.Update
End Sub

Private Sub sdbgCampos_InitColumnProps()
    sdbgCampos.Columns("DATO").caption = msDato
    sdbgCampos.Columns("VISIBLE").caption = msVisible
    sdbgCampos.Columns("ESCRITURA").caption = msEscritura
    sdbgCampos.Columns("OBLIGATORIO").caption = msObligatorio
End Sub

Private Sub sdbgCampos_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If sdbgCampos.DataChanged Then
                sdbgCampos.Update
                If m_bErrorCumplimentaciones Then
                    Exit Sub
                End If
            End If
    End Select
End Sub

Private Sub sdbgCampos_LostFocus()
    If sdbgCampos.DataChanged Then
        sdbgCampos.Update
    End If
End Sub

Private Sub sdbgCampos_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If sdbgCampos.Columns("CAMPO_GS").Value = TipoCampoGS.Desglose Or sdbgCampos.Columns("SUBTIPO").Value = TiposDeAtributos.TipoDesglose Then
        sdbgCampos.Columns("DATO").Style = ssStyleEditButton
    Else
        sdbgCampos.Columns("DATO").Style = ssStyleEdit
    End If
    If sdbgCampos.Columns("TIPO").Value = TipoCampoPredefinido.Atributo And sdbgCampos.Columns("INTRO").Value Then
        sdbgCampos.Columns("ATRIBUTO").Style = ssStyleEditButton
    Else
        sdbgCampos.Columns("ATRIBUTO").Style = ssStyleEdit
    End If
End Sub

Private Sub sdbgCampos_RowLoaded(ByVal Bookmark As Variant)
    If sdbgCampos.Columns("VISIBLE").CellValue(Bookmark) = False Then
        sdbgCampos.Columns("DATO").CellStyleSet "Gris"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.CampoGS Then
        sdbgCampos.Columns("DATO").CellStyleSet "Amarillo"
    ElseIf sdbgCampos.Columns("TIPO").CellValue(Bookmark) = TipoCampoPredefinido.Calculado Then
        sdbgCampos.Columns("DATO").CellStyleSet "Calculado"
    Else
        sdbgCampos.Columns("DATO").CellStyleSet ""
    End If
    'Si el campo es de Desglose Vinculado
    If sdbgCampos.Columns("CAMPO_GS").CellValue(Bookmark) = idsficticios.DesgloseVinculado Then
        sdbgCampos.Columns("VISIBLE").CellStyleSet "Gris"
        sdbgCampos.Columns("ESCRITURA").CellStyleSet "Gris"
    End If
End Sub

''' Se llama al m�todo que actualiza el campo ORDEN de todos los campos con el orden que ha quedado en el grid
''' Antes de 32100.7.002 se almacenaba el orden campo a campo a medida que lo cambiaban, ahora no solo se guarda al cambiar de pesta�a o al cerrar el form
''' Tambi�n se actualiza la variable de control para no tener que realizar la actualizaci�n m�s de una vez
''' <remarks>Llamada desde: frmFlujos.frm, frmFlujosAnyadirEtapa.frm, frmFlujosConfEtapa.frm, frmFlujosDetalleAccion.frm, frmFujosNombreEtapa.frm
''' frmFlujosRoles, frmBloqueosCondiciones, frmBloqueoFlujos</remarks>
Private Function GrabarOrdenTodos() As Boolean
Dim oCumplimentacion As CPMConfCumplimentacion
Dim teserror As TipoErrorSummit
Dim i As Integer
Dim vBookmark  As Variant

GrabarOrdenTodos = True
With sdbgCampos
    For i = 0 To .Rows - 1
        vBookmark = .AddItemBookmark(i)
        Set oCumplimentacion = moCumplimentaciones.Item(CStr(.Columns("ID").CellValue(vBookmark)))
        oCumplimentacion.Orden = i + 1
    Next
End With

teserror = moCumplimentaciones.GuardarOrdenCampos
If teserror.NumError <> TESnoerror Then
    basErrores.TratarError teserror
    GrabarOrdenTodos = False
End If
Set oCumplimentacion = Nothing
'Marcamos la variable para que se sepa que hay cambios y habr� que dar los avisos para guardar
frmFlujos.HayCambios
End Function
