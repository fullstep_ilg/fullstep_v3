VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmItemModificarValores 
   BackColor       =   &H00808000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Modificar valores"
   ClientHeight    =   9045
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7035
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmItemModificarValores.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9045
   ScaleWidth      =   7035
   Begin VB.CommandButton cmdBuscarSolicitud 
      Height          =   285
      Left            =   4060
      Picture         =   "frmItemModificarValores.frx":0CB2
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   8190
      Width           =   315
   End
   Begin VB.CommandButton cmdBorrarSolicitud 
      Height          =   285
      Left            =   3700
      Picture         =   "frmItemModificarValores.frx":0D3F
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   8190
      Width           =   315
   End
   Begin VB.CheckBox chkProve 
      BackColor       =   &H00808000&
      Caption         =   "Introducir proveedor de la �ltima adjudicaci�n"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   180
      TabIndex        =   36
      Top             =   7485
      Width           =   5500
   End
   Begin VB.PictureBox picCant 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   495
      Left            =   300
      ScaleHeight     =   495
      ScaleWidth      =   6000
      TabIndex        =   34
      Top             =   1680
      Width           =   6000
      Begin VB.CheckBox chkCantDef 
         BackColor       =   &H00808000&
         Caption         =   "Introducir autom�ticamente la cantidad estimada para el art�culo"
         ForeColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   2550
         TabIndex        =   8
         Top             =   10
         Width           =   3645
      End
      Begin VB.TextBox txtCant 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1005
         TabIndex        =   7
         Top             =   40
         Width           =   1110
      End
      Begin VB.Label lblValorCant 
         BackColor       =   &H00808000&
         Caption         =   "Valor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   6
         Top             =   85
         Width           =   1000
      End
   End
   Begin VB.PictureBox picUni 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   700
      Left            =   300
      ScaleHeight     =   705
      ScaleWidth      =   6300
      TabIndex        =   33
      Top             =   450
      Width           =   6300
      Begin VB.CheckBox chkUniDef 
         BackColor       =   &H00808000&
         Caption         =   "Introducir la unidad por defecto"
         ForeColor       =   &H00FFFFFF&
         Height          =   360
         Left            =   0
         TabIndex        =   4
         Top             =   400
         Width           =   4000
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUnidadCod 
         Height          =   285
         Left            =   1020
         TabIndex        =   2
         Top             =   0
         Width           =   1125
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1984
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcUnidadDen 
         Height          =   285
         Left            =   2160
         TabIndex        =   3
         Top             =   0
         Width           =   3705
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         ForeColorEven   =   0
         BackColorEven   =   -2147483648
         BackColorOdd    =   12632256
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6482
         Columns(1).Caption=   "Denominaci�n"
         Columns(1).Name =   "DEN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6535
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Label lblValorUni 
         BackColor       =   &H00808000&
         Caption         =   "Valor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   1000
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   315
      Left            =   3540
      TabIndex        =   32
      Top             =   8640
      Width           =   1095
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2355
      TabIndex        =   31
      Top             =   8640
      Width           =   1095
   End
   Begin VB.TextBox txtFecFin 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   4860
      TabIndex        =   25
      Top             =   7845
      Width           =   1125
   End
   Begin VB.CommandButton cmdCalFecFin 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   6030
      Picture         =   "frmItemModificarValores.frx":0DE4
      Style           =   1  'Graphical
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   8025
      Width           =   315
   End
   Begin VB.TextBox txtFecIni 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1485
      TabIndex        =   23
      Top             =   7845
      Width           =   1125
   End
   Begin VB.CommandButton cmdCalFecIni 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   2640
      Picture         =   "frmItemModificarValores.frx":136E
      Style           =   1  'Graphical
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   7845
      Width           =   315
   End
   Begin VB.CheckBox chkPres 
      BackColor       =   &H00808000&
      Caption         =   "Presupuesto"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   9
      Top             =   2325
      Width           =   2085
   End
   Begin VB.CheckBox chkCant 
      BackColor       =   &H00808000&
      Caption         =   "Cantidad"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   5
      Top             =   1470
      Width           =   2085
   End
   Begin VB.CheckBox chkUni 
      BackColor       =   &H00808000&
      Caption         =   "Unidad"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   2085
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcDestCod 
      Height          =   285
      Left            =   1485
      TabIndex        =   14
      Top             =   6435
      Width           =   1125
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   1905
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3201
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Direcci�n"
      Columns(2).Name =   "DIR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2143
      Columns(3).Caption=   "Poblaci�n"
      Columns(3).Name =   "POB"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1826
      Columns(4).Caption=   "CP"
      Columns(4).Name =   "CP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Pais"
      Columns(5).Name =   "PAIS"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Provincia"
      Columns(6).Name =   "PROVI"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   1984
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcDestDen 
      Height          =   285
      Left            =   2625
      TabIndex        =   15
      Top             =   6435
      Width           =   3705
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   7
      Columns(0).Width=   3307
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 1"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2143
      Columns(1).Caption=   "Cod"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 0"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   2487
      Columns(2).Caption=   "Direcci�n"
      Columns(2).Name =   "DIR"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   1958
      Columns(3).Caption=   "Poblaci�n"
      Columns(3).Name =   "POB"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   1773
      Columns(4).Caption=   "CP"
      Columns(4).Name =   "CP"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Pais"
      Columns(5).Name =   "PAIS"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(6).Width=   2487
      Columns(6).Caption=   "Provincia"
      Columns(6).Name =   "PROVI"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      _ExtentX        =   6535
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcPagoCod 
      Height          =   285
      Left            =   1485
      TabIndex        =   17
      Top             =   6795
      Width           =   1125
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   1826
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   6033
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   1984
      _ExtentY        =   503
      _StockProps     =   93
      ForeColor       =   -2147483642
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcPagoDen 
      Height          =   285
      Left            =   2625
      TabIndex        =   18
      Top             =   6795
      Width           =   3705
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   4815
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2302
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6535
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProveCod 
      Height          =   285
      Left            =   1485
      TabIndex        =   20
      Top             =   7155
      Width           =   1125
      DataFieldList   =   "Column 0"
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2117
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   5292
      Columns(1).Caption=   "Denominaci�n"
      Columns(1).Name =   "DEN"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   1984
      _ExtentY        =   503
      _StockProps     =   93
      ForeColor       =   -2147483642
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B.SSDBCombo sdbcProveDen 
      Height          =   285
      Left            =   2625
      TabIndex        =   21
      Top             =   7155
      Width           =   3705
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   4180
      Columns(0).Caption=   "Denominaci�n"
      Columns(0).Name =   "DEN"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   2143
      Columns(1).Caption=   "C�digo"
      Columns(1).Name =   "COD"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6535
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.PictureBox PicPresEsc 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   2895
      Left            =   270
      ScaleHeight     =   2895
      ScaleWidth      =   6540
      TabIndex        =   39
      Top             =   3345
      Visible         =   0   'False
      Width           =   6540
      Begin VB.TextBox txtPresUniItem 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   2940
         TabIndex        =   43
         Top             =   908
         Width           =   1830
      End
      Begin VB.CheckBox chkPresPlanifEsc 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el presupuesto unitario planificado si est� disponible"
         ForeColor       =   &H00FFFFFF&
         Height          =   370
         Left            =   315
         TabIndex        =   41
         Top             =   405
         Width           =   6015
      End
      Begin VB.CheckBox chkPresDefEsc 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el precio de la �ltima adjudicaci�n si est� disponible"
         ForeColor       =   &H80000005&
         Height          =   400
         Left            =   315
         TabIndex        =   40
         Top             =   0
         Width           =   6015
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGridEscalados 
         Height          =   1290
         Left            =   270
         TabIndex        =   42
         Top             =   1545
         Width           =   5970
         ScrollBars      =   2
         _Version        =   196617
         DataMode        =   2
         Col.Count       =   5
         stylesets.count =   1
         stylesets(0).Name=   "Normal"
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmItemModificarValores.frx":18F8
         MultiLine       =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         StyleSet        =   "Normal"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "ID"
         Columns(0).Name =   "ID"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6403
         Columns(1).Caption=   "Cantidades directas"
         Columns(1).Name =   "DIR"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).NumberFormat=   "standard"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   3200
         Columns(2).Caption=   "Cantidad inicial"
         Columns(2).Name =   "INI"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).NumberFormat=   "standard"
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3200
         Columns(3).Caption=   "Cantidad final"
         Columns(3).Name =   "FIN"
         Columns(3).Alignment=   1
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).NumberFormat=   "standard"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Caption=   "Presupuesto Unitario"
         Columns(4).Name =   "PRES"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).NumberFormat=   "#,##0.00####################"
         Columns(4).FieldLen=   256
         _ExtentX        =   10530
         _ExtentY        =   2275
         _StockProps     =   79
         ForeColor       =   0
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label LabelPresUni 
         AutoSize        =   -1  'True
         BackColor       =   &H00808000&
         Caption         =   "Presupuestos unitarios escalados:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   315
         TabIndex        =   45
         Top             =   1290
         Width           =   2445
      End
      Begin VB.Label lblPresUniItem 
         BackColor       =   &H00808000&
         Caption         =   "Presupuesto unitario para el item:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   315
         TabIndex        =   44
         Top             =   930
         Width           =   2535
      End
   End
   Begin VB.PictureBox picPresu 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      Height          =   780
      Left            =   300
      ScaleHeight     =   780
      ScaleWidth      =   6450
      TabIndex        =   35
      Top             =   2550
      Width           =   6450
      Begin VB.CheckBox chkPresPlanif 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el presupuesto unitario planificado si est� disponible"
         ForeColor       =   &H00FFFFFF&
         Height          =   350
         Left            =   2550
         TabIndex        =   38
         Top             =   440
         Width           =   3850
      End
      Begin VB.TextBox txtPres 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1005
         TabIndex        =   11
         Top             =   85
         Width           =   1110
      End
      Begin VB.CheckBox chkPresDef 
         BackColor       =   &H00808000&
         Caption         =   "Sustituir por el precio de la �ltima adjudicaci�n si est� disponible"
         ForeColor       =   &H00FFFFFF&
         Height          =   350
         Left            =   2550
         TabIndex        =   12
         Top             =   0
         Width           =   3850
      End
      Begin VB.Label lblValorPres 
         BackColor       =   &H00808000&
         Caption         =   "Valor:"
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   0
         TabIndex        =   10
         Top             =   130
         Width           =   1000
      End
   End
   Begin VB.Line Line3 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   30
      X2              =   6810
      Y1              =   6315
      Y2              =   6315
   End
   Begin VB.Label lblSolicitud 
      BackColor       =   &H00C0FFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Left            =   1485
      TabIndex        =   27
      Top             =   8190
      Width           =   2145
   End
   Begin VB.Label lblLitSolicit 
      BackStyle       =   0  'Transparent
      Caption         =   "DSolicitud"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   37
      Top             =   8235
      Width           =   1335
   End
   Begin VB.Label lblDest 
      BackColor       =   &H00808000&
      Caption         =   "Destino"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   13
      Top             =   6435
      Width           =   900
   End
   Begin VB.Label lblPago 
      BackColor       =   &H00808000&
      Caption         =   "Forma de pago"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   16
      Top             =   6795
      Width           =   1185
   End
   Begin VB.Label lblProve 
      BackColor       =   &H00808000&
      Caption         =   "Proveedor"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   180
      TabIndex        =   19
      Top             =   7155
      Width           =   1140
   End
   Begin VB.Label lblFecFin 
      BackStyle       =   0  'Transparent
      Caption         =   "Fin suministro"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   3705
      TabIndex        =   30
      Top             =   7890
      Width           =   1095
   End
   Begin VB.Label lblFecIni 
      BackStyle       =   0  'Transparent
      Caption         =   "Inicio suministro"
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   180
      TabIndex        =   22
      Top             =   7890
      Width           =   1275
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   30
      X2              =   6810
      Y1              =   2190
      Y2              =   2190
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      X1              =   30
      X2              =   6810
      Y1              =   1305
      Y2              =   1305
   End
End
Attribute VB_Name = "frmItemModificarValores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bDestRespetarCombo As Boolean
Private m_bDestCargarComboDesde As Boolean
Private m_bPagoRespetarCombo As Boolean
Private m_bPagoCargarComboDesde As Boolean
Private m_bRespetarComboProve As Boolean
Private m_bUniRespetarCombo As Boolean
Private m_bUniCargarComboDesde As Boolean

Private m_oDestinos As CDestinos
Private m_oPagos As CPagos
Private m_oProves As CProveedores
Private m_oUnidades As CUnidades

Private m_sIdiPres As String
Private m_sIdiUni As String
Private m_sIdiCant As String
Private m_sIdiFecIni As String
Private m_sIdiFecFin As String
Private m_sIdiSeleccion As String
Private m_sPresUniItem As String

Public g_oPresupuestos1Nivel4 As CPresProyectosNivel4
Public g_oPresupuestos2Nivel4 As CPresContablesNivel4
Public g_oPresupuestos3Nivel4 As CPresConceptos3Nivel4
Public g_oPresupuestos4Nivel4 As CPresConceptos4Nivel4

Public g_bRDest As Boolean
Public bArtCodif As Boolean
Public sCodUnidades As String

Private g_ogrupo As CGrupo
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean

Private m_sMsgError As String
''' <summary>Carga los textos de los controles del formulario</summary>
''' <remarks>Llamada desde Form_Load</remarks>
''' <revision>LTG 06/02/2012</revision>

Private Sub CargarRecursos()
    Dim Adores As Ador.Recordset

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next

    Set Adores = oGestorIdiomas.DevolverTextosDelModulo(FRM_ITEM_MODIF_VALORES, basPublic.gParametrosInstalacion.gIdioma)

    If Not Adores Is Nothing Then
        Me.caption = Adores(0).Value
        Adores.MoveNext
        chkUni.caption = Adores(0).Value
        m_sIdiUni = Adores(0).Value
        Adores.MoveNext
        lblValorUni.caption = Adores(0).Value & ":"
        lblValorCant.caption = Adores(0).Value & ":"
        lblValorPres.caption = Adores(0).Value & ":"
        Adores.MoveNext
        chkUniDef.caption = Adores(0).Value
        Adores.MoveNext
        chkCant.caption = Adores(0).Value
        m_sIdiCant = Adores(0).Value
        Adores.MoveNext
        chkCantDef.caption = Adores(0).Value
        Adores.MoveNext
        chkPres.caption = Adores(0).Value
        m_sIdiPres = Adores(0).Value
        Adores.MoveNext
        chkPresDef.caption = Adores(0).Value
        chkPresDefEsc.caption = Adores(0).Value
        Adores.MoveNext
        lblDest.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblPago.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblProve.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblFecIni.caption = Adores(0).Value & ":"
        Adores.MoveNext
        lblFecFin.caption = Adores(0).Value & ":"
        Adores.MoveNext
        cmdAceptar.caption = Adores(0).Value
        Adores.MoveNext
        cmdCancelar.caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("COD").caption = Adores(0).Value
        sdbcDestDen.Columns("COD").caption = Adores(0).Value
        sdbcPagoCod.Columns("COD").caption = Adores(0).Value
        sdbcPagoDen.Columns("COD").caption = Adores(0).Value
        sdbcProveCod.Columns("COD").caption = Adores(0).Value
        sdbcProveDen.Columns("COD").caption = Adores(0).Value
        sdbcUnidadCod.Columns("COD").caption = Adores(0).Value
        sdbcUnidadDen.Columns("COD").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("DEN").caption = Adores(0).Value
        sdbcDestDen.Columns("DEN").caption = Adores(0).Value
        sdbcPagoCod.Columns("DEN").caption = Adores(0).Value
        sdbcPagoDen.Columns("DEN").caption = Adores(0).Value
        sdbcProveCod.Columns("DEN").caption = Adores(0).Value
        sdbcProveDen.Columns("DEN").caption = Adores(0).Value
        sdbcUnidadCod.Columns("DEN").caption = Adores(0).Value
        sdbcUnidadDen.Columns("DEN").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("DIR").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("POB").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("CP").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("PAIS").caption = Adores(0).Value
        Adores.MoveNext
        sdbcDestCod.Columns("PROVI").caption = Adores(0).Value
        Adores.MoveNext
        chkProve.caption = Adores(0).Value
        Adores.MoveNext
        lblLitSolicit.caption = Adores(0).Value
        Adores.MoveNext
        chkPresPlanif.caption = Adores(0).Value
        chkPresPlanifEsc.caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("INI").caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("FIN").caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("DIR").caption = Adores(0).Value
        Adores.MoveNext
        Me.SSDBGridEscalados.Columns("PRES").caption = Adores(0).Value
        Adores.MoveNext
        m_sPresUniItem = Adores(0).Value
        lblPresUniItem.caption = Adores(0).Value & ":"
        Adores.MoveNext
        LabelPresUni.caption = Adores(0).Value & ":"
        
        Adores.Close
    
    End If

    Set Adores = Nothing
    
    m_sIdiFecIni = "Inicio suministro"
    m_sIdiFecFin = "Fin suministro"
    m_sIdiSeleccion = "Selecci�n"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>Configura el tama�o y posici�n de los controles del formulario</summary>
''' <remarks>Llamada desde Form_Load</remarks>
''' <revision>LTG 06/02/2012</revision>

Private Sub ConfigurarControles()
    'Modo Escalado/ modo normal
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        Me.picPresu.Visible = False
        Me.PicPresEsc.Visible = True
        
        'No permitimos los cambios en la unidad
        chkUni.Visible = False
        lblValorUni.Visible = False
        sdbcUnidadCod.Visible = False
        sdbcUnidadDen.Visible = False
        picUni.Visible = False
        chkUniDef.Visible = False
        Line1.Visible = False
    Else
        'Permitimos los cambios en la unidad
        chkUni.Visible = True
        lblValorUni.Visible = True
        sdbcUnidadCod.Visible = True
        sdbcUnidadDen.Visible = True
        picUni.Visible = True
        chkUniDef.Visible = True
        Line1.Visible = True
  
        Me.picPresu.Visible = True
        Me.PicPresEsc.Visible = False
    End If
    
   
    
    Call Arrange
    

    If Not frmPROCE.sdbgItems.Columns("DEST").Visible Then
        lblDest.Visible = False
        sdbcDestCod.Visible = False
        sdbcDestDen.Visible = False
    End If
    
    If Not frmPROCE.sdbgItems.Columns("PAG").Visible Then
        lblPago.Visible = False
        sdbcPagoCod.Visible = False
        sdbcPagoDen.Visible = False
    End If

    If Not frmPROCE.sdbgItems.Columns("PROVE").Visible Then
        lblProve.Visible = False
        sdbcProveCod.Visible = False
        sdbcProveDen.Visible = False
        chkProve.Visible = False
    End If

    If Not frmPROCE.sdbgItems.Columns("INI").Visible Then
        lblFecIni.Visible = False
        lblFecFin.Visible = False
        cmdCalFecIni.Visible = False
        cmdCalFecFin.Visible = False
        txtFecIni.Visible = False
        txtFecFin.Visible = False
    End If
    
    If Not frmPROCE.sdbgItems.Columns("SOLICITUD").Visible Then
        lblLitSolicit.Visible = False
        lblSolicitud.Visible = False
        cmdBorrarSolicitud.Visible = False
        cmdBuscarSolicitud.Visible = False
    ElseIf frmPROCE.sdbgItems.Columns("SOLICITUD").Locked Then
        lblLitSolicit.Visible = False
        lblSolicitud.Visible = False
        cmdBorrarSolicitud.Visible = False
        cmdBuscarSolicitud.Visible = False
    End If
            
    '@@@@@@@@@@@@@@@@@
    If Not frmPROCE.sdbgItems.Columns("DEST").Visible Then
        lblLitSolicit.Top = lblFecIni.Top
        lblSolicitud.Top = txtFecIni.Top
        cmdBorrarSolicitud.Top = lblSolicitud.Top
        cmdBuscarSolicitud.Top = lblSolicitud.Top
        
        lblFecIni.Top = chkProve.Top + 45
        lblFecFin.Top = chkProve.Top + 45
        txtFecIni.Top = chkProve.Top
        txtFecFin.Top = chkProve.Top
        cmdCalFecIni.Top = txtFecIni.Top
        cmdCalFecFin.Top = txtFecIni.Top
        
        lblProve.Top = lblPago.Top
        sdbcProveCod.Top = sdbcPagoCod.Top
        sdbcProveDen.Top = sdbcPagoCod.Top
        chkProve.Top = sdbcProveCod.Top + 350
        
        lblPago.Top = lblDest.Top
        sdbcPagoCod.Top = sdbcDestCod.Top
        sdbcPagoDen.Top = sdbcDestDen.Top
        
        Me.Height = Me.Height - sdbcDestCod.Height - 80
        cmdAceptar.Top = cmdAceptar.Top - sdbcDestCod.Height - 80
    End If
    
    If Not frmPROCE.sdbgItems.Columns("PAG").Visible Then
        lblLitSolicit.Top = lblFecIni.Top
        lblSolicitud.Top = txtFecIni.Top
        cmdBorrarSolicitud.Top = lblSolicitud.Top
        cmdBuscarSolicitud.Top = lblSolicitud.Top
        
        lblFecIni.Top = chkProve.Top + 45
        lblFecFin.Top = chkProve.Top
        txtFecIni.Top = chkProve.Top
        txtFecFin.Top = chkProve.Top
        cmdCalFecIni.Top = txtFecIni.Top
        cmdCalFecFin.Top = txtFecIni.Top
        
        lblProve.Top = lblPago.Top
        sdbcProveCod.Top = sdbcPagoCod.Top
        sdbcProveDen.Top = sdbcPagoCod.Top
        chkProve.Top = sdbcProveCod.Top + 350

        Me.Height = Me.Height - sdbcPagoCod.Height - 80
        cmdAceptar.Top = cmdAceptar.Top - sdbcPagoCod.Height - 80
    End If
    
    If Not frmPROCE.sdbgItems.Columns("PROVE").Visible Then
        lblLitSolicit.Top = chkProve.Top
        lblSolicitud.Top = chkProve.Top
        cmdBorrarSolicitud.Top = chkProve.Top
        cmdBuscarSolicitud.Top = chkProve.Top
        
        lblFecIni.Top = lblProve.Top
        lblFecFin.Top = lblProve.Top
        txtFecIni.Top = sdbcProveCod.Top
        txtFecFin.Top = sdbcProveCod.Top
        cmdCalFecIni.Top = txtFecIni.Top
        cmdCalFecFin.Top = txtFecIni.Top
        
        Me.Height = Me.Height - sdbcProveCod.Height - 80
        cmdAceptar.Top = cmdAceptar.Top - sdbcProveCod.Height - 80
    End If
    
    If Not frmPROCE.sdbgItems.Columns("INI").Visible Then
        lblLitSolicit.Top = Me.lblFecIni.Top
        lblSolicitud.Top = Me.txtFecIni.Top
        cmdBorrarSolicitud.Top = lblSolicitud.Top
        cmdBuscarSolicitud.Top = lblSolicitud.Top
        
        Me.Height = Me.Height - txtFecIni.Height - 80
        cmdAceptar.Top = cmdAceptar.Top - txtFecIni.Height - 80
    End If
    
    If Not frmPROCE.sdbgItems.Columns("SOLICITUD").Visible Or frmPROCE.sdbgItems.Columns("SOLICITUD").Locked Then
        Me.Height = Me.Height - lblSolicitud.Height - 80
        cmdAceptar.Top = cmdAceptar.Top - lblSolicitud.Height - 80
    End If
    
    If frmPROCE.sdbgItems.Columns("DEST").Visible = False And frmPROCE.sdbgItems.Columns("PROVE").Visible = False And frmPROCE.sdbgItems.Columns("INI").Visible = False And (frmPROCE.sdbgItems.Columns("SOLICITUD").Visible = False Or frmPROCE.sdbgItems.Columns("SOLICITUD").Locked) Then
        'Todos invisibles
        Line3.Visible = False
        
        Me.Height = Me.Height - 350
        cmdAceptar.Top = cmdAceptar.Top - 350
    End If
    
    If Not frmPROCE.g_oProcesoSeleccionado.NoCalPresUniEnBasePrecioOfer And frmPROCE.g_oProcesoSeleccionado.tieneofertas > 0 Then
        chkPres.Enabled = False
        txtPres.Enabled = False
        chkPresPlanif.Enabled = False
        chkPresDef.Enabled = False
    End If
    
    
    cmdCancelar.Top = cmdAceptar.Top
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "ConfigurarControles", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function ComprobarSeleccion() As Boolean

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkUni.Value = vbChecked Then
        ComprobarSeleccion = True
        Exit Function
    End If
    
    If chkCant.Value = vbChecked Then
        ComprobarSeleccion = True
        Exit Function
    End If
    
    If chkPres.Value = vbChecked Then
        ComprobarSeleccion = True
        Exit Function
    End If
                
    If sdbcDestCod.Visible And sdbcDestCod.Value <> "" Then
        ComprobarSeleccion = True
        Exit Function
    End If
    
    If sdbcPagoCod.Visible And sdbcPagoCod.Value <> "" Then
        ComprobarSeleccion = True
        Exit Function
    End If
                        
    If (sdbcProveCod.Visible And sdbcProveCod.Value <> "") Or (chkProve.Visible And chkProve.Value = vbChecked) Then
        ComprobarSeleccion = True
        Exit Function
    End If
                            
    If txtFecIni.Visible And txtFecIni.Text <> "" Then
        ComprobarSeleccion = True
        Exit Function
    End If
                                
    If txtFecFin.Visible And txtFecFin.Text <> "" Then
        ComprobarSeleccion = True
        Exit Function
    End If

    If lblSolicitud.Visible And lblSolicitud.caption <> "" Then
        ComprobarSeleccion = True
        Exit Function
    End If
    
    ComprobarSeleccion = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "ComprobarSeleccion", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Sub chkCant_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkCant.Value = vbChecked Then
        picCant.Enabled = True
    Else
        chkCantDef.Value = vbUnchecked
        txtCant.Text = ""
        picCant.Enabled = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "chkCant_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkPres_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPres.Value = vbChecked Then
        picPresu.Enabled = True
        
        
        If gParametrosInstalacion.gbPresupuestoPlanificado = True Then
            chkPresPlanif.Value = 1
            chkPresPlanifEsc.Value = 1
        Else
            chkPresDef.Value = 1
            chkPresDefEsc.Value = 1
        End If
            
        PicPresEsc.Enabled = True
    
    Else
        chkPresDef.Value = vbUnchecked
        txtPres.Text = ""
        picPresu.Enabled = False
        chkPresPlanif.Value = 0
        chkPresDef.Value = 0
                
        PicPresEsc.Enabled = False
        chkPresPlanifEsc.Value = 0
        chkPresDefEsc.Value = 0
        
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "chkPres_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkPresDef_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresDef.Value = 1 Then
        chkPresPlanif.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "chkPresDef_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkPresDefEsc_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresDefEsc.Value = 1 Then
        chkPresPlanifEsc.Value = 0
    End If
    chkPresDef.Value = chkPresDefEsc.Value
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "chkPresDefEsc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkPresPlanif_Click()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresPlanif.Value = 1 Then
        chkPresDef.Value = 0
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "chkPresPlanif_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkPresPlanifEsc_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkPresPlanifEsc.Value = 1 Then
        chkPresDefEsc.Value = 0
    End If
    chkPresPlanif.Value = chkPresPlanifEsc.Value
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "chkPresPlanifEsc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub chkUni_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If chkUni.Value = vbChecked Then
        picUni.Enabled = True
    Else
        chkUniDef.Value = vbUnchecked
        sdbcUnidadCod.Value = ""
        picUni.Enabled = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "chkUni_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Modificar Valores del Item
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAceptar_Click()
    Dim oItems As CItems
    Dim oItem As CItem
    Dim oNItem As CItem
    Dim i As Integer
    Dim udtTeserror As TipoErrorSummit
    Dim bErrores As Boolean
    Dim vErrores As Variant
    Dim bCambiarPresup As Boolean
    Dim irespuesta As Integer
    Dim vPresActual As Variant
    Dim dPresAbierto As Double
    Dim dPresAbiertoMod As Double
    Dim iPresAnu1 As Integer
    Dim iPresAnu2 As Integer
    Dim iPres1 As Integer
    Dim iPres2 As Integer
    Dim sAcciones As String 'String para guardar en el Log los datos que se van cambiando
    Dim bCambiarCant As Boolean
    Dim bRecalcularOfertas As Boolean
    Dim iOfertas As Integer
    Dim vSolicit As Variant
    Dim iPos As Integer
    Dim oEscalados As CEscalados
    Dim oEscalado As CEscalado
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAcciones = ""
    If chkUni.Value = vbChecked Then
        If sdbcUnidadCod.Text = "" Then
            oMensajes.NoValido m_sIdiUni
            Exit Sub
        End If
    End If
    
    If chkCant.Value = vbChecked Then
        If txtCant.Text = "" Or txtCant.Text = "0" Then
            If frmPROCE.chkEscaladoPrecios.Value <> vbChecked Then
            'En modo escalados se permite cantidad 0 o vac�o
                oMensajes.NoValido m_sIdiCant
                Exit Sub
            Else
                If oMensajes.PreguntarItemsSinCantidad <> vbYes Then Exit Sub
            End If
        Else
            If Not IsNumeric(txtCant.Text) Then
                oMensajes.NoValido m_sIdiCant
                Exit Sub
            End If
        End If
    End If
    
    If chkPres.Value = vbChecked Then
        If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
            If txtPresUniItem.Text <> "" Then
                If Not IsNumeric(txtPresUniItem.Text) Then
                    oMensajes.NoValido m_sPresUniItem
                    Exit Sub
                End If
            End If
            
            'Modo Escalados
            If Me.SSDBGridEscalados.DataChanged Then Me.SSDBGridEscalados.Update
        Else
            'Modo normal
            If txtPres.Text = "" Then
                oMensajes.NoValido m_sIdiPres
                Exit Sub
            End If
            If Not IsNumeric(txtPres.Text) Then
                oMensajes.NoValido m_sIdiPres
                Exit Sub
            End If
        End If
    End If
    
    If txtFecIni.Visible Then
        If txtFecIni.Text <> "" And Not IsDate(txtFecIni.Text) Then
            oMensajes.NoValido m_sIdiFecIni
            Exit Sub
        End If
    End If
    
    If txtFecFin.Visible Then
        If txtFecFin.Text <> "" And Not IsDate(txtFecFin.Text) Then
            oMensajes.NoValido m_sIdiFecFin
            Exit Sub
        End If
    End If
    
    If txtFecIni.Visible And txtFecFin.Visible Then
        If txtFecIni.Text <> "" And txtFecFin.Text <> "" Then
            If CDate(txtFecIni.Text) > CDate(txtFecFin.Text) Then
                oMensajes.FechaDesdeMayorFechaHasta 2
                Exit Sub
            End If
            If frmPROCE.txtFecNec.Text <> "" Then
                If ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = False) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                    frmPROCE.txtFecNec.Text = txtFecIni.Text
                ElseIf ((frmPROCE.g_oProcesoSeleccionado.FechaNecModificadaPorUsu = True) And (CDate(txtFecIni.Text) < CDate(frmPROCE.txtFecNec.Text))) Then
                    oMensajes.FechaMenorQueNecesidad frmPROCE.m_sIdiFecNec, frmPROCE.txtFecNec.Text
                    Exit Sub
                End If
            End If
        End If
    End If
    
    'Si no se ha seleccionado nada mensaje
    If Not ComprobarSeleccion Then
        oMensajes.FaltanDatos m_sIdiSeleccion
        Exit Sub
    End If
            
    'Escalados
    If Me.chkPres.Value = vbChecked Then
        Set oEscalados = oFSGSRaiz.Generar_CEscalados
        
        If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
            If Not EscaladosConPresupuestos Then
                If oMensajes.PreguntarEscaladosSinPresup <> vbYes Then Exit Sub
            End If
    
            Dim Id As Long
            Dim bm As Variant
                        
            oEscalados.modo = frmPROCE.g_oGrupoSeleccionado.TipoEscalados
            
            For i = 0 To SSDBGridEscalados.Rows - 1
                bm = SSDBGridEscalados.AddItemBookmark(i)
                If CStr(SSDBGridEscalados.Columns("ID").CellValue(bm)) = "" Then
                    Id = 0
                Else
                    Id = SSDBGridEscalados.Columns("ID").CellValue(bm)
                End If
                    
                If frmPROCE.g_oGrupoSeleccionado.TipoEscalados = ModoRangos Then
                    Set oEscalado = oEscalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(bm), SSDBGridEscalados.Columns("FIN").CellValue(bm), SSDBGridEscalados.Columns("PRES").CellValue(bm))
                Else
                    Set oEscalado = oEscalados.Add(Id, SSDBGridEscalados.Columns("INI").CellValue(bm), Null, SSDBGridEscalados.Columns("PRES").CellValue(bm))
                End If
                
            Next i
        End If
    End If
    
    
    Set oItem = oFSGSRaiz.Generar_CItem
    Set oItems = oFSGSRaiz.Generar_CItems
    bCambiarCant = False
    For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
        
        With oItem
        
            .Id = frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
            .GrupoID = frmPROCE.sdbgItems.Columns("GRUPOID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            'DEST
            If sdbcDestCod.Value = "" Then
                Select Case frmPROCE.g_oProcesoSeleccionado.DefDestino
                    Case EnItem
                            .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefDestino Then
                                .DestCod = frmPROCE.g_oGrupoSeleccionado.DestCod
                            Else
                                .DestCod = frmPROCE.sdbgItems.Columns("DEST").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .DestCod = frmPROCE.g_oProcesoSeleccionado.DestCod
                End Select
            Else
                .DestCod = sdbcDestCod.Value
            End If
            
            'PAG
            If sdbcPagoCod.Value = "" Then
                Select Case frmPROCE.g_oProcesoSeleccionado.DefFormaPago
                    Case EnItem
                            .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    Case EnGrupo
                            If frmPROCE.g_oGrupoSeleccionado.DefFormaPago Then
                                .PagCod = frmPROCE.g_oGrupoSeleccionado.PagCod
                            Else
                                .PagCod = frmPROCE.sdbgItems.Columns("PAG").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            End If
                    Case EnProceso
                            .PagCod = frmPROCE.g_oProcesoSeleccionado.PagCod
                End Select
            Else
                .PagCod = sdbcPagoCod.Value
            End If
        
            'FECHAS
            Select Case frmPROCE.g_oProcesoSeleccionado.DefFechasSum
                Case EnItem
                        If txtFecIni.Text = "" Then
                            .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        Else
                            .FechaInicioSuministro = txtFecIni.Text
                        End If
                        If txtFecFin.Text = "" Then
                            .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                        Else
                            .FechaFinSuministro = txtFecFin.Text
                        End If
                Case EnGrupo
                        If frmPROCE.g_oGrupoSeleccionado.DefFechasSum Then
                            If txtFecIni.Text = "" Then
                                .FechaInicioSuministro = frmPROCE.g_oGrupoSeleccionado.FechaInicioSuministro
                            Else
                                .FechaInicioSuministro = txtFecIni.Text
                            End If
                            If txtFecFin.Text = "" Then
                                .FechaFinSuministro = frmPROCE.g_oGrupoSeleccionado.FechaFinSuministro
                            Else
                                .FechaFinSuministro = txtFecFin.Text
                            End If
                        Else
                            If txtFecIni.Text = "" Then
                                .FechaInicioSuministro = frmPROCE.sdbgItems.Columns("INI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            Else
                                .FechaInicioSuministro = txtFecIni.Text
                            End If
                            If txtFecFin.Text = "" Then
                                .FechaFinSuministro = frmPROCE.sdbgItems.Columns("FIN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            Else
                                .FechaFinSuministro = txtFecFin.Text
                            End If
                        End If
                Case EnProceso
                        If txtFecIni.Text = "" Then
                            .FechaInicioSuministro = frmPROCE.g_oProcesoSeleccionado.FechaInicioSuministro
                        Else
                            .FechaInicioSuministro = txtFecIni.Text
                        End If
                        If txtFecFin.Text = "" Then
                            .FechaFinSuministro = frmPROCE.g_oProcesoSeleccionado.FechaFinSuministro
                        Else
                            .FechaFinSuministro = txtFecFin.Text
                        End If
            End Select
        
            'PROVE
            If sdbcProveCod.Value = "" Then
                Select Case frmPROCE.g_oProcesoSeleccionado.DefProveActual
                    Case EnItem
                            .ProveAct = StrToNull(frmPROCE.sdbgItems.Columns("PROVE").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))
                    Case EnGrupo
                            .ProveAct = frmPROCE.g_oGrupoSeleccionado.ProveActual
                    Case EnProceso
                            .ProveAct = NullToStr(frmPROCE.g_oProcesoSeleccionado.ProveActual)
                End Select
            Else
                .ProveAct = sdbcProveCod.Value
            End If
        
            'SOLICITUD DE COMPRA
            If lblSolicitud.caption = "" Then
                Select Case frmPROCE.g_oProcesoSeleccionado.DefSolicitud
                    Case EnItem
                        If frmPROCE.sdbgItems.Columns("SOLICITUD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) <> "" Then
                            vSolicit = frmPROCE.sdbgItems.Columns("SOLICITUD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                            
                            'Si hay reemisiones en el campo de solicitud va la solicitud m�s las reemisiones entre par�ntesis
                            If Not IsNumeric(vSolicit) Then
                                iPos = InStr(0, "(", vSolicit)
                                If iPos > 0 Then vSolicit = Left(vSolicit, iPos)
                            End If
                            .SolicitudId = CLng(vSolicit)
                        Else
                            .SolicitudId = Null
                        End If
                    Case EnGrupo
                            .SolicitudId = frmPROCE.g_oGrupoSeleccionado.SolicitudId
                    Case EnProceso
                            .SolicitudId = frmPROCE.g_oProcesoSeleccionado.SolicitudId
                End Select
            Else
                .SolicitudId = CLng(lblSolicitud.Tag)
            End If
            
            'ART
            If frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .ArticuloCod = Null
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            If frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) = "" Then
                .Descr = Null
                .ArticuloCod = frmPROCE.sdbgItems.Columns("COD").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .Descr = frmPROCE.sdbgItems.Columns("DEN").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            'UNI
            If sdbcUnidadCod.Value = "" Or gParametrosGenerales.gbOBLUnidadMedida Then
                .UniCod = frmPROCE.sdbgItems.Columns("UNI").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .UniCod = sdbcUnidadCod.Value
            End If
            'CANT
            If Me.chkCant.Value = vbChecked Then
                If txtCant.Text = "" Then
                    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
                        .Cantidad = Null
                        bCambiarPresup = Me.chkCant
                    Else
                        .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
                    End If
                Else
                    If txtCant.Text <> frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)) Then bCambiarCant = True
                    .Cantidad = txtCant.Text
                    bCambiarPresup = True
                End If
            Else
                .Cantidad = frmPROCE.sdbgItems.Columns("CANT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            End If
            If txtPres.Text = "" Then
                .Precio = frmPROCE.sdbgItems.Columns("PREC").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            Else
                .Precio = txtPres.Text
                bCambiarPresup = True
            End If
            .Presupuesto = frmPROCE.sdbgItems.Columns("PRES").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            
            '�hay escalados?
            If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
                If chkPres.Value = vbChecked Then
                    If txtPresUniItem.Text <> "" Then
                        .Precio = txtPresUniItem.Text
                        bCambiarPresup = True
                    Else
                        .Precio = Null
                    End If
                End If
            
                Set .proceso = frmPROCE.g_oProcesoSeleccionado
                .GrupoID = frmPROCE.g_oGrupoSeleccionado.Id
                
                .CargarPresEscalados
            End If
            
            .FECACT = frmPROCE.sdbgItems.Columns("FECACT").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GMN1Cod = frmPROCE.sdbgItems.Columns("GMN1").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GMN2Cod = frmPROCE.sdbgItems.Columns("GMN2").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GMN3Cod = frmPROCE.sdbgItems.Columns("GMN3").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
            .GMN4Cod = frmPROCE.sdbgItems.Columns("GMN4").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))
        End With
            
        Set oNItem = oItems.Add(frmPROCE.g_oProcesoSeleccionado, oItem.Id, oItem.DestCod, oItem.UniCod, oItem.PagCod, oItem.Cantidad, oItem.FechaInicioSuministro, oItem.FechaFinSuministro, oItem.ArticuloCod, oItem.Descr, oItem.Precio, oItem.Presupuesto, , , , , , , , oItem.ProveAct, , frmPROCE.g_oGrupoSeleccionado.Codigo, oItem.FECACT, oItem.SolicitudId, , , , oItem.GMN1Cod, oItem.GMN2Cod, oItem.GMN3Cod, oItem.GMN4Cod, , oItem.GrupoID)
        Set oNItem.Escalados = oEscalados
    Next

    bRecalcularOfertas = False
    iOfertas = frmPROCE.g_oProcesoSeleccionado.tieneofertas
    If bCambiarCant And iOfertas > 0 Then
        If iOfertas > 1 Then
            irespuesta = oMensajes.AvisoConConfirmacion(frmPROCE.m_sMensajeModifCant)
            If irespuesta = vbNo Then Exit Sub
        End If
        bRecalcularOfertas = True
    End If
    If gParametrosGenerales.gbPresupuestosAut Then
        Screen.MousePointer = vbHourglass
        udtTeserror = oItems.ModificarValores(chkUniDef.Value, chkCantDef.Value, chkPresDef.Value, , Nothing, Nothing, Nothing, Nothing, , , chkProve.Value, lblSolicitud.Visible, chkPresPlanif.Value, basOptimizacion.gvarCodUsuario, bRecalcularOfertas, oEscalados)
    Else
        'Si se ha introducido valor para la cantidad o el precio, se comprueba si alguno
        'de los presupuestos est� definido a nivel de proceso o del grupo de los items, si tienen alg�n presupuesto distribuido
        If bCambiarPresup Then
            bCambiarPresup = False
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo1 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo1 Then
                bCambiarPresup = True
                iPresAnu1 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo1 And frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo1 Then
                    bCambiarPresup = True
                    iPresAnu1 = 2
                End If
            End If
            If frmPROCE.g_oProcesoSeleccionado.DefPresAnualTipo2 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresAnualTipo2 Then
                bCambiarPresup = True
                iPresAnu2 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresAnualTipo2 And frmPROCE.g_oGrupoSeleccionado.HayPresAnualTipo2 Then
                    bCambiarPresup = True
                    iPresAnu2 = 2
                End If
            End If
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo1 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresTipo1 Then
                bCambiarPresup = True
                iPres1 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresTipo1 And frmPROCE.g_oGrupoSeleccionado.HayPresTipo1 Then
                    bCambiarPresup = True
                    iPres1 = 2
                End If
            End If
            If frmPROCE.g_oProcesoSeleccionado.DefPresTipo2 = EnProceso And frmPROCE.g_oProcesoSeleccionado.HayPresTipo2 Then
                bCambiarPresup = True
                iPres2 = 1
            Else
                If frmPROCE.g_oGrupoSeleccionado.DefPresTipo2 And frmPROCE.g_oGrupoSeleccionado.HayPresTipo2 Then
                    bCambiarPresup = True
                    iPres2 = 2
                End If
            End If
        End If
        
        If iPresAnu1 > 0 Or iPresAnu2 > 0 Or iPres1 > 0 Or iPres2 > 0 Then
            irespuesta = oMensajes.PreguntaReasignarPresupuestos
            If irespuesta = vbNo Then
                Screen.MousePointer = vbHourglass
                udtTeserror = oItems.ModificarValores(chkUniDef.Value, chkCantDef.Value, chkPresDef.Value, , Nothing, Nothing, Nothing, Nothing, , , chkProve.Value, lblSolicitud.Visible, chkPresPlanif.Value, basOptimizacion.gvarCodUsuario, , oEscalados)
            Else
                'pantallas de presupuestos
                'Los presupuestos que no est�n distribuidos no hace falta redistribuirlos
                vPresActual = oItems.DevolverPresupuestoActual(IIf(txtCant.Text <> "", True, False), chkCantDef.Value, IIf(txtPres.Text <> "", True, False), chkPresDef.Value, , , , , chkPresPlanif.Value)
                '*****PRESANU1
                If iPresAnu1 > 0 Then
                    If iPresAnu1 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPresAnu1 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    frmPRESAsig.g_iAmbitoPresup = iPresAnu1
                    frmPRESAsig.g_iTipoPres = 1
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo1
                    frmPRESAsig.g_sOrigen = "MODIF_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                '*****PRESANU2
                If iPresAnu2 > 0 Then
                    If iPresAnu2 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPresAnu2 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    frmPRESAsig.g_iTipoPres = 2
                    frmPRESAsig.g_iAmbitoPresup = iPresAnu2
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresAnuTipo2
                    frmPRESAsig.g_sOrigen = "MODIF_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                '*****PRES1
                If iPres1 > 0 Then
                    If iPres1 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPres1 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    frmPRESAsig.g_iTipoPres = 3
                    frmPRESAsig.g_iAmbitoPresup = iPres1
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo1
                    frmPRESAsig.g_sOrigen = "MODIF_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                '*****PRES2
                If iPres2 > 0 Then
                    If iPres2 = 1 Then
                        dPresAbierto = frmPROCE.g_oProcesoSeleccionado.DevolverPresupuestoAbierto
                    Else
                        If iPres2 = 2 Then
                            dPresAbierto = frmPROCE.g_oGrupoSeleccionado.DevolverPresupuestoAbierto
                        End If
                    End If
                    'AbiertoDespuesMod=Abierto - PresActual + PresDespuesMod
                    dPresAbiertoMod = dPresAbierto - vPresActual(0) + vPresActual(1)
                    frmPRESAsig.g_iTipoPres = 4
                    frmPRESAsig.g_iAmbitoPresup = iPres2
                    frmPRESAsig.g_bRUO = frmPROCE.g_bRUOPresTipo2
                    frmPRESAsig.g_sOrigen = "MODIF_ITEMS"
                    frmPRESAsig.g_dblAbierto = dPresAbiertoMod
                    frmPRESAsig.g_dblAsignado = dPresAbierto
                    frmPRESAsig.g_bHayPres = True
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_bModif = True
                    frmPRESAsig.Show 1
                End If
                Screen.MousePointer = vbHourglass
                udtTeserror = oItems.ModificarValores(chkUniDef.Value, chkCantDef.Value, chkPresDef.Value, , g_oPresupuestos1Nivel4, g_oPresupuestos2Nivel4, g_oPresupuestos3Nivel4, g_oPresupuestos4Nivel4, IIf(txtCant.Text <> "", True, False), IIf(txtPres.Text <> "", True, False), chkProve.Value, lblSolicitud.Visible, chkPresPlanif.Value, basOptimizacion.gvarCodUsuario, bRecalcularOfertas, oEscalados)
            End If
        Else
            Screen.MousePointer = vbHourglass
            udtTeserror = oItems.ModificarValores(chkUniDef.Value, chkCantDef.Value, chkPresDef.Value, , , , , , , , chkProve.Value, lblSolicitud.Visible, chkPresPlanif.Value, basOptimizacion.gvarCodUsuario, bRecalcularOfertas, oEscalados)
        End If
    End If
    
    If udtTeserror.NumError <> TESnoerror Then
        Screen.MousePointer = vbNormal
        
        If udtTeserror.NumError <> TESVolumenAdjDirSuperado Then
            basErrores.TratarError udtTeserror
            
            Set oItems = Nothing
            Set oItem = Nothing
            Set g_oPresupuestos1Nivel4 = Nothing
            Set g_oPresupuestos2Nivel4 = Nothing
            Set g_oPresupuestos3Nivel4 = Nothing
            Set g_oPresupuestos4Nivel4 = Nothing
            Unload Me
            frmPROCE.sdbgItems.SelBookmarks.RemoveAll
            Exit Sub
        Else
            If frmPROCE.chkIgnoraRestrAdjDir.Value = vbUnchecked Then basErrores.TratarError udtTeserror
        End If
    Else
        bErrores = udtTeserror.Arg2
        vErrores = udtTeserror.Arg1
        If bErrores Then
            Screen.MousePointer = vbNormal
            oMensajes.ImposibleModificarMultiplesItems vErrores, oItems.Count
            Screen.MousePointer = vbHourglass
        End If
    End If
    Set oItem = Nothing
    Set oNItem = Nothing
    
    LockWindowUpdate frmPROCE.hWnd 'Para bloquear
    Dim bmAct As Variant
    bmAct = frmPROCE.sdbgItems.Bookmark
    Dim bmSel As Variant
    bmSel = frmPROCE.sdbgItems.SelBookmarks

    If udtTeserror.NumError = TESVolumenAdjDirSuperado Then
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
            Set oItem = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))))
            frmPROCE.sdbgItems.Columns("UNI").Value = oItem.UniCod
            frmPROCE.sdbgItems.Columns("CANT").Value = oItem.Cantidad
            frmPROCE.sdbgItems.Columns("PREC").Value = oItem.Precio
            If IsNumeric(oItem.Cantidad) And IsNumeric(oItem.Precio) Then
                frmPROCE.sdbgItems.Columns("PRES").Value = CDec(oItem.Cantidad * oItem.Precio)
            Else
                frmPROCE.sdbgItems.Columns("PRES").Value = Null
            End If
            If frmPROCE.sdbgItems.Columns("DEST").Visible Then
                If sdbcDestCod.Value <> "" Then
                    frmPROCE.sdbgItems.Columns("DEST").Value = sdbcDestCod.Value
                    sAcciones = sAcciones & " Dest:" & sdbcDestCod.Value
                End If
            End If
            If frmPROCE.sdbgItems.Columns("PAG").Visible Then
                If sdbcPagoCod.Value <> "" Then
                    frmPROCE.sdbgItems.Columns("PAG").Value = sdbcPagoCod.Value
                    sAcciones = sAcciones & " Pago:" & sdbcPagoCod.Value
                End If
            End If
            If frmPROCE.sdbgItems.Columns("PROVE").Visible Then
                If sdbcProveCod.Value <> "" Or chkProve.Value = vbChecked Then
                    frmPROCE.sdbgItems.Columns("PROVE").Value = oItem.ProveAct
                    sAcciones = sAcciones & " Prove:" & oItem.ProveAct
                End If
            End If
            If frmPROCE.sdbgItems.Columns("SOLICITUD").Visible And Not frmPROCE.sdbgItems.Columns("SOLICITUD").Locked Then
                If lblSolicitud.caption <> "" Then
                    frmPROCE.sdbgItems.Columns("SOLICITUD").Value = lblSolicitud.Tag
                    sAcciones = sAcciones & " Sol:" & lblSolicitud.Tag
                End If
            End If
            If frmPROCE.sdbgItems.Columns("INI").Visible Then
                If txtFecIni.Text <> "" Then
                    frmPROCE.sdbgItems.Columns("INI").Value = txtFecIni.Text
                    sAcciones = sAcciones & " FecIni:" & txtFecIni.Text
                End If
            End If
            If frmPROCE.sdbgItems.Columns("FIN").Visible Then
                If txtFecFin.Text <> "" Then
                    frmPROCE.sdbgItems.Columns("FIN").Value = txtFecFin.Text
                    sAcciones = sAcciones & " FecFin:" & txtFecFin.Text
                End If
            End If
            frmPROCE.sdbgItems.Columns("FECACT").Value = oItem.FECACT
            'Recalcular los presupuestos
            frmPROCE.g_dPresGrupo = (frmPROCE.g_dPresGrupo - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.g_oProcesoSeleccionado.PresAbierto = (frmPROCE.g_oProcesoSeleccionado.PresAbierto - frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))) + frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.sdbgItems.Columns("PRESACTUAL").Value = frmPROCE.sdbgItems.Columns("PRES").Value
            frmPROCE.sdbgItems.Columns("CANTOLD").Value = oItem.Cantidad
            frmPROCE.sdbgItems.Columns("PRECOLD").Value = oItem.Precio
            Set oItem = Nothing
        Next
               
    Else
        frmPROCE.g_bUpdate = False
        For i = 0 To frmPROCE.sdbgItems.SelBookmarks.Count - 1
            If vErrores(1, i) = 0 Then 'Ning�n error
                frmPROCE.sdbgItems.Bookmark = frmPROCE.sdbgItems.SelBookmarks.Item(i)
                Set oItem = oItems.Item(CStr(frmPROCE.sdbgItems.Columns("ID").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i))))
                frmPROCE.sdbgItems.Columns("UNI").Value = oItem.UniCod
                'Registro la acci�n (cambio en la cantidad del item)
                If frmPROCE.sdbgItems.Columns("CANT").Value <> oItem.Cantidad Then
                    basSeguridad.RegistrarAccion ACCCondOfeCantidad, "Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Cod:" & frmPROCE.sdbcProceCod.Value & " Id:" & frmPROCE.sdbgItems.Columns("ID").Value
                End If
                frmPROCE.sdbgItems.Columns("CANT").Value = oItem.Cantidad
                frmPROCE.sdbgItems.Columns("PREC").Value = oItem.Precio
                frmPROCE.sdbgItems.Columns("PRES").Value = oItem.Presupuesto
                
                If frmPROCE.sdbgItems.Columns("DEST").Visible Then
                    If sdbcDestCod.Value <> "" Then
                        frmPROCE.sdbgItems.Columns("DEST").Value = sdbcDestCod.Value
                        sAcciones = sAcciones & "Dest:" & sdbcDestCod.Value
                    End If
                End If
                If frmPROCE.sdbgItems.Columns("PAG").Visible Then
                    If sdbcPagoCod.Value <> "" Then
                        frmPROCE.sdbgItems.Columns("PAG").Value = sdbcPagoCod.Value
                        sAcciones = sAcciones & " Pago:" & sdbcPagoCod.Value
                    End If
                End If
                If frmPROCE.sdbgItems.Columns("PROVE").Visible Then
                    If sdbcProveCod.Value <> "" Or chkProve.Value = vbChecked Then
                        frmPROCE.sdbgItems.Columns("PROVE").Value = oItem.ProveAct
                        sAcciones = sAcciones & " Prove:" & oItem.ProveAct
                    End If
                End If
                If frmPROCE.sdbgItems.Columns("SOLICITUD").Visible And Not frmPROCE.sdbgItems.Columns("SOLICITUD").Locked Then
                    If lblSolicitud.caption <> "" Then
                        frmPROCE.sdbgItems.Columns("SOLICITUD").Value = lblSolicitud.Tag
                        sAcciones = sAcciones & "Solicitud:" & lblSolicitud.Tag
                    End If
                End If
                If frmPROCE.sdbgItems.Columns("INI").Visible Then
                    If txtFecIni.Text <> "" Then
                        frmPROCE.sdbgItems.Columns("INI").Value = txtFecIni.Text
                        sAcciones = sAcciones & "FecIni:" & txtFecIni.Text
                    End If
                End If
                If frmPROCE.sdbgItems.Columns("FIN").Visible Then
                    If txtFecFin.Text <> "" Then
                        frmPROCE.sdbgItems.Columns("FIN").Value = txtFecFin.Text
                        sAcciones = sAcciones & "FecFin:" & txtFecFin.Text
                    End If
                End If
                If txtCant.Text <> "" Then
                    sAcciones = sAcciones & "Cant:" & oItem.Cantidad
                End If
                frmPROCE.sdbgItems.Columns("FECACT").Value = oItem.FECACT
                
                If Me.chkPresDef.Value = vbChecked Then
                    If IsNull(oItem.Precio) Or IsEmpty(oItem.Precio) Or Not IsNumeric(oItem.Precio) Then
                        oItem.Precio = oItem.Escalados.PresupuestoCantidad(oItem.Cantidad)
                    End If
                    frmPROCE.sdbgItems.Columns("PREC").Value = oItem.Precio
                    If Not IsNull(oItem.Cantidad) And Not IsEmpty(oItem.Cantidad) And Not IsNull(oItem.Precio) And Not IsEmpty(oItem.Precio) And IsNumeric(oItem.Cantidad) And IsNumeric(oItem.Precio) Then
                        oItem.Presupuesto = oItem.Cantidad * oItem.Precio
                    End If
                    frmPROCE.sdbgItems.Columns("PRES").Value = oItem.Presupuesto
                End If
                'Recalcular los presupuestos
                frmPROCE.g_dPresGrupo = (frmPROCE.g_dPresGrupo - StrToDbl0(frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))) + StrToDbl0(frmPROCE.sdbgItems.Columns("PRES").Value)
                frmPROCE.g_oProcesoSeleccionado.PresAbierto = (frmPROCE.g_oProcesoSeleccionado.PresAbierto - StrToDbl0(frmPROCE.sdbgItems.Columns("PRESACTUAL").CellValue(frmPROCE.sdbgItems.SelBookmarks.Item(i)))) + StrToDbl0(frmPROCE.sdbgItems.Columns("PRES").Value)
                sAcciones = sAcciones & " Pres:" & frmPROCE.g_oProcesoSeleccionado.PresAbierto
                frmPROCE.sdbgItems.Columns("PRESACTUAL").Value = frmPROCE.sdbgItems.Columns("PRES").Value
                frmPROCE.sdbgItems.Columns("CANTOLD").Value = oItem.Cantidad
                frmPROCE.sdbgItems.Columns("PRECOLD").Value = oItem.Precio
                
                Set oItem = Nothing
                
                basSeguridad.RegistrarAccion accionessummit.ACCProceModifMultItems, "CODIGO:" & frmPROCE.sdbgItems.Columns("COD").Value & sAcciones '& " Uni:" & frmPROCE.sdbgItems.Columns("UNI").Value & " Cant:" & frmPROCE.sdbgItems.Columns("CANT").Value & " Pres:" & frmPROCE.sdbgItems.Columns("PRES").Value
                
                sAcciones = ""
                
            End If
        Next
        'Registramos llamada a recalcular
        If bRecalcularOfertas Then
             basSeguridad.RegistrarAccion accionessummit.ACCCondOfeRecalculo, "Llamada desde:Apertura Anyo:" & frmPROCE.sdbcAnyo.Value & " GMN1:" & frmPROCE.sdbcGMN1_4Cod.Value & " Cod:" & frmPROCE.sdbcProceCod.Value
        End If
    End If
    
    If StrToDbl0(frmPROCE.lblPresGrupo) <> frmPROCE.g_dPresGrupo Then
        frmPROCE.lblPresGrupo = Format(frmPROCE.g_dPresGrupo, "#,##0.00####################")
        frmPROCE.lblPresTotalProce = Format(frmPROCE.g_oProcesoSeleccionado.PresAbierto, "#,##0.00####################")
    End If
    
    frmPROCE.sdbgItems.Update
    frmPROCE.g_bUpdate = True
    Screen.MousePointer = vbNormal
        
    Set oItems = Nothing
    Set oItem = Nothing
    Set oEscalado = Nothing
    Set oEscalados = Nothing
    Set g_oPresupuestos1Nivel4 = Nothing
    Set g_oPresupuestos2Nivel4 = Nothing
    Set g_oPresupuestos3Nivel4 = Nothing
    Set g_oPresupuestos4Nivel4 = Nothing
    Screen.MousePointer = vbNormal
    Unload Me
    
    frmPROCE.CargarGridItems 0

    frmPROCE.sdbgItems.Bookmark = bmAct
    LockWindowUpdate 0& ' Para desbloquear
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "cmdAceptar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>Comprueba si se ha establecido presupuesto para los escalados</summary>
''' <remarks>Llamada desde ConfigurarControles</remarks>
''' <revision>LTG 06/02/2012</revision>

Private Function EscaladosConPresupuestos() As Boolean
    Dim i As Integer
    Dim vBmk As Variant
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    EscaladosConPresupuestos = False
    
    With SSDBGridEscalados
        For i = 0 To .Rows - 1
            vBmk = .AddItemBookmark(i)
            
            If .Columns("PRES").CellValue(vBmk) <> "" Then
                EscaladosConPresupuestos = True
                Exit For
            End If
        Next
    End With
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "EscaladosConPresupuestos", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Function
   End If
End Function

Private Sub cmdBorrarSolicitud_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicitud.caption = ""
    lblSolicitud.Tag = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "cmdBorrarSolicitud_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdBuscarSolicitud_Click()
    'LLama al formulario de b�squeda de solicitudes
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmSolicitudBuscar.g_sOrigen = "frmItemModificarValores"
    frmSolicitudBuscar.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "cmdBuscarSolicitud_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecFin_Click()
    AbrirFormCalendar Me, txtFecFin, "FIN"
End Sub
''' <summary>
''' Realiza la llamada al calendario para luego almacenar el valor.
''' </summary>
''' <remarks>Llamada desde: Sistema;Tiempo m�ximo=0</remarks>
Private Sub cmdCalFecIni_Click()
    AbrirFormCalendar Me, txtFecIni, "INI"
End Sub

Private Sub cmdCancelar_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "cmdCancelar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Load()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------

m_bActivado = False
oFSGSRaiz.pg_sFrmCargado Me.Name, True
    CargarRecursos

    PonerFieldSeparator Me

    ConfigurarControles

    Me.Top = MDI.ScaleHeight / 2 - Me.Height / 2
    Me.Left = MDI.ScaleWidth / 2 - Me.Width / 2


    Set m_oDestinos = oFSGSRaiz.Generar_CDestinos
    Set m_oPagos = oFSGSRaiz.generar_CPagos
    Set m_oProves = oFSGSRaiz.generar_CProveedores
    Set m_oUnidades = oFSGSRaiz.Generar_CUnidades


    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then ' Escalados
        CargarEscalados
        
        'Calculamos las  dimensiones del form
        Dim separador As Long: separador = 180
        Dim SeparadorV As Long: SeparadorV = 80

        'Me.Width = 5670
        Dim nFilas As Long
        nFilas = Me.SSDBGridEscalados.Rows
        If nFilas > 4 Then nFilas = 4
        Me.SSDBGridEscalados.Height = 280 + (nFilas * Me.SSDBGridEscalados.RowHeight)
        Me.PicPresEsc.Height = SSDBGridEscalados.Top + SSDBGridEscalados.Height

        'Ajustar las columnas del grid
        Dim Scroll As Long
            Scroll = 200
        Dim Marcadores As Long
            Marcadores = 380
        
         If g_ogrupo.TipoEscalados = ModoRangos Then
             Me.SSDBGridEscalados.Columns("INI").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
             Me.SSDBGridEscalados.Columns("FIN").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.3
             Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.4
         Else
             Me.SSDBGridEscalados.Columns("DIR").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
             Me.SSDBGridEscalados.Columns("PRES").Width = (Me.SSDBGridEscalados.Width - Scroll - Marcadores) * 0.5
         End If
    
        'Establecer la unidad
        Me.sdbcUnidadCod.Text = Me.sCodUnidades
        sdbcUnidadCod_Validate (False)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      m_bDescargarFrm = False
        oMensajes.MensajeOKOnly m_sMsgError, Critical
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set m_oDestinos = Nothing
    Set m_oPagos = Nothing
    Set m_oProves = Nothing
    Set m_oUnidades = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestCod.DroppedDown Then
        sdbcDestCod.Value = ""
        sdbcDestDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCod.Value = "..." Then
        sdbcDestCod.Text = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
    sdbcDestCod.Text = sdbcDestCod.Columns(0).Text
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_DropDown()
Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestCod.RemoveAll
    
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestCod.Value), , , g_bRDest)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestCod.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestCod.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestCod.AddItem ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestCod.Rows = 0 Then
        sdbcDestCod.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestCod.DataFieldList = "Column 0"
    sdbcDestCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestCod_PositionList(ByVal Text As String)
PositionList sdbcDestCod, Text
End Sub

Private Sub sdbcDestCod_Validate(Cancel As Boolean)
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestCod.Text = "" Then Exit Sub

    If sdbcDestCod.Text = sdbcDestCod.Columns(0).Text Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = sdbcDestCod.Columns(1).Text
        m_bDestRespetarCombo = False
        Exit Sub
    End If

    If sdbcDestCod.Text = sdbcDestDen.Columns(1).Text Then
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
        m_bDestRespetarCombo = False
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    ''' Solo continuamos si existe el destino
    If g_bRDest Then
         m_oDestinos.CargarTodosLosDestinosUON oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3, , , UCase(CStr(sdbcDestCod.Value)), , True
    Else
         m_oDestinos.CargarTodosLosDestinos UCase(CStr(sdbcDestCod.Value)), , True, , , , , , , , True
    End If
    
    If m_oDestinos.Count = 0 Then
        sdbcDestCod.Text = ""
        Screen.MousePointer = vbNormal
    Else
        m_bDestRespetarCombo = True
        sdbcDestDen.Text = m_oDestinos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den

        sdbcDestCod.Columns(0).Text = sdbcDestCod.Text
        sdbcDestCod.Columns(1).Text = sdbcDestDen.Text

        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = False
    End If

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bDestRespetarCombo Then
        m_bDestRespetarCombo = True
        sdbcDestCod.Text = ""
        m_bDestRespetarCombo = False
        m_bDestCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcDestDen.DroppedDown Then
        sdbcDestDen.Value = ""
        sdbcDestCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcDestDen.Value = "..." Then
        sdbcDestDen.Text = ""
        Exit Sub
    End If
    
    m_bDestRespetarCombo = True
    sdbcDestCod.Text = sdbcDestDen.Columns(1).Text
    sdbcDestDen.Text = sdbcDestDen.Columns(0).Text
    m_bDestRespetarCombo = False
    
    m_bDestCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcDestDen_DropDown()
Dim ADORs As Ador.Recordset
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcDestDen.RemoveAll
    
    If m_bDestCargarComboDesde Then
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestDen.Value), , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(CStr(sdbcDestDen.Value), , , g_bRDest)
        End If
    Else
        If Not oUsuarioSummit.Persona Is Nothing Then
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
        Else
            Set ADORs = m_oDestinos.DevolverTodosLosDestinos(, , , g_bRDest)
        End If
    End If
    
    If ADORs Is Nothing Then
        sdbcDestDen.RemoveAll
        Screen.MousePointer = vbNormal
        sdbcDestDen.AddItem ""
        Exit Sub
    End If
    
    While Not ADORs.EOF
        sdbcDestDen.AddItem ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value & Chr(m_lSeparador) & ADORs("DESTINOCOD").Value & Chr(m_lSeparador) & ADORs("DESTINODIR").Value & Chr(m_lSeparador) & ADORs("DESTINOPOB").Value & Chr(m_lSeparador) & ADORs("DESTINOCP").Value & Chr(m_lSeparador) & ADORs("DESTINOPAI").Value & Chr(m_lSeparador) & ADORs("DESTINOPROVI").Value
        ADORs.MoveNext
    Wend
    
    If sdbcDestDen.Rows = 0 Then
        sdbcDestDen.AddItem ""
    End If
    
    ADORs.Close
    Set ADORs = Nothing

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcDestDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcDestDen.DataFieldList = "Column 0"
    sdbcDestDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcDestDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcDestDen_PositionList(ByVal Text As String)
PositionList sdbcDestDen, Text
End Sub

Private Sub sdbcPagoCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Text = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPagoCod.DroppedDown Then
        sdbcPagoCod.Value = ""
        sdbcPagoDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPagoCod.Value = "..." Then
        sdbcPagoCod.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoDen.Text = sdbcPagoCod.Columns(1).Text
    sdbcPagoCod.Text = sdbcPagoCod.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcPagoCod.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, CStr(sdbcPagoCod.Value), , , False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , , False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoCod.AddItem "..."
    End If

    sdbcPagoCod.SelStart = 0
    sdbcPagoCod.SelLength = Len(sdbcPagoCod.Text)
    sdbcPagoCod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPagoCod.DataFieldList = "Column 0"
    sdbcPagoCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcPagoCod_PositionList(ByVal Text As String)
PositionList sdbcPagoCod, Text
End Sub

Private Sub sdbcPagoCod_Validate(Cancel As Boolean)
Dim oPagos As CPagos
Dim bExiste As Boolean
        
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oPagos = oFSGSRaiz.generar_CPagos
    If sdbcPagoCod.Text = "" Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    oPagos.CargarTodosLosPagos sdbcPagoCod.Text, , True, , False
    Screen.MousePointer = vbNormal
    
    bExiste = Not (oPagos.Count = 0)
    
    If Not bExiste Then
        sdbcPagoCod.Text = ""
    Else
        m_bPagoRespetarCombo = True
        sdbcPagoDen.Text = oPagos.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
        sdbcPagoCod.Columns(0).Value = sdbcPagoCod.Text
        sdbcPagoCod.Columns(1).Value = sdbcPagoDen.Text
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = False
    End If
    
    Set oPagos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bPagoRespetarCombo Then
        m_bPagoRespetarCombo = True
        sdbcPagoCod.Text = ""
        m_bPagoRespetarCombo = False
        m_bPagoCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcPagoDen.DroppedDown Then
        sdbcPagoDen.Value = ""
        sdbcPagoCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcPagoDen.Value = "..." Then
        sdbcPagoDen.Text = ""
        Exit Sub
    End If
    
    m_bPagoRespetarCombo = True
    sdbcPagoCod.Text = sdbcPagoDen.Columns(1).Text
    sdbcPagoDen.Text = sdbcPagoDen.Columns(0).Text
    m_bPagoRespetarCombo = False
    
    m_bPagoCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    Set m_oPagos = Nothing
    Set m_oPagos = oFSGSRaiz.generar_CPagos
     
    sdbcPagoDen.RemoveAll
    
    If m_bPagoCargarComboDesde Then
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , CStr(sdbcPagoDen.Value), True, False
    Else
        m_oPagos.CargarTodosLosPagosDesde gParametrosInstalacion.giCargaMaximaCombos, , , True, False
    End If
    
    Codigos = m_oPagos.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcPagoDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oPagos.EOF Then
        sdbcPagoDen.AddItem "..."
    End If

    sdbcPagoDen.SelStart = 0
    sdbcPagoDen.SelLength = Len(sdbcPagoDen.Text)
    sdbcPagoDen.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcPagoDen.DataFieldList = "Column 0"
    sdbcPagoDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcPagoDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcPagoDen_PositionList(ByVal Text As String)
PositionList sdbcPagoDen, Text
End Sub

Private Sub sdbcProveCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarComboProve Then
        m_bRespetarComboProve = True
        sdbcProveDen.Text = ""
        m_bRespetarComboProve = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveCod.DroppedDown Then
        sdbcProveCod.Value = ""
        sdbcProveDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Value = "..." Then
        sdbcProveCod.Text = ""
        Exit Sub
    End If
      
    If sdbcProveCod.Value = "" Then Exit Sub
    
    m_bRespetarComboProve = True
    sdbcProveDen.Text = sdbcProveCod.Columns(1).Text
    sdbcProveCod.Text = sdbcProveCod.Columns(0).Text
    m_bRespetarComboProve = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveCod.RemoveAll
            
    m_oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, Trim(sdbcProveCod.Text), , , , False
    
    Codigos = m_oProves.DevolverLosCodigos

    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveCod.AddItem Codigos.Cod(i) & Chr(m_lSeparador) & Codigos.Den(i)
    Next
    
    If Not m_oProves.EOF Then
        sdbcProveCod.AddItem "..."
    End If
    
    sdbcProveCod.SelStart = 0
    sdbcProveCod.SelLength = Len(sdbcProveCod.Text)
    sdbcProveCod.Refresh
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveCod.DataFieldList = "Column 0"
    sdbcProveCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveCod_PositionList(ByVal Text As String)
PositionList sdbcProveCod, Text
End Sub

Private Sub sdbcProveCod_Validate(Cancel As Boolean)
Dim bExiste As Boolean
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveCod.Text = "" Then Exit Sub
    
    ''' Solo continuamos si existe el proveedor

    m_oProves.CargarTodosLosProveedoresDesde3 1, Trim(sdbcProveCod.Text), , True, , False

    bExiste = Not (m_oProves.Count = 0)
    If bExiste Then bExiste = (UCase(m_oProves.Item(1).Cod) = UCase(sdbcProveCod.Text))
    
    If Not bExiste Then
        sdbcProveCod.Text = ""
    Else
        m_bRespetarComboProve = True
        sdbcProveDen.Text = m_oProves.Item(1).Den
        sdbcProveCod.Columns(0).Value = sdbcProveCod.Text
        sdbcProveCod.Columns(1).Value = sdbcProveDen.Text
        m_bRespetarComboProve = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub sdbcProveDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bRespetarComboProve Then
        m_bRespetarComboProve = True
        sdbcProveCod.Text = ""
        m_bRespetarComboProve = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcProveDen.DroppedDown Then
        sdbcProveCod.Value = ""
        sdbcProveDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcProveDen.Value = "..." Then
        sdbcProveDen.Text = ""
        Exit Sub
    End If
    
    If sdbcProveDen.Value = "" Then Exit Sub
    
    m_bRespetarComboProve = True
    sdbcProveCod.Text = sdbcProveDen.Columns(1).Text
    sdbcProveDen.Text = sdbcProveDen.Columns(0).Text
    m_bRespetarComboProve = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_DropDown()
Dim Codigos As TipoDatosCombo
Dim i As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Screen.MousePointer = vbHourglass
    
    sdbcProveDen.RemoveAll

    m_oProves.CargarTodosLosProveedoresDesde3 gParametrosInstalacion.giCargaMaximaCombos, , sdbcProveDen.Value, , True, False
    
    Codigos = m_oProves.DevolverLosCodigos
    
    For i = 0 To UBound(Codigos.Cod) - 1
        sdbcProveDen.AddItem Codigos.Den(i) & Chr(m_lSeparador) & Codigos.Cod(i)
    Next
    
    If Not m_oProves.EOF Then
        sdbcProveDen.AddItem "..."
    End If

    sdbcProveDen.SelStart = 0
    sdbcProveDen.SelLength = Len(sdbcProveDen.Text)
    sdbcProveCod.Refresh

    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcProveDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcProveDen.DataFieldList = "Column 0"
    sdbcProveDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcProveDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub


Private Sub sdbcProveDen_PositionList(ByVal Text As String)
PositionList sdbcProveDen, Text
End Sub


Private Sub sdbcUnidadCod_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadCod_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadCod.DroppedDown Then
        sdbcUnidadCod.Value = ""
        sdbcUnidadDen.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadCod_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Value = "..." Then
        sdbcUnidadCod.Text = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadDen.Text = sdbcUnidadCod.Columns(1).Text
    sdbcUnidadCod.Text = sdbcUnidadCod.Columns(0).Text
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadCod_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_DropDown()
Dim oUni As CUnidad

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.RemoveAll

    Screen.MousePointer = vbHourglass
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades CStr(sdbcDestCod.Value)
    Else
        m_oUnidades.CargarTodasLasUnidades
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadCod.AddItem oUni.Cod & Chr(m_lSeparador) & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
    Next

    If sdbcUnidadCod.Rows = 0 Then
        sdbcUnidadCod.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadCod_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadCod.DataFieldList = "Column 0"
    sdbcUnidadCod.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadCod_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadCod_PositionList(ByVal Text As String)
PositionList sdbcUnidadCod, Text
End Sub

Private Sub sdbcUnidadCod_Validate(Cancel As Boolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadCod.Text = "" Then Exit Sub

    If sdbcUnidadCod.Text = sdbcUnidadCod.Columns(0).Text Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = sdbcUnidadCod.Columns(1).Text
        m_bUniRespetarCombo = False
        Exit Sub
    End If

    If sdbcUnidadCod.Text = sdbcUnidadDen.Columns(1).Text Then
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = sdbcUnidadDen.Columns(0).Text
        m_bUniRespetarCombo = False
        Exit Sub
    End If
    
    ''' Solo continuamos si existe el destino
    m_oUnidades.CargarTodasLasUnidades CStr(sdbcUnidadCod.Value), , True
    If m_oUnidades.Count = 0 Then
        sdbcUnidadCod.Text = ""
        Screen.MousePointer = vbNormal
    Else
        m_bUniRespetarCombo = True
        sdbcUnidadDen.Text = m_oUnidades.Item(1).Denominaciones.Item(gParametrosInstalacion.gIdioma).Den

        sdbcUnidadCod.Columns(0).Text = sdbcUnidadCod.Text
        sdbcUnidadCod.Columns(1).Text = sdbcUnidadDen.Text

        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadCod_Validate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_Change()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not m_bUniRespetarCombo Then
        m_bUniRespetarCombo = True
        sdbcUnidadCod.Text = ""
        m_bUniRespetarCombo = False
        m_bUniCargarComboDesde = True
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadDen_Change", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not sdbcUnidadDen.DroppedDown Then
        sdbcUnidadDen.Value = ""
        sdbcUnidadCod.Value = ""
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadDen_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_CloseUp()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If sdbcUnidadDen.Value = "..." Then
        sdbcUnidadDen.Text = ""
        Exit Sub
    End If
    
    m_bUniRespetarCombo = True
    sdbcUnidadCod.Text = sdbcUnidadDen.Columns(1).Text
    sdbcUnidadDen.Text = sdbcUnidadDen.Columns(0).Text
    m_bUniRespetarCombo = False
    
    m_bUniCargarComboDesde = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadDen_CloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_DropDown()
Dim oUni As CUnidad
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.RemoveAll
    
    Screen.MousePointer = vbHourglass
    
    If m_bUniCargarComboDesde Then
        m_oUnidades.CargarTodasLasUnidades , CStr(sdbcUnidadDen.Value), , True
    Else
        m_oUnidades.CargarTodasLasUnidades , , , True
    End If

    For Each oUni In m_oUnidades
        sdbcUnidadDen.AddItem oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den & Chr(m_lSeparador) & oUni.Cod
    Next

    If sdbcUnidadDen.Rows = 0 Then
        sdbcUnidadDen.AddItem ""
    End If

    Set oUni = Nothing
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadDen_DropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_InitColumnProps()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sdbcUnidadDen.DataFieldList = "Column 0"
    sdbcUnidadDen.DataFieldToDisplay = "Column 0"
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "sdbcUnidadDen_InitColumnProps", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub sdbcUnidadDen_PositionList(ByVal Text As String)
PositionList sdbcUnidadDen, Text
End Sub

Public Sub PonerSolicitudSeleccionada(ByVal oSolic As CInstancia)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    lblSolicitud.caption = oSolic.Id & " " & oSolic.DescrBreve
    lblSolicitud.Tag = oSolic.Id
            
    Set oSolic = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "PonerSolicitudSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>Establece la posici�n y el tama�o de los controles del formulario</summary>
''' <remarks>Llamada desde ConfigurarControles</remarks>
''' <revision>LTG 06/02/2012</revision>

Private Sub Arrange()
    Const TopLine1 = 1305

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If gParametrosGenerales.gbOBLUnidadMedida And bArtCodif And chkUni.Visible Then
        ' Si hay escalados ya se han ocultado previamente las opciones de unidad
        chkUni.Visible = False
        lblValorUni.Visible = False
        sdbcUnidadCod.Visible = False
        sdbcUnidadDen.Visible = False
        picUni.Visible = False
        chkUniDef.Visible = False
        Line1.Visible = False
        
        chkCant.Top = 1470 - TopLine1
        chkPres.Top = 2325 - TopLine1
        chkProve.Top = 4550 - TopLine1
        cmdAceptar.Top = 5700 - TopLine1
        cmdBorrarSolicitud.Top = 5250 - TopLine1
        cmdBuscarSolicitud.Top = 5250 - TopLine1
        cmdCalFecFin.Top = 4900 - TopLine1
        cmdCalFecIni.Top = 4900 - TopLine1
        cmdCancelar.Top = 5700 - TopLine1
        lblDest.Top = 3490 - TopLine1
        lblFecFin.Top = 4945 - TopLine1
        lblFecIni.Top = 4945 - TopLine1
        lblLitSolicit.Top = 5295 - TopLine1
        lblPago.Top = 3850 - TopLine1
        lblProve.Top = 4210 - TopLine1
        lblSolicitud.Top = 5250 - TopLine1
        Line2.Y1 = 2190 - TopLine1
        Line2.Y2 = 2190 - TopLine1
        Line3.Y1 = 3370 - TopLine1
        Line3.Y2 = 3370 - TopLine1
        picCant.Top = 1680 - TopLine1
        picPresu.Top = 2550 - TopLine1
        sdbcDestCod.Top = 3490 - TopLine1
        sdbcDestDen.Top = 3490 - TopLine1
        sdbcPagoCod.Top = 3850 - TopLine1
        sdbcPagoDen.Top = 3850 - TopLine1
        sdbcProveCod.Top = 4210 - TopLine1
        sdbcProveDen.Top = 4210 - TopLine1
        txtFecFin.Top = 4900 - TopLine1
        txtFecIni.Top = 4900 - TopLine1
        
        Me.Height = 6540 - TopLine1
    Else
        Me.Height = 6540
                                
        chkCant.Top = 1470
        chkPres.Top = 2325
        chkProve.Top = 4550
        cmdAceptar.Top = 5700
        cmdBorrarSolicitud.Top = 5250
        cmdBuscarSolicitud.Top = 5250
        cmdCalFecFin.Top = 4900
        cmdCalFecIni.Top = 4900
        cmdCancelar.Top = 5700
        lblDest.Top = 3490
        lblFecFin.Top = 4945
        lblFecIni.Top = 4945
        lblLitSolicit.Top = 5295
        lblPago.Top = 3850
        lblProve.Top = 4210
        lblSolicitud.Top = 5250
        Line2.Y1 = 2190
        Line2.Y2 = 2190
        Line3.Y1 = 3370
        Line3.Y2 = 3370
        picCant.Top = 1680
        picPresu.Top = 2550
        sdbcDestCod.Top = 3490
        sdbcDestDen.Top = 3490
        sdbcPagoCod.Top = 3850
        sdbcPagoDen.Top = 3850
        sdbcProveCod.Top = 4210
        sdbcProveDen.Top = 4210
        txtFecFin.Top = 4900
        txtFecIni.Top = 4900
        
    End If
            
    If frmPROCE.chkEscaladoPrecios.Value = vbChecked Then
        PicPresEsc.Top = picPresu.Top
        
        'Si la opci�n de escalados est� activada hay que desplazar hacia abajo todos los controles de la parte inferior
        Dim desp As Long: desp = PicPresEsc.Height - picPresu.Height
                
        If Me.chkUni.Visible = False Then
            Dim despUni As Long
            despUni = Me.Line1.Y1
            Me.chkCant.Top = Me.chkCant.Top - despUni
            Me.picCant.Top = Me.picCant.Top - despUni
            Me.chkPres.Top = Me.chkPres.Top - despUni
            Me.PicPresEsc.Top = Me.PicPresEsc.Top - despUni
            Me.Line2.Y1 = Me.Line2.Y1 - despUni
            Me.Line2.Y2 = Me.Line2.Y1
            desp = desp - despUni
            'Me.Height = Me.Height - despUni
        End If
                        
        Line3.Y1 = Line3.Y1 + desp
        Line3.Y2 = Line3.Y2 + desp
        lblDest.Top = lblDest.Top + desp
        sdbcDestCod.Top = sdbcDestCod.Top + desp
        sdbcDestDen.Top = sdbcDestDen.Top + desp
        lblPago.Top = lblPago.Top + desp
        sdbcPagoCod.Top = sdbcPagoCod.Top + desp
        sdbcPagoDen.Top = sdbcPagoDen.Top + desp
        lblProve.Top = lblProve.Top + desp
        sdbcProveCod.Top = sdbcProveCod.Top + desp
        sdbcProveDen.Top = sdbcProveDen.Top + desp
        txtFecFin.Top = txtFecFin.Top + desp
        txtFecIni.Top = txtFecIni.Top + desp
        chkProve.Top = chkProve.Top + desp
        lblFecIni.Top = lblFecIni.Top + desp
        lblFecFin.Top = lblFecFin.Top + desp
        cmdCalFecIni.Top = cmdCalFecIni.Top + desp
        cmdCalFecFin.Top = cmdCalFecFin.Top + desp
        lblLitSolicit.Top = lblLitSolicit.Top + desp
        lblSolicitud.Top = lblSolicitud.Top + desp
        cmdBorrarSolicitud.Top = cmdBorrarSolicitud.Top + desp
        cmdBuscarSolicitud.Top = cmdBuscarSolicitud.Top + desp
        cmdAceptar.Top = cmdAceptar.Top + desp
        cmdCancelar.Top = cmdCancelar.Top + desp
        Me.Height = Me.Height + desp
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "Arrange", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

'--<summary>
'--Carga los escalados en el grid
'--</summary>
'--<remarks>Llamada desde Form-Load</remarks>
'--<revision>DPD 09/08/2011</revision>
Sub CargarEscalados()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
Set g_ogrupo = frmPROCE.g_oGrupoSeleccionado
g_ogrupo.CargarEscalados

If Not g_ogrupo.Escalados Is Nothing Then

    Dim cEsc As CEscalado
    Dim Linea As String
    For Each cEsc In g_ogrupo.Escalados
    
        Linea = cEsc.Id & Chr(m_lSeparador)
        Linea = Linea & cEsc.Inicial & Chr(m_lSeparador) & cEsc.Inicial & Chr(m_lSeparador)
        
        If g_ogrupo.TipoEscalados = ModoRangos Then
            Linea = Linea & cEsc.final & Chr(m_lSeparador)
        Else
            Linea = Linea & "" & Chr(m_lSeparador)
        End If
        
        Linea = Linea & cEsc.Presupuesto
        
        Me.SSDBGridEscalados.AddItem Linea

    Next
    
    Set cEsc = Nothing
End If

    If g_ogrupo.TipoEscalados = ModoRangos Then
        ModoARangos
    Else
        ModoADirecto
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "CargarEscalados", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

'Establece el modo de escalado en Rangos
Sub ModoARangos()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = False
    SSDBGridEscalados.Columns("INI").Visible = True
    SSDBGridEscalados.Columns("FIN").Visible = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "ModoARangos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub
'Establece el modo de escalado en Cantidades directas
Sub ModoADirecto()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSDBGridEscalados.Columns("DIR").Visible = True
    SSDBGridEscalados.Columns("INI").Visible = False
    SSDBGridEscalados.Columns("FIN").Visible = False
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "ModoADirecto", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub


'--<summary>
'--Comprueba que se han introducido correctamente los presupuestos unitarios necesarios
'--En caso de encontrar un valor no  v�lido muestra un mensaje, y se posiciona en la celda en modo edici�n
'--</summary>
'--<remarks>Click Bot�n Aceptar</remarks>
'--<revision>DPD 08/08/2011</revision>

Function ComprobarFormulario() As Boolean
    
    Dim vPres As Variant
    Dim i As Long
    Dim bm As Variant ' Bookmark
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Function
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    For i = 0 To Me.SSDBGridEscalados.Rows
        'Comprobamos los presupuestos
        bm = SSDBGridEscalados.AddItemBookmark(i)
        vPres = SSDBGridEscalados.Columns("PRES").CellValue(bm)
        If Not IsNumeric(vPres) Then
            oMensajes.valorCampoNoValido
            SSDBGridEscalados.Bookmark = bm
            SSDBGridEscalados.col = SSDBGridEscalados.Columns("PRES").Position
            SSDBGridEscalados.Columns("PRES").Value = " "
            SSDBGridEscalados.Columns("PRES").Value = ""
            ComprobarFormulario = False
            Exit Function
        End If
    Next i

    ComprobarFormulario = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmItemModificarValores", "ComprobarFormulario", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function
