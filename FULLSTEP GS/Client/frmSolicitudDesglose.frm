VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{5A9433E9-DD7B-4529-91B6-A5E8CA054615}#2.0#0"; "IGUltraGrid20.ocx"
Begin VB.Form frmSolicitudDesglose 
   Caption         =   "Form1"
   ClientHeight    =   4815
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11370
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSolicitudDesglose.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4815
   ScaleWidth      =   11370
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox picNavigate 
      BorderStyle     =   0  'None
      Height          =   380
      Left            =   120
      ScaleHeight     =   375
      ScaleWidth      =   7815
      TabIndex        =   1
      Top             =   4420
      Width           =   7815
      Begin VB.CommandButton cmdMultiFila 
         Height          =   312
         Left            =   1080
         Picture         =   "frmSolicitudDesglose.frx":0CB2
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   0
         Visible         =   0   'False
         Width           =   432
      End
      Begin VB.CommandButton cmdAplicar 
         Caption         =   "&DAplicar cambios"
         Height          =   345
         Left            =   1080
         TabIndex        =   9
         Top             =   0
         Visible         =   0   'False
         Width           =   1515
      End
      Begin VB.CommandButton cmdAbrirProc 
         Caption         =   "Abrir &proceso"
         Height          =   345
         Left            =   4305
         TabIndex        =   6
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPedidoDir 
         Caption         =   "Pedido directo"
         Height          =   345
         Left            =   5580
         TabIndex        =   5
         Top             =   0
         Width           =   1200
      End
      Begin VB.CommandButton cmdDeshacer 
         Caption         =   "&Deshacer cambios"
         Height          =   345
         Left            =   2685
         TabIndex        =   4
         Top             =   0
         Visible         =   0   'False
         Width           =   1515
      End
      Begin VB.CommandButton cmdAnyaLinea 
         Height          =   312
         Left            =   0
         Picture         =   "frmSolicitudDesglose.frx":1087
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   0
         Width           =   432
      End
      Begin VB.CommandButton cmdElimLinea 
         Height          =   312
         Left            =   540
         Picture         =   "frmSolicitudDesglose.frx":1109
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   0
         Width           =   432
      End
   End
   Begin UltraGrid.SSUltraGrid SSLineasTab 
      Height          =   4125
      Left            =   5730
      TabIndex        =   8
      Top             =   450
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   7276
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   68157440
      Override        =   "frmSolicitudDesglose.frx":119B
      Caption         =   "SSLineasTab"
   End
   Begin MSComctlLib.TabStrip SSTabGrupos 
      Height          =   4695
      Left            =   5580
      TabIndex        =   7
      Top             =   60
      Width           =   5745
      _ExtentX        =   10134
      _ExtentY        =   8281
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin UltraGrid.SSUltraGrid ssLineas 
      Height          =   4305
      Left            =   30
      TabIndex        =   0
      Top             =   60
      Width           =   5445
      _ExtentX        =   9604
      _ExtentY        =   7594
      _Version        =   131072
      GridFlags       =   17040384
      LayoutFlags     =   68157440
      Override        =   "frmSolicitudDesglose.frx":11F1
      Caption         =   "DDesglose"
   End
End
Attribute VB_Name = "frmSolicitudDesglose"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Variables p�blicas:
Public g_oCampoDesglose As CFormItem
Public g_oInstanciaSeleccionada As CInstancia
Public g_bModif As Boolean
Public g_sCodOrganizacionCompras As String
Public g_sCodCentro As String
Public m_sUON1Sel As String
Public m_sUON2Sel As String
Public m_sUON3Sel As String
Public m_sUODescrip As String
Public g_sOrigen As String
Public g_strPK As String

'Variables privadas
Private m_sValorCTablaExterna As String
Private m_strPK_old As String
Private m_sProveCod As String
Private m_sValorPres As String
Private m_sMat As String
Private m_bCalculando As Boolean
Public m_sPerCod As String
Private m_bValorListaGridSeleccionado As Boolean 'Esta variable estara a true en el momento que se seleccione un elemento de un campo que sea lista, asi en el evento CloseUp se sabra que realmente se ha seleccionado un valor y no se ha desplegado la lista y no se ha seleccionado nada o dejado el valor que ya estaba

'Variables de idiomas
Private m_sIdiSolicitud As String
Private m_sIdiTrue As String
Private m_sIdiFalse As String
Private m_sMensaje(7) As String
Private m_sIdiKB As String
Private m_sIdiGrupo As String
Private m_sIdiErrorEvaluacion(2) As String
Private m_sSeleccionarMatFormulario As String

'Para el filtro de material
Private m_sGMN1Cod As String
Private m_sGMN2Cod As String
Private m_sGMN3Cod As String
Private m_sGMN4Cod As String

'Seguridad:
Private m_bRestrDest As Boolean
Private m_bRestrMat As Boolean
Private m_bRestrProve As Boolean
Private m_bRestrPresUO As Boolean

Private m_sMensajeDenArt(2) As String

Private m_oTablaExterna As CTablaExterna

'3114
Private m_bPermProcMultimaterial As Boolean  'Permitir abrir procesos multimaterial

Private m_bAbrirProc As Boolean
Private m_bPedidoDirecto As Boolean

Private m_sNumLinea As String
Public m_iNumLinea As Integer
Public g_bSeleccionable As Boolean
Private m_bActivado As Boolean
Public m_bDescargarFrm As Boolean
Private m_sMsgError As String
Private m_sTotalAdjudicado As String

Private m_sTotalPreadjudicado As String
Private bNoValidarCelda As Boolean
Private m_iMaxLinBd As Integer 'numero m�ximo linea en bbdd.
'Las lineas q insertes seran m_iMaxLinBd +1, +2, etc. Si borras una linea insertada como ya te ha hecho m_iMaxLinBd +1, evitas tener dos lineas numero 7. La colecci�n usa el numero de linea.

'GS. Detalle de solicitud. Aplicar la cumplimentaci�n de los campos del formulario.
Public g_oCumplimentacionesGS As CSolCumplimentacionesGs

Public Sub CargarProveedorConBusqueda()
    Dim oProves As CProveedores
    Dim oCampo As CFormItem
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then Exit Sub
   
    Set oProves = frmPROVEBuscar.oProveEncontrados
    Set frmPROVEBuscar.oProveEncontrados = Nothing

    m_sProveCod = oProves.Item(1).Cod
    ssLineas.ActiveCell.Value = oProves.Item(1).Cod & " - " & oProves.Item(1).Den
    
    'Muestra el bot�n de aplicar y deshacer
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If
    
    Set oProves = Nothing
    'Si al seleccionar un proveedor, en la linea del desglose esta el campo PROVEEDOR ERP se borrara si este tuviera un valor
    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
            Exit For
        End If
    Next

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CargarProveedorConBusqueda", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub CrearValueList()
Dim oCampo As CFormItem
Dim oValorLista As CCampoValorLista
Dim ADORs As Ador.Recordset
Dim oPagos As CPagos
Dim oDestinos As CDestinos
Dim oPago As CPago
Dim oUnidades As CUnidades
Dim oUni As CUnidad
Dim oPaises As CPaises
Dim oPais As CPais
Dim oMonedas As CMonedas
Dim oMoneda As CMoneda
Dim oOrganizacionCompras As COrganizacionCompras

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    With ssLineas.ValueLists
        If ssLineas.ValueLists.Exists("ListaBool") = False Then
            .Add "ListaBool"
            .Item("ListaBool").ValueListItems.Add m_sIdiTrue
            .Item("ListaBool").ValueListItems.Add m_sIdiFalse
        End If
    End With
     
    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            Select Case oCampo.CampoGS
                Case TipoCampoGS.Dest
                    If ssLineas.ValueLists.Exists("ListaDestinos") = False Then
                        ssLineas.ValueLists.Add "ListaDestinos"
                        
                        Set oDestinos = oFSGSRaiz.Generar_CDestinos
                        If Not oUsuarioSummit.Persona Is Nothing Then
                            Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest, oUsuarioSummit.Persona.UON1, oUsuarioSummit.Persona.UON2, oUsuarioSummit.Persona.UON3)
                        Else
                            Set ADORs = oDestinos.DevolverTodosLosDestinos(, , , m_bRestrDest)
                        End If
                        While Not ADORs.EOF
                            ssLineas.ValueLists.Item("ListaDestinos").ValueListItems.Add ADORs("DESTINOCOD").Value, ADORs("DESTINOCOD").Value & " - " & ADORs("DEN_" & gParametrosInstalacion.gIdioma).Value
                            ADORs.MoveNext
                        Wend
                        ADORs.Close
                        Set ADORs = Nothing
                        Set oDestinos = Nothing
                    End If
        
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaDestinos"
                    
                Case TipoCampoGS.FormaPago
                    If ssLineas.ValueLists.Exists("ListaFPago") = False Then
                        ssLineas.ValueLists.Add "ListaFPago"
                    
                        Set oPagos = oFSGSRaiz.generar_CPagos
                        oPagos.CargarTodosLosPagos
                        For Each oPago In oPagos
                            ssLineas.ValueLists.Item("ListaFPago").ValueListItems.Add oPago.Cod, oPago.Cod & " - " & oPago.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Next
                        Set oPagos = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaFPago"
                    
                Case TipoCampoGS.Unidad
                    If ssLineas.ValueLists.Exists("ListaUnidad") = False Then
                        ssLineas.ValueLists.Add "ListaUnidad"
                        
                        Set oUnidades = oFSGSRaiz.Generar_CUnidades
                        oUnidades.CargarTodasLasUnidades , , , , False
                        For Each oUni In oUnidades
                            ssLineas.ValueLists.Item("ListaUnidad").ValueListItems.Add oUni.Cod, oUni.Cod & " - " & oUni.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Next
                        Set oUnidades = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaUnidad"
                    
                Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                    If ssLineas.ValueLists.Exists("ListaArt") = False Then
                        ssLineas.ValueLists.Add "ListaArt"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaArt"
                
                Case TipoCampoGS.DenArticulo
                    If ssLineas.ValueLists.Exists("ListaDen") = False Then
                        ssLineas.ValueLists.Add "ListaDen"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaDen"
                Case TipoCampoGS.ProveedorERP
                    If ssLineas.ValueLists.Exists("ListaProveedoresERP") = False Then
                        ssLineas.ValueLists.Add "ListaProveedoresERP"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaProveedoresERP"
                
                Case TipoCampoGS.Pais
                    If ssLineas.ValueLists.Exists("ListaPais") = False Then
                        ssLineas.ValueLists.Add "ListaPais"
                        
                        Set oPaises = oFSGSRaiz.Generar_CPaises
                        oPaises.CargarTodosLosPaises , , , True
                        For Each oPais In oPaises
                            ssLineas.ValueLists.Item("ListaPais").ValueListItems.Add oPais.Cod, oPais.Cod & " - " & oPais.Den
                        Next
                        Set oPaises = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaPais"
                    
                Case TipoCampoGS.provincia
                    If ssLineas.ValueLists.Exists("ListaProvi") = False Then
                        ssLineas.ValueLists.Add "ListaProvi"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaProvi"
                    
                Case TipoCampoGS.Moneda
                    If ssLineas.ValueLists.Exists("ListaMoneda") = False Then
                        ssLineas.ValueLists.Add "ListaMoneda"
                        
                        Set oMonedas = oFSGSRaiz.Generar_CMonedas
                        oMonedas.CargarTodasLasMonedas
                        For Each oMoneda In oMonedas
                            ssLineas.ValueLists.Item("ListaMoneda").ValueListItems.Add oMoneda.Cod, oMoneda.Cod & " - " & oMoneda.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den
                        Next
                        Set oMonedas = Nothing
                    End If
                    
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaMoneda"
             
                Case TipoCampoGS.Departamento
                    If ssLineas.ValueLists.Exists("ListaDepartamentos") = False Then
                        ssLineas.ValueLists.Add "ListaDepartamentos"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaDepartamentos"
                
                Case TipoCampoGS.OrganizacionCompras
                    If ssLineas.ValueLists.Exists("ListaOrganizacionCompras") = False Then
                        ssLineas.ValueLists.Add "ListaOrganizacionCompras"
                        
                        Dim oOrganizacionesCompras As COrganizacionesCompras
                        Set oOrganizacionesCompras = oFSGSRaiz.Generar_COrganizacionesCompras
                        oOrganizacionesCompras.CargarOrganizacionesCompra
                        For Each oOrganizacionCompras In oOrganizacionesCompras.OrganizacionesCompras
                            ssLineas.ValueLists.Item("ListaOrganizacionCompras").ValueListItems.Add oOrganizacionCompras.Cod, oOrganizacionCompras.Cod & " - " & oOrganizacionCompras.Den
                        Next
                        Set oOrganizacionesCompras = Nothing
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaOrganizacionCompras"
                Case TipoCampoGS.ProveedorERP
                    If ssLineas.ValueLists.Exists("ListaProveedoresERP") = False Then
                        ssLineas.ValueLists.Add "ListaProveedoresERP"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaProveedoresERP"
                
                Case TipoCampoGS.Centro
                    If ssLineas.ValueLists.Exists("ListaCentros") = False Then
                        ssLineas.ValueLists.Add "ListaCentros"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaCentros"
                    
                Case TipoCampoGS.Almacen
                    If ssLineas.ValueLists.Exists("ListaAlmacenes") = False Then
                        ssLineas.ValueLists.Add "ListaAlmacenes"
                    End If
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaAlmacenes"
                    
            End Select
            
        Else
            If oCampo.TipoIntroduccion = TAtributoIntroduccion.Introselec Then  'Es una lista
                oCampo.CargarValoresLista
                If ssLineas.ValueLists.Exists("Lista" & oCampo.Id) = False Then
                    ssLineas.ValueLists.Add "Lista" & oCampo.Id
                End If
                For Each oValorLista In oCampo.ValoresLista
                    Select Case oCampo.Tipo
                        Case TiposDeAtributos.TipoFecha
                            ssLineas.ValueLists.Item("Lista" & oCampo.Id).ValueListItems.Add CDate(oValorLista.valorFec)
                        Case TiposDeAtributos.TipoNumerico
                            ssLineas.ValueLists.Item("Lista" & oCampo.Id).ValueListItems.Add oValorLista.valorNum
                        Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            ssLineas.ValueLists.Item("Lista" & oCampo.Id).ValueListItems.Add oValorLista.Orden, oValorLista.valorText.Item(gParametrosInstalacion.gIdioma).Den
                    End Select
                Next
                
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "Lista" & oCampo.Id
                
            Else  'Introducci�n libre de tipo boolean
                If oCampo.Tipo = TiposDeAtributos.TipoBoolean Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).ValueList = "ListaBool"
                End If
            End If
        End If
    Next
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CrearValueList", err, Erl, , m_bActivado)
      Exit Sub
   End If
        
End Sub

Private Function NumRegistrosMultifila() As Long
Dim intContLineas As Long
Dim oRow As SSRow

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
intContLineas = 0

Set oRow = ssLineas.GetRow(ssChildRowFirst)

Do While Not oRow Is Nothing

    intContLineas = intContLineas + 1

    If oRow.HasNextSibling = True Then
        Set oRow = oRow.GetSibling(ssSiblingRowNext)
    Else
        Set oRow = Nothing
    End If
Loop
    
NumRegistrosMultifila = intContLineas
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "NumRegistrosMultifila", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Private Function LiteralAdjuntos(ByVal oLinea As CLineaDesglose) As String
Dim sAdjuntos As String
Dim oAdjunto As CCampoAdjunto

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sAdjuntos = ""
    For Each oAdjunto In oLinea.Adjuntos
        sAdjuntos = sAdjuntos & oAdjunto.nombre & " (" & TamanyoAdjuntos(oAdjunto.DataSize / 1024) & m_sIdiKB & ");"
    Next
    If sAdjuntos <> "" Then
        sAdjuntos = Mid$(sAdjuntos, 1, Len(sAdjuntos) - 1)
    End If
    
    LiteralAdjuntos = sAdjuntos
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "LiteralAdjuntos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Function ModificarAdjuntos(ByVal oLinea As CLineaDesglose)
    Dim scod As String
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Function
   
    If g_bModif = False Then Exit Function
    If Not EscrituraParaDetalleSolicGS(oLinea.CampoHijo.IdCampoDef) Then Exit Function
    
    scod = oLinea.Linea & "$" & CStr(oLinea.CampoHijo.CampoPadre.Id) & "$" & CStr(oLinea.CampoHijo.Id)
    
    If g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
        g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oLinea.CampoHijo, Null, Null, Null, Null, oLinea.FECACT, , oLinea.Instancia
    End If
    
    ssLineas.ActiveCell.Value = LiteralAdjuntos(oLinea)
    
    'Muestra el bot�n de aplicar y deshacer
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If

    Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ModificarAdjuntos", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

Public Sub PonerMatSeleccionado()
    Dim tArray() As String
    Dim i As Byte
    Dim oFila As SSRow
    Dim blnBorrame As Boolean
    Dim oGmn As Variant
    Dim sNuevoCod1 As String
    Dim sNuevoCod2 As String
    Dim sNuevoCod3 As String
    Dim sNuevoCod4 As String
    Dim sGridValor As String
    Dim oCampo As CFormItem
    Dim bEdit As Boolean
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------


    
    Set oGmn = frmSELMAT.oGMNSeleccionado
    
    If Not oGmn Is Nothing Then
        sNuevoCod1 = oGmn.GMN1Cod
        sNuevoCod2 = oGmn.GMN2Cod
        sNuevoCod3 = oGmn.GMN3Cod
        sNuevoCod4 = oGmn.GMN4Cod
        
        sGridValor = oGmn.titulo
        m_sMat = sNuevoCod1 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(sNuevoCod1))
        m_sMat = m_sMat & sNuevoCod2 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(sNuevoCod2))
        m_sMat = m_sMat & sNuevoCod3 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(sNuevoCod3))
        m_sMat = m_sMat & sNuevoCod4 & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(sNuevoCod4))
    End If

    
    Set oGmn = Nothing
    
    bEdit = ssLineas.IsInEditMode
    If bEdit = True Then
        ssLineas.PerformAction ssKeyActionExitEditMode
    End If
    ssLineas.ActiveCell.Value = sGridValor
    If bEdit = True Then
        ssLineas.PerformAction ssKeyActionEnterEditMode
    End If
    
    For Each oCampo In g_oCampoDesglose.Desglose  'Quita el valor de los art�culos
        If oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Or oCampo.CampoGS = TipoCampoGS.DenArticulo Then
            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
            blnBorrame = True
        ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
            If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                Set oFila = ssLineas.ActiveRow
                m_sValorCTablaExterna = ValorColumTablaExterna(0)
                Set ssLineas.ActiveRow = oFila
                If m_sValorCTablaExterna <> "" Then
                    tArray = Split(m_sValorCTablaExterna, "#")
                    For i = LBound(tArray) To UBound(tArray)
                        ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                        ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                    Next
                End If
            End If
        End If
     Next
    SacarColorRiesgo ssLineas
    'Muestra el bot�n de aplicar y deshacer
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "PonerMatSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga los idiomas del formulario.
''' </summary>
''' <remarks>Llamada desde=Form_load; Tiempo m�ximo=0,2</remarks>
Private Sub CargarRecursos()
    Dim Ador As Ador.Recordset
    Dim i As Integer
    
    ' EN PRIMER LUGAR SE CARGAN ELEMENTOS DEL FORMULARIO
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    On Error Resume Next
    
    Set Ador = oGestorIdiomas.DevolverTextosDelModulo(FRM_COPIA_DESGLOSE, basPublic.gParametrosInstalacion.gIdioma)
    
    If Not Ador Is Nothing Then
        ssLineas.caption = Ador(0).Value  '1 Desglose de material
        SSLineasTab.caption = ssLineas.caption
        Ador.MoveNext
        cmdDeshacer.caption = Ador(0).Value  '2 &Deshacer cambios
        Ador.MoveNext
        m_sIdiSolicitud = Ador(0).Value '3 solicitud
        Ador.MoveNext
        m_sIdiTrue = Ador(0).Value  '4 S�
        Ador.MoveNext
        m_sIdiFalse = Ador(0).Value  '5 No
        Ador.MoveNext
        m_sIdiKB = Ador(0).Value  '6 kb
        Ador.MoveNext
        m_sIdiGrupo = Ador(0).Value '7 Grupo
        For i = 1 To 7
            Ador.MoveNext
            m_sMensaje(i) = Ador(0).Value
        Next i
        Ador.MoveNext
        m_sIdiErrorEvaluacion(1) = Ador(0).Value  '15 Error al realizar el c�lculo:F�rmula inv�lida.
        Ador.MoveNext
        m_sIdiErrorEvaluacion(2) = Ador(0).Value  '16 Error al realizar el c�lculo:Valores incorrectos.
        For i = 1 To 2
            Ador.MoveNext
            m_sMensajeDenArt(i) = Ador(0).Value
        Next i
        Ador.MoveNext
        cmdAbrirProc.caption = Ador(0).Value
        Ador.MoveNext
        cmdPedidoDir.caption = Ador(0).Value
        Ador.MoveNext
        m_sNumLinea = Ador(0).Value
        Ador.MoveNext
        cmdAplicar.caption = Ador(0).Value
        Ador.MoveNext
        m_sTotalAdjudicado = Ador(0).Value
        Ador.MoveNext
        m_sTotalPreadjudicado = Ador(0).Value
        Ador.MoveNext
        m_sSeleccionarMatFormulario = Ador(0).Value
        Ador.Close
    End If
    
    Set Ador = Nothing

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CargarRecursos", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga los campos de desglose en el grid
''' </summary>
''' <remarks>Llamada desde: cmdDeshacer_Click    CargaSinTabs   frmSolicitudDetalle/cmdDeshacer_Click; Tiempo m�ximo: 0,2</remarks>
Public Sub CargarValoresDesglose()
    Dim oLinea As CLineaDesglose
  
    'Carga los campos de desglose:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If (Me.g_sOrigen = "frmPROCEItem") Then
        g_oCampoDesglose.CargarDesglose False, True, UsarLinea:=True
    Else
        g_oCampoDesglose.CargarDesglose False, False, UsarLinea:=True
    End If
    
    m_sGMN1Cod = NullToStr(g_oCampoDesglose.GMN1Cod)
    m_sGMN2Cod = NullToStr(g_oCampoDesglose.GMN2Cod)
    m_sGMN3Cod = NullToStr(g_oCampoDesglose.GMN3Cod)
    m_sGMN4Cod = NullToStr(g_oCampoDesglose.GMN4Cod)
    
    'Asigna a la grid los valores de las l�neas de desglose:
    Set ssLineas.DataSource = g_oCampoDesglose.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse, , m_iNumLinea)
    
    For Each oLinea In g_oCampoDesglose.LineasDesglose
        If m_iMaxLinBd < oLinea.Linea Then m_iMaxLinBd = oLinea.Linea
    Next
    

    
    'Forzar selecci�n simple (impedir multiselecci�n)
    ssLineas.Override.SelectTypeRow = ssSelectTypeSingle
    
    'Crea las listas de dropdown:
    ssLineas.ValueLists.clear
    CrearValueList
    

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CargarValoresDesglose", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

''' <summary>
''' Abrir proceso
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdAbrirProc_Click()
    Dim Ador As Ador.Recordset
    Dim iResp As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_bPermProcMultimaterial Then
        Set Ador = g_oInstanciaSeleccionada.DevolverMaterialesInstancia_Campo
        If Ador.RecordCount > 1 Then
            iResp = oMensajes.MensajeYesNo(948)
            If iResp = vbNo Then
                Exit Sub
            End If
        Else
            Set Ador = g_oInstanciaSeleccionada.DevolverMaterialesInstancia_Desglose
            If Ador.RecordCount > 1 Then
                iResp = oMensajes.MensajeYesNo(948)
                If iResp = vbNo Then
                    Exit Sub
                End If
            End If
        End If
    End If
        
    Dim arSolicitudes(0) As CInstancia
    Set arSolicitudes(0) = g_oInstanciaSeleccionada
    
    frmSOLAbrirProc.g_arSolicitudes = arSolicitudes
    frmSOLAbrirProc.m_iGenerar = 0
    frmSOLAbrirProc.Show vbModal
    Set Ador = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "cmdAbrirProc_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Sub cmdAnyaLinea_Click()
Dim lLinea As Long
Dim lLineaColeccion As Long
Dim oFila As SSRow
Dim oCampo As CFormItem
Dim bEdit As Boolean
Dim sValue As String
Dim oGmn As Variant
Dim lineaDefecto As New ADODB.Recordset
Dim col As Integer
Dim i As Integer
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ssLineas.Bands(0).Columns.Count = 1 Then Exit Sub
    
    If ssLineas.HasRows = True Then
        Set oFila = ssLineas.GetRow(ssChildRowLast)
        lLinea = oFila.Cells("LINEA").Value + 1
        lLineaColeccion = m_iMaxLinBd + 1
        m_iMaxLinBd = m_iMaxLinBd + 1
    Else
        lLinea = 1
        lLineaColeccion = 1
    End If
    
    'a�ade una nueva fila a la grid
    ssLineas.Bands(0).AddNew
    
    'Pone el foco en el campo c�digo de la nueva fila:
    ssLineas.Selected.Cells.clear
    ssLineas.ActiveRow.Cells("LINEA").Value = lLinea
    If Me.Visible Then ssLineas.SetFocus
    ssLineas.PerformAction ssKeyActionEnterEditMode
        
    'si s�lo se pueden seleccionar unos materiales en concreto le pone el de por defecto que se ha seleccionado:
    If m_sGMN1Cod <> "" And m_sGMN2Cod <> "" And m_sGMN3Cod <> "" And m_sGMN4Cod <> "" Then
    
        Set oGmn = createGMN(m_sGMN1Cod, m_sGMN2Cod, m_sGMN3Cod, m_sGMN4Cod)
        oGmn.cargarDenominacion
        sValue = sValue & ",'" & DblQuote(oGmn.titulo) & "'"
        i = oGmn.DevolverRiesgo
        Set oGmn = Nothing
                
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.material Then
                m_sMat = m_sGMN1Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN1 - Len(m_sGMN1Cod))
                m_sMat = m_sMat & m_sGMN2Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN2 - Len(m_sGMN2Cod))
                m_sMat = m_sMat & m_sGMN3Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN3 - Len(m_sGMN3Cod))
                m_sMat = m_sMat & m_sGMN4Cod & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGMN4 - Len(m_sGMN4Cod))
                
                bEdit = ssLineas.IsInEditMode
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionExitEditMode
                End If
                
                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = sValue
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionEnterEditMode
                End If
                
                Select Case i
                    Case 1  'Alto
                        ssLineas.ActiveRow.Appearance.Forecolor = &HC0&
                    Case 2  'Medio
                        ssLineas.ActiveRow.Appearance.Forecolor = &H80FF&
                    Case 3 'Bajo
                        ssLineas.ActiveRow.Appearance.Forecolor = &H8000&
                    Case Else
                        ssLineas.ActiveRow.Appearance.Forecolor = vbBlack
                End Select
                ssLineas.Update
                Exit For
            End If
        Next
    End If
    
    Set lineaDefecto = g_oCampoDesglose.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse, , , True)
    If Not lineaDefecto.EOF Then
        For col = 0 To lineaDefecto.Fields.Count - 1
            If Not lineaDefecto.Fields.Item(col).Name = "LINEA" And Not IsNull(lineaDefecto.Fields.Item(col).Value) Then
                bEdit = ssLineas.IsInEditMode
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionExitEditMode
                End If
                bNoValidarCelda = True
                ssLineas.ActiveRow.Cells(lineaDefecto.Fields.Item(col).Name).Value = lineaDefecto.Fields.Item(col).Value
                bNoValidarCelda = False
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionEnterEditMode
                End If
                ssLineas.Update
            End If
        Next
    End If
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "cmdAnyaLinea_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdAplicar_Click()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    LockWindowUpdate Me.hWnd
    
    ssLineas.Update
    
    MostrarBotonesEdicion False, True
    
    LockWindowUpdate 0&
        
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "cmdAplicar_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdDeshacer_Click()
    Dim oGrupo As CFormGrupo
    Dim oCampo As CFormItem
    Dim bGuardar As Boolean

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
       
    SendKeys "{ESC}", True
           
    LockWindowUpdate Me.hWnd
    
    Set ssLineas.DataSource = Nothing
    CargarValoresDesglose
    '_______________________
    CargarTagTablasExternas
    '�����������������������
        
    'realiza el c�lculo total
    CalculoFormulaOrigen
    
    MostrarBotonesEdicion False, False
    
    LockWindowUpdate 0&
    
    'comprueba si ha habido cambios en la pantalla de la solicitud.si no,tb se quita el
    'bot�n de deshacer
    For Each oGrupo In g_oInstanciaSeleccionada.Grupos
        If Not oGrupo.CAMPOS Is Nothing Then
            For Each oCampo In oGrupo.CAMPOS
                If oCampo.modificado = True Then
                    bGuardar = True
                    Exit For
                End If
            Next
        End If
    Next
    If bGuardar = False Then
        If (Me.g_sOrigen = "frmPedidos") Then
            frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = True
            frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = False
        ElseIf (Me.g_sOrigen = "frmPROCE") Then
        Else
            frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = True
            frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = False
        End If
    End If
    
    cmdMultiFila.Visible = False

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "cmdDeshacer_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Elimina la l�nea de desglose de base de datos
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdElimLinea_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ssLineas.ActiveRow Is Nothing Then Exit Sub
        
    ssLineas.ActiveRow.Delete
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "cmdElimLinea_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub cmdMultiFila_Click()

Dim valorCelda As String
Dim indiceColumna As Integer
Dim oRow As SSRow
Dim intContLineas As Integer

If Not ssLineas.ActiveCell Is Nothing Then

If IsNull(ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.key).Value) Then
    valorCelda = ""
Else
    valorCelda = CStr(ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.key).Value)
End If

indiceColumna = ssLineas.ActiveCell.Column.Index

intContLineas = 0

Set oRow = ssLineas.GetRow(ssChildRowFirst)

Do While Not oRow Is Nothing
    ssLineas.ActiveRow = oRow
 
    oRow.Cells(indiceColumna).Value = valorCelda

    intContLineas = intContLineas + 1

    If oRow.HasNextSibling = True Then
        Set oRow = oRow.GetSibling(ssSiblingRowNext)
    Else
        Set oRow = Nothing
    End If
Loop

Set oRow = ssLineas.GetRow(ssChildRowFirst)
ssLineas.ActiveRow = oRow

MostrarBotonesEdicion True, True

frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False

End If

End Sub

''' <summary>
''' Crear pedido directo
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub cmdPedidoDir_Click()
    Dim irespuesta As Integer
    Dim bBloqueoEtapa As Boolean
    Dim bBloqueoCondiciones As Boolean
    Dim Ador As Ador.Recordset
    Dim iResp As Integer
    
    'Si el estado de la Instancia es distinta que aprobada, miramos si su solicitud permite los pedidos directos:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oInstanciaSeleccionada.Estado < EstadoSolicitud.Pendiente Then
        g_oInstanciaSeleccionada.Solicitud.CargarConfiguracion , , , , True
        If Not g_oInstanciaSeleccionada.Solicitud.PermitirPedidoDirecto Then
            oMensajes.NoPermitidoPedido
            Exit Sub
        End If
    End If
        
    'Si el usuario no tiene permisos para emitir pedidos directos le sale un mensaje indic�ndoselo:
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        If oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.PEDDIREmitir)) Is Nothing Then
            irespuesta = oMensajes.PreguntaGenerarPedDirecto
            If irespuesta = vbNo Then Exit Sub
        End If
    End If
    
    If gParametrosGenerales.gbSolicitudesCompras Then
        bBloqueoEtapa = False
        bBloqueoCondiciones = False
        
        'Se comprueban las condiciones de bloqueo por etapa
        bBloqueoEtapa = BloqueoEtapa(g_oInstanciaSeleccionada.Id)
        If bBloqueoEtapa Then
            oMensajes.MensajeOKOnly 904, Exclamation  'La solicitud se encuentra en una etapa en la que no se permite la emisi�n de pedidos directos
        Else
            'Se comprueban las condiciones de bloqueo por f�rmula
            bBloqueoCondiciones = BloqueoCondiciones(g_oInstanciaSeleccionada.Id, TipoBloqueo.PedidosDirectos, m_sIdiErrorEvaluacion(1), m_sIdiErrorEvaluacion(2))
        End If

        If bBloqueoEtapa Or bBloqueoCondiciones Then
            Exit Sub
        End If
    End If
    
    If m_bPermProcMultimaterial Then
        Set Ador = g_oInstanciaSeleccionada.DevolverMaterialesInstancia_Campo
        If Ador.RecordCount > 1 Then
            iResp = oMensajes.MensajeYesNo(948)
            If iResp = vbNo Then
                Exit Sub
            End If
        Else
            Set Ador = g_oInstanciaSeleccionada.DevolverMaterialesInstancia_Desglose
            If Ador.RecordCount > 1 Then
                iResp = oMensajes.MensajeYesNo(948)
                If iResp = vbNo Then
                    Exit Sub
                End If
            End If
        End If
    End If
    
    Dim arSolicitudes(0) As CInstancia
    Set arSolicitudes(0) = g_oInstanciaSeleccionada
        
    frmSOLAbrirProc.g_arSolicitudes = arSolicitudes
    frmSOLAbrirProc.g_bGenerarPedido = True
    frmSOLAbrirProc.m_iGenerar = 3
    frmSOLAbrirProc.Show vbModal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "cmdPedidoDir_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub Form_Activate()

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Unload Me
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If Not m_bActivado Then
    m_bActivado = True
End If

'    Set oRow = ssLineas.GetRow(ssChildRowFirst)
'    ssLineas.ActiveRow = oRow

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "Form_Activate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Muestra la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Load()
    Dim bVarios As Boolean
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bActivado = False
    bNoValidarCelda = False
    oFSGSRaiz.pg_sFrmCargado Me.Name, True
    Screen.MousePointer = vbNormal
    
    Set m_oTablaExterna = oFSGSRaiz.Generar_CTablaExterna
    
    Me.Height = 5325
    Me.Width = 11490
    
    If Me.Top + Me.Height > MDI.Top + MDI.ScaleHeight Or Me.Left + Me.Width > MDI.Left + MDI.ScaleWidth Then
        Me.Top = 0
        Me.Left = 0
    End If
    
    CargarRecursos
    
    ConfigurarSeguridad
    
    If Not EscrituraParaDetalleSolicGS(g_oCampoDesglose.IdCampoDef) Then
        Me.cmdAnyaLinea.Visible = False
        Me.cmdElimLinea.Visible = False
        
        Me.cmdMultiFila.Left = Me.cmdAnyaLinea.Left
        Me.cmdAplicar.Left = Me.cmdAnyaLinea.Left
        Me.cmdDeshacer.Left = cmdAplicar.Left + cmdAplicar.Width + 85
    End If
    
    If m_iNumLinea = -1 Then
        CargarTabs bVarios
        
        Me.ssTabGrupos.Visible = False
        Me.SSLineasTab.Visible = True
        Me.ssLineas.Visible = False
        
        If bVarios Then
            Me.ssTabGrupos.Visible = True
            Me.SSLineasTab.Visible = True
            Me.ssLineas.Visible = False
        End If
            
        Me.caption = m_sIdiSolicitud & ":" & g_oInstanciaSeleccionada.DescrBreve
        
        Form_Resize
    Else
        Me.ssTabGrupos.Visible = False
        Me.SSLineasTab.Visible = False
        Me.ssLineas.Visible = True
    
        Me.caption = m_sIdiSolicitud & ":" & g_oInstanciaSeleccionada.DescrBreve & "/" & m_sIdiGrupo & ":" & NullToStr(g_oCampoDesglose.Grupo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den) & "/" & NullToStr(g_oCampoDesglose.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
        
        CargaSinTabs
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "Form_Load", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Redimensiona la pantalla
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Resize()
Dim oCol As SSColumn
Dim oCampo As CFormItem

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Me.Width < 500 Then Exit Sub
    If Me.Height < 1500 Then Exit Sub
    
    If ssTabGrupos.Visible = False And Not (m_iNumLinea = -1) Then
    
        ssLineas.Width = Me.Width - 315
        If picNavigate.Visible = True Then
            ssLineas.Height = Me.Height - 1025
        Else
            ssLineas.Height = Me.Height - 645
        End If
        
        If ssLineas.Bands.Count > 0 Then
            For Each oCol In ssLineas.Bands(0).Columns
                If Not g_oCampoDesglose Is Nothing Then
                    If g_oCampoDesglose.Desglose.Count > 0 Then
                        If ((ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count) < 1000 Then
                            oCol.Width = 1000
                        Else
                            oCol.Width = (ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count
                        End If
                    Else
                        oCol.Width = 1000
                    End If
                    
                    Set oCampo = g_oCampoDesglose.Desglose.Item(Right(oCol.key, Len(oCol.key) - 2))
                    If Not oCampo Is Nothing Then
                        If oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                           oCol.Width = 2 * oCol.Width
                        ElseIf oCampo.CampoGS = TipoCampoGS.DenArticulo Then
                           oCol.Width = 3.7 * oCol.Width 'sitio como para 42 caracteres
                        End If
                    End If
                Else
                    oCol.Width = 1000
                End If
            Next
        End If
        
        If picNavigate.Visible = True Then
            picNavigate.Top = ssLineas.Top + ssLineas.Height + 65
        End If
    Else
        If ssTabGrupos.Visible Then
            SSLineasTab.caption = ""
            
            ssTabGrupos.Left = ssLineas.Left
            SSLineasTab.Left = ssTabGrupos.Left + 150
            
            SSLineasTab.Top = ssTabGrupos.Top + 450
            
            ssTabGrupos.Width = Me.Width - 315
            ssTabGrupos.Height = Me.Height - 645
            
            SSLineasTab.Height = ssTabGrupos.Height - 580
            SSLineasTab.Width = ssTabGrupos.Width - 270
        
        Else
            SSLineasTab.caption = ssLineas.caption
            
            SSLineasTab.Left = ssLineas.Left
            SSLineasTab.Top = ssLineas.Top
                    
            SSLineasTab.Height = Me.Height - 645
            SSLineasTab.Width = Me.Width - 315
        End If
        
        If SSLineasTab.Bands.Count > 0 Then
            For Each oCol In SSLineasTab.Bands(0).Columns
                If Not g_oCampoDesglose Is Nothing Then
                    If g_oCampoDesglose.Desglose.Count > 0 Then
                        If ((SSLineasTab.Width - 340) / g_oCampoDesglose.Desglose.Count) < 1000 Then
                            oCol.Width = 1000
                        Else
                            oCol.Width = (SSLineasTab.Width - 340) / g_oCampoDesglose.Desglose.Count
                        End If
                    Else
                        oCol.Width = 1000
                    End If

                    Set oCampo = g_oCampoDesglose.Desglose.Item(Right(oCol.key, Len(oCol.key) - 2))
                    If Not oCampo Is Nothing Then
                        If oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                           oCol.Width = 2 * oCol.Width
                        ElseIf oCampo.CampoGS = TipoCampoGS.DenArticulo Then
                           oCol.Width = 3.7 * oCol.Width 'sitio como para 42 caracteres
                        End If
                    End If
                Else
                    oCol.Width = 1000
                End If
            Next
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "Form_Resize", err, Erl)
      Exit Sub
   End If
End Sub

''' <summary>
''' Descarga la pantalla y los objetos
''' </summary>
''' <param name="Cancel">Cancelar el cierre</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub Form_Unload(Cancel As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        oMensajes.MensajeOKOnly m_sMsgError, Critical
    End If

    If Me.Visible Then
        Me.Hide
        Cancel = True
        Exit Sub
    End If

    If m_bDescargarFrm = False Then
        If ssLineas.IsInEditMode Then ssLineas.Update
    End If
    m_bDescargarFrm = False
    Set m_oTablaExterna = Nothing
    Set g_oCampoDesglose = Nothing
    m_sGMN1Cod = ""
    m_sGMN2Cod = ""
    m_sGMN3Cod = ""
    m_sGMN4Cod = ""
    g_bModif = False
    m_iNumLinea = 0
    m_iMaxLinBd = 0
    
    Set g_oCumplimentacionesGS = Nothing
    g_sOrigen = ""

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "Form_Unload", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_AfterCellActivate()

Dim oCellArt As SSCell
Dim bAnyadirArt As Boolean
Dim oCampo As CFormItem
Dim numRegistros As Long
    
cmdMultiFila.Visible = False

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub

    If Not IsNull(g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS) Then
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
            Set oCellArt = ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.Index - 1)
            If Not IsNull(oCellArt.Value) Then
                bAnyadirArt = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellArt.Column.key, 3))).AnyadirArt
                If oCellArt.Value <> "" Then
                    If Not esGenerico(oCellArt.Value, bAnyadirArt) Then
                        ssLineas.ActiveCell.Column.Style = ssStyleDropDownList
                    Else
                        ssLineas.ActiveCell.Column.Style = ssStyleDropDown
                    End If
                Else
                    ssLineas.ActiveCell.Column.Style = ssStyleDropDownList
                End If
            End If
  
        End If
    End If
        
    numRegistros = NumRegistrosMultifila
    
    If numRegistros > 1 Then
    
    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3)))
    
    If Not g_sOrigen = "frmVisorSolicitudes" Then
        If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                    'Si son campos de GS:
                    Select Case oCampo.CampoGS
                        Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4, TipoCampoGS.Proveedor, TipoCampoGS.Almacen, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                            MostrarBotonMultifila
                        Case Else
                            OcultarBotonMultifila
                    End Select
        ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Atributo Then
            If Not IsNull(oCampo.idAtrib) Then
                MostrarBotonMultifila
            Else
                OcultarBotonMultifila
            End If
        End If
    End If
    
    End If
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_AfterCellActivate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub SacarColorRiesgo(ByRef ssGrid As SSUltraGrid)
Dim i As Integer
Dim oMat As CGrupoMatNivel4
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim intContLineas As Integer
Dim sMat As String
Dim arrMat() As String
Dim iRiesgo As String
Dim oGMN4 As CGrupoMatNivel4
On Error GoTo Error
'---------------------------------------------------------------------------------------------
If gParametrosGenerales.glArtibRiesgo = 0 Then Exit Sub
intContLineas = 0
Set oRow = ssGrid.GetRow(ssChildRowFirst)
Do While Not oRow Is Nothing
    ssGrid.ActiveRow = oRow
    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.CampoGS = TipoCampoGS.material Then
            intContLineas = intContLineas + 1
            sMat = "" & ssGrid.ActiveRow.Cells("C_" & oCampo.Id).Value
            arrMat() = Split(sMat, "-")
            Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
            If Not UBound(arrMat) Then
                If arrMat(3) <> "" Then
                    oGMN4.GMN1Cod = Trim(arrMat(0))
                    oGMN4.GMN2Cod = Trim(arrMat(1))
                    oGMN4.GMN3Cod = Trim(arrMat(2))
                    oGMN4.Cod = Trim(arrMat(3))
                    i = oGMN4.DevolverRiesgo
                    Select Case i
                        Case 1  'Alto
                            oRow.Appearance.Forecolor = &HC0&
                        Case 2  'Medio
                            oRow.Appearance.Forecolor = &H80FF&
                        Case 3 'Bajo
                            oRow.Appearance.Forecolor = &H8000&
                        Case Else
                            oRow.Appearance.Forecolor = vbBlack
                    End Select
                End If
            End If
            Set oGMN4 = Nothing
        End If
    Next
    If oRow.HasNextSibling = True Then
        Set oRow = oRow.GetSibling(ssSiblingRowNext)
    Else
        Set oRow = Nothing
    End If
Loop

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SacarColorRiesgo", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
''' <summary>
''' Evento de sistema q responde a un cambio en una celda que se depliega de un desglose
''' </summary>
''' <param name="Cell">Celda modificada</param>
''' <remarks>Llamada desde: sistema; Tiempo m�ximo: 0,1</remarks>
Private Sub ssLineas_AfterCellListCloseUp(ByVal Cell As UltraGrid.SSCell)
Dim tArray() As String
Dim blnBorrame As Boolean
Dim oCellSiguiente As SSCell
Dim i, contTerminar As Integer
Dim sMoneda As String
Dim sArt As Variant
Dim oCampo As CFormItem
Dim sCodArt As String
Dim oLinea As CLineaDesglose
Dim scod As String
Dim oFila As SSRow
Dim contTerminarHasta As Integer

    '--------------------------------------------------------------------------------------------------------------------------------
    'cuando seleccione un pa�s se quita la provincia seleccionada anteriormente,si es que hay alguna:
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))) Is Nothing Then Exit Sub
    
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Pais Then
        Set oCellSiguiente = ssLineas.ActiveRow.Cells(Cell.Column.Index + 1)
          
        If oCellSiguiente.Value <> "" And Not IsNull(oCellSiguiente.Value) Then
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.provincia Then
                oCellSiguiente.Value = ""
            End If
        End If
    End If
    
    '--------------------------------------------------------------------------------------------------------------------------------
    'Cuando seleccionamos un c�digo de art�culo, asignar a la siguiente celda la denominaci�n
    If (g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo _
    Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo _
    ) And Cell.GetText <> "" Then
        sMoneda = "EUR"
        
        If Not g_oInstanciaSeleccionada Is Nothing Then
            sMoneda = g_oInstanciaSeleccionada.Moneda
        End If
        
        sArt = Split(Cell.GetText, " - ")
        If UBound(sArt) > 0 Then
            sCodArt = sArt(0)
        Else
            sCodArt = Cell.GetText
        End If
        
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            Set oCellSiguiente = ssLineas.ActiveRow.Cells(Cell.Column.Index + 1)
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
                If InStr(1, Cell.GetText, " - ") > 0 Then
                    oCellSiguiente.Value = Mid$(Cell.GetText, InStr(1, Cell.GetText, " - ") + 3)
                End If
            End If
        End If
        If sCodArt <> "" Then
            If esGenerico(sCodArt) Then
                contTerminarHasta = 1 'si es gen�rico solo hay que buscar la unidad y el material
            Else
                contTerminarHasta = 4 'hay que buscar el precio, proveedor, material y unidad
            End If
        End If
        
        i = 1
        contTerminar = 0
        'Buscar el PRECIO / PROVEEDOR / MATERIAL / UNIDAD
        While (i < ssLineas.ActiveRow.Cells.Count) And (contTerminar <> contTerminarHasta)
            Set oCellSiguiente = ssLineas.ActiveRow.Cells(i)
            'Material
            If Not g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))) Is Nothing Then
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.material Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverMaterialArticulo(sCodArt, gParametrosInstalacion.gIdioma, m_sMat)
                    
                    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3)))
                    scod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                    Set oLinea = g_oCampoDesglose.LineasDesglose.Item(scod)
                    If oLinea Is Nothing Then
                    Else
                        oLinea.valorText = m_sMat
                    End If
                    
                    contTerminar = contTerminar + 1
                End If
                
                If Not esGenerico(sCodArt) Then
                    'PRECIO
                    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoSC.PrecioUnitario And _
                    g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                        oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoPrecioADJ(sCodArt, sMoneda)
                        contTerminar = contTerminar + 1
                    End If
                    'PROVEEDOR
                    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Proveedor And _
                    g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                        oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoProveedorADJ(sCodArt)
                        contTerminar = contTerminar + 1
                    End If
                End If
                'Cargar la UNIDAD
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Unidad Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUnidadArticulo(sCodArt)
                    contTerminar = contTerminar + 1
                End If
            End If
            i = i + 1
        Wend
    End If
    
    '--------------------------------------------------------------------------------------------------------------------------------
    'Cuando seleccionamos una denominaci�n de un art�culo, asignar a la celda  anterior el c�digo
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo And Cell.GetText <> "" Then
        sArt = Split(Cell.GetText, " - ")
        If UBound(sArt) > 0 Then
            sCodArt = sArt(0)
        Else
            sCodArt = Cell.GetText
        End If

        Set oCellSiguiente = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            oCellSiguiente.Value = sCodArt
        End If

        sMoneda = "EUR"
        If Not g_oInstanciaSeleccionada Is Nothing Then
            sMoneda = g_oInstanciaSeleccionada.Moneda
        End If
        
        If esGenerico(sCodArt) Then
            contTerminarHasta = 1 'si es gen�rico solo hay que buscar la unidad y el material
        Else
            contTerminarHasta = 4 'hay que buscar el precio, proveedor, material y unidad
        End If
        
        i = 1
        contTerminar = 0
        'Buscar el PRECIO / PROVEEDOR / MATERIAL / UNIDAD
        While (i < ssLineas.ActiveRow.Cells.Count) And (contTerminar <> contTerminarHasta)
            Set oCellSiguiente = ssLineas.ActiveRow.Cells(i)
            'Material
            If Not g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))) Is Nothing Then
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.material Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverMaterialArticulo(sCodArt, gParametrosInstalacion.gIdioma, m_sMat)
                    
                    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3)))
                    scod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                    Set oLinea = g_oCampoDesglose.LineasDesglose.Item(scod)
                    If oLinea Is Nothing Then
                    Else
                        oLinea.valorText = m_sMat
                    End If
                    
                    contTerminar = contTerminar + 1
                End If
                If Not esGenerico(sCodArt) Then
                    'PRECIO
                    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoSC.PrecioUnitario And _
                    g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                        oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoPrecioADJ(sCodArt, sMoneda)
                        contTerminar = contTerminar + 1
                    End If
                    'PROVEEDOR
                    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Proveedor And _
                    g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CargarUltADJ = True Then
                        oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUltimoProveedorADJ(sCodArt)
                        contTerminar = contTerminar + 1
                    End If
                End If
                'Cargar la UNIDAD
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Unidad Then
                    oCellSiguiente.Value = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).DevolverUnidadArticulo(sCodArt)
                    contTerminar = contTerminar + 1
                End If
            End If
            i = i + 1
        Wend
    End If
    
    '--------------------------------------------------------------------------------------------------------------------------------
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.OrganizacionCompras Then
        If Not ((ssLineas.ActiveRow.Band.Columns.Count - 1) < (Cell.Column.Index + 1)) Then
            'Borrar el articulo, el Centro y el Almacen
            Dim bCentroEliminado As Boolean
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.CampoGS = TipoCampoGS.Centro And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    bCentroEliminado = True
                ElseIf oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 2 And bCentroEliminado Then
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                ElseIf oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                    If m_bValorListaGridSeleccionado Then 'Unicamente si se ha seleccionado algo en la combo
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    End If
                ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                     ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                     blnBorrame = True
                ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                    If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                        Set oFila = ssLineas.ActiveRow
                        m_sValorCTablaExterna = ValorColumTablaExterna(0)
                        Set ssLineas.ActiveRow = oFila
                        If m_sValorCTablaExterna <> "" Then
                            tArray = Split(m_sValorCTablaExterna, "#")
                            For i = LBound(tArray) To UBound(tArray)
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                            Next
                        End If
                    End If
                End If
            Next
        Else
            If m_bValorListaGridSeleccionado Then 'Unicamente si se ha seleccionado algo en la combo
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        Exit For
                    End If
                Next
            End If
        End If
    '--------------------------------------------------------------------------------------------------------------------------------
    'Cuando seleccionamos Centro --> Borrar el articulo y el Almacen
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
        If Not ((ssLineas.ActiveRow.Band.Columns.Count - 1) < (Cell.Column.Index + 1)) Then
           'Borra el Articulo y el Almacen
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                     ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                     blnBorrame = True
                ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                    If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                        Set oFila = ssLineas.ActiveRow
                        m_sValorCTablaExterna = ValorColumTablaExterna(0)
                        Set ssLineas.ActiveRow = oFila
                        If m_sValorCTablaExterna <> "" Then
                            tArray = Split(m_sValorCTablaExterna, "#")
                            For i = LBound(tArray) To UBound(tArray)
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                            Next
                        End If
                    End If
                End If
            Next
        End If
    End If
    
    ssLineas.Update
    
    'Muestra el bot�n de aplicar y deshacer
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If
    
    '--------------------------------------------------------------------------------------------------------------------------------
    'Cuando seleccionamos Articulo --> borramos TablaExterna relacionada con articulo
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
        ssLineas.ValueLists.Item("ListaArt").ValueListItems.clear
        If ssLineas.ValueLists.Exists("ListaArt") = False Then
            ssLineas.ValueLists.Add "ListaArt"
        End If
        ssLineas.Bands(0).Columns(Cell.Column.key).ValueList = "ListaArt"
        '_____________________________________________________________________  __
        If Not ((ssLineas.ActiveRow.Band.Columns.Count - 1) < (Cell.Column.Index + 1)) Then
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.TablaExterna <> "" And ssLineas.HasRows = True Then
                    If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                        Set oFila = ssLineas.ActiveRow
                        m_sValorCTablaExterna = ValorColumTablaExterna(0)
                        Set ssLineas.ActiveRow = oFila
                        If m_sValorCTablaExterna <> "" Then
                            tArray = Split(m_sValorCTablaExterna, "#")
                            For i = LBound(tArray) To UBound(tArray)
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                            Next
                        End If
                    End If
                End If
            Next
        End If
        '�����������������������������������������������������������������������
    '--------------------------------------------------------------------------------------------------------------------------------
    'Cuando seleccionamos DenArticulo -->
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        ssLineas.ValueLists.Item("ListaDen").ValueListItems.clear
        If ssLineas.ValueLists.Exists("ListaDen") = False Then
            ssLineas.ValueLists.Add "ListaDen"
        End If
        ssLineas.Bands(0).Columns(Cell.Column.key).ValueList = "ListaDen"
        
        If Not IsNull(sCodArt) Then
            If sCodArt <> "" Then
                If Not esGenerico(sCodArt) Then
                    ssLineas.ActiveCell.Column.Style = ssStyleDropDownList
                Else
                    ssLineas.ActiveCell.Column.Style = ssStyleDropDown
                End If
            End If
        End If
    End If
    
    m_bValorListaGridSeleccionado = False 'Una vez ya cerrado el combo se pone a false
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_AfterCellListCloseUp", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_AfterCellUpdate(ByVal Cell As UltraGrid.SSCell)
Dim scod As String
Dim oLinea As CLineaDesglose
Dim oCampo As CFormItem
Dim bAnyadir As Boolean
Dim oCalculo As CFormItem
Dim lErrCode As Integer
Dim lIndex As Integer
Dim iEq As USPExpression
Dim sVariables() As String
Dim dValues() As Double
Dim bCalculoTotal As Boolean
Dim i As Integer

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ssLineas.ActiveRow Is Nothing Then Exit Sub
    If Cell.DataChanged = False Then Exit Sub
    If Cell.Column.key = "LINEA" Then Exit Sub
    If m_bCalculando = True Then Exit Sub
    
    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3)))
    If oCampo Is Nothing Then Exit Sub
    
    Screen.MousePointer = vbHourglass
    
    scod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
    
    Set oLinea = g_oCampoDesglose.LineasDesglose.Item(scod)
    If oLinea Is Nothing Then
        Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
        oLinea.Linea = Cell.Row.Cells("LINEA").Value
        bAnyadir = True
    Else
        'Modificar
        If oLinea.Anyadido = False Then
            oLinea.modificado = True
        End If
    End If
    
    Set oLinea.CampoHijo = oCampo
    oLinea.valorBool = Null
    oLinea.valorFec = Null
    oLinea.valorText = Null
    oLinea.valorNum = Null

    
    If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
        Select Case oCampo.Tipo
            Case TipoTextoMedio
                oLinea.valorText = Cell.TagVariant
                oLinea.valorBool = Null: oLinea.valorNum = Null: oLinea.valorFec = Null
            Case TipoNumerico
                oLinea.valorNum = Cell.TagVariant
                oLinea.valorBool = Null: oLinea.valorText = Null: oLinea.valorFec = Null
            Case TipoFecha
                oLinea.valorFec = Cell.TagVariant
                oLinea.valorBool = Null: oLinea.valorText = Null: oLinea.valorNum = Null
        End Select
    Else
        If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            Select Case oCampo.CampoGS
                Case TipoCampoSC.DescrBreve, TipoCampoSC.DescrDetallada
                    oLinea.valorText = Cell.Value
                    
                Case TipoCampoSC.importe, TipoCampoSC.Cantidad, TipoCampoSC.PrecioUnitario
                    oLinea.valorNum = Cell.Value
                    
                Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                    oLinea.valorFec = Cell.Value
                
                Case TipoCampoGS.Proveedor
                    If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                        oLinea.valorText = m_sProveCod
                    End If
                Case TipoCampoGS.ProveedorERP
                    If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                        oLinea.valorText = Cell.Value
                    End If
                Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                    If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                        oLinea.valorText = m_sValorPres
                    End If
                    
                Case TipoCampoGS.Dest, TipoCampoGS.FormaPago, TipoCampoGS.Unidad, TipoCampoGS.Pais, TipoCampoGS.provincia, TipoCampoGS.Moneda
                    oLinea.valorText = Cell.Value
                    
                Case TipoCampoGS.material
                     If Cell.Value <> "" Then
                        oLinea.valorText = m_sMat
                    Else
                        oLinea.valorText = ""
                    End If
                    
                Case TipoCampoGS.CodArticulo, TipoCampoGS.NuevoCodArticulo
                    If Not IsNull(Cell.Value) Then
                        If (UBound(Split(Cell.Value, " - ")) > 0) Then
                            oLinea.valorText = Trim(Mid$(Cell.Value, 1, InStr(1, Cell.Value, " - ")))
                        Else
                            oLinea.valorText = Cell.Value
                        End If
                    End If
                
                Case TipoCampoGS.DenArticulo
                    If Not IsNull(Cell.Value) Then
                        oLinea.valorText = Cell.Value
                    End If
                    
                Case TipoCampoSC.ArchivoEspecific
                    oLinea.valorText = Cell.Value
                
                Case TipoCampoGS.CampoPersona
                    If Not IsNull(Cell.Value) And Cell.Value <> "" Then
                        oLinea.valorText = m_sPerCod
                    End If
                    
                Case TipoCampoGS.UnidadOrganizativa, TipoCampoGS.Departamento
                    oLinea.valorText = Cell.Value
                Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro
                    oLinea.valorText = Cell.Value
                Case TipoCampoGS.Almacen
                    oLinea.valorNum = Cell.Value
            End Select
            
        Else
            Select Case oCampo.Tipo
                Case TiposDeAtributos.TipoBoolean
                    If Cell.Value = m_sIdiTrue Then
                        oLinea.valorBool = 1
                    ElseIf Cell.Value = m_sIdiFalse Then
                        oLinea.valorBool = 0
                    End If
                Case TiposDeAtributos.TipoFecha
                    oLinea.valorFec = Cell.Value
                Case TiposDeAtributos.TipoNumerico
                    oLinea.valorNum = Cell.Value
                Case TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoMedio, TiposDeAtributos.TipoTextoLargo
                    If oCampo.TipoIntroduccion = TAtributoIntroduccion.Introselec Then
                        oLinea.valorText = Cell.GetText(ssMaskModeIncludeBoth)
                        oLinea.valorNum = Cell.Value
                    Else
                        oLinea.valorText = Cell.Value
                    End If
                    
                Case TiposDeAtributos.TipoArchivo, TiposDeAtributos.TipoEditor
                    oLinea.valorText = Cell.Value
            End Select
        End If
    End If
    
    
    If bAnyadir = True Then
        g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oLinea.CampoHijo, oLinea.valorNum, oLinea.valorText, oLinea.valorFec, oLinea.valorBool, , , g_oCampoDesglose.Grupo.Instancia
        g_oCampoDesglose.LineasDesglose.Item(scod).Anyadido = True
    End If
        
    
    'Si se ha modificado un campo num�rico se recalculan los campos calculados (si hay):
    If oCampo.Tipo = TiposDeAtributos.TipoNumerico Then
        If Not g_oCampoDesglose.Formulas Is Nothing Then
            If g_oCampoDesglose.Formulas.Count > 0 Then
                bCalculoTotal = False
                Set iEq = New USPExpression
                i = 1
                'Almacena las variables en un array:
                ReDim sVariables(g_oCampoDesglose.Formulas.Count)
                ReDim dValues(g_oCampoDesglose.Formulas.Count)
        
                For Each oCalculo In g_oCampoDesglose.Formulas
                    sVariables(i) = oCalculo.IdCalculo
                    scod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCalculo.Id)
                    If Not g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                        dValues(i) = NullToDbl0(g_oCampoDesglose.LineasDesglose.Item(scod).valorNum)
                    Else
                        dValues(i) = 0
                    End If
                    i = i + 1
                Next
                
                'Ahora realiza el c�lculo de cada f�rmula para la l�nea actual que se acaba de modificar:
                i = 1
                For Each oCalculo In g_oCampoDesglose.Formulas
                    If oCalculo.TipoPredef = TipoCampoPredefinido.Calculado Then
                        lIndex = iEq.Parse(oCalculo.Formula, sVariables, lErrCode)
                        If lErrCode = USPEX_NO_ERROR Then
                            dValues(i) = iEq.Evaluate(dValues)
                            'almacena en la colecci�n:
                            scod = CStr(ssLineas.ActiveRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCalculo.Id)
                            If g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then  'Insertar
                                Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
                                oLinea.Linea = Cell.Row.Cells("LINEA").Value
                                Set oLinea.CampoHijo = oCalculo
                                g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oLinea.CampoHijo, dValues(i), , , , , , g_oCampoDesglose.Grupo.Instancia
                                g_oCampoDesglose.LineasDesglose.Item(scod).Anyadido = True
                                m_bCalculando = True
                                ssLineas.ActiveRow.Cells("C_" & oCalculo.Id).Value = dValues(i)
                                m_bCalculando = False
                                bCalculoTotal = True
                            Else  'Modificar
                                If g_oCampoDesglose.LineasDesglose.Item(scod).valorNum <> dValues(i) Then
                                    g_oCampoDesglose.LineasDesglose.Item(scod).modificado = True
                                    g_oCampoDesglose.LineasDesglose.Item(scod).valorNum = dValues(i)
                                    m_bCalculando = True
                                    ssLineas.ActiveRow.Cells("C_" & oCalculo.Id).Value = dValues(i)
                                    m_bCalculando = False
                                    bCalculoTotal = True
                                End If
                            End If
                        End If
                    End If
                    i = i + 1
                Next
                
                Set iEq = Nothing
                
                'ahora realiza el c�lculo total
                If bCalculoTotal = True Then
                    CalculoFormulaOrigen
                End If
            
            End If
        End If
    End If
    
    
    Screen.MousePointer = vbNormal
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_AfterCellUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If

End Sub

Private Sub ssLineas_AfterRowsDeleted()
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    'Calcula el total de la f�rmula origen
    If basParametros.gParametrosGenerales.gsAccesoFSWS = AccesoFSWSCompleto Then
        CalculoFormulaOrigen
    End If
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_AfterRowsDeleted", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_AfterRowUpdate(ByVal Row As UltraGrid.SSRow)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    MostrarBotonesEdicion False, True
   
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_AfterRowUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Impide q se pueda acceder a celda LINEA/Activo/CentroCoste/PartidaPresupuestaria
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub ssLineas_BeforeCellActivate(ByVal Cell As UltraGrid.SSCell, ByVal Cancel As UltraGrid.SSReturnBoolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))) Is Nothing Then Exit Sub
    
    If Not IsNull(g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS) Then
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Activo _
        Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CentroCoste _
        Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.PartidaPresupuestaria Then
            Cancel = True
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_BeforeCellActivate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_BeforeCellListDropDown(ByVal Cell As UltraGrid.SSCell, ByVal Cancel As UltraGrid.SSReturnBoolean)
Dim oArticulos As CArticulos
Dim oArt As CArticulo
Dim i, contTerminar As Integer
Dim sMat As String
Dim arrMat As Variant
Dim oCampo As CFormItem
Dim oPais As CPais
Dim oProvi As CProvincia
Dim oCellAnt As SSCell
Dim oDepartamento As CDepartamento
Dim oCentro As CCentro
Dim oAlmacen As CAlmacen
Dim sCodOrgCompras, sCodCentro As String
Dim bUsarOrgCompras As Boolean
Dim vAux As Variant
Dim numRegistros As Long
Dim oCampoMaterial As CFormItem
Dim oUON As IUon
Dim oGMN4 As CGrupoMatNivel4

ReDim arrMat(4)

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))) Is Nothing Then Exit Sub
    
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo Or _
    g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Or _
    g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        If gParametrosGenerales.gbUsarOrgCompras Then
            'Comprobar si a nivel del desglose hay campo de Organizacion de compras y Centro
            i = 1
            contTerminar = 0
            bUsarOrgCompras = False
            While i <= g_oCampoDesglose.Desglose.Count And contTerminar <> 1
                If g_oCampoDesglose.Desglose.Item(i).CampoGS = TipoCampoGS.OrganizacionCompras Then
                    If Not IsNull(ssLineas.ActiveRow.Cells(i).Value) Then
                        vAux = Split(ssLineas.ActiveRow.Cells(i).Value, " - ")
                        If UBound(vAux) > 0 Then
                            sCodOrgCompras = vAux(0)
                        Else
                            sCodOrgCompras = ssLineas.ActiveRow.Cells(i).Value
                        End If
                    End If
                    
                    If g_oCampoDesglose.Desglose.Item(i + 1).CampoGS = TipoCampoGS.Centro Then
                        If Not IsNull(ssLineas.ActiveRow.Cells(i + 1).Value) Then
                            vAux = Split(ssLineas.ActiveRow.Cells(i + 1).Value, " - ")
                            If UBound(vAux) > 0 Then
                                sCodCentro = vAux(0)
                            Else
                                sCodCentro = ssLineas.ActiveRow.Cells(i + 1).Value
                            End If
                        End If
                        
                        bUsarOrgCompras = True
                    End If
                    contTerminar = contTerminar + 1
                End If
                i = i + 1
            Wend
            'Sino Comprobar a nivel de Formularios
            If Not bUsarOrgCompras And g_sCodOrganizacionCompras <> "-1" Then
                i = 1
                contTerminar = 0
                If g_sCodOrganizacionCompras <> "-1" Then
                    sCodOrgCompras = g_sCodOrganizacionCompras
                    If g_sCodCentro <> "-1" Then
                        sCodCentro = g_sCodCentro
                        contTerminar = 1
                        bUsarOrgCompras = True
                    End If
                End If
                While i <= g_oCampoDesglose.Desglose.Count And contTerminar <> 1
                    If g_oCampoDesglose.Desglose.Item(i).CampoGS = TipoCampoGS.Centro Then
                        If Not IsNull(ssLineas.ActiveRow.Cells(i).Value) Then
                            vAux = Split(ssLineas.ActiveRow.Cells(i).Value, " - ")
                            If UBound(vAux) > 0 Then
                                sCodCentro = vAux(0)
                            Else
                                sCodCentro = ssLineas.ActiveRow.Cells(i).Value
                            End If
                        End If
                        contTerminar = contTerminar + 1
                        bUsarOrgCompras = True
                    End If
                    i = i + 1
                Wend
            
            End If
        End If
        
         If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo Or _
    g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
   
            ssLineas.ValueLists.Item("ListaArt").ValueListItems.clear
        Else
            ssLineas.ValueLists.Item("ListaDen").ValueListItems.clear
        End If
        
        If m_sGMN1Cod = "" And m_sGMN2Cod = "" And m_sGMN3Cod = "" And m_sGMN4Cod = "" Then
            For Each oCampo In g_oCampoDesglose.Desglose
                If oCampo.CampoGS = TipoCampoGS.material Then
                    If g_oCampoDesglose.LineasDesglose.Item(CStr(ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id))) Is Nothing Then Exit Sub
                    sMat = NullToStr(g_oCampoDesglose.LineasDesglose.Item(CStr(ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id))).valorText)
                    If sMat <> "" Then
                        arrMat = DevolverMaterial(sMat)
                    Else
                        'Tarea 3495 - Campo Material a nivel de formulario
                        'Si no hemos seleccionado ning�n material en la linea pero tenemos un nivel de selecci�n obligatorio a nivel de formulario  solo deber�amos
                        'mostrar los art�culos de la familia seleccionada en el material del formulario.
                        'Si existe pero no tiene material seleccionado mostramos un mensaje para que el usuario rellene el material obligatorio.
                        If g_oInstanciaSeleccionada.ExisteNivelSeleccionMatObligatorio Then
                            Set oCampoMaterial = g_oInstanciaSeleccionada.getCampoTipoNivelSeleccionObligatoria()
                            If NoHayParametro(oCampoMaterial.valorText) Then
                                oMensajes.mensajeGenericoOkOnly m_sSeleccionarMatFormulario
                                Exit Sub
                            Else
                                arrMat = DevolverMaterial(oCampoMaterial.valorText)
                                frmSELMAT.nivelSeleccion = oCampoMaterial.nivelSeleccion
                                frmSELMAT.sGMN1 = arrMat(1)
                                frmSELMAT.sGmn2 = arrMat(2)
                                frmSELMAT.sGmn3 = arrMat(3)
                                frmSELMAT.sGmn4 = arrMat(4)
                            End If
                        End If
                    End If
                    Exit For
                End If
            Next
        Else
            arrMat(1) = m_sGMN1Cod
            arrMat(2) = m_sGMN2Cod
            arrMat(3) = m_sGMN3Cod
            arrMat(4) = m_sGMN4Cod
        End If
        
        
        Set oGMN4 = oFSGSRaiz.Generar_CGrupoMatNivel4
        oGMN4.GMN1Cod = IIf(NoHayParametro(arrMat(1)), "", arrMat(1))
        oGMN4.GMN2Cod = IIf(NoHayParametro(arrMat(2)), "", arrMat(2))
        oGMN4.GMN3Cod = IIf(NoHayParametro(arrMat(3)), "", arrMat(3))
        oGMN4.GMN4Cod = IIf(NoHayParametro(arrMat(4)), "", arrMat(4))
        
        If Not IsEmpty(arrMat(4)) Then
            If bUsarOrgCompras Then
                oGMN4.CargarTodosLosArticulos , , , , , False, , , , , , , , , sCodOrgCompras, sCodCentro
            Else
                oGMN4.CargarTodosLosArticulos , , , , , , False
            End If
        Else
            oGMN4.CargarTodosLosArticulos
        End If
        Set oArticulos = oGMN4.ARTICULOS
        Set oGMN4 = Nothing
        
         If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.CodArticulo Or _
    g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
  
            For Each oArt In oArticulos
                ssLineas.ValueLists.Item("ListaArt").ValueListItems.Add oArt.Cod, oArt.Cod & " - " & oArt.Den
            Next
            Set Cell.Column.ValueList = ssLineas.ValueLists("ListaArt")
        Else
            For Each oArt In oArticulos
                ssLineas.ValueLists.Item("ListaDen").ValueListItems.Add oArt.Den, oArt.Cod & " - " & oArt.Den
            Next
            Set Cell.Column.ValueList = ssLineas.ValueLists("ListaDen")
        End If

        Set oArticulos = Nothing
        
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.provincia Then
        ssLineas.ValueLists.Item("ListaProvi").ValueListItems.clear
    
        Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
          
        If oCellAnt.Value <> "" And Not IsNull(oCellAnt.Value) Then
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.Pais Then
                Set oPais = oFSGSRaiz.generar_CPais
                oPais.Cod = oCellAnt.Value
                oPais.CargarTodasLasProvincias
                    
                For Each oProvi In oPais.Provincias
                    ssLineas.ValueLists.Item("ListaProvi").ValueListItems.Add oProvi.Cod, oProvi.Cod & " - " & oProvi.Den
                Next
                Set oPais = Nothing
            End If
        End If
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaProvi")
        
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Departamento Then
        ssLineas.ValueLists.Item("ListaDepartamentos").ValueListItems.clear
    
        Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
          
        If oCellAnt.Value <> "" And Not IsNull(oCellAnt.Value) Then
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.UnidadOrganizativa Then
                Dim vUnidadOrganizativa As Variant
                vUnidadOrganizativa = Split(oCellAnt.Value, " - ")
                ReDim Preserve vUnidadOrganizativa(UBound(vUnidadOrganizativa) - 1)
                ReDim Preserve vUnidadOrganizativa(3)
                Set oUON = createUon(vUnidadOrganizativa(0), vUnidadOrganizativa(1), vUnidadOrganizativa(2))
                oUON.CargarTodosLosDepartamentos
                For Each oDepartamento In oUON.Departamentos
                    ssLineas.ValueLists.Item("ListaDepartamentos").ValueListItems.Add oDepartamento.Cod, oDepartamento.Cod & " - " & oDepartamento.Den
                Next
                Set oUON = Nothing
            End If
        End If
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaDepartamentos")
    
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.ProveedorERP Then
        Dim sCodProveedor As String
        Dim sOrgCompras As String
        sCodProveedor = DevuelveValueCampoGS(TipoCampoGS.Proveedor)
        sOrgCompras = DevuelveValueCampoGS(TipoCampoGS.OrganizacionCompras)
        ssLineas.ValueLists.Item("ListaProveedoresERP").ValueListItems.clear
        Dim oProvesERP As CProveERPs
        Dim oProveERP As CProveERP
        Set oProvesERP = oFSGSRaiz.Generar_CProveERPs
        If sCodProveedor <> "" And sOrgCompras <> "" Then
            oProvesERP.CargarProveedoresERP , sCodProveedor, , , sOrgCompras, True
        End If
        For Each oProveERP In oProvesERP
            ssLineas.ValueLists.Item("ListaProveedoresERP").ValueListItems.Add oProveERP.Cod, oProveERP.Cod & " - " & oProveERP.Den
        Next
        Set oProvesERP = Nothing
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaProveedoresERP")
    
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
        'Carga el Combo con los CENTROS
        '------------------------------
        ssLineas.ValueLists.Item("ListaCentros").ValueListItems.clear
        
        Dim oCentros As CCentros
        Set oCentros = oFSGSRaiz.Generar_CCentros
        
        
        If Cell.Column.Index > 1 Then
            Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
            
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.OrganizacionCompras Then
                If Not IsNull(oCellAnt.Value) Then
                    vAux = Split(oCellAnt.Value, " - ")
                    If UBound(vAux) > 0 Then
                        oCentros.CargarTodosLosCentros , vAux(0)
                    Else
                        oCentros.CargarTodosLosCentros , oCellAnt.Value
                    End If
                End If
                
            Else
                'El campo anterior al desglose es de tipo ORGANIZACION DE COMPRAS
                If g_sCodOrganizacionCompras <> "-1" Then
                    oCentros.CargarTodosLosCentros , g_sCodOrganizacionCompras
                Else
                    oCentros.CargarTodosLosCentros
                End If
            End If
            
        Else
            'El campo anterior al desglose es de tipo ORGANIZACION DE COMPRAS
            If g_sCodOrganizacionCompras <> "-1" Then
                oCentros.CargarTodosLosCentros , g_sCodOrganizacionCompras
            Else
                oCentros.CargarTodosLosCentros
            End If
        End If
               
        For Each oCentro In oCentros.Centros
            ssLineas.ValueLists.Item("ListaCentros").ValueListItems.Add oCentro.Cod, oCentro.Cod & " - " & oCentro.Den
        Next
        Set oCentros = Nothing
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaCentros")
        
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))).CampoGS = TipoCampoGS.Almacen Then
        'Carga el Combo con los ALMACENES
        '------------------------------
        Dim oAlmacenes As CAlmacenes
        Set oAlmacenes = oFSGSRaiz.Generar_CAlmacenes
        
        ssLineas.ValueLists.Item("ListaAlmacenes").ValueListItems.clear
        
        If Cell.Column.Index > 1 Then
            Set oCellAnt = ssLineas.ActiveRow.Cells(Cell.Column.Index - 1)
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellAnt.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
                If Not IsNull(oCellAnt.Value) Then
                    vAux = Split(oCellAnt.Value, " - ")
                    If UBound(vAux) > 0 Then
                        oAlmacenes.CargarTodosLosAlmacenes , vAux(0)
                    Else
                        oAlmacenes.CargarTodosLosAlmacenes , oCellAnt.Value
                    End If
                End If
            
            ElseIf g_sCodCentro <> "-1" Then
                oAlmacenes.CargarTodosLosAlmacenes , g_sCodCentro
            Else
                oAlmacenes.CargarTodosLosAlmacenes
            End If
            
        ElseIf g_sCodCentro <> "-1" Then
            oAlmacenes.CargarTodosLosAlmacenes , g_sCodCentro
        Else
             oAlmacenes.CargarTodosLosAlmacenes
        End If
        
               
        For Each oAlmacen In oAlmacenes
            ssLineas.ValueLists.Item("ListaAlmacenes").ValueListItems.Add oAlmacen.Id, CStr(oAlmacen.Cod) & " - " & oAlmacen.Den
        Next
        Set oAlmacenes = Nothing
        
        Set Cell.Column.ValueList = ssLineas.ValueLists("ListaAlmacenes")
        
    End If
    
    numRegistros = NumRegistrosMultifila
    
    If numRegistros > 1 Then

    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub
    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3)))
    
    If Not g_sOrigen = "frmVisorSolicitudes" Then
        If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                    'Si son campos de GS:
                    Select Case oCampo.CampoGS
                        Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4, TipoCampoGS.Proveedor, TipoCampoGS.Almacen, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                            MostrarBotonMultifila
                    End Select
        ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Atributo Then
            If Not IsNull(oCampo.idAtrib) Then
                MostrarBotonMultifila
            End If
        End If
    End If
    End If

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_BeforeCellListDropDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_BeforeCellUpdate(ByVal Cell As UltraGrid.SSCell, NewValue As Variant, ByVal Cancel As UltraGrid.SSReturnBoolean)
Dim oCampo As CFormItem
    
    '******** REALIZA LAS COMPROBACIONES CORRESPONDIENTES ANTES DE GUARDAR **************
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
   If bNoValidarCelda Then Exit Sub
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3)))
    If oCampo Is Nothing Then Exit Sub
    If m_bCalculando = True Then Exit Sub
    
    If Not (NewValue = "" Or IsNull(NewValue)) Then
        'Si es una lista comprueba que el valor introducido sea correcto:
        If Cell.Column.Style = ssStyleDropDown And Not (oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Or oCampo.CampoGS = TipoCampoGS.DenArticulo Or oCampo.CampoGS = TipoCampoGS.Unidad) Then
            If Not (ssLineas.ActiveCell Is Nothing) Then
                If ssLineas.ActiveCell.Column.ValueList.Find(NewValue, ssValueListFindDataValue) Is Nothing Then
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If

        If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
            'todo ok
            Exit Sub
            
        ElseIf oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            'Si son los campos de GS importe o cantidad:
            If oCampo.CampoGS = TipoCampoSC.importe Or oCampo.CampoGS = TipoCampoSC.Cantidad Or oCampo.CampoGS = TipoCampoSC.PrecioUnitario Then
                If Not IsNumeric(NewValue) Then
                    Screen.MousePointer = vbNormal
                    oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(1)
                    Cancel = True
                    Exit Sub
                End If
            End If
        ElseIf oCampo.TipoIntroduccion <> TAtributoIntroduccion.Introselec Then   'Introducci�n libre
            Select Case oCampo.Tipo
                Case TiposDeAtributos.TipoFecha
                    If Not IsDate(NewValue) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(2)
                        Cancel = True
                        Exit Sub
                    End If
                    'comprueba que el valor introducido est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(oCampo.Maximo) Then
                        If CDate(NewValue) > CDate(oCampo.Maximo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    If Not IsNull(oCampo.Minimo) Then
                        If CDate(NewValue) < CDate(oCampo.Minimo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    
                Case TiposDeAtributos.TipoNumerico
                    If Not IsNumeric(NewValue) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(1)
                        Cancel = True
                        Exit Sub
                    End If
                    'comprueba que el valor introducido est� dentro de los valores de m�ximo y m�nimo
                    If Not IsNull(oCampo.Maximo) Then
                        If CDbl(NewValue) > CDbl(oCampo.Maximo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    If Not IsNull(oCampo.Minimo) Then
                        If CDbl(NewValue) < CDbl(oCampo.Minimo) Then
                            Screen.MousePointer = vbNormal
                            oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(7)
                            Cancel = True
                            Exit Sub
                        End If
                    End If
                    
                Case TiposDeAtributos.TipoBoolean
                    If UCase(NewValue) <> UCase(m_sIdiTrue) And UCase(NewValue) <> UCase(m_sIdiFalse) Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(3)
                        Cancel = True
                        Exit Sub
                    End If
                Case TiposDeAtributos.TipoTextoCorto
                    If Len(NewValue) > 100 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(4)
                        Cancel = True
                        Exit Sub
                    End If
                    
                Case TiposDeAtributos.TipoTextoMedio
                    If Len(NewValue) > 800 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(5)
                        Cancel = True
                        Exit Sub
                    End If
                Case TiposDeAtributos.TipoTextoLargo
                    If Len(NewValue) > 4000 Then
                        Screen.MousePointer = vbNormal
                        oMensajes.NoValido Cell.Column.Header.caption & " " & m_sMensaje(6)
                        Cancel = True
                        Exit Sub
                    End If
            End Select
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_BeforeCellUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cancela el delete de las filas del grid si estan vinculadas
''' </summary>
''' <param name="Rows">Rows seleccionadas</param>
''' <param name="DisplayPromptMsg">si saca mensaje antes de borrar</param>
''' <param name="Cancel">Cancelar el delete</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub ssLineas_BeforeRowsDeleted(ByVal Rows As UltraGrid.SSSelectedRows, ByVal DisplayPromptMsg As UltraGrid.SSReturnBoolean, ByVal Cancel As UltraGrid.SSReturnBoolean)
 Dim FilaSiguiente As SSRow
    Dim scod As String
    Dim oLinea As CLineaDesglose
    
    'para que no salte el mensaje de confirmaci�n de borrado de las filas
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    DisplayPromptMsg = False

    'Elimina la l�nea de desglose de base de datos:
    If ssLineas.ActiveRow Is Nothing Then Exit Sub
    
    If g_oCampoDesglose.NoBorrablePorVinculada(ssLineas.ActiveRow.Cells("LINEA").Value) Then
        oMensajes.MensajeOKOnly 1391, Exclamation
        
        Cancel = True
        Exit Sub
    End If
     
    'Marca las l�neas de desglose como eliminadas en la colecci�n:
    For Each oLinea In g_oCampoDesglose.LineasDesglose
        If oLinea.Linea = ssLineas.ActiveRow.Cells("LINEA").Value Then
            scod = oLinea.Linea & "$" & g_oCampoDesglose.Id & "$" & oLinea.CampoHijo.Id
            g_oCampoDesglose.LineasDesglose.Item(scod).eliminado = True
        End If
    Next
        
    'elimina una fila de la grid
    If ssLineas.ActiveRow.HasNextSibling = True Then
        'se situa en la siguiente del mismo nivel
        Set FilaSiguiente = ssLineas.ActiveRow.GetSibling(ssSiblingRowNext)
    ElseIf ssLineas.ActiveRow.HasPrevSibling = True Then
        Set FilaSiguiente = ssLineas.ActiveRow.GetSibling(ssSiblingRowPrevious)
    End If
   
    If Not FilaSiguiente Is Nothing Then
        ssLineas.ActiveRow = FilaSiguiente
    End If
        
    If Me.Visible Then ssLineas.SetFocus
    
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_BeforeRowsDeleted", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_CellChange(ByVal Cell As UltraGrid.SSCell)
    Dim vMaxLength As Variant
    Dim bEdit As Boolean
        
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
   
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If
     
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub
    
    'Controlar los tipos TipoTextoCorto, TipoTextoMedio, TipoTextoLargo para que no superen el maximo permitido cuando se hace copy/paste
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
        If IsNull(vMaxLength) Or vMaxLength = "" Then
            vMaxLength = basParametros.gLongitudesDeCodigos.giLongCodDENART
        End If
        
        If (Len(ssLineas.ActiveCell.GetText) > CInt(vMaxLength)) Then
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If

            ssLineas.ActiveCell.Value = Left(ssLineas.ActiveCell.GetText, CInt(vMaxLength))

            If bEdit = True Then
                 ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        End If
    Else
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoMedio Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoLargo Then
            vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
            If IsNull(vMaxLength) Or vMaxLength = "" Then
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Then
                    vMaxLength = 100
                ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo Then
                    vMaxLength = 800
                Else
                    vMaxLength = -1
                End If
            End If
            
            If CInt(vMaxLength) > 0 And (Len(ssLineas.ActiveCell.GetText) > CInt(vMaxLength)) Then
                bEdit = ssLineas.IsInEditMode
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionExitEditMode
                End If
    
                ssLineas.ActiveCell.Value = Left(ssLineas.ActiveCell.GetText, CInt(vMaxLength))
    
                If bEdit = True Then
                     ssLineas.PerformAction ssKeyActionEnterEditMode
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_CellChange", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Evento que responde a cuando se selecciona un valor de un combo de la grid
''' </summary>
Private Sub ssLineas_CellListSelect(ByVal Cell As UltraGrid.SSCell)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_bValorListaGridSeleccionado = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_CellListSelect", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_Click()
    Dim oCampo As CFormItem
    Dim numRegistros As Long
        
    cmdMultiFila.Visible = False
    
    numRegistros = NumRegistrosMultifila
    
        On Error GoTo ERROR_Frm
        
        If numRegistros > 1 Then
        
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub
        Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3)))
        
        If Not g_sOrigen = "frmVisorSolicitudes" Then
            If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                        'Si son campos de GS:
                        Select Case oCampo.CampoGS
                            Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4, TipoCampoGS.Proveedor, TipoCampoGS.Almacen, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                                MostrarBotonMultifila
                            Case Else
                                OcultarBotonMultifila
                        End Select
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Atributo Then
                If Not IsNull(oCampo.idAtrib) Then
                    MostrarBotonMultifila
                Else
                    OcultarBotonMultifila
                End If
            End If
        End If
    
    End If
    
ERROR_Frm:
    
    Exit Sub

End Sub

''' <summary>
''' Evento de grid cuando se pulsa algun boton en una celda del grid
''' </summary>
''' <param name="Cell">celda del grid</param>
''' <remarks>Llamada desde: Evento de grid; Tiempo m�ximo: instantes</remarks>
Private Sub ssLineas_ClickCellButton(ByVal Cell As UltraGrid.SSCell)
    Dim blnEnse�arBoton As Boolean
    Dim oCampo As CFormItem
    Dim scod As String
    Dim oLinea As CLineaDesglose
    Dim teserror As TipoErrorSummit
    Dim oIBaseDatos As IBaseDatos
    Dim valor As Variant
    Dim oArticulo As CArticulo
    Dim iLinea As Integer
    Dim arrMat As Variant
    Dim sObs As String
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
       
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3))) Is Nothing Then Exit Sub
    Set oCampo = g_oCampoDesglose.Desglose.Item(CStr(Mid$(Cell.Column.key, 3)))
    
    If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
        If g_bModif = True And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef) Then
            Dim intTabla As Integer
            Dim strNombreTabla As String, strPrimerCampoNoPK As String, strPK As String
            Dim TipoDeDatoPK As TiposDeAtributos
            intTabla = oCampo.TablaExterna
            strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
            strPrimerCampoNoPK = m_oTablaExterna.SacarPrimerCampoDistinto(intTabla)
            strPK = m_oTablaExterna.SacarPK(intTabla)
            TipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
            '_______________________________________________________________________
            If ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value <> "" Then
                valor = ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant
                Select Case TipoDeDatoPK
                    Case TipoFecha
                        valor = CDate(valor)
                    Case TipoNumerico
                        valor = CLng(valor)
                End Select
            End If
    
    
            Dim intLinea As Integer
            intLinea = ssLineas.ActiveRow.Cells("LINEA").Value
            Dim sIdART As String
            If ExisteAtributoArticulo(intLinea) Then
                sIdART = DevuelveIdArticulo(intLinea)
            Else
                sIdART = ""
            End If

            m_strPK_old = frmSolicitudDesglose.g_strPK
            
            FSGSForm.MostrarFormTablaExterna oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, oFSGSRaiz, oMensajes, g_strPK, valor, _
                                             m_oTablaExterna.SacarDenominacionTabla(intTabla), strNombreTabla, strPK, sIdART, intTabla, "frmSolicitudDesglose"
            
            'aqui devuelve la primary key del registro en g_strPK
            'hay que sacar la primary key + el primer campo no primary key .lo buscamos:
            'colocamos en la columna la clave y el primer valor que no sea clave
            blnEnse�arBoton = True
            '_______________________________________________________________________
            If frmSolicitudDesglose.g_strPK = "vbFormControlMenu" Then
                'lo dejo como esta
                frmSolicitudDesglose.g_strPK = m_strPK_old
            ElseIf frmSolicitudDesglose.g_strPK <> "" Then
                Select Case TipoDeDatoPK
                    Case TipoTextoMedio
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = frmSolicitudDesglose.g_strPK
                    Case TipoFecha
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CDate(frmSolicitudDesglose.g_strPK)
                    Case TipoNumerico
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CLng(frmSolicitudDesglose.g_strPK)
                End Select
                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = frmSolicitudDesglose.g_strPK & " " & m_oTablaExterna.ValorCampo(frmSolicitudDesglose.g_strPK, strPK, strPrimerCampoNoPK, strNombreTabla, TipoDeDatoPK)
            Else    '?
                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = ""
            End If
            '�����������������������������������������������������������������������
        End If
    Else
        If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            'Si son campos de GS:
            Select Case oCampo.CampoGS
                Case TipoCampoSC.DescrDetallada
                    scod = ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                    If g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                        sObs = ""
                    Else
                        sObs = NullToStr(g_oCampoDesglose.LineasDesglose.Item(scod).valorText)
                    End If
                    If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den), _
                    g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef), -1, sObs) Then
                        If (Me.g_sOrigen = "frmPedidos") Then
                            frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = sObs
                            frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
                        Else
                            If NullToStr(frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value) <> sObs Then
                                frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
                                frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
                            End If
                            frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = sObs
                            frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
                        End If
                        
                        If g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef) Then blnEnse�arBoton = True
                    End If
                    
                Case TipoCampoGS.Proveedor
                    frmPROVEBuscar.sOrigen = "frmSolicitudDesglose"
                    If (Me.g_sOrigen = "frmPedidos") Then frmPROVEBuscar.sOrigen = frmPROVEBuscar.sOrigen & "P"
                    If m_bRestrProve = True Then
                        frmPROVEBuscar.codEqp = basOptimizacion.gCodEqpUsuario
                    End If
                    frmPROVEBuscar.Show vbModal
                    
                Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                    Dim oCell As UltraGrid.SSCell
                    Dim oCampoAux As CFormItem
                    Dim dCantidad As Double
                    dCantidad = 0
                    For Each oCell In Cell.Row.Cells
                        If Not g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCell.Column.key, 3))) Is Nothing Then
                        
                        Set oCampoAux = g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCell.Column.key, 3)))
                        If oCampoAux.CampoGS = TipoCampoSC.Cantidad Then
                            If Not IsNull(oCell.Value) Then
                                dCantidad = oCell.Value
                            End If
                            Exit For
                        End If
                        End If
                    Next
                    
                    scod = CStr(Cell.Row.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & CStr(oCampo.Id)
                    m_sValorPres = ""
                    
                    If Not g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                        If Not IsNull(g_oCampoDesglose.LineasDesglose.Item(scod).valorText) Then
                            m_sValorPres = g_oCampoDesglose.LineasDesglose.Item(scod).valorText
                        End If
                    End If
                    If dCantidad > 0 Then
                        frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(m_sValorPres) * dCantidad / 100
                    Else
                        frmPRESAsig.g_dblAsignado = ObtenerPresupAsignado(m_sValorPres)
                    End If
                    frmPRESAsig.g_bHayPres = (frmPRESAsig.g_dblAsignado > 0)
                    frmPRESAsig.g_bHayPresBajaLog = False
                    frmPRESAsig.g_sOrigen = "frmSolicitudDesglose"
                    If (Me.g_sOrigen = "frmPedidos") Then frmPRESAsig.g_sOrigen = frmPRESAsig.g_sOrigen & "P"
                    frmPRESAsig.g_dblAbierto = dCantidad
                    Select Case oCampo.CampoGS
                        Case TipoCampoGS.Pres1
                            frmPRESAsig.g_iTipoPres = 1
                        Case TipoCampoGS.Pres2
                            frmPRESAsig.g_iTipoPres = 2
                        Case TipoCampoGS.Pres3
                            frmPRESAsig.g_iTipoPres = 3
                        Case TipoCampoGS.Pres4
                            frmPRESAsig.g_iTipoPres = 4
                    End Select
    
                    frmPRESAsig.g_bModif = g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef)
                    frmPRESAsig.g_bSinPermisos = False
                    frmPRESAsig.g_bRUO = m_bRestrPresUO
                    frmPRESAsig.g_sValorPresFormulario = m_sValorPres
                    frmPRESAsig.Show vbModal
                    
                        
                Case TipoCampoGS.material
                    frmSELMAT.sOrigen = "frmSolicitudDesglose"
                    If (Me.g_sOrigen = "frmPedidos") Then frmSELMAT.sOrigen = frmSELMAT.sOrigen & "P"
                    
                    frmSELMAT.bRComprador = m_bRestrMat
                    
                    If g_oInstanciaSeleccionada.ExisteNivelSeleccionMatObligatorio() Then
                        Dim oCampoMaterial As CFormItem
                        
                        Set oCampoMaterial = g_oInstanciaSeleccionada.getCampoTipoNivelSeleccionObligatoria()
                                                
                        If NoHayParametro(oCampoMaterial.valorText) Then
                            oMensajes.mensajeGenericoOkOnly m_sSeleccionarMatFormulario
                            Exit Sub
                        Else
                            arrMat = DevolverMaterial(oCampoMaterial.valorText)
                            frmSELMAT.nivelSeleccion = oCampoMaterial.nivelSeleccion
                            frmSELMAT.sGMN1 = arrMat(1)
                            frmSELMAT.sGmn2 = arrMat(2)
                            frmSELMAT.sGmn3 = arrMat(3)
                            frmSELMAT.sGmn4 = arrMat(4)
                        End If

                        Set oCampoMaterial = Nothing
                    End If
                    
                    
                    frmSELMAT.Show vbModal
                    
                Case TipoCampoSC.ArchivoEspecific
                    frmFormAdjuntos.g_sOrigen = "frmSolicitudDesglose"
                    If (Me.g_sOrigen = "frmPedidos") Then frmFormAdjuntos.g_sOrigen = frmFormAdjuntos.g_sOrigen & "P"
                    scod = CStr(Cell.Row.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & CStr(oCampo.Id)
                    If g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                        Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
                        oLinea.Linea = Cell.Row.Cells("LINEA").Value
                        oLinea.valorBool = Null
                        oLinea.valorFec = Null
                        oLinea.valorNum = Null
                        oLinea.valorText = Null
                        Set oLinea.CampoHijo = oCampo
                        Set oLinea.Instancia = g_oInstanciaSeleccionada
                        Set oIBaseDatos = oLinea
                        teserror = oIBaseDatos.AnyadirABaseDatos
                        If teserror.NumError <> TESnoerror Then
                            basErrores.TratarError teserror
                            Exit Sub
                        End If
                        g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oCampo, Null, Null, Null, Null, oLinea.FECACT, , oLinea.Instancia
                        Set oLinea = Nothing
                        Set oIBaseDatos = Nothing
                    End If
                    Set frmFormAdjuntos.g_oLineaSeleccionada = g_oCampoDesglose.LineasDesglose.Item(scod)
                    Set frmFormAdjuntos.g_oInstanciaSeleccionada = g_oInstanciaSeleccionada
                    frmFormAdjuntos.g_bModif = g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef)
                    frmFormAdjuntos.Show vbModal
                    
                Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                    Set frmCalendar.frmDestination = Me
                    Set frmCalendar.ctrDestination = Nothing
                    frmCalendar.addtotop = 900 + 360
                    frmCalendar.addtoleft = 180
                    If ssLineas.ActiveCell.Value <> "" Then
                        frmCalendar.Calendar.Value = ssLineas.ActiveCell.Value
                    Else
                        frmCalendar.Calendar.Value = Date
                    End If
                    frmCalendar.Show vbModal
                    blnEnse�arBoton = True
                    
                Case TipoCampoGS.CampoPersona
                    frmSOLSelPersona.g_sOrigen = "frmSolicitudDesglose"
                    If (Me.g_sOrigen = "frmPedidos") Then frmSOLSelPersona.g_sOrigen = frmSOLSelPersona.g_sOrigen & "P"
                    frmSOLSelPersona.bAllowSelUON = False
                    frmSOLSelPersona.bRDep = False 'No vamos a poner restricciones.
                    frmSOLSelPersona.bRuo = False 'No tiene sentido restringir al UO o Dep del que configura el Formulario.
                    frmSOLSelPersona.Show vbModal
                    
                Case TipoCampoGS.UnidadOrganizativa
                    frmSELUO.sOrigen = "frmSolicitudDesglose"
                    frmSELUO.Show vbModal
                    MostrarUOSeleccionada
                Case TipoCampoSC.TotalLineaAdj
                    
                    Set oArticulo = oFSGSRaiz.Generar_CArticulo
                    
                    iLinea = ssLineas.ActiveRow.Cells("LINEA").Value
                    oArticulo.Cod = DevuelveIdArticulo(iLinea)
                    
                    Set frmESTRMATProve.oArticulo = oArticulo
                    Set frmESTRMATProve.oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt
                    frmESTRMATProve.mostrarAdjudicacionesLineaSolicitud = True
                    frmESTRMATProve.mostrarMonPrecio = False
                    frmESTRMATProve.oAdjudicaciones.DevolverAdjudicacionesLineaInstancia oArticulo, g_oInstanciaSeleccionada.Id, g_oCampoDesglose.IdCampoDef, iLinea, IIf(oCampo.CampoGS = TipoCampoSC.TotalLineaPreadj, 1, 0)
                    frmESTRMATProve.g_sOrigen = Me.Name
                    frmESTRMATProve.g_bPreAdj = IIf(oCampo.CampoGS = TipoCampoSC.TotalLineaPreadj, True, False)
                    frmESTRMATProve.caption = m_sTotalAdjudicado
                    frmESTRMATProve.Show vbModal
                Case TipoCampoSC.TotalLineaPreadj
                    
                    Set oArticulo = oFSGSRaiz.Generar_CArticulo
                    
                    iLinea = ssLineas.ActiveRow.Cells("LINEA").Value
                    oArticulo.Cod = DevuelveIdArticulo(iLinea)
                    Set frmESTRMATProve.oArticulo = oArticulo
                    Set frmESTRMATProve.oAdjudicaciones = oFSGSRaiz.Generar_CAdjsDeArt
                    frmESTRMATProve.mostrarAdjudicacionesLineaSolicitud = True
                    frmESTRMATProve.mostrarMonPrecio = False
                    frmESTRMATProve.g_sOrigen = Me.Name
                    frmESTRMATProve.oAdjudicaciones.DevolverAdjudicacionesLineaInstancia oArticulo, g_oInstanciaSeleccionada.Id, g_oCampoDesglose.IdCampoDef, iLinea, True
                    frmESTRMATProve.g_bPreAdj = True
                    frmESTRMATProve.caption = m_sTotalPreadjudicado
                    frmESTRMATProve.Show vbModal
            End Select
                    
        Else
            'Si es un campo de tipo archivo
            If oCampo.Tipo = TiposDeAtributos.TipoArchivo Then
                frmFormAdjuntos.g_sOrigen = "frmSolicitudDesglose"
                If (Me.g_sOrigen = "frmPedidos") Then frmFormAdjuntos.g_sOrigen = frmFormAdjuntos.g_sOrigen & "P"
                scod = CStr(Cell.Row.Cells("LINEA").Value) & "$" & g_oCampoDesglose.Id & "$" & CStr(oCampo.Id)
                If g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                    Set oLinea = oFSGSRaiz.Generar_CLineaDesglose
                    oLinea.Linea = Cell.Row.Cells("LINEA").Value
                    oLinea.valorBool = Null
                    oLinea.valorFec = Null
                    oLinea.valorNum = Null
                    oLinea.valorText = Null
                    Set oLinea.CampoHijo = oCampo
                    Set oLinea.Instancia = g_oInstanciaSeleccionada
                    Set oIBaseDatos = oLinea
                    teserror = oIBaseDatos.AnyadirABaseDatos
                    If teserror.NumError <> TESnoerror Then
                        basErrores.TratarError teserror
                        Exit Sub
                    End If
                    g_oCampoDesglose.LineasDesglose.Add oLinea.Linea, oCampo, Null, Null, Null, Null, oLinea.FECACT, , oLinea.Instancia
                    Set oLinea = Nothing
                    Set oIBaseDatos = Nothing
                End If
                Set frmFormAdjuntos.g_oLineaSeleccionada = g_oCampoDesglose.LineasDesglose.Item(scod)
                Set frmFormAdjuntos.g_oInstanciaSeleccionada = g_oInstanciaSeleccionada
                frmFormAdjuntos.g_bModif = g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef)
                frmFormAdjuntos.Show vbModal
                
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoFecha Then
                Set frmCalendar.frmDestination = Me
                Set frmCalendar.ctrDestination = Nothing
                frmCalendar.addtotop = 900 + 360
                frmCalendar.addtoleft = 180
                If ssLineas.ActiveCell.Value <> "" Then
                    frmCalendar.Calendar.Value = ssLineas.ActiveCell.Value
                Else
                    frmCalendar.Calendar.Value = Date
                End If
                frmCalendar.Show vbModal
                'Muestra el bot�n de deshacer
                blnEnse�arBoton = True
                
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoTextoMedio Then
                scod = ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                If g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                    sObs = ""
                Else
                    sObs = NullToStr(g_oCampoDesglose.LineasDesglose.Item(scod).valorText)
                End If
                If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den), _
                                             (g_bModif And Not oCampo.TipoIntroduccion = IntroListaExterna And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef)), IIf(IsNull(oCampo.MaxLength), 800, oCampo.MaxLength), sObs) Then
                    If (Me.g_sOrigen = "frmPedidos") Then
                        frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = sObs
                        frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
                    Else
                        If NullToStr(frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value) <> sObs Then
                            frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
                            frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
                        End If
                        frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = sObs
                        frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
                    End If
                    
                    If g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef) Then blnEnse�arBoton = True
                End If
                
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoTextoLargo Then
                scod = ssLineas.ActiveRow.Cells("LINEA").Value & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCampo.Id)
                If g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                    sObs = ""
                Else
                    sObs = NullToStr(g_oCampoDesglose.LineasDesglose.Item(scod).valorText)
                End If
                If MostrarFormFormCampoDescr(oGestorIdiomas, basPublic.gParametrosInstalacion.gIdioma, NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den), _
                                             g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef), IIf(IsNull(oCampo.MaxLength), -1, oCampo.MaxLength), sObs) Then
                    If (Me.g_sOrigen = "frmPedidos") Then
                        frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = sObs
                        frmPEDIDOS.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
                    Else
                        If NullToStr(frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value) <> sObs Then
                            frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
                            frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
                        End If
                        frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.ActiveCell.Value = sObs
                        frmSolicitudes.g_ofrmDetalleSolic.g_ofrmDesgloseValores.ssLineas.Update
                    End If
                    
                    If g_bModif And EscrituraParaDetalleSolicGS(oCampo.IdCampoDef) Then blnEnse�arBoton = True
                End If
                                
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoEditor Then
                Dim params As String
                Dim strSessionId As String
                Dim bReadOnly As Boolean
                            
                strSessionId = DevolverSessionIdEstablecido(oUsuarioSummit, gParametrosGenerales.gsURLSessionService)
                params = "?desdeGS=1&sessionId=" & strSessionId & "&campo_hijo=" & CStr(oCampo.Id) & "&linea=" & ssLineas.ActiveRow.Cells("LINEA").Value
                            
                If g_bModif = False Or Not EscrituraParaDetalleSolicGS(oCampo.IdCampoDef) Then
                    params = params & "&readOnly=1"
                    bReadOnly = True
                Else
                    params = params & "&readOnly=0"
                    bReadOnly = False
                End If
                                                        
                With frmEditor
                    .caption = NullToStr(oCampo.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den)
                    .g_sOrigen = "frmSolicitudDesglose"
                    .g_sRuta = gParametrosGenerales.gsURLCampoEditor & params
                    .g_bReadOnly = bReadOnly
                    .Show vbModal
                End With
            End If
        End If
    End If
    
    Set oCampo = Nothing
    '_______________________________________________________________________
    If blnEnse�arBoton Then
        MostrarBotonesEdicion True, True
        
        If (Me.g_sOrigen = "frmPedidos") Then
            frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
            frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
        ElseIf (Me.g_sOrigen = "frmPROCE") Then
        Else
            frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
            frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
        End If
    End If
    '�����������������������������������������������������������������������

    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_ClickCellButton", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

Private Sub ssLineas_Error(ByVal ErrorInfo As UltraGrid.SSError)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
     ErrorInfo.DisplayErrorDialog = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_Error", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Configura el aspecto del grid
''' </summary>
''' <param name="Context">Contexto del Layout</param>
''' <param name="Layout">Layout</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub ssLineas_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    Dim oCampo As CFormItem
    Dim bEscritura As Boolean

    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ssLineas.Override.DefaultRowHeight = 239
    
     'Carga las cabeceras y los tipos de las columnas
    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.CampoGS = idsficticios.numLinea Then
            ssLineas.Bands(0).Columns("LINEA").Header.caption = m_sNumLinea
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Hidden = True
            bEscritura = True 'Como antes 13462
        Else
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Header.caption = NullToStr(oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
            
            bEscritura = EscrituraParaDetalleSolicGS(oCampo.IdCampoDef)
        End If
        
        If g_bModif = True And bEscritura Then
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationAllowEdit
        
            If oCampo.TipoPredef = TipoCampoPredefinido.externo Then
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                Select Case oCampo.CampoGS
                    Case TipoCampoSC.ArchivoEspecific, TipoCampoSC.DescrDetallada, TipoCampoGS.Proveedor, TipoCampoGS.CampoPersona
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                        
                    Case TipoCampoSC.DescrBreve
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        
                    Case TipoCampoSC.Cantidad, TipoCampoSC.importe, TipoCampoSC.PrecioUnitario
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        
                    Case TipoCampoGS.Dest, TipoCampoGS.FormaPago, TipoCampoGS.Unidad, TipoCampoGS.CodArticulo, TipoCampoGS.Pais, TipoCampoGS.provincia, TipoCampoGS.Moneda, TipoCampoGS.ProveedorERP
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        
                    Case TipoCampoSC.FecNecesidad, TipoCampoSC.IniSuministro, TipoCampoSC.FinSuministro
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        
                    Case TipoCampoGS.Pres1, TipoCampoGS.Pres2, TipoCampoGS.Pres3, TipoCampoGS.Pres4
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                        
                    Case TipoCampoGS.material
                        If IsNull(oCampo.GMN1Cod) And IsNull(oCampo.GMN2Cod) And IsNull(oCampo.GMN3Cod) And IsNull(oCampo.GMN4Cod) Then
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        Else
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        End If
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    
                    Case TipoCampoGS.NuevoCodArticulo
                        oCampo.CargarCopiaAnyadirArt
                        If (oCampo.AnyadirArt) Then
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown  'deja modificar sin seleccionar del combo para introducir art�culos no codificados
                        Else
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDownList
                        End If
                    
                    Case TipoCampoGS.DenArticulo
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        
                    Case TipoCampoGS.UnidadOrganizativa
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        
                    Case TipoCampoGS.Departamento
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        
                    Case TipoCampoGS.OrganizacionCompras, TipoCampoGS.Centro, TipoCampoGS.Almacen
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                    
                    Case TipoCampoSC.PrecioUnitarioAdj
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    Case TipoCampoSC.CantidadAdj
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    Case TipoCampoSC.ProveedorAdj, TipoCampoGS.AnyoImputacion
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                    Case idsficticios.numLinea
                        ssLineas.Bands(0).Columns("LINEA").Style = UltraGrid.ssStyleEdit
                        ssLineas.Bands(0).Columns("LINEA").Activation = UltraGrid.ssActivationActivateOnly
                    Case TipoCampoSC.TotalLineaAdj, TipoCampoSC.TotalLineaPreadj
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                End Select
            
            ElseIf oCampo.TipoPredef = TipoCampoPredefinido.Calculado Then
                'Si es un campo calculado no se podr� modificar:
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                
            Else  'es un campo normal o de selecci�n de lista de valores
                If oCampo.TipoIntroduccion = TAtributoIntroduccion.IntroLibre Then
                    Select Case oCampo.Tipo
                        Case TiposDeAtributos.TipoFecha
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Short Date"
                        Case TiposDeAtributos.TipoBoolean
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                        Case TiposDeAtributos.TipoArchivo, TiposDeAtributos.TipoEditor
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                        Case TiposDeAtributos.TipoNumerico
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                        Case TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                        Case Else
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                    End Select
                ElseIf oCampo.TipoIntroduccion = TAtributoIntroduccion.Introselec Then
                    'Selecci�n mediante una lista
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleDropDown
                    If oCampo.Tipo = TiposDeAtributos.TipoNumerico Then
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).CellAppearance.TextAlign = UltraGrid.ssAlignRight
                    
                    Else
                        If oCampo.Tipo = TiposDeAtributos.TipoFecha Then
                            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Short Date"
                        End If
                        ssLineas.Bands(0).Columns("C_" & oCampo.Id).CellAppearance.TextAlign = UltraGrid.ssAlignLeft
                    End If
                Else
                    'Atributos de tipo lista externa
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                End If
            End If
        
        Else
            'si no hay permisos de modificaci�n solo podr� ver los botones de archivos y descripci�n detallada:
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
            
            If oCampo.Tipo = TiposDeAtributos.TipoFecha Then
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Short Date"
            ElseIf oCampo.Tipo = TiposDeAtributos.TipoNumerico Then
                ssLineas.Bands(0).Columns("C_" & oCampo.Id).Format = "Standard"
            End If
                        
            If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                If oCampo.CampoGS = TipoCampoSC.ArchivoEspecific Or oCampo.CampoGS = TipoCampoSC.DescrDetallada Or oCampo.CampoGS = TipoCampoGS.Desglose Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                ElseIf oCampo.CampoGS = TipoCampoSC.TotalLineaAdj Or oCampo.CampoGS = TipoCampoSC.TotalLineaPreadj Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                Else
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                End If
            Else
                If oCampo.Tipo = TiposDeAtributos.TipoArchivo Or oCampo.Tipo = TiposDeAtributos.TipoTextoLargo Or oCampo.Tipo = TiposDeAtributos.TipoTextoMedio Or oCampo.Tipo = TiposDeAtributos.TipoEditor Then
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                Else
                    ssLineas.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                End If
            End If
        End If
        
        If ((ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count) < 1000 Then
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Width = 1000
        Else
            ssLineas.Bands(0).Columns("C_" & oCampo.Id).Width = (ssLineas.Width - 340) / g_oCampoDesglose.Desglose.Count
        End If
    Next

    ssLineas.Override.AllowGroupMoving = ssAllowGroupMovingNotAllowed
    ssLineas.Override.AllowColMoving = ssAllowColMovingNotAllowed
    
    If g_bModif = False Then
        ssLineas.Override.AllowUpdate = ssAllowUpdateNo
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_InitializeLayout", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Responde a la pulsacion de teclado: borrar
''' </summary>
''' <param name="KeyCode">tecla pulsada</param>
''' <param name="Shift">si esta pulsado el Shift</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub ssLineas_KeyDown(KeyCode As UltraGrid.SSReturnShort, Shift As Integer)
Dim tArray() As String
Dim i As Byte
Dim blnEnse�arBoton As Boolean
Dim oFila As SSRow
Dim blnBorrame As Boolean
Dim bEdit As Boolean
Dim oCampo As CFormItem
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    On Error Resume Next
    If g_bModif = False Then Exit Sub
    If ssLineas.ActiveCell Is Nothing Then Exit Sub
    If ssLineas.ActiveCell.Value = "" Then Exit Sub
    If ssLineas.Bands(0).Columns.Count < 0 Then Exit Sub
    If Not EscrituraParaDetalleSolicGS(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Then Exit Sub
    
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Desglose Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = idsficticios.numLinea Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoArchivo Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoEditor Then Exit Sub
    
    If KeyCode = vbKeyBack Or KeyCode = vbKeyDelete Then
        '_______________________________________________________________________
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).TablaExterna <> "" Then
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If
            blnEnse�arBoton = True
            ssLineas.ActiveCell.TagVariant = ""
            ssLineas.ActiveCell.Value = ""
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        End If
        '�����������������������������������������������������������������������
    
        If ssLineas.ActiveCell.Column.Style = UltraGrid.ssStyleDropDown Or ssLineas.ActiveCell.Column.Style = UltraGrid.ssStyleEditButton Then
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then Exit Sub
            
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If
            
            'Si borramos el material borraremos tambi�n los art�culos:
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.material Then
              If Not IsNull(ssLineas.ActiveCell.Value) Then
                Dim arrMat As Variant
                Dim oArticulos As CArticulos
                Set oArticulos = oFSGSRaiz.Generar_CArticulos
                
                arrMat = DevolverMaterial(ssLineas.ActiveCell.Value)
                ssLineas.ActiveCell.Value = ""  'esto es para borrar material que a veces no lo hace
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                        If Not IsNull(ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value) Then
                            oArticulos.CargarTodosLosArticulos ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value, , True, , , , arrMat(1), arrMat(2), arrMat(3), arrMat(4)
                            If oArticulos.Count > 0 Then
                                ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                                'Puesta a blanco de la denominaci�n
                                ssLineas.ActiveRow.Cells("C_" & (oCampo.Id + 1)).Value = ""
                                blnBorrame = True
                            End If
                        Else
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                            blnBorrame = True
                        End If
                        'Exit For
                    ElseIf oCampo.CampoGS = TipoCampoGS.CodArticulo Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        blnBorrame = True
                        'Exit For
                    ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                        blnEnse�arBoton = True
                        If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                            Set oFila = ssLineas.ActiveRow
                            m_sValorCTablaExterna = ValorColumTablaExterna(0)
                            Set ssLineas.ActiveRow = oFila
                            If m_sValorCTablaExterna <> "" Then
                                tArray = Split(m_sValorCTablaExterna, "#")
                                For i = LBound(tArray) To UBound(tArray)
                                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                                Next
                            End If
                        End If
                    End If
                Next
             End If
            '--------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Pais Then
                'Si borramos el pa�s borraremos tambi�n la provincia:
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.provincia Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        Exit For
                    End If
                Next
            '--------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.UnidadOrganizativa Then
                    'Si borramos la UNIDAD ORGANIZATIVA borraremos tambi�n el DEPARTAMENTO:
                    For Each oCampo In g_oCampoDesglose.Desglose
                        If oCampo.CampoGS = TipoCampoGS.Departamento And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                            Exit For
                        End If
                    Next
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Proveedor Then
                'Si borramos el campo PROVEEDOR y existe un campo PROVEEDOR ERP lo borraremos
                For Each oCampo In g_oCampoDesglose.Desglose
                    If oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                        ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        Exit For
                    End If
                Next
            '--------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.OrganizacionCompras Then
                    'Si borramos la ORGANIZACION de COMPRAS borraremos tambi�n el CENTRO  y  ALMAC�N:
                    Dim bCentroEliminado As Boolean
                    For Each oCampo In g_oCampoDesglose.Desglose
                        If oCampo.CampoGS = TipoCampoGS.Centro And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                            bCentroEliminado = True
                        ElseIf oCampo.CampoGS = TipoCampoGS.ProveedorERP Then
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        ElseIf oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 2 And bCentroEliminado Then
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                            'Exit For
                        ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                             ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                        End If
                    Next
            '--------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.Centro Then
                    'Si borramos el CENTRO borraremos tambi�n el ALMAC�N:
                    For Each oCampo In g_oCampoDesglose.Desglose
                        If oCampo.CampoGS = TipoCampoGS.Almacen And oCampo.Orden = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Orden + 1 Then
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                            'Exit For
                        ElseIf ((oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo) Or (oCampo.CampoGS = TipoCampoGS.CodArticulo) Or (oCampo.CampoGS = TipoCampoGS.DenArticulo)) Then
                             ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                             blnBorrame = True
                        ElseIf oCampo.TablaExterna <> "" And ssLineas.HasRows = True And blnBorrame Then
                            blnEnse�arBoton = True
                            If m_oTablaExterna.SacarTablaTieneART(oCampo.TablaExterna) Then
                                Set oFila = ssLineas.ActiveRow
                                m_sValorCTablaExterna = ValorColumTablaExterna(0)
                                Set ssLineas.ActiveRow = oFila
                                If m_sValorCTablaExterna <> "" Then
                                    tArray = Split(m_sValorCTablaExterna, "#")
                                    For i = LBound(tArray) To UBound(tArray)
                                        ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                                        ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                                    Next
                                End If
                            End If
                        End If
                    Next
            '--------------------------------------------------------------------------------------------------------------------------------
            ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoMedio Or _
                    g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoLargo Then
                blnEnse�arBoton = True
            End If
            ssLineas.ActiveCell.Value = Null
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        '--------------------------------------------------------------------------------------------------------------------------------
        ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
            bEdit = ssLineas.IsInEditMode
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionExitEditMode
            End If
            
            ssLineas.ActiveCell.Value = ""
            blnEnse�arBoton = True
            Set oFila = ssLineas.ActiveRow
            m_sValorCTablaExterna = ValorColumTablaExterna(0)
            Set ssLineas.ActiveRow = oFila
            If m_sValorCTablaExterna <> "" Then
                tArray = Split(m_sValorCTablaExterna, "#")
                For i = LBound(tArray) To UBound(tArray)
                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).TagVariant = ""
                    ssLineas.ActiveRow.Cells("C_" & tArray(i)).Value = ""
                Next
            End If
            If bEdit = True Then
                ssLineas.PerformAction ssKeyActionEnterEditMode
            End If
        '--------------------------------------------------------------------------------------------------------------------------------
        ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
                bEdit = ssLineas.IsInEditMode
                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionExitEditMode
                End If
                
                ssLineas.ActiveCell.Value = ""

                If bEdit = True Then
                    ssLineas.PerformAction ssKeyActionEnterEditMode
                End If
                
                Dim oCellSiguiente As SSCell
                Set oCellSiguiente = ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.Index - 1)
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.NuevoCodArticulo Then
                    oCellSiguiente.Value = ""
                End If
        End If
    End If
    
    '_______________________________________________________________________
    If blnEnse�arBoton Then
        MostrarBotonesEdicion True, True
        
        If (Me.g_sOrigen = "frmPedidos") Then
            frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
            frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
        ElseIf (Me.g_sOrigen = "frmPROCE") Then
        Else
            frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
            frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
        End If
    End If
    '�����������������������������������������������������������������������
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_KeyDown", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' cargar los permisos y restricciones del usuario
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub ConfigurarSeguridad()
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If basOptimizacion.gTipoDeUsuario <> TipoDeUsuario.Administrador Then
        'Restricciones de destino,material y proveedor:
        If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrDest)) Is Nothing) Then
            m_bRestrDest = True
        End If
        
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrMat)) Is Nothing) Then
            m_bRestrMat = True
        End If
        
        If basOptimizacion.gTipoDeUsuario = TipoDeUsuario.comprador And Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICRestrProve)) Is Nothing) Then
            m_bRestrProve = True
        End If
        
        'Si no es el administrador comprueba los datos del usuario
        Dim oUsuario As CUsuario
        Set oUsuario = oFSGSRaiz.generar_cusuario
        oUsuario.Cod = basOptimizacion.gvarCodUsuario
        oUsuario.ExpandirUsuario
        If oUsuario.FSWSPresUO = True Then
            m_bRestrPresUO = True
        End If
        Set oUsuario = Nothing
        
    End If
    
    'Abrir proceso
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICAbrirProc)) Is Nothing) Then
        m_bAbrirProc = True
    End If
    
    'Pedido directo
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.SOLICPedidoDirecto)) Is Nothing) Then
        m_bPedidoDirecto = True
    End If
    
    Me.cmdAbrirProc.Visible = False
    Me.cmdPedidoDir.Visible = False
    
    If g_bModif = False Then
        If g_sOrigen = "frmVisorSolicitudes" Then
            If g_oInstanciaSeleccionada.Estado = EstadoSolicitud.Cerrada Or g_oInstanciaSeleccionada.Estado = EstadoSolicitud.Anulada Or g_oInstanciaSeleccionada.Estado = EstadoSolicitud.Rechazada Then
                picNavigate.Visible = False
            Else
                If Not m_bAbrirProc Then
                Else
                    Me.cmdAbrirProc.Visible = True
                    Me.cmdAbrirProc.Left = Me.cmdAnyaLinea.Left
                End If
                
                If Not m_bPedidoDirecto Then
                Else
                    Me.cmdPedidoDir.Visible = True
                    If Not m_bAbrirProc Then
                        Me.cmdPedidoDir.Left = Me.cmdAnyaLinea.Left
                    Else
                        Me.cmdPedidoDir.Left = Me.cmdAbrirProc.Left + Me.cmdAbrirProc.Width + 120
                    End If
                End If
                
                Me.cmdAnyaLinea.Visible = False
            End If
        Else
            picNavigate.Visible = False
        End If
    End If
    
    'Abrir procesos multimaterial
    If Not (oUsuarioSummit.Acciones.Item(CStr(AccionesDeSeguridad.APEPermProcMultimaterial)) Is Nothing) Then
        If gParametrosGenerales.gbMultiMaterial Then
            m_bPermProcMultimaterial = True
        Else
            m_bPermProcMultimaterial = False
        End If
    Else
        m_bPermProcMultimaterial = False
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ConfigurarSeguridad", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub


Private Sub CalculoFormulaOrigen()
    Dim scod As String
    Dim oCampo As CFormItem
    Dim oCalculo As CFormItem
    Dim lErrCode As Integer
    Dim lIndex As Integer
    Dim iEq As USPExpression
    Dim sVariables() As String
    Dim dValues() As Double
    Dim i As Integer
    Dim oRow As SSRow
    Dim j As Integer
    Dim vbm As Variant
                    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
  
    Set iEq = New USPExpression
    i = 1
    
    'Almacena las variables en un array:
    ReDim sVariables(g_oCampoDesglose.Formulas.Count)
    ReDim dValues(g_oCampoDesglose.Formulas.Count)
                    
    For Each oCalculo In g_oCampoDesglose.Formulas
        sVariables(i) = oCalculo.IdCalculo
        dValues(i) = 0
        Set oRow = ssLineas.GetRow(ssChildRowFirst)
        Do While Not oRow Is Nothing
            scod = CStr(oRow.Cells("LINEA").Value) & "$" & CStr(g_oCampoDesglose.Id) & "$" & CStr(oCalculo.Id)
            If Not g_oCampoDesglose.LineasDesglose.Item(scod) Is Nothing Then
                If g_oCampoDesglose.LineasDesglose.Item(scod).eliminado <> True Then
                    dValues(i) = dValues(i) + NullToDbl0(g_oCampoDesglose.LineasDesglose.Item(scod).valorNum)
                End If
            End If
            If oRow.HasNextSibling = True Then
                Set oRow = oRow.GetSibling(ssSiblingRowNext)
            Else
                Set oRow = Nothing
            End If
        Loop
        i = i + 1
    Next
    
    For Each oCampo In g_oCampoDesglose.Grupo.CAMPOS
        If Not IsNull(oCampo.IdCalculo) And Not IsNull(oCampo.Formula) And oCampo.OrigenCalcDesglose = g_oCampoDesglose.IdCampoDef Then
            lIndex = iEq.Parse(oCampo.Formula, sVariables, lErrCode)
            If lErrCode = USPEX_NO_ERROR Then
                 oCampo.valorNum = iEq.Evaluate(dValues)
                 'Actualiza en la grid de detalle:
                If (Me.g_sOrigen = "frmPedidos") Then
                   If frmPEDIDOS.g_ofrmDetalleSolic.g_oGrupoSeleccionado.Id = oCampo.Grupo.Id Then
                       
                       For j = 0 To frmPEDIDOS.g_ofrmDetalleSolic.sdbgCampos.Rows - 1
                           vbm = frmPEDIDOS.g_ofrmDetalleSolic.sdbgCampos.AddItemBookmark(j)
                           If CLng(frmPEDIDOS.g_ofrmDetalleSolic.sdbgCampos.Columns("ID").CellValue(vbm)) = CLng(oCampo.Id) Then
                                frmPEDIDOS.g_ofrmDetalleSolic.sdbgCampos.Bookmark = vbm
                                If IsNull(oCampo.valorNum) Then
                                    frmPEDIDOS.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = oCampo.valorNum
                                Else
                                    frmPEDIDOS.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = Format(oCampo.valorNum, "Standard")
                                End If
                                frmPEDIDOS.g_ofrmDetalleSolic.sdbgCampos.Update
                                Exit For
                           End If
                       Next j
                    End If
                ElseIf (Me.g_sOrigen = "frmPROCE") Then
                Else
                    If frmSolicitudes.g_ofrmDetalleSolic.g_oGrupoSeleccionado.Id = oCampo.Grupo.Id Then
                       
                       For j = 0 To frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Rows - 1
                           vbm = frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.AddItemBookmark(j)
                           If CLng(frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("ID").CellValue(vbm)) = CLng(oCampo.Id) Then
                               frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Bookmark = vbm
                               frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = oCampo.valorNum
                               If IsNull(oCampo.valorNum) Then
                                    frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = oCampo.valorNum
                                Else
                                    frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Columns("VALOR").Value = Format(oCampo.valorNum, "Standard")
                                End If
                               frmSolicitudes.g_ofrmDetalleSolic.sdbgCampos.Update
                               Exit For
                           End If
                       Next j
                    End If
                End If
            Else
                 oMensajes.FormulaIncorrecta (m_sIdiErrorEvaluacion(1))
                 Screen.MousePointer = vbNormal
                 Exit Sub
            End If
        End If
    Next
                    
    Set iEq = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CalculoFormulaOrigen", err, Erl, , m_bActivado)
      Exit Sub
   End If
    
End Sub

Public Sub MostrarPresSeleccionado(ByVal sValor As String, ByVal iTipo As Integer)
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then Exit Sub
    
    m_sValorPres = sValor
    ssLineas.ActiveCell.Value = GenerarDenominacionPresupuesto(sValor, iTipo)
    
    'Muestra el bot�n de aplicar y deshacer
    MostrarBotonesEdicion True, True
    
    If (Me.g_sOrigen = "frmPedidos") Then
        frmPEDIDOS.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmPEDIDOS.g_ofrmDetalleSolic.picControl.Visible = False
    ElseIf (Me.g_sOrigen = "frmPROCE") Then
    Else
        frmSolicitudes.g_ofrmDetalleSolic.picEdicion.Visible = True
        frmSolicitudes.g_ofrmDetalleSolic.picControl.Visible = False
    End If
    
    Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "MostrarPresSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Function ObtenerPresupAsignado(ByVal sValor As Variant) As Double
Dim arrPresupuestos() As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
arrPresupuestos = Split(sValor, "#")

Dim oPresup As Variant
Dim arrPresup As Variant
Dim dPorcent As Double

For Each oPresup In arrPresupuestos
    arrPresup = Split(oPresup, "_")

    dPorcent = dPorcent + Numero(arrPresup(2), ".")
Next

ObtenerPresupAsignado = dPorcent * 100
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ObtenerPresupAsignado", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function


Public Sub MostrarUOSeleccionada()

Dim oCellSiguiente As SSCell
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If frmSolicitudDesglose.m_sUON3Sel <> "" Then
        ssLineas.ActiveCell.Value = frmSolicitudDesglose.m_sUON1Sel & " - " & frmSolicitudDesglose.m_sUON2Sel & " - " & frmSolicitudDesglose.m_sUON3Sel & " - " & frmSolicitudDesglose.m_sUODescrip
    ElseIf frmSolicitudDesglose.m_sUON2Sel <> "" Then
        ssLineas.ActiveCell.Value = frmSolicitudDesglose.m_sUON1Sel & " - " & frmSolicitudDesglose.m_sUON2Sel & " - " & frmSolicitudDesglose.m_sUODescrip
    ElseIf frmSolicitudDesglose.m_sUON1Sel <> "" Then
        ssLineas.ActiveCell.Value = frmSolicitudDesglose.m_sUON1Sel & " - " & frmSolicitudDesglose.m_sUODescrip
    ElseIf frmSolicitudDesglose.m_sUODescrip <> "" Then
        ssLineas.ActiveCell.Value = frmSolicitudDesglose.m_sUODescrip
    End If
   
   If (ssLineas.ActiveRow.Cells.Count - 1) > 1 Then
        Set oCellSiguiente = ssLineas.ActiveRow.Cells(ssLineas.ActiveCell.Column.Index + 1)
   
        'Se borra el contenido del departamento
        If Not g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))) Is Nothing Then
            If g_oCampoDesglose.Desglose.Item(CStr(Mid$(oCellSiguiente.Column.key, 3))).CampoGS = TipoCampoGS.Departamento Then
            
                oCellSiguiente.Value = ""
            End If
        End If
    End If

    ssLineas.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "MostrarUOSeleccionada", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub

Private Function esGenerico(sCodArt As String, Optional bAnyadirArt As Boolean = False) As Boolean

    Dim oArticulos As CArticulos
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oArticulos = oFSGSRaiz.Generar_CArticulos

    oArticulos.CargarTodosLosArticulos sCodArt, , True
    
    If oArticulos.Count > 0 Then
        esGenerico = CBool(oArticulos.Item(1).Generico)

    Else
        If bAnyadirArt Then
            esGenerico = True
        Else
            esGenerico = False
        End If
    End If
    
    Set oArticulos = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "esGenerico", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function

''' <summary>
''' Control para que los tipos TipoTextoCorto, TipoTextoMedio y TipoTextoLargo no superen el m�ximo permitido
''' Control para que las ceoldas LINEA no sean editables
''' </summary>
''' <param name="KeyAscii">tecla pulsada</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub ssLineas_KeyPress(KeyAscii As UltraGrid.SSReturnShort)
Dim vMaxLength As Variant

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If KeyAscii = vbKeyBack Then Exit Sub
    If KeyAscii = vbKeyEscape Then Exit Sub
    If (ssLineas.ActiveCell Is Nothing) Or IsNull(ssLineas.ActiveCell) Then Exit Sub
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))) Is Nothing Then Exit Sub
    
    'Control para que los tipos TipoTextoCorto, TipoTextoMedio y TipoTextoLargo no superen el m�ximo permitido
    If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = TipoCampoGS.DenArticulo Then
        vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
        If IsNull(vMaxLength) Or vMaxLength = "" Then
            vMaxLength = basParametros.gLongitudesDeCodigos.giLongCodDENART
        End If
        
        If (Len(ssLineas.ActiveCell.GetText) >= CInt(vMaxLength)) And ssLineas.ActiveCell.SelLength = 0 Then
            KeyAscii = 0
        End If
    ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).CampoGS = idsficticios.numLinea Then
        KeyAscii = 0
    Else
        If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoMedio Or g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoLargo Then
            vMaxLength = g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).MaxLength
            If IsNull(vMaxLength) Or vMaxLength = "" Then
                If g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoCorto Then
                    vMaxLength = 100
                ElseIf g_oCampoDesglose.Desglose.Item(CStr(Mid$(ssLineas.ActiveCell.Column.key, 3))).Tipo = TiposDeAtributos.TipoTextoMedio Then
                    vMaxLength = 800
                Else
                    vMaxLength = -1
                End If
            End If
            If ssLineas.ActiveCell.GetText <> "" Then
                If CInt(vMaxLength) > 0 And (Len(ssLineas.ActiveCell.GetText) >= CInt(vMaxLength)) And ssLineas.ActiveCell.SelLength = 0 Then
                    KeyAscii = 0
                End If
            End If
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ssLineas_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub



Private Function ExisteAtributoArticulo(piLinea As Integer) As Boolean
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim intContLineas As Integer
Dim strArticulo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    intContLineas = 0
    'ssLineas.ActiveRow.Cells("LINEA").Value = piLinea


    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                intContLineas = intContLineas + 1
                strArticulo = "" & ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value
                If piLinea = intContLineas And strArticulo <> "" Then
                    ExisteAtributoArticulo = True
                    Exit Function
                Else
                    ExisteAtributoArticulo = False
                End If
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ExisteAtributoArticulo", err, Erl, , m_bActivado)
      Exit Function
   End If
End Function



Private Function DevuelveIdArticulo(piLinea As Integer) As String
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim intContLineas As Integer
Dim strArticulo As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    intContLineas = 0

    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.CampoGS = TipoCampoGS.CodArticulo Or oCampo.CampoGS = TipoCampoGS.NuevoCodArticulo Then
                intContLineas = intContLineas + 1
                strArticulo = "" & ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value
                If piLinea = intContLineas And strArticulo <> "" Then
                    DevuelveIdArticulo = strArticulo
                    Exit Function
                Else
                    DevuelveIdArticulo = ""
                End If
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "DevuelveIdArticulo", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Function
   End If
End Function


'Es necesario para el posicionamiento en frmTablasExternas
'y ponemos bien el value
Private Sub CargarTagTablasExternas()
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim sValorCampo
Dim tArray() As String
Dim intTabla As Integer
Dim strNombreTabla As String, strPK As String
Dim TipoDeDatoPK As TiposDeAtributos
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.TablaExterna <> "" Then
                intTabla = CInt(oCampo.TablaExterna)
                strNombreTabla = m_oTablaExterna.SacarNombreTabla(intTabla)
                strPK = m_oTablaExterna.SacarPK(intTabla)
                TipoDeDatoPK = m_oTablaExterna.SacarTipoDeDatoPK(strNombreTabla, strPK)
                '_______________________________________________________________________
                sValorCampo = ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value
                If Trim(sValorCampo) <> "#" Then
                    tArray = Split(sValorCampo, "#")
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = tArray(0)
                    Select Case TipoDeDatoPK
                        Case TipoTextoMedio
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = tArray(1)
                        Case TipoFecha
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CDate(tArray(1))
                        Case TipoNumerico
                            ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = CLng(tArray(1))
                    End Select
                Else
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).Value = ""
                    ssLineas.ActiveRow.Cells("C_" & oCampo.Id).TagVariant = ""
                End If
                '�����������������������������������������������������������������������
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
    ssLineas.Update
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CargarTagTablasExternas", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

'Solo habra una tabla externa sobre articulo; de ahi el exit sub
'm_ValorColumTablaExterna

Private Function ValorColumTablaExterna(pLinea) As String
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim intTabla As Integer
Dim strCampos As String

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If oFSGSRaiz.fg_bProgramando Then On Error GoTo ERROR_Frm
    'Set oRow = ssLineas.GetRow(ssChildRowFirst)
    Set oRow = ssLineas.GetRow(pLinea)
    Do While Not oRow Is Nothing
        ssLineas.ActiveRow = oRow
        For Each oCampo In g_oCampoDesglose.Desglose
            If oCampo.TablaExterna <> "" Then
                intTabla = CInt(oCampo.TablaExterna)
                If m_oTablaExterna.SacarTablaTieneART(intTabla) Then
                    If strCampos = "" Then
                        strCampos = CStr(oCampo.Id)
                    Else
                        strCampos = strCampos & "#" & CStr(oCampo.Id)
                    End If
                End If
            End If
        Next
        If oRow.HasNextSibling = True Then
            Set oRow = oRow.GetSibling(ssSiblingRowNext)
        Else
            Set oRow = Nothing
        End If
    Loop
    ValorColumTablaExterna = strCampos
    Exit Function
ERROR_Frm:
    ValorColumTablaExterna = ""
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "ValorColumTablaExterna", err, Erl, Me, m_bActivado, m_sMsgError)
      GoTo ERROR_Frm
      Exit Function
   End If
End Function
    

'Funcion que devuelve el valor de esa linea del desglose del campo Gs que se pasa por parametro
Private Function DevuelveValueCampoGS(tipoGs As TipoCampoGS) As Variant
Dim oLineaDesglose As CLineaDesglose
Dim oCampo As CFormItem
Dim oRow As SSRow
Dim ValueCampoGS As Variant


If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Function
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oRow = ssLineas.ActiveRow
    
    ssLineas.ActiveRow = oRow
    'Comprobamos que el valor este en la linea del desglose
    For Each oLineaDesglose In g_oCampoDesglose.LineasDesglose
        If oLineaDesglose.CampoHijo.CampoGS = tipoGs And oRow.Cells("LINEA").Value = oLineaDesglose.Linea Then
            Select Case oLineaDesglose.CampoHijo.Tipo
                Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                    ValueCampoGS = NullToStr(oLineaDesglose.valorText)
                Case TiposDeAtributos.TipoBoolean
                    ValueCampoGS = oLineaDesglose.valorBool
                Case TiposDeAtributos.TipoNumerico
                    ValueCampoGS = NullToDbl0(oLineaDesglose.valorNum)
                Case TiposDeAtributos.TipoFecha
                    ValueCampoGS = oLineaDesglose.valorFec
            End Select
            Exit For
        End If
    Next
    'Si no se encuentra el valor en la linea del desglose, segun que campoGS sea,
    'se mirara si tiene valor a nivel de formulario en el grupo del desglose
    If ValueCampoGS = Empty Then
        Select Case tipoGs
            Case TipoCampoGS.OrganizacionCompras
                For Each oCampo In g_oCampoDesglose.Grupo.CAMPOS
                    If oCampo.CampoGS = tipoGs Then
                        Select Case oCampo.Tipo
                            Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                ValueCampoGS = NullToStr(oCampo.valorText)
                            Case TiposDeAtributos.TipoBoolean
                                ValueCampoGS = oCampo.valorBool
                            Case TiposDeAtributos.TipoNumerico
                                ValueCampoGS = NullToDbl0(oCampo.valorNum)
                            Case TiposDeAtributos.TipoFecha
                                ValueCampoGS = oCampo.valorFec
                        End Select
                        Exit For
                    End If
                Next
        End Select
    End If
    
    'Si no se ha encontrado en los campos del formulario del grupo seleccionado, ni en el propio desglose
    'se buscaria en el resto de grupos
    If ValueCampoGS = Empty Then
        Dim g As CFormGrupo
        Dim c As CFormItem
        
        If Not g_oInstanciaSeleccionada Is Nothing Then
            If g_oInstanciaSeleccionada.Grupos Is Nothing Then
                g_oInstanciaSeleccionada.CargarGrupos
            End If
            For Each g In g_oInstanciaSeleccionada.Grupos
                If g.CAMPOS Is Nothing Then
                    g.CargarTodosLosCampos
                End If
                
                If Not g.CAMPOS Is Nothing And Not g.Id = g_oCampoDesglose.Grupo.Id Then
                    For Each c In g.CAMPOS
                        If c.CampoGS = tipoGs Then
                            Select Case c.Tipo
                                Case TiposDeAtributos.TipoString, TiposDeAtributos.TipoTextoCorto, TiposDeAtributos.TipoTextoLargo, TiposDeAtributos.TipoTextoMedio
                                    ValueCampoGS = NullToStr(c.valorText)
                                Case TiposDeAtributos.TipoBoolean
                                    ValueCampoGS = c.valorBool
                                Case TiposDeAtributos.TipoNumerico
                                    ValueCampoGS = NullToDbl0(c.valorNum)
                                Case TiposDeAtributos.TipoFecha
                                    ValueCampoGS = c.valorFec
                            End Select
                            Exit For
                        End If
                    Next
                End If
            Next
        End If
    End If
    
    DevuelveValueCampoGS = ValueCampoGS
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "DevuelveValueCampoGS", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Function
   End If
End Function

''' <summary>
''' Cancela la activaci�n de las celdas del grid de solo consulta
''' </summary>
''' <param name="Cell">Celda</param>
''' <param name="Cancel">Cancelar la activacion</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSLineasTab_BeforeCellActivate(ByVal Cell As UltraGrid.SSCell, ByVal Cancel As UltraGrid.SSReturnBoolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSLineasTab_BeforeCellActivate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Cancela el update de las celdas del grid de solo consulta
''' </summary>
''' <param name="Cell">Celda</param>
''' <param name="NewValue">New Value de la celda</param>
''' <param name="Cancel">Cancelar el update</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSLineasTab_BeforeCellUpdate(ByVal Cell As UltraGrid.SSCell, NewValue As Variant, ByVal Cancel As UltraGrid.SSReturnBoolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSLineasTab_BeforeCellUpdate", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cancela el delete de las filas del grid de solo consulta
''' </summary>
''' <param name="Rows">Rows seleccionadas</param>
''' <param name="DisplayPromptMsg">si saca mensaje antes de borrar</param>
''' <param name="Cancel">Cancelar el delete</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSLineasTab_BeforeRowsDeleted(ByVal Rows As UltraGrid.SSSelectedRows, ByVal DisplayPromptMsg As UltraGrid.SSReturnBoolean, ByVal Cancel As UltraGrid.SSReturnBoolean)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Cancel = True
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSLineasTab_BeforeRowsDeleted", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Llevar a la apertura la linea seleccionada
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSLineasTab_DblClick()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    frmPROCE.PonerLineaSolicitudSeleccionada g_oInstanciaSeleccionada.Id, g_oCampoDesglose.IdCampoDef, SSLineasTab.ActiveRow.Cells("LINEA").Value
    Unload Me
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSLineasTab_DblClick", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub
''' <summary>
''' Cancelar el mensaje de "se ha producido un error"
''' </summary>
''' <param name="ErrorInfo">ErrorInfo</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSLineasTab_Error(ByVal ErrorInfo As UltraGrid.SSError)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ErrorInfo.DisplayErrorDialog = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSLineasTab_Error", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Configura el aspecto del grid
''' </summary>
''' <param name="Context">Contexto del Layout</param>
''' <param name="Layout">Layout</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSLineasTab_InitializeLayout(ByVal Context As UltraGrid.Constants_Context, ByVal Layout As UltraGrid.SSLayout)
    Dim oCampo As CFormItem
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    SSLineasTab.Override.DefaultRowHeight = 239
    
    SSLineasTab.Bands(0).Columns("LINEA").Header.caption = m_sNumLinea
    
    For Each oCampo In g_oCampoDesglose.Desglose
        If oCampo.CampoGS = idsficticios.numLinea Then
            SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Hidden = True
        Else
            SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Header.caption = NullToStr(oCampo.Denominaciones.Item(CStr(gParametrosInstalacion.gIdioma)).Den)
        End If
        
        'si no hay permisos de modificaci�n solo podr� ver los botones de archivos y descripci�n detallada:
        If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
            'si no hay permisos de modificaci�n solo podr� ver los botones de archivos y descripci�n detallada:
            If oCampo.TipoPredef = TipoCampoPredefinido.CampoGS Then
                If oCampo.CampoGS = TipoCampoSC.ArchivoEspecific Or oCampo.CampoGS = TipoCampoSC.DescrDetallada Or oCampo.CampoGS = TipoCampoGS.Desglose Then
                    SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                    SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                Else
                    SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                End If
            Else
                If oCampo.Tipo = TiposDeAtributos.TipoArchivo Or oCampo.Tipo = TiposDeAtributos.TipoTextoLargo Or oCampo.Tipo = TiposDeAtributos.TipoTextoMedio Or oCampo.Tipo = TiposDeAtributos.TipoEditor Then
                    SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEditButton
                    SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Activation = UltraGrid.ssActivationActivateOnly
                Else
                    SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Style = UltraGrid.ssStyleEdit
                End If
            End If
        End If
        
        If ((SSLineasTab.Width - 340) / g_oCampoDesglose.Desglose.Count) < 1000 Then
            SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Width = 1000
        Else
            SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Width = (SSLineasTab.Width - 340) / g_oCampoDesglose.Desglose.Count
        End If
        
        If oCampo.CampoGS = idsficticios.numLinea Then SSLineasTab.Bands(0).Columns("LINEA").Width = SSLineasTab.Bands(0).Columns("C_" & oCampo.Id).Width
    Next
    
    SSLineasTab.Override.AllowGroupMoving = ssAllowGroupMovingNotAllowed
    SSLineasTab.Override.AllowColMoving = ssAllowColMovingNotAllowed
    SSLineasTab.Override.AllowUpdate = ssAllowUpdateNo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSLineasTab_InitializeLayout", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Impide la escritura en grid de solo consulta
''' </summary>
''' <param name="KeyAscii">tecla pulsada</param>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSLineasTab_KeyPress(KeyAscii As UltraGrid.SSReturnShort)
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    KeyAscii = 0
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSLineasTab_KeyPress", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Cambia al desglose q es visible.
''' </summary>
''' <remarks>Llamada desde: Sistema ; Tiempo m�ximo: 0,2</remarks>
Private Sub SSTabGrupos_Click()
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Me.SSLineasTab.Update

    Set g_oCampoDesglose = g_oInstanciaSeleccionada.Desgloses.Item(ssTabGrupos.selectedItem.Index)

    If g_oCampoDesglose Is Nothing Then Exit Sub
    
    DesgloseSeleccionado
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "SSTabGrupos_Click", err, Erl, Me, m_bActivado, m_sMsgError)
      Exit Sub
   End If
End Sub

''' <summary>
''' Carga al desglose seleccionado.
''' </summary>
''' <remarks>Llamada desde: SSTabGrupos_Click ; Tiempo m�ximo: 0,2</remarks>
Private Sub DesgloseSeleccionado()
    Dim oLinea As CLineaDesglose
    
    If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
    If m_bDescargarFrm Then
        Exit Sub
    End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If ssTabGrupos.selectedItem.Tag <> "" Then
        g_oCampoDesglose.CargarDesglose False, True
        Set SSLineasTab.DataSource = g_oCampoDesglose.DevolverLineasDesglose(m_sIdiTrue, m_sIdiFalse)
        
        For Each oLinea In g_oCampoDesglose.LineasDesglose
            If m_iMaxLinBd < oLinea.Linea Then m_iMaxLinBd = oLinea.Linea
        Next
        
        SacarColorRiesgo SSLineasTab
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "DesgloseSeleccionado", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
''' <summary>
''' Carga las pesta�as y el numero de grid q va a contener la pantalla
''' </summary>
''' <param name="bVarios">Indica el numero de grid q va a contener la pantalla</param>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargarTabs(ByRef bVarios As Boolean)
    Dim Num As Integer
    Dim oDesglose As CFormItem
    Dim iTab As Integer
    
If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    bVarios = True
    
    Num = g_oInstanciaSeleccionada.CargarYDevolverNumeroDesgloses

    'A�ade un tab para cada grupo:
    ssTabGrupos.Tabs.clear
    iTab = 1
    For Each oDesglose In g_oInstanciaSeleccionada.Desgloses
        ssTabGrupos.Tabs.Add iTab, "A" & oDesglose.Id, oDesglose.Denominaciones.Item(gParametrosInstalacion.gIdioma).Den      'si no es multiidioma ser� la misma para todos
        ssTabGrupos.Tabs(iTab).Tag = CStr(oDesglose.Id)
        iTab = iTab + 1
    Next
    If ssTabGrupos.Tabs.Count > 0 Then
        ssTabGrupos.Tabs(1).Selected = True
    End If

    bVarios = (Num > 1)
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CargarTabs", err, Erl, , m_bActivado)
      Exit Sub
   End If

End Sub
''' <summary>
''' Carga el desglose seleccionado
''' </summary>
''' <remarks>Llamada desde: Form_Load ; Tiempo m�ximo: 0,2</remarks>
Private Sub CargaSinTabs()
Dim oRow As SSRow

If Not oFSGSRaiz.fg_bProgramando Then On Error GoTo Error
   If m_bDescargarFrm Then
      Exit Sub
   End If
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    CargarValoresDesglose
    
    '_______________________
    CargarTagTablasExternas
    '�����������������������
    
    'Carga las f�rmulas por si hay campos calculados:
    If basParametros.gParametrosGenerales.gsAccesoFSWS = AccesoFSWSCompleto Then
        g_oCampoDesglose.CargarCamposCalculadosDesglose
    End If
    
    'Sacamos el riesgo
    SacarColorRiesgo ssLineas
                                                  
    Set oRow = ssLineas.GetRow(ssChildRowFirst)
    ssLineas.ActiveRow = oRow
        
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If err.Number <> 0 Then
      m_bDescargarFrm = oFSGSRaiz.TratarError("Form", "frmSolicitudDesglose", "CargaSinTabs", err, Erl, , m_bActivado)
      Exit Sub
   End If
End Sub

Private Sub MostrarBotonesEdicion(ByVal bAplicarVisible As Boolean, ByVal bDeshacerVisible As Boolean)
    cmdAplicar.Visible = bAplicarVisible
    cmdDeshacer.Visible = bDeshacerVisible
    
    If Not bAplicarVisible Then
        If Not cmdMultiFila.Visible = True Then
            cmdDeshacer.Left = cmdAplicar.Left
        Else
            cmdDeshacer.Left = cmdMultiFila.Left + cmdMultiFila.Width + 85
        End If
    Else
        If Not cmdMultiFila.Visible = True Then
            cmdAplicar.Left = cmdMultiFila.Left
            cmdDeshacer.Left = cmdAplicar.Left + cmdAplicar.Width + 85
        Else
            cmdAplicar.Left = cmdMultiFila.Left + cmdMultiFila.Width + 85
            cmdDeshacer.Left = cmdAplicar.Left + cmdAplicar.Width + 85
        End If
    End If
    cmdAbrirProc.Left = cmdDeshacer.Left + cmdDeshacer.Width + 85
    cmdPedidoDir.Left = cmdAbrirProc.Left + cmdAbrirProc.Width + 85
End Sub

'''Muestra el bot�n de copiar multifila y desplaza hacia la derecha el resto de botones.
Private Sub MostrarBotonMultifila()
    cmdMultiFila.Visible = True
    If cmdAplicar.Visible = True Then
        cmdAplicar.Left = cmdMultiFila.Left + cmdMultiFila.Width + 85
        cmdDeshacer.Left = cmdAplicar.Left + cmdAplicar.Width + 85
    ElseIf cmdDeshacer.Visible = True Then
        cmdDeshacer.Left = cmdMultiFila.Left + cmdMultiFila.Width + 85
    End If
End Sub

Private Sub OcultarBotonMultifila()
    If cmdAplicar.Visible = True Then
        cmdAplicar.Left = cmdMultiFila.Left
        cmdDeshacer.Left = cmdAplicar.Left + cmdAplicar.Width + 85
    ElseIf cmdDeshacer.Visible = True Then
        cmdDeshacer.Left = cmdMultiFila.Left
    End If
End Sub

Private Function EscrituraParaDetalleSolicGS(ByVal lID As Long) As Boolean
    If g_sOrigen = "frmSolicitudes" Then
        EscrituraParaDetalleSolicGS = g_oCumplimentacionesGS.Item(CStr(lID)).ESCRITURA
    Else
        'Por ejemplo: Pedidos. El mantenimiento 13462 (GS. Detalle de solicitud. Aplicar la cumplimentaci�n de los campos del formulario.) no aplica
        EscrituraParaDetalleSolicGS = True
    End If
End Function
